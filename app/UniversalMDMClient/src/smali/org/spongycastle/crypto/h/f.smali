.class public Lorg/spongycastle/crypto/h/f;
.super Ljava/lang/Object;
.source "EAXBlockCipher.java"

# interfaces
.implements Lorg/spongycastle/crypto/h/a;


# instance fields
.field private Ws:I

.field private Wt:Z

.field private aaa:Lorg/spongycastle/crypto/o;

.field private abG:I

.field private abL:I

.field private ach:[B

.field private acj:Lorg/spongycastle/crypto/h/l;

.field private ack:[B

.field private acl:[B

.field private acm:[B


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/d;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-interface {p1}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    .line 59
    new-instance v0, Lorg/spongycastle/crypto/g/c;

    invoke-direct {v0, p1}, Lorg/spongycastle/crypto/g/c;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    .line 60
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/f;->ach:[B

    .line 61
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    .line 62
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/f;->acl:[B

    .line 63
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/f;->ack:[B

    .line 64
    new-instance v0, Lorg/spongycastle/crypto/h/l;

    invoke-direct {v0, p1}, Lorg/spongycastle/crypto/h/l;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    .line 65
    return-void
.end method

.method private a(B[BI)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 264
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v2, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    aput-byte p1, v0, v2

    .line 266
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    array-length v2, v2

    if-ne v0, v2, :cond_1

    .line 270
    iget-boolean v0, p0, Lorg/spongycastle/crypto/h/f;->Wt:Z

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    invoke-virtual {v0, v2, v1, p2, p3}, Lorg/spongycastle/crypto/h/l;->a([BI[BI)I

    move-result v0

    .line 274
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget v3, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-interface {v2, p2, p3, v3}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 283
    :goto_0
    iget v2, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    iput v2, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    .line 284
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v3, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    iget-object v4, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v5, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-static {v2, v3, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 289
    :goto_1
    return v0

    .line 278
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v3, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-interface {v0, v2, v1, v3}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 280
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    invoke-virtual {v0, v2, v1, p2, p3}, Lorg/spongycastle/crypto/h/l;->a([BI[BI)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 289
    goto :goto_1
.end method

.method private qF()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 134
    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    new-array v1, v1, [B

    .line 135
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v2, v1, v0}, Lorg/spongycastle/crypto/o;->doFinal([BI)I

    .line 137
    :goto_0
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->ach:[B

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 139
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->ach:[B

    iget-object v3, p0, Lorg/spongycastle/crypto/h/f;->ack:[B

    aget-byte v3, v3, v0

    iget-object v4, p0, Lorg/spongycastle/crypto/h/f;->acl:[B

    aget-byte v4, v4, v0

    xor-int/2addr v3, v4

    aget-byte v4, v1, v0

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :cond_0
    return-void
.end method

.method private r([BI)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 294
    move v0, v1

    :goto_0
    iget v2, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    if-ge v0, v2, :cond_1

    .line 296
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->ach:[B

    aget-byte v2, v2, v0

    add-int v3, p2, v0

    aget-byte v3, p1, v3

    if-eq v2, v3, :cond_0

    .line 302
    :goto_1
    return v1

    .line 294
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 302
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private reset(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 151
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/h/l;->reset()V

    .line 152
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0}, Lorg/spongycastle/crypto/o;->reset()V

    .line 154
    iput v3, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    .line 155
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    invoke-static {v0, v3}, Lorg/spongycastle/util/a;->fill([BB)V

    .line 157
    if-eqz p1, :cond_0

    .line 159
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->ach:[B

    invoke-static {v0, v3}, Lorg/spongycastle/util/a;->fill([BB)V

    .line 162
    :cond_0
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    new-array v0, v0, [B

    .line 163
    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x2

    aput-byte v2, v0, v1

    .line 164
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget v2, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-interface {v1, v0, v3, v2}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 165
    return-void
.end method


# virtual methods
.method public doFinal([BI)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 189
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    .line 190
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    array-length v1, v1

    new-array v1, v1, [B

    .line 192
    iput v6, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    .line 194
    iget-boolean v2, p0, Lorg/spongycastle/crypto/h/f;->Wt:Z

    if-eqz v2, :cond_0

    .line 196
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    invoke-virtual {v2, v3, v6, v1, v6}, Lorg/spongycastle/crypto/h/l;->a([BI[BI)I

    .line 197
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v4, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    iget v5, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-virtual {v2, v3, v4, v1, v5}, Lorg/spongycastle/crypto/h/l;->a([BI[BI)I

    .line 199
    invoke-static {v1, v6, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 201
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v2, v1, v6, v0}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 203
    invoke-direct {p0}, Lorg/spongycastle/crypto/h/f;->qF()V

    .line 205
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->ach:[B

    add-int v2, p2, v0

    iget v3, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    invoke-static {v1, v6, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    invoke-direct {p0, v6}, Lorg/spongycastle/crypto/h/f;->reset(Z)V

    .line 209
    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    add-int/2addr v0, v1

    .line 232
    :goto_0
    return v0

    .line 213
    :cond_0
    iget v2, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    if-le v0, v2, :cond_1

    .line 215
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v4, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    sub-int v4, v0, v4

    invoke-interface {v2, v3, v6, v4}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 217
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    invoke-virtual {v2, v3, v6, v1, v6}, Lorg/spongycastle/crypto/h/l;->a([BI[BI)I

    .line 218
    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v4, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    iget v5, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-virtual {v2, v3, v4, v1, v5}, Lorg/spongycastle/crypto/h/l;->a([BI[BI)I

    .line 220
    iget v2, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    sub-int v2, v0, v2

    invoke-static {v1, v6, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 223
    :cond_1
    invoke-direct {p0}, Lorg/spongycastle/crypto/h/f;->qF()V

    .line 225
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->acm:[B

    iget v2, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    sub-int v2, v0, v2

    invoke-direct {p0, v1, v2}, Lorg/spongycastle/crypto/h/f;->r([BI)Z

    move-result v1

    if-nez v1, :cond_2

    .line 227
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string v1, "mac check in EAX failed"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_2
    invoke-direct {p0, v6}, Lorg/spongycastle/crypto/h/f;->reset(Z)V

    .line 232
    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getOutputSize(I)I
    .locals 2

    .prologue
    .line 252
    iget-boolean v0, p0, Lorg/spongycastle/crypto/h/f;->Wt:Z

    if-eqz v0, :cond_0

    .line 254
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    add-int/2addr v0, p1

    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    add-int/2addr v0, v1

    .line 258
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    add-int/2addr v0, p1

    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getUnderlyingCipher()Lorg/spongycastle/crypto/d;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/h/l;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateOutputSize(I)I
    .locals 2

    .prologue
    .line 247
    iget v0, p0, Lorg/spongycastle/crypto/h/f;->Ws:I

    add-int/2addr v0, p1

    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    div-int/2addr v0, v1

    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 85
    iput-boolean p1, p0, Lorg/spongycastle/crypto/h/f;->Wt:Z

    .line 90
    instance-of v0, p2, Lorg/spongycastle/crypto/j/a;

    if-eqz v0, :cond_0

    .line 92
    check-cast p2, Lorg/spongycastle/crypto/j/a;

    .line 94
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qI()[B

    move-result-object v2

    .line 95
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qH()[B

    move-result-object v1

    .line 96
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qc()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    .line 97
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qG()Lorg/spongycastle/crypto/j/ak;

    move-result-object v0

    .line 113
    :goto_0
    iget v3, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    new-array v3, v3, [B

    .line 115
    iget-object v4, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v4, v0}, Lorg/spongycastle/crypto/o;->a(Lorg/spongycastle/crypto/h;)V

    .line 116
    iget v4, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    add-int/lit8 v4, v4, -0x1

    aput-byte v7, v3, v4

    .line 117
    iget-object v4, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget v5, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-interface {v4, v3, v6, v5}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 118
    iget-object v4, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    array-length v5, v1

    invoke-interface {v4, v1, v6, v5}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 119
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget-object v4, p0, Lorg/spongycastle/crypto/h/f;->acl:[B

    invoke-interface {v1, v4, v6}, Lorg/spongycastle/crypto/o;->doFinal([BI)I

    .line 121
    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    add-int/lit8 v1, v1, -0x1

    aput-byte v6, v3, v1

    .line 122
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget v4, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-interface {v1, v3, v6, v4}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 123
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    array-length v4, v2

    invoke-interface {v1, v2, v6, v4}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 124
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/f;->ack:[B

    invoke-interface {v1, v2, v6}, Lorg/spongycastle/crypto/o;->doFinal([BI)I

    .line 126
    iget v1, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x2

    aput-byte v2, v3, v1

    .line 127
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    iget v2, p0, Lorg/spongycastle/crypto/h/f;->abL:I

    invoke-interface {v1, v3, v6, v2}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 129
    iget-object v1, p0, Lorg/spongycastle/crypto/h/f;->acj:Lorg/spongycastle/crypto/h/l;

    new-instance v2, Lorg/spongycastle/crypto/j/an;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/f;->ack:[B

    invoke-direct {v2, v0, v3}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    invoke-virtual {v1, v7, v2}, Lorg/spongycastle/crypto/h/l;->init(ZLorg/spongycastle/crypto/h;)V

    .line 130
    return-void

    .line 99
    :cond_0
    instance-of v0, p2, Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_1

    .line 101
    check-cast p2, Lorg/spongycastle/crypto/j/an;

    .line 103
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->getIV()[B

    move-result-object v2

    .line 104
    new-array v1, v6, [B

    .line 105
    iget-object v0, p0, Lorg/spongycastle/crypto/h/f;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/spongycastle/crypto/h/f;->abG:I

    .line 106
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid parameters passed to EAX"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public processByte(B[BI)I
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0, p1, p2, p3}, Lorg/spongycastle/crypto/h/f;->a(B[BI)I

    move-result v0

    return v0
.end method

.method public processBytes([BII[BI)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 176
    move v1, v0

    .line 178
    :goto_0
    if-eq v0, p3, :cond_0

    .line 180
    add-int v2, p2, v0

    aget-byte v2, p1, v2

    add-int v3, p5, v1

    invoke-direct {p0, v2, p4, v3}, Lorg/spongycastle/crypto/h/f;->a(B[BI)I

    move-result v2

    add-int/2addr v1, v2

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_0
    return v1
.end method
