.class public Lorg/spongycastle/crypto/a/a;
.super Ljava/lang/Object;
.source "DHBasicAgreement.java"

# interfaces
.implements Lorg/spongycastle/crypto/c;


# instance fields
.field private Wy:Lorg/spongycastle/crypto/j/h;

.field private Wz:Lorg/spongycastle/crypto/j/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/crypto/h;)V
    .locals 2

    .prologue
    .line 31
    instance-of v0, p1, Lorg/spongycastle/crypto/j/ao;

    if-eqz v0, :cond_0

    .line 33
    check-cast p1, Lorg/spongycastle/crypto/j/ao;

    .line 34
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/ao;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/crypto/j/b;

    .line 41
    :goto_0
    instance-of v1, v0, Lorg/spongycastle/crypto/j/h;

    if-nez v1, :cond_1

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DHEngine expects DHPrivateKeyParameters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    check-cast p1, Lorg/spongycastle/crypto/j/b;

    move-object v0, p1

    goto :goto_0

    .line 46
    :cond_1
    check-cast v0, Lorg/spongycastle/crypto/j/h;

    iput-object v0, p0, Lorg/spongycastle/crypto/a/a;->Wy:Lorg/spongycastle/crypto/j/h;

    .line 47
    iget-object v0, p0, Lorg/spongycastle/crypto/a/a;->Wy:Lorg/spongycastle/crypto/j/h;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/h;->qJ()Lorg/spongycastle/crypto/j/g;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/a/a;->Wz:Lorg/spongycastle/crypto/j/g;

    .line 48
    return-void
.end method

.method public b(Lorg/spongycastle/crypto/h;)Ljava/math/BigInteger;
    .locals 3

    .prologue
    .line 57
    check-cast p1, Lorg/spongycastle/crypto/j/i;

    .line 59
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/i;->qJ()Lorg/spongycastle/crypto/j/g;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/crypto/a/a;->Wz:Lorg/spongycastle/crypto/j/g;

    invoke-virtual {v0, v1}, Lorg/spongycastle/crypto/j/g;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Diffie-Hellman public key has wrong parameters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/i;->getY()Ljava/math/BigInteger;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/crypto/a/a;->Wy:Lorg/spongycastle/crypto/j/h;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/h;->getX()Ljava/math/BigInteger;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/crypto/a/a;->Wz:Lorg/spongycastle/crypto/j/g;

    invoke-virtual {v2}, Lorg/spongycastle/crypto/j/g;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
