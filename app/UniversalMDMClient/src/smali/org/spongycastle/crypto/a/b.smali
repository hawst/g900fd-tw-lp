.class public Lorg/spongycastle/crypto/a/b;
.super Ljava/lang/Object;
.source "ECDHBasicAgreement.java"

# interfaces
.implements Lorg/spongycastle/crypto/c;


# instance fields
.field private WA:Lorg/spongycastle/crypto/j/t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/crypto/h;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lorg/spongycastle/crypto/j/t;

    iput-object p1, p0, Lorg/spongycastle/crypto/a/b;->WA:Lorg/spongycastle/crypto/j/t;

    .line 35
    return-void
.end method

.method public b(Lorg/spongycastle/crypto/h;)Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 40
    check-cast p1, Lorg/spongycastle/crypto/j/u;

    .line 41
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/u;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/crypto/a/b;->WA:Lorg/spongycastle/crypto/j/t;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/t;->getD()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/j;->e(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
