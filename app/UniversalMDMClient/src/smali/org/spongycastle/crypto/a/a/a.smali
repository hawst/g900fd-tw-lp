.class public Lorg/spongycastle/crypto/a/a/a;
.super Ljava/lang/Object;
.source "DHKDFParameters.java"

# interfaces
.implements Lorg/spongycastle/crypto/k;


# instance fields
.field private WC:Lorg/spongycastle/asn1/l;

.field private WD:[B

.field private WE:[B

.field private keySize:I


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/bc;I[B)V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/spongycastle/crypto/a/a/a;-><init>(Lorg/spongycastle/asn1/bc;I[B[B)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/bc;I[B[B)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lorg/spongycastle/asn1/l;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/spongycastle/crypto/a/a/a;->WC:Lorg/spongycastle/asn1/l;

    .line 30
    iput p2, p0, Lorg/spongycastle/crypto/a/a/a;->keySize:I

    .line 31
    iput-object p3, p0, Lorg/spongycastle/crypto/a/a/a;->WD:[B

    .line 32
    iput-object p4, p0, Lorg/spongycastle/crypto/a/a/a;->WE:[B

    .line 33
    return-void
.end method


# virtual methods
.method public getKeySize()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lorg/spongycastle/crypto/a/a/a;->keySize:I

    return v0
.end method

.method public om()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/spongycastle/crypto/a/a/a;->WC:Lorg/spongycastle/asn1/l;

    return-object v0
.end method

.method public qd()[B
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/spongycastle/crypto/a/a/a;->WD:[B

    return-object v0
.end method
