.class public Lorg/spongycastle/crypto/c/b;
.super Ljava/lang/Object;
.source "OAEPEncoding.java"

# interfaces
.implements Lorg/spongycastle/crypto/a;


# instance fields
.field private Wt:Z

.field private Yb:Lorg/spongycastle/crypto/a;

.field private Yd:[B

.field private Ye:Lorg/spongycastle/crypto/l;

.field private Yf:Lorg/spongycastle/crypto/l;

.field private random:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/a;Lorg/spongycastle/crypto/l;Lorg/spongycastle/crypto/l;[B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    .line 54
    iput-object p2, p0, Lorg/spongycastle/crypto/c/b;->Ye:Lorg/spongycastle/crypto/l;

    .line 55
    iput-object p3, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    .line 56
    invoke-interface {p2}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    .line 58
    if-eqz p4, :cond_0

    .line 60
    array-length v0, p4

    invoke-interface {p2, p4, v1, v0}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 63
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    invoke-interface {p2, v0, v1}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 64
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/crypto/a;Lorg/spongycastle/crypto/l;[B)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p2, p3}, Lorg/spongycastle/crypto/c/b;-><init>(Lorg/spongycastle/crypto/a;Lorg/spongycastle/crypto/l;Lorg/spongycastle/crypto/l;[B)V

    .line 45
    return-void
.end method

.method private a([BIII)[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 316
    new-array v2, p4, [B

    .line 317
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    invoke-interface {v0}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v0

    new-array v3, v0, [B

    .line 318
    const/4 v0, 0x4

    new-array v4, v0, [B

    .line 321
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Ye:Lorg/spongycastle/crypto/l;

    invoke-interface {v0}, Lorg/spongycastle/crypto/l;->reset()V

    move v0, v1

    .line 325
    :cond_0
    invoke-direct {p0, v0, v4}, Lorg/spongycastle/crypto/c/b;->d(I[B)V

    .line 327
    iget-object v5, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    invoke-interface {v5, p1, p2, p3}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 328
    iget-object v5, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    array-length v6, v4

    invoke-interface {v5, v4, v1, v6}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 329
    iget-object v5, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    invoke-interface {v5, v3, v1}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 331
    array-length v5, v3

    mul-int/2addr v5, v0

    array-length v6, v3

    invoke-static {v3, v1, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 333
    add-int/lit8 v0, v0, 0x1

    array-length v5, v3

    div-int v5, p4, v5

    if-lt v0, v5, :cond_0

    .line 335
    array-length v5, v3

    mul-int/2addr v5, v0

    if-ge v5, p4, :cond_1

    .line 337
    invoke-direct {p0, v0, v4}, Lorg/spongycastle/crypto/c/b;->d(I[B)V

    .line 339
    iget-object v5, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    invoke-interface {v5, p1, p2, p3}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 340
    iget-object v5, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    array-length v6, v4

    invoke-interface {v5, v4, v1, v6}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 341
    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yf:Lorg/spongycastle/crypto/l;

    invoke-interface {v4, v3, v1}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 343
    array-length v4, v3

    mul-int/2addr v4, v0

    array-length v5, v2

    array-length v6, v3

    mul-int/2addr v0, v6

    sub-int v0, v5, v0

    invoke-static {v3, v1, v2, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 346
    :cond_1
    return-object v2
.end method

.method private d(I[B)V
    .locals 2

    .prologue
    .line 301
    const/4 v0, 0x0

    ushr-int/lit8 v1, p1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    .line 302
    const/4 v0, 0x1

    ushr-int/lit8 v1, p1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    .line 303
    const/4 v0, 0x2

    ushr-int/lit8 v1, p1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    .line 304
    const/4 v0, 0x3

    ushr-int/lit8 v1, p1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    .line 305
    return-void
.end method


# virtual methods
.method public f([BII)[B
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lorg/spongycastle/crypto/c/b;->Wt:Z

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0, p1, p2, p3}, Lorg/spongycastle/crypto/c/b;->j([BII)[B

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lorg/spongycastle/crypto/c/b;->k([BII)[B

    move-result-object v0

    goto :goto_0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 1

    .prologue
    .line 75
    instance-of v0, p2, Lorg/spongycastle/crypto/j/ao;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 77
    check-cast v0, Lorg/spongycastle/crypto/j/ao;

    .line 79
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ao;->qb()Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/c/b;->random:Ljava/security/SecureRandom;

    .line 86
    :goto_0
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    invoke-interface {v0, p1, p2}, Lorg/spongycastle/crypto/a;->init(ZLorg/spongycastle/crypto/h;)V

    .line 88
    iput-boolean p1, p0, Lorg/spongycastle/crypto/c/b;->Wt:Z

    .line 89
    return-void

    .line 83
    :cond_0
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/crypto/c/b;->random:Ljava/security/SecureRandom;

    goto :goto_0
.end method

.method public j([BII)[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-virtual {p0}, Lorg/spongycastle/crypto/c/b;->pW()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    new-array v2, v0, [B

    .line 146
    array-length v0, v2

    sub-int/2addr v0, p3

    invoke-static {p1, p2, v2, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    array-length v0, v2

    sub-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    const/4 v3, 0x1

    aput-byte v3, v2, v0

    .line 160
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    iget-object v3, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v3, v3

    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v0, v0

    new-array v3, v0, [B

    .line 167
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->random:Ljava/security/SecureRandom;

    invoke-virtual {v0, v3}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 172
    array-length v0, v3

    array-length v4, v2

    iget-object v5, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v5, v5

    sub-int/2addr v4, v5

    invoke-direct {p0, v3, v1, v0, v4}, Lorg/spongycastle/crypto/c/b;->a([BIII)[B

    move-result-object v4

    .line 174
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v0, v0

    :goto_0
    array-length v5, v2

    if-eq v0, v5, :cond_0

    .line 176
    aget-byte v5, v2, v0

    iget-object v6, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v6, v6

    sub-int v6, v0, v6

    aget-byte v6, v4, v6

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v2, v0

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v0, v0

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 187
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v0, v0

    array-length v3, v2

    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    invoke-direct {p0, v2, v0, v3, v4}, Lorg/spongycastle/crypto/c/b;->a([BIII)[B

    move-result-object v3

    move v0, v1

    .line 190
    :goto_1
    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    if-eq v0, v4, :cond_1

    .line 192
    aget-byte v4, v2, v0

    aget-byte v5, v3, v0

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v2, v0

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    array-length v3, v2

    invoke-interface {v0, v2, v1, v3}, Lorg/spongycastle/crypto/a;->f([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public k([BII)[B
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 208
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    invoke-interface {v0, p1, p2, p3}, Lorg/spongycastle/crypto/a;->f([BII)[B

    move-result-object v1

    .line 216
    array-length v0, v1

    iget-object v3, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    invoke-interface {v3}, Lorg/spongycastle/crypto/a;->pX()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 218
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    invoke-interface {v0}, Lorg/spongycastle/crypto/a;->pX()I

    move-result v0

    new-array v0, v0, [B

    .line 220
    array-length v3, v0

    array-length v4, v1

    sub-int/2addr v3, v4

    array-length v4, v1

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    :goto_0
    array-length v1, v0

    iget-object v3, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    if-ge v1, v3, :cond_1

    .line 229
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string v1, "data too short"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v1

    .line 224
    goto :goto_0

    .line 235
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v1, v1

    array-length v3, v0

    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/spongycastle/crypto/c/b;->a([BIII)[B

    move-result-object v3

    move v1, v2

    .line 238
    :goto_1
    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    if-eq v1, v4, :cond_2

    .line 240
    aget-byte v4, v0, v1

    aget-byte v5, v3, v1

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    .line 238
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 246
    :cond_2
    iget-object v1, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v1, v1

    array-length v3, v0

    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    invoke-direct {p0, v0, v2, v1, v3}, Lorg/spongycastle/crypto/c/b;->a([BIII)[B

    move-result-object v3

    .line 248
    iget-object v1, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v1, v1

    :goto_2
    array-length v4, v0

    if-eq v1, v4, :cond_3

    .line 250
    aget-byte v4, v0, v1

    iget-object v5, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v5, v5

    sub-int v5, v1, v5

    aget-byte v5, v3, v5

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    .line 248
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v1, v2

    .line 256
    :goto_3
    iget-object v3, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v3, v3

    if-eq v1, v3, :cond_5

    .line 258
    iget-object v3, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    aget-byte v3, v3, v1

    iget-object v4, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v4, v4

    add-int/2addr v4, v1

    aget-byte v4, v0, v4

    if-eq v3, v4, :cond_4

    .line 260
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string v1, "data hash wrong"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 269
    :cond_5
    iget-object v1, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    :goto_4
    array-length v3, v0

    if-eq v1, v3, :cond_6

    .line 271
    aget-byte v3, v0, v1

    if-eqz v3, :cond_8

    .line 277
    :cond_6
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_7

    aget-byte v3, v0, v1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_9

    .line 279
    :cond_7
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "data start wrong "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 282
    :cond_9
    add-int/lit8 v1, v1, 0x1

    .line 287
    array-length v3, v0

    sub-int/2addr v3, v1

    new-array v3, v3, [B

    .line 289
    array-length v4, v3

    invoke-static {v0, v1, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 291
    return-object v3
.end method

.method public pW()I
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    invoke-interface {v0}, Lorg/spongycastle/crypto/a;->pW()I

    move-result v0

    .line 95
    iget-boolean v1, p0, Lorg/spongycastle/crypto/c/b;->Wt:Z

    if-eqz v1, :cond_0

    .line 97
    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 101
    :cond_0
    return v0
.end method

.method public pX()I
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lorg/spongycastle/crypto/c/b;->Yb:Lorg/spongycastle/crypto/a;

    invoke-interface {v0}, Lorg/spongycastle/crypto/a;->pX()I

    move-result v0

    .line 109
    iget-boolean v1, p0, Lorg/spongycastle/crypto/c/b;->Wt:Z

    if-eqz v1, :cond_0

    .line 115
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lorg/spongycastle/crypto/c/b;->Yd:[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    goto :goto_0
.end method
