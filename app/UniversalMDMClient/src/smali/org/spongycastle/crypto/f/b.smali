.class public Lorg/spongycastle/crypto/f/b;
.super Ljava/io/OutputStream;
.source "DigestOutputStream.java"


# instance fields
.field protected digest:Lorg/spongycastle/crypto/l;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/l;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 16
    iput-object p1, p0, Lorg/spongycastle/crypto/f/b;->digest:Lorg/spongycastle/crypto/l;

    .line 17
    return-void
.end method


# virtual methods
.method public getDigest()[B
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lorg/spongycastle/crypto/f/b;->digest:Lorg/spongycastle/crypto/l;

    invoke-interface {v0}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v0

    new-array v0, v0, [B

    .line 38
    iget-object v1, p0, Lorg/spongycastle/crypto/f/b;->digest:Lorg/spongycastle/crypto/l;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 40
    return-object v0
.end method

.method public write(I)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lorg/spongycastle/crypto/f/b;->digest:Lorg/spongycastle/crypto/l;

    int-to-byte v1, p1

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/l;->update(B)V

    .line 23
    return-void
.end method

.method public write([BII)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lorg/spongycastle/crypto/f/b;->digest:Lorg/spongycastle/crypto/l;

    invoke-interface {v0, p1, p2, p3}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 32
    return-void
.end method
