.class public Lorg/spongycastle/crypto/f/d;
.super Ljava/io/OutputStream;
.source "MacOutputStream.java"


# instance fields
.field protected aaa:Lorg/spongycastle/crypto/o;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/o;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 16
    iput-object p1, p0, Lorg/spongycastle/crypto/f/d;->aaa:Lorg/spongycastle/crypto/o;

    .line 17
    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lorg/spongycastle/crypto/f/d;->aaa:Lorg/spongycastle/crypto/o;

    int-to-byte v1, p1

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 23
    return-void
.end method

.method public write([BII)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lorg/spongycastle/crypto/f/d;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0, p1, p2, p3}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 32
    return-void
.end method
