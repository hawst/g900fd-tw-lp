.class public Lorg/spongycastle/crypto/e/j;
.super Ljava/lang/Object;
.source "ECKeyPairGenerator.java"

# interfaces
.implements Lorg/spongycastle/a/a/b;


# instance fields
.field abA:Lorg/spongycastle/crypto/j/q;

.field random:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/crypto/n;)V
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lorg/spongycastle/crypto/j/r;

    .line 27
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/r;->qb()Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/e/j;->random:Ljava/security/SecureRandom;

    .line 28
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/r;->qL()Lorg/spongycastle/crypto/j/q;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/e/j;->abA:Lorg/spongycastle/crypto/j/q;

    .line 29
    return-void
.end method

.method public qw()Lorg/spongycastle/crypto/b;
    .locals 5

    .prologue
    .line 37
    iget-object v0, p0, Lorg/spongycastle/crypto/e/j;->abA:Lorg/spongycastle/crypto/j/q;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/q;->pQ()Ljava/math/BigInteger;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v1

    .line 43
    :cond_0
    new-instance v2, Ljava/math/BigInteger;

    iget-object v3, p0, Lorg/spongycastle/crypto/e/j;->random:Ljava/security/SecureRandom;

    invoke-direct {v2, v1, v3}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    .line 45
    sget-object v3, Lorg/spongycastle/crypto/e/j;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v3

    if-gez v3, :cond_0

    .line 47
    iget-object v0, p0, Lorg/spongycastle/crypto/e/j;->abA:Lorg/spongycastle/crypto/j/q;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/q;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v0

    invoke-virtual {v0, v2}, Lorg/spongycastle/a/a/j;->e(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    .line 49
    new-instance v1, Lorg/spongycastle/crypto/b;

    new-instance v3, Lorg/spongycastle/crypto/j/u;

    iget-object v4, p0, Lorg/spongycastle/crypto/e/j;->abA:Lorg/spongycastle/crypto/j/q;

    invoke-direct {v3, v0, v4}, Lorg/spongycastle/crypto/j/u;-><init>(Lorg/spongycastle/a/a/j;Lorg/spongycastle/crypto/j/q;)V

    new-instance v0, Lorg/spongycastle/crypto/j/t;

    iget-object v4, p0, Lorg/spongycastle/crypto/e/j;->abA:Lorg/spongycastle/crypto/j/q;

    invoke-direct {v0, v2, v4}, Lorg/spongycastle/crypto/j/t;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/q;)V

    invoke-direct {v1, v3, v0}, Lorg/spongycastle/crypto/b;-><init>(Lorg/spongycastle/crypto/h;Lorg/spongycastle/crypto/h;)V

    return-object v1
.end method
