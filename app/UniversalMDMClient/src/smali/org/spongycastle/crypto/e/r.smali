.class public Lorg/spongycastle/crypto/e/r;
.super Lorg/spongycastle/crypto/p;
.source "PKCS5S1ParametersGenerator.java"


# instance fields
.field private digest:Lorg/spongycastle/crypto/l;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/l;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/spongycastle/crypto/p;-><init>()V

    .line 31
    iput-object p1, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    .line 32
    return-void
.end method

.method private qD()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 39
    iget-object v0, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    invoke-interface {v0}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v0

    new-array v1, v0, [B

    .line 41
    iget-object v0, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    iget-object v2, p0, Lorg/spongycastle/crypto/e/r;->password:[B

    iget-object v3, p0, Lorg/spongycastle/crypto/e/r;->password:[B

    array-length v3, v3

    invoke-interface {v0, v2, v4, v3}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 42
    iget-object v0, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    iget-object v2, p0, Lorg/spongycastle/crypto/e/r;->salt:[B

    iget-object v3, p0, Lorg/spongycastle/crypto/e/r;->salt:[B

    array-length v3, v3

    invoke-interface {v0, v2, v4, v3}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 44
    iget-object v0, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    invoke-interface {v0, v1, v4}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 45
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Lorg/spongycastle/crypto/e/r;->iterationCount:I

    if-ge v0, v2, :cond_0

    .line 47
    iget-object v2, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    array-length v3, v1

    invoke-interface {v2, v1, v4, v3}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 48
    iget-object v2, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    invoke-interface {v2, v1, v4}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return-object v1
.end method


# virtual methods
.method public generateDerivedMacParameters(I)Lorg/spongycastle/crypto/h;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0, p1}, Lorg/spongycastle/crypto/e/r;->generateDerivedParameters(I)Lorg/spongycastle/crypto/h;

    move-result-object v0

    return-object v0
.end method

.method public generateDerivedParameters(I)Lorg/spongycastle/crypto/h;
    .locals 4

    .prologue
    .line 65
    div-int/lit8 v0, p1, 0x8

    .line 67
    iget-object v1, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    invoke-interface {v1}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 69
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t generate a derived key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes long."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 73
    :cond_0
    invoke-direct {p0}, Lorg/spongycastle/crypto/e/r;->qD()[B

    move-result-object v1

    .line 75
    new-instance v2, Lorg/spongycastle/crypto/j/ak;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3, v0}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    return-object v2
.end method

.method public generateDerivedParameters(II)Lorg/spongycastle/crypto/h;
    .locals 6

    .prologue
    .line 92
    div-int/lit8 v0, p1, 0x8

    .line 93
    div-int/lit8 v1, p2, 0x8

    .line 95
    add-int v2, v0, v1

    iget-object v3, p0, Lorg/spongycastle/crypto/e/r;->digest:Lorg/spongycastle/crypto/l;

    invoke-interface {v3}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 97
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t generate a derived key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/2addr v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes long."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 101
    :cond_0
    invoke-direct {p0}, Lorg/spongycastle/crypto/e/r;->qD()[B

    move-result-object v2

    .line 103
    new-instance v3, Lorg/spongycastle/crypto/j/an;

    new-instance v4, Lorg/spongycastle/crypto/j/ak;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v5, v0}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    invoke-direct {v3, v4, v2, v0, v1}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[BII)V

    return-object v3
.end method
