.class public Lorg/spongycastle/crypto/e/d;
.super Ljava/lang/Object;
.source "DHBasicKeyPairGenerator.java"


# instance fields
.field private param:Lorg/spongycastle/crypto/j/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/crypto/n;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lorg/spongycastle/crypto/j/e;

    iput-object p1, p0, Lorg/spongycastle/crypto/e/d;->param:Lorg/spongycastle/crypto/j/e;

    .line 28
    return-void
.end method

.method public qw()Lorg/spongycastle/crypto/b;
    .locals 5

    .prologue
    .line 32
    sget-object v0, Lorg/spongycastle/crypto/e/e;->aby:Lorg/spongycastle/crypto/e/e;

    .line 33
    iget-object v1, p0, Lorg/spongycastle/crypto/e/d;->param:Lorg/spongycastle/crypto/j/e;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/e;->qJ()Lorg/spongycastle/crypto/j/g;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lorg/spongycastle/crypto/e/d;->param:Lorg/spongycastle/crypto/j/e;

    invoke-virtual {v2}, Lorg/spongycastle/crypto/j/e;->qb()Ljava/security/SecureRandom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/spongycastle/crypto/e/e;->a(Lorg/spongycastle/crypto/j/g;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v2

    .line 36
    invoke-virtual {v0, v1, v2}, Lorg/spongycastle/crypto/e/e;->a(Lorg/spongycastle/crypto/j/g;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 38
    new-instance v3, Lorg/spongycastle/crypto/b;

    new-instance v4, Lorg/spongycastle/crypto/j/i;

    invoke-direct {v4, v0, v1}, Lorg/spongycastle/crypto/j/i;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/g;)V

    new-instance v0, Lorg/spongycastle/crypto/j/h;

    invoke-direct {v0, v2, v1}, Lorg/spongycastle/crypto/j/h;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/g;)V

    invoke-direct {v3, v4, v0}, Lorg/spongycastle/crypto/b;-><init>(Lorg/spongycastle/crypto/h;Lorg/spongycastle/crypto/h;)V

    return-object v3
.end method
