.class public Lorg/spongycastle/crypto/e/k;
.super Ljava/lang/Object;
.source "ElGamalKeyPairGenerator.java"


# instance fields
.field private param:Lorg/spongycastle/crypto/j/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/crypto/n;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lorg/spongycastle/crypto/j/v;

    iput-object p1, p0, Lorg/spongycastle/crypto/e/k;->param:Lorg/spongycastle/crypto/j/v;

    .line 29
    return-void
.end method

.method public qw()Lorg/spongycastle/crypto/b;
    .locals 7

    .prologue
    .line 33
    sget-object v0, Lorg/spongycastle/crypto/e/e;->aby:Lorg/spongycastle/crypto/e/e;

    .line 34
    iget-object v1, p0, Lorg/spongycastle/crypto/e/k;->param:Lorg/spongycastle/crypto/j/v;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/v;->qN()Lorg/spongycastle/crypto/j/x;

    move-result-object v1

    .line 35
    new-instance v2, Lorg/spongycastle/crypto/j/g;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/x;->getP()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/x;->getG()Ljava/math/BigInteger;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/x;->getL()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/spongycastle/crypto/j/g;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;I)V

    .line 37
    iget-object v3, p0, Lorg/spongycastle/crypto/e/k;->param:Lorg/spongycastle/crypto/j/v;

    invoke-virtual {v3}, Lorg/spongycastle/crypto/j/v;->qb()Ljava/security/SecureRandom;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/spongycastle/crypto/e/e;->a(Lorg/spongycastle/crypto/j/g;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v3

    .line 38
    invoke-virtual {v0, v2, v3}, Lorg/spongycastle/crypto/e/e;->a(Lorg/spongycastle/crypto/j/g;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 40
    new-instance v2, Lorg/spongycastle/crypto/b;

    new-instance v4, Lorg/spongycastle/crypto/j/z;

    invoke-direct {v4, v0, v1}, Lorg/spongycastle/crypto/j/z;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/x;)V

    new-instance v0, Lorg/spongycastle/crypto/j/y;

    invoke-direct {v0, v3, v1}, Lorg/spongycastle/crypto/j/y;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/x;)V

    invoke-direct {v2, v4, v0}, Lorg/spongycastle/crypto/b;-><init>(Lorg/spongycastle/crypto/h;Lorg/spongycastle/crypto/h;)V

    return-object v2
.end method
