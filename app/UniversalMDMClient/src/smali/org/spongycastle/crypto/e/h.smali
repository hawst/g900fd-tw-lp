.class public Lorg/spongycastle/crypto/e/h;
.super Ljava/lang/Object;
.source "DSAKeyPairGenerator.java"


# static fields
.field private static final ONE:Ljava/math/BigInteger;


# instance fields
.field private param:Lorg/spongycastle/crypto/j/k;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/crypto/e/h;->ONE:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p1, p2, p0}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lorg/spongycastle/crypto/e/h;->ONE:Ljava/math/BigInteger;

    sget-object v1, Lorg/spongycastle/crypto/e/h;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p0, v1}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lorg/spongycastle/util/b;->d(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lorg/spongycastle/crypto/n;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lorg/spongycastle/crypto/j/k;

    iput-object p1, p0, Lorg/spongycastle/crypto/e/h;->param:Lorg/spongycastle/crypto/j/k;

    .line 32
    return-void
.end method

.method public qw()Lorg/spongycastle/crypto/b;
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lorg/spongycastle/crypto/e/h;->param:Lorg/spongycastle/crypto/j/k;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/k;->qK()Lorg/spongycastle/crypto/j/m;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/m;->getQ()Ljava/math/BigInteger;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/crypto/e/h;->param:Lorg/spongycastle/crypto/j/k;

    invoke-virtual {v2}, Lorg/spongycastle/crypto/j/k;->qb()Ljava/security/SecureRandom;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/spongycastle/crypto/e/h;->a(Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v1

    .line 39
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/m;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/m;->getG()Ljava/math/BigInteger;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lorg/spongycastle/crypto/e/h;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    .line 41
    new-instance v3, Lorg/spongycastle/crypto/b;

    new-instance v4, Lorg/spongycastle/crypto/j/o;

    invoke-direct {v4, v2, v0}, Lorg/spongycastle/crypto/j/o;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/m;)V

    new-instance v2, Lorg/spongycastle/crypto/j/n;

    invoke-direct {v2, v1, v0}, Lorg/spongycastle/crypto/j/n;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/m;)V

    invoke-direct {v3, v4, v2}, Lorg/spongycastle/crypto/b;-><init>(Lorg/spongycastle/crypto/h;Lorg/spongycastle/crypto/h;)V

    return-object v3
.end method
