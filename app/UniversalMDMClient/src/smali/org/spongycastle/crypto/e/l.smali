.class public Lorg/spongycastle/crypto/e/l;
.super Ljava/lang/Object;
.source "ElGamalParametersGenerator.java"


# instance fields
.field private certainty:I

.field private random:Ljava/security/SecureRandom;

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/security/SecureRandom;)V
    .locals 0

    .prologue
    .line 19
    iput p1, p0, Lorg/spongycastle/crypto/e/l;->size:I

    .line 20
    iput p2, p0, Lorg/spongycastle/crypto/e/l;->certainty:I

    .line 21
    iput-object p3, p0, Lorg/spongycastle/crypto/e/l;->random:Ljava/security/SecureRandom;

    .line 22
    return-void
.end method

.method public qB()Lorg/spongycastle/crypto/j/x;
    .locals 3

    .prologue
    .line 35
    iget v0, p0, Lorg/spongycastle/crypto/e/l;->size:I

    iget v1, p0, Lorg/spongycastle/crypto/e/l;->certainty:I

    iget-object v2, p0, Lorg/spongycastle/crypto/e/l;->random:Ljava/security/SecureRandom;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/crypto/e/g;->b(IILjava/security/SecureRandom;)[Ljava/math/BigInteger;

    move-result-object v0

    .line 37
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 38
    const/4 v2, 0x1

    aget-object v0, v0, v2

    .line 39
    iget-object v2, p0, Lorg/spongycastle/crypto/e/l;->random:Ljava/security/SecureRandom;

    invoke-static {v1, v0, v2}, Lorg/spongycastle/crypto/e/g;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v0

    .line 41
    new-instance v2, Lorg/spongycastle/crypto/j/x;

    invoke-direct {v2, v1, v0}, Lorg/spongycastle/crypto/j/x;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v2
.end method
