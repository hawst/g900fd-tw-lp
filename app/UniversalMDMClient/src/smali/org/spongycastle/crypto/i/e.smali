.class public Lorg/spongycastle/crypto/i/e;
.super Lorg/spongycastle/crypto/f;
.source "PaddedBufferedBlockCipher.java"


# instance fields
.field abF:Lorg/spongycastle/crypto/i/a;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/d;)V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lorg/spongycastle/crypto/i/d;

    invoke-direct {v0}, Lorg/spongycastle/crypto/i/d;-><init>()V

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/crypto/i/e;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/spongycastle/crypto/f;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    .line 33
    iput-object p2, p0, Lorg/spongycastle/crypto/i/e;->abF:Lorg/spongycastle/crypto/i/a;

    .line 35
    invoke-interface {p1}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    .line 37
    return-void
.end method


# virtual methods
.method public doFinal([BI)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 246
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    .line 249
    iget-boolean v2, p0, Lorg/spongycastle/crypto/i/e;->Wt:Z

    if-eqz v2, :cond_1

    .line 251
    iget v2, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    if-ne v2, v0, :cond_3

    .line 253
    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p2

    array-length v2, p1

    if-le v0, v2, :cond_0

    .line 255
    invoke-virtual {p0}, Lorg/spongycastle/crypto/i/e;->reset()V

    .line 257
    new-instance v0, Lorg/spongycastle/crypto/DataLengthException;

    const-string v1, "output buffer too short"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/DataLengthException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v2, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    invoke-interface {v0, v2, v1, p1, p2}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move-result v0

    .line 261
    iput v1, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    .line 264
    :goto_0
    iget-object v2, p0, Lorg/spongycastle/crypto/i/e;->abF:Lorg/spongycastle/crypto/i/a;

    iget-object v3, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    iget v4, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    invoke-interface {v2, v3, v4}, Lorg/spongycastle/crypto/i/a;->s([BI)I

    .line 266
    iget-object v2, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v3, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    add-int v4, p2, v0

    invoke-interface {v2, v3, v1, p1, v4}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    invoke-virtual {p0}, Lorg/spongycastle/crypto/i/e;->reset()V

    .line 296
    :goto_1
    return v0

    .line 272
    :cond_1
    iget v2, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    if-ne v2, v0, :cond_2

    .line 274
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v2, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    iget-object v3, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    invoke-interface {v0, v2, v1, v3, v1}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move-result v0

    .line 275
    iput v1, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    .line 286
    :try_start_0
    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->abF:Lorg/spongycastle/crypto/i/a;

    iget-object v2, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    invoke-interface {v1, v2}, Lorg/spongycastle/crypto/i/a;->N([B)I

    move-result v1

    sub-int/2addr v0, v1

    .line 288
    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    const/4 v2, 0x0

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    invoke-virtual {p0}, Lorg/spongycastle/crypto/i/e;->reset()V

    goto :goto_1

    .line 279
    :cond_2
    invoke-virtual {p0}, Lorg/spongycastle/crypto/i/e;->reset()V

    .line 281
    new-instance v0, Lorg/spongycastle/crypto/DataLengthException;

    const-string v1, "last block incomplete in decryption"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/DataLengthException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lorg/spongycastle/crypto/i/e;->reset()V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getOutputSize(I)I
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    add-int/2addr v0, p1

    .line 96
    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v1, v1

    rem-int v1, v0, v1

    .line 98
    if-nez v1, :cond_1

    .line 100
    iget-boolean v1, p0, Lorg/spongycastle/crypto/i/e;->Wt:Z

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 108
    :cond_0
    :goto_0
    return v0

    :cond_1
    sub-int/2addr v0, v1

    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v1, v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getUpdateOutputSize(I)I
    .locals 2

    .prologue
    .line 122
    iget v0, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    add-int/2addr v0, p1

    .line 123
    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v1, v1

    rem-int v1, v0, v1

    .line 125
    if-nez v1, :cond_0

    .line 127
    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    .line 130
    :goto_0
    return v0

    :cond_0
    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 2

    .prologue
    .line 64
    iput-boolean p1, p0, Lorg/spongycastle/crypto/i/e;->Wt:Z

    .line 66
    invoke-virtual {p0}, Lorg/spongycastle/crypto/i/e;->reset()V

    .line 68
    instance-of v0, p2, Lorg/spongycastle/crypto/j/ao;

    if-eqz v0, :cond_0

    .line 70
    check-cast p2, Lorg/spongycastle/crypto/j/ao;

    .line 72
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->abF:Lorg/spongycastle/crypto/i/a;

    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/ao;->qb()Ljava/security/SecureRandom;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/i/a;->init(Ljava/security/SecureRandom;)V

    .line 74
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/ao;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lorg/spongycastle/crypto/d;->init(ZLorg/spongycastle/crypto/h;)V

    .line 82
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->abF:Lorg/spongycastle/crypto/i/a;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/i/a;->init(Ljava/security/SecureRandom;)V

    .line 80
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0, p1, p2}, Lorg/spongycastle/crypto/d;->init(ZLorg/spongycastle/crypto/h;)V

    goto :goto_0
.end method

.method public processByte(B[BI)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 149
    .line 151
    iget v0, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    iget-object v2, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v2, v2

    if-ne v0, v2, :cond_0

    .line 153
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v2, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    invoke-interface {v0, v2, v1, p2, p3}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move-result v0

    .line 154
    iput v1, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    .line 157
    :goto_0
    iget-object v1, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    iget v2, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    aput-byte p1, v1, v2

    .line 159
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public processBytes([BII[BI)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 182
    if-gez p3, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t have a negative input length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/crypto/i/e;->getBlockSize()I

    move-result v3

    .line 188
    invoke-virtual {p0, p3}, Lorg/spongycastle/crypto/i/e;->getUpdateOutputSize(I)I

    move-result v0

    .line 190
    if-lez v0, :cond_1

    .line 192
    add-int/2addr v0, p5

    array-length v2, p4

    if-le v0, v2, :cond_1

    .line 194
    new-instance v0, Lorg/spongycastle/crypto/DataLengthException;

    const-string v1, "output buffer too short"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/DataLengthException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v0, v0

    iget v2, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    sub-int v2, v0, v2

    .line 201
    if-le p3, v2, :cond_2

    .line 203
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    iget v4, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    invoke-static {p1, p2, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 205
    iget-object v0, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v4, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    invoke-interface {v0, v4, v1, p4, p5}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move-result v0

    add-int/2addr v0, v1

    .line 207
    iput v1, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    .line 208
    sub-int v1, p3, v2

    .line 209
    add-int/2addr v2, p2

    .line 211
    :goto_0
    iget-object v4, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    array-length v4, v4

    if-le v1, v4, :cond_3

    .line 213
    iget-object v4, p0, Lorg/spongycastle/crypto/i/e;->Wu:Lorg/spongycastle/crypto/d;

    add-int v5, p5, v0

    invoke-interface {v4, p1, v2, p4, v5}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move-result v4

    add-int/2addr v0, v4

    .line 215
    sub-int/2addr v1, v3

    .line 216
    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    move v0, v1

    move v2, p2

    move v1, p3

    .line 220
    :cond_3
    iget-object v3, p0, Lorg/spongycastle/crypto/i/e;->buf:[B

    iget v4, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    invoke-static {p1, v2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 222
    iget v2, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/spongycastle/crypto/i/e;->Ws:I

    .line 224
    return v0
.end method
