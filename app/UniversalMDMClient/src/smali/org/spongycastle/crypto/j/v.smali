.class public Lorg/spongycastle/crypto/j/v;
.super Lorg/spongycastle/crypto/n;
.source "ElGamalKeyGenerationParameters.java"


# instance fields
.field private acQ:Lorg/spongycastle/crypto/j/x;


# direct methods
.method public constructor <init>(Ljava/security/SecureRandom;Lorg/spongycastle/crypto/j/x;)V
    .locals 1

    .prologue
    .line 16
    invoke-static {p2}, Lorg/spongycastle/crypto/j/v;->a(Lorg/spongycastle/crypto/j/x;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/crypto/n;-><init>(Ljava/security/SecureRandom;I)V

    .line 18
    iput-object p2, p0, Lorg/spongycastle/crypto/j/v;->acQ:Lorg/spongycastle/crypto/j/x;

    .line 19
    return-void
.end method

.method static a(Lorg/spongycastle/crypto/j/x;)I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lorg/spongycastle/crypto/j/x;->getL()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/spongycastle/crypto/j/x;->getL()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/crypto/j/x;->getP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public qN()Lorg/spongycastle/crypto/j/x;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lorg/spongycastle/crypto/j/v;->acQ:Lorg/spongycastle/crypto/j/x;

    return-object v0
.end method
