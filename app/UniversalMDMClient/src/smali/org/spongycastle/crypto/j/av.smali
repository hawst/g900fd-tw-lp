.class public Lorg/spongycastle/crypto/j/av;
.super Lorg/spongycastle/crypto/j/au;
.source "RSAPrivateCrtKeyParameters.java"


# instance fields
.field private acH:Ljava/math/BigInteger;

.field private adj:Ljava/math/BigInteger;

.field private adk:Ljava/math/BigInteger;

.field private adl:Ljava/math/BigInteger;

.field private adm:Ljava/math/BigInteger;

.field private p:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p3}, Lorg/spongycastle/crypto/j/au;-><init>(ZLjava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 30
    iput-object p2, p0, Lorg/spongycastle/crypto/j/av;->adj:Ljava/math/BigInteger;

    .line 31
    iput-object p4, p0, Lorg/spongycastle/crypto/j/av;->p:Ljava/math/BigInteger;

    .line 32
    iput-object p5, p0, Lorg/spongycastle/crypto/j/av;->acH:Ljava/math/BigInteger;

    .line 33
    iput-object p6, p0, Lorg/spongycastle/crypto/j/av;->adk:Ljava/math/BigInteger;

    .line 34
    iput-object p7, p0, Lorg/spongycastle/crypto/j/av;->adl:Ljava/math/BigInteger;

    .line 35
    iput-object p8, p0, Lorg/spongycastle/crypto/j/av;->adm:Ljava/math/BigInteger;

    .line 36
    return-void
.end method


# virtual methods
.method public getP()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/spongycastle/crypto/j/av;->p:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getPublicExponent()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/spongycastle/crypto/j/av;->adj:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/spongycastle/crypto/j/av;->acH:Ljava/math/BigInteger;

    return-object v0
.end method

.method public rc()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/spongycastle/crypto/j/av;->adk:Ljava/math/BigInteger;

    return-object v0
.end method

.method public rd()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/spongycastle/crypto/j/av;->adl:Ljava/math/BigInteger;

    return-object v0
.end method

.method public re()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/spongycastle/crypto/j/av;->adm:Ljava/math/BigInteger;

    return-object v0
.end method
