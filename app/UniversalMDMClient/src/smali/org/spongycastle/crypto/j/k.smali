.class public Lorg/spongycastle/crypto/j/k;
.super Lorg/spongycastle/crypto/n;
.source "DSAKeyGenerationParameters.java"


# instance fields
.field private acL:Lorg/spongycastle/crypto/j/m;


# direct methods
.method public constructor <init>(Ljava/security/SecureRandom;Lorg/spongycastle/crypto/j/m;)V
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/m;->getP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/crypto/n;-><init>(Ljava/security/SecureRandom;I)V

    .line 18
    iput-object p2, p0, Lorg/spongycastle/crypto/j/k;->acL:Lorg/spongycastle/crypto/j/m;

    .line 19
    return-void
.end method


# virtual methods
.method public qK()Lorg/spongycastle/crypto/j/m;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lorg/spongycastle/crypto/j/k;->acL:Lorg/spongycastle/crypto/j/m;

    return-object v0
.end method
