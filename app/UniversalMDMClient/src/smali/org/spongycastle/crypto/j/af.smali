.class public Lorg/spongycastle/crypto/j/af;
.super Ljava/lang/Object;
.source "GOST3410ValidationParameters.java"


# instance fields
.field private acT:I

.field private acU:I

.field private acV:J

.field private acW:J


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lorg/spongycastle/crypto/j/af;->acT:I

    .line 16
    iput p2, p0, Lorg/spongycastle/crypto/j/af;->acU:I

    .line 17
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lorg/spongycastle/crypto/j/af;->acV:J

    .line 24
    iput-wide p3, p0, Lorg/spongycastle/crypto/j/af;->acW:J

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 50
    instance-of v1, p1, Lorg/spongycastle/crypto/j/af;

    if-nez v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    check-cast p1, Lorg/spongycastle/crypto/j/af;

    .line 57
    iget v1, p1, Lorg/spongycastle/crypto/j/af;->acU:I

    iget v2, p0, Lorg/spongycastle/crypto/j/af;->acU:I

    if-ne v1, v2, :cond_0

    .line 62
    iget v1, p1, Lorg/spongycastle/crypto/j/af;->acT:I

    iget v2, p0, Lorg/spongycastle/crypto/j/af;->acT:I

    if-ne v1, v2, :cond_0

    .line 67
    iget-wide v2, p1, Lorg/spongycastle/crypto/j/af;->acW:J

    iget-wide v4, p0, Lorg/spongycastle/crypto/j/af;->acW:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 72
    iget-wide v2, p1, Lorg/spongycastle/crypto/j/af;->acV:J

    iget-wide v4, p0, Lorg/spongycastle/crypto/j/af;->acV:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 77
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 82
    iget v0, p0, Lorg/spongycastle/crypto/j/af;->acT:I

    iget v1, p0, Lorg/spongycastle/crypto/j/af;->acU:I

    xor-int/2addr v0, v1

    iget-wide v2, p0, Lorg/spongycastle/crypto/j/af;->acV:J

    long-to-int v1, v2

    xor-int/2addr v0, v1

    iget-wide v2, p0, Lorg/spongycastle/crypto/j/af;->acV:J

    shr-long/2addr v2, v4

    long-to-int v1, v2

    xor-int/2addr v0, v1

    iget-wide v2, p0, Lorg/spongycastle/crypto/j/af;->acW:J

    long-to-int v1, v2

    xor-int/2addr v0, v1

    iget-wide v2, p0, Lorg/spongycastle/crypto/j/af;->acW:J

    shr-long/2addr v2, v4

    long-to-int v1, v2

    xor-int/2addr v0, v1

    return v0
.end method
