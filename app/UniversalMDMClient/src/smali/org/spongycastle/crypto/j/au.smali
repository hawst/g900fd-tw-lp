.class public Lorg/spongycastle/crypto/j/au;
.super Lorg/spongycastle/crypto/j/b;
.source "RSAKeyParameters.java"


# instance fields
.field private adi:Ljava/math/BigInteger;

.field private modulus:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(ZLjava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lorg/spongycastle/crypto/j/b;-><init>(Z)V

    .line 18
    iput-object p2, p0, Lorg/spongycastle/crypto/j/au;->modulus:Ljava/math/BigInteger;

    .line 19
    iput-object p3, p0, Lorg/spongycastle/crypto/j/au;->adi:Ljava/math/BigInteger;

    .line 20
    return-void
.end method


# virtual methods
.method public getExponent()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lorg/spongycastle/crypto/j/au;->adi:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getModulus()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lorg/spongycastle/crypto/j/au;->modulus:Ljava/math/BigInteger;

    return-object v0
.end method
