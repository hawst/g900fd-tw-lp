.class public Lorg/spongycastle/crypto/j/f;
.super Lorg/spongycastle/crypto/j/b;
.source "DHKeyParameters.java"


# instance fields
.field private acG:Lorg/spongycastle/crypto/j/g;


# direct methods
.method protected constructor <init>(ZLorg/spongycastle/crypto/j/g;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lorg/spongycastle/crypto/j/b;-><init>(Z)V

    .line 15
    iput-object p2, p0, Lorg/spongycastle/crypto/j/f;->acG:Lorg/spongycastle/crypto/j/g;

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 26
    instance-of v1, p1, Lorg/spongycastle/crypto/j/f;

    if-nez v1, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    check-cast p1, Lorg/spongycastle/crypto/j/f;

    .line 33
    iget-object v1, p0, Lorg/spongycastle/crypto/j/f;->acG:Lorg/spongycastle/crypto/j/g;

    if-nez v1, :cond_2

    .line 35
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/f;->qJ()Lorg/spongycastle/crypto/j/g;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 39
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/crypto/j/f;->acG:Lorg/spongycastle/crypto/j/g;

    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/f;->qJ()Lorg/spongycastle/crypto/j/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/crypto/j/g;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lorg/spongycastle/crypto/j/f;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 47
    :goto_0
    iget-object v1, p0, Lorg/spongycastle/crypto/j/f;->acG:Lorg/spongycastle/crypto/j/g;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lorg/spongycastle/crypto/j/f;->acG:Lorg/spongycastle/crypto/j/g;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/g;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 52
    :cond_0
    return v0

    .line 45
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public qJ()Lorg/spongycastle/crypto/j/g;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/spongycastle/crypto/j/f;->acG:Lorg/spongycastle/crypto/j/g;

    return-object v0
.end method
