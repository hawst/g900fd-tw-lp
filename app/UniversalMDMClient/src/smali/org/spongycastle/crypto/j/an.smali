.class public Lorg/spongycastle/crypto/j/an;
.super Ljava/lang/Object;
.source "ParametersWithIV.java"

# interfaces
.implements Lorg/spongycastle/crypto/h;


# instance fields
.field private ade:Lorg/spongycastle/crypto/h;

.field private iv:[B


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/h;[B)V
    .locals 2

    .prologue
    .line 15
    const/4 v0, 0x0

    array-length v1, p2

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[BII)V

    .line 16
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/crypto/h;[BII)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-array v0, p4, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/j/an;->iv:[B

    .line 25
    iput-object p1, p0, Lorg/spongycastle/crypto/j/an;->ade:Lorg/spongycastle/crypto/h;

    .line 27
    iget-object v0, p0, Lorg/spongycastle/crypto/j/an;->iv:[B

    const/4 v1, 0x0

    invoke-static {p2, p3, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 28
    return-void
.end method


# virtual methods
.method public getIV()[B
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/spongycastle/crypto/j/an;->iv:[B

    return-object v0
.end method

.method public qY()Lorg/spongycastle/crypto/h;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/spongycastle/crypto/j/an;->ade:Lorg/spongycastle/crypto/h;

    return-object v0
.end method
