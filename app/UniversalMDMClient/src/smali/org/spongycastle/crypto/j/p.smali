.class public Lorg/spongycastle/crypto/j/p;
.super Ljava/lang/Object;
.source "DSAValidationParameters.java"


# instance fields
.field private Gc:I

.field private UW:[B


# direct methods
.method public constructor <init>([BI)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lorg/spongycastle/crypto/j/p;->UW:[B

    .line 15
    iput p2, p0, Lorg/spongycastle/crypto/j/p;->Gc:I

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 36
    instance-of v1, p1, Lorg/spongycastle/crypto/j/p;

    if-nez v1, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    check-cast p1, Lorg/spongycastle/crypto/j/p;

    .line 43
    iget v1, p1, Lorg/spongycastle/crypto/j/p;->Gc:I

    iget v2, p0, Lorg/spongycastle/crypto/j/p;->Gc:I

    if-ne v1, v2, :cond_0

    .line 48
    iget-object v0, p0, Lorg/spongycastle/crypto/j/p;->UW:[B

    iget-object v1, p1, Lorg/spongycastle/crypto/j/p;->UW:[B

    invoke-static {v0, v1}, Lorg/spongycastle/util/a;->h([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 30
    iget v0, p0, Lorg/spongycastle/crypto/j/p;->Gc:I

    iget-object v1, p0, Lorg/spongycastle/crypto/j/p;->UW:[B

    invoke-static {v1}, Lorg/spongycastle/util/a;->hashCode([B)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
