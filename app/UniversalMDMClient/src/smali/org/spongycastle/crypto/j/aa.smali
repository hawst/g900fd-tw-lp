.class public Lorg/spongycastle/crypto/j/aa;
.super Lorg/spongycastle/crypto/n;
.source "GOST3410KeyGenerationParameters.java"


# instance fields
.field private acR:Lorg/spongycastle/crypto/j/ac;


# direct methods
.method public constructor <init>(Ljava/security/SecureRandom;Lorg/spongycastle/crypto/j/ac;)V
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/ac;->getP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/crypto/n;-><init>(Ljava/security/SecureRandom;I)V

    .line 18
    iput-object p2, p0, Lorg/spongycastle/crypto/j/aa;->acR:Lorg/spongycastle/crypto/j/ac;

    .line 19
    return-void
.end method


# virtual methods
.method public qO()Lorg/spongycastle/crypto/j/ac;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lorg/spongycastle/crypto/j/aa;->acR:Lorg/spongycastle/crypto/j/ac;

    return-object v0
.end method
