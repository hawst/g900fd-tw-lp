.class public Lorg/spongycastle/crypto/j/q;
.super Ljava/lang/Object;
.source "ECDomainParameters.java"

# interfaces
.implements Lorg/spongycastle/a/a/b;


# instance fields
.field UV:Lorg/spongycastle/a/a/c;

.field UW:[B

.field Va:Ljava/math/BigInteger;

.field acN:Lorg/spongycastle/a/a/j;

.field n:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lorg/spongycastle/crypto/j/q;->UV:Lorg/spongycastle/a/a/c;

    .line 24
    iput-object p2, p0, Lorg/spongycastle/crypto/j/q;->acN:Lorg/spongycastle/a/a/j;

    .line 25
    iput-object p3, p0, Lorg/spongycastle/crypto/j/q;->n:Ljava/math/BigInteger;

    .line 26
    sget-object v0, Lorg/spongycastle/crypto/j/q;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lorg/spongycastle/crypto/j/q;->Va:Ljava/math/BigInteger;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/crypto/j/q;->UW:[B

    .line 28
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/spongycastle/crypto/j/q;->UV:Lorg/spongycastle/a/a/c;

    .line 37
    iput-object p2, p0, Lorg/spongycastle/crypto/j/q;->acN:Lorg/spongycastle/a/a/j;

    .line 38
    iput-object p3, p0, Lorg/spongycastle/crypto/j/q;->n:Ljava/math/BigInteger;

    .line 39
    iput-object p4, p0, Lorg/spongycastle/crypto/j/q;->Va:Ljava/math/BigInteger;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/crypto/j/q;->UW:[B

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/spongycastle/crypto/j/q;->UV:Lorg/spongycastle/a/a/c;

    .line 51
    iput-object p2, p0, Lorg/spongycastle/crypto/j/q;->acN:Lorg/spongycastle/a/a/j;

    .line 52
    iput-object p3, p0, Lorg/spongycastle/crypto/j/q;->n:Ljava/math/BigInteger;

    .line 53
    iput-object p4, p0, Lorg/spongycastle/crypto/j/q;->Va:Ljava/math/BigInteger;

    .line 54
    iput-object p5, p0, Lorg/spongycastle/crypto/j/q;->UW:[B

    .line 55
    return-void
.end method


# virtual methods
.method public getSeed()[B
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/spongycastle/crypto/j/q;->UW:[B

    return-object v0
.end method

.method public pO()Lorg/spongycastle/a/a/c;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/spongycastle/crypto/j/q;->UV:Lorg/spongycastle/a/a/c;

    return-object v0
.end method

.method public pP()Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/spongycastle/crypto/j/q;->acN:Lorg/spongycastle/a/a/j;

    return-object v0
.end method

.method public pQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/spongycastle/crypto/j/q;->n:Ljava/math/BigInteger;

    return-object v0
.end method

.method public pR()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/crypto/j/q;->Va:Ljava/math/BigInteger;

    return-object v0
.end method
