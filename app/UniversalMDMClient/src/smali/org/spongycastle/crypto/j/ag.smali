.class public Lorg/spongycastle/crypto/j/ag;
.super Ljava/lang/Object;
.source "IESParameters.java"

# interfaces
.implements Lorg/spongycastle/crypto/h;


# instance fields
.field private acX:[B

.field private acY:I

.field private encoding:[B


# direct methods
.method public constructor <init>([B[BI)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lorg/spongycastle/crypto/j/ag;->acX:[B

    .line 26
    iput-object p2, p0, Lorg/spongycastle/crypto/j/ag;->encoding:[B

    .line 27
    iput p3, p0, Lorg/spongycastle/crypto/j/ag;->acY:I

    .line 28
    return-void
.end method


# virtual methods
.method public qP()[B
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/spongycastle/crypto/j/ag;->acX:[B

    return-object v0
.end method

.method public qQ()[B
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/spongycastle/crypto/j/ag;->encoding:[B

    return-object v0
.end method

.method public qR()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lorg/spongycastle/crypto/j/ag;->acY:I

    return v0
.end method
