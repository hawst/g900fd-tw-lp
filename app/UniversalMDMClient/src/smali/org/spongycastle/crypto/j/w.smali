.class public Lorg/spongycastle/crypto/j/w;
.super Lorg/spongycastle/crypto/j/b;
.source "ElGamalKeyParameters.java"


# instance fields
.field private acQ:Lorg/spongycastle/crypto/j/x;


# direct methods
.method protected constructor <init>(ZLorg/spongycastle/crypto/j/x;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lorg/spongycastle/crypto/j/b;-><init>(Z)V

    .line 15
    iput-object p2, p0, Lorg/spongycastle/crypto/j/w;->acQ:Lorg/spongycastle/crypto/j/x;

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 31
    instance-of v1, p1, Lorg/spongycastle/crypto/j/w;

    if-nez v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    check-cast p1, Lorg/spongycastle/crypto/j/w;

    .line 38
    iget-object v1, p0, Lorg/spongycastle/crypto/j/w;->acQ:Lorg/spongycastle/crypto/j/x;

    if-nez v1, :cond_2

    .line 40
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/w;->qN()Lorg/spongycastle/crypto/j/x;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 44
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/crypto/j/w;->acQ:Lorg/spongycastle/crypto/j/x;

    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/w;->qN()Lorg/spongycastle/crypto/j/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/crypto/j/x;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lorg/spongycastle/crypto/j/w;->acQ:Lorg/spongycastle/crypto/j/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/crypto/j/w;->acQ:Lorg/spongycastle/crypto/j/x;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/x;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public qN()Lorg/spongycastle/crypto/j/x;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/spongycastle/crypto/j/w;->acQ:Lorg/spongycastle/crypto/j/x;

    return-object v0
.end method
