.class public Lorg/spongycastle/crypto/j/r;
.super Lorg/spongycastle/crypto/n;
.source "ECKeyGenerationParameters.java"


# instance fields
.field private acO:Lorg/spongycastle/crypto/j/q;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/j/q;Ljava/security/SecureRandom;)V
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/q;->pQ()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lorg/spongycastle/crypto/n;-><init>(Ljava/security/SecureRandom;I)V

    .line 18
    iput-object p1, p0, Lorg/spongycastle/crypto/j/r;->acO:Lorg/spongycastle/crypto/j/q;

    .line 19
    return-void
.end method


# virtual methods
.method public qL()Lorg/spongycastle/crypto/j/q;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lorg/spongycastle/crypto/j/r;->acO:Lorg/spongycastle/crypto/j/q;

    return-object v0
.end method
