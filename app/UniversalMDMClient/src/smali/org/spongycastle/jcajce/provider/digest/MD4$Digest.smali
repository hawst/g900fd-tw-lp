.class public Lorg/spongycastle/jcajce/provider/digest/MD4$Digest;
.super Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;
.source "MD4.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lorg/spongycastle/crypto/b/e;

    invoke-direct {v0}, Lorg/spongycastle/crypto/b/e;-><init>()V

    invoke-direct {p0, v0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;-><init>(Lorg/spongycastle/crypto/l;)V

    .line 41
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 46
    invoke-super {p0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/jcajce/provider/digest/MD4$Digest;

    .line 47
    new-instance v2, Lorg/spongycastle/crypto/b/e;

    iget-object v1, p0, Lorg/spongycastle/jcajce/provider/digest/MD4$Digest;->digest:Lorg/spongycastle/crypto/l;

    check-cast v1, Lorg/spongycastle/crypto/b/e;

    invoke-direct {v2, v1}, Lorg/spongycastle/crypto/b/e;-><init>(Lorg/spongycastle/crypto/b/e;)V

    iput-object v2, v0, Lorg/spongycastle/jcajce/provider/digest/MD4$Digest;->digest:Lorg/spongycastle/crypto/l;

    .line 49
    return-object v0
.end method
