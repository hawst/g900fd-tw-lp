.class public Lorg/spongycastle/jcajce/provider/asymmetric/ec/ECUtil;
.super Ljava/lang/Object;
.source "ECUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static convertMidTerms([I)[I
    .locals 6

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 39
    new-array v0, v2, [I

    .line 41
    array-length v1, p0

    if-ne v1, v3, :cond_0

    .line 43
    aget v1, p0, v5

    aput v1, v0, v5

    .line 96
    :goto_0
    return-object v0

    .line 47
    :cond_0
    array-length v1, p0

    if-eq v1, v2, :cond_1

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only Trinomials and pentanomials supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_1
    aget v1, p0, v5

    aget v2, p0, v3

    if-ge v1, v2, :cond_3

    aget v1, p0, v5

    aget v2, p0, v4

    if-ge v1, v2, :cond_3

    .line 54
    aget v1, p0, v5

    aput v1, v0, v5

    .line 55
    aget v1, p0, v3

    aget v2, p0, v4

    if-ge v1, v2, :cond_2

    .line 57
    aget v1, p0, v3

    aput v1, v0, v3

    .line 58
    aget v1, p0, v4

    aput v1, v0, v4

    goto :goto_0

    .line 62
    :cond_2
    aget v1, p0, v4

    aput v1, v0, v3

    .line 63
    aget v1, p0, v3

    aput v1, v0, v4

    goto :goto_0

    .line 66
    :cond_3
    aget v1, p0, v3

    aget v2, p0, v4

    if-ge v1, v2, :cond_5

    .line 68
    aget v1, p0, v3

    aput v1, v0, v5

    .line 69
    aget v1, p0, v5

    aget v2, p0, v4

    if-ge v1, v2, :cond_4

    .line 71
    aget v1, p0, v5

    aput v1, v0, v3

    .line 72
    aget v1, p0, v4

    aput v1, v0, v4

    goto :goto_0

    .line 76
    :cond_4
    aget v1, p0, v4

    aput v1, v0, v3

    .line 77
    aget v1, p0, v5

    aput v1, v0, v4

    goto :goto_0

    .line 82
    :cond_5
    aget v1, p0, v4

    aput v1, v0, v5

    .line 83
    aget v1, p0, v5

    aget v2, p0, v3

    if-ge v1, v2, :cond_6

    .line 85
    aget v1, p0, v5

    aput v1, v0, v3

    .line 86
    aget v1, p0, v3

    aput v1, v0, v4

    goto :goto_0

    .line 90
    :cond_6
    aget v1, p0, v3

    aput v1, v0, v3

    .line 91
    aget v1, p0, v5

    aput v1, v0, v4

    goto :goto_0
.end method

.method public static generatePrivateKeyParameter(Ljava/security/PrivateKey;)Lorg/spongycastle/crypto/j/b;
    .locals 8

    .prologue
    .line 139
    instance-of v0, p0, Lorg/spongycastle/jce/interfaces/ECPrivateKey;

    if-eqz v0, :cond_0

    .line 141
    check-cast p0, Lorg/spongycastle/jce/interfaces/ECPrivateKey;

    .line 142
    invoke-interface {p0}, Lorg/spongycastle/jce/interfaces/ECPrivateKey;->getParameters()Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    .line 144
    if-nez v0, :cond_1

    .line 146
    sget-object v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->CONFIGURATION:Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    move-object v5, v0

    .line 149
    :goto_0
    new-instance v6, Lorg/spongycastle/crypto/j/t;

    invoke-interface {p0}, Lorg/spongycastle/jce/interfaces/ECPrivateKey;->getD()Ljava/math/BigInteger;

    move-result-object v7

    new-instance v0, Lorg/spongycastle/crypto/j/q;

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v2

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pQ()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pR()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->getSeed()[B

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/crypto/j/q;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    invoke-direct {v6, v7, v0}, Lorg/spongycastle/crypto/j/t;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/crypto/j/q;)V

    return-object v6

    .line 154
    :cond_0
    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "can\'t identify EC private key."

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v5, v0

    goto :goto_0
.end method

.method public static generatePublicKeyParameter(Ljava/security/PublicKey;)Lorg/spongycastle/crypto/j/b;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 103
    instance-of v0, p0, Lorg/spongycastle/jce/interfaces/ECPublicKey;

    if-eqz v0, :cond_1

    .line 105
    check-cast p0, Lorg/spongycastle/jce/interfaces/ECPublicKey;

    .line 106
    invoke-interface {p0}, Lorg/spongycastle/jce/interfaces/ECPublicKey;->getParameters()Lorg/spongycastle/jce/spec/e;

    move-result-object v5

    .line 108
    if-nez v5, :cond_0

    .line 110
    sget-object v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->CONFIGURATION:Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lorg/spongycastle/jce/spec/e;

    move-result-object v5

    .line 112
    new-instance v6, Lorg/spongycastle/crypto/j/u;

    check-cast p0, Lorg/spongycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;

    invoke-virtual {p0}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;->engineGetQ()Lorg/spongycastle/a/a/j;

    move-result-object v7

    new-instance v0, Lorg/spongycastle/crypto/j/q;

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v2

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pQ()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pR()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->getSeed()[B

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/crypto/j/q;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    invoke-direct {v6, v7, v0}, Lorg/spongycastle/crypto/j/u;-><init>(Lorg/spongycastle/a/a/j;Lorg/spongycastle/crypto/j/q;)V

    move-object v0, v6

    .line 127
    :goto_0
    return-object v0

    .line 118
    :cond_0
    new-instance v6, Lorg/spongycastle/crypto/j/u;

    invoke-interface {p0}, Lorg/spongycastle/jce/interfaces/ECPublicKey;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v7

    new-instance v0, Lorg/spongycastle/crypto/j/q;

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v2

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pQ()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pR()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->getSeed()[B

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/crypto/j/q;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    invoke-direct {v6, v7, v0}, Lorg/spongycastle/crypto/j/u;-><init>(Lorg/spongycastle/a/a/j;Lorg/spongycastle/crypto/j/q;)V

    move-object v0, v6

    goto :goto_0

    .line 123
    :cond_1
    instance-of v0, p0, Ljava/security/interfaces/ECPublicKey;

    if-eqz v0, :cond_2

    .line 125
    check-cast p0, Ljava/security/interfaces/ECPublicKey;

    .line 126
    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-static {v0, v2}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertSpec(Ljava/security/spec/ECParameterSpec;Z)Lorg/spongycastle/jce/spec/e;

    move-result-object v5

    .line 127
    new-instance v6, Lorg/spongycastle/crypto/j/u;

    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertPoint(Ljava/security/spec/ECParameterSpec;Ljava/security/spec/ECPoint;Z)Lorg/spongycastle/a/a/j;

    move-result-object v7

    new-instance v0, Lorg/spongycastle/crypto/j/q;

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v2

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pQ()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->pR()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/e;->getSeed()[B

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/crypto/j/q;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    invoke-direct {v6, v7, v0}, Lorg/spongycastle/crypto/j/u;-><init>(Lorg/spongycastle/a/a/j;Lorg/spongycastle/crypto/j/q;)V

    move-object v0, v6

    goto :goto_0

    .line 132
    :cond_2
    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "cannot identify EC public key."

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getCurveName(Lorg/spongycastle/asn1/l;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    invoke-static {p0}, Lorg/spongycastle/asn1/r/c;->b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v0

    .line 208
    if-nez v0, :cond_2

    .line 210
    invoke-static {p0}, Lorg/spongycastle/asn1/m/c;->b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v0

    .line 211
    if-nez v0, :cond_0

    .line 213
    invoke-static {p0}, Lorg/spongycastle/asn1/i/a;->b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v0

    .line 215
    :cond_0
    if-nez v0, :cond_1

    .line 217
    invoke-static {p0}, Lorg/spongycastle/asn1/n/a;->b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v0

    .line 219
    :cond_1
    if-nez v0, :cond_2

    .line 221
    invoke-static {p0}, Lorg/spongycastle/asn1/c/b;->b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v0

    .line 225
    :cond_2
    return-object v0
.end method

.method public static getNamedCurveByOid(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;
    .locals 1

    .prologue
    .line 185
    invoke-static {p0}, Lorg/spongycastle/asn1/r/c;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 187
    if-nez v0, :cond_1

    .line 189
    invoke-static {p0}, Lorg/spongycastle/asn1/m/c;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 190
    if-nez v0, :cond_0

    .line 192
    invoke-static {p0}, Lorg/spongycastle/asn1/i/a;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 194
    :cond_0
    if-nez v0, :cond_1

    .line 196
    invoke-static {p0}, Lorg/spongycastle/asn1/n/a;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 200
    :cond_1
    return-object v0
.end method

.method public static getNamedCurveOid(Ljava/lang/String;)Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 160
    invoke-static {p0}, Lorg/spongycastle/asn1/r/c;->getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    .line 162
    if-nez v0, :cond_2

    .line 164
    invoke-static {p0}, Lorg/spongycastle/asn1/m/c;->getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    .line 165
    if-nez v0, :cond_0

    .line 167
    invoke-static {p0}, Lorg/spongycastle/asn1/i/a;->getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    .line 169
    :cond_0
    if-nez v0, :cond_1

    .line 171
    invoke-static {p0}, Lorg/spongycastle/asn1/n/a;->getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    .line 173
    :cond_1
    if-nez v0, :cond_2

    .line 175
    invoke-static {p0}, Lorg/spongycastle/asn1/c/b;->getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    .line 179
    :cond_2
    return-object v0
.end method
