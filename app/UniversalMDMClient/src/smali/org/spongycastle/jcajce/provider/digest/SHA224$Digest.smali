.class public Lorg/spongycastle/jcajce/provider/digest/SHA224$Digest;
.super Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;
.source "SHA224.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lorg/spongycastle/crypto/b/m;

    invoke-direct {v0}, Lorg/spongycastle/crypto/b/m;-><init>()V

    invoke-direct {p0, v0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;-><init>(Lorg/spongycastle/crypto/l;)V

    .line 21
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 26
    invoke-super {p0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/jcajce/provider/digest/SHA224$Digest;

    .line 27
    new-instance v2, Lorg/spongycastle/crypto/b/m;

    iget-object v1, p0, Lorg/spongycastle/jcajce/provider/digest/SHA224$Digest;->digest:Lorg/spongycastle/crypto/l;

    check-cast v1, Lorg/spongycastle/crypto/b/m;

    invoke-direct {v2, v1}, Lorg/spongycastle/crypto/b/m;-><init>(Lorg/spongycastle/crypto/b/m;)V

    iput-object v2, v0, Lorg/spongycastle/jcajce/provider/digest/SHA224$Digest;->digest:Lorg/spongycastle/crypto/l;

    .line 29
    return-object v0
.end method
