.class public interface abstract Lorg/spongycastle/jcajce/provider/util/AsymmetricKeyInfoConverter;
.super Ljava/lang/Object;
.source "AsymmetricKeyInfoConverter.java"


# virtual methods
.method public abstract generatePrivate(Lorg/spongycastle/asn1/l/s;)Ljava/security/PrivateKey;
.end method

.method public abstract generatePublic(Lorg/spongycastle/asn1/q/ah;)Ljava/security/PublicKey;
.end method
