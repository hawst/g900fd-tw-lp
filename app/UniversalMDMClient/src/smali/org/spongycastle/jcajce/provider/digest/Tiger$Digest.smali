.class public Lorg/spongycastle/jcajce/provider/digest/Tiger$Digest;
.super Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;
.source "Tiger.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lorg/spongycastle/crypto/b/q;

    invoke-direct {v0}, Lorg/spongycastle/crypto/b/q;-><init>()V

    invoke-direct {p0, v0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;-><init>(Lorg/spongycastle/crypto/l;)V

    .line 20
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 25
    invoke-super {p0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/jcajce/provider/digest/Tiger$Digest;

    .line 26
    new-instance v2, Lorg/spongycastle/crypto/b/q;

    iget-object v1, p0, Lorg/spongycastle/jcajce/provider/digest/Tiger$Digest;->digest:Lorg/spongycastle/crypto/l;

    check-cast v1, Lorg/spongycastle/crypto/b/q;

    invoke-direct {v2, v1}, Lorg/spongycastle/crypto/b/q;-><init>(Lorg/spongycastle/crypto/b/q;)V

    iput-object v2, v0, Lorg/spongycastle/jcajce/provider/digest/Tiger$Digest;->digest:Lorg/spongycastle/crypto/l;

    .line 28
    return-object v0
.end method
