.class public Lorg/spongycastle/jcajce/provider/digest/MD5$Digest;
.super Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;
.source "MD5.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lorg/spongycastle/crypto/b/f;

    invoke-direct {v0}, Lorg/spongycastle/crypto/b/f;-><init>()V

    invoke-direct {p0, v0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;-><init>(Lorg/spongycastle/crypto/l;)V

    .line 42
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 47
    invoke-super {p0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/jcajce/provider/digest/MD5$Digest;

    .line 48
    new-instance v2, Lorg/spongycastle/crypto/b/f;

    iget-object v1, p0, Lorg/spongycastle/jcajce/provider/digest/MD5$Digest;->digest:Lorg/spongycastle/crypto/l;

    check-cast v1, Lorg/spongycastle/crypto/b/f;

    invoke-direct {v2, v1}, Lorg/spongycastle/crypto/b/f;-><init>(Lorg/spongycastle/crypto/b/f;)V

    iput-object v2, v0, Lorg/spongycastle/jcajce/provider/digest/MD5$Digest;->digest:Lorg/spongycastle/crypto/l;

    .line 50
    return-object v0
.end method
