.class public Lorg/spongycastle/jcajce/provider/asymmetric/util/KeyUtil;
.super Ljava/lang/Object;
.source "KeyUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEncodedPrivateKeyInfo(Lorg/spongycastle/asn1/l/s;)[B
    .locals 1

    .prologue
    .line 65
    :try_start_0
    const-string v0, "DER"

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/l/s;->getEncoded(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 69
    :goto_0
    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 69
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getEncodedPrivateKeyInfo(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)[B
    .locals 2

    .prologue
    .line 51
    :try_start_0
    new-instance v0, Lorg/spongycastle/asn1/l/s;

    invoke-interface {p1}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/spongycastle/asn1/l/s;-><init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)V

    .line 53
    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/util/KeyUtil;->getEncodedPrivateKeyInfo(Lorg/spongycastle/asn1/l/s;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    .line 55
    :catch_0
    move-exception v0

    .line 57
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getEncodedSubjectPublicKeyInfo(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)[B
    .locals 1

    .prologue
    .line 15
    :try_start_0
    new-instance v0, Lorg/spongycastle/asn1/q/ah;

    invoke-direct {v0, p0, p1}, Lorg/spongycastle/asn1/q/ah;-><init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)V

    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/util/KeyUtil;->getEncodedSubjectPublicKeyInfo(Lorg/spongycastle/asn1/q/ah;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 19
    :goto_0
    return-object v0

    .line 17
    :catch_0
    move-exception v0

    .line 19
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getEncodedSubjectPublicKeyInfo(Lorg/spongycastle/asn1/q/a;[B)[B
    .locals 1

    .prologue
    .line 27
    :try_start_0
    new-instance v0, Lorg/spongycastle/asn1/q/ah;

    invoke-direct {v0, p0, p1}, Lorg/spongycastle/asn1/q/ah;-><init>(Lorg/spongycastle/asn1/q/a;[B)V

    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/util/KeyUtil;->getEncodedSubjectPublicKeyInfo(Lorg/spongycastle/asn1/q/ah;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 31
    :goto_0
    return-object v0

    .line 29
    :catch_0
    move-exception v0

    .line 31
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getEncodedSubjectPublicKeyInfo(Lorg/spongycastle/asn1/q/ah;)[B
    .locals 1

    .prologue
    .line 39
    :try_start_0
    const-string v0, "DER"

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/q/ah;->getEncoded(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 43
    const/4 v0, 0x0

    goto :goto_0
.end method
