.class public Lorg/spongycastle/jcajce/provider/digest/RIPEMD320$Digest;
.super Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;
.source "RIPEMD320.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lorg/spongycastle/crypto/b/k;

    invoke-direct {v0}, Lorg/spongycastle/crypto/b/k;-><init>()V

    invoke-direct {p0, v0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;-><init>(Lorg/spongycastle/crypto/l;)V

    .line 19
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 24
    invoke-super {p0}, Lorg/spongycastle/jcajce/provider/digest/BCMessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/jcajce/provider/digest/RIPEMD320$Digest;

    .line 25
    new-instance v2, Lorg/spongycastle/crypto/b/k;

    iget-object v1, p0, Lorg/spongycastle/jcajce/provider/digest/RIPEMD320$Digest;->digest:Lorg/spongycastle/crypto/l;

    check-cast v1, Lorg/spongycastle/crypto/b/k;

    invoke-direct {v2, v1}, Lorg/spongycastle/crypto/b/k;-><init>(Lorg/spongycastle/crypto/b/k;)V

    iput-object v2, v0, Lorg/spongycastle/jcajce/provider/digest/RIPEMD320$Digest;->digest:Lorg/spongycastle/crypto/l;

    .line 27
    return-object v0
.end method
