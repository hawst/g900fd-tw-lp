.class public Lorg/spongycastle/jce/e;
.super Ljava/lang/Object;
.source "PrincipalUtil.java"


# direct methods
.method public static a(Ljava/security/cert/X509Certificate;)Lorg/spongycastle/jce/h;
    .locals 2

    .prologue
    .line 32
    :try_start_0
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getTBSCertificate()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/an;->bb(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/an;

    move-result-object v0

    .line 35
    new-instance v1, Lorg/spongycastle/jce/h;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->oE()Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/aw;->bj(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/aw;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/jce/h;-><init>(Lorg/spongycastle/asn1/q/aw;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 37
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/security/cert/CertificateEncodingException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/security/cert/CertificateEncodingException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static b(Ljava/security/cert/X509Certificate;)Lorg/spongycastle/jce/h;
    .locals 2

    .prologue
    .line 52
    :try_start_0
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getTBSCertificate()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/an;->bb(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/an;

    move-result-object v0

    .line 54
    new-instance v1, Lorg/spongycastle/jce/h;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->px()Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/aw;->bj(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/aw;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/jce/h;-><init>(Lorg/spongycastle/asn1/q/aw;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 56
    :catch_0
    move-exception v0

    .line 58
    new-instance v1, Ljava/security/cert/CertificateEncodingException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/security/cert/CertificateEncodingException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
