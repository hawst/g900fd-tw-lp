.class public Lorg/spongycastle/jce/b;
.super Ljava/lang/Object;
.source "ECNamedCurveTable.java"


# direct methods
.method public static cS(Ljava/lang/String;)Lorg/spongycastle/jce/spec/c;
    .locals 7

    .prologue
    .line 29
    invoke-static {p0}, Lorg/spongycastle/asn1/r/c;->cG(Ljava/lang/String;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 30
    if-nez v0, :cond_0

    .line 34
    :try_start_0
    new-instance v1, Lorg/spongycastle/asn1/l;

    invoke-direct {v1, p0}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/spongycastle/asn1/r/c;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 42
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 44
    invoke-static {p0}, Lorg/spongycastle/asn1/m/c;->cG(Ljava/lang/String;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 45
    if-nez v0, :cond_1

    .line 49
    :try_start_1
    new-instance v1, Lorg/spongycastle/asn1/l;

    invoke-direct {v1, p0}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/spongycastle/asn1/m/c;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 58
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 60
    invoke-static {p0}, Lorg/spongycastle/asn1/n/a;->cG(Ljava/lang/String;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 61
    if-nez v0, :cond_2

    .line 65
    :try_start_2
    new-instance v1, Lorg/spongycastle/asn1/l;

    invoke-direct {v1, p0}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/spongycastle/asn1/n/a;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    .line 74
    :cond_2
    :goto_2
    if-nez v0, :cond_4

    .line 76
    invoke-static {p0}, Lorg/spongycastle/asn1/i/a;->cG(Ljava/lang/String;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    move-object v1, v0

    .line 79
    :goto_3
    if-nez v1, :cond_3

    .line 81
    const/4 v0, 0x0

    .line 84
    :goto_4
    return-object v0

    :cond_3
    new-instance v0, Lorg/spongycastle/jce/spec/c;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v2

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v3

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pQ()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pR()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->getSeed()[B

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lorg/spongycastle/jce/spec/c;-><init>(Ljava/lang/String;Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    goto :goto_4

    .line 67
    :catch_0
    move-exception v1

    goto :goto_2

    .line 51
    :catch_1
    move-exception v1

    goto :goto_1

    .line 36
    :catch_2
    move-exception v1

    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_3
.end method
