.class public Lorg/spongycastle/jce/spec/m;
.super Ljava/lang/Object;
.source "GOST3410ParameterSpec.java"

# interfaces
.implements Ljava/security/spec/AlgorithmParameterSpec;
.implements Lorg/spongycastle/jce/interfaces/d;


# instance fields
.field private aew:Lorg/spongycastle/jce/spec/o;

.field private aex:Ljava/lang/String;

.field private aey:Ljava/lang/String;

.field private aez:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lorg/spongycastle/asn1/c/a;->Ht:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/spongycastle/jce/spec/m;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/spongycastle/jce/spec/m;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    .line 32
    :try_start_0
    new-instance v1, Lorg/spongycastle/asn1/l;

    invoke-direct {v1, p1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/spongycastle/asn1/c/c;->c(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/c/d;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 44
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "no key parameter set for passed in name/OID."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :catch_0
    move-exception v1

    .line 36
    invoke-static {p1}, Lorg/spongycastle/asn1/c/c;->getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v1

    .line 37
    if-eqz v1, :cond_0

    .line 39
    invoke-virtual {v1}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object p1

    .line 40
    invoke-static {v1}, Lorg/spongycastle/asn1/c/c;->c(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/c/d;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_1
    new-instance v1, Lorg/spongycastle/jce/spec/o;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/c/d;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0}, Lorg/spongycastle/asn1/c/d;->getQ()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0}, Lorg/spongycastle/asn1/c/d;->getA()Ljava/math/BigInteger;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lorg/spongycastle/jce/spec/o;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v1, p0, Lorg/spongycastle/jce/spec/m;->aew:Lorg/spongycastle/jce/spec/o;

    .line 54
    iput-object p1, p0, Lorg/spongycastle/jce/spec/m;->aex:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lorg/spongycastle/jce/spec/m;->aey:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/jce/spec/o;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lorg/spongycastle/jce/spec/m;->aew:Lorg/spongycastle/jce/spec/o;

    .line 76
    sget-object v0, Lorg/spongycastle/asn1/c/a;->Ht:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/spec/m;->aey:Ljava/lang/String;

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public static a(Lorg/spongycastle/asn1/c/e;)Lorg/spongycastle/jce/spec/m;
    .locals 4

    .prologue
    .line 124
    invoke-virtual {p0}, Lorg/spongycastle/asn1/c/e;->ns()Lorg/spongycastle/asn1/l;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Lorg/spongycastle/jce/spec/m;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/c/e;->nq()Lorg/spongycastle/asn1/l;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/c/e;->nr()Lorg/spongycastle/asn1/l;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lorg/spongycastle/asn1/c/e;->ns()Lorg/spongycastle/asn1/l;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/spongycastle/jce/spec/m;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/jce/spec/m;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/c/e;->nq()Lorg/spongycastle/asn1/l;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/c/e;->nr()Lorg/spongycastle/asn1/l;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/spec/m;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 102
    instance-of v1, p1, Lorg/spongycastle/jce/spec/m;

    if-eqz v1, :cond_1

    .line 104
    check-cast p1, Lorg/spongycastle/jce/spec/m;

    .line 106
    iget-object v1, p0, Lorg/spongycastle/jce/spec/m;->aew:Lorg/spongycastle/jce/spec/o;

    iget-object v2, p1, Lorg/spongycastle/jce/spec/m;->aew:Lorg/spongycastle/jce/spec/o;

    invoke-virtual {v1, v2}, Lorg/spongycastle/jce/spec/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/spongycastle/jce/spec/m;->aey:Ljava/lang/String;

    iget-object v2, p1, Lorg/spongycastle/jce/spec/m;->aey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    iget-object v2, p1, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    iget-object v2, p1, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 112
    :cond_1
    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lorg/spongycastle/jce/spec/m;->aew:Lorg/spongycastle/jce/spec/o;

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/o;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/spongycastle/jce/spec/m;->aey:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v1, v0

    iget-object v0, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/spongycastle/jce/spec/m;->aex:Ljava/lang/String;

    return-object v0
.end method

.method public rU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/spongycastle/jce/spec/m;->aey:Ljava/lang/String;

    return-object v0
.end method

.method public rV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/spongycastle/jce/spec/m;->aez:Ljava/lang/String;

    return-object v0
.end method

.method public rW()Lorg/spongycastle/jce/spec/o;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/spongycastle/jce/spec/m;->aew:Lorg/spongycastle/jce/spec/o;

    return-object v0
.end method
