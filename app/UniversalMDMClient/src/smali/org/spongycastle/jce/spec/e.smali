.class public Lorg/spongycastle/jce/spec/e;
.super Ljava/lang/Object;
.source "ECParameterSpec.java"

# interfaces
.implements Ljava/security/spec/AlgorithmParameterSpec;


# instance fields
.field private UV:Lorg/spongycastle/a/a/c;

.field private UW:[B

.field private Va:Ljava/math/BigInteger;

.field private acN:Lorg/spongycastle/a/a/j;

.field private n:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lorg/spongycastle/jce/spec/e;->UV:Lorg/spongycastle/a/a/c;

    .line 54
    iput-object p2, p0, Lorg/spongycastle/jce/spec/e;->acN:Lorg/spongycastle/a/a/j;

    .line 55
    iput-object p3, p0, Lorg/spongycastle/jce/spec/e;->n:Ljava/math/BigInteger;

    .line 56
    iput-object p4, p0, Lorg/spongycastle/jce/spec/e;->Va:Ljava/math/BigInteger;

    .line 57
    iput-object p5, p0, Lorg/spongycastle/jce/spec/e;->UW:[B

    .line 58
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 107
    instance-of v1, p1, Lorg/spongycastle/jce/spec/e;

    if-nez v1, :cond_1

    .line 114
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    check-cast p1, Lorg/spongycastle/jce/spec/e;

    .line 114
    invoke-virtual {p0}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/spongycastle/jce/spec/e;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/jce/spec/e;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSeed()[B
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/spongycastle/jce/spec/e;->UW:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/spongycastle/jce/spec/e;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/a/a/j;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public pO()Lorg/spongycastle/a/a/c;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/spongycastle/jce/spec/e;->UV:Lorg/spongycastle/a/a/c;

    return-object v0
.end method

.method public pP()Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/spongycastle/jce/spec/e;->acN:Lorg/spongycastle/a/a/j;

    return-object v0
.end method

.method public pQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/spongycastle/jce/spec/e;->n:Ljava/math/BigInteger;

    return-object v0
.end method

.method public pR()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/spongycastle/jce/spec/e;->Va:Ljava/math/BigInteger;

    return-object v0
.end method
