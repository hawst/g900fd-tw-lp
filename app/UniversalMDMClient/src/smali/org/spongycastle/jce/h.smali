.class public Lorg/spongycastle/jce/h;
.super Lorg/spongycastle/asn1/q/aw;
.source "X509Principal.java"

# interfaces
.implements Ljava/security/Principal;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/p/c;)V
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p1}, Lorg/spongycastle/asn1/p/c;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/q/aw;-><init>(Lorg/spongycastle/asn1/r;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q/aw;)V
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/aw;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/q/aw;-><init>(Lorg/spongycastle/asn1/r;)V

    .line 58
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/h;-><init>([B)V

    invoke-static {v0}, Lorg/spongycastle/jce/h;->a(Lorg/spongycastle/asn1/h;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/q/aw;-><init>(Lorg/spongycastle/asn1/r;)V

    .line 49
    return-void
.end method

.method private static a(Lorg/spongycastle/asn1/h;)Lorg/spongycastle/asn1/r;
    .locals 4

    .prologue
    .line 33
    :try_start_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 35
    :catch_0
    move-exception v0

    .line 37
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not an ASN.1 Sequence: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getEncoded()[B
    .locals 2

    .prologue
    .line 157
    :try_start_0
    const-string v0, "DER"

    invoke-virtual {p0, v0}, Lorg/spongycastle/jce/h;->getEncoded(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 159
    :catch_0
    move-exception v0

    .line 161
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lorg/spongycastle/jce/h;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
