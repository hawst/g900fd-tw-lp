.class public Lorg/spongycastle/jce/g;
.super Ljava/lang/Object;
.source "X509LDAPCertStoreParameters.java"


# instance fields
.field private adL:Ljava/lang/String;

.field private adM:Ljava/lang/String;

.field private adN:Ljava/lang/String;

.field private adO:Ljava/lang/String;

.field private adP:Ljava/lang/String;

.field private adQ:Ljava/lang/String;

.field private adR:Ljava/lang/String;

.field private adS:Ljava/lang/String;

.field private adT:Ljava/lang/String;

.field private adU:Ljava/lang/String;

.field private adV:Ljava/lang/String;

.field private adW:Ljava/lang/String;

.field private adX:Ljava/lang/String;

.field private adY:Ljava/lang/String;

.field private adZ:Ljava/lang/String;

.field private aea:Ljava/lang/String;

.field private aeb:Ljava/lang/String;

.field private aec:Ljava/lang/String;

.field private aed:Ljava/lang/String;

.field private aee:Ljava/lang/String;

.field private aef:Ljava/lang/String;

.field private aeg:Ljava/lang/String;

.field private aeh:Ljava/lang/String;

.field private aei:Ljava/lang/String;

.field private aej:Ljava/lang/String;

.field private aek:Ljava/lang/String;

.field private ael:Ljava/lang/String;

.field private aem:Ljava/lang/String;

.field private aen:Ljava/lang/String;

.field private aeo:Ljava/lang/String;

.field private aep:Ljava/lang/String;

.field private aeq:Ljava/lang/String;

.field private aer:Ljava/lang/String;

.field private aes:Ljava/lang/String;

.field private aet:Ljava/lang/String;

.field private baseDN:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 177
    const-string v0, "ldap://localhost:389"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lorg/spongycastle/jce/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p1, p0, Lorg/spongycastle/jce/g;->adL:Ljava/lang/String;

    .line 183
    if-nez p2, :cond_0

    .line 185
    const-string v0, ""

    iput-object v0, p0, Lorg/spongycastle/jce/g;->baseDN:Ljava/lang/String;

    .line 192
    :goto_0
    const-string v0, "userCertificate"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adM:Ljava/lang/String;

    .line 193
    const-string v0, "cACertificate"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adN:Ljava/lang/String;

    .line 194
    const-string v0, "crossCertificatePair"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adO:Ljava/lang/String;

    .line 195
    const-string v0, "certificateRevocationList"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adP:Ljava/lang/String;

    .line 196
    const-string v0, "deltaRevocationList"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adQ:Ljava/lang/String;

    .line 197
    const-string v0, "authorityRevocationList"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adR:Ljava/lang/String;

    .line 198
    const-string v0, "attributeCertificateAttribute"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adS:Ljava/lang/String;

    .line 199
    const-string v0, "aACertificate"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adT:Ljava/lang/String;

    .line 200
    const-string v0, "attributeDescriptorCertificate"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adU:Ljava/lang/String;

    .line 201
    const-string v0, "attributeCertificateRevocationList"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adV:Ljava/lang/String;

    .line 202
    const-string v0, "attributeAuthorityRevocationList"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adW:Ljava/lang/String;

    .line 203
    const-string v0, "cn"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adX:Ljava/lang/String;

    .line 204
    const-string v0, "cn ou o"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adY:Ljava/lang/String;

    .line 205
    const-string v0, "cn ou o"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->adZ:Ljava/lang/String;

    .line 206
    const-string v0, "cn ou o"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aea:Ljava/lang/String;

    .line 207
    const-string v0, "cn ou o"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aeb:Ljava/lang/String;

    .line 208
    const-string v0, "cn ou o"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aec:Ljava/lang/String;

    .line 209
    const-string v0, "cn"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aed:Ljava/lang/String;

    .line 210
    const-string v0, "cn o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aee:Ljava/lang/String;

    .line 211
    const-string v0, "cn o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aef:Ljava/lang/String;

    .line 212
    const-string v0, "cn o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aeg:Ljava/lang/String;

    .line 213
    const-string v0, "cn o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aeh:Ljava/lang/String;

    .line 214
    const-string v0, "cn"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aei:Ljava/lang/String;

    .line 215
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aej:Ljava/lang/String;

    .line 216
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aek:Ljava/lang/String;

    .line 217
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->ael:Ljava/lang/String;

    .line 218
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aem:Ljava/lang/String;

    .line 219
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aen:Ljava/lang/String;

    .line 220
    const-string v0, "cn"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aeo:Ljava/lang/String;

    .line 221
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aep:Ljava/lang/String;

    .line 222
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aeq:Ljava/lang/String;

    .line 223
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aer:Ljava/lang/String;

    .line 224
    const-string v0, "o ou"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aes:Ljava/lang/String;

    .line 225
    const-string v0, "uid serialNumber cn"

    iput-object v0, p0, Lorg/spongycastle/jce/g;->aet:Ljava/lang/String;

    .line 226
    return-void

    .line 189
    :cond_0
    iput-object p2, p0, Lorg/spongycastle/jce/g;->baseDN:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method static synthetic A(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aek:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->ael:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic C(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aem:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic D(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aen:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic F(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aep:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic G(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic H(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aer:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic I(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aes:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic J(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aet:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->baseDN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adM:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adO:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adQ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adR:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adS:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adT:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adU:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adV:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adW:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adX:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adY:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adZ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aea:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeb:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aec:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aed:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aee:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic v(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aef:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeh:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aei:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z(Lorg/spongycastle/jce/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/jce/g;->aej:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public rS()Lorg/spongycastle/jce/f;
    .locals 2

    .prologue
    .line 770
    iget-object v0, p0, Lorg/spongycastle/jce/g;->adX:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->adY:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->adZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aea:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeb:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aec:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aed:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aee:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aef:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeh:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aei:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aej:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aek:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->ael:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aem:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aen:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeo:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aep:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aeq:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aer:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/g;->aes:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 793
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Necessary parameters not specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 796
    :cond_1
    new-instance v0, Lorg/spongycastle/jce/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/spongycastle/jce/f;-><init>(Lorg/spongycastle/jce/g;Lorg/spongycastle/jce/f$1;)V

    return-object v0
.end method
