.class public Lorg/spongycastle/jce/provider/JCEECPublicKey;
.super Ljava/lang/Object;
.source "JCEECPublicKey.java"

# interfaces
.implements Ljava/security/interfaces/ECPublicKey;
.implements Lorg/spongycastle/jce/interfaces/ECPublicKey;


# instance fields
.field private algorithm:Ljava/lang/String;

.field private ecSpec:Ljava/security/spec/ECParameterSpec;

.field private gostParams:Lorg/spongycastle/asn1/c/e;

.field private q:Lorg/spongycastle/a/a/j;

.field private withCompression:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/security/spec/ECPublicKeySpec;)V
    .locals 3

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 67
    invoke-virtual {p2}, Ljava/security/spec/ECPublicKeySpec;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 68
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {p2}, Ljava/security/spec/ECPublicKeySpec;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertPoint(Ljava/security/spec/ECParameterSpec;Ljava/security/spec/ECPoint;Z)Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/spongycastle/crypto/j/u;)V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 150
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 151
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/u;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 153
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/spongycastle/crypto/j/u;Ljava/security/spec/ECParameterSpec;)V
    .locals 3

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 102
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/u;->qM()Lorg/spongycastle/crypto/j/q;

    move-result-object v0

    .line 104
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 105
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/u;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 107
    if-nez p3, :cond_0

    .line 109
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/q;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/q;->getSeed()[B

    move-result-object v2

    invoke-static {v1, v2}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Lorg/spongycastle/a/a/c;[B)Ljava/security/spec/EllipticCurve;

    move-result-object v1

    .line 111
    invoke-direct {p0, v1, v0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->createSpec(Ljava/security/spec/EllipticCurve;Lorg/spongycastle/crypto/j/q;)Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iput-object p3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/spongycastle/crypto/j/u;Lorg/spongycastle/jce/spec/e;)V
    .locals 3

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 124
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/u;->qM()Lorg/spongycastle/crypto/j/q;

    move-result-object v0

    .line 126
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 127
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/u;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 129
    if-nez p3, :cond_0

    .line 131
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/q;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/q;->getSeed()[B

    move-result-object v2

    invoke-static {v1, v2}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Lorg/spongycastle/a/a/c;[B)Ljava/security/spec/EllipticCurve;

    move-result-object v1

    .line 133
    invoke-direct {p0, v1, v0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->createSpec(Ljava/security/spec/EllipticCurve;Lorg/spongycastle/crypto/j/q;)Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 141
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual {p3}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    invoke-virtual {p3}, Lorg/spongycastle/jce/spec/e;->getSeed()[B

    move-result-object v1

    invoke-static {v0, v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Lorg/spongycastle/a/a/c;[B)Ljava/security/spec/EllipticCurve;

    move-result-object v0

    .line 139
    invoke-static {v0, p3}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertSpec(Ljava/security/spec/EllipticCurve;Lorg/spongycastle/jce/spec/e;)Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/spongycastle/jce/provider/JCEECPublicKey;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 56
    iget-object v0, p2, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 57
    iget-object v0, p2, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 58
    iget-boolean v0, p2, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    iput-boolean v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    .line 59
    iget-object v0, p2, Lorg/spongycastle/jce/provider/JCEECPublicKey;->gostParams:Lorg/spongycastle/asn1/c/e;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->gostParams:Lorg/spongycastle/asn1/c/e;

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/spongycastle/jce/spec/g;)V
    .locals 4

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 75
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 76
    invoke-virtual {p2}, Lorg/spongycastle/jce/spec/g;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 78
    invoke-virtual {p2}, Lorg/spongycastle/jce/spec/g;->sc()Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p2}, Lorg/spongycastle/jce/spec/g;->sc()Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    .line 81
    invoke-virtual {p2}, Lorg/spongycastle/jce/spec/g;->sc()Lorg/spongycastle/jce/spec/e;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/jce/spec/e;->getSeed()[B

    move-result-object v1

    invoke-static {v0, v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Lorg/spongycastle/a/a/c;[B)Ljava/security/spec/EllipticCurve;

    move-result-object v0

    .line 83
    invoke-virtual {p2}, Lorg/spongycastle/jce/spec/g;->sc()Lorg/spongycastle/jce/spec/e;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertSpec(Ljava/security/spec/EllipticCurve;Lorg/spongycastle/jce/spec/e;)Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 95
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/j;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    if-nez v0, :cond_1

    .line 89
    sget-object v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->CONFIGURATION:Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v1}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lorg/spongycastle/a/a/c;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 93
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/security/interfaces/ECPublicKey;)V
    .locals 3

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 169
    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 170
    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 171
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertPoint(Ljava/security/spec/ECParameterSpec;Ljava/security/spec/ECPoint;Z)Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 172
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/asn1/q/ah;)V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "EC"

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 177
    invoke-direct {p0, p1}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->populateFromPubKeyInfo(Lorg/spongycastle/asn1/q/ah;)V

    .line 178
    return-void
.end method

.method private createSpec(Ljava/security/spec/EllipticCurve;Lorg/spongycastle/crypto/j/q;)Ljava/security/spec/ECParameterSpec;
    .locals 4

    .prologue
    .line 157
    new-instance v0, Ljava/security/spec/ECParameterSpec;

    new-instance v1, Ljava/security/spec/ECPoint;

    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/q;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/q;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/security/spec/ECPoint;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/q;->pQ()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/q;->pR()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigInteger;->intValue()I

    move-result v3

    invoke-direct {v0, p1, v1, v2, v3}, Ljava/security/spec/ECParameterSpec;-><init>(Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;I)V

    return-object v0
.end method

.method private extractBytes([BILjava/math/BigInteger;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v5, 0x20

    .line 397
    invoke-virtual {p3}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    .line 398
    array-length v0, v1

    if-ge v0, v5, :cond_1

    .line 400
    new-array v0, v5, [B

    .line 401
    array-length v3, v0

    array-length v4, v1

    sub-int/2addr v3, v4

    array-length v4, v1

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    move v1, v2

    .line 405
    :goto_1
    if-eq v1, v5, :cond_0

    .line 407
    add-int v2, p2, v1

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v1

    aget-byte v3, v0, v3

    aput-byte v3, p1, v2

    .line 405
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 409
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private populateFromPubKeyInfo(Lorg/spongycastle/asn1/q/ah;)V
    .locals 11

    .prologue
    const/16 v2, 0x20

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 182
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/ah;->nJ()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->on()Lorg/spongycastle/asn1/l;

    move-result-object v0

    sget-object v1, Lorg/spongycastle/asn1/c/a;->Hq:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/ah;->pr()Lorg/spongycastle/asn1/aq;

    move-result-object v0

    .line 186
    const-string v1, "ECGOST3410"

    iput-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 190
    :try_start_0
    invoke-virtual {v0}, Lorg/spongycastle/asn1/aq;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v1

    .line 198
    new-array v3, v2, [B

    .line 199
    new-array v4, v2, [B

    move v0, v6

    .line 201
    :goto_0
    array-length v2, v3

    if-eq v0, v2, :cond_0

    .line 203
    rsub-int/lit8 v2, v0, 0x1f

    aget-byte v2, v1, v2

    aput-byte v2, v3, v0

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :catch_0
    move-exception v0

    .line 194
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "error recovering public key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v6

    .line 206
    :goto_1
    array-length v2, v4

    if-eq v0, v2, :cond_1

    .line 208
    rsub-int/lit8 v2, v0, 0x3f

    aget-byte v2, v1, v2

    aput-byte v2, v4, v0

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 211
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/c/e;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/ah;->nJ()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/c/e;-><init>(Lorg/spongycastle/asn1/r;)V

    iput-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->gostParams:Lorg/spongycastle/asn1/c/e;

    .line 213
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->gostParams:Lorg/spongycastle/asn1/c/e;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/c/e;->nq()Lorg/spongycastle/asn1/l;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/c/b;->b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/jce/a;->cS(Ljava/lang/String;)Lorg/spongycastle/jce/spec/c;

    move-result-object v5

    .line 215
    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/c;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    .line 216
    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/c;->getSeed()[B

    move-result-object v1

    invoke-static {v0, v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Lorg/spongycastle/a/a/c;[B)Ljava/security/spec/EllipticCurve;

    move-result-object v2

    .line 218
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v9, v3}, Ljava/math/BigInteger;-><init>(I[B)V

    new-instance v3, Ljava/math/BigInteger;

    invoke-direct {v3, v9, v4}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-virtual {v0, v1, v3, v6}, Lorg/spongycastle/a/a/c;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    .line 220
    new-instance v0, Lorg/spongycastle/jce/spec/d;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->gostParams:Lorg/spongycastle/asn1/c/e;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/c/e;->nq()Lorg/spongycastle/asn1/l;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/c/b;->b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/security/spec/ECPoint;

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/c;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/c;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v6

    invoke-virtual {v6}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v6

    invoke-virtual {v6}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Ljava/security/spec/ECPoint;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/c;->pQ()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/jce/spec/c;->pR()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/jce/spec/d;-><init>(Ljava/lang/String;Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 301
    :goto_2
    return-void

    .line 231
    :cond_2
    new-instance v1, Lorg/spongycastle/asn1/r/d;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/ah;->nJ()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/q;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/r/d;-><init>(Lorg/spongycastle/asn1/q;)V

    .line 235
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/d;->pL()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 237
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/d;->od()Lorg/spongycastle/asn1/q;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lorg/spongycastle/asn1/l;

    .line 238
    invoke-static {v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/ECUtil;->getNamedCurveByOid(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v5

    .line 240
    invoke-virtual {v5}, Lorg/spongycastle/asn1/r/f;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v7

    .line 241
    invoke-virtual {v5}, Lorg/spongycastle/asn1/r/f;->getSeed()[B

    move-result-object v0

    invoke-static {v7, v0}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Lorg/spongycastle/a/a/c;[B)Ljava/security/spec/EllipticCurve;

    move-result-object v2

    .line 243
    new-instance v0, Lorg/spongycastle/jce/spec/d;

    invoke-static {v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/ECUtil;->getCurveName(Lorg/spongycastle/asn1/l;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/security/spec/ECPoint;

    invoke-virtual {v5}, Lorg/spongycastle/asn1/r/f;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/asn1/r/f;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v8

    invoke-virtual {v8}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v8

    invoke-virtual {v8}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v8

    invoke-direct {v3, v4, v8}, Ljava/security/spec/ECPoint;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-virtual {v5}, Lorg/spongycastle/asn1/r/f;->pQ()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v5}, Lorg/spongycastle/asn1/r/f;->pR()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/jce/spec/d;-><init>(Ljava/lang/String;Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    move-object v1, v7

    .line 273
    :goto_3
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/ah;->pr()Lorg/spongycastle/asn1/aq;

    move-result-object v0

    .line 274
    invoke-virtual {v0}, Lorg/spongycastle/asn1/aq;->getBytes()[B

    move-result-object v2

    .line 275
    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-direct {v0, v2}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    .line 280
    aget-byte v3, v2, v6

    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    aget-byte v3, v2, v9

    array-length v4, v2

    add-int/lit8 v4, v4, -0x2

    if-ne v3, v4, :cond_4

    aget-byte v3, v2, v10

    if-eq v3, v10, :cond_3

    aget-byte v3, v2, v10

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 283
    :cond_3
    new-instance v3, Lorg/spongycastle/asn1/r/k;

    invoke-direct {v3}, Lorg/spongycastle/asn1/r/k;-><init>()V

    invoke-virtual {v3, v1}, Lorg/spongycastle/asn1/r/k;->a(Lorg/spongycastle/a/a/c;)I

    move-result v3

    .line 285
    array-length v4, v2

    add-int/lit8 v4, v4, -0x3

    if-lt v3, v4, :cond_4

    .line 289
    :try_start_1
    invoke-static {v2}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 297
    :cond_4
    new-instance v2, Lorg/spongycastle/asn1/r/h;

    invoke-direct {v2, v1, v0}, Lorg/spongycastle/asn1/r/h;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/asn1/m;)V

    .line 299
    invoke-virtual {v2}, Lorg/spongycastle/asn1/r/h;->pT()Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    goto/16 :goto_2

    .line 252
    :cond_5
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/d;->pM()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    .line 255
    sget-object v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->CONFIGURATION:Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    .line 259
    :cond_6
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/d;->od()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r/f;->bn(Ljava/lang/Object;)Lorg/spongycastle/asn1/r/f;

    move-result-object v1

    .line 261
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    .line 262
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->getSeed()[B

    move-result-object v2

    invoke-static {v0, v2}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Lorg/spongycastle/a/a/c;[B)Ljava/security/spec/EllipticCurve;

    move-result-object v2

    .line 264
    new-instance v3, Ljava/security/spec/ECParameterSpec;

    new-instance v4, Ljava/security/spec/ECPoint;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v5

    invoke-virtual {v5}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v5

    invoke-virtual {v5}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v7

    invoke-virtual {v7}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v7

    invoke-virtual {v7}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v7

    invoke-direct {v4, v5, v7}, Ljava/security/spec/ECPoint;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pQ()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/f;->pR()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    invoke-direct {v3, v2, v4, v5, v1}, Ljava/security/spec/ECParameterSpec;-><init>(Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;I)V

    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    move-object v1, v0

    goto/16 :goto_3

    .line 291
    :catch_1
    move-exception v0

    .line 293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "error recovering public key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    .line 504
    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ah;->aX(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ah;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->populateFromPubKeyInfo(Lorg/spongycastle/asn1/q/ah;)V

    .line 506
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    .line 507
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    .line 508
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 514
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->getEncoded()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 515
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 516
    iget-boolean v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 517
    return-void
.end method


# virtual methods
.method public engineGetQ()Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    return-object v0
.end method

.method engineGetSpec()Lorg/spongycastle/jce/spec/e;
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    iget-boolean v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    invoke-static {v0, v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertSpec(Ljava/security/spec/ECParameterSpec;Z)Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    .line 460
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->CONFIGURATION:Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lorg/spongycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 483
    instance-of v1, p1, Lorg/spongycastle/jce/provider/JCEECPublicKey;

    if-nez v1, :cond_1

    .line 490
    :cond_0
    :goto_0
    return v0

    .line 488
    :cond_1
    check-cast p1, Lorg/spongycastle/jce/provider/JCEECPublicKey;

    .line 490
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->engineGetQ()Lorg/spongycastle/a/a/j;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->engineGetQ()Lorg/spongycastle/a/a/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->engineGetSpec()Lorg/spongycastle/jce/spec/e;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->engineGetSpec()Lorg/spongycastle/jce/spec/e;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/jce/spec/e;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getEncoded()[B
    .locals 6

    .prologue
    .line 318
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->algorithm:Ljava/lang/String;

    const-string v1, "ECGOST3410"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 320
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->gostParams:Lorg/spongycastle/asn1/c/e;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->gostParams:Lorg/spongycastle/asn1/c/e;

    .line 347
    :goto_0
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v1}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v1

    .line 348
    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    .line 349
    const/16 v3, 0x40

    new-array v3, v3, [B

    .line 351
    const/4 v4, 0x0

    invoke-direct {p0, v3, v4, v1}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->extractBytes([BILjava/math/BigInteger;)V

    .line 352
    const/16 v1, 0x20

    invoke-direct {p0, v3, v1, v2}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->extractBytes([BILjava/math/BigInteger;)V

    .line 354
    new-instance v1, Lorg/spongycastle/asn1/q/ah;

    new-instance v2, Lorg/spongycastle/asn1/q/a;

    sget-object v4, Lorg/spongycastle/asn1/c/a;->Hq:Lorg/spongycastle/asn1/l;

    invoke-direct {v2, v4, v0}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-direct {v0, v3}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/q/ah;-><init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)V

    move-object v0, v1

    .line 392
    :goto_1
    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/util/KeyUtil;->getEncodedSubjectPublicKeyInfo(Lorg/spongycastle/asn1/q/ah;)[B

    move-result-object v0

    return-object v0

    .line 326
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    instance-of v0, v0, Lorg/spongycastle/jce/spec/d;

    if-eqz v0, :cond_1

    .line 328
    new-instance v1, Lorg/spongycastle/asn1/c/e;

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    check-cast v0, Lorg/spongycastle/jce/spec/d;

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/c/b;->getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sget-object v2, Lorg/spongycastle/asn1/c/a;->Ht:Lorg/spongycastle/asn1/l;

    invoke-direct {v1, v0, v2}, Lorg/spongycastle/asn1/c/e;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/l;)V

    move-object v0, v1

    goto :goto_0

    .line 334
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Ljava/security/spec/EllipticCurve;)Lorg/spongycastle/a/a/c;

    move-result-object v1

    .line 336
    new-instance v0, Lorg/spongycastle/asn1/r/f;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v2}, Ljava/security/spec/ECParameterSpec;->getGenerator()Ljava/security/spec/ECPoint;

    move-result-object v2

    iget-boolean v3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    invoke-static {v1, v2, v3}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertPoint(Lorg/spongycastle/a/a/c;Ljava/security/spec/ECPoint;Z)Lorg/spongycastle/a/a/j;

    move-result-object v2

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v3}, Ljava/security/spec/ECParameterSpec;->getOrder()Ljava/math/BigInteger;

    move-result-object v3

    iget-object v4, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v4}, Ljava/security/spec/ECParameterSpec;->getCofactor()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    iget-object v5, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v5}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v5

    invoke-virtual {v5}, Ljava/security/spec/EllipticCurve;->getSeed()[B

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/asn1/r/f;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    .line 343
    new-instance v1, Lorg/spongycastle/asn1/r/d;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/r/d;-><init>(Lorg/spongycastle/asn1/r/f;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 358
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    instance-of v0, v0, Lorg/spongycastle/jce/spec/d;

    if-eqz v0, :cond_4

    .line 360
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    check-cast v0, Lorg/spongycastle/jce/spec/d;

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/ECUtil;->getNamedCurveOid(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    .line 361
    if-nez v0, :cond_3

    .line 363
    new-instance v1, Lorg/spongycastle/asn1/l;

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    check-cast v0, Lorg/spongycastle/jce/spec/d;

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 365
    :cond_3
    new-instance v1, Lorg/spongycastle/asn1/r/d;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/r/d;-><init>(Lorg/spongycastle/asn1/l;)V

    .line 385
    :goto_2
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->engineGetQ()Lorg/spongycastle/a/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/j;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    .line 386
    new-instance v2, Lorg/spongycastle/asn1/r/h;

    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->getQ()Lorg/spongycastle/a/a/j;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v4

    iget-boolean v5, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    invoke-virtual {v0, v3, v4, v5}, Lorg/spongycastle/a/a/c;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lorg/spongycastle/a/a/j;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/r/h;-><init>(Lorg/spongycastle/a/a/j;)V

    invoke-virtual {v2}, Lorg/spongycastle/asn1/r/h;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    .line 389
    new-instance v2, Lorg/spongycastle/asn1/q/ah;

    new-instance v3, Lorg/spongycastle/asn1/q/a;

    sget-object v4, Lorg/spongycastle/asn1/r/l;->Vq:Lorg/spongycastle/asn1/l;

    invoke-direct {v3, v4, v1}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/spongycastle/asn1/q/ah;-><init>(Lorg/spongycastle/asn1/q/a;[B)V

    move-object v0, v2

    goto/16 :goto_1

    .line 367
    :cond_4
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    if-nez v0, :cond_5

    .line 369
    new-instance v0, Lorg/spongycastle/asn1/r/d;

    sget-object v1, Lorg/spongycastle/asn1/ba;->GC:Lorg/spongycastle/asn1/ba;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/r/d;-><init>(Lorg/spongycastle/asn1/q;)V

    move-object v1, v0

    goto :goto_2

    .line 373
    :cond_5
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertCurve(Ljava/security/spec/EllipticCurve;)Lorg/spongycastle/a/a/c;

    move-result-object v1

    .line 375
    new-instance v0, Lorg/spongycastle/asn1/r/f;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v2}, Ljava/security/spec/ECParameterSpec;->getGenerator()Ljava/security/spec/ECPoint;

    move-result-object v2

    iget-boolean v3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    invoke-static {v1, v2, v3}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertPoint(Lorg/spongycastle/a/a/c;Ljava/security/spec/ECPoint;Z)Lorg/spongycastle/a/a/j;

    move-result-object v2

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v3}, Ljava/security/spec/ECParameterSpec;->getOrder()Ljava/math/BigInteger;

    move-result-object v3

    iget-object v4, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v4}, Ljava/security/spec/ECParameterSpec;->getCofactor()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    iget-object v5, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v5}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v5

    invoke-virtual {v5}, Ljava/security/spec/EllipticCurve;->getSeed()[B

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/asn1/r/f;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    .line 382
    new-instance v1, Lorg/spongycastle/asn1/r/d;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/r/d;-><init>(Lorg/spongycastle/asn1/r/f;)V

    goto/16 :goto_2
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    const-string v0, "X.509"

    return-object v0
.end method

.method public getParameters()Lorg/spongycastle/jce/spec/e;
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    if-nez v0, :cond_0

    .line 420
    const/4 v0, 0x0

    .line 423
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    iget-boolean v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    invoke-static {v0, v1}, Lorg/spongycastle/jcajce/provider/asymmetric/ec/EC5Util;->convertSpec(Ljava/security/spec/ECParameterSpec;Z)Lorg/spongycastle/jce/spec/e;

    move-result-object v0

    goto :goto_0
.end method

.method public getParams()Ljava/security/spec/ECParameterSpec;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    return-object v0
.end method

.method public getQ()Lorg/spongycastle/a/a/j;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 433
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->ecSpec:Ljava/security/spec/ECParameterSpec;

    if-nez v0, :cond_1

    .line 435
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    instance-of v0, v0, Lorg/spongycastle/a/a/l;

    if-eqz v0, :cond_0

    .line 437
    new-instance v0, Lorg/spongycastle/a/a/l;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v1}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2}, Lorg/spongycastle/a/a/l;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V

    .line 445
    :goto_0
    return-object v0

    .line 441
    :cond_0
    new-instance v0, Lorg/spongycastle/a/a/k;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v1}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2}, Lorg/spongycastle/a/a/k;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V

    goto :goto_0

    .line 445
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    goto :goto_0
.end method

.method public getW()Ljava/security/spec/ECPoint;
    .locals 3

    .prologue
    .line 428
    new-instance v0, Ljava/security/spec/ECPoint;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v1}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/security/spec/ECPoint;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 495
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->engineGetQ()Lorg/spongycastle/a/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/j;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEECPublicKey;->engineGetSpec()Lorg/spongycastle/jce/spec/e;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/jce/spec/e;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public setPointFormat(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 478
    const-string v0, "UNCOMPRESSED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->withCompression:Z

    .line 479
    return-void

    .line 478
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 465
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 466
    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 468
    const-string v2, "EC Public Key"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 469
    const-string v2, "            X: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v3}, Lorg/spongycastle/a/a/j;->st()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 470
    const-string v2, "            Y: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEECPublicKey;->q:Lorg/spongycastle/a/a/j;

    invoke-virtual {v3}, Lorg/spongycastle/a/a/j;->su()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 472
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
