.class public Lorg/spongycastle/jce/provider/BrokenPBE$Util;
.super Ljava/lang/Object;
.source "BrokenPBE.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static makePBEGenerator(II)Lorg/spongycastle/crypto/p;
    .locals 2

    .prologue
    .line 293
    if-nez p0, :cond_0

    .line 295
    packed-switch p1, :pswitch_data_0

    .line 304
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PKCS5 scheme 1 only supports only MD5 and SHA1."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :pswitch_0
    new-instance v0, Lorg/spongycastle/crypto/e/r;

    new-instance v1, Lorg/spongycastle/crypto/b/f;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/f;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/e/r;-><init>(Lorg/spongycastle/crypto/l;)V

    .line 346
    :goto_0
    return-object v0

    .line 301
    :pswitch_1
    new-instance v0, Lorg/spongycastle/crypto/e/r;

    new-instance v1, Lorg/spongycastle/crypto/b/l;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/l;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/e/r;-><init>(Lorg/spongycastle/crypto/l;)V

    goto :goto_0

    .line 307
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 309
    new-instance v0, Lorg/spongycastle/crypto/e/s;

    invoke-direct {v0}, Lorg/spongycastle/crypto/e/s;-><init>()V

    goto :goto_0

    .line 311
    :cond_1
    const/4 v0, 0x3

    if-ne p0, v0, :cond_2

    .line 313
    packed-switch p1, :pswitch_data_1

    .line 325
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unknown digest scheme for PBE encryption."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :pswitch_2
    new-instance v0, Lorg/spongycastle/jce/provider/OldPKCS12ParametersGenerator;

    new-instance v1, Lorg/spongycastle/crypto/b/f;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/f;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/OldPKCS12ParametersGenerator;-><init>(Lorg/spongycastle/crypto/l;)V

    goto :goto_0

    .line 319
    :pswitch_3
    new-instance v0, Lorg/spongycastle/jce/provider/OldPKCS12ParametersGenerator;

    new-instance v1, Lorg/spongycastle/crypto/b/l;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/l;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/OldPKCS12ParametersGenerator;-><init>(Lorg/spongycastle/crypto/l;)V

    goto :goto_0

    .line 322
    :pswitch_4
    new-instance v0, Lorg/spongycastle/jce/provider/OldPKCS12ParametersGenerator;

    new-instance v1, Lorg/spongycastle/crypto/b/i;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/i;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/OldPKCS12ParametersGenerator;-><init>(Lorg/spongycastle/crypto/l;)V

    goto :goto_0

    .line 330
    :cond_2
    packed-switch p1, :pswitch_data_2

    .line 342
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unknown digest scheme for PBE encryption."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :pswitch_5
    new-instance v0, Lorg/spongycastle/crypto/e/q;

    new-instance v1, Lorg/spongycastle/crypto/b/f;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/f;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/e/q;-><init>(Lorg/spongycastle/crypto/l;)V

    goto :goto_0

    .line 336
    :pswitch_6
    new-instance v0, Lorg/spongycastle/crypto/e/q;

    new-instance v1, Lorg/spongycastle/crypto/b/l;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/l;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/e/q;-><init>(Lorg/spongycastle/crypto/l;)V

    goto :goto_0

    .line 339
    :pswitch_7
    new-instance v0, Lorg/spongycastle/crypto/e/q;

    new-instance v1, Lorg/spongycastle/crypto/b/i;

    invoke-direct {v1}, Lorg/spongycastle/crypto/b/i;-><init>()V

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/e/q;-><init>(Lorg/spongycastle/crypto/l;)V

    goto :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 313
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 330
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static makePBEMacParameters(Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;III)Lorg/spongycastle/crypto/h;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 419
    if-eqz p1, :cond_0

    instance-of v0, p1, Ljavax/crypto/spec/PBEParameterSpec;

    if-nez v0, :cond_1

    .line 421
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Need a PBEParameter spec with a PBE key."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424
    :cond_1
    check-cast p1, Ljavax/crypto/spec/PBEParameterSpec;

    .line 425
    invoke-static {p2, p3}, Lorg/spongycastle/jce/provider/BrokenPBE$Util;->makePBEGenerator(II)Lorg/spongycastle/crypto/p;

    move-result-object v0

    .line 426
    invoke-virtual {p0}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getEncoded()[B

    move-result-object v2

    .line 429
    invoke-virtual {p1}, Ljavax/crypto/spec/PBEParameterSpec;->getSalt()[B

    move-result-object v3

    invoke-virtual {p1}, Ljavax/crypto/spec/PBEParameterSpec;->getIterationCount()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lorg/spongycastle/crypto/p;->init([B[BI)V

    .line 431
    invoke-virtual {v0, p4}, Lorg/spongycastle/crypto/p;->generateDerivedMacParameters(I)Lorg/spongycastle/crypto/h;

    move-result-object v3

    move v0, v1

    .line 433
    :goto_0
    array-length v4, v2

    if-eq v0, v4, :cond_2

    .line 435
    aput-byte v1, v2, v0

    .line 433
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 438
    :cond_2
    return-object v3
.end method

.method static makePBEParameters(Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;IILjava/lang/String;II)Lorg/spongycastle/crypto/h;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 362
    if-eqz p1, :cond_0

    instance-of v0, p1, Ljavax/crypto/spec/PBEParameterSpec;

    if-nez v0, :cond_1

    .line 364
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Need a PBEParameter spec with a PBE key."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_1
    check-cast p1, Ljavax/crypto/spec/PBEParameterSpec;

    .line 368
    invoke-static {p2, p3}, Lorg/spongycastle/jce/provider/BrokenPBE$Util;->makePBEGenerator(II)Lorg/spongycastle/crypto/p;

    move-result-object v0

    .line 369
    invoke-virtual {p0}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getEncoded()[B

    move-result-object v3

    .line 372
    invoke-virtual {p1}, Ljavax/crypto/spec/PBEParameterSpec;->getSalt()[B

    move-result-object v1

    invoke-virtual {p1}, Ljavax/crypto/spec/PBEParameterSpec;->getIterationCount()I

    move-result v4

    invoke-virtual {v0, v3, v1, v4}, Lorg/spongycastle/crypto/p;->init([B[BI)V

    .line 374
    if-eqz p6, :cond_3

    .line 376
    invoke-virtual {v0, p5, p6}, Lorg/spongycastle/crypto/p;->generateDerivedParameters(II)Lorg/spongycastle/crypto/h;

    move-result-object v1

    .line 383
    :goto_0
    const-string v0, "DES"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385
    instance-of v0, v1, Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 387
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/an;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/crypto/j/ak;

    .line 389
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ak;->getKey()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/jce/provider/BrokenPBE$Util;->setOddParity([B)V

    :cond_2
    :goto_1
    move v0, v2

    .line 399
    :goto_2
    array-length v4, v3

    if-eq v0, v4, :cond_5

    .line 401
    aput-byte v2, v3, v0

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 380
    :cond_3
    invoke-virtual {v0, p5}, Lorg/spongycastle/crypto/p;->generateDerivedParameters(I)Lorg/spongycastle/crypto/h;

    move-result-object v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 393
    check-cast v0, Lorg/spongycastle/crypto/j/ak;

    .line 395
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ak;->getKey()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/jce/provider/BrokenPBE$Util;->setOddParity([B)V

    goto :goto_1

    .line 404
    :cond_5
    return-object v1
.end method

.method private static setOddParity([B)V
    .locals 5

    .prologue
    .line 273
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 275
    aget-byte v1, p0, v0

    .line 276
    and-int/lit16 v2, v1, 0xfe

    shr-int/lit8 v3, v1, 0x1

    shr-int/lit8 v4, v1, 0x2

    xor-int/2addr v3, v4

    shr-int/lit8 v4, v1, 0x3

    xor-int/2addr v3, v4

    shr-int/lit8 v4, v1, 0x4

    xor-int/2addr v3, v4

    shr-int/lit8 v4, v1, 0x5

    xor-int/2addr v3, v4

    shr-int/lit8 v4, v1, 0x6

    xor-int/2addr v3, v4

    shr-int/lit8 v1, v1, 0x7

    xor-int/2addr v1, v3

    xor-int/lit8 v1, v1, 0x1

    or-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 273
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_0
    return-void
.end method
