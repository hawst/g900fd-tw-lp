.class public Lorg/spongycastle/jce/provider/PKIXCRLUtil;
.super Ljava/lang/Object;
.source "PKIXCRLUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final findCRLs(Lorg/spongycastle/x509/i;Ljava/util/List;)Ljava/util/Collection;
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 107
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 108
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 110
    const/4 v3, 0x0

    .line 111
    const/4 v2, 0x0

    .line 113
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 117
    instance-of v6, v0, Lorg/spongycastle/x509/n;

    if-eqz v6, :cond_0

    .line 119
    check-cast v0, Lorg/spongycastle/x509/n;

    .line 123
    :try_start_0
    invoke-virtual {v0, p1}, Lorg/spongycastle/x509/n;->a(Lorg/spongycastle/util/e;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lorg/spongycastle/util/StoreException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    move-object v2, v3

    :goto_1
    move-object v3, v2

    move v2, v0

    .line 147
    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    move-object v3, v0

    .line 128
    new-instance v0, Lorg/spongycastle/jce/provider/AnnotatedException;

    const-string v6, "Exception searching in X.509 CRL store."

    invoke-direct {v0, v6, v3}, Lorg/spongycastle/jce/provider/AnnotatedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v7, v2

    move-object v2, v0

    move v0, v7

    goto :goto_1

    .line 134
    :cond_0
    check-cast v0, Ljava/security/cert/CertStore;

    .line 138
    :try_start_1
    invoke-virtual {v0, p1}, Ljava/security/cert/CertStore;->getCRLs(Ljava/security/cert/CRLSelector;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catch Ljava/security/cert/CertStoreException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    move-object v2, v3

    .line 145
    goto :goto_1

    .line 141
    :catch_1
    move-exception v0

    .line 143
    new-instance v3, Lorg/spongycastle/jce/provider/AnnotatedException;

    const-string v6, "Exception searching in X.509 CRL store."

    invoke-direct {v3, v6, v0}, Lorg/spongycastle/jce/provider/AnnotatedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    move-object v2, v3

    goto :goto_1

    .line 148
    :cond_1
    if-nez v2, :cond_2

    if-eqz v3, :cond_2

    .line 150
    throw v3

    .line 152
    :cond_2
    return-object v4
.end method


# virtual methods
.method public findCRLs(Lorg/spongycastle/x509/i;Ljava/security/cert/PKIXParameters;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 81
    :try_start_0
    invoke-virtual {p2}, Ljava/security/cert/PKIXParameters;->getCertStores()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/jce/provider/PKIXCRLUtil;->findCRLs(Lorg/spongycastle/x509/i;Ljava/util/List;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lorg/spongycastle/jce/provider/AnnotatedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    return-object v0

    .line 83
    :catch_0
    move-exception v0

    .line 85
    new-instance v1, Lorg/spongycastle/jce/provider/AnnotatedException;

    const-string v2, "Exception obtaining complete CRLs."

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/jce/provider/AnnotatedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public findCRLs(Lorg/spongycastle/x509/i;Lorg/spongycastle/x509/d;Ljava/util/Date;)Ljava/util/Set;
    .locals 5

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 30
    :try_start_0
    invoke-virtual {p2}, Lorg/spongycastle/x509/d;->sX()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/jce/provider/PKIXCRLUtil;->findCRLs(Lorg/spongycastle/x509/i;Ljava/util/List;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 31
    invoke-virtual {p2}, Lorg/spongycastle/x509/d;->sY()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/jce/provider/PKIXCRLUtil;->findCRLs(Lorg/spongycastle/x509/i;Ljava/util/List;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 32
    invoke-virtual {p2}, Lorg/spongycastle/x509/d;->getCertStores()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/jce/provider/PKIXCRLUtil;->findCRLs(Lorg/spongycastle/x509/i;Ljava/util/List;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lorg/spongycastle/jce/provider/AnnotatedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 42
    invoke-virtual {p2}, Lorg/spongycastle/x509/d;->getDate()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 44
    invoke-virtual {p2}, Lorg/spongycastle/x509/d;->getDate()Ljava/util/Date;

    move-result-object p3

    .line 48
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 50
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509CRL;

    .line 52
    invoke-virtual {v0}, Ljava/security/cert/X509CRL;->getNextUpdate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 54
    invoke-virtual {p1}, Lorg/spongycastle/x509/i;->getCertificateChecking()Ljava/security/cert/X509Certificate;

    move-result-object v3

    .line 56
    if-eqz v3, :cond_2

    .line 58
    invoke-virtual {v0}, Ljava/security/cert/X509CRL;->getThisUpdate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getNotAfter()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 36
    new-instance v1, Lorg/spongycastle/jce/provider/AnnotatedException;

    const-string v2, "Exception obtaining complete CRLs."

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/jce/provider/AnnotatedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 65
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :cond_3
    return-object v1
.end method
