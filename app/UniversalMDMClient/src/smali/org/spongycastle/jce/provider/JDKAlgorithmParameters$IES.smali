.class public Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;
.super Lorg/spongycastle/jce/provider/JDKAlgorithmParameters;
.source "JDKAlgorithmParameters.java"


# instance fields
.field currentSpec:Lorg/spongycastle/jce/spec/q;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0}, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters;-><init>()V

    return-void
.end method


# virtual methods
.method protected engineGetEncoded()[B
    .locals 3

    .prologue
    .line 224
    :try_start_0
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 226
    new-instance v1, Lorg/spongycastle/asn1/bd;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->currentSpec:Lorg/spongycastle/jce/spec/q;

    invoke-virtual {v2}, Lorg/spongycastle/jce/spec/q;->qP()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 227
    new-instance v1, Lorg/spongycastle/asn1/bd;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->currentSpec:Lorg/spongycastle/jce/spec/q;

    invoke-virtual {v2}, Lorg/spongycastle/jce/spec/q;->qQ()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 228
    new-instance v1, Lorg/spongycastle/asn1/az;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->currentSpec:Lorg/spongycastle/jce/spec/q;

    invoke-virtual {v2}, Lorg/spongycastle/jce/spec/q;->qR()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/az;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 230
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    const-string v0, "DER"

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/bh;->getEncoded(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 232
    :catch_0
    move-exception v0

    .line 234
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error encoding IESParameters"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineGetEncoded(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0, p1}, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->isASN1FormatString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "X.509"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->engineGetEncoded()[B

    move-result-object v0

    .line 246
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected engineInit(Ljava/security/spec/AlgorithmParameterSpec;)V
    .locals 2

    .prologue
    .line 265
    instance-of v0, p1, Lorg/spongycastle/jce/spec/q;

    if-nez v0, :cond_0

    .line 267
    new-instance v0, Ljava/security/spec/InvalidParameterSpecException;

    const-string v1, "IESParameterSpec required to initialise a IES algorithm parameters object"

    invoke-direct {v0, v1}, Ljava/security/spec/InvalidParameterSpecException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_0
    check-cast p1, Lorg/spongycastle/jce/spec/q;

    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->currentSpec:Lorg/spongycastle/jce/spec/q;

    .line 271
    return-void
.end method

.method protected engineInit([B)V
    .locals 5

    .prologue
    .line 279
    :try_start_0
    invoke-static {p1}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    .line 281
    new-instance v2, Lorg/spongycastle/jce/spec/q;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/asn1/m;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/asn1/m;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/az;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    invoke-direct {v2, v3, v1, v0}, Lorg/spongycastle/jce/spec/q;-><init>([B[BI)V

    iput-object v2, p0, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->currentSpec:Lorg/spongycastle/jce/spec/q;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 294
    return-void

    .line 286
    :catch_0
    move-exception v0

    .line 288
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not a valid IES Parameter encoding."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :catch_1
    move-exception v0

    .line 292
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not a valid IES Parameter encoding."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineInit([BLjava/lang/String;)V
    .locals 3

    .prologue
    .line 301
    invoke-virtual {p0, p2}, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->isASN1FormatString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "X.509"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    :cond_0
    invoke-virtual {p0, p1}, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->engineInit([B)V

    .line 309
    return-void

    .line 307
    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown parameter format "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    const-string v0, "IES Parameters"

    return-object v0
.end method

.method protected localEngineGetParameterSpec(Ljava/lang/Class;)Ljava/security/spec/AlgorithmParameterSpec;
    .locals 2

    .prologue
    .line 253
    const-class v0, Lorg/spongycastle/jce/spec/q;

    if-ne p1, v0, :cond_0

    .line 255
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKAlgorithmParameters$IES;->currentSpec:Lorg/spongycastle/jce/spec/q;

    return-object v0

    .line 258
    :cond_0
    new-instance v0, Ljava/security/spec/InvalidParameterSpecException;

    const-string v1, "unknown parameter spec passed to ElGamal parameters object."

    invoke-direct {v0, v1}, Ljava/security/spec/InvalidParameterSpecException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
