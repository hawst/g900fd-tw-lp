.class public Lorg/spongycastle/jce/provider/JCEBlockCipher;
.super Ljavax/crypto/CipherSpi;
.source "JCEBlockCipher.java"

# interfaces
.implements Lorg/spongycastle/jcajce/provider/symmetric/util/PBE;


# static fields
.field private static final EMPTY_BYTE_ARRAY:[B


# instance fields
.field private availableSpecs:[Ljava/lang/Class;

.field private baseEngine:Lorg/spongycastle/crypto/d;

.field private cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

.field private engineParams:Ljava/security/AlgorithmParameters;

.field private ivLength:I

.field private ivParam:Lorg/spongycastle/crypto/j/an;

.field private modeName:Ljava/lang/String;

.field private padded:Z

.field private pbeAlgorithm:Ljava/lang/String;

.field private pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->EMPTY_BYTE_ARRAY:[B

    return-void
.end method

.method protected constructor <init>(Lorg/spongycastle/crypto/d;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 101
    invoke-direct {p0}, Ljavax/crypto/CipherSpi;-><init>()V

    .line 72
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Ljavax/crypto/spec/RC2ParameterSpec;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-class v2, Ljavax/crypto/spec/RC5ParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Ljavax/crypto/spec/IvParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Ljavax/crypto/spec/PBEParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lorg/spongycastle/jce/spec/l;

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->availableSpecs:[Ljava/lang/Class;

    .line 85
    iput v4, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 89
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    .line 90
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeAlgorithm:Ljava/lang/String;

    .line 92
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    .line 102
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    .line 104
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    invoke-direct {v0, p1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    .line 105
    return-void
.end method

.method protected constructor <init>(Lorg/spongycastle/crypto/d;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 110
    invoke-direct {p0}, Ljavax/crypto/CipherSpi;-><init>()V

    .line 72
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Ljavax/crypto/spec/RC2ParameterSpec;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-class v2, Ljavax/crypto/spec/RC5ParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Ljavax/crypto/spec/IvParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Ljavax/crypto/spec/PBEParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lorg/spongycastle/jce/spec/l;

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->availableSpecs:[Ljava/lang/Class;

    .line 85
    iput v4, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 89
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    .line 90
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeAlgorithm:Ljava/lang/String;

    .line 92
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    .line 111
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    .line 113
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    invoke-direct {v0, p1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    .line 114
    div-int/lit8 v0, p2, 0x8

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 115
    return-void
.end method

.method protected constructor <init>(Lorg/spongycastle/crypto/f;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 120
    invoke-direct {p0}, Ljavax/crypto/CipherSpi;-><init>()V

    .line 72
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Ljavax/crypto/spec/RC2ParameterSpec;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-class v2, Ljavax/crypto/spec/RC5ParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Ljavax/crypto/spec/IvParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Ljavax/crypto/spec/PBEParameterSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lorg/spongycastle/jce/spec/l;

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->availableSpecs:[Ljava/lang/Class;

    .line 85
    iput v4, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 89
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    .line 90
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeAlgorithm:Ljava/lang/String;

    .line 92
    iput-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Lorg/spongycastle/crypto/f;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    .line 123
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    invoke-direct {v0, p1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/f;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    .line 124
    div-int/lit8 v0, p2, 0x8

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 125
    return-void
.end method

.method private isAEADModeName(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 743
    const-string v0, "CCM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EAX"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GCM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected engineDoFinal([BII[BI)I
    .locals 6

    .prologue
    .line 719
    const/4 v0, 0x0

    .line 721
    if-eqz p3, :cond_0

    .line 723
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->processBytes([BII[BI)I

    move-result v0

    .line 728
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    add-int v2, p5, v0

    invoke-interface {v1, p4, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->doFinal([BI)I
    :try_end_0
    .catch Lorg/spongycastle/crypto/DataLengthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/spongycastle/crypto/InvalidCipherTextException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 730
    :catch_0
    move-exception v0

    .line 732
    new-instance v1, Ljavax/crypto/IllegalBlockSizeException;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/DataLengthException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljavax/crypto/IllegalBlockSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 734
    :catch_1
    move-exception v0

    .line 736
    new-instance v1, Ljavax/crypto/BadPaddingException;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/InvalidCipherTextException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljavax/crypto/BadPaddingException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected engineDoFinal([BII)[B
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 678
    .line 679
    invoke-virtual {p0, p3}, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineGetOutputSize(I)I

    move-result v0

    new-array v4, v0, [B

    .line 681
    if-eqz p3, :cond_1

    .line 683
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->processBytes([BII[BI)I

    move-result v0

    .line 688
    :goto_0
    :try_start_0
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1, v4, v0}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->doFinal([BI)I
    :try_end_0
    .catch Lorg/spongycastle/crypto/DataLengthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/spongycastle/crypto/InvalidCipherTextException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    add-int/2addr v1, v0

    .line 699
    array-length v0, v4

    if-ne v1, v0, :cond_0

    .line 708
    :goto_1
    return-object v4

    .line 690
    :catch_0
    move-exception v0

    .line 692
    new-instance v1, Ljavax/crypto/IllegalBlockSizeException;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/DataLengthException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljavax/crypto/IllegalBlockSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 694
    :catch_1
    move-exception v0

    .line 696
    new-instance v1, Ljavax/crypto/BadPaddingException;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/InvalidCipherTextException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljavax/crypto/BadPaddingException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 704
    :cond_0
    new-array v0, v1, [B

    .line 706
    invoke-static {v4, v5, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v4, v0

    .line 708
    goto :goto_1

    :cond_1
    move v0, v5

    goto :goto_0
.end method

.method protected engineGetBlockSize()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    return v0
.end method

.method protected engineGetIV()[B
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/an;->getIV()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected engineGetKeySize(Ljava/security/Key;)I
    .locals 1

    .prologue
    .line 140
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x8

    return v0
.end method

.method protected engineGetOutputSize(I)I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v0, p1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getOutputSize(I)I

    move-result v0

    return v0
.end method

.method protected engineGetParameters()Ljava/security/AlgorithmParameters;
    .locals 3

    .prologue
    const/16 v2, 0x2f

    .line 151
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    if-eqz v0, :cond_1

    .line 157
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeAlgorithm:Ljava/lang/String;

    sget-object v1, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->PROVIDER_NAME:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/security/AlgorithmParameters;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/AlgorithmParameters;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    .line 158
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    invoke-virtual {v0, v1}, Ljava/security/AlgorithmParameters;->init(Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    :goto_1
    return-object v0

    .line 160
    :catch_0
    move-exception v0

    .line 162
    const/4 v0, 0x0

    goto :goto_1

    .line 165
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v0}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v0

    .line 169
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_2

    .line 171
    const/4 v1, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 176
    :cond_2
    :try_start_1
    sget-object v1, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->PROVIDER_NAME:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/security/AlgorithmParameters;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/AlgorithmParameters;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    .line 177
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/an;->getIV()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/AlgorithmParameters;->init([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 179
    :catch_1
    move-exception v0

    .line 181
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V
    .locals 3

    .prologue
    .line 576
    const/4 v1, 0x0

    .line 578
    if-eqz p3, :cond_0

    .line 580
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->availableSpecs:[Ljava/lang/Class;

    array-length v2, v2

    if-eq v0, v2, :cond_2

    .line 584
    :try_start_0
    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->availableSpecs:[Ljava/lang/Class;

    aget-object v2, v2, v0

    invoke-virtual {p3, v2}, Ljava/security/AlgorithmParameters;->getParameterSpec(Ljava/lang/Class;)Ljava/security/spec/AlgorithmParameterSpec;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 593
    :goto_1
    if-nez v0, :cond_1

    .line 595
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t handle parameter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/security/AlgorithmParameters;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 587
    :catch_0
    move-exception v2

    .line 580
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 599
    :cond_1
    invoke-virtual {p0, p1, p2, v0, p4}, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    .line 601
    iput-object p3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    .line 602
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/SecureRandom;)V
    .locals 2

    .prologue
    .line 612
    const/4 v0, 0x0

    :try_start_0
    check-cast v0, Ljava/security/spec/AlgorithmParameterSpec;

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    :try_end_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    return-void

    .line 614
    :catch_0
    move-exception v0

    .line 616
    new-instance v1, Ljava/security/InvalidKeyException;

    invoke-virtual {v0}, Ljava/security/InvalidAlgorithmParameterException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 365
    iput-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    .line 366
    iput-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeAlgorithm:Ljava/lang/String;

    .line 367
    iput-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->engineParams:Ljava/security/AlgorithmParameters;

    .line 372
    instance-of v0, p2, Ljavax/crypto/SecretKey;

    if-nez v0, :cond_0

    .line 374
    new-instance v0, Ljava/security/InvalidKeyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Key for algorithm "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not suitable for symmetric enryption."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_0
    if-nez p3, :cond_1

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC5-64"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v1, "RC5 requires an RC5ParametersSpec to be passed in."

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388
    :cond_1
    instance-of v0, p2, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;

    if-eqz v0, :cond_7

    .line 390
    check-cast p2, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;

    .line 392
    invoke-virtual {p2}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getOID()Lorg/spongycastle/asn1/bc;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 394
    invoke-virtual {p2}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getOID()Lorg/spongycastle/asn1/bc;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeAlgorithm:Ljava/lang/String;

    .line 401
    :goto_0
    invoke-virtual {p2}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getParam()Lorg/spongycastle/crypto/h;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 403
    invoke-virtual {p2}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getParam()Lorg/spongycastle/crypto/h;

    move-result-object v1

    .line 404
    new-instance v0, Ljavax/crypto/spec/PBEParameterSpec;

    invoke-virtual {p2}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getSalt()[B

    move-result-object v2

    invoke-virtual {p2}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getIterationCount()I

    move-result v3

    invoke-direct {v0, v2, v3}, Ljavax/crypto/spec/PBEParameterSpec;-><init>([BI)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    .line 416
    :goto_1
    instance-of v0, v1, Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 418
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    :cond_2
    move-object v0, v1

    .line 519
    :goto_2
    iget v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    if-eqz v1, :cond_16

    instance-of v1, v0, Lorg/spongycastle/crypto/j/an;

    if-nez v1, :cond_16

    .line 523
    if-nez p4, :cond_17

    .line 525
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 528
    :goto_3
    if-eq p1, v4, :cond_3

    const/4 v2, 0x3

    if-ne p1, v2, :cond_14

    .line 530
    :cond_3
    iget v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    new-array v2, v2, [B

    .line 532
    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 533
    new-instance v1, Lorg/spongycastle/crypto/j/an;

    invoke-direct {v1, v0, v2}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    move-object v0, v1

    .line 534
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    .line 542
    :goto_4
    if-eqz p4, :cond_15

    iget-boolean v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->padded:Z

    if-eqz v0, :cond_15

    .line 544
    new-instance v0, Lorg/spongycastle/crypto/j/ao;

    invoke-direct {v0, v1, p4}, Lorg/spongycastle/crypto/j/ao;-><init>(Lorg/spongycastle/crypto/h;Ljava/security/SecureRandom;)V

    .line 549
    :goto_5
    packed-switch p1, :pswitch_data_0

    .line 560
    :try_start_0
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown opmode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " passed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 563
    :catch_0
    move-exception v0

    .line 565
    new-instance v1, Ljava/security/InvalidKeyException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 398
    :cond_4
    invoke-virtual {p2}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeAlgorithm:Ljava/lang/String;

    goto/16 :goto_0

    .line 406
    :cond_5
    instance-of v0, p3, Ljavax/crypto/spec/PBEParameterSpec;

    if-eqz v0, :cond_6

    move-object v0, p3

    .line 408
    check-cast v0, Ljavax/crypto/spec/PBEParameterSpec;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;

    .line 409
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v0}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lorg/spongycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEParameters(Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;Ljava/lang/String;)Lorg/spongycastle/crypto/h;

    move-result-object v1

    goto/16 :goto_1

    .line 413
    :cond_6
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v1, "PBE requires PBE parameters to be set."

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421
    :cond_7
    if-nez p3, :cond_8

    .line 423
    new-instance v1, Lorg/spongycastle/crypto/j/ak;

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/crypto/j/ak;-><init>([B)V

    move-object v0, v1

    goto/16 :goto_2

    .line 425
    :cond_8
    instance-of v0, p3, Ljavax/crypto/spec/IvParameterSpec;

    if-eqz v0, :cond_d

    .line 427
    iget v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    if-eqz v0, :cond_b

    .line 429
    check-cast p3, Ljavax/crypto/spec/IvParameterSpec;

    .line 431
    invoke-virtual {p3}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v0

    array-length v0, v0

    iget v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/spongycastle/jce/provider/JCEBlockCipher;->isAEADModeName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 433
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IV must be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes long."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :cond_9
    instance-of v0, p2, Lorg/spongycastle/jce/spec/RepeatedSecretKeySpec;

    if-eqz v0, :cond_a

    .line 438
    new-instance v1, Lorg/spongycastle/crypto/j/an;

    invoke-virtual {p3}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    move-object v0, v1

    .line 439
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    :goto_6
    move-object v0, v1

    .line 446
    goto/16 :goto_2

    .line 443
    :cond_a
    new-instance v1, Lorg/spongycastle/crypto/j/an;

    new-instance v0, Lorg/spongycastle/crypto/j/ak;

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/spongycastle/crypto/j/ak;-><init>([B)V

    invoke-virtual {p3}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    move-object v0, v1

    .line 444
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    goto :goto_6

    .line 449
    :cond_b
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "ECB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 451
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v1, "ECB mode does not use an IV"

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_c
    new-instance v1, Lorg/spongycastle/crypto/j/ak;

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/crypto/j/ak;-><init>([B)V

    move-object v0, v1

    goto/16 :goto_2

    .line 457
    :cond_d
    instance-of v0, p3, Lorg/spongycastle/jce/spec/l;

    if-eqz v0, :cond_e

    move-object v0, p3

    .line 459
    check-cast v0, Lorg/spongycastle/jce/spec/l;

    .line 461
    new-instance v2, Lorg/spongycastle/crypto/j/ap;

    new-instance v1, Lorg/spongycastle/crypto/j/ak;

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/spongycastle/crypto/j/ak;-><init>([B)V

    check-cast p3, Lorg/spongycastle/jce/spec/l;

    invoke-virtual {p3}, Lorg/spongycastle/jce/spec/l;->se()[B

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lorg/spongycastle/crypto/j/ap;-><init>(Lorg/spongycastle/crypto/h;[B)V

    .line 464
    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/l;->getIV()[B

    move-result-object v1

    if-eqz v1, :cond_1a

    iget v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    if-eqz v1, :cond_1a

    .line 466
    new-instance v1, Lorg/spongycastle/crypto/j/an;

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/l;->getIV()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    move-object v0, v1

    .line 467
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    :goto_7
    move-object v0, v1

    .line 469
    goto/16 :goto_2

    .line 470
    :cond_e
    instance-of v0, p3, Ljavax/crypto/spec/RC2ParameterSpec;

    if-eqz v0, :cond_f

    move-object v0, p3

    .line 472
    check-cast v0, Ljavax/crypto/spec/RC2ParameterSpec;

    .line 474
    new-instance v2, Lorg/spongycastle/crypto/j/aq;

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v1

    check-cast p3, Ljavax/crypto/spec/RC2ParameterSpec;

    invoke-virtual {p3}, Ljavax/crypto/spec/RC2ParameterSpec;->getEffectiveKeyBits()I

    move-result v3

    invoke-direct {v2, v1, v3}, Lorg/spongycastle/crypto/j/aq;-><init>([BI)V

    .line 476
    invoke-virtual {v0}, Ljavax/crypto/spec/RC2ParameterSpec;->getIV()[B

    move-result-object v1

    if-eqz v1, :cond_19

    iget v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    if-eqz v1, :cond_19

    .line 478
    new-instance v1, Lorg/spongycastle/crypto/j/an;

    invoke-virtual {v0}, Ljavax/crypto/spec/RC2ParameterSpec;->getIV()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    move-object v0, v1

    .line 479
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    :goto_8
    move-object v0, v1

    .line 481
    goto/16 :goto_2

    .line 482
    :cond_f
    instance-of v0, p3, Ljavax/crypto/spec/RC5ParameterSpec;

    if-eqz v0, :cond_13

    move-object v0, p3

    .line 484
    check-cast v0, Ljavax/crypto/spec/RC5ParameterSpec;

    .line 486
    new-instance v2, Lorg/spongycastle/crypto/j/ar;

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v1

    check-cast p3, Ljavax/crypto/spec/RC5ParameterSpec;

    invoke-virtual {p3}, Ljavax/crypto/spec/RC5ParameterSpec;->getRounds()I

    move-result v3

    invoke-direct {v2, v1, v3}, Lorg/spongycastle/crypto/j/ar;-><init>([BI)V

    .line 487
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v1}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "RC5"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 489
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v1}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "RC5-32"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 491
    invoke-virtual {v0}, Ljavax/crypto/spec/RC5ParameterSpec;->getWordSize()I

    move-result v1

    const/16 v3, 0x20

    if-eq v1, v3, :cond_12

    .line 493
    new-instance v1, Ljava/security/InvalidAlgorithmParameterException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RC5 already set up for a word size of 32 not "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljavax/crypto/spec/RC5ParameterSpec;->getWordSize()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 496
    :cond_10
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v1}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "RC5-64"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 498
    invoke-virtual {v0}, Ljavax/crypto/spec/RC5ParameterSpec;->getWordSize()I

    move-result v1

    const/16 v3, 0x40

    if-eq v1, v3, :cond_12

    .line 500
    new-instance v1, Ljava/security/InvalidAlgorithmParameterException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RC5 already set up for a word size of 64 not "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljavax/crypto/spec/RC5ParameterSpec;->getWordSize()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 506
    :cond_11
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v1, "RC5 parameters passed to a cipher that is not RC5."

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 508
    :cond_12
    invoke-virtual {v0}, Ljavax/crypto/spec/RC5ParameterSpec;->getIV()[B

    move-result-object v1

    if-eqz v1, :cond_18

    iget v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    if-eqz v1, :cond_18

    .line 510
    new-instance v1, Lorg/spongycastle/crypto/j/an;

    invoke-virtual {v0}, Ljavax/crypto/spec/RC5ParameterSpec;->getIV()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    move-object v0, v1

    .line 511
    check-cast v0, Lorg/spongycastle/crypto/j/an;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivParam:Lorg/spongycastle/crypto/j/an;

    :goto_9
    move-object v0, v1

    .line 513
    goto/16 :goto_2

    .line 516
    :cond_13
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v1, "unknown parameter type."

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 536
    :cond_14
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v1

    invoke-interface {v1}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PGPCFB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_16

    .line 538
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v1, "no IV set when one expected"

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->init(ZLorg/spongycastle/crypto/h;)V

    .line 567
    :goto_a
    return-void

    .line 557
    :pswitch_1
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->init(ZLorg/spongycastle/crypto/h;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_a

    :cond_15
    move-object v0, v1

    goto/16 :goto_5

    :cond_16
    move-object v1, v0

    goto/16 :goto_4

    :cond_17
    move-object v1, p4

    goto/16 :goto_3

    :cond_18
    move-object v1, v2

    goto :goto_9

    :cond_19
    move-object v1, v2

    goto/16 :goto_8

    :cond_1a
    move-object v1, v2

    goto/16 :goto_7

    .line 549
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected engineSetMode(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 193
    invoke-static {p1}, Lorg/spongycastle/util/g;->cZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    .line 195
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "ECB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iput v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 198
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    .line 298
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "CBC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 203
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/b;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/h/b;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto :goto_0

    .line 206
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "OFB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 208
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 209
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 211
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 213
    new-instance v1, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v2, Lorg/spongycastle/crypto/h/i;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v2, v3, v0}, Lorg/spongycastle/crypto/h/i;-><init>(Lorg/spongycastle/crypto/d;I)V

    invoke-direct {v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto :goto_0

    .line 218
    :cond_2
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/i;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v3}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v3

    mul-int/lit8 v3, v3, 0x8

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/crypto/h/i;-><init>(Lorg/spongycastle/crypto/d;I)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto :goto_0

    .line 222
    :cond_3
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "CFB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 224
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 225
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, v2, :cond_4

    .line 227
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 229
    new-instance v1, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v2, Lorg/spongycastle/crypto/h/d;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v2, v3, v0}, Lorg/spongycastle/crypto/h/d;-><init>(Lorg/spongycastle/crypto/d;I)V

    invoke-direct {v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 234
    :cond_4
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/d;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v3}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v3

    mul-int/lit8 v3, v3, 0x8

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/crypto/h/d;-><init>(Lorg/spongycastle/crypto/d;I)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 238
    :cond_5
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "PGP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 240
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "PGPCFBwithIV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 242
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v1}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v1

    iput v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 243
    new-instance v1, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v2, Lorg/spongycastle/crypto/h/k;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v2, v3, v0}, Lorg/spongycastle/crypto/h/k;-><init>(Lorg/spongycastle/crypto/d;Z)V

    invoke-direct {v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 246
    :cond_6
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "OpenPGPCFB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 248
    iput v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 249
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/j;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/h/j;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 252
    :cond_7
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "SIC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 254
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 255
    iget v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_8

    .line 257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Warning: SIC-Mode can become a twotime-pad if the blocksize of the cipher is too small. Use a cipher with a block size of at least 128 bits (e.g. AES)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_8
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/f;

    new-instance v2, Lorg/spongycastle/crypto/h/l;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v2, v3}, Lorg/spongycastle/crypto/h/l;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/f;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/f;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 262
    :cond_9
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "CTR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 264
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 265
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/f;

    new-instance v2, Lorg/spongycastle/crypto/h/l;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v2, v3}, Lorg/spongycastle/crypto/h/l;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/f;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/f;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 268
    :cond_a
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "GOFB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 270
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 271
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/f;

    new-instance v2, Lorg/spongycastle/crypto/h/h;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v2, v3}, Lorg/spongycastle/crypto/h/h;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/f;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/f;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 274
    :cond_b
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "CTS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 276
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 277
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/e;

    new-instance v2, Lorg/spongycastle/crypto/h/b;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v2, v3}, Lorg/spongycastle/crypto/h/b;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/h/e;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/f;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 279
    :cond_c
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "CCM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 281
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 282
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/c;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/h/c;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/h/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 284
    :cond_d
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "EAX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 286
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 287
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/f;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/h/f;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/h/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 289
    :cond_e
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    const-string v1, "GCM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 291
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->ivLength:I

    .line 292
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/g;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->baseEngine:Lorg/spongycastle/crypto/d;

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/h/g;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/h/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 296
    :cond_f
    new-instance v0, Ljava/security/NoSuchAlgorithmException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t support mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineSetPadding(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 304
    invoke-static {p1}, Lorg/spongycastle/util/g;->cZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 306
    const-string v1, "NOPADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 308
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v0}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->wrapOnNoPadding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/f;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/f;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/f;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    const-string v1, "WITHCTS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 315
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    new-instance v1, Lorg/spongycastle/crypto/h/e;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/crypto/h/e;-><init>(Lorg/spongycastle/crypto/d;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/f;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto :goto_0

    .line 319
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->padded:Z

    .line 321
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->modeName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher;->isAEADModeName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 323
    new-instance v0, Ljavax/crypto/NoSuchPaddingException;

    const-string v1, "Only NoPadding can be used with AEAD modes."

    invoke-direct {v0, v1}, Ljavax/crypto/NoSuchPaddingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :cond_3
    const-string v1, "PKCS5PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "PKCS7PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 327
    :cond_4
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto :goto_0

    .line 329
    :cond_5
    const-string v1, "ZEROBYTEPADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 331
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v1

    new-instance v2, Lorg/spongycastle/crypto/i/h;

    invoke-direct {v2}, Lorg/spongycastle/crypto/i/h;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto :goto_0

    .line 333
    :cond_6
    const-string v1, "ISO10126PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "ISO10126-2PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 335
    :cond_7
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v1

    new-instance v2, Lorg/spongycastle/crypto/i/b;

    invoke-direct {v2}, Lorg/spongycastle/crypto/i/b;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 337
    :cond_8
    const-string v1, "X9.23PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "X923PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 339
    :cond_9
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v1

    new-instance v2, Lorg/spongycastle/crypto/i/g;

    invoke-direct {v2}, Lorg/spongycastle/crypto/i/g;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 341
    :cond_a
    const-string v1, "ISO7816-4PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "ISO9797-1PADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 343
    :cond_b
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v1

    new-instance v2, Lorg/spongycastle/crypto/i/c;

    invoke-direct {v2}, Lorg/spongycastle/crypto/i/c;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 345
    :cond_c
    const-string v1, "TBCPADDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 347
    new-instance v0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v1}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v1

    new-instance v2, Lorg/spongycastle/crypto/i/f;

    invoke-direct {v2}, Lorg/spongycastle/crypto/i/f;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    goto/16 :goto_0

    .line 351
    :cond_d
    new-instance v0, Ljavax/crypto/NoSuchPaddingException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Padding "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unknown."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/crypto/NoSuchPaddingException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineUpdate([BII[BI)I
    .locals 6

    .prologue
    .line 664
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->processBytes([BII[BI)I
    :try_end_0
    .catch Lorg/spongycastle/crypto/DataLengthException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 666
    :catch_0
    move-exception v0

    .line 668
    new-instance v1, Ljavax/crypto/ShortBufferException;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/DataLengthException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljavax/crypto/ShortBufferException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected engineUpdate([BII)[B
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 625
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    invoke-interface {v0, p3}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->getUpdateOutputSize(I)I

    move-result v0

    .line 627
    if-lez v0, :cond_2

    .line 629
    new-array v4, v0, [B

    .line 631
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->processBytes([BII[BI)I

    move-result v1

    .line 633
    if-nez v1, :cond_1

    .line 635
    sget-object v4, Lorg/spongycastle/jce/provider/JCEBlockCipher;->EMPTY_BYTE_ARRAY:[B

    .line 651
    :cond_0
    :goto_0
    return-object v4

    .line 637
    :cond_1
    array-length v0, v4

    if-eq v1, v0, :cond_0

    .line 639
    new-array v0, v1, [B

    .line 641
    invoke-static {v4, v5, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v4, v0

    .line 643
    goto :goto_0

    .line 649
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher;->cipher:Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;

    const/4 v4, 0x0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;->processBytes([BII[BI)I

    .line 651
    sget-object v4, Lorg/spongycastle/jce/provider/JCEBlockCipher;->EMPTY_BYTE_ARRAY:[B

    goto :goto_0
.end method
