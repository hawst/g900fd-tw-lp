.class Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;
.super Ljava/lang/Object;
.source "JCEBlockCipher.java"

# interfaces
.implements Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;


# instance fields
.field private cipher:Lorg/spongycastle/crypto/f;


# direct methods
.method constructor <init>(Lorg/spongycastle/crypto/d;)V
    .locals 1

    .prologue
    .line 953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 954
    new-instance v0, Lorg/spongycastle/crypto/i/e;

    invoke-direct {v0, p1}, Lorg/spongycastle/crypto/i/e;-><init>(Lorg/spongycastle/crypto/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    .line 955
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V
    .locals 1

    .prologue
    .line 958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 959
    new-instance v0, Lorg/spongycastle/crypto/i/e;

    invoke-direct {v0, p1, p2}, Lorg/spongycastle/crypto/i/e;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/i/a;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    .line 960
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/crypto/f;)V
    .locals 0

    .prologue
    .line 948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 949
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    .line 950
    return-void
.end method


# virtual methods
.method public doFinal([BI)I
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0, p1, p2}, Lorg/spongycastle/crypto/f;->doFinal([BI)I

    move-result v0

    return v0
.end method

.method public getAlgorithmName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 975
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/f;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOutputSize(I)I
    .locals 1

    .prologue
    .line 985
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0, p1}, Lorg/spongycastle/crypto/f;->getOutputSize(I)I

    move-result v0

    return v0
.end method

.method public getUnderlyingCipher()Lorg/spongycastle/crypto/d;
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/f;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateOutputSize(I)I
    .locals 1

    .prologue
    .line 990
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0, p1}, Lorg/spongycastle/crypto/f;->getUpdateOutputSize(I)I

    move-result v0

    return v0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0, p1, p2}, Lorg/spongycastle/crypto/f;->init(ZLorg/spongycastle/crypto/h;)V

    .line 966
    return-void
.end method

.method public processByte(B[BI)I
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0, p1, p2, p3}, Lorg/spongycastle/crypto/f;->processByte(B[BI)I

    move-result v0

    return v0
.end method

.method public processBytes([BII[BI)I
    .locals 6

    .prologue
    .line 1000
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/crypto/f;->processBytes([BII[BI)I

    move-result v0

    return v0
.end method

.method public wrapOnNoPadding()Z
    .locals 1

    .prologue
    .line 970
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$BufferedGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/f;

    instance-of v0, v0, Lorg/spongycastle/crypto/h/e;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
