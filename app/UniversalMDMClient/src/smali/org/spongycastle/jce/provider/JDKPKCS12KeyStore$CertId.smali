.class Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;
.super Ljava/lang/Object;
.source "JDKPKCS12KeyStore.java"


# instance fields
.field id:[B

.field final synthetic this$0:Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;


# direct methods
.method constructor <init>(Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;Ljava/security/PublicKey;)V
    .locals 1

    .prologue
    .line 124
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;->this$0:Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    # invokes: Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;->createSubjectKeyId(Ljava/security/PublicKey;)Lorg/spongycastle/asn1/q/ag;
    invoke-static {p1, p2}, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;->access$100(Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;Ljava/security/PublicKey;)Lorg/spongycastle/asn1/q/ag;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ag;->getKeyIdentifier()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;->id:[B

    .line 126
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;[B)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;->this$0:Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p2, p0, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;->id:[B

    .line 132
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 142
    if-ne p1, p0, :cond_0

    .line 144
    const/4 v0, 0x1

    .line 154
    :goto_0
    return v0

    .line 147
    :cond_0
    instance-of v0, p1, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;

    if-nez v0, :cond_1

    .line 149
    const/4 v0, 0x0

    goto :goto_0

    .line 152
    :cond_1
    check-cast p1, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;

    .line 154
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;->id:[B

    iget-object v1, p1, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;->id:[B

    invoke-static {v0, v1}, Lorg/spongycastle/util/a;->h([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKPKCS12KeyStore$CertId;->id:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->hashCode([B)I

    move-result v0

    return v0
.end method
