.class public Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;
.super Lorg/spongycastle/jce/provider/JDKKeyStore;
.source "JDKKeyStore.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 922
    invoke-direct {p0}, Lorg/spongycastle/jce/provider/JDKKeyStore;-><init>()V

    return-void
.end method


# virtual methods
.method public engineLoad(Ljava/io/InputStream;[C)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    .line 930
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->table:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 932
    if-nez p1, :cond_1

    .line 996
    :cond_0
    return-void

    .line 937
    :cond_1
    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 938
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 940
    if-eq v0, v2, :cond_2

    .line 942
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 944
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Wrong version of key store."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 948
    :cond_2
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    new-array v4, v1, [B

    .line 950
    array-length v1, v4

    const/16 v3, 0x14

    if-eq v1, v3, :cond_3

    .line 952
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Key store corrupted."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 955
    :cond_3
    invoke-virtual {v6, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 957
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 959
    if-ltz v5, :cond_4

    const/16 v1, 0x1000

    if-le v5, v1, :cond_5

    .line 961
    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Key store corrupted."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 965
    :cond_5
    if-nez v0, :cond_6

    .line 967
    const-string v1, "OldPBEWithSHAAndTwofish-CBC"

    :goto_0
    move-object v0, p0

    move-object v3, p2

    .line 974
    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->makePBECipher(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 975
    new-instance v1, Ljavax/crypto/CipherInputStream;

    invoke-direct {v1, v6, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    .line 977
    new-instance v0, Lorg/spongycastle/crypto/b/l;

    invoke-direct {v0}, Lorg/spongycastle/crypto/b/l;-><init>()V

    .line 978
    new-instance v2, Lorg/spongycastle/crypto/f/a;

    invoke-direct {v2, v1, v0}, Lorg/spongycastle/crypto/f/a;-><init>(Ljava/io/InputStream;Lorg/spongycastle/crypto/l;)V

    .line 980
    invoke-virtual {p0, v2}, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->loadStore(Ljava/io/InputStream;)V

    .line 983
    invoke-interface {v0}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v2

    new-array v2, v2, [B

    .line 984
    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 988
    invoke-interface {v0}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v0

    new-array v0, v0, [B

    .line 989
    invoke-static {v1, v0}, Lorg/spongycastle/util/io/a;->a(Ljava/io/InputStream;[B)I

    .line 991
    invoke-static {v2, v0}, Lorg/spongycastle/util/a;->i([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 993
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->table:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 994
    new-instance v0, Ljava/io/IOException;

    const-string v1, "KeyStore integrity check failed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 971
    :cond_6
    const-string v1, "PBEWithSHAAndTwofish-CBC"

    goto :goto_0
.end method

.method public engineStore(Ljava/io/OutputStream;[C)V
    .locals 7

    .prologue
    .line 1002
    new-instance v6, Ljava/io/DataOutputStream;

    invoke-direct {v6, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1003
    const/16 v0, 0x14

    new-array v4, v0, [B

    .line 1004
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->random:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v0

    and-int/lit16 v0, v0, 0x3ff

    add-int/lit16 v5, v0, 0x400

    .line 1006
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->random:Ljava/security/SecureRandom;

    invoke-virtual {v0, v4}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 1008
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 1009
    array-length v0, v4

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 1010
    invoke-virtual {v6, v4}, Ljava/io/DataOutputStream;->write([B)V

    .line 1011
    invoke-virtual {v6, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 1013
    const-string v1, "PBEWithSHAAndTwofish-CBC"

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->makePBECipher(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 1015
    new-instance v1, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v1, v6, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    .line 1016
    new-instance v0, Lorg/spongycastle/crypto/f/b;

    new-instance v2, Lorg/spongycastle/crypto/b/l;

    invoke-direct {v2}, Lorg/spongycastle/crypto/b/l;-><init>()V

    invoke-direct {v0, v2}, Lorg/spongycastle/crypto/f/b;-><init>(Lorg/spongycastle/crypto/l;)V

    .line 1018
    new-instance v2, Lorg/spongycastle/util/io/b;

    invoke-direct {v2, v1, v0}, Lorg/spongycastle/util/io/b;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p0, v2}, Lorg/spongycastle/jce/provider/JDKKeyStore$BouncyCastleStore;->saveStore(Ljava/io/OutputStream;)V

    .line 1020
    invoke-virtual {v0}, Lorg/spongycastle/crypto/f/b;->getDigest()[B

    move-result-object v0

    .line 1022
    invoke-virtual {v1, v0}, Ljavax/crypto/CipherOutputStream;->write([B)V

    .line 1024
    invoke-virtual {v1}, Ljavax/crypto/CipherOutputStream;->close()V

    .line 1025
    return-void
.end method
