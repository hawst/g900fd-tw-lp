.class public Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;
.super Ljava/lang/Object;
.source "JCEElGamalPublicKey.java"

# interfaces
.implements Ljavax/crypto/interfaces/DHPublicKey;
.implements Lorg/spongycastle/jce/interfaces/ElGamalPublicKey;


# static fields
.field static final serialVersionUID:J = 0x78e9d455552c6634L


# instance fields
.field private elSpec:Lorg/spongycastle/jce/spec/i;

.field private y:Ljava/math/BigInteger;


# direct methods
.method constructor <init>(Ljava/math/BigInteger;Lorg/spongycastle/jce/spec/i;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 72
    iput-object p2, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 73
    return-void
.end method

.method constructor <init>(Ljavax/crypto/interfaces/DHPublicKey;)V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-interface {p1}, Ljavax/crypto/interfaces/DHPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 57
    new-instance v0, Lorg/spongycastle/jce/spec/i;

    invoke-interface {p1}, Ljavax/crypto/interfaces/DHPublicKey;->getParams()Ljavax/crypto/spec/DHParameterSpec;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/crypto/spec/DHParameterSpec;->getP()Ljava/math/BigInteger;

    move-result-object v1

    invoke-interface {p1}, Ljavax/crypto/interfaces/DHPublicKey;->getParams()Ljavax/crypto/spec/DHParameterSpec;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/crypto/spec/DHParameterSpec;->getG()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/spec/i;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 58
    return-void
.end method

.method constructor <init>(Ljavax/crypto/spec/DHPublicKeySpec;)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p1}, Ljavax/crypto/spec/DHPublicKeySpec;->getY()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 43
    new-instance v0, Lorg/spongycastle/jce/spec/i;

    invoke-virtual {p1}, Ljavax/crypto/spec/DHPublicKeySpec;->getP()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p1}, Ljavax/crypto/spec/DHPublicKeySpec;->getG()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/spec/i;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 44
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/asn1/q/ah;)V
    .locals 3

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v1, Lorg/spongycastle/asn1/k/a;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/ah;->nJ()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/k/a;-><init>(Lorg/spongycastle/asn1/r;)V

    .line 83
    :try_start_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/ah;->pq()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/az;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    invoke-virtual {v0}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 91
    new-instance v0, Lorg/spongycastle/jce/spec/i;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/k/a;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1}, Lorg/spongycastle/asn1/k/a;->getG()Ljava/math/BigInteger;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/spongycastle/jce/spec/i;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 92
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid info structure in DSA public key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method constructor <init>(Lorg/spongycastle/crypto/j/z;)V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/z;->getY()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 64
    new-instance v0, Lorg/spongycastle/jce/spec/i;

    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/z;->qN()Lorg/spongycastle/crypto/j/x;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/x;->getP()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/crypto/j/z;->qN()Lorg/spongycastle/crypto/j/x;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/crypto/j/x;->getG()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/spec/i;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 65
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/jce/interfaces/ElGamalPublicKey;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-interface {p1}, Lorg/spongycastle/jce/interfaces/ElGamalPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 50
    invoke-interface {p1}, Lorg/spongycastle/jce/interfaces/ElGamalPublicKey;->getParameters()Lorg/spongycastle/jce/spec/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 51
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/jce/spec/k;)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Lorg/spongycastle/jce/spec/k;->getY()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 36
    new-instance v0, Lorg/spongycastle/jce/spec/i;

    invoke-virtual {p1}, Lorg/spongycastle/jce/spec/k;->sd()Lorg/spongycastle/jce/spec/i;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/jce/spec/i;->getP()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/jce/spec/k;->sd()Lorg/spongycastle/jce/spec/i;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/jce/spec/i;->getG()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/jce/spec/i;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 37
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3

    .prologue
    .line 128
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigInteger;

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    .line 129
    new-instance v2, Lorg/spongycastle/jce/spec/i;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigInteger;

    invoke-direct {v2, v0, v1}, Lorg/spongycastle/jce/spec/i;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v2, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    .line 130
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 137
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/i;->getP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    invoke-virtual {v0}, Lorg/spongycastle/jce/spec/i;->getG()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 139
    return-void
.end method


# virtual methods
.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    const-string v0, "ElGamal"

    return-object v0
.end method

.method public getEncoded()[B
    .locals 5

    .prologue
    .line 106
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/k/b;->Kk:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/k/a;

    iget-object v3, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    invoke-virtual {v3}, Lorg/spongycastle/jce/spec/i;->getP()Ljava/math/BigInteger;

    move-result-object v3

    iget-object v4, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    invoke-virtual {v4}, Lorg/spongycastle/jce/spec/i;->getG()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/spongycastle/asn1/k/a;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    new-instance v1, Lorg/spongycastle/asn1/az;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-static {v0, v1}, Lorg/spongycastle/jcajce/provider/asymmetric/util/KeyUtil;->getEncodedSubjectPublicKeyInfo(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)[B

    move-result-object v0

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const-string v0, "X.509"

    return-object v0
.end method

.method public getParameters()Lorg/spongycastle/jce/spec/i;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    return-object v0
.end method

.method public getParams()Ljavax/crypto/spec/DHParameterSpec;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Ljavax/crypto/spec/DHParameterSpec;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    invoke-virtual {v1}, Lorg/spongycastle/jce/spec/i;->getP()Ljava/math/BigInteger;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->elSpec:Lorg/spongycastle/jce/spec/i;

    invoke-virtual {v2}, Lorg/spongycastle/jce/spec/i;->getG()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/DHParameterSpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v0
.end method

.method public getY()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEElGamalPublicKey;->y:Ljava/math/BigInteger;

    return-object v0
.end method
