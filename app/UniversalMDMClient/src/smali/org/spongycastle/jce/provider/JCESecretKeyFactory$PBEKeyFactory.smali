.class public Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;
.super Lorg/spongycastle/jce/provider/JCESecretKeyFactory;
.source "JCESecretKeyFactory.java"


# instance fields
.field private digest:I

.field private forCipher:Z

.field private ivSize:I

.field private keySize:I

.field private scheme:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/spongycastle/asn1/bc;ZIIII)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Lorg/spongycastle/jce/provider/JCESecretKeyFactory;-><init>(Ljava/lang/String;Lorg/spongycastle/asn1/bc;)V

    .line 127
    iput-boolean p3, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->forCipher:Z

    .line 128
    iput p4, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->scheme:I

    .line 129
    iput p5, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->digest:I

    .line 130
    iput p6, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->keySize:I

    .line 131
    iput p7, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->ivSize:I

    .line 132
    return-void
.end method


# virtual methods
.method protected engineGenerateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;
    .locals 9

    .prologue
    .line 138
    instance-of v0, p1, Ljavax/crypto/spec/PBEKeySpec;

    if-eqz v0, :cond_2

    move-object v7, p1

    .line 140
    check-cast v7, Ljavax/crypto/spec/PBEKeySpec;

    .line 143
    invoke-virtual {v7}, Ljavax/crypto/spec/PBEKeySpec;->getSalt()[B

    move-result-object v0

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->algName:Ljava/lang/String;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->algOid:Lorg/spongycastle/asn1/bc;

    iget v3, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->scheme:I

    iget v4, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->digest:I

    iget v5, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->keySize:I

    iget v6, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->ivSize:I

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;-><init>(Ljava/lang/String;Lorg/spongycastle/asn1/bc;IIIILjavax/crypto/spec/PBEKeySpec;Lorg/spongycastle/crypto/h;)V

    .line 157
    :goto_0
    return-object v0

    .line 148
    :cond_0
    iget-boolean v0, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->forCipher:Z

    if-eqz v0, :cond_1

    .line 150
    iget v0, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->scheme:I

    iget v1, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->digest:I

    iget v2, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->keySize:I

    iget v3, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->ivSize:I

    invoke-static {v7, v0, v1, v2, v3}, Lorg/spongycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEParameters(Ljavax/crypto/spec/PBEKeySpec;IIII)Lorg/spongycastle/crypto/h;

    move-result-object v8

    .line 157
    :goto_1
    new-instance v0, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->algName:Ljava/lang/String;

    iget-object v2, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->algOid:Lorg/spongycastle/asn1/bc;

    iget v3, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->scheme:I

    iget v4, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->digest:I

    iget v5, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->keySize:I

    iget v6, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->ivSize:I

    invoke-direct/range {v0 .. v8}, Lorg/spongycastle/jcajce/provider/symmetric/util/BCPBEKey;-><init>(Ljava/lang/String;Lorg/spongycastle/asn1/bc;IIIILjavax/crypto/spec/PBEKeySpec;Lorg/spongycastle/crypto/h;)V

    goto :goto_0

    .line 154
    :cond_1
    iget v0, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->scheme:I

    iget v1, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->digest:I

    iget v2, p0, Lorg/spongycastle/jce/provider/JCESecretKeyFactory$PBEKeyFactory;->keySize:I

    invoke-static {v7, v0, v1, v2}, Lorg/spongycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEMacParameters(Ljavax/crypto/spec/PBEKeySpec;III)Lorg/spongycastle/crypto/h;

    move-result-object v8

    goto :goto_1

    .line 160
    :cond_2
    new-instance v0, Ljava/security/spec/InvalidKeySpecException;

    const-string v1, "Invalid KeySpec"

    invoke-direct {v0, v1}, Ljava/security/spec/InvalidKeySpecException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
