.class public Lorg/spongycastle/jce/provider/X509AttrCertParser;
.super Lorg/spongycastle/x509/q;
.source "X509AttrCertParser.java"


# static fields
.field private static final PEM_PARSER:Lorg/spongycastle/jce/provider/PEMUtil;


# instance fields
.field private currentStream:Ljava/io/InputStream;

.field private sData:Lorg/spongycastle/asn1/t;

.field private sDataObjectCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lorg/spongycastle/jce/provider/PEMUtil;

    const-string v1, "ATTRIBUTE CERTIFICATE"

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/provider/PEMUtil;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->PEM_PARSER:Lorg/spongycastle/jce/provider/PEMUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Lorg/spongycastle/x509/q;-><init>()V

    .line 27
    iput-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sDataObjectCount:I

    .line 29
    iput-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    return-void
.end method

.method private getCertificate()Lorg/spongycastle/x509/h;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    if-eqz v0, :cond_1

    .line 58
    :cond_0
    iget v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sDataObjectCount:I

    iget-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/t;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 60
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    iget v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sDataObjectCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sDataObjectCount:I

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/t;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    .line 62
    instance-of v0, v1, Lorg/spongycastle/asn1/x;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lorg/spongycastle/asn1/x;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 64
    new-instance v0, Lorg/spongycastle/x509/t;

    check-cast v1, Lorg/spongycastle/asn1/x;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->getEncoded()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/x509/t;-><init>([B)V

    .line 70
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readDERCertificate(Ljava/io/InputStream;)Lorg/spongycastle/x509/h;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 35
    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/h;-><init>(Ljava/io/InputStream;)V

    .line 36
    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    .line 38
    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    if-le v1, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    instance-of v1, v1, Lorg/spongycastle/asn1/bc;

    if-eqz v1, :cond_0

    .line 41
    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    sget-object v2, Lorg/spongycastle/asn1/l/q;->Lw:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    new-instance v1, Lorg/spongycastle/asn1/l/z;

    invoke-virtual {v0, v3}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    invoke-static {v0, v3}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/l/z;-><init>(Lorg/spongycastle/asn1/r;)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/l/z;->nZ()Lorg/spongycastle/asn1/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    .line 46
    invoke-direct {p0}, Lorg/spongycastle/jce/provider/X509AttrCertParser;->getCertificate()Lorg/spongycastle/x509/h;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lorg/spongycastle/x509/t;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->getEncoded()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/x509/t;-><init>([B)V

    move-object v0, v1

    goto :goto_0
.end method

.method private readPEMCertificate(Ljava/io/InputStream;)Lorg/spongycastle/x509/h;
    .locals 2

    .prologue
    .line 77
    sget-object v0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->PEM_PARSER:Lorg/spongycastle/jce/provider/PEMUtil;

    invoke-virtual {v0, p1}, Lorg/spongycastle/jce/provider/PEMUtil;->readPEMObject(Ljava/io/InputStream;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_0

    .line 81
    new-instance v0, Lorg/spongycastle/x509/t;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->getEncoded()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/x509/t;-><init>([B)V

    .line 84
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public engineInit(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 89
    iput-object p1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sDataObjectCount:I

    .line 93
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    .line 97
    :cond_0
    return-void
.end method

.method public engineRead()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 104
    :try_start_0
    iget-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_2

    .line 106
    iget v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sDataObjectCount:I

    iget-object v2, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/t;->size()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 108
    invoke-direct {p0}, Lorg/spongycastle/jce/provider/X509AttrCertParser;->getCertificate()Lorg/spongycastle/x509/h;

    move-result-object v0

    .line 134
    :cond_0
    :goto_0
    return-object v0

    .line 112
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sData:Lorg/spongycastle/asn1/t;

    .line 113
    const/4 v1, 0x0

    iput v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->sDataObjectCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 139
    new-instance v1, Lorg/spongycastle/x509/util/StreamParsingException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/x509/util/StreamParsingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 118
    :cond_2
    :try_start_1
    iget-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/InputStream;->mark(I)V

    .line 119
    iget-object v1, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 121
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 126
    const/16 v0, 0x30

    if-eq v1, v0, :cond_3

    .line 128
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 129
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/spongycastle/jce/provider/X509AttrCertParser;->readPEMCertificate(Ljava/io/InputStream;)Lorg/spongycastle/x509/h;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_3
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 134
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509AttrCertParser;->currentStream:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/spongycastle/jce/provider/X509AttrCertParser;->readDERCertificate(Ljava/io/InputStream;)Lorg/spongycastle/x509/h;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method public engineReadAll()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 147
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 149
    :goto_0
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/X509AttrCertParser;->engineRead()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/x509/h;

    if-eqz v0, :cond_0

    .line 151
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :cond_0
    return-object v1
.end method
