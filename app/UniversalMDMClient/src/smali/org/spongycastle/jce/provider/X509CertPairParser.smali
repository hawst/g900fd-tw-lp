.class public Lorg/spongycastle/jce/provider/X509CertPairParser;
.super Lorg/spongycastle/x509/q;
.source "X509CertPairParser.java"


# instance fields
.field private currentStream:Ljava/io/InputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/spongycastle/x509/q;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    return-void
.end method

.method private readDERCrossCertificatePair(Ljava/io/InputStream;)Lorg/spongycastle/x509/l;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/h;-><init>(Ljava/io/InputStream;)V

    .line 28
    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    .line 29
    invoke-static {v0}, Lorg/spongycastle/asn1/q/n;->aI(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/n;

    move-result-object v0

    .line 30
    new-instance v1, Lorg/spongycastle/x509/l;

    invoke-direct {v1, v0}, Lorg/spongycastle/x509/l;-><init>(Lorg/spongycastle/asn1/q/n;)V

    return-object v1
.end method


# virtual methods
.method public engineInit(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 35
    iput-object p1, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    .line 37
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    .line 41
    :cond_0
    return-void
.end method

.method public engineRead()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 48
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->mark(I)V

    .line 49
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 51
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 57
    :goto_0
    return-object v0

    .line 56
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 57
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509CertPairParser;->currentStream:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/spongycastle/jce/provider/X509CertPairParser;->readDERCrossCertificatePair(Ljava/io/InputStream;)Lorg/spongycastle/x509/l;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 61
    new-instance v1, Lorg/spongycastle/x509/util/StreamParsingException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/x509/util/StreamParsingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public engineReadAll()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 70
    :goto_0
    invoke-virtual {p0}, Lorg/spongycastle/jce/provider/X509CertPairParser;->engineRead()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/x509/l;

    if-eqz v0, :cond_0

    .line 72
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :cond_0
    return-object v1
.end method
