.class public Lorg/spongycastle/jce/provider/X509StoreCRLCollection;
.super Lorg/spongycastle/x509/p;
.source "X509StoreCRLCollection.java"


# instance fields
.field private _store:Lorg/spongycastle/util/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lorg/spongycastle/x509/p;-><init>()V

    .line 18
    return-void
.end method


# virtual methods
.method public engineGetMatches(Lorg/spongycastle/util/e;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/spongycastle/jce/provider/X509StoreCRLCollection;->_store:Lorg/spongycastle/util/c;

    invoke-virtual {v0, p1}, Lorg/spongycastle/util/c;->a(Lorg/spongycastle/util/e;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public engineInit(Lorg/spongycastle/x509/o;)V
    .locals 2

    .prologue
    .line 22
    instance-of v0, p1, Lorg/spongycastle/x509/m;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    new-instance v0, Lorg/spongycastle/util/c;

    check-cast p1, Lorg/spongycastle/x509/m;

    invoke-virtual {p1}, Lorg/spongycastle/x509/m;->getCollection()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/util/c;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/X509StoreCRLCollection;->_store:Lorg/spongycastle/util/c;

    .line 28
    return-void
.end method
