.class Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;
.super Ljava/lang/Object;
.source "JCEBlockCipher.java"

# interfaces
.implements Lorg/spongycastle/jce/provider/JCEBlockCipher$GenericBlockCipher;


# instance fields
.field private cipher:Lorg/spongycastle/crypto/h/a;


# direct methods
.method constructor <init>(Lorg/spongycastle/crypto/h/a;)V
    .locals 0

    .prologue
    .line 1015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1016
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    .line 1017
    return-void
.end method


# virtual methods
.method public doFinal([BI)I
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    invoke-interface {v0, p1, p2}, Lorg/spongycastle/crypto/h/a;->doFinal([BI)I

    move-result v0

    return v0
.end method

.method public getAlgorithmName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1027
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    invoke-interface {v0}, Lorg/spongycastle/crypto/h/a;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOutputSize(I)I
    .locals 1

    .prologue
    .line 1042
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    invoke-interface {v0, p1}, Lorg/spongycastle/crypto/h/a;->getOutputSize(I)I

    move-result v0

    return v0
.end method

.method public getUnderlyingCipher()Lorg/spongycastle/crypto/d;
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    invoke-interface {v0}, Lorg/spongycastle/crypto/h/a;->getUnderlyingCipher()Lorg/spongycastle/crypto/d;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateOutputSize(I)I
    .locals 1

    .prologue
    .line 1047
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    invoke-interface {v0, p1}, Lorg/spongycastle/crypto/h/a;->getUpdateOutputSize(I)I

    move-result v0

    return v0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 1

    .prologue
    .line 1022
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    invoke-interface {v0, p1, p2}, Lorg/spongycastle/crypto/h/a;->init(ZLorg/spongycastle/crypto/h;)V

    .line 1023
    return-void
.end method

.method public processByte(B[BI)I
    .locals 1

    .prologue
    .line 1052
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    invoke-interface {v0, p1, p2, p3}, Lorg/spongycastle/crypto/h/a;->processByte(B[BI)I

    move-result v0

    return v0
.end method

.method public processBytes([BII[BI)I
    .locals 6

    .prologue
    .line 1057
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JCEBlockCipher$AEADGenericBlockCipher;->cipher:Lorg/spongycastle/crypto/h/a;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/spongycastle/crypto/h/a;->processBytes([BII[BI)I

    move-result v0

    return v0
.end method

.method public wrapOnNoPadding()Z
    .locals 1

    .prologue
    .line 1032
    const/4 v0, 0x0

    return v0
.end method
