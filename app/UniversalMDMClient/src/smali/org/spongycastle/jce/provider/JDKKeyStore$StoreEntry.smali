.class Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;
.super Ljava/lang/Object;
.source "JDKKeyStore.java"


# instance fields
.field alias:Ljava/lang/String;

.field certChain:[Ljava/security/cert/Certificate;

.field date:Ljava/util/Date;

.field obj:Ljava/lang/Object;

.field final synthetic this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

.field type:I


# direct methods
.method constructor <init>(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
    .locals 8

    .prologue
    .line 127
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    .line 128
    const/4 v0, 0x4

    iput v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->type:I

    .line 129
    iput-object p2, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->alias:Ljava/lang/String;

    .line 130
    iput-object p5, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->certChain:[Ljava/security/cert/Certificate;

    .line 132
    const/16 v0, 0x14

    new-array v4, v0, [B

    .line 134
    iget-object v0, p1, Lorg/spongycastle/jce/provider/JDKKeyStore;->random:Ljava/security/SecureRandom;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/security/SecureRandom;->setSeed(J)V

    .line 135
    iget-object v0, p1, Lorg/spongycastle/jce/provider/JDKKeyStore;->random:Ljava/security/SecureRandom;

    invoke-virtual {v0, v4}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 137
    iget-object v0, p1, Lorg/spongycastle/jce/provider/JDKKeyStore;->random:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v0

    and-int/lit16 v0, v0, 0x3ff

    add-int/lit16 v5, v0, 0x400

    .line 140
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 141
    new-instance v7, Ljava/io/DataOutputStream;

    invoke-direct {v7, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 143
    array-length v0, v4

    invoke-virtual {v7, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 144
    invoke-virtual {v7, v4}, Ljava/io/DataOutputStream;->write([B)V

    .line 145
    invoke-virtual {v7, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 147
    const-string v1, "PBEWithSHAAnd3-KeyTripleDES-CBC"

    const/4 v2, 0x1

    move-object v0, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JDKKeyStore;->makePBECipher(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 148
    new-instance v1, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v1, v7, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    .line 150
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 152
    # invokes: Lorg/spongycastle/jce/provider/JDKKeyStore;->encodeKey(Ljava/security/Key;Ljava/io/DataOutputStream;)V
    invoke-static {p1, p3, v0}, Lorg/spongycastle/jce/provider/JDKKeyStore;->access$000(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/security/Key;Ljava/io/DataOutputStream;)V

    .line 154
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 156
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    .line 157
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/lang/String;Ljava/security/cert/Certificate;)V
    .locals 1

    .prologue
    .line 103
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    .line 104
    const/4 v0, 0x1

    iput v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->type:I

    .line 105
    iput-object p2, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->alias:Ljava/lang/String;

    .line 106
    iput-object p3, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->certChain:[Ljava/security/cert/Certificate;

    .line 108
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/lang/String;Ljava/util/Date;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 164
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    .line 165
    iput-object p2, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->alias:Ljava/lang/String;

    .line 166
    iput-object p3, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    .line 167
    iput p4, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->type:I

    .line 168
    iput-object p5, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    .line 169
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/lang/String;Ljava/util/Date;ILjava/lang/Object;[Ljava/security/cert/Certificate;)V
    .locals 1

    .prologue
    .line 177
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    .line 178
    iput-object p2, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->alias:Ljava/lang/String;

    .line 179
    iput-object p3, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    .line 180
    iput p4, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->type:I

    .line 181
    iput-object p5, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    .line 182
    iput-object p6, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->certChain:[Ljava/security/cert/Certificate;

    .line 183
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/lang/String;[B[Ljava/security/cert/Certificate;)V
    .locals 1

    .prologue
    .line 114
    iput-object p1, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    .line 115
    const/4 v0, 0x3

    iput v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->type:I

    .line 116
    iput-object p2, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->alias:Ljava/lang/String;

    .line 117
    iput-object p3, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    .line 118
    iput-object p4, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->certChain:[Ljava/security/cert/Certificate;

    .line 119
    return-void
.end method


# virtual methods
.method getAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->alias:Ljava/lang/String;

    return-object v0
.end method

.method getCertificateChain()[Ljava/security/cert/Certificate;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->certChain:[Ljava/security/cert/Certificate;

    return-object v0
.end method

.method getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->date:Ljava/util/Date;

    return-object v0
.end method

.method getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method getObject([C)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 204
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 206
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/security/Key;

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    .line 295
    :goto_0
    return-object v0

    .line 212
    :cond_1
    iget v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 214
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 215
    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 219
    :try_start_0
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    new-array v4, v0, [B

    .line 221
    invoke-virtual {v6, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 223
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 225
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    const-string v1, "PBEWithSHAAnd3-KeyTripleDES-CBC"

    const/4 v2, 0x2

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JDKKeyStore;->makePBECipher(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 227
    new-instance v1, Ljavax/crypto/CipherInputStream;

    invoke-direct {v1, v6, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 231
    :try_start_1
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    # invokes: Lorg/spongycastle/jce/provider/JDKKeyStore;->decodeKey(Ljava/io/DataInputStream;)Ljava/security/Key;
    invoke-static {v0, v2}, Lorg/spongycastle/jce/provider/JDKKeyStore;->access$100(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/io/DataInputStream;)Ljava/security/Key;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 235
    :try_start_2
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 236
    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 238
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    new-array v4, v0, [B

    .line 240
    invoke-virtual {v6, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 242
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 244
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    const-string v1, "BrokenPBEWithSHAAnd3-KeyTripleDES-CBC"

    const/4 v2, 0x2

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JDKKeyStore;->makePBECipher(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 246
    new-instance v1, Ljavax/crypto/CipherInputStream;

    invoke-direct {v1, v6, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 252
    :try_start_3
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    # invokes: Lorg/spongycastle/jce/provider/JDKKeyStore;->decodeKey(Ljava/io/DataInputStream;)Ljava/security/Key;
    invoke-static {v0, v2}, Lorg/spongycastle/jce/provider/JDKKeyStore;->access$100(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/io/DataInputStream;)Ljava/security/Key;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v6

    .line 275
    :goto_1
    if-eqz v6, :cond_2

    .line 277
    :try_start_4
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 278
    new-instance v8, Ljava/io/DataOutputStream;

    invoke-direct {v8, v7}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 280
    array-length v0, v4

    invoke-virtual {v8, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 281
    invoke-virtual {v8, v4}, Ljava/io/DataOutputStream;->write([B)V

    .line 282
    invoke-virtual {v8, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 284
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    const-string v1, "PBEWithSHAAnd3-KeyTripleDES-CBC"

    const/4 v2, 0x1

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JDKKeyStore;->makePBECipher(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 285
    new-instance v1, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v1, v8, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    .line 287
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 289
    iget-object v1, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    # invokes: Lorg/spongycastle/jce/provider/JDKKeyStore;->encodeKey(Ljava/security/Key;Ljava/io/DataOutputStream;)V
    invoke-static {v1, v6, v0}, Lorg/spongycastle/jce/provider/JDKKeyStore;->access$000(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/security/Key;Ljava/io/DataOutputStream;)V

    .line 291
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 293
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    move-object v0, v6

    .line 295
    goto/16 :goto_0

    .line 254
    :catch_1
    move-exception v0

    .line 256
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 257
    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 259
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    new-array v4, v0, [B

    .line 261
    invoke-virtual {v6, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 263
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 265
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    const-string v1, "OldPBEWithSHAAnd3-KeyTripleDES-CBC"

    const/4 v2, 0x2

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/jce/provider/JDKKeyStore;->makePBECipher(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 267
    new-instance v1, Ljavax/crypto/CipherInputStream;

    invoke-direct {v1, v6, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    .line 269
    iget-object v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->this$0:Lorg/spongycastle/jce/provider/JDKKeyStore;

    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    # invokes: Lorg/spongycastle/jce/provider/JDKKeyStore;->decodeKey(Ljava/io/DataInputStream;)Ljava/security/Key;
    invoke-static {v0, v2}, Lorg/spongycastle/jce/provider/JDKKeyStore;->access$100(Lorg/spongycastle/jce/provider/JDKKeyStore;Ljava/io/DataInputStream;)Ljava/security/Key;

    move-result-object v6

    goto :goto_1

    .line 299
    :cond_2
    new-instance v0, Ljava/security/UnrecoverableKeyException;

    const-string v1, "no match"

    invoke-direct {v0, v1}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 303
    :catch_2
    move-exception v0

    .line 305
    new-instance v0, Ljava/security/UnrecoverableKeyException;

    const-string v1, "no match"

    invoke-direct {v0, v1}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "forget something!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getType()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lorg/spongycastle/jce/provider/JDKKeyStore$StoreEntry;->type:I

    return v0
.end method
