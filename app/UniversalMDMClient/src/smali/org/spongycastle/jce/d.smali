.class public Lorg/spongycastle/jce/d;
.super Lorg/spongycastle/asn1/l/d;
.source "PKCS10CertificationRequest.java"


# static fields
.field private static adI:Ljava/util/Hashtable;

.field private static adJ:Ljava/util/Hashtable;

.field private static adK:Ljava/util/Set;

.field private static algorithms:Ljava/util/Hashtable;

.field private static params:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 76
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    .line 77
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    .line 78
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/jce/d;->adI:Ljava/util/Hashtable;

    .line 79
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    .line 80
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    .line 84
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "MD2WITHRSAENCRYPTION"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.2"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "MD2WITHRSA"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.2"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "MD5WITHRSAENCRYPTION"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.4"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "MD5WITHRSA"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.4"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RSAWITHMD5"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.4"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA1WITHRSAENCRYPTION"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.5"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA1WITHRSA"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.5"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA224WITHRSAENCRYPTION"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KW:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA224WITHRSA"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KW:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA256WITHRSAENCRYPTION"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KT:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA256WITHRSA"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KT:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA384WITHRSAENCRYPTION"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KU:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA384WITHRSA"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KU:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA512WITHRSAENCRYPTION"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KV:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA512WITHRSA"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KV:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA1WITHRSAANDMGF1"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KS:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA224WITHRSAANDMGF1"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KS:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA256WITHRSAANDMGF1"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KS:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA384WITHRSAANDMGF1"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KS:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA512WITHRSAANDMGF1"

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KS:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RSAWITHSHA1"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.113549.1.1.5"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RIPEMD128WITHRSAENCRYPTION"

    sget-object v2, Lorg/spongycastle/asn1/n/b;->PC:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RIPEMD128WITHRSA"

    sget-object v2, Lorg/spongycastle/asn1/n/b;->PC:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RIPEMD160WITHRSAENCRYPTION"

    sget-object v2, Lorg/spongycastle/asn1/n/b;->PB:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RIPEMD160WITHRSA"

    sget-object v2, Lorg/spongycastle/asn1/n/b;->PB:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RIPEMD256WITHRSAENCRYPTION"

    sget-object v2, Lorg/spongycastle/asn1/n/b;->PD:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "RIPEMD256WITHRSA"

    sget-object v2, Lorg/spongycastle/asn1/n/b;->PD:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA1WITHDSA"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.10040.4.3"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "DSAWITHSHA1"

    new-instance v2, Lorg/spongycastle/asn1/bc;

    const-string v3, "1.2.840.10040.4.3"

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA224WITHDSA"

    sget-object v2, Lorg/spongycastle/asn1/i/b;->JO:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA256WITHDSA"

    sget-object v2, Lorg/spongycastle/asn1/i/b;->JP:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA384WITHDSA"

    sget-object v2, Lorg/spongycastle/asn1/i/b;->JQ:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA512WITHDSA"

    sget-object v2, Lorg/spongycastle/asn1/i/b;->JR:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA1WITHECDSA"

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vo:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA224WITHECDSA"

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vs:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA256WITHECDSA"

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vt:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA384WITHECDSA"

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vu:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "SHA512WITHECDSA"

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vv:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "ECDSAWITHSHA1"

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vo:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "GOST3411WITHGOST3410"

    sget-object v2, Lorg/spongycastle/asn1/c/a;->Hr:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "GOST3410WITHGOST3411"

    sget-object v2, Lorg/spongycastle/asn1/c/a;->Hr:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "GOST3411WITHECGOST3410"

    sget-object v2, Lorg/spongycastle/asn1/c/a;->Hs:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "GOST3411WITHECGOST3410-2001"

    sget-object v2, Lorg/spongycastle/asn1/c/a;->Hs:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    const-string v1, "GOST3411WITHGOST3410-2001"

    sget-object v2, Lorg/spongycastle/asn1/c/a;->Hs:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    new-instance v1, Lorg/spongycastle/asn1/bc;

    const-string v2, "1.2.840.113549.1.1.5"

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    const-string v2, "SHA1WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KW:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA224WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KT:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA256WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KU:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA384WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KV:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA512WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/c/a;->Hr:Lorg/spongycastle/asn1/l;

    const-string v2, "GOST3411WITHGOST3410"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/c/a;->Hs:Lorg/spongycastle/asn1/l;

    const-string v2, "GOST3411WITHECGOST3410"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    new-instance v1, Lorg/spongycastle/asn1/bc;

    const-string v2, "1.2.840.113549.1.1.4"

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    const-string v2, "MD5WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    new-instance v1, Lorg/spongycastle/asn1/bc;

    const-string v2, "1.2.840.113549.1.1.2"

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    const-string v2, "MD2WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    new-instance v1, Lorg/spongycastle/asn1/bc;

    const-string v2, "1.2.840.10040.4.3"

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V

    const-string v2, "SHA1WITHDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vo:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA1WITHECDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vs:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA224WITHECDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vt:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA256WITHECDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vu:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA384WITHECDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vv:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA512WITHECDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/k/b;->Kj:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA1WITHRSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/k/b;->Ki:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA1WITHDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JO:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA224WITHDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lorg/spongycastle/jce/d;->adJ:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JP:Lorg/spongycastle/asn1/l;

    const-string v2, "SHA256WITHDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lorg/spongycastle/jce/d;->adI:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KK:Lorg/spongycastle/asn1/l;

    const-string v2, "RSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lorg/spongycastle/jce/d;->adI:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->VZ:Lorg/spongycastle/asn1/l;

    const-string v2, "DSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vo:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vs:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vt:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vu:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Vv:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/r/l;->Wa:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 169
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JO:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 170
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JP:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 175
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/c/a;->Hr:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 176
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/c/a;->Hs:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 180
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/k/b;->Kh:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/ba;

    invoke-direct {v2}, Lorg/spongycastle/asn1/ba;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    .line 181
    sget-object v1, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    const-string v2, "SHA1WITHRSAANDMGF1"

    const/16 v3, 0x14

    invoke-static {v0, v3}, Lorg/spongycastle/jce/d;->a(Lorg/spongycastle/asn1/q/a;I)Lorg/spongycastle/asn1/l/x;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->Jq:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/ba;

    invoke-direct {v2}, Lorg/spongycastle/asn1/ba;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    .line 184
    sget-object v1, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    const-string v2, "SHA224WITHRSAANDMGF1"

    const/16 v3, 0x1c

    invoke-static {v0, v3}, Lorg/spongycastle/jce/d;->a(Lorg/spongycastle/asn1/q/a;I)Lorg/spongycastle/asn1/l/x;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->Jn:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/ba;

    invoke-direct {v2}, Lorg/spongycastle/asn1/ba;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    .line 187
    sget-object v1, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    const-string v2, "SHA256WITHRSAANDMGF1"

    const/16 v3, 0x20

    invoke-static {v0, v3}, Lorg/spongycastle/jce/d;->a(Lorg/spongycastle/asn1/q/a;I)Lorg/spongycastle/asn1/l/x;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->Jo:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/ba;

    invoke-direct {v2}, Lorg/spongycastle/asn1/ba;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    .line 190
    sget-object v1, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    const-string v2, "SHA384WITHRSAANDMGF1"

    const/16 v3, 0x30

    invoke-static {v0, v3}, Lorg/spongycastle/jce/d;->a(Lorg/spongycastle/asn1/q/a;I)Lorg/spongycastle/asn1/l/x;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->Jp:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/ba;

    invoke-direct {v2}, Lorg/spongycastle/asn1/ba;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    .line 193
    sget-object v1, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    const-string v2, "SHA512WITHRSAANDMGF1"

    const/16 v3, 0x40

    invoke-static {v0, v3}, Lorg/spongycastle/jce/d;->a(Lorg/spongycastle/asn1/q/a;I)Lorg/spongycastle/asn1/l/x;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljavax/security/auth/x500/X500Principal;Ljava/security/PublicKey;Lorg/spongycastle/asn1/t;Ljava/security/PrivateKey;)V
    .locals 7

    .prologue
    .line 276
    invoke-static {p2}, Lorg/spongycastle/jce/d;->a(Ljavax/security/auth/x500/X500Principal;)Lorg/spongycastle/asn1/q/aw;

    move-result-object v2

    sget-object v6, Lorg/spongycastle/jce/provider/BouncyCastleProvider;->PROVIDER_NAME:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/spongycastle/jce/d;-><init>(Ljava/lang/String;Lorg/spongycastle/asn1/q/aw;Ljava/security/PublicKey;Lorg/spongycastle/asn1/t;Ljava/security/PrivateKey;Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/spongycastle/asn1/q/aw;Ljava/security/PublicKey;Lorg/spongycastle/asn1/t;Ljava/security/PrivateKey;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 307
    invoke-direct {p0}, Lorg/spongycastle/asn1/l/d;-><init>()V

    .line 308
    invoke-static {p1}, Lorg/spongycastle/util/g;->cZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 309
    sget-object v0, Lorg/spongycastle/jce/d;->algorithms:Ljava/util/Hashtable;

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/bc;

    .line 311
    if-nez v0, :cond_5

    .line 315
    :try_start_0
    new-instance v0, Lorg/spongycastle/asn1/bc;

    invoke-direct {v0, v2}, Lorg/spongycastle/asn1/bc;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 323
    :goto_0
    if-nez p2, :cond_0

    .line 325
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "subject must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :catch_0
    move-exception v0

    .line 319
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown signature type requested"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_0
    if-nez p3, :cond_1

    .line 330
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "public key must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_1
    sget-object v0, Lorg/spongycastle/jce/d;->adK:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 335
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/bc;)V

    iput-object v0, p0, Lorg/spongycastle/jce/d;->Ks:Lorg/spongycastle/asn1/q/a;

    .line 348
    :goto_1
    :try_start_1
    invoke-interface {p3}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    .line 349
    new-instance v1, Lorg/spongycastle/asn1/l/e;

    new-instance v2, Lorg/spongycastle/asn1/q/ah;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/q/ah;-><init>(Lorg/spongycastle/asn1/r;)V

    invoke-direct {v1, p2, v2, p4}, Lorg/spongycastle/asn1/l/e;-><init>(Lorg/spongycastle/asn1/q/aw;Lorg/spongycastle/asn1/q/ah;Lorg/spongycastle/asn1/t;)V

    iput-object v1, p0, Lorg/spongycastle/jce/d;->Kr:Lorg/spongycastle/asn1/l/e;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 357
    if-nez p6, :cond_4

    .line 359
    invoke-static {p1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 366
    :goto_2
    invoke-virtual {v0, p5}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    .line 370
    :try_start_2
    iget-object v1, p0, Lorg/spongycastle/jce/d;->Kr:Lorg/spongycastle/asn1/l/e;

    const-string v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/l/e;->getEncoded(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 377
    new-instance v1, Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/aq;-><init>([B)V

    iput-object v1, p0, Lorg/spongycastle/jce/d;->Kt:Lorg/spongycastle/asn1/aq;

    .line 378
    return-void

    .line 337
    :cond_2
    sget-object v0, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 339
    new-instance v3, Lorg/spongycastle/asn1/q/a;

    sget-object v0, Lorg/spongycastle/jce/d;->params:Ljava/util/Hashtable;

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-direct {v3, v1, v0}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/bc;Lorg/spongycastle/asn1/d;)V

    iput-object v3, p0, Lorg/spongycastle/jce/d;->Ks:Lorg/spongycastle/asn1/q/a;

    goto :goto_1

    .line 343
    :cond_3
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/ba;->GC:Lorg/spongycastle/asn1/ba;

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/bc;Lorg/spongycastle/asn1/d;)V

    iput-object v0, p0, Lorg/spongycastle/jce/d;->Ks:Lorg/spongycastle/asn1/q/a;

    goto :goto_1

    .line 351
    :catch_1
    move-exception v0

    .line 353
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "can\'t encode public key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 363
    :cond_4
    invoke-static {p1, p6}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    goto :goto_2

    .line 372
    :catch_2
    move-exception v0

    .line 374
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception encoding TBS cert request - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 227
    invoke-static {p1}, Lorg/spongycastle/jce/d;->Q([B)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/l/d;-><init>(Lorg/spongycastle/asn1/r;)V

    .line 228
    return-void
.end method

.method private static Q([B)Lorg/spongycastle/asn1/r;
    .locals 2

    .prologue
    .line 210
    :try_start_0
    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/h;-><init>([B)V

    .line 212
    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 214
    :catch_0
    move-exception v0

    .line 216
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "badly encoded request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lorg/spongycastle/asn1/q/a;I)Lorg/spongycastle/asn1/l/x;
    .locals 5

    .prologue
    .line 198
    new-instance v0, Lorg/spongycastle/asn1/l/x;

    new-instance v1, Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/l/q;->KQ:Lorg/spongycastle/asn1/l;

    invoke-direct {v1, v2, p0}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    new-instance v2, Lorg/spongycastle/asn1/i;

    invoke-direct {v2, p1}, Lorg/spongycastle/asn1/i;-><init>(I)V

    new-instance v3, Lorg/spongycastle/asn1/i;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/spongycastle/asn1/l/x;-><init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/i;Lorg/spongycastle/asn1/i;)V

    return-object v0
.end method

.method private static a(Ljavax/security/auth/x500/X500Principal;)Lorg/spongycastle/asn1/q/aw;
    .locals 2

    .prologue
    .line 256
    :try_start_0
    new-instance v0, Lorg/spongycastle/jce/h;

    invoke-virtual {p0}, Ljavax/security/auth/x500/X500Principal;->getEncoded()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/jce/h;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 258
    :catch_0
    move-exception v0

    .line 260
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "can\'t convert name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getEncoded()[B
    .locals 2

    .prologue
    .line 530
    :try_start_0
    const-string v0, "DER"

    invoke-virtual {p0, v0}, Lorg/spongycastle/jce/d;->getEncoded(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 532
    :catch_0
    move-exception v0

    .line 534
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
