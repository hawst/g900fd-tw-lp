.class public Lorg/spongycastle/jce/a;
.super Ljava/lang/Object;
.source "ECGOST3410NamedCurveTable.java"


# direct methods
.method public static cS(Ljava/lang/String;)Lorg/spongycastle/jce/spec/c;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-static {p0}, Lorg/spongycastle/asn1/c/b;->cF(Ljava/lang/String;)Lorg/spongycastle/crypto/j/q;

    move-result-object v1

    .line 26
    if-nez v1, :cond_0

    .line 30
    :try_start_0
    new-instance v1, Lorg/spongycastle/asn1/l;

    invoke-direct {v1, p0}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/spongycastle/asn1/c/b;->a(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/crypto/j/q;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 38
    :cond_0
    if-nez v1, :cond_1

    .line 43
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/spongycastle/jce/spec/c;

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/q;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v2

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/q;->pP()Lorg/spongycastle/a/a/j;

    move-result-object v3

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/q;->pQ()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/q;->pR()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v1}, Lorg/spongycastle/crypto/j/q;->getSeed()[B

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lorg/spongycastle/jce/spec/c;-><init>(Ljava/lang/String;Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    goto :goto_0

    .line 32
    :catch_0
    move-exception v1

    goto :goto_0
.end method
