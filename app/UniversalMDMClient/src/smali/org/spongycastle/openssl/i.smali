.class Lorg/spongycastle/openssl/i;
.super Ljava/lang/Object;
.source "PEMReader.java"

# interfaces
.implements Lorg/spongycastle/util/io/pem/d;


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;


# direct methods
.method private constructor <init>(Lorg/spongycastle/openssl/b;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lorg/spongycastle/openssl/i;->afk:Lorg/spongycastle/openssl/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/spongycastle/openssl/b;Lorg/spongycastle/openssl/b$1;)V
    .locals 0

    .prologue
    .line 584
    invoke-direct {p0, p1}, Lorg/spongycastle/openssl/i;-><init>(Lorg/spongycastle/openssl/b;)V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 599
    :try_start_0
    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-virtual {p1}, Lorg/spongycastle/util/io/pem/b;->sN()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/h;-><init>([B)V

    .line 601
    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/b/a;->U(Ljava/lang/Object;)Lorg/spongycastle/asn1/b/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 603
    :catch_0
    move-exception v0

    .line 605
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "problem parsing PKCS7 object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method
