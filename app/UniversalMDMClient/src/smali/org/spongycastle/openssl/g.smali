.class abstract Lorg/spongycastle/openssl/g;
.super Ljava/lang/Object;
.source "PEMReader.java"

# interfaces
.implements Lorg/spongycastle/util/io/pem/d;


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;

.field protected afn:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lorg/spongycastle/openssl/g;->afk:Lorg/spongycastle/openssl/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p2, p0, Lorg/spongycastle/openssl/g;->afn:Ljava/lang/String;

    .line 181
    return-void
.end method


# virtual methods
.method protected b(Lorg/spongycastle/util/io/pem/b;)Lorg/spongycastle/asn1/r;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 190
    .line 191
    const/4 v4, 0x0

    .line 192
    invoke-virtual {p1}, Lorg/spongycastle/util/io/pem/b;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 194
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v6, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/util/io/pem/a;

    .line 198
    invoke-virtual {v1}, Lorg/spongycastle/util/io/pem/a;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "Proc-Type"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lorg/spongycastle/util/io/pem/a;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v5, "4,ENCRYPTED"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    const/4 v1, 0x1

    move v2, v1

    move-object v1, v4

    :goto_1
    move-object v4, v1

    move v6, v2

    .line 206
    goto :goto_0

    .line 202
    :cond_0
    invoke-virtual {v1}, Lorg/spongycastle/util/io/pem/a;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "DEK-Info"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 204
    invoke-virtual {v1}, Lorg/spongycastle/util/io/pem/a;->getValue()Ljava/lang/String;

    move-result-object v1

    move v2, v6

    goto :goto_1

    .line 211
    :cond_1
    invoke-virtual {p1}, Lorg/spongycastle/util/io/pem/b;->sN()[B

    move-result-object v2

    .line 213
    if-eqz v6, :cond_4

    .line 215
    iget-object v1, p0, Lorg/spongycastle/openssl/g;->afk:Lorg/spongycastle/openssl/b;

    invoke-static {v1}, Lorg/spongycastle/openssl/b;->a(Lorg/spongycastle/openssl/b;)Lorg/spongycastle/openssl/s;

    move-result-object v1

    if-nez v1, :cond_2

    .line 217
    new-instance v0, Lorg/spongycastle/openssl/PasswordException;

    const-string v1, "No password finder specified, but a password is required"

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/PasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_2
    iget-object v1, p0, Lorg/spongycastle/openssl/g;->afk:Lorg/spongycastle/openssl/b;

    invoke-static {v1}, Lorg/spongycastle/openssl/b;->a(Lorg/spongycastle/openssl/b;)Lorg/spongycastle/openssl/s;

    move-result-object v1

    invoke-interface {v1}, Lorg/spongycastle/openssl/s;->getPassword()[C

    move-result-object v3

    .line 222
    if-nez v3, :cond_3

    .line 224
    new-instance v0, Lorg/spongycastle/openssl/PasswordException;

    const-string v1, "Password is null, but a password is required"

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/PasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_3
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v5, ","

    invoke-direct {v1, v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 229
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/util/a/d;->dc(Ljava/lang/String;)[B

    move-result-object v5

    .line 231
    iget-object v1, p0, Lorg/spongycastle/openssl/g;->afn:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lorg/spongycastle/openssl/q;->a(ZLjava/lang/String;[B[CLjava/lang/String;[B)[B

    move-result-object v2

    .line 236
    :cond_4
    :try_start_0
    invoke-static {v2}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 238
    :catch_0
    move-exception v0

    .line 240
    if-eqz v6, :cond_5

    .line 242
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    const-string v2, "exception decoding - please check password and data."

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 246
    :cond_5
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 249
    :catch_1
    move-exception v0

    .line 251
    if-eqz v6, :cond_6

    .line 253
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    const-string v2, "exception decoding - please check password and data."

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 257
    :cond_6
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :cond_7
    move-object v1, v4

    move v2, v6

    goto/16 :goto_1
.end method
