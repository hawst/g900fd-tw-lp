.class Lorg/spongycastle/openssl/d;
.super Lorg/spongycastle/openssl/g;
.source "PEMReader.java"


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;


# direct methods
.method public constructor <init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lorg/spongycastle/openssl/d;->afk:Lorg/spongycastle/openssl/b;

    .line 320
    invoke-direct {p0, p1, p2}, Lorg/spongycastle/openssl/g;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    .line 321
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 328
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/spongycastle/openssl/d;->b(Lorg/spongycastle/util/io/pem/b;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    .line 330
    invoke-static {v0}, Lorg/spongycastle/asn1/m/a;->as(Ljava/lang/Object;)Lorg/spongycastle/asn1/m/a;

    move-result-object v0

    .line 331
    new-instance v1, Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vq:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m/a;->od()Lorg/spongycastle/asn1/q;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    .line 332
    new-instance v2, Lorg/spongycastle/asn1/l/s;

    invoke-direct {v2, v1, v0}, Lorg/spongycastle/asn1/l/s;-><init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)V

    .line 333
    new-instance v3, Lorg/spongycastle/asn1/q/ah;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m/a;->oc()Lorg/spongycastle/asn1/aq;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/aq;->getBytes()[B

    move-result-object v0

    invoke-direct {v3, v1, v0}, Lorg/spongycastle/asn1/q/ah;-><init>(Lorg/spongycastle/asn1/q/a;[B)V

    .line 335
    new-instance v0, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/s;->getEncoded()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    .line 336
    new-instance v1, Ljava/security/spec/X509EncodedKeySpec;

    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/ah;->getEncoded()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 339
    const-string v2, "ECDSA"

    iget-object v3, p0, Lorg/spongycastle/openssl/d;->afn:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v2

    .line 342
    new-instance v3, Ljava/security/KeyPair;

    invoke-virtual {v2, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v1

    invoke-virtual {v2, v0}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v0

    invoke-direct {v3, v1, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    return-object v3

    .line 346
    :catch_0
    move-exception v0

    .line 348
    throw v0

    .line 350
    :catch_1
    move-exception v0

    .line 352
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "problem creating EC private key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method
