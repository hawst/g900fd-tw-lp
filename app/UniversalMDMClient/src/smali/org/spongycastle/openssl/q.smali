.class final Lorg/spongycastle/openssl/q;
.super Ljava/lang/Object;
.source "PEMUtilities.java"


# static fields
.field private static final afo:Ljava/util/Map;

.field private static final afp:Ljava/util/Set;

.field private static final afq:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xc0

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/spongycastle/openssl/q;->afo:Ljava/util/Map;

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/spongycastle/openssl/q;->afq:Ljava/util/Set;

    .line 36
    sget-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Lb:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Lc:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Ld:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Le:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Lf:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Lg:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lorg/spongycastle/openssl/q;->afq:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Lh:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    sget-object v0, Lorg/spongycastle/openssl/q;->afq:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Lk:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lorg/spongycastle/openssl/q;->afq:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->Jt:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v0, Lorg/spongycastle/openssl/q;->afq:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JA:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lorg/spongycastle/openssl/q;->afq:Ljava/util/Set;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JH:Lorg/spongycastle/asn1/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v0, Lorg/spongycastle/openssl/q;->afo:Ljava/util/Map;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Lk:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lorg/spongycastle/openssl/q;->afo:Ljava/util/Map;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->Jt:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lorg/spongycastle/openssl/q;->afo:Ljava/util/Map;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JA:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lorg/spongycastle/openssl/q;->afo:Ljava/util/Map;

    sget-object v1, Lorg/spongycastle/asn1/i/b;->JH:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void
.end method

.method static a(Ljava/lang/String;[C[BI)Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lorg/spongycastle/crypto/e/s;

    invoke-direct {v0}, Lorg/spongycastle/crypto/e/s;-><init>()V

    .line 84
    invoke-static {p1}, Lorg/spongycastle/crypto/p;->PKCS5PasswordToBytes([C)[B

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lorg/spongycastle/crypto/p;->init([B[BI)V

    .line 89
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {p0}, Lorg/spongycastle/openssl/q;->cT(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lorg/spongycastle/crypto/p;->generateDerivedParameters(I)Lorg/spongycastle/crypto/h;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/crypto/j/ak;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ak;->getKey()[B

    move-result-object v0

    invoke-direct {v1, v0, p0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method private static a([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lorg/spongycastle/openssl/q;->a([CLjava/lang/String;I[BZ)Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method private static a([CLjava/lang/String;I[BZ)Ljavax/crypto/SecretKey;
    .locals 4

    .prologue
    .line 265
    new-instance v0, Lorg/spongycastle/crypto/e/p;

    invoke-direct {v0}, Lorg/spongycastle/crypto/e/p;-><init>()V

    .line 267
    invoke-static {p0}, Lorg/spongycastle/crypto/p;->PKCS5PasswordToBytes([C)[B

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lorg/spongycastle/crypto/e/p;->e([B[B)V

    .line 270
    mul-int/lit8 v1, p2, 0x8

    invoke-virtual {v0, v1}, Lorg/spongycastle/crypto/e/p;->generateDerivedParameters(I)Lorg/spongycastle/crypto/h;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/crypto/j/ak;

    .line 271
    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ak;->getKey()[B

    move-result-object v0

    .line 272
    if-eqz p4, :cond_0

    array-length v1, v0

    const/16 v2, 0x18

    if-lt v1, v2, :cond_0

    .line 275
    const/4 v1, 0x0

    const/16 v2, 0x10

    const/16 v3, 0x8

    invoke-static {v0, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 277
    :cond_0
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v1, v0, p1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method static a(ZLjava/lang/String;[B[CLjava/lang/String;[B)[B
    .locals 6

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 102
    if-eqz p1, :cond_0

    .line 104
    invoke-static {p1}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v1

    .line 105
    if-nez v1, :cond_0

    .line 107
    new-instance v0, Lorg/spongycastle/openssl/EncryptionException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cannot find provider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/EncryptionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 111
    invoke-static/range {v0 .. v5}, Lorg/spongycastle/openssl/q;->a(ZLjava/security/Provider;[B[CLjava/lang/String;[B)[B

    move-result-object v0

    return-object v0
.end method

.method static a(ZLjava/security/Provider;[B[CLjava/lang/String;[B)[B
    .locals 10

    .prologue
    const/16 v5, 0x80

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v8, 0x8

    .line 123
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, p5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 125
    const-string v1, "CBC"

    .line 126
    const-string v0, "PKCS5Padding"

    .line 130
    const-string v6, "-CFB"

    invoke-virtual {p4, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 132
    const-string v1, "CFB"

    .line 133
    const-string v0, "NoPadding"

    .line 135
    :cond_0
    const-string v6, "-ECB"

    invoke-virtual {p4, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "DES-EDE"

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "DES-EDE3"

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 141
    :cond_1
    const-string v1, "ECB"

    .line 142
    const/4 v2, 0x0

    .line 144
    :cond_2
    const-string v6, "-OFB"

    invoke-virtual {p4, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 146
    const-string v1, "OFB"

    .line 147
    const-string v0, "NoPadding"

    .line 152
    :cond_3
    const-string v6, "DES-EDE"

    invoke-virtual {p4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 154
    const-string v5, "DESede"

    .line 157
    const-string v6, "DES-EDE3"

    invoke-virtual {p4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    move v3, v4

    .line 158
    :cond_4
    const/16 v6, 0x18

    invoke-static {p3, v5, v6, p5, v3}, Lorg/spongycastle/openssl/q;->a([CLjava/lang/String;I[BZ)Ljavax/crypto/SecretKey;

    move-result-object v3

    move-object v9, v3

    move-object v3, v5

    move-object v5, v2

    move-object v2, v9

    .line 226
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    :try_start_0
    invoke-static {v0, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 231
    if-eqz p0, :cond_10

    .line 233
    :goto_1
    if-nez v5, :cond_11

    .line 235
    invoke-virtual {v0, v4, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 241
    :goto_2
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 160
    :cond_5
    const-string v6, "DES-"

    invoke-virtual {p4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 162
    const-string v5, "DES"

    .line 163
    invoke-static {p3, v5, v8, p5}, Lorg/spongycastle/openssl/q;->a([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v3

    move-object v9, v3

    move-object v3, v5

    move-object v5, v2

    move-object v2, v9

    goto :goto_0

    .line 165
    :cond_6
    const-string v6, "BF-"

    invoke-virtual {p4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 167
    const-string v5, "Blowfish"

    .line 168
    const/16 v3, 0x10

    invoke-static {p3, v5, v3, p5}, Lorg/spongycastle/openssl/q;->a([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v3

    move-object v9, v3

    move-object v3, v5

    move-object v5, v2

    move-object v2, v9

    goto :goto_0

    .line 170
    :cond_7
    const-string v6, "RC2-"

    invoke-virtual {p4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 172
    const-string v6, "RC2"

    .line 174
    const-string v3, "RC2-40-"

    invoke-virtual {p4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 176
    const/16 v3, 0x28

    .line 182
    :goto_3
    div-int/lit8 v5, v3, 0x8

    invoke-static {p3, v6, v5, p5}, Lorg/spongycastle/openssl/q;->a([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v5

    .line 183
    if-nez v2, :cond_9

    .line 185
    new-instance v2, Ljavax/crypto/spec/RC2ParameterSpec;

    invoke-direct {v2, v3}, Ljavax/crypto/spec/RC2ParameterSpec;-><init>(I)V

    :goto_4
    move-object v3, v6

    move-object v9, v5

    move-object v5, v2

    move-object v2, v9

    .line 191
    goto :goto_0

    .line 178
    :cond_8
    const-string v3, "RC2-64-"

    invoke-virtual {p4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 180
    const/16 v3, 0x40

    goto :goto_3

    .line 189
    :cond_9
    new-instance v2, Ljavax/crypto/spec/RC2ParameterSpec;

    invoke-direct {v2, v3, p5}, Ljavax/crypto/spec/RC2ParameterSpec;-><init>(I[B)V

    goto :goto_4

    .line 192
    :cond_a
    const-string v6, "AES-"

    invoke-virtual {p4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 194
    const-string v7, "AES"

    .line 196
    array-length v6, p5

    if-le v6, v8, :cond_b

    .line 198
    new-array v6, v8, [B

    .line 199
    invoke-static {p5, v3, v6, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p5, v6

    .line 203
    :cond_b
    const-string v3, "AES-128-"

    invoke-virtual {p4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 219
    :goto_5
    const-string v3, "AES"

    div-int/lit8 v5, v5, 0x8

    invoke-static {p3, v3, v5, p5}, Lorg/spongycastle/openssl/q;->a([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v3

    move-object v5, v2

    move-object v2, v3

    move-object v3, v7

    .line 220
    goto/16 :goto_0

    .line 207
    :cond_c
    const-string v3, "AES-192-"

    invoke-virtual {p4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 209
    const/16 v5, 0xc0

    goto :goto_5

    .line 211
    :cond_d
    const-string v3, "AES-256-"

    invoke-virtual {p4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 213
    const/16 v5, 0x100

    goto :goto_5

    .line 217
    :cond_e
    new-instance v0, Lorg/spongycastle/openssl/EncryptionException;

    const-string v1, "unknown AES encryption with private key"

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/EncryptionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_f
    new-instance v0, Lorg/spongycastle/openssl/EncryptionException;

    const-string v1, "unknown encryption with private key"

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/EncryptionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_10
    const/4 v4, 0x2

    goto/16 :goto_1

    .line 239
    :cond_11
    :try_start_1
    invoke-virtual {v0, v4, v2, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 243
    :catch_0
    move-exception v0

    .line 245
    new-instance v1, Lorg/spongycastle/openssl/EncryptionException;

    const-string v2, "exception using cipher - please check password and data."

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/EncryptionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_12
    move v3, v5

    goto/16 :goto_3
.end method

.method static b(Lorg/spongycastle/asn1/bc;)Z
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lorg/spongycastle/openssl/q;->afp:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static c(Lorg/spongycastle/asn1/bc;)Z
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/spongycastle/asn1/l/q;->Ne:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static cT(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 57
    sget-object v0, Lorg/spongycastle/openssl/q;->afo:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no key size for algorithm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    sget-object v0, Lorg/spongycastle/openssl/q;->afo:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static g(Lorg/spongycastle/asn1/l;)Z
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lorg/spongycastle/openssl/q;->afq:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
