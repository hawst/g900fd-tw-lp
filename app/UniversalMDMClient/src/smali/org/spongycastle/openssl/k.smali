.class Lorg/spongycastle/openssl/k;
.super Ljava/lang/Object;
.source "PEMReader.java"

# interfaces
.implements Lorg/spongycastle/util/io/pem/d;


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;

.field private afn:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lorg/spongycastle/openssl/k;->afk:Lorg/spongycastle/openssl/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    iput-object p2, p0, Lorg/spongycastle/openssl/k;->afn:Ljava/lang/String;

    .line 414
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 419
    new-instance v1, Ljava/security/spec/X509EncodedKeySpec;

    invoke-virtual {p1}, Lorg/spongycastle/util/io/pem/b;->sN()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 420
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "DSA"

    aput-object v3, v2, v0

    const/4 v3, 0x1

    const-string v4, "RSA"

    aput-object v4, v2, v3

    .line 421
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 425
    :try_start_0
    aget-object v3, v2, v0

    iget-object v4, p0, Lorg/spongycastle/openssl/k;->afn:Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v3

    .line 426
    invoke-virtual {v3, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 444
    :goto_1
    return-object v0

    .line 438
    :catch_0
    move-exception v0

    .line 440
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t find provider "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/openssl/k;->afn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :catch_1
    move-exception v3

    .line 421
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 444
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 430
    :catch_2
    move-exception v3

    goto :goto_2
.end method
