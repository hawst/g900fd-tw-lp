.class public Lorg/spongycastle/openssl/b;
.super Lorg/spongycastle/util/io/pem/e;
.source "PEMReader.java"


# instance fields
.field private final afi:Ljava/util/Map;

.field private afj:Lorg/spongycastle/openssl/s;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2

    .prologue
    .line 85
    const/4 v0, 0x0

    const-string v1, "SC"

    invoke-direct {p0, p1, v0, v1}, Lorg/spongycastle/openssl/b;-><init>(Ljava/io/Reader;Lorg/spongycastle/openssl/s;Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;Lorg/spongycastle/openssl/s;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2, p3, p3}, Lorg/spongycastle/openssl/b;-><init>(Ljava/io/Reader;Lorg/spongycastle/openssl/s;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;Lorg/spongycastle/openssl/s;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 131
    invoke-direct {p0, p1}, Lorg/spongycastle/util/io/pem/e;-><init>(Ljava/io/Reader;)V

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    .line 133
    iput-object p2, p0, Lorg/spongycastle/openssl/b;->afj:Lorg/spongycastle/openssl/s;

    .line 135
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "CERTIFICATE REQUEST"

    new-instance v2, Lorg/spongycastle/openssl/h;

    invoke-direct {v2, p0, v3}, Lorg/spongycastle/openssl/h;-><init>(Lorg/spongycastle/openssl/b;Lorg/spongycastle/openssl/b$1;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "NEW CERTIFICATE REQUEST"

    new-instance v2, Lorg/spongycastle/openssl/h;

    invoke-direct {v2, p0, v3}, Lorg/spongycastle/openssl/h;-><init>(Lorg/spongycastle/openssl/b;Lorg/spongycastle/openssl/b$1;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "CERTIFICATE"

    new-instance v2, Lorg/spongycastle/openssl/p;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/p;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "X509 CERTIFICATE"

    new-instance v2, Lorg/spongycastle/openssl/p;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/p;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "X509 CRL"

    new-instance v2, Lorg/spongycastle/openssl/o;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/o;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "PKCS7"

    new-instance v2, Lorg/spongycastle/openssl/i;

    invoke-direct {v2, p0, v3}, Lorg/spongycastle/openssl/i;-><init>(Lorg/spongycastle/openssl/b;Lorg/spongycastle/openssl/b$1;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "ATTRIBUTE CERTIFICATE"

    new-instance v2, Lorg/spongycastle/openssl/n;

    invoke-direct {v2, p0, v3}, Lorg/spongycastle/openssl/n;-><init>(Lorg/spongycastle/openssl/b;Lorg/spongycastle/openssl/b$1;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "EC PARAMETERS"

    new-instance v2, Lorg/spongycastle/openssl/e;

    invoke-direct {v2, p0, v3}, Lorg/spongycastle/openssl/e;-><init>(Lorg/spongycastle/openssl/b;Lorg/spongycastle/openssl/b$1;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "PUBLIC KEY"

    new-instance v2, Lorg/spongycastle/openssl/k;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/k;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "RSA PUBLIC KEY"

    new-instance v2, Lorg/spongycastle/openssl/m;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/m;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "RSA PRIVATE KEY"

    new-instance v2, Lorg/spongycastle/openssl/l;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/l;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "DSA PRIVATE KEY"

    new-instance v2, Lorg/spongycastle/openssl/c;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/c;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "EC PRIVATE KEY"

    new-instance v2, Lorg/spongycastle/openssl/d;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/d;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "ENCRYPTED PRIVATE KEY"

    new-instance v2, Lorg/spongycastle/openssl/f;

    invoke-direct {v2, p0, p3, p4}, Lorg/spongycastle/openssl/f;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    const-string v1, "PRIVATE KEY"

    new-instance v2, Lorg/spongycastle/openssl/j;

    invoke-direct {v2, p0, p4}, Lorg/spongycastle/openssl/j;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    return-void
.end method

.method static synthetic a(Lorg/spongycastle/openssl/b;)Lorg/spongycastle/openssl/s;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/spongycastle/openssl/b;->afj:Lorg/spongycastle/openssl/s;

    return-object v0
.end method


# virtual methods
.method public readObject()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/spongycastle/openssl/b;->sO()Lorg/spongycastle/util/io/pem/b;

    move-result-object v1

    .line 157
    if-eqz v1, :cond_1

    .line 159
    invoke-virtual {v1}, Lorg/spongycastle/util/io/pem/b;->getType()Ljava/lang/String;

    move-result-object v0

    .line 160
    iget-object v2, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    iget-object v2, p0, Lorg/spongycastle/openssl/b;->afi:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/util/io/pem/d;

    invoke-interface {v0, v1}, Lorg/spongycastle/util/io/pem/d;->a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    .line 166
    :cond_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unrecognised object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
