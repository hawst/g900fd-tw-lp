.class Lorg/spongycastle/openssl/f;
.super Ljava/lang/Object;
.source "PEMReader.java"

# interfaces
.implements Lorg/spongycastle/util/io/pem/d;


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;

.field private afl:Ljava/lang/String;

.field private afm:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lorg/spongycastle/openssl/f;->afk:Lorg/spongycastle/openssl/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 658
    iput-object p2, p0, Lorg/spongycastle/openssl/f;->afl:Ljava/lang/String;

    .line 659
    iput-object p3, p0, Lorg/spongycastle/openssl/f;->afm:Ljava/lang/String;

    .line 660
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 673
    :try_start_0
    invoke-virtual {p1}, Lorg/spongycastle/util/io/pem/b;->sN()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/i;->ac(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/i;

    move-result-object v1

    .line 674
    invoke-virtual {v1}, Lorg/spongycastle/asn1/l/i;->nz()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    .line 676
    iget-object v2, p0, Lorg/spongycastle/openssl/f;->afk:Lorg/spongycastle/openssl/b;

    invoke-static {v2}, Lorg/spongycastle/openssl/b;->a(Lorg/spongycastle/openssl/b;)Lorg/spongycastle/openssl/s;

    move-result-object v2

    if-nez v2, :cond_0

    .line 678
    new-instance v0, Lorg/spongycastle/openssl/PEMException;

    const-string v1, "no PasswordFinder specified"

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 754
    :catch_0
    move-exception v0

    .line 756
    throw v0

    .line 681
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/openssl/q;->g(Lorg/spongycastle/asn1/l;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 683
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/n;->ag(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/n;

    move-result-object v0

    .line 684
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/n;->nD()Lorg/spongycastle/asn1/l/k;

    move-result-object v2

    .line 685
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/n;->nE()Lorg/spongycastle/asn1/l/j;

    move-result-object v3

    .line 686
    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/k;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l/o;

    .line 688
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/o;->nC()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v2

    .line 689
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/o;->getSalt()[B

    move-result-object v0

    .line 691
    invoke-virtual {v3}, Lorg/spongycastle/asn1/l/j;->om()Lorg/spongycastle/asn1/l;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v4

    .line 693
    iget-object v5, p0, Lorg/spongycastle/openssl/f;->afk:Lorg/spongycastle/openssl/b;

    invoke-static {v5}, Lorg/spongycastle/openssl/b;->a(Lorg/spongycastle/openssl/b;)Lorg/spongycastle/openssl/s;

    move-result-object v5

    invoke-interface {v5}, Lorg/spongycastle/openssl/s;->getPassword()[C

    move-result-object v5

    invoke-static {v4, v5, v0, v2}, Lorg/spongycastle/openssl/q;->a(Ljava/lang/String;[C[BI)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 695
    iget-object v2, p0, Lorg/spongycastle/openssl/f;->afl:Ljava/lang/String;

    invoke-static {v4, v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 696
    iget-object v5, p0, Lorg/spongycastle/openssl/f;->afl:Ljava/lang/String;

    invoke-static {v4, v5}, Ljava/security/AlgorithmParameters;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/AlgorithmParameters;

    move-result-object v4

    .line 698
    invoke-virtual {v3}, Lorg/spongycastle/asn1/l/j;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v3

    invoke-interface {v3}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/asn1/q;->getEncoded()[B

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/security/AlgorithmParameters;->init([B)V

    .line 700
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/AlgorithmParameters;)V

    .line 702
    invoke-virtual {v1}, Lorg/spongycastle/asn1/l/i;->getEncryptedData()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/s;->ak(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/s;

    move-result-object v0

    .line 703
    new-instance v1, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/s;->getEncoded()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    .line 705
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/s;->nI()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/openssl/f;->afm:Ljava/lang/String;

    invoke-static {v0, v2}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    .line 707
    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v0

    .line 747
    :goto_0
    return-object v0

    .line 709
    :cond_1
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/openssl/q;->c(Lorg/spongycastle/asn1/bc;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 711
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/l/p;->ai(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/p;

    move-result-object v2

    .line 712
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    .line 713
    new-instance v3, Ljavax/crypto/spec/PBEKeySpec;

    iget-object v4, p0, Lorg/spongycastle/openssl/f;->afk:Lorg/spongycastle/openssl/b;

    invoke-static {v4}, Lorg/spongycastle/openssl/b;->a(Lorg/spongycastle/openssl/b;)Lorg/spongycastle/openssl/s;

    move-result-object v4

    invoke-interface {v4}, Lorg/spongycastle/openssl/s;->getPassword()[C

    move-result-object v4

    invoke-direct {v3, v4}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C)V

    .line 715
    iget-object v4, p0, Lorg/spongycastle/openssl/f;->afl:Ljava/lang/String;

    invoke-static {v0, v4}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v4

    .line 716
    new-instance v5, Ljavax/crypto/spec/PBEParameterSpec;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/p;->getIV()[B

    move-result-object v6

    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/p;->nF()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v2

    invoke-direct {v5, v6, v2}, Ljavax/crypto/spec/PBEParameterSpec;-><init>([BI)V

    .line 718
    iget-object v2, p0, Lorg/spongycastle/openssl/f;->afl:Ljava/lang/String;

    invoke-static {v0, v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 720
    const/4 v2, 0x2

    invoke-virtual {v4, v3}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 722
    invoke-virtual {v1}, Lorg/spongycastle/asn1/l/i;->getEncryptedData()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/s;->ak(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/s;

    move-result-object v0

    .line 723
    new-instance v1, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/s;->getEncoded()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    .line 725
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/s;->nJ()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/openssl/f;->afm:Ljava/lang/String;

    invoke-static {v0, v2}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    .line 727
    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v0

    goto :goto_0

    .line 729
    :cond_2
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/openssl/q;->b(Lorg/spongycastle/asn1/bc;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 731
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/l/m;->af(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/m;

    move-result-object v2

    .line 732
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    .line 733
    new-instance v3, Ljavax/crypto/spec/PBEKeySpec;

    iget-object v4, p0, Lorg/spongycastle/openssl/f;->afk:Lorg/spongycastle/openssl/b;

    invoke-static {v4}, Lorg/spongycastle/openssl/b;->a(Lorg/spongycastle/openssl/b;)Lorg/spongycastle/openssl/s;

    move-result-object v4

    invoke-interface {v4}, Lorg/spongycastle/openssl/s;->getPassword()[C

    move-result-object v4

    invoke-direct {v3, v4}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C)V

    .line 735
    iget-object v4, p0, Lorg/spongycastle/openssl/f;->afl:Ljava/lang/String;

    invoke-static {v0, v4}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v4

    .line 736
    new-instance v5, Ljavax/crypto/spec/PBEParameterSpec;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/m;->getSalt()[B

    move-result-object v6

    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/m;->nC()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v2

    invoke-direct {v5, v6, v2}, Ljavax/crypto/spec/PBEParameterSpec;-><init>([BI)V

    .line 738
    iget-object v2, p0, Lorg/spongycastle/openssl/f;->afl:Ljava/lang/String;

    invoke-static {v0, v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 740
    const/4 v2, 0x2

    invoke-virtual {v4, v3}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 742
    invoke-virtual {v1}, Lorg/spongycastle/asn1/l/i;->getEncryptedData()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/s;->ak(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/s;

    move-result-object v0

    .line 743
    new-instance v1, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/s;->getEncoded()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    .line 745
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/s;->nJ()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/openssl/f;->afm:Ljava/lang/String;

    invoke-static {v0, v2}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    .line 747
    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v0

    goto/16 :goto_0

    .line 751
    :cond_3
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown algorithm: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->om()Lorg/spongycastle/asn1/l;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 758
    :catch_1
    move-exception v0

    .line 760
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "problem parsing ENCRYPTED PRIVATE KEY: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method
