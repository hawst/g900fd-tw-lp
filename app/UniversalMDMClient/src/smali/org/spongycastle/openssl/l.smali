.class Lorg/spongycastle/openssl/l;
.super Lorg/spongycastle/openssl/g;
.source "PEMReader.java"


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;


# direct methods
.method public constructor <init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lorg/spongycastle/openssl/l;->afk:Lorg/spongycastle/openssl/b;

    .line 363
    invoke-direct {p0, p1, p2}, Lorg/spongycastle/openssl/g;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    .line 364
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 371
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/spongycastle/openssl/l;->b(Lorg/spongycastle/util/io/pem/b;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_0

    .line 375
    new-instance v0, Lorg/spongycastle/openssl/PEMException;

    const-string v1, "malformed sequence in RSA private key"

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 394
    :catch_0
    move-exception v0

    .line 396
    throw v0

    .line 378
    :cond_0
    :try_start_1
    invoke-static {v0}, Lorg/spongycastle/asn1/l/v;->an(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/v;

    move-result-object v8

    .line 380
    new-instance v9, Ljava/security/spec/RSAPublicKeySpec;

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v1

    invoke-direct {v9, v0, v1}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 382
    new-instance v0, Ljava/security/spec/RSAPrivateCrtKeySpec;

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->getModulus()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->nP()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->nQ()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->nR()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->nS()Ljava/math/BigInteger;

    move-result-object v7

    invoke-virtual {v8}, Lorg/spongycastle/asn1/l/v;->nT()Ljava/math/BigInteger;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Ljava/security/spec/RSAPrivateCrtKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 388
    const-string v1, "RSA"

    iget-object v2, p0, Lorg/spongycastle/openssl/l;->afn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 390
    new-instance v2, Ljava/security/KeyPair;

    invoke-virtual {v1, v9}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v3

    invoke-virtual {v1, v0}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object v2

    .line 398
    :catch_1
    move-exception v0

    .line 400
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "problem creating RSA private key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method
