.class Lorg/spongycastle/openssl/c;
.super Lorg/spongycastle/openssl/g;
.source "PEMReader.java"


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;


# direct methods
.method public constructor <init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lorg/spongycastle/openssl/c;->afk:Lorg/spongycastle/openssl/b;

    .line 268
    invoke-direct {p0, p1, p2}, Lorg/spongycastle/openssl/g;-><init>(Lorg/spongycastle/openssl/b;Ljava/lang/String;)V

    .line 269
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 276
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/spongycastle/openssl/c;->b(Lorg/spongycastle/util/io/pem/b;)Lorg/spongycastle/asn1/r;

    move-result-object v4

    .line 278
    invoke-virtual {v4}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 280
    new-instance v0, Lorg/spongycastle/openssl/PEMException;

    const-string v1, "malformed sequence in DSA private key"

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 303
    :catch_0
    move-exception v0

    .line 305
    throw v0

    .line 284
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v4, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/az;

    .line 285
    const/4 v1, 0x2

    invoke-virtual {v4, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/asn1/az;

    .line 286
    const/4 v2, 0x3

    invoke-virtual {v4, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    check-cast v2, Lorg/spongycastle/asn1/az;

    .line 287
    const/4 v3, 0x4

    invoke-virtual {v4, v3}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    check-cast v3, Lorg/spongycastle/asn1/az;

    .line 288
    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v4

    check-cast v4, Lorg/spongycastle/asn1/az;

    .line 290
    new-instance v5, Ljava/security/spec/DSAPrivateKeySpec;

    invoke-virtual {v4}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v0}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v1}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v7

    invoke-virtual {v2}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v8

    invoke-direct {v5, v4, v6, v7, v8}, Ljava/security/spec/DSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 293
    new-instance v4, Ljava/security/spec/DSAPublicKeySpec;

    invoke-virtual {v3}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v1}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v2}, Lorg/spongycastle/asn1/az;->nb()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v4, v3, v0, v1, v2}, Ljava/security/spec/DSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 297
    const-string v0, "DSA"

    iget-object v1, p0, Lorg/spongycastle/openssl/c;->afn:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    .line 299
    new-instance v1, Ljava/security/KeyPair;

    invoke-virtual {v0, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v2

    invoke-virtual {v0, v5}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    .line 307
    :catch_1
    move-exception v0

    .line 309
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "problem creating DSA private key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method
