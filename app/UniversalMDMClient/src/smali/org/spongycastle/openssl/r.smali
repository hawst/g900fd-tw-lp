.class public Lorg/spongycastle/openssl/r;
.super Lorg/spongycastle/util/io/pem/f;
.source "PEMWriter.java"


# instance fields
.field private afn:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 27
    const-string v0, "SC"

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/openssl/r;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lorg/spongycastle/util/io/pem/f;-><init>(Ljava/io/Writer;)V

    .line 36
    iput-object p2, p0, Lorg/spongycastle/openssl/r;->afn:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/c;)V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0, p1}, Lorg/spongycastle/util/io/pem/f;->a(Lorg/spongycastle/util/io/pem/c;)V

    .line 63
    return-void
.end method

.method public writeObject(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 45
    :try_start_0
    new-instance v0, Lorg/spongycastle/openssl/a;

    invoke-direct {v0, p1}, Lorg/spongycastle/openssl/a;-><init>(Ljava/lang/Object;)V

    invoke-super {p0, v0}, Lorg/spongycastle/util/io/pem/f;->a(Lorg/spongycastle/util/io/pem/c;)V
    :try_end_0
    .catch Lorg/spongycastle/util/io/pem/PemGenerationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 49
    invoke-virtual {v0}, Lorg/spongycastle/util/io/pem/PemGenerationException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/io/IOException;

    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {v0}, Lorg/spongycastle/util/io/pem/PemGenerationException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 54
    :cond_0
    throw v0
.end method
