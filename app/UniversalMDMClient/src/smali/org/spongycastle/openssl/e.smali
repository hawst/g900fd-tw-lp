.class Lorg/spongycastle/openssl/e;
.super Ljava/lang/Object;
.source "PEMReader.java"

# interfaces
.implements Lorg/spongycastle/util/io/pem/d;


# instance fields
.field final synthetic afk:Lorg/spongycastle/openssl/b;


# direct methods
.method private constructor <init>(Lorg/spongycastle/openssl/b;)V
    .locals 0

    .prologue
    .line 620
    iput-object p1, p0, Lorg/spongycastle/openssl/e;->afk:Lorg/spongycastle/openssl/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/spongycastle/openssl/b;Lorg/spongycastle/openssl/b$1;)V
    .locals 0

    .prologue
    .line 620
    invoke-direct {p0, p1}, Lorg/spongycastle/openssl/e;-><init>(Lorg/spongycastle/openssl/b;)V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/io/pem/b;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 628
    :try_start_0
    invoke-virtual {p1}, Lorg/spongycastle/util/io/pem/b;->sN()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/bc;

    .line 630
    invoke-virtual {v0}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/jce/b;->cS(Ljava/lang/String;)Lorg/spongycastle/jce/spec/c;

    move-result-object v0

    .line 632
    if-nez v0, :cond_0

    .line 634
    new-instance v0, Ljava/io/IOException;

    const-string v1, "object ID not found in EC curve table"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 639
    :catch_0
    move-exception v0

    .line 641
    throw v0

    .line 643
    :catch_1
    move-exception v0

    .line 645
    new-instance v1, Lorg/spongycastle/openssl/PEMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception extracting EC named curve: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/openssl/PEMException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 637
    :cond_0
    return-object v0
.end method
