.class public Lorg/spongycastle/openssl/a;
.super Ljava/lang/Object;
.source "MiscPEMGenerator.java"

# interfaces
.implements Lorg/spongycastle/util/io/pem/c;


# static fields
.field private static final afg:[B


# instance fields
.field private afh:[C

.field private algorithm:Ljava/lang/String;

.field private obj:Ljava/lang/Object;

.field private provider:Ljava/security/Provider;

.field private random:Ljava/security/SecureRandom;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/spongycastle/openssl/a;->afg:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/spongycastle/openssl/a;->obj:Ljava/lang/Object;

    .line 62
    return-void
.end method

.method private S([B)Ljava/lang/String;
    .locals 6

    .prologue
    .line 217
    array-length v0, p1

    mul-int/lit8 v0, v0, 0x2

    new-array v1, v0, [C

    .line 219
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-eq v0, v2, :cond_0

    .line 221
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    .line 223
    mul-int/lit8 v3, v0, 0x2

    sget-object v4, Lorg/spongycastle/openssl/a;->afg:[B

    ushr-int/lit8 v5, v2, 0x4

    aget-byte v4, v4, v5

    int-to-char v4, v4

    aput-char v4, v1, v3

    .line 224
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    sget-object v4, Lorg/spongycastle/openssl/a;->afg:[B

    and-int/lit8 v2, v2, 0xf

    aget-byte v2, v4, v2

    int-to-char v2, v2

    aput-char v2, v1, v3

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/String;[CLjava/security/SecureRandom;)Lorg/spongycastle/util/io/pem/b;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 237
    instance-of v0, p1, Ljava/security/KeyPair;

    if-eqz v0, :cond_0

    .line 239
    check-cast p1, Ljava/security/KeyPair;

    invoke-virtual {p1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lorg/spongycastle/openssl/a;->a(Ljava/lang/Object;Ljava/lang/String;[CLjava/security/SecureRandom;)Lorg/spongycastle/util/io/pem/b;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    .line 245
    :cond_0
    instance-of v0, p1, Ljava/security/interfaces/RSAPrivateCrtKey;

    if-eqz v0, :cond_2

    .line 247
    const-string v9, "RSA PRIVATE KEY"

    move-object v8, p1

    .line 249
    check-cast v8, Ljava/security/interfaces/RSAPrivateCrtKey;

    .line 251
    new-instance v0, Lorg/spongycastle/asn1/l/v;

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v1

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v2

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v3

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeP()Ljava/math/BigInteger;

    move-result-object v4

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeQ()Ljava/math/BigInteger;

    move-result-object v5

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentP()Ljava/math/BigInteger;

    move-result-object v6

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentQ()Ljava/math/BigInteger;

    move-result-object v7

    invoke-interface {v8}, Ljava/security/interfaces/RSAPrivateCrtKey;->getCrtCoefficient()Ljava/math/BigInteger;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lorg/spongycastle/asn1/l/v;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 262
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/v;->getEncoded()[B

    move-result-object v2

    move-object v6, v9

    .line 294
    :goto_1
    if-eqz v6, :cond_1

    if-nez v2, :cond_4

    .line 297
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object type not supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_2
    instance-of v0, p1, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v0, :cond_3

    .line 266
    const-string v1, "DSA PRIVATE KEY"

    move-object v0, p1

    .line 268
    check-cast v0, Ljava/security/interfaces/DSAPrivateKey;

    .line 269
    invoke-interface {v0}, Ljava/security/interfaces/DSAPrivateKey;->getParams()Ljava/security/interfaces/DSAParams;

    move-result-object v2

    .line 270
    new-instance v3, Lorg/spongycastle/asn1/e;

    invoke-direct {v3}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 272
    new-instance v4, Lorg/spongycastle/asn1/az;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lorg/spongycastle/asn1/az;-><init>(I)V

    invoke-virtual {v3, v4}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 273
    new-instance v4, Lorg/spongycastle/asn1/az;

    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getP()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v3, v4}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 274
    new-instance v4, Lorg/spongycastle/asn1/az;

    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getQ()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v3, v4}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 275
    new-instance v4, Lorg/spongycastle/asn1/az;

    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getG()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v3, v4}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 277
    invoke-interface {v0}, Ljava/security/interfaces/DSAPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v0

    .line 278
    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getG()Ljava/math/BigInteger;

    move-result-object v4

    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    .line 280
    new-instance v4, Lorg/spongycastle/asn1/az;

    invoke-direct {v4, v2}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v3, v4}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 281
    new-instance v2, Lorg/spongycastle/asn1/az;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v3, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 283
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v3}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bh;->getEncoded()[B

    move-result-object v2

    move-object v6, v1

    .line 284
    goto/16 :goto_1

    .line 285
    :cond_3
    instance-of v0, p1, Ljava/security/PrivateKey;

    if-eqz v0, :cond_7

    const-string v1, "ECDSA"

    move-object v0, p1

    check-cast v0, Ljava/security/PrivateKey;

    invoke-interface {v0}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 287
    const-string v1, "EC PRIVATE KEY"

    move-object v0, p1

    .line 289
    check-cast v0, Ljava/security/PrivateKey;

    invoke-interface {v0}, Ljava/security/PrivateKey;->getEncoded()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/s;->ak(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/s;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Lorg/spongycastle/asn1/l/s;->nK()Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->getEncoded()[B

    move-result-object v2

    move-object v6, v1

    goto/16 :goto_1

    .line 300
    :cond_4
    invoke-static {p2}, Lorg/spongycastle/util/g;->cZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 303
    const-string v0, "DESEDE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 305
    const-string v4, "DES-EDE3-CBC"

    .line 308
    :cond_5
    const-string v0, "AES-"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x10

    .line 310
    :goto_2
    new-array v5, v0, [B

    .line 311
    invoke-virtual {p4, v5}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 313
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/openssl/a;->provider:Ljava/security/Provider;

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lorg/spongycastle/openssl/q;->a(ZLjava/security/Provider;[B[CLjava/lang/String;[B)[B

    move-result-object v1

    .line 315
    new-instance v2, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 317
    new-instance v0, Lorg/spongycastle/util/io/pem/a;

    const-string v3, "Proc-Type"

    const-string v7, "4,ENCRYPTED"

    invoke-direct {v0, v3, v7}, Lorg/spongycastle/util/io/pem/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    new-instance v0, Lorg/spongycastle/util/io/pem/a;

    const-string v3, "DEK-Info"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ","

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, v5}, Lorg/spongycastle/openssl/a;->S([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lorg/spongycastle/util/io/pem/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    new-instance v0, Lorg/spongycastle/util/io/pem/b;

    invoke-direct {v0, v6, v2, v1}, Lorg/spongycastle/util/io/pem/b;-><init>(Ljava/lang/String;Ljava/util/List;[B)V

    goto/16 :goto_0

    .line 308
    :cond_6
    const/16 v0, 0x8

    goto :goto_2

    :cond_7
    move-object v6, v2

    goto/16 :goto_1
.end method

.method private bo(Ljava/lang/Object;)Lorg/spongycastle/util/io/pem/b;
    .locals 5

    .prologue
    .line 107
    instance-of v0, p1, Lorg/spongycastle/util/io/pem/b;

    if-eqz v0, :cond_0

    .line 109
    check-cast p1, Lorg/spongycastle/util/io/pem/b;

    .line 211
    :goto_0
    return-object p1

    .line 111
    :cond_0
    instance-of v0, p1, Lorg/spongycastle/util/io/pem/c;

    if-eqz v0, :cond_1

    .line 113
    check-cast p1, Lorg/spongycastle/util/io/pem/c;

    invoke-interface {p1}, Lorg/spongycastle/util/io/pem/c;->sL()Lorg/spongycastle/util/io/pem/b;

    move-result-object p1

    goto :goto_0

    .line 115
    :cond_1
    instance-of v0, p1, Ljava/security/cert/X509Certificate;

    if-eqz v0, :cond_2

    .line 117
    const-string v1, "CERTIFICATE"

    .line 120
    :try_start_0
    check-cast p1, Ljava/security/cert/X509Certificate;

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getEncoded()[B
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 211
    :goto_1
    new-instance p1, Lorg/spongycastle/util/io/pem/b;

    invoke-direct {p1, v1, v0}, Lorg/spongycastle/util/io/pem/b;-><init>(Ljava/lang/String;[B)V

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 124
    new-instance v1, Lorg/spongycastle/util/io/pem/PemGenerationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot encode object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/security/cert/CertificateEncodingException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/util/io/pem/PemGenerationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 127
    :cond_2
    instance-of v0, p1, Ljava/security/cert/X509CRL;

    if-eqz v0, :cond_3

    .line 129
    const-string v1, "X509 CRL"

    .line 132
    :try_start_1
    check-cast p1, Ljava/security/cert/X509CRL;

    invoke-virtual {p1}, Ljava/security/cert/X509CRL;->getEncoded()[B
    :try_end_1
    .catch Ljava/security/cert/CRLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_1

    .line 134
    :catch_1
    move-exception v0

    .line 136
    new-instance v1, Lorg/spongycastle/util/io/pem/PemGenerationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot encode object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/security/cert/CRLException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/util/io/pem/PemGenerationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 139
    :cond_3
    instance-of v0, p1, Ljava/security/KeyPair;

    if-eqz v0, :cond_4

    .line 141
    check-cast p1, Ljava/security/KeyPair;

    invoke-virtual {p1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/spongycastle/openssl/a;->bo(Ljava/lang/Object;)Lorg/spongycastle/util/io/pem/b;

    move-result-object p1

    goto :goto_0

    .line 143
    :cond_4
    instance-of v0, p1, Ljava/security/PrivateKey;

    if-eqz v0, :cond_8

    .line 145
    new-instance v2, Lorg/spongycastle/asn1/l/s;

    move-object v0, p1

    check-cast v0, Ljava/security/Key;

    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/l/s;-><init>(Lorg/spongycastle/asn1/r;)V

    .line 148
    instance-of v0, p1, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v0, :cond_5

    .line 150
    const-string v1, "RSA PRIVATE KEY"

    .line 152
    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/s;->nK()Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->getEncoded()[B

    move-result-object v0

    goto/16 :goto_1

    .line 154
    :cond_5
    instance-of v0, p1, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v0, :cond_6

    .line 156
    const-string v1, "DSA PRIVATE KEY"

    .line 158
    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/s;->nI()Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/a;->oo()Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/o;->aJ(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/o;

    move-result-object v0

    .line 159
    new-instance v2, Lorg/spongycastle/asn1/e;

    invoke-direct {v2}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 161
    new-instance v3, Lorg/spongycastle/asn1/az;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lorg/spongycastle/asn1/az;-><init>(I)V

    invoke-virtual {v2, v3}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 162
    new-instance v3, Lorg/spongycastle/asn1/az;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/o;->getP()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v2, v3}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 163
    new-instance v3, Lorg/spongycastle/asn1/az;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/o;->getQ()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v2, v3}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 164
    new-instance v3, Lorg/spongycastle/asn1/az;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/o;->getG()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v2, v3}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 166
    check-cast p1, Ljava/security/interfaces/DSAPrivateKey;

    invoke-interface {p1}, Ljava/security/interfaces/DSAPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v3

    .line 167
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/o;->getG()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/o;->getP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v4, v3, v0}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 169
    new-instance v4, Lorg/spongycastle/asn1/az;

    invoke-direct {v4, v0}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v2, v4}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 170
    new-instance v0, Lorg/spongycastle/asn1/az;

    invoke-direct {v0, v3}, Lorg/spongycastle/asn1/az;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v2, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 172
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v2}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bh;->getEncoded()[B

    move-result-object v0

    goto/16 :goto_1

    .line 174
    :cond_6
    check-cast p1, Ljava/security/PrivateKey;

    invoke-interface {p1}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ECDSA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 176
    const-string v1, "EC PRIVATE KEY"

    .line 178
    invoke-virtual {v2}, Lorg/spongycastle/asn1/l/s;->nK()Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->getEncoded()[B

    move-result-object v0

    goto/16 :goto_1

    .line 182
    :cond_7
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cannot identify private key"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_8
    instance-of v0, p1, Ljava/security/PublicKey;

    if-eqz v0, :cond_9

    .line 187
    const-string v1, "PUBLIC KEY"

    .line 189
    check-cast p1, Ljava/security/PublicKey;

    invoke-interface {p1}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v0

    goto/16 :goto_1

    .line 191
    :cond_9
    instance-of v0, p1, Lorg/spongycastle/x509/h;

    if-eqz v0, :cond_a

    .line 193
    const-string v1, "ATTRIBUTE CERTIFICATE"

    .line 194
    check-cast p1, Lorg/spongycastle/x509/t;

    invoke-virtual {p1}, Lorg/spongycastle/x509/t;->getEncoded()[B

    move-result-object v0

    goto/16 :goto_1

    .line 196
    :cond_a
    instance-of v0, p1, Lorg/spongycastle/jce/d;

    if-eqz v0, :cond_b

    .line 198
    const-string v1, "CERTIFICATE REQUEST"

    .line 199
    check-cast p1, Lorg/spongycastle/jce/d;

    invoke-virtual {p1}, Lorg/spongycastle/jce/d;->getEncoded()[B

    move-result-object v0

    goto/16 :goto_1

    .line 201
    :cond_b
    instance-of v0, p1, Lorg/spongycastle/asn1/b/a;

    if-eqz v0, :cond_c

    .line 203
    const-string v1, "PKCS7"

    .line 204
    check-cast p1, Lorg/spongycastle/asn1/b/a;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/b/a;->getEncoded()[B

    move-result-object v0

    goto/16 :goto_1

    .line 208
    :cond_c
    new-instance v0, Lorg/spongycastle/util/io/pem/PemGenerationException;

    const-string v1, "unknown object passed - can\'t encode."

    invoke-direct {v0, v1}, Lorg/spongycastle/util/io/pem/PemGenerationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public sL()Lorg/spongycastle/util/io/pem/b;
    .locals 4

    .prologue
    .line 328
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/openssl/a;->algorithm:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lorg/spongycastle/openssl/a;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lorg/spongycastle/openssl/a;->algorithm:Ljava/lang/String;

    iget-object v2, p0, Lorg/spongycastle/openssl/a;->afh:[C

    iget-object v3, p0, Lorg/spongycastle/openssl/a;->random:Ljava/security/SecureRandom;

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/spongycastle/openssl/a;->a(Ljava/lang/Object;Ljava/lang/String;[CLjava/security/SecureRandom;)Lorg/spongycastle/util/io/pem/b;

    move-result-object v0

    .line 333
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/openssl/a;->obj:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lorg/spongycastle/openssl/a;->bo(Ljava/lang/Object;)Lorg/spongycastle/util/io/pem/b;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 335
    :catch_0
    move-exception v0

    .line 337
    new-instance v1, Lorg/spongycastle/util/io/pem/PemGenerationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "encoding exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/util/io/pem/PemGenerationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
