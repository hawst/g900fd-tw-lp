.class public Lorg/spongycastle/asn1/bu;
.super Lorg/spongycastle/asn1/x;
.source "DLTaggedObject.java"


# static fields
.field private static final GI:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/spongycastle/asn1/bu;->GI:[B

    return-void
.end method

.method public constructor <init>(ZILorg/spongycastle/asn1/d;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lorg/spongycastle/asn1/x;-><init>(ZILorg/spongycastle/asn1/d;)V

    .line 26
    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    .prologue
    const/16 v0, 0xa0

    .line 78
    iget-boolean v1, p0, Lorg/spongycastle/asn1/bu;->FU:Z

    if-nez v1, :cond_2

    .line 80
    iget-object v1, p0, Lorg/spongycastle/asn1/bu;->FW:Lorg/spongycastle/asn1/d;

    invoke-interface {v1}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->mK()Lorg/spongycastle/asn1/q;

    move-result-object v1

    .line 82
    iget-boolean v2, p0, Lorg/spongycastle/asn1/bu;->FV:Z

    if-eqz v2, :cond_0

    .line 84
    iget v2, p0, Lorg/spongycastle/asn1/bu;->FT:I

    invoke-virtual {p1, v0, v2}, Lorg/spongycastle/asn1/o;->k(II)V

    .line 85
    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->mO()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->bF(I)V

    .line 86
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/o;->b(Lorg/spongycastle/asn1/d;)V

    .line 111
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->mN()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    :goto_1
    iget v2, p0, Lorg/spongycastle/asn1/bu;->FT:I

    invoke-virtual {p1, v0, v2}, Lorg/spongycastle/asn1/o;->k(II)V

    .line 104
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/o;->b(Lorg/spongycastle/asn1/q;)V

    goto :goto_0

    .line 100
    :cond_1
    const/16 v0, 0x80

    goto :goto_1

    .line 109
    :cond_2
    iget v1, p0, Lorg/spongycastle/asn1/bu;->FT:I

    sget-object v2, Lorg/spongycastle/asn1/bu;->GI:[B

    invoke-virtual {p1, v0, v1, v2}, Lorg/spongycastle/asn1/o;->a(II[B)V

    goto :goto_0
.end method

.method mN()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 30
    iget-boolean v1, p0, Lorg/spongycastle/asn1/bu;->FU:Z

    if-nez v1, :cond_0

    .line 32
    iget-boolean v1, p0, Lorg/spongycastle/asn1/bu;->FV:Z

    if-eqz v1, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v0

    .line 38
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/asn1/bu;->FW:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->mK()Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->mN()Z

    move-result v0

    goto :goto_0
.end method

.method mO()I
    .locals 3

    .prologue
    .line 52
    iget-boolean v0, p0, Lorg/spongycastle/asn1/bu;->FU:Z

    if-nez v0, :cond_1

    .line 54
    iget-object v0, p0, Lorg/spongycastle/asn1/bu;->FW:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->mK()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->mO()I

    move-result v0

    .line 56
    iget-boolean v1, p0, Lorg/spongycastle/asn1/bu;->FV:Z

    if-eqz v1, :cond_0

    .line 58
    iget v1, p0, Lorg/spongycastle/asn1/bu;->FT:I

    invoke-static {v1}, Lorg/spongycastle/asn1/cc;->bM(I)I

    move-result v1

    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bL(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 70
    :goto_0
    return v0

    .line 63
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 65
    iget v1, p0, Lorg/spongycastle/asn1/bu;->FT:I

    invoke-static {v1}, Lorg/spongycastle/asn1/cc;->bM(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 70
    :cond_1
    iget v0, p0, Lorg/spongycastle/asn1/bu;->FT:I

    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bM(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
