.class public Lorg/spongycastle/asn1/v;
.super Ljava/lang/Object;
.source "ASN1StreamParser.java"


# instance fields
.field private final FK:[[B

.field private final FS:I

.field private final _in:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 16
    invoke-static {p1}, Lorg/spongycastle/asn1/cc;->d(Ljava/io/InputStream;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;I)V

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    .line 24
    iput p2, p0, Lorg/spongycastle/asn1/v;->FS:I

    .line 26
    const/16 v0, 0xb

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/spongycastle/asn1/v;->FK:[[B

    .line 27
    return-void
.end method

.method private R(Z)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    instance-of v0, v0, Lorg/spongycastle/asn1/bx;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    check-cast v0, Lorg/spongycastle/asn1/bx;

    invoke-virtual {v0, p1}, Lorg/spongycastle/asn1/bx;->T(Z)V

    .line 226
    :cond_0
    return-void
.end method


# virtual methods
.method bH(I)Lorg/spongycastle/asn1/d;
    .locals 3

    .prologue
    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 51
    new-instance v0, Lorg/spongycastle/asn1/ASN1Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown BER object encountered: 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :sswitch_0
    new-instance v0, Lorg/spongycastle/asn1/au;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/au;-><init>(Lorg/spongycastle/asn1/v;)V

    .line 49
    :goto_0
    return-object v0

    .line 45
    :sswitch_1
    new-instance v0, Lorg/spongycastle/asn1/af;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/af;-><init>(Lorg/spongycastle/asn1/v;)V

    goto :goto_0

    .line 47
    :sswitch_2
    new-instance v0, Lorg/spongycastle/asn1/ai;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/ai;-><init>(Lorg/spongycastle/asn1/v;)V

    goto :goto_0

    .line 49
    :sswitch_3
    new-instance v0, Lorg/spongycastle/asn1/ak;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/ak;-><init>(Lorg/spongycastle/asn1/v;)V

    goto :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_3
    .end sparse-switch
.end method

.method f(ZI)Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 98
    if-nez p1, :cond_0

    .line 101
    iget-object v0, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    check-cast v0, Lorg/spongycastle/asn1/bv;

    .line 102
    new-instance v1, Lorg/spongycastle/asn1/bm;

    new-instance v2, Lorg/spongycastle/asn1/bd;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bv;->toByteArray()[B

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-direct {v1, v3, p2, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    move-object v0, v1

    .line 114
    :goto_0
    return-object v0

    .line 105
    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/v;->mS()Lorg/spongycastle/asn1/e;

    move-result-object v1

    .line 107
    iget-object v0, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    instance-of v0, v0, Lorg/spongycastle/asn1/bx;

    if-eqz v0, :cond_2

    .line 109
    invoke-virtual {v1}, Lorg/spongycastle/asn1/e;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    new-instance v0, Lorg/spongycastle/asn1/al;

    invoke-virtual {v1, v3}, Lorg/spongycastle/asn1/e;->bE(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-direct {v0, v2, p2, v1}, Lorg/spongycastle/asn1/al;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/al;

    invoke-static {v1}, Lorg/spongycastle/asn1/ad;->a(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/ah;

    move-result-object v1

    invoke-direct {v0, v3, p2, v1}, Lorg/spongycastle/asn1/al;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 114
    :cond_2
    invoke-virtual {v1}, Lorg/spongycastle/asn1/e;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    new-instance v0, Lorg/spongycastle/asn1/bm;

    invoke-virtual {v1, v3}, Lorg/spongycastle/asn1/e;->bE(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-direct {v0, v2, p2, v1}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lorg/spongycastle/asn1/bm;

    invoke-static {v1}, Lorg/spongycastle/asn1/av;->b(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v3, p2, v1}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method public mR()Lorg/spongycastle/asn1/d;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 122
    iget-object v2, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 123
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 125
    const/4 v0, 0x0

    .line 211
    :goto_0
    return-object v0

    .line 131
    :cond_0
    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/v;->R(Z)V

    .line 136
    iget-object v3, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    invoke-static {v3, v2}, Lorg/spongycastle/asn1/h;->b(Ljava/io/InputStream;I)I

    move-result v3

    .line 138
    and-int/lit8 v4, v2, 0x20

    if-eqz v4, :cond_1

    move v0, v1

    .line 143
    :cond_1
    iget-object v4, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    iget v5, p0, Lorg/spongycastle/asn1/v;->FS:I

    invoke-static {v4, v5}, Lorg/spongycastle/asn1/h;->c(Ljava/io/InputStream;I)I

    move-result v4

    .line 145
    if-gez v4, :cond_5

    .line 147
    if-nez v0, :cond_2

    .line 149
    new-instance v0, Ljava/io/IOException;

    const-string v1, "indefinite length primitive encoding encountered"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_2
    new-instance v0, Lorg/spongycastle/asn1/bx;

    iget-object v4, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    iget v5, p0, Lorg/spongycastle/asn1/v;->FS:I

    invoke-direct {v0, v4, v5}, Lorg/spongycastle/asn1/bx;-><init>(Ljava/io/InputStream;I)V

    .line 153
    new-instance v4, Lorg/spongycastle/asn1/v;

    iget v5, p0, Lorg/spongycastle/asn1/v;->FS:I

    invoke-direct {v4, v0, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;I)V

    .line 155
    and-int/lit8 v0, v2, 0x40

    if-eqz v0, :cond_3

    .line 157
    new-instance v0, Lorg/spongycastle/asn1/ab;

    invoke-direct {v0, v3, v4}, Lorg/spongycastle/asn1/ab;-><init>(ILorg/spongycastle/asn1/v;)V

    goto :goto_0

    .line 160
    :cond_3
    and-int/lit16 v0, v2, 0x80

    if-eqz v0, :cond_4

    .line 162
    new-instance v0, Lorg/spongycastle/asn1/am;

    invoke-direct {v0, v1, v3, v4}, Lorg/spongycastle/asn1/am;-><init>(ZILorg/spongycastle/asn1/v;)V

    goto :goto_0

    .line 165
    :cond_4
    invoke-virtual {v4, v3}, Lorg/spongycastle/asn1/v;->bH(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    goto :goto_0

    .line 169
    :cond_5
    new-instance v5, Lorg/spongycastle/asn1/bv;

    iget-object v1, p0, Lorg/spongycastle/asn1/v;->_in:Ljava/io/InputStream;

    invoke-direct {v5, v1, v4}, Lorg/spongycastle/asn1/bv;-><init>(Ljava/io/InputStream;I)V

    .line 171
    and-int/lit8 v1, v2, 0x40

    if-eqz v1, :cond_6

    .line 173
    new-instance v1, Lorg/spongycastle/asn1/ao;

    invoke-virtual {v5}, Lorg/spongycastle/asn1/bv;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v0, v3, v2}, Lorg/spongycastle/asn1/ao;-><init>(ZI[B)V

    move-object v0, v1

    goto :goto_0

    .line 176
    :cond_6
    and-int/lit16 v1, v2, 0x80

    if-eqz v1, :cond_7

    .line 178
    new-instance v1, Lorg/spongycastle/asn1/am;

    new-instance v2, Lorg/spongycastle/asn1/v;

    invoke-direct {v2, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0, v3, v2}, Lorg/spongycastle/asn1/am;-><init>(ZILorg/spongycastle/asn1/v;)V

    move-object v0, v1

    goto :goto_0

    .line 181
    :cond_7
    if-eqz v0, :cond_8

    .line 184
    sparse-switch v3, :sswitch_data_0

    .line 198
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " encountered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :sswitch_0
    new-instance v0, Lorg/spongycastle/asn1/af;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/af;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    .line 192
    :sswitch_1
    new-instance v0, Lorg/spongycastle/asn1/bi;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bi;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    .line 194
    :sswitch_2
    new-instance v0, Lorg/spongycastle/asn1/bk;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bk;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    .line 196
    :sswitch_3
    new-instance v0, Lorg/spongycastle/asn1/au;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/au;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    .line 203
    :cond_8
    packed-switch v3, :pswitch_data_0

    .line 211
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/asn1/v;->FK:[[B

    invoke-static {v3, v5, v0}, Lorg/spongycastle/asn1/h;->a(ILorg/spongycastle/asn1/bv;[[B)Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 206
    :pswitch_0
    new-instance v0, Lorg/spongycastle/asn1/be;

    invoke-direct {v0, v5}, Lorg/spongycastle/asn1/be;-><init>(Lorg/spongycastle/asn1/bv;)V

    goto/16 :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 215
    new-instance v1, Lorg/spongycastle/asn1/ASN1Exception;

    const-string v2, "corrupted stream detected"

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 184
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_3
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch

    .line 203
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method mS()Lorg/spongycastle/asn1/e;
    .locals 3

    .prologue
    .line 230
    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 233
    :goto_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/v;->mR()Lorg/spongycastle/asn1/d;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 235
    instance-of v2, v0, Lorg/spongycastle/asn1/bw;

    if-eqz v2, :cond_0

    .line 237
    check-cast v0, Lorg/spongycastle/asn1/bw;

    invoke-interface {v0}, Lorg/spongycastle/asn1/bw;->mI()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 241
    :cond_0
    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 245
    :cond_1
    return-object v1
.end method
