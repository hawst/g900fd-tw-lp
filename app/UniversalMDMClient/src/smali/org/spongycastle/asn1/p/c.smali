.class public Lorg/spongycastle/asn1/p/c;
.super Lorg/spongycastle/asn1/k;
.source "X500Name.java"

# interfaces
.implements Lorg/spongycastle/asn1/c;


# static fields
.field private static Qb:Lorg/spongycastle/asn1/p/e;


# instance fields
.field private Qc:Z

.field private Qd:I

.field private Qe:Lorg/spongycastle/asn1/p/e;

.field private Qf:[Lorg/spongycastle/asn1/p/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lorg/spongycastle/asn1/p/a/a;->Qi:Lorg/spongycastle/asn1/p/e;

    sput-object v0, Lorg/spongycastle/asn1/p/c;->Qb:Lorg/spongycastle/asn1/p/e;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lorg/spongycastle/asn1/p/c;->Qb:Lorg/spongycastle/asn1/p/e;

    invoke-direct {p0, v0, p1}, Lorg/spongycastle/asn1/p/c;-><init>(Lorg/spongycastle/asn1/p/e;Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/p/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 143
    invoke-interface {p1, p2}, Lorg/spongycastle/asn1/p/e;->cK(Ljava/lang/String;)[Lorg/spongycastle/asn1/p/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/p/c;-><init>([Lorg/spongycastle/asn1/p/b;)V

    .line 145
    iput-object p1, p0, Lorg/spongycastle/asn1/p/c;->Qe:Lorg/spongycastle/asn1/p/e;

    .line 146
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/p/e;Lorg/spongycastle/asn1/r;)V
    .locals 5

    .prologue
    .line 107
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 108
    iput-object p1, p0, Lorg/spongycastle/asn1/p/c;->Qe:Lorg/spongycastle/asn1/p/e;

    .line 109
    invoke-virtual {p2}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    new-array v0, v0, [Lorg/spongycastle/asn1/p/b;

    iput-object v0, p0, Lorg/spongycastle/asn1/p/c;->Qf:[Lorg/spongycastle/asn1/p/b;

    .line 111
    const/4 v0, 0x0

    .line 113
    invoke-virtual {p2}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v3, p0, Lorg/spongycastle/asn1/p/c;->Qf:[Lorg/spongycastle/asn1/p/b;

    add-int/lit8 v1, v0, 0x1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lorg/spongycastle/asn1/p/b;->av(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/b;

    move-result-object v4

    aput-object v4, v3, v0

    move v0, v1

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/p/e;[Lorg/spongycastle/asn1/p/b;)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 129
    iput-object p2, p0, Lorg/spongycastle/asn1/p/c;->Qf:[Lorg/spongycastle/asn1/p/b;

    .line 130
    iput-object p1, p0, Lorg/spongycastle/asn1/p/c;->Qe:Lorg/spongycastle/asn1/p/e;

    .line 131
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lorg/spongycastle/asn1/p/c;->Qb:Lorg/spongycastle/asn1/p/e;

    invoke-direct {p0, v0, p1}, Lorg/spongycastle/asn1/p/c;-><init>(Lorg/spongycastle/asn1/p/e;Lorg/spongycastle/asn1/r;)V

    .line 102
    return-void
.end method

.method public constructor <init>([Lorg/spongycastle/asn1/p/b;)V
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lorg/spongycastle/asn1/p/c;->Qb:Lorg/spongycastle/asn1/p/e;

    invoke-direct {p0, v0, p1}, Lorg/spongycastle/asn1/p/c;-><init>(Lorg/spongycastle/asn1/p/e;[Lorg/spongycastle/asn1/p/b;)V

    .line 123
    return-void
.end method

.method public static aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;
    .locals 2

    .prologue
    .line 65
    instance-of v0, p0, Lorg/spongycastle/asn1/p/c;

    if-eqz v0, :cond_0

    .line 67
    check-cast p0, Lorg/spongycastle/asn1/p/c;

    .line 74
    :goto_0
    return-object p0

    .line 69
    :cond_0
    if-eqz p0, :cond_1

    .line 71
    new-instance v0, Lorg/spongycastle/asn1/p/c;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/p/c;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 74
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static j(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 270
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 293
    :goto_0
    return v0

    .line 275
    :cond_0
    instance-of v0, p1, Lorg/spongycastle/asn1/p/c;

    if-nez v0, :cond_1

    instance-of v0, p1, Lorg/spongycastle/asn1/r;

    if-nez v0, :cond_1

    move v0, v2

    .line 277
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 280
    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 282
    invoke-virtual {p0}, Lorg/spongycastle/asn1/p/c;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v3

    invoke-virtual {v3, v0}, Lorg/spongycastle/asn1/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 284
    goto :goto_0

    .line 289
    :cond_2
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/asn1/p/c;->Qe:Lorg/spongycastle/asn1/p/e;

    new-instance v1, Lorg/spongycastle/asn1/p/c;

    check-cast p1, Lorg/spongycastle/asn1/d;

    invoke-interface {p1}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v3

    invoke-static {v3}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/spongycastle/asn1/p/c;-><init>(Lorg/spongycastle/asn1/r;)V

    invoke-interface {v0, p0, v1}, Lorg/spongycastle/asn1/p/e;->a(Lorg/spongycastle/asn1/p/c;Lorg/spongycastle/asn1/p/c;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    move v0, v2

    .line 293
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lorg/spongycastle/asn1/p/c;->Qc:Z

    if-eqz v0, :cond_0

    .line 255
    iget v0, p0, Lorg/spongycastle/asn1/p/c;->Qd:I

    .line 262
    :goto_0
    return v0

    .line 258
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/p/c;->Qc:Z

    .line 260
    iget-object v0, p0, Lorg/spongycastle/asn1/p/c;->Qe:Lorg/spongycastle/asn1/p/e;

    invoke-interface {v0, p0}, Lorg/spongycastle/asn1/p/e;->a(Lorg/spongycastle/asn1/p/c;)I

    move-result v0

    iput v0, p0, Lorg/spongycastle/asn1/p/c;->Qd:I

    .line 262
    iget v0, p0, Lorg/spongycastle/asn1/p/c;->Qd:I

    goto :goto_0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 248
    new-instance v0, Lorg/spongycastle/asn1/bh;

    iget-object v1, p0, Lorg/spongycastle/asn1/p/c;->Qf:[Lorg/spongycastle/asn1/p/b;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bh;-><init>([Lorg/spongycastle/asn1/d;)V

    return-object v0
.end method

.method public ok()[Lorg/spongycastle/asn1/p/b;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 155
    iget-object v0, p0, Lorg/spongycastle/asn1/p/c;->Qf:[Lorg/spongycastle/asn1/p/b;

    array-length v0, v0

    new-array v0, v0, [Lorg/spongycastle/asn1/p/b;

    .line 157
    iget-object v1, p0, Lorg/spongycastle/asn1/p/c;->Qf:[Lorg/spongycastle/asn1/p/b;

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 159
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lorg/spongycastle/asn1/p/c;->Qe:Lorg/spongycastle/asn1/p/e;

    invoke-interface {v0, p0}, Lorg/spongycastle/asn1/p/e;->b(Lorg/spongycastle/asn1/p/c;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
