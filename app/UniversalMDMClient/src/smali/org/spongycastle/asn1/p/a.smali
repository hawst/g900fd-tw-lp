.class public Lorg/spongycastle/asn1/p/a;
.super Lorg/spongycastle/asn1/k;
.source "AttributeTypeAndValue.java"


# instance fields
.field private PY:Lorg/spongycastle/asn1/l;

.field private PZ:Lorg/spongycastle/asn1/d;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/spongycastle/asn1/p/a;->PY:Lorg/spongycastle/asn1/l;

    .line 42
    iput-object p2, p0, Lorg/spongycastle/asn1/p/a;->PZ:Lorg/spongycastle/asn1/d;

    .line 43
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 19
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/p/a;->PY:Lorg/spongycastle/asn1/l;

    .line 20
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/p/a;->PZ:Lorg/spongycastle/asn1/d;

    .line 21
    return-void
.end method

.method public static au(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/a;
    .locals 2

    .prologue
    .line 25
    instance-of v0, p0, Lorg/spongycastle/asn1/p/a;

    if-eqz v0, :cond_0

    .line 27
    check-cast p0, Lorg/spongycastle/asn1/p/a;

    .line 31
    :goto_0
    return-object p0

    .line 29
    :cond_0
    if-eqz p0, :cond_1

    .line 31
    new-instance v0, Lorg/spongycastle/asn1/p/a;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/p/a;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null value in getInstance()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 67
    iget-object v1, p0, Lorg/spongycastle/asn1/p/a;->PY:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 68
    iget-object v1, p0, Lorg/spongycastle/asn1/p/a;->PZ:Lorg/spongycastle/asn1/d;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 70
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public of()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/spongycastle/asn1/p/a;->PY:Lorg/spongycastle/asn1/l;

    return-object v0
.end method

.method public og()Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/spongycastle/asn1/p/a;->PZ:Lorg/spongycastle/asn1/d;

    return-object v0
.end method
