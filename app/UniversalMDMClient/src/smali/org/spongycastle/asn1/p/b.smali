.class public Lorg/spongycastle/asn1/p/b;
.super Lorg/spongycastle/asn1/k;
.source "RDN.java"


# instance fields
.field private Qa:Lorg/spongycastle/asn1/t;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 44
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 46
    invoke-virtual {v0, p1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 47
    invoke-virtual {v0, p2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 49
    new-instance v1, Lorg/spongycastle/asn1/bj;

    new-instance v2, Lorg/spongycastle/asn1/bh;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bj;-><init>(Lorg/spongycastle/asn1/d;)V

    iput-object v1, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    .line 50
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/t;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 19
    iput-object p1, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    .line 20
    return-void
.end method

.method public constructor <init>([Lorg/spongycastle/asn1/p/a;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 64
    new-instance v0, Lorg/spongycastle/asn1/bj;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/bj;-><init>([Lorg/spongycastle/asn1/d;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    .line 65
    return-void
.end method

.method public static av(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/b;
    .locals 2

    .prologue
    .line 24
    instance-of v0, p0, Lorg/spongycastle/asn1/p/b;

    if-eqz v0, :cond_0

    .line 26
    check-cast p0, Lorg/spongycastle/asn1/p/b;

    .line 33
    :goto_0
    return-object p0

    .line 28
    :cond_0
    if-eqz p0, :cond_1

    .line 30
    new-instance v0, Lorg/spongycastle/asn1/p/b;

    invoke-static {p0}, Lorg/spongycastle/asn1/t;->L(Ljava/lang/Object;)Lorg/spongycastle/asn1/t;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/p/b;-><init>(Lorg/spongycastle/asn1/t;)V

    move-object p0, v0

    goto :goto_0

    .line 33
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    return-object v0
.end method

.method public oh()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 69
    iget-object v1, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/t;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public oi()Lorg/spongycastle/asn1/p/a;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/t;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/t;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/a;->au(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/a;

    move-result-object v0

    goto :goto_0
.end method

.method public oj()[Lorg/spongycastle/asn1/p/a;
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/t;->size()I

    move-result v0

    new-array v1, v0, [Lorg/spongycastle/asn1/p/a;

    .line 96
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-eq v0, v2, :cond_0

    .line 98
    iget-object v2, p0, Lorg/spongycastle/asn1/p/b;->Qa:Lorg/spongycastle/asn1/t;

    invoke-virtual {v2, v0}, Lorg/spongycastle/asn1/t;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/p/a;->au(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/a;

    move-result-object v2

    aput-object v2, v1, v0

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    return-object v1
.end method
