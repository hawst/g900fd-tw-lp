.class public Lorg/spongycastle/asn1/p/d;
.super Ljava/lang/Object;
.source "X500NameBuilder.java"


# instance fields
.field private Qg:Lorg/spongycastle/asn1/p/e;

.field private Qh:Ljava/util/Vector;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/p/e;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/p/d;->Qh:Ljava/util/Vector;

    .line 15
    iput-object p1, p0, Lorg/spongycastle/asn1/p/d;->Qg:Lorg/spongycastle/asn1/p/e;

    .line 16
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/asn1/l;Ljava/lang/String;)Lorg/spongycastle/asn1/p/d;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/spongycastle/asn1/p/d;->Qg:Lorg/spongycastle/asn1/p/e;

    invoke-interface {v0, p1, p2}, Lorg/spongycastle/asn1/p/e;->b(Lorg/spongycastle/asn1/l;Ljava/lang/String;)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/spongycastle/asn1/p/d;->a(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)Lorg/spongycastle/asn1/p/d;

    .line 22
    return-object p0
.end method

.method public a(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)Lorg/spongycastle/asn1/p/d;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lorg/spongycastle/asn1/p/d;->Qh:Ljava/util/Vector;

    new-instance v1, Lorg/spongycastle/asn1/p/b;

    invoke-direct {v1, p1, p2}, Lorg/spongycastle/asn1/p/b;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 29
    return-object p0
.end method

.method public a([Lorg/spongycastle/asn1/l;[Ljava/lang/String;)Lorg/spongycastle/asn1/p/d;
    .locals 5

    .prologue
    .line 41
    array-length v0, p2

    new-array v1, v0, [Lorg/spongycastle/asn1/d;

    .line 43
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-eq v0, v2, :cond_0

    .line 45
    iget-object v2, p0, Lorg/spongycastle/asn1/p/d;->Qg:Lorg/spongycastle/asn1/p/e;

    aget-object v3, p1, v0

    aget-object v4, p2, v0

    invoke-interface {v2, v3, v4}, Lorg/spongycastle/asn1/p/e;->b(Lorg/spongycastle/asn1/l;Ljava/lang/String;)Lorg/spongycastle/asn1/d;

    move-result-object v2

    aput-object v2, v1, v0

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0, p1, v1}, Lorg/spongycastle/asn1/p/d;->a([Lorg/spongycastle/asn1/l;[Lorg/spongycastle/asn1/d;)Lorg/spongycastle/asn1/p/d;

    move-result-object v0

    return-object v0
.end method

.method public a([Lorg/spongycastle/asn1/l;[Lorg/spongycastle/asn1/d;)Lorg/spongycastle/asn1/p/d;
    .locals 5

    .prologue
    .line 53
    array-length v0, p1

    new-array v1, v0, [Lorg/spongycastle/asn1/p/a;

    .line 55
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-eq v0, v2, :cond_0

    .line 57
    new-instance v2, Lorg/spongycastle/asn1/p/a;

    aget-object v3, p1, v0

    aget-object v4, p2, v0

    invoke-direct {v2, v3, v4}, Lorg/spongycastle/asn1/p/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    aput-object v2, v1, v0

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p0, v1}, Lorg/spongycastle/asn1/p/d;->a([Lorg/spongycastle/asn1/p/a;)Lorg/spongycastle/asn1/p/d;

    move-result-object v0

    return-object v0
.end method

.method public a([Lorg/spongycastle/asn1/p/a;)Lorg/spongycastle/asn1/p/d;
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lorg/spongycastle/asn1/p/d;->Qh:Ljava/util/Vector;

    new-instance v1, Lorg/spongycastle/asn1/p/b;

    invoke-direct {v1, p1}, Lorg/spongycastle/asn1/p/b;-><init>([Lorg/spongycastle/asn1/p/a;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 67
    return-object p0
.end method

.method public ol()Lorg/spongycastle/asn1/p/c;
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lorg/spongycastle/asn1/p/d;->Qh:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v2, v0, [Lorg/spongycastle/asn1/p/b;

    .line 74
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-eq v1, v0, :cond_0

    .line 76
    iget-object v0, p0, Lorg/spongycastle/asn1/p/d;->Qh:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/p/b;

    aput-object v0, v2, v1

    .line 74
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 79
    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/p/c;

    iget-object v1, p0, Lorg/spongycastle/asn1/p/d;->Qg:Lorg/spongycastle/asn1/p/e;

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/p/c;-><init>(Lorg/spongycastle/asn1/p/e;[Lorg/spongycastle/asn1/p/b;)V

    return-object v0
.end method
