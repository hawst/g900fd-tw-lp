.class public Lorg/spongycastle/asn1/ao;
.super Lorg/spongycastle/asn1/q;
.source "DERApplicationSpecific.java"


# instance fields
.field private final Gj:Z

.field private final Gk:[B

.field private final tag:I


# direct methods
.method public constructor <init>(ILorg/spongycastle/asn1/e;)V
    .locals 4

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 71
    iput p1, p0, Lorg/spongycastle/asn1/ao;->tag:I

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/ao;->Gj:Z

    .line 73
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 75
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Lorg/spongycastle/asn1/e;->size()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 79
    :try_start_0
    invoke-virtual {p2, v1}, Lorg/spongycastle/asn1/e;->bE(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/k;

    const-string v3, "DER"

    invoke-virtual {v0, v3}, Lorg/spongycastle/asn1/k;->getEncoded(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 83
    new-instance v1, Lorg/spongycastle/asn1/ASN1ParsingException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "malformed object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/ASN1ParsingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 86
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    .line 87
    return-void
.end method

.method constructor <init>(ZI[B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 23
    iput-boolean p1, p0, Lorg/spongycastle/asn1/ao;->Gj:Z

    .line 24
    iput p2, p0, Lorg/spongycastle/asn1/ao;->tag:I

    .line 25
    iput-object p3, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    .line 26
    return-void
.end method

.method private c(I[B)[B
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 240
    aget-byte v0, p2, v4

    and-int/lit8 v0, v0, 0x1f

    .line 245
    const/16 v2, 0x1f

    if-ne v0, v2, :cond_2

    .line 249
    const/4 v2, 0x2

    aget-byte v0, p2, v1

    and-int/lit16 v0, v0, 0xff

    .line 253
    and-int/lit8 v3, v0, 0x7f

    if-nez v3, :cond_1

    .line 255
    new-instance v0, Lorg/spongycastle/asn1/ASN1ParsingException;

    const-string v1, "corrupted stream - invalid high tag number found"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ASN1ParsingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :goto_0
    if-ltz v2, :cond_0

    and-int/lit16 v5, v2, 0x80

    if-eqz v5, :cond_0

    .line 260
    and-int/lit8 v2, v2, 0x7f

    or-int/2addr v2, v3

    .line 261
    shl-int/lit8 v3, v2, 0x7

    .line 262
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p2, v0

    and-int/lit16 v0, v0, 0xff

    move v6, v0

    move v0, v2

    move v2, v6

    goto :goto_0

    .line 265
    :cond_0
    and-int/lit8 v2, v2, 0x7f

    or-int/2addr v2, v3

    .line 268
    :goto_1
    array-length v2, p2

    sub-int/2addr v2, v0

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [B

    .line 270
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    invoke-static {p2, v0, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 272
    int-to-byte v0, p1

    aput-byte v0, v2, v4

    .line 274
    return-object v2

    :cond_1
    move v3, v4

    move v6, v2

    move v2, v0

    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    .prologue
    .line 208
    const/16 v0, 0x40

    .line 209
    iget-boolean v1, p0, Lorg/spongycastle/asn1/ao;->Gj:Z

    if-eqz v1, :cond_0

    .line 211
    const/16 v0, 0x60

    .line 214
    :cond_0
    iget v1, p0, Lorg/spongycastle/asn1/ao;->tag:I

    iget-object v2, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    invoke-virtual {p1, v0, v1, v2}, Lorg/spongycastle/asn1/o;->a(II[B)V

    .line 215
    return-void
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 220
    instance-of v1, p1, Lorg/spongycastle/asn1/ao;

    if-nez v1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v0

    .line 225
    :cond_1
    check-cast p1, Lorg/spongycastle/asn1/ao;

    .line 227
    iget-boolean v1, p0, Lorg/spongycastle/asn1/ao;->Gj:Z

    iget-boolean v2, p1, Lorg/spongycastle/asn1/ao;->Gj:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lorg/spongycastle/asn1/ao;->tag:I

    iget v2, p1, Lorg/spongycastle/asn1/ao;->tag:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    iget-object v2, p1, Lorg/spongycastle/asn1/ao;->Gk:[B

    invoke-static {v1, v2}, Lorg/spongycastle/util/a;->h([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bI(I)Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    const/16 v0, 0x1f

    if-lt p1, v0, :cond_0

    .line 183
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unsupported tag number"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/ao;->getEncoded()[B

    move-result-object v0

    .line 187
    invoke-direct {p0, p1, v0}, Lorg/spongycastle/asn1/ao;->c(I[B)[B

    move-result-object v1

    .line 189
    aget-byte v0, v0, v2

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_1

    .line 191
    aget-byte v0, v1, v2

    or-int/lit8 v0, v0, 0x20

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    .line 194
    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/h;-><init>([B)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 234
    iget-boolean v0, p0, Lorg/spongycastle/asn1/ao;->Gj:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lorg/spongycastle/asn1/ao;->tag:I

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    invoke-static {v1}, Lorg/spongycastle/util/a;->hashCode([B)I

    move-result v1

    xor-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mN()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lorg/spongycastle/asn1/ao;->Gj:Z

    return v0
.end method

.method mO()I
    .locals 2

    .prologue
    .line 200
    iget v0, p0, Lorg/spongycastle/asn1/ao;->tag:I

    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bM(I)I

    move-result v0

    iget-object v1, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    array-length v1, v1

    invoke-static {v1}, Lorg/spongycastle/asn1/cc;->bL(I)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public mX()[B
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/spongycastle/asn1/ao;->Gk:[B

    return-object v0
.end method

.method public mY()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lorg/spongycastle/asn1/ao;->tag:I

    return v0
.end method
