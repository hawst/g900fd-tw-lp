.class public interface abstract Lorg/spongycastle/asn1/j/a;
.super Ljava/lang/Object;
.source "NTTObjectIdentifiers.java"


# static fields
.field public static final JS:Lorg/spongycastle/asn1/l;

.field public static final JT:Lorg/spongycastle/asn1/l;

.field public static final JU:Lorg/spongycastle/asn1/l;

.field public static final JV:Lorg/spongycastle/asn1/l;

.field public static final JW:Lorg/spongycastle/asn1/l;

.field public static final JX:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.392.200011.61.1.1.1.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/j/a;->JS:Lorg/spongycastle/asn1/l;

    .line 11
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.392.200011.61.1.1.1.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/j/a;->JT:Lorg/spongycastle/asn1/l;

    .line 12
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.392.200011.61.1.1.1.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/j/a;->JU:Lorg/spongycastle/asn1/l;

    .line 14
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.392.200011.61.1.1.3.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/j/a;->JV:Lorg/spongycastle/asn1/l;

    .line 15
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.392.200011.61.1.1.3.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/j/a;->JW:Lorg/spongycastle/asn1/l;

    .line 16
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.392.200011.61.1.1.3.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/j/a;->JX:Lorg/spongycastle/asn1/l;

    return-void
.end method
