.class public abstract Lorg/spongycastle/asn1/q;
.super Lorg/spongycastle/asn1/k;
.source "ASN1Primitive.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 11
    return-void
.end method

.method public static j([B)Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/h;-><init>([B)V

    .line 27
    :try_start_0
    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 29
    :catch_0
    move-exception v0

    .line 31
    new-instance v0, Ljava/io/IOException;

    const-string v1, "cannot recognise object in stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method abstract a(Lorg/spongycastle/asn1/o;)V
.end method

.method abstract a(Lorg/spongycastle/asn1/q;)Z
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 37
    if-ne p0, p1, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lorg/spongycastle/asn1/d;

    if-eqz v1, :cond_2

    check-cast p1, Lorg/spongycastle/asn1/d;

    invoke-interface {p1}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/spongycastle/asn1/q;->a(Lorg/spongycastle/asn1/q;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract hashCode()I
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 0

    .prologue
    .line 47
    return-object p0
.end method

.method mJ()Lorg/spongycastle/asn1/q;
    .locals 0

    .prologue
    .line 52
    return-object p0
.end method

.method mK()Lorg/spongycastle/asn1/q;
    .locals 0

    .prologue
    .line 57
    return-object p0
.end method

.method abstract mN()Z
.end method

.method abstract mO()I
.end method
