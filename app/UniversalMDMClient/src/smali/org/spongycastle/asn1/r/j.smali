.class public Lorg/spongycastle/asn1/r/j;
.super Lorg/spongycastle/asn1/k;
.source "X9FieldID.java"

# interfaces
.implements Lorg/spongycastle/asn1/r/l;


# instance fields
.field private Ve:Lorg/spongycastle/asn1/l;

.field private Vf:Lorg/spongycastle/asn1/q;


# direct methods
.method public constructor <init>(IIII)V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 52
    sget-object v0, Lorg/spongycastle/asn1/r/j;->Vj:Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/r/j;->Ve:Lorg/spongycastle/asn1/l;

    .line 53
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 54
    new-instance v1, Lorg/spongycastle/asn1/i;

    invoke-direct {v1, p1}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 56
    if-nez p3, :cond_0

    .line 58
    sget-object v1, Lorg/spongycastle/asn1/r/j;->Vl:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 59
    new-instance v1, Lorg/spongycastle/asn1/i;

    invoke-direct {v1, p2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 71
    :goto_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    iput-object v1, p0, Lorg/spongycastle/asn1/r/j;->Vf:Lorg/spongycastle/asn1/q;

    .line 72
    return-void

    .line 63
    :cond_0
    sget-object v1, Lorg/spongycastle/asn1/r/j;->Vm:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 64
    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 65
    new-instance v2, Lorg/spongycastle/asn1/i;

    invoke-direct {v2, p2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 66
    new-instance v2, Lorg/spongycastle/asn1/i;

    invoke-direct {v2, p3}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 67
    new-instance v2, Lorg/spongycastle/asn1/i;

    invoke-direct {v2, p4}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 68
    new-instance v2, Lorg/spongycastle/asn1/bh;

    invoke-direct {v2, v1}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 31
    sget-object v0, Lorg/spongycastle/asn1/r/j;->Vi:Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/r/j;->Ve:Lorg/spongycastle/asn1/l;

    .line 32
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/r/j;->Vf:Lorg/spongycastle/asn1/q;

    .line 33
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 77
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/r/j;->Ve:Lorg/spongycastle/asn1/l;

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/q;

    iput-object v0, p0, Lorg/spongycastle/asn1/r/j;->Vf:Lorg/spongycastle/asn1/q;

    .line 79
    return-void
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 104
    iget-object v1, p0, Lorg/spongycastle/asn1/r/j;->Ve:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 105
    iget-object v1, p0, Lorg/spongycastle/asn1/r/j;->Vf:Lorg/spongycastle/asn1/q;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 107
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public od()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/spongycastle/asn1/r/j;->Vf:Lorg/spongycastle/asn1/q;

    return-object v0
.end method

.method public pV()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/spongycastle/asn1/r/j;->Ve:Lorg/spongycastle/asn1/l;

    return-object v0
.end method
