.class public Lorg/spongycastle/asn1/r/i;
.super Lorg/spongycastle/asn1/k;
.source "X9FieldElement.java"


# static fields
.field private static converter:Lorg/spongycastle/asn1/r/k;


# instance fields
.field protected Vd:Lorg/spongycastle/a/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lorg/spongycastle/asn1/r/k;

    invoke-direct {v0}, Lorg/spongycastle/asn1/r/k;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/r/i;->converter:Lorg/spongycastle/asn1/r/k;

    return-void
.end method

.method public constructor <init>(IIIILorg/spongycastle/asn1/m;)V
    .locals 6

    .prologue
    .line 33
    new-instance v0, Lorg/spongycastle/a/a/g;

    new-instance v5, Ljava/math/BigInteger;

    const/4 v1, 0x1

    invoke-virtual {p5}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v2

    invoke-direct {v5, v1, v2}, Ljava/math/BigInteger;-><init>(I[B)V

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/a/a/g;-><init>(IIIILjava/math/BigInteger;)V

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/r/i;-><init>(Lorg/spongycastle/a/a/f;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/m;)V
    .locals 4

    .prologue
    .line 28
    new-instance v0, Lorg/spongycastle/a/a/h;

    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x1

    invoke-virtual {p2}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-direct {v0, p1, v1}, Lorg/spongycastle/a/a/h;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/r/i;-><init>(Lorg/spongycastle/a/a/f;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/f;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 23
    iput-object p1, p0, Lorg/spongycastle/asn1/r/i;->Vd:Lorg/spongycastle/a/a/f;

    .line 24
    return-void
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 59
    sget-object v0, Lorg/spongycastle/asn1/r/i;->converter:Lorg/spongycastle/asn1/r/k;

    iget-object v1, p0, Lorg/spongycastle/asn1/r/i;->Vd:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r/k;->a(Lorg/spongycastle/a/a/f;)I

    move-result v0

    .line 60
    sget-object v1, Lorg/spongycastle/asn1/r/i;->converter:Lorg/spongycastle/asn1/r/k;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/i;->Vd:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/spongycastle/asn1/r/k;->a(Ljava/math/BigInteger;I)[B

    move-result-object v0

    .line 62
    new-instance v1, Lorg/spongycastle/asn1/bd;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    return-object v1
.end method

.method public pU()Lorg/spongycastle/a/a/f;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/spongycastle/asn1/r/i;->Vd:Lorg/spongycastle/a/a/f;

    return-object v0
.end method
