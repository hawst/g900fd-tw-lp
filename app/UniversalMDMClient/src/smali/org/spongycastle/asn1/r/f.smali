.class public Lorg/spongycastle/asn1/r/f;
.super Lorg/spongycastle/asn1/k;
.source "X9ECParameters.java"

# interfaces
.implements Lorg/spongycastle/asn1/r/l;


# static fields
.field private static final ONE:Ljava/math/BigInteger;


# instance fields
.field private UV:Lorg/spongycastle/a/a/c;

.field private UW:[B

.field private UY:Lorg/spongycastle/asn1/r/j;

.field private UZ:Lorg/spongycastle/a/a/j;

.field private Va:Ljava/math/BigInteger;

.field private n:Ljava/math/BigInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/f;->ONE:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 6

    .prologue
    .line 85
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/asn1/r/f;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    .line 86
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V
    .locals 5

    .prologue
    .line 94
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 95
    iput-object p1, p0, Lorg/spongycastle/asn1/r/f;->UV:Lorg/spongycastle/a/a/c;

    .line 96
    iput-object p2, p0, Lorg/spongycastle/asn1/r/f;->UZ:Lorg/spongycastle/a/a/j;

    .line 97
    iput-object p3, p0, Lorg/spongycastle/asn1/r/f;->n:Ljava/math/BigInteger;

    .line 98
    iput-object p4, p0, Lorg/spongycastle/asn1/r/f;->Va:Ljava/math/BigInteger;

    .line 99
    iput-object p5, p0, Lorg/spongycastle/asn1/r/f;->UW:[B

    .line 101
    instance-of v0, p1, Lorg/spongycastle/a/a/e;

    if-eqz v0, :cond_1

    .line 103
    new-instance v0, Lorg/spongycastle/asn1/r/j;

    check-cast p1, Lorg/spongycastle/a/a/e;

    invoke-virtual {p1}, Lorg/spongycastle/a/a/e;->getQ()Ljava/math/BigInteger;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/r/j;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/r/f;->UY:Lorg/spongycastle/asn1/r/j;

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    instance-of v0, p1, Lorg/spongycastle/a/a/d;

    if-eqz v0, :cond_0

    .line 109
    check-cast p1, Lorg/spongycastle/a/a/d;

    .line 110
    new-instance v0, Lorg/spongycastle/asn1/r/j;

    invoke-virtual {p1}, Lorg/spongycastle/a/a/d;->getM()I

    move-result v1

    invoke-virtual {p1}, Lorg/spongycastle/a/a/d;->sm()I

    move-result v2

    invoke-virtual {p1}, Lorg/spongycastle/a/a/d;->sn()I

    move-result v3

    invoke-virtual {p1}, Lorg/spongycastle/a/a/d;->so()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/spongycastle/asn1/r/j;-><init>(IIII)V

    iput-object v0, p0, Lorg/spongycastle/asn1/r/f;->UY:Lorg/spongycastle/asn1/r/j;

    goto :goto_0
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 35
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/i;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lorg/spongycastle/asn1/r/f;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bad version in X9ECParameters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/r/e;

    new-instance v2, Lorg/spongycastle/asn1/r/j;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/r/j;-><init>(Lorg/spongycastle/asn1/r;)V

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/r/e;-><init>(Lorg/spongycastle/asn1/r/j;Lorg/spongycastle/asn1/r;)V

    .line 45
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/e;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/f;->UV:Lorg/spongycastle/a/a/c;

    .line 46
    new-instance v2, Lorg/spongycastle/asn1/r/h;

    iget-object v3, p0, Lorg/spongycastle/asn1/r/f;->UV:Lorg/spongycastle/a/a/c;

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    invoke-direct {v2, v3, v0}, Lorg/spongycastle/asn1/r/h;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/asn1/m;)V

    invoke-virtual {v2}, Lorg/spongycastle/asn1/r/h;->pT()Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/f;->UZ:Lorg/spongycastle/a/a/j;

    .line 47
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/f;->n:Ljava/math/BigInteger;

    .line 48
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/e;->getSeed()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/f;->UW:[B

    .line 50
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    .line 52
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/f;->Va:Ljava/math/BigInteger;

    .line 54
    :cond_2
    return-void
.end method

.method public static bn(Ljava/lang/Object;)Lorg/spongycastle/asn1/r/f;
    .locals 2

    .prologue
    .line 58
    instance-of v0, p0, Lorg/spongycastle/asn1/r/f;

    if-eqz v0, :cond_0

    .line 60
    check-cast p0, Lorg/spongycastle/asn1/r/f;

    .line 68
    :goto_0
    return-object p0

    .line 63
    :cond_0
    if-eqz p0, :cond_1

    .line 65
    new-instance v0, Lorg/spongycastle/asn1/r/f;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/r/f;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 68
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getSeed()[B
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/spongycastle/asn1/r/f;->UW:[B

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    .line 161
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 163
    new-instance v1, Lorg/spongycastle/asn1/i;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 164
    iget-object v1, p0, Lorg/spongycastle/asn1/r/f;->UY:Lorg/spongycastle/asn1/r/j;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 165
    new-instance v1, Lorg/spongycastle/asn1/r/e;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/f;->UV:Lorg/spongycastle/a/a/c;

    iget-object v3, p0, Lorg/spongycastle/asn1/r/f;->UW:[B

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/asn1/r/e;-><init>(Lorg/spongycastle/a/a/c;[B)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 166
    new-instance v1, Lorg/spongycastle/asn1/r/h;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/f;->UZ:Lorg/spongycastle/a/a/j;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/r/h;-><init>(Lorg/spongycastle/a/a/j;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 167
    new-instance v1, Lorg/spongycastle/asn1/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/f;->n:Ljava/math/BigInteger;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 169
    iget-object v1, p0, Lorg/spongycastle/asn1/r/f;->Va:Ljava/math/BigInteger;

    if-eqz v1, :cond_0

    .line 171
    new-instance v1, Lorg/spongycastle/asn1/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/f;->Va:Ljava/math/BigInteger;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 174
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public pO()Lorg/spongycastle/a/a/c;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/spongycastle/asn1/r/f;->UV:Lorg/spongycastle/a/a/c;

    return-object v0
.end method

.method public pP()Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lorg/spongycastle/asn1/r/f;->UZ:Lorg/spongycastle/a/a/j;

    return-object v0
.end method

.method public pQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/spongycastle/asn1/r/f;->n:Ljava/math/BigInteger;

    return-object v0
.end method

.method public pR()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/spongycastle/asn1/r/f;->Va:Ljava/math/BigInteger;

    if-nez v0, :cond_0

    .line 135
    sget-object v0, Lorg/spongycastle/asn1/r/f;->ONE:Ljava/math/BigInteger;

    .line 138
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/r/f;->Va:Ljava/math/BigInteger;

    goto :goto_0
.end method
