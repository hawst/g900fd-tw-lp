.class public Lorg/spongycastle/asn1/r/h;
.super Lorg/spongycastle/asn1/k;
.source "X9ECPoint.java"


# instance fields
.field Vc:Lorg/spongycastle/a/a/j;


# direct methods
.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/asn1/m;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 28
    invoke-virtual {p2}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/spongycastle/a/a/c;->R([B)Lorg/spongycastle/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/h;->Vc:Lorg/spongycastle/a/a/j;

    .line 29
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/j;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 21
    iput-object p1, p0, Lorg/spongycastle/asn1/r/h;->Vc:Lorg/spongycastle/a/a/j;

    .line 22
    return-void
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lorg/spongycastle/asn1/bd;

    iget-object v1, p0, Lorg/spongycastle/asn1/r/h;->Vc:Lorg/spongycastle/a/a/j;

    invoke-virtual {v1}, Lorg/spongycastle/a/a/j;->getEncoded()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    return-object v0
.end method

.method public pT()Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/spongycastle/asn1/r/h;->Vc:Lorg/spongycastle/a/a/j;

    return-object v0
.end method
