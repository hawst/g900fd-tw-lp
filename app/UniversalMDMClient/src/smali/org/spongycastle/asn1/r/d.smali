.class public Lorg/spongycastle/asn1/r/d;
.super Lorg/spongycastle/asn1/k;
.source "X962Parameters.java"

# interfaces
.implements Lorg/spongycastle/asn1/c;


# instance fields
.field private UU:Lorg/spongycastle/asn1/q;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    .line 48
    iput-object p1, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    .line 49
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    .line 54
    iput-object p1, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r/f;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    .line 42
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r/f;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    .line 43
    return-void
.end method

.method public static bm(Ljava/lang/Object;)Lorg/spongycastle/asn1/r/d;
    .locals 2

    .prologue
    .line 19
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/r/d;

    if-eqz v0, :cond_1

    .line 21
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/r/d;

    .line 26
    :goto_0
    return-object p0

    .line 24
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/q;

    if-eqz v0, :cond_2

    .line 26
    new-instance v0, Lorg/spongycastle/asn1/r/d;

    check-cast p0, Lorg/spongycastle/asn1/q;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/r/d;-><init>(Lorg/spongycastle/asn1/q;)V

    move-object p0, v0

    goto :goto_0

    .line 29
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown object in getInstance()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    return-object v0
.end method

.method public od()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    return-object v0
.end method

.method public pL()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    instance-of v0, v0, Lorg/spongycastle/asn1/l;

    return v0
.end method

.method public pM()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/spongycastle/asn1/r/d;->UU:Lorg/spongycastle/asn1/q;

    instance-of v0, v0, Lorg/spongycastle/asn1/j;

    return v0
.end method
