.class public interface abstract Lorg/spongycastle/asn1/r/l;
.super Ljava/lang/Object;
.source "X9ObjectIdentifiers.java"


# static fields
.field public static final OA:Lorg/spongycastle/asn1/l;

.field public static final VA:Lorg/spongycastle/asn1/l;

.field public static final VB:Lorg/spongycastle/asn1/l;

.field public static final VC:Lorg/spongycastle/asn1/l;

.field public static final VD:Lorg/spongycastle/asn1/l;

.field public static final VE:Lorg/spongycastle/asn1/l;

.field public static final VF:Lorg/spongycastle/asn1/l;

.field public static final VG:Lorg/spongycastle/asn1/l;

.field public static final VH:Lorg/spongycastle/asn1/l;

.field public static final VI:Lorg/spongycastle/asn1/l;

.field public static final VJ:Lorg/spongycastle/asn1/l;

.field public static final VK:Lorg/spongycastle/asn1/l;

.field public static final VL:Lorg/spongycastle/asn1/l;

.field public static final VM:Lorg/spongycastle/asn1/l;

.field public static final VN:Lorg/spongycastle/asn1/l;

.field public static final VO:Lorg/spongycastle/asn1/l;

.field public static final VP:Lorg/spongycastle/asn1/l;

.field public static final VQ:Lorg/spongycastle/asn1/l;

.field public static final VR:Lorg/spongycastle/asn1/l;

.field public static final VS:Lorg/spongycastle/asn1/l;

.field public static final VT:Lorg/spongycastle/asn1/l;

.field public static final VU:Lorg/spongycastle/asn1/l;

.field public static final VV:Lorg/spongycastle/asn1/l;

.field public static final VW:Lorg/spongycastle/asn1/l;

.field public static final VX:Lorg/spongycastle/asn1/l;

.field public static final VY:Lorg/spongycastle/asn1/l;

.field public static final VZ:Lorg/spongycastle/asn1/l;

.field public static final Vg:Lorg/spongycastle/asn1/l;

.field public static final Vh:Lorg/spongycastle/asn1/l;

.field public static final Vi:Lorg/spongycastle/asn1/l;

.field public static final Vj:Lorg/spongycastle/asn1/l;

.field public static final Vk:Lorg/spongycastle/asn1/l;

.field public static final Vl:Lorg/spongycastle/asn1/l;

.field public static final Vm:Lorg/spongycastle/asn1/l;

.field public static final Vn:Lorg/spongycastle/asn1/l;

.field public static final Vo:Lorg/spongycastle/asn1/l;

.field public static final Vp:Lorg/spongycastle/asn1/l;

.field public static final Vq:Lorg/spongycastle/asn1/l;

.field public static final Vr:Lorg/spongycastle/asn1/l;

.field public static final Vs:Lorg/spongycastle/asn1/l;

.field public static final Vt:Lorg/spongycastle/asn1/l;

.field public static final Vu:Lorg/spongycastle/asn1/l;

.field public static final Vv:Lorg/spongycastle/asn1/l;

.field public static final Vw:Lorg/spongycastle/asn1/l;

.field public static final Vx:Lorg/spongycastle/asn1/l;

.field public static final Vy:Lorg/spongycastle/asn1/l;

.field public static final Vz:Lorg/spongycastle/asn1/l;

.field public static final Wa:Lorg/spongycastle/asn1/l;

.field public static final Wb:Lorg/spongycastle/asn1/l;

.field public static final Wc:Lorg/spongycastle/asn1/l;

.field public static final Wd:Lorg/spongycastle/asn1/l;

.field public static final We:Lorg/spongycastle/asn1/l;

.field public static final Wf:Lorg/spongycastle/asn1/l;

.field public static final Wg:Lorg/spongycastle/asn1/l;

.field public static final Wh:Lorg/spongycastle/asn1/l;

.field public static final Wi:Lorg/spongycastle/asn1/l;

.field public static final Wj:Lorg/spongycastle/asn1/l;

.field public static final Wk:Lorg/spongycastle/asn1/l;

.field public static final Wl:Lorg/spongycastle/asn1/l;

.field public static final Wm:Lorg/spongycastle/asn1/l;

.field public static final Wn:Lorg/spongycastle/asn1/l;

.field public static final Wo:Lorg/spongycastle/asn1/l;

.field public static final Wp:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 13
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.840.10045"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vg:Lorg/spongycastle/asn1/l;

    .line 14
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vg:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vh:Lorg/spongycastle/asn1/l;

    .line 16
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vh:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vi:Lorg/spongycastle/asn1/l;

    .line 18
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vh:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vj:Lorg/spongycastle/asn1/l;

    .line 20
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vh:Lorg/spongycastle/asn1/l;

    const-string v1, "2.3.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vk:Lorg/spongycastle/asn1/l;

    .line 22
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vh:Lorg/spongycastle/asn1/l;

    const-string v1, "2.3.2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vl:Lorg/spongycastle/asn1/l;

    .line 24
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vh:Lorg/spongycastle/asn1/l;

    const-string v1, "2.3.3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vm:Lorg/spongycastle/asn1/l;

    .line 26
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vg:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vn:Lorg/spongycastle/asn1/l;

    .line 28
    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lorg/spongycastle/asn1/r/l;->Vn:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vo:Lorg/spongycastle/asn1/l;

    .line 30
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vg:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vp:Lorg/spongycastle/asn1/l;

    .line 32
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vp:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vq:Lorg/spongycastle/asn1/l;

    .line 34
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vn:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vr:Lorg/spongycastle/asn1/l;

    .line 36
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vr:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vs:Lorg/spongycastle/asn1/l;

    .line 38
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vr:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vt:Lorg/spongycastle/asn1/l;

    .line 40
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vr:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vu:Lorg/spongycastle/asn1/l;

    .line 42
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vr:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vv:Lorg/spongycastle/asn1/l;

    .line 47
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vg:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->OA:Lorg/spongycastle/asn1/l;

    .line 52
    sget-object v0, Lorg/spongycastle/asn1/r/l;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    .line 54
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vx:Lorg/spongycastle/asn1/l;

    .line 55
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vy:Lorg/spongycastle/asn1/l;

    .line 56
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Vz:Lorg/spongycastle/asn1/l;

    .line 57
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VA:Lorg/spongycastle/asn1/l;

    .line 58
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VB:Lorg/spongycastle/asn1/l;

    .line 59
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VC:Lorg/spongycastle/asn1/l;

    .line 60
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VD:Lorg/spongycastle/asn1/l;

    .line 61
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VE:Lorg/spongycastle/asn1/l;

    .line 62
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VF:Lorg/spongycastle/asn1/l;

    .line 63
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "10"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VG:Lorg/spongycastle/asn1/l;

    .line 64
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "11"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VH:Lorg/spongycastle/asn1/l;

    .line 65
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "12"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VI:Lorg/spongycastle/asn1/l;

    .line 66
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "13"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VJ:Lorg/spongycastle/asn1/l;

    .line 67
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "14"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VK:Lorg/spongycastle/asn1/l;

    .line 68
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VL:Lorg/spongycastle/asn1/l;

    .line 69
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "16"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VM:Lorg/spongycastle/asn1/l;

    .line 70
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "17"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VN:Lorg/spongycastle/asn1/l;

    .line 71
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "18"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VO:Lorg/spongycastle/asn1/l;

    .line 72
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "19"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VP:Lorg/spongycastle/asn1/l;

    .line 73
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Vw:Lorg/spongycastle/asn1/l;

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VQ:Lorg/spongycastle/asn1/l;

    .line 78
    sget-object v0, Lorg/spongycastle/asn1/r/l;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    .line 80
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VS:Lorg/spongycastle/asn1/l;

    .line 81
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VT:Lorg/spongycastle/asn1/l;

    .line 82
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VU:Lorg/spongycastle/asn1/l;

    .line 83
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VV:Lorg/spongycastle/asn1/l;

    .line 84
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VW:Lorg/spongycastle/asn1/l;

    .line 85
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VX:Lorg/spongycastle/asn1/l;

    .line 86
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VR:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VY:Lorg/spongycastle/asn1/l;

    .line 93
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.840.10040.4.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/r/l;->VZ:Lorg/spongycastle/asn1/l;

    .line 99
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.840.10040.4.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wa:Lorg/spongycastle/asn1/l;

    .line 104
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.133.16.840.63.0"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wb:Lorg/spongycastle/asn1/l;

    .line 105
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wb:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wc:Lorg/spongycastle/asn1/l;

    .line 106
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wb:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wd:Lorg/spongycastle/asn1/l;

    .line 107
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wb:Lorg/spongycastle/asn1/l;

    const-string v1, "16"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->We:Lorg/spongycastle/asn1/l;

    .line 113
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.840.10046"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wf:Lorg/spongycastle/asn1/l;

    .line 121
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wf:Lorg/spongycastle/asn1/l;

    const-string v1, "2.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wg:Lorg/spongycastle/asn1/l;

    .line 123
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wf:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    .line 124
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wi:Lorg/spongycastle/asn1/l;

    .line 125
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wj:Lorg/spongycastle/asn1/l;

    .line 126
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wk:Lorg/spongycastle/asn1/l;

    .line 127
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wl:Lorg/spongycastle/asn1/l;

    .line 128
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wm:Lorg/spongycastle/asn1/l;

    .line 129
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wn:Lorg/spongycastle/asn1/l;

    .line 130
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wo:Lorg/spongycastle/asn1/l;

    .line 131
    sget-object v0, Lorg/spongycastle/asn1/r/l;->Wh:Lorg/spongycastle/asn1/l;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/r/l;->Wp:Lorg/spongycastle/asn1/l;

    return-void
.end method
