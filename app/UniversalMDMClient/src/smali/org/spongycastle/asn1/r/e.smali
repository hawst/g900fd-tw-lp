.class public Lorg/spongycastle/asn1/r/e;
.super Lorg/spongycastle/asn1/k;
.source "X9Curve.java"

# interfaces
.implements Lorg/spongycastle/asn1/r/l;


# instance fields
.field private UV:Lorg/spongycastle/a/a/c;

.field private UW:[B

.field private UX:Lorg/spongycastle/asn1/l;


# direct methods
.method public constructor <init>(Lorg/spongycastle/a/a/c;[B)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    .line 40
    iput-object p1, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    .line 41
    iput-object p2, p0, Lorg/spongycastle/asn1/r/e;->UW:[B

    .line 42
    invoke-direct {p0}, Lorg/spongycastle/asn1/r/e;->pN()V

    .line 43
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r/j;Lorg/spongycastle/asn1/r;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 48
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    .line 49
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r/j;->pV()Lorg/spongycastle/asn1/l;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    .line 50
    iget-object v0, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    sget-object v1, Lorg/spongycastle/asn1/r/e;->Vi:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r/j;->od()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v1

    .line 53
    new-instance v2, Lorg/spongycastle/asn1/r/i;

    invoke-virtual {p2, v5}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    invoke-direct {v2, v1, v0}, Lorg/spongycastle/asn1/r/i;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/m;)V

    .line 54
    new-instance v3, Lorg/spongycastle/asn1/r/i;

    invoke-virtual {p2, v7}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    invoke-direct {v3, v1, v0}, Lorg/spongycastle/asn1/r/i;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/m;)V

    .line 55
    new-instance v0, Lorg/spongycastle/a/a/e;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/r/i;->pU()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v3}, Lorg/spongycastle/asn1/r/i;->pU()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/spongycastle/a/a/e;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 98
    invoke-virtual {p2, v11}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/aq;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/r/e;->UW:[B

    .line 100
    :cond_1
    return-void

    .line 59
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    sget-object v1, Lorg/spongycastle/asn1/r/e;->Vj:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r/j;->od()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v2

    .line 63
    invoke-virtual {v2, v5}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    .line 65
    invoke-virtual {v2, v7}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 71
    sget-object v3, Lorg/spongycastle/asn1/r/e;->Vl:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v3}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    invoke-virtual {v2, v11}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v2

    move v4, v5

    move v3, v5

    .line 89
    :goto_1
    new-instance v0, Lorg/spongycastle/asn1/r/i;

    invoke-virtual {p2, v5}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v5

    check-cast v5, Lorg/spongycastle/asn1/m;

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/asn1/r/i;-><init>(IIIILorg/spongycastle/asn1/m;)V

    .line 90
    new-instance v5, Lorg/spongycastle/asn1/r/i;

    invoke-virtual {p2, v7}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v10

    check-cast v10, Lorg/spongycastle/asn1/m;

    move v6, v1

    move v7, v2

    move v8, v3

    move v9, v4

    invoke-direct/range {v5 .. v10}, Lorg/spongycastle/asn1/r/i;-><init>(IIIILorg/spongycastle/asn1/m;)V

    .line 92
    new-instance v7, Lorg/spongycastle/a/a/d;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r/i;->pU()Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual {v5}, Lorg/spongycastle/asn1/r/i;->pU()Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v6

    move-object v0, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v6}, Lorg/spongycastle/a/a/d;-><init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;)V

    iput-object v7, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    goto/16 :goto_0

    .line 80
    :cond_3
    invoke-virtual {v2, v11}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/bh;

    .line 82
    invoke-virtual {v0, v5}, Lorg/spongycastle/asn1/bh;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    check-cast v2, Lorg/spongycastle/asn1/i;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v6

    .line 84
    invoke-virtual {v0, v7}, Lorg/spongycastle/asn1/bh;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    check-cast v2, Lorg/spongycastle/asn1/i;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v3

    .line 86
    invoke-virtual {v0, v11}, Lorg/spongycastle/asn1/bh;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v4

    move v2, v6

    goto :goto_1
.end method

.method private pN()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    instance-of v0, v0, Lorg/spongycastle/a/a/e;

    if-eqz v0, :cond_0

    .line 106
    sget-object v0, Lorg/spongycastle/asn1/r/e;->Vi:Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    .line 117
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    instance-of v0, v0, Lorg/spongycastle/a/a/d;

    if-eqz v0, :cond_1

    .line 110
    sget-object v0, Lorg/spongycastle/asn1/r/e;->Vj:Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    goto :goto_0

    .line 114
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This type of ECCurve is not implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getSeed()[B
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/spongycastle/asn1/r/e;->UW:[B

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 141
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 143
    iget-object v1, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/r/e;->Vi:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 145
    new-instance v1, Lorg/spongycastle/asn1/r/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/c;->sg()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/r/i;-><init>(Lorg/spongycastle/a/a/f;)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/i;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 146
    new-instance v1, Lorg/spongycastle/asn1/r/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/c;->sh()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/r/i;-><init>(Lorg/spongycastle/a/a/f;)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/i;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 154
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/spongycastle/asn1/r/e;->UW:[B

    if-eqz v1, :cond_1

    .line 156
    new-instance v1, Lorg/spongycastle/asn1/aq;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/e;->UW:[B

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/aq;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 159
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1

    .line 148
    :cond_2
    iget-object v1, p0, Lorg/spongycastle/asn1/r/e;->UX:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/r/e;->Vj:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    new-instance v1, Lorg/spongycastle/asn1/r/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/c;->sg()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/r/i;-><init>(Lorg/spongycastle/a/a/f;)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/i;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 151
    new-instance v1, Lorg/spongycastle/asn1/r/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/c;->sh()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/r/i;-><init>(Lorg/spongycastle/a/a/f;)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r/i;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method public pO()Lorg/spongycastle/a/a/c;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/spongycastle/asn1/r/e;->UV:Lorg/spongycastle/a/a/c;

    return-object v0
.end method
