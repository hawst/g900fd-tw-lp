.class public interface abstract Lorg/spongycastle/asn1/d/a;
.super Ljava/lang/Object;
.source "EACObjectIdentifiers.java"


# static fields
.field public static final HU:Lorg/spongycastle/asn1/l;

.field public static final HV:Lorg/spongycastle/asn1/l;

.field public static final HW:Lorg/spongycastle/asn1/l;

.field public static final HX:Lorg/spongycastle/asn1/l;

.field public static final HY:Lorg/spongycastle/asn1/l;

.field public static final HZ:Lorg/spongycastle/asn1/l;

.field public static final Ia:Lorg/spongycastle/asn1/l;

.field public static final Ib:Lorg/spongycastle/asn1/l;

.field public static final Ic:Lorg/spongycastle/asn1/l;

.field public static final Id:Lorg/spongycastle/asn1/l;

.field public static final Ie:Lorg/spongycastle/asn1/l;

.field public static final If:Lorg/spongycastle/asn1/l;

.field public static final Ig:Lorg/spongycastle/asn1/l;

.field public static final Ih:Lorg/spongycastle/asn1/l;

.field public static final Ii:Lorg/spongycastle/asn1/l;

.field public static final Ij:Lorg/spongycastle/asn1/l;

.field public static final Ik:Lorg/spongycastle/asn1/l;

.field public static final Il:Lorg/spongycastle/asn1/l;

.field public static final Im:Lorg/spongycastle/asn1/l;

.field public static final In:Lorg/spongycastle/asn1/l;

.field public static final Io:Lorg/spongycastle/asn1/l;

.field public static final Ip:Lorg/spongycastle/asn1/l;

.field public static final Iq:Lorg/spongycastle/asn1/l;

.field public static final Ir:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "0.4.0.127.0.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/d/a;->HU:Lorg/spongycastle/asn1/l;

    .line 16
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HU:Lorg/spongycastle/asn1/l;

    const-string v1, "2.2.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->HV:Lorg/spongycastle/asn1/l;

    .line 18
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HV:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->HW:Lorg/spongycastle/asn1/l;

    .line 19
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HV:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->HX:Lorg/spongycastle/asn1/l;

    .line 24
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HU:Lorg/spongycastle/asn1/l;

    const-string v1, "2.2.3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->HY:Lorg/spongycastle/asn1/l;

    .line 25
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HY:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->HZ:Lorg/spongycastle/asn1/l;

    .line 26
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HZ:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ia:Lorg/spongycastle/asn1/l;

    .line 27
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HY:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ib:Lorg/spongycastle/asn1/l;

    .line 28
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Ib:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ic:Lorg/spongycastle/asn1/l;

    .line 34
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HU:Lorg/spongycastle/asn1/l;

    const-string v1, "2.2.2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Id:Lorg/spongycastle/asn1/l;

    .line 36
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Id:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ie:Lorg/spongycastle/asn1/l;

    .line 37
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Ie:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->If:Lorg/spongycastle/asn1/l;

    .line 38
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Ie:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ig:Lorg/spongycastle/asn1/l;

    .line 39
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Ie:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ih:Lorg/spongycastle/asn1/l;

    .line 40
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Ie:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ii:Lorg/spongycastle/asn1/l;

    .line 41
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Ie:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ij:Lorg/spongycastle/asn1/l;

    .line 42
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Ie:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ik:Lorg/spongycastle/asn1/l;

    .line 43
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Id:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Il:Lorg/spongycastle/asn1/l;

    .line 44
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Il:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Im:Lorg/spongycastle/asn1/l;

    .line 45
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Il:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->In:Lorg/spongycastle/asn1/l;

    .line 46
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Il:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Io:Lorg/spongycastle/asn1/l;

    .line 47
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Il:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ip:Lorg/spongycastle/asn1/l;

    .line 48
    sget-object v0, Lorg/spongycastle/asn1/d/a;->Il:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Iq:Lorg/spongycastle/asn1/l;

    .line 54
    sget-object v0, Lorg/spongycastle/asn1/d/a;->HU:Lorg/spongycastle/asn1/l;

    const-string v1, "3.1.2.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/d/a;->Ir:Lorg/spongycastle/asn1/l;

    return-void
.end method
