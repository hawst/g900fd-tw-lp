.class public Lorg/spongycastle/asn1/ay;
.super Lorg/spongycastle/asn1/q;
.source "DERIA5String.java"

# interfaces
.implements Lorg/spongycastle/asn1/w;


# instance fields
.field private FL:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/asn1/ay;-><init>(Ljava/lang/String;Z)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 88
    if-nez p1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "string cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p1}, Lorg/spongycastle/asn1/ay;->cC(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string contains illegal characters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_1
    invoke-static {p1}, Lorg/spongycastle/util/g;->db(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    .line 98
    return-void
.end method

.method constructor <init>([B)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 64
    iput-object p1, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    .line 65
    return-void
.end method

.method public static R(Ljava/lang/Object;)Lorg/spongycastle/asn1/ay;
    .locals 3

    .prologue
    .line 25
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/ay;

    if-eqz v0, :cond_1

    .line 27
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/ay;

    return-object p0

    .line 30
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "illegal object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static cC(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 159
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 161
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 163
    const/16 v2, 0x7f

    if-le v1, v2, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 169
    :goto_1
    return v0

    .line 159
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 169
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static g(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/ay;
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 48
    if-nez p1, :cond_0

    instance-of v1, v0, Lorg/spongycastle/asn1/ay;

    if-eqz v1, :cond_1

    .line 50
    :cond_0
    invoke-static {v0}, Lorg/spongycastle/asn1/ay;->R(Ljava/lang/Object;)Lorg/spongycastle/asn1/ay;

    move-result-object v0

    .line 54
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/ay;

    check-cast v0, Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ay;-><init>([B)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2

    .prologue
    .line 129
    const/16 v0, 0x16

    iget-object v1, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->b(I[B)V

    .line 130
    return-void
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 2

    .prologue
    .line 140
    instance-of v0, p1, Lorg/spongycastle/asn1/ay;

    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x0

    .line 147
    :goto_0
    return v0

    .line 145
    :cond_0
    check-cast p1, Lorg/spongycastle/asn1/ay;

    .line 147
    iget-object v0, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    iget-object v1, p1, Lorg/spongycastle/asn1/ay;->FL:[B

    invoke-static {v0, v1}, Lorg/spongycastle/util/a;->h([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    invoke-static {v0}, Lorg/spongycastle/util/g;->V([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->hashCode([B)I

    move-result v0

    return v0
.end method

.method mN()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method mO()I
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    array-length v0, v0

    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bL(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/ay;->FL:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/spongycastle/asn1/ay;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
