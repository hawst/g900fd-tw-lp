.class public Lorg/spongycastle/asn1/aw;
.super Lorg/spongycastle/asn1/q;
.source "DERGeneralString.java"

# interfaces
.implements Lorg/spongycastle/asn1/w;


# instance fields
.field private FL:[B


# direct methods
.method constructor <init>([B)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/spongycastle/asn1/aw;->FL:[B

    .line 45
    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2

    .prologue
    .line 80
    const/16 v0, 0x1b

    iget-object v1, p0, Lorg/spongycastle/asn1/aw;->FL:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->b(I[B)V

    .line 81
    return-void
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 2

    .prologue
    .line 90
    instance-of v0, p1, Lorg/spongycastle/asn1/aw;

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    .line 94
    :cond_0
    check-cast p1, Lorg/spongycastle/asn1/aw;

    .line 96
    iget-object v0, p0, Lorg/spongycastle/asn1/aw;->FL:[B

    iget-object v1, p1, Lorg/spongycastle/asn1/aw;->FL:[B

    invoke-static {v0, v1}, Lorg/spongycastle/util/a;->h([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/spongycastle/asn1/aw;->FL:[B

    invoke-static {v0}, Lorg/spongycastle/util/g;->V([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/spongycastle/asn1/aw;->FL:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->hashCode([B)I

    move-result v0

    return v0
.end method

.method mN()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method mO()I
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/asn1/aw;->FL:[B

    array-length v0, v0

    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bL(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/aw;->FL:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/spongycastle/asn1/aw;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
