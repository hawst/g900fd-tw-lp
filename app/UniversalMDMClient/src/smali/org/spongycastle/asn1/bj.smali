.class public Lorg/spongycastle/asn1/bj;
.super Lorg/spongycastle/asn1/t;
.source "DERSet.java"


# instance fields
.field private GH:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/spongycastle/asn1/t;-><init>()V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lorg/spongycastle/asn1/bj;->GH:I

    .line 19
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lorg/spongycastle/asn1/t;-><init>(Lorg/spongycastle/asn1/d;)V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lorg/spongycastle/asn1/bj;->GH:I

    .line 28
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/e;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/asn1/t;-><init>(Lorg/spongycastle/asn1/e;Z)V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lorg/spongycastle/asn1/bj;->GH:I

    .line 37
    return-void
.end method

.method constructor <init>(Lorg/spongycastle/asn1/e;Z)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lorg/spongycastle/asn1/t;-><init>(Lorg/spongycastle/asn1/e;Z)V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lorg/spongycastle/asn1/bj;->GH:I

    .line 53
    return-void
.end method

.method public constructor <init>([Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/asn1/t;-><init>([Lorg/spongycastle/asn1/d;Z)V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lorg/spongycastle/asn1/bj;->GH:I

    .line 46
    return-void
.end method

.method private nm()I
    .locals 3

    .prologue
    .line 58
    iget v0, p0, Lorg/spongycastle/asn1/bj;->GH:I

    if-gez v0, :cond_1

    .line 60
    const/4 v0, 0x0

    .line 62
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bj;->mQ()Ljava/util/Enumeration;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    .line 66
    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->mJ()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->mO()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 67
    goto :goto_0

    .line 69
    :cond_0
    iput v1, p0, Lorg/spongycastle/asn1/bj;->GH:I

    .line 72
    :cond_1
    iget v0, p0, Lorg/spongycastle/asn1/bj;->GH:I

    return v0
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p1}, Lorg/spongycastle/asn1/o;->mL()Lorg/spongycastle/asn1/o;

    move-result-object v1

    .line 96
    invoke-direct {p0}, Lorg/spongycastle/asn1/bj;->nm()I

    move-result v0

    .line 98
    const/16 v2, 0x31

    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/o;->write(I)V

    .line 99
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->bF(I)V

    .line 101
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bj;->mQ()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    .line 105
    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/o;->b(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method

.method mO()I
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Lorg/spongycastle/asn1/bj;->nm()I

    move-result v0

    .line 80
    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bL(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method
