.class public interface abstract Lorg/spongycastle/asn1/f/a;
.super Ljava/lang/Object;
.source "ISISMTTObjectIdentifiers.java"


# static fields
.field public static final IA:Lorg/spongycastle/asn1/l;

.field public static final IB:Lorg/spongycastle/asn1/l;

.field public static final IC:Lorg/spongycastle/asn1/l;

.field public static final ID:Lorg/spongycastle/asn1/l;

.field public static final IE:Lorg/spongycastle/asn1/l;

.field public static final IF:Lorg/spongycastle/asn1/l;

.field public static final IG:Lorg/spongycastle/asn1/l;

.field public static final IH:Lorg/spongycastle/asn1/l;

.field public static final II:Lorg/spongycastle/asn1/l;

.field public static final IJ:Lorg/spongycastle/asn1/l;

.field public static final IK:Lorg/spongycastle/asn1/l;

.field public static final IL:Lorg/spongycastle/asn1/l;

.field public static final IM:Lorg/spongycastle/asn1/l;

.field public static final IO:Lorg/spongycastle/asn1/l;

.field public static final IP:Lorg/spongycastle/asn1/l;

.field public static final IQ:Lorg/spongycastle/asn1/l;

.field public static final IR:Lorg/spongycastle/asn1/l;

.field public static final Ix:Lorg/spongycastle/asn1/l;

.field public static final Iy:Lorg/spongycastle/asn1/l;

.field public static final Iz:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.36.8"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/f/a;->Ix:Lorg/spongycastle/asn1/l;

    .line 10
    sget-object v0, Lorg/spongycastle/asn1/f/a;->Ix:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->Iy:Lorg/spongycastle/asn1/l;

    .line 19
    sget-object v0, Lorg/spongycastle/asn1/f/a;->Iy:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->Iz:Lorg/spongycastle/asn1/l;

    .line 21
    sget-object v0, Lorg/spongycastle/asn1/f/a;->Ix:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    .line 30
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IB:Lorg/spongycastle/asn1/l;

    .line 36
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IC:Lorg/spongycastle/asn1/l;

    .line 42
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->ID:Lorg/spongycastle/asn1/l;

    .line 51
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IE:Lorg/spongycastle/asn1/l;

    .line 57
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IF:Lorg/spongycastle/asn1/l;

    .line 67
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IG:Lorg/spongycastle/asn1/l;

    .line 78
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IH:Lorg/spongycastle/asn1/l;

    .line 91
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->II:Lorg/spongycastle/asn1/l;

    .line 107
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IJ:Lorg/spongycastle/asn1/l;

    .line 116
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "10"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IK:Lorg/spongycastle/asn1/l;

    .line 121
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "11"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IL:Lorg/spongycastle/asn1/l;

    .line 133
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "12"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IM:Lorg/spongycastle/asn1/l;

    .line 140
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "13"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IO:Lorg/spongycastle/asn1/l;

    .line 150
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "14"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IP:Lorg/spongycastle/asn1/l;

    .line 163
    sget-object v0, Lorg/spongycastle/asn1/f/a;->IA:Lorg/spongycastle/asn1/l;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IQ:Lorg/spongycastle/asn1/l;

    .line 179
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "0.2.262.1.10.12.0"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/f/a;->IR:Lorg/spongycastle/asn1/l;

    return-void
.end method
