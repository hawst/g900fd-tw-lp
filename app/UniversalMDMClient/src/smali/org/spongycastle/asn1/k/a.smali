.class public Lorg/spongycastle/asn1/k/a;
.super Lorg/spongycastle/asn1/k;
.source "ElGamalParameter.java"


# instance fields
.field HN:Lorg/spongycastle/asn1/i;

.field JY:Lorg/spongycastle/asn1/i;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 22
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/k/a;->HN:Lorg/spongycastle/asn1/i;

    .line 23
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/k/a;->JY:Lorg/spongycastle/asn1/i;

    .line 24
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 29
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 31
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/k/a;->HN:Lorg/spongycastle/asn1/i;

    .line 32
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/k/a;->JY:Lorg/spongycastle/asn1/i;

    .line 33
    return-void
.end method


# virtual methods
.method public getG()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/spongycastle/asn1/k/a;->JY:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getP()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/spongycastle/asn1/k/a;->HN:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 49
    iget-object v1, p0, Lorg/spongycastle/asn1/k/a;->HN:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 50
    iget-object v1, p0, Lorg/spongycastle/asn1/k/a;->JY:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 52
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
