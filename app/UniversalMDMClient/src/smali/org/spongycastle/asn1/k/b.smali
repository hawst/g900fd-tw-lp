.class public interface abstract Lorg/spongycastle/asn1/k/b;
.super Ljava/lang/Object;
.source "OIWObjectIdentifiers.java"


# static fields
.field public static final JZ:Lorg/spongycastle/asn1/l;

.field public static final Ka:Lorg/spongycastle/asn1/l;

.field public static final Kb:Lorg/spongycastle/asn1/l;

.field public static final Kc:Lorg/spongycastle/asn1/l;

.field public static final Kd:Lorg/spongycastle/asn1/l;

.field public static final Ke:Lorg/spongycastle/asn1/l;

.field public static final Kf:Lorg/spongycastle/asn1/l;

.field public static final Kg:Lorg/spongycastle/asn1/l;

.field public static final Kh:Lorg/spongycastle/asn1/l;

.field public static final Ki:Lorg/spongycastle/asn1/l;

.field public static final Kj:Lorg/spongycastle/asn1/l;

.field public static final Kk:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->JZ:Lorg/spongycastle/asn1/l;

    .line 10
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Ka:Lorg/spongycastle/asn1/l;

    .line 11
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kb:Lorg/spongycastle/asn1/l;

    .line 13
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.6"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kc:Lorg/spongycastle/asn1/l;

    .line 14
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kd:Lorg/spongycastle/asn1/l;

    .line 15
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.8"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Ke:Lorg/spongycastle/asn1/l;

    .line 16
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.9"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kf:Lorg/spongycastle/asn1/l;

    .line 18
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.17"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kg:Lorg/spongycastle/asn1/l;

    .line 20
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.26"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kh:Lorg/spongycastle/asn1/l;

    .line 22
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.27"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Ki:Lorg/spongycastle/asn1/l;

    .line 24
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kj:Lorg/spongycastle/asn1/l;

    .line 29
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.7.2.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/k/b;->Kk:Lorg/spongycastle/asn1/l;

    return-void
.end method
