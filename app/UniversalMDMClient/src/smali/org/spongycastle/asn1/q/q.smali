.class public Lorg/spongycastle/asn1/q/q;
.super Lorg/spongycastle/asn1/k;
.source "DistributionPoint.java"


# instance fields
.field RC:Lorg/spongycastle/asn1/q/r;

.field RE:Lorg/spongycastle/asn1/q/af;

.field RF:Lorg/spongycastle/asn1/q/v;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/r;Lorg/spongycastle/asn1/q/af;Lorg/spongycastle/asn1/q/v;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 77
    iput-object p1, p0, Lorg/spongycastle/asn1/q/q;->RC:Lorg/spongycastle/asn1/q/r;

    .line 78
    iput-object p2, p0, Lorg/spongycastle/asn1/q/q;->RE:Lorg/spongycastle/asn1/q/af;

    .line 79
    iput-object p3, p0, Lorg/spongycastle/asn1/q/q;->RF:Lorg/spongycastle/asn1/q/v;

    .line 80
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    move v0, v1

    .line 55
    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 57
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v2

    .line 58
    invoke-virtual {v2}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 55
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :pswitch_0
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/spongycastle/asn1/q/r;->l(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/r;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/q;->RC:Lorg/spongycastle/asn1/q/r;

    goto :goto_1

    .line 64
    :pswitch_1
    new-instance v3, Lorg/spongycastle/asn1/q/af;

    invoke-static {v2, v1}, Lorg/spongycastle/asn1/aq;->e(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/aq;

    move-result-object v2

    invoke-direct {v3, v2}, Lorg/spongycastle/asn1/q/af;-><init>(Lorg/spongycastle/asn1/aq;)V

    iput-object v3, p0, Lorg/spongycastle/asn1/q/q;->RE:Lorg/spongycastle/asn1/q/af;

    goto :goto_1

    .line 67
    :pswitch_2
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/q/v;->n(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/v;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/q;->RF:Lorg/spongycastle/asn1/q/v;

    goto :goto_1

    .line 70
    :cond_0
    return-void

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 147
    const-string v0, "    "

    .line 149
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 152
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 153
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    invoke-virtual {p1, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 157
    return-void
.end method

.method public static aL(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/q;
    .locals 3

    .prologue
    .line 39
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/q;

    if-eqz v0, :cond_1

    .line 41
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/q;

    .line 46
    :goto_0
    return-object p0

    .line 44
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_2

    .line 46
    new-instance v0, Lorg/spongycastle/asn1/q/q;

    check-cast p0, Lorg/spongycastle/asn1/r;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/q;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 49
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid DistributionPoint: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 99
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 101
    iget-object v1, p0, Lorg/spongycastle/asn1/q/q;->RC:Lorg/spongycastle/asn1/q/r;

    if-eqz v1, :cond_0

    .line 106
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/q/q;->RC:Lorg/spongycastle/asn1/q/r;

    invoke-direct {v1, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 109
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/q;->RE:Lorg/spongycastle/asn1/q/af;

    if-eqz v1, :cond_1

    .line 111
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/spongycastle/asn1/q/q;->RE:Lorg/spongycastle/asn1/q/af;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 114
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/q/q;->RF:Lorg/spongycastle/asn1/q/v;

    if-eqz v1, :cond_2

    .line 116
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/spongycastle/asn1/q/q;->RF:Lorg/spongycastle/asn1/q/v;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 119
    :cond_2
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public oP()Lorg/spongycastle/asn1/q/r;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/spongycastle/asn1/q/q;->RC:Lorg/spongycastle/asn1/q/r;

    return-object v0
.end method

.method public oQ()Lorg/spongycastle/asn1/q/af;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/spongycastle/asn1/q/q;->RE:Lorg/spongycastle/asn1/q/af;

    return-object v0
.end method

.method public oR()Lorg/spongycastle/asn1/q/v;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/asn1/q/q;->RF:Lorg/spongycastle/asn1/q/v;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 124
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 126
    const-string v2, "DistributionPoint: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    iget-object v2, p0, Lorg/spongycastle/asn1/q/q;->RC:Lorg/spongycastle/asn1/q/r;

    if-eqz v2, :cond_0

    .line 130
    const-string v2, "distributionPoint"

    iget-object v3, p0, Lorg/spongycastle/asn1/q/q;->RC:Lorg/spongycastle/asn1/q/r;

    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/r;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/q;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    iget-object v2, p0, Lorg/spongycastle/asn1/q/q;->RE:Lorg/spongycastle/asn1/q/af;

    if-eqz v2, :cond_1

    .line 134
    const-string v2, "reasons"

    iget-object v3, p0, Lorg/spongycastle/asn1/q/q;->RE:Lorg/spongycastle/asn1/q/af;

    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/af;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/q;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_1
    iget-object v2, p0, Lorg/spongycastle/asn1/q/q;->RF:Lorg/spongycastle/asn1/q/v;

    if-eqz v2, :cond_2

    .line 138
    const-string v2, "cRLIssuer"

    iget-object v3, p0, Lorg/spongycastle/asn1/q/q;->RF:Lorg/spongycastle/asn1/q/v;

    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/v;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/q;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_2
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 141
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
