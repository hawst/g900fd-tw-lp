.class public Lorg/spongycastle/asn1/q/ao;
.super Lorg/spongycastle/asn1/k;
.source "Target.java"

# interfaces
.implements Lorg/spongycastle/asn1/c;


# instance fields
.field private Tg:Lorg/spongycastle/asn1/q/u;

.field private Th:Lorg/spongycastle/asn1/q/u;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/x;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 67
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 68
    invoke-virtual {p1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :pswitch_0
    invoke-static {p1, v1}, Lorg/spongycastle/asn1/q/u;->m(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/u;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ao;->Tg:Lorg/spongycastle/asn1/q/u;

    .line 79
    :goto_0
    return-void

    .line 74
    :pswitch_1
    invoke-static {p1, v1}, Lorg/spongycastle/asn1/q/u;->m(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/u;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ao;->Th:Lorg/spongycastle/asn1/q/u;

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static bc(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ao;
    .locals 3

    .prologue
    .line 47
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/ao;

    if-eqz v0, :cond_1

    .line 49
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/ao;

    .line 53
    :goto_0
    return-object p0

    .line 51
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/x;

    if-eqz v0, :cond_2

    .line 53
    new-instance v0, Lorg/spongycastle/asn1/q/ao;

    check-cast p0, Lorg/spongycastle/asn1/x;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/ao;-><init>(Lorg/spongycastle/asn1/x;)V

    move-object p0, v0

    goto :goto_0

    .line 56
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in factory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 129
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ao;->Tg:Lorg/spongycastle/asn1/q/u;

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Lorg/spongycastle/asn1/bm;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/spongycastle/asn1/q/ao;->Tg:Lorg/spongycastle/asn1/q/u;

    invoke-direct {v0, v3, v1, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    .line 135
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/bm;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/ao;->Th:Lorg/spongycastle/asn1/q/u;

    invoke-direct {v0, v3, v3, v1}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method public pC()Lorg/spongycastle/asn1/q/u;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ao;->Th:Lorg/spongycastle/asn1/q/u;

    return-object v0
.end method

.method public pD()Lorg/spongycastle/asn1/q/u;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ao;->Tg:Lorg/spongycastle/asn1/q/u;

    return-object v0
.end method
