.class public Lorg/spongycastle/asn1/q/ap;
.super Lorg/spongycastle/asn1/k;
.source "TargetInformation.java"


# instance fields
.field private Ti:Lorg/spongycastle/asn1/r;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 57
    iput-object p1, p0, Lorg/spongycastle/asn1/q/ap;->Ti:Lorg/spongycastle/asn1/r;

    .line 58
    return-void
.end method

.method public static bd(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ap;
    .locals 2

    .prologue
    .line 36
    instance-of v0, p0, Lorg/spongycastle/asn1/q/ap;

    if-eqz v0, :cond_0

    .line 38
    check-cast p0, Lorg/spongycastle/asn1/q/ap;

    .line 45
    :goto_0
    return-object p0

    .line 40
    :cond_0
    if-eqz p0, :cond_1

    .line 42
    new-instance v0, Lorg/spongycastle/asn1/q/ap;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/ap;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 45
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ap;->Ti:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public pE()[Lorg/spongycastle/asn1/q/aq;
    .locals 5

    .prologue
    .line 67
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ap;->Ti:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    new-array v2, v0, [Lorg/spongycastle/asn1/q/aq;

    .line 68
    const/4 v0, 0x0

    .line 69
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ap;->Ti:Lorg/spongycastle/asn1/r;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    add-int/lit8 v1, v0, 0x1

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lorg/spongycastle/asn1/q/aq;->be(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/aq;

    move-result-object v4

    aput-object v4, v2, v0

    move v0, v1

    goto :goto_0

    .line 73
    :cond_0
    return-object v2
.end method
