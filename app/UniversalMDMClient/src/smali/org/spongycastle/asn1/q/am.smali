.class public Lorg/spongycastle/asn1/q/am;
.super Lorg/spongycastle/asn1/k;
.source "TBSCertificate.java"


# instance fields
.field Ku:Lorg/spongycastle/asn1/i;

.field Kv:Lorg/spongycastle/asn1/p/c;

.field NQ:Lorg/spongycastle/asn1/r;

.field Ri:Lorg/spongycastle/asn1/q/a;

.field Rj:Lorg/spongycastle/asn1/i;

.field Rn:Lorg/spongycastle/asn1/q/t;

.field SR:Lorg/spongycastle/asn1/p/c;

.field Ta:Lorg/spongycastle/asn1/q/ar;

.field Tb:Lorg/spongycastle/asn1/q/ar;

.field Tc:Lorg/spongycastle/asn1/q/ah;

.field Td:Lorg/spongycastle/asn1/aq;

.field Te:Lorg/spongycastle/asn1/aq;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 72
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 75
    iput-object p1, p0, Lorg/spongycastle/asn1/q/am;->NQ:Lorg/spongycastle/asn1/r;

    .line 80
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/bm;

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    invoke-static {v0, v5}, Lorg/spongycastle/asn1/i;->h(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Ku:Lorg/spongycastle/asn1/i;

    move v1, v2

    .line 90
    :goto_0
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Rj:Lorg/spongycastle/asn1/i;

    .line 92
    add-int/lit8 v0, v1, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Ri:Lorg/spongycastle/asn1/q/a;

    .line 93
    add-int/lit8 v0, v1, 0x3

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->SR:Lorg/spongycastle/asn1/p/c;

    .line 98
    add-int/lit8 v0, v1, 0x4

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    .line 100
    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    invoke-static {v3}, Lorg/spongycastle/asn1/q/ar;->bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;

    move-result-object v3

    iput-object v3, p0, Lorg/spongycastle/asn1/q/am;->Ta:Lorg/spongycastle/asn1/q/ar;

    .line 101
    invoke-virtual {v0, v5}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ar;->bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Tb:Lorg/spongycastle/asn1/q/ar;

    .line 103
    add-int/lit8 v0, v1, 0x5

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Kv:Lorg/spongycastle/asn1/p/c;

    .line 108
    add-int/lit8 v0, v1, 0x6

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ah;->aX(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ah;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Tc:Lorg/spongycastle/asn1/q/ah;

    .line 110
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    add-int/lit8 v3, v1, 0x6

    sub-int/2addr v0, v3

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-lez v3, :cond_1

    .line 112
    add-int/lit8 v0, v1, 0x6

    add-int/2addr v0, v3

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/bm;

    .line 114
    invoke-virtual {v0}, Lorg/spongycastle/asn1/bm;->mT()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 110
    :goto_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    .line 86
    :cond_0
    const/4 v0, -0x1

    .line 87
    new-instance v1, Lorg/spongycastle/asn1/i;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    iput-object v1, p0, Lorg/spongycastle/asn1/q/am;->Ku:Lorg/spongycastle/asn1/i;

    move v1, v0

    goto/16 :goto_0

    .line 117
    :pswitch_0
    invoke-static {v0, v2}, Lorg/spongycastle/asn1/aq;->e(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Td:Lorg/spongycastle/asn1/aq;

    goto :goto_2

    .line 120
    :pswitch_1
    invoke-static {v0, v2}, Lorg/spongycastle/asn1/aq;->e(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Te:Lorg/spongycastle/asn1/aq;

    goto :goto_2

    .line 123
    :pswitch_2
    invoke-static {v0, v5}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/t;->aN(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/am;->Rn:Lorg/spongycastle/asn1/q/t;

    goto :goto_2

    .line 126
    :cond_1
    return-void

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static ba(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/am;
    .locals 2

    .prologue
    .line 58
    instance-of v0, p0, Lorg/spongycastle/asn1/q/am;

    if-eqz v0, :cond_0

    .line 60
    check-cast p0, Lorg/spongycastle/asn1/q/am;

    .line 67
    :goto_0
    return-object p0

    .line 62
    :cond_0
    if-eqz p0, :cond_1

    .line 64
    new-instance v0, Lorg/spongycastle/asn1/q/am;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/am;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 67
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lorg/spongycastle/asn1/q/am;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public oE()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/spongycastle/asn1/q/am;->SR:Lorg/spongycastle/asn1/p/c;

    return-object v0
.end method
