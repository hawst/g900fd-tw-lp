.class public Lorg/spongycastle/asn1/q/a;
.super Lorg/spongycastle/asn1/k;
.source "AlgorithmIdentifier.java"


# instance fields
.field private QX:Lorg/spongycastle/asn1/l;

.field private QY:Lorg/spongycastle/asn1/d;

.field private QZ:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 68
    new-instance v0, Lorg/spongycastle/asn1/l;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    .line 69
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/bc;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 78
    new-instance v0, Lorg/spongycastle/asn1/l;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    .line 79
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/bc;Lorg/spongycastle/asn1/d;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 91
    new-instance v0, Lorg/spongycastle/asn1/l;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bc;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    .line 92
    iput-object p2, p0, Lorg/spongycastle/asn1/q/a;->QY:Lorg/spongycastle/asn1/d;

    .line 93
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/l;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 58
    iput-object p1, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 100
    iput-object p1, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    .line 101
    iput-object p2, p0, Lorg/spongycastle/asn1/q/a;->QY:Lorg/spongycastle/asn1/d;

    .line 102
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 106
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    iput-boolean v2, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 107
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-le v0, v3, :cond_1

    .line 109
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_1
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l;->T(Ljava/lang/Object;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    .line 115
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 117
    iput-boolean v1, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    .line 118
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/a;->QY:Lorg/spongycastle/asn1/d;

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/a;->QY:Lorg/spongycastle/asn1/d;

    goto :goto_0
.end method

.method public static ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;
    .locals 3

    .prologue
    .line 32
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/a;

    if-eqz v0, :cond_1

    .line 34
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/a;

    .line 49
    :goto_0
    return-object p0

    .line 37
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/l;

    if-eqz v0, :cond_2

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    check-cast p0, Lorg/spongycastle/asn1/l;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;)V

    move-object p0, v0

    goto :goto_0

    .line 42
    :cond_2
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 44
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    check-cast p0, Ljava/lang/String;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/a;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    goto :goto_0

    .line 47
    :cond_3
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-nez v0, :cond_4

    instance-of v0, p0, Lorg/spongycastle/asn1/s;

    if-eqz v0, :cond_5

    .line 49
    :cond_4
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 52
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in factory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static k(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 26
    invoke-static {p0, p1}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 155
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 157
    iget-object v1, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 159
    iget-boolean v1, p0, Lorg/spongycastle/asn1/q/a;->QZ:Z

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lorg/spongycastle/asn1/q/a;->QY:Lorg/spongycastle/asn1/d;

    if-eqz v1, :cond_1

    .line 163
    iget-object v1, p0, Lorg/spongycastle/asn1/q/a;->QY:Lorg/spongycastle/asn1/d;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 171
    :cond_0
    :goto_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1

    .line 167
    :cond_1
    sget-object v1, Lorg/spongycastle/asn1/ba;->GC:Lorg/spongycastle/asn1/ba;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method public om()Lorg/spongycastle/asn1/l;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Lorg/spongycastle/asn1/l;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public on()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lorg/spongycastle/asn1/q/a;->QX:Lorg/spongycastle/asn1/l;

    return-object v0
.end method

.method public oo()Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/spongycastle/asn1/q/a;->QY:Lorg/spongycastle/asn1/d;

    return-object v0
.end method
