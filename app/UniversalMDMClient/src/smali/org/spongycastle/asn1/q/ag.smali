.class public Lorg/spongycastle/asn1/q/ag;
.super Lorg/spongycastle/asn1/k;
.source "SubjectKeyIdentifier.java"


# instance fields
.field private SP:[B


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/ah;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 77
    invoke-static {p1}, Lorg/spongycastle/asn1/q/ag;->a(Lorg/spongycastle/asn1/q/ah;)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ag;->SP:[B

    .line 78
    return-void
.end method

.method private static a(Lorg/spongycastle/asn1/q/ah;)[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 122
    new-instance v0, Lorg/spongycastle/crypto/b/l;

    invoke-direct {v0}, Lorg/spongycastle/crypto/b/l;-><init>()V

    .line 123
    invoke-interface {v0}, Lorg/spongycastle/crypto/l;->getDigestSize()I

    move-result v1

    new-array v1, v1, [B

    .line 125
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/ah;->pr()Lorg/spongycastle/asn1/aq;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/aq;->getBytes()[B

    move-result-object v2

    .line 126
    array-length v3, v2

    invoke-interface {v0, v2, v4, v3}, Lorg/spongycastle/crypto/l;->update([BII)V

    .line 127
    invoke-interface {v0, v1, v4}, Lorg/spongycastle/crypto/l;->doFinal([BI)I

    .line 128
    return-object v1
.end method


# virtual methods
.method public getKeyIdentifier()[B
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ag;->SP:[B

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lorg/spongycastle/asn1/bd;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/ag;->SP:[B

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    return-object v0
.end method
