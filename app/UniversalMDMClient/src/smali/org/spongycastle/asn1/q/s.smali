.class public Lorg/spongycastle/asn1/q/s;
.super Ljava/lang/Object;
.source "Extension.java"


# static fields
.field public static final RH:Lorg/spongycastle/asn1/l;

.field public static final RI:Lorg/spongycastle/asn1/l;

.field public static final RJ:Lorg/spongycastle/asn1/l;

.field public static final RK:Lorg/spongycastle/asn1/l;

.field public static final RL:Lorg/spongycastle/asn1/l;

.field public static final RM:Lorg/spongycastle/asn1/l;

.field public static final RN:Lorg/spongycastle/asn1/l;

.field public static final RO:Lorg/spongycastle/asn1/l;

.field public static final RQ:Lorg/spongycastle/asn1/l;

.field public static final RR:Lorg/spongycastle/asn1/l;

.field public static final RS:Lorg/spongycastle/asn1/l;

.field public static final RU:Lorg/spongycastle/asn1/l;

.field public static final RV:Lorg/spongycastle/asn1/l;

.field public static final RW:Lorg/spongycastle/asn1/l;

.field public static final RX:Lorg/spongycastle/asn1/l;

.field public static final RY:Lorg/spongycastle/asn1/l;

.field public static final RZ:Lorg/spongycastle/asn1/l;

.field public static final Sa:Lorg/spongycastle/asn1/l;

.field public static final Sb:Lorg/spongycastle/asn1/l;

.field public static final Sc:Lorg/spongycastle/asn1/l;

.field public static final Sd:Lorg/spongycastle/asn1/l;

.field public static final Se:Lorg/spongycastle/asn1/l;

.field public static final Sf:Lorg/spongycastle/asn1/l;

.field public static final Sg:Lorg/spongycastle/asn1/l;

.field public static final Sh:Lorg/spongycastle/asn1/l;

.field public static final Si:Lorg/spongycastle/asn1/l;

.field public static final Sj:Lorg/spongycastle/asn1/l;

.field public static final Sk:Lorg/spongycastle/asn1/l;

.field public static final Sl:Lorg/spongycastle/asn1/l;

.field public static final Sm:Lorg/spongycastle/asn1/l;

.field public static final Sn:Lorg/spongycastle/asn1/l;


# instance fields
.field private So:Lorg/spongycastle/asn1/l;

.field Sp:Lorg/spongycastle/asn1/m;

.field critical:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.9"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RH:Lorg/spongycastle/asn1/l;

    .line 25
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.14"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RI:Lorg/spongycastle/asn1/l;

    .line 30
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.15"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RJ:Lorg/spongycastle/asn1/l;

    .line 35
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.16"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RK:Lorg/spongycastle/asn1/l;

    .line 40
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.17"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RL:Lorg/spongycastle/asn1/l;

    .line 45
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.18"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RM:Lorg/spongycastle/asn1/l;

    .line 50
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.19"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RN:Lorg/spongycastle/asn1/l;

    .line 55
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.20"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RO:Lorg/spongycastle/asn1/l;

    .line 60
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.21"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RQ:Lorg/spongycastle/asn1/l;

    .line 65
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.23"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RR:Lorg/spongycastle/asn1/l;

    .line 70
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.24"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RS:Lorg/spongycastle/asn1/l;

    .line 75
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.27"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RU:Lorg/spongycastle/asn1/l;

    .line 80
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.28"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RV:Lorg/spongycastle/asn1/l;

    .line 85
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RW:Lorg/spongycastle/asn1/l;

    .line 90
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.30"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RX:Lorg/spongycastle/asn1/l;

    .line 95
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.31"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RY:Lorg/spongycastle/asn1/l;

    .line 100
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.32"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->RZ:Lorg/spongycastle/asn1/l;

    .line 105
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.33"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sa:Lorg/spongycastle/asn1/l;

    .line 110
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.35"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sb:Lorg/spongycastle/asn1/l;

    .line 115
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.36"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sc:Lorg/spongycastle/asn1/l;

    .line 120
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.37"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sd:Lorg/spongycastle/asn1/l;

    .line 125
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.46"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Se:Lorg/spongycastle/asn1/l;

    .line 130
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.54"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sf:Lorg/spongycastle/asn1/l;

    .line 135
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sg:Lorg/spongycastle/asn1/l;

    .line 140
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.11"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sh:Lorg/spongycastle/asn1/l;

    .line 145
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.12"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Si:Lorg/spongycastle/asn1/l;

    .line 150
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sj:Lorg/spongycastle/asn1/l;

    .line 155
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sk:Lorg/spongycastle/asn1/l;

    .line 160
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sl:Lorg/spongycastle/asn1/l;

    .line 165
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.56"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sm:Lorg/spongycastle/asn1/l;

    .line 170
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.55"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/s;->Sn:Lorg/spongycastle/asn1/l;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/b;Lorg/spongycastle/asn1/m;)V
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p2}, Lorg/spongycastle/asn1/b;->na()Z

    move-result v0

    invoke-direct {p0, p1, v0, p3}, Lorg/spongycastle/asn1/q/s;-><init>(Lorg/spongycastle/asn1/l;ZLorg/spongycastle/asn1/m;)V

    .line 183
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/l;ZLorg/spongycastle/asn1/m;)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    iput-object p1, p0, Lorg/spongycastle/asn1/q/s;->So:Lorg/spongycastle/asn1/l;

    .line 199
    iput-boolean p2, p0, Lorg/spongycastle/asn1/q/s;->critical:Z

    .line 200
    iput-object p3, p0, Lorg/spongycastle/asn1/q/s;->Sp:Lorg/spongycastle/asn1/m;

    .line 201
    return-void
.end method

.method private static a(Lorg/spongycastle/asn1/q/s;)Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    .line 259
    :try_start_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/s;->oT()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 261
    :catch_0
    move-exception v0

    .line 263
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t convert extension: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 236
    instance-of v1, p1, Lorg/spongycastle/asn1/q/s;

    if-nez v1, :cond_1

    .line 243
    :cond_0
    :goto_0
    return v0

    .line 241
    :cond_1
    check-cast p1, Lorg/spongycastle/asn1/q/s;

    .line 243
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/s;->oT()Lorg/spongycastle/asn1/m;

    move-result-object v1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/s;->oT()Lorg/spongycastle/asn1/m;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/s;->isCritical()Z

    move-result v1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/s;->isCritical()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/s;->isCritical()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/s;->oT()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->hashCode()I

    move-result v0

    .line 230
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/s;->oT()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->hashCode()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public isCritical()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lorg/spongycastle/asn1/q/s;->critical:Z

    return v0
.end method

.method public oT()Lorg/spongycastle/asn1/m;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/spongycastle/asn1/q/s;->Sp:Lorg/spongycastle/asn1/m;

    return-object v0
.end method

.method public oU()Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 220
    invoke-static {p0}, Lorg/spongycastle/asn1/q/s;->a(Lorg/spongycastle/asn1/q/s;)Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method
