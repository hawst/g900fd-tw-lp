.class public Lorg/spongycastle/asn1/q/ai;
.super Lorg/spongycastle/asn1/k;
.source "TBSCertList.java"


# instance fields
.field Ku:Lorg/spongycastle/asn1/i;

.field Ri:Lorg/spongycastle/asn1/q/a;

.field SR:Lorg/spongycastle/asn1/p/c;

.field SS:Lorg/spongycastle/asn1/q/ar;

.field SU:Lorg/spongycastle/asn1/q/ar;

.field SV:Lorg/spongycastle/asn1/r;

.field SW:Lorg/spongycastle/asn1/q/t;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 171
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 172
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v2, 0x7

    if-le v0, v2, :cond_1

    .line 174
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_1
    const/4 v0, 0x0

    .line 179
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    instance-of v2, v2, Lorg/spongycastle/asn1/i;

    if-eqz v2, :cond_6

    .line 181
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ai;->Ku:Lorg/spongycastle/asn1/i;

    move v0, v1

    .line 188
    :goto_0
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ai;->Ri:Lorg/spongycastle/asn1/q/a;

    .line 189
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SR:Lorg/spongycastle/asn1/p/c;

    .line 190
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p1, v3}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/q/ar;->bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/ai;->SS:Lorg/spongycastle/asn1/q/ar;

    .line 192
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    instance-of v2, v2, Lorg/spongycastle/asn1/bn;

    if-nez v2, :cond_2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    instance-of v2, v2, Lorg/spongycastle/asn1/ax;

    if-nez v2, :cond_2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    instance-of v2, v2, Lorg/spongycastle/asn1/q/ar;

    if-eqz v2, :cond_3

    .line 197
    :cond_2
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ar;->bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SU:Lorg/spongycastle/asn1/q/ar;

    move v0, v2

    .line 200
    :cond_3
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    instance-of v2, v2, Lorg/spongycastle/asn1/bm;

    if-nez v2, :cond_4

    .line 203
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    move v0, v2

    .line 206
    :cond_4
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    instance-of v2, v2, Lorg/spongycastle/asn1/bm;

    if-eqz v2, :cond_5

    .line 209
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/t;->aN(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SW:Lorg/spongycastle/asn1/q/t;

    .line 211
    :cond_5
    return-void

    .line 185
    :cond_6
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/spongycastle/asn1/q/ai;->Ku:Lorg/spongycastle/asn1/i;

    goto/16 :goto_0
.end method

.method public static aY(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ai;
    .locals 2

    .prologue
    .line 157
    instance-of v0, p0, Lorg/spongycastle/asn1/q/ai;

    if-eqz v0, :cond_0

    .line 159
    check-cast p0, Lorg/spongycastle/asn1/q/ai;

    .line 166
    :goto_0
    return-object p0

    .line 161
    :cond_0
    if-eqz p0, :cond_1

    .line 163
    new-instance v0, Lorg/spongycastle/asn1/q/ai;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/ai;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 166
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    .line 281
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 283
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->Ku:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_0

    .line 285
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 287
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->Ri:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 288
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SR:Lorg/spongycastle/asn1/p/c;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 290
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SS:Lorg/spongycastle/asn1/q/ar;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 291
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SU:Lorg/spongycastle/asn1/q/ar;

    if-eqz v1, :cond_1

    .line 293
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SU:Lorg/spongycastle/asn1/q/ar;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 297
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    if-eqz v1, :cond_2

    .line 299
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 302
    :cond_2
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SW:Lorg/spongycastle/asn1/q/t;

    if-eqz v1, :cond_3

    .line 304
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/spongycastle/asn1/q/ai;->SW:Lorg/spongycastle/asn1/q/t;

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 307
    :cond_3
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public oE()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SR:Lorg/spongycastle/asn1/p/c;

    return-object v0
.end method

.method public oG()[Lorg/spongycastle/asn1/q/aj;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 249
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    if-nez v1, :cond_0

    .line 251
    new-array v0, v0, [Lorg/spongycastle/asn1/q/aj;

    .line 261
    :goto_0
    return-object v0

    .line 254
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    new-array v1, v1, [Lorg/spongycastle/asn1/q/aj;

    .line 256
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 258
    iget-object v2, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    invoke-virtual {v2, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/q/aj;->aZ(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/aj;

    move-result-object v2

    aput-object v2, v1, v0

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 261
    goto :goto_0
.end method

.method public oH()Ljava/util/Enumeration;
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    if-nez v0, :cond_0

    .line 268
    new-instance v0, Lorg/spongycastle/asn1/q/ak;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/spongycastle/asn1/q/ak;-><init>(Lorg/spongycastle/asn1/q/ai;Lorg/spongycastle/asn1/q/ai$1;)V

    .line 271
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/q/al;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/ai;->SV:Lorg/spongycastle/asn1/r;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/spongycastle/asn1/q/al;-><init>(Lorg/spongycastle/asn1/q/ai;Ljava/util/Enumeration;)V

    goto :goto_0
.end method

.method public oK()I
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->Ku:Lorg/spongycastle/asn1/i;

    if-nez v0, :cond_0

    .line 217
    const/4 v0, 0x1

    .line 219
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public oL()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SS:Lorg/spongycastle/asn1/q/ar;

    return-object v0
.end method

.method public oM()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SU:Lorg/spongycastle/asn1/q/ar;

    return-object v0
.end method

.method public oz()Lorg/spongycastle/asn1/q/t;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->SW:Lorg/spongycastle/asn1/q/t;

    return-object v0
.end method

.method public ps()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ai;->Ri:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method
