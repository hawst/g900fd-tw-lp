.class public Lorg/spongycastle/asn1/q/aw;
.super Lorg/spongycastle/asn1/k;
.source "X509Name.java"


# static fields
.field private static final FALSE:Ljava/lang/Boolean;

.field public static final QA:Lorg/spongycastle/asn1/l;

.field public static final QB:Lorg/spongycastle/asn1/l;

.field public static final QC:Lorg/spongycastle/asn1/l;

.field public static final QD:Lorg/spongycastle/asn1/l;

.field public static final QE:Lorg/spongycastle/asn1/l;

.field public static final QF:Lorg/spongycastle/asn1/l;

.field public static final QG:Lorg/spongycastle/asn1/l;

.field public static final QH:Lorg/spongycastle/asn1/l;

.field public static final QI:Lorg/spongycastle/asn1/l;

.field public static final QJ:Lorg/spongycastle/asn1/l;

.field public static final QK:Lorg/spongycastle/asn1/l;

.field public static final QL:Lorg/spongycastle/asn1/l;

.field public static final QM:Lorg/spongycastle/asn1/l;

.field public static final QN:Lorg/spongycastle/asn1/l;

.field public static final QO:Lorg/spongycastle/asn1/l;

.field public static final QP:Lorg/spongycastle/asn1/l;

.field public static final QQ:Lorg/spongycastle/asn1/l;

.field public static final QS:Lorg/spongycastle/asn1/l;

.field public static final QT:Ljava/util/Hashtable;

.field public static final QU:Ljava/util/Hashtable;

.field public static final Qj:Lorg/spongycastle/asn1/l;

.field public static final Qk:Lorg/spongycastle/asn1/l;

.field public static final Ql:Lorg/spongycastle/asn1/l;

.field public static final Qm:Lorg/spongycastle/asn1/l;

.field public static final Qn:Lorg/spongycastle/asn1/l;

.field public static final Qo:Lorg/spongycastle/asn1/l;

.field public static final Qp:Lorg/spongycastle/asn1/l;

.field public static final Qq:Lorg/spongycastle/asn1/l;

.field public static final Qr:Lorg/spongycastle/asn1/l;

.field public static final Qs:Lorg/spongycastle/asn1/l;

.field public static final Qt:Lorg/spongycastle/asn1/l;

.field public static final Qu:Lorg/spongycastle/asn1/l;

.field public static final Qv:Lorg/spongycastle/asn1/l;

.field public static final Qw:Lorg/spongycastle/asn1/l;

.field public static final Qx:Lorg/spongycastle/asn1/l;

.field public static final Qy:Lorg/spongycastle/asn1/l;

.field public static final Qz:Lorg/spongycastle/asn1/l;

.field private static final TRUE:Ljava/lang/Boolean;

.field public static TS:Z

.field public static final TT:Ljava/util/Hashtable;

.field public static final TU:Ljava/util/Hashtable;

.field public static final TV:Ljava/util/Hashtable;

.field public static final TW:Ljava/util/Hashtable;


# instance fields
.field private NQ:Lorg/spongycastle/asn1/r;

.field private Qc:Z

.field private Qd:I

.field private Sr:Ljava/util/Vector;

.field private TX:Lorg/spongycastle/asn1/q/ax;

.field private TY:Ljava/util/Vector;

.field private TZ:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.6"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qj:Lorg/spongycastle/asn1/l;

    .line 51
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.10"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qk:Lorg/spongycastle/asn1/l;

    .line 57
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.11"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Ql:Lorg/spongycastle/asn1/l;

    .line 63
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.12"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qm:Lorg/spongycastle/asn1/l;

    .line 69
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qn:Lorg/spongycastle/asn1/l;

    .line 74
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.5"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qo:Lorg/spongycastle/asn1/l;

    .line 79
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.9"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qp:Lorg/spongycastle/asn1/l;

    .line 84
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->Qo:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qq:Lorg/spongycastle/asn1/l;

    .line 89
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qr:Lorg/spongycastle/asn1/l;

    .line 94
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.8"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qs:Lorg/spongycastle/asn1/l;

    .line 99
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qt:Lorg/spongycastle/asn1/l;

    .line 100
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.42"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qu:Lorg/spongycastle/asn1/l;

    .line 101
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.43"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qv:Lorg/spongycastle/asn1/l;

    .line 102
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.44"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qw:Lorg/spongycastle/asn1/l;

    .line 103
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.45"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qx:Lorg/spongycastle/asn1/l;

    .line 108
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.15"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qy:Lorg/spongycastle/asn1/l;

    .line 114
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.17"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->Qz:Lorg/spongycastle/asn1/l;

    .line 120
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.46"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QA:Lorg/spongycastle/asn1/l;

    .line 126
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.65"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QB:Lorg/spongycastle/asn1/l;

    .line 133
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.9.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QC:Lorg/spongycastle/asn1/l;

    .line 139
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.9.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QD:Lorg/spongycastle/asn1/l;

    .line 145
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.9.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QE:Lorg/spongycastle/asn1/l;

    .line 152
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.9.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QF:Lorg/spongycastle/asn1/l;

    .line 159
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.9.5"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QG:Lorg/spongycastle/asn1/l;

    .line 166
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.36.8.3.14"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QH:Lorg/spongycastle/asn1/l;

    .line 172
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.16"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QI:Lorg/spongycastle/asn1/l;

    .line 177
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.54"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QJ:Lorg/spongycastle/asn1/l;

    .line 182
    sget-object v0, Lorg/spongycastle/asn1/q/ay;->Ug:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QK:Lorg/spongycastle/asn1/l;

    .line 187
    sget-object v0, Lorg/spongycastle/asn1/q/ay;->Uh:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QL:Lorg/spongycastle/asn1/l;

    .line 194
    sget-object v0, Lorg/spongycastle/asn1/l/q;->LC:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QM:Lorg/spongycastle/asn1/l;

    .line 199
    sget-object v0, Lorg/spongycastle/asn1/l/q;->LD:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QN:Lorg/spongycastle/asn1/l;

    .line 200
    sget-object v0, Lorg/spongycastle/asn1/l/q;->LJ:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QO:Lorg/spongycastle/asn1/l;

    .line 205
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QM:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QP:Lorg/spongycastle/asn1/l;

    .line 210
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "0.9.2342.19200300.100.1.25"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QQ:Lorg/spongycastle/asn1/l;

    .line 215
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "0.9.2342.19200300.100.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QS:Lorg/spongycastle/asn1/l;

    .line 221
    sput-boolean v2, Lorg/spongycastle/asn1/q/aw;->TS:Z

    .line 227
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    .line 233
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    .line 239
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    .line 244
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    .line 250
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->TV:Ljava/util/Hashtable;

    .line 256
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->TW:Ljava/util/Hashtable;

    .line 258
    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->TRUE:Ljava/lang/Boolean;

    .line 259
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v2}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lorg/spongycastle/asn1/q/aw;->FALSE:Ljava/lang/Boolean;

    .line 263
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qj:Lorg/spongycastle/asn1/l;

    const-string v2, "C"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qk:Lorg/spongycastle/asn1/l;

    const-string v2, "O"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qm:Lorg/spongycastle/asn1/l;

    const-string v2, "T"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Ql:Lorg/spongycastle/asn1/l;

    const-string v2, "OU"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qn:Lorg/spongycastle/asn1/l;

    const-string v2, "CN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qr:Lorg/spongycastle/asn1/l;

    const-string v2, "L"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qs:Lorg/spongycastle/asn1/l;

    const-string v2, "ST"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qo:Lorg/spongycastle/asn1/l;

    const-string v2, "SERIALNUMBER"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QM:Lorg/spongycastle/asn1/l;

    const-string v2, "E"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QQ:Lorg/spongycastle/asn1/l;

    const-string v2, "DC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QS:Lorg/spongycastle/asn1/l;

    const-string v2, "UID"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qp:Lorg/spongycastle/asn1/l;

    const-string v2, "STREET"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qt:Lorg/spongycastle/asn1/l;

    const-string v2, "SURNAME"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qu:Lorg/spongycastle/asn1/l;

    const-string v2, "GIVENNAME"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qv:Lorg/spongycastle/asn1/l;

    const-string v2, "INITIALS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qw:Lorg/spongycastle/asn1/l;

    const-string v2, "GENERATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QO:Lorg/spongycastle/asn1/l;

    const-string v2, "unstructuredAddress"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QN:Lorg/spongycastle/asn1/l;

    const-string v2, "unstructuredName"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qx:Lorg/spongycastle/asn1/l;

    const-string v2, "UniqueIdentifier"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QA:Lorg/spongycastle/asn1/l;

    const-string v2, "DN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QB:Lorg/spongycastle/asn1/l;

    const-string v2, "Pseudonym"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QI:Lorg/spongycastle/asn1/l;

    const-string v2, "PostalAddress"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QH:Lorg/spongycastle/asn1/l;

    const-string v2, "NameAtBirth"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QF:Lorg/spongycastle/asn1/l;

    const-string v2, "CountryOfCitizenship"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QG:Lorg/spongycastle/asn1/l;

    const-string v2, "CountryOfResidence"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QE:Lorg/spongycastle/asn1/l;

    const-string v2, "Gender"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QD:Lorg/spongycastle/asn1/l;

    const-string v2, "PlaceOfBirth"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QC:Lorg/spongycastle/asn1/l;

    const-string v2, "DateOfBirth"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qz:Lorg/spongycastle/asn1/l;

    const-string v2, "PostalCode"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qy:Lorg/spongycastle/asn1/l;

    const-string v2, "BusinessCategory"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QK:Lorg/spongycastle/asn1/l;

    const-string v2, "TelephoneNumber"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QL:Lorg/spongycastle/asn1/l;

    const-string v2, "Name"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qj:Lorg/spongycastle/asn1/l;

    const-string v2, "C"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qk:Lorg/spongycastle/asn1/l;

    const-string v2, "O"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Ql:Lorg/spongycastle/asn1/l;

    const-string v2, "OU"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qn:Lorg/spongycastle/asn1/l;

    const-string v2, "CN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qr:Lorg/spongycastle/asn1/l;

    const-string v2, "L"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qs:Lorg/spongycastle/asn1/l;

    const-string v2, "ST"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qp:Lorg/spongycastle/asn1/l;

    const-string v2, "STREET"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QQ:Lorg/spongycastle/asn1/l;

    const-string v2, "DC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TT:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QS:Lorg/spongycastle/asn1/l;

    const-string v2, "UID"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qj:Lorg/spongycastle/asn1/l;

    const-string v2, "C"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qk:Lorg/spongycastle/asn1/l;

    const-string v2, "O"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Ql:Lorg/spongycastle/asn1/l;

    const-string v2, "OU"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qn:Lorg/spongycastle/asn1/l;

    const-string v2, "CN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qr:Lorg/spongycastle/asn1/l;

    const-string v2, "L"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qs:Lorg/spongycastle/asn1/l;

    const-string v2, "ST"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TU:Ljava/util/Hashtable;

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->Qp:Lorg/spongycastle/asn1/l;

    const-string v2, "STREET"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "c"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qj:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "o"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qk:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "t"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qm:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "ou"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Ql:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "cn"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qn:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "l"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qr:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "st"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qs:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "sn"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qo:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "serialnumber"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qo:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "street"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qp:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "emailaddress"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QP:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "dc"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QQ:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "e"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QP:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "uid"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QS:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "surname"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qt:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "givenname"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qu:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "initials"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qv:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "generation"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qw:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "unstructuredaddress"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QO:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "unstructuredname"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QN:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "uniqueidentifier"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qx:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "dn"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QA:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "pseudonym"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QB:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "postaladdress"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QI:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "nameofbirth"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QH:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "countryofcitizenship"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QF:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "countryofresidence"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QG:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "gender"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QE:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "placeofbirth"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QD:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "dateofbirth"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QC:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "postalcode"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qz:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "businesscategory"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->Qy:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "telephonenumber"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QK:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->QU:Ljava/util/Hashtable;

    const-string v1, "name"

    sget-object v2, Lorg/spongycastle/asn1/q/aw;->QL:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 394
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TX:Lorg/spongycastle/asn1/q/ax;

    .line 351
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    .line 352
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    .line 353
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TZ:Ljava/util/Vector;

    .line 396
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 404
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TX:Lorg/spongycastle/asn1/q/ax;

    .line 351
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    .line 352
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    .line 353
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TZ:Ljava/util/Vector;

    .line 405
    iput-object p1, p0, Lorg/spongycastle/asn1/q/aw;->NQ:Lorg/spongycastle/asn1/r;

    .line 407
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v3

    .line 409
    :cond_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 411
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/t;->L(Ljava/lang/Object;)Lorg/spongycastle/asn1/t;

    move-result-object v4

    move v1, v2

    .line 413
    :goto_0
    invoke-virtual {v4}, Lorg/spongycastle/asn1/t;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 415
    invoke-virtual {v4, v1}, Lorg/spongycastle/asn1/t;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    .line 417
    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->size()I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    .line 419
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "badly sized pair"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_1
    iget-object v5, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v6

    invoke-static {v6}, Lorg/spongycastle/asn1/l;->T(Ljava/lang/Object;)Lorg/spongycastle/asn1/l;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 424
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    .line 425
    instance-of v5, v0, Lorg/spongycastle/asn1/w;

    if-eqz v5, :cond_3

    instance-of v5, v0, Lorg/spongycastle/asn1/bp;

    if-nez v5, :cond_3

    .line 427
    check-cast v0, Lorg/spongycastle/asn1/w;

    invoke-interface {v0}, Lorg/spongycastle/asn1/w;->getString()Ljava/lang/String;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x23

    if-ne v5, v6, :cond_2

    .line 430
    iget-object v5, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\\"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 448
    :goto_1
    iget-object v5, p0, Lorg/spongycastle/asn1/q/aw;->TZ:Ljava/util/Vector;

    if-eqz v1, :cond_4

    sget-object v0, Lorg/spongycastle/asn1/q/aw;->TRUE:Ljava/lang/Boolean;

    :goto_2
    invoke-virtual {v5, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 413
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 434
    :cond_2
    iget-object v5, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 441
    :cond_3
    :try_start_0
    iget-object v5, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    const-string v7, "DER"

    invoke-virtual {v0, v7}, Lorg/spongycastle/asn1/q;->getEncoded(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/util/a/d;->i([B)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/q/aw;->o([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 443
    :catch_0
    move-exception v0

    .line 445
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot encode value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_4
    sget-object v0, Lorg/spongycastle/asn1/q/aw;->FALSE:Ljava/lang/Boolean;

    goto :goto_2

    .line 451
    :cond_5
    return-void
.end method

.method private J(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1065
    invoke-direct {p0, p1}, Lorg/spongycastle/asn1/q/aw;->cL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1066
    invoke-direct {p0, p2}, Lorg/spongycastle/asn1/q/aw;->cL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1068
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1070
    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/q/aw;->cN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1071
    invoke-direct {p0, v1}, Lorg/spongycastle/asn1/q/aw;->cN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1073
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1075
    const/4 v0, 0x0

    .line 1079
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/lang/StringBuffer;Ljava/util/Hashtable;Lorg/spongycastle/asn1/l;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x5c

    const/16 v4, 0x3d

    .line 1142
    invoke-virtual {p2, p3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1144
    if-eqz v0, :cond_3

    .line 1146
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1153
    :goto_0
    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1155
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 1157
    invoke-virtual {p1, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1159
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 1161
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_0

    .line 1163
    add-int/lit8 v1, v1, 0x2

    .line 1166
    :cond_0
    :goto_1
    if-eq v1, v0, :cond_4

    .line 1168
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    const/16 v3, 0x2c

    if-eq v2, v3, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    const/16 v3, 0x22

    if-eq v2, v3, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    const/16 v3, 0x2b

    if-eq v2, v3, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    if-eq v2, v4, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    const/16 v3, 0x3c

    if-eq v2, v3, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    const/16 v3, 0x3e

    if-eq v2, v3, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    const/16 v3, 0x3b

    if-ne v2, v3, :cond_2

    .line 1177
    :cond_1
    const-string v2, "\\"

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 1178
    add-int/lit8 v1, v1, 0x1

    .line 1179
    add-int/lit8 v0, v0, 0x1

    .line 1182
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1150
    :cond_3
    invoke-virtual {p3}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1184
    :cond_4
    return-void
.end method

.method public static bj(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/aw;
    .locals 2

    .prologue
    .line 377
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/aw;

    if-eqz v0, :cond_1

    .line 379
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/aw;

    .line 390
    :goto_0
    return-object p0

    .line 381
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/p/c;

    if-eqz v0, :cond_2

    .line 383
    new-instance v0, Lorg/spongycastle/asn1/q/aw;

    check-cast p0, Lorg/spongycastle/asn1/p/c;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/p/c;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/aw;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 385
    :cond_2
    if-eqz p0, :cond_3

    .line 387
    new-instance v0, Lorg/spongycastle/asn1/q/aw;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/aw;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 390
    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private cL(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1084
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/util/g;->da(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1086
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x23

    if-ne v0, v2, :cond_0

    .line 1088
    invoke-direct {p0, v1}, Lorg/spongycastle/asn1/q/aw;->cM(Ljava/lang/String;)Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 1090
    instance-of v2, v0, Lorg/spongycastle/asn1/w;

    if-eqz v2, :cond_0

    .line 1092
    check-cast v0, Lorg/spongycastle/asn1/w;

    invoke-interface {v0}, Lorg/spongycastle/asn1/w;->getString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/util/g;->da(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1096
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private cM(Ljava/lang/String;)Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    .line 1103
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/util/a/d;->dc(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1105
    :catch_0
    move-exception v0

    .line 1107
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown encoding in name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private cN(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 1114
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1116
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1118
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1120
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1122
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1124
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1125
    if-ne v1, v4, :cond_0

    if-eq v2, v4, :cond_1

    .line 1127
    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1122
    :cond_1
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 1133
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o([B)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1266
    array-length v0, p1

    new-array v1, v0, [C

    .line 1268
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-eq v0, v2, :cond_0

    .line 1270
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    int-to-char v2, v2

    aput-char v2, v1, v0

    .line 1268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1273
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method


# virtual methods
.method public a(ZLjava/util/Hashtable;)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x2c

    const/4 v4, 0x0

    .line 1202
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 1203
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 1204
    const/4 v5, 0x1

    .line 1206
    const/4 v2, 0x0

    move v3, v4

    .line 1208
    :goto_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 1210
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TZ:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212
    const/16 v0, 0x2b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1213
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v2, p2, v0, v1}, Lorg/spongycastle/asn1/q/aw;->a(Ljava/lang/StringBuffer;Ljava/util/Hashtable;Lorg/spongycastle/asn1/l;Ljava/lang/String;)V

    move-object v0, v2

    .line 1208
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v2, v0

    goto :goto_0

    .line 1219
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1220
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v2, p2, v0, v1}, Lorg/spongycastle/asn1/q/aw;->a(Ljava/lang/StringBuffer;Ljava/util/Hashtable;Lorg/spongycastle/asn1/l;Ljava/lang/String;)V

    .line 1223
    invoke-virtual {v7, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_1

    .line 1227
    :cond_1
    if-eqz p1, :cond_3

    .line 1229
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    move v0, v5

    :goto_2
    if-ltz v1, :cond_5

    .line 1231
    if-eqz v0, :cond_2

    move v0, v4

    .line 1240
    :goto_3
    invoke-virtual {v7, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1229
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 1237
    :cond_2
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_3
    move v0, v4

    move v1, v5

    .line 1245
    :goto_4
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 1247
    if-eqz v1, :cond_4

    move v1, v4

    .line 1256
    :goto_5
    invoke-virtual {v7, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1245
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1253
    :cond_4
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 1260
    :cond_5
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 12

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 975
    if-ne p1, p0, :cond_1

    move v7, v4

    .line 1060
    :cond_0
    :goto_0
    return v7

    .line 980
    :cond_1
    instance-of v0, p1, Lorg/spongycastle/asn1/q/aw;

    if-nez v0, :cond_2

    instance-of v0, p1, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_0

    :cond_2
    move-object v0, p1

    .line 985
    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 987
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/aw;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/spongycastle/asn1/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v7, v4

    .line 989
    goto :goto_0

    .line 996
    :cond_3
    :try_start_0
    invoke-static {p1}, Lorg/spongycastle/asn1/q/aw;->bj(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/aw;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 1003
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v6

    .line 1005
    iget-object v0, v10, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ne v6, v0, :cond_0

    .line 1010
    new-array v11, v6, [Z

    .line 1013
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0, v7}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, v10, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v2, v7}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v3, v4

    move v5, v6

    move v0, v7

    :goto_1
    move v9, v0

    .line 1026
    :goto_2
    if-eq v9, v5, :cond_7

    .line 1029
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0, v9}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 1030
    iget-object v1, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v1, v9}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move v8, v7

    .line 1032
    :goto_3
    if-ge v8, v6, :cond_8

    .line 1034
    aget-boolean v2, v11, v8

    if-eqz v2, :cond_6

    .line 1032
    :cond_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_3

    .line 1021
    :cond_5
    add-int/lit8 v0, v6, -0x1

    move v3, v1

    move v5, v1

    .line 1023
    goto :goto_1

    .line 1039
    :cond_6
    iget-object v2, v10, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v2, v8}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/spongycastle/asn1/l;

    .line 1041
    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1043
    iget-object v2, v10, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v2, v8}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1045
    invoke-direct {p0, v1, v2}, Lorg/spongycastle/asn1/q/aw;->J(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1047
    aput-boolean v4, v11, v8

    move v0, v4

    .line 1054
    :goto_4
    if-eqz v0, :cond_0

    .line 1026
    add-int v0, v9, v3

    move v9, v0

    goto :goto_2

    :cond_7
    move v7, v4

    .line 1060
    goto/16 :goto_0

    .line 998
    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_8
    move v0, v7

    goto :goto_4
.end method

.method public f(Lorg/spongycastle/asn1/l;)Ljava/util/Vector;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 811
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move v1, v2

    .line 813
    :goto_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-eq v1, v0, :cond_2

    .line 815
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 819
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5c

    if-ne v4, v5, :cond_1

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x23

    if-ne v4, v5, :cond_1

    .line 821
    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 813
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 825
    :cond_1
    invoke-virtual {v3, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 830
    :cond_2
    return-object v3
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 948
    iget-boolean v0, p0, Lorg/spongycastle/asn1/q/aw;->Qc:Z

    if-eqz v0, :cond_0

    .line 950
    iget v0, p0, Lorg/spongycastle/asn1/q/aw;->Qd:I

    .line 967
    :goto_0
    return v0

    .line 953
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/aw;->Qc:Z

    .line 956
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-eq v1, v0, :cond_1

    .line 958
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 960
    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/q/aw;->cL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 961
    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/q/aw;->cN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 963
    iget v2, p0, Lorg/spongycastle/asn1/q/aw;->Qd:I

    iget-object v3, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    xor-int/2addr v2, v3

    iput v2, p0, Lorg/spongycastle/asn1/q/aw;->Qd:I

    .line 964
    iget v2, p0, Lorg/spongycastle/asn1/q/aw;->Qd:I

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    iput v0, p0, Lorg/spongycastle/asn1/q/aw;->Qd:I

    .line 956
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 967
    :cond_1
    iget v0, p0, Lorg/spongycastle/asn1/q/aw;->Qd:I

    goto :goto_0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 8

    .prologue
    .line 835
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->NQ:Lorg/spongycastle/asn1/r;

    if-nez v0, :cond_3

    .line 837
    new-instance v5, Lorg/spongycastle/asn1/e;

    invoke-direct {v5}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 838
    new-instance v2, Lorg/spongycastle/asn1/e;

    invoke-direct {v2}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 839
    const/4 v1, 0x0

    .line 841
    const/4 v0, 0x0

    move-object v3, v1

    move-object v4, v2

    move v2, v0

    :goto_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-eq v2, v0, :cond_2

    .line 843
    new-instance v6, Lorg/spongycastle/asn1/e;

    invoke-direct {v6}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 844
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->Sr:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 846
    invoke-virtual {v6, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 848
    iget-object v1, p0, Lorg/spongycastle/asn1/q/aw;->TY:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 850
    iget-object v7, p0, Lorg/spongycastle/asn1/q/aw;->TX:Lorg/spongycastle/asn1/q/ax;

    invoke-virtual {v7, v0, v1}, Lorg/spongycastle/asn1/q/ax;->c(Lorg/spongycastle/asn1/l;Ljava/lang/String;)Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v6, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 852
    if-eqz v3, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/q/aw;->TZ:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 855
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v6}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v4, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    move-object v3, v4

    .line 841
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v4, v3

    move-object v3, v0

    goto :goto_0

    .line 859
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/bj;

    invoke-direct {v1, v4}, Lorg/spongycastle/asn1/bj;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v5, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 860
    new-instance v3, Lorg/spongycastle/asn1/e;

    invoke-direct {v3}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 862
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v6}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v3, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_1

    .line 868
    :cond_2
    new-instance v0, Lorg/spongycastle/asn1/bj;

    invoke-direct {v0, v4}, Lorg/spongycastle/asn1/bj;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v5, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 870
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v5}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aw;->NQ:Lorg/spongycastle/asn1/r;

    .line 873
    :cond_3
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aw;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1278
    sget-boolean v0, Lorg/spongycastle/asn1/q/aw;->TS:Z

    sget-object v1, Lorg/spongycastle/asn1/q/aw;->QT:Ljava/util/Hashtable;

    invoke-virtual {p0, v0, v1}, Lorg/spongycastle/asn1/q/aw;->a(ZLjava/util/Hashtable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
