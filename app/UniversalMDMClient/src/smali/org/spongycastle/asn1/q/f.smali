.class public Lorg/spongycastle/asn1/q/f;
.super Lorg/spongycastle/asn1/k;
.source "AttributeCertificateInfo.java"


# instance fields
.field private Ku:Lorg/spongycastle/asn1/i;

.field private Rg:Lorg/spongycastle/asn1/q/x;

.field private Rh:Lorg/spongycastle/asn1/q/b;

.field private Ri:Lorg/spongycastle/asn1/q/a;

.field private Rj:Lorg/spongycastle/asn1/i;

.field private Rk:Lorg/spongycastle/asn1/q/c;

.field private Rl:Lorg/spongycastle/asn1/r;

.field private Rm:Lorg/spongycastle/asn1/aq;

.field private Rn:Lorg/spongycastle/asn1/q/t;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    const/4 v0, 0x7

    .line 50
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 51
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    if-lt v1, v0, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    const/16 v2, 0x9

    if-le v1, v2, :cond_1

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Ku:Lorg/spongycastle/asn1/i;

    .line 57
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/x;->aR(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/x;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rg:Lorg/spongycastle/asn1/q/x;

    .line 58
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/b;->ax(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/b;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rh:Lorg/spongycastle/asn1/q/b;

    .line 59
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Ri:Lorg/spongycastle/asn1/q/a;

    .line 60
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rj:Lorg/spongycastle/asn1/i;

    .line 61
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/c;->ay(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/c;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rk:Lorg/spongycastle/asn1/q/c;

    .line 62
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rl:Lorg/spongycastle/asn1/r;

    .line 64
    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 66
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    .line 68
    instance-of v2, v1, Lorg/spongycastle/asn1/aq;

    if-eqz v2, :cond_3

    .line 70
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/aq;->N(Ljava/lang/Object;)Lorg/spongycastle/asn1/aq;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rm:Lorg/spongycastle/asn1/aq;

    .line 64
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_3
    instance-of v2, v1, Lorg/spongycastle/asn1/r;

    if-nez v2, :cond_4

    instance-of v1, v1, Lorg/spongycastle/asn1/q/t;

    if-eqz v1, :cond_2

    .line 74
    :cond_4
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/t;->aN(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/t;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rn:Lorg/spongycastle/asn1/q/t;

    goto :goto_1

    .line 77
    :cond_5
    return-void
.end method

.method public static aB(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/f;
    .locals 2

    .prologue
    .line 36
    instance-of v0, p0, Lorg/spongycastle/asn1/q/f;

    if-eqz v0, :cond_0

    .line 38
    check-cast p0, Lorg/spongycastle/asn1/q/f;

    .line 45
    :goto_0
    return-object p0

    .line 40
    :cond_0
    if-eqz p0, :cond_1

    .line 42
    new-instance v0, Lorg/spongycastle/asn1/q/f;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/f;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 45
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 144
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 146
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 147
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rg:Lorg/spongycastle/asn1/q/x;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 148
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rh:Lorg/spongycastle/asn1/q/b;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 149
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Ri:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 150
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rj:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 151
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rk:Lorg/spongycastle/asn1/q/c;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 152
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rl:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 154
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rm:Lorg/spongycastle/asn1/aq;

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rm:Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 159
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rn:Lorg/spongycastle/asn1/q/t;

    if-eqz v1, :cond_1

    .line 161
    iget-object v1, p0, Lorg/spongycastle/asn1/q/f;->Rn:Lorg/spongycastle/asn1/q/t;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 164
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public ou()Lorg/spongycastle/asn1/q/x;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/spongycastle/asn1/q/f;->Rg:Lorg/spongycastle/asn1/q/x;

    return-object v0
.end method

.method public ov()Lorg/spongycastle/asn1/q/b;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/spongycastle/asn1/q/f;->Rh:Lorg/spongycastle/asn1/q/b;

    return-object v0
.end method

.method public ow()Lorg/spongycastle/asn1/i;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/spongycastle/asn1/q/f;->Rj:Lorg/spongycastle/asn1/i;

    return-object v0
.end method

.method public ox()Lorg/spongycastle/asn1/q/c;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/spongycastle/asn1/q/f;->Rk:Lorg/spongycastle/asn1/q/c;

    return-object v0
.end method

.method public oy()Lorg/spongycastle/asn1/r;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/spongycastle/asn1/q/f;->Rl:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public oz()Lorg/spongycastle/asn1/q/t;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/spongycastle/asn1/q/f;->Rn:Lorg/spongycastle/asn1/q/t;

    return-object v0
.end method
