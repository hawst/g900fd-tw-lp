.class public Lorg/spongycastle/asn1/q/p;
.super Lorg/spongycastle/asn1/k;
.source "DigestInfo.java"


# instance fields
.field private algId:Lorg/spongycastle/asn1/q/a;

.field private digest:[B


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/a;[B)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 54
    iput-object p2, p0, Lorg/spongycastle/asn1/q/p;->digest:[B

    .line 55
    iput-object p1, p0, Lorg/spongycastle/asn1/q/p;->algId:Lorg/spongycastle/asn1/q/a;

    .line 56
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 61
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 63
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/p;->algId:Lorg/spongycastle/asn1/q/a;

    .line 64
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/m;->J(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/p;->digest:[B

    .line 65
    return-void
.end method

.method public static aK(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/p;
    .locals 2

    .prologue
    .line 38
    instance-of v0, p0, Lorg/spongycastle/asn1/q/p;

    if-eqz v0, :cond_0

    .line 40
    check-cast p0, Lorg/spongycastle/asn1/q/p;

    .line 47
    :goto_0
    return-object p0

    .line 42
    :cond_0
    if-eqz p0, :cond_1

    .line 44
    new-instance v0, Lorg/spongycastle/asn1/q/p;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/p;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 47
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDigest()[B
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/asn1/q/p;->digest:[B

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 79
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 81
    iget-object v1, p0, Lorg/spongycastle/asn1/q/p;->algId:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 82
    new-instance v1, Lorg/spongycastle/asn1/bd;

    iget-object v2, p0, Lorg/spongycastle/asn1/q/p;->digest:[B

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 84
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nJ()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/spongycastle/asn1/q/p;->algId:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method
