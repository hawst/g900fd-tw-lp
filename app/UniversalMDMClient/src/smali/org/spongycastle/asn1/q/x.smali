.class public Lorg/spongycastle/asn1/q/x;
.super Lorg/spongycastle/asn1/k;
.source "Holder.java"


# instance fields
.field Sw:Lorg/spongycastle/asn1/q/y;

.field Sx:Lorg/spongycastle/asn1/q/v;

.field Sy:Lorg/spongycastle/asn1/q/ac;

.field private version:I


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 52
    iput v4, p0, Lorg/spongycastle/asn1/q/x;->version:I

    .line 100
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_0

    .line 102
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 106
    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 108
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v2

    .line 111
    invoke-virtual {v2}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown tag in Holder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :pswitch_0
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/q/y;->o(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/y;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/x;->Sw:Lorg/spongycastle/asn1/q/y;

    .line 106
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :pswitch_1
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/q/v;->n(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/v;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/x;->Sx:Lorg/spongycastle/asn1/q/v;

    goto :goto_1

    .line 120
    :pswitch_2
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/q/ac;->p(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/ac;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/x;->Sy:Lorg/spongycastle/asn1/q/ac;

    goto :goto_1

    .line 126
    :cond_1
    iput v4, p0, Lorg/spongycastle/asn1/q/x;->version:I

    .line 127
    return-void

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/x;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lorg/spongycastle/asn1/q/x;->version:I

    .line 79
    invoke-virtual {p1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown tag in Holder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :pswitch_0
    invoke-static {p1, v1}, Lorg/spongycastle/asn1/q/y;->o(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/y;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sw:Lorg/spongycastle/asn1/q/y;

    .line 90
    :goto_0
    iput v1, p0, Lorg/spongycastle/asn1/q/x;->version:I

    .line 91
    return-void

    .line 85
    :pswitch_1
    invoke-static {p1, v1}, Lorg/spongycastle/asn1/q/v;->n(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/v;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sx:Lorg/spongycastle/asn1/q/v;

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static aR(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/x;
    .locals 2

    .prologue
    .line 56
    instance-of v0, p0, Lorg/spongycastle/asn1/q/x;

    if-eqz v0, :cond_0

    .line 58
    check-cast p0, Lorg/spongycastle/asn1/q/x;

    .line 69
    :goto_0
    return-object p0

    .line 60
    :cond_0
    instance-of v0, p0, Lorg/spongycastle/asn1/x;

    if-eqz v0, :cond_1

    .line 62
    new-instance v0, Lorg/spongycastle/asn1/q/x;

    invoke-static {p0}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/x;-><init>(Lorg/spongycastle/asn1/x;)V

    move-object p0, v0

    goto :goto_0

    .line 64
    :cond_1
    if-eqz p0, :cond_2

    .line 66
    new-instance v0, Lorg/spongycastle/asn1/q/x;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/x;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 69
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 212
    iget v0, p0, Lorg/spongycastle/asn1/q/x;->version:I

    if-ne v0, v3, :cond_3

    .line 214
    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 216
    iget-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sw:Lorg/spongycastle/asn1/q/y;

    if-eqz v0, :cond_0

    .line 218
    new-instance v0, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/q/x;->Sw:Lorg/spongycastle/asn1/q/y;

    invoke-direct {v0, v4, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 221
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sx:Lorg/spongycastle/asn1/q/v;

    if-eqz v0, :cond_1

    .line 223
    new-instance v0, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/q/x;->Sx:Lorg/spongycastle/asn1/q/v;

    invoke-direct {v0, v4, v3, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 226
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sy:Lorg/spongycastle/asn1/q/ac;

    if-eqz v0, :cond_2

    .line 228
    new-instance v0, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/spongycastle/asn1/q/x;->Sy:Lorg/spongycastle/asn1/q/ac;

    invoke-direct {v0, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 231
    :cond_2
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    .line 241
    :goto_0
    return-object v0

    .line 235
    :cond_3
    iget-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sx:Lorg/spongycastle/asn1/q/v;

    if-eqz v0, :cond_4

    .line 237
    new-instance v0, Lorg/spongycastle/asn1/bm;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/x;->Sx:Lorg/spongycastle/asn1/q/v;

    invoke-direct {v0, v4, v3, v1}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 241
    :cond_4
    new-instance v0, Lorg/spongycastle/asn1/bm;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/x;->Sw:Lorg/spongycastle/asn1/q/y;

    invoke-direct {v0, v4, v4, v1}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method public oY()Lorg/spongycastle/asn1/q/y;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sw:Lorg/spongycastle/asn1/q/y;

    return-object v0
.end method

.method public oZ()Lorg/spongycastle/asn1/q/v;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sx:Lorg/spongycastle/asn1/q/v;

    return-object v0
.end method

.method public pa()Lorg/spongycastle/asn1/q/ac;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/spongycastle/asn1/q/x;->Sy:Lorg/spongycastle/asn1/q/ac;

    return-object v0
.end method
