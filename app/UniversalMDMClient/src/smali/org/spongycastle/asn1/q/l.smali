.class public Lorg/spongycastle/asn1/q/l;
.super Lorg/spongycastle/asn1/k;
.source "Certificate.java"


# instance fields
.field Ks:Lorg/spongycastle/asn1/q/a;

.field NQ:Lorg/spongycastle/asn1/r;

.field Rw:Lorg/spongycastle/asn1/q/am;

.field Rx:Lorg/spongycastle/asn1/aq;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 54
    iput-object p1, p0, Lorg/spongycastle/asn1/q/l;->NQ:Lorg/spongycastle/asn1/r;

    .line 59
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/am;->ba(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/am;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/l;->Rw:Lorg/spongycastle/asn1/q/am;

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/l;->Ks:Lorg/spongycastle/asn1/q/a;

    .line 64
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/aq;->N(Ljava/lang/Object;)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/l;->Rx:Lorg/spongycastle/asn1/aq;

    .line 70
    return-void

    .line 68
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sequence wrong size for a certificate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static aG(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/l;
    .locals 2

    .prologue
    .line 39
    instance-of v0, p0, Lorg/spongycastle/asn1/q/l;

    if-eqz v0, :cond_0

    .line 41
    check-cast p0, Lorg/spongycastle/asn1/q/l;

    .line 48
    :goto_0
    return-object p0

    .line 43
    :cond_0
    if-eqz p0, :cond_1

    .line 45
    new-instance v0, Lorg/spongycastle/asn1/q/l;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/l;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 48
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/spongycastle/asn1/q/l;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public oE()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/asn1/q/l;->Rw:Lorg/spongycastle/asn1/q/am;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/am;->oE()Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    return-object v0
.end method
