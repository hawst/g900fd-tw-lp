.class public Lorg/spongycastle/asn1/q/ah;
.super Lorg/spongycastle/asn1/k;
.source "SubjectPublicKeyInfo.java"


# instance fields
.field private SQ:Lorg/spongycastle/asn1/aq;

.field private algId:Lorg/spongycastle/asn1/q/a;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 54
    new-instance v0, Lorg/spongycastle/asn1/aq;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/aq;-><init>(Lorg/spongycastle/asn1/d;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ah;->SQ:Lorg/spongycastle/asn1/aq;

    .line 55
    iput-object p1, p0, Lorg/spongycastle/asn1/q/ah;->algId:Lorg/spongycastle/asn1/q/a;

    .line 56
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q/a;[B)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 62
    new-instance v0, Lorg/spongycastle/asn1/aq;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/aq;-><init>([B)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ah;->SQ:Lorg/spongycastle/asn1/aq;

    .line 63
    iput-object p1, p0, Lorg/spongycastle/asn1/q/ah;->algId:Lorg/spongycastle/asn1/q/a;

    .line 64
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    .line 68
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 69
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/ah;->algId:Lorg/spongycastle/asn1/q/a;

    .line 78
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/aq;->N(Ljava/lang/Object;)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ah;->SQ:Lorg/spongycastle/asn1/aq;

    .line 79
    return-void
.end method

.method public static aX(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ah;
    .locals 2

    .prologue
    .line 38
    instance-of v0, p0, Lorg/spongycastle/asn1/q/ah;

    if-eqz v0, :cond_0

    .line 40
    check-cast p0, Lorg/spongycastle/asn1/q/ah;

    .line 47
    :goto_0
    return-object p0

    .line 42
    :cond_0
    if-eqz p0, :cond_1

    .line 44
    new-instance v0, Lorg/spongycastle/asn1/q/ah;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/ah;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 47
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 150
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ah;->algId:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 151
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ah;->SQ:Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 153
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nJ()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ah;->algId:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public pp()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ah;->algId:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public pq()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Lorg/spongycastle/asn1/h;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/ah;->SQ:Lorg/spongycastle/asn1/aq;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/aq;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/h;-><init>([B)V

    .line 108
    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method

.method public pr()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ah;->SQ:Lorg/spongycastle/asn1/aq;

    return-object v0
.end method
