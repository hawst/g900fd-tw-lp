.class public Lorg/spongycastle/asn1/q/an;
.super Lorg/spongycastle/asn1/k;
.source "TBSCertificateStructure.java"

# interfaces
.implements Lorg/spongycastle/asn1/l/q;
.implements Lorg/spongycastle/asn1/q/ay;


# instance fields
.field Ku:Lorg/spongycastle/asn1/i;

.field Kv:Lorg/spongycastle/asn1/p/c;

.field NQ:Lorg/spongycastle/asn1/r;

.field Ri:Lorg/spongycastle/asn1/q/a;

.field Rj:Lorg/spongycastle/asn1/i;

.field SR:Lorg/spongycastle/asn1/p/c;

.field Ta:Lorg/spongycastle/asn1/q/ar;

.field Tb:Lorg/spongycastle/asn1/q/ar;

.field Tc:Lorg/spongycastle/asn1/q/ah;

.field Td:Lorg/spongycastle/asn1/aq;

.field Te:Lorg/spongycastle/asn1/aq;

.field Tf:Lorg/spongycastle/asn1/q/av;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 74
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 77
    iput-object p1, p0, Lorg/spongycastle/asn1/q/an;->NQ:Lorg/spongycastle/asn1/r;

    .line 82
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/bm;

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    invoke-static {v0, v4}, Lorg/spongycastle/asn1/i;->h(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Ku:Lorg/spongycastle/asn1/i;

    move v1, v2

    .line 92
    :goto_0
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Rj:Lorg/spongycastle/asn1/i;

    .line 94
    add-int/lit8 v0, v1, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Ri:Lorg/spongycastle/asn1/q/a;

    .line 95
    add-int/lit8 v0, v1, 0x3

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->SR:Lorg/spongycastle/asn1/p/c;

    .line 100
    add-int/lit8 v0, v1, 0x4

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    .line 102
    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    invoke-static {v3}, Lorg/spongycastle/asn1/q/ar;->bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;

    move-result-object v3

    iput-object v3, p0, Lorg/spongycastle/asn1/q/an;->Ta:Lorg/spongycastle/asn1/q/ar;

    .line 103
    invoke-virtual {v0, v4}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ar;->bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Tb:Lorg/spongycastle/asn1/q/ar;

    .line 105
    add-int/lit8 v0, v1, 0x5

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Kv:Lorg/spongycastle/asn1/p/c;

    .line 110
    add-int/lit8 v0, v1, 0x6

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ah;->aX(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ah;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Tc:Lorg/spongycastle/asn1/q/ah;

    .line 112
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    add-int/lit8 v3, v1, 0x6

    sub-int/2addr v0, v3

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-lez v3, :cond_1

    .line 114
    add-int/lit8 v0, v1, 0x6

    add-int/2addr v0, v3

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/bm;

    .line 116
    invoke-virtual {v0}, Lorg/spongycastle/asn1/bm;->mT()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 112
    :goto_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    .line 88
    :cond_0
    const/4 v0, -0x1

    .line 89
    new-instance v1, Lorg/spongycastle/asn1/i;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    iput-object v1, p0, Lorg/spongycastle/asn1/q/an;->Ku:Lorg/spongycastle/asn1/i;

    move v1, v0

    goto/16 :goto_0

    .line 119
    :pswitch_0
    invoke-static {v0, v2}, Lorg/spongycastle/asn1/aq;->e(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Td:Lorg/spongycastle/asn1/aq;

    goto :goto_2

    .line 122
    :pswitch_1
    invoke-static {v0, v2}, Lorg/spongycastle/asn1/aq;->e(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Te:Lorg/spongycastle/asn1/aq;

    goto :goto_2

    .line 125
    :pswitch_2
    invoke-static {v0}, Lorg/spongycastle/asn1/q/av;->bi(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/av;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/an;->Tf:Lorg/spongycastle/asn1/q/av;

    goto :goto_2

    .line 128
    :cond_1
    return-void

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static bb(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/an;
    .locals 2

    .prologue
    .line 60
    instance-of v0, p0, Lorg/spongycastle/asn1/q/an;

    if-eqz v0, :cond_0

    .line 62
    check-cast p0, Lorg/spongycastle/asn1/q/an;

    .line 69
    :goto_0
    return-object p0

    .line 64
    :cond_0
    if-eqz p0, :cond_1

    .line 66
    new-instance v0, Lorg/spongycastle/asn1/q/an;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/an;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 69
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getVersion()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public oE()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->SR:Lorg/spongycastle/asn1/p/c;

    return-object v0
.end method

.method public ow()Lorg/spongycastle/asn1/i;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Rj:Lorg/spongycastle/asn1/i;

    return-object v0
.end method

.method public pA()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Te:Lorg/spongycastle/asn1/aq;

    return-object v0
.end method

.method public pB()Lorg/spongycastle/asn1/q/av;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Tf:Lorg/spongycastle/asn1/q/av;

    return-object v0
.end method

.method public ps()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Ri:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public pv()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Ta:Lorg/spongycastle/asn1/q/ar;

    return-object v0
.end method

.method public pw()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Tb:Lorg/spongycastle/asn1/q/ar;

    return-object v0
.end method

.method public px()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Kv:Lorg/spongycastle/asn1/p/c;

    return-object v0
.end method

.method public py()Lorg/spongycastle/asn1/q/ah;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Tc:Lorg/spongycastle/asn1/q/ah;

    return-object v0
.end method

.method public pz()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lorg/spongycastle/asn1/q/an;->Td:Lorg/spongycastle/asn1/aq;

    return-object v0
.end method
