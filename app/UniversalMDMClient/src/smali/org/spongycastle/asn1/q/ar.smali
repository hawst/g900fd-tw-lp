.class public Lorg/spongycastle/asn1/q/ar;
.super Lorg/spongycastle/asn1/k;
.source "Time.java"

# interfaces
.implements Lorg/spongycastle/asn1/c;


# instance fields
.field Tj:Lorg/spongycastle/asn1/q;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 31
    instance-of v0, p1, Lorg/spongycastle/asn1/bn;

    if-nez v0, :cond_0

    instance-of v0, p1, Lorg/spongycastle/asn1/ax;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown object passed to Time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    .line 38
    return-void
.end method

.method public static bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;
    .locals 3

    .prologue
    .line 69
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/ar;

    if-eqz v0, :cond_1

    .line 71
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/ar;

    .line 79
    :goto_0
    return-object p0

    .line 73
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/bn;

    if-eqz v0, :cond_2

    .line 75
    new-instance v0, Lorg/spongycastle/asn1/q/ar;

    check-cast p0, Lorg/spongycastle/asn1/bn;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/ar;-><init>(Lorg/spongycastle/asn1/q;)V

    move-object p0, v0

    goto :goto_0

    .line 77
    :cond_2
    instance-of v0, p0, Lorg/spongycastle/asn1/ax;

    if-eqz v0, :cond_3

    .line 79
    new-instance v0, Lorg/spongycastle/asn1/q/ar;

    check-cast p0, Lorg/spongycastle/asn1/ax;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/ar;-><init>(Lorg/spongycastle/asn1/q;)V

    move-object p0, v0

    goto :goto_0

    .line 82
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in factory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 4

    .prologue
    .line 101
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    instance-of v0, v0, Lorg/spongycastle/asn1/bn;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    check-cast v0, Lorg/spongycastle/asn1/bn;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bn;->nn()Ljava/util/Date;

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    check-cast v0, Lorg/spongycastle/asn1/ax;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ax;->getDate()Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 112
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid date string: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    return-object v0
.end method

.method public nh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    instance-of v0, v0, Lorg/spongycastle/asn1/bn;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    check-cast v0, Lorg/spongycastle/asn1/bn;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bn;->no()Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ar;->Tj:Lorg/spongycastle/asn1/q;

    check-cast v0, Lorg/spongycastle/asn1/ax;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ax;->nh()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/ar;->nh()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
