.class public interface abstract Lorg/spongycastle/asn1/q/ay;
.super Ljava/lang/Object;
.source "X509ObjectIdentifiers.java"


# static fields
.field public static final Px:Lorg/spongycastle/asn1/l;

.field public static final Ua:Lorg/spongycastle/asn1/l;

.field public static final Ub:Lorg/spongycastle/asn1/l;

.field public static final Uc:Lorg/spongycastle/asn1/l;

.field public static final Ud:Lorg/spongycastle/asn1/l;

.field public static final Ue:Lorg/spongycastle/asn1/l;

.field public static final Uf:Lorg/spongycastle/asn1/l;

.field public static final Ug:Lorg/spongycastle/asn1/l;

.field public static final Uh:Lorg/spongycastle/asn1/l;

.field public static final Ui:Lorg/spongycastle/asn1/l;

.field public static final Uj:Lorg/spongycastle/asn1/l;

.field public static final Uk:Lorg/spongycastle/asn1/l;

.field public static final Ul:Lorg/spongycastle/asn1/l;

.field public static final Um:Lorg/spongycastle/asn1/l;

.field public static final Un:Lorg/spongycastle/asn1/l;

.field public static final Uo:Lorg/spongycastle/asn1/l;

.field public static final Up:Lorg/spongycastle/asn1/l;

.field public static final Uq:Lorg/spongycastle/asn1/l;

.field public static final Ur:Lorg/spongycastle/asn1/l;

.field public static final Us:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ua:Lorg/spongycastle/asn1/l;

    .line 13
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.6"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ub:Lorg/spongycastle/asn1/l;

    .line 14
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Uc:Lorg/spongycastle/asn1/l;

    .line 15
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.8"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ud:Lorg/spongycastle/asn1/l;

    .line 16
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.10"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ue:Lorg/spongycastle/asn1/l;

    .line 17
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.11"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Uf:Lorg/spongycastle/asn1/l;

    .line 19
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.20"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ug:Lorg/spongycastle/asn1/l;

    .line 20
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.4.41"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Uh:Lorg/spongycastle/asn1/l;

    .line 24
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.14.3.2.26"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ui:Lorg/spongycastle/asn1/l;

    .line 30
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.36.3.2.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Px:Lorg/spongycastle/asn1/l;

    .line 36
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.36.3.3.1.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Uj:Lorg/spongycastle/asn1/l;

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.8.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Uk:Lorg/spongycastle/asn1/l;

    .line 42
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ul:Lorg/spongycastle/asn1/l;

    .line 47
    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lorg/spongycastle/asn1/q/ay;->Ul:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Um:Lorg/spongycastle/asn1/l;

    .line 52
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Un:Lorg/spongycastle/asn1/l;

    .line 57
    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lorg/spongycastle/asn1/q/ay;->Ul:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".48"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Uo:Lorg/spongycastle/asn1/l;

    .line 58
    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lorg/spongycastle/asn1/q/ay;->Uo:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Up:Lorg/spongycastle/asn1/l;

    .line 59
    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lorg/spongycastle/asn1/q/ay;->Uo:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Uq:Lorg/spongycastle/asn1/l;

    .line 64
    sget-object v0, Lorg/spongycastle/asn1/q/ay;->Uq:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Ur:Lorg/spongycastle/asn1/l;

    .line 65
    sget-object v0, Lorg/spongycastle/asn1/q/ay;->Up:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/q/ay;->Us:Lorg/spongycastle/asn1/l;

    return-void
.end method
