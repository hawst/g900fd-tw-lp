.class public Lorg/spongycastle/asn1/q/e;
.super Lorg/spongycastle/asn1/k;
.source "AttributeCertificate.java"


# instance fields
.field Rd:Lorg/spongycastle/asn1/q/f;

.field Re:Lorg/spongycastle/asn1/q/a;

.field Rf:Lorg/spongycastle/asn1/aq;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 48
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/f;->aB(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/f;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/e;->Rd:Lorg/spongycastle/asn1/q/f;

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/e;->Re:Lorg/spongycastle/asn1/q/a;

    .line 56
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/aq;->N(Ljava/lang/Object;)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/e;->Rf:Lorg/spongycastle/asn1/aq;

    .line 57
    return-void
.end method

.method public static aA(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/e;
    .locals 2

    .prologue
    .line 23
    instance-of v0, p0, Lorg/spongycastle/asn1/q/e;

    if-eqz v0, :cond_0

    .line 25
    check-cast p0, Lorg/spongycastle/asn1/q/e;

    .line 32
    :goto_0
    return-object p0

    .line 27
    :cond_0
    if-eqz p0, :cond_1

    .line 29
    new-instance v0, Lorg/spongycastle/asn1/q/e;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/e;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 32
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 88
    iget-object v1, p0, Lorg/spongycastle/asn1/q/e;->Rd:Lorg/spongycastle/asn1/q/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 89
    iget-object v1, p0, Lorg/spongycastle/asn1/q/e;->Re:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 90
    iget-object v1, p0, Lorg/spongycastle/asn1/q/e;->Rf:Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 92
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public ot()Lorg/spongycastle/asn1/q/f;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/spongycastle/asn1/q/e;->Rd:Lorg/spongycastle/asn1/q/f;

    return-object v0
.end method
