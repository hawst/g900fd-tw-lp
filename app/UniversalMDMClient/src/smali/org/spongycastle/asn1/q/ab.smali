.class public Lorg/spongycastle/asn1/q/ab;
.super Lorg/spongycastle/asn1/k;
.source "NameConstraints.java"


# instance fields
.field private SH:Lorg/spongycastle/asn1/r;

.field private SI:Lorg/spongycastle/asn1/r;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 35
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 36
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 42
    :pswitch_0
    invoke-static {v1, v3}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/ab;->SH:Lorg/spongycastle/asn1/r;

    goto :goto_0

    .line 45
    :pswitch_1
    invoke-static {v1, v3}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/ab;->SI:Lorg/spongycastle/asn1/r;

    goto :goto_0

    .line 49
    :cond_0
    return-void

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static aU(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ab;
    .locals 2

    .prologue
    .line 21
    instance-of v0, p0, Lorg/spongycastle/asn1/q/ab;

    if-eqz v0, :cond_0

    .line 23
    check-cast p0, Lorg/spongycastle/asn1/q/ab;

    .line 30
    :goto_0
    return-object p0

    .line 25
    :cond_0
    if-eqz p0, :cond_1

    .line 27
    new-instance v0, Lorg/spongycastle/asn1/q/ab;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/ab;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 30
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 104
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 106
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ab;->SH:Lorg/spongycastle/asn1/r;

    if-eqz v1, :cond_0

    .line 108
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/q/ab;->SH:Lorg/spongycastle/asn1/r;

    invoke-direct {v1, v4, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 111
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ab;->SI:Lorg/spongycastle/asn1/r;

    if-eqz v1, :cond_1

    .line 113
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/spongycastle/asn1/q/ab;->SI:Lorg/spongycastle/asn1/r;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 116
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public pi()Lorg/spongycastle/asn1/r;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ab;->SH:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public pj()Lorg/spongycastle/asn1/r;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ab;->SI:Lorg/spongycastle/asn1/r;

    return-object v0
.end method
