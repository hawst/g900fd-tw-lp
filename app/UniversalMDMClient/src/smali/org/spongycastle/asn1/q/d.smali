.class public Lorg/spongycastle/asn1/q/d;
.super Lorg/spongycastle/asn1/k;
.source "Attribute.java"


# instance fields
.field private Kl:Lorg/spongycastle/asn1/l;

.field private Km:Lorg/spongycastle/asn1/t;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 43
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l;->T(Ljava/lang/Object;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/d;->Kl:Lorg/spongycastle/asn1/l;

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/t;->L(Ljava/lang/Object;)Lorg/spongycastle/asn1/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/d;->Km:Lorg/spongycastle/asn1/t;

    .line 50
    return-void
.end method

.method public static az(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/d;
    .locals 2

    .prologue
    .line 27
    instance-of v0, p0, Lorg/spongycastle/asn1/q/d;

    if-eqz v0, :cond_0

    .line 29
    check-cast p0, Lorg/spongycastle/asn1/q/d;

    .line 37
    :goto_0
    return-object p0

    .line 32
    :cond_0
    if-eqz p0, :cond_1

    .line 34
    new-instance v0, Lorg/spongycastle/asn1/q/d;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/d;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 37
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 88
    iget-object v1, p0, Lorg/spongycastle/asn1/q/d;->Kl:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 89
    iget-object v1, p0, Lorg/spongycastle/asn1/q/d;->Km:Lorg/spongycastle/asn1/t;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 91
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public os()Lorg/spongycastle/asn1/l;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lorg/spongycastle/asn1/l;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/d;->Kl:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
