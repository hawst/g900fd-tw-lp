.class public Lorg/spongycastle/asn1/q/m;
.super Lorg/spongycastle/asn1/k;
.source "CertificateList.java"


# instance fields
.field Ks:Lorg/spongycastle/asn1/q/a;

.field Rx:Lorg/spongycastle/asn1/aq;

.field Ry:Lorg/spongycastle/asn1/q/ai;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 60
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ai;->aY(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ai;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ks:Lorg/spongycastle/asn1/q/a;

    .line 64
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/aq;->N(Ljava/lang/Object;)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/m;->Rx:Lorg/spongycastle/asn1/aq;

    .line 70
    return-void

    .line 68
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sequence wrong size for CertificateList"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static aH(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/m;
    .locals 2

    .prologue
    .line 45
    instance-of v0, p0, Lorg/spongycastle/asn1/q/m;

    if-eqz v0, :cond_0

    .line 47
    check-cast p0, Lorg/spongycastle/asn1/q/m;

    .line 54
    :goto_0
    return-object p0

    .line 49
    :cond_0
    if-eqz p0, :cond_1

    .line 51
    new-instance v0, Lorg/spongycastle/asn1/q/m;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/m;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 54
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 121
    iget-object v1, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 122
    iget-object v1, p0, Lorg/spongycastle/asn1/q/m;->Ks:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 123
    iget-object v1, p0, Lorg/spongycastle/asn1/q/m;->Rx:Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 125
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public oE()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ai;->oE()Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    return-object v0
.end method

.method public oF()Lorg/spongycastle/asn1/q/ai;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    return-object v0
.end method

.method public oG()[Lorg/spongycastle/asn1/q/aj;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ai;->oG()[Lorg/spongycastle/asn1/q/aj;

    move-result-object v0

    return-object v0
.end method

.method public oH()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ai;->oH()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public oI()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ks:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public oJ()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Rx:Lorg/spongycastle/asn1/aq;

    return-object v0
.end method

.method public oK()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ai;->oK()I

    move-result v0

    return v0
.end method

.method public oL()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ai;->oL()Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    return-object v0
.end method

.method public oM()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/spongycastle/asn1/q/m;->Ry:Lorg/spongycastle/asn1/q/ai;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ai;->oM()Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    return-object v0
.end method
