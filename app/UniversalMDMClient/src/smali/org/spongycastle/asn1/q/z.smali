.class public Lorg/spongycastle/asn1/q/z;
.super Lorg/spongycastle/asn1/k;
.source "IssuingDistributionPoint.java"


# instance fields
.field private NQ:Lorg/spongycastle/asn1/r;

.field private RC:Lorg/spongycastle/asn1/q/r;

.field private SC:Z

.field private SD:Z

.field private SE:Lorg/spongycastle/asn1/q/af;

.field private SF:Z

.field private SG:Z


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 145
    iput-object p1, p0, Lorg/spongycastle/asn1/q/z;->NQ:Lorg/spongycastle/asn1/r;

    move v0, v1

    .line 147
    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 149
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v2

    .line 151
    invoke-virtual {v2}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown tag in IssuingDistributionPoint"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :pswitch_0
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/spongycastle/asn1/q/r;->l(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/r;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/z;->RC:Lorg/spongycastle/asn1/q/r;

    .line 147
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :pswitch_1
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/ar;->f(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/ar;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/ar;->na()Z

    move-result v2

    iput-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SC:Z

    goto :goto_1

    .line 161
    :pswitch_2
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/ar;->f(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/ar;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/ar;->na()Z

    move-result v2

    iput-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SD:Z

    goto :goto_1

    .line 164
    :pswitch_3
    new-instance v3, Lorg/spongycastle/asn1/q/af;

    invoke-static {v2, v1}, Lorg/spongycastle/asn1/q/af;->e(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/aq;

    move-result-object v2

    invoke-direct {v3, v2}, Lorg/spongycastle/asn1/q/af;-><init>(Lorg/spongycastle/asn1/aq;)V

    iput-object v3, p0, Lorg/spongycastle/asn1/q/z;->SE:Lorg/spongycastle/asn1/q/af;

    goto :goto_1

    .line 167
    :pswitch_4
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/ar;->f(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/ar;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/ar;->na()Z

    move-result v2

    iput-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SF:Z

    goto :goto_1

    .line 170
    :pswitch_5
    invoke-static {v2, v1}, Lorg/spongycastle/asn1/ar;->f(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/ar;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/ar;->na()Z

    move-result v2

    iput-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SG:Z

    goto :goto_1

    .line 177
    :cond_0
    return-void

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private V(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    if-eqz p1, :cond_0

    const-string v0, "true"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method private a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 258
    const-string v0, "    "

    .line 260
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 261
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 262
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 263
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 264
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 265
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 266
    invoke-virtual {p1, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 267
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 268
    return-void
.end method

.method public static aT(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/z;
    .locals 2

    .prologue
    .line 50
    instance-of v0, p0, Lorg/spongycastle/asn1/q/z;

    if-eqz v0, :cond_0

    .line 52
    check-cast p0, Lorg/spongycastle/asn1/q/z;

    .line 59
    :goto_0
    return-object p0

    .line 54
    :cond_0
    if-eqz p0, :cond_1

    .line 56
    new-instance v0, Lorg/spongycastle/asn1/q/z;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/z;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 59
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lorg/spongycastle/asn1/q/z;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public oP()Lorg/spongycastle/asn1/q/r;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/spongycastle/asn1/q/z;->RC:Lorg/spongycastle/asn1/q/r;

    return-object v0
.end method

.method public pd()Z
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lorg/spongycastle/asn1/q/z;->SC:Z

    return v0
.end method

.method public pe()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lorg/spongycastle/asn1/q/z;->SD:Z

    return v0
.end method

.method public pf()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lorg/spongycastle/asn1/q/z;->SF:Z

    return v0
.end method

.method public pg()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lorg/spongycastle/asn1/q/z;->SG:Z

    return v0
.end method

.method public ph()Lorg/spongycastle/asn1/q/af;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/spongycastle/asn1/q/z;->SE:Lorg/spongycastle/asn1/q/af;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 222
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 223
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 225
    const-string v2, "IssuingDistributionPoint: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 226
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 227
    iget-object v2, p0, Lorg/spongycastle/asn1/q/z;->RC:Lorg/spongycastle/asn1/q/r;

    if-eqz v2, :cond_0

    .line 229
    const-string v2, "distributionPoint"

    iget-object v3, p0, Lorg/spongycastle/asn1/q/z;->RC:Lorg/spongycastle/asn1/q/r;

    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/r;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/z;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_0
    iget-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SC:Z

    if-eqz v2, :cond_1

    .line 233
    const-string v2, "onlyContainsUserCerts"

    iget-boolean v3, p0, Lorg/spongycastle/asn1/q/z;->SC:Z

    invoke-direct {p0, v3}, Lorg/spongycastle/asn1/q/z;->V(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/z;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_1
    iget-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SD:Z

    if-eqz v2, :cond_2

    .line 237
    const-string v2, "onlyContainsCACerts"

    iget-boolean v3, p0, Lorg/spongycastle/asn1/q/z;->SD:Z

    invoke-direct {p0, v3}, Lorg/spongycastle/asn1/q/z;->V(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/z;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :cond_2
    iget-object v2, p0, Lorg/spongycastle/asn1/q/z;->SE:Lorg/spongycastle/asn1/q/af;

    if-eqz v2, :cond_3

    .line 241
    const-string v2, "onlySomeReasons"

    iget-object v3, p0, Lorg/spongycastle/asn1/q/z;->SE:Lorg/spongycastle/asn1/q/af;

    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/af;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/z;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_3
    iget-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SG:Z

    if-eqz v2, :cond_4

    .line 245
    const-string v2, "onlyContainsAttributeCerts"

    iget-boolean v3, p0, Lorg/spongycastle/asn1/q/z;->SG:Z

    invoke-direct {p0, v3}, Lorg/spongycastle/asn1/q/z;->V(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/z;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_4
    iget-boolean v2, p0, Lorg/spongycastle/asn1/q/z;->SF:Z

    if-eqz v2, :cond_5

    .line 249
    const-string v2, "indirectCRL"

    iget-boolean v3, p0, Lorg/spongycastle/asn1/q/z;->SF:Z

    invoke-direct {p0, v3}, Lorg/spongycastle/asn1/q/z;->V(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lorg/spongycastle/asn1/q/z;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_5
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 252
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 253
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
