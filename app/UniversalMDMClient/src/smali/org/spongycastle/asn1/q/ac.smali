.class public Lorg/spongycastle/asn1/q/ac;
.super Lorg/spongycastle/asn1/k;
.source "ObjectDigestInfo.java"


# instance fields
.field SJ:Lorg/spongycastle/asn1/f;

.field SK:Lorg/spongycastle/asn1/l;

.field SL:Lorg/spongycastle/asn1/q/a;

.field SM:Lorg/spongycastle/asn1/aq;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 115
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-gt v2, v4, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    .line 117
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_1
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/as;->P(Ljava/lang/Object;)Lorg/spongycastle/asn1/f;

    move-result-object v2

    iput-object v2, p0, Lorg/spongycastle/asn1/q/ac;->SJ:Lorg/spongycastle/asn1/f;

    .line 125
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 127
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/l;->T(Ljava/lang/Object;)Lorg/spongycastle/asn1/l;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/ac;->SK:Lorg/spongycastle/asn1/l;

    .line 131
    :goto_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/ac;->SL:Lorg/spongycastle/asn1/q/a;

    .line 133
    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/aq;->N(Ljava/lang/Object;)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/ac;->SM:Lorg/spongycastle/asn1/aq;

    .line 134
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static aV(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ac;
    .locals 2

    .prologue
    .line 63
    instance-of v0, p0, Lorg/spongycastle/asn1/q/ac;

    if-eqz v0, :cond_0

    .line 65
    check-cast p0, Lorg/spongycastle/asn1/q/ac;

    .line 73
    :goto_0
    return-object p0

    .line 68
    :cond_0
    if-eqz p0, :cond_1

    .line 70
    new-instance v0, Lorg/spongycastle/asn1/q/ac;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/ac;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 73
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static p(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/ac;
    .locals 1

    .prologue
    .line 80
    invoke-static {p0, p1}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ac;->aV(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ac;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 177
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 179
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ac;->SJ:Lorg/spongycastle/asn1/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 181
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ac;->SK:Lorg/spongycastle/asn1/l;

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ac;->SK:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 186
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ac;->SL:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 187
    iget-object v1, p0, Lorg/spongycastle/asn1/q/ac;->SM:Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 189
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public pk()Lorg/spongycastle/asn1/as;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ac;->SJ:Lorg/spongycastle/asn1/f;

    return-object v0
.end method

.method public pl()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ac;->SL:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public pm()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lorg/spongycastle/asn1/q/ac;->SM:Lorg/spongycastle/asn1/aq;

    return-object v0
.end method
