.class public Lorg/spongycastle/asn1/q/h;
.super Lorg/spongycastle/asn1/k;
.source "BasicConstraints.java"


# instance fields
.field Rr:Lorg/spongycastle/asn1/ar;

.field Rs:Lorg/spongycastle/asn1/i;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 17
    new-instance v0, Lorg/spongycastle/asn1/ar;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ar;-><init>(Z)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    .line 18
    iput-object v2, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    .line 49
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 51
    iput-object v2, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    .line 52
    iput-object v2, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/ar;

    if-eqz v0, :cond_2

    .line 58
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/ar;->O(Ljava/lang/Object;)Lorg/spongycastle/asn1/b;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    .line 65
    :goto_1
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 67
    iget-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    if-eqz v0, :cond_3

    .line 69
    invoke-virtual {p1, v3}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    goto :goto_0

    .line 62
    :cond_2
    iput-object v2, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    .line 63
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    goto :goto_1

    .line 73
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "wrong sequence in constructor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static aD(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/h;
    .locals 2

    .prologue
    .line 30
    instance-of v0, p0, Lorg/spongycastle/asn1/q/h;

    if-eqz v0, :cond_0

    .line 32
    check-cast p0, Lorg/spongycastle/asn1/q/h;

    .line 43
    :goto_0
    return-object p0

    .line 34
    :cond_0
    instance-of v0, p0, Lorg/spongycastle/asn1/q/au;

    if-eqz v0, :cond_1

    .line 36
    check-cast p0, Lorg/spongycastle/asn1/q/au;

    invoke-static {p0}, Lorg/spongycastle/asn1/q/au;->a(Lorg/spongycastle/asn1/q/au;)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/h;->aD(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/h;

    move-result-object p0

    goto :goto_0

    .line 38
    :cond_1
    if-eqz p0, :cond_2

    .line 40
    new-instance v0, Lorg/spongycastle/asn1/q/h;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/h;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 43
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 133
    iget-object v1, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 138
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_1

    .line 140
    iget-object v1, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 143
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public oA()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ar;->na()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public oB()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    if-nez v0, :cond_1

    .line 150
    iget-object v0, p0, Lorg/spongycastle/asn1/q/h;->Rr:Lorg/spongycastle/asn1/ar;

    if-nez v0, :cond_0

    .line 152
    const-string v0, "BasicConstraints: isCa(false)"

    .line 156
    :goto_0
    return-object v0

    .line 154
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BasicConstraints: isCa("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/h;->oA()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 156
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BasicConstraints: isCa("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/h;->oA()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), pathLenConstraint = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/asn1/q/h;->Rs:Lorg/spongycastle/asn1/i;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
