.class public Lorg/spongycastle/asn1/q/au;
.super Ljava/lang/Object;
.source "X509Extension.java"


# static fields
.field public static final RH:Lorg/spongycastle/asn1/l;

.field public static final RI:Lorg/spongycastle/asn1/l;

.field public static final RJ:Lorg/spongycastle/asn1/l;

.field public static final RK:Lorg/spongycastle/asn1/l;

.field public static final RL:Lorg/spongycastle/asn1/l;

.field public static final RM:Lorg/spongycastle/asn1/l;

.field public static final RN:Lorg/spongycastle/asn1/l;

.field public static final RO:Lorg/spongycastle/asn1/l;

.field public static final RQ:Lorg/spongycastle/asn1/l;

.field public static final RR:Lorg/spongycastle/asn1/l;

.field public static final RS:Lorg/spongycastle/asn1/l;

.field public static final RU:Lorg/spongycastle/asn1/l;

.field public static final RV:Lorg/spongycastle/asn1/l;

.field public static final RW:Lorg/spongycastle/asn1/l;

.field public static final RX:Lorg/spongycastle/asn1/l;

.field public static final RY:Lorg/spongycastle/asn1/l;

.field public static final RZ:Lorg/spongycastle/asn1/l;

.field public static final Sa:Lorg/spongycastle/asn1/l;

.field public static final Sb:Lorg/spongycastle/asn1/l;

.field public static final Sc:Lorg/spongycastle/asn1/l;

.field public static final Sd:Lorg/spongycastle/asn1/l;

.field public static final Se:Lorg/spongycastle/asn1/l;

.field public static final Sf:Lorg/spongycastle/asn1/l;

.field public static final Sg:Lorg/spongycastle/asn1/l;

.field public static final Sh:Lorg/spongycastle/asn1/l;

.field public static final Si:Lorg/spongycastle/asn1/l;

.field public static final Sj:Lorg/spongycastle/asn1/l;

.field public static final Sk:Lorg/spongycastle/asn1/l;

.field public static final Sl:Lorg/spongycastle/asn1/l;

.field public static final Sm:Lorg/spongycastle/asn1/l;

.field public static final Sn:Lorg/spongycastle/asn1/l;


# instance fields
.field Sp:Lorg/spongycastle/asn1/m;

.field critical:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.9"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RH:Lorg/spongycastle/asn1/l;

    .line 24
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.14"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RI:Lorg/spongycastle/asn1/l;

    .line 29
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.15"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RJ:Lorg/spongycastle/asn1/l;

    .line 34
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.16"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RK:Lorg/spongycastle/asn1/l;

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.17"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RL:Lorg/spongycastle/asn1/l;

    .line 44
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.18"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RM:Lorg/spongycastle/asn1/l;

    .line 49
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.19"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RN:Lorg/spongycastle/asn1/l;

    .line 54
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.20"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RO:Lorg/spongycastle/asn1/l;

    .line 59
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.21"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RQ:Lorg/spongycastle/asn1/l;

    .line 64
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.23"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RR:Lorg/spongycastle/asn1/l;

    .line 69
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.24"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RS:Lorg/spongycastle/asn1/l;

    .line 74
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.27"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RU:Lorg/spongycastle/asn1/l;

    .line 79
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.28"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RV:Lorg/spongycastle/asn1/l;

    .line 84
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RW:Lorg/spongycastle/asn1/l;

    .line 89
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.30"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RX:Lorg/spongycastle/asn1/l;

    .line 94
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.31"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RY:Lorg/spongycastle/asn1/l;

    .line 99
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.32"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->RZ:Lorg/spongycastle/asn1/l;

    .line 104
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.33"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sa:Lorg/spongycastle/asn1/l;

    .line 109
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.35"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sb:Lorg/spongycastle/asn1/l;

    .line 114
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.36"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sc:Lorg/spongycastle/asn1/l;

    .line 119
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.37"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sd:Lorg/spongycastle/asn1/l;

    .line 124
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.46"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Se:Lorg/spongycastle/asn1/l;

    .line 129
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.54"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sf:Lorg/spongycastle/asn1/l;

    .line 134
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sg:Lorg/spongycastle/asn1/l;

    .line 139
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.11"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sh:Lorg/spongycastle/asn1/l;

    .line 144
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.12"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Si:Lorg/spongycastle/asn1/l;

    .line 149
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sj:Lorg/spongycastle/asn1/l;

    .line 154
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sk:Lorg/spongycastle/asn1/l;

    .line 159
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sl:Lorg/spongycastle/asn1/l;

    .line 164
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.56"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sm:Lorg/spongycastle/asn1/l;

    .line 169
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.55"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/au;->Sn:Lorg/spongycastle/asn1/l;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/ar;Lorg/spongycastle/asn1/m;)V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    invoke-virtual {p1}, Lorg/spongycastle/asn1/ar;->na()Z

    move-result v0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/q/au;->critical:Z

    .line 179
    iput-object p2, p0, Lorg/spongycastle/asn1/q/au;->Sp:Lorg/spongycastle/asn1/m;

    .line 180
    return-void
.end method

.method public constructor <init>(ZLorg/spongycastle/asn1/m;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-boolean p1, p0, Lorg/spongycastle/asn1/q/au;->critical:Z

    .line 187
    iput-object p2, p0, Lorg/spongycastle/asn1/q/au;->Sp:Lorg/spongycastle/asn1/m;

    .line 188
    return-void
.end method

.method public static a(Lorg/spongycastle/asn1/q/au;)Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    .line 241
    :try_start_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/au;->pI()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 243
    :catch_0
    move-exception v0

    .line 245
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t convert extension: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 218
    instance-of v1, p1, Lorg/spongycastle/asn1/q/au;

    if-nez v1, :cond_1

    .line 225
    :cond_0
    :goto_0
    return v0

    .line 223
    :cond_1
    check-cast p1, Lorg/spongycastle/asn1/q/au;

    .line 225
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/au;->pI()Lorg/spongycastle/asn1/m;

    move-result-object v1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/au;->pI()Lorg/spongycastle/asn1/m;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/au;->isCritical()Z

    move-result v1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/au;->isCritical()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/au;->isCritical()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/au;->pI()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->hashCode()I

    move-result v0

    .line 212
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/au;->pI()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->hashCode()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public isCritical()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lorg/spongycastle/asn1/q/au;->critical:Z

    return v0
.end method

.method public pI()Lorg/spongycastle/asn1/m;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lorg/spongycastle/asn1/q/au;->Sp:Lorg/spongycastle/asn1/m;

    return-object v0
.end method
