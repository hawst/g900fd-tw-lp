.class public Lorg/spongycastle/asn1/q/g;
.super Lorg/spongycastle/asn1/k;
.source "AuthorityKeyIdentifier.java"


# instance fields
.field Ro:Lorg/spongycastle/asn1/m;

.field Rp:Lorg/spongycastle/asn1/q/v;

.field Rq:Lorg/spongycastle/asn1/i;


# direct methods
.method protected constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 64
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 36
    iput-object v0, p0, Lorg/spongycastle/asn1/q/g;->Ro:Lorg/spongycastle/asn1/m;

    .line 37
    iput-object v0, p0, Lorg/spongycastle/asn1/q/g;->Rp:Lorg/spongycastle/asn1/q/v;

    .line 38
    iput-object v0, p0, Lorg/spongycastle/asn1/q/g;->Rq:Lorg/spongycastle/asn1/i;

    .line 65
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 67
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/bm;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "illegal tag"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :pswitch_0
    invoke-static {v1, v3}, Lorg/spongycastle/asn1/m;->a(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/m;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/g;->Ro:Lorg/spongycastle/asn1/m;

    goto :goto_0

    .line 77
    :pswitch_1
    invoke-static {v1, v3}, Lorg/spongycastle/asn1/q/v;->n(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/v;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/g;->Rp:Lorg/spongycastle/asn1/q/v;

    goto :goto_0

    .line 80
    :pswitch_2
    invoke-static {v1, v3}, Lorg/spongycastle/asn1/i;->h(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/i;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/g;->Rq:Lorg/spongycastle/asn1/i;

    goto :goto_0

    .line 86
    :cond_0
    return-void

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static aC(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/g;
    .locals 2

    .prologue
    .line 50
    instance-of v0, p0, Lorg/spongycastle/asn1/q/g;

    if-eqz v0, :cond_0

    .line 52
    check-cast p0, Lorg/spongycastle/asn1/q/g;

    .line 59
    :goto_0
    return-object p0

    .line 54
    :cond_0
    if-eqz p0, :cond_1

    .line 56
    new-instance v0, Lorg/spongycastle/asn1/q/g;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/g;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 59
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getKeyIdentifier()[B
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lorg/spongycastle/asn1/q/g;->Ro:Lorg/spongycastle/asn1/m;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lorg/spongycastle/asn1/q/g;->Ro:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    .line 179
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 204
    iget-object v1, p0, Lorg/spongycastle/asn1/q/g;->Ro:Lorg/spongycastle/asn1/m;

    if-eqz v1, :cond_0

    .line 206
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/q/g;->Ro:Lorg/spongycastle/asn1/m;

    invoke-direct {v1, v4, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 209
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/g;->Rp:Lorg/spongycastle/asn1/q/v;

    if-eqz v1, :cond_1

    .line 211
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/spongycastle/asn1/q/g;->Rp:Lorg/spongycastle/asn1/q/v;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 214
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/q/g;->Rq:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_2

    .line 216
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/spongycastle/asn1/q/g;->Rq:Lorg/spongycastle/asn1/i;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 220
    :cond_2
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AuthorityKeyIdentifier: KeyID("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/asn1/q/g;->Ro:Lorg/spongycastle/asn1/m;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
