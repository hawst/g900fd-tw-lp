.class public Lorg/spongycastle/asn1/q/as;
.super Lorg/spongycastle/asn1/k;
.source "V2Form.java"


# instance fields
.field Sw:Lorg/spongycastle/asn1/q/y;

.field Sy:Lorg/spongycastle/asn1/q/ac;

.field Tk:Lorg/spongycastle/asn1/q/v;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 49
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v3, 0x3

    if-le v0, v3, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/x;

    if-nez v0, :cond_4

    .line 59
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/v;->aP(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/v;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/as;->Tk:Lorg/spongycastle/asn1/q/v;

    move v0, v1

    .line 62
    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v3

    if-eq v0, v3, :cond_3

    .line 64
    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    invoke-static {v3}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v3

    .line 65
    invoke-virtual {v3}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v4

    if-nez v4, :cond_1

    .line 67
    invoke-static {v3, v2}, Lorg/spongycastle/asn1/q/y;->o(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/y;

    move-result-object v3

    iput-object v3, p0, Lorg/spongycastle/asn1/q/as;->Sw:Lorg/spongycastle/asn1/q/y;

    .line 62
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {v3}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v4

    if-ne v4, v1, :cond_2

    .line 71
    invoke-static {v3, v2}, Lorg/spongycastle/asn1/q/ac;->p(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/ac;

    move-result-object v3

    iput-object v3, p0, Lorg/spongycastle/asn1/q/as;->Sy:Lorg/spongycastle/asn1/q/ac;

    goto :goto_1

    .line 75
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad tag number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public static bg(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/as;
    .locals 3

    .prologue
    .line 28
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/as;

    if-eqz v0, :cond_1

    .line 30
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/as;

    .line 34
    :goto_0
    return-object p0

    .line 32
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_2

    .line 34
    new-instance v0, Lorg/spongycastle/asn1/q/as;

    check-cast p0, Lorg/spongycastle/asn1/r;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/as;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 37
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in factory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static q(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/as;
    .locals 1

    .prologue
    .line 22
    invoke-static {p0, p1}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/as;->bg(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/as;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 111
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 113
    iget-object v1, p0, Lorg/spongycastle/asn1/q/as;->Tk:Lorg/spongycastle/asn1/q/v;

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lorg/spongycastle/asn1/q/as;->Tk:Lorg/spongycastle/asn1/q/v;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 118
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/as;->Sw:Lorg/spongycastle/asn1/q/y;

    if-eqz v1, :cond_1

    .line 120
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/q/as;->Sw:Lorg/spongycastle/asn1/q/y;

    invoke-direct {v1, v4, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 123
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/q/as;->Sy:Lorg/spongycastle/asn1/q/ac;

    if-eqz v1, :cond_2

    .line 125
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/spongycastle/asn1/q/as;->Sy:Lorg/spongycastle/asn1/q/ac;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 128
    :cond_2
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public oY()Lorg/spongycastle/asn1/q/y;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/spongycastle/asn1/q/as;->Sw:Lorg/spongycastle/asn1/q/y;

    return-object v0
.end method

.method public pG()Lorg/spongycastle/asn1/q/v;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/spongycastle/asn1/q/as;->Tk:Lorg/spongycastle/asn1/q/v;

    return-object v0
.end method
