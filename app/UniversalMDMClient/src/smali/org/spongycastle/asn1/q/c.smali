.class public Lorg/spongycastle/asn1/q/c;
.super Lorg/spongycastle/asn1/k;
.source "AttCertValidityPeriod.java"


# instance fields
.field Rb:Lorg/spongycastle/asn1/ax;

.field Rc:Lorg/spongycastle/asn1/ax;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 34
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/ax;->Q(Ljava/lang/Object;)Lorg/spongycastle/asn1/g;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/c;->Rb:Lorg/spongycastle/asn1/ax;

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/ax;->Q(Ljava/lang/Object;)Lorg/spongycastle/asn1/g;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/c;->Rc:Lorg/spongycastle/asn1/ax;

    .line 42
    return-void
.end method

.method public static ay(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/c;
    .locals 2

    .prologue
    .line 19
    instance-of v0, p0, Lorg/spongycastle/asn1/q/c;

    if-eqz v0, :cond_0

    .line 21
    check-cast p0, Lorg/spongycastle/asn1/q/c;

    .line 28
    :goto_0
    return-object p0

    .line 23
    :cond_0
    if-eqz p0, :cond_1

    .line 25
    new-instance v0, Lorg/spongycastle/asn1/q/c;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/c;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 28
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 79
    iget-object v1, p0, Lorg/spongycastle/asn1/q/c;->Rb:Lorg/spongycastle/asn1/ax;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 80
    iget-object v1, p0, Lorg/spongycastle/asn1/q/c;->Rc:Lorg/spongycastle/asn1/ax;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 82
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public oq()Lorg/spongycastle/asn1/ax;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/spongycastle/asn1/q/c;->Rb:Lorg/spongycastle/asn1/ax;

    return-object v0
.end method

.method public or()Lorg/spongycastle/asn1/ax;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/spongycastle/asn1/q/c;->Rc:Lorg/spongycastle/asn1/ax;

    return-object v0
.end method
