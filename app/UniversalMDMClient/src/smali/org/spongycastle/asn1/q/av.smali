.class public Lorg/spongycastle/asn1/q/av;
.super Lorg/spongycastle/asn1/k;
.source "X509Extensions.java"


# static fields
.field public static final TA:Lorg/spongycastle/asn1/l;

.field public static final TB:Lorg/spongycastle/asn1/l;

.field public static final TD:Lorg/spongycastle/asn1/l;

.field public static final TE:Lorg/spongycastle/asn1/l;

.field public static final TF:Lorg/spongycastle/asn1/l;

.field public static final TG:Lorg/spongycastle/asn1/l;

.field public static final TH:Lorg/spongycastle/asn1/l;

.field public static final TI:Lorg/spongycastle/asn1/l;

.field public static final TJ:Lorg/spongycastle/asn1/l;

.field public static final TK:Lorg/spongycastle/asn1/l;

.field public static final TL:Lorg/spongycastle/asn1/l;

.field public static final TM:Lorg/spongycastle/asn1/l;

.field public static final TN:Lorg/spongycastle/asn1/l;

.field public static final TO:Lorg/spongycastle/asn1/l;

.field public static final TP:Lorg/spongycastle/asn1/l;

.field public static final TQ:Lorg/spongycastle/asn1/l;

.field public static final TR:Lorg/spongycastle/asn1/l;

.field public static final Tm:Lorg/spongycastle/asn1/l;

.field public static final Tn:Lorg/spongycastle/asn1/l;

.field public static final To:Lorg/spongycastle/asn1/l;

.field public static final Tp:Lorg/spongycastle/asn1/l;

.field public static final Tq:Lorg/spongycastle/asn1/l;

.field public static final Tr:Lorg/spongycastle/asn1/l;

.field public static final Ts:Lorg/spongycastle/asn1/l;

.field public static final Tt:Lorg/spongycastle/asn1/l;

.field public static final Tu:Lorg/spongycastle/asn1/l;

.field public static final Tv:Lorg/spongycastle/asn1/l;

.field public static final Tw:Lorg/spongycastle/asn1/l;

.field public static final Tx:Lorg/spongycastle/asn1/l;

.field public static final Ty:Lorg/spongycastle/asn1/l;

.field public static final Tz:Lorg/spongycastle/asn1/l;


# instance fields
.field private Sq:Ljava/util/Hashtable;

.field private Sr:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.9"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tm:Lorg/spongycastle/asn1/l;

    .line 34
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.14"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tn:Lorg/spongycastle/asn1/l;

    .line 40
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.15"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->To:Lorg/spongycastle/asn1/l;

    .line 46
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.16"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tp:Lorg/spongycastle/asn1/l;

    .line 52
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.17"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tq:Lorg/spongycastle/asn1/l;

    .line 58
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.18"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tr:Lorg/spongycastle/asn1/l;

    .line 64
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.19"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Ts:Lorg/spongycastle/asn1/l;

    .line 70
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.20"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tt:Lorg/spongycastle/asn1/l;

    .line 76
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.21"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tu:Lorg/spongycastle/asn1/l;

    .line 82
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.23"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tv:Lorg/spongycastle/asn1/l;

    .line 88
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.24"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tw:Lorg/spongycastle/asn1/l;

    .line 94
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.27"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tx:Lorg/spongycastle/asn1/l;

    .line 100
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.28"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Ty:Lorg/spongycastle/asn1/l;

    .line 106
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->Tz:Lorg/spongycastle/asn1/l;

    .line 112
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.30"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TA:Lorg/spongycastle/asn1/l;

    .line 118
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.31"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TB:Lorg/spongycastle/asn1/l;

    .line 124
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.32"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TD:Lorg/spongycastle/asn1/l;

    .line 130
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.33"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TE:Lorg/spongycastle/asn1/l;

    .line 136
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.35"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TF:Lorg/spongycastle/asn1/l;

    .line 142
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.36"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TG:Lorg/spongycastle/asn1/l;

    .line 148
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.37"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TH:Lorg/spongycastle/asn1/l;

    .line 154
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.46"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TI:Lorg/spongycastle/asn1/l;

    .line 160
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.54"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TJ:Lorg/spongycastle/asn1/l;

    .line 166
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TK:Lorg/spongycastle/asn1/l;

    .line 172
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.11"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TL:Lorg/spongycastle/asn1/l;

    .line 178
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.12"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TM:Lorg/spongycastle/asn1/l;

    .line 184
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TN:Lorg/spongycastle/asn1/l;

    .line 190
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TO:Lorg/spongycastle/asn1/l;

    .line 196
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.6.1.5.5.7.1.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TP:Lorg/spongycastle/asn1/l;

    .line 202
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.56"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TQ:Lorg/spongycastle/asn1/l;

    .line 208
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.5.29.55"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/q/av;->TR:Lorg/spongycastle/asn1/l;

    return-void
.end method

.method public constructor <init>(Ljava/util/Vector;Ljava/util/Vector;)V
    .locals 5

    .prologue
    .line 333
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 210
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/av;->Sq:Ljava/util/Hashtable;

    .line 211
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/av;->Sr:Ljava/util/Vector;

    .line 334
    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 336
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lorg/spongycastle/asn1/q/av;->Sr:Ljava/util/Vector;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 341
    :cond_0
    const/4 v0, 0x0

    .line 343
    iget-object v1, p0, Lorg/spongycastle/asn1/q/av;->Sr:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v3

    move v2, v0

    .line 345
    :goto_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 348
    invoke-virtual {p2, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/asn1/q/au;

    .line 350
    iget-object v4, p0, Lorg/spongycastle/asn1/q/av;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v4, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 352
    goto :goto_1

    .line 353
    :cond_1
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 253
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 210
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/av;->Sq:Ljava/util/Hashtable;

    .line 211
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/av;->Sr:Ljava/util/Vector;

    .line 254
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 256
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 258
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    .line 260
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 262
    iget-object v2, p0, Lorg/spongycastle/asn1/q/av;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v1, v7}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    new-instance v4, Lorg/spongycastle/asn1/q/au;

    invoke-virtual {v1, v8}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v5

    invoke-static {v5}, Lorg/spongycastle/asn1/ar;->O(Ljava/lang/Object;)Lorg/spongycastle/asn1/b;

    move-result-object v5

    invoke-virtual {v1, v9}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v6

    invoke-static {v6}, Lorg/spongycastle/asn1/m;->J(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/spongycastle/asn1/q/au;-><init>(Lorg/spongycastle/asn1/ar;Lorg/spongycastle/asn1/m;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    :goto_1
    iget-object v2, p0, Lorg/spongycastle/asn1/q/av;->Sr:Ljava/util/Vector;

    invoke-virtual {v1, v7}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 264
    :cond_0
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-ne v2, v9, :cond_1

    .line 266
    iget-object v2, p0, Lorg/spongycastle/asn1/q/av;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v1, v7}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    new-instance v4, Lorg/spongycastle/asn1/q/au;

    invoke-virtual {v1, v8}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v5

    invoke-static {v5}, Lorg/spongycastle/asn1/m;->J(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v5

    invoke-direct {v4, v7, v5}, Lorg/spongycastle/asn1/q/au;-><init>(ZLorg/spongycastle/asn1/m;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 270
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad sequence size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_2
    return-void
.end method

.method public static bi(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/av;
    .locals 3

    .prologue
    .line 223
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/av;

    if-eqz v0, :cond_1

    .line 225
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/av;

    .line 240
    :goto_0
    return-object p0

    .line 228
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_2

    .line 230
    new-instance v0, Lorg/spongycastle/asn1/q/av;

    check-cast p0, Lorg/spongycastle/asn1/r;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/av;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 233
    :cond_2
    instance-of v0, p0, Lorg/spongycastle/asn1/q/t;

    if-eqz v0, :cond_3

    .line 235
    new-instance v1, Lorg/spongycastle/asn1/q/av;

    check-cast p0, Lorg/spongycastle/asn1/q/t;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/q/t;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/q/av;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v1

    goto :goto_0

    .line 238
    :cond_3
    instance-of v0, p0, Lorg/spongycastle/asn1/x;

    if-eqz v0, :cond_4

    .line 240
    check-cast p0, Lorg/spongycastle/asn1/x;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/av;->bi(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/av;

    move-result-object p0

    goto :goto_0

    .line 243
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "illegal object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lorg/spongycastle/asn1/bc;)Lorg/spongycastle/asn1/q/au;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lorg/spongycastle/asn1/q/av;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/q/au;

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 6

    .prologue
    .line 398
    new-instance v2, Lorg/spongycastle/asn1/e;

    invoke-direct {v2}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 399
    iget-object v0, p0, Lorg/spongycastle/asn1/q/av;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v3

    .line 401
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 404
    iget-object v1, p0, Lorg/spongycastle/asn1/q/av;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/asn1/q/au;

    .line 405
    new-instance v4, Lorg/spongycastle/asn1/e;

    invoke-direct {v4}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 407
    invoke-virtual {v4, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 409
    invoke-virtual {v1}, Lorg/spongycastle/asn1/q/au;->isCritical()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    new-instance v0, Lorg/spongycastle/asn1/ar;

    const/4 v5, 0x1

    invoke-direct {v0, v5}, Lorg/spongycastle/asn1/ar;-><init>(Z)V

    invoke-virtual {v4, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 414
    :cond_0
    invoke-virtual {v1}, Lorg/spongycastle/asn1/q/au;->pI()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 416
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v4}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v2, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 419
    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v2}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v0
.end method

.method public oV()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lorg/spongycastle/asn1/q/av;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method
