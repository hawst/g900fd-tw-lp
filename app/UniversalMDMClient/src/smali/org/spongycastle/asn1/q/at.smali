.class public Lorg/spongycastle/asn1/q/at;
.super Lorg/spongycastle/asn1/k;
.source "X509CertificateStructure.java"

# interfaces
.implements Lorg/spongycastle/asn1/l/q;
.implements Lorg/spongycastle/asn1/q/ay;


# instance fields
.field Ks:Lorg/spongycastle/asn1/q/a;

.field NQ:Lorg/spongycastle/asn1/r;

.field Rx:Lorg/spongycastle/asn1/aq;

.field Tl:Lorg/spongycastle/asn1/q/an;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 57
    iput-object p1, p0, Lorg/spongycastle/asn1/q/at;->NQ:Lorg/spongycastle/asn1/r;

    .line 62
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/an;->bb(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/an;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    .line 65
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/at;->Ks:Lorg/spongycastle/asn1/q/a;

    .line 67
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/aq;->N(Ljava/lang/Object;)Lorg/spongycastle/asn1/aq;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/at;->Rx:Lorg/spongycastle/asn1/aq;

    .line 73
    return-void

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sequence wrong size for a certificate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static bh(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/at;
    .locals 2

    .prologue
    .line 42
    instance-of v0, p0, Lorg/spongycastle/asn1/q/at;

    if-eqz v0, :cond_0

    .line 44
    check-cast p0, Lorg/spongycastle/asn1/q/at;

    .line 51
    :goto_0
    return-object p0

    .line 46
    :cond_0
    if-eqz p0, :cond_1

    .line 48
    new-instance v0, Lorg/spongycastle/asn1/q/at;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/at;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 51
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static r(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/at;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0, p1}, Lorg/spongycastle/asn1/r;->b(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/r;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/at;->bh(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/at;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getVersion()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->getVersion()I

    move-result v0

    return v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public oE()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->oE()Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    return-object v0
.end method

.method public oI()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Ks:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public oJ()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Rx:Lorg/spongycastle/asn1/aq;

    return-object v0
.end method

.method public ow()Lorg/spongycastle/asn1/i;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->ow()Lorg/spongycastle/asn1/i;

    move-result-object v0

    return-object v0
.end method

.method public pH()Lorg/spongycastle/asn1/q/an;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    return-object v0
.end method

.method public pv()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->pv()Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    return-object v0
.end method

.method public pw()Lorg/spongycastle/asn1/q/ar;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->pw()Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    return-object v0
.end method

.method public px()Lorg/spongycastle/asn1/p/c;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->px()Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    return-object v0
.end method

.method public py()Lorg/spongycastle/asn1/q/ah;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/spongycastle/asn1/q/at;->Tl:Lorg/spongycastle/asn1/q/an;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/an;->py()Lorg/spongycastle/asn1/q/ah;

    move-result-object v0

    return-object v0
.end method
