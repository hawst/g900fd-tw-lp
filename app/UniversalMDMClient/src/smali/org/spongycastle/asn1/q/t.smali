.class public Lorg/spongycastle/asn1/q/t;
.super Lorg/spongycastle/asn1/k;
.source "Extensions.java"


# instance fields
.field private Sq:Ljava/util/Hashtable;

.field private Sr:Ljava/util/Vector;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 52
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/t;->Sq:Ljava/util/Hashtable;

    .line 21
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/t;->Sr:Ljava/util/Vector;

    .line 53
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 55
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 57
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 61
    iget-object v2, p0, Lorg/spongycastle/asn1/q/t;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v1, v8}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    new-instance v4, Lorg/spongycastle/asn1/q/s;

    invoke-virtual {v1, v8}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v5

    invoke-static {v5}, Lorg/spongycastle/asn1/l;->T(Ljava/lang/Object;)Lorg/spongycastle/asn1/l;

    move-result-object v5

    invoke-virtual {v1, v9}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v6

    invoke-static {v6}, Lorg/spongycastle/asn1/b;->O(Ljava/lang/Object;)Lorg/spongycastle/asn1/b;

    move-result-object v6

    invoke-virtual {v1, v10}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v7

    invoke-static {v7}, Lorg/spongycastle/asn1/m;->J(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lorg/spongycastle/asn1/q/s;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/b;Lorg/spongycastle/asn1/m;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    :goto_1
    iget-object v2, p0, Lorg/spongycastle/asn1/q/t;->Sr:Ljava/util/Vector;

    invoke-virtual {v1, v8}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_0
    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    if-ne v2, v10, :cond_1

    .line 65
    iget-object v2, p0, Lorg/spongycastle/asn1/q/t;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v1, v8}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v3

    new-instance v4, Lorg/spongycastle/asn1/q/s;

    invoke-virtual {v1, v8}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v5

    invoke-static {v5}, Lorg/spongycastle/asn1/l;->T(Ljava/lang/Object;)Lorg/spongycastle/asn1/l;

    move-result-object v5

    invoke-virtual {v1, v9}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v6

    invoke-static {v6}, Lorg/spongycastle/asn1/m;->J(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v6

    invoke-direct {v4, v5, v8, v6}, Lorg/spongycastle/asn1/q/s;-><init>(Lorg/spongycastle/asn1/l;ZLorg/spongycastle/asn1/m;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad sequence size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_2
    return-void
.end method

.method public static aN(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/t;
    .locals 2

    .prologue
    .line 33
    instance-of v0, p0, Lorg/spongycastle/asn1/q/t;

    if-eqz v0, :cond_0

    .line 35
    check-cast p0, Lorg/spongycastle/asn1/q/t;

    .line 42
    :goto_0
    return-object p0

    .line 37
    :cond_0
    if-eqz p0, :cond_1

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/q/t;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/t;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 42
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public e(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/q/s;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/spongycastle/asn1/q/t;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/q/s;

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    .line 125
    new-instance v2, Lorg/spongycastle/asn1/e;

    invoke-direct {v2}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 126
    iget-object v0, p0, Lorg/spongycastle/asn1/q/t;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v3

    .line 128
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 131
    iget-object v1, p0, Lorg/spongycastle/asn1/q/t;->Sq:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/asn1/q/s;

    .line 132
    new-instance v4, Lorg/spongycastle/asn1/e;

    invoke-direct {v4}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 134
    invoke-virtual {v4, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 136
    invoke-virtual {v1}, Lorg/spongycastle/asn1/q/s;->isCritical()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/spongycastle/asn1/b;->S(Z)Lorg/spongycastle/asn1/b;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 141
    :cond_0
    invoke-virtual {v1}, Lorg/spongycastle/asn1/q/s;->oT()Lorg/spongycastle/asn1/m;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 143
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v4}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    invoke-virtual {v2, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 146
    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v2}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v0
.end method

.method public oV()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/spongycastle/asn1/q/t;->Sr:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method
