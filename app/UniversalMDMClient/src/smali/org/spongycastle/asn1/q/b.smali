.class public Lorg/spongycastle/asn1/q/b;
.super Lorg/spongycastle/asn1/k;
.source "AttCertIssuer.java"

# interfaces
.implements Lorg/spongycastle/asn1/c;


# instance fields
.field FW:Lorg/spongycastle/asn1/d;

.field Ra:Lorg/spongycastle/asn1/q;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/as;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 68
    iput-object p1, p0, Lorg/spongycastle/asn1/q/b;->FW:Lorg/spongycastle/asn1/d;

    .line 69
    new-instance v0, Lorg/spongycastle/asn1/bm;

    iget-object v1, p0, Lorg/spongycastle/asn1/q/b;->FW:Lorg/spongycastle/asn1/d;

    invoke-direct {v0, v2, v2, v1}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/q/b;->Ra:Lorg/spongycastle/asn1/q;

    .line 70
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q/v;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/spongycastle/asn1/q/b;->FW:Lorg/spongycastle/asn1/d;

    .line 62
    iget-object v0, p0, Lorg/spongycastle/asn1/q/b;->FW:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/b;->Ra:Lorg/spongycastle/asn1/q;

    .line 63
    return-void
.end method

.method public static ax(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/b;
    .locals 3

    .prologue
    .line 21
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/b;

    if-eqz v0, :cond_1

    .line 23
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/b;

    .line 39
    :goto_0
    return-object p0

    .line 25
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/q/as;

    if-eqz v0, :cond_2

    .line 27
    new-instance v0, Lorg/spongycastle/asn1/q/b;

    invoke-static {p0}, Lorg/spongycastle/asn1/q/as;->bg(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/as;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/b;-><init>(Lorg/spongycastle/asn1/q/as;)V

    move-object p0, v0

    goto :goto_0

    .line 29
    :cond_2
    instance-of v0, p0, Lorg/spongycastle/asn1/q/v;

    if-eqz v0, :cond_3

    .line 31
    new-instance v0, Lorg/spongycastle/asn1/q/b;

    check-cast p0, Lorg/spongycastle/asn1/q/v;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/b;-><init>(Lorg/spongycastle/asn1/q/v;)V

    move-object p0, v0

    goto :goto_0

    .line 33
    :cond_3
    instance-of v0, p0, Lorg/spongycastle/asn1/x;

    if-eqz v0, :cond_4

    .line 35
    new-instance v0, Lorg/spongycastle/asn1/q/b;

    check-cast p0, Lorg/spongycastle/asn1/x;

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lorg/spongycastle/asn1/q/as;->q(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/as;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/b;-><init>(Lorg/spongycastle/asn1/q/as;)V

    move-object p0, v0

    goto :goto_0

    .line 37
    :cond_4
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_5

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/q/b;

    invoke-static {p0}, Lorg/spongycastle/asn1/q/v;->aP(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/v;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/b;-><init>(Lorg/spongycastle/asn1/q/v;)V

    move-object p0, v0

    goto :goto_0

    .line 42
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in factory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/spongycastle/asn1/q/b;->Ra:Lorg/spongycastle/asn1/q;

    return-object v0
.end method

.method public op()Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/asn1/q/b;->FW:Lorg/spongycastle/asn1/d;

    return-object v0
.end method
