.class public Lorg/spongycastle/asn1/q/aj;
.super Lorg/spongycastle/asn1/k;
.source "TBSCertList.java"


# instance fields
.field NQ:Lorg/spongycastle/asn1/r;

.field SX:Lorg/spongycastle/asn1/q/t;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 51
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_1
    iput-object p1, p0, Lorg/spongycastle/asn1/q/aj;->NQ:Lorg/spongycastle/asn1/r;

    .line 57
    return-void
.end method

.method public static aZ(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/aj;
    .locals 2

    .prologue
    .line 61
    instance-of v0, p0, Lorg/spongycastle/asn1/q/aj;

    if-eqz v0, :cond_0

    .line 63
    check-cast p0, Lorg/spongycastle/asn1/q/aj;

    .line 70
    :goto_0
    return-object p0

    .line 65
    :cond_0
    if-eqz p0, :cond_1

    .line 67
    new-instance v0, Lorg/spongycastle/asn1/q/aj;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/aj;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 70
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public hasExtensions()Z
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->NQ:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public oz()Lorg/spongycastle/asn1/q/t;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->SX:Lorg/spongycastle/asn1/q/t;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->NQ:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 87
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->NQ:Lorg/spongycastle/asn1/r;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/t;->aN(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/q/aj;->SX:Lorg/spongycastle/asn1/q/t;

    .line 90
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->SX:Lorg/spongycastle/asn1/q/t;

    return-object v0
.end method

.method public pt()Lorg/spongycastle/asn1/i;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->NQ:Lorg/spongycastle/asn1/r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v0

    return-object v0
.end method

.method public pu()Lorg/spongycastle/asn1/q/ar;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lorg/spongycastle/asn1/q/aj;->NQ:Lorg/spongycastle/asn1/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ar;->bf(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ar;

    move-result-object v0

    return-object v0
.end method
