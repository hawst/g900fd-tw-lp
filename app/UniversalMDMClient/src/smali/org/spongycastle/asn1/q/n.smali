.class public Lorg/spongycastle/asn1/q/n;
.super Lorg/spongycastle/asn1/k;
.source "CertificatePair.java"


# instance fields
.field private RB:Lorg/spongycastle/asn1/q/at;

.field private Rz:Lorg/spongycastle/asn1/q/at;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/at;Lorg/spongycastle/asn1/q/at;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 120
    iput-object p1, p0, Lorg/spongycastle/asn1/q/n;->Rz:Lorg/spongycastle/asn1/q/at;

    .line 121
    iput-object p2, p0, Lorg/spongycastle/asn1/q/n;->RB:Lorg/spongycastle/asn1/q/at;

    .line 122
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 84
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 85
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 93
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 95
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v1

    .line 96
    invoke-virtual {v1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    if-nez v2, :cond_1

    .line 98
    invoke-static {v1, v3}, Lorg/spongycastle/asn1/q/at;->r(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/at;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/n;->Rz:Lorg/spongycastle/asn1/q/at;

    goto :goto_0

    .line 100
    :cond_1
    invoke-virtual {v1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 102
    invoke-static {v1, v3}, Lorg/spongycastle/asn1/q/at;->r(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/at;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/q/n;->RB:Lorg/spongycastle/asn1/q/at;

    goto :goto_0

    .line 106
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad tag number: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_3
    return-void
.end method

.method public static aI(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/n;
    .locals 3

    .prologue
    .line 55
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/q/n;

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/q/n;

    .line 62
    :goto_0
    return-object p0

    .line 60
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_2

    .line 62
    new-instance v0, Lorg/spongycastle/asn1/q/n;

    check-cast p0, Lorg/spongycastle/asn1/r;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/q/n;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 65
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "illegal object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    .line 140
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 142
    iget-object v1, p0, Lorg/spongycastle/asn1/q/n;->Rz:Lorg/spongycastle/asn1/q/at;

    if-eqz v1, :cond_0

    .line 144
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/spongycastle/asn1/q/n;->Rz:Lorg/spongycastle/asn1/q/at;

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 146
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/q/n;->RB:Lorg/spongycastle/asn1/q/at;

    if-eqz v1, :cond_1

    .line 148
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/spongycastle/asn1/q/n;->RB:Lorg/spongycastle/asn1/q/at;

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 151
    :cond_1
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public oN()Lorg/spongycastle/asn1/q/at;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lorg/spongycastle/asn1/q/n;->Rz:Lorg/spongycastle/asn1/q/at;

    return-object v0
.end method

.method public oO()Lorg/spongycastle/asn1/q/at;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lorg/spongycastle/asn1/q/n;->RB:Lorg/spongycastle/asn1/q/at;

    return-object v0
.end method
