.class public abstract Lorg/spongycastle/asn1/t;
.super Lorg/spongycastle/asn1/q;
.source "ASN1Set.java"


# instance fields
.field private FQ:Ljava/util/Vector;

.field private FR:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 11
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/t;->FR:Z

    .line 138
    return-void
.end method

.method protected constructor <init>(Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 11
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/t;->FR:Z

    .line 146
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 147
    return-void
.end method

.method protected constructor <init>(Lorg/spongycastle/asn1/e;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 155
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 11
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    .line 12
    iput-boolean v0, p0, Lorg/spongycastle/asn1/t;->FR:Z

    .line 156
    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/e;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 158
    iget-object v1, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/e;->bE(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_0
    if-eqz p2, :cond_1

    .line 163
    invoke-virtual {p0}, Lorg/spongycastle/asn1/t;->sort()V

    .line 165
    :cond_1
    return-void
.end method

.method protected constructor <init>([Lorg/spongycastle/asn1/d;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 173
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 11
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    .line 12
    iput-boolean v0, p0, Lorg/spongycastle/asn1/t;->FR:Z

    .line 174
    :goto_0
    array-length v1, p1

    if-eq v0, v1, :cond_0

    .line 176
    iget-object v1, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :cond_0
    if-eqz p2, :cond_1

    .line 181
    invoke-virtual {p0}, Lorg/spongycastle/asn1/t;->sort()V

    .line 183
    :cond_1
    return-void
.end method

.method public static L(Ljava/lang/Object;)Lorg/spongycastle/asn1/t;
    .locals 4

    .prologue
    .line 23
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/t;

    if-eqz v0, :cond_1

    .line 25
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/t;

    move-object v0, p0

    .line 48
    :goto_0
    return-object v0

    .line 27
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/u;

    if-eqz v0, :cond_2

    .line 29
    check-cast p0, Lorg/spongycastle/asn1/u;

    invoke-interface {p0}, Lorg/spongycastle/asn1/u;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/t;->L(Ljava/lang/Object;)Lorg/spongycastle/asn1/t;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_2
    instance-of v0, p0, [B

    if-eqz v0, :cond_3

    .line 35
    :try_start_0
    check-cast p0, [B

    check-cast p0, [B

    invoke-static {p0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/t;->L(Ljava/lang/Object;)Lorg/spongycastle/asn1/t;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to construct set from byte[]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42
    :cond_3
    instance-of v0, p0, Lorg/spongycastle/asn1/d;

    if-eqz v0, :cond_4

    move-object v0, p0

    .line 44
    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 46
    instance-of v1, v0, Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_4

    .line 48
    check-cast v0, Lorg/spongycastle/asn1/t;

    goto :goto_0

    .line 52
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ljava/util/Enumeration;)Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 359
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    .line 362
    if-nez v0, :cond_0

    .line 364
    sget-object v0, Lorg/spongycastle/asn1/ba;->GC:Lorg/spongycastle/asn1/ba;

    .line 367
    :cond_0
    return-object v0
.end method

.method private a([B[B)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 377
    array-length v2, p1

    array-length v3, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v2, v1

    .line 378
    :goto_0
    if-eq v2, v3, :cond_3

    .line 380
    aget-byte v4, p1, v2

    aget-byte v5, p2, v2

    if-eq v4, v5, :cond_2

    .line 382
    aget-byte v3, p1, v2

    and-int/lit16 v3, v3, 0xff

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    if-ge v3, v2, :cond_1

    .line 385
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 382
    goto :goto_1

    .line 378
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 385
    :cond_3
    array-length v2, p1

    if-eq v3, v2, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public static c(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/t;
    .locals 3

    .prologue
    .line 75
    if-eqz p1, :cond_1

    .line 77
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "object implicit - explicit expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/t;

    .line 127
    :goto_0
    return-object v0

    .line 91
    :cond_1
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mU()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 93
    instance-of v0, p0, Lorg/spongycastle/asn1/al;

    if-eqz v0, :cond_2

    .line 95
    new-instance v0, Lorg/spongycastle/asn1/aj;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/aj;-><init>(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 99
    :cond_2
    new-instance v0, Lorg/spongycastle/asn1/bt;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bt;-><init>(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    .line 104
    :cond_3
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/t;

    if-eqz v0, :cond_4

    .line 106
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/t;

    goto :goto_0

    .line 115
    :cond_4
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 117
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_6

    .line 119
    invoke-virtual {p0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r;

    .line 121
    instance-of v1, p0, Lorg/spongycastle/asn1/al;

    if-eqz v1, :cond_5

    .line 123
    new-instance v1, Lorg/spongycastle/asn1/aj;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->mP()[Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/aj;-><init>([Lorg/spongycastle/asn1/d;)V

    move-object v0, v1

    goto :goto_0

    .line 127
    :cond_5
    new-instance v1, Lorg/spongycastle/asn1/bt;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->mP()[Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bt;-><init>([Lorg/spongycastle/asn1/d;)V

    move-object v0, v1

    goto :goto_0

    .line 133
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c(Lorg/spongycastle/asn1/d;)[B
    .locals 2

    .prologue
    .line 391
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 392
    new-instance v1, Lorg/spongycastle/asn1/o;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/o;-><init>(Ljava/io/OutputStream;)V

    .line 396
    :try_start_0
    invoke-virtual {v1, p1}, Lorg/spongycastle/asn1/o;->b(Lorg/spongycastle/asn1/d;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 398
    :catch_0
    move-exception v0

    .line 400
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot encode object added to SET"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 323
    instance-of v1, p1, Lorg/spongycastle/asn1/t;

    if-nez v1, :cond_1

    .line 354
    :cond_0
    :goto_0
    return v0

    .line 328
    :cond_1
    check-cast p1, Lorg/spongycastle/asn1/t;

    .line 330
    invoke-virtual {p0}, Lorg/spongycastle/asn1/t;->size()I

    move-result v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/t;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 335
    invoke-virtual {p0}, Lorg/spongycastle/asn1/t;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 336
    invoke-virtual {p1}, Lorg/spongycastle/asn1/t;->mQ()Ljava/util/Enumeration;

    move-result-object v2

    .line 338
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 340
    invoke-direct {p0, v1}, Lorg/spongycastle/asn1/t;->a(Ljava/util/Enumeration;)Lorg/spongycastle/asn1/d;

    move-result-object v3

    .line 341
    invoke-direct {p0, v2}, Lorg/spongycastle/asn1/t;->a(Ljava/util/Enumeration;)Lorg/spongycastle/asn1/d;

    move-result-object v4

    .line 343
    invoke-interface {v3}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v3

    .line 344
    invoke-interface {v4}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v4

    .line 346
    if-eq v3, v4, :cond_2

    invoke-virtual {v3, v4}, Lorg/spongycastle/asn1/q;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 354
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bG(I)Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 268
    invoke-virtual {p0}, Lorg/spongycastle/asn1/t;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 269
    invoke-virtual {p0}, Lorg/spongycastle/asn1/t;->size()I

    move-result v0

    .line 271
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 273
    invoke-direct {p0, v1}, Lorg/spongycastle/asn1/t;->a(Ljava/util/Enumeration;)Lorg/spongycastle/asn1/d;

    move-result-object v2

    .line 274
    mul-int/lit8 v0, v0, 0x11

    .line 276
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 277
    goto :goto_0

    .line 279
    :cond_0
    return v0
.end method

.method mJ()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 284
    iget-boolean v0, p0, Lorg/spongycastle/asn1/t;->FR:Z

    if-eqz v0, :cond_0

    .line 286
    new-instance v0, Lorg/spongycastle/asn1/bj;

    invoke-direct {v0}, Lorg/spongycastle/asn1/bj;-><init>()V

    .line 288
    iget-object v1, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    iput-object v1, v0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    .line 307
    :goto_0
    return-object v0

    .line 294
    :cond_0
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 296
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 298
    iget-object v2, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 296
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 301
    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/bj;

    invoke-direct {v0}, Lorg/spongycastle/asn1/bj;-><init>()V

    .line 303
    iput-object v1, v0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    .line 305
    invoke-virtual {v0}, Lorg/spongycastle/asn1/t;->sort()V

    goto :goto_0
.end method

.method mK()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 313
    new-instance v0, Lorg/spongycastle/asn1/bt;

    invoke-direct {v0}, Lorg/spongycastle/asn1/bt;-><init>()V

    .line 315
    iget-object v1, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    iput-object v1, v0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    .line 317
    return-object v0
.end method

.method mN()Z
    .locals 1

    .prologue
    .line 454
    const/4 v0, 0x1

    return v0
.end method

.method public mQ()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method protected sort()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 408
    iget-boolean v0, p0, Lorg/spongycastle/asn1/t;->FR:Z

    if-nez v0, :cond_2

    .line 410
    iput-boolean v5, p0, Lorg/spongycastle/asn1/t;->FR:Z

    .line 411
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, v5, :cond_2

    .line 414
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    move v7, v2

    move v4, v5

    .line 416
    :goto_0
    if-eqz v4, :cond_2

    .line 420
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/t;->c(Lorg/spongycastle/asn1/d;)[B

    move-result-object v1

    move v2, v6

    move v3, v6

    move v4, v6

    .line 424
    :goto_1
    if-eq v3, v7, :cond_1

    .line 426
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v0, v8}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/t;->c(Lorg/spongycastle/asn1/d;)[B

    move-result-object v0

    .line 428
    invoke-direct {p0, v1, v0}, Lorg/spongycastle/asn1/t;->a([B[B)Z

    move-result v8

    if-eqz v8, :cond_0

    move v1, v2

    move v2, v4

    .line 443
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v4, v2

    move v2, v1

    move-object v1, v0

    .line 444
    goto :goto_1

    .line 434
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 436
    iget-object v2, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    iget-object v4, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v4, v8}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 437
    iget-object v2, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v2, v0, v4}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    move-object v0, v1

    move v2, v5

    move v1, v3

    .line 440
    goto :goto_2

    :cond_1
    move v7, v2

    .line 447
    goto :goto_0

    .line 450
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lorg/spongycastle/asn1/t;->FQ:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
