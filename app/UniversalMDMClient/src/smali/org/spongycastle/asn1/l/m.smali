.class public Lorg/spongycastle/asn1/l/m;
.super Lorg/spongycastle/asn1/k;
.source "PBEParameter.java"


# instance fields
.field KD:Lorg/spongycastle/asn1/i;

.field KE:Lorg/spongycastle/asn1/m;


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 35
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/m;->KE:Lorg/spongycastle/asn1/m;

    .line 36
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/m;->KD:Lorg/spongycastle/asn1/i;

    .line 37
    return-void
.end method

.method public static af(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/m;
    .locals 2

    .prologue
    .line 42
    instance-of v0, p0, Lorg/spongycastle/asn1/l/m;

    if-eqz v0, :cond_0

    .line 44
    check-cast p0, Lorg/spongycastle/asn1/l/m;

    .line 51
    :goto_0
    return-object p0

    .line 46
    :cond_0
    if-eqz p0, :cond_1

    .line 48
    new-instance v0, Lorg/spongycastle/asn1/l/m;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/m;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 51
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getSalt()[B
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/spongycastle/asn1/l/m;->KE:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 68
    iget-object v1, p0, Lorg/spongycastle/asn1/l/m;->KE:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 69
    iget-object v1, p0, Lorg/spongycastle/asn1/l/m;->KD:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 71
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nC()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/spongycastle/asn1/l/m;->KD:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
