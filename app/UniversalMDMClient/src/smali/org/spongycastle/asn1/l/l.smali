.class public Lorg/spongycastle/asn1/l/l;
.super Lorg/spongycastle/asn1/k;
.source "MacData.java"


# static fields
.field private static final ONE:Ljava/math/BigInteger;


# instance fields
.field KB:Lorg/spongycastle/asn1/q/p;

.field KC:Ljava/math/BigInteger;

.field salt:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/l/l;->ONE:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q/p;[BI)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/spongycastle/asn1/l/l;->KB:Lorg/spongycastle/asn1/q/p;

    .line 62
    iput-object p2, p0, Lorg/spongycastle/asn1/l/l;->salt:[B

    .line 63
    int-to-long v0, p3

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/l;->KC:Ljava/math/BigInteger;

    .line 64
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 42
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/p;->aK(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/p;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/l;->KB:Lorg/spongycastle/asn1/q/p;

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/l;->salt:[B

    .line 46
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 48
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/l;->KC:Ljava/math/BigInteger;

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    sget-object v0, Lorg/spongycastle/asn1/l/l;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/l;->KC:Ljava/math/BigInteger;

    goto :goto_0
.end method

.method public static ae(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/l;
    .locals 2

    .prologue
    .line 27
    instance-of v0, p0, Lorg/spongycastle/asn1/l/l;

    if-eqz v0, :cond_0

    .line 29
    check-cast p0, Lorg/spongycastle/asn1/l/l;

    .line 36
    :goto_0
    return-object p0

    .line 31
    :cond_0
    if-eqz p0, :cond_1

    .line 33
    new-instance v0, Lorg/spongycastle/asn1/l/l;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/l;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 36
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getSalt()[B
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/spongycastle/asn1/l/l;->salt:[B

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 96
    iget-object v1, p0, Lorg/spongycastle/asn1/l/l;->KB:Lorg/spongycastle/asn1/q/p;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 97
    new-instance v1, Lorg/spongycastle/asn1/bd;

    iget-object v2, p0, Lorg/spongycastle/asn1/l/l;->salt:[B

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 99
    iget-object v1, p0, Lorg/spongycastle/asn1/l/l;->KC:Ljava/math/BigInteger;

    sget-object v2, Lorg/spongycastle/asn1/l/l;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    new-instance v1, Lorg/spongycastle/asn1/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/l/l;->KC:Ljava/math/BigInteger;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 104
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nB()Lorg/spongycastle/asn1/q/p;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/spongycastle/asn1/l/l;->KB:Lorg/spongycastle/asn1/q/p;

    return-object v0
.end method

.method public nC()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/spongycastle/asn1/l/l;->KC:Ljava/math/BigInteger;

    return-object v0
.end method
