.class public Lorg/spongycastle/asn1/l/g;
.super Lorg/spongycastle/asn1/k;
.source "DHParameter.java"


# instance fields
.field HN:Lorg/spongycastle/asn1/i;

.field JY:Lorg/spongycastle/asn1/i;

.field Ky:Lorg/spongycastle/asn1/i;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;I)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 23
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/g;->HN:Lorg/spongycastle/asn1/i;

    .line 24
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/g;->JY:Lorg/spongycastle/asn1/i;

    .line 26
    if-eqz p3, :cond_0

    .line 28
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p3}, Lorg/spongycastle/asn1/i;-><init>(I)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/g;->Ky:Lorg/spongycastle/asn1/i;

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/g;->Ky:Lorg/spongycastle/asn1/i;

    goto :goto_0
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 55
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 57
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/l/g;->HN:Lorg/spongycastle/asn1/i;

    .line 58
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/i;->S(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/l/g;->JY:Lorg/spongycastle/asn1/i;

    .line 60
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/g;->Ky:Lorg/spongycastle/asn1/i;

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/g;->Ky:Lorg/spongycastle/asn1/i;

    goto :goto_0
.end method

.method public static aa(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/g;
    .locals 2

    .prologue
    .line 39
    instance-of v0, p0, Lorg/spongycastle/asn1/l/g;

    if-eqz v0, :cond_0

    .line 41
    check-cast p0, Lorg/spongycastle/asn1/l/g;

    .line 49
    :goto_0
    return-object p0

    .line 44
    :cond_0
    if-eqz p0, :cond_1

    .line 46
    new-instance v0, Lorg/spongycastle/asn1/l/g;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/g;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 49
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getG()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/spongycastle/asn1/l/g;->JY:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getP()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/spongycastle/asn1/l/g;->HN:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 94
    iget-object v1, p0, Lorg/spongycastle/asn1/l/g;->HN:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 95
    iget-object v1, p0, Lorg/spongycastle/asn1/l/g;->JY:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 97
    invoke-virtual {p0}, Lorg/spongycastle/asn1/l/g;->ny()Ljava/math/BigInteger;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lorg/spongycastle/asn1/l/g;->Ky:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 102
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public ny()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/spongycastle/asn1/l/g;->Ky:Lorg/spongycastle/asn1/i;

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/l/g;->Ky:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0
.end method
