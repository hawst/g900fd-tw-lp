.class public Lorg/spongycastle/asn1/l/b;
.super Lorg/spongycastle/asn1/k;
.source "AuthenticatedSafe.java"


# instance fields
.field private Kn:[Lorg/spongycastle/asn1/l/f;

.field private Ko:Z


# direct methods
.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/l/b;->Ko:Z

    .line 19
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    new-array v0, v0, [Lorg/spongycastle/asn1/l/f;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/b;->Kn:[Lorg/spongycastle/asn1/l/f;

    .line 21
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/spongycastle/asn1/l/b;->Kn:[Lorg/spongycastle/asn1/l/f;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 23
    iget-object v1, p0, Lorg/spongycastle/asn1/l/b;->Kn:[Lorg/spongycastle/asn1/l/f;

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/l/f;->Z(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/f;

    move-result-object v2

    aput-object v2, v1, v0

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_0
    instance-of v0, p1, Lorg/spongycastle/asn1/ah;

    iput-boolean v0, p0, Lorg/spongycastle/asn1/l/b;->Ko:Z

    .line 27
    return-void
.end method

.method public constructor <init>([Lorg/spongycastle/asn1/l/f;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/l/b;->Ko:Z

    .line 48
    iput-object p1, p0, Lorg/spongycastle/asn1/l/b;->Kn:[Lorg/spongycastle/asn1/l/f;

    .line 49
    return-void
.end method

.method public static W(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/b;
    .locals 2

    .prologue
    .line 32
    instance-of v0, p0, Lorg/spongycastle/asn1/l/b;

    if-eqz v0, :cond_0

    .line 34
    check-cast p0, Lorg/spongycastle/asn1/l/b;

    .line 42
    :goto_0
    return-object p0

    .line 37
    :cond_0
    if-eqz p0, :cond_1

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/l/b;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/b;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 42
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 58
    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 60
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lorg/spongycastle/asn1/l/b;->Kn:[Lorg/spongycastle/asn1/l/f;

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 62
    iget-object v2, p0, Lorg/spongycastle/asn1/l/b;->Kn:[Lorg/spongycastle/asn1/l/f;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    iget-boolean v0, p0, Lorg/spongycastle/asn1/l/b;->Ko:Z

    if-eqz v0, :cond_1

    .line 67
    new-instance v0, Lorg/spongycastle/asn1/ah;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    .line 71
    :goto_1
    return-object v0

    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/bs;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bs;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_1
.end method

.method public nt()[Lorg/spongycastle/asn1/l/f;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/spongycastle/asn1/l/b;->Kn:[Lorg/spongycastle/asn1/l/f;

    return-object v0
.end method
