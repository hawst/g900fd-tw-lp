.class public Lorg/spongycastle/asn1/l/n;
.super Lorg/spongycastle/asn1/k;
.source "PBES2Parameters.java"

# interfaces
.implements Lorg/spongycastle/asn1/l/q;


# instance fields
.field private KF:Lorg/spongycastle/asn1/l/k;

.field private KG:Lorg/spongycastle/asn1/l/j;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 5

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 38
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 39
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    .line 41
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v2

    sget-object v3, Lorg/spongycastle/asn1/l/n;->Li:Lorg/spongycastle/asn1/l;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    new-instance v2, Lorg/spongycastle/asn1/l/k;

    sget-object v3, Lorg/spongycastle/asn1/l/n;->Li:Lorg/spongycastle/asn1/l;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/o;->ah(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/o;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/spongycastle/asn1/l/k;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    iput-object v2, p0, Lorg/spongycastle/asn1/l/n;->KF:Lorg/spongycastle/asn1/l/k;

    .line 50
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/j;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l/j;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/n;->KG:Lorg/spongycastle/asn1/l/j;

    .line 51
    return-void

    .line 47
    :cond_0
    new-instance v2, Lorg/spongycastle/asn1/l/k;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/l/k;-><init>(Lorg/spongycastle/asn1/r;)V

    iput-object v2, p0, Lorg/spongycastle/asn1/l/n;->KF:Lorg/spongycastle/asn1/l/k;

    goto :goto_0
.end method

.method public static ag(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/n;
    .locals 3

    .prologue
    .line 22
    if-eqz p0, :cond_0

    instance-of v0, p0, Lorg/spongycastle/asn1/l/n;

    if-eqz v0, :cond_1

    .line 24
    :cond_0
    check-cast p0, Lorg/spongycastle/asn1/l/n;

    .line 29
    :goto_0
    return-object p0

    .line 27
    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_2

    .line 29
    new-instance v0, Lorg/spongycastle/asn1/l/n;

    check-cast p0, Lorg/spongycastle/asn1/r;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/l/n;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 32
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown object in factory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 67
    iget-object v1, p0, Lorg/spongycastle/asn1/l/n;->KF:Lorg/spongycastle/asn1/l/k;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 68
    iget-object v1, p0, Lorg/spongycastle/asn1/l/n;->KG:Lorg/spongycastle/asn1/l/j;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 70
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nD()Lorg/spongycastle/asn1/l/k;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/spongycastle/asn1/l/n;->KF:Lorg/spongycastle/asn1/l/k;

    return-object v0
.end method

.method public nE()Lorg/spongycastle/asn1/l/j;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/spongycastle/asn1/l/n;->KG:Lorg/spongycastle/asn1/l/j;

    return-object v0
.end method
