.class public Lorg/spongycastle/asn1/l/r;
.super Lorg/spongycastle/asn1/k;
.source "Pfx.java"

# interfaces
.implements Lorg/spongycastle/asn1/l/q;


# instance fields
.field private No:Lorg/spongycastle/asn1/l/f;

.field private Np:Lorg/spongycastle/asn1/l/l;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l/f;Lorg/spongycastle/asn1/l/l;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/r;->Np:Lorg/spongycastle/asn1/l/l;

    .line 59
    iput-object p1, p0, Lorg/spongycastle/asn1/l/r;->No:Lorg/spongycastle/asn1/l/f;

    .line 60
    iput-object p2, p0, Lorg/spongycastle/asn1/l/r;->Np:Lorg/spongycastle/asn1/l/l;

    .line 61
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 24
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/r;->Np:Lorg/spongycastle/asn1/l/l;

    .line 25
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "wrong version for PFX PDU"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/f;->Z(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/f;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/r;->No:Lorg/spongycastle/asn1/l/f;

    .line 33
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 35
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/l;->ae(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/l;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/r;->Np:Lorg/spongycastle/asn1/l/l;

    .line 37
    :cond_1
    return-void
.end method

.method public static aj(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/r;
    .locals 2

    .prologue
    .line 42
    instance-of v0, p0, Lorg/spongycastle/asn1/l/r;

    if-eqz v0, :cond_0

    .line 44
    check-cast p0, Lorg/spongycastle/asn1/l/r;

    .line 52
    :goto_0
    return-object p0

    .line 47
    :cond_0
    if-eqz p0, :cond_1

    .line 49
    new-instance v0, Lorg/spongycastle/asn1/l/r;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/r;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 52
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 75
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 77
    new-instance v1, Lorg/spongycastle/asn1/i;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 78
    iget-object v1, p0, Lorg/spongycastle/asn1/l/r;->No:Lorg/spongycastle/asn1/l/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 80
    iget-object v1, p0, Lorg/spongycastle/asn1/l/r;->Np:Lorg/spongycastle/asn1/l/l;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lorg/spongycastle/asn1/l/r;->Np:Lorg/spongycastle/asn1/l/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 85
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/ah;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nG()Lorg/spongycastle/asn1/l/f;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/spongycastle/asn1/l/r;->No:Lorg/spongycastle/asn1/l/f;

    return-object v0
.end method

.method public nH()Lorg/spongycastle/asn1/l/l;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/spongycastle/asn1/l/r;->Np:Lorg/spongycastle/asn1/l/l;

    return-object v0
.end method
