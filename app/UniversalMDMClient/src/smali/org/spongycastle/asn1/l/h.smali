.class public Lorg/spongycastle/asn1/l/h;
.super Lorg/spongycastle/asn1/k;
.source "EncryptedData.java"


# instance fields
.field Kz:Lorg/spongycastle/asn1/r;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 75
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 77
    invoke-virtual {v0, p1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 78
    invoke-virtual {p2}, Lorg/spongycastle/asn1/q/a;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 79
    new-instance v1, Lorg/spongycastle/asn1/al;

    invoke-direct {v1, v2, v2, p3}, Lorg/spongycastle/asn1/al;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 81
    new-instance v1, Lorg/spongycastle/asn1/ah;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    iput-object v1, p0, Lorg/spongycastle/asn1/l/h;->Kz:Lorg/spongycastle/asn1/r;

    .line 82
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    .line 62
    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sequence not version 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/h;->Kz:Lorg/spongycastle/asn1/r;

    .line 68
    return-void
.end method

.method public static ab(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/h;
    .locals 2

    .prologue
    .line 44
    instance-of v0, p0, Lorg/spongycastle/asn1/l/h;

    if-eqz v0, :cond_0

    .line 46
    check-cast p0, Lorg/spongycastle/asn1/l/h;

    .line 54
    :goto_0
    return-object p0

    .line 49
    :cond_0
    if-eqz p0, :cond_1

    .line 51
    new-instance v0, Lorg/spongycastle/asn1/l/h;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/h;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 54
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 108
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 110
    new-instance v1, Lorg/spongycastle/asn1/i;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 111
    iget-object v1, p0, Lorg/spongycastle/asn1/l/h;->Kz:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 113
    new-instance v1, Lorg/spongycastle/asn1/ah;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nA()Lorg/spongycastle/asn1/m;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lorg/spongycastle/asn1/l/h;->Kz:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 98
    iget-object v0, p0, Lorg/spongycastle/asn1/l/h;->Kz:Lorg/spongycastle/asn1/r;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/x;->M(Ljava/lang/Object;)Lorg/spongycastle/asn1/x;

    move-result-object v0

    .line 100
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/m;->a(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/m;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nz()Lorg/spongycastle/asn1/q/a;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lorg/spongycastle/asn1/l/h;->Kz:Lorg/spongycastle/asn1/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    return-object v0
.end method
