.class public Lorg/spongycastle/asn1/l/i;
.super Lorg/spongycastle/asn1/k;
.source "EncryptedPrivateKeyInfo.java"


# instance fields
.field private KA:Lorg/spongycastle/asn1/m;

.field private algId:Lorg/spongycastle/asn1/q/a;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/a;[B)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/spongycastle/asn1/l/i;->algId:Lorg/spongycastle/asn1/q/a;

    .line 34
    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/i;->KA:Lorg/spongycastle/asn1/m;

    .line 35
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 23
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v0

    .line 25
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v1

    iput-object v1, p0, Lorg/spongycastle/asn1/l/i;->algId:Lorg/spongycastle/asn1/q/a;

    .line 26
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/m;->J(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/i;->KA:Lorg/spongycastle/asn1/m;

    .line 27
    return-void
.end method

.method public static ac(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/i;
    .locals 2

    .prologue
    .line 40
    instance-of v0, p0, Lorg/spongycastle/asn1/l/h;

    if-eqz v0, :cond_0

    .line 42
    check-cast p0, Lorg/spongycastle/asn1/l/i;

    .line 49
    :goto_0
    return-object p0

    .line 44
    :cond_0
    if-eqz p0, :cond_1

    .line 46
    new-instance v0, Lorg/spongycastle/asn1/l/i;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/i;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 49
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getEncryptedData()[B
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/spongycastle/asn1/l/i;->KA:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 81
    iget-object v1, p0, Lorg/spongycastle/asn1/l/i;->algId:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 82
    iget-object v1, p0, Lorg/spongycastle/asn1/l/i;->KA:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 84
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nz()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/spongycastle/asn1/l/i;->algId:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method
