.class public Lorg/spongycastle/asn1/l/e;
.super Lorg/spongycastle/asn1/k;
.source "CertificationRequestInfo.java"


# instance fields
.field Ku:Lorg/spongycastle/asn1/i;

.field Kv:Lorg/spongycastle/asn1/p/c;

.field Kw:Lorg/spongycastle/asn1/q/ah;

.field Kx:Lorg/spongycastle/asn1/t;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/aw;Lorg/spongycastle/asn1/q/ah;Lorg/spongycastle/asn1/t;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 36
    new-instance v0, Lorg/spongycastle/asn1/i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/i;-><init>(I)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Ku:Lorg/spongycastle/asn1/i;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kx:Lorg/spongycastle/asn1/t;

    .line 79
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q/aw;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kv:Lorg/spongycastle/asn1/p/c;

    .line 80
    iput-object p2, p0, Lorg/spongycastle/asn1/l/e;->Kw:Lorg/spongycastle/asn1/q/ah;

    .line 81
    iput-object p3, p0, Lorg/spongycastle/asn1/l/e;->Kx:Lorg/spongycastle/asn1/t;

    .line 83
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/l/e;->Ku:Lorg/spongycastle/asn1/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kw:Lorg/spongycastle/asn1/q/ah;

    if-nez v0, :cond_1

    .line 85
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not all mandatory fields set in CertificationRequestInfo generator."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 36
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/i;-><init>(I)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Ku:Lorg/spongycastle/asn1/i;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kx:Lorg/spongycastle/asn1/t;

    .line 92
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Ku:Lorg/spongycastle/asn1/i;

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p/c;->aw(Ljava/lang/Object;)Lorg/spongycastle/asn1/p/c;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kv:Lorg/spongycastle/asn1/p/c;

    .line 95
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ah;->aX(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ah;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kw:Lorg/spongycastle/asn1/q/ah;

    .line 101
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 103
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/bm;

    .line 104
    invoke-static {v0, v1}, Lorg/spongycastle/asn1/t;->c(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kx:Lorg/spongycastle/asn1/t;

    .line 107
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kv:Lorg/spongycastle/asn1/p/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/spongycastle/asn1/l/e;->Ku:Lorg/spongycastle/asn1/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/spongycastle/asn1/l/e;->Kw:Lorg/spongycastle/asn1/q/ah;

    if-nez v0, :cond_2

    .line 109
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not all mandatory fields set in CertificationRequestInfo generator."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_2
    return-void
.end method

.method public static Y(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/e;
    .locals 2

    .prologue
    .line 44
    instance-of v0, p0, Lorg/spongycastle/asn1/l/e;

    if-eqz v0, :cond_0

    .line 46
    check-cast p0, Lorg/spongycastle/asn1/l/e;

    .line 53
    :goto_0
    return-object p0

    .line 48
    :cond_0
    if-eqz p0, :cond_1

    .line 50
    new-instance v0, Lorg/spongycastle/asn1/l/e;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/e;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 53
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 135
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 137
    iget-object v1, p0, Lorg/spongycastle/asn1/l/e;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 138
    iget-object v1, p0, Lorg/spongycastle/asn1/l/e;->Kv:Lorg/spongycastle/asn1/p/c;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 139
    iget-object v1, p0, Lorg/spongycastle/asn1/l/e;->Kw:Lorg/spongycastle/asn1/q/ah;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 141
    iget-object v1, p0, Lorg/spongycastle/asn1/l/e;->Kx:Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_0

    .line 143
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/l/e;->Kx:Lorg/spongycastle/asn1/t;

    invoke-direct {v1, v3, v3, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 146
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
