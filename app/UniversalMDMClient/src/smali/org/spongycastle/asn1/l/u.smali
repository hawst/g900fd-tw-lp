.class public Lorg/spongycastle/asn1/l/u;
.super Lorg/spongycastle/asn1/k;
.source "RSAESOAEPparams.java"


# static fields
.field public static final Nu:Lorg/spongycastle/asn1/q/a;

.field public static final Nv:Lorg/spongycastle/asn1/q/a;

.field public static final Nw:Lorg/spongycastle/asn1/q/a;


# instance fields
.field private Nr:Lorg/spongycastle/asn1/q/a;

.field private Ns:Lorg/spongycastle/asn1/q/a;

.field private Nt:Lorg/spongycastle/asn1/q/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/k/b;->Kh:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/ba;

    invoke-direct {v2}, Lorg/spongycastle/asn1/ba;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    sput-object v0, Lorg/spongycastle/asn1/l/u;->Nu:Lorg/spongycastle/asn1/q/a;

    .line 23
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KQ:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/l/u;->Nu:Lorg/spongycastle/asn1/q/a;

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    sput-object v0, Lorg/spongycastle/asn1/l/u;->Nv:Lorg/spongycastle/asn1/q/a;

    .line 24
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KR:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/bd;

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    sput-object v0, Lorg/spongycastle/asn1/l/u;->Nw:Lorg/spongycastle/asn1/q/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 46
    sget-object v0, Lorg/spongycastle/asn1/l/u;->Nu:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 47
    sget-object v0, Lorg/spongycastle/asn1/l/u;->Nv:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Ns:Lorg/spongycastle/asn1/q/a;

    .line 48
    sget-object v0, Lorg/spongycastle/asn1/l/u;->Nw:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nt:Lorg/spongycastle/asn1/q/a;

    .line 49
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/q/a;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 56
    iput-object p1, p0, Lorg/spongycastle/asn1/l/u;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 57
    iput-object p2, p0, Lorg/spongycastle/asn1/l/u;->Ns:Lorg/spongycastle/asn1/q/a;

    .line 58
    iput-object p3, p0, Lorg/spongycastle/asn1/l/u;->Nt:Lorg/spongycastle/asn1/q/a;

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 63
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 64
    sget-object v0, Lorg/spongycastle/asn1/l/u;->Nu:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 65
    sget-object v0, Lorg/spongycastle/asn1/l/u;->Nv:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Ns:Lorg/spongycastle/asn1/q/a;

    .line 66
    sget-object v0, Lorg/spongycastle/asn1/l/u;->Nw:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nt:Lorg/spongycastle/asn1/q/a;

    .line 68
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 70
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    .line 72
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown tag"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :pswitch_0
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/q/a;->k(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 68
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 78
    :pswitch_1
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/q/a;->k(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Ns:Lorg/spongycastle/asn1/q/a;

    goto :goto_1

    .line 81
    :pswitch_2
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/q/a;->k(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nt:Lorg/spongycastle/asn1/q/a;

    goto :goto_1

    .line 87
    :cond_0
    return-void

    .line 72
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static am(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/u;
    .locals 2

    .prologue
    .line 29
    instance-of v0, p0, Lorg/spongycastle/asn1/l/u;

    if-eqz v0, :cond_0

    .line 31
    check-cast p0, Lorg/spongycastle/asn1/l/u;

    .line 38
    :goto_0
    return-object p0

    .line 33
    :cond_0
    if-eqz p0, :cond_1

    .line 35
    new-instance v0, Lorg/spongycastle/asn1/l/u;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/u;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 38
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 132
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 134
    iget-object v1, p0, Lorg/spongycastle/asn1/l/u;->Nr:Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/l/u;->Nu:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/spongycastle/asn1/l/u;->Nr:Lorg/spongycastle/asn1/q/a;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 139
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/l/u;->Ns:Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/l/u;->Nv:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 141
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/l/u;->Ns:Lorg/spongycastle/asn1/q/a;

    invoke-direct {v1, v4, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 144
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/l/u;->Nt:Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/l/u;->Nw:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 146
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/spongycastle/asn1/l/u;->Nt:Lorg/spongycastle/asn1/q/a;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 149
    :cond_2
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nM()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nr:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public nN()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/spongycastle/asn1/l/u;->Ns:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public nO()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/spongycastle/asn1/l/u;->Nt:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method
