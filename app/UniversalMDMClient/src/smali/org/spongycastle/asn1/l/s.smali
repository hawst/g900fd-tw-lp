.class public Lorg/spongycastle/asn1/l/s;
.super Lorg/spongycastle/asn1/k;
.source "PrivateKeyInfo.java"


# instance fields
.field private Kx:Lorg/spongycastle/asn1/t;

.field private Nq:Lorg/spongycastle/asn1/m;

.field private algId:Lorg/spongycastle/asn1/q/a;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/spongycastle/asn1/l/s;-><init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;Lorg/spongycastle/asn1/t;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/d;Lorg/spongycastle/asn1/t;)V
    .locals 3

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 65
    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-interface {p2}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v1

    const-string v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q;->getEncoded(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/s;->Nq:Lorg/spongycastle/asn1/m;

    .line 66
    iput-object p1, p0, Lorg/spongycastle/asn1/l/s;->algId:Lorg/spongycastle/asn1/q/a;

    .line 67
    iput-object p3, p0, Lorg/spongycastle/asn1/l/s;->Kx:Lorg/spongycastle/asn1/t;

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 73
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 75
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "wrong version for private key info"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/s;->algId:Lorg/spongycastle/asn1/q/a;

    .line 82
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/m;->J(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/s;->Nq:Lorg/spongycastle/asn1/m;

    .line 84
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/t;->c(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/s;->Kx:Lorg/spongycastle/asn1/t;

    .line 88
    :cond_1
    return-void
.end method

.method public static ak(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/s;
    .locals 2

    .prologue
    .line 39
    instance-of v0, p0, Lorg/spongycastle/asn1/l/s;

    if-eqz v0, :cond_0

    .line 41
    check-cast p0, Lorg/spongycastle/asn1/l/s;

    .line 48
    :goto_0
    return-object p0

    .line 43
    :cond_0
    if-eqz p0, :cond_1

    .line 45
    new-instance v0, Lorg/spongycastle/asn1/l/s;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/s;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 48
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 147
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 149
    new-instance v1, Lorg/spongycastle/asn1/i;

    invoke-direct {v1, v3}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 150
    iget-object v1, p0, Lorg/spongycastle/asn1/l/s;->algId:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 151
    iget-object v1, p0, Lorg/spongycastle/asn1/l/s;->Nq:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 153
    iget-object v1, p0, Lorg/spongycastle/asn1/l/s;->Kx:Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_0

    .line 155
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/l/s;->Kx:Lorg/spongycastle/asn1/t;

    invoke-direct {v1, v3, v3, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 158
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nI()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/spongycastle/asn1/l/s;->algId:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public nJ()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/spongycastle/asn1/l/s;->algId:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public nK()Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/spongycastle/asn1/l/s;->Nq:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method
