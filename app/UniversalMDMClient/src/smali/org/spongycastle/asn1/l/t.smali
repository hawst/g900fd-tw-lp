.class public Lorg/spongycastle/asn1/l/t;
.super Lorg/spongycastle/asn1/k;
.source "RC2CBCParameter.java"


# instance fields
.field IV:Lorg/spongycastle/asn1/m;

.field Ku:Lorg/spongycastle/asn1/i;


# direct methods
.method public constructor <init>(I[B)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 46
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/i;-><init>(I)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    .line 47
    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->IV:Lorg/spongycastle/asn1/m;

    .line 48
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 53
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    .line 56
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->IV:Lorg/spongycastle/asn1/m;

    .line 63
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    .line 61
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->IV:Lorg/spongycastle/asn1/m;

    goto :goto_0
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    iput-object v0, p0, Lorg/spongycastle/asn1/l/t;->IV:Lorg/spongycastle/asn1/m;

    .line 40
    return-void
.end method

.method public static al(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/t;
    .locals 2

    .prologue
    .line 23
    instance-of v0, p0, Lorg/spongycastle/asn1/l/t;

    if-eqz v0, :cond_0

    .line 25
    check-cast p0, Lorg/spongycastle/asn1/l/t;

    .line 32
    :goto_0
    return-object p0

    .line 27
    :cond_0
    if-eqz p0, :cond_1

    .line 29
    new-instance v0, Lorg/spongycastle/asn1/l/t;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/t;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 32
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getIV()[B
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/spongycastle/asn1/l/t;->IV:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 84
    iget-object v1, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 89
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/l/t;->IV:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 91
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nL()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    if-nez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/l/t;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0
.end method
