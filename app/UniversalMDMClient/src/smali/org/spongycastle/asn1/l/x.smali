.class public Lorg/spongycastle/asn1/l/x;
.super Lorg/spongycastle/asn1/k;
.source "RSASSAPSSparams.java"


# static fields
.field public static final NH:Lorg/spongycastle/asn1/i;

.field public static final NI:Lorg/spongycastle/asn1/i;

.field public static final Nu:Lorg/spongycastle/asn1/q/a;

.field public static final Nv:Lorg/spongycastle/asn1/q/a;


# instance fields
.field private NF:Lorg/spongycastle/asn1/i;

.field private NG:Lorg/spongycastle/asn1/i;

.field private Nr:Lorg/spongycastle/asn1/q/a;

.field private Ns:Lorg/spongycastle/asn1/q/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/k/b;->Kh:Lorg/spongycastle/asn1/l;

    new-instance v2, Lorg/spongycastle/asn1/ba;

    invoke-direct {v2}, Lorg/spongycastle/asn1/ba;-><init>()V

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    sput-object v0, Lorg/spongycastle/asn1/l/x;->Nu:Lorg/spongycastle/asn1/q/a;

    .line 26
    new-instance v0, Lorg/spongycastle/asn1/q/a;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->KQ:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/l/x;->Nu:Lorg/spongycastle/asn1/q/a;

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V

    sput-object v0, Lorg/spongycastle/asn1/l/x;->Nv:Lorg/spongycastle/asn1/q/a;

    .line 27
    new-instance v0, Lorg/spongycastle/asn1/i;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/i;-><init>(I)V

    sput-object v0, Lorg/spongycastle/asn1/l/x;->NH:Lorg/spongycastle/asn1/i;

    .line 28
    new-instance v0, Lorg/spongycastle/asn1/i;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/i;-><init>(I)V

    sput-object v0, Lorg/spongycastle/asn1/l/x;->NI:Lorg/spongycastle/asn1/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 50
    sget-object v0, Lorg/spongycastle/asn1/l/x;->Nu:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 51
    sget-object v0, Lorg/spongycastle/asn1/l/x;->Nv:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->Ns:Lorg/spongycastle/asn1/q/a;

    .line 52
    sget-object v0, Lorg/spongycastle/asn1/l/x;->NH:Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->NF:Lorg/spongycastle/asn1/i;

    .line 53
    sget-object v0, Lorg/spongycastle/asn1/l/x;->NI:Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->NG:Lorg/spongycastle/asn1/i;

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/q/a;Lorg/spongycastle/asn1/i;Lorg/spongycastle/asn1/i;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 62
    iput-object p1, p0, Lorg/spongycastle/asn1/l/x;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 63
    iput-object p2, p0, Lorg/spongycastle/asn1/l/x;->Ns:Lorg/spongycastle/asn1/q/a;

    .line 64
    iput-object p3, p0, Lorg/spongycastle/asn1/l/x;->NF:Lorg/spongycastle/asn1/i;

    .line 65
    iput-object p4, p0, Lorg/spongycastle/asn1/l/x;->NG:Lorg/spongycastle/asn1/i;

    .line 66
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 70
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 71
    sget-object v0, Lorg/spongycastle/asn1/l/x;->Nu:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 72
    sget-object v0, Lorg/spongycastle/asn1/l/x;->Nv:Lorg/spongycastle/asn1/q/a;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->Ns:Lorg/spongycastle/asn1/q/a;

    .line 73
    sget-object v0, Lorg/spongycastle/asn1/l/x;->NH:Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->NF:Lorg/spongycastle/asn1/i;

    .line 74
    sget-object v0, Lorg/spongycastle/asn1/l/x;->NI:Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->NG:Lorg/spongycastle/asn1/i;

    .line 76
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 78
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    .line 80
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown tag"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_0
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/q/a;->k(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->Nr:Lorg/spongycastle/asn1/q/a;

    .line 76
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 86
    :pswitch_1
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/q/a;->k(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->Ns:Lorg/spongycastle/asn1/q/a;

    goto :goto_1

    .line 89
    :pswitch_2
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/i;->h(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->NF:Lorg/spongycastle/asn1/i;

    goto :goto_1

    .line 92
    :pswitch_3
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/i;->h(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/i;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/x;->NG:Lorg/spongycastle/asn1/i;

    goto :goto_1

    .line 98
    :cond_0
    return-void

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static ap(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/x;
    .locals 2

    .prologue
    .line 33
    instance-of v0, p0, Lorg/spongycastle/asn1/l/x;

    if-eqz v0, :cond_0

    .line 35
    check-cast p0, Lorg/spongycastle/asn1/l/x;

    .line 42
    :goto_0
    return-object p0

    .line 37
    :cond_0
    if-eqz p0, :cond_1

    .line 39
    new-instance v0, Lorg/spongycastle/asn1/l/x;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/x;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 42
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 148
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 150
    iget-object v1, p0, Lorg/spongycastle/asn1/l/x;->Nr:Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/l/x;->Nu:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 152
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/spongycastle/asn1/l/x;->Nr:Lorg/spongycastle/asn1/q/a;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 155
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/l/x;->Ns:Lorg/spongycastle/asn1/q/a;

    sget-object v2, Lorg/spongycastle/asn1/l/x;->Nv:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 157
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/l/x;->Ns:Lorg/spongycastle/asn1/q/a;

    invoke-direct {v1, v4, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 160
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/l/x;->NF:Lorg/spongycastle/asn1/i;

    sget-object v2, Lorg/spongycastle/asn1/l/x;->NH:Lorg/spongycastle/asn1/i;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 162
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/spongycastle/asn1/l/x;->NF:Lorg/spongycastle/asn1/i;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 165
    :cond_2
    iget-object v1, p0, Lorg/spongycastle/asn1/l/x;->NG:Lorg/spongycastle/asn1/i;

    sget-object v2, Lorg/spongycastle/asn1/l/x;->NI:Lorg/spongycastle/asn1/i;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 167
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x3

    iget-object v3, p0, Lorg/spongycastle/asn1/l/x;->NG:Lorg/spongycastle/asn1/i;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 170
    :cond_3
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nM()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/spongycastle/asn1/l/x;->Nr:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public nN()Lorg/spongycastle/asn1/q/a;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/spongycastle/asn1/l/x;->Ns:Lorg/spongycastle/asn1/q/a;

    return-object v0
.end method

.method public nU()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/spongycastle/asn1/l/x;->NF:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public nV()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/spongycastle/asn1/l/x;->NG:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
