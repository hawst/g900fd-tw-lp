.class public Lorg/spongycastle/asn1/l/y;
.super Lorg/spongycastle/asn1/k;
.source "SafeBag.java"


# instance fields
.field private NJ:Lorg/spongycastle/asn1/l;

.field private NK:Lorg/spongycastle/asn1/d;

.field private NL:Lorg/spongycastle/asn1/t;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;Lorg/spongycastle/asn1/t;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/spongycastle/asn1/l/y;->NJ:Lorg/spongycastle/asn1/l;

    .line 36
    iput-object p2, p0, Lorg/spongycastle/asn1/l/y;->NK:Lorg/spongycastle/asn1/d;

    .line 37
    iput-object p3, p0, Lorg/spongycastle/asn1/l/y;->NL:Lorg/spongycastle/asn1/t;

    .line 38
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 59
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/y;->NJ:Lorg/spongycastle/asn1/l;

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/y;->NK:Lorg/spongycastle/asn1/d;

    .line 61
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 63
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/t;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/y;->NL:Lorg/spongycastle/asn1/t;

    .line 65
    :cond_0
    return-void
.end method

.method public static aq(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/y;
    .locals 2

    .prologue
    .line 43
    instance-of v0, p0, Lorg/spongycastle/asn1/l/y;

    if-eqz v0, :cond_0

    .line 45
    check-cast p0, Lorg/spongycastle/asn1/l/y;

    .line 53
    :goto_0
    return-object p0

    .line 48
    :cond_0
    if-eqz p0, :cond_1

    .line 50
    new-instance v0, Lorg/spongycastle/asn1/l/y;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/y;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 53
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    .line 84
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 86
    iget-object v1, p0, Lorg/spongycastle/asn1/l/y;->NJ:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 87
    new-instance v1, Lorg/spongycastle/asn1/bu;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/spongycastle/asn1/l/y;->NK:Lorg/spongycastle/asn1/d;

    invoke-direct {v1, v2, v3, v4}, Lorg/spongycastle/asn1/bu;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 89
    iget-object v1, p0, Lorg/spongycastle/asn1/l/y;->NL:Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lorg/spongycastle/asn1/l/y;->NL:Lorg/spongycastle/asn1/t;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 94
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bs;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bs;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nW()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/spongycastle/asn1/l/y;->NJ:Lorg/spongycastle/asn1/l;

    return-object v0
.end method

.method public nX()Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/asn1/l/y;->NK:Lorg/spongycastle/asn1/d;

    return-object v0
.end method

.method public nY()Lorg/spongycastle/asn1/t;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/spongycastle/asn1/l/y;->NL:Lorg/spongycastle/asn1/t;

    return-object v0
.end method
