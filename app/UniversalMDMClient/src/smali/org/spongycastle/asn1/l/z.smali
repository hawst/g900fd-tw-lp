.class public Lorg/spongycastle/asn1/l/z;
.super Lorg/spongycastle/asn1/k;
.source "SignedData.java"

# interfaces
.implements Lorg/spongycastle/asn1/l/q;


# instance fields
.field private Ku:Lorg/spongycastle/asn1/i;

.field private NM:Lorg/spongycastle/asn1/t;

.field private NN:Lorg/spongycastle/asn1/t;

.field private NO:Lorg/spongycastle/asn1/t;

.field private NP:Lorg/spongycastle/asn1/t;

.field private No:Lorg/spongycastle/asn1/l/f;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/i;Lorg/spongycastle/asn1/t;Lorg/spongycastle/asn1/l/f;Lorg/spongycastle/asn1/t;Lorg/spongycastle/asn1/t;Lorg/spongycastle/asn1/t;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/spongycastle/asn1/l/z;->Ku:Lorg/spongycastle/asn1/i;

    .line 52
    iput-object p2, p0, Lorg/spongycastle/asn1/l/z;->NM:Lorg/spongycastle/asn1/t;

    .line 53
    iput-object p3, p0, Lorg/spongycastle/asn1/l/z;->No:Lorg/spongycastle/asn1/l/f;

    .line 54
    iput-object p4, p0, Lorg/spongycastle/asn1/l/z;->NN:Lorg/spongycastle/asn1/t;

    .line 55
    iput-object p5, p0, Lorg/spongycastle/asn1/l/z;->NO:Lorg/spongycastle/asn1/t;

    .line 56
    iput-object p6, p0, Lorg/spongycastle/asn1/l/z;->NP:Lorg/spongycastle/asn1/t;

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 61
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 62
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 64
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/z;->Ku:Lorg/spongycastle/asn1/i;

    .line 65
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/t;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/z;->NM:Lorg/spongycastle/asn1/t;

    .line 66
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/f;->Z(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/f;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/z;->No:Lorg/spongycastle/asn1/l/f;

    .line 68
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/q;

    .line 76
    instance-of v2, v0, Lorg/spongycastle/asn1/bm;

    if-eqz v2, :cond_0

    .line 78
    check-cast v0, Lorg/spongycastle/asn1/bm;

    .line 80
    invoke-virtual {v0}, Lorg/spongycastle/asn1/bm;->mT()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown tag value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bm;->mT()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :pswitch_0
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/t;->c(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/z;->NN:Lorg/spongycastle/asn1/t;

    goto :goto_0

    .line 86
    :pswitch_1
    invoke-static {v0, v3}, Lorg/spongycastle/asn1/t;->c(Lorg/spongycastle/asn1/x;Z)Lorg/spongycastle/asn1/t;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/z;->NO:Lorg/spongycastle/asn1/t;

    goto :goto_0

    .line 94
    :cond_0
    check-cast v0, Lorg/spongycastle/asn1/t;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/z;->NP:Lorg/spongycastle/asn1/t;

    goto :goto_0

    .line 97
    :cond_1
    return-void

    .line 80
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static ar(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/z;
    .locals 2

    .prologue
    .line 31
    instance-of v0, p0, Lorg/spongycastle/asn1/l/z;

    if-eqz v0, :cond_0

    .line 33
    check-cast p0, Lorg/spongycastle/asn1/l/z;

    .line 40
    :goto_0
    return-object p0

    .line 35
    :cond_0
    if-eqz p0, :cond_1

    .line 37
    new-instance v0, Lorg/spongycastle/asn1/l/z;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/z;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 40
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 146
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 148
    iget-object v1, p0, Lorg/spongycastle/asn1/l/z;->Ku:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 149
    iget-object v1, p0, Lorg/spongycastle/asn1/l/z;->NM:Lorg/spongycastle/asn1/t;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 150
    iget-object v1, p0, Lorg/spongycastle/asn1/l/z;->No:Lorg/spongycastle/asn1/l/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 152
    iget-object v1, p0, Lorg/spongycastle/asn1/l/z;->NN:Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_0

    .line 154
    new-instance v1, Lorg/spongycastle/asn1/bm;

    iget-object v2, p0, Lorg/spongycastle/asn1/l/z;->NN:Lorg/spongycastle/asn1/t;

    invoke-direct {v1, v4, v4, v2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 157
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/l/z;->NO:Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_1

    .line 159
    new-instance v1, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/spongycastle/asn1/l/z;->NO:Lorg/spongycastle/asn1/t;

    invoke-direct {v1, v4, v2, v3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 162
    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/l/z;->NP:Lorg/spongycastle/asn1/t;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 164
    new-instance v1, Lorg/spongycastle/asn1/ah;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nZ()Lorg/spongycastle/asn1/t;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/spongycastle/asn1/l/z;->NN:Lorg/spongycastle/asn1/t;

    return-object v0
.end method

.method public oa()Lorg/spongycastle/asn1/t;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/spongycastle/asn1/l/z;->NO:Lorg/spongycastle/asn1/t;

    return-object v0
.end method
