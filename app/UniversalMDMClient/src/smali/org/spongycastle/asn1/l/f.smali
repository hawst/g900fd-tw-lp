.class public Lorg/spongycastle/asn1/l/f;
.super Lorg/spongycastle/asn1/k;
.source "ContentInfo.java"

# interfaces
.implements Lorg/spongycastle/asn1/l/q;


# instance fields
.field private Hl:Lorg/spongycastle/asn1/l;

.field private Hm:Lorg/spongycastle/asn1/d;

.field private Ko:Z


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/l/f;->Ko:Z

    .line 59
    iput-object p1, p0, Lorg/spongycastle/asn1/l/f;->Hl:Lorg/spongycastle/asn1/l;

    .line 60
    iput-object p2, p0, Lorg/spongycastle/asn1/l/f;->Hm:Lorg/spongycastle/asn1/d;

    .line 61
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/l/f;->Ko:Z

    .line 43
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 45
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/f;->Hl:Lorg/spongycastle/asn1/l;

    .line 47
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/f;->Hm:Lorg/spongycastle/asn1/d;

    .line 52
    :cond_0
    instance-of v0, p1, Lorg/spongycastle/asn1/ah;

    iput-boolean v0, p0, Lorg/spongycastle/asn1/l/f;->Ko:Z

    .line 53
    return-void
.end method

.method public static Z(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/f;
    .locals 2

    .prologue
    .line 27
    instance-of v0, p0, Lorg/spongycastle/asn1/l/f;

    if-eqz v0, :cond_0

    .line 29
    check-cast p0, Lorg/spongycastle/asn1/l/f;

    .line 37
    :goto_0
    return-object p0

    .line 32
    :cond_0
    if-eqz p0, :cond_1

    .line 34
    new-instance v0, Lorg/spongycastle/asn1/l/f;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l/f;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 37
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 5

    .prologue
    .line 84
    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 86
    iget-object v0, p0, Lorg/spongycastle/asn1/l/f;->Hl:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 88
    iget-object v0, p0, Lorg/spongycastle/asn1/l/f;->Hm:Lorg/spongycastle/asn1/d;

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Lorg/spongycastle/asn1/al;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/spongycastle/asn1/l/f;->Hm:Lorg/spongycastle/asn1/d;

    invoke-direct {v0, v2, v3, v4}, Lorg/spongycastle/asn1/al;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 93
    :cond_0
    iget-boolean v0, p0, Lorg/spongycastle/asn1/l/f;->Ko:Z

    if-eqz v0, :cond_1

    .line 95
    new-instance v0, Lorg/spongycastle/asn1/ah;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    .line 99
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/bs;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bs;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method

.method public nw()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/spongycastle/asn1/l/f;->Hl:Lorg/spongycastle/asn1/l;

    return-object v0
.end method

.method public nx()Lorg/spongycastle/asn1/d;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/spongycastle/asn1/l/f;->Hm:Lorg/spongycastle/asn1/d;

    return-object v0
.end method
