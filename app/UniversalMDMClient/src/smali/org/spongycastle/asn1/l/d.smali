.class public Lorg/spongycastle/asn1/l/d;
.super Lorg/spongycastle/asn1/k;
.source "CertificationRequest.java"


# instance fields
.field protected Kr:Lorg/spongycastle/asn1/l/e;

.field protected Ks:Lorg/spongycastle/asn1/q/a;

.field protected Kt:Lorg/spongycastle/asn1/aq;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 24
    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Kr:Lorg/spongycastle/asn1/l/e;

    .line 25
    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Ks:Lorg/spongycastle/asn1/q/a;

    .line 26
    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Kt:Lorg/spongycastle/asn1/aq;

    .line 45
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 24
    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Kr:Lorg/spongycastle/asn1/l/e;

    .line 25
    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Ks:Lorg/spongycastle/asn1/q/a;

    .line 26
    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Kt:Lorg/spongycastle/asn1/aq;

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l/e;->Y(Ljava/lang/Object;)Lorg/spongycastle/asn1/l/e;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Kr:Lorg/spongycastle/asn1/l/e;

    .line 61
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/a;->ad(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/a;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Ks:Lorg/spongycastle/asn1/q/a;

    .line 62
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/aq;

    iput-object v0, p0, Lorg/spongycastle/asn1/l/d;->Kt:Lorg/spongycastle/asn1/aq;

    .line 63
    return-void
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 85
    iget-object v1, p0, Lorg/spongycastle/asn1/l/d;->Kr:Lorg/spongycastle/asn1/l/e;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 86
    iget-object v1, p0, Lorg/spongycastle/asn1/l/d;->Ks:Lorg/spongycastle/asn1/q/a;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 87
    iget-object v1, p0, Lorg/spongycastle/asn1/l/d;->Kt:Lorg/spongycastle/asn1/aq;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 89
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
