.class Lorg/spongycastle/asn1/ad;
.super Ljava/lang/Object;
.source "BERFactory.java"


# static fields
.field static final FZ:Lorg/spongycastle/asn1/ah;

.field static final Ga:Lorg/spongycastle/asn1/aj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lorg/spongycastle/asn1/ah;

    invoke-direct {v0}, Lorg/spongycastle/asn1/ah;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/ad;->FZ:Lorg/spongycastle/asn1/ah;

    .line 6
    new-instance v0, Lorg/spongycastle/asn1/aj;

    invoke-direct {v0}, Lorg/spongycastle/asn1/aj;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/ad;->Ga:Lorg/spongycastle/asn1/aj;

    return-void
.end method

.method static a(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/ah;
    .locals 2

    .prologue
    .line 10
    invoke-virtual {p0}, Lorg/spongycastle/asn1/e;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/ad;->FZ:Lorg/spongycastle/asn1/ah;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/ah;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method
