.class public Lorg/spongycastle/asn1/h/a;
.super Lorg/spongycastle/asn1/k;
.source "CAST5CBCParameters.java"


# instance fields
.field IU:Lorg/spongycastle/asn1/i;

.field IV:Lorg/spongycastle/asn1/m;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    iput-object v0, p0, Lorg/spongycastle/asn1/h/a;->IV:Lorg/spongycastle/asn1/m;

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/h/a;->IU:Lorg/spongycastle/asn1/i;

    .line 46
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 37
    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    iput-object v0, p0, Lorg/spongycastle/asn1/h/a;->IV:Lorg/spongycastle/asn1/m;

    .line 38
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    iput-object v0, p0, Lorg/spongycastle/asn1/h/a;->IU:Lorg/spongycastle/asn1/i;

    .line 39
    return-void
.end method

.method public static V(Ljava/lang/Object;)Lorg/spongycastle/asn1/h/a;
    .locals 2

    .prologue
    .line 21
    instance-of v0, p0, Lorg/spongycastle/asn1/h/a;

    if-eqz v0, :cond_0

    .line 23
    check-cast p0, Lorg/spongycastle/asn1/h/a;

    .line 30
    :goto_0
    return-object p0

    .line 25
    :cond_0
    if-eqz p0, :cond_1

    .line 27
    new-instance v0, Lorg/spongycastle/asn1/h/a;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/h/a;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 30
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getIV()[B
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/spongycastle/asn1/h/a;->IV:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    return-object v0
.end method

.method public getKeyLength()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/spongycastle/asn1/h/a;->IU:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nb()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    return v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 73
    iget-object v1, p0, Lorg/spongycastle/asn1/h/a;->IV:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 74
    iget-object v1, p0, Lorg/spongycastle/asn1/h/a;->IU:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 76
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
