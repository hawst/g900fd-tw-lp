.class public interface abstract Lorg/spongycastle/asn1/h/c;
.super Ljava/lang/Object;
.source "MiscObjectIdentifiers.java"


# static fields
.field public static final IW:Lorg/spongycastle/asn1/l;

.field public static final IX:Lorg/spongycastle/asn1/l;

.field public static final IY:Lorg/spongycastle/asn1/l;

.field public static final IZ:Lorg/spongycastle/asn1/l;

.field public static final Ja:Lorg/spongycastle/asn1/l;

.field public static final Jb:Lorg/spongycastle/asn1/l;

.field public static final Jc:Lorg/spongycastle/asn1/l;

.field public static final Jd:Lorg/spongycastle/asn1/l;

.field public static final Je:Lorg/spongycastle/asn1/l;

.field public static final Jf:Lorg/spongycastle/asn1/l;

.field public static final Jg:Lorg/spongycastle/asn1/l;

.field public static final Jh:Lorg/spongycastle/asn1/l;

.field public static final Ji:Lorg/spongycastle/asn1/l;

.field public static final Jj:Lorg/spongycastle/asn1/l;

.field public static final Jk:Lorg/spongycastle/asn1/l;

.field public static final Jl:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.16.840.1.113730.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    .line 12
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->IX:Lorg/spongycastle/asn1/l;

    .line 13
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->IY:Lorg/spongycastle/asn1/l;

    .line 14
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->IZ:Lorg/spongycastle/asn1/l;

    .line 15
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Ja:Lorg/spongycastle/asn1/l;

    .line 16
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jb:Lorg/spongycastle/asn1/l;

    .line 17
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jc:Lorg/spongycastle/asn1/l;

    .line 18
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "12"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jd:Lorg/spongycastle/asn1/l;

    .line 19
    sget-object v0, Lorg/spongycastle/asn1/h/c;->IW:Lorg/spongycastle/asn1/l;

    const-string v1, "13"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Je:Lorg/spongycastle/asn1/l;

    .line 25
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.16.840.1.113733.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jf:Lorg/spongycastle/asn1/l;

    .line 30
    sget-object v0, Lorg/spongycastle/asn1/h/c;->Jf:Lorg/spongycastle/asn1/l;

    const-string v1, "6.3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jg:Lorg/spongycastle/asn1/l;

    .line 32
    sget-object v0, Lorg/spongycastle/asn1/h/c;->Jf:Lorg/spongycastle/asn1/l;

    const-string v1, "6.15"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jh:Lorg/spongycastle/asn1/l;

    .line 38
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.16.840.1.113719"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Ji:Lorg/spongycastle/asn1/l;

    .line 39
    sget-object v0, Lorg/spongycastle/asn1/h/c;->Ji:Lorg/spongycastle/asn1/l;

    const-string v1, "1.9.4.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jj:Lorg/spongycastle/asn1/l;

    .line 45
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.840.113533.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jk:Lorg/spongycastle/asn1/l;

    .line 46
    sget-object v0, Lorg/spongycastle/asn1/h/c;->Jk:Lorg/spongycastle/asn1/l;

    const-string v1, "65.0"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/h/c;->Jl:Lorg/spongycastle/asn1/l;

    return-void
.end method
