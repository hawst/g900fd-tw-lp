.class public Lorg/spongycastle/asn1/h/b;
.super Lorg/spongycastle/asn1/k;
.source "IDEACBCPar.java"


# instance fields
.field IV:Lorg/spongycastle/asn1/m;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 40
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 42
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    iput-object v0, p0, Lorg/spongycastle/asn1/h/b;->IV:Lorg/spongycastle/asn1/m;

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/h/b;->IV:Lorg/spongycastle/asn1/m;

    goto :goto_0
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 34
    new-instance v0, Lorg/spongycastle/asn1/bd;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    iput-object v0, p0, Lorg/spongycastle/asn1/h/b;->IV:Lorg/spongycastle/asn1/m;

    .line 35
    return-void
.end method


# virtual methods
.method public getIV()[B
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/spongycastle/asn1/h/b;->IV:Lorg/spongycastle/asn1/m;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lorg/spongycastle/asn1/h/b;->IV:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 74
    iget-object v1, p0, Lorg/spongycastle/asn1/h/b;->IV:Lorg/spongycastle/asn1/m;

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lorg/spongycastle/asn1/h/b;->IV:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 79
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
