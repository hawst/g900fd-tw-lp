.class Lorg/spongycastle/asn1/av;
.super Ljava/lang/Object;
.source "DERFactory.java"


# static fields
.field static final GA:Lorg/spongycastle/asn1/t;

.field static final Gz:Lorg/spongycastle/asn1/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0}, Lorg/spongycastle/asn1/bh;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/av;->Gz:Lorg/spongycastle/asn1/r;

    .line 6
    new-instance v0, Lorg/spongycastle/asn1/bj;

    invoke-direct {v0}, Lorg/spongycastle/asn1/bj;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/av;->GA:Lorg/spongycastle/asn1/t;

    return-void
.end method

.method static b(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/r;
    .locals 2

    .prologue
    .line 10
    invoke-virtual {p0}, Lorg/spongycastle/asn1/e;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/av;->Gz:Lorg/spongycastle/asn1/r;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/bs;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/bs;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method

.method static c(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/t;
    .locals 2

    .prologue
    .line 15
    invoke-virtual {p0}, Lorg/spongycastle/asn1/e;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/av;->GA:Lorg/spongycastle/asn1/t;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/bt;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/bt;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method
