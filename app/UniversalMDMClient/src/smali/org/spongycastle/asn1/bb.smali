.class public Lorg/spongycastle/asn1/bb;
.super Lorg/spongycastle/asn1/q;
.source "DERNumericString.java"

# interfaces
.implements Lorg/spongycastle/asn1/w;


# instance fields
.field private FL:[B


# direct methods
.method constructor <init>([B)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    .line 64
    iput-object p1, p0, Lorg/spongycastle/asn1/bb;->FL:[B

    .line 65
    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2

    .prologue
    .line 125
    const/16 v0, 0x12

    iget-object v1, p0, Lorg/spongycastle/asn1/bb;->FL:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->b(I[B)V

    .line 126
    return-void
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 2

    .prologue
    .line 136
    instance-of v0, p1, Lorg/spongycastle/asn1/bb;

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 143
    :goto_0
    return v0

    .line 141
    :cond_0
    check-cast p1, Lorg/spongycastle/asn1/bb;

    .line 143
    iget-object v0, p0, Lorg/spongycastle/asn1/bb;->FL:[B

    iget-object v1, p1, Lorg/spongycastle/asn1/bb;->FL:[B

    invoke-static {v0, v1}, Lorg/spongycastle/util/a;->h([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/spongycastle/asn1/bb;->FL:[B

    invoke-static {v0}, Lorg/spongycastle/util/g;->V([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lorg/spongycastle/asn1/bb;->FL:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->hashCode([B)I

    move-result v0

    return v0
.end method

.method mN()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method mO()I
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lorg/spongycastle/asn1/bb;->FL:[B

    array-length v0, v0

    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bL(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/bb;->FL:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bb;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
