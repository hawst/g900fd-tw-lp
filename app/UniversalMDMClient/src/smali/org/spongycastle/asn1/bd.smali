.class public Lorg/spongycastle/asn1/bd;
.super Lorg/spongycastle/asn1/m;
.source "DEROctetString.java"


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/d;)V
    .locals 2

    .prologue
    .line 21
    invoke-interface {p1}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    const-string v1, "DER"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/q;->getEncoded(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/m;-><init>([B)V

    .line 22
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lorg/spongycastle/asn1/m;-><init>([B)V

    .line 15
    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2

    .prologue
    .line 38
    const/4 v0, 0x4

    iget-object v1, p0, Lorg/spongycastle/asn1/bd;->FL:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->b(I[B)V

    .line 39
    return-void
.end method

.method mN()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method mO()I
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lorg/spongycastle/asn1/bd;->FL:[B

    array-length v0, v0

    invoke-static {v0}, Lorg/spongycastle/asn1/cc;->bL(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/bd;->FL:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method
