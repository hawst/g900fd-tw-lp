.class public interface abstract Lorg/spongycastle/asn1/i/b;
.super Ljava/lang/Object;
.source "NISTObjectIdentifiers.java"


# static fields
.field public static final JA:Lorg/spongycastle/asn1/l;

.field public static final JB:Lorg/spongycastle/asn1/l;

.field public static final JC:Lorg/spongycastle/asn1/l;

.field public static final JD:Lorg/spongycastle/asn1/l;

.field public static final JE:Lorg/spongycastle/asn1/l;

.field public static final JF:Lorg/spongycastle/asn1/l;

.field public static final JG:Lorg/spongycastle/asn1/l;

.field public static final JH:Lorg/spongycastle/asn1/l;

.field public static final JI:Lorg/spongycastle/asn1/l;

.field public static final JJ:Lorg/spongycastle/asn1/l;

.field public static final JK:Lorg/spongycastle/asn1/l;

.field public static final JL:Lorg/spongycastle/asn1/l;

.field public static final JM:Lorg/spongycastle/asn1/l;

.field public static final JN:Lorg/spongycastle/asn1/l;

.field public static final JO:Lorg/spongycastle/asn1/l;

.field public static final JP:Lorg/spongycastle/asn1/l;

.field public static final JQ:Lorg/spongycastle/asn1/l;

.field public static final JR:Lorg/spongycastle/asn1/l;

.field public static final Jm:Lorg/spongycastle/asn1/l;

.field public static final Jn:Lorg/spongycastle/asn1/l;

.field public static final Jo:Lorg/spongycastle/asn1/l;

.field public static final Jp:Lorg/spongycastle/asn1/l;

.field public static final Jq:Lorg/spongycastle/asn1/l;

.field public static final Jr:Lorg/spongycastle/asn1/l;

.field public static final Js:Lorg/spongycastle/asn1/l;

.field public static final Jt:Lorg/spongycastle/asn1/l;

.field public static final Ju:Lorg/spongycastle/asn1/l;

.field public static final Jv:Lorg/spongycastle/asn1/l;

.field public static final Jw:Lorg/spongycastle/asn1/l;

.field public static final Jx:Lorg/spongycastle/asn1/l;

.field public static final Jy:Lorg/spongycastle/asn1/l;

.field public static final Jz:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "2.16.840.1.101.3.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jm:Lorg/spongycastle/asn1/l;

    .line 16
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jm:Lorg/spongycastle/asn1/l;

    const-string v1, "2.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jn:Lorg/spongycastle/asn1/l;

    .line 17
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jm:Lorg/spongycastle/asn1/l;

    const-string v1, "2.2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jo:Lorg/spongycastle/asn1/l;

    .line 18
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jm:Lorg/spongycastle/asn1/l;

    const-string v1, "2.3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jp:Lorg/spongycastle/asn1/l;

    .line 19
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jm:Lorg/spongycastle/asn1/l;

    const-string v1, "2.4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jq:Lorg/spongycastle/asn1/l;

    .line 21
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jm:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    .line 23
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Js:Lorg/spongycastle/asn1/l;

    .line 24
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jt:Lorg/spongycastle/asn1/l;

    .line 25
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Ju:Lorg/spongycastle/asn1/l;

    .line 26
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jv:Lorg/spongycastle/asn1/l;

    .line 27
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jw:Lorg/spongycastle/asn1/l;

    .line 28
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jx:Lorg/spongycastle/asn1/l;

    .line 29
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jy:Lorg/spongycastle/asn1/l;

    .line 31
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "21"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->Jz:Lorg/spongycastle/asn1/l;

    .line 32
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "22"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JA:Lorg/spongycastle/asn1/l;

    .line 33
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "23"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JB:Lorg/spongycastle/asn1/l;

    .line 34
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "24"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JC:Lorg/spongycastle/asn1/l;

    .line 35
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "25"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JD:Lorg/spongycastle/asn1/l;

    .line 36
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "26"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JE:Lorg/spongycastle/asn1/l;

    .line 37
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "27"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JF:Lorg/spongycastle/asn1/l;

    .line 39
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "41"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JG:Lorg/spongycastle/asn1/l;

    .line 40
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "42"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JH:Lorg/spongycastle/asn1/l;

    .line 41
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "43"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JI:Lorg/spongycastle/asn1/l;

    .line 42
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "44"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JJ:Lorg/spongycastle/asn1/l;

    .line 43
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "45"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JK:Lorg/spongycastle/asn1/l;

    .line 44
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "46"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JL:Lorg/spongycastle/asn1/l;

    .line 45
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jr:Lorg/spongycastle/asn1/l;

    const-string v1, "47"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JM:Lorg/spongycastle/asn1/l;

    .line 50
    sget-object v0, Lorg/spongycastle/asn1/i/b;->Jm:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JN:Lorg/spongycastle/asn1/l;

    .line 52
    sget-object v0, Lorg/spongycastle/asn1/i/b;->JN:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JO:Lorg/spongycastle/asn1/l;

    .line 53
    sget-object v0, Lorg/spongycastle/asn1/i/b;->JN:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JP:Lorg/spongycastle/asn1/l;

    .line 54
    sget-object v0, Lorg/spongycastle/asn1/i/b;->JN:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JQ:Lorg/spongycastle/asn1/l;

    .line 55
    sget-object v0, Lorg/spongycastle/asn1/i/b;->JN:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/i/b;->JR:Lorg/spongycastle/asn1/l;

    return-void
.end method
