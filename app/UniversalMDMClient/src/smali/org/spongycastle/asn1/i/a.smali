.class public Lorg/spongycastle/asn1/i/a;
.super Ljava/lang/Object;
.source "NISTNamedCurves.java"


# static fields
.field static final HI:Ljava/util/Hashtable;

.field static final HJ:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/i/a;->HI:Ljava/util/Hashtable;

    .line 18
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/i/a;->HJ:Ljava/util/Hashtable;

    .line 30
    const-string v0, "B-571"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pf:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 31
    const-string v0, "B-409"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pd:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 32
    const-string v0, "B-283"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->ON:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 33
    const-string v0, "B-233"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OT:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 34
    const-string v0, "B-163"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OL:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 35
    const-string v0, "P-521"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pb:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 36
    const-string v0, "P-384"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pa:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 37
    const-string v0, "P-256"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Ph:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 38
    const-string v0, "P-224"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OZ:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 39
    const-string v0, "P-192"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pg:Lorg/spongycastle/asn1/l;

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/i/a;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    .line 40
    return-void
.end method

.method static a(Ljava/lang/String;Lorg/spongycastle/asn1/l;)V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lorg/spongycastle/asn1/i/a;->HI:Ljava/util/Hashtable;

    invoke-virtual {v0, p0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lorg/spongycastle/asn1/i/a;->HJ:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method

.method public static b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lorg/spongycastle/asn1/i/a;->HJ:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static cG(Ljava/lang/String;)Lorg/spongycastle/asn1/r/f;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lorg/spongycastle/asn1/i/a;->HI:Ljava/util/Hashtable;

    invoke-static {p0}, Lorg/spongycastle/util/g;->cZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 47
    if-eqz v0, :cond_0

    .line 49
    invoke-static {v0}, Lorg/spongycastle/asn1/i/a;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;
    .locals 1

    .prologue
    .line 64
    invoke-static {p0}, Lorg/spongycastle/asn1/m/c;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    return-object v0
.end method

.method public static getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lorg/spongycastle/asn1/i/a;->HI:Ljava/util/Hashtable;

    invoke-static {p0}, Lorg/spongycastle/util/g;->cZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    return-object v0
.end method
