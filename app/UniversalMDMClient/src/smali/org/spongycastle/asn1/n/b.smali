.class public interface abstract Lorg/spongycastle/asn1/n/b;
.super Ljava/lang/Object;
.source "TeleTrusTObjectIdentifiers.java"


# static fields
.field public static final OA:Lorg/spongycastle/asn1/l;

.field public static final PA:Lorg/spongycastle/asn1/l;

.field public static final PB:Lorg/spongycastle/asn1/l;

.field public static final PC:Lorg/spongycastle/asn1/l;

.field public static final PD:Lorg/spongycastle/asn1/l;

.field public static final PE:Lorg/spongycastle/asn1/l;

.field public static final PF:Lorg/spongycastle/asn1/l;

.field public static final PG:Lorg/spongycastle/asn1/l;

.field public static final PH:Lorg/spongycastle/asn1/l;

.field public static final PI:Lorg/spongycastle/asn1/l;

.field public static final PJ:Lorg/spongycastle/asn1/l;

.field public static final PK:Lorg/spongycastle/asn1/l;

.field public static final PL:Lorg/spongycastle/asn1/l;

.field public static final PM:Lorg/spongycastle/asn1/l;

.field public static final PN:Lorg/spongycastle/asn1/l;

.field public static final PO:Lorg/spongycastle/asn1/l;

.field public static final PP:Lorg/spongycastle/asn1/l;

.field public static final PQ:Lorg/spongycastle/asn1/l;

.field public static final PR:Lorg/spongycastle/asn1/l;

.field public static final PS:Lorg/spongycastle/asn1/l;

.field public static final PT:Lorg/spongycastle/asn1/l;

.field public static final PU:Lorg/spongycastle/asn1/l;

.field public static final PV:Lorg/spongycastle/asn1/l;

.field public static final PW:Lorg/spongycastle/asn1/l;

.field public static final Pw:Lorg/spongycastle/asn1/l;

.field public static final Px:Lorg/spongycastle/asn1/l;

.field public static final Py:Lorg/spongycastle/asn1/l;

.field public static final Pz:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.36.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/n/b;->Pw:Lorg/spongycastle/asn1/l;

    .line 9
    sget-object v0, Lorg/spongycastle/asn1/n/b;->Pw:Lorg/spongycastle/asn1/l;

    const-string v1, "2.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->Px:Lorg/spongycastle/asn1/l;

    .line 10
    sget-object v0, Lorg/spongycastle/asn1/n/b;->Pw:Lorg/spongycastle/asn1/l;

    const-string v1, "2.2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->Py:Lorg/spongycastle/asn1/l;

    .line 11
    sget-object v0, Lorg/spongycastle/asn1/n/b;->Pw:Lorg/spongycastle/asn1/l;

    const-string v1, "2.3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->Pz:Lorg/spongycastle/asn1/l;

    .line 13
    sget-object v0, Lorg/spongycastle/asn1/n/b;->Pw:Lorg/spongycastle/asn1/l;

    const-string v1, "3.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PA:Lorg/spongycastle/asn1/l;

    .line 15
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PA:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PB:Lorg/spongycastle/asn1/l;

    .line 16
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PA:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PC:Lorg/spongycastle/asn1/l;

    .line 17
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PA:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PD:Lorg/spongycastle/asn1/l;

    .line 19
    sget-object v0, Lorg/spongycastle/asn1/n/b;->Pw:Lorg/spongycastle/asn1/l;

    const-string v1, "3.2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PE:Lorg/spongycastle/asn1/l;

    .line 21
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PE:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PF:Lorg/spongycastle/asn1/l;

    .line 22
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PE:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PG:Lorg/spongycastle/asn1/l;

    .line 24
    sget-object v0, Lorg/spongycastle/asn1/n/b;->Pw:Lorg/spongycastle/asn1/l;

    const-string v1, "3.2.8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PH:Lorg/spongycastle/asn1/l;

    .line 25
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PH:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->OA:Lorg/spongycastle/asn1/l;

    .line 26
    sget-object v0, Lorg/spongycastle/asn1/n/b;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    .line 28
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PJ:Lorg/spongycastle/asn1/l;

    .line 29
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PK:Lorg/spongycastle/asn1/l;

    .line 30
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PL:Lorg/spongycastle/asn1/l;

    .line 31
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PM:Lorg/spongycastle/asn1/l;

    .line 32
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PN:Lorg/spongycastle/asn1/l;

    .line 33
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PO:Lorg/spongycastle/asn1/l;

    .line 34
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PP:Lorg/spongycastle/asn1/l;

    .line 35
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PQ:Lorg/spongycastle/asn1/l;

    .line 36
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PR:Lorg/spongycastle/asn1/l;

    .line 37
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "10"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PS:Lorg/spongycastle/asn1/l;

    .line 38
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "11"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PT:Lorg/spongycastle/asn1/l;

    .line 39
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "12"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PU:Lorg/spongycastle/asn1/l;

    .line 40
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "13"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PV:Lorg/spongycastle/asn1/l;

    .line 41
    sget-object v0, Lorg/spongycastle/asn1/n/b;->PI:Lorg/spongycastle/asn1/l;

    const-string v1, "14"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/n/b;->PW:Lorg/spongycastle/asn1/l;

    return-void
.end method
