.class public interface abstract Lorg/spongycastle/asn1/c/a;
.super Ljava/lang/Object;
.source "CryptoProObjectIdentifiers.java"


# static fields
.field public static final HA:Lorg/spongycastle/asn1/l;

.field public static final HB:Lorg/spongycastle/asn1/l;

.field public static final HC:Lorg/spongycastle/asn1/l;

.field public static final HD:Lorg/spongycastle/asn1/l;

.field public static final HE:Lorg/spongycastle/asn1/l;

.field public static final HF:Lorg/spongycastle/asn1/l;

.field public static final HG:Lorg/spongycastle/asn1/l;

.field public static final HH:Lorg/spongycastle/asn1/l;

.field public static final Hn:Lorg/spongycastle/asn1/l;

.field public static final Ho:Lorg/spongycastle/asn1/l;

.field public static final Hp:Lorg/spongycastle/asn1/l;

.field public static final Hq:Lorg/spongycastle/asn1/l;

.field public static final Hr:Lorg/spongycastle/asn1/l;

.field public static final Hs:Lorg/spongycastle/asn1/l;

.field public static final Ht:Lorg/spongycastle/asn1/l;

.field public static final Hu:Lorg/spongycastle/asn1/l;

.field public static final Hv:Lorg/spongycastle/asn1/l;

.field public static final Hw:Lorg/spongycastle/asn1/l;

.field public static final Hx:Lorg/spongycastle/asn1/l;

.field public static final Hy:Lorg/spongycastle/asn1/l;

.field public static final Hz:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.9"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hn:Lorg/spongycastle/asn1/l;

    .line 13
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.21"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Ho:Lorg/spongycastle/asn1/l;

    .line 15
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.20"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hp:Lorg/spongycastle/asn1/l;

    .line 16
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.19"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hq:Lorg/spongycastle/asn1/l;

    .line 17
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hr:Lorg/spongycastle/asn1/l;

    .line 18
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hs:Lorg/spongycastle/asn1/l;

    .line 21
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.30.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Ht:Lorg/spongycastle/asn1/l;

    .line 24
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.32.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hu:Lorg/spongycastle/asn1/l;

    .line 25
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.32.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hv:Lorg/spongycastle/asn1/l;

    .line 26
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.32.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hw:Lorg/spongycastle/asn1/l;

    .line 27
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.32.5"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hx:Lorg/spongycastle/asn1/l;

    .line 30
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.33.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hy:Lorg/spongycastle/asn1/l;

    .line 31
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.33.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->Hz:Lorg/spongycastle/asn1/l;

    .line 32
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.33.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HA:Lorg/spongycastle/asn1/l;

    .line 35
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.35.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HB:Lorg/spongycastle/asn1/l;

    .line 36
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.35.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HC:Lorg/spongycastle/asn1/l;

    .line 37
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.35.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HD:Lorg/spongycastle/asn1/l;

    .line 40
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.36.0"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HE:Lorg/spongycastle/asn1/l;

    .line 41
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.36.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HF:Lorg/spongycastle/asn1/l;

    .line 43
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.36.0"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HG:Lorg/spongycastle/asn1/l;

    .line 44
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.2.643.2.2.36.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/c/a;->HH:Lorg/spongycastle/asn1/l;

    return-void
.end method
