.class public Lorg/spongycastle/asn1/c/e;
.super Lorg/spongycastle/asn1/k;
.source "GOST3410PublicKeyAlgParameters.java"


# instance fields
.field private HQ:Lorg/spongycastle/asn1/l;

.field private HR:Lorg/spongycastle/asn1/l;

.field private HT:Lorg/spongycastle/asn1/l;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/l;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/spongycastle/asn1/c/e;->HQ:Lorg/spongycastle/asn1/l;

    .line 46
    iput-object p2, p0, Lorg/spongycastle/asn1/c/e;->HR:Lorg/spongycastle/asn1/l;

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/c/e;->HT:Lorg/spongycastle/asn1/l;

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/l;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/spongycastle/asn1/c/e;->HQ:Lorg/spongycastle/asn1/l;

    .line 56
    iput-object p2, p0, Lorg/spongycastle/asn1/c/e;->HR:Lorg/spongycastle/asn1/l;

    .line 57
    iput-object p3, p0, Lorg/spongycastle/asn1/c/e;->HT:Lorg/spongycastle/asn1/l;

    .line 58
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 62
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 63
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/c/e;->HQ:Lorg/spongycastle/asn1/l;

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/c/e;->HR:Lorg/spongycastle/asn1/l;

    .line 66
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 68
    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/c/e;->HT:Lorg/spongycastle/asn1/l;

    .line 70
    :cond_0
    return-void
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 91
    iget-object v1, p0, Lorg/spongycastle/asn1/c/e;->HQ:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 92
    iget-object v1, p0, Lorg/spongycastle/asn1/c/e;->HR:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 94
    iget-object v1, p0, Lorg/spongycastle/asn1/c/e;->HT:Lorg/spongycastle/asn1/l;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lorg/spongycastle/asn1/c/e;->HT:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 99
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public nq()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/spongycastle/asn1/c/e;->HQ:Lorg/spongycastle/asn1/l;

    return-object v0
.end method

.method public nr()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/spongycastle/asn1/c/e;->HR:Lorg/spongycastle/asn1/l;

    return-object v0
.end method

.method public ns()Lorg/spongycastle/asn1/l;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/spongycastle/asn1/c/e;->HT:Lorg/spongycastle/asn1/l;

    return-object v0
.end method
