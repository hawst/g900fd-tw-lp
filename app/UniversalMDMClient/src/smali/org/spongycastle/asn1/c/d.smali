.class public Lorg/spongycastle/asn1/c/d;
.super Lorg/spongycastle/asn1/k;
.source "GOST3410ParamSetParameters.java"


# instance fields
.field HN:Lorg/spongycastle/asn1/i;

.field HO:Lorg/spongycastle/asn1/i;

.field HP:Lorg/spongycastle/asn1/i;

.field keySize:I


# direct methods
.method public constructor <init>(ILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 49
    iput p1, p0, Lorg/spongycastle/asn1/c/d;->keySize:I

    .line 50
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/c/d;->HN:Lorg/spongycastle/asn1/i;

    .line 51
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p3}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/c/d;->HO:Lorg/spongycastle/asn1/i;

    .line 52
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-direct {v0, p4}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/c/d;->HP:Lorg/spongycastle/asn1/i;

    .line 53
    return-void
.end method


# virtual methods
.method public getA()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/spongycastle/asn1/c/d;->HP:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getP()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/spongycastle/asn1/c/d;->HN:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/spongycastle/asn1/c/d;->HO:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->nk()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public mC()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 98
    new-instance v1, Lorg/spongycastle/asn1/i;

    iget v2, p0, Lorg/spongycastle/asn1/c/d;->keySize:I

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 99
    iget-object v1, p0, Lorg/spongycastle/asn1/c/d;->HN:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 100
    iget-object v1, p0, Lorg/spongycastle/asn1/c/d;->HO:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 101
    iget-object v1, p0, Lorg/spongycastle/asn1/c/d;->HP:Lorg/spongycastle/asn1/i;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 103
    new-instance v1, Lorg/spongycastle/asn1/bh;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
