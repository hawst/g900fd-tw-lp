.class public Lorg/spongycastle/asn1/am;
.super Ljava/lang/Object;
.source "BERTaggedObjectParser.java"

# interfaces
.implements Lorg/spongycastle/asn1/y;


# instance fields
.field private Ge:Lorg/spongycastle/asn1/v;

.field private Gf:Z

.field private Gg:I


# direct methods
.method constructor <init>(ZILorg/spongycastle/asn1/v;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-boolean p1, p0, Lorg/spongycastle/asn1/am;->Gf:Z

    .line 18
    iput p2, p0, Lorg/spongycastle/asn1/am;->Gg:I

    .line 19
    iput-object p3, p0, Lorg/spongycastle/asn1/am;->Ge:Lorg/spongycastle/asn1/v;

    .line 20
    return-void
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 59
    :try_start_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/am;->mI()Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 61
    :catch_0
    move-exception v0

    .line 63
    new-instance v1, Lorg/spongycastle/asn1/ASN1ParsingException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ASN1ParsingException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public mI()Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lorg/spongycastle/asn1/am;->Ge:Lorg/spongycastle/asn1/v;

    iget-boolean v1, p0, Lorg/spongycastle/asn1/am;->Gf:Z

    iget v2, p0, Lorg/spongycastle/asn1/am;->Gg:I

    invoke-virtual {v0, v1, v2}, Lorg/spongycastle/asn1/v;->f(ZI)Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method
