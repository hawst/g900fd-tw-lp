.class public interface abstract Lorg/spongycastle/asn1/m/d;
.super Ljava/lang/Object;
.source "SECObjectIdentifiers.java"


# static fields
.field public static final OA:Lorg/spongycastle/asn1/l;

.field public static final OB:Lorg/spongycastle/asn1/l;

.field public static final OC:Lorg/spongycastle/asn1/l;

.field public static final OD:Lorg/spongycastle/asn1/l;

.field public static final OE:Lorg/spongycastle/asn1/l;

.field public static final OF:Lorg/spongycastle/asn1/l;

.field public static final OG:Lorg/spongycastle/asn1/l;

.field public static final OH:Lorg/spongycastle/asn1/l;

.field public static final OI:Lorg/spongycastle/asn1/l;

.field public static final OJ:Lorg/spongycastle/asn1/l;

.field public static final OK:Lorg/spongycastle/asn1/l;

.field public static final OL:Lorg/spongycastle/asn1/l;

.field public static final OM:Lorg/spongycastle/asn1/l;

.field public static final ON:Lorg/spongycastle/asn1/l;

.field public static final OO:Lorg/spongycastle/asn1/l;

.field public static final OP:Lorg/spongycastle/asn1/l;

.field public static final OQ:Lorg/spongycastle/asn1/l;

.field public static final OR:Lorg/spongycastle/asn1/l;

.field public static final OS:Lorg/spongycastle/asn1/l;

.field public static final OT:Lorg/spongycastle/asn1/l;

.field public static final OU:Lorg/spongycastle/asn1/l;

.field public static final OV:Lorg/spongycastle/asn1/l;

.field public static final OW:Lorg/spongycastle/asn1/l;

.field public static final OX:Lorg/spongycastle/asn1/l;

.field public static final OY:Lorg/spongycastle/asn1/l;

.field public static final OZ:Lorg/spongycastle/asn1/l;

.field public static final Pa:Lorg/spongycastle/asn1/l;

.field public static final Pb:Lorg/spongycastle/asn1/l;

.field public static final Pc:Lorg/spongycastle/asn1/l;

.field public static final Pd:Lorg/spongycastle/asn1/l;

.field public static final Pe:Lorg/spongycastle/asn1/l;

.field public static final Pf:Lorg/spongycastle/asn1/l;

.field public static final Pg:Lorg/spongycastle/asn1/l;

.field public static final Ph:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string v1, "1.3.132.0"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    .line 15
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OB:Lorg/spongycastle/asn1/l;

    .line 16
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OC:Lorg/spongycastle/asn1/l;

    .line 17
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OD:Lorg/spongycastle/asn1/l;

    .line 18
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OE:Lorg/spongycastle/asn1/l;

    .line 19
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OF:Lorg/spongycastle/asn1/l;

    .line 20
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OG:Lorg/spongycastle/asn1/l;

    .line 21
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OH:Lorg/spongycastle/asn1/l;

    .line 22
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OI:Lorg/spongycastle/asn1/l;

    .line 23
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OJ:Lorg/spongycastle/asn1/l;

    .line 24
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "10"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OK:Lorg/spongycastle/asn1/l;

    .line 25
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OL:Lorg/spongycastle/asn1/l;

    .line 26
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "16"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OM:Lorg/spongycastle/asn1/l;

    .line 27
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "17"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->ON:Lorg/spongycastle/asn1/l;

    .line 28
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "22"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OO:Lorg/spongycastle/asn1/l;

    .line 29
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "23"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OP:Lorg/spongycastle/asn1/l;

    .line 30
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "24"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OQ:Lorg/spongycastle/asn1/l;

    .line 31
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "25"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OR:Lorg/spongycastle/asn1/l;

    .line 32
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "26"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OS:Lorg/spongycastle/asn1/l;

    .line 33
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "27"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OT:Lorg/spongycastle/asn1/l;

    .line 34
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "28"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OU:Lorg/spongycastle/asn1/l;

    .line 35
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "29"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OV:Lorg/spongycastle/asn1/l;

    .line 36
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "30"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OW:Lorg/spongycastle/asn1/l;

    .line 37
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "31"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OX:Lorg/spongycastle/asn1/l;

    .line 38
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "32"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OY:Lorg/spongycastle/asn1/l;

    .line 39
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "33"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->OZ:Lorg/spongycastle/asn1/l;

    .line 40
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "34"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Pa:Lorg/spongycastle/asn1/l;

    .line 41
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "35"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Pb:Lorg/spongycastle/asn1/l;

    .line 42
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "36"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Pc:Lorg/spongycastle/asn1/l;

    .line 43
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "37"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Pd:Lorg/spongycastle/asn1/l;

    .line 44
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "38"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Pe:Lorg/spongycastle/asn1/l;

    .line 45
    sget-object v0, Lorg/spongycastle/asn1/m/d;->OA:Lorg/spongycastle/asn1/l;

    const-string v1, "39"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->cB(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Pf:Lorg/spongycastle/asn1/l;

    .line 47
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VS:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Pg:Lorg/spongycastle/asn1/l;

    .line 48
    sget-object v0, Lorg/spongycastle/asn1/r/l;->VY:Lorg/spongycastle/asn1/l;

    sput-object v0, Lorg/spongycastle/asn1/m/d;->Ph:Lorg/spongycastle/asn1/l;

    return-void
.end method
