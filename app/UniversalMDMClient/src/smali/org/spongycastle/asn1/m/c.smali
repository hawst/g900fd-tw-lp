.class public Lorg/spongycastle/asn1/m/c;
.super Ljava/lang/Object;
.source "SECNamedCurves.java"


# static fields
.field static final HI:Ljava/util/Hashtable;

.field static final HJ:Ljava/util/Hashtable;

.field static NR:Lorg/spongycastle/asn1/r/g;

.field static NT:Lorg/spongycastle/asn1/r/g;

.field static NU:Lorg/spongycastle/asn1/r/g;

.field static NV:Lorg/spongycastle/asn1/r/g;

.field static NW:Lorg/spongycastle/asn1/r/g;

.field static NX:Lorg/spongycastle/asn1/r/g;

.field static NY:Lorg/spongycastle/asn1/r/g;

.field static NZ:Lorg/spongycastle/asn1/r/g;

.field static Oa:Lorg/spongycastle/asn1/r/g;

.field static Ob:Lorg/spongycastle/asn1/r/g;

.field static Oc:Lorg/spongycastle/asn1/r/g;

.field static Od:Lorg/spongycastle/asn1/r/g;

.field static Oe:Lorg/spongycastle/asn1/r/g;

.field static Of:Lorg/spongycastle/asn1/r/g;

.field static Og:Lorg/spongycastle/asn1/r/g;

.field static Oh:Lorg/spongycastle/asn1/r/g;

.field static Oi:Lorg/spongycastle/asn1/r/g;

.field static Oj:Lorg/spongycastle/asn1/r/g;

.field static Ok:Lorg/spongycastle/asn1/r/g;

.field static Ol:Lorg/spongycastle/asn1/r/g;

.field static Om:Lorg/spongycastle/asn1/r/g;

.field static On:Lorg/spongycastle/asn1/r/g;

.field static Oo:Lorg/spongycastle/asn1/r/g;

.field static Op:Lorg/spongycastle/asn1/r/g;

.field static Oq:Lorg/spongycastle/asn1/r/g;

.field static Or:Lorg/spongycastle/asn1/r/g;

.field static Os:Lorg/spongycastle/asn1/r/g;

.field static Ot:Lorg/spongycastle/asn1/r/g;

.field static Ou:Lorg/spongycastle/asn1/r/g;

.field static Ov:Lorg/spongycastle/asn1/r/g;

.field static Ow:Lorg/spongycastle/asn1/r/g;

.field static Ox:Lorg/spongycastle/asn1/r/g;

.field static Oy:Lorg/spongycastle/asn1/r/g;

.field static final Oz:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    new-instance v0, Lorg/spongycastle/asn1/m/c$1;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$1;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NR:Lorg/spongycastle/asn1/r/g;

    .line 53
    new-instance v0, Lorg/spongycastle/asn1/m/c$12;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$12;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NT:Lorg/spongycastle/asn1/r/g;

    .line 79
    new-instance v0, Lorg/spongycastle/asn1/m/c$23;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$23;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NU:Lorg/spongycastle/asn1/r/g;

    .line 105
    new-instance v0, Lorg/spongycastle/asn1/m/c$28;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$28;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NV:Lorg/spongycastle/asn1/r/g;

    .line 131
    new-instance v0, Lorg/spongycastle/asn1/m/c$29;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$29;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NW:Lorg/spongycastle/asn1/r/g;

    .line 157
    new-instance v0, Lorg/spongycastle/asn1/m/c$30;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$30;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NX:Lorg/spongycastle/asn1/r/g;

    .line 183
    new-instance v0, Lorg/spongycastle/asn1/m/c$31;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$31;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NY:Lorg/spongycastle/asn1/r/g;

    .line 209
    new-instance v0, Lorg/spongycastle/asn1/m/c$32;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$32;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->NZ:Lorg/spongycastle/asn1/r/g;

    .line 235
    new-instance v0, Lorg/spongycastle/asn1/m/c$33;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$33;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oa:Lorg/spongycastle/asn1/r/g;

    .line 261
    new-instance v0, Lorg/spongycastle/asn1/m/c$2;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$2;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ob:Lorg/spongycastle/asn1/r/g;

    .line 287
    new-instance v0, Lorg/spongycastle/asn1/m/c$3;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$3;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oc:Lorg/spongycastle/asn1/r/g;

    .line 313
    new-instance v0, Lorg/spongycastle/asn1/m/c$4;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$4;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Od:Lorg/spongycastle/asn1/r/g;

    .line 339
    new-instance v0, Lorg/spongycastle/asn1/m/c$5;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$5;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oe:Lorg/spongycastle/asn1/r/g;

    .line 365
    new-instance v0, Lorg/spongycastle/asn1/m/c$6;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$6;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Of:Lorg/spongycastle/asn1/r/g;

    .line 391
    new-instance v0, Lorg/spongycastle/asn1/m/c$7;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$7;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Og:Lorg/spongycastle/asn1/r/g;

    .line 417
    new-instance v0, Lorg/spongycastle/asn1/m/c$8;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$8;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oh:Lorg/spongycastle/asn1/r/g;

    .line 444
    new-instance v0, Lorg/spongycastle/asn1/m/c$9;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$9;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oi:Lorg/spongycastle/asn1/r/g;

    .line 471
    new-instance v0, Lorg/spongycastle/asn1/m/c$10;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$10;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oj:Lorg/spongycastle/asn1/r/g;

    .line 500
    new-instance v0, Lorg/spongycastle/asn1/m/c$11;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$11;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ok:Lorg/spongycastle/asn1/r/g;

    .line 529
    new-instance v0, Lorg/spongycastle/asn1/m/c$13;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$13;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ol:Lorg/spongycastle/asn1/r/g;

    .line 558
    new-instance v0, Lorg/spongycastle/asn1/m/c$14;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$14;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Om:Lorg/spongycastle/asn1/r/g;

    .line 587
    new-instance v0, Lorg/spongycastle/asn1/m/c$15;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$15;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->On:Lorg/spongycastle/asn1/r/g;

    .line 616
    new-instance v0, Lorg/spongycastle/asn1/m/c$16;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$16;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oo:Lorg/spongycastle/asn1/r/g;

    .line 643
    new-instance v0, Lorg/spongycastle/asn1/m/c$17;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$17;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Op:Lorg/spongycastle/asn1/r/g;

    .line 670
    new-instance v0, Lorg/spongycastle/asn1/m/c$18;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$18;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oq:Lorg/spongycastle/asn1/r/g;

    .line 697
    new-instance v0, Lorg/spongycastle/asn1/m/c$19;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$19;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Or:Lorg/spongycastle/asn1/r/g;

    .line 724
    new-instance v0, Lorg/spongycastle/asn1/m/c$20;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$20;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Os:Lorg/spongycastle/asn1/r/g;

    .line 751
    new-instance v0, Lorg/spongycastle/asn1/m/c$21;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$21;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ot:Lorg/spongycastle/asn1/r/g;

    .line 780
    new-instance v0, Lorg/spongycastle/asn1/m/c$22;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$22;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ou:Lorg/spongycastle/asn1/r/g;

    .line 809
    new-instance v0, Lorg/spongycastle/asn1/m/c$24;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$24;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ov:Lorg/spongycastle/asn1/r/g;

    .line 836
    new-instance v0, Lorg/spongycastle/asn1/m/c$25;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$25;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ow:Lorg/spongycastle/asn1/r/g;

    .line 863
    new-instance v0, Lorg/spongycastle/asn1/m/c$26;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$26;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Ox:Lorg/spongycastle/asn1/r/g;

    .line 892
    new-instance v0, Lorg/spongycastle/asn1/m/c$27;

    invoke-direct {v0}, Lorg/spongycastle/asn1/m/c$27;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oy:Lorg/spongycastle/asn1/r/g;

    .line 919
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->HI:Ljava/util/Hashtable;

    .line 920
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->Oz:Ljava/util/Hashtable;

    .line 921
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/m/c;->HJ:Ljava/util/Hashtable;

    .line 932
    const-string v0, "secp112r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OG:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NR:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 933
    const-string v0, "secp112r2"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OH:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NT:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 934
    const-string v0, "secp128r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OU:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NU:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 935
    const-string v0, "secp128r2"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OV:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NV:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 936
    const-string v0, "secp160k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OJ:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NW:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 937
    const-string v0, "secp160r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OI:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NX:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 938
    const-string v0, "secp160r2"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OW:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NY:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 939
    const-string v0, "secp192k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OX:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->NZ:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 940
    const-string v0, "secp192r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pg:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oa:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 941
    const-string v0, "secp224k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OY:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ob:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 942
    const-string v0, "secp224r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OZ:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oc:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 943
    const-string v0, "secp256k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OK:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Od:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 944
    const-string v0, "secp256r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Ph:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oe:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 945
    const-string v0, "secp384r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pa:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Of:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 946
    const-string v0, "secp521r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pb:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Og:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 948
    const-string v0, "sect113r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OE:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oh:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 949
    const-string v0, "sect113r2"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OF:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oi:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 950
    const-string v0, "sect131r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OO:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oj:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 951
    const-string v0, "sect131r2"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OP:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ok:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 952
    const-string v0, "sect163k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OB:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ol:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 953
    const-string v0, "sect163r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OC:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Om:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 954
    const-string v0, "sect163r2"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OL:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->On:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 955
    const-string v0, "sect193r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OQ:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oo:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 956
    const-string v0, "sect193r2"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OR:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Op:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 957
    const-string v0, "sect233k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OS:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oq:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 958
    const-string v0, "sect233r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OT:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Or:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 959
    const-string v0, "sect239k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OD:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Os:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 960
    const-string v0, "sect283k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->OM:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ot:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 961
    const-string v0, "sect283r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->ON:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ou:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 962
    const-string v0, "sect409k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pc:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ov:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 963
    const-string v0, "sect409r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pd:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ow:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 964
    const-string v0, "sect571k1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pe:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Ox:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 965
    const-string v0, "sect571r1"

    sget-object v1, Lorg/spongycastle/asn1/m/d;->Pf:Lorg/spongycastle/asn1/l;

    sget-object v2, Lorg/spongycastle/asn1/m/c;->Oy:Lorg/spongycastle/asn1/r/g;

    invoke-static {v0, v1, v2}, Lorg/spongycastle/asn1/m/c;->a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V

    .line 966
    return-void
.end method

.method static a(Ljava/lang/String;Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/r/g;)V
    .locals 1

    .prologue
    .line 925
    sget-object v0, Lorg/spongycastle/asn1/m/c;->HI:Ljava/util/Hashtable;

    invoke-virtual {v0, p0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 926
    sget-object v0, Lorg/spongycastle/asn1/m/c;->HJ:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 927
    sget-object v0, Lorg/spongycastle/asn1/m/c;->Oz:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    return-void
.end method

.method public static b(Lorg/spongycastle/asn1/l;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1018
    sget-object v0, Lorg/spongycastle/asn1/m/c;->HJ:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static cG(Ljava/lang/String;)Lorg/spongycastle/asn1/r/f;
    .locals 2

    .prologue
    .line 971
    sget-object v0, Lorg/spongycastle/asn1/m/c;->HI:Ljava/util/Hashtable;

    invoke-static {p0}, Lorg/spongycastle/util/g;->da(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    .line 973
    if-eqz v0, :cond_0

    .line 975
    invoke-static {v0}, Lorg/spongycastle/asn1/m/c;->d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 978
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static cH(Ljava/lang/String;)Ljava/math/BigInteger;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Ljava/math/BigInteger;

    const/4 v1, 0x1

    invoke-static {p0}, Lorg/spongycastle/util/a/d;->dc(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/math/BigInteger;-><init>(I[B)V

    return-object v0
.end method

.method static synthetic cI(Ljava/lang/String;)Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 16
    invoke-static {p0}, Lorg/spongycastle/asn1/m/c;->cH(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lorg/spongycastle/asn1/l;)Lorg/spongycastle/asn1/r/f;
    .locals 1

    .prologue
    .line 990
    sget-object v0, Lorg/spongycastle/asn1/m/c;->Oz:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/r/g;

    .line 992
    if-eqz v0, :cond_0

    .line 994
    invoke-virtual {v0}, Lorg/spongycastle/asn1/r/g;->pS()Lorg/spongycastle/asn1/r/f;

    move-result-object v0

    .line 997
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getOID(Ljava/lang/String;)Lorg/spongycastle/asn1/l;
    .locals 2

    .prologue
    .line 1009
    sget-object v0, Lorg/spongycastle/asn1/m/c;->HI:Ljava/util/Hashtable;

    invoke-static {p0}, Lorg/spongycastle/util/g;->da(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    return-object v0
.end method
