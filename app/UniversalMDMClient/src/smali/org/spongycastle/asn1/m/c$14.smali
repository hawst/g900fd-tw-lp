.class final Lorg/spongycastle/asn1/m/c$14;
.super Lorg/spongycastle/asn1/r/g;
.source "SECNamedCurves.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/spongycastle/asn1/m/c;
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Lorg/spongycastle/asn1/r/g;-><init>()V

    return-void
.end method


# virtual methods
.method protected oe()Lorg/spongycastle/asn1/r/f;
    .locals 12

    .prologue
    .line 562
    const/16 v1, 0xa3

    .line 563
    const/4 v2, 0x3

    .line 564
    const/4 v3, 0x6

    .line 565
    const/4 v4, 0x7

    .line 567
    const-string v0, "07B6882CAAEFA84F9554FF8428BD88E246D2782AE2"

    invoke-static {v0}, Lorg/spongycastle/asn1/m/c;->cI(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v5

    .line 568
    const-string v0, "0713612DCDDCB40AAB946BDA29CA91F73AF958AFD9"

    invoke-static {v0}, Lorg/spongycastle/asn1/m/c;->cI(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v6

    .line 569
    const-string v0, "24B7B137C8A14D696E6768756151756FD0DA2E5C"

    invoke-static {v0}, Lorg/spongycastle/util/a/d;->dc(Ljava/lang/String;)[B

    move-result-object v9

    .line 570
    const-string v0, "03FFFFFFFFFFFFFFFFFFFF48AAB689C29CA710279B"

    invoke-static {v0}, Lorg/spongycastle/asn1/m/c;->cI(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v7

    .line 571
    const-wide/16 v10, 0x2

    invoke-static {v10, v11}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v8

    .line 573
    new-instance v0, Lorg/spongycastle/a/a/d;

    invoke-direct/range {v0 .. v8}, Lorg/spongycastle/a/a/d;-><init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 576
    const-string v1, "040369979697AB43897789566789567F787A7876A65400435EDB42EFAFB2989D51FEFCE3C80988F41FF883"

    invoke-static {v1}, Lorg/spongycastle/util/a/d;->dc(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/c;->R([B)Lorg/spongycastle/a/a/j;

    move-result-object v6

    .line 580
    new-instance v4, Lorg/spongycastle/asn1/r/f;

    move-object v5, v0

    invoke-direct/range {v4 .. v9}, Lorg/spongycastle/asn1/r/f;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    return-object v4
.end method
