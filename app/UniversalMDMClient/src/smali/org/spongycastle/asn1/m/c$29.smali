.class final Lorg/spongycastle/asn1/m/c$29;
.super Lorg/spongycastle/asn1/r/g;
.source "SECNamedCurves.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/spongycastle/asn1/m/c;
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Lorg/spongycastle/asn1/r/g;-><init>()V

    return-void
.end method


# virtual methods
.method protected oe()Lorg/spongycastle/asn1/r/f;
    .locals 10

    .prologue
    .line 136
    const-string v0, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFAC73"

    invoke-static {v0}, Lorg/spongycastle/asn1/m/c;->cI(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v0

    .line 137
    sget-object v2, Lorg/spongycastle/a/a/b;->ZERO:Ljava/math/BigInteger;

    .line 138
    const-wide/16 v4, 0x7

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    .line 139
    const/4 v5, 0x0

    .line 140
    const-string v1, "0100000000000000000001B8FA16DFAB9ACA16B6B3"

    invoke-static {v1}, Lorg/spongycastle/asn1/m/c;->cI(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v3

    .line 141
    const-wide/16 v8, 0x1

    invoke-static {v8, v9}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    .line 143
    new-instance v1, Lorg/spongycastle/a/a/e;

    invoke-direct {v1, v0, v2, v6}, Lorg/spongycastle/a/a/e;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 146
    const-string v0, "043B4C382CE37AA192A4019E763036F4F5DD4D7EBB938CF935318FDCED6BC28286531733C3F03C4FEE"

    invoke-static {v0}, Lorg/spongycastle/util/a/d;->dc(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/spongycastle/a/a/c;->R([B)Lorg/spongycastle/a/a/j;

    move-result-object v2

    .line 150
    new-instance v0, Lorg/spongycastle/asn1/r/f;

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/asn1/r/f;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    return-object v0
.end method
