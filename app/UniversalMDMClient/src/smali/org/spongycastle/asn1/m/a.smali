.class public Lorg/spongycastle/asn1/m/a;
.super Lorg/spongycastle/asn1/k;
.source "ECPrivateKey.java"


# instance fields
.field private NQ:Lorg/spongycastle/asn1/r;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/aq;Lorg/spongycastle/asn1/k;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 74
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 75
    invoke-static {p1}, Lorg/spongycastle/util/b;->g(Ljava/math/BigInteger;)[B

    move-result-object v0

    .line 77
    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 79
    new-instance v2, Lorg/spongycastle/asn1/i;

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 80
    new-instance v2, Lorg/spongycastle/asn1/bd;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 82
    if-eqz p3, :cond_0

    .line 84
    new-instance v0, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x0

    invoke-direct {v0, v3, v2, p3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 87
    :cond_0
    if-eqz p2, :cond_1

    .line 89
    new-instance v0, Lorg/spongycastle/asn1/bm;

    invoke-direct {v0, v3, v3, p2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 92
    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/m/a;->NQ:Lorg/spongycastle/asn1/r;

    .line 93
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/k;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lorg/spongycastle/asn1/m/a;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/aq;Lorg/spongycastle/asn1/k;)V

    .line 68
    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 31
    iput-object p1, p0, Lorg/spongycastle/asn1/m/a;->NQ:Lorg/spongycastle/asn1/r;

    .line 32
    return-void
.end method

.method public static as(Ljava/lang/Object;)Lorg/spongycastle/asn1/m/a;
    .locals 2

    .prologue
    .line 37
    instance-of v0, p0, Lorg/spongycastle/asn1/m/a;

    if-eqz v0, :cond_0

    .line 39
    check-cast p0, Lorg/spongycastle/asn1/m/a;

    .line 47
    :goto_0
    return-object p0

    .line 42
    :cond_0
    if-eqz p0, :cond_1

    .line 44
    new-instance v0, Lorg/spongycastle/asn1/m/a;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/m/a;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 47
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private bN(I)Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lorg/spongycastle/asn1/m/a;->NQ:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 116
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    .line 120
    instance-of v2, v0, Lorg/spongycastle/asn1/x;

    if-eqz v2, :cond_0

    .line 122
    check-cast v0, Lorg/spongycastle/asn1/x;

    .line 123
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 125
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/spongycastle/asn1/m/a;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public ob()Ljava/math/BigInteger;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 97
    iget-object v0, p0, Lorg/spongycastle/asn1/m/a;->NQ:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    .line 99
    new-instance v1, Ljava/math/BigInteger;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    return-object v1
.end method

.method public oc()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/m/a;->bN(I)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/aq;

    return-object v0
.end method

.method public od()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/m/a;->bN(I)Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method
