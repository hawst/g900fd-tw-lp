.class public Lorg/spongycastle/asn1/m/b;
.super Lorg/spongycastle/asn1/k;
.source "ECPrivateKeyStructure.java"


# instance fields
.field private NQ:Lorg/spongycastle/asn1/r;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/aq;Lorg/spongycastle/asn1/d;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 59
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 60
    invoke-static {p1}, Lorg/spongycastle/util/b;->g(Ljava/math/BigInteger;)[B

    move-result-object v0

    .line 62
    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 64
    new-instance v2, Lorg/spongycastle/asn1/i;

    invoke-direct {v2, v3}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 65
    new-instance v2, Lorg/spongycastle/asn1/bd;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/bd;-><init>([B)V

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 67
    if-eqz p3, :cond_0

    .line 69
    new-instance v0, Lorg/spongycastle/asn1/bm;

    const/4 v2, 0x0

    invoke-direct {v0, v3, v2, p3}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 72
    :cond_0
    if-eqz p2, :cond_1

    .line 74
    new-instance v0, Lorg/spongycastle/asn1/bm;

    invoke-direct {v0, v3, v3, p2}, Lorg/spongycastle/asn1/bm;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 77
    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;)V

    iput-object v0, p0, Lorg/spongycastle/asn1/m/b;->NQ:Lorg/spongycastle/asn1/r;

    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/d;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lorg/spongycastle/asn1/m/b;-><init>(Ljava/math/BigInteger;Lorg/spongycastle/asn1/aq;Lorg/spongycastle/asn1/d;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/spongycastle/asn1/m/b;->NQ:Lorg/spongycastle/asn1/r;

    .line 33
    return-void
.end method

.method private bN(I)Lorg/spongycastle/asn1/q;
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lorg/spongycastle/asn1/m/b;->NQ:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->mQ()Ljava/util/Enumeration;

    move-result-object v1

    .line 101
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    .line 105
    instance-of v2, v0, Lorg/spongycastle/asn1/x;

    if-eqz v2, :cond_0

    .line 107
    check-cast v0, Lorg/spongycastle/asn1/x;

    .line 108
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 110
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->mC()Lorg/spongycastle/asn1/q;

    move-result-object v0

    .line 114
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/spongycastle/asn1/m/b;->NQ:Lorg/spongycastle/asn1/r;

    return-object v0
.end method

.method public ob()Ljava/math/BigInteger;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 82
    iget-object v0, p0, Lorg/spongycastle/asn1/m/b;->NQ:Lorg/spongycastle/asn1/r;

    invoke-virtual {v0, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    .line 84
    new-instance v1, Ljava/math/BigInteger;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->getOctets()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    return-object v1
.end method

.method public oc()Lorg/spongycastle/asn1/aq;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/m/b;->bN(I)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/aq;

    return-object v0
.end method
