.class public Lorg/spongycastle/asn1/b/a;
.super Lorg/spongycastle/asn1/k;
.source "ContentInfo.java"


# instance fields
.field private Hl:Lorg/spongycastle/asn1/l;

.field private Hm:Lorg/spongycastle/asn1/d;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 37
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 38
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-lt v0, v2, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    .line 40
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/b/a;->Hl:Lorg/spongycastle/asn1/l;

    .line 45
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->size()I

    move-result v0

    if-le v0, v2, :cond_4

    .line 47
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->bG(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/x;

    .line 48
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mU()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mT()I

    move-result v1

    if-eqz v1, :cond_3

    .line 50
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad tag for \'content\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_3
    invoke-virtual {v0}, Lorg/spongycastle/asn1/x;->mV()Lorg/spongycastle/asn1/q;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/b/a;->Hm:Lorg/spongycastle/asn1/d;

    .line 55
    :cond_4
    return-void
.end method

.method public static U(Ljava/lang/Object;)Lorg/spongycastle/asn1/b/a;
    .locals 2

    .prologue
    .line 23
    instance-of v0, p0, Lorg/spongycastle/asn1/b/a;

    if-eqz v0, :cond_0

    .line 25
    check-cast p0, Lorg/spongycastle/asn1/b/a;

    .line 32
    :goto_0
    return-object p0

    .line 27
    :cond_0
    if-eqz p0, :cond_1

    .line 29
    new-instance v0, Lorg/spongycastle/asn1/b/a;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->K(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/b/a;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    .line 32
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mC()Lorg/spongycastle/asn1/q;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 88
    iget-object v1, p0, Lorg/spongycastle/asn1/b/a;->Hl:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 90
    iget-object v1, p0, Lorg/spongycastle/asn1/b/a;->Hm:Lorg/spongycastle/asn1/d;

    if-eqz v1, :cond_0

    .line 92
    new-instance v1, Lorg/spongycastle/asn1/al;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/spongycastle/asn1/b/a;->Hm:Lorg/spongycastle/asn1/d;

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/asn1/al;-><init>(ILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 95
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/ah;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ah;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
