.class abstract Lorg/spongycastle/asn1/ca;
.super Ljava/io/InputStream;
.source "LimitedInputStream.java"


# instance fields
.field private FS:I

.field protected final _in:Ljava/io/InputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 15
    iput-object p1, p0, Lorg/spongycastle/asn1/ca;->_in:Ljava/io/InputStream;

    .line 16
    iput p2, p0, Lorg/spongycastle/asn1/ca;->FS:I

    .line 17
    return-void
.end method


# virtual methods
.method protected U(Z)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lorg/spongycastle/asn1/ca;->_in:Ljava/io/InputStream;

    instance-of v0, v0, Lorg/spongycastle/asn1/bx;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lorg/spongycastle/asn1/ca;->_in:Ljava/io/InputStream;

    check-cast v0, Lorg/spongycastle/asn1/bx;

    invoke-virtual {v0, p1}, Lorg/spongycastle/asn1/bx;->T(Z)V

    .line 31
    :cond_0
    return-void
.end method

.method getRemaining()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lorg/spongycastle/asn1/ca;->FS:I

    return v0
.end method
