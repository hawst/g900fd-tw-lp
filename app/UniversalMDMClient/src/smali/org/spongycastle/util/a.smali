.class public final Lorg/spongycastle/util/a;
.super Ljava/lang/Object;
.source "Arrays.java"


# direct methods
.method public static T([B)[B
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 350
    if-nez p0, :cond_0

    .line 352
    const/4 v0, 0x0

    .line 358
    :goto_0
    return-object v0

    .line 354
    :cond_0
    array-length v0, p0

    new-array v0, v0, [B

    .line 356
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public static a([C[C)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 49
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 59
    array-length v0, p0

    array-length v3, p1

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 64
    :goto_1
    array-length v3, p0

    if-eq v0, v3, :cond_2

    .line 66
    aget-char v3, p0, v0

    aget-char v4, p1, v0

    if-ne v3, v4, :cond_0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 72
    goto :goto_0
.end method

.method public static fill([BB)V
    .locals 2

    .prologue
    .line 236
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 238
    aput-byte p1, p0, v0

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_0
    return-void
.end method

.method public static fill([JJ)V
    .locals 3

    .prologue
    .line 246
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 248
    aput-wide p1, p0, v0

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_0
    return-void
.end method

.method public static fill([SS)V
    .locals 2

    .prologue
    .line 256
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 258
    aput-short p1, p0, v0

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_0
    return-void
.end method

.method public static h([B[B)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 79
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 84
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 89
    array-length v0, p0

    array-length v3, p1

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 94
    :goto_1
    array-length v3, p0

    if-eq v0, v3, :cond_2

    .line 96
    aget-byte v3, p0, v0

    aget-byte v4, p1, v0

    if-ne v3, v4, :cond_0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 102
    goto :goto_0
.end method

.method public static hashCode([B)I
    .locals 3

    .prologue
    .line 274
    if-nez p0, :cond_1

    .line 276
    const/4 v0, 0x0

    .line 288
    :cond_0
    return v0

    .line 279
    :cond_1
    array-length v1, p0

    .line 280
    add-int/lit8 v0, v1, 0x1

    .line 282
    :goto_0
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    .line 284
    mul-int/lit16 v0, v0, 0x101

    .line 285
    aget-byte v2, p0, v1

    xor-int/2addr v0, v2

    goto :goto_0
.end method

.method public static hashCode([C)I
    .locals 3

    .prologue
    .line 293
    if-nez p0, :cond_1

    .line 295
    const/4 v0, 0x0

    .line 307
    :cond_0
    return v0

    .line 298
    :cond_1
    array-length v1, p0

    .line 299
    add-int/lit8 v0, v1, 0x1

    .line 301
    :goto_0
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    .line 303
    mul-int/lit16 v0, v0, 0x101

    .line 304
    aget-char v2, p0, v1

    xor-int/2addr v0, v2

    goto :goto_0
.end method

.method public static i([B[B)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 117
    if-ne p0, p1, :cond_1

    move v1, v3

    .line 139
    :cond_0
    :goto_0
    return v1

    .line 122
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 127
    array-length v0, p0

    array-length v2, p1

    if-ne v0, v2, :cond_0

    move v0, v1

    move v2, v1

    .line 134
    :goto_1
    array-length v4, p0

    if-eq v0, v4, :cond_2

    .line 136
    aget-byte v4, p0, v0

    aget-byte v5, p1, v0

    xor-int/2addr v4, v5

    or-int/2addr v2, v4

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 139
    :cond_2
    if-nez v2, :cond_0

    move v1, v3

    goto :goto_0
.end method

.method public static i([I)[I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 363
    if-nez p0, :cond_0

    .line 365
    const/4 v0, 0x0

    .line 371
    :goto_0
    return-object v0

    .line 367
    :cond_0
    array-length v0, p0

    new-array v0, v0, [I

    .line 369
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method
