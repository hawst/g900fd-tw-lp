.class public Lcom/msc/sa/aidl/SAccountSevice;
.super Ljava/lang/Object;
.source "SAccountSevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/msc/sa/aidl/SAccountSevice$SACallback;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field filename:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mISaService:Lcom/msc/sa/aidl/ISAService;

.field private mRegistertCode:Ljava/lang/String;

.field private mRequestID:I

.field private mSACallback:Lcom/msc/sa/aidl/SAccountSevice$SACallback;

.field mServiceConnection:Landroid/content/ServiceConnection;

.field private mcallback:Landroid/content/ISAccountCallback;

.field private sa_password:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;

    .line 32
    iput-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mSACallback:Lcom/msc/sa/aidl/SAccountSevice$SACallback;

    .line 34
    iput-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->sa_password:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mRegistertCode:Ljava/lang/String;

    .line 44
    const-string v0, "SAccountSevice"

    iput-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->TAG:Ljava/lang/String;

    .line 120
    new-instance v0, Lcom/msc/sa/aidl/SAccountSevice$1;

    invoke-direct {v0, p0}, Lcom/msc/sa/aidl/SAccountSevice$1;-><init>(Lcom/msc/sa/aidl/SAccountSevice;)V

    iput-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 245
    const-string v0, "BridgeServiceLog2.txt"

    iput-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->filename:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/msc/sa/aidl/SAccountSevice;)Lcom/msc/sa/aidl/ISAService;
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-object v0
.end method

.method static synthetic access$102(Lcom/msc/sa/aidl/SAccountSevice;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;
    .locals 0
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;
    .param p1, "x1"    # Lcom/msc/sa/aidl/ISAService;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/msc/sa/aidl/SAccountSevice;)Lcom/msc/sa/aidl/SAccountSevice$SACallback;
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mSACallback:Lcom/msc/sa/aidl/SAccountSevice$SACallback;

    return-object v0
.end method

.method static synthetic access$202(Lcom/msc/sa/aidl/SAccountSevice;Lcom/msc/sa/aidl/SAccountSevice$SACallback;)Lcom/msc/sa/aidl/SAccountSevice$SACallback;
    .locals 0
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;
    .param p1, "x1"    # Lcom/msc/sa/aidl/SAccountSevice$SACallback;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mSACallback:Lcom/msc/sa/aidl/SAccountSevice$SACallback;

    return-object p1
.end method

.method static synthetic access$300(Lcom/msc/sa/aidl/SAccountSevice;)I
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mRequestID:I

    return v0
.end method

.method static synthetic access$302(Lcom/msc/sa/aidl/SAccountSevice;I)I
    .locals 0
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;
    .param p1, "x1"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mRequestID:I

    return p1
.end method

.method static synthetic access$400(Lcom/msc/sa/aidl/SAccountSevice;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mRegistertCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/msc/sa/aidl/SAccountSevice;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mRegistertCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->sa_password:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/msc/sa/aidl/SAccountSevice;)Landroid/content/ISAccountCallback;
    .locals 1
    .param p0, "x0"    # Lcom/msc/sa/aidl/SAccountSevice;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    return-object v0
.end method


# virtual methods
.method public checkSAccount(Ljava/lang/String;Landroid/content/Context;Landroid/content/ISAccountCallback;)V
    .locals 5
    .param p1, "sa_id"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "callback"    # Landroid/content/ISAccountCallback;

    .prologue
    .line 48
    iput-object p3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    .line 50
    iput-object p2, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    .line 52
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 54
    .local v2, "manager":Landroid/accounts/AccountManager;
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 58
    .local v0, "accountArr":[Landroid/accounts/Account;
    :try_start_0
    array-length v3, v0

    if-lez v3, :cond_1

    .line 60
    const/4 v3, 0x0

    aget-object v3, v0, v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/content/ISAccountCallback;->sAccountCallback(Z)V

    .line 82
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/content/ISAccountCallback;->sAccountCallback(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 72
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/content/ISAccountCallback;->sAccountCallback(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public checkSAccountPassword(Ljava/lang/String;Landroid/content/Context;Landroid/content/ISAccountCallback;)V
    .locals 7
    .param p1, "sa_password"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "callback"    # Landroid/content/ISAccountCallback;

    .prologue
    .line 87
    iput-object p3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    .line 89
    iput-object p2, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    .line 91
    iput-object p1, p0, Lcom/msc/sa/aidl/SAccountSevice;->sa_password:Ljava/lang/String;

    .line 93
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 95
    .local v2, "manager":Landroid/accounts/AccountManager;
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 99
    .local v0, "accountArr":[Landroid/accounts/Account;
    :try_start_0
    array-length v3, v0

    if-lez v3, :cond_0

    .line 101
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 118
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/content/ISAccountCallback;->sAccountCallback(Z)V

    .line 108
    iget-object v3, p0, Lcom/msc/sa/aidl/SAccountSevice;->TAG:Ljava/lang/String;

    const-string v4, "NO account"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public destory()V
    .locals 3

    .prologue
    .line 227
    :try_start_0
    iget-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;

    iget-object v2, p0, Lcom/msc/sa/aidl/SAccountSevice;->mRegistertCode:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z

    .line 229
    iget-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/msc/sa/aidl/SAccountSevice;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 231
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 233
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
