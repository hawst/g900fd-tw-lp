.class Lcom/msc/sa/aidl/SAccountSevice$1;
.super Ljava/lang/Object;
.source "SAccountSevice.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/msc/sa/aidl/SAccountSevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/msc/sa/aidl/SAccountSevice;


# direct methods
.method constructor <init>(Lcom/msc/sa/aidl/SAccountSevice;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 125
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->TAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/msc/sa/aidl/SAccountSevice;->access$000(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "onServiceConnected"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v6

    # setter for: Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v5, v6}, Lcom/msc/sa/aidl/SAccountSevice;->access$102(Lcom/msc/sa/aidl/SAccountSevice;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;

    .line 129
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    new-instance v6, Lcom/msc/sa/aidl/SAccountSevice$SACallback;

    iget-object v7, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    invoke-direct {v6, v7}, Lcom/msc/sa/aidl/SAccountSevice$SACallback;-><init>(Lcom/msc/sa/aidl/SAccountSevice;)V

    # setter for: Lcom/msc/sa/aidl/SAccountSevice;->mSACallback:Lcom/msc/sa/aidl/SAccountSevice$SACallback;
    invoke-static {v5, v6}, Lcom/msc/sa/aidl/SAccountSevice;->access$202(Lcom/msc/sa/aidl/SAccountSevice;Lcom/msc/sa/aidl/SAccountSevice$SACallback;)Lcom/msc/sa/aidl/SAccountSevice$SACallback;

    .line 131
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    const/16 v6, 0x64

    # setter for: Lcom/msc/sa/aidl/SAccountSevice;->mRequestID:I
    invoke-static {v5, v6}, Lcom/msc/sa/aidl/SAccountSevice;->access$302(Lcom/msc/sa/aidl/SAccountSevice;I)I

    .line 133
    const-string v1, "9g3975zg63"

    .line 135
    .local v1, "client_id":Ljava/lang/String;
    const-string v2, "99004120A4150A4555E45E878846957A"

    .line 137
    .local v2, "client_secret":Ljava/lang/String;
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/msc/sa/aidl/SAccountSevice;->access$400(Lcom/msc/sa/aidl/SAccountSevice;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 141
    .local v4, "packageName":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    iget-object v6, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v6}, Lcom/msc/sa/aidl/SAccountSevice;->access$100(Lcom/msc/sa/aidl/SAccountSevice;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v6

    iget-object v7, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mSACallback:Lcom/msc/sa/aidl/SAccountSevice$SACallback;
    invoke-static {v7}, Lcom/msc/sa/aidl/SAccountSevice;->access$200(Lcom/msc/sa/aidl/SAccountSevice;)Lcom/msc/sa/aidl/SAccountSevice$SACallback;

    move-result-object v7

    invoke-interface {v6, v1, v2, v4, v7}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/msc/sa/aidl/SAccountSevice;->mRegistertCode:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/msc/sa/aidl/SAccountSevice;->access$502(Lcom/msc/sa/aidl/SAccountSevice;Ljava/lang/String;)Ljava/lang/String;

    .line 144
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 146
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "password"

    iget-object v6, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->sa_password:Ljava/lang/String;
    invoke-static {v6}, Lcom/msc/sa/aidl/SAccountSevice;->access$600(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v5}, Lcom/msc/sa/aidl/SAccountSevice;->access$100(Lcom/msc/sa/aidl/SAccountSevice;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v5

    iget-object v6, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mRequestID:I
    invoke-static {v6}, Lcom/msc/sa/aidl/SAccountSevice;->access$300(Lcom/msc/sa/aidl/SAccountSevice;)I

    move-result v6

    iget-object v7, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mRegistertCode:Ljava/lang/String;
    invoke-static {v7}, Lcom/msc/sa/aidl/SAccountSevice;->access$500(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7, v0}, Lcom/msc/sa/aidl/ISAService;->requestPasswordConfirmation(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v3

    .line 152
    .local v3, "e":Landroid/os/RemoteException;
    iget-object v5, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->TAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/msc/sa/aidl/SAccountSevice;->access$000(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onServiceDisconnected "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    const/4 v1, 0x0

    # setter for: Lcom/msc/sa/aidl/SAccountSevice;->mISaService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v0, v1}, Lcom/msc/sa/aidl/SAccountSevice;->access$102(Lcom/msc/sa/aidl/SAccountSevice;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;

    .line 163
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice$1;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/msc/sa/aidl/SAccountSevice;->access$000(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-void
.end method
