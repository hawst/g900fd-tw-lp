.class public Lcom/msc/sa/aidl/SAccountSevice$SACallback;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "SAccountSevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/msc/sa/aidl/SAccountSevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SACallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/msc/sa/aidl/SAccountSevice;


# direct methods
.method public constructor <init>(Lcom/msc/sa/aidl/SAccountSevice;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/msc/sa/aidl/SAccountSevice$SACallback;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 175
    return-void
.end method

.method public onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 193
    return-void
.end method

.method public onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 181
    return-void
.end method

.method public onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 187
    return-void
.end method

.method public onReceivePasswordConfirmation(IZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 205
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice$SACallback;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/msc/sa/aidl/SAccountSevice;->access$000(Lcom/msc/sa/aidl/SAccountSevice;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "packageName"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    if-ne p2, v3, :cond_0

    .line 209
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice$SACallback;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;
    invoke-static {v0}, Lcom/msc/sa/aidl/SAccountSevice;->access$700(Lcom/msc/sa/aidl/SAccountSevice;)Landroid/content/ISAccountCallback;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/content/ISAccountCallback;->sAccountCallback(Z)V

    .line 217
    :goto_0
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice$SACallback;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    invoke-virtual {v0}, Lcom/msc/sa/aidl/SAccountSevice;->destory()V

    .line 219
    return-void

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/msc/sa/aidl/SAccountSevice$SACallback;->this$0:Lcom/msc/sa/aidl/SAccountSevice;

    # getter for: Lcom/msc/sa/aidl/SAccountSevice;->mcallback:Landroid/content/ISAccountCallback;
    invoke-static {v0}, Lcom/msc/sa/aidl/SAccountSevice;->access$700(Lcom/msc/sa/aidl/SAccountSevice;)Landroid/content/ISAccountCallback;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/content/ISAccountCallback;->sAccountCallback(Z)V

    goto :goto_0
.end method

.method public onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 199
    return-void
.end method
