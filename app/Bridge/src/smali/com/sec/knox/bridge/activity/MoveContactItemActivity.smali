.class public Lcom/sec/knox/bridge/activity/MoveContactItemActivity;
.super Landroid/app/Activity;
.source "MoveContactItemActivity.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mAction:Ljava/lang/String;

.field private mContact_ids:[J

.field private mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

.field private mInputBundle:Landroid/os/Bundle;

.field private mKnoxIdNamePair:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKnoxInfoForApp:Landroid/os/Bundle;

.field private mLaunchAfterLockCheck:Z

.field private mMoveToDstCId:I

.field private mMoveToSrcCId:I

.field private mPersonaManager:Landroid/os/PersonaManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    const-string v0, "MoveContactItemActivity"

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mLaunchAfterLockCheck:Z

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveContactItemActivity;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveContactItemActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveContactItemActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->moveToContact(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private checkLockState()Z
    .locals 5

    .prologue
    .line 211
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mPersonaManager:Landroid/os/PersonaManager;

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/pm/PersonaState;->toString()Ljava/lang/String;

    move-result-object v1

    .line 212
    .local v1, "state":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Persona state = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mPersonaManager:Landroid/os/PersonaManager;

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v2

    sget-object v3, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 215
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "Persona is locked while moving contact to KNOX"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    new-instance v0, Landroid/content/Intent;

    const-string v2, "action.sec.knox.moveto"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 217
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.knox.bridge"

    const-string v3, "com.sec.knox.bridge.handlers.MoveToHandler"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    const-string v2, "actions"

    const-string v3, "movetoContact"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mInputBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 222
    const-string v2, "moveToDstCId"

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 224
    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 226
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->finish()V

    .line 228
    const/4 v2, 0x1

    .line 231
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v2

    .line 230
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "Persona isn\'t locked"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private moveToContact(Z)V
    .locals 7
    .param p1, "toKnox"    # Z

    .prologue
    .line 236
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "moveToContact() action = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mAction:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; mLaunchAfterLockCheck = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mLaunchAfterLockCheck:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; mMoveToDstCId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mPersonaManager:Landroid/os/PersonaManager;

    iget v5, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v4, v5}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v3

    .line 241
    .local v3, "personaInfo":Landroid/content/pm/PersonaInfo;
    const/4 v2, 0x0

    .line 242
    .local v2, "isSdpEnabled":Z
    if-eqz v3, :cond_0

    .line 243
    iget-boolean v2, v3, Landroid/content/pm/PersonaInfo;->sdpEnabled:Z

    .line 244
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sdpEnabled = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :goto_0
    sget-boolean v4, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isUseLockStateCheck:Z

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    .line 250
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->checkLockState()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 271
    :goto_1
    return-void

    .line 246
    :cond_0
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v5, "PersonaInfo is null"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_1
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v4, "action.com.sec.knox.container.exchangeData"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 258
    .local v1, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mInputBundle:Landroid/os/Bundle;

    invoke-virtual {v1, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 259
    const-string v4, "moveToDstCId"

    iget v5, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 260
    const-string v4, "launchFromPersonaManager"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 261
    const-string v4, "com.sec.knox.bridge"

    const-string v5, "com.sec.knox.bridge.operations.ExchangeDataReceiver"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    invoke-virtual {p0, v1}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 265
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 266
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v5, "exception has occured while broadcasting"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 269
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->finish()V

    goto :goto_1
.end method

.method private showDstKnoxSelectionDialog()Z
    .locals 21

    .prologue
    .line 119
    if-eqz p0, :cond_5

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 121
    .local v8, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 122
    .local v9, "knoxFirstId":I
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 123
    .local v11, "knoxSecondId":I
    const-string v17, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_1

    const-string v17, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 125
    .local v10, "knoxFirstName":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 127
    .local v12, "knoxSecondName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f030003

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    .line 130
    .local v16, "view":Landroid/view/View;
    const v17, 0x7f080009

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 131
    .local v4, "firstKnox":Landroid/widget/TextView;
    const v17, 0x7f08000c

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 133
    .local v13, "secondKnox":Landroid/widget/TextView;
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    sget-boolean v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v17, :cond_2

    .line 137
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->whiteThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 138
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->whiteThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 144
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 145
    .local v7, "iconDrawable":Landroid/graphics/drawable/Drawable;
    const v17, 0x7f080008

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 146
    .local v5, "firstKnoxImg":Landroid/widget/ImageView;
    const v17, 0x7f08000b

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    .line 147
    .local v14, "secondKnoxImg":Landroid/widget/ImageView;
    if-eqz v7, :cond_0

    .line 148
    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 149
    .local v6, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_0

    .line 150
    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 153
    .end local v6    # "iconBitmap":Landroid/graphics/Bitmap;
    :cond_0
    new-instance v17, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;-><init>(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;I)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    new-instance v17, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$2;-><init>(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;I)V

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToSrcCId:I

    move/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 176
    const v15, 0x7f060013

    .line 183
    .local v15, "titleId":I
    :goto_2
    sget-boolean v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v17, :cond_4

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "mDstKnoxSelectionDialog is light theme"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    new-instance v17, Landroid/app/AlertDialog$Builder;

    new-instance v18, Landroid/view/ContextThemeWrapper;

    const v19, 0x103012b

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct/range {v17 .. v18}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 195
    .local v3, "builder":Landroid/app/AlertDialog$Builder;
    :goto_3
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$3;-><init>(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;)V

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->show()V

    .line 204
    const/16 v17, 0x1

    .line 206
    .end local v3    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v4    # "firstKnox":Landroid/widget/TextView;
    .end local v5    # "firstKnoxImg":Landroid/widget/ImageView;
    .end local v8    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v9    # "knoxFirstId":I
    .end local v10    # "knoxFirstName":Ljava/lang/String;
    .end local v11    # "knoxSecondId":I
    .end local v12    # "knoxSecondName":Ljava/lang/String;
    .end local v13    # "secondKnox":Landroid/widget/TextView;
    .end local v14    # "secondKnoxImg":Landroid/widget/ImageView;
    .end local v15    # "titleId":I
    .end local v16    # "view":Landroid/view/View;
    :goto_4
    return v17

    .line 123
    .restart local v8    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v9    # "knoxFirstId":I
    .restart local v11    # "knoxSecondId":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v10, v17

    goto/16 :goto_0

    .line 140
    .restart local v4    # "firstKnox":Landroid/widget/TextView;
    .restart local v10    # "knoxFirstName":Ljava/lang/String;
    .restart local v12    # "knoxSecondName":Ljava/lang/String;
    .restart local v13    # "secondKnox":Landroid/widget/TextView;
    .restart local v16    # "view":Landroid/view/View;
    :cond_2
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->darkThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 141
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->darkThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 178
    .restart local v5    # "firstKnoxImg":Landroid/widget/ImageView;
    .restart local v14    # "secondKnoxImg":Landroid/widget/ImageView;
    :cond_3
    const v15, 0x7f060014

    .restart local v15    # "titleId":I
    goto :goto_2

    .line 189
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "mDstKnoxSelectionDialog is dark theme"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    new-instance v17, Landroid/app/AlertDialog$Builder;

    new-instance v18, Landroid/view/ContextThemeWrapper;

    const v19, 0x1030128

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct/range {v17 .. v18}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .restart local v3    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_3

    .line 206
    .end local v3    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v4    # "firstKnox":Landroid/widget/TextView;
    .end local v5    # "firstKnoxImg":Landroid/widget/ImageView;
    .end local v8    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v9    # "knoxFirstId":I
    .end local v10    # "knoxFirstName":Ljava/lang/String;
    .end local v11    # "knoxSecondId":I
    .end local v12    # "knoxSecondName":Ljava/lang/String;
    .end local v13    # "secondKnox":Landroid/widget/TextView;
    .end local v14    # "secondKnoxImg":Landroid/widget/ImageView;
    .end local v15    # "titleId":I
    .end local v16    # "view":Landroid/view/View;
    :cond_5
    const/16 v17, 0x0

    goto :goto_4
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 275
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 276
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 59
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mInputBundle:Landroid/os/Bundle;

    .line 60
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mInputBundle:Landroid/os/Bundle;

    const-string v3, "action"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mAction:Ljava/lang/String;

    .line 61
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mInputBundle:Landroid/os/Bundle;

    const-string v3, "contact_ids"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mContact_ids:[J

    .line 62
    const-string v2, "launchAfterLockCheck"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mLaunchAfterLockCheck:Z

    .line 63
    const-string v2, "moveToDstCId"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    .line 66
    const-string v2, "persona"

    invoke-virtual {p0, v2}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mPersonaManager:Landroid/os/PersonaManager;

    .line 68
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mContact_ids:[J

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mContact_ids:[J

    array-length v2, v2

    if-nez v2, :cond_1

    .line 69
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MoveTo Contact failed, no contact ids received"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->finish()V

    .line 116
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate() action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mAction:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mLaunchAfterLockCheck = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mLaunchAfterLockCheck:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mMoveToDstCId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 80
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "Move to Contact after unlock"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0, v6}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->moveToContact(Z)V

    goto :goto_0

    .line 84
    :cond_2
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "Normal case"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    iput v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToSrcCId:I

    .line 87
    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToSrcCId:I

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 88
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "Move to Personal mode"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    iput v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    .line 91
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Move to Personal mMoveToSrcCId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToSrcCId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mMoveToDstCId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0, v5}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->moveToContact(Z)V

    goto/16 :goto_0

    .line 96
    :cond_3
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "Move to KNOX"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    .line 98
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    const-string v3, "KnoxIdNamePair"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    .line 100
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate() - size of KnoxIdNamePair is ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 103
    .local v0, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ne v2, v6, :cond_5

    .line 104
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 105
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I

    goto :goto_1

    .line 107
    :cond_4
    invoke-direct {p0, v6}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->moveToContact(Z)V

    goto/16 :goto_0

    .line 108
    :cond_5
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 109
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->showDstKnoxSelectionDialog()Z

    goto/16 :goto_0

    .line 111
    :cond_6
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v3, "Can\'t reach here. Do nothing"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->finish()V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 293
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 294
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 299
    :cond_0
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    .line 302
    :cond_1
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mAction:Ljava/lang/String;

    .line 303
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mContact_ids:[J

    .line 304
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mInputBundle:Landroid/os/Bundle;

    .line 305
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    .line 306
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    .line 307
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mPersonaManager:Landroid/os/PersonaManager;

    .line 308
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 287
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 288
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 281
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 282
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method
