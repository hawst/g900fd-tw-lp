.class public Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;
.super Ljava/lang/Object;
.source "UploadKnoxUsageModule.java"


# static fields
.field private static uploadInProgress:Z


# instance fields
.field private isFirstUpload:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->uploadInProgress:Z

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isFirstUpload"    # Z

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->isFirstUpload:Z

    .line 43
    iput-boolean p1, p0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->isFirstUpload:Z

    .line 44
    return-void
.end method

.method static synthetic access$000(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 25
    invoke-static {p0}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->setUploadInProgress(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->isFirstUpload:Z

    return v0
.end method

.method public static isUploadInProgress()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->uploadInProgress:Z

    return v0
.end method

.method private static setUploadInProgress(Z)V
    .locals 3
    .param p0, "uploadInProgress"    # Z

    .prologue
    .line 38
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUploadInProgress :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    sput-boolean p0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->uploadInProgress:Z

    .line 40
    return-void
.end method


# virtual methods
.method public processUpload()V
    .locals 4

    .prologue
    .line 47
    invoke-static {}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->isUploadInProgress()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    const-string v2, "KnoxUsageLogPro"

    const-string v3, "processUpload : First Upload already in progress - just return"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-static {}, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->getInstance()Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;

    move-result-object v2

    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->isAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 51
    const-string v2, "KnoxUsageLogPro"

    const-string v3, "processUpload : Policy isAvailable: false - just return"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 54
    :cond_1
    new-instance v0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;

    invoke-direct {v0, p0}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;-><init>(Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;)V

    .line 178
    .local v0, "processKnoxUsagelUploadRunnable":Ljava/lang/Runnable;
    new-instance v1, Ljava/lang/Thread;

    const-string v2, "processUploadThread"

    invoke-direct {v1, v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 180
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
