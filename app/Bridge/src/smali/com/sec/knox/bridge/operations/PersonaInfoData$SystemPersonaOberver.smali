.class Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;
.super Landroid/content/pm/ISystemPersonaObserver$Stub;
.source "PersonaInfoData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/operations/PersonaInfoData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SystemPersonaOberver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/operations/PersonaInfoData;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/operations/PersonaInfoData;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;->this$0:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    invoke-direct {p0}, Landroid/content/pm/ISystemPersonaObserver$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/operations/PersonaInfoData;Lcom/sec/knox/bridge/operations/PersonaInfoData$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/operations/PersonaInfoData;
    .param p2, "x1"    # Lcom/sec/knox/bridge/operations/PersonaInfoData$1;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;-><init>(Lcom/sec/knox/bridge/operations/PersonaInfoData;)V

    return-void
.end method

.method private processOnContainerLaunchForKnoxUsage(I)V
    .locals 5
    .param p1, "personaId"    # I

    .prologue
    .line 123
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    const-string v2, "KnoxUsageLogPro"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SystemPersonaOberver : processOnContainerLaunchForKnoxUsage: UserHandle.USER_OWNER != "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " just return"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_0
    return-void

    .line 127
    :cond_0
    sget-object v2, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 128
    const-string v2, "KnoxUsageLogPro"

    const-string v3, "processOnContainerLaunchForKnoxUsage: mCtx is null, just return"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_1
    sget-object v2, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProcessible(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 132
    const-string v2, "KnoxUsageLogPro"

    const-string v3, "processOnContainerLaunchForKnoxUsage : Not Processible, just return"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_2
    sget-object v2, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    const-string v3, "persona"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 137
    .local v0, "mPm":Landroid/os/PersonaManager;
    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v2

    sget-object v3, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    if-ne v2, v3, :cond_3

    .line 138
    const-string v2, "KnoxUsageLogPro"

    const-string v3, "SystemPersonaOberver : processOnContainerLaunchForKnoxUsage: CurrentState:LOCKED No need to count this attempt just return"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_3
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    const-class v3, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 142
    .local v1, "serviceIntent":Landroid/content/Intent;
    const-string v2, "android.intent.extra.user_handle"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 143
    const-string v2, "com.samsung.knox.bridge.knoxusage.peronaLaunch"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    sget-object v2, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method


# virtual methods
.method public onKnoxContainerLaunch(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 108
    const-string v1, "PersonaInfoData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKnoxContainerLaunch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :try_start_0
    sget-object v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    if-eqz v1, :cond_0

    .line 111
    sget-object v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    iget-object v1, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mSimplePersonaInfo:Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    invoke-virtual {v1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->initialize()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :cond_0
    :goto_0
    const/16 v1, 0x64

    if-lt p1, v1, :cond_1

    .line 117
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;->processOnContainerLaunchForKnoxUsage(I)V

    .line 120
    :cond_1
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onPersonaActive(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 78
    const-string v1, "PersonaInfoData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPersonaActive "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :try_start_0
    sget-object v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    if-eqz v1, :cond_0

    .line 82
    sget-object v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    iget-object v1, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mSimplePersonaInfo:Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    invoke-virtual {v1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->initialize()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onRemovePersona(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 91
    const-string v1, "PersonaInfoData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onRemovePersona "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :try_start_0
    sget-object v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    if-eqz v1, :cond_0

    .line 95
    sget-object v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    iget-object v1, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mSimplePersonaInfo:Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    invoke-virtual {v1, p1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->removeItem(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onResetPersona(I)V
    .locals 3
    .param p1, "personaId"    # I

    .prologue
    .line 103
    const-string v0, "PersonaInfoData"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResetPersona "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public onStateChange(ILandroid/content/pm/PersonaState;Landroid/content/pm/PersonaState;)V
    .locals 3
    .param p1, "personaId"    # I
    .param p2, "oldState"    # Landroid/content/pm/PersonaState;
    .param p3, "newState"    # Landroid/content/pm/PersonaState;

    .prologue
    .line 149
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChange : oldState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/pm/PersonaState;->getKnox2_0State()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/content/pm/PersonaState;->getKnox2_0State()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v0, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    if-ne p2, v0, :cond_0

    sget-object v0, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    if-ne p3, v0, :cond_0

    .line 151
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onStateChange : LOCKED -> ACTIVE"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;->processOnContainerLaunchForKnoxUsage(I)V

    .line 155
    :cond_0
    return-void
.end method
