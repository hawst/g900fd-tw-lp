.class public Lcom/sec/knox/bridge/uploadmanager/HttpsManager;
.super Ljava/lang/Object;
.source "HttpsManager.java"


# static fields
.field protected static final hexArray:[C


# instance fields
.field private final KNOXUSAGE_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

.field private MAX_RETRY:I

.field private aesKey:Ljavax/crypto/SecretKey;

.field private conn:Ljava/net/HttpURLConnection;

.field private httpHost:Lorg/apache/http/HttpHost;

.field private isFirstUpload:Z

.field private final mRequestData:Lorg/json/JSONObject;

.field private mSecureRandom:Ljava/security/SecureRandom;

.field private final mServerURI:Ljava/lang/String;

.field msg:Landroid/os/Message;

.field private proxy:Ljava/net/Proxy;

.field private random48_array:[B

.field private secureDataGenerator:Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->hexArray:[C

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/json/JSONObject;Z)V
    .locals 2
    .param p1, "serverURI"    # Ljava/lang/String;
    .param p2, "requestData"    # Lorg/json/JSONObject;
    .param p3, "isFirstTime"    # Z

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-string v0, " Thread ID :  "

    iput-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->KNOXUSAGE_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->isFirstUpload:Z

    .line 55
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->msg:Landroid/os/Message;

    .line 56
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->aesKey:Ljavax/crypto/SecretKey;

    .line 61
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mSecureRandom:Ljava/security/SecureRandom;

    .line 62
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->secureDataGenerator:Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;

    .line 63
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    .line 64
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    .line 65
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    .line 66
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->MAX_RETRY:I

    .line 67
    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->random48_array:[B

    .line 71
    iput-object p1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mRequestData:Lorg/json/JSONObject;

    .line 73
    iput-boolean p3, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->isFirstUpload:Z

    .line 74
    return-void
.end method

.method public static bytesToHex([B)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B

    .prologue
    .line 155
    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    new-array v0, v3, [C

    .line 156
    .local v0, "hexChars":[C
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_0

    .line 157
    aget-byte v3, p0, v1

    and-int/lit16 v2, v3, 0xff

    .line 158
    .local v2, "v":I
    mul-int/lit8 v3, v1, 0x2

    sget-object v4, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->hexArray:[C

    ushr-int/lit8 v5, v2, 0x4

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 159
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    sget-object v4, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->hexArray:[C

    and-int/lit8 v5, v2, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 161
    .end local v2    # "v":I
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    return-object v3
.end method

.method private checkProxyEnable()V
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 93
    .local v0, "inetSocketAddress":Ljava/net/InetSocketAddress;
    const-string v1, "KnoxUsageLogPro"

    const-string v2, "HttpsManager : CheckProxyEnable"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Proxy;->getPreferredHttpHost(Landroid/content/Context;Ljava/lang/String;)Lorg/apache/http/HttpHost;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    .line 96
    iget-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    if-eqz v1, :cond_0

    .line 97
    new-instance v0, Ljava/net/InetSocketAddress;

    .end local v0    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    iget-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    invoke-virtual {v1}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 98
    .restart local v0    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    new-instance v1, Ljava/net/Proxy;

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    invoke-direct {v1, v2, v0}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    iput-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    .line 99
    const-string v1, "HttpsManager.run :  Thread ID :   Set the httpHost Proxy"

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 104
    :goto_0
    return-void

    .line 101
    :cond_0
    const-string v1, "HttpsManager.run :  Thread ID :   httpHost Proxy is NULL"

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doBackOff(I)V
    .locals 8
    .param p1, "numTry"    # I

    .prologue
    .line 330
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->MAX_RETRY:I

    if-le p1, v1, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    const-wide/16 v4, 0x3e8

    const/4 v1, 0x1

    rem-int/lit8 v6, p1, 0x3

    shl-int/2addr v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-long v6, v1

    mul-long v2, v4, v6

    .line 334
    .local v2, "ms":J
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doBackOff ["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "] sleep(ms)="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 335
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private getDecryptedBody(Ljava/lang/String;Ljava/security/Key;[B)Ljava/lang/String;
    .locals 5
    .param p1, "cipherText"    # Ljava/lang/String;
    .param p2, "originalKey"    # Ljava/security/Key;
    .param p3, "randomArray"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "DecryptedMessage":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->secureDataGenerator:Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;

    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    const/16 v3, 0x20

    const/16 v4, 0x10

    invoke-direct {v2, p3, v3, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([BII)V

    invoke-virtual {v1, p1, v2, p2}, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;->getDecryptedNetworkData(Ljava/lang/String;Ljavax/crypto/spec/IvParameterSpec;Ljava/security/Key;)Ljava/lang/String;

    move-result-object v0

    .line 148
    if-nez v0, :cond_0

    .line 149
    new-instance v1, Lorg/json/JSONException;

    const-string v2, "HttpsManager.run :  Thread ID :   DecryptedMessage is Null"

    invoke-direct {v1, v2}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 151
    :cond_0
    return-object v0
.end method

.method private getEncryptedAESKey(Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;[B)Ljava/lang/String;
    .locals 2
    .param p1, "mPUBSerialNumber"    # Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;
    .param p2, "test"    # [B

    .prologue
    .line 209
    iget-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->secureDataGenerator:Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;

    invoke-virtual {v1, p2, p1}, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;->getEncryptedAESSessionKey([BLcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;)Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "mResult":Ljava/lang/String;
    return-object v0
.end method

.method private getEncryptedBody(Ljava/security/Key;Ljava/lang/String;[B)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/security/Key;
    .param p2, "plainText"    # Ljava/lang/String;
    .param p3, "test"    # [B

    .prologue
    .line 196
    const/4 v1, 0x0

    .line 197
    .local v1, "encryptedMessage":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->secureDataGenerator:Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;

    invoke-virtual {v2, p2, p1, p3}, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;->getEncryptedNetworkData(Ljava/lang/String;Ljava/security/Key;[B)Ljava/lang/String;

    move-result-object v1

    .line 198
    if-nez v1, :cond_0

    .line 200
    :try_start_0
    new-instance v2, Lorg/json/JSONException;

    const-string v3, "HttpsManager.run :  Thread ID :   CompriseEncryptedMessage is Null"

    invoke-direct {v2, v3}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 205
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v1
.end method

.method private getPUDSerialNumber()Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, "mResult":Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProductShip(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    sget-object v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->PROD_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    .line 141
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpsManager.run :  Thread ID :   ALIAS : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->getAlias()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 142
    return-object v0

    .line 139
    :cond_0
    sget-object v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->QA_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    goto :goto_0
.end method

.method private getknoxUsageUploadResponse(ILorg/json/JSONObject;)Landroid/os/Message;
    .locals 6
    .param p1, "responseCode"    # I
    .param p2, "responseJSONData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 354
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 355
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 356
    .local v0, "data":Landroid/os/Bundle;
    const-string v4, "status"

    invoke-virtual {p2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "success"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 357
    const-string v4, "KnoxUsageLogPro"

    const-string v5, "getUploadResponseContainer : response success"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const-string v4, "next_upload"

    invoke-virtual {p2, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 359
    .local v2, "next_upload":J
    const-string v4, "next_upload"

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 360
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 365
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "next_upload":J
    :goto_0
    return-object v1

    .line 363
    .restart local v1    # "msg":Landroid/os/Message;
    :cond_0
    const-string v4, "KnoxUsageLogPro"

    const-string v5, "getUploadResponseContainer : response not success"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private networkEnableCheck()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/bridge/uploadmanager/NetworkManager;->isWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    const-string v0, "HttpsManager.run :  Thread ID :  Network is disabled"

    invoke-static {v0}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 87
    new-instance v0, Landroid/accounts/NetworkErrorException;

    const-string v1, "Network is disabled"

    invoke-direct {v0, v1}, Landroid/accounts/NetworkErrorException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    return-void
.end method

.method private networkResponseProcess(ILorg/json/JSONObject;)Landroid/os/Message;
    .locals 2
    .param p1, "responseCode"    # I
    .param p2, "responseJSONData"    # Lorg/json/JSONObject;

    .prologue
    .line 344
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->getknoxUsageUploadResponse(ILorg/json/JSONObject;)Landroid/os/Message;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 348
    :goto_0
    return-object v1

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 348
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private nullParameterCheck()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mRequestData:Lorg/json/JSONObject;

    if-nez v0, :cond_1

    .line 78
    :cond_0
    const-string v0, "HttpsManager.run :  Thread ID :   Null Paramter was input"

    invoke-static {v0}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 79
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Null Parameter"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    return-void
.end method

.method private readDataFromBufferedReader(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 3
    .param p1, "br"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    const-string v2, "HttpsManager.run :  Thread ID :  ReadDataFromBufferedReader()"

    invoke-static {v2}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 225
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 227
    .local v0, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 230
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private readResponseData(I)Lorg/json/JSONObject;
    .locals 8
    .param p1, "responseCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 234
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run :  Thread ID :  readResponseData("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 235
    const/4 v3, 0x0

    .line 236
    .local v3, "mResult":Lorg/json/JSONObject;
    const/4 v0, 0x0

    .line 239
    .local v0, "br":Ljava/io/BufferedReader;
    sparse-switch p1, :sswitch_data_0

    .line 246
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    move-object v0, v1

    .line 248
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->readDataFromBufferedReader(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->aesKey:Ljavax/crypto/SecretKey;

    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->random48_array:[B

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->getDecryptedBody(Ljava/lang/String;Ljava/security/Key;[B)Ljava/lang/String;

    move-result-object v4

    .line 249
    .local v4, "plainText":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.plainText : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 251
    new-instance v3, Lorg/json/JSONObject;

    .end local v3    # "mResult":Lorg/json/JSONObject;
    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    .restart local v3    # "mResult":Lorg/json/JSONObject;
    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 263
    :cond_0
    return-object v3

    .line 243
    .end local v4    # "plainText":Ljava/lang/String;
    :sswitch_0
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v0, v1

    .line 244
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 252
    .end local v3    # "mResult":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 253
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 259
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    :cond_1
    throw v5

    .line 254
    :catch_1
    move-exception v2

    .line 255
    .local v2, "e":Ljava/lang/NullPointerException;
    :try_start_3
    throw v2

    .line 256
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v2

    .line 257
    .local v2, "e":Lorg/json/JSONException;
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 239
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x12d -> :sswitch_0
        0x12e -> :sswitch_0
    .end sparse-switch
.end method

.method private receiverNetworkResponseCode()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    iget-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 218
    .local v0, "mResult":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpsManager.run :  Thread ID :  ResponseCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 219
    return v0
.end method

.method private sendNetworkRequestPacket()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 167
    const-string v5, "KnoxUsageLogPro"

    const-string v6, "HttpsManager.run :  Thread ID :   SendNetworkRequestPacket()"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->getPUDSerialNumber()Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    move-result-object v1

    .line 169
    .local v1, "XSN":Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mSecureRandom:Ljava/security/SecureRandom;

    const/16 v6, 0x30

    invoke-virtual {v5, v6}, Ljava/security/SecureRandom;->generateSeed(I)[B

    move-result-object v5

    iput-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->random48_array:[B

    .line 170
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->random48_array:[B

    invoke-static {v5}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->bytesToHex([B)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, "msk":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v6, "X-SS"

    const-string v7, "rna1.2"

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v6, "X-SN"

    invoke-virtual {v1}, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->getSerialNumebr()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v6, "MSK"

    invoke-virtual {v5, v6, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v6, "SK"

    const/16 v7, 0x3f

    invoke-virtual {v2, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v6, "IV"

    const/16 v7, 0x40

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v6, "X-SK"

    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->random48_array:[B

    invoke-direct {p0, v1, v7}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->getEncryptedAESKey(Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;[B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v6, "Content-Type"

    const-string v7, "application/knox-crypto-stream"

    invoke-virtual {v5, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const/16 v6, 0x2710

    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 179
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5, v9}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 181
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 184
    new-instance v5, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v6, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->random48_array:[B

    const/16 v7, 0x20

    const-string v8, "AES"

    invoke-direct {v5, v6, v9, v7, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->aesKey:Ljavax/crypto/SecretKey;

    .line 185
    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->aesKey:Ljavax/crypto/SecretKey;

    iget-object v6, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mRequestData:Lorg/json/JSONObject;

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->random48_array:[B

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->getEncryptedBody(Ljava/security/Key;Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "EncryptedBody":Ljava/lang/String;
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {v3, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 187
    .local v3, "os":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 188
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 190
    new-instance v4, Ljava/io/DataOutputStream;

    iget-object v5, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 191
    .local v4, "wr":Ljava/io/DataOutputStream;
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->write([B)V

    .line 192
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    .line 193
    return-void
.end method

.method private setNetworkConnection(Ljava/net/URL;)V
    .locals 3
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 110
    const-string v1, "KnoxUsageLogPro"

    const-string v2, "HttpsManager.run :  Thread ID :   setNetworkConnection()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    invoke-static {}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->trustCheckHosts()V

    .line 117
    iget-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    if-nez v1, :cond_0

    .line 118
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .line 126
    .local v0, "https":Ljavax/net/ssl/HttpsURLConnection;
    :goto_0
    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 127
    iput-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    .line 132
    return-void

    .line 120
    .end local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    invoke-virtual {p1, v1}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .restart local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    goto :goto_0

    .line 129
    .end local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    :cond_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "URL is not a HTTPS Host."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static trustCheckHosts()V
    .locals 3

    .prologue
    .line 39
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 41
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 43
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 44
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpsManager.run : trustCheckHosts has Exception() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 46
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public execute()V
    .locals 10

    .prologue
    .line 268
    const/4 v0, 0x1

    .line 269
    .local v0, "doRetry":Z
    const-string v7, "KnoxUsageLogPro"

    const-string v8, "HttpsManager : Execute"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->MAX_RETRY:I

    if-ge v2, v7, :cond_0

    .line 271
    if-nez v0, :cond_1

    .line 327
    :cond_0
    :goto_1
    return-void

    .line 273
    :cond_1
    const-string v7, "KnoxUsageLogPro"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HttpsManager : try:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :try_start_0
    new-instance v7, Ljava/security/SecureRandom;

    invoke-direct {v7}, Ljava/security/SecureRandom;-><init>()V

    iput-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mSecureRandom:Ljava/security/SecureRandom;

    .line 276
    new-instance v7, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;

    invoke-direct {v7}, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;-><init>()V

    iput-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->secureDataGenerator:Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;

    .line 278
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->nullParameterCheck()V

    .line 279
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->networkEnableCheck()V

    .line 281
    new-instance v6, Ljava/net/URL;

    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 282
    .local v6, "url":Ljava/net/URL;
    const-string v7, "HttpsManager.run : START Thread ID :  States = "

    invoke-static {v7}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 283
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HttpsManager.run :  Thread ID :  Server URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 284
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->checkProxyEnable()V

    .line 285
    invoke-direct {p0, v6}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->setNetworkConnection(Ljava/net/URL;)V

    .line 287
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v7, :cond_7

    .line 288
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->sendNetworkRequestPacket()V

    .line 289
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->receiverNetworkResponseCode()I

    move-result v4

    .line 290
    .local v4, "responseCode":I
    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->readResponseData(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 291
    .local v5, "responseJSONData":Lorg/json/JSONObject;
    invoke-direct {p0, v4, v5}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->networkResponseProcess(ILorg/json/JSONObject;)Landroid/os/Message;

    move-result-object v3

    .line 292
    .local v3, "msg":Landroid/os/Message;
    if-eqz v3, :cond_2

    .line 293
    iget-boolean v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->isFirstUpload:Z

    if-eqz v7, :cond_3

    .line 294
    const/4 v7, 0x1

    iput v7, v3, Landroid/os/Message;->what:I

    .line 295
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 300
    :goto_2
    const-string v7, "KnoxUsageLogPro"

    const-string v8, "msg is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HttpsManager.run :  Thread ID :  General Exception was occurred  restry count"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 321
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v7, :cond_0

    .line 322
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_1

    .line 297
    :cond_3
    const/4 v7, 0x2

    :try_start_1
    iput v7, v3, Landroid/os/Message;->what:I

    .line 298
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 305
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "responseCode":I
    .end local v5    # "responseJSONData":Lorg/json/JSONObject;
    .end local v6    # "url":Ljava/net/URL;
    :catch_0
    move-exception v1

    .line 306
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    instance-of v7, v1, Landroid/accounts/NetworkErrorException;

    if-nez v7, :cond_4

    instance-of v7, v1, Ljava/security/InvalidParameterException;

    if-nez v7, :cond_4

    instance-of v7, v1, Ljava/lang/NullPointerException;

    if-nez v7, :cond_4

    instance-of v7, v1, Lorg/json/JSONException;

    if-eqz v7, :cond_8

    .line 308
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HttpsManager.run :  Thread ID :  General Exception was occurred \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 309
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 310
    const/4 v0, 0x0

    .line 320
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HttpsManager.run :  Thread ID :  General Exception was occurred  restry count"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 321
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v7, :cond_6

    .line 322
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 270
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 320
    .restart local v6    # "url":Ljava/net/URL;
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HttpsManager.run :  Thread ID :  General Exception was occurred  restry count"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 321
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v7, :cond_6

    .line 322
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_3

    .line 313
    .end local v6    # "url":Ljava/net/URL;
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_8
    :try_start_3
    iget v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->MAX_RETRY:I

    add-int/lit8 v7, v7, -0x1

    if-ge v2, v7, :cond_5

    .line 314
    const/4 v0, 0x1

    .line 315
    invoke-direct {p0, v2}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->doBackOff(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 320
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HttpsManager.run :  Thread ID :  General Exception was occurred  restry count"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 321
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v7, :cond_6

    .line 322
    iget-object v7, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_3

    .line 320
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HttpsManager.run :  Thread ID :  General Exception was occurred  restry count"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 321
    iget-object v8, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v8, :cond_9

    .line 322
    iget-object v8, p0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_9
    throw v7
.end method
