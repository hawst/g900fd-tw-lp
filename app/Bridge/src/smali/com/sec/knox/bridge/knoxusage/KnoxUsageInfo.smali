.class public Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
.super Ljava/lang/Object;
.source "KnoxUsageInfo.java"


# instance fields
.field private containerId:I

.field private containerStatus:I

.field private entranceCount:I

.field private startOfWeek:J

.field private uploadTime:J


# direct methods
.method public constructor <init>(IIIJJ)V
    .locals 0
    .param p1, "containerId"    # I
    .param p2, "containerStatus"    # I
    .param p3, "entrance_count"    # I
    .param p4, "startOfWeek"    # J
    .param p6, "upload_time"    # J

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->containerId:I

    .line 15
    iput p2, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->containerStatus:I

    .line 16
    iput p3, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->entranceCount:I

    .line 17
    iput-wide p4, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->startOfWeek:J

    .line 18
    iput-wide p6, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->uploadTime:J

    .line 19
    return-void
.end method


# virtual methods
.method public getContainerId()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->containerId:I

    return v0
.end method

.method public getEntranceCount()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->entranceCount:I

    return v0
.end method

.method public getStartOfWeek()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->startOfWeek:J

    return-wide v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->containerStatus:I

    return v0
.end method

.method public getUploadTime()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->uploadTime:J

    return-wide v0
.end method
