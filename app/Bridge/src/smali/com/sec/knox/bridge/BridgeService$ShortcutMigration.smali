.class public Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;
.super Landroid/content/BroadcastReceiver;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ShortcutMigration"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 2799
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2802
    const-string v2, "com.sec.knox.action.SHORTCUT_MIGRATION_FOR_2_3_0"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2803
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, " MIGRATE SHORTCUTS "

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2804
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2805
    .local v1, "service":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mIBridgeProvider:Landroid/content/IProviderCallBack;

    check-cast v2, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;

    const-string v3, "RemoteShortcuts"

    # invokes: Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    invoke-static {v2, v3}, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->access$3000(Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 2807
    .local v0, "cn":Landroid/content/ComponentName;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2808
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2809
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, " STARTING SERVICE "

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2810
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-virtual {v2, v1}, Lcom/sec/knox/bridge/BridgeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2812
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "service":Landroid/content/Intent;
    :cond_0
    return-void
.end method
