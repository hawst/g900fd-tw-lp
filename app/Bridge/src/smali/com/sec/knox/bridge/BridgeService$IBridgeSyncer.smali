.class Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;
.super Landroid/content/ISyncCallBack$Stub;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IBridgeSyncer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 1173
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/content/ISyncCallBack$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p2, "x1"    # Lcom/sec/knox/bridge/BridgeService$1;

    .prologue
    .line 1173
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    return-void
.end method

.method private deleteShortcutsForUser(I)V
    .locals 7
    .param p1, "userId"    # I

    .prologue
    .line 1264
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "deleteShortcutsForUser()"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    const-string v4, "RemoteShortcuts"

    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 1268
    .local v1, "cn":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 1269
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ProviderNames MATCH, we will now sync up"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, " SyncToAsyncService I am waiting now"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1272
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1273
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1275
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1276
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "userId"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1277
    const-string v4, "createOrRemove"

    const-string v5, "removeAllForUser"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1280
    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-virtual {v4, v3}, Lcom/sec/knox/bridge/BridgeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1287
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 1281
    :catch_0
    move-exception v2

    .line 1283
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 11
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 1291
    iget-object v8, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const-string v9, "PROVIDER_PREFS"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/knox/bridge/BridgeService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1293
    .local v6, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    .line 1295
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 1296
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1297
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1298
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1299
    .local v5, "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {p1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1300
    new-instance v7, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v7, v9, v8}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1301
    .local v7, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    iget-object v1, v7, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1302
    .local v1, "fullServiceName":Ljava/lang/String;
    iget-object v4, v7, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 1303
    .local v4, "packageName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " Found fullServiceName: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for providerName: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; packageName="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v4, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    .end local v1    # "fullServiceName":Ljava/lang/String;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :goto_0
    return-object v0

    .line 1311
    :cond_1
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot find service name for providerName: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public allClipsCleared(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1375
    .local p1, "clearedClipsArray":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v6, "allClipsCleared"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v6}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1376
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, "IBridgeSyncer - allClipsCleared()"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1377
    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v6, v6, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    const-string v7, "SYNCER_PREFS"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 1379
    .local v5, "syncerPreferences":Landroid/content/SharedPreferences;
    const-string v6, "Clipboard"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1380
    .local v4, "syncService":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 1381
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, "IBridgeSyncer - allClipsCleared(): Bridge Prefs does NOT contain a syncService for Clipboard"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1396
    .end local p1    # "clearedClipsArray":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 1385
    .restart local p1    # "clearedClipsArray":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    new-instance v3, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {v3, v6, v4}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1386
    .local v3, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    iget-object v4, v3, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1387
    iget-object v2, v3, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 1388
    .local v2, "packageName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IBridgeSyncer - allClipsCleared(): package = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and syncService = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1391
    .local v1, "i":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v2, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    .local v0, "cn":Landroid/content/ComponentName;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1393
    const-string v6, "dowhat"

    const-string v7, "CLIPS_CLEARED"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1394
    const-string v6, "ClearedClipsArray"

    check-cast p1, Ljava/util/ArrayList;

    .end local p1    # "clearedClipsArray":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v1, v6, p1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1395
    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v6, v6, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->onStartCommand(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public deletePersonaData(I)V
    .locals 16
    .param p1, "personaID"    # I

    .prologue
    .line 1216
    const-string v13, "deletePersonaData"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v13}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1217
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v13

    const-string v14, " IBridgeSyncer - deletePersonaData() "

    invoke-static {v13, v14}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v13, v13, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    const-string v14, "SYNCER_PREFS"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 1221
    .local v12, "syncerPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v12}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v7

    .line 1222
    .local v7, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_1

    .line 1223
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1224
    .local v5, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1225
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 1227
    .local v9, "pairs":Ljava/util/Map$Entry;
    if-eqz v9, :cond_0

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_0

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 1228
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1230
    .local v11, "syncerName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " deletePersonaDataOfAllSyncers pairs.getKey() : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 1234
    .local v4, "i":Landroid/content/Intent;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v14

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " Sync during switch package+service == "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v14, v13}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    new-instance v10, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-direct {v10, v14, v13}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1239
    .local v10, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    iget-object v3, v10, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1240
    .local v3, "fullServiceName":Ljava/lang/String;
    iget-object v8, v10, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 1241
    .local v8, "packageName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " Sync during switch package == "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "; service == "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, v8, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1245
    .local v2, "cn":Landroid/content/ComponentName;
    invoke-virtual {v4, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1246
    const-string v13, "dowhat"

    const-string v14, "DELETE"

    invoke-virtual {v4, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1247
    const-string v13, "personaid"

    move/from16 v0, p1

    invoke-virtual {v4, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1248
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1249
    .local v1, "bundle":Landroid/os/Bundle;
    new-instance v6, Landroid/os/Messenger;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v13, v13, Lcom/sec/knox/bridge/BridgeService;->mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    invoke-direct {v6, v13}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 1250
    .local v6, "mProxyMessenger":Landroid/os/Messenger;
    const-string v13, "proxy"

    invoke-virtual {v1, v13, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1251
    const-string v13, "binderBundle"

    invoke-virtual {v4, v13, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1252
    const-string v13, "Notifications"

    invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 1255
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-virtual {v13, v4}, Lcom/sec/knox/bridge/BridgeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 1261
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "cn":Landroid/content/ComponentName;
    .end local v3    # "fullServiceName":Ljava/lang/String;
    .end local v4    # "i":Landroid/content/Intent;
    .end local v5    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v6    # "mProxyMessenger":Landroid/os/Messenger;
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v9    # "pairs":Ljava/util/Map$Entry;
    .end local v10    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    .end local v11    # "syncerName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public deleteShortcuts(I)V
    .locals 2
    .param p1, "personaID"    # I

    .prologue
    .line 1210
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, " IBridgeSyncer - deleteShortcuts "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1211
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->deleteShortcutsForUser(I)V

    .line 1212
    return-void
.end method

.method public doSync()V
    .locals 8

    .prologue
    .line 1177
    const-string v4, "doSync"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v4}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1178
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "IBridgeSyncer - doSync() - will kick off all syncers in bridge proxy here"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1183
    .local v2, "i":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v4, "com.sec.knox.bridge"

    const-string v5, "com.sec.knox.bridge.notifsync.NotifSyncService"

    invoke-direct {v1, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    .local v1, "cn":Landroid/content/ComponentName;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1187
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1188
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v3, Landroid/os/Messenger;

    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v4, v4, Lcom/sec/knox/bridge/BridgeService;->mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    invoke-direct {v3, v4}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 1189
    .local v3, "mProxyMessenger":Landroid/os/Messenger;
    const-string v4, "proxy"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1191
    const-string v4, "binderBundle"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1196
    # getter for: Lcom/sec/knox/bridge/BridgeService;->mHandlerDoSync:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1600()Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer$1;

    invoke-direct {v5, p0}, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer$1;-><init>(Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;)V

    const-wide/16 v6, 0xfa0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1206
    return-void
.end method

.method public singleClipDeleted(Ljava/lang/String;Z)V
    .locals 11
    .param p1, "removedClipId"    # Ljava/lang/String;
    .param p2, "reADD"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1317
    if-nez p2, :cond_1

    .line 1318
    const-string v7, "singleClipDeleted"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v7}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1319
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    const-string v8, "IBridgeSyncer - singleClipDeleted() - reADD:false ori "

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, v7, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    const-string v8, "SYNCER_PREFS"

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1322
    .local v6, "syncerPreferences":Landroid/content/SharedPreferences;
    const-string v7, "Clipboard"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1324
    .local v5, "syncService":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 1325
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    const-string v8, "IBridgeSyncer - singleClipDeleted(): Bridge Prefs does NOT contain a syncService for Clipboard"

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1371
    :goto_0
    return-void

    .line 1329
    :cond_0
    new-instance v4, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {v4, v7, v5}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1330
    .local v4, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    iget-object v1, v4, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1331
    .local v1, "fullServiceName":Ljava/lang/String;
    iget-object v3, v4, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 1332
    .local v3, "packageName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Sync during switch package == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; service == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IBridgeSyncer - singleClipDeleted(): package = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and fullServiceName = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1338
    .local v2, "i":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v3, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1339
    .local v0, "cn":Landroid/content/ComponentName;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1340
    const-string v7, "dowhat"

    const-string v8, "CLIP_REMOVED"

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1341
    const-string v7, "RemovedClipId"

    invoke-virtual {v2, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1342
    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, v7, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->onStartCommand(Landroid/content/Intent;)I

    goto :goto_0

    .line 1344
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "fullServiceName":Ljava/lang/String;
    .end local v2    # "i":Landroid/content/Intent;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    .end local v5    # "syncService":Ljava/lang/String;
    .end local v6    # "syncerPreferences":Landroid/content/SharedPreferences;
    :cond_1
    const-string v7, "singleClipDeleted"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v7}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1345
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    const-string v8, "IBridgeSyncer - singleClipDeleted() - reADD:TRUE new"

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, v7, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    const-string v8, "SYNCER_PREFS"

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1348
    .restart local v6    # "syncerPreferences":Landroid/content/SharedPreferences;
    const-string v7, "Clipboard"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1350
    .restart local v5    # "syncService":Ljava/lang/String;
    if-nez v5, :cond_2

    .line 1351
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    const-string v8, "IBridgeSyncer - singleClipDeleted(): Bridge Prefs does NOT contain a syncService for Clipboard"

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1355
    :cond_2
    new-instance v4, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {v4, v7, v5}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1356
    .restart local v4    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    iget-object v1, v4, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1357
    .restart local v1    # "fullServiceName":Ljava/lang/String;
    iget-object v3, v4, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 1358
    .restart local v3    # "packageName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Sync during switch package == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; service == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1361
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IBridgeSyncer - singleClipDeleted(): package = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and fullServiceName = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1363
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1364
    .restart local v2    # "i":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v3, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    .restart local v0    # "cn":Landroid/content/ComponentName;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1366
    const-string v7, "dowhat"

    const-string v8, "CLIP_REMOVED"

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1367
    const-string v7, "RemovedClipId"

    invoke-virtual {v2, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1368
    const-string v7, "reADD"

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1369
    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, v7, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->onStartCommand(Landroid/content/Intent;)I

    goto/16 :goto_0
.end method
