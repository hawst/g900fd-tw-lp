.class Lcom/sec/knox/bridge/BridgeService$RunnableCallback;
.super Landroid/os/IRunnableCallback$Stub;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RunnableCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 2615
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/os/IRunnableCallback$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p2, "x1"    # Lcom/sec/knox/bridge/BridgeService$1;

    .prologue
    .line 2615
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    return-void
.end method


# virtual methods
.method public run(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 29
    .param p1, "bdl"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2620
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "RunnableCallback() called "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2622
    const-string v25, "MoveTo"

    const-string v26, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 2623
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "action:MoveTo "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const-string v25, "rawContact"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v25

    check-cast v25, Landroid/content/CustomCursor;

    const-string v26, "contactData"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v26

    check-cast v26, Landroid/content/CustomCursor;

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/operations/ExchangeContactData;->insertContact(Landroid/content/Context;Landroid/content/CustomCursor;Landroid/content/CustomCursor;)Landroid/os/Bundle;

    move-result-object v20

    .line 2785
    :cond_0
    :goto_0
    return-object v20

    .line 2628
    :cond_1
    const-string v25, "RequestProxy"

    const-string v26, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 2629
    const-string v25, "queryPersonaInfos"

    const-string v26, "cmd"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 2630
    new-instance v20, Landroid/os/Bundle;

    invoke-direct/range {v20 .. v20}, Landroid/os/Bundle;-><init>()V

    .line 2632
    .local v20, "resp":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getInstance()Lcom/sec/knox/bridge/operations/PersonaInfoData;

    move-result-object v25

    if-eqz v25, :cond_0

    .line 2633
    invoke-static {}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getInstance()Lcom/sec/knox/bridge/operations/PersonaInfoData;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getSimplePersonaInfo()Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    move-result-object v19

    .line 2635
    .local v19, "pinfos":Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->getSize()I

    move-result v21

    .line 2637
    .local v21, "size":I
    if-lez v21, :cond_0

    .line 2638
    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    .line 2639
    .local v18, "personaTypes":[Ljava/lang/String;
    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    .line 2640
    .local v17, "personaNames":[Ljava/lang/String;
    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v16, v0

    .line 2643
    .local v16, "personaIds":[I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move/from16 v0, v21

    if-ge v10, v0, :cond_3

    .line 2644
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->getItem(I)Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;

    move-result-object v12

    .line 2645
    .local v12, "info":Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;
    if-eqz v12, :cond_2

    .line 2646
    iget-object v0, v12, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->type:Ljava/lang/String;

    move-object/from16 v25, v0

    aput-object v25, v18, v10

    .line 2647
    iget v0, v12, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->id:I

    move/from16 v25, v0

    aput v25, v16, v10

    .line 2648
    iget-object v0, v12, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->name:Ljava/lang/String;

    move-object/from16 v25, v0

    aput-object v25, v17, v10

    .line 2650
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "queryInfo "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget v0, v12, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->id:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget-object v0, v12, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget-object v0, v12, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->type:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2643
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 2655
    .end local v12    # "info":Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;
    :cond_3
    const-string v25, "personaIds"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 2656
    const-string v25, "personaTypes"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2657
    const-string v25, "personaNames"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2664
    .end local v10    # "i":I
    .end local v16    # "personaIds":[I
    .end local v17    # "personaNames":[Ljava/lang/String;
    .end local v18    # "personaTypes":[Ljava/lang/String;
    .end local v19    # "pinfos":Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;
    .end local v20    # "resp":Landroid/os/Bundle;
    .end local v21    # "size":I
    :cond_4
    const-string v25, "cmd"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    # invokes: Lcom/sec/knox/bridge/BridgeService;->isBridgeCmd(Ljava/lang/String;)Z
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$2600(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 2665
    const-string v25, "callerPkgName"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    # invokes: Lcom/sec/knox/bridge/BridgeService;->isAllowedPackagesForBridgeCmd(Ljava/lang/String;)Z
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$2700(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 2666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    # invokes: Lcom/sec/knox/bridge/BridgeService;->bridgeCmd(Landroid/os/Bundle;)Landroid/os/Bundle;
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/BridgeService;->access$2800(Lcom/sec/knox/bridge/BridgeService;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v20

    goto/16 :goto_0

    .line 2669
    :cond_5
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 2670
    :cond_6
    const-string v25, "notifysync"

    const-string v26, "cmd"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 2672
    const-string v25, "callerPkgName"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    # invokes: Lcom/sec/knox/bridge/BridgeService;->isAllowedPackagesForBridgeCmd(Ljava/lang/String;)Z
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$2700(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 2674
    const-string v25, "contentIntent"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/app/PendingIntent;

    .line 2676
    .local v13, "intent":Landroid/app/PendingIntent;
    invoke-virtual {v13}, Landroid/app/PendingIntent;->getIntent()Landroid/content/Intent;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 2678
    .local v6, "bundle":Landroid/os/Bundle;
    new-instance v8, Landroid/app/Command;

    invoke-direct {v8}, Landroid/app/Command;-><init>()V

    .line 2679
    .local v8, "command":Landroid/app/Command;
    const-string v25, "personaId"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    iput v0, v8, Landroid/app/Command;->personaId:I

    .line 2680
    const-string v25, "contentIntent"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v25

    check-cast v25, Landroid/app/PendingIntent;

    move-object/from16 v0, v25

    iput-object v0, v8, Landroid/app/Command;->contentIntent:Landroid/app/PendingIntent;

    .line 2681
    const-string v25, "commandType"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    iput-object v0, v8, Landroid/app/Command;->type:Ljava/lang/String;

    .line 2682
    const-string v25, "intent"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v25

    check-cast v25, Landroid/content/Intent;

    move-object/from16 v0, v25

    iput-object v0, v8, Landroid/app/Command;->intent:Landroid/content/Intent;

    .line 2684
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "personaId"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ":"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "contentIntent"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ":"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "commandType"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2689
    sget-object v25, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Landroid/os/RCPManager;->executeCommandForPersona(Landroid/app/Command;)V

    .line 2785
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v8    # "command":Landroid/app/Command;
    .end local v13    # "intent":Landroid/app/PendingIntent;
    :cond_7
    :goto_2
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 2692
    :cond_8
    const-string v25, "UpdateBadgeCount"

    const-string v26, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 2693
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v25

    if-eqz v25, :cond_7

    .line 2694
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->updateShortcutBadgeCount(Landroid/os/Bundle;)V

    goto :goto_2

    .line 2696
    :cond_9
    const-string v25, "RequestUpdateBadgeCount"

    const-string v26, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 2697
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v25

    if-eqz v25, :cond_7

    .line 2698
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->trySync(Landroid/os/Bundle;)V

    goto :goto_2

    .line 2700
    :cond_a
    const-string v25, "RequestBadgeInfo"

    const-string v26, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 2701
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v25

    if-eqz v25, :cond_7

    .line 2702
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    const-string v27, "pkgName"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getBadgeInfoByPkgName(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v20

    goto/16 :goto_0

    .line 2705
    :cond_b
    const-string v25, "RequestSyncInfo"

    const-string v26, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 2706
    const/4 v5, 0x1

    .line 2707
    .local v5, "bDirtyContact":Z
    const/4 v4, 0x1

    .line 2708
    .local v4, "bDirtyCalendar":Z
    const-string v25, "userid"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 2710
    .local v22, "userid":I
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2712
    .restart local v6    # "bundle":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/knox/bridge/operations/DbObserver;->getInstance()Lcom/sec/knox/bridge/operations/DbObserver;

    move-result-object v25

    if-eqz v25, :cond_c

    .line 2713
    invoke-static {}, Lcom/sec/knox/bridge/operations/DbObserver;->getInstance()Lcom/sec/knox/bridge/operations/DbObserver;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/operations/DbObserver;->isContactDirty(I)Z

    move-result v5

    .line 2714
    invoke-static {}, Lcom/sec/knox/bridge/operations/DbObserver;->getInstance()Lcom/sec/knox/bridge/operations/DbObserver;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/operations/DbObserver;->isCalendarDirty(I)Z

    move-result v4

    .line 2717
    :cond_c
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "RequestSyncInfo id: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "->"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2720
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v11

    .line 2721
    .local v11, "id":I
    const/4 v15, 0x0

    .line 2722
    .local v15, "isChangeContactPolicy":Z
    const/4 v14, 0x0

    .line 2723
    .local v14, "isChangeCalendarPolicy":Z
    if-eqz v11, :cond_d

    .line 2725
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$700(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v25

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 2726
    .local v23, "vl1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$800(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v25

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 2728
    .local v24, "vl2":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "RunnableCallback personaID: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", Contact: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", Calendar: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2734
    if-eqz v23, :cond_f

    .line 2735
    const-string v25, "T"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    .line 2739
    :goto_3
    if-eqz v24, :cond_10

    .line 2740
    const-string v25, "T"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v14

    .line 2749
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$700(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v25

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    const-string v27, "F"

    invoke-virtual/range {v25 .. v27}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$800(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v25

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    const-string v27, "F"

    invoke-virtual/range {v25 .. v27}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2754
    .end local v23    # "vl1":Ljava/lang/String;
    .end local v24    # "vl2":Ljava/lang/String;
    :cond_d
    :goto_5
    if-eqz v15, :cond_11

    .line 2755
    const-string v25, "isDirtyContact"

    const/16 v26, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2759
    :goto_6
    if-nez v14, :cond_e

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mAccountChanged:Z
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$2900()Z

    move-result v25

    if-eqz v25, :cond_12

    .line 2760
    :cond_e
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "RequestSyncInfo AccountsChangedReceiver "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mAccountChanged:Z
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$2900()Z

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2761
    const/16 v25, 0x0

    # setter for: Lcom/sec/knox/bridge/BridgeService;->mAccountChanged:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$2902(Z)Z

    .line 2762
    const-string v25, "isDirtyCalendar"

    const/16 v26, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_7
    move-object/from16 v20, v6

    .line 2766
    goto/16 :goto_0

    .line 2737
    .restart local v23    # "vl1":Ljava/lang/String;
    .restart local v24    # "vl2":Ljava/lang/String;
    :cond_f
    const/4 v15, 0x1

    goto :goto_3

    .line 2742
    :cond_10
    const/4 v14, 0x1

    goto :goto_4

    .line 2744
    .end local v23    # "vl1":Ljava/lang/String;
    .end local v24    # "vl2":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 2745
    .local v9, "e":Ljava/lang/Exception;
    const/4 v15, 0x1

    .line 2746
    const/4 v14, 0x1

    .line 2747
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$700(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v25

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    const-string v27, "F"

    invoke-virtual/range {v25 .. v27}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/bridge/BridgeService;->access$800(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v25

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    const-string v27, "F"

    invoke-virtual/range {v25 .. v27}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 2749
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v26 .. v26}, Lcom/sec/knox/bridge/BridgeService;->access$700(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v26

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    const-string v28, "F"

    invoke-virtual/range {v26 .. v28}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v26 .. v26}, Lcom/sec/knox/bridge/BridgeService;->access$800(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v26

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    const-string v28, "F"

    invoke-virtual/range {v26 .. v28}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    throw v25

    .line 2757
    :cond_11
    const-string v25, "isDirtyContact"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_6

    .line 2764
    :cond_12
    const-string v25, "isDirtyCalendar"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_7

    .line 2767
    .end local v4    # "bDirtyCalendar":Z
    .end local v5    # "bDirtyContact":Z
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v11    # "id":I
    .end local v14    # "isChangeCalendarPolicy":Z
    .end local v15    # "isChangeContactPolicy":Z
    .end local v22    # "userid":I
    :cond_13
    const-string v25, "RequestSyncUpdate"

    const-string v26, "action"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 2768
    const-string v25, "userid"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 2769
    .restart local v22    # "userid":I
    const-string v25, "app_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2770
    .local v3, "app_name":Ljava/lang/String;
    const-string v25, "cmd"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2772
    .local v7, "cmd":Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/operations/DbObserver;->getInstance()Lcom/sec/knox/bridge/operations/DbObserver;

    move-result-object v25

    if-eqz v25, :cond_14

    .line 2773
    const-string v25, "clear"

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 2774
    const-string v25, "Contact"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_15

    .line 2775
    invoke-static {}, Lcom/sec/knox/bridge/operations/DbObserver;->getInstance()Lcom/sec/knox/bridge/operations/DbObserver;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/operations/DbObserver;->clearContactDirty(I)V

    .line 2782
    :cond_14
    :goto_8
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "RequestSyncUpdate "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ":"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2776
    :cond_15
    const-string v25, "Calendar"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 2777
    invoke-static {}, Lcom/sec/knox/bridge/operations/DbObserver;->getInstance()Lcom/sec/knox/bridge/operations/DbObserver;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/operations/DbObserver;->clearCalendarDirty(I)V

    goto :goto_8
.end method
