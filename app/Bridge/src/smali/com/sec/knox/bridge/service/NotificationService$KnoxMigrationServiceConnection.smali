.class Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;
.super Ljava/lang/Object;
.source "NotificationService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/service/NotificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KnoxMigrationServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/service/NotificationService;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/service/NotificationService;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/service/NotificationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/service/NotificationService;Lcom/sec/knox/bridge/service/NotificationService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/service/NotificationService;
    .param p2, "x1"    # Lcom/sec/knox/bridge/service/NotificationService$1;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;-><init>(Lcom/sec/knox/bridge/service/NotificationService;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "compName"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 131
    const-string v2, "NotificationService"

    const-string v3, "KnoxMigrationServiceConnection:onServiceConnected"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/service/NotificationService;

    invoke-static {p2}, Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    move-result-object v3

    # setter for: Lcom/sec/knox/bridge/service/NotificationService;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;
    invoke-static {v2, v3}, Lcom/sec/knox/bridge/service/NotificationService;->access$102(Lcom/sec/knox/bridge/service/NotificationService;Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;)Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    .line 134
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/service/NotificationService;

    # getter for: Lcom/sec/knox/bridge/service/NotificationService;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;
    invoke-static {v2}, Lcom/sec/knox/bridge/service/NotificationService;->access$100(Lcom/sec/knox/bridge/service/NotificationService;)Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;->getMigrationCurrentState()I

    move-result v1

    .line 135
    .local v1, "migrationState":I
    const-string v2, "NotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "migrationState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    if-eqz v1, :cond_0

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/service/NotificationService;

    # getter for: Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;
    invoke-static {v2}, Lcom/sec/knox/bridge/service/NotificationService;->access$200(Lcom/sec/knox/bridge/service/NotificationService;)Lcom/sec/knox/bridge/util/NotificationUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->showNotification()V

    .line 149
    .end local v1    # "migrationState":I
    :goto_0
    return-void

    .line 141
    .restart local v1    # "migrationState":I
    :cond_1
    const-string v2, "NotificationService"

    const-string v3, "Do not Show notification"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 143
    .end local v1    # "migrationState":I
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 146
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/service/NotificationService;

    # getter for: Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;
    invoke-static {v2}, Lcom/sec/knox/bridge/service/NotificationService;->access$200(Lcom/sec/knox/bridge/service/NotificationService;)Lcom/sec/knox/bridge/util/NotificationUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->showNotification()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "compName"    # Landroid/content/ComponentName;

    .prologue
    .line 153
    const-string v0, "NotificationService"

    const-string v1, "KnoxMigrationServiceConnection:onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/service/NotificationService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/knox/bridge/service/NotificationService;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/service/NotificationService;->access$102(Lcom/sec/knox/bridge/service/NotificationService;Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;)Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    .line 155
    return-void
.end method
