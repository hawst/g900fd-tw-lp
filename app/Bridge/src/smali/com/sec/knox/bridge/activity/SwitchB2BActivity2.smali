.class public Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;
.super Landroid/app/Activity;
.source "SwitchB2BActivity2.java"


# static fields
.field public static isInLockChecking:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->isInLockChecking:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static processAfterKeyCheck(Landroid/content/Context;)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 99
    sget-boolean v0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->isInLockChecking:Z

    if-eqz v0, :cond_0

    .line 100
    invoke-static {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->switchToB2BUser(Landroid/content/Context;)V

    .line 101
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->isInLockChecking:Z

    .line 103
    :cond_0
    return-void
.end method

.method private static switchToB2BUser(Landroid/content/Context;)V
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/16 v9, 0x5a

    .line 33
    const/4 v5, -0x1

    .line 34
    .local v5, "userid":I
    invoke-static {}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->getKnoxUser2()I

    move-result v5

    .line 35
    const-string v6, "SwitchB2BActivity2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "switch to b2b container "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    if-gtz v5, :cond_1

    .line 38
    const-string v6, "SwitchB2BActivity2"

    const-string v7, "switchToB2BUser() userid not found for knox2"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-static {p0}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->initialize(Landroid/content/Context;)V

    .line 40
    if-gtz v5, :cond_1

    .line 41
    const-string v6, "SwitchB2BActivity2"

    const-string v7, "switchToB2BUser() userid not found for knox2"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v4

    .line 49
    .local v4, "status":I
    const-string v6, "SwitchB2BActivity2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "switch to persona "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->isAdminLocked(Landroid/content/Context;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 52
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->startActivityAdminLocked(Landroid/content/Context;I)V

    goto :goto_0

    .line 56
    :cond_2
    if-eq v4, v9, :cond_4

    .line 57
    sget-boolean v6, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->isInLockChecking:Z

    if-nez v6, :cond_3

    .line 58
    const/4 v6, 0x1

    sput-boolean v6, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->isInLockChecking:Z

    .line 59
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->sendRequestKeyStatus(Landroid/content/Context;I)V

    goto :goto_0

    .line 63
    :cond_3
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v4

    .line 65
    if-eq v4, v9, :cond_4

    .line 66
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->startActivity(Landroid/content/Context;I)V

    .line 67
    const-string v6, "SwitchB2BActivity2"

    const-string v7, "KLMS status is not valid"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :cond_4
    :try_start_0
    const-string v6, "persona"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    .line 77
    .local v2, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v2, v5}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v3

    .line 78
    .local v3, "pInfo":Landroid/content/pm/PersonaInfo;
    const-string v6, "SwitchB2BActivity2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "PersonaInfo:getType() :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Landroid/content/pm/PersonaInfo;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {v2, v5}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v6

    sget-object v7, Landroid/content/pm/PersonaState;->CREATING:Landroid/content/pm/PersonaState;

    invoke-virtual {v6, v7}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 80
    iget-boolean v6, v3, Landroid/content/pm/PersonaInfo;->isLightWeightContainer:Z

    if-eqz v6, :cond_5

    .line 81
    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 82
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "com.sec.knox.knoxsetupwizardclient"

    const-string v7, "com.sec.knox.knoxsetupwizardclient.shortcut.FolderContainer"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string v6, "userId"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    const-string v6, "folderMode"

    const-string v7, "lwc"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const/high16 v6, 0x4000000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 87
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->notifyLwcLaunchToKnoxusageMonitorService(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 92
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "mPersona":Landroid/os/PersonaManager;
    .end local v3    # "pInfo":Landroid/content/pm/PersonaInfo;
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "SwitchB2BActivity2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "switch error message : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 89
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "mPersona":Landroid/os/PersonaManager;
    .restart local v3    # "pInfo":Landroid/content/pm/PersonaInfo;
    :cond_5
    :try_start_1
    invoke-virtual {v2, v5}, Landroid/os/PersonaManager;->launchPersonaHome(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const-string v0, "SwitchB2BActivity2"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-static {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->switchToB2BUser(Landroid/content/Context;)V

    .line 29
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->finish()V

    .line 30
    return-void
.end method
