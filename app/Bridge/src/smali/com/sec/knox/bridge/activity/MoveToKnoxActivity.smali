.class public Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
.super Landroid/app/Activity;
.source "MoveToKnoxActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;
    }
.end annotation


# static fields
.field public static darkThemeTextColor:I

.field public static isDarkTheme:Z

.field public static isUseLockStateCheck:Z

.field public static whiteThemeTextColor:I


# instance fields
.field private TAG:Ljava/lang/String;

.field private mCurrentProgress:I

.field private mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

.field private mDstPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mIsMoveFailed:Z

.field private mKnoxIdNamePair:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKnoxInfoForApp:Landroid/os/Bundle;

.field private mLaunchAfterLockCheck:Z

.field private mMoveToCurrnetCnt:I

.field private mMoveToDstCId:I

.field private mMoveToDstName:Ljava/lang/String;

.field private mMoveToSrcCId:I

.field private mMoveToThreadId:J

.field private mMoveToTotalCnt:I

.field private mPersonaManager:Landroid/os/PersonaManager;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mRCPInterfaceCallBack:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;

.field private mRcpProxy:Landroid/content/IRCPInterface;

.field private mRequestApp:I

.field private mSrcPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isUseLockStateCheck:Z

    .line 78
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    .line 80
    const-string v0, "#000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->whiteThemeTextColor:I

    .line 81
    const-string v0, "#f5f5f5"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->darkThemeTextColor:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    const-string v0, "MoveToKnoxActivity"

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRequestApp:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mLaunchAfterLockCheck:Z

    .line 61
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    .line 63
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRCPInterfaceCallBack:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;

    .line 67
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 68
    iput v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    .line 69
    iput v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I

    .line 70
    iput v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I

    .line 71
    iput-boolean v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mIsMoveFailed:Z

    .line 93
    new-instance v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;-><init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;

    .line 415
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->moveToKnox()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getSuccessMsg()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mIsMoveFailed:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mIsMoveFailed:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I

    return v0
.end method

.method static synthetic access$308(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showToast(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToThreadId:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/content/IRCPInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    return p1
.end method

.method private checkLockState()Z
    .locals 5

    .prologue
    .line 492
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mPersonaManager:Landroid/os/PersonaManager;

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v1

    .line 493
    .local v1, "state":Landroid/content/pm/PersonaState;
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Persona state = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    sget-object v2, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    if-ne v1, v2, :cond_0

    .line 495
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v3, "Persona is locked while moving files to KNOX"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    new-instance v0, Landroid/content/Intent;

    const-string v2, "action.sec.knox.moveto"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 497
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.knox.bridge"

    const-string v3, "com.sec.knox.bridge.handlers.MoveToHandler"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 500
    const-string v2, "actions"

    const-string v3, "movetoKNOX"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 501
    const-string v3, "srcFilePaths"

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 502
    const-string v3, "dstFilePaths"

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 503
    const-string v2, "requestApp"

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRequestApp:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 504
    const-string v2, "moveToDstCId"

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 506
    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 508
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->finish()V

    .line 510
    const/4 v2, 0x1

    .line 513
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v2

    .line 512
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v3, "Persona isn\'t locked"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getSuccessMsg()Ljava/lang/String;
    .locals 11

    .prologue
    const v10, 0x7f060010

    const v9, 0x7f06000e

    const/4 v6, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 380
    const/4 v3, 0x0

    .line 381
    .local v3, "successMsg":Ljava/lang/String;
    const/4 v1, 0x0

    .line 383
    .local v1, "isCurrentLanguageKorean":Z
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, "currentLanguage":Ljava/lang/String;
    sget-object v4, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 385
    const/4 v1, 0x1

    .line 388
    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    if-nez v4, :cond_2

    .line 389
    iget v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    if-ne v4, v7, :cond_1

    .line 390
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstName:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 411
    :goto_0
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "successMsg = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    return-object v3

    .line 393
    :cond_1
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstName:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v4, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 398
    :cond_2
    const/4 v2, 0x0

    .line 399
    .local v2, "strPersonal":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060011

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 401
    iget v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    if-ne v4, v7, :cond_3

    if-nez v1, :cond_3

    .line 402
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 405
    :cond_3
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    aput-object v2, v5, v7

    invoke-virtual {v4, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private moveToKnox()V
    .locals 9

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveToKnox() srcFilePaths="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; destFilePaths="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; reqApp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRequestApp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mMoveToSrcCId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mMoveToDstCId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mPersonaManager:Landroid/os/PersonaManager;

    iget v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v8

    .line 524
    .local v8, "personaInfo":Landroid/content/pm/PersonaInfo;
    const/4 v7, 0x0

    .line 525
    .local v7, "isSdpEnabled":Z
    if-eqz v8, :cond_0

    .line 526
    iget-boolean v7, v8, Landroid/content/pm/PersonaInfo;->sdpEnabled:Z

    .line 527
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sdpEnabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    :goto_0
    sget-boolean v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isUseLockStateCheck:Z

    if-eqz v0, :cond_1

    if-eqz v7, :cond_1

    .line 533
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->checkLockState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 546
    :goto_1
    return-void

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "PersonaInfo is null"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 539
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;

    iget v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    iget-object v5, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRCPInterfaceCallBack:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;

    invoke-interface/range {v0 .. v5}, Landroid/content/IRCPInterface;->moveFiles(ILjava/util/List;ILjava/util/List;Landroid/content/IRCPInterfaceCallback;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToThreadId:J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 541
    :catch_0
    move-exception v6

    .line 542
    .local v6, "e":Landroid/os/RemoteException;
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "moveFiles throws remote exception"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    .line 544
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->finish()V

    goto :goto_1
.end method

.method private showDstKnoxSelectionDialog()Z
    .locals 21

    .prologue
    .line 288
    if-eqz p0, :cond_5

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 290
    .local v8, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 291
    .local v9, "knoxFirstId":I
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 292
    .local v11, "knoxSecondId":I
    const-string v17, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_1

    const-string v17, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 294
    .local v10, "knoxFirstName":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 296
    .local v12, "knoxSecondName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f030003

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    .line 299
    .local v16, "view":Landroid/view/View;
    const v17, 0x7f080009

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 300
    .local v4, "firstKnox":Landroid/widget/TextView;
    const v17, 0x7f08000c

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 302
    .local v13, "secondKnox":Landroid/widget/TextView;
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    sget-boolean v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v17, :cond_2

    .line 306
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->whiteThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 307
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->whiteThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 313
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 314
    .local v7, "iconDrawable":Landroid/graphics/drawable/Drawable;
    const v17, 0x7f080008

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 315
    .local v5, "firstKnoxImg":Landroid/widget/ImageView;
    const v17, 0x7f08000b

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    .line 316
    .local v14, "secondKnoxImg":Landroid/widget/ImageView;
    if-eqz v7, :cond_0

    .line 317
    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 318
    .local v6, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_0

    .line 319
    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 322
    .end local v6    # "iconBitmap":Landroid/graphics/Bitmap;
    :cond_0
    new-instance v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$3;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9, v10}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$3;-><init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;ILjava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    new-instance v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11, v12}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;-><init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;ILjava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    move/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 347
    const v15, 0x7f060013

    .line 354
    .local v15, "titleId":I
    :goto_2
    sget-boolean v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v17, :cond_4

    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "mDstKnoxSelectionDialog is light theme"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    new-instance v17, Landroid/app/AlertDialog$Builder;

    new-instance v18, Landroid/view/ContextThemeWrapper;

    const v19, 0x103012b

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct/range {v17 .. v18}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 364
    .local v3, "builder":Landroid/app/AlertDialog$Builder;
    :goto_3
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    .line 365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$5;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$5;-><init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->show()V

    .line 373
    const/16 v17, 0x1

    .line 375
    .end local v3    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v4    # "firstKnox":Landroid/widget/TextView;
    .end local v5    # "firstKnoxImg":Landroid/widget/ImageView;
    .end local v8    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v9    # "knoxFirstId":I
    .end local v10    # "knoxFirstName":Ljava/lang/String;
    .end local v11    # "knoxSecondId":I
    .end local v12    # "knoxSecondName":Ljava/lang/String;
    .end local v13    # "secondKnox":Landroid/widget/TextView;
    .end local v14    # "secondKnoxImg":Landroid/widget/ImageView;
    .end local v15    # "titleId":I
    .end local v16    # "view":Landroid/view/View;
    :goto_4
    return v17

    .line 292
    .restart local v8    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v9    # "knoxFirstId":I
    .restart local v11    # "knoxSecondId":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v10, v17

    goto/16 :goto_0

    .line 309
    .restart local v4    # "firstKnox":Landroid/widget/TextView;
    .restart local v10    # "knoxFirstName":Ljava/lang/String;
    .restart local v12    # "knoxSecondName":Ljava/lang/String;
    .restart local v13    # "secondKnox":Landroid/widget/TextView;
    .restart local v16    # "view":Landroid/view/View;
    :cond_2
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->darkThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 310
    sget v17, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->darkThemeTextColor:I

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 349
    .restart local v5    # "firstKnoxImg":Landroid/widget/ImageView;
    .restart local v14    # "secondKnoxImg":Landroid/widget/ImageView;
    :cond_3
    const v15, 0x7f060014

    .restart local v15    # "titleId":I
    goto :goto_2

    .line 359
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "mDstKnoxSelectionDialog is dark theme"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    new-instance v17, Landroid/app/AlertDialog$Builder;

    new-instance v18, Landroid/view/ContextThemeWrapper;

    const v19, 0x1030128

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct/range {v17 .. v18}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .restart local v3    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_3

    .line 375
    .end local v3    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v4    # "firstKnox":Landroid/widget/TextView;
    .end local v5    # "firstKnoxImg":Landroid/widget/ImageView;
    .end local v8    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v9    # "knoxFirstId":I
    .end local v10    # "knoxFirstName":Ljava/lang/String;
    .end local v11    # "knoxSecondId":I
    .end local v12    # "knoxSecondName":Ljava/lang/String;
    .end local v13    # "secondKnox":Landroid/widget/TextView;
    .end local v14    # "secondKnoxImg":Landroid/widget/ImageView;
    .end local v15    # "titleId":I
    .end local v16    # "view":Landroid/view/View;
    :cond_5
    const/16 v17, 0x0

    goto :goto_4
.end method

.method private showToast(Ljava/lang/String;I)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 549
    sget-boolean v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v0, :cond_0

    .line 550
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x103012b

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 558
    :goto_0
    return-void

    .line 554
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x1030128

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 562
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 563
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x64

    const/4 v2, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 165
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 166
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 171
    .local v8, "intent":Landroid/content/Intent;
    const-string v0, "requestApp"

    invoke-virtual {v8, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRequestApp:I

    .line 172
    const-string v0, "srcFilePaths"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    .line 173
    const-string v0, "dstFilePaths"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    .line 174
    const-string v0, "launchAfterLockCheck"

    invoke-virtual {v8, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mLaunchAfterLockCheck:Z

    .line 175
    const-string v0, "moveToDstCId"

    invoke-virtual {v8, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    .line 177
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() srcFilePaths="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; destFilePaths="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; reqApp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRequestApp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mLaunchAfterLockCheck = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mLaunchAfterLockCheck:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mMoveToDstCId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    new-instance v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;-><init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRCPInterfaceCallBack:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;

    .line 183
    const-string v0, "persona"

    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mPersonaManager:Landroid/os/PersonaManager;

    .line 184
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mPersonaManager:Landroid/os/PersonaManager;

    invoke-virtual {v0}, Landroid/os/PersonaManager;->getRCPInterface()Landroid/content/IRCPInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;

    .line 187
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    .line 188
    iput v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I

    .line 189
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 190
    sget-boolean v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "mProgressDialog is light theme"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance v0, Landroid/app/ProgressDialog;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x103012b

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 201
    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 202
    const v9, 0x7f060013

    .line 207
    .local v9, "progressTitleId":I
    :goto_1
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v9}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 210
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 213
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v10}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 214
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 215
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    const v2, 0x7f060016

    invoke-virtual {p0, v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;-><init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 233
    iget-boolean v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mLaunchAfterLockCheck:Z

    if-ne v0, v5, :cond_3

    iget v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    if-lt v0, v10, :cond_3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 235
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "Move to KNOX after unlock"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    .line 238
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    const-string v1, "KnoxIdNamePair"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    .line 240
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    .line 241
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstName:Ljava/lang/String;

    .line 243
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->moveToKnox()V

    .line 285
    :goto_2
    return-void

    .line 195
    .end local v9    # "progressTitleId":I
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "mProgressDialog is dark theme"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    new-instance v0, Landroid/app/ProgressDialog;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x1030128

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 204
    :cond_2
    const v9, 0x7f060014

    .restart local v9    # "progressTitleId":I
    goto/16 :goto_1

    .line 245
    :cond_3
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "Normal case"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    .line 248
    iget v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 249
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "Move to Personal mode"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    .line 252
    :try_start_0
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Move to Personal srcFilePaths="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; destFilePaths="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; reqApp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRequestApp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mMoveToSrcCId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mMoveToDstCId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;

    iget v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    iget v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    iget-object v5, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRCPInterfaceCallBack:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;

    invoke-interface/range {v0 .. v5}, Landroid/content/IRCPInterface;->moveFiles(ILjava/util/List;ILjava/util/List;Landroid/content/IRCPInterfaceCallback;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToThreadId:J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 258
    :catch_0
    move-exception v6

    .line 259
    .local v6, "e":Landroid/os/RemoteException;
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "moveFiles throws remote exception"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    .line 261
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->finish()V

    goto/16 :goto_2

    .line 264
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_4
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "Move to KNOX"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    .line 266
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    const-string v1, "KnoxIdNamePair"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    .line 268
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() - size of KnoxIdNamePair is ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 271
    .local v7, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-ne v0, v5, :cond_6

    .line 272
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 273
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    .line 274
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstName:Ljava/lang/String;

    goto :goto_3

    .line 276
    :cond_5
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->moveToKnox()V

    goto/16 :goto_2

    .line 277
    :cond_6
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 278
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showDstKnoxSelectionDialog()Z

    goto/16 :goto_2

    .line 280
    :cond_7
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "Can\'t reach here. Do nothing"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->finish()V

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 580
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 581
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 586
    :cond_0
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;

    .line 589
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    .line 590
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 591
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 592
    :cond_2
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 595
    :cond_3
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mSrcPaths:Ljava/util/List;

    .line 596
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstPaths:Ljava/util/List;

    .line 597
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxInfoForApp:Landroid/os/Bundle;

    .line 598
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mKnoxIdNamePair:Ljava/util/HashMap;

    .line 599
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRCPInterfaceCallBack:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;

    .line 600
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;

    .line 601
    iput-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mPersonaManager:Landroid/os/PersonaManager;

    .line 602
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 574
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 575
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 568
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 569
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    return-void
.end method
