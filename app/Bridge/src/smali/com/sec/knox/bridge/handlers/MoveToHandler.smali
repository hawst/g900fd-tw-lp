.class public Lcom/sec/knox/bridge/handlers/MoveToHandler;
.super Landroid/app/Service;
.source "MoveToHandler.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDstPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInputBundle:Landroid/os/Bundle;

.field private mMoveToDstCId:I

.field private mMoveToKnoxIsProgressing:Z

.field private mPersonaObservers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mPm:Landroid/os/PersonaManager;

.field private mRequestApp:I

.field private mRequestFromMoveToContact:Z

.field private mRequestFromMoveToFiles:Z

.field private mSrcPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/knox/bridge/handlers/MoveToHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPm:Landroid/os/PersonaManager;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    .line 39
    iput-boolean v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z

    .line 40
    iput-boolean v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToContact:Z

    .line 41
    iput-boolean v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToFiles:Z

    .line 42
    iput-object v1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mInputBundle:Landroid/os/Bundle;

    .line 43
    iput v3, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    .line 44
    iput-object v1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mSrcPaths:Ljava/util/List;

    .line 45
    iput-object v1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mDstPaths:Ljava/util/List;

    .line 46
    iput v3, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestApp:I

    .line 144
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/knox/bridge/handlers/MoveToHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToFiles:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mSrcPaths:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mDstPaths:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/knox/bridge/handlers/MoveToHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestApp:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/knox/bridge/handlers/MoveToHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToContact:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mInputBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/knox/bridge/handlers/MoveToHandler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/MoveToHandler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->unregisterPersonaObserver(I)V

    return-void
.end method

.method private registerPersonaObserver(I)V
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 106
    iget-object v1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 108
    new-instance v0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;

    invoke-virtual {p0}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x7

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;-><init>(Lcom/sec/knox/bridge/handlers/MoveToHandler;Landroid/content/Context;II)V

    .line 112
    .local v0, "observer":Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;
    iget-object v1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    .end local v0    # "observer":Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;
    :cond_0
    return-void
.end method

.method private sendKeyguardCommand(I)V
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 98
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 99
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "resetKeyguard"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 100
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.knox.containeragent2"

    const-string v3, "com.sec.knox.containeragent2.ui.keyguard.KnoxKeyguardService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 102
    new-instance v1, Landroid/os/UserHandle;

    invoke-direct {v1, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 103
    return-void
.end method

.method private unregisterPersonaObserver(I)V
    .locals 5
    .param p1, "userId"    # I

    .prologue
    .line 118
    sget-object v2, Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unregisterPersonaObserver : personaId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", myUserId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", processId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 122
    .local v0, "key":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;

    .line 123
    .local v1, "observer":Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;
    if-eqz v1, :cond_0

    .line 124
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-virtual {v1}, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->unregisterPersonaObserver()V

    .line 127
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 206
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "persona"

    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPm:Landroid/os/PersonaManager;

    .line 51
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 52
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 131
    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 132
    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 133
    .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 134
    .local v1, "key":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;

    .line 135
    .local v3, "observer":Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;
    if-eqz v3, :cond_0

    .line 136
    invoke-virtual {v3}, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->unregisterPersonaObserver()V

    goto :goto_0

    .line 139
    .end local v1    # "key":Ljava/lang/Integer;
    .end local v3    # "observer":Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;
    :cond_1
    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 141
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_2
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 142
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 56
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v2, "action.sec.knox.moveto"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    const/4 v1, 0x0

    .line 59
    .local v1, "bValidMoveToAction":Z
    const-string v2, "movetoContact"

    const-string v3, "actions"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 60
    iput-boolean v5, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToContact:Z

    .line 61
    iput-boolean v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToFiles:Z

    .line 62
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mInputBundle:Landroid/os/Bundle;

    .line 63
    const/4 v1, 0x1

    .line 64
    const-string v2, "moveToDstCId"

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    .line 66
    sget-object v2, Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "registerPersonaObserver : personaId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", myUserId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", processId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 81
    iput-boolean v5, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z

    .line 82
    const-string v2, "moveToDstCId"

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    .line 84
    sget-object v2, Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "registerPersonaObserver : personaId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", myUserId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", processId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    invoke-direct {p0, v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->registerPersonaObserver(I)V

    .line 91
    iget v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I

    invoke-direct {p0, v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->sendKeyguardCommand(I)V

    .line 94
    .end local v1    # "bValidMoveToAction":Z
    :cond_1
    const/4 v2, 0x2

    return v2

    .line 70
    .restart local v1    # "bValidMoveToAction":Z
    :cond_2
    const-string v2, "movetoKNOX"

    const-string v3, "actions"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    iput-object v6, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mSrcPaths:Ljava/util/List;

    .line 72
    iput-object v6, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mDstPaths:Ljava/util/List;

    .line 73
    iput-boolean v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToContact:Z

    .line 74
    iput-boolean v5, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToFiles:Z

    .line 75
    const-string v2, "requestApp"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestApp:I

    .line 76
    const-string v2, "srcFilePaths"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mSrcPaths:Ljava/util/List;

    .line 77
    const-string v2, "dstFilePaths"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler;->mDstPaths:Ljava/util/List;

    .line 78
    const/4 v1, 0x1

    goto/16 :goto_0
.end method
