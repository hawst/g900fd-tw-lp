.class Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;
.super Ljava/lang/Object;
.source "SwitchB2BActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/activity/SwitchB2BActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KnoxMigrationServiceConnection"
.end annotation


# instance fields
.field mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

.field final synthetic this$0:Lcom/sec/knox/bridge/activity/SwitchB2BActivity;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/activity/SwitchB2BActivity;)V
    .locals 1

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/activity/SwitchB2BActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/activity/SwitchB2BActivity;Lcom/sec/knox/bridge/activity/SwitchB2BActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/activity/SwitchB2BActivity;
    .param p2, "x1"    # Lcom/sec/knox/bridge/activity/SwitchB2BActivity$1;

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;-><init>(Lcom/sec/knox/bridge/activity/SwitchB2BActivity;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "compName"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 158
    const-string v2, "SwitchB2BActivity"

    const-string v3, "KnoxMigrationServiceConnection:onServiceConnected"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-static {p2}, Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    .line 160
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    if-eqz v2, :cond_1

    .line 162
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    invoke-interface {v2}, Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;->getMigrationCurrentState()I

    move-result v1

    .line 163
    .local v1, "migrationState":I
    if-eqz v1, :cond_0

    const/16 v2, 0xc

    if-ne v1, v2, :cond_2

    .line 164
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/activity/SwitchB2BActivity;

    # invokes: Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->switchAndFinish()V
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->access$100(Lcom/sec/knox/bridge/activity/SwitchB2BActivity;)V

    .line 173
    .end local v1    # "migrationState":I
    :cond_1
    :goto_0
    return-void

    .line 166
    .restart local v1    # "migrationState":I
    :cond_2
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    invoke-interface {v2}, Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;->finishMigration()V

    .line 167
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/activity/SwitchB2BActivity;

    invoke-virtual {v2}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->finish()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 169
    .end local v1    # "migrationState":I
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->this$0:Lcom/sec/knox/bridge/activity/SwitchB2BActivity;

    # invokes: Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->switchAndFinish()V
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->access$100(Lcom/sec/knox/bridge/activity/SwitchB2BActivity;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "compName"    # Landroid/content/ComponentName;

    .prologue
    .line 177
    const-string v0, "SwitchB2BActivity"

    const-string v1, "KnoxMigrationServiceConnection:onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    .line 179
    return-void
.end method
