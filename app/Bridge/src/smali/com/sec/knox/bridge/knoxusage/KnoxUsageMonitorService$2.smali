.class final Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$2;
.super Ljava/lang/Object;
.source "KnoxUsageMonitorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const-wide/32 v8, 0x1499700

    .line 196
    const-string v1, "KnoxUsageLogPro"

    const-string v6, "KnoxUsageUpoadTimerThread run"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->loadNextUploadTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 199
    .local v2, "nextUploadTime":J
    const-string v1, "KnoxUsageLogPro"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "next uploadTime loaded from sharedpref:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const-wide/16 v6, -0x1

    cmp-long v1, v6, v2

    if-nez v1, :cond_0

    .line 202
    const-string v1, "KnoxUsageLogPro"

    const-string v6, "KnoxUsageUpoadHandlerThread-next Upload time not valid return"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v2, v6

    .line 207
    .local v4, "timeDifference":J
    cmp-long v1, v4, v8

    if-ltz v1, :cond_1

    .line 208
    const-string v1, "KnoxUsageLogPro"

    const-string v6, "timedifferenc >= SIX_HOURS - send message again"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sget-object v6, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 212
    :cond_1
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-gtz v1, :cond_2

    .line 213
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 214
    .local v0, "moveMsg":Landroid/os/Message;
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 215
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 216
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sget-object v6, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 219
    .end local v0    # "moveMsg":Landroid/os/Message;
    :cond_2
    const-string v1, "KnoxUsageLogPro"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "timedifferenc <= SIX_HOURS- run after :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sget-object v6, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v6, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
