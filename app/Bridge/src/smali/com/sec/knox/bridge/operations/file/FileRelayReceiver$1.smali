.class Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;
.super Ljava/lang/Object;
.source "FileRelayReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->fileMoveByThread(Ljava/lang/String;Ljava/util/ArrayList;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field TotalFileSize:J

.field TotalMoveSize:J

.field mPM:Landroid/os/PersonaManager;

.field mRcpInterface:Landroid/content/IRCPInterface;

.field sessionId:J

.field final synthetic this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

.field final synthetic val$destinationUserId:I

.field final synthetic val$sndPackageName:Ljava/lang/String;

.field final synthetic val$srcPathsOrig:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    iput-object p2, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$srcPathsOrig:Ljava/util/ArrayList;

    iput p4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$destinationUserId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->sessionId:J

    .line 155
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    .line 157
    iput-object v2, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mPM:Landroid/os/PersonaManager;

    .line 158
    iput-object v2, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mRcpInterface:Landroid/content/IRCPInterface;

    return-void
.end method


# virtual methods
.method public fileMove(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 29
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "sndPackageName"    # Ljava/lang/String;
    .param p3, "destinationUserId"    # I

    .prologue
    .line 216
    new-instance v26, Ljava/io/File;

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    .local v26, "srcFile":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " remove dir success: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    const-string v3, "com.sec.knox.container.FileRelayDone"

    const/4 v6, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 229
    :goto_0
    const/16 v18, 0x0

    .line 336
    :goto_1
    return v18

    .line 225
    :cond_0
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " remove dir failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    const-string v3, "com.sec.knox.container.FileRelayFail"

    const/4 v6, 0x3

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    .line 232
    :cond_1
    const/16 v24, 0x0

    .line 234
    .local v24, "retCode":I
    const-string v2, "/"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v19

    .line 235
    .local v19, "lastindex":I
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v28

    .line 236
    .local v28, "srcFolderPath":Ljava/lang/String;
    add-int/lit8 v2, v19, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v27

    .line 237
    .local v27, "srcFileName":Ljava/lang/String;
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " srcFolderPath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; srcFileName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-static/range {v27 .. v27}, Lcom/sec/knox/bridge/util/FileUtils;->isAllowedFileName(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    const-string v3, "com.sec.knox.container.FileRelayFail"

    const/4 v6, 0x5

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 242
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 245
    :cond_2
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 246
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " srcFile is not Exist!! File Move Fail!! : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    const-string v3, "com.sec.knox.container.FileRelayFail"

    const/4 v6, 0x6

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 249
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 251
    :cond_3
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->length()J

    move-result-wide v14

    .line 254
    .local v14, "currentFileSize":J
    const/16 v9, 0x2800

    .line 255
    .local v9, "bufferSize":I
    const/16 v20, 0x0

    .line 256
    .local v20, "offset":I
    const/16 v25, 0x0

    .line 258
    .local v25, "sendcnt":I
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalMoveSize:J

    move-wide/from16 v16, v0

    .line 260
    .local v16, "currentTotalMoveSize":J
    :cond_4
    :goto_2
    move/from16 v0, v20

    int-to-long v4, v0

    cmp-long v2, v4, v14

    if-gez v2, :cond_5

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mRcpInterface:Landroid/content/IRCPInterface;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    # getter for: Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mCurrentUserId:I
    invoke-static {v4}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->access$100(Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;)I

    move-result v3

    move/from16 v0, v20

    int-to-long v7, v0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->sessionId:J

    const/4 v12, 0x1

    move-object/from16 v4, p1

    move/from16 v5, p3

    move-object/from16 v6, p1

    invoke-interface/range {v2 .. v12}, Landroid/content/IRCPInterface;->copyChunks(ILjava/lang/String;ILjava/lang/String;JIJZ)I

    move-result v24

    .line 265
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "From fileMove(): retCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; message="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mRcpInterface:Landroid/content/IRCPInterface;

    move/from16 v0, v24

    invoke-interface {v5, v0}, Landroid/content/IRCPInterface;->getErrorMessage(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    if-eqz v24, :cond_8

    .line 269
    invoke-static/range {v24 .. v24}, Lcom/sec/knox/bridge/util/FileUtils;->getErrorCodeMatch(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v24

    .line 306
    .end local v16    # "currentTotalMoveSize":J
    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalMoveSize:J

    add-long/2addr v4, v14

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalMoveSize:J

    .line 308
    if-nez v24, :cond_b

    .line 309
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_7

    .line 310
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalMoveSize:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    const-wide/16 v10, 0x64

    div-long/2addr v6, v10

    div-long v22, v4, v6

    .line 311
    .local v22, "percentage":J
    const-wide/16 v4, 0x64

    cmp-long v2, v22, v4

    if-lez v2, :cond_6

    .line 312
    const-wide/16 v22, 0x64

    .line 314
    :cond_6
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "End...  TotalMoveSize / TotalFileSize : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalMoveSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " percentage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " %"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    const-string v3, "com.sec.knox.container.FileRelayProgress"

    move-wide/from16 v0, v22

    long-to-int v6, v0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 330
    .end local v22    # "percentage":J
    :cond_7
    :goto_4
    if-nez v24, :cond_c

    const/16 v18, 0x1

    .line 331
    .local v18, "isSuccess":Z
    :goto_5
    if-eqz v18, :cond_d

    const-string v3, "com.sec.knox.container.FileRelayDone"

    .line 332
    .local v3, "intentType":Ljava/lang/String;
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v6, v24

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 333
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v18, :cond_e

    const-string v2, "File Move Success"

    :goto_7
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "; retCode="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "; srcPath="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 273
    .end local v3    # "intentType":Ljava/lang/String;
    .end local v18    # "isSuccess":Z
    .restart local v16    # "currentTotalMoveSize":J
    :cond_8
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->getIsCancelFor(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 274
    const/16 v24, 0x2

    .line 275
    goto/16 :goto_3

    .line 278
    :cond_9
    add-int v20, v20, v9

    .line 280
    add-int/lit8 v25, v25, 0x1

    .line 282
    int-to-long v4, v9

    add-long v16, v16, v4

    .line 284
    move/from16 v0, v25

    rem-int/lit16 v2, v0, 0x3e8

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_4

    .line 285
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    const-wide/16 v6, 0x64

    div-long/2addr v4, v6

    div-long v22, v16, v4

    .line 286
    .restart local v22    # "percentage":J
    const-wide/16 v4, 0x64

    cmp-long v2, v22, v4

    if-lez v2, :cond_a

    .line 287
    const-wide/16 v22, 0x64

    .line 289
    :cond_a
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " currentTotalMoveSize / TotalFileSize : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " percentage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " %"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    const-string v3, "com.sec.knox.container.FileRelayProgress"

    move-wide/from16 v0, v22

    long-to-int v6, v0

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 296
    .end local v16    # "currentTotalMoveSize":J
    .end local v22    # "percentage":J
    :catch_0
    move-exception v21

    .line 297
    .local v21, "re":Landroid/os/RemoteException;
    const/16 v24, 0xa

    .line 298
    invoke-virtual/range {v21 .. v21}, Landroid/os/RemoteException;->printStackTrace()V

    .line 299
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File move fail with RemoteException for: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 300
    .end local v21    # "re":Landroid/os/RemoteException;
    :catch_1
    move-exception v13

    .line 301
    .local v13, "e":Ljava/lang/Exception;
    const/16 v24, 0x7

    .line 302
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 303
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File move fail with Exception for: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 321
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_b
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mRcpInterface:Landroid/content/IRCPInterface;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->sessionId:J

    invoke-interface {v2, v4, v5}, Landroid/content/IRCPInterface;->cancelCopyChunks(J)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_4

    .line 322
    :catch_2
    move-exception v21

    .line 324
    .restart local v21    # "re":Landroid/os/RemoteException;
    invoke-virtual/range {v21 .. v21}, Landroid/os/RemoteException;->printStackTrace()V

    .line 325
    const-string v2, "FileRelayReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fileMove() cancelCopyChunks fail with RemoteException for: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 330
    .end local v21    # "re":Landroid/os/RemoteException;
    :cond_c
    const/16 v18, 0x0

    goto/16 :goto_5

    .line 331
    .restart local v18    # "isSuccess":Z
    :cond_d
    const-string v3, "com.sec.knox.container.FileRelayFail"

    goto/16 :goto_6

    .line 333
    .restart local v3    # "intentType":Ljava/lang/String;
    :cond_e
    const-string v2, "File Move Failed"

    goto/16 :goto_7
.end method

.method public run()V
    .locals 8

    .prologue
    .line 163
    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$srcPathsOrig:Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 164
    .local v2, "srcPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 166
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    # getter for: Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->access$000(Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "persona"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PersonaManager;

    iput-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mPM:Landroid/os/PersonaManager;

    .line 167
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " fileMoveByThread() mPM: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v4}, Landroid/os/PersonaManager;->getRCPInterface()Landroid/content/IRCPInterface;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mRcpInterface:Landroid/content/IRCPInterface;

    .line 170
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " fileMoveByThread() mRcpInterface: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->mRcpInterface:Landroid/content/IRCPInterface;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const/4 v3, 0x0

    .line 174
    .local v3, "successCnt":I
    invoke-static {v2}, Lcom/sec/knox/bridge/util/FileUtils;->getTotalFileSize(Ljava/util/ArrayList;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    .line 176
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalMoveSize:J

    .line 177
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " TotalFileSize : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->TotalFileSize:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 180
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 182
    .local v1, "srcPath":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 183
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " srcPath == null for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 187
    :cond_1
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    iget-object v5, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->getIsCancelFor(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 188
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " process cancel intent: in \'for\' loop "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 192
    :cond_2
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    iget-object v5, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isAllowedApp(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 194
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    iget v5, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$destinationUserId:I

    invoke-virtual {p0, v1, v4, v5}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->fileMove(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 195
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 199
    :cond_3
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Warning!!! Intent sender is not alloewd: sndPackageName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 205
    .end local v1    # "srcPath":Ljava/lang/String;
    :cond_4
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " intent INTENT_FILE_RELAY_COMPLETE sndPackageName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / successCnt : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "i":I
    const-string v4, "com.sec.knox.container.FileRelayComplete"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v0, "i":Landroid/content/Intent;
    const-string v4, "PATH"

    iget-object v5, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$srcPathsOrig:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 209
    const-string v4, "PACKAGENAME"

    iget-object v5, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$sndPackageName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const-string v4, "DESTUSERID"

    iget v5, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->val$destinationUserId:I

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 211
    const-string v4, "SUCCESSCNT"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 212
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;->this$0:Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    # getter for: Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->access$000(Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 213
    return-void
.end method
