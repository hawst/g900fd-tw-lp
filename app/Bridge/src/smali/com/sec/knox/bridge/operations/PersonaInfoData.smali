.class public Lcom/sec/knox/bridge/operations/PersonaInfoData;
.super Ljava/lang/Object;
.source "PersonaInfoData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/operations/PersonaInfoData$1;,
        Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;,
        Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;,
        Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;
    }
.end annotation


# static fields
.field static mCtx:Landroid/content/Context;

.field static sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;


# instance fields
.field mPersonaObr:Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;

.field mSimplePersonaInfo:Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    sput-object v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    .line 37
    sput-object v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;-><init>(Lcom/sec/knox/bridge/operations/PersonaInfoData;Lcom/sec/knox/bridge/operations/PersonaInfoData$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mPersonaObr:Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;

    .line 40
    new-instance v0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    invoke-direct {v0, p0}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;-><init>(Lcom/sec/knox/bridge/operations/PersonaInfoData;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mSimplePersonaInfo:Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    .line 43
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mSimplePersonaInfo:Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    invoke-virtual {v0}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->initialize()V

    .line 44
    return-void
.end method

.method public static getInstance()Lcom/sec/knox/bridge/operations/PersonaInfoData;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/PersonaInfoData;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 47
    sget-object v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    if-nez v0, :cond_1

    .line 48
    const-class v1, Lcom/sec/knox/bridge/operations/PersonaInfoData;

    monitor-enter v1

    .line 49
    :try_start_0
    sget-object v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    if-nez v0, :cond_0

    .line 50
    sput-object p0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;

    invoke-direct {v0}, Lcom/sec/knox/bridge/operations/PersonaInfoData;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    .line 53
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    if-eqz v0, :cond_2

    .line 55
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    sget-object v2, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    iget-object v2, v2, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mPersonaObr:Lcom/sec/knox/bridge/operations/PersonaInfoData$SystemPersonaOberver;

    invoke-virtual {v0, v2}, Landroid/os/PersonaManager;->registerSystemPersonaObserver(Landroid/content/pm/ISystemPersonaObserver;)Z

    .line 61
    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :cond_1
    sget-object v0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->sInstance:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    return-object v0

    .line 58
    :cond_2
    :try_start_1
    const-string v0, "PersonaInfoData"

    const-string v2, "failed to register PersonaObserver!!"

    invoke-static {v0, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getSimplePersonaInfo()Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mSimplePersonaInfo:Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    return-object v0
.end method
