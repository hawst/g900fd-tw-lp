.class Lcom/sec/knox/bridge/operations/ProcessBadgeData$1;
.super Landroid/database/ContentObserver;
.source "ProcessBadgeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/operations/ProcessBadgeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/operations/ProcessBadgeData;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/operations/ProcessBadgeData;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/ProcessBadgeData$1;->this$0:Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 73
    const-string v0, "ProcessBadgeData"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 78
    const-string v1, "ProcessBadgeData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onChange uri "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/ProcessBadgeData$1;->this$0:Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    # invokes: Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getPersonaIds()[I
    invoke-static {v1}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->access$000(Lcom/sec/knox/bridge/operations/ProcessBadgeData;)[I

    move-result-object v0

    .line 83
    .local v0, "ids":[I
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/ProcessBadgeData$1;->this$0:Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    sget-object v2, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->mCtx:Landroid/content/Context;

    # invokes: Lcom/sec/knox/bridge/operations/ProcessBadgeData;->handleObserverOwner(Landroid/content/Context;Landroid/net/Uri;[I)V
    invoke-static {v1, v2, p2, v0}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->access$100(Lcom/sec/knox/bridge/operations/ProcessBadgeData;Landroid/content/Context;Landroid/net/Uri;[I)V

    .line 87
    .end local v0    # "ids":[I
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/ProcessBadgeData$1;->this$0:Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    sget-object v2, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->mCtx:Landroid/content/Context;

    # invokes: Lcom/sec/knox/bridge/operations/ProcessBadgeData;->handleObserverPersona(Landroid/content/Context;Landroid/net/Uri;)V
    invoke-static {v1, v2, p2}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->access$200(Lcom/sec/knox/bridge/operations/ProcessBadgeData;Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method
