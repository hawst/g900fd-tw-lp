.class public Lcom/sec/knox/bridge/util/PersonaInfoMap;
.super Ljava/lang/Object;
.source "PersonaInfoMap.java"


# static fields
.field private static knox1_userid:I

.field private static knox2_userid:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    sput v0, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    .line 34
    sput v0, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    return-void
.end method

.method private static addShortcut(Landroid/content/Context;I)V
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "knox"    # I

    .prologue
    .line 188
    const/4 v2, 0x0

    .line 189
    .local v2, "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 190
    .local v0, "comp":Landroid/content/ComponentName;
    const/4 v1, 0x0

    .line 192
    .local v1, "iconRes":Landroid/content/Intent$ShortcutIconResource;
    const-string v4, "PersonaInfoMap"

    const-string v5, "addShortcut START"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 195
    .local v3, "shortcut":Landroid/content/Intent;
    const/4 v4, 0x1

    if-ne p1, v4, :cond_2

    .line 196
    const-string v4, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    invoke-static {v4, p0}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 197
    if-nez v2, :cond_0

    .line 198
    const v4, 0x7f060037

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 199
    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "comp":Landroid/content/ComponentName;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".activity.SwitchB2BActivity"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    .restart local v0    # "comp":Landroid/content/ComponentName;
    const v4, 0x7f02000e

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 211
    :cond_1
    :goto_0
    if-nez v0, :cond_3

    .line 212
    const-string v4, "PersonaInfoMap"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addShortcut() activity not found "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :goto_1
    return-void

    .line 203
    :cond_2
    const/4 v4, 0x2

    if-ne p1, v4, :cond_1

    .line 204
    const v4, 0x7f060038

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 205
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "comp":Landroid/content/ComponentName;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".activity.SwitchB2BActivity2"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    .restart local v0    # "comp":Landroid/content/ComponentName;
    const v4, 0x7f02000f

    invoke-static {p0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    goto :goto_0

    .line 217
    :cond_3
    const-string v4, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    const-string v4, "duplicate"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 221
    const-string v4, "android.intent.extra.shortcut.INTENT"

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 226
    const-string v4, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 228
    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 230
    const-string v4, "PersonaInfoMap"

    const-string v5, "addShortcut END"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static delShortcut(Landroid/content/Context;I)V
    .locals 26
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "knox"    # I

    .prologue
    .line 234
    const/16 v20, 0x0

    .line 235
    .local v20, "name":Ljava/lang/String;
    const/4 v15, 0x0

    .line 236
    .local v15, "comp":Landroid/content/ComponentName;
    const-string v17, "com.sec.knox.bridge.activity.SwitchB2BActivity"

    .line 237
    .local v17, "extraname":Ljava/lang/String;
    const/4 v14, 0x0

    .line 238
    .local v14, "id":I
    const/16 v24, 0x0

    .line 239
    .local v24, "targetPkg":Ljava/lang/String;
    const-string v22, "com.sec.knox.bridge"

    .line 241
    .local v22, "packagename":Ljava/lang/String;
    const-string v3, "PersonaInfoMap"

    const-string v5, "delShortcut START"

    invoke-static {v3, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    new-instance v23, Landroid/content/Intent;

    const-string v3, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 245
    .local v23, "shortcut":Landroid/content/Intent;
    const/4 v3, 0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_3

    .line 246
    const v3, 0x7f060037

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 247
    new-instance v15, Landroid/content/ComponentName;

    .end local v15    # "comp":Landroid/content/ComponentName;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".activity.SwitchB2BActivity"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v15, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    .restart local v15    # "comp":Landroid/content/ComponentName;
    :cond_0
    :goto_0
    const-string v3, "android.intent.extra.shortcut.NAME"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v3, "android.intent.extra.shortcut.INTENT"

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v15}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 260
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 264
    const/4 v3, 0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_4

    .line 265
    :try_start_0
    const-string v17, "com.sec.knox.bridge.activity.SwitchB2BActivity"

    .line 269
    :cond_1
    :goto_1
    new-instance v18, Landroid/os/Bundle;

    const/4 v3, 0x5

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 270
    .local v18, "extras":Landroid/os/Bundle;
    const-string v3, "android.intent.extra.changed_component_name"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v21, v0

    .line 272
    .local v21, "nameList":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object v17, v21, v3

    .line 273
    const-string v3, "android.intent.extra.changed_component_name_list"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 274
    const-string v3, "android.intent.extra.DONT_KILL_APP"

    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 275
    const-string v3, "com.samsung.android.intent.extra.SECRET_APP"

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 276
    const-string v3, "android.intent.extra.UID"

    const/16 v5, 0x3e8

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 278
    new-instance v4, Landroid/content/Intent;

    const-string v3, "android.intent.action.PACKAGE_CHANGED"

    const-string v5, "package"

    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-static {v5, v0, v6}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 280
    .local v4, "intent":Landroid/content/Intent;
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 281
    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    const-string v3, "android.intent.extra.UID"

    const/4 v5, -0x1

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v25

    .line 285
    .local v25, "uid":I
    if-lez v25, :cond_2

    invoke-static/range {v25 .. v25}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    if-eq v3, v14, :cond_2

    .line 286
    invoke-static/range {v25 .. v25}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v3

    invoke-static {v14, v3}, Landroid/os/UserHandle;->getUid(II)I

    move-result v25

    .line 287
    const-string v3, "android.intent.extra.UID"

    move/from16 v0, v25

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 289
    :cond_2
    const-string v3, "android.intent.extra.user_handle"

    invoke-virtual {v4, v3, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 290
    const/high16 v3, 0x4000000

    invoke-virtual {v4, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 291
    new-instance v19, Ljava/lang/RuntimeException;

    const-string v3, "It\'s not exception, just print call stack"

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 293
    .local v19, "here":Ljava/lang/RuntimeException;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 295
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    .line 297
    .local v2, "am":Landroid/app/IActivityManager;
    const-string v3, "PersonaInfoMap"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sending to user "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    if-eqz v12, :cond_5

    const/4 v12, 0x1

    :goto_2
    const/4 v13, 0x0

    invoke-interface/range {v2 .. v14}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;IZZI)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    .end local v2    # "am":Landroid/app/IActivityManager;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v18    # "extras":Landroid/os/Bundle;
    .end local v19    # "here":Ljava/lang/RuntimeException;
    .end local v21    # "nameList":[Ljava/lang/String;
    .end local v25    # "uid":I
    :goto_3
    const-string v3, "PersonaInfoMap"

    const-string v5, "delShortcut END"

    invoke-static {v3, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    return-void

    .line 249
    :cond_3
    const/4 v3, 0x2

    move/from16 v0, p1

    if-ne v0, v3, :cond_0

    .line 250
    const v3, 0x7f060038

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 251
    new-instance v15, Landroid/content/ComponentName;

    .end local v15    # "comp":Landroid/content/ComponentName;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".activity.SwitchB2BActivity2"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v15, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v15    # "comp":Landroid/content/ComponentName;
    goto/16 :goto_0

    .line 266
    :cond_4
    const/4 v3, 0x2

    move/from16 v0, p1

    if-ne v0, v3, :cond_1

    .line 267
    :try_start_1
    const-string v17, "com.sec.knox.bridge.activity.SwitchB2BActivity2"
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 302
    .restart local v2    # "am":Landroid/app/IActivityManager;
    .restart local v4    # "intent":Landroid/content/Intent;
    .restart local v18    # "extras":Landroid/os/Bundle;
    .restart local v19    # "here":Ljava/lang/RuntimeException;
    .restart local v21    # "nameList":[Ljava/lang/String;
    .restart local v25    # "uid":I
    :cond_5
    const/4 v12, 0x0

    goto :goto_2

    .line 304
    .end local v2    # "am":Landroid/app/IActivityManager;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v18    # "extras":Landroid/os/Bundle;
    .end local v19    # "here":Ljava/lang/RuntimeException;
    .end local v21    # "nameList":[Ljava/lang/String;
    .end local v25    # "uid":I
    :catch_0
    move-exception v16

    .line 305
    .local v16, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v16 .. v16}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method

.method private static findUser(Landroid/content/Context;)V
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 82
    const-string v6, "persona"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 84
    .local v1, "mPersona":Landroid/os/PersonaManager;
    if-eqz v1, :cond_2

    .line 85
    invoke-virtual {v1, v8}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v2

    .line 86
    .local v2, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 87
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PersonaInfo;

    .line 98
    .local v3, "pi":Landroid/content/pm/PersonaInfo;
    const-string v6, "user"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/UserManager;

    .line 99
    .local v5, "um":Landroid/os/UserManager;
    iget v6, v3, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v5, v6}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v4

    .line 100
    .local v4, "ui":Landroid/content/pm/UserInfo;
    const-string v6, "KNOX"

    iget-object v7, v4, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 101
    iget v6, v3, Landroid/content/pm/PersonaInfo;->id:I

    sput v6, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    goto :goto_0

    .line 102
    :cond_1
    const-string v6, "KNOX II"

    iget-object v7, v4, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 103
    iget v6, v3, Landroid/content/pm/PersonaInfo;->id:I

    sput v6, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    goto :goto_0

    .line 110
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v3    # "pi":Landroid/content/pm/PersonaInfo;
    .end local v4    # "ui":Landroid/content/pm/UserInfo;
    .end local v5    # "um":Landroid/os/UserManager;
    :cond_2
    const-string v6, "PersonaInfoMap"

    const-string v7, "fail to get PersonaManager"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    sput v8, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    .line 112
    sput v8, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    .line 115
    :cond_3
    const-string v6, "PersonaInfoMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "findUser : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public static getKnoxUser1()I
    .locals 3

    .prologue
    .line 40
    const-string v0, "PersonaInfoMap"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getKnoxUser1() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    sget v0, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    return v0
.end method

.method public static getKnoxUser2()I
    .locals 3

    .prologue
    .line 46
    const-string v0, "PersonaInfoMap"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getKnoxUser2() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    sget v0, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    return v0
.end method

.method public static hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 336
    const/4 v5, 0x0

    .line 337
    .local v5, "myKnoxAdmin":Ljava/lang/String;
    const/4 v1, 0x0

    .line 339
    .local v1, "iconDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-direct {v0, p0}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;)V

    .line 340
    .local v0, "edManager":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getMyKnoxAdmin()Ljava/lang/String;

    move-result-object v5

    .line 341
    const-string v7, "PersonaInfoMap"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MyKnoxAdmin from EnterpriseDeviceMnagaer:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    if-eqz v5, :cond_0

    if-eqz v5, :cond_1

    const-string v7, "com.samsung.knoxpb.mdm"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 343
    :cond_0
    const-string v7, "PersonaInfoMap"

    const-string v8, "MyKnoxAdmin is null or myknoxAdmin is different"

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 353
    .end local v1    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    .local v2, "iconDrawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v2

    .line 346
    .end local v2    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v1    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/sec/enterprise/ApplicationPolicy;

    move-result-object v6

    .line 347
    .local v6, "policy":Landroid/sec/enterprise/ApplicationPolicy;
    const-string v7, "com.sec.knox.bridge"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/sec/enterprise/ApplicationPolicy;->getApplicationIconFromDb(Ljava/lang/String;I)[B

    move-result-object v3

    .line 348
    .local v3, "imageData":[B
    if-eqz v3, :cond_2

    .line 349
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 350
    .local v4, "is":Ljava/io/ByteArrayInputStream;
    const/4 v7, 0x0

    invoke-static {v4, v7}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 352
    .end local v4    # "is":Ljava/io/ByteArrayInputStream;
    :cond_2
    const-string v7, "PersonaInfoMap"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "New icon got from Application Policy DB:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 353
    .end local v1    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method public static hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p0, "component"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    .line 312
    const-string v7, "PersonaInfoMap"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "hasKnoxTitleChanged:Bridge:Comp:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const/4 v2, 0x0

    .line 314
    .local v2, "newName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 315
    .local v1, "myKnoxAdmin":Ljava/lang/String;
    const/4 v6, 0x0

    .line 317
    .local v6, "realPackageName":Ljava/lang/String;
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-direct {v0, p1}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;)V

    .line 318
    .local v0, "edManager":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getMyKnoxAdmin()Ljava/lang/String;

    move-result-object v1

    .line 319
    const-string v7, "PersonaInfoMap"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "hasKnoxTitleChanged:MyKnoxAdmin from EnterpriseDeviceMnagaer:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    if-eqz v1, :cond_0

    if-eqz v1, :cond_1

    const-string v7, "com.samsung.knoxpb.mdm"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    move-object v3, v2

    .line 332
    .end local v2    # "newName":Ljava/lang/String;
    .local v3, "newName":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 323
    .end local v3    # "newName":Ljava/lang/String;
    .restart local v2    # "newName":Ljava/lang/String;
    :cond_1
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/sec/enterprise/ApplicationPolicy;

    move-result-object v5

    .line 324
    .local v5, "policy":Landroid/sec/enterprise/ApplicationPolicy;
    invoke-virtual {v5, p0, v10}, Landroid/sec/enterprise/ApplicationPolicy;->getApplicationNameFromDb(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 325
    if-nez v2, :cond_2

    .line 327
    const-string v7, "/"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 328
    .local v4, "parts":[Ljava/lang/String;
    aget-object v6, v4, v10

    .line 329
    invoke-virtual {v5, v6, v10}, Landroid/sec/enterprise/ApplicationPolicy;->getApplicationNameFromDb(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 331
    .end local v4    # "parts":[Ljava/lang/String;
    :cond_2
    const-string v7, "PersonaInfoMap"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "New name got from Application Policy DB:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    .line 332
    .end local v2    # "newName":Ljava/lang/String;
    .restart local v3    # "newName":Ljava/lang/String;
    goto :goto_0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 169
    const-string v1, "PersonaInfoMap"

    const-string v2, "initialize()"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const/4 v1, 0x0

    :try_start_0
    sput v1, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    .line 173
    const/4 v1, 0x0

    sput v1, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    .line 174
    invoke-static {p0}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->findUser(Landroid/content/Context;)V

    .line 175
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    :goto_0
    return-void

    .line 179
    :cond_0
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->updateEnabledState(Landroid/content/Context;I)V

    .line 180
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->updateEnabledState(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isAdminLocked(Landroid/content/Context;I)Z
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "userid"    # I

    .prologue
    const/4 v6, 0x0

    .line 52
    const/4 v5, 0x0

    .line 53
    .local v5, "ret":Z
    const/4 v2, 0x0

    .line 55
    .local v2, "mPersona":Landroid/os/PersonaManager;
    if-nez p1, :cond_0

    .line 77
    :goto_0
    return v6

    .line 59
    :cond_0
    const-string v7, "persona"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "mPersona":Landroid/os/PersonaManager;
    check-cast v2, Landroid/os/PersonaManager;

    .line 61
    .restart local v2    # "mPersona":Landroid/os/PersonaManager;
    if-eqz v2, :cond_2

    .line 62
    invoke-virtual {v2, v6}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v3

    .line 63
    .local v3, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 64
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PersonaInfo;

    .line 65
    .local v4, "pi":Landroid/content/pm/PersonaInfo;
    iget v6, v4, Landroid/content/pm/PersonaInfo;->id:I

    if-ne v6, p1, :cond_1

    .line 66
    invoke-virtual {v2, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v6

    sget-object v7, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v6, v7}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v1

    .line 67
    .local v1, "isSuperLocked":Z
    const-string v6, "PersonaInfoMap"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "persona info "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Landroid/content/pm/PersonaInfo;->type:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v4, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, v4, Landroid/content/pm/PersonaInfo;->isUserManaged:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    if-eqz v1, :cond_2

    .line 69
    const/4 v5, 0x1

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "isSuperLocked":Z
    .end local v3    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v4    # "pi":Landroid/content/pm/PersonaInfo;
    :cond_2
    move v6, v5

    .line 77
    goto/16 :goto_0
.end method

.method private static updateEnabledState(Landroid/content/Context;I)V
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "knox"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 119
    invoke-static {p0}, Landroid/os/PersonaManager;->isKioskModeEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 120
    const-string v4, "PersonaInfoMap"

    const-string v5, "updateEnabledState -- Do not create shortcut as kiosk container exists on device!!!"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void

    .line 124
    :cond_0
    const/4 v0, 0x0

    .line 125
    .local v0, "enabled":I
    const/4 v3, 0x0

    .line 126
    .local v3, "userid":I
    const/4 v2, 0x0

    .line 128
    .local v2, "switchActivity":Landroid/content/ComponentName;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 130
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-ne p1, v7, :cond_2

    .line 131
    new-instance v2, Landroid/content/ComponentName;

    .end local v2    # "switchActivity":Landroid/content/ComponentName;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".activity.SwitchB2BActivity"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .restart local v2    # "switchActivity":Landroid/content/ComponentName;
    sget v3, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox1_userid:I

    .line 140
    :cond_1
    :goto_1
    if-nez v2, :cond_3

    .line 141
    const-string v4, "PersonaInfoMap"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateEnabledState() activity not found "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_2
    if-ne p1, v8, :cond_1

    .line 135
    new-instance v2, Landroid/content/ComponentName;

    .end local v2    # "switchActivity":Landroid/content/ComponentName;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".activity.SwitchB2BActivity2"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .restart local v2    # "switchActivity":Landroid/content/ComponentName;
    sget v3, Lcom/sec/knox/bridge/util/PersonaInfoMap;->knox2_userid:I

    goto :goto_1

    .line 147
    :cond_3
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    .line 149
    if-lez v3, :cond_6

    if-eqz v0, :cond_4

    if-ne v0, v8, :cond_6

    .line 152
    :cond_4
    invoke-virtual {v1, v2, v7, v7}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 155
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->addShortcut(Landroid/content/Context;I)V

    .line 165
    :cond_5
    :goto_2
    const-string v4, "PersonaInfoMap"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkB2B() KNOX"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 157
    :cond_6
    if-gtz v3, :cond_5

    if-ne v0, v7, :cond_5

    .line 159
    invoke-virtual {v1, v2, v8, v7}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 162
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->delShortcut(Landroid/content/Context;I)V

    goto :goto_2
.end method
