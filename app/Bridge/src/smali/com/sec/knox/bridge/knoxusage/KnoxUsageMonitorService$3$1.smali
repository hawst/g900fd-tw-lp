.class Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$1;
.super Ljava/lang/Object;
.source "KnoxUsageMonitorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

.field final synthetic val$next_uploadTime:J


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;J)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$1;->this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

    iput-wide p2, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$1;->val$next_uploadTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 286
    const-string v0, "KnoxUsageLogPro"

    const-string v1, " Received MSG_UPLOAD_COMPLETE_FIRST_TIME add entries to Current table"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-wide v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$1;->val$next_uploadTime:J

    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;->addEntriesForExistingUsers(JLandroid/content/Context;)V

    .line 290
    return-void
.end method
