.class public Lcom/sec/knox/bridge/BridgeService;
.super Landroid/app/Service;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;,
        Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;,
        Lcom/sec/knox/bridge/BridgeService$RunnableCallback;,
        Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;,
        Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;,
        Lcom/sec/knox/bridge/BridgeService$IRCPGlobalContactsDirectoryService;,
        Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;,
        Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;,
        Lcom/sec/knox/bridge/BridgeService$IBridgeCommandExecutor;,
        Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;,
        Lcom/sec/knox/bridge/BridgeService$IncomingHandler;,
        Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;
    }
.end annotation


# static fields
.field private static RCP_POLICY_CHANGED:Ljava/lang/String;

.field private static SEANDROID_SECURITY_VERIFICATION:Z

.field private static TAG:Ljava/lang/String;

.field private static mAccountChanged:Z

.field public static mCurrentInvalidKnoxName:Ljava/lang/String;

.field static final mForceSanitizeNotification:[Ljava/lang/String;

.field private static mHandlerDoSync:Landroid/os/Handler;

.field private static mHandlerPolicyChanged:Landroid/os/Handler;

.field private static mListPolicyChanged:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/bridge/util/PolicyChanged;",
            ">;"
        }
    .end annotation
.end field

.field static final mSkipNotificationSync:[Ljava/lang/String;

.field static mbridge:Landroid/os/RCPManager;

.field private static sContext:Landroid/content/Context;

.field static sInstance:Lcom/sec/knox/bridge/BridgeService;


# instance fields
.field private final ACTION_SHORTUCT_MIGRATION:Ljava/lang/String;

.field private BADGE_PROVIDER_URI:Ljava/lang/String;

.field private accountsChangedReceiver:Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;

.field private badgeCountMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field cb:Landroid/os/IRunnableCallback;

.field private isChangedCalendarExportPolicy:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isChangedContactExportPolicy:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBridgeProviderList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/IProviderCallBack;",
            ">;"
        }
    .end annotation
.end field

.field mCallerInfoResult:Landroid/content/CustomCursor;

.field mContext:Landroid/content/Context;

.field public mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

.field public mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

.field mIBridgeCommandExe:Landroid/content/ICommandExeCallBack;

.field mIBridgeProvider:Landroid/content/IProviderCallBack;

.field mIBridgeSync:Landroid/content/ISyncCallBack;

.field mIRCPGlobalContactsDirectoryService:Lcom/sec/knox/bridge/BridgeService$IRCPGlobalContactsDirectoryService;

.field mIRCPInterfaceCallBack:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

.field final mMessenger:Landroid/os/Messenger;

.field public mPm:Landroid/os/PersonaManager;

.field private mShortcutMigration:Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;

.field private mThreadMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private policyChangeReceiver:Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;

.field private preInstalledAppsMinusEmail:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private providers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field result:Landroid/content/CustomCursor;

.field private final resultLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 86
    const-string v0, "samsung.knox.intent.action.RCP_POLICY_CHANGED"

    sput-object v0, Lcom/sec/knox/bridge/BridgeService;->RCP_POLICY_CHANGED:Ljava/lang/String;

    .line 99
    const-string v0, "BridgeProxyService"

    sput-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    .line 116
    sput-object v2, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    .line 117
    sput-object v2, Lcom/sec/knox/bridge/BridgeService;->sInstance:Lcom/sec/knox/bridge/BridgeService;

    .line 147
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "com.sec.android.app.music"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/knox/bridge/BridgeService;->mForceSanitizeNotification:[Ljava/lang/String;

    .line 150
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "com.sec.android.app.voicenote"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/knox/bridge/BridgeService;->mSkipNotificationSync:[Ljava/lang/String;

    .line 158
    sput-boolean v4, Lcom/sec/knox/bridge/BridgeService;->SEANDROID_SECURITY_VERIFICATION:Z

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;

    .line 164
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/BridgeService;->mHandlerDoSync:Landroid/os/Handler;

    .line 165
    sput-object v2, Lcom/sec/knox/bridge/BridgeService;->sContext:Landroid/content/Context;

    .line 166
    sput-boolean v3, Lcom/sec/knox/bridge/BridgeService;->mAccountChanged:Z

    .line 167
    sput-object v2, Lcom/sec/knox/bridge/BridgeService;->mCurrentInvalidKnoxName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 88
    const-string v0, "com.sec.knox.action.SHORTCUT_MIGRATION_FOR_2_3_0"

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->ACTION_SHORTUCT_MIGRATION:Ljava/lang/String;

    .line 105
    iput-object v2, p0, Lcom/sec/knox/bridge/BridgeService;->result:Landroid/content/CustomCursor;

    .line 106
    iput-object v2, p0, Lcom/sec/knox/bridge/BridgeService;->mCallerInfoResult:Landroid/content/CustomCursor;

    .line 107
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;

    .line 109
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    invoke-direct {v0, p0, v2}, Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;-><init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    .line 110
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;

    invoke-direct {v0, p0, v2}, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;-><init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeProvider:Landroid/content/IProviderCallBack;

    .line 111
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;

    invoke-direct {v0, p0, v2}, Lcom/sec/knox/bridge/BridgeService$IBridgeSyncer;-><init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeSync:Landroid/content/ISyncCallBack;

    .line 112
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$IBridgeCommandExecutor;

    invoke-direct {v0, p0, v2}, Lcom/sec/knox/bridge/BridgeService$IBridgeCommandExecutor;-><init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeCommandExe:Landroid/content/ICommandExeCallBack;

    .line 113
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$IRCPGlobalContactsDirectoryService;

    invoke-direct {v0, p0, v2}, Lcom/sec/knox/bridge/BridgeService$IRCPGlobalContactsDirectoryService;-><init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mIRCPGlobalContactsDirectoryService:Lcom/sec/knox/bridge/BridgeService$IRCPGlobalContactsDirectoryService;

    .line 114
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    invoke-direct {v0, p0}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mIRCPInterfaceCallBack:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    .line 119
    iput-object v2, p0, Lcom/sec/knox/bridge/BridgeService;->mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    .line 121
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mBridgeProviderList:Ljava/util/HashMap;

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->providers:Ljava/util/HashMap;

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;

    .line 125
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;

    .line 127
    iput-object v2, p0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    .line 173
    const-string v0, "content://com.sec.badge/apps"

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->BADGE_PROVIDER_URI:Ljava/lang/String;

    .line 1464
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mMessenger:Landroid/os/Messenger;

    .line 2613
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;

    invoke-direct {v0, p0, v2}, Lcom/sec/knox/bridge/BridgeService$RunnableCallback;-><init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->cb:Landroid/os/IRunnableCallback;

    .line 2799
    return-void
.end method

.method static synthetic access$1000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1002(Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Landroid/os/Handler;

    .prologue
    .line 84
    sput-object p0, Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/knox/bridge/BridgeService;->policyChanged()V

    return-void
.end method

.method static synthetic access$1200()Ljava/util/List;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1300(Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-static {p0}, Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mHandlerDoSync:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/BridgeService;->getPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/knox/bridge/BridgeService;->getSyncPolicy()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mBridgeProviderList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/knox/bridge/BridgeService;II)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/BridgeService;->getFilesPolicy(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-static {p0}, Lcom/sec/knox/bridge/BridgeService;->isBridgeCmd(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-static {p0}, Lcom/sec/knox/bridge/BridgeService;->isAllowedPackagesForBridgeCmd(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/knox/bridge/BridgeService;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p1, "x1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService;->bridgeCmd(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900()Z
    .locals 1

    .prologue
    .line 84
    sget-boolean v0, Lcom/sec/knox/bridge/BridgeService;->mAccountChanged:Z

    return v0
.end method

.method static synthetic access$2902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 84
    sput-boolean p0, Lcom/sec/knox/bridge/BridgeService;->mAccountChanged:Z

    return p0
.end method

.method static synthetic access$500(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/knox/bridge/BridgeService;->doSyncForAllSyncers()V

    return-void
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->RCP_POLICY_CHANGED:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/knox/bridge/BridgeService;->checkHandler()V

    return-void
.end method

.method private bridgeCmd(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 17
    .param p1, "bdl"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2854
    const-string v1, "cmd"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2856
    .local v7, "cmd":Ljava/lang/String;
    const-string v1, "contactsquery"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2857
    const-string v1, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2858
    .local v2, "uri":Landroid/net/Uri;
    const-string v1, "projection"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2859
    .local v3, "projection":[Ljava/lang/String;
    const-string v1, "selection"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2860
    .local v4, "selection":Ljava/lang/String;
    const-string v1, "selectionArgs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 2861
    .local v5, "selectionArgs":[Ljava/lang/String;
    const-string v1, "sortOrder"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2863
    .local v6, "sortOrder":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2865
    .local v11, "mCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/knox/bridge/BridgeService;->getProxyResponse(Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v13

    .line 2933
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v4    # "selection":Ljava/lang/String;
    .end local v5    # "selectionArgs":[Ljava/lang/String;
    .end local v6    # "sortOrder":Ljava/lang/String;
    .end local v11    # "mCursor":Landroid/database/Cursor;
    :goto_0
    return-object v13

    .line 2867
    :cond_0
    const-string v1, "contactsinsert"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2868
    const-string v1, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2869
    .restart local v2    # "uri":Landroid/net/Uri;
    const-string v1, "values"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Landroid/content/ContentValues;

    .line 2871
    .local v15, "values":Landroid/content/ContentValues;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v2, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v14

    .line 2872
    .local v14, "result":Landroid/net/Uri;
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 2873
    .local v13, "resp":Landroid/os/Bundle;
    const-string v1, "result"

    invoke-virtual {v13, v1, v14}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 2876
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v13    # "resp":Landroid/os/Bundle;
    .end local v14    # "result":Landroid/net/Uri;
    .end local v15    # "values":Landroid/content/ContentValues;
    :cond_1
    const-string v1, "contactsdelete"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2877
    const-string v1, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2878
    .restart local v2    # "uri":Landroid/net/Uri;
    const-string v1, "selection"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2879
    .restart local v4    # "selection":Ljava/lang/String;
    const-string v1, "selectionArgs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 2881
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v14

    .line 2883
    .local v14, "result":I
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 2884
    .restart local v13    # "resp":Landroid/os/Bundle;
    const-string v1, "result"

    invoke-virtual {v13, v1, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 2887
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v4    # "selection":Ljava/lang/String;
    .end local v5    # "selectionArgs":[Ljava/lang/String;
    .end local v13    # "resp":Landroid/os/Bundle;
    .end local v14    # "result":I
    :cond_2
    const-string v1, "contactsupdate"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2888
    const-string v1, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2889
    .restart local v2    # "uri":Landroid/net/Uri;
    const-string v1, "values"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Landroid/content/ContentValues;

    .line 2890
    .restart local v15    # "values":Landroid/content/ContentValues;
    const-string v1, "selection"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2891
    .restart local v4    # "selection":Ljava/lang/String;
    const-string v1, "selectionArgs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 2893
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v2, v15, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v14

    .line 2895
    .restart local v14    # "result":I
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 2896
    .restart local v13    # "resp":Landroid/os/Bundle;
    const-string v1, "result"

    invoke-virtual {v13, v1, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2899
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v4    # "selection":Ljava/lang/String;
    .end local v5    # "selectionArgs":[Ljava/lang/String;
    .end local v13    # "resp":Landroid/os/Bundle;
    .end local v14    # "result":I
    .end local v15    # "values":Landroid/content/ContentValues;
    :cond_3
    const-string v1, "contactsbulkInsert"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2900
    const-string v1, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2901
    .restart local v2    # "uri":Landroid/net/Uri;
    const-string v1, "values"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, [Landroid/os/Parcelable;

    .line 2903
    .local v15, "values":[Landroid/os/Parcelable;
    const/4 v8, 0x0

    .line 2904
    .local v8, "content":[Landroid/content/ContentValues;
    if-eqz v15, :cond_4

    .line 2905
    array-length v1, v15

    new-array v8, v1, [Landroid/content/ContentValues;

    .line 2906
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    array-length v1, v15

    if-ge v10, v1, :cond_4

    .line 2907
    aget-object v1, v15, v10

    check-cast v1, Landroid/content/ContentValues;

    aput-object v1, v8, v10

    .line 2906
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 2911
    .end local v10    # "i":I
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v2, v8}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v14

    .line 2912
    .restart local v14    # "result":I
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 2913
    .restart local v13    # "resp":Landroid/os/Bundle;
    const-string v1, "result"

    invoke-virtual {v13, v1, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2916
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v8    # "content":[Landroid/content/ContentValues;
    .end local v13    # "resp":Landroid/os/Bundle;
    .end local v14    # "result":I
    .end local v15    # "values":[Landroid/os/Parcelable;
    :cond_5
    const-string v1, "contactsapplyBatch"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2917
    const-string v1, "operations"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 2920
    .local v12, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v14, 0x0

    .line 2922
    .local v14, "result":[Landroid/content/ContentProviderResult;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v16, "com.android.contacts"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0, v12}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 2927
    :goto_2
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 2928
    .restart local v13    # "resp":Landroid/os/Bundle;
    const-string v1, "result"

    invoke-virtual {v13, v1, v14}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    goto/16 :goto_0

    .line 2923
    .end local v13    # "resp":Landroid/os/Bundle;
    :catch_0
    move-exception v9

    .line 2924
    .local v9, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v9}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_2

    .line 2933
    .end local v9    # "e":Landroid/content/OperationApplicationException;
    .end local v12    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v14    # "result":[Landroid/content/ContentProviderResult;
    :cond_6
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method private static checkCallerPermissionFor(Ljava/lang/String;)I
    .locals 5
    .param p0, "methodName"    # Ljava/lang/String;

    .prologue
    .line 178
    const-string v1, "Proxy"

    .line 179
    .local v1, "serviceName":Ljava/lang/String;
    sget-object v2, Lcom/sec/knox/bridge/BridgeService;->sContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/enterprise/knox/seams/SEAMS;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v2, v3, v4, v1, p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->forceAuthorized(IILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 181
    sget-boolean v2, Lcom/sec/knox/bridge/BridgeService;->SEANDROID_SECURITY_VERIFICATION:Z

    if-eqz v2, :cond_0

    .line 182
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security Exception Occurred while pid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] with uid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] trying to access methodName ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] in ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .line 187
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 188
    throw v0

    .line 190
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_0
    sget-object v2, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Security Exception Occurred while pid["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] with uid["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] trying to access methodName ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] in ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] service"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security Exception Occurred while pid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] with uid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] trying to access methodName ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] in ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .line 198
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 201
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method private checkHandler()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 714
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 715
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v1, " onReceive RCP_POLICY_CHANGED removeCallbacksAndMessages "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 717
    sput-object v2, Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;

    .line 719
    :cond_0
    return-void
.end method

.method private doSyncForAllSyncers()V
    .locals 33

    .prologue
    .line 964
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    const-string v31, "SYNCER_PREFS"

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v28

    .line 966
    .local v28, "syncerPreferences":Landroid/content/SharedPreferences;
    invoke-interface/range {v28 .. v28}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v18

    .line 967
    .local v18, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->isEmpty()Z

    move-result v30

    if-nez v30, :cond_18

    .line 968
    const/4 v9, 0x0

    .line 969
    .local v9, "ids":[I
    const/4 v14, 0x0

    .line 970
    .local v14, "isRequiredToSyncContact":Z
    const/4 v13, 0x0

    .line 973
    .local v13, "isRequiredToSyncCalendar":Z
    :try_start_0
    new-instance v24, Landroid/os/Bundle;

    invoke-direct/range {v24 .. v24}, Landroid/os/Bundle;-><init>()V

    .line 974
    .local v24, "req":Landroid/os/Bundle;
    const-string v30, "userid"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v31

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 975
    const-string v30, "action"

    const-string v31, "RequestSyncInfo"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v30

    if-nez v30, :cond_2

    .line 978
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/os/PersonaManager;->getPersonaIds()[I

    move-result-object v9

    .line 980
    if-eqz v9, :cond_4

    .line 981
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v9

    move/from16 v30, v0

    move/from16 v0, v30

    if-ge v8, v0, :cond_4

    .line 982
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    aget v32, v9, v8

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move/from16 v2, v32

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/RCPManager;->exchangeData(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 984
    .local v4, "bundle":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    const-string v30, "isDirtyContact"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 985
    const/4 v14, 0x1

    .line 987
    :cond_0
    if-eqz v4, :cond_1

    const-string v30, "isDirtyCalendar"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 988
    const/4 v13, 0x1

    .line 981
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 994
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v8    # "i":I
    :cond_2
    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v10, v0, [I

    const/16 v30, 0x0

    const/16 v31, 0x0

    aput v31, v10, v30
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 997
    .end local v9    # "ids":[I
    .local v10, "ids":[I
    :try_start_1
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    aget v32, v10, v32

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move/from16 v2, v32

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/RCPManager;->exchangeData(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 999
    .restart local v4    # "bundle":Landroid/os/Bundle;
    if-eqz v4, :cond_19

    .line 1000
    const-string v30, "isDirtyContact"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 1001
    const/4 v14, 0x1

    .line 1003
    :cond_3
    const-string v30, "isDirtyCalendar"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v30

    if-eqz v30, :cond_19

    .line 1004
    const/4 v13, 0x1

    move-object v9, v10

    .line 1016
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v10    # "ids":[I
    .end local v24    # "req":Landroid/os/Bundle;
    .restart local v9    # "ids":[I
    :cond_4
    :goto_1
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "isRequiredToSync "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "/"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 1022
    .local v15, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-static {}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getInstance()Lcom/sec/knox/bridge/operations/PersonaInfoData;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getSimplePersonaInfo()Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    move-result-object v30

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->getExtraInfo(I)Landroid/os/Bundle;

    move-result-object v22

    .line 1025
    .local v22, "persona_info":Landroid/os/Bundle;
    :cond_5
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_18

    .line 1026
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 1027
    .local v20, "pairs":Ljava/util/Map$Entry;
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 1028
    .local v27, "syncerName":Ljava/lang/String;
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 1030
    .local v25, "serviceName":Ljava/lang/String;
    if-eqz v27, :cond_6

    if-nez v25, :cond_7

    .line 1031
    :cond_6
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Issue doSyncForAllSyncers(); skipping null values: syncerName="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "; serviceName="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1009
    .end local v15    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v20    # "pairs":Ljava/util/Map$Entry;
    .end local v22    # "persona_info":Landroid/os/Bundle;
    .end local v25    # "serviceName":Ljava/lang/String;
    .end local v27    # "syncerName":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 1010
    .local v6, "e":Ljava/lang/Exception;
    :goto_3
    const/4 v14, 0x1

    .line 1011
    const/4 v13, 0x1

    .line 1013
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 1036
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v15    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v20    # "pairs":Ljava/util/Map$Entry;
    .restart local v22    # "persona_info":Landroid/os/Bundle;
    .restart local v25    # "serviceName":Ljava/lang/String;
    .restart local v27    # "syncerName":Ljava/lang/String;
    :cond_7
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1037
    .local v8, "i":Landroid/content/Intent;
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, " Sync during switch package+service == "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    const-string v30, "Contacts"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_8

    const-string v30, "Calendar"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_8

    const-string v30, "CallLog"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_8

    const-string v30, "Bookmarks"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1044
    :cond_8
    const-string v30, "knox-import-data"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/bridge/BridgeService;->getPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1045
    .local v23, "policy":Ljava/lang/String;
    if-eqz v23, :cond_e

    const-string v30, "false"

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 1047
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v29

    .line 1048
    .local v29, "userId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1049
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->getParentId(I)I

    move-result v21

    .line 1050
    .local v21, "parent":I
    const-string v30, "dowhat"

    const-string v31, "DELETE"

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1051
    const-string v30, "personaid"

    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1053
    if-eqz v22, :cond_a

    .line 1054
    const-string v30, "Contacts"

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1055
    const-string v30, "false"

    const-string v31, "last_import_contact_status"

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1057
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "SKIP for delete synced data policy = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " userId = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " parent = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " syncerName = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1062
    :cond_9
    const-string v30, "last_import_contact_status"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    :cond_a
    :goto_4
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, " delete synced data policy = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " userId = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " parent = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " syncerName = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    .end local v21    # "parent":I
    .end local v23    # "policy":Ljava/lang/String;
    .end local v29    # "userId":I
    :cond_b
    :goto_5
    new-instance v26, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1147
    .local v26, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    move-object/from16 v0, v26

    iget-object v7, v0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1148
    .local v7, "fullServiceName":Ljava/lang/String;
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 1149
    .local v19, "packageName":Ljava/lang/String;
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, " Sync during switch package == "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "; service == "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    new-instance v5, Landroid/content/ComponentName;

    move-object/from16 v0, v19

    invoke-direct {v5, v0, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    .local v5, "cn":Landroid/content/ComponentName;
    invoke-virtual {v8, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1155
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1156
    .restart local v4    # "bundle":Landroid/os/Bundle;
    new-instance v17, Landroid/os/Messenger;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    move-object/from16 v30, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 1157
    .local v17, "mProxyMessenger":Landroid/os/Messenger;
    const-string v30, "proxy"

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1159
    const-string v30, "binderBundle"

    move-object/from16 v0, v30

    invoke-virtual {v8, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1161
    const-string v30, "Clipboard"

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_16

    .line 1162
    invoke-static/range {p0 .. p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->onStartCommand(Landroid/content/Intent;)I

    goto/16 :goto_2

    .line 1065
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "cn":Landroid/content/ComponentName;
    .end local v7    # "fullServiceName":Ljava/lang/String;
    .end local v17    # "mProxyMessenger":Landroid/os/Messenger;
    .end local v19    # "packageName":Ljava/lang/String;
    .end local v26    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    .restart local v21    # "parent":I
    .restart local v23    # "policy":Ljava/lang/String;
    .restart local v29    # "userId":I
    :cond_c
    const-string v30, "Calendar"

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 1066
    const-string v30, "false"

    const-string v31, "last_import_calendar_status"

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 1069
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "SKIP for delete synced data policy = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " userId = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " parent = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " syncerName = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1074
    :cond_d
    const-string v30, "last_import_calendar_status"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1088
    .end local v21    # "parent":I
    .end local v29    # "userId":I
    :cond_e
    :try_start_2
    const-string v30, "Contacts"

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_12

    .line 1089
    const/4 v12, 0x0

    .line 1090
    .local v12, "isContactSyncPolicy":Z
    if-eqz v22, :cond_f

    .line 1091
    const-string v30, "false"

    const-string v31, "last_import_contact_status"

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_11

    const/4 v12, 0x1

    .line 1094
    :goto_6
    const-string v30, "last_import_contact_status"

    const-string v31, "true"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    :cond_f
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "isContactSyncPolicy : "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    if-nez v12, :cond_10

    if-eqz v14, :cond_5

    .line 1102
    :cond_10
    new-instance v24, Landroid/os/Bundle;

    invoke-direct/range {v24 .. v24}, Landroid/os/Bundle;-><init>()V

    .line 1103
    .restart local v24    # "req":Landroid/os/Bundle;
    const-string v30, "userid"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v31

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1104
    const-string v30, "action"

    const-string v31, "RequestSyncUpdate"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    const-string v30, "cmd"

    const-string v31, "clear"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    const-string v30, "app_name"

    const-string v31, "Contact"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_7
    array-length v0, v9

    move/from16 v30, v0

    move/from16 v0, v16

    move/from16 v1, v30

    if-ge v0, v1, :cond_b

    .line 1109
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    aget v32, v9, v16

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move/from16 v2, v32

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/RCPManager;->exchangeData(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/os/Bundle;

    .line 1108
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 1091
    .end local v16    # "j":I
    .end local v24    # "req":Landroid/os/Bundle;
    :cond_11
    const/4 v12, 0x0

    goto/16 :goto_6

    .line 1112
    .end local v12    # "isContactSyncPolicy":Z
    :cond_12
    const-string v30, "Calendar"

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1113
    const/4 v11, 0x0

    .line 1114
    .local v11, "isCalendarSyncPolicy":Z
    if-eqz v22, :cond_13

    .line 1115
    const-string v30, "false"

    const-string v31, "last_import_calendar_status"

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_15

    const/4 v11, 0x1

    .line 1119
    :goto_8
    const-string v30, "last_import_calendar_status"

    const-string v31, "true"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    :cond_13
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "isCalendarSync : "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    if-nez v11, :cond_14

    if-eqz v13, :cond_5

    .line 1128
    :cond_14
    new-instance v24, Landroid/os/Bundle;

    invoke-direct/range {v24 .. v24}, Landroid/os/Bundle;-><init>()V

    .line 1129
    .restart local v24    # "req":Landroid/os/Bundle;
    const-string v30, "userid"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v31

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1130
    const-string v30, "action"

    const-string v31, "RequestSyncUpdate"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    const-string v30, "cmd"

    const-string v31, "clear"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    const-string v30, "app_name"

    const-string v31, "Calendar"

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    const/16 v16, 0x0

    .restart local v16    # "j":I
    :goto_9
    array-length v0, v9

    move/from16 v30, v0

    move/from16 v0, v16

    move/from16 v1, v30

    if-ge v0, v1, :cond_b

    .line 1135
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    aget v32, v9, v16

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move/from16 v2, v32

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/RCPManager;->exchangeData(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1134
    add-int/lit8 v16, v16, 0x1

    goto :goto_9

    .line 1115
    .end local v16    # "j":I
    .end local v24    # "req":Landroid/os/Bundle;
    :cond_15
    const/4 v11, 0x0

    goto/16 :goto_8

    .line 1140
    .end local v11    # "isCalendarSyncPolicy":Z
    :catch_1
    move-exception v6

    .line 1141
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 1163
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v23    # "policy":Ljava/lang/String;
    .restart local v4    # "bundle":Landroid/os/Bundle;
    .restart local v5    # "cn":Landroid/content/ComponentName;
    .restart local v7    # "fullServiceName":Ljava/lang/String;
    .restart local v17    # "mProxyMessenger":Landroid/os/Messenger;
    .restart local v19    # "packageName":Ljava/lang/String;
    .restart local v26    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :cond_16
    const-string v30, "Notifications"

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_17

    .line 1164
    sget-object v30, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v31, "skip notification sync since it\'s already called with no delay"

    invoke-static/range {v30 .. v31}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1166
    :cond_17
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/knox/bridge/BridgeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2

    .line 1170
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "cn":Landroid/content/ComponentName;
    .end local v7    # "fullServiceName":Ljava/lang/String;
    .end local v8    # "i":Landroid/content/Intent;
    .end local v9    # "ids":[I
    .end local v13    # "isRequiredToSyncCalendar":Z
    .end local v14    # "isRequiredToSyncContact":Z
    .end local v15    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v17    # "mProxyMessenger":Landroid/os/Messenger;
    .end local v19    # "packageName":Ljava/lang/String;
    .end local v20    # "pairs":Ljava/util/Map$Entry;
    .end local v22    # "persona_info":Landroid/os/Bundle;
    .end local v25    # "serviceName":Ljava/lang/String;
    .end local v26    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    .end local v27    # "syncerName":Ljava/lang/String;
    :cond_18
    return-void

    .line 1009
    .restart local v10    # "ids":[I
    .restart local v13    # "isRequiredToSyncCalendar":Z
    .restart local v14    # "isRequiredToSyncContact":Z
    .restart local v24    # "req":Landroid/os/Bundle;
    :catch_2
    move-exception v6

    move-object v9, v10

    .end local v10    # "ids":[I
    .restart local v9    # "ids":[I
    goto/16 :goto_3

    .end local v9    # "ids":[I
    .restart local v4    # "bundle":Landroid/os/Bundle;
    .restart local v10    # "ids":[I
    :cond_19
    move-object v9, v10

    .end local v10    # "ids":[I
    .restart local v9    # "ids":[I
    goto/16 :goto_1
.end method

.method private enableComponent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "compName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 555
    invoke-virtual {p0}, Lcom/sec/knox/bridge/BridgeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 557
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    .local v0, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    .line 559
    .local v3, "setting":I
    if-eq v3, v5, :cond_0

    .line 560
    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    .end local v0    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "setting":I
    :cond_0
    :goto_0
    return-void

    .line 564
    :catch_0
    move-exception v1

    .line 565
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pkg :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", component :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not installed."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private fillPreInstalledAppsMap()V
    .locals 2

    .prologue
    .line 385
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v1, "fill preinstalled apps start"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.app.memo"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 389
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.infraware.polarisviewer5"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 391
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.browser.provider"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 393
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.marvin.talkback"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 395
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 397
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.chrome"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.apps.maps"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 401
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gms"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 403
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gsf"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 405
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.setupwizard"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 407
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.gsf.login"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 409
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.feedback"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 411
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.partnersetup"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 413
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.vending"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 415
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.street"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 417
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.backuptransport"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 419
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.configupdater"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 421
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.syncadapters.contacts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 423
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.syncadapters.calendar"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 425
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.google.android.tts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 427
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.gd.mobicore.pa"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 429
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.baidu.map.location"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 431
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.amap.android.location"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 433
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.locationhistory"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 435
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.calendar"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 437
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.contacts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 439
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.camera"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 441
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.widgetapp.SPlannerAppWidget"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 443
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.widgetapp.digitalclock"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 445
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.music"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 447
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.soundalive"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 449
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.myfiles"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 451
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.samsungapps"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 453
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.osp.app.signin"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 455
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.billing"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 457
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.sbrowser"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 459
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.sprextension"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 461
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.browser"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 463
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.gallery3d"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 465
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.mdm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 467
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.fingerprint.service"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 469
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.android.externalstorage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 471
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.everglades.video"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 473
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.videoplayer"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 475
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.android.app.SamsungContentsAgent"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 477
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.android.app.pinboard"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 479
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.sec.enterprise.mdm.vpn"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 481
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    const-string v1, "com.samsung.helphub"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 482
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v1, "fill preinstall apps end"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    return-void
.end method

.method private getFilesPolicy(II)I
    .locals 3
    .param p1, "containerId"    # I
    .param p2, "policyType"    # I

    .prologue
    .line 918
    const v1, -0xf423f

    .line 919
    .local v1, "retCode":I
    const/4 v0, 0x0

    .line 921
    .local v0, "policy":Z
    const/4 v2, 0x0

    return v2
.end method

.method public static getInstance()Lcom/sec/knox/bridge/BridgeService;
    .locals 1

    .prologue
    .line 2605
    const-string v0, "getInstance"

    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 2606
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->sInstance:Lcom/sec/knox/bridge/BridgeService;

    if-eqz v0, :cond_0

    .line 2607
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->sInstance:Lcom/sec/knox/bridge/BridgeService;

    .line 2610
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "policyProperty"    # Ljava/lang/String;

    .prologue
    .line 886
    const/4 v2, 0x0

    .line 888
    .local v2, "policy":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPolicy: appName(syncer) = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ; policyProperty = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    const-string v4, "persona_policy"

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager;->getPersonaService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaPolicyManager;

    .line 892
    .local v1, "personaPolicyMgr":Landroid/os/PersonaPolicyManager;
    invoke-virtual {v1, p1, p2}, Landroid/os/PersonaPolicyManager;->getRCPDataPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 897
    .end local v1    # "personaPolicyMgr":Landroid/os/PersonaPolicyManager;
    :goto_0
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPolicy: policy value returned = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    return-object v2

    .line 893
    :catch_0
    move-exception v0

    .line 894
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPolicy: threw an exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getProxyResponse(Landroid/database/Cursor;)Landroid/os/Bundle;
    .locals 5
    .param p1, "mCursor"    # Landroid/database/Cursor;

    .prologue
    .line 2833
    const/4 v1, 0x0

    .line 2834
    .local v1, "remoteCursor":Landroid/content/CustomCursor;
    if-eqz p1, :cond_0

    .line 2835
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2836
    new-instance v3, Landroid/database/CursorWindow;

    const-string v4, "contactsquery"

    invoke-direct {v3, v4}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 2838
    .local v3, "window":Landroid/database/CursorWindow;
    new-instance v0, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v0, p1}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 2839
    .local v0, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 2841
    new-instance v1, Landroid/content/CustomCursor;

    .end local v1    # "remoteCursor":Landroid/content/CustomCursor;
    invoke-direct {v1, v3}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 2842
    .restart local v1    # "remoteCursor":Landroid/content/CustomCursor;
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 2843
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/content/CustomCursor;->setAutoClose(Z)V

    .line 2845
    invoke-virtual {v0}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 2848
    .end local v0    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v3    # "window":Landroid/database/CursorWindow;
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2849
    .local v2, "resp":Landroid/os/Bundle;
    const-string v4, "result"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2850
    return-object v2
.end method

.method public static getRcpManager()Landroid/os/RCPManager;
    .locals 1

    .prologue
    .line 2600
    const-string v0, "getRcpManager"

    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 2601
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    return-object v0
.end method

.method private getSyncPolicy()Ljava/lang/String;
    .locals 6

    .prologue
    .line 902
    const/4 v2, 0x0

    .line 904
    .local v2, "policy":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v4, "getSyncPolicy() called"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    const-string v4, "persona_policy"

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager;->getPersonaService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaPolicyManager;

    .line 907
    .local v1, "personaPolicyMgr":Landroid/os/PersonaPolicyManager;
    const-string v3, "Contacts"

    invoke-virtual {v1, v3}, Landroid/os/PersonaPolicyManager;->getSyncPolicy(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 912
    .end local v1    # "personaPolicyMgr":Landroid/os/PersonaPolicyManager;
    :goto_0
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSyncPolicy(): policy value returned = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    return-object v2

    .line 908
    :catch_0
    move-exception v0

    .line 909
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSyncPolicy(): threw an exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initializeShortcutBadgeCountMap()V
    .locals 9

    .prologue
    .line 486
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v2, "Initializing hash map"

    invoke-static {v0, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    const/4 v6, 0x0

    .line 488
    .local v6, "badgeCursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->BADGE_PROVIDER_URI:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 489
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->badgeCountMap:Ljava/util/Map;

    .line 490
    const-string v8, "|"

    .line 494
    .local v8, "separator":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "badgecount"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "class"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "package"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 498
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "USER IS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 502
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 504
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "class name "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "class"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "badgecount"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and package name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "package"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "class"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 518
    .local v7, "packageClassAppended":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->badgeCountMap:Ljava/util/Map;

    const-string v2, "badgecount"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 524
    .end local v7    # "packageClassAppended":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 525
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 524
    :cond_1
    if-eqz v6, :cond_2

    .line 525
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 528
    :cond_2
    return-void
.end method

.method private static isAllowedPackagesForBridgeCmd(Ljava/lang/String;)Z
    .locals 9
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2957
    const/4 v7, 0x2

    new-array v0, v7, [Ljava/lang/String;

    const-string v7, "com.android.providers.contacts"

    aput-object v7, v0, v5

    const-string v7, "com.android.systemui"

    aput-object v7, v0, v6

    .line 2961
    .local v0, "allowedPackages":[Ljava/lang/String;
    if-nez p0, :cond_0

    .line 2962
    sget-object v6, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isAllowedPackagesForBridgeCmd : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2973
    :goto_0
    return v5

    .line 2966
    :cond_0
    move-object v1, v0

    .local v1, "arr$":[Ljava/lang/String;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v1, v2

    .line 2967
    .local v4, "pkg":Ljava/lang/String;
    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v5, v6

    .line 2968
    goto :goto_0

    .line 2966
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2972
    .end local v4    # "pkg":Ljava/lang/String;
    :cond_2
    sget-object v6, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isAllowedPackagesForBridgeCmd : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static isBridgeCmd(Ljava/lang/String;)Z
    .locals 9
    .param p0, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2937
    const/4 v7, 0x6

    new-array v1, v7, [Ljava/lang/String;

    const-string v7, "contactsquery"

    aput-object v7, v1, v5

    const-string v7, "contactsinsert"

    aput-object v7, v1, v6

    const/4 v7, 0x2

    const-string v8, "contactsdelete"

    aput-object v8, v1, v7

    const/4 v7, 0x3

    const-string v8, "contactsupdate"

    aput-object v8, v1, v7

    const/4 v7, 0x4

    const-string v8, "contactsbulkInsert"

    aput-object v8, v1, v7

    const/4 v7, 0x5

    const-string v8, "contactsapplyBatch"

    aput-object v8, v1, v7

    .line 2942
    .local v1, "cmdList":[Ljava/lang/String;
    if-nez p0, :cond_0

    .line 2953
    :goto_0
    return v5

    .line 2946
    :cond_0
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 2947
    .local v4, "pkg":Ljava/lang/String;
    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v5, v6

    .line 2948
    goto :goto_0

    .line 2946
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2952
    .end local v4    # "pkg":Ljava/lang/String;
    :cond_2
    sget-object v6, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isBridgeCmd : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private kioskSetup()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 538
    new-array v0, v6, [[Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.TetherSettings"

    aput-object v3, v2, v5

    aput-object v2, v0, v4

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.Settings$TetherSettingsActivity"

    aput-object v3, v2, v5

    aput-object v2, v0, v5

    .line 548
    .local v0, "enableComponents_com":[[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 549
    aget-object v2, v0, v1

    aget-object v2, v2, v4

    aget-object v3, v0, v1

    aget-object v3, v3, v5

    invoke-direct {p0, v2, v3}, Lcom/sec/knox/bridge/BridgeService;->enableComponent(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 552
    :cond_0
    return-void
.end method

.method private policyChanged()V
    .locals 6

    .prologue
    .line 722
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " RCP_POLICY_CHANGED (this methode should be called only once in 10-15 sec) listPolicyChanged = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 726
    .local v2, "syncerNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/bridge/util/PolicyChanged;

    .line 727
    .local v1, "pc":Lcom/sec/knox/bridge/util/PolicyChanged;
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " RCP_POLICY_CHANGED PolicyChanged pId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->personaId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " syncer = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->syncerName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " policy = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->policyName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " RCP_POLICY_CHANGED syncerNameList "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    iget-object v3, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->syncerName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 731
    iget-object v3, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->syncerName:Ljava/lang/String;

    iget-object v4, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->policyName:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/sec/knox/bridge/BridgeService;->policyChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    const-string v3, "knox-import-data"

    iget-object v4, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->policyName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 733
    iget-object v3, v1, Lcom/sec/knox/bridge/util/PolicyChanged;->syncerName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 736
    .end local v1    # "pc":Lcom/sec/knox/bridge/util/PolicyChanged;
    :cond_1
    sget-object v3, Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 737
    const/4 v3, 0x0

    sput-object v3, Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;

    .line 738
    return-void
.end method

.method private policyChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 18
    .param p1, "syncerName"    # Ljava/lang/String;
    .param p2, "policyName"    # Ljava/lang/String;

    .prologue
    .line 741
    sget-object v15, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " RCP_POLICY_CHANGED syncerName = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    const-string v16, "SYNCER_PREFS"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 744
    .local v13, "syncerPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v13}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v7

    .line 745
    .local v7, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_3

    .line 747
    move-object/from16 v0, p1

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    if-eqz v15, :cond_5

    .line 749
    sget-object v16, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " RCP_POLICY_CHANGED package+service == "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p1

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 751
    .local v5, "i":Landroid/content/Intent;
    new-instance v12, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    move-object/from16 v0, p1

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v15}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 752
    .local v12, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    iget-object v4, v12, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 753
    .local v4, "fullServiceName":Ljava/lang/String;
    iget-object v8, v12, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 754
    .local v8, "packageName":Ljava/lang/String;
    sget-object v15, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " RCP_POLICY_CHANGED package == "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "; service == "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v8, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    .local v3, "cn":Landroid/content/ComponentName;
    invoke-virtual {v5, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 758
    const-string v15, "knox-import-data"

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    const-string v15, "Calendar"

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    const-string v15, "Contacts"

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 761
    :cond_0
    const-string v15, "knox-import-data"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/sec/knox/bridge/BridgeService;->getPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 762
    .local v11, "policyValue":Ljava/lang/String;
    if-eqz v11, :cond_1

    const-string v15, "false"

    invoke-virtual {v15, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 763
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v14

    .line 764
    .local v14, "userId":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v15, v14}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 765
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v15, v14}, Landroid/os/PersonaManager;->getParentId(I)I

    move-result v9

    .line 766
    .local v9, "parent":I
    const-string v15, "dowhat"

    const-string v16, "DELETE"

    move-object/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 767
    const-string v15, "personaid"

    invoke-virtual {v5, v15, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 768
    sget-object v15, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " delete synced data policyName = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " userId = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " parent = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " syncerName = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    .end local v9    # "parent":I
    .end local v14    # "userId":I
    :cond_1
    sget-object v15, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " RCP_POLICY_CHANGED syncerName == "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "; policyValue == "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    invoke-static {}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getInstance()Lcom/sec/knox/bridge/operations/PersonaInfoData;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getSimplePersonaInfo()Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;

    move-result-object v15

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->getExtraInfo(I)Landroid/os/Bundle;

    move-result-object v10

    .line 780
    .local v10, "persona_info":Landroid/os/Bundle;
    const-string v15, "Calendar"

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 781
    const-string v15, "last_import_calendar_status"

    invoke-virtual {v10, v15, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    .end local v10    # "persona_info":Landroid/os/Bundle;
    .end local v11    # "policyValue":Ljava/lang/String;
    :cond_2
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 789
    .local v2, "bundle":Landroid/os/Bundle;
    new-instance v6, Landroid/os/Messenger;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    invoke-direct {v6, v15}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 790
    .local v6, "mProxyMessenger":Landroid/os/Messenger;
    const-string v15, "proxy"

    invoke-virtual {v2, v15, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 791
    const-string v15, "binderBundle"

    invoke-virtual {v5, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 792
    sget-object v15, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " RCP_POLICY_CHANGED started for sync/delete ComponentName = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "cn":Landroid/content/ComponentName;
    .end local v4    # "fullServiceName":Ljava/lang/String;
    .end local v5    # "i":Landroid/content/Intent;
    .end local v6    # "mProxyMessenger":Landroid/os/Messenger;
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v12    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :cond_3
    :goto_1
    return-void

    .line 783
    .restart local v3    # "cn":Landroid/content/ComponentName;
    .restart local v4    # "fullServiceName":Ljava/lang/String;
    .restart local v5    # "i":Landroid/content/Intent;
    .restart local v8    # "packageName":Ljava/lang/String;
    .restart local v10    # "persona_info":Landroid/os/Bundle;
    .restart local v11    # "policyValue":Ljava/lang/String;
    .restart local v12    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :cond_4
    const-string v15, "Contacts"

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 784
    const-string v15, "last_import_contact_status"

    invoke-virtual {v10, v15, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 795
    .end local v3    # "cn":Landroid/content/ComponentName;
    .end local v4    # "fullServiceName":Ljava/lang/String;
    .end local v5    # "i":Landroid/content/Intent;
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v10    # "persona_info":Landroid/os/Bundle;
    .end local v11    # "policyValue":Ljava/lang/String;
    .end local v12    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :cond_5
    sget-object v15, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " RCP_POLICY_CHANGED map.get(syncerName) == null for syncerName "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public getBadgeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 531
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->badgeCountMap:Ljava/util/Map;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 804
    sget-object v1, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v2, " BridgeService onBind"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 807
    .local v0, "callingUid":I
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    .line 809
    sget-object v1, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v2, "onBind Called"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    return-object v1
.end method

.method public onCreate()V
    .locals 28

    .prologue
    .line 207
    invoke-super/range {p0 .. p0}, Landroid/app/Service;->onCreate()V

    .line 208
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    sput-object v24, Lcom/sec/knox/bridge/BridgeService;->sContext:Landroid/content/Context;

    .line 210
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v23

    .line 211
    .local v23, "uid":I
    invoke-static/range {v23 .. v23}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v4

    .line 212
    .local v4, "UserId":I
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "onCreate BridgeService is starting for user "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const-string v25, "FIRST_TIME_PREF"

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 215
    .local v11, "firstTimePreferences":Landroid/content/SharedPreferences;
    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .line 217
    .local v10, "firstTimeEditor":Landroid/content/SharedPreferences$Editor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const-string v25, "persona"

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/os/PersonaManager;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    .line 218
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/BridgeService;->mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    .line 220
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "onCreate BridgeService user id is "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    sput-object p0, Lcom/sec/knox/bridge/BridgeService;->sInstance:Lcom/sec/knox/bridge/BridgeService;

    .line 224
    invoke-static/range {p0 .. p0}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    .line 225
    invoke-static/range {p0 .. p0}, Lcom/sec/knox/bridge/operations/PersonaInfoData;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/PersonaInfoData;

    .line 226
    invoke-static/range {p0 .. p0}, Lcom/sec/knox/bridge/operations/DbObserver;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/DbObserver;

    .line 227
    invoke-static/range {p0 .. p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    .line 228
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v25, "intitalize the badge count hash map"

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->initializeShortcutBadgeCountMap()V

    .line 230
    new-instance v24, Ljava/util/HashSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;

    .line 231
    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->fillPreInstalledAppsMap()V

    .line 233
    const-string v24, "FIRST_TIME"

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-nez v24, :cond_4

    .line 235
    const/16 v12, 0x80

    .line 236
    .local v12, "flags":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v6

    .line 238
    .local v6, "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const-string v25, "PROVIDER_PREFS"

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    .line 240
    .local v18, "providerPreferences":Landroid/content/SharedPreferences;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const-string v25, "SYNCER_PREFS"

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v22

    .line 242
    .local v22, "syncerPreferences":Landroid/content/SharedPreferences;
    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    .line 243
    .local v16, "providerEditor":Landroid/content/SharedPreferences$Editor;
    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    .line 245
    .local v20, "syncerEditor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ApplicationInfo;

    .line 246
    .local v5, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 248
    .local v7, "bundle":Landroid/os/Bundle;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/os/Bundle;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_0

    .line 251
    invoke-virtual {v7}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 253
    .local v15, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_0

    .line 254
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 255
    .local v14, "key":Ljava/lang/String;
    if-eqz v14, :cond_1

    .line 258
    const-string v24, "RCPSyncerName_"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 260
    const/16 v24, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v7, v14, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 261
    .local v19, "serviceName":Ljava/lang/String;
    const-string v24, "RCPSyncerName_"

    const-string v25, ""

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v21

    .line 262
    .local v21, "syncerName":Ljava/lang/String;
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "onCreate(): serviceName = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "; syncerName="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "; UserId="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    if-eqz v19, :cond_1

    .line 265
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "onCreate(): Adding syncer for UserId="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "|"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 268
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 269
    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 271
    .end local v19    # "serviceName":Ljava/lang/String;
    .end local v21    # "syncerName":Ljava/lang/String;
    :cond_2
    const-string v24, "RCPProviderName_"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 273
    const/16 v24, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v7, v14, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 274
    .restart local v19    # "serviceName":Ljava/lang/String;
    const-string v24, "RCPProviderName_"

    const-string v25, ""

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v17

    .line 275
    .local v17, "providerName":Ljava/lang/String;
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "onCreate(): serviceName = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "; providerName="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "; UserId="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    if-eqz v19, :cond_1

    .line 278
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "onCreate(): Adding provider for UserId="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "|"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 281
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 282
    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 305
    .end local v5    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v7    # "bundle":Landroid/os/Bundle;
    .end local v14    # "key":Ljava/lang/String;
    .end local v15    # "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v17    # "providerName":Ljava/lang/String;
    .end local v19    # "serviceName":Ljava/lang/String;
    :cond_3
    const-string v24, "FIRST_TIME"

    const/16 v25, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-interface {v10, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 306
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 309
    .end local v6    # "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v12    # "flags":I
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v16    # "providerEditor":Landroid/content/SharedPreferences$Editor;
    .end local v18    # "providerPreferences":Landroid/content/SharedPreferences;
    .end local v20    # "syncerEditor":Landroid/content/SharedPreferences$Editor;
    .end local v22    # "syncerPreferences":Landroid/content/SharedPreferences;
    :cond_4
    const-string v24, "rcp"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/BridgeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/os/RCPManager;

    sput-object v24, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    .line 311
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;

    const-string v25, " BridgeService onCreate"

    invoke-static/range {v24 .. v25}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    const-string v25, "ALL_PROVIDERS"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeProvider:Landroid/content/IProviderCallBack;

    move-object/from16 v26, v0

    invoke-virtual/range {v24 .. v26}, Landroid/os/RCPManager;->registerProvider(Ljava/lang/String;Landroid/content/IProviderCallBack;)V

    .line 314
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeSync:Landroid/content/ISyncCallBack;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/os/RCPManager;->registerSync(Landroid/content/ISyncCallBack;)V

    .line 315
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeCommandExe:Landroid/content/ICommandExeCallBack;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/os/RCPManager;->registerCommandExe(Landroid/content/ICommandExeCallBack;)V

    .line 316
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mIRCPGlobalContactsDirectoryService:Lcom/sec/knox/bridge/BridgeService$IRCPGlobalContactsDirectoryService;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/os/RCPManager;->registerRCPGlobalContactsDir(Landroid/content/IRCPGlobalContactsDir;)V

    .line 317
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mIRCPInterfaceCallBack:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/os/RCPManager;->registerRCPInterface(Landroid/content/IRCPInterface;)V

    .line 320
    :try_start_0
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->cb:Landroid/os/IRunnableCallback;

    move-object/from16 v26, v0

    invoke-virtual/range {v24 .. v26}, Landroid/os/RCPManager;->registerExchangeData(Landroid/content/Context;Landroid/os/IRunnableCallback;)Z

    .line 322
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v24

    if-eqz v24, :cond_5

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v24

    const/16 v25, 0x64

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_5

    .line 323
    invoke-static {}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->getInstance()Lcom/sec/knox/bridge/operations/ProcessBadgeData;

    move-result-object v24

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Lcom/sec/knox/bridge/operations/ProcessBadgeData;->requestTrySync(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :cond_5
    :goto_1
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 338
    .local v9, "filter":Landroid/content/IntentFilter;
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->RCP_POLICY_CHANGED:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 339
    new-instance v24, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/BridgeService;->policyChangeReceiver:Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->policyChangeReceiver:Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 343
    new-instance v9, Landroid/content/IntentFilter;

    .end local v9    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 344
    .restart local v9    # "filter":Landroid/content/IntentFilter;
    const-string v24, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 345
    new-instance v24, Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/BridgeService;->accountsChangedReceiver:Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->accountsChangedReceiver:Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 349
    new-instance v9, Landroid/content/IntentFilter;

    .end local v9    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 350
    .restart local v9    # "filter":Landroid/content/IntentFilter;
    const-string v24, "com.sec.knox.action.SHORTCUT_MIGRATION_FOR_2_3_0"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 351
    new-instance v24, Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/BridgeService;->mShortcutMigration:Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mShortcutMigration:Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/os/PersonaManager;->isKioskContainerExistOnDevice()Z

    move-result v24

    if-eqz v24, :cond_6

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v24

    const/16 v25, 0x64

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_6

    .line 358
    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/bridge/BridgeService;->kioskSetup()V

    .line 372
    :cond_6
    sget-object v24, Lcom/sec/knox/bridge/BridgeService;->mHandlerDoSync:Landroid/os/Handler;

    new-instance v25, Lcom/sec/knox/bridge/BridgeService$1;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/BridgeService$1;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    const-wide/16 v26, 0xfa0

    invoke-virtual/range {v24 .. v27}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 382
    return-void

    .line 325
    .end local v9    # "filter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v8

    .line 326
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 815
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 817
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->policyChangeReceiver:Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/BridgeService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 819
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService;->mShortcutMigration:Lcom/sec/knox/bridge/BridgeService$ShortcutMigration;

    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/BridgeService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 821
    return-void
.end method
