.class public Lcom/sec/knox/bridge/service/NotificationService;
.super Landroid/app/Service;
.source "NotificationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/service/NotificationService$1;,
        Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;
    }
.end annotation


# instance fields
.field isMigrationRunning:Z

.field private mContext:Landroid/content/Context;

.field private mMigrationConn:Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;

.field private mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

.field private mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

.field personaInfo:Landroid/content/pm/PersonaInfo;

.field private pm:Landroid/os/PersonaManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    .line 31
    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mContext:Landroid/content/Context;

    .line 33
    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->pm:Landroid/os/PersonaManager;

    .line 34
    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->personaInfo:Landroid/content/pm/PersonaInfo;

    .line 35
    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mMigrationConn:Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;

    .line 36
    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->isMigrationRunning:Z

    .line 128
    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/service/NotificationService;)Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/service/NotificationService;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/knox/bridge/service/NotificationService;Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;)Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/service/NotificationService;
    .param p1, "x1"    # Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/knox/bridge/service/NotificationService;->mService:Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/service/NotificationService;)Lcom/sec/knox/bridge/util/NotificationUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/service/NotificationService;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    return-object v0
.end method

.method private connecttoMigrationService()V
    .locals 5

    .prologue
    .line 110
    :try_start_0
    new-instance v2, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;-><init>(Lcom/sec/knox/bridge/service/NotificationService;Lcom/sec/knox/bridge/service/NotificationService$1;)V

    iput-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->mMigrationConn:Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;

    .line 111
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.knox.migrationagent.BIND_MIGRATION_STATE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/knox/bridge/service/NotificationService;->mMigrationConn:Lcom/sec/knox/bridge/service/NotificationService$KnoxMigrationServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/knox/bridge/service/NotificationService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 114
    .local v1, "status":Z
    const-string v2, "NotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connectToMigrationService:status --- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    if-nez v1, :cond_0

    .line 117
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-virtual {v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->showNotification()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    .end local v1    # "status":Z
    :cond_0
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "NotificationService"

    const-string v3, "connectToMigrationService:UnableToStart"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-virtual {v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->showNotification()V

    goto :goto_0
.end method

.method private getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->pm:Landroid/os/PersonaManager;

    if-nez v0, :cond_0

    .line 101
    const-string v0, "persona"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->pm:Landroid/os/PersonaManager;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->pm:Landroid/os/PersonaManager;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 44
    iput-object p0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mContext:Landroid/content/Context;

    .line 45
    new-instance v0, Lcom/sec/knox/bridge/util/NotificationUtil;

    iget-object v1, p0, Lcom/sec/knox/bridge/service/NotificationService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/util/NotificationUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    .line 47
    invoke-virtual {p0}, Lcom/sec/knox/bridge/service/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->initialize(Landroid/content/Context;)V

    .line 49
    const-string v0, "NotificationService"

    const-string v1, "onCreate called for NotificationService"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 54
    const-string v2, "NotificationService"

    const-string v3, "onStartCommand called for NotificationService"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "action":Ljava/lang/String;
    const/4 v1, -0x1

    .line 58
    .local v1, "userId":I
    if-eqz p1, :cond_0

    .line 59
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 60
    const-string v2, "android.intent.extra.user_handle"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 63
    :cond_0
    const-string v2, "android.intent.action.MANAGED_PROFILE_ADDED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 64
    invoke-virtual {p0}, Lcom/sec/knox/bridge/service/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->initialize(Landroid/content/Context;)V

    .line 76
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/knox/bridge/service/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/knox/bridge/service/NotificationService;->getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->pm:Landroid/os/PersonaManager;

    .line 77
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->pm:Landroid/os/PersonaManager;

    if-eqz v2, :cond_2

    .line 78
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->pm:Landroid/os/PersonaManager;

    invoke-virtual {v2, v1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->personaInfo:Landroid/content/pm/PersonaInfo;

    .line 80
    :cond_2
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->personaInfo:Landroid/content/pm/PersonaInfo;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->personaInfo:Landroid/content/pm/PersonaInfo;

    invoke-virtual {v2}, Landroid/content/pm/PersonaInfo;->isMigratedPersona()Z

    move-result v2

    if-nez v2, :cond_5

    .line 81
    const-string v2, "NotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not persona show notification:userId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-virtual {v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->showNotification()V

    .line 90
    :goto_1
    const/4 v2, 0x1

    return v2

    .line 67
    :cond_3
    const-string v2, "com.sec.knox.container.action.containerremovalstarted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 68
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-virtual {v2, v1}, Lcom/sec/knox/bridge/util/NotificationUtil;->cancalNotification(I)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/knox/bridge/service/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->initialize(Landroid/content/Context;)V

    goto :goto_0

    .line 70
    :cond_4
    const-string v2, "samsung.knox.intent.action.MODE_SWITCH_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    iget-object v2, p0, Lcom/sec/knox/bridge/service/NotificationService;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-virtual {v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->cancalNotification()V

    goto :goto_0

    .line 86
    :cond_5
    invoke-direct {p0}, Lcom/sec/knox/bridge/service/NotificationService;->connecttoMigrationService()V

    goto :goto_1
.end method
