.class public Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AccountsChangedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 2790
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$AccountsChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2793
    const-string v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2794
    const/4 v0, 0x1

    # setter for: Lcom/sec/knox/bridge/BridgeService;->mAccountChanged:Z
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$2902(Z)Z

    .line 2796
    :cond_0
    return-void
.end method
