.class Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;
.super Landroid/os/Handler;
.source "MoveToKnoxActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mHandler what = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " arg1 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 157
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Can\'t reach here. Something is wrong"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :goto_0
    monitor-exit p0

    return-void

    .line 98
    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 99
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 103
    :pswitch_1
    :try_start_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/16 v3, 0x5a

    if-lt v2, v3, :cond_0

    .line 104
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    const/4 v3, -0x2

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 106
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 107
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 110
    :pswitch_2
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 111
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 114
    :pswitch_3
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 115
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 118
    :pswitch_4
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 121
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 124
    :pswitch_5
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "errMsg":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 126
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 127
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 129
    if-eqz v0, :cond_1

    .line 130
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "errMsg = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    const/4 v3, 0x0

    # invokes: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showToast(Ljava/lang/String;I)V
    invoke-static {v2, v0, v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$500(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Ljava/lang/String;I)V

    .line 136
    :goto_1
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 133
    :cond_1
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "errMsg is null"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    const v4, 0x7f060004

    invoke-virtual {v3, v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    # invokes: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showToast(Ljava/lang/String;I)V
    invoke-static {v2, v3, v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$500(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Ljava/lang/String;I)V

    goto :goto_1

    .line 139
    .end local v0    # "errMsg":Ljava/lang/String;
    :pswitch_6
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "successMsg":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 141
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "successMsg = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    const/4 v3, 0x0

    # invokes: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showToast(Ljava/lang/String;I)V
    invoke-static {v2, v1, v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$500(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Ljava/lang/String;I)V

    .line 147
    :goto_2
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 144
    :cond_2
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "successMsg is null"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    const v4, 0x7f060005

    invoke-virtual {v3, v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    # invokes: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showToast(Ljava/lang/String;I)V
    invoke-static {v2, v3, v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$500(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Ljava/lang/String;I)V

    goto :goto_2

    .line 150
    .end local v1    # "successMsg":Ljava/lang/String;
    :pswitch_7
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 153
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 154
    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
