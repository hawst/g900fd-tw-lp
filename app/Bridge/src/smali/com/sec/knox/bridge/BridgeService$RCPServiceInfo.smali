.class Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
.super Ljava/lang/Object;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RCPServiceInfo"
.end annotation


# instance fields
.field public packageName:Ljava/lang/String;

.field public serviceName:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V
    .locals 4
    .param p2, "packageNameAndServiceName"    # Ljava/lang/String;

    .prologue
    .line 2579
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2576
    const-string v1, "<NONE>"

    iput-object v1, p0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 2577
    const-string v1, "<NONE>"

    iput-object v1, p0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 2580
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ServiceInfo: packageNameAndServiceName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2581
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 2596
    :cond_0
    :goto_0
    return-void

    .line 2585
    :cond_1
    const-string v1, "|"

    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2586
    .local v0, "index":I
    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 2587
    :cond_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ServiceInfo: index < 1 || (index+ 1) == packageNameAndServiceName.length()"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2592
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 2593
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    goto :goto_0
.end method
