.class public Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;
.super Ljava/lang/Object;
.source "KnoxClipboardDataSync.java"


# static fields
.field static bridgeService:Lcom/sec/knox/bridge/IBridgeService;

.field static sInstance:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;


# instance fields
.field private KNOX_DEBUG:Z

.field TAG:Ljava/lang/String;

.field private clipboardSandBoxRoot:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mRcpProxy:Landroid/content/IRCPInterface;

.field private mSetStartServiceCallers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSyncThread:Ljava/lang/Thread;

.field private removePersona:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    .line 52
    sput-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->sInstance:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "ClipboardDataSyncService"

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->removePersona:Z

    .line 47
    iput-object v1, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSetStartServiceCallers:Ljava/util/List;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mRcpProxy:Landroid/content/IRCPInterface;

    .line 55
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v1, "ClipboardDataSyncService onCreate()"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSetStartServiceCallers:Ljava/util/List;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->removePersona:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->doSync()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    return-void
.end method

.method private delete(Ljava/io/File;)V
    .locals 7
    .param p1, "f"    # Ljava/io/File;

    .prologue
    .line 537
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delete. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 540
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 541
    .local v1, "c":Ljava/io/File;
    invoke-direct {p0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->delete(Ljava/io/File;)V

    .line 540
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 543
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "c":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_1

    .line 544
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to delete "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_1
    return-void
.end method

.method private doSync()V
    .locals 1

    .prologue
    .line 228
    :try_start_0
    invoke-direct {p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->doSyncImpl()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_0
    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private doSyncImpl()V
    .locals 43

    .prologue
    .line 236
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_0

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "Bridge Service is bound, doSync()"

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_1

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync() - go get the user id\'s of who to sync to"

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    const-string v40, "persona"

    invoke-virtual/range {v39 .. v40}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/os/PersonaManager;

    .line 243
    .local v25, "mPM":Landroid/os/PersonaManager;
    invoke-virtual/range {v25 .. v25}, Landroid/os/PersonaManager;->getRCPInterface()Landroid/content/IRCPInterface;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mRcpProxy:Landroid/content/IRCPInterface;

    .line 246
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->setupCbSandbox()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 255
    .local v5, "callingUid":I
    const v39, 0x186a0

    div-int v27, v5, v39

    .line 256
    .local v27, "myUserId":I
    const/16 v37, 0x0

    .line 258
    .local v37, "usersToSyncFrom":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "myUserId = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :try_start_1
    sget-object v39, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    const-string v40, "Clipboard"

    invoke-interface/range {v39 .. v40}, Lcom/sec/knox/bridge/IBridgeService;->getUsersToSyncFrom(Ljava/lang/String;)[I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v37

    .line 268
    if-nez v37, :cond_5

    .line 269
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->removePersona:Z

    move/from16 v39, v0

    if-eqz v39, :cond_2

    .line 271
    :try_start_2
    sget-object v40, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "/data/clipboard"

    move-object/from16 v0, v39

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    if-nez v27, :cond_4

    const-string v39, ""

    :goto_0
    move-object/from16 v0, v41

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v41, "/shared"

    move-object/from16 v0, v39

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Lcom/sec/knox/bridge/IBridgeService;->deleteClipboardFile(Ljava/lang/String;)V

    .line 273
    new-instance v39, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-direct/range {v39 .. v40}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->delete(Ljava/io/File;)V

    .line 275
    new-instance v22, Landroid/content/Intent;

    const-string v39, "com.samsung.knox.clipboard.sync"

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    .local v22, "i":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 277
    sget-object v39, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    invoke-interface/range {v39 .. v39}, Lcom/sec/knox/bridge/IBridgeService;->refreshClipboard()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 283
    .end local v22    # "i":Landroid/content/Intent;
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync() - bridge returned NO users to sync from for clipboard"

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    .end local v5    # "callingUid":I
    .end local v27    # "myUserId":I
    .end local v37    # "usersToSyncFrom":[I
    :cond_3
    :goto_2
    return-void

    .line 247
    :catch_0
    move-exception v16

    .line 248
    .local v16, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync() - Exception trying to setup the sandbox"

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 262
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v5    # "callingUid":I
    .restart local v27    # "myUserId":I
    .restart local v37    # "usersToSyncFrom":[I
    :catch_1
    move-exception v16

    .line 263
    .local v16, "e":Landroid/os/RemoteException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync() - RemoteException trying to get users to sync from bridge"

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 271
    .end local v16    # "e":Landroid/os/RemoteException;
    :cond_4
    :try_start_3
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v39

    goto/16 :goto_0

    .line 278
    :catch_2
    move-exception v16

    .line 279
    .restart local v16    # "e":Landroid/os/RemoteException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_2

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync() - RemoteException thrown clipboard"

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 288
    .end local v16    # "e":Landroid/os/RemoteException;
    :cond_5
    new-instance v18, Ljava/io/File;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "from"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "clips.info"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v18

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 290
    .local v18, "fromSandboxClipInfo":Ljava/io/File;
    new-instance v34, Ljava/io/File;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "to"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "shared_clips.info"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v34

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 293
    .local v34, "toSandboxClipInfo":Ljava/io/File;
    const/16 v31, 0x0

    .local v31, "numUsers":I
    :goto_3
    move-object/from16 v0, v37

    array-length v0, v0

    move/from16 v39, v0

    move/from16 v0, v31

    move/from16 v1, v39

    if-ge v0, v1, :cond_18

    .line 295
    aget v36, v37, v31

    .line 297
    .local v36, "userId":I
    if-gez v36, :cond_7

    .line 293
    :cond_6
    :goto_4
    add-int/lit8 v31, v31, 0x1

    goto :goto_3

    .line 300
    :cond_7
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "/data/clipboard"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    if-nez v36, :cond_9

    const-string v39, ""

    :goto_5
    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "clips.info"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 302
    .local v35, "userClipFile":Ljava/lang/String;
    const/4 v11, -0x1

    .line 304
    .local v11, "copySuccess":I
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mRcpProxy:Landroid/content/IRCPInterface;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v35

    move/from16 v3, v27

    move-object/from16 v4, v41

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/content/IRCPInterface;->copyFile(ILjava/lang/String;ILjava/lang/String;)I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3

    move-result v11

    .line 311
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_8

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "doSync() - copying user "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "\'s clip.info file : \n "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "   to\n"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "\n success? = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_8
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v39

    if-nez v39, :cond_a

    .line 317
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_6

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "doSync() - info file: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, " does not exists! nothing to sync"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 300
    .end local v11    # "copySuccess":I
    .end local v35    # "userClipFile":Ljava/lang/String;
    :cond_9
    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v39

    goto/16 :goto_5

    .line 306
    .restart local v11    # "copySuccess":I
    .restart local v35    # "userClipFile":Ljava/lang/String;
    :catch_3
    move-exception v16

    .line 307
    .restart local v16    # "e":Landroid/os/RemoteException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "doSync() - RemoteException mRcpProxy.copyFile: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 323
    .end local v16    # "e":Landroid/os/RemoteException;
    :cond_a
    new-instance v19, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;-><init>(Ljava/io/File;)V

    .line 328
    .local v19, "fromUserclips":Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_b

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "will try and copy all of user "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "\'s folders to us"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_b
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_6
    invoke-virtual/range {v19 .. v19}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->size()I

    move-result v39

    move/from16 v0, v22

    move/from16 v1, v39

    if-ge v0, v1, :cond_16

    .line 332
    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->getWrap(I)Landroid/sec/clipboard/data/file/WrapFileClipData;

    move-result-object v13

    .line 333
    .local v13, "data":Landroid/sec/clipboard/data/file/WrapFileClipData;
    if-nez v13, :cond_d

    .line 331
    :cond_c
    :goto_7
    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    .line 336
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_e

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "WrapFileClipData "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, ".  = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v13}, Landroid/sec/clipboard/data/file/WrapFileClipData;->getFile()Ljava/io/File;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_e
    invoke-virtual {v13}, Landroid/sec/clipboard/data/file/WrapFileClipData;->getFile()Ljava/io/File;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    .line 345
    .local v10, "clipDir":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    const-string v40, "deletedClips"

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 347
    .local v14, "deletedClips":Landroid/content/SharedPreferences;
    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-interface {v14, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_f

    .line 348
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    .line 349
    .local v16, "e":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x1

    move-object/from16 v0, v16

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 350
    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_7

    .line 354
    .end local v16    # "e":Landroid/content/SharedPreferences$Editor;
    :cond_f
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "to"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 356
    .local v9, "clipDestPath":Ljava/lang/String;
    const/4 v12, -0x1

    .line 359
    .local v12, "copySuccess2":I
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mRcpProxy:Landroid/content/IRCPInterface;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v41

    move/from16 v3, v27

    invoke-interface {v0, v1, v2, v3, v9}, Landroid/content/IRCPInterface;->copyFile(ILjava/lang/String;ILjava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_4

    move-result v12

    .line 365
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_10

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "copying  "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "\n into sandbox/from ="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "\n success? ="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :cond_10
    if-gez v12, :cond_11

    .line 370
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_c

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "failed to copy "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "\n into sandbox/from ="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 360
    :catch_4
    move-exception v16

    .line 361
    .local v16, "e":Landroid/os/RemoteException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync() - RemoteException trying to setup the sandbox"

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 376
    .end local v16    # "e":Landroid/os/RemoteException;
    :cond_11
    new-instance v29, Ljava/io/File;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "to"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 378
    .local v29, "newFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_12

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "copying file \n"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "\n     into     \n"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :cond_12
    invoke-virtual/range {v29 .. v29}, Ljava/io/File;->exists()Z

    move-result v39

    if-eqz v39, :cond_14

    .line 382
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_13

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "file : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, " alread exists... continue"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_13
    invoke-virtual/range {v29 .. v29}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x1f8

    const/16 v41, -0x1

    const/16 v42, -0x1

    invoke-static/range {v39 .. v42}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    goto/16 :goto_7

    .line 390
    :cond_14
    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v10, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->copyDirectory(Ljava/io/File;Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_7

    .line 391
    :catch_5
    move-exception v16

    .line 392
    .local v16, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_15

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "Exception copying wrap."

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 394
    :cond_15
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_7

    .line 399
    .end local v9    # "clipDestPath":Ljava/lang/String;
    .end local v10    # "clipDir":Ljava/io/File;
    .end local v12    # "copySuccess2":I
    .end local v13    # "data":Landroid/sec/clipboard/data/file/WrapFileClipData;
    .end local v14    # "deletedClips":Landroid/content/SharedPreferences;
    .end local v16    # "e":Ljava/io/IOException;
    .end local v29    # "newFile":Ljava/io/File;
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_17

    .line 400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "deleting from/clips.info file so we can copy the new one from the new user in"

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :cond_17
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    goto/16 :goto_4

    .line 410
    .end local v11    # "copySuccess":I
    .end local v19    # "fromUserclips":Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;
    .end local v22    # "i":I
    .end local v35    # "userClipFile":Ljava/lang/String;
    .end local v36    # "userId":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    const-string v40, "deletedClips"

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 412
    .restart local v14    # "deletedClips":Landroid/content/SharedPreferences;
    invoke-interface {v14}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v26

    .line 413
    .local v26, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface/range {v26 .. v26}, Ljava/util/Map;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_1b

    .line 414
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    .line 415
    .local v16, "e":Landroid/content/SharedPreferences$Editor;
    invoke-interface/range {v26 .. v26}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_1a

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/Map$Entry;

    .line 416
    .local v17, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v39

    const/16 v40, 0x0

    invoke-static/range {v40 .. v40}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_19

    .line 417
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_8

    .line 419
    :cond_19
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    const/16 v40, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_8

    .line 422
    .end local v17    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    :cond_1a
    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 425
    .end local v16    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v23    # "i$":Ljava/util/Iterator;
    :cond_1b
    new-instance v33, Ljava/io/File;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "to"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 427
    .local v33, "toDir":Ljava/io/File;
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->exists()Z

    move-result v39

    if-nez v39, :cond_1d

    .line 428
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_1c

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync(). \'to\' dir did not exist... mkdir"

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_1c
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->mkdir()Z

    .line 431
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x1f8

    const/16 v41, -0x1

    const/16 v42, -0x1

    invoke-static/range {v39 .. v42}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    goto/16 :goto_2

    .line 435
    :cond_1d
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "/data/clipboard"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    if-nez v27, :cond_1e

    const-string v39, ""

    :goto_9
    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "shared"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 438
    .local v15, "destDirString":Ljava/lang/String;
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v8

    .line 440
    .local v8, "children":[Ljava/lang/String;
    if-nez v8, :cond_1f

    .line 441
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync(). to folder does not contain any childred! done syncing."

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 435
    .end local v8    # "children":[Ljava/lang/String;
    .end local v15    # "destDirString":Ljava/lang/String;
    :cond_1e
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v39

    goto :goto_9

    .line 446
    .restart local v8    # "children":[Ljava/lang/String;
    .restart local v15    # "destDirString":Ljava/lang/String;
    :cond_1f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_20

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "doSync(). to folder now contains "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    array-length v0, v8

    move/from16 v41, v0

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, " items"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    :cond_20
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    .line 451
    .local v38, "wraps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/sec/clipboard/data/file/WrapFileClipData;>;"
    const/16 v22, 0x0

    .restart local v22    # "i":I
    :goto_a
    array-length v0, v8

    move/from16 v39, v0

    move/from16 v0, v22

    move/from16 v1, v39

    if-ge v0, v1, :cond_26

    .line 453
    new-instance v7, Ljava/io/File;

    aget-object v39, v8, v22

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-direct {v7, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 454
    .local v7, "child":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v39

    if-eqz v39, :cond_23

    .line 455
    invoke-virtual {v7}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v21

    .line 456
    .local v21, "grandGhildren":[Ljava/lang/String;
    const/16 v24, 0x0

    .local v24, "k":I
    :goto_b
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v39, v0

    move/from16 v0, v24

    move/from16 v1, v39

    if-ge v0, v1, :cond_25

    .line 457
    new-instance v20, Ljava/io/File;

    aget-object v39, v21, v24

    move-object/from16 v0, v20

    move-object/from16 v1, v39

    invoke-direct {v0, v7, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 458
    .local v20, "grandChild":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v39

    const-string v40, "clip"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_22

    .line 459
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_21

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "doSync(). found clip in "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_21
    new-instance v32, Landroid/sec/clipboard/data/file/WrapFileClipData;

    const/16 v39, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Landroid/sec/clipboard/data/file/WrapFileClipData;-><init>(Landroid/sec/clipboard/data/ClipboardData;)V

    .line 462
    .local v32, "tempWrap":Landroid/sec/clipboard/data/file/WrapFileClipData;
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 464
    .local v28, "newDirString":Ljava/lang/String;
    new-instance v30, Ljava/io/File;

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    move-object/from16 v2, v39

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    .local v30, "newWrapFile":Ljava/io/File;
    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/data/file/WrapFileClipData;->setFile(Ljava/io/File;)V

    .line 466
    move-object/from16 v0, v38

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    .end local v28    # "newDirString":Ljava/lang/String;
    .end local v30    # "newWrapFile":Ljava/io/File;
    .end local v32    # "tempWrap":Landroid/sec/clipboard/data/file/WrapFileClipData;
    :cond_22
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_b

    .line 470
    .end local v20    # "grandChild":Ljava/io/File;
    .end local v21    # "grandGhildren":[Ljava/lang/String;
    .end local v24    # "k":I
    :cond_23
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v39

    const-string v40, "clip"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_25

    .line 471
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_24

    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "doSync(). found clip in "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    :cond_24
    new-instance v32, Landroid/sec/clipboard/data/file/WrapFileClipData;

    const/16 v39, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Landroid/sec/clipboard/data/file/WrapFileClipData;-><init>(Landroid/sec/clipboard/data/ClipboardData;)V

    .line 474
    .restart local v32    # "tempWrap":Landroid/sec/clipboard/data/file/WrapFileClipData;
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v40, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v7}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 476
    .restart local v28    # "newDirString":Ljava/lang/String;
    new-instance v30, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    move-object/from16 v2, v39

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    .restart local v30    # "newWrapFile":Ljava/io/File;
    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/data/file/WrapFileClipData;->setFile(Ljava/io/File;)V

    .line 478
    move-object/from16 v0, v38

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    .end local v28    # "newDirString":Ljava/lang/String;
    .end local v30    # "newWrapFile":Ljava/io/File;
    .end local v32    # "tempWrap":Landroid/sec/clipboard/data/file/WrapFileClipData;
    :cond_25
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_a

    .line 484
    .end local v7    # "child":Ljava/io/File;
    :cond_26
    new-instance v6, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;

    new-instance v39, Ljava/io/File;

    const-string v40, "shared_clips.info"

    move-object/from16 v0, v39

    move-object/from16 v1, v33

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v39

    invoke-direct {v6, v0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;-><init>(Ljava/io/File;)V

    .line 486
    .local v6, "cfs":Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;
    move-object/from16 v0, v38

    invoke-virtual {v6, v0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;->update(Ljava/util/ArrayList;)V

    .line 489
    :try_start_7
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->saveSharedSandbox(I)V

    .line 492
    new-instance v22, Landroid/content/Intent;

    .end local v22    # "i":I
    const-string v39, "com.samsung.knox.clipboard.sync"

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 493
    .local v22, "i":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 495
    sget-object v39, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    invoke-interface/range {v39 .. v39}, Lcom/sec/knox/bridge/IBridgeService;->refreshClipboard()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_2

    .line 496
    .end local v22    # "i":Landroid/content/Intent;
    :catch_6
    move-exception v16

    .line 497
    .local v16, "e":Landroid/os/RemoteException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 498
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "doSync() - RemoteException finalize clipboard"

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2
.end method

.method private fileCopy(Ljava/io/File;Ljava/io/File;)Z
    .locals 19
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dest"    # Ljava/io/File;

    .prologue
    .line 571
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_0

    .line 572
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "fileCopy "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, " to "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :cond_0
    const/4 v2, 0x0

    .line 575
    .local v2, "Result":Z
    const/4 v10, 0x0

    .line 576
    .local v10, "inputStream":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    .line 579
    .local v13, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->createNewFile()Z

    .line 580
    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .local v11, "inputStream":Ljava/io/FileInputStream;
    move-object v10, v11

    .line 593
    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    :goto_0
    :try_start_1
    new-instance v14, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .end local v13    # "outputStream":Ljava/io/FileOutputStream;
    .local v14, "outputStream":Ljava/io/FileOutputStream;
    move-object v13, v14

    .line 601
    .end local v14    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v13    # "outputStream":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 602
    .local v3, "fcin":Ljava/nio/channels/FileChannel;
    const/4 v8, 0x0

    .line 604
    .local v8, "fcout":Ljava/nio/channels/FileChannel;
    :try_start_2
    invoke-virtual {v10}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 605
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v8

    .line 607
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    .line 608
    .local v6, "lSize":J
    const-wide/16 v4, 0x0

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v16

    .line 610
    .local v16, "result":J
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 611
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 613
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    .line 614
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 615
    cmp-long v4, v6, v16

    if-eqz v4, :cond_9

    .line 616
    const/4 v4, 0x0

    .line 635
    if-eqz v3, :cond_2

    .line 636
    :try_start_3
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 638
    :cond_2
    if-eqz v8, :cond_3

    .line 639
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 641
    :cond_3
    if-eqz v10, :cond_4

    .line 642
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 644
    :cond_4
    if-eqz v13, :cond_5

    .line 645
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 653
    .end local v3    # "fcin":Ljava/nio/channels/FileChannel;
    .end local v6    # "lSize":J
    .end local v8    # "fcout":Ljava/nio/channels/FileChannel;
    .end local v16    # "result":J
    :cond_5
    :goto_2
    return v4

    .line 582
    :catch_0
    move-exception v9

    .line 583
    .local v9, "e":Ljava/io/FileNotFoundException;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_6

    .line 584
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "InputStream~ FileNotFoundException Error : "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_7

    .line 586
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FileCopy~ Source file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "/ Destination file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v4, v2

    .line 587
    goto :goto_2

    .line 588
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v9

    .line 589
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 594
    .end local v9    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v9

    .line 595
    .local v9, "e":Ljava/io/FileNotFoundException;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_8

    .line 596
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "OutputStream~ FileNotFoundException Error : "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_1

    .line 598
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FileCopy~ Source file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "/ Destination file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 647
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "fcin":Ljava/nio/channels/FileChannel;
    .restart local v6    # "lSize":J
    .restart local v8    # "fcout":Ljava/nio/channels/FileChannel;
    .restart local v16    # "result":J
    :catch_3
    move-exception v9

    .line 648
    .local v9, "e":Ljava/io/IOException;
    const-string v5, "BridgeManagerService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "close : "

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v5, v15}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 618
    .end local v9    # "e":Ljava/io/IOException;
    :cond_9
    const/4 v2, 0x1

    .line 619
    :try_start_4
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1f8

    const/4 v15, -0x1

    const/16 v18, -0x1

    move/from16 v0, v18

    invoke-static {v4, v5, v15, v0}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 635
    if-eqz v3, :cond_a

    .line 636
    :try_start_5
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 638
    :cond_a
    if-eqz v8, :cond_b

    .line 639
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 641
    :cond_b
    if-eqz v10, :cond_c

    .line 642
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 644
    :cond_c
    if-eqz v13, :cond_d

    .line 645
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .end local v6    # "lSize":J
    .end local v16    # "result":J
    :cond_d
    :goto_3
    move v4, v2

    .line 653
    goto/16 :goto_2

    .line 647
    .restart local v6    # "lSize":J
    .restart local v16    # "result":J
    :catch_4
    move-exception v9

    .line 648
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v4, "BridgeManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "close : "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 621
    .end local v6    # "lSize":J
    .end local v9    # "e":Ljava/io/IOException;
    .end local v16    # "result":J
    :catch_5
    move-exception v9

    .line 622
    .restart local v9    # "e":Ljava/io/IOException;
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_e

    .line 623
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FileCopy~ IOException Error : "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_f

    .line 625
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FileCopy~ Source file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "/ Destination file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 626
    :cond_f
    const/4 v2, 0x0

    .line 635
    if-eqz v3, :cond_10

    .line 636
    :try_start_7
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 638
    :cond_10
    if-eqz v8, :cond_11

    .line 639
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 641
    :cond_11
    if-eqz v10, :cond_12

    .line 642
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 644
    :cond_12
    if-eqz v13, :cond_d

    .line 645
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_3

    .line 647
    :catch_6
    move-exception v9

    .line 648
    const-string v4, "BridgeManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "close : "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 627
    .end local v9    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v12

    .line 628
    .local v12, "npe":Ljava/lang/NullPointerException;
    :try_start_8
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_13

    .line 629
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FileCopy~ NPE Error : "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v4, :cond_14

    .line 631
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FileCopy~ Source file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "/ Destination file:"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 632
    :cond_14
    const/4 v2, 0x0

    .line 635
    if-eqz v3, :cond_15

    .line 636
    :try_start_9
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 638
    :cond_15
    if-eqz v8, :cond_16

    .line 639
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 641
    :cond_16
    if-eqz v10, :cond_17

    .line 642
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 644
    :cond_17
    if-eqz v13, :cond_d

    .line 645
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    goto/16 :goto_3

    .line 647
    :catch_8
    move-exception v9

    .line 648
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v4, "BridgeManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "close : "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 634
    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "npe":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v4

    .line 635
    if-eqz v3, :cond_18

    .line 636
    :try_start_a
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 638
    :cond_18
    if-eqz v8, :cond_19

    .line 639
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 641
    :cond_19
    if-eqz v10, :cond_1a

    .line 642
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 644
    :cond_1a
    if-eqz v13, :cond_1b

    .line 645
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 650
    :cond_1b
    :goto_4
    throw v4

    .line 647
    :catch_9
    move-exception v9

    .line 648
    .restart local v9    # "e":Ljava/io/IOException;
    const-string v5, "BridgeManagerService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "close : "

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v5, v15}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 63
    sget-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->sInstance:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    if-nez v0, :cond_1

    .line 64
    const-class v1, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    monitor-enter v1

    .line 65
    :try_start_0
    sget-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->sInstance:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    invoke-direct {v0, p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->sInstance:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    .line 67
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mIBridgeBinder:Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;

    sput-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    .line 69
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :cond_1
    sget-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->sInstance:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private isBridgeServiceValid()Z
    .locals 2

    .prologue
    .line 196
    sget-object v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v1, "isBridgeServiceValid(): bridgeService == null"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private declared-synchronized registerRequestStartService(Ljava/lang/String;)V
    .locals 3
    .param p1, "whoWantToStart"    # Ljava/lang/String;

    .prologue
    .line 661
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerRequestStartService():  whoWantToStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSetStartServiceCallers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 663
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSetStartServiceCallers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    :cond_0
    monitor-exit p0

    return-void

    .line 661
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private saveSharedSandbox(I)V
    .locals 8
    .param p1, "toUser"    # I

    .prologue
    .line 503
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "saveSharedSandbox() to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "to"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 507
    .local v2, "sandboxTo":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/clipboard"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p1, :cond_0

    const-string v4, ""

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/shared"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 510
    .local v3, "sharedClipsDestination":Ljava/io/File;
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sharedClipsDestination.getPath() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :try_start_0
    sget-object v4, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->bridgeService:Lcom/sec/knox/bridge/IBridgeService;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/sec/knox/bridge/IBridgeService;->deleteClipboardFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    :goto_1
    const/4 v1, -0x1

    .line 523
    .local v1, "result":I
    :try_start_1
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mRcpProxy:Landroid/content/IRCPInterface;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, p1, v5, v6, v7}, Landroid/content/IRCPInterface;->copyFile(ILjava/lang/String;ILjava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 530
    :goto_2
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result of copying sandbox to our shared? = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    invoke-direct {p0, v2}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->delete(Ljava/io/File;)V

    .line 533
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "from"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->delete(Ljava/io/File;)V

    .line 534
    return-void

    .line 507
    .end local v1    # "result":I
    .end local v3    # "sharedClipsDestination":Ljava/io/File;
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 516
    .restart local v3    # "sharedClipsDestination":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v5, "saveSharedSandbox() exception trying to clear the shared directory"

    invoke-static {v4, v5, v0}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 518
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 525
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "result":I
    :catch_1
    move-exception v0

    .line 526
    .restart local v0    # "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v5, "saveSharedSandbox() exception trying to export sandbox to user"

    invoke-static {v4, v5, v0}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 527
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private setupCbSandbox()V
    .locals 6

    .prologue
    .line 205
    iget-object v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v4, "setupCbSandbox entered "

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ClipboardSandbox"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    .line 208
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    .local v0, "cbSandboxDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 210
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 212
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "from"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 213
    .local v1, "sandboxFrom":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "to"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    .local v2, "sandboxTo":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 216
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 218
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 219
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 221
    :cond_2
    iget-object v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setupCbSandbox Dirs: sandboxRoot = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->clipboardSandBoxRoot:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setupCbSandbox Dirs: sandboxRoot From = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setupCbSandbox Dirs: sandboxRoot To = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method private stopMySelf(Ljava/lang/String;)V
    .locals 0
    .param p1, "whoWantToStop"    # Ljava/lang/String;

    .prologue
    .line 672
    return-void
.end method


# virtual methods
.method public copyDirectory(Ljava/io/File;Ljava/io/File;)V
    .locals 6
    .param p1, "sourceLocation"    # Ljava/io/File;
    .param p2, "targetLocation"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 549
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 550
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 551
    invoke-virtual {p2}, Ljava/io/File;->mkdir()Z

    .line 552
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1f8

    invoke-static {v3, v4, v5, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 555
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 556
    .local v0, "children":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_2

    .line 557
    new-instance v3, Ljava/io/File;

    aget-object v4, v0, v1

    invoke-direct {v3, p1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    aget-object v5, v0, v1

    invoke-direct {v4, p2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 556
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 561
    .end local v0    # "children":[Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    .line 562
    .local v2, "result":Z
    if-nez v2, :cond_2

    .line 563
    iget-boolean v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v3, :cond_2

    .line 564
    iget-object v3, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to copy from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    .end local v2    # "result":Z
    :cond_2
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;)I
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 76
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onStartCommand() entered - Current user = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    if-nez p1, :cond_1

    .line 80
    iget-boolean v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v10, :cond_0

    .line 81
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v11, "onStartCommand(): incoming intent is null."

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_0
    const-string v10, "<DUMMY>"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    .line 83
    const/4 v10, 0x3

    .line 192
    :goto_0
    return v10

    .line 86
    :cond_1
    const-string v2, ""

    .line 87
    .local v2, "doWhat":Ljava/lang/String;
    const-string v10, "dowhat"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 88
    const-string v10, "dowhat"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 90
    :cond_2
    iget-boolean v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v10, :cond_3

    .line 91
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onStartCommand() doWhat : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_3
    const-string v10, "DELETE"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 94
    const-string v10, "personaid"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 95
    .local v7, "personaID":I
    iget-boolean v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v10, :cond_4

    .line 96
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onStartCommand() DELETE personaID : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_4
    const/4 v10, -0x1

    if-eq v7, v10, :cond_8

    .line 98
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v10}, Ljava/lang/Thread;->isAlive()Z

    move-result v10

    if-nez v10, :cond_9

    .line 99
    :cond_5
    invoke-direct {p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->isBridgeServiceValid()Z

    move-result v10

    if-nez v10, :cond_7

    .line 100
    iget-boolean v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v10, :cond_6

    .line 101
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v11, "onStartCommand() failed; bridgeService is not valid."

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_6
    const-string v10, "<DUMMY>"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    .line 104
    :cond_7
    const-string v10, "DELETE"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->registerRequestStartService(Ljava/lang/String;)V

    .line 105
    new-instance v10, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;

    invoke-direct {v10, p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;-><init>(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)V

    iput-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    .line 114
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 192
    .end local v7    # "personaID":I
    :cond_8
    :goto_1
    const/4 v10, 0x3

    goto/16 :goto_0

    .line 116
    .restart local v7    # "personaID":I
    :cond_9
    iget-boolean v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v10, :cond_a

    .line 117
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v11, "onStartCommand(); mSyncThread == null || !mSyncThread.isAlive()... "

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_a
    const-string v10, "<DUMMY>"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    goto :goto_1

    .line 122
    .end local v7    # "personaID":I
    :cond_b
    const-string v10, "CLIP_REMOVED"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 123
    const-string v10, "CLIP_REMOVED"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->registerRequestStartService(Ljava/lang/String;)V

    .line 124
    const-string v10, "reADD"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 125
    .local v8, "reADD":Z
    if-nez v8, :cond_d

    .line 126
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v11, "@@reADD : false ori"

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v10, "RemovedClipId"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 128
    .local v9, "removedClipId":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onStartCommand() CLIP_REMOVED - removedClipId = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    if-eqz v9, :cond_c

    .line 130
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    const-string v11, "deletedClips"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 132
    .local v1, "deletedClips":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 133
    .local v3, "e":Landroid/content/SharedPreferences$Editor;
    const/4 v10, 0x0

    invoke-interface {v3, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 137
    .end local v1    # "deletedClips":Landroid/content/SharedPreferences;
    .end local v3    # "e":Landroid/content/SharedPreferences$Editor;
    :cond_c
    const-string v10, "CLIP_REMOVED"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    goto :goto_1

    .line 139
    .end local v9    # "removedClipId":Ljava/lang/String;
    :cond_d
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v11, "@@reADD :true new"

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v10, "RemovedClipId"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 141
    .restart local v9    # "removedClipId":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onStartCommand() CLIP_REMOVED - removedClipId = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    if-eqz v9, :cond_e

    .line 143
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    const-string v11, "deletedClips"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 145
    .restart local v1    # "deletedClips":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 146
    .restart local v3    # "e":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v3, v9}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 148
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 150
    .end local v1    # "deletedClips":Landroid/content/SharedPreferences;
    .end local v3    # "e":Landroid/content/SharedPreferences$Editor;
    :cond_e
    const-string v10, "CLIP_REMOVED"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 152
    .end local v8    # "reADD":Z
    .end local v9    # "removedClipId":Ljava/lang/String;
    :cond_f
    const-string v10, "CLIPS_CLEARED"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 153
    const-string v10, "CLIPS_CLEARED"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->registerRequestStartService(Ljava/lang/String;)V

    .line 154
    const-string v10, "ClearedClipsArray"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 156
    .local v0, "clearedClipsArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onStartCommand() CLIPS_CLEARED - clearedClipsArray = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mContext:Landroid/content/Context;

    const-string v11, "deletedClips"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 159
    .restart local v1    # "deletedClips":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 160
    .restart local v3    # "e":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 161
    .local v4, "entry":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 162
    .local v5, "f":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 163
    .restart local v9    # "removedClipId":Ljava/lang/String;
    const/4 v10, 0x0

    invoke-interface {v3, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 166
    .end local v4    # "entry":Ljava/lang/String;
    .end local v5    # "f":Ljava/io/File;
    .end local v9    # "removedClipId":Ljava/lang/String;
    :cond_10
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 167
    const-string v10, "CLIPS_CLEARED"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 169
    .end local v0    # "clearedClipsArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "deletedClips":Landroid/content/SharedPreferences;
    .end local v3    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_11
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    if-eqz v10, :cond_12

    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v10}, Ljava/lang/Thread;->isAlive()Z

    move-result v10

    if-nez v10, :cond_15

    .line 170
    :cond_12
    invoke-direct {p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->isBridgeServiceValid()Z

    move-result v10

    if-nez v10, :cond_14

    .line 171
    iget-boolean v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v10, :cond_13

    .line 172
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v11, "onStartCommand() failed; bridgeService is not valid."

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_13
    const-string v10, "<DUMMY>"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    .line 175
    :cond_14
    const-string v10, "DO_SYNC"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->registerRequestStartService(Ljava/lang/String;)V

    .line 176
    new-instance v10, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;

    invoke-direct {v10, p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;-><init>(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)V

    iput-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    .line 184
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->mSyncThread:Ljava/lang/Thread;

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 186
    :cond_15
    iget-boolean v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z

    if-eqz v10, :cond_16

    .line 187
    iget-object v10, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v11, "onStartCommand(); mSyncThread == null || !mSyncThread.isAlive()... "

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_16
    const-string v10, "<DUMMY>"

    invoke-direct {p0, v10}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
