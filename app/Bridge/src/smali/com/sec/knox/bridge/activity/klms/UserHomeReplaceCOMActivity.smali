.class public Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;
.super Landroid/app/Activity;
.source "UserHomeReplaceCOMActivity.java"


# static fields
.field private static LOCK_TYPE:Landroid/content/pm/PersonaState;


# instance fields
.field isKioskEnabled:Z

.field private mPersona:Landroid/os/PersonaManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->isKioskEnabled:Z

    return-void
.end method

.method private getKioskContainerId()I
    .locals 5

    .prologue
    .line 137
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    if-eqz v3, :cond_1

    .line 138
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v1

    .line 139
    .local v1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 140
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 141
    .local v2, "pi":Landroid/content/pm/PersonaInfo;
    if-eqz v2, :cond_0

    iget-boolean v3, v2, Landroid/content/pm/PersonaInfo;->isKioskModeEnabled:Z

    if-eqz v3, :cond_0

    .line 142
    iget v3, v2, Landroid/content/pm/PersonaInfo;->id:I

    .line 147
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v2    # "pi":Landroid/content/pm/PersonaInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, -0x1

    goto :goto_0
.end method

.method private isAdminLocked(I)Z
    .locals 8
    .param p1, "userid"    # I

    .prologue
    .line 105
    const/4 v3, 0x0

    .line 107
    .local v3, "ret":Z
    sget-object v4, Landroid/content/pm/PersonaState;->INVALID:Landroid/content/pm/PersonaState;

    sput-object v4, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    .line 109
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    if-eqz v4, :cond_2

    .line 110
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v1

    .line 111
    .local v1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 112
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 113
    .local v2, "pi":Landroid/content/pm/PersonaInfo;
    iget v4, v2, Landroid/content/pm/PersonaInfo;->id:I

    if-ne v4, p1, :cond_0

    .line 115
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v4, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v4

    sget-object v5, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v4, v5}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 116
    const/4 v3, 0x1

    .line 118
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v4, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v4

    sput-object v4, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    .line 120
    :cond_1
    const-string v4, "UserHomeReplaceCOMActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "persona info "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/content/pm/PersonaInfo;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v6, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v6

    sget-object v7, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v6, v7}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v2    # "pi":Landroid/content/pm/PersonaInfo;
    :cond_2
    return v3
.end method

.method private startActivityAdminLocked(I)V
    .locals 5
    .param p1, "userid"    # I

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x1

    .line 84
    .local v1, "lockType":I
    sget-object v2, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    sget-object v3, Landroid/content/pm/PersonaState;->LICENSE_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v2, v3}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    const/4 v1, 0x2

    .line 87
    :cond_0
    const-string v2, "UserHomeReplaceCOMActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startActivityAdminLocked(): lockType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->getKnoxUser1()I

    move-result v2

    if-ne p1, v2, :cond_1

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".activity.klms.ContainerKLMSLockedActivity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    :goto_0
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 97
    const-string v2, "type"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 98
    const-string v2, "userid"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->startActivity(Landroid/content/Intent;)V

    .line 101
    const-string v2, "UserHomeReplaceCOMActivity"

    const-string v3, "startActivityAdminLocked done."

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void

    .line 93
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".activity.klms.ContainerKLMSLockedActivity2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->isKioskEnabled:Z

    if-eqz v0, :cond_0

    .line 155
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const-string v0, "UserHomeReplaceCOMActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->setContentView(I)V

    .line 64
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 69
    const-string v1, "UserHomeReplaceCOMActivity"

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v1, "persona"

    invoke-virtual {p0, v1}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    iput-object v1, p0, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->mPersona:Landroid/os/PersonaManager;

    .line 72
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->getKioskContainerId()I

    move-result v0

    .line 74
    .local v0, "kioskContainerId":I
    invoke-direct {p0, v0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->isAdminLocked(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    invoke-direct {p0, v0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->startActivityAdminLocked(I)V

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/UserHomeReplaceCOMActivity;->finish()V

    .line 78
    return-void
.end method
