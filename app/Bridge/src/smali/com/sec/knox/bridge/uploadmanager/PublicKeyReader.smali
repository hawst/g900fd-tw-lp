.class public Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;
.super Ljava/lang/Object;
.source "PublicKeyReader.java"


# direct methods
.method private static decryptF([BLjava/security/Key;Ljavax/crypto/Cipher;)Ljava/lang/String;
    .locals 3
    .param p0, "encryptionBytes"    # [B
    .param p1, "pkey"    # Ljava/security/Key;
    .param p2, "c"    # Ljavax/crypto/Cipher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljavax/crypto/BadPaddingException;,
            Ljavax/crypto/IllegalBlockSizeException;
        }
    .end annotation

    .prologue
    .line 88
    const/4 v2, 0x2

    invoke-virtual {p2, v2, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 90
    invoke-virtual {p2, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 92
    .local v0, "decrypt":[B
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 94
    .local v1, "decrypted":Ljava/lang/String;
    return-object v1
.end method

.method private static extractSecSHBase64(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "_key"    # Ljava/lang/String;

    .prologue
    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .local v1, "base64Data":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 146
    .local v7, "startKey":Z
    const/4 v8, 0x0

    .line 147
    .local v8, "startKeyBody":Z
    const/4 v2, 0x0

    .line 148
    .local v2, "endKey":Z
    const/4 v6, 0x0

    .line 149
    .local v6, "nextLineIsHeader":Z
    const-string v10, "\n"

    invoke-virtual {p0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v5, v0, v3

    .line 150
    .local v5, "line":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 151
    .local v9, "trimLine":Ljava/lang/String;
    if-nez v7, :cond_1

    const-string v10, "-----BEGIN PUBLIC KEY-----"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 152
    const/4 v7, 0x1

    .line 149
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 153
    :cond_1
    if-eqz v7, :cond_0

    .line 154
    const-string v10, "-----END PUBLIC KEY-----"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 155
    const/4 v2, 0x1

    .line 174
    .end local v5    # "line":Ljava/lang/String;
    .end local v9    # "trimLine":Ljava/lang/String;
    :cond_2
    if-nez v2, :cond_3

    .line 175
    const-string v10, "getEncryptedAESSessionKey : bytes not found Corrupt SECSSH public key string"

    invoke-static {v10}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 179
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    .line 157
    .restart local v5    # "line":Ljava/lang/String;
    .restart local v9    # "trimLine":Ljava/lang/String;
    :cond_4
    if-eqz v6, :cond_5

    .line 158
    const-string v10, "\\"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 159
    const/4 v6, 0x0

    goto :goto_1

    .line 161
    :cond_5
    const/16 v10, 0x3a

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    if-lez v10, :cond_7

    .line 162
    if-eqz v8, :cond_6

    .line 163
    const-string v10, "getEncryptedAESSessionKey : bytes not found Corrupt SECSSH public key string"

    invoke-static {v10}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 164
    :cond_6
    const-string v10, "\\"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 165
    const/4 v6, 0x1

    goto :goto_1

    .line 168
    :cond_7
    const/4 v8, 0x1

    .line 169
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static getBaseTable(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "pubfilename"    # Ljava/lang/String;

    .prologue
    .line 101
    const/4 v9, 0x0

    .line 102
    .local v9, "fInStream":Ljava/io/InputStream;
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 103
    .local v0, "am":Landroid/content/res/AssetManager;
    const/4 v2, 0x0

    .line 104
    .local v2, "bufferedInputStream":Ljava/io/BufferedInputStream;
    const/4 v4, 0x0

    .line 106
    .local v4, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "certificates/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v9

    .line 108
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 109
    .end local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .local v3, "bufferedInputStream":Ljava/io/BufferedInputStream;
    :try_start_1
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 111
    .end local v4    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .local v5, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v12, 0x64

    :try_start_2
    new-array v1, v12, [B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 115
    .local v1, "buffer":[B
    :goto_0
    :try_start_3
    invoke-virtual {v3, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v10

    .local v10, "length":I
    if-lez v10, :cond_1

    .line 116
    const/4 v12, 0x0

    invoke-virtual {v5, v1, v12, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 118
    .end local v10    # "length":I
    :catch_0
    move-exception v6

    .line 119
    .local v6, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 120
    if-eqz v5, :cond_0

    .line 121
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 123
    :cond_0
    if-eqz v3, :cond_1

    .line 124
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 127
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 128
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 129
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 130
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    .line 131
    .local v7, "encryptedBaseTable":[B
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v7}, Ljava/lang/String;-><init>([B)V

    .line 132
    .local v11, "strDecBaseTable":Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->extractSecSHBase64(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v8

    .local v8, "extractedKey":Ljava/lang/String;
    move-object v4, v5

    .end local v5    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    move-object v2, v3

    .line 139
    .end local v1    # "buffer":[B
    .end local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v7    # "encryptedBaseTable":[B
    .end local v8    # "extractedKey":Ljava/lang/String;
    .end local v11    # "strDecBaseTable":Ljava/lang/String;
    .restart local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :goto_1
    return-object v8

    .line 135
    :catch_1
    move-exception v6

    .line 136
    .restart local v6    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 139
    const/4 v8, 0x0

    goto :goto_1

    .line 135
    .end local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :catch_2
    move-exception v6

    move-object v2, v3

    .end local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_2

    .end local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v4    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v5    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    :catch_3
    move-exception v6

    move-object v4, v5

    .end local v5    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    move-object v2, v3

    .end local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_2
.end method

.method private static getDecryptedKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 62
    const/4 v7, 0x2

    invoke-static {p0, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    .line 63
    .local v4, "encrpt":[B
    invoke-static {}, Lcom/sec/knox/bridge/uploadmanager/SecureKeyLoader;->getTyrfingr()Ljava/lang/String;

    move-result-object v6

    .line 64
    .local v6, "tyrfingr":Ljava/lang/String;
    new-instance v5, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x18

    const-string v10, "AES"

    invoke-direct {v5, v7, v8, v9, v10}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    .line 65
    .local v5, "skeySpec":Ljavax/crypto/spec/SecretKeySpec;
    const/4 v1, 0x0

    .line 66
    .local v1, "decryptedBytesd":Ljava/lang/String;
    const/4 v0, 0x0

    .line 68
    .local v0, "c":Ljavax/crypto/Cipher;
    :try_start_0
    const-string v7, "AES"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 73
    :goto_0
    :try_start_1
    invoke-static {v4, v5, v0}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->decryptF([BLjava/security/Key;Ljavax/crypto/Cipher;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    .line 81
    :goto_1
    return-object v1

    .line 69
    :catch_0
    move-exception v3

    .line 70
    .local v3, "e1":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v3}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    goto :goto_0

    .line 74
    .end local v3    # "e1":Ljavax/crypto/NoSuchPaddingException;
    :catch_1
    move-exception v2

    .line 75
    .local v2, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v2}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_1

    .line 76
    .end local v2    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v2

    .line 77
    .local v2, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    goto :goto_1

    .line 78
    .end local v2    # "e":Ljavax/crypto/BadPaddingException;
    :catch_3
    move-exception v2

    .line 79
    .local v2, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v2}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getKey(Z)Ljava/security/PublicKey;
    .locals 2
    .param p0, "isShipMode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 34
    const/4 v0, 0x0

    .line 36
    .local v0, "publicKey":Ljava/security/PublicKey;
    if-eqz p0, :cond_0

    .line 37
    const-string v1, "prodknoxusagetyrfinger"

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->getPublicKey(Ljava/lang/String;)Ljava/security/PublicKey;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    .line 39
    :cond_0
    const-string v1, "qaknoxusagetyrfinger"

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->getPublicKey(Ljava/lang/String;)Ljava/security/PublicKey;

    move-result-object v0

    goto :goto_0
.end method

.method private static getPublicKey(Ljava/lang/String;)Ljava/security/PublicKey;
    .locals 6
    .param p0, "keyname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;
        }
    .end annotation

    .prologue
    .line 46
    const/4 v4, 0x0

    .line 47
    .local v4, "spec":Ljava/security/spec/X509EncodedKeySpec;
    const/4 v1, 0x0

    .line 48
    .local v1, "decryptedPubKey":Ljava/lang/String;
    const-string v5, "qaknoxusagetyrfinger"

    if-ne p0, v5, :cond_0

    .line 49
    const-string v5, "knoxlog-cloud-qa"

    invoke-static {v5}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->getBaseTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "des":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->getDecryptedKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    :goto_0
    const/4 v5, 0x0

    invoke-static {v1, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 56
    .local v0, "decode":[B
    new-instance v4, Ljava/security/spec/X509EncodedKeySpec;

    .end local v4    # "spec":Ljava/security/spec/X509EncodedKeySpec;
    invoke-direct {v4, v0}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 57
    .restart local v4    # "spec":Ljava/security/spec/X509EncodedKeySpec;
    const-string v5, "RSA"

    invoke-static {v5}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v3

    .line 58
    .local v3, "kf":Ljava/security/KeyFactory;
    invoke-virtual {v3, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v5

    return-object v5

    .line 52
    .end local v0    # "decode":[B
    .end local v2    # "des":Ljava/lang/String;
    .end local v3    # "kf":Ljava/security/KeyFactory;
    :cond_0
    const-string v5, "knoxlog-cloud-prod"

    invoke-static {v5}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->getBaseTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    .restart local v2    # "des":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->getDecryptedKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
