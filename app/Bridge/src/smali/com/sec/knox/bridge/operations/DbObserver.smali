.class public Lcom/sec/knox/bridge/operations/DbObserver;
.super Ljava/lang/Object;
.source "DbObserver.java"


# static fields
.field public static CALENDAR_EVENT_URI:Landroid/net/Uri;

.field public static CALENDAR_SYNC_STATE_URI:Landroid/net/Uri;

.field public static CALENDAR_TASK_URI:Landroid/net/Uri;

.field private static CON_DATA_URI:Landroid/net/Uri;

.field private static CON_RAW_CONTACTS_URI:Landroid/net/Uri;

.field static mCtx:Landroid/content/Context;

.field static sInstance:Lcom/sec/knox/bridge/operations/DbObserver;


# instance fields
.field calendarEventObserver:Landroid/database/ContentObserver;

.field calendarSyncObserver:Landroid/database/ContentObserver;

.field calendarTaskObserver:Landroid/database/ContentObserver;

.field contactDataObserver:Landroid/database/ContentObserver;

.field mCalendarDirtyFlag:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field mContactDirtyFlag:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field rawContactObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    sput-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->mCtx:Landroid/content/Context;

    .line 22
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    .line 23
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->CON_DATA_URI:Landroid/net/Uri;

    .line 25
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->CALENDAR_EVENT_URI:Landroid/net/Uri;

    .line 26
    sget-object v0, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->CALENDAR_SYNC_STATE_URI:Landroid/net/Uri;

    .line 27
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->CALENDAR_TASK_URI:Landroid/net/Uri;

    .line 32
    sput-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->sInstance:Lcom/sec/knox/bridge/operations/DbObserver;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->mContactDirtyFlag:Ljava/util/HashMap;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->mCalendarDirtyFlag:Ljava/util/HashMap;

    .line 34
    new-instance v0, Lcom/sec/knox/bridge/operations/DbObserver$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/operations/DbObserver$1;-><init>(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->rawContactObserver:Landroid/database/ContentObserver;

    .line 52
    new-instance v0, Lcom/sec/knox/bridge/operations/DbObserver$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/operations/DbObserver$2;-><init>(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->contactDataObserver:Landroid/database/ContentObserver;

    .line 70
    new-instance v0, Lcom/sec/knox/bridge/operations/DbObserver$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/operations/DbObserver$3;-><init>(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarEventObserver:Landroid/database/ContentObserver;

    .line 88
    new-instance v0, Lcom/sec/knox/bridge/operations/DbObserver$4;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/operations/DbObserver$4;-><init>(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarTaskObserver:Landroid/database/ContentObserver;

    .line 106
    new-instance v0, Lcom/sec/knox/bridge/operations/DbObserver$5;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/operations/DbObserver$5;-><init>(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarSyncObserver:Landroid/database/ContentObserver;

    .line 125
    sget-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->mCtx:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/operations/DbObserver;->init(Landroid/content/Context;)V

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/DbObserver;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/DbObserver;->unregisterContactObserver(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/DbObserver;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/DbObserver;->unregisterCalendarObserver(Landroid/content/Context;)V

    return-void
.end method

.method public static getInstance()Lcom/sec/knox/bridge/operations/DbObserver;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->sInstance:Lcom/sec/knox/bridge/operations/DbObserver;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/operations/DbObserver;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 129
    sget-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->sInstance:Lcom/sec/knox/bridge/operations/DbObserver;

    if-nez v0, :cond_0

    .line 130
    sput-object p0, Lcom/sec/knox/bridge/operations/DbObserver;->mCtx:Landroid/content/Context;

    .line 131
    new-instance v0, Lcom/sec/knox/bridge/operations/DbObserver;

    invoke-direct {v0}, Lcom/sec/knox/bridge/operations/DbObserver;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->sInstance:Lcom/sec/knox/bridge/operations/DbObserver;

    .line 134
    :cond_0
    sget-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->sInstance:Lcom/sec/knox/bridge/operations/DbObserver;

    return-object v0
.end method

.method private registerCalendarObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 177
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->CALENDAR_SYNC_STATE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarSyncObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 178
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->CALENDAR_EVENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarEventObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 180
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->CALENDAR_TASK_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarTaskObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 182
    return-void
.end method

.method private registerContactObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 166
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/knox/bridge/operations/DbObserver;->rawContactObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 168
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->CON_DATA_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/knox/bridge/operations/DbObserver;->contactDataObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 169
    return-void
.end method

.method private unregisterCalendarObserver(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 185
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarEventObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 186
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarTaskObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/operations/DbObserver;->calendarSyncObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 188
    return-void
.end method

.method private unregisterContactObserver(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 172
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/operations/DbObserver;->rawContactObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 173
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/operations/DbObserver;->contactDataObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 174
    return-void
.end method


# virtual methods
.method public clearCalendarDirty(I)V
    .locals 3
    .param p1, "userid"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->mCalendarDirtyFlag:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->mCtx:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/knox/bridge/operations/DbObserver;->registerCalendarObserver(Landroid/content/Context;)V

    .line 155
    return-void
.end method

.method public clearContactDirty(I)V
    .locals 3
    .param p1, "userid"    # I

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->mContactDirtyFlag:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/knox/bridge/operations/DbObserver;->mCtx:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/knox/bridge/operations/DbObserver;->registerContactObserver(Landroid/content/Context;)V

    .line 150
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 142
    const-string v0, "DbObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialize "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v0, "DbObserver"

    const-string v1, "initialize register obs"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method public isCalendarDirty(I)Z
    .locals 2
    .param p1, "userid"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->mCalendarDirtyFlag:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContactDirty(I)Z
    .locals 2
    .param p1, "userid"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver;->mContactDirtyFlag:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
