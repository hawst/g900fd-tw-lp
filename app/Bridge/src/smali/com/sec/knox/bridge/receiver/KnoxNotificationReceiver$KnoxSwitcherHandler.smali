.class Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;
.super Landroid/os/Handler;
.source "KnoxNotificationReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KnoxSwitcherHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->this$0:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;

    .line 234
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 235
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 238
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 239
    .local v3, "personaId":I
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Context;

    .line 241
    .local v0, "context":Landroid/content/Context;
    const-string v5, "KnoxNotificationReceiver"

    const-string v6, "MSG_START_PERSONA_SWITCH collapse...."

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :try_start_0
    const-string v5, "statusbar"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBarService;

    move-result-object v4

    .line 245
    .local v4, "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v4, :cond_0

    .line 246
    invoke-interface {v4}, Lcom/android/internal/statusbar/IStatusBarService;->collapsePanels()V

    .line 249
    :cond_0
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 302
    .end local v4    # "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
    :goto_0
    return-void

    .line 268
    .restart local v4    # "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
    :pswitch_0
    const-string v5, "KnoxNotificationReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MSG_START_PERSONA_SWITCH called :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    # invokes: Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleSwitchAction(Landroid/content/Context;I)V
    invoke-static {v0, v3}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->access$000(Landroid/content/Context;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 298
    .end local v4    # "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
    :catch_0
    move-exception v2

    .line 300
    .local v2, "ex":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 273
    .end local v2    # "ex":Landroid/os/RemoteException;
    .restart local v4    # "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
    :pswitch_1
    const-wide/16 v6, 0x12c

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 289
    :goto_1
    :try_start_2
    const-string v5, "KnoxNotificationReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MSG_START_PERSONA_SWITCH_LOCK called :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget-object v5, p0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->this$0:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;

    # invokes: Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleSwitchLockAction(Landroid/content/Context;I)V
    invoke-static {v5, v0, v3}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->access$100(Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;Landroid/content/Context;I)V

    goto :goto_0

    .line 285
    :catch_1
    move-exception v1

    .line 286
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 293
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :pswitch_2
    const-string v5, "KnoxNotificationReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MSG_START_PERSONA_LOCK called :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object v5, p0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->this$0:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;

    # invokes: Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleLockAction(Landroid/content/Context;I)V
    invoke-static {v5, v0, v3}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->access$200(Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;Landroid/content/Context;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
