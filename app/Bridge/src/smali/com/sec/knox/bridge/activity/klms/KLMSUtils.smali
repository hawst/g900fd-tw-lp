.class public Lcom/sec/knox/bridge/activity/klms/KLMSUtils;
.super Ljava/lang/Object;
.source "KLMSUtils.java"


# static fields
.field private static instance:Lcom/sec/knox/bridge/activity/klms/KLMSUtils;

.field private static progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    sput-object v0, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->instance:Lcom/sec/knox/bridge/activity/klms/KLMSUtils;

    .line 31
    sput-object v0, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->progressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public static getKLMSErrorText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "code"    # I

    .prologue
    const/16 v5, 0x32

    const/16 v4, 0x28

    const/16 v3, 0x1e

    const/16 v2, 0x14

    .line 109
    const/4 v0, 0x0

    .line 111
    .local v0, "errorText":Ljava/lang/String;
    const/16 v1, 0xa

    if-gt v1, p1, :cond_1

    if-ge p1, v2, :cond_1

    .line 113
    const-string v1, "KLMSUtils"

    const-string v2, "getKLMSError : License Error"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const v1, 0x7f06003b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    :cond_0
    :goto_0
    return-object v0

    .line 116
    :cond_1
    if-gt v2, p1, :cond_2

    if-ge p1, v3, :cond_2

    .line 118
    const-string v1, "KLMSUtils"

    const-string v2, "getKLMSError : Server Error"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const v1, 0x7f06003c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_2
    if-gt v3, p1, :cond_3

    if-ge p1, v4, :cond_3

    .line 123
    const-string v1, "KLMSUtils"

    const-string v2, "getKLMSError : Network Error"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const v1, 0x7f06003f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 125
    :cond_3
    if-gt v4, p1, :cond_4

    if-ge p1, v5, :cond_4

    .line 127
    const-string v1, "KLMSUtils"

    const-string v2, "getKLMSError : User disagreed Eula"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const v1, 0x7f06003d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129
    :cond_4
    if-gt v5, p1, :cond_5

    const/16 v1, 0x5a

    if-ge p1, v1, :cond_5

    .line 131
    const-string v1, "KLMSUtils"

    const-string v2, "getKLMSError : Server Lock Container"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const v1, 0x7f06003a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_5
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 134
    const-string v1, "KLMSUtils"

    const-string v2, "getKLMSError : Validate in progress"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const v1, 0x7f06003e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static isKLMSPackageInstalled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 200
    const/4 v1, 0x0

    .line 202
    .local v1, "bKLMSinstalled":Z
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 204
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.samsung.klmsagent"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 207
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    .line 208
    const/4 v1, 0x1

    .line 214
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return v1

    .line 210
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public static sendRequestKeyStatus(Landroid/content/Context;I)V
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "container_id"    # I

    .prologue
    .line 54
    invoke-static {p0}, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->isKLMSPackageInstalled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    const-string v1, "KLMSUtils"

    const-string v2, "do not send klms intent.."

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :goto_0
    return-void

    .line 59
    :cond_0
    const-string v1, "KLMSUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendRequestKeyStatus() id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.knox.containeragent.klms.licensekey.check"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 62
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "container_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
