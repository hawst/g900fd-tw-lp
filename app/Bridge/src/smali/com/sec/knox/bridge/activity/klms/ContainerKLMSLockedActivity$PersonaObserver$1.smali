.class Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;
.super Ljava/lang/Object;
.source "ContainerKLMSLockedActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->onStateChange(Landroid/content/pm/PersonaState;Landroid/content/pm/PersonaState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

.field final synthetic val$newState:Landroid/content/pm/PersonaState;

.field final synthetic val$previousState:Landroid/content/pm/PersonaState;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;Landroid/content/pm/PersonaState;Landroid/content/pm/PersonaState;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    iput-object p2, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->val$previousState:Landroid/content/pm/PersonaState;

    iput-object p3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->val$newState:Landroid/content/pm/PersonaState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 374
    const-string v0, "ContainerKLMSLockedActivity"

    const-string v1, "onStateChange() checking condition ..."

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->val$previousState:Landroid/content/pm/PersonaState;

    sget-object v1, Landroid/content/pm/PersonaState;->ADMIN_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v0, v1}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->val$previousState:Landroid/content/pm/PersonaState;

    sget-object v1, Landroid/content/pm/PersonaState;->LICENSE_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v0, v1}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->val$newState:Landroid/content/pm/PersonaState;

    sget-object v1, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v0, v1}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->this$0:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;

    # getter for: Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->access$100(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;)Landroid/os/PersonaManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    # getter for: Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->containerId:I
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->access$000(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->isKioskModeEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    const-string v0, "ContainerKLMSLockedActivity"

    const-string v1, "launching COM container....."

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->this$0:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->blockIncomingCall(Z)V
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->access$200(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;Z)V

    .line 380
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->this$0:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;

    # getter for: Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->access$100(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;)Landroid/os/PersonaManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    # getter for: Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->containerId:I
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->access$300(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->launchPersonaHome(I)Z

    .line 382
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;->this$1:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->this$0:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;

    invoke-virtual {v0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->finish()V

    .line 385
    :cond_1
    return-void
.end method
