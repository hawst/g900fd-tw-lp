.class public Lcom/sec/knox/bridge/uploadmanager/NetworkManager;
.super Ljava/lang/Object;
.source "NetworkManager.java"


# direct methods
.method public static callRestAPI(Ljava/lang/String;Lorg/json/JSONObject;Z)V
    .locals 1
    .param p0, "serverURI"    # Ljava/lang/String;
    .param p1, "jsonData"    # Lorg/json/JSONObject;
    .param p2, "isFirstTime"    # Z

    .prologue
    .line 13
    new-instance v0, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Z)V

    invoke-virtual {v0}, Lcom/sec/knox/bridge/uploadmanager/HttpsManager;->execute()V

    .line 14
    return-void
.end method

.method public static isWifiAvailable(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 23
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 25
    .local v3, "netConnectMgr":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    .line 27
    .local v0, "bConnect":Z
    if-nez v3, :cond_0

    .line 38
    :goto_0
    return v5

    .line 31
    :cond_0
    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v3, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 33
    .local v2, "info":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_1

    move v0, v4

    :goto_1
    move v5, v0

    .line 38
    goto :goto_0

    :cond_1
    move v0, v5

    .line 33
    goto :goto_1

    .line 35
    .end local v2    # "info":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v1

    .line 36
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method
