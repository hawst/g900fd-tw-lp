.class Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;
.super Ljava/lang/Thread;
.source "KnoxClipboardDataSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->onStartCommand(Landroid/content/Intent;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    # getter for: Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z
    invoke-static {v0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->access$000(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v1, "onStartCommand() DELETE/DO_SYNC thread started"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    const/4 v1, 0x1

    # setter for: Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->removePersona:Z
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->access$102(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;Z)Z

    .line 110
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    # invokes: Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->doSync()V
    invoke-static {v0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->access$200(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)V

    .line 111
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$1;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    const-string v1, "DELETE"

    # invokes: Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->access$300(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;Ljava/lang/String;)V

    .line 112
    return-void
.end method
