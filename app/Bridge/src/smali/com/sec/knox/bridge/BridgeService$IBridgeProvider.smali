.class Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;
.super Landroid/content/IProviderCallBack$Stub;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IBridgeProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 1467
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/content/IProviderCallBack$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p2, "x1"    # Lcom/sec/knox/bridge/BridgeService$1;

    .prologue
    .line 1467
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 1467
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method private getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 11
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 1711
    iget-object v8, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const-string v9, "PROVIDER_PREFS"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/knox/bridge/BridgeService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1713
    .local v6, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    .line 1715
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 1716
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1717
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1718
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1719
    .local v5, "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {p1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1720
    new-instance v7, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v7, v9, v8}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1721
    .local v7, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    iget-object v1, v7, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1722
    .local v1, "fullServiceName":Ljava/lang/String;
    iget-object v4, v7, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 1723
    .local v4, "packageName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " package name of provider sync service ==  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; service == "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1725
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v4, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1731
    .end local v1    # "fullServiceName":Ljava/lang/String;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :goto_0
    return-object v0

    .line 1730
    :cond_1
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot find service name for providerName: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1731
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getResizedBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1693
    const/16 v2, 0x90

    .line 1694
    .local v2, "maxSize":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1695
    .local v3, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 1696
    .local v1, "height":I
    if-ne v1, v3, :cond_0

    if-ne v3, v2, :cond_0

    .line 1706
    .end local p1    # "image":Landroid/graphics/Bitmap;
    :goto_0
    return-object p1

    .line 1698
    .restart local p1    # "image":Landroid/graphics/Bitmap;
    :cond_0
    div-int v4, v3, v1

    int-to-float v0, v4

    .line 1699
    .local v0, "bitmapRatio":F
    const/4 v4, 0x0

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    .line 1700
    move v3, v2

    .line 1701
    int-to-float v4, v3

    div-float/2addr v4, v0

    float-to-int v1, v4

    .line 1706
    :goto_1
    const/4 v4, 0x1

    invoke-static {p1, v3, v1, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    .line 1703
    :cond_1
    move v1, v2

    .line 1704
    int-to-float v4, v1

    mul-float/2addr v4, v0

    float-to-int v3, v4

    goto :goto_1
.end method


# virtual methods
.method public checkSAccount(Ljava/lang/String;Landroid/content/ISAccountCallback;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "callback"    # Landroid/content/ISAccountCallback;

    .prologue
    .line 1736
    const-string v1, "checkSAccount"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1737
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkSAccount"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    new-instance v0, Lcom/msc/sa/aidl/SAccountSevice;

    invoke-direct {v0}, Lcom/msc/sa/aidl/SAccountSevice;-><init>()V

    .line 1739
    .local v0, "service":Lcom/msc/sa/aidl/SAccountSevice;
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v1, v1, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, v1, p2}, Lcom/msc/sa/aidl/SAccountSevice;->checkSAccount(Ljava/lang/String;Landroid/content/Context;Landroid/content/ISAccountCallback;)V

    .line 1740
    return-void
.end method

.method public checkSAccountPassword(Ljava/lang/String;Landroid/content/ISAccountCallback;)V
    .locals 3
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "callback"    # Landroid/content/ISAccountCallback;

    .prologue
    .line 1744
    const-string v1, "checkSAccountPassword"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1745
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkSAccountPassword"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746
    new-instance v0, Lcom/msc/sa/aidl/SAccountSevice;

    invoke-direct {v0}, Lcom/msc/sa/aidl/SAccountSevice;-><init>()V

    .line 1747
    .local v0, "service":Lcom/msc/sa/aidl/SAccountSevice;
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v1, v1, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, v1, p2}, Lcom/msc/sa/aidl/SAccountSevice;->checkSAccountPassword(Ljava/lang/String;Landroid/content/Context;Landroid/content/ISAccountCallback;)V

    .line 1748
    return-void
.end method

.method public getCallerInfoDetails(Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 10
    .param p1, "contactRefUriAsString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1579
    const-string v4, "getCallerInfoDetails"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v4}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1580
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "getCallerInfoDetails() called"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    const-string v4, "Contacts"

    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 1584
    .local v1, "cn":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 1585
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ProviderNames MATCH, we will now sync up"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    const-class v5, Lcom/sec/knox/bridge/BridgeService;

    monitor-enter v5

    .line 1588
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v6, " SyncToAsyncService I am waiting now"

    invoke-static {v4, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1589
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1590
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1592
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1593
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "action"

    const-string v6, "CALLER_INFO"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    const-string v4, "contactRefUriAsString"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1596
    const-string v4, "messenger"

    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v6, v6, Lcom/sec/knox/bridge/BridgeService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1597
    const-string v4, "binderBundle"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1599
    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-virtual {v4, v3}, Lcom/sec/knox/bridge/BridgeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1600
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IProviderCallBack.Stub before result: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, v7, Lcom/sec/knox/bridge/BridgeService;->mCallerInfoResult:Landroid/content/CustomCursor;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1601
    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1602
    :try_start_1
    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v4

    const-wide/16 v8, 0x7530

    invoke-virtual {v4, v8, v9}, Ljava/lang/Object;->wait(J)V

    .line 1603
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1605
    :try_start_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IProviderCallBack.Stub after result: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, v7, Lcom/sec/knox/bridge/BridgeService;->mCallerInfoResult:Landroid/content/CustomCursor;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1606
    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v4, v4, Lcom/sec/knox/bridge/BridgeService;->mCallerInfoResult:Landroid/content/CustomCursor;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1613
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v4

    .line 1603
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1607
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 1609
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1611
    monitor-exit v5

    .line 1613
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 1611
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v4
.end method

.method public handleShortcut(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "userId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "packageLabel"    # Ljava/lang/String;
    .param p4, "shortcutIcon"    # Landroid/graphics/Bitmap;
    .param p5, "shortcutIntentUri"    # Ljava/lang/String;
    .param p6, "createOrRemove"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1620
    const-string v6, "handleShortcut"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v6}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1621
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, "createShortcut()"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1623
    const-string v6, "RemoteShortcuts"

    invoke-direct {p0, v6}, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 1625
    .local v1, "cn":Landroid/content/ComponentName;
    if-eqz v1, :cond_3

    .line 1626
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ProviderNames MATCH, we will now sync up"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1628
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, " SyncToAsyncService I am waiting now"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1629
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1630
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1631
    if-eqz p4, :cond_0

    .line 1632
    invoke-direct {p0, p4}, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->getResizedBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p4

    .line 1633
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " before startservice intent shortcutIcon.getWidth():"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " shortcutIcon.getHeight(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    :cond_0
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Package name is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1640
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1641
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "userId"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1642
    const-string v6, "packageName"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1643
    const-string v6, "packageLabel"

    invoke-virtual {v0, v6, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1644
    const-string v6, "shortcutIcon"

    invoke-virtual {v0, v6, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1645
    const-string v6, "shortcutIntentUri"

    invoke-virtual {v0, v6, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    const-string v6, "createOrRemove"

    invoke-virtual {v0, v6, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    const-string v6, "create"

    invoke-virtual {p6, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1649
    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;
    invoke-static {v6}, Lcom/sec/knox/bridge/BridgeService;->access$1900(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/Set;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 1650
    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->preInstalledAppsMinusEmail:Ljava/util/Set;
    invoke-static {v6}, Lcom/sec/knox/bridge/BridgeService;->access$1900(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1651
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " preinstalled Package name is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1652
    const-string v6, "isPreInstalledPackage"

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1660
    :cond_1
    :goto_0
    sget-object v6, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    if-eqz v6, :cond_2

    .line 1661
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1662
    .local v4, "req":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 1664
    .local v5, "resp":Landroid/os/Bundle;
    const-string v6, "action"

    const-string v7, "RequestBadgeInfo"

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1665
    const-string v6, "pkgName"

    invoke-virtual {v4, v6, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667
    sget-object v6, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    iget-object v7, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, v7, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7, p1, v4}, Landroid/os/RCPManager;->exchangeData(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v5

    .line 1669
    if-eqz v5, :cond_2

    .line 1670
    const-string v6, "pkg_names"

    const-string v7, "pkg_names"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    const-string v6, "class_names"

    const-string v7, "class_names"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    const-string v6, "counts"

    const-string v7, "counts"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1674
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BadgeInfo: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "pkg_names"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "class_names"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "counts"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1681
    .end local v4    # "req":Landroid/os/Bundle;
    .end local v5    # "resp":Landroid/os/Bundle;
    :cond_2
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1682
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, " before startservice intent"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683
    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-virtual {v6, v3}, Lcom/sec/knox/bridge/BridgeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1690
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_3
    :goto_1
    return-void

    .line 1654
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_4
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, " set does not contain package"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1684
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 1686
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1656
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_5
    :try_start_1
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, " set is null "

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public queryProvider(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 24
    .param p1, "providerName"    # Ljava/lang/String;
    .param p2, "resourceName"    # Ljava/lang/String;
    .param p3, "containerId"    # I
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1473
    const-string v19, "queryProvider"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static/range {v19 .. v19}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1474
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    const-string v20, " IBridgeProvider queryProvider() called"

    invoke-static/range {v19 .. v20}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    const-string v20, "PROVIDER_PREFS"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/sec/knox/bridge/BridgeService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 1478
    .local v14, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v14}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v10

    .line 1479
    .local v10, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/Map;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_5

    .line 1480
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 1481
    .local v9, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1482
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    .line 1484
    .local v12, "pairs":Ljava/util/Map$Entry;
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1485
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "ProviderNames MATCH, we will now sync up for providerName="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    const-string v15, "false"

    .line 1489
    .local v15, "sanitizeData":Ljava/lang/String;
    const/16 v17, 0x0

    .line 1492
    .local v17, "syncOnlyFavoritesContactsPolicy":Z
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    if-eqz v19, :cond_4

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    const-string v20, "Contacts"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    const-string v20, "Calendar"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    const-string v20, "CallLog"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    const-string v20, "Bookmarks"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1497
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    const-string v21, "knox-export-data"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->getPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v19 .. v21}, Lcom/sec/knox/bridge/BridgeService;->access$1700(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1499
    .local v13, "policy":Ljava/lang/String;
    if-eqz v13, :cond_2

    const-string v19, "false"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1500
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    const-string v20, "Blocked EXPORT_DATA by  policy."

    invoke-static/range {v19 .. v20}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1501
    const/16 v19, 0x0

    .line 1572
    .end local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v12    # "pairs":Ljava/util/Map$Entry;
    .end local v13    # "policy":Ljava/lang/String;
    .end local v15    # "sanitizeData":Ljava/lang/String;
    .end local v17    # "syncOnlyFavoritesContactsPolicy":Z
    :goto_1
    return-object v19

    .line 1503
    .restart local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v12    # "pairs":Ljava/util/Map$Entry;
    .restart local v13    # "policy":Ljava/lang/String;
    .restart local v15    # "sanitizeData":Ljava/lang/String;
    .restart local v17    # "syncOnlyFavoritesContactsPolicy":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    const-string v21, "knox-sanitize-data"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->getPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v19 .. v21}, Lcom/sec/knox/bridge/BridgeService;->access$1700(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1507
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    const-string v20, "Contacts"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    # invokes: Lcom/sec/knox/bridge/BridgeService;->getSyncPolicy()Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/sec/knox/bridge/BridgeService;->access$1800(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/String;

    move-result-object v18

    .line 1510
    .local v18, "syncPolicy":Ljava/lang/String;
    const-string v19, "fav_contacts"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1511
    const/16 v17, 0x1

    .line 1513
    :cond_3
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Contacts: syncOnlyFavoritesContactsPolicy = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1517
    .end local v13    # "policy":Ljava/lang/String;
    .end local v18    # "syncPolicy":Ljava/lang/String;
    :cond_4
    const-class v20, Lcom/sec/knox/bridge/BridgeService;

    monitor-enter v20

    .line 1519
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    const-string v21, " SyncToAsyncService I am waiting now"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1520
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1521
    .local v8, "i":Landroid/content/Intent;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v21

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, " SyncToAsyncService  package+fullServiceName == "

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    new-instance v16, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v21, v0

    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;-><init>(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;)V

    .line 1525
    .local v16, "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->serviceName:Ljava/lang/String;

    .line 1526
    .local v7, "fullServiceName":Ljava/lang/String;
    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;->packageName:Ljava/lang/String;

    .line 1527
    .local v11, "packageName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, " Sync during switch package == "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "; service == "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "package name of provider sync service == "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532
    new-instance v5, Landroid/content/ComponentName;

    invoke-direct {v5, v11, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    .local v5, "cn":Landroid/content/ComponentName;
    invoke-virtual {v8, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1535
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1536
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v19, "messenger"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mMessenger:Landroid/os/Messenger;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1539
    const-string v19, "providerName"

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    const-string v19, "resourceName"

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1541
    const-string v19, "containerId"

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1542
    const-string v19, "projection"

    move-object/from16 v0, v19

    move-object/from16 v1, p4

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1543
    const-string v19, "selection"

    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1544
    const-string v19, "selectionArgs"

    move-object/from16 v0, v19

    move-object/from16 v1, p6

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1545
    const-string v19, "sortOrder"

    move-object/from16 v0, v19

    move-object/from16 v1, p7

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    const-string v19, "sanitizeData"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1547
    const-string v19, "syncOnlyFavoritesContactsPolicy"

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1550
    const-string v19, "binderBundle"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/knox/bridge/BridgeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1552
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "IProviderCallBack.Stub before result: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->result:Landroid/content/CustomCursor;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static/range {v19 .. v19}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v21

    monitor-enter v21
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1554
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static/range {v19 .. v19}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v19

    const-wide/16 v22, 0x7530

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V

    .line 1555
    monitor-exit v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1558
    :try_start_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "IProviderCallBack.Stub after result: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->result:Landroid/content/CustomCursor;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IBridgeProvider;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->result:Landroid/content/CustomCursor;

    move-object/from16 v19, v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v20

    goto/16 :goto_1

    .line 1565
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "cn":Landroid/content/ComponentName;
    .end local v7    # "fullServiceName":Ljava/lang/String;
    .end local v8    # "i":Landroid/content/Intent;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v16    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :catchall_0
    move-exception v19

    monitor-exit v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v19

    .line 1555
    .restart local v4    # "bundle":Landroid/os/Bundle;
    .restart local v5    # "cn":Landroid/content/ComponentName;
    .restart local v7    # "fullServiceName":Ljava/lang/String;
    .restart local v8    # "i":Landroid/content/Intent;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v16    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :catchall_1
    move-exception v19

    :try_start_4
    monitor-exit v21
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v19
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1561
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "cn":Landroid/content/ComponentName;
    .end local v7    # "fullServiceName":Ljava/lang/String;
    .end local v8    # "i":Landroid/content/Intent;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v16    # "si":Lcom/sec/knox/bridge/BridgeService$RCPServiceInfo;
    :catch_0
    move-exception v6

    .line 1563
    .local v6, "e":Ljava/lang/InterruptedException;
    :try_start_6
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1565
    monitor-exit v20
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 1569
    .end local v6    # "e":Ljava/lang/InterruptedException;
    .end local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v12    # "pairs":Ljava/util/Map$Entry;
    .end local v15    # "sanitizeData":Ljava/lang/String;
    .end local v17    # "syncOnlyFavoritesContactsPolicy":Z
    :cond_5
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v19

    const-string v20, "No services registered to provide. (prefs empty)"

    invoke-static/range {v19 .. v20}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    :cond_6
    const/16 v19, 0x0

    goto/16 :goto_1
.end method
