.class public Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;
.super Ljava/lang/Object;
.source "SecureDataGenerator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private decrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B
    .locals 7
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "cipherData"    # [B

    .prologue
    .line 75
    const/4 v4, 0x0

    .line 76
    .local v4, "plainData":[B
    const/4 v1, 0x0

    .line 78
    .local v1, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v5

    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 79
    .local v3, "ks":Ljavax/crypto/spec/SecretKeySpec;
    const-string v5, "AES/CBC/PKCS7Padding"

    invoke-static {v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 80
    invoke-direct {p0, p2}, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;->getChangedIV(Ljavax/crypto/spec/IvParameterSpec;)Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v0

    .line 81
    .local v0, "changedIvParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v5, 0x2

    invoke-virtual {v1, v5, v3, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 82
    invoke-virtual {v1, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v4

    .line 102
    .end local v0    # "changedIvParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v3    # "ks":Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v4

    .line 83
    :catch_0
    move-exception v2

    .line 84
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 85
    const/4 v4, 0x0

    .line 101
    goto :goto_0

    .line 86
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v2

    .line 87
    .local v2, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v2}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 88
    const/4 v4, 0x0

    .line 101
    goto :goto_0

    .line 89
    .end local v2    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v2

    .line 90
    .local v2, "e":Ljava/security/InvalidAlgorithmParameterException;
    invoke-virtual {v2}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    .line 91
    const/4 v4, 0x0

    .line 101
    goto :goto_0

    .line 92
    .end local v2    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_3
    move-exception v2

    .line 93
    .local v2, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v2}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    .line 94
    const/4 v4, 0x0

    .line 101
    goto :goto_0

    .line 95
    .end local v2    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v2

    .line 96
    .local v2, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    .line 97
    const/4 v4, 0x0

    .line 101
    goto :goto_0

    .line 98
    .end local v2    # "e":Ljavax/crypto/BadPaddingException;
    :catch_5
    move-exception v2

    .line 99
    .local v2, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    .line 100
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private encrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B
    .locals 6
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "plainData"    # [B

    .prologue
    .line 45
    const/4 v1, 0x0

    .line 46
    .local v1, "ciphertext":[B
    const/4 v0, 0x0

    .line 48
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 49
    .local v3, "ks":Ljavax/crypto/spec/SecretKeySpec;
    const-string v4, "AES/CBC/PKCS7Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 50
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 51
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v1

    .line 71
    .end local v3    # "ks":Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v1

    .line 52
    :catch_0
    move-exception v2

    .line 53
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 54
    const/4 v1, 0x0

    .line 70
    goto :goto_0

    .line 55
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v2

    .line 56
    .local v2, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v2}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 57
    const/4 v1, 0x0

    .line 70
    goto :goto_0

    .line 58
    .end local v2    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v2

    .line 59
    .local v2, "e":Ljava/security/InvalidAlgorithmParameterException;
    invoke-virtual {v2}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    .line 60
    const/4 v1, 0x0

    .line 70
    goto :goto_0

    .line 61
    .end local v2    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_3
    move-exception v2

    .line 62
    .local v2, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v2}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    .line 63
    const/4 v1, 0x0

    .line 70
    goto :goto_0

    .line 64
    .end local v2    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v2

    .line 65
    .local v2, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    .line 66
    const/4 v1, 0x0

    .line 70
    goto :goto_0

    .line 67
    .end local v2    # "e":Ljavax/crypto/BadPaddingException;
    :catch_5
    move-exception v2

    .line 68
    .local v2, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    .line 69
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getChangedIV(Ljavax/crypto/spec/IvParameterSpec;)Ljavax/crypto/spec/IvParameterSpec;
    .locals 9
    .param p1, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x0

    .line 106
    const-string v4, "KnoxUsageLogPro"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getChangedIV :src:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    new-instance v4, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/math/BigInteger;-><init>([B)V

    new-instance v5, Ljava/math/BigInteger;

    const-string v6, "1"

    invoke-direct {v5, v6}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 108
    .local v0, "bigIV":Ljava/math/BigInteger;
    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v3

    .line 109
    .local v3, "srcIV":[B
    new-array v2, v8, [B

    .line 110
    .local v2, "dstIV":[B
    array-length v4, v3

    array-length v5, v2

    if-gt v4, v5, :cond_0

    .line 111
    array-length v4, v2

    array-length v5, v3

    sub-int/2addr v4, v5

    array-length v5, v3

    invoke-static {v3, v7, v2, v4, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 114
    :goto_0
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v1, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 115
    .local v1, "changedIVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const-string v4, "KnoxUsageLogPro"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getChangedIV :src:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return-object v1

    .line 113
    .end local v1    # "changedIVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    :cond_0
    invoke-static {v3, v7, v2, v7, v8}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0
.end method


# virtual methods
.method public getDecryptedNetworkData(Ljava/lang/String;Ljavax/crypto/spec/IvParameterSpec;Ljava/security/Key;)Ljava/lang/String;
    .locals 6
    .param p1, "cryptoData"    # Ljava/lang/String;
    .param p2, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "inKey"    # Ljava/security/Key;

    .prologue
    .line 32
    move-object v1, p1

    .line 34
    .local v1, "mResult":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v3

    .line 35
    .local v3, "receivedData":[B
    invoke-direct {p0, p3, p2, v3}, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;->decrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object v2

    .line 36
    .local v2, "msgArr":[B
    new-instance v1, Ljava/lang/String;

    .end local v1    # "mResult":Ljava/lang/String;
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    .end local v2    # "msgArr":[B
    .end local v3    # "receivedData":[B
    .restart local v1    # "mResult":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 37
    .end local v1    # "mResult":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 39
    .restart local v1    # "mResult":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getEncryptedAESSessionKey([BLcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;)Ljava/lang/String;
    .locals 8
    .param p1, "randomarray"    # [B
    .param p2, "mPUBSerialNumber"    # Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    .prologue
    .line 121
    const/4 v3, 0x0

    .line 124
    .local v3, "mResult":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProductShip(Landroid/content/Context;)Z

    move-result v6

    invoke-static {v6}, Lcom/sec/knox/bridge/uploadmanager/PublicKeyReader;->getKey(Z)Ljava/security/PublicKey;

    move-result-object v4

    .line 125
    .local v4, "publicKey":Ljava/security/PublicKey;
    if-nez v4, :cond_0

    .line 126
    const-string v6, "getEncryptedAESSessionKey : publicKey is null"

    invoke-static {v6}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 127
    const/4 v6, 0x0

    .line 141
    .end local v4    # "publicKey":Ljava/security/PublicKey;
    :goto_0
    return-object v6

    .line 130
    .restart local v4    # "publicKey":Ljava/security/PublicKey;
    :cond_0
    const-string v6, "RSA/ECB/PKCS1Padding"

    invoke-static {v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v5

    .line 131
    .local v5, "rsaCipher":Ljavax/crypto/Cipher;
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 132
    invoke-virtual {v5, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    .line 134
    .local v2, "encryptedPassword":[B
    array-length v6, v2

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 135
    .local v0, "bf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 136
    new-instance v3, Ljava/lang/String;

    .end local v3    # "mResult":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "bf":Ljava/nio/ByteBuffer;
    .end local v2    # "encryptedPassword":[B
    .end local v4    # "publicKey":Ljava/security/PublicKey;
    .end local v5    # "rsaCipher":Ljavax/crypto/Cipher;
    .restart local v3    # "mResult":Ljava/lang/String;
    :goto_1
    move-object v6, v3

    .line 141
    goto :goto_0

    .line 137
    .end local v3    # "mResult":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    .line 139
    .restart local v3    # "mResult":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getEncryptedNetworkData(Ljava/lang/String;Ljava/security/Key;[B)Ljava/lang/String;
    .locals 6
    .param p1, "plainText"    # Ljava/lang/String;
    .param p2, "inKey"    # Ljava/security/Key;
    .param p3, "randomarray"    # [B

    .prologue
    .line 145
    move-object v1, p1

    .line 147
    .local v1, "mResult":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    const/16 v4, 0x20

    const/16 v5, 0x10

    invoke-direct {v3, p3, v4, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([BII)V

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {p0, p2, v3, v4}, Lcom/sec/knox/bridge/uploadmanager/SecureDataGenerator;->encrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object v2

    .line 148
    .local v2, "msgArr":[B
    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 153
    .end local v2    # "msgArr":[B
    :goto_0
    return-object v1

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 151
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
