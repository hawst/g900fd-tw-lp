.class Lcom/sec/knox/bridge/activity/MoveContactItemActivity$3;
.super Ljava/lang/Object;
.source "MoveContactItemActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->showDstKnoxSelectionDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$3;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$3;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->access$300(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mDstKnoxSelectionDialog is canceled. finish the activity"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$3;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    invoke-virtual {v0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->finish()V

    .line 201
    return-void
.end method
