.class Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;
.super Ljava/lang/Object;
.source "MoveContactItemActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->showDstKnoxSelectionDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

.field final synthetic val$knoxFirstId:I


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;I)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    iput p2, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;->val$knoxFirstId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    iget v1, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;->val$knoxFirstId:I

    # setter for: Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mMoveToDstCId:I
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->access$002(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;I)I

    .line 157
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 158
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->access$102(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 160
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveContactItemActivity$1;->this$0:Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->moveToContact(Z)V
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveContactItemActivity;Z)V

    .line 161
    return-void
.end method
