.class Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$3;
.super Ljava/lang/Object;
.source "KnoxUsageMonitorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

.field final synthetic val$currentUser:I


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;I)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$3;->this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

    iput p2, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$3;->val$currentUser:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 340
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->setUpdateInProgress(Z)V

    .line 341
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Received MSG_UPDATE_USAGE currentUser"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$3;->val$currentUser:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    iget v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$3;->val$currentUser:I

    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;->updateLoginCountForPersona(ILandroid/content/Context;)V

    .line 346
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->setUpdateInProgress(Z)V

    .line 347
    return-void
.end method
