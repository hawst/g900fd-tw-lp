.class public Lcom/sec/knox/bridge/service/BackgroundWorkerService;
.super Landroid/app/IntentService;
.source "BackgroundWorkerService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mMainHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/knox/bridge/service/BackgroundWorkerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    sget-object v0, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->mMainHandler:Landroid/os/Handler;

    .line 49
    return-void
.end method

.method private getContactDataToMove(Landroid/os/Bundle;Landroid/database/Cursor;)Landroid/os/Bundle;
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "contact_cur"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x0

    .line 242
    const/4 v1, 0x0

    .line 243
    .local v1, "rawContact":Landroid/content/CustomCursor;
    const/4 v0, 0x0

    .line 244
    .local v0, "contactData":Landroid/content/CustomCursor;
    sget-object v2, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CONTACT_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lcom/sec/knox/bridge/operations/ExchangeContactData;->getRawContacts(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v1

    .line 247
    if-eqz v1, :cond_0

    .line 248
    sget-object v2, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CONTACT_DATA_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "raw_contact_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lcom/sec/knox/bridge/operations/ExchangeContactData;->getContactData(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v0

    .line 253
    :cond_0
    if-eqz v1, :cond_1

    .line 254
    const-string v2, "rawContact"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 257
    :cond_1
    if-eqz v0, :cond_2

    .line 258
    const-string v2, "contactData"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 260
    :cond_2
    return-object p1
.end method

.method public static getMoveContactsToTargetIntent(Landroid/os/Bundle;I)Landroid/content/Intent;
    .locals 2
    .param p0, "aBundle"    # Landroid/os/Bundle;
    .param p1, "aTarget"    # I

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.knox.bridge.service.ACTION_MOVE_CONTACT_TO_TARGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 60
    .local v0, "lIntent":Landroid/content/Intent;
    if-eqz p0, :cond_0

    .line 61
    invoke-virtual {v0, p0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 62
    :cond_0
    const-string v1, "target"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    return-object v0
.end method

.method private moveContactsToTarget(Landroid/content/Intent;)V
    .locals 21
    .param p1, "aIntent"    # Landroid/content/Intent;

    .prologue
    .line 68
    const/16 v16, 0x0

    .line 72
    .local v16, "result":Landroid/os/Bundle;
    const-string v2, "target"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 74
    .local v18, "target":I
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 76
    .local v8, "bundle":Landroid/os/Bundle;
    const-string v2, "contact_ids"

    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v10

    .line 78
    .local v10, "contact_ids":[J
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MoveTo Contact "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v10}, Lcom/sec/knox/bridge/operations/ExchangeContactData;->getListIds([J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const/16 v16, 0x0

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "contact_id in ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v10}, Lcom/sec/knox/bridge/operations/ExchangeContactData;->getListIds([J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 90
    .local v9, "contact_cur":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 92
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    const-string v3, "onReceive:MoveTo no raw contacts"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :goto_0
    if-eqz v16, :cond_3

    const-string v2, "true"

    const-string v3, "result"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 140
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    const-string v3, "MoveToContact success"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "contact_id in ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v10}, Lcom/sec/knox/bridge/operations/ExchangeContactData;->getListIds([J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 154
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    if-nez v2, :cond_4

    .line 156
    :cond_0
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    const-string v3, "BridgeService is not instantiated"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :goto_1
    return-void

    .line 96
    :cond_1
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send raw_contact "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getRcpManager()Landroid/os/RCPManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 104
    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getContactDataToMove(Landroid/os/Bundle;Landroid/database/Cursor;)Landroid/os/Bundle;

    .line 105
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getRcpManager()Landroid/os/RCPManager;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1, v8}, Landroid/os/RCPManager;->exchangeData(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v16

    .line 108
    if-nez v16, :cond_1

    .line 113
    :try_start_1
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x5

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->sleep(J)V

    .line 114
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getContactDataToMove(Landroid/os/Bundle;Landroid/database/Cursor;)Landroid/os/Bundle;

    .line 115
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getRcpManager()Landroid/os/RCPManager;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1, v8}, Landroid/os/RCPManager;->exchangeData(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v16

    goto :goto_2

    .line 118
    :catch_0
    move-exception v12

    .line 119
    .local v12, "e1":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 124
    .end local v12    # "e1":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v11

    .line 126
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 120
    .end local v11    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v11

    .line 121
    .local v11, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-virtual {v11}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 134
    .end local v11    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 148
    :cond_3
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    const-string v3, "MoveToContact failed"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->showToast(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 162
    :cond_4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-nez v2, :cond_7

    .line 164
    const/4 v14, 0x0

    .line 166
    .local v14, "name":Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v15

    .line 168
    .local v15, "pmInfo":Landroid/content/pm/PersonaInfo;
    if-eqz v15, :cond_5

    .line 171
    const-string v2, "user"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/os/UserManager;

    .line 172
    .local v20, "um":Landroid/os/UserManager;
    iget v2, v15, Landroid/content/pm/PersonaInfo;->id:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v19

    .line 173
    .local v19, "ui":Landroid/content/pm/UserInfo;
    move-object/from16 v0, v19

    iget-object v14, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 178
    .end local v19    # "ui":Landroid/content/pm/UserInfo;
    .end local v20    # "um":Landroid/os/UserManager;
    :cond_5
    array-length v2, v10

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 180
    const/4 v13, 0x0

    .line 182
    .local v13, "msg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06000e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v14, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 186
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    invoke-static {v2, v13}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->showToast(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 192
    .end local v13    # "msg":Ljava/lang/String;
    :cond_6
    const/4 v13, 0x0

    .line 194
    .restart local v13    # "msg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060010

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    array-length v6, v10

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v14, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 198
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    invoke-static {v2, v13}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->showToast(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 206
    .end local v13    # "msg":Ljava/lang/String;
    .end local v14    # "name":Ljava/lang/String;
    .end local v15    # "pmInfo":Landroid/content/pm/PersonaInfo;
    :cond_7
    const/16 v17, 0x0

    .line 208
    .local v17, "strPersonal":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 210
    array-length v2, v10

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 212
    const/4 v13, 0x0

    .line 214
    .restart local v13    # "msg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06000e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v17, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 218
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    invoke-static {v2, v13}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->showToast(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 224
    .end local v13    # "msg":Ljava/lang/String;
    :cond_8
    const/4 v13, 0x0

    .line 226
    .restart local v13    # "msg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060010

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    array-length v6, v10

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v17, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 231
    sget-object v2, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->TAG:Ljava/lang/String;

    invoke-static {v2, v13}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->showToast(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 1
    .param p1, "aMsg"    # Ljava/lang/String;

    .prologue
    .line 264
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->showToast(Ljava/lang/String;I)V

    .line 265
    return-void
.end method

.method private showToast(Ljava/lang/String;I)V
    .locals 2
    .param p1, "aMsg"    # Ljava/lang/String;
    .param p2, "aDuration"    # I

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;-><init>(Lcom/sec/knox/bridge/service/BackgroundWorkerService;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 285
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "aIntent"    # Landroid/content/Intent;

    .prologue
    .line 53
    const-string v0, "com.sec.knox.bridge.service.ACTION_MOVE_CONTACT_TO_TARGET"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->moveContactsToTarget(Landroid/content/Intent;)V

    .line 56
    :cond_0
    return-void
.end method
