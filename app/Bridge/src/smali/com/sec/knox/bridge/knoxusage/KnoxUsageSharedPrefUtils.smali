.class public Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;
.super Ljava/lang/Object;
.source "KnoxUsageSharedPrefUtils.java"


# static fields
.field public static KNOX_USAGE_PREF:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "knoxusage"

    sput-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->KNOX_USAGE_PREF:Ljava/lang/String;

    return-void
.end method

.method public static getSharedPreferences(Ljava/lang/String;ILandroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "mode"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-virtual {p2, p0, p1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static loadIsFirstTime(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    sget-object v1, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->KNOX_USAGE_PREF:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->getSharedPreferences(Ljava/lang/String;ILandroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 31
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "firstTime"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static loadNextUploadTime(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    sget-object v1, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->KNOX_USAGE_PREF:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->getSharedPreferences(Ljava/lang/String;ILandroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 36
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "upload_frequency"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static saveIsFirstTime(ZLandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # Z
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    sget-object v2, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->KNOX_USAGE_PREF:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->getSharedPreferences(Ljava/lang/String;ILandroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 18
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 19
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "firstTime"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 20
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 21
    return-void
.end method

.method public static saveNextUploadTime(JLandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # J
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    sget-object v2, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->KNOX_USAGE_PREF:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3, p2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->getSharedPreferences(Ljava/lang/String;ILandroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 25
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 26
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "upload_frequency"

    invoke-interface {v0, v2, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 27
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 28
    return-void
.end method
