.class public Lcom/sec/knox/bridge/receiver/CommonReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CommonReceiver.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    return-void
.end method

.method private sendRestartNotification(I)V
    .locals 10
    .param p1, "personaId"    # I

    .prologue
    const/16 v9, 0x3e9

    .line 80
    const-string v7, "CommonReceiver"

    const-string v8, "knox security policy update noti"

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v7, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    if-nez v7, :cond_0

    .line 83
    const-string v7, "CommonReceiver"

    const-string v8, "context is null. so can\'t display notification."

    invoke-static {v7, v8}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v7, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 87
    .local v5, "notificationManager":Landroid/app/NotificationManager;
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    const-class v8, Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v7, 0x10000000

    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 89
    iget-object v7, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v7, v9, v1, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 90
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    invoke-virtual {v5, v9}, Landroid/app/NotificationManager;->cancel(I)V

    .line 91
    iget-object v7, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    const v8, 0x7f060043

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "message":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    const v8, 0x7f060044

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 93
    .local v6, "title":Ljava/lang/String;
    new-instance v7, Landroid/app/Notification$Builder;

    iget-object v8, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x7f020011

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    new-instance v8, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v8}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v8, v2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 101
    .local v3, "notiBuilder":Landroid/app/Notification$Builder;
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    .line 102
    .local v4, "notification":Landroid/app/Notification;
    const/4 v7, 0x0

    new-instance v8, Landroid/os/UserHandle;

    invoke-direct {v8, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v5, v7, v9, v4, v8}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, -0x1

    .line 36
    const/4 v0, 0x0

    .line 38
    .local v0, "action":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 43
    iput-object p1, p0, Lcom/sec/knox/bridge/receiver/CommonReceiver;->mContext:Landroid/content/Context;

    .line 44
    const-string v5, "CommonReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onReceive "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v5, "com.sec.knox.containeragent.klms.licensekey.status"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 47
    const-string v5, "code"

    const/16 v6, -0x3e8

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 49
    .local v1, "code":I
    const-string v5, "container_id"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 51
    .local v2, "container_id":I
    invoke-static {p1, v1, v2}, Lcom/sec/knox/bridge/util/PreferenceManager;->updateKlmsStatus(Landroid/content/Context;II)V

    .line 54
    :try_start_0
    invoke-static {p1}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->processAfterKeyCheck(Landroid/content/Context;)V

    .line 55
    invoke-static {p1}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity2;->processAfterKeyCheck(Landroid/content/Context;)V

    .line 56
    invoke-static {p1}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->processAfterKeyCheck(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_1
    const-string v5, "CommonReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "status "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1, v2}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :catch_0
    move-exception v3

    .line 58
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 64
    .end local v1    # "code":I
    .end local v2    # "container_id":I
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 68
    const-string v5, "com.sec.knox.container.need.restart"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 70
    const-string v5, "containerId"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 72
    .local v4, "personaId":I
    const-string v5, "CommonReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "INTENT_CONTAINER_NEED_RESTART personaId-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const/16 v5, 0x64

    if-lt v4, v5, :cond_0

    .line 74
    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/receiver/CommonReceiver;->sendRestartNotification(I)V

    goto/16 :goto_0
.end method
