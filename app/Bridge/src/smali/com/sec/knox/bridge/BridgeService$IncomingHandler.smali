.class Lcom/sec/knox/bridge/BridgeService$IncomingHandler;
.super Landroid/os/Handler;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IncomingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 826
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 829
    const-string v1, "handleMessage"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 830
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 860
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 862
    :goto_0
    return-void

    .line 832
    :pswitch_0
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 835
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const/4 v3, 0x0

    iput-object v3, v1, Lcom/sec/knox/bridge/BridgeService;->result:Landroid/content/CustomCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 837
    :try_start_1
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;

    const-string v4, "RESULT"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/CustomCursor;

    iput-object v1, v3, Lcom/sec/knox/bridge/BridgeService;->result:Landroid/content/CustomCursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 842
    :goto_1
    :try_start_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IncomingHandler result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v4, v4, Lcom/sec/knox/bridge/BridgeService;->result:Landroid/content/CustomCursor;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 844
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 838
    :catch_0
    move-exception v0

    .line 839
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "From IncomingHandler().result: e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 847
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 848
    :try_start_4
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const/4 v3, 0x0

    iput-object v3, v1, Lcom/sec/knox/bridge/BridgeService;->mCallerInfoResult:Landroid/content/CustomCursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 850
    :try_start_5
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;

    const-string v4, "RESULT"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/CustomCursor;

    iput-object v1, v3, Lcom/sec/knox/bridge/BridgeService;->mCallerInfoResult:Landroid/content/CustomCursor;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 855
    :goto_2
    :try_start_6
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IncomingHandler result for case 2: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v4, v4, Lcom/sec/knox/bridge/BridgeService;->mCallerInfoResult:Landroid/content/CustomCursor;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IncomingHandler;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->resultLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1400(Lcom/sec/knox/bridge/BridgeService;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 857
    monitor-exit v2

    goto/16 :goto_0

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v1

    .line 851
    :catch_1
    move-exception v0

    .line 852
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_7
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "From IncomingHandler().mCallerInfoResult: e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    .line 830
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
