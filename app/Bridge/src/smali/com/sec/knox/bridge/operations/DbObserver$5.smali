.class Lcom/sec/knox/bridge/operations/DbObserver$5;
.super Landroid/database/ContentObserver;
.source "DbObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/operations/DbObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/operations/DbObserver;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/DbObserver$5;->this$0:Lcom/sec/knox/bridge/operations/DbObserver;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 109
    const-string v0, "DbObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange calendar _sync_state"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver$5;->this$0:Lcom/sec/knox/bridge/operations/DbObserver;

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/DbObserver;->mCalendarDirtyFlag:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 112
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver$5;->this$0:Lcom/sec/knox/bridge/operations/DbObserver;

    sget-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->mCtx:Landroid/content/Context;

    # invokes: Lcom/sec/knox/bridge/operations/DbObserver;->unregisterCalendarObserver(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/operations/DbObserver;->access$100(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/content/Context;)V

    .line 113
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 117
    const-string v0, "DbObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange calendar _sync_state uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver$5;->this$0:Lcom/sec/knox/bridge/operations/DbObserver;

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/DbObserver;->mCalendarDirtyFlag:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 120
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/DbObserver$5;->this$0:Lcom/sec/knox/bridge/operations/DbObserver;

    sget-object v1, Lcom/sec/knox/bridge/operations/DbObserver;->mCtx:Landroid/content/Context;

    # invokes: Lcom/sec/knox/bridge/operations/DbObserver;->unregisterCalendarObserver(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/operations/DbObserver;->access$100(Lcom/sec/knox/bridge/operations/DbObserver;Landroid/content/Context;)V

    .line 121
    return-void
.end method
