.class Lcom/sec/knox/bridge/BridgeService$IBridgeCommandExecutor;
.super Landroid/content/ICommandExeCallBack$Stub;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IBridgeCommandExecutor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 1400
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeCommandExecutor;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/content/ICommandExeCallBack$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p2, "x1"    # Lcom/sec/knox/bridge/BridgeService$1;

    .prologue
    .line 1400
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$IBridgeCommandExecutor;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    return-void
.end method


# virtual methods
.method public execute(Landroid/app/Command;)V
    .locals 9
    .param p1, "command"    # Landroid/app/Command;

    .prologue
    .line 1403
    const-string v6, "execute"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v6}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1404
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, "IBridgeCommandExecutor - executeCommand() - will start the commands"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    if-eqz p1, :cond_0

    .line 1406
    const-string v6, "launchActivity"

    iget-object v7, p1, Landroid/app/Command;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1452
    :cond_0
    :goto_0
    return-void

    .line 1408
    :cond_1
    const-string v6, "launchIntent"

    iget-object v7, p1, Landroid/app/Command;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1410
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " IBridgeCommandExecutor launchIntent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Landroid/app/Command;->intent:Landroid/content/Intent;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeCommandExecutor;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v7, p1, Landroid/app/Command;->intent:Landroid/content/Intent;

    invoke-virtual {v6, v7}, Lcom/sec/knox/bridge/BridgeService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1412
    :catch_0
    move-exception v0

    .line 1413
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Exception caught in execute command"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1415
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v6, "launchNotification"

    iget-object v7, p1, Landroid/app/Command;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1416
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, " IBridgeCommandExecutor launchPendingIntent is received"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    :try_start_1
    iget-object v6, p1, Landroid/app/Command;->contentIntent:Landroid/app/PendingIntent;

    if-eqz v6, :cond_3

    .line 1419
    iget-object v5, p1, Landroid/app/Command;->contentIntent:Landroid/app/PendingIntent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1421
    .local v5, "wrappedContentIntent":Landroid/app/PendingIntent;
    :try_start_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " IBridgeCommandExecutor launchContentIntent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Landroid/app/Command;->contentIntent:Landroid/app/PendingIntent;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    invoke-virtual {v5}, Landroid/app/PendingIntent;->send()V
    :try_end_2
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1429
    .end local v5    # "wrappedContentIntent":Landroid/app/PendingIntent;
    :cond_3
    :goto_1
    :try_start_3
    iget-object v4, p1, Landroid/app/Command;->intent:Landroid/content/Intent;

    .line 1430
    .local v4, "notiInfoIntent":Landroid/content/Intent;
    if-eqz v4, :cond_0

    .line 1431
    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1432
    .local v1, "intentBundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 1433
    const-string v6, "sbnPackage"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1434
    .local v2, "mPkg":Ljava/lang/String;
    const-string v6, "sbnTag"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1435
    .local v3, "mTag":Ljava/lang/String;
    const-string v6, "sbnId"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 1447
    .end local v1    # "intentBundle":Landroid/os/Bundle;
    .end local v2    # "mPkg":Ljava/lang/String;
    .end local v3    # "mTag":Ljava/lang/String;
    .end local v4    # "notiInfoIntent":Landroid/content/Intent;
    :catch_1
    move-exception v0

    .line 1448
    .restart local v0    # "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v6

    const-string v7, " Exception caught in execute command"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1424
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v5    # "wrappedContentIntent":Landroid/app/PendingIntent;
    :catch_2
    move-exception v6

    goto :goto_1
.end method
