.class public Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;
.super Ljava/lang/Object;
.source "KnoxClipboardFileManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;
    }
.end annotation


# instance fields
.field dataClipList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/sec/clipboard/data/file/WrapFileClipData;",
            ">;"
        }
    .end annotation
.end field

.field private isShared:Z

.field private final mSupportKOX:Z

.field userFS:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1, "clipInfoFile"    # Ljava/io/File;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-boolean v0, Landroid/sec/clipboard/data/ClipboardDefine;->SUPPORT_KNOX:Z

    iput-boolean v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->mSupportKOX:Z

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->isShared:Z

    .line 46
    const-string v0, "KnoxClipboardFileManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KnoxClipboardFileManager() - initializing FM with path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;

    invoke-direct {v0, p1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->userFS:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;

    .line 49
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->userFS:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;

    invoke-virtual {v0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager$ConnectFileSystem;->load()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->dataClipList:Ljava/util/ArrayList;

    .line 50
    return-void
.end method


# virtual methods
.method public getWrap(I)Landroid/sec/clipboard/data/file/WrapFileClipData;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 258
    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->dataClipList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/data/file/WrapFileClipData;

    .line 261
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->dataClipList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 236
    const-string v0, "KnoxClipboardFileManager"

    const-string v1, "size() - list of clips is NULL"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v0, -0x1

    .line 239
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardFileManager;->dataClipList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method
