.class Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;
.super Ljava/lang/Thread;
.source "KnoxClipboardDataSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->onStartCommand(Landroid/content/Intent;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    # getter for: Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->KNOX_DEBUG:Z
    invoke-static {v0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->access$000(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    iget-object v0, v0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->TAG:Ljava/lang/String;

    const-string v1, "onStartCommand() DO_SYNC thread started"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    # invokes: Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->doSync()V
    invoke-static {v0}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->access$200(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;)V

    .line 181
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync$2;->this$0:Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;

    const-string v1, "DO_SYNC"

    # invokes: Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->stopMySelf(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;->access$300(Lcom/sec/knox/bridge/operations/clipboard/KnoxClipboardDataSync;Ljava/lang/String;)V

    .line 182
    return-void
.end method
