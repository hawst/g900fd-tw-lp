.class public Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;
.super Landroid/content/IRCPInterface$Stub;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "IRCPInterfaceCallBack"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method protected constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 1883
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/content/IRCPInterface$Stub;-><init>()V

    .line 1982
    return-void
.end method

.method static synthetic access$2200(Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 1883
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->changeExtSdPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;
    .param p1, "x1"    # I

    .prologue
    .line 1883
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->isContainerStateIsValid(I)Z

    move-result v0

    return v0
.end method

.method private changeExtSdPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "srcFilePath"    # Ljava/lang/String;
    .param p2, "destFilePath"    # Ljava/lang/String;

    .prologue
    .line 2355
    move-object v1, p2

    .line 2357
    .local v1, "retFilePath":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2358
    const-string v2, "/mnt/extSdCard"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2359
    const-string v2, "^/mnt/extSdCard"

    const-string v3, "/mnt/sdcard"

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2370
    :cond_0
    :goto_0
    return-object v1

    .line 2361
    :cond_1
    const-string v2, "/storage/extSdCard"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2362
    const-string v2, "^/storage/extSdCard"

    const-string v3, "/mnt/sdcard"

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 2365
    :catch_0
    move-exception v0

    .line 2366
    .local v0, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 2367
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "changeExtSdPath(): npe has occured"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isContainerStateIsValid(I)Z
    .locals 5
    .param p1, "moveToDstCId"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2324
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    if-ne p1, v3, :cond_0

    .line 2325
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Move to Personal mode. return true"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351
    :goto_0
    return v1

    .line 2328
    :cond_0
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v0

    .line 2329
    .local v0, "info":Landroid/content/pm/PersonaInfo;
    if-nez v0, :cond_1

    .line 2330
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t get a persona info. moveToDstCId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2331
    goto :goto_0

    .line 2334
    :cond_1
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2335
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Persona is superlocked"

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2336
    goto :goto_0

    .line 2337
    :cond_2
    iget-boolean v3, v0, Landroid/content/pm/PersonaInfo;->partial:Z

    if-ne v3, v1, :cond_3

    .line 2338
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Persona is partially created"

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2339
    goto :goto_0

    .line 2340
    :cond_3
    iget-boolean v3, v0, Landroid/content/pm/PersonaInfo;->removePersona:Z

    if-ne v3, v1, :cond_4

    .line 2341
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Persona is removed"

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2342
    goto :goto_0

    .line 2344
    :cond_4
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->DELETING:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->CREATING:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2346
    :cond_5
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Persona state is invalid for moving files "

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 2347
    goto/16 :goto_0

    .line 2350
    :cond_6
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Persona state is valid for moving files"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public cancel(J)V
    .locals 5
    .param p1, "threadId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2560
    const-string v2, "cancel"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v2}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2561
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cancel() threadId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2563
    const/4 v1, 0x0

    .line 2565
    .local v1, "cancelFileOperation":Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v3

    monitor-enter v3

    .line 2566
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;

    move-object v1, v0

    .line 2567
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2569
    if-eqz v1, :cond_0

    .line 2570
    invoke-virtual {v1}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->cancel()V

    .line 2571
    :cond_0
    return-void

    .line 2567
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public cancelCopyChunks(J)V
    .locals 3
    .param p1, "sessionId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2553
    const-string v0, "cancelCopyChunks"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2554
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelCopyChunks() sessionId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2555
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1, p2}, Landroid/os/RCPManager;->cancelCopyChunks(J)V

    .line 2556
    return-void
.end method

.method public copyChunks(ILjava/lang/String;ILjava/lang/String;JIJZ)I
    .locals 2
    .param p1, "srcContainerId"    # I
    .param p2, "srcFilePath"    # Ljava/lang/String;
    .param p3, "destContainerId"    # I
    .param p4, "destFilePath"    # Ljava/lang/String;
    .param p5, "offset"    # J
    .param p7, "length"    # I
    .param p8, "sessionId"    # J
    .param p10, "deleteSrc"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2534
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Warning!!!!  copyChunks() is disabled!!!"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2535
    const v0, -0x2ed993

    return v0
.end method

.method public copyFile(ILjava/lang/String;ILjava/lang/String;)I
    .locals 5
    .param p1, "srcContainerId"    # I
    .param p2, "srcFilePath"    # Ljava/lang/String;
    .param p3, "destContainerId"    # I
    .param p4, "destFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2245
    const-string v2, "copyFile"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v2}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2246
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copyFile() srcContainerId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; srcFilePath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; destContainerId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; destFilePath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2251
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const/4 v3, 0x1

    # invokes: Lcom/sec/knox/bridge/BridgeService;->getFilesPolicy(II)I
    invoke-static {v2, p1, v3}, Lcom/sec/knox/bridge/BridgeService;->access$2400(Lcom/sec/knox/bridge/BridgeService;II)I

    move-result v0

    .line 2252
    .local v0, "exportCheck":I
    if-eqz v0, :cond_0

    .line 2253
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copyFile(): Permissions (POLICY_NOT_ALLOWED)  or error for srcContainerId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; exportCheck="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2270
    .end local v0    # "exportCheck":I
    :goto_0
    return v0

    .line 2260
    .restart local v0    # "exportCheck":I
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const/4 v3, 0x2

    # invokes: Lcom/sec/knox/bridge/BridgeService;->getFilesPolicy(II)I
    invoke-static {v2, p3, v3}, Lcom/sec/knox/bridge/BridgeService;->access$2400(Lcom/sec/knox/bridge/BridgeService;II)I

    move-result v1

    .line 2261
    .local v1, "importCheck":I
    if-eqz v1, :cond_1

    .line 2262
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copyFile(): Permissions (POLICY_NOT_ALLOWED) or error for destContainerId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; importCheck="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 2265
    goto :goto_0

    .line 2268
    :cond_1
    invoke-direct {p0, p2, p4}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->changeExtSdPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 2270
    sget-object v2, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/os/RCPManager;->copyFileInternal(ILjava/lang/String;ILjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public copyFiles(ILjava/util/List;ILjava/util/List;Landroid/content/IRCPInterfaceCallback;)J
    .locals 16
    .param p1, "srcContainerId"    # I
    .param p3, "destContainerId"    # I
    .param p5, "callback"    # Landroid/content/IRCPInterfaceCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/IRCPInterfaceCallback;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1906
    .local p2, "srcFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "destFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "copyFiles"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v3}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1907
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "copyFiles() srcContainerId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; srcFilePaths="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; destContainerId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; destFilePaths="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1912
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v12, v3, :cond_0

    .line 1913
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 1914
    .local v13, "srcPath":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1915
    .local v11, "destPath":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v11}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->changeExtSdPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1917
    .local v10, "chDestPath":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-interface {v0, v12, v10}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1912
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 1920
    .end local v10    # "chDestPath":Ljava/lang/String;
    .end local v11    # "destPath":Ljava/lang/String;
    .end local v13    # "srcPath":Ljava/lang/String;
    :cond_0
    new-instance v2, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v9, p5

    invoke-direct/range {v2 .. v9}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;-><init>(Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;ILjava/util/List;ILjava/util/List;ZLandroid/content/IRCPInterfaceCallback;)V

    .line 1922
    .local v2, "newRequest":Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;
    invoke-virtual {v2}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->getId()J

    move-result-wide v14

    .line 1923
    .local v14, "threadId":J
    invoke-virtual {v2, v14, v15}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->setSessionId(J)V

    .line 1925
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v4

    monitor-enter v4

    .line 1926
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1927
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929
    invoke-virtual {v2}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->start()V

    .line 1931
    return-wide v14

    .line 1927
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public getErrorMessage(I)Ljava/lang/String;
    .locals 6
    .param p1, "errorId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2457
    const-string v2, "getErrorMessage"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v2}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2458
    const/16 v1, 0x1f4

    .line 2460
    .local v1, "resourceId":I
    sparse-switch p1, :sswitch_data_0

    .line 2490
    const v1, 0x7f060004

    .line 2493
    :goto_0
    const v2, 0x7f060002

    if-ne v1, v2, :cond_0

    .line 2494
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/knox/bridge/BridgeService;->mCurrentInvalidKnoxName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2503
    :goto_1
    return-object v2

    .line 2462
    :sswitch_0
    const v1, 0x7f060005

    .line 2463
    goto :goto_0

    .line 2466
    :sswitch_1
    const v1, 0x7f060006

    .line 2467
    goto :goto_0

    .line 2470
    :sswitch_2
    const v1, 0x7f060003

    .line 2471
    goto :goto_0

    .line 2474
    :sswitch_3
    const v1, 0x7f060002

    .line 2475
    goto :goto_0

    .line 2486
    :sswitch_4
    const v1, 0x7f060004

    .line 2487
    goto :goto_0

    .line 2496
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    .line 2498
    :catch_0
    move-exception v0

    .line 2499
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getErrorMessage(): cannot get error resource for resourceId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2503
    const-string v2, "General error"

    goto :goto_1

    .line 2460
    :sswitch_data_0
    .sparse-switch
        -0xf4242 -> :sswitch_4
        -0xf4241 -> :sswitch_3
        -0xf4240 -> :sswitch_2
        -0x24 -> :sswitch_4
        -0x1c -> :sswitch_1
        -0x10 -> :sswitch_4
        -0xd -> :sswitch_4
        -0x2 -> :sswitch_4
        0x0 -> :sswitch_0
        0x1 -> :sswitch_4
        0x6 -> :sswitch_4
        0x8 -> :sswitch_1
        0xa -> :sswitch_4
        0xb -> :sswitch_2
        0xc -> :sswitch_3
        0xd -> :sswitch_4
        0xe -> :sswitch_4
    .end sparse-switch
.end method

.method public getFileInfo(Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "containerId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2520
    const-string v0, "getFileInfo"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2521
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFileInfo(): path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; containerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2522
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1, p2}, Landroid/os/RCPManager;->getFileInfo(Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getFiles(Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "containerId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2514
    const-string v0, "getFiles"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2515
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1, p2}, Landroid/os/RCPManager;->getFiles(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method isContainerIdIsValid(II)Z
    .locals 3
    .param p1, "srcContainerId"    # I
    .param p2, "destContainerId"    # I

    .prologue
    const/4 v1, 0x0

    .line 2374
    if-ne p1, p2, :cond_1

    .line 2396
    :cond_0
    :goto_0
    return v1

    .line 2377
    :cond_1
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    .line 2381
    const/4 v0, 0x0

    .line 2382
    .local v0, "uid":I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    .line 2384
    if-nez v0, :cond_3

    .line 2385
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v2, p2}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    .line 2396
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 2388
    :cond_3
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v2, v0}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2389
    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v2, p1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method public isFileExist(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "containerId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2508
    const-string v0, "isFileExist"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2509
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1, p2}, Landroid/os/RCPManager;->isFileExist(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method isMoveFilesAllowed(II)Z
    .locals 12
    .param p1, "srcContainerId"    # I
    .param p2, "destContainerId"    # I

    .prologue
    .line 2400
    const/4 v4, 0x0

    .line 2402
    .local v4, "pi":Landroid/content/pm/PersonaInfo;
    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v9, v9, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v9, v9, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v9, p1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2403
    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v9, v9, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v9, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v4

    .line 2408
    :goto_0
    if-eqz v4, :cond_1

    iget-boolean v9, v4, Landroid/content/pm/PersonaInfo;->isUserManaged:Z

    if-eqz v9, :cond_1

    .line 2409
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    const-string v10, "isMoveFilesAllowed : user managed container. return true"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410
    const/4 v6, 0x1

    .line 2452
    :goto_1
    return v6

    .line 2405
    :cond_0
    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v9, v9, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v9, p2}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v4

    goto :goto_0

    .line 2413
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v2

    .line 2414
    .local v2, "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    const/4 v8, 0x0

    .line 2415
    .local v8, "uid":I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v8

    .line 2416
    const/4 v7, 0x0

    .line 2417
    .local v7, "retToOwner":Z
    const/4 v6, 0x1

    .line 2419
    .local v6, "retToKnox":Z
    if-nez v8, :cond_2

    .line 2421
    :try_start_0
    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v9, v9, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v9, p2}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v0

    .line 2423
    .local v0, "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getRCPPolicy()Lcom/sec/enterprise/knox/container/RCPPolicy;

    move-result-object v5

    .line 2424
    .local v5, "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/container/RCPPolicy;->isMoveFilesToContainerAllowed()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    .line 2432
    .end local v0    # "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .end local v5    # "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    :goto_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isMoveFilesAllowed : retToKnox = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2425
    :catch_0
    move-exception v1

    .line 2426
    .local v1, "e":Ljava/lang/SecurityException;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    const-string v10, "isMoveFilesAllowed : SecurityException occured"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2427
    const/4 v6, 0x1

    .line 2431
    goto :goto_2

    .line 2428
    .end local v1    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v3

    .line 2429
    .local v3, "npe":Ljava/lang/NullPointerException;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    const-string v10, "isMoveFilesAllowed : NullPointerException occured"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2430
    const/4 v6, 0x1

    goto :goto_2

    .line 2435
    .end local v3    # "npe":Ljava/lang/NullPointerException;
    :cond_2
    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v9, v9, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v9, v8}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2437
    :try_start_1
    iget-object v9, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v9, v9, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v9, v8}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v0

    .line 2438
    .restart local v0    # "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getRCPPolicy()Lcom/sec/enterprise/knox/container/RCPPolicy;

    move-result-object v5

    .line 2439
    .restart local v5    # "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/container/RCPPolicy;->isMoveFilesToOwnerAllowed()Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v7

    .line 2447
    .end local v0    # "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .end local v5    # "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    :goto_3
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isMoveFilesAllowed : retToOwner = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 2448
    goto/16 :goto_1

    .line 2440
    :catch_2
    move-exception v1

    .line 2441
    .restart local v1    # "e":Ljava/lang/SecurityException;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    const-string v10, "isMoveFilesAllowed : SecurityException occured"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2442
    const/4 v7, 0x0

    .line 2446
    goto :goto_3

    .line 2443
    .end local v1    # "e":Ljava/lang/SecurityException;
    :catch_3
    move-exception v3

    .line 2444
    .restart local v3    # "npe":Ljava/lang/NullPointerException;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    const-string v10, "isMoveFilesAllowed : NullPointerException occured"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    const/4 v7, 0x0

    goto :goto_3

    .line 2451
    .end local v3    # "npe":Ljava/lang/NullPointerException;
    :cond_3
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v9

    const-string v10, "isMoveFilesAllowed : End of function. Return false"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2452
    const/4 v6, 0x0

    goto/16 :goto_1
.end method

.method public moveFile(ILjava/lang/String;ILjava/lang/String;)I
    .locals 5
    .param p1, "srcContainerId"    # I
    .param p2, "srcFilePath"    # Ljava/lang/String;
    .param p3, "destContainerId"    # I
    .param p4, "destFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2276
    const-string v2, "moveFile"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v2}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2277
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "moveFile() srcContainerId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; srcFilePath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; destContainerId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; destFilePath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2281
    invoke-virtual {p0, p1, p3}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->isContainerIdIsValid(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2282
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "moveFile() containerID is not valid"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2283
    const/4 v2, 0x6

    .line 2320
    :goto_0
    return v2

    .line 2286
    :cond_0
    invoke-direct {p0, p3}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->isContainerStateIsValid(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2287
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fileOperation() container state is not valid"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2290
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const-string v3, "user"

    invoke-virtual {v2, v3}, Lcom/sec/knox/bridge/BridgeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    .line 2291
    .local v1, "um":Landroid/os/UserManager;
    iget-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v2, v2, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v2, p3}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    .line 2292
    .local v0, "ui":Landroid/content/pm/UserInfo;
    iget-object v2, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    sput-object v2, Lcom/sec/knox/bridge/BridgeService;->mCurrentInvalidKnoxName:Ljava/lang/String;

    .line 2294
    const v2, -0xf4241

    goto :goto_0

    .line 2297
    .end local v0    # "ui":Landroid/content/pm/UserInfo;
    .end local v1    # "um":Landroid/os/UserManager;
    :cond_1
    invoke-virtual {p0, p1, p3}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->isMoveFilesAllowed(II)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2298
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "moveFile() policy is denied"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299
    const/16 v2, 0xb

    goto :goto_0

    .line 2302
    :cond_2
    invoke-static {p2}, Lcom/sec/knox/bridge/util/FileUtils;->isAllowedFileName(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2303
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "moveFile() not allowed file name"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2304
    const/4 v2, 0x5

    goto :goto_0

    .line 2318
    :cond_3
    invoke-direct {p0, p2, p4}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->changeExtSdPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 2320
    sget-object v2, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/os/RCPManager;->moveFile(ILjava/lang/String;ILjava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public moveFiles(ILjava/util/List;ILjava/util/List;Landroid/content/IRCPInterfaceCallback;)J
    .locals 10
    .param p1, "srcContainerId"    # I
    .param p3, "destContainerId"    # I
    .param p5, "callback"    # Landroid/content/IRCPInterfaceCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/IRCPInterfaceCallback;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1938
    .local p2, "srcFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "destFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "moveFiles"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1939
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "moveFiles() srcContainerId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; srcFilePaths="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; destContainerId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; destFilePaths="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1944
    new-instance v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;

    const/4 v6, 0x1

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;-><init>(Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;ILjava/util/List;ILjava/util/List;ZLandroid/content/IRCPInterfaceCallback;)V

    .line 1946
    .local v0, "newRequest":Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;
    invoke-virtual {v0}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->getId()J

    move-result-wide v8

    .line 1947
    .local v8, "threadId":J
    invoke-virtual {v0, v8, v9}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->setSessionId(J)V

    .line 1949
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v2

    monitor-enter v2

    .line 1950
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1953
    invoke-virtual {v0}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->start()V

    .line 1955
    return-wide v8

    .line 1951
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public moveFilesForApp(ILjava/util/List;Ljava/util/List;)J
    .locals 6
    .param p1, "requestApp"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1961
    .local p2, "srcFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "destFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "moveFilesForApp"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v3}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1962
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moveFilesForApp() srcFilePaths="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; destFilePaths="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "reqApp = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1967
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1968
    .local v2, "srcPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1970
    .local v0, "dstPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1971
    .local v1, "moveTo":Landroid/content/Intent;
    const-string v3, "srcFilePaths"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1972
    const-string v3, "dstFilePaths"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1973
    const-string v3, "requestApp"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1974
    const-string v3, "launchAfterLockCheck"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1975
    const-string v3, "moveToDstCId"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1976
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1977
    iget-object v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1979
    const-wide/16 v4, 0x0

    return-wide v4
.end method
