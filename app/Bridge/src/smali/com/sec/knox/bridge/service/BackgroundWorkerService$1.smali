.class Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;
.super Ljava/lang/Object;
.source "BackgroundWorkerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/service/BackgroundWorkerService;->showToast(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/service/BackgroundWorkerService;

.field final synthetic val$aDuration:I

.field final synthetic val$aMsg:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/service/BackgroundWorkerService;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->this$0:Lcom/sec/knox/bridge/service/BackgroundWorkerService;

    iput-object p2, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->val$aMsg:Ljava/lang/String;

    iput p3, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->val$aDuration:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 272
    sget-boolean v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v0, :cond_0

    .line 273
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->this$0:Lcom/sec/knox/bridge/service/BackgroundWorkerService;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->val$aMsg:Ljava/lang/String;

    iget v2, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->val$aDuration:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 283
    :goto_0
    return-void

    .line 278
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->this$0:Lcom/sec/knox/bridge/service/BackgroundWorkerService;

    const v2, 0x1030128

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->val$aMsg:Ljava/lang/String;

    iget v2, p0, Lcom/sec/knox/bridge/service/BackgroundWorkerService$1;->val$aDuration:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
