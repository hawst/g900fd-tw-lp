.class public Lcom/sec/knox/bridge/operations/ExchangeContactData;
.super Ljava/lang/Object;
.source "ExchangeContactData.java"


# static fields
.field public static final CONTACT_DATA_PROJECTION:[Ljava/lang/String;

.field public static final CONTACT_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

.field public static CON_DATA_URI:Landroid/net/Uri;

.field public static CON_RAW_CONTACTS_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    .line 26
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CON_DATA_URI:Landroid/net/Uri;

    .line 28
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "is_primary"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "data10"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "data11"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "data12"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "data13"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "data14"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "data15"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CONTACT_DATA_PROJECTION:[Ljava/lang/String;

    .line 50
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "version"

    aput-object v1, v0, v3

    const-string v1, "contact_id"

    aput-object v1, v0, v4

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "starred"

    aput-object v1, v0, v6

    const-string v1, "name_verified"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "display_name_source"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CONTACT_RAWCONTACTS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static getContactData(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 110
    const/4 v10, 0x0

    .line 111
    .local v10, "result":Landroid/database/Cursor;
    const-string v0, "ExchangeData"

    const-string v1, "getContactData() START"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 117
    if-eqz v10, :cond_5

    .line 119
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 120
    const/4 v8, 0x0

    .local v8, "k":I
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v8, v0, :cond_2

    .line 121
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    const/4 v7, 0x0

    .local v7, "h":I
    :goto_1
    array-length v0, p1

    if-ge v7, v0, :cond_0

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 127
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    .line 120
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 129
    .end local v7    # "h":I
    :cond_1
    const-string v0, "ExchangeData"

    const-string v1, "getDataTableData After Last"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 137
    new-instance v11, Landroid/database/CursorWindow;

    const-string v0, "MyCursorWindow"

    invoke-direct {v11, v0}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 138
    .local v11, "window":Landroid/database/CursorWindow;
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 139
    new-instance v6, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v6, v10}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 140
    .local v6, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v11}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 142
    new-instance v9, Landroid/content/CustomCursor;

    invoke-direct {v9, v11}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 143
    .local v9, "remoteCursor":Landroid/content/CustomCursor;
    invoke-interface {v10}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 149
    if-eqz v6, :cond_3

    .line 150
    invoke-virtual {v6}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 153
    :cond_3
    if-eqz v9, :cond_4

    .line 154
    const-string v0, "ExchangeData"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getContactData returns count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Landroid/content/CustomCursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/content/CustomCursor;->setAutoClose(Z)V

    .line 166
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v8    # "k":I
    .end local v9    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v11    # "window":Landroid/database/CursorWindow;
    :goto_2
    return-object v9

    .line 158
    .restart local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .restart local v8    # "k":I
    .restart local v9    # "remoteCursor":Landroid/content/CustomCursor;
    .restart local v11    # "window":Landroid/database/CursorWindow;
    :cond_4
    const-string v0, "ExchangeData"

    const-string v1, "getContactData returns null "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 164
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v8    # "k":I
    .end local v9    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v11    # "window":Landroid/database/CursorWindow;
    :cond_5
    const-string v0, "ExchangeData"

    const-string v1, "getContactData() END"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v4

    .line 166
    goto :goto_2
.end method

.method public static getListIds([J)Ljava/lang/String;
    .locals 6
    .param p0, "ids"    # [J

    .prologue
    .line 387
    const/4 v1, 0x0

    .line 389
    .local v1, "ret":Ljava/lang/String;
    if-eqz p0, :cond_0

    array-length v2, p0

    if-nez v2, :cond_1

    .line 390
    :cond_0
    const-string v2, ""

    .line 399
    :goto_0
    return-object v2

    .line 393
    :cond_1
    const-string v1, "0"

    .line 395
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 396
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-wide v4, p0, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 395
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 399
    goto :goto_0
.end method

.method public static getRawContacts(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 64
    const/4 v8, 0x0

    .line 66
    .local v8, "result":Landroid/database/Cursor;
    const-string v0, "ExchangeData"

    const-string v1, "getRawContacts() START"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 76
    if-eqz v8, :cond_2

    .line 78
    new-instance v9, Landroid/database/CursorWindow;

    const-string v0, "MyCursorWindow"

    invoke-direct {v9, v0}, Landroid/database/CursorWindow;-><init>(Ljava/lang/String;)V

    .line 79
    .local v9, "window":Landroid/database/CursorWindow;
    new-instance v6, Landroid/database/CrossProcessCursorWrapper;

    invoke-direct {v6, v8}, Landroid/database/CrossProcessCursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 80
    .local v6, "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v9}, Landroid/database/CrossProcessCursorWrapper;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 82
    new-instance v7, Landroid/content/CustomCursor;

    invoke-direct {v7, v9}, Landroid/content/CustomCursor;-><init>(Landroid/database/CursorWindow;)V

    .line 83
    .local v7, "remoteCursor":Landroid/content/CustomCursor;
    invoke-virtual {v7, p1}, Landroid/content/CustomCursor;->setColumnNames([Ljava/lang/String;)V

    .line 89
    if-eqz v6, :cond_0

    .line 90
    invoke-virtual {v6}, Landroid/database/CrossProcessCursorWrapper;->close()V

    .line 93
    :cond_0
    if-eqz v7, :cond_1

    .line 94
    const-string v0, "ExchangeData"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRawContacts returns value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/content/CustomCursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/content/CustomCursor;->setAutoClose(Z)V

    .line 104
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v9    # "window":Landroid/database/CursorWindow;
    :goto_0
    return-object v7

    .line 98
    .restart local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .restart local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .restart local v9    # "window":Landroid/database/CursorWindow;
    :cond_1
    const-string v0, "ExchangeData"

    const-string v1, "getRawContacts returns null "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    .end local v6    # "crossProcessCursor":Landroid/database/CrossProcessCursorWrapper;
    .end local v7    # "remoteCursor":Landroid/content/CustomCursor;
    .end local v9    # "window":Landroid/database/CursorWindow;
    :cond_2
    const-string v0, "ExchangeData"

    const-string v1, "getRawContacts returns null "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v4

    .line 104
    goto :goto_0
.end method

.method public static insertContact(Landroid/content/Context;Landroid/content/CustomCursor;Landroid/content/CustomCursor;)Landroid/os/Bundle;
    .locals 13
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "raw_contact"    # Landroid/content/CustomCursor;
    .param p2, "contact_data"    # Landroid/content/CustomCursor;

    .prologue
    .line 172
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 173
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v10, "ExchangeData"

    const-string v11, "insertContact() START"

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    if-eqz p1, :cond_0

    if-nez p2, :cond_3

    .line 176
    :cond_0
    const-string v10, "ExchangeData"

    const-string v11, "raw_contact OR contact_data is null. Do nothing"

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    if-eqz p1, :cond_1

    .line 178
    invoke-virtual {p1}, Landroid/content/CustomCursor;->close()V

    .line 180
    :cond_1
    if-eqz p2, :cond_2

    .line 181
    invoke-virtual {p2}, Landroid/content/CustomCursor;->close()V

    .line 184
    :cond_2
    :try_start_0
    const-string v10, "result"

    const-string v11, "false"

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    :goto_0
    return-object v1

    .line 186
    :catch_0
    move-exception v4

    .line 187
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 192
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-virtual {p1}, Landroid/content/CustomCursor;->moveToFirst()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 201
    const-string v10, "_id"

    invoke-virtual {p1, v10}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {p1, v10}, Landroid/content/CustomCursor;->getLong(I)J

    move-result-wide v8

    .line 203
    .local v8, "s_id":J
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .local v7, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v10, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CON_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    const-string v11, "account_type"

    const-string v12, "vnd.sec.contact.phone"

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    const-string v11, "account_name"

    const-string v12, "vnd.sec.contact.phone"

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    const-string v11, "starred"

    const-string v12, "starred"

    invoke-virtual {p1, v12}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {p1, v12}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    const-string v11, "display_name_source"

    const-string v12, "display_name_source"

    invoke-virtual {p1, v12}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {p1, v12}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    const-string v11, "name_verified"

    const-string v12, "name_verified"

    invoke-virtual {p1, v12}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {p1, v12}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    const-string v11, "raw_contact_is_read_only"

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    if-eqz p2, :cond_9

    :try_start_1
    invoke-virtual {p2}, Landroid/content/CustomCursor;->getCount()I

    move-result v10

    if-eqz v10, :cond_9

    .line 230
    invoke-virtual {p2}, Landroid/content/CustomCursor;->moveToFirst()Z

    .line 235
    :cond_4
    sget-object v10, Lcom/sec/knox/bridge/operations/ExchangeContactData;->CON_DATA_URI:Landroid/net/Uri;

    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 238
    .local v0, "builder_data":Landroid/content/ContentProviderOperation$Builder;
    const-string v10, "mimetype"

    invoke-virtual {p2, v10}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {p2, v10}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 242
    .local v6, "mimetype":Ljava/lang/String;
    const-string v10, "mimetype"

    invoke-virtual {v0, v10, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 246
    const-string v10, "is_primary"

    const-string v11, "is_primary"

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 251
    const-string v10, "is_super_primary"

    const-string v11, "is_super_primary"

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getInt(I)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 257
    const-string v10, "vnd.android.cursor.item/photo"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 259
    const-string v10, "data14"

    const-string v11, "data14"

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 265
    const-string v10, "data15"

    const-string v11, "data15"

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {p2, v11}, Landroid/content/CustomCursor;->getBlob(I)[B

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 350
    :cond_5
    const-string v10, "is_read_only"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 352
    const-string v10, "raw_contact_id"

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 356
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    :cond_6
    invoke-virtual {p2}, Landroid/content/CustomCursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    if-nez v10, :cond_4

    .line 369
    .end local v0    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .end local v6    # "mimetype":Ljava/lang/String;
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "com.android.contacts"

    invoke-virtual {v10, v11, v7}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v3

    .line 371
    .local v3, "cpr":[Landroid/content/ContentProviderResult;
    const-string v10, "result"

    const-string v11, "true"

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 379
    .end local v3    # "cpr":[Landroid/content/ContentProviderResult;
    .end local v7    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v8    # "s_id":J
    :cond_7
    :goto_2
    invoke-virtual {p1}, Landroid/content/CustomCursor;->close()V

    .line 380
    invoke-virtual {p2}, Landroid/content/CustomCursor;->close()V

    goto/16 :goto_0

    .line 272
    .restart local v0    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .restart local v6    # "mimetype":Ljava/lang/String;
    .restart local v7    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v8    # "s_id":J
    :cond_8
    :try_start_3
    const-string v10, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 339
    invoke-virtual {p2}, Landroid/content/CustomCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    .line 341
    .local v2, "column_names":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_3
    array-length v10, v2

    if-ge v5, v10, :cond_5

    .line 342
    aget-object v10, v2, v5

    invoke-virtual {p2, v5}, Landroid/content/CustomCursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 341
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 360
    .end local v0    # "builder_data":Landroid/content/ContentProviderOperation$Builder;
    .end local v2    # "column_names":[Ljava/lang/String;
    .end local v5    # "k":I
    .end local v6    # "mimetype":Ljava/lang/String;
    :cond_9
    const-string v10, "ExchangeData"

    const-string v11, "contact data is null"

    invoke-static {v10, v11}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 362
    :catch_1
    move-exception v4

    .line 363
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 365
    invoke-virtual {p1}, Landroid/content/CustomCursor;->moveToNext()Z

    goto :goto_1

    .line 372
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v4

    .line 373
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method
