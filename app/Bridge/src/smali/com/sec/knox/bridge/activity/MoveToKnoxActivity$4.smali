.class Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;
.super Ljava/lang/Object;
.source "MoveToKnoxActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showDstKnoxSelectionDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

.field final synthetic val$knoxSecondId:I

.field final synthetic val$knoxSecondName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    iput p2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->val$knoxSecondId:I

    iput-object p3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->val$knoxSecondName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    iget v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->val$knoxSecondId:I

    # setter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstCId:I
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$902(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;I)I

    .line 337
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->val$knoxSecondName:Ljava/lang/String;

    # setter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToDstName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1002(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 338
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 339
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mDstKnoxSelectionDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1102(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 341
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$4;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # invokes: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->moveToKnox()V
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V

    .line 342
    return-void
.end method
