.class Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$5;
.super Ljava/lang/Object;
.source "MoveToKnoxActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->showDstKnoxSelectionDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$5;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$5;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mDstKnoxSelectionDialog is canceled. finish the activity"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$5;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-virtual {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->finish()V

    .line 370
    return-void
.end method
