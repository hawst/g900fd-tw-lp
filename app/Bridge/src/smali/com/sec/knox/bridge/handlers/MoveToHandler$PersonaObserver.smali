.class Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;
.super Landroid/content/pm/AbstractPersonaObserver;
.source "MoveToHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/handlers/MoveToHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonaObserver"
.end annotation


# instance fields
.field private mCtx:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/handlers/MoveToHandler;Landroid/content/Context;II)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "containerId"    # I
    .param p4, "flags"    # I

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    .line 148
    invoke-direct {p0, p2, p3, p4}, Landroid/content/pm/AbstractPersonaObserver;-><init>(Landroid/content/Context;II)V

    .line 149
    iput-object p2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->mCtx:Landroid/content/Context;

    .line 150
    return-void
.end method


# virtual methods
.method public onKeyGuardStateChanged(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 200
    return-void
.end method

.method public onSessionExpired()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public onStateChange(Landroid/content/pm/PersonaState;Landroid/content/pm/PersonaState;)V
    .locals 7
    .param p1, "state"    # Landroid/content/pm/PersonaState;
    .param p2, "oldState"    # Landroid/content/pm/PersonaState;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 158
    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStateChange state["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " oldState["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] + user["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mMoveToKnoxIsProgressing["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z
    invoke-static {v4}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$100(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    sget-object v2, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {p2, v2}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    invoke-virtual {p1, v2}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z
    invoke-static {v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$100(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z

    move-result v2

    if-ne v2, v5, :cond_2

    .line 164
    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onStateChange : Is container unlocked by user?"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # setter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z
    invoke-static {v2, v6}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$102(Lcom/sec/knox/bridge/handlers/MoveToHandler;Z)Z

    .line 166
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToFiles:Z
    invoke-static {v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$200(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z

    move-result v2

    if-ne v2, v5, :cond_1

    .line 167
    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onStateChange : moving files"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->mCtx:Landroid/content/Context;

    const-class v3, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 169
    .local v1, "moveTo":Landroid/content/Intent;
    const-string v3, "srcFilePaths"

    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mSrcPaths:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$300(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 171
    const-string v3, "dstFilePaths"

    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mDstPaths:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$400(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 173
    const-string v2, "requestApp"

    iget-object v3, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestApp:I
    invoke-static {v3}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$500(Lcom/sec/knox/bridge/handlers/MoveToHandler;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const-string v2, "launchAfterLockCheck"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 175
    const-string v2, "moveToDstCId"

    iget-object v3, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I
    invoke-static {v3}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$600(Lcom/sec/knox/bridge/handlers/MoveToHandler;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 176
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 177
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->mCtx:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 188
    .end local v1    # "moveTo":Landroid/content/Intent;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    iget-object v3, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I
    invoke-static {v3}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$600(Lcom/sec/knox/bridge/handlers/MoveToHandler;)I

    move-result v3

    # invokes: Lcom/sec/knox/bridge/handlers/MoveToHandler;->unregisterPersonaObserver(I)V
    invoke-static {v2, v3}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$900(Lcom/sec/knox/bridge/handlers/MoveToHandler;I)V

    .line 196
    :goto_1
    return-void

    .line 178
    :cond_1
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mRequestFromMoveToContact:Z
    invoke-static {v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$700(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z

    move-result v2

    if-ne v2, v5, :cond_0

    .line 179
    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onStateChange : moving contact data"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string v2, "action.com.sec.knox.container.exchangeData"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mInputBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$800(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 182
    const-string v2, "moveToDstCId"

    iget-object v3, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToDstCId:I
    invoke-static {v3}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$600(Lcom/sec/knox/bridge/handlers/MoveToHandler;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 183
    const-string v2, "launchFromPersonaManager"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 184
    const-string v2, "com.sec.knox.bridge"

    const-string v3, "com.sec.knox.bridge.operations.ExchangeDataReceiver"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->mCtx:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 191
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStateChange state["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " oldState["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] + user["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mMoveToKnoxIsProgressing["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/MoveToHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/MoveToHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/MoveToHandler;->mMoveToKnoxIsProgressing:Z
    invoke-static {v4}, Lcom/sec/knox/bridge/handlers/MoveToHandler;->access$100(Lcom/sec/knox/bridge/handlers/MoveToHandler;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
