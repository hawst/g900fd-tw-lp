.class Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;
.super Landroid/content/pm/AbstractPersonaObserver;
.source "NotificationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/handlers/NotificationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonaObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/handlers/NotificationHandler;Landroid/content/Context;II)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "containerId"    # I
    .param p4, "flags"    # I

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;

    .line 212
    invoke-direct {p0, p2, p3, p4}, Landroid/content/pm/AbstractPersonaObserver;-><init>(Landroid/content/Context;II)V

    .line 213
    return-void
.end method


# virtual methods
.method public onKeyGuardStateChanged(Z)V
    .locals 2
    .param p1, "state"    # Z

    .prologue
    .line 250
    const-string v0, "NotificationHandler"

    const-string v1, "onKeyGuardStateChanged()"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/NotificationHandler;->taskHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->access$000(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$4;

    invoke-direct {v1, p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$4;-><init>(Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 258
    return-void
.end method

.method public onSessionExpired()V
    .locals 2

    .prologue
    .line 217
    const-string v0, "NotificationHandler"

    const-string v1, "  onSessionExpired "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/NotificationHandler;->taskHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->access$000(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$1;

    invoke-direct {v1, p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$1;-><init>(Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    return-void
.end method

.method public onStateChange(Landroid/content/pm/PersonaState;Landroid/content/pm/PersonaState;)V
    .locals 2
    .param p1, "state"    # Landroid/content/pm/PersonaState;
    .param p2, "oldState"    # Landroid/content/pm/PersonaState;

    .prologue
    .line 228
    const-string v0, "NotificationHandler"

    const-string v1, " inside onstatechange "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    sget-object v0, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {p1, v0}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/content/pm/PersonaState;->ADMIN_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {p1, v0}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {p1, v0}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/NotificationHandler;->taskHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->access$000(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$2;

    invoke-direct {v1, p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$2;-><init>(Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 246
    :cond_1
    :goto_0
    return-void

    .line 238
    :cond_2
    sget-object v0, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {p2, v0}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    invoke-virtual {p1, v0}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/NotificationHandler;->taskHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->access$000(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$3;

    invoke-direct {v1, p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver$3;-><init>(Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method startNotificationService(I)V
    .locals 5
    .param p1, "container_id"    # I

    .prologue
    .line 263
    const/4 v1, 0x0

    .line 264
    .local v1, "currentUser":I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    .line 266
    .local v0, "contextUser":I
    if-ne v1, v0, :cond_0

    .line 267
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;

    # getter for: Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->access$200(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/knox/bridge/service/NotificationService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 268
    .local v2, "service":Landroid/content/Intent;
    const-string v3, "com.sec.knox.bridge.receiver.ACTION_STATE_CHANGE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    const-string v3, "android.intent.extra.user_handle"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 271
    iget-object v3, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->this$0:Lcom/sec/knox/bridge/handlers/NotificationHandler;

    invoke-virtual {v3, v2}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 273
    .end local v2    # "service":Landroid/content/Intent;
    :cond_0
    return-void
.end method
