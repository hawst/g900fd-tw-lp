.class public Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RCPPolicyChangedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 575
    const-string v18, "RCPPolicyChangedReceiver"

    const-string v19, " onReceive called "

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    if-nez v18, :cond_2

    .line 578
    :cond_0
    const-string v18, "RCPPolicyChangedReceiver"

    const-string v19, " onReceive RCP_POLICY_CHANGED intent OR intent.getAction() is null "

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    :cond_1
    :goto_0
    return-void

    .line 582
    :cond_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->RCP_POLICY_CHANGED:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$600()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 583
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "policyChangedBundleExport"

    invoke-virtual/range {v18 .. v19}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v10

    .line 585
    .local v10, "policyChangedBundleExport":Landroid/os/Bundle;
    if-eqz v10, :cond_6

    .line 586
    const-string v18, "personaId"

    const/16 v19, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 587
    .local v8, "personaId":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v8, v0, :cond_6

    .line 588
    const-string v18, "syncerList"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    .line 590
    .local v14, "syncerListExport":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 591
    .local v15, "syncerName":Ljava/lang/String;
    const-string v18, "Contacts"

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$700(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v18

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    const-string v20, "T"

    invoke-virtual/range {v18 .. v20}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    :cond_4
    const-string v18, "Calendar"

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$800(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v18

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    const-string v20, "T"

    invoke-virtual/range {v18 .. v20}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 598
    .end local v15    # "syncerName":Ljava/lang/String;
    :cond_5
    const-string v19, "RCPPolicyChangedReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "onReceive RCP_POLICY_CHANGED policyChangedBundleExport personaID: "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, ", Contact: "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedContactExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$700(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v18

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, ", Calendar: "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/knox/bridge/BridgeService;->isChangedCalendarExportPolicy:Ljava/util/HashMap;
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$800(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v18

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v8    # "personaId":I
    .end local v14    # "syncerListExport":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    const-string v18, "policyChangedBundle"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_7

    .line 611
    const-string v18, "RCPPolicyChangedReceiver"

    const-string v19, " onReceive RCP_POLICY_CHANGED bundle is null "

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 615
    :cond_7
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "policyChangedBundle"

    invoke-virtual/range {v18 .. v19}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    .line 616
    .local v9, "policyChangedBundle":Landroid/os/Bundle;
    if-nez v9, :cond_8

    .line 617
    const-string v18, "RCPPolicyChangedReceiver"

    const-string v19, " onReceive RCP_POLICY_CHANGED policyChangedBundle == null"

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 621
    :cond_8
    const-string v18, "RCPPolicyChangedReceiver"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " onReceive RCP_POLICY_CHANGED policyChangedBundle = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    const-string v18, "personaId"

    const/16 v19, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 625
    .local v6, "pId":I
    const-string v18, "syncerList"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    .line 626
    .local v13, "syncerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v18, "policyName"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 627
    .local v11, "policyName":Ljava/lang/String;
    const-string v18, "policyValue"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 629
    .local v12, "policyValue":Ljava/lang/String;
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v6, v0, :cond_9

    if-eqz v13, :cond_9

    if-nez v11, :cond_a

    .line 630
    :cond_9
    const-string v18, "RCPPolicyChangedReceiver"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " onReceive RCP_POLICY_CHANGED invalid data in bundle .. returning .... : pId = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " syncerList = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " policyName = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 637
    :cond_a
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v17

    .line 638
    .local v17, "userId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->getParentId(I)I

    move-result v7

    .line 642
    .local v7, "pUserId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkHandler()V
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$900(Lcom/sec/knox/bridge/BridgeService;)V

    .line 643
    new-instance v18, Landroid/os/Handler;

    invoke-direct/range {v18 .. v18}, Landroid/os/Handler;-><init>()V

    # setter for: Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$1002(Landroid/os/Handler;)Landroid/os/Handler;

    .line 644
    new-instance v16, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver$1;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver$1;-><init>(Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;)V

    .line 650
    .local v16, "task":Ljava/lang/Runnable;
    if-nez v12, :cond_b

    .line 651
    # getter for: Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1000()Landroid/os/Handler;

    move-result-object v18

    const-wide/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 657
    :goto_2
    const-string v18, "RCPPolicyChangedReceiver"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " onReceive RCP_POLICY_CHANGED pId = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " userId = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " pUserId = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    move/from16 v0, v17

    if-ne v7, v0, :cond_c

    .line 662
    const-string v18, "knox-import-data"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_12

    .line 663
    const-string v18, "RCPPolicyChangedReceiver"

    const-string v19, " Owner Case and Policy Change for Persona and Policy is import ... returning ..."

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    # getter for: Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1200()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkHandler()V
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$900(Lcom/sec/knox/bridge/BridgeService;)V

    goto/16 :goto_0

    .line 654
    :cond_b
    # getter for: Lcom/sec/knox/bridge/BridgeService;->mHandlerPolicyChanged:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1000()Landroid/os/Handler;

    move-result-object v18

    const-wide/16 v20, 0x1b58

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 671
    :cond_c
    move/from16 v0, v17

    if-ne v0, v6, :cond_e

    .line 672
    const-string v18, "knox-export-data"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    const-string v18, "knox-sanitize-data"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_12

    .line 674
    :cond_d
    const-string v18, "RCPPolicyChangedReceiver"

    const-string v19, " Persona Case and Policy Change for same persona and Policy is export/sanitize ... returning ..."

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    # getter for: Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1200()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkHandler()V
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$900(Lcom/sec/knox/bridge/BridgeService;)V

    goto/16 :goto_0

    .line 682
    :cond_e
    const-string v18, "knox-import-data"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_f

    .line 683
    const-string v18, "RCPPolicyChangedReceiver"

    const-string v19, " Persona Case and Policy Change for diff persona and Policy is import ... returning ..."

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    # getter for: Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1200()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$RCPPolicyChangedReceiver;->this$0:Lcom/sec/knox/bridge/BridgeService;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkHandler()V
    invoke-static/range {v18 .. v18}, Lcom/sec/knox/bridge/BridgeService;->access$900(Lcom/sec/knox/bridge/BridgeService;)V

    goto/16 :goto_0

    .line 694
    :cond_f
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 695
    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_10
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_12

    .line 696
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string v19, "Clipboard"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_11

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string v19, "Notifications"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_10

    .line 698
    :cond_11
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 705
    .end local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_12
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 706
    .restart local v15    # "syncerName":Ljava/lang/String;
    const-string v18, "RCPPolicyChangedReceiver"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " onReceive RCP_POLICY_CHANGED syncerName = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    # getter for: Lcom/sec/knox/bridge/BridgeService;->mListPolicyChanged:Ljava/util/List;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1200()Ljava/util/List;

    move-result-object v18

    new-instance v19, Lcom/sec/knox/bridge/util/PolicyChanged;

    move-object/from16 v0, v19

    invoke-direct {v0, v6, v15, v11}, Lcom/sec/knox/bridge/util/PolicyChanged;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method
