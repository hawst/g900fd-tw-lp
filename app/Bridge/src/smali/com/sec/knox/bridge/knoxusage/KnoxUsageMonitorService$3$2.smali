.class Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;
.super Ljava/lang/Object;
.source "KnoxUsageMonitorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

.field final synthetic val$dataBaseHelper:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

.field final synthetic val$nextUploadTime:J


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;J)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;->this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

    iput-object p2, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;->val$dataBaseHelper:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    iput-wide p3, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;->val$nextUploadTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 307
    const-string v0, "KnoxUsageLogPro"

    const-string v1, " Received MSG_UPLOAD_COMPLETE_REGULAR"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    iget-object v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;->val$dataBaseHelper:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    const-string v1, "uploads"

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->cleartable(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;->val$dataBaseHelper:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    iget-wide v2, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;->val$nextUploadTime:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->updateUploadFrequencyInCurrentTable(J)V

    .line 313
    return-void
.end method
