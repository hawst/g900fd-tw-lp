.class public Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KnoxUsageReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private processBootCompletedIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceIntent"    # Landroid/content/Intent;
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver -processBootCompletedIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->personasExistOnDevice(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-direct {p0, p2, p3, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->startUsageMonitorService(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V

    .line 72
    :cond_0
    return-void
.end method

.method private processSystemEvents(Landroid/content/Context;Landroid/content/Intent;ZLjava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceIntent"    # Landroid/content/Intent;
    .param p3, "isFirstTime"    # Z
    .param p4, "action"    # Ljava/lang/String;

    .prologue
    .line 113
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver -processSystemEvents"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-static {p1}, Lcom/sec/knox/bridge/uploadmanager/NetworkManager;->isWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "processSystemEvents: Wifi not connected just return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->personasExistOnDevice(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    .line 119
    invoke-direct {p0, p2, p4, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->startUsageMonitorService(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 121
    :cond_2
    if-nez p3, :cond_3

    .line 122
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "processSystemEvents :Registered already, Check upload table"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->isUploadTableEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "processSystemEvents :Registered already. Upload table not empty, start service to trigger upload"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-direct {p0, p2, p4, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->startUsageMonitorService(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 128
    :cond_3
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "processSystemEvents: - No personas,Not registered, No need to process System intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private processUserAddedIntent(Landroid/content/Context;Landroid/content/Intent;ILjava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceIntent"    # Landroid/content/Intent;
    .param p3, "userId"    # I
    .param p4, "action"    # Ljava/lang/String;

    .prologue
    .line 94
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver -processUserAddedIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const/16 v0, 0x64

    if-lt p3, v0, :cond_0

    .line 96
    const-string v0, "android.intent.extra.user_handle"

    invoke-virtual {p2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 97
    invoke-direct {p0, p2, p4, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->startUsageMonitorService(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V

    .line 99
    :cond_0
    return-void
.end method

.method private processUserRemovedIntent(Landroid/content/Context;Landroid/content/Intent;ZILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceIntent"    # Landroid/content/Intent;
    .param p3, "isFirstTime"    # Z
    .param p4, "userId"    # I
    .param p5, "action"    # Ljava/lang/String;

    .prologue
    .line 77
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver -processUserRemovedIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/16 v0, 0x64

    if-ge p4, v0, :cond_0

    .line 79
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KnoxUsageReceiver personaId < PersonaManager.MIN_PERSONA_ID. Just return. PersonaId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :goto_0
    return-void

    .line 82
    :cond_0
    if-nez p3, :cond_1

    .line 83
    const-string v0, "android.intent.extra.user_handle"

    invoke-virtual {p2, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    invoke-direct {p0, p2, p5, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->startUsageMonitorService(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 86
    :cond_1
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "Not registered, No need to process remove intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startUsageMonitorService(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p1, "serviceIntent"    # Landroid/content/Intent;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KnoxUsageReceiver -startUsageMonitorService:action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {p1, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    invoke-virtual {p3, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 107
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 17
    if-nez p2, :cond_1

    .line 18
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver onReceive :intent == null just return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 22
    .local v5, "action":Ljava/lang/String;
    if-nez v5, :cond_2

    .line 23
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver onReceive :action == null just return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 27
    :cond_2
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    .line 28
    .local v6, "myUserId":I
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KnoxUsageReceiver onReceive :"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " myUserId="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    if-eqz v6, :cond_3

    .line 30
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KnoxUsageReceiver onReceive : UserHandle.USER_OWNER != "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " just return"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 33
    :cond_3
    const-string v0, "android.intent.action.USER_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 34
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isEulaShownIncludeDying(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 35
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver onReceive : Eula not shown for removed persona, just return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 40
    :cond_4
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProcessible(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 41
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "KnoxUsageReceiver onReceive : not Processible, just return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 46
    :cond_5
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v2, "serviceIntent":Landroid/content/Intent;
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->loadIsFirstTime(Landroid/content/Context;)Z

    move-result v3

    .line 49
    .local v3, "isFirstTime":Z
    const-string v0, "android.intent.extra.user_handle"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 51
    .local v4, "userId":I
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 52
    invoke-direct {p0, p1, v2, v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->processBootCompletedIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 53
    :cond_6
    const-string v0, "android.intent.action.USER_ADDED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 54
    invoke-direct {p0, p1, v2, v4, v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->processUserAddedIntent(Landroid/content/Context;Landroid/content/Intent;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 55
    :cond_7
    const-string v0, "android.intent.action.USER_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v0, p0

    move-object v1, p1

    .line 56
    invoke-direct/range {v0 .. v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->processUserRemovedIntent(Landroid/content/Context;Landroid/content/Intent;ZILjava/lang/String;)V

    goto/16 :goto_0

    .line 57
    :cond_8
    const-string v0, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    :cond_9
    invoke-direct {p0, p1, v2, v3, v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageReceiver;->processSystemEvents(Landroid/content/Context;Landroid/content/Intent;ZLjava/lang/String;)V

    goto/16 :goto_0
.end method
