.class Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;
.super Ljava/lang/Object;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/util/NotificationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncedNotificationManager"
.end annotation


# instance fields
.field nm:Landroid/app/NotificationManager;

.field final synthetic this$0:Lcom/sec/knox/bridge/util/NotificationUtil;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/util/NotificationUtil;)V
    .locals 2

    .prologue
    .line 359
    iput-object p1, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->this$0:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->this$0:Lcom/sec/knox/bridge/util/NotificationUtil;

    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$100(Lcom/sec/knox/bridge/util/NotificationUtil;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->nm:Landroid/app/NotificationManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/util/NotificationUtil;Lcom/sec/knox/bridge/util/NotificationUtil$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/util/NotificationUtil;
    .param p2, "x1"    # Lcom/sec/knox/bridge/util/NotificationUtil$1;

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;-><init>(Lcom/sec/knox/bridge/util/NotificationUtil;)V

    return-void
.end method


# virtual methods
.method public cancel(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 371
    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$200()Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 372
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$200()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Lcom/sec/knox/bridge/util/NotificationUtil$tagId;

    iget-object v3, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->this$0:Lcom/sec/knox/bridge/util/NotificationUtil;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, p1}, Lcom/sec/knox/bridge/util/NotificationUtil$tagId;-><init>(Lcom/sec/knox/bridge/util/NotificationUtil;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 373
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->nm:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 375
    return-void

    .line 373
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public cancelAll()V
    .locals 6

    .prologue
    .line 392
    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$200()Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 393
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$200()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/bridge/util/NotificationUtil$tagId;

    .line 394
    .local v1, "ti":Lcom/sec/knox/bridge/util/NotificationUtil$tagId;
    iget-object v2, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->nm:Landroid/app/NotificationManager;

    iget-object v4, v1, Lcom/sec/knox/bridge/util/NotificationUtil$tagId;->tag:Ljava/lang/String;

    iget v5, v1, Lcom/sec/knox/bridge/util/NotificationUtil$tagId;->id:I

    invoke-virtual {v2, v4, v5}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_0

    .line 397
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "ti":Lcom/sec/knox/bridge/util/NotificationUtil$tagId;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 396
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$200()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 397
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    return-void
.end method

.method public notify(ILandroid/app/Notification;)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "n"    # Landroid/app/Notification;

    .prologue
    .line 378
    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$200()Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 379
    :try_start_0
    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$200()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Lcom/sec/knox/bridge/util/NotificationUtil$tagId;

    iget-object v3, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->this$0:Lcom/sec/knox/bridge/util/NotificationUtil;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, p1}, Lcom/sec/knox/bridge/util/NotificationUtil$tagId;-><init>(Lcom/sec/knox/bridge/util/NotificationUtil;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->nm:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 382
    return-void

    .line 380
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
