.class public Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;
.super Ljava/lang/Object;
.source "DeviceInfoCollector.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTelephonyMgr:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->mContext:Landroid/content/Context;

    .line 9
    iput-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 12
    iput-object p1, p0, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->mContext:Landroid/content/Context;

    .line 13
    return-void
.end method

.method private getTelephonyManager()Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    return-object v0
.end method


# virtual methods
.method public getIMEI()Ljava/lang/String;
    .locals 3

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 17
    .local v0, "imei":Ljava/lang/String;
    const/4 v1, 0x0

    .line 18
    .local v1, "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 20
    invoke-static {v0}, Lcom/sec/knox/bridge/uploadmanager/JSON;->getHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 24
    :goto_0
    return-object v1

    .line 22
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
