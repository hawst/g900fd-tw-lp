.class public Lcom/sec/knox/bridge/handlers/NotificationHandler;
.super Landroid/app/Service;
.source "NotificationHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

.field private mPersona:Landroid/os/PersonaManager;

.field mPersonaObservers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;",
            ">;"
        }
    .end annotation
.end field

.field private taskHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    .line 37
    iput-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersona:Landroid/os/PersonaManager;

    .line 38
    iput-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    .line 180
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersonaObservers:Ljava/util/HashMap;

    .line 209
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/NotificationHandler;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->taskHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Lcom/sec/knox/bridge/util/NotificationUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/NotificationHandler;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/handlers/NotificationHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/handlers/NotificationHandler;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getService()Landroid/os/PersonaManager;
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersona:Landroid/os/PersonaManager;

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    const-string v1, "persona"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersona:Landroid/os/PersonaManager;

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersona:Landroid/os/PersonaManager;

    return-object v0
.end method

.method private registerPersonaObserver(I)V
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->getService()Landroid/os/PersonaManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 185
    new-instance v0, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;

    iget-object v1, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    const/4 v2, 0x7

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;-><init>(Lcom/sec/knox/bridge/handlers/NotificationHandler;Landroid/content/Context;II)V

    .line 191
    .local v0, "observer":Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;
    iget-object v1, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    .end local v0    # "observer":Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;
    :cond_0
    return-void
.end method

.method private resetKiosk(I)V
    .locals 6
    .param p1, "userId"    # I

    .prologue
    const/4 v5, 0x0

    .line 142
    invoke-direct {p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->getService()Landroid/os/PersonaManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/PersonaManager;->isKioskContainerExistOnDevice()Z

    move-result v1

    .line 144
    .local v1, "kioskmode":Z
    if-eqz v1, :cond_1

    const/16 v3, 0x64

    if-lt p1, v3, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v3

    if-nez v3, :cond_1

    .line 146
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 147
    .local v0, "am":Landroid/app/ActivityManager;
    const-string v3, "com.android.contacts"

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->hasTPhone()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    iget-object v3, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "skt_phone20_settings"

    invoke-static {v3, v4, v5, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 152
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.skt.prod.dialer.CHANGE_TPHONE_MODE_SETTING"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 154
    .local v2, "skt":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->sendBroadcast(Landroid/content/Intent;)V

    .line 156
    .end local v2    # "skt":Landroid/content/Intent;
    :cond_0
    const-string v3, "com.android.mms"

    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/android/internal/telephony/SmsApplication;->setDefaultApplication(Ljava/lang/String;Landroid/content/Context;)V

    .line 165
    .end local v0    # "am":Landroid/app/ActivityManager;
    :cond_1
    return-void
.end method

.method private unregisterObserver(I)V
    .locals 5
    .param p1, "userId"    # I

    .prologue
    .line 197
    const-string v2, "NotificationHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unregisterObserver : personaId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", myUserId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", processId - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 201
    .local v0, "key":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;

    .line 202
    .local v1, "observer":Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;
    if-eqz v1, :cond_0

    .line 204
    iget-object v2, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mPersonaObservers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    invoke-virtual {v1}, Lcom/sec/knox/bridge/handlers/NotificationHandler$PersonaObserver;->unregisterPersonaObserver()V

    .line 207
    :cond_0
    return-void
.end method


# virtual methods
.method public hasTPhone()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 122
    const/4 v3, 0x0

    .line 123
    .local v3, "result":Z
    const-string v0, "com.skt.prod.phone"

    .line 125
    .local v0, "TPHONE_PKG_NAME":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 126
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_0

    .line 127
    const-string v6, "com.skt.prod.phone"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 129
    .local v4, "tPhone":Landroid/content/pm/ApplicationInfo;
    if-eqz v4, :cond_1

    const/4 v3, 0x1

    .end local v4    # "tPhone":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    move v5, v3

    .line 134
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    return v5

    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    .restart local v4    # "tPhone":Landroid/content/pm/ApplicationInfo;
    :cond_1
    move v3, v5

    .line 129
    goto :goto_0

    .line 131
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "tPhone":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 132
    .local v1, "ex":Ljava/lang/Exception;
    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 169
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 44
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 45
    iput-object p0, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    .line 46
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->taskHandler:Landroid/os/Handler;

    .line 47
    new-instance v3, Lcom/sec/knox/bridge/util/NotificationUtil;

    iget-object v4, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/knox/bridge/util/NotificationUtil;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    .line 49
    const-string v3, "NotificationHandler"

    const-string v4, "onCreate called for NotificationHandler"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->getService()Landroid/os/PersonaManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v2

    .line 52
    .local v2, "personaList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v2, :cond_0

    .line 53
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PersonaInfo;

    .line 54
    .local v1, "info":Landroid/content/pm/PersonaInfo;
    const-string v3, "NotificationHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "registerPersonaObserver : personaId - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", myUserId - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", processId - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget v3, v1, Landroid/content/pm/PersonaInfo;->id:I

    invoke-direct {p0, v3}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->registerPersonaObserver(I)V

    goto :goto_0

    .line 70
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Landroid/content/pm/PersonaInfo;
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v7, -0x1

    .line 74
    const-string v5, "NotificationHandler"

    const-string v6, "onStartCommand called for NotificationHandler"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "action":Ljava/lang/String;
    const/4 v4, -0x1

    .line 78
    .local v4, "userId":I
    if-eqz p1, :cond_0

    .line 79
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 80
    const-string v5, "android.intent.extra.user_handle"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 81
    if-ne v4, v7, :cond_0

    .line 82
    iget-object v5, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-virtual {v5}, Lcom/sec/knox/bridge/util/NotificationUtil;->getCurrentUserId()I

    move-result v4

    .line 86
    :cond_0
    new-instance v3, Landroid/content/Intent;

    iget-object v5, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mContext:Landroid/content/Context;

    const-class v6, Lcom/sec/knox/bridge/service/NotificationService;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    .local v3, "service":Landroid/content/Intent;
    const-string v5, "android.intent.action.MANAGED_PROFILE_ADDED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 90
    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->registerPersonaObserver(I)V

    .line 91
    const-string v5, "android.intent.action.MANAGED_PROFILE_ADDED"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v5, "android.intent.extra.user_handle"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 93
    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->resetKiosk(I)V

    .line 108
    :cond_1
    :goto_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    .line 109
    .local v2, "currentUser":I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 116
    .local v1, "contextUser":I
    invoke-virtual {p0, v3}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 118
    const/4 v5, 0x1

    return v5

    .line 94
    .end local v1    # "contextUser":I
    .end local v2    # "currentUser":I
    :cond_2
    const-string v5, "com.sec.knox.container.action.containerremovalstarted"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 95
    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->unregisterObserver(I)V

    .line 96
    const-string v5, "com.sec.knox.container.action.containerremovalstarted"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    const-string v5, "android.intent.extra.user_handle"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 98
    invoke-direct {p0, v4}, Lcom/sec/knox/bridge/handlers/NotificationHandler;->resetKiosk(I)V

    goto :goto_0

    .line 99
    :cond_3
    const-string v5, "com.sec.knox.bridge.receiver.ACTION_USER_SWITCHED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 100
    const-string v5, "com.sec.knox.bridge.receiver.ACTION_USER_SWITCHED"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v5, "android.intent.extra.user_handle"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 102
    :cond_4
    const-string v5, "samsung.knox.intent.action.MODE_SWITCH_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 103
    iget-object v5, p0, Lcom/sec/knox/bridge/handlers/NotificationHandler;->mNm:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-virtual {v5, v4}, Lcom/sec/knox/bridge/util/NotificationUtil;->setCurrentUserId(I)V

    .line 104
    const-string v5, "samsung.knox.intent.action.MODE_SWITCH_CHANGED"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const-string v5, "android.intent.extra.user_handle"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method
