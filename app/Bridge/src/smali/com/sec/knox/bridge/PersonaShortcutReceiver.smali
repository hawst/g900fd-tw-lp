.class public Lcom/sec/knox/bridge/PersonaShortcutReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PersonaShortcutReceiver.java"


# static fields
.field public static ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

.field public static ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

.field public static PERSONA_SHORTCUT:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "android.intent.action.LAUNCH_PERSONA_SHORTCUT"

    sput-object v0, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->PERSONA_SHORTCUT:Ljava/lang/String;

    .line 35
    const-string v0, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    sput-object v0, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

    .line 36
    const-string v0, "com.android.launcher.action.INSTALL_SHORTCUT"

    sput-object v0, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private addMultipleShortcut(Landroid/content/Context;Ljava/util/List;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p2, "sclist":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v8

    .line 218
    .local v8, "currentUser":I
    const-string v0, "PersonaShortcutReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sclist size :"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v10, v0, :cond_1

    .line 222
    const-string v0, "PersonaShortcutReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "index :"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-interface {p2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Bundle;

    .line 225
    .local v11, "scitem":Landroid/os/Bundle;
    const-string v0, "personaId"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 226
    .local v2, "itemPersonaId":I
    const-string v0, "package"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 227
    .local v3, "itemPackageName":Ljava/lang/String;
    const-string v0, "commandType"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 228
    .local v4, "itemCommandType":Ljava/lang/String;
    const-string v0, "component"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/content/ComponentName;

    .line 229
    .local v5, "itemCn":Landroid/content/ComponentName;
    const-string v0, "label"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 230
    .local v6, "itemLabel":Ljava/lang/String;
    const-string v0, "iconBitmap"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/graphics/Bitmap;

    .line 232
    .local v7, "itemIconBitmap":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_0

    .line 233
    const-string v0, "addShortcut parameter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v12, "/"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v12, "/"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v12, "/"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v12, "/"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v0, p0

    move-object v1, p1

    .line 240
    invoke-direct/range {v0 .. v7}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->addShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 221
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 237
    :cond_0
    const-string v0, "PersonaShortcutReceiver"

    const-string v1, "ComponentName from launcher is null"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 244
    .end local v2    # "itemPersonaId":I
    .end local v3    # "itemPackageName":Ljava/lang/String;
    .end local v4    # "itemCommandType":Ljava/lang/String;
    .end local v5    # "itemCn":Landroid/content/ComponentName;
    .end local v6    # "itemLabel":Ljava/lang/String;
    .end local v7    # "itemIconBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "scitem":Landroid/os/Bundle;
    :catch_0
    move-exception v9

    .line 245
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 247
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v0, "PersonaShortcutReceiver"

    const-string v1, "complete addMultipleShortcut"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    return-void
.end method

.method private addShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "personaId"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "commandType"    # Ljava/lang/String;
    .param p5, "cn"    # Landroid/content/ComponentName;
    .param p6, "label"    # Ljava/lang/String;
    .param p7, "iconBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 117
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    .line 119
    .local v4, "currentUser":I
    const-string v5, "addShortcut"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "currentUser:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    if-eqz p5, :cond_1

    .line 122
    const-string v5, "addShortcut parameter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p5 .. p5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p5 .. p5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :goto_0
    const-string v5, "rcp"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/RCPManager;

    .line 131
    .local v3, "mBridge":Landroid/os/RCPManager;
    :try_start_0
    const-string v5, "PersonaShortcutReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "pi for package 1: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v5, "createShortcut"

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 134
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, " creating sc"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    .line 136
    .local v16, "pm":Landroid/content/pm/PackageManager;
    const/4 v5, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    .line 137
    .local v15, "pi":Landroid/content/pm/PackageInfo;
    const/4 v13, 0x0

    .line 138
    .local v13, "packageLabel":Ljava/lang/CharSequence;
    if-eqz p6, :cond_2

    .line 139
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "label from launcher is not null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    move-object/from16 v13, p6

    .line 146
    :goto_1
    const/4 v7, 0x0

    .line 147
    .local v7, "shortcutIcon":Landroid/graphics/Bitmap;
    if-eqz p7, :cond_3

    .line 148
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "bitmap from launcher is not null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    move-object/from16 v7, p7

    .line 156
    :goto_2
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "Now badge isn\'t required because of shortcut folder"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const/16 v17, 0x0

    .line 167
    .local v17, "shortcutIntent":Landroid/content/Intent;
    if-eqz p5, :cond_4

    .line 168
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "ComponentName from launcher is not null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->getShortcutIntentWithCn(Ljava/lang/String;Landroid/content/ComponentName;I)Landroid/content/Intent;

    move-result-object v17

    .line 174
    :goto_3
    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v8

    .line 175
    .local v8, "shortcutIntentUri":Ljava/lang/String;
    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v9, "create"

    move-object/from16 v5, p3

    invoke-virtual/range {v3 .. v9}, Landroid/os/RCPManager;->handleShortcut(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v5, "persona"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/PersonaManager;

    .line 180
    .local v12, "mPersona":Landroid/os/PersonaManager;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-virtual {v12, v5}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v14

    .line 203
    .local v14, "personaInfo":Landroid/content/pm/PersonaInfo;
    const-string v5, "user"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/os/UserManager;

    .line 204
    .local v18, "um":Landroid/os/UserManager;
    iget v5, v14, Landroid/content/pm/PersonaInfo;->id:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    .end local v7    # "shortcutIcon":Landroid/graphics/Bitmap;
    .end local v8    # "shortcutIntentUri":Ljava/lang/String;
    .end local v12    # "mPersona":Landroid/os/PersonaManager;
    .end local v13    # "packageLabel":Ljava/lang/CharSequence;
    .end local v14    # "personaInfo":Landroid/content/pm/PersonaInfo;
    .end local v15    # "pi":Landroid/content/pm/PackageInfo;
    .end local v16    # "pm":Landroid/content/pm/PackageManager;
    .end local v17    # "shortcutIntent":Landroid/content/Intent;
    .end local v18    # "um":Landroid/os/UserManager;
    :cond_0
    :goto_4
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, " complete sc"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-void

    .line 126
    .end local v3    # "mBridge":Landroid/os/RCPManager;
    :cond_1
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "ComponentName from launcher is null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 142
    .restart local v3    # "mBridge":Landroid/os/RCPManager;
    .restart local v13    # "packageLabel":Ljava/lang/CharSequence;
    .restart local v15    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v16    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    :try_start_1
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "label from launcher is null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v5, v15, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v13

    goto :goto_1

    .line 151
    .restart local v7    # "shortcutIcon":Landroid/graphics/Bitmap;
    :cond_3
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "bitmap from launcher is null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v5, v15, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 153
    .local v11, "icon":Landroid/graphics/drawable/Drawable;
    check-cast v11, Landroid/graphics/drawable/BitmapDrawable;

    .end local v11    # "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v11}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    goto/16 :goto_2

    .line 171
    .restart local v17    # "shortcutIntent":Landroid/content/Intent;
    :cond_4
    const-string v5, "PersonaShortcutReceiver"

    const-string v6, "ComponentName from launcher is null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->getShortcutIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v17

    goto :goto_3

    .line 208
    .end local v7    # "shortcutIcon":Landroid/graphics/Bitmap;
    .end local v13    # "packageLabel":Ljava/lang/CharSequence;
    .end local v15    # "pi":Landroid/content/pm/PackageInfo;
    .end local v16    # "pm":Landroid/content/pm/PackageManager;
    .end local v17    # "shortcutIntent":Landroid/content/Intent;
    :catch_0
    move-exception v10

    .line 209
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4
.end method

.method private getShortcutIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "currentUser"    # I

    .prologue
    .line 280
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 281
    .local v2, "pManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, p2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 282
    .local v1, "launchIntent":Landroid/content/Intent;
    const/high16 v4, 0x14000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 285
    new-instance v3, Landroid/content/Intent;

    sget-object v4, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->PERSONA_SHORTCUT:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 286
    .local v3, "shortcutIntent":Landroid/content/Intent;
    const-string v4, "persona_shortcut://"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 287
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 288
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "personaId"

    invoke-virtual {v0, v4, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 289
    const-string v4, "package"

    invoke-virtual {v0, v4, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const-string v4, "commandType"

    const-string v5, "launchActivity"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-string v4, "uri"

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 294
    return-object v3
.end method

.method private getShortcutIntentWithCn(Ljava/lang/String;Landroid/content/ComponentName;I)Landroid/content/Intent;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "cn"    # Landroid/content/ComponentName;
    .param p3, "currentUser"    # I

    .prologue
    .line 298
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 299
    .local v1, "launchIntent":Landroid/content/Intent;
    invoke-virtual {v1, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 300
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    const/high16 v3, 0x14000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 304
    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->PERSONA_SHORTCUT:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 305
    .local v2, "shortcutIntent":Landroid/content/Intent;
    const-string v3, "persona_shortcut://"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 306
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 307
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "personaId"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 308
    const-string v3, "package"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const-string v3, "commandType"

    const-string v4, "launchActivity"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const-string v3, "uri"

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 313
    return-object v2
.end method

.method private removeShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "personaId"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "commandType"    # Ljava/lang/String;

    .prologue
    .line 252
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 254
    .local v1, "currentUser":I
    const-string v2, "removeShortcut"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "currentUser:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v2, "rcp"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/RCPManager;

    .line 259
    .local v0, "mBridge":Landroid/os/RCPManager;
    :try_start_0
    const-string v2, "PersonaShortcutReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pi for package 1: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v2, "deleteShortcut"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 262
    const-string v2, "PersonaShortcutReceiver"

    const-string v3, " removing sc"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "remove"

    move-object v2, p3

    invoke-virtual/range {v0 .. v6}, Landroid/os/RCPManager;->handleShortcut(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :cond_0
    :goto_0
    const-string v2, "PersonaShortcutReceiver"

    const-string v3, " complete sc"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    return-void

    .line 265
    :catch_0
    move-exception v7

    .line 266
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 43
    :cond_0
    const-string v3, "PersonaShortcutReceiver"

    const-string v4, " PersonaShortcutReceiver onReceive() intent is null"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_1
    :goto_0
    return-void

    .line 47
    :cond_2
    const-string v3, "PersonaShortcutReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " PersonaShortcutReceiver onReceive() intent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    sget-object v3, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->PERSONA_SHORTCUT:Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 49
    const-string v3, "PersonaShortcutReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " received "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 51
    .local v11, "b":Landroid/os/Bundle;
    if-nez v11, :cond_3

    .line 52
    const-string v3, "PersonaShortcutReceiver"

    const-string v4, "onReceive() - intent has no extras (personaid and package)"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_3
    new-instance v12, Landroid/app/Command;

    invoke-direct {v12}, Landroid/app/Command;-><init>()V

    .line 57
    .local v12, "command":Landroid/app/Command;
    const-string v3, "personaId"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v12, Landroid/app/Command;->personaId:I

    .line 58
    const-string v3, "package"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v12, Landroid/app/Command;->packageToLaunch:Ljava/lang/String;

    .line 59
    const-string v3, "intent"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    iput-object v3, v12, Landroid/app/Command;->intent:Landroid/content/Intent;

    .line 60
    const-string v3, "contentIntent"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/app/PendingIntent;

    iput-object v3, v12, Landroid/app/Command;->contentIntent:Landroid/app/PendingIntent;

    .line 61
    const-string v3, "commandType"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v12, Landroid/app/Command;->type:Ljava/lang/String;

    .line 62
    const-string v3, "uri"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v12, Landroid/app/Command;->uri:Ljava/lang/String;

    .line 64
    const-string v3, "PersonaShortcutReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " PersonaShortcutReceiver notifId is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "notifId"

    invoke-virtual {v11, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v3, "rcp"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/os/RCPManager;

    .line 69
    .local v14, "mBridge":Landroid/os/RCPManager;
    const-string v3, "createShortcut"

    iget-object v4, v12, Landroid/app/Command;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 70
    const-string v3, "sclist"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v18

    .line 71
    .local v18, "sclist":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    if-eqz v18, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 72
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->addMultipleShortcut(Landroid/content/Context;Ljava/util/List;)V

    goto/16 :goto_0

    .line 74
    :cond_4
    const-string v3, "component"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/content/ComponentName;

    .line 75
    .local v8, "cn":Landroid/content/ComponentName;
    const-string v3, "label"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 76
    .local v9, "label":Ljava/lang/String;
    const-string v3, "iconBitmap"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    .line 77
    .local v10, "iconBitmap":Landroid/graphics/Bitmap;
    iget v5, v12, Landroid/app/Command;->personaId:I

    iget-object v6, v12, Landroid/app/Command;->packageToLaunch:Ljava/lang/String;

    iget-object v7, v12, Landroid/app/Command;->type:Ljava/lang/String;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v10}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->addShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 80
    .end local v8    # "cn":Landroid/content/ComponentName;
    .end local v9    # "label":Ljava/lang/String;
    .end local v10    # "iconBitmap":Landroid/graphics/Bitmap;
    .end local v18    # "sclist":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_5
    const-string v3, "deleteShortcut"

    iget-object v4, v12, Landroid/app/Command;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 81
    iget v3, v12, Landroid/app/Command;->personaId:I

    iget-object v4, v12, Landroid/app/Command;->packageToLaunch:Ljava/lang/String;

    iget-object v5, v12, Landroid/app/Command;->type:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->removeShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 84
    :cond_6
    const-string v3, "PersonaShortcutReceiver"

    const-string v4, "onReceive() - calling into system bridge to switch user"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v14, v12}, Landroid/os/RCPManager;->executeCommandForPersona(Landroid/app/Command;)V

    goto/16 :goto_0

    .line 88
    .end local v11    # "b":Landroid/os/Bundle;
    .end local v12    # "command":Landroid/app/Command;
    .end local v14    # "mBridge":Landroid/os/RCPManager;
    :cond_7
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 89
    const-string v3, "PersonaShortcutReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " package removed with intent.getData():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v3, "android.intent.extra.DATA_REMOVED"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_8

    .line 92
    const-string v3, "PersonaShortcutReceiver"

    const-string v4, " package update. So skip removeShortcut "

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 95
    :cond_8
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    const-string v5, "deleteShortcut"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->removeShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 97
    :cond_9
    const-string v3, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 99
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v15

    .line 100
    .local v15, "pkgName":Ljava/lang/String;
    const-string v3, "PersonaShortcutReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package changed with intent.getData():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 102
    .local v17, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v16

    .line 103
    .local v16, "pkgState":I
    const/4 v3, 0x2

    move/from16 v0, v16

    if-eq v0, v3, :cond_a

    const/4 v3, 0x3

    move/from16 v0, v16

    if-ne v0, v3, :cond_1

    .line 105
    :cond_a
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    const-string v4, "deleteShortcut"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v15, v4}, Lcom/sec/knox/bridge/PersonaShortcutReceiver;->removeShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 107
    .end local v15    # "pkgName":Ljava/lang/String;
    .end local v16    # "pkgState":I
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v13

    .line 108
    .local v13, "e":Ljava/lang/Exception;
    const-string v3, "PersonaShortcutReceiver"

    const-string v4, "Exception catched - PACKAGE_CHANGED"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method
