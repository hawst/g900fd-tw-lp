.class Lcom/sec/knox/bridge/util/NotificationUtil$1;
.super Ljava/lang/Object;
.source "NotificationUtil.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/util/NotificationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/content/pm/PersonaInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/util/NotificationUtil;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/util/NotificationUtil;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/knox/bridge/util/NotificationUtil$1;->this$0:Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/content/pm/PersonaInfo;Landroid/content/pm/PersonaInfo;)I
    .locals 4
    .param p1, "pi1"    # Landroid/content/pm/PersonaInfo;
    .param p2, "pi2"    # Landroid/content/pm/PersonaInfo;

    .prologue
    .line 231
    iget-object v2, p0, Lcom/sec/knox/bridge/util/NotificationUtil$1;->this$0:Lcom/sec/knox/bridge/util/NotificationUtil;

    # getter for: Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->access$100(Lcom/sec/knox/bridge/util/NotificationUtil;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "user"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    .line 232
    .local v1, "um":Landroid/os/UserManager;
    iget v2, p1, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    .line 233
    .local v0, "ui":Landroid/content/pm/UserInfo;
    const-string v2, "KNOX"

    iget-object v3, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234
    const/4 v2, -0x1

    .line 237
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 224
    check-cast p1, Landroid/content/pm/PersonaInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/content/pm/PersonaInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/knox/bridge/util/NotificationUtil$1;->compare(Landroid/content/pm/PersonaInfo;Landroid/content/pm/PersonaInfo;)I

    move-result v0

    return v0
.end method
