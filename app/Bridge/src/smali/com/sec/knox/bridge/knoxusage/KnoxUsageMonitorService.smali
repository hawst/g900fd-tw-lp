.class public Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;
.super Landroid/app/Service;
.source "KnoxUsageMonitorService.java"


# static fields
.field static KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

.field private static isUpdateInProgress:Z

.field public static final mHandler:Landroid/os/Handler;

.field private static mcontext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->isUpdateInProgress:Z

    .line 194
    new-instance v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$2;

    invoke-direct {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$2;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    .line 268
    new-instance v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

    invoke-direct {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-static {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->scheduleNextUpload(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100()V
    .locals 0

    .prologue
    .line 17
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->startFirstUpload()V

    return-void
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 17
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->triggerUpload()V

    return-void
.end method

.method private addEntryForSecondUser(I)V
    .locals 8
    .param p1, "userId"    # I

    .prologue
    .line 159
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->loadNextUploadTime(Landroid/content/Context;)J

    move-result-wide v6

    .line 161
    .local v6, "nextUploadTime":J
    new-instance v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;-><init>(IIIJJ)V

    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;->addAnEntryForAnUser(Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;Landroid/content/Context;)V

    .line 166
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 384
    sget-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->mcontext:Landroid/content/Context;

    return-object v0
.end method

.method public static getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 380
    sget-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private handleBootCompleted(Z)V
    .locals 2
    .param p1, "isFirstTime"    # Z

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 121
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->startFirstUpload()V

    .line 127
    :goto_0
    return-void

    .line 123
    :cond_0
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "handleBootCompleted :  personaList >0 : Not First Time Just schedule timer "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->scheduleNextUpload(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleServiceRestarted(Z)V
    .locals 2
    .param p1, "isFirstTime"    # Z

    .prologue
    .line 93
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "handleServiceRestarted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    sget-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->mcontext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->personasExistOnDevice(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    if-eqz p1, :cond_0

    .line 96
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->startFirstUpload()V

    .line 104
    :goto_0
    return-void

    .line 98
    :cond_0
    sget-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->mcontext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->scheduleNextUpload(Landroid/content/Context;)V

    goto :goto_0

    .line 101
    :cond_1
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "handleServiceRestarted : No personas"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static handleSystemEvents(Z)V
    .locals 0
    .param p0, "isFirstTime"    # Z

    .prologue
    .line 182
    if-eqz p0, :cond_0

    .line 183
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->startFirstUpload()V

    .line 187
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->triggerUpload()V

    goto :goto_0
.end method

.method private handleUserAdded(IZ)V
    .locals 2
    .param p1, "userId"    # I
    .param p2, "isFirstTime"    # Z

    .prologue
    .line 107
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onStartCommand - null != intent && intent.getAction().equals(Intent.ACTION_USER_ADDED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    if-eqz p2, :cond_0

    .line 110
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->startFirstUpload()V

    .line 117
    :goto_0
    return-void

    .line 112
    :cond_0
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onStartCommand - Second User added Just add an entry to this user."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->addEntryForSecondUser(I)V

    .line 115
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->scheduleNextUpload(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleUserChanged(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 145
    const/16 v1, 0x64

    if-ge p1, v1, :cond_0

    .line 146
    const-string v1, "KnoxUsageLogPro"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleUserChanged: personaId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " just return"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :goto_0
    return-void

    .line 150
    :cond_0
    const-string v1, "KnoxUsageLogPro"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleUserChanged "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 152
    .local v0, "updateMsg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 153
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 154
    sget-object v1, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private handleUserDeleted(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "personaId"    # I

    .prologue
    .line 135
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$1;-><init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;ILandroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 142
    return-void
.end method

.method public static isUpdateInProgress()Z
    .locals 3

    .prologue
    .line 33
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isUpdateInProgress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->isUpdateInProgress:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    sget-boolean v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->isUpdateInProgress:Z

    return v0
.end method

.method private static scheduleNextUpload(Landroid/content/Context;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v10, 0x0

    const-wide/32 v8, 0x1499700

    .line 230
    const-string v1, "KnoxUsageLogPro"

    const-string v6, "scheduleNextUpload  -KnoxUsageMonitorService"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->loadNextUploadTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 234
    .local v2, "nextUploadTime":J
    const-string v1, "KnoxUsageLogPro"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "next uploadTime loaded from sharedpref:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const-wide/16 v6, -0x1

    cmp-long v1, v6, v2

    if-nez v1, :cond_1

    .line 237
    const-string v1, "KnoxUsageLogPro"

    const-string v6, "KnoxUsageUpoadHandlerThread-next Upload time not valid return"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v2, v6

    .line 243
    .local v4, "timeDifference":J
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sget-object v6, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 250
    cmp-long v1, v4, v8

    if-ltz v1, :cond_2

    .line 251
    const-string v1, "KnoxUsageLogPro"

    const-string v6, "timedifferenc >= SIX_HOURS - schedule again"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sget-object v6, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 254
    :cond_2
    cmp-long v1, v4, v10

    if-lez v1, :cond_3

    cmp-long v1, v4, v8

    if-gez v1, :cond_3

    .line 255
    const-string v1, "KnoxUsageLogPro"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "timedifferenc < SIX_HOURS - schedule again after:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sget-object v6, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v6, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 260
    :cond_3
    cmp-long v1, v4, v10

    if-gtz v1, :cond_0

    .line 261
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 262
    .local v0, "moveMsg":Landroid/os/Message;
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 263
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 264
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sget-object v6, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->KnoxUsageUpoadHandlerThread:Ljava/lang/Runnable;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static setUpdateInProgress(Z)V
    .locals 3
    .param p0, "isUpdateInProgress"    # Z

    .prologue
    .line 39
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUpdateInProgress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    sput-boolean p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->isUpdateInProgress:Z

    .line 42
    return-void
.end method

.method private static startFirstUpload()V
    .locals 2

    .prologue
    .line 130
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "startFirstUpload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    new-instance v0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;-><init>(Z)V

    invoke-virtual {v0}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->processUpload()V

    .line 132
    return-void
.end method

.method private static triggerUpload()V
    .locals 2

    .prologue
    .line 190
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "triggerUpload: start upload "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    new-instance v0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;-><init>(Z)V

    invoke-virtual {v0}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->processUpload()V

    .line 192
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 177
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onBind - KnoxUsageMonitorService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onCreate - KnoxUsageMonitorService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 50
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->mcontext:Landroid/content/Context;

    .line 51
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    .line 52
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 170
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onDestroy - KnoxUsageMonitorService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 173
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 56
    const-string v3, "KnoxUsageLogPro"

    const-string v4, "onStartCommand - KnoxUsageMonitorService"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->loadIsFirstTime(Landroid/content/Context;)Z

    move-result v1

    .line 60
    .local v1, "isFirstTime":Z
    const-string v0, ""

    .line 61
    .local v0, "action":Ljava/lang/String;
    const/4 v2, 0x0

    .line 62
    .local v2, "userId":I
    if-eqz p1, :cond_0

    .line 63
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 64
    const-string v3, "android.intent.extra.user_handle"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 66
    :cond_0
    const-string v3, "KnoxUsageLogPro"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStartCommand - userId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Action= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    if-eqz p1, :cond_1

    const-string v3, "android.intent.action.USER_ADDED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 69
    invoke-direct {p0, v2, v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->handleUserAdded(IZ)V

    .line 89
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 70
    :cond_1
    if-eqz p1, :cond_3

    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 76
    :cond_2
    invoke-static {v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->handleSystemEvents(Z)V

    goto :goto_0

    .line 77
    :cond_3
    if-eqz p1, :cond_4

    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 79
    invoke-direct {p0, v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->handleBootCompleted(Z)V

    goto :goto_0

    .line 80
    :cond_4
    const-string v3, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 81
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->handleUserDeleted(Landroid/content/Context;I)V

    goto :goto_0

    .line 82
    :cond_5
    const-string v3, "com.samsung.knox.bridge.knoxusage.lwcsfolderlaunch"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 83
    invoke-direct {p0, v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->handleUserChanged(I)V

    goto :goto_0

    .line 84
    :cond_6
    const-string v3, "com.samsung.knox.bridge.knoxusage.peronaLaunch"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 85
    invoke-direct {p0, v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->handleUserChanged(I)V

    goto :goto_0

    .line 87
    :cond_7
    invoke-direct {p0, v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->handleServiceRestarted(Z)V

    goto :goto_0
.end method
