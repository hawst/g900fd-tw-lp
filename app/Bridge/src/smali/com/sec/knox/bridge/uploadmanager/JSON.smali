.class public Lcom/sec/knox/bridge/uploadmanager/JSON;
.super Ljava/lang/Object;
.source "JSON.java"


# direct methods
.method public static compriseKnoxUsageLogUploadRequestJSON(Ljava/util/List;)Lorg/json/JSONObject;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "knoxUploadRequests":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;>;"
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 22
    .local v7, "uploadRequestJSON":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 24
    .local v6, "uploadJSONArray":Lorg/json/JSONArray;
    :try_start_0
    new-instance v2, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;

    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v2, v8}, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;-><init>(Landroid/content/Context;)V

    .line 25
    .local v2, "deviceInfoCollector":Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;
    invoke-virtual {v2}, Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;->getIMEI()Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "device":Ljava/lang/String;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;

    .line 27
    .local v5, "info":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 28
    .local v0, "dataJSON":Lorg/json/JSONObject;
    const-string v8, "id"

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getContainerId()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 29
    const-string v8, "status"

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getStatus()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 30
    const-string v8, "track_start_ts"

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getStartOfWeek()J

    move-result-wide v10

    invoke-virtual {v0, v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 31
    const-string v8, "entrance"

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getEntranceCount()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 32
    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 33
    const-string v8, "KnoxUsageLogPro"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "compriseKnoxUsageRequestJSON :container id "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getContainerId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const-string v8, "KnoxUsageLogPro"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "compriseKnoxUsageRequestJSON :entrance count"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getStatus()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    const-string v8, "KnoxUsageLogPro"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "compriseKnoxUsageRequestJSON :time Spent"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getStartOfWeek()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    const-string v8, "KnoxUsageLogPro"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "compriseKnoxUsageRequestJSON :upload Time"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getUploadTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 43
    .end local v0    # "dataJSON":Lorg/json/JSONObject;
    .end local v1    # "device":Ljava/lang/String;
    .end local v2    # "deviceInfoCollector":Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "info":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    :catch_0
    move-exception v3

    .line 44
    .local v3, "e":Lorg/json/JSONException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "compriseUploadTIMADataRequestJSON : JSONException was occurred : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 45
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    .line 46
    const/4 v7, 0x0

    .line 52
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_1
    return-object v7

    .line 39
    .restart local v1    # "device":Ljava/lang/String;
    .restart local v2    # "deviceInfoCollector":Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    const-string v8, "device_id"

    invoke-virtual {v7, v8, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 40
    const-string v8, "upload_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v7, v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 41
    const-string v8, "container"

    invoke-virtual {v7, v8, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 47
    .end local v1    # "device":Ljava/lang/String;
    .end local v2    # "deviceInfoCollector":Lcom/sec/knox/bridge/uploadmanager/DeviceInfoCollector;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v3

    .line 48
    .local v3, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "compriseUploadTIMADataRequestJSON : Exception was occurred : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->e(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 50
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static getHash(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 56
    const/4 v2, 0x0

    .line 58
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    const-string v3, "SHA-256"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 59
    .local v1, "md":Ljava/security/MessageDigest;
    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 60
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 61
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 70
    .end local v1    # "md":Ljava/security/MessageDigest;
    :goto_0
    return-object v2

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v3, "error - NoSuchAlgorithmException"

    invoke-static {v3}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 64
    const/4 v2, 0x0

    .line 68
    goto :goto_0

    .line 65
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "getApkHash() failed"

    invoke-static {v3}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 67
    const/4 v2, 0x0

    goto :goto_0
.end method
