.class public Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ExchangeDataReceiver.java"


# instance fields
.field private mPersona:Landroid/os/PersonaManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->mPersona:Landroid/os/PersonaManager;

    return-void
.end method

.method private getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->mPersona:Landroid/os/PersonaManager;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 193
    const-string v0, "persona"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->mPersona:Landroid/os/PersonaManager;

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->mPersona:Landroid/os/PersonaManager;

    return-object v0
.end method

.method private isContainerStateIsValid(I)Z
    .locals 5
    .param p1, "moveToDstCId"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 200
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    if-ne p1, v3, :cond_0

    .line 201
    const-string v2, "ExchangeDataReceiver"

    const-string v3, "Move to Personal mode. return true"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :goto_0
    return v1

    .line 204
    :cond_0
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v0

    .line 206
    .local v0, "info":Landroid/content/pm/PersonaInfo;
    if-nez v0, :cond_1

    .line 207
    const-string v1, "ExchangeDataReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t get a persona info. moveToDstCId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 208
    goto :goto_0

    .line 212
    :cond_1
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 213
    const-string v1, "ExchangeDataReceiver"

    const-string v3, "Persona is superlocked"

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 214
    goto :goto_0

    .line 215
    :cond_2
    iget-boolean v3, v0, Landroid/content/pm/PersonaInfo;->partial:Z

    if-ne v3, v1, :cond_3

    .line 216
    const-string v1, "ExchangeDataReceiver"

    const-string v3, "Persona is partially created"

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 217
    goto :goto_0

    .line 218
    :cond_3
    iget-boolean v3, v0, Landroid/content/pm/PersonaInfo;->removePersona:Z

    if-ne v3, v1, :cond_4

    .line 219
    const-string v1, "ExchangeDataReceiver"

    const-string v3, "Persona is removed"

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 220
    goto :goto_0

    .line 222
    :cond_4
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->DELETING:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->CREATING:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 224
    :cond_5
    const-string v1, "ExchangeDataReceiver"

    const-string v3, "Persona state is invalid for moving files "

    invoke-static {v1, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 225
    goto/16 :goto_0

    .line 228
    :cond_6
    const-string v2, "ExchangeDataReceiver"

    const-string v3, "Persona state is valid for moving files"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private showToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "duration"    # I

    .prologue
    .line 233
    sget-boolean v0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v0, :cond_0

    .line 234
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x103012b

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0, p2, p3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 240
    :goto_0
    return-void

    .line 237
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x1030128

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0, p2, p3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method isMoveToPolicyAllowed(Landroid/content/Context;II)Z
    .locals 7
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "src"    # I
    .param p3, "target"    # I

    .prologue
    .line 141
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 142
    const/4 v2, 0x0

    .line 143
    .local v2, "isPolicyAllowed":Z
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v5

    iget-object v1, v5, Lcom/sec/knox/bridge/BridgeService;->mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    .line 145
    .local v1, "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    const/4 v3, 0x0

    .line 147
    .local v3, "pi":Landroid/content/pm/PersonaInfo;
    if-nez p2, :cond_2

    .line 148
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v3

    .line 154
    :cond_0
    :goto_0
    if-eqz v3, :cond_3

    iget-boolean v5, v3, Landroid/content/pm/PersonaInfo;->isUserManaged:Z

    if-eqz v5, :cond_3

    .line 155
    const-string v5, "ExchangeDataReceiver"

    const-string v6, "isMoveToPolicyAllowed : user managed container. return true"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const/4 v2, 0x1

    .line 188
    .end local v1    # "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .end local v2    # "isPolicyAllowed":Z
    .end local v3    # "pi":Landroid/content/pm/PersonaInfo;
    :cond_1
    :goto_1
    return v2

    .line 150
    .restart local v1    # "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .restart local v2    # "isPolicyAllowed":Z
    .restart local v3    # "pi":Landroid/content/pm/PersonaInfo;
    :cond_2
    if-nez p3, :cond_0

    .line 151
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;

    move-result-object v5

    invoke-virtual {v5, p2}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v3

    goto :goto_0

    .line 159
    :cond_3
    if-eqz v1, :cond_1

    .line 160
    if-nez p2, :cond_4

    .line 161
    invoke-virtual {v1, p1, p3}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v0

    .line 163
    .local v0, "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    if-eqz v0, :cond_1

    .line 164
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getRCPPolicy()Lcom/sec/enterprise/knox/container/RCPPolicy;

    move-result-object v4

    .line 166
    .local v4, "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    if-eqz v4, :cond_1

    .line 167
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/container/RCPPolicy;->isMoveFilesToContainerAllowed()Z

    move-result v2

    goto :goto_1

    .line 170
    .end local v0    # "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .end local v4    # "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    :cond_4
    if-nez p3, :cond_1

    .line 171
    invoke-virtual {v1, p1, p2}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v0

    .line 173
    .restart local v0    # "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    if-eqz v0, :cond_1

    .line 174
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getRCPPolicy()Lcom/sec/enterprise/knox/container/RCPPolicy;

    move-result-object v4

    .line 176
    .restart local v4    # "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    if-eqz v4, :cond_1

    .line 177
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/container/RCPPolicy;->isMoveFilesToOwnerAllowed()Z

    move-result v2

    goto :goto_1

    .line 185
    .end local v0    # "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .end local v1    # "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .end local v2    # "isPolicyAllowed":Z
    .end local v3    # "pi":Landroid/content/pm/PersonaInfo;
    .end local v4    # "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    :cond_5
    const-string v5, "ExchangeDataReceiver"

    const-string v6, "isMoveToPolicyAllowed() instance is not initialized"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v17

    if-nez v17, :cond_2

    .line 38
    :cond_0
    const-string v17, "ExchangeDataReceiver"

    const-string v18, " ExchangeDataReceiver onReceive() intent is null"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_1
    :goto_0
    return-void

    .line 44
    :cond_2
    const-string v17, "ExchangeDataReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ExchangeDataReceiver "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 48
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v17, "MoveTo"

    const-string v18, "action"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 49
    const-string v17, "launchFromPersonaManager"

    const/16 v18, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 51
    .local v9, "launchFromPersonaManager":Z
    const-string v17, "targetIdAdded"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    .line 53
    .local v13, "targetIdAdded":Z
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v9, v0, :cond_3

    .line 54
    const-string v17, "target"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 60
    .local v12, "target":I
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->isContainerStateIsValid(I)Z

    move-result v17

    if-nez v17, :cond_4

    .line 61
    const-string v17, "ExchangeDataReceiver"

    const-string v18, "container state is not valid. Do nothing"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const/4 v14, 0x0

    .line 69
    .local v14, "toastMsg":Ljava/lang/String;
    const-string v17, "user"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/os/UserManager;

    .line 70
    .local v16, "um":Landroid/os/UserManager;
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v15

    .line 71
    .local v15, "ui":Landroid/content/pm/UserInfo;
    iget-object v7, v15, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 72
    .local v7, "knoxName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f060002

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v7, v19, v20

    invoke-virtual/range {v17 .. v19}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 75
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v14, v2}, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 56
    .end local v7    # "knoxName":Ljava/lang/String;
    .end local v12    # "target":I
    .end local v14    # "toastMsg":Ljava/lang/String;
    .end local v15    # "ui":Landroid/content/pm/UserInfo;
    .end local v16    # "um":Landroid/os/UserManager;
    :cond_3
    const-string v17, "moveToDstCId"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 57
    .restart local v12    # "target":I
    const-string v17, "ExchangeDataReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "onReceive()  = target = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 79
    :cond_4
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v9, v0, :cond_6

    if-eqz v13, :cond_5

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v13, v0, :cond_6

    const/16 v17, 0x64

    move/from16 v0, v17

    if-lt v12, v0, :cond_6

    .line 80
    :cond_5
    const-string v17, "ExchangeDataReceiver"

    const-string v18, "onReceive() launchFromPersonaManager = true"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    new-instance v10, Landroid/content/Intent;

    const-class v17, Lcom/sec/knox/bridge/activity/MoveContactItemActivity;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .local v10, "moveTo":Landroid/content/Intent;
    invoke-virtual {v10, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 83
    const-string v17, "moveToDstCId"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    const-string v17, "launchAfterLockCheck"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    const/high16 v17, 0x10000000

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 87
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 90
    .end local v10    # "moveTo":Landroid/content/Intent;
    :cond_6
    const-string v17, "ExchangeDataReceiver"

    const-string v18, "onReceive() launchFromPersonaManager = false"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v11

    .line 98
    .local v11, "src":I
    const-string v17, "contact_ids"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v4

    .line 100
    .local v4, "contact_ids":[J
    if-eqz v4, :cond_7

    array-length v0, v4

    move/from16 v17, v0

    if-nez v17, :cond_8

    .line 101
    :cond_7
    const-string v17, "ExchangeDataReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "MoveTo Contact failed, no contact ids received"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    :cond_8
    const-string v17, "ExchangeDataReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "MoveTo Contact "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v11, v12}, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->isMoveToPolicyAllowed(Landroid/content/Context;II)Z

    move-result v17

    if-nez v17, :cond_9

    .line 108
    const/4 v6, 0x0

    .line 109
    .local v6, "errText":Ljava/lang/String;
    const-string v17, "ExchangeDataReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "MoveTo failed by policy "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f060003

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 113
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 117
    .end local v6    # "errText":Ljava/lang/String;
    :cond_9
    const/4 v5, 0x0

    .line 120
    .local v5, "containsSyncedData":Z
    if-eqz v5, :cond_a

    .line 121
    const/4 v6, 0x0

    .line 123
    .restart local v6    # "errText":Ljava/lang/String;
    const-string v17, "ExchangeDataReceiver"

    const-string v18, "Contact Move Failed due to contains synced data"

    invoke-static/range {v17 .. v18}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f060012

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 128
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/knox/bridge/operations/ExchangeDataReceiver;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 134
    .end local v6    # "errText":Ljava/lang/String;
    :cond_a
    invoke-static {v3, v12}, Lcom/sec/knox/bridge/service/BackgroundWorkerService;->getMoveContactsToTargetIntent(Landroid/os/Bundle;I)Landroid/content/Intent;

    move-result-object v8

    .line 135
    .local v8, "lIntent":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
