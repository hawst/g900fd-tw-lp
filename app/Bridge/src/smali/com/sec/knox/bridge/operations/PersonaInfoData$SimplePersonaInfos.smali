.class public Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;
.super Ljava/lang/Object;
.source "PersonaInfoData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/operations/PersonaInfoData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SimplePersonaInfos"
.end annotation


# instance fields
.field isInitialized:Z

.field lock:Ljava/lang/Object;

.field mExtraInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field mList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/knox/bridge/operations/PersonaInfoData;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/operations/PersonaInfoData;)V
    .locals 1

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->this$0:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->isInitialized:Z

    .line 162
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->lock:Ljava/lang/Object;

    .line 164
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mExtraInfo:Ljava/util/HashMap;

    .line 169
    return-void
.end method

.method private addItem(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->isExist(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    :goto_0
    return-void

    .line 204
    :cond_0
    new-instance v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;

    iget-object v2, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->this$0:Lcom/sec/knox/bridge/operations/PersonaInfoData;

    invoke-direct {v1, v2}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;-><init>(Lcom/sec/knox/bridge/operations/PersonaInfoData;)V

    .line 205
    .local v1, "info":Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 207
    .local v0, "bundle":Landroid/os/Bundle;
    iput p1, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->id:I

    .line 208
    iput-object p2, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->name:Ljava/lang/String;

    .line 209
    iput-object p3, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->type:Ljava/lang/String;

    .line 211
    const-string v2, "KNOX"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 212
    iget-object v2, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 217
    :goto_1
    const-string v2, "user_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 218
    const-string v2, "user_name"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v2, "type"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v2, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mExtraInfo:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 214
    :cond_1
    iget-object v2, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private isExist(I)Z
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 226
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 227
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;

    iget v1, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->id:I

    if-ne v1, p1, :cond_0

    .line 228
    const/4 v1, 0x1

    .line 232
    :goto_1
    return v1

    .line 226
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getExtraInfo(I)Landroid/os/Bundle;
    .locals 2
    .param p1, "userid"    # I

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mExtraInfo:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method public getItem(I)Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->isInitialized:Z

    if-nez v0, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->initialize()V

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 263
    :try_start_0
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    if-ltz p1, :cond_1

    .line 264
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;

    monitor-exit v1

    .line 266
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSize()I
    .locals 2

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->isInitialized:Z

    if-nez v0, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->initialize()V

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 276
    :try_start_0
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public initialize()V
    .locals 9

    .prologue
    .line 172
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    if-nez v5, :cond_0

    .line 173
    const-string v5, "PersonaInfoData"

    const-string v6, "initialize() failed, no BridgeService instance"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 177
    :cond_0
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->getInstance()Lcom/sec/knox/bridge/BridgeService;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v5}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v1

    .line 178
    .local v1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-nez v1, :cond_1

    .line 179
    const-string v5, "PersonaInfoData"

    const-string v6, "initialize() failed, personas is null"

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :cond_1
    iget-object v6, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->lock:Ljava/lang/Object;

    monitor-enter v6

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    :try_start_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 184
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 187
    .local v2, "pi":Landroid/content/pm/PersonaInfo;
    sget-object v5, Lcom/sec/knox/bridge/operations/PersonaInfoData;->mCtx:Landroid/content/Context;

    const-string v7, "user"

    invoke-virtual {v5, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/UserManager;

    .line 188
    .local v4, "um":Landroid/os/UserManager;
    iget v5, v2, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v4, v5}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v3

    .line 189
    .local v3, "ui":Landroid/content/pm/UserInfo;
    iget v5, v2, Landroid/content/pm/PersonaInfo;->id:I

    iget-object v7, v3, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    iget-object v8, v2, Landroid/content/pm/PersonaInfo;->type:Ljava/lang/String;

    invoke-direct {p0, v5, v7, v8}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->addItem(ILjava/lang/String;Ljava/lang/String;)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 193
    .end local v2    # "pi":Landroid/content/pm/PersonaInfo;
    .end local v3    # "ui":Landroid/content/pm/UserInfo;
    .end local v4    # "um":Landroid/os/UserManager;
    :cond_2
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->isInitialized:Z

    .line 194
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public removeItem(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 236
    iget-boolean v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->isInitialized:Z

    if-nez v1, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->initialize()V

    .line 251
    :goto_0
    return-void

    .line 241
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 242
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 243
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;

    iget v1, v1, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfo;->id:I

    if-ne p1, v1, :cond_2

    .line 244
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 249
    :cond_1
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/PersonaInfoData$SimplePersonaInfos;->mExtraInfo:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 242
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
