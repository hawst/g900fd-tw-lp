.class Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$4;
.super Ljava/lang/Object;
.source "KnoxUsageMonitorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$4;->this$0:Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 362
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;->moveDataFromCurrentToUploadTable(Landroid/content/Context;)V

    .line 364
    # invokes: Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->triggerUpload()V
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->access$200()V

    .line 365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x240c8400

    add-long v0, v2, v4

    .line 367
    .local v0, "nextuploadTime":J
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->saveNextUploadTime(JLandroid/content/Context;)V

    .line 369
    return-void
.end method
