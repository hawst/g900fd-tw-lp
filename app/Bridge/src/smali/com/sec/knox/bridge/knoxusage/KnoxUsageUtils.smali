.class public Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;
.super Ljava/lang/Object;
.source "KnoxUsageUtils.java"


# direct methods
.method public static isEulaShown(Landroid/content/Context;)Z
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 45
    const-string v5, "persona"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 46
    .local v1, "mPm":Landroid/os/PersonaManager;
    invoke-virtual {v1, v4}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v3

    .line 47
    .local v3, "personaList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 48
    .local v2, "personaInfo":Landroid/content/pm/PersonaInfo;
    iget-boolean v5, v2, Landroid/content/pm/PersonaInfo;->isEulaShown:Z

    if-eqz v5, :cond_0

    .line 49
    const-string v5, "KnoxUsageLogPro"

    const-string v6, "isEulaShown : True"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    .end local v2    # "personaInfo":Landroid/content/pm/PersonaInfo;
    :goto_0
    return v4

    .line 53
    :cond_1
    const-string v4, "KnoxUsageLogPro"

    const-string v5, "isEulaShown : false"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isEulaShownIncludeDying(Landroid/content/Context;)Z
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 58
    const-string v4, "persona"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 59
    .local v1, "mPm":Landroid/os/PersonaManager;
    invoke-virtual {v1}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v3

    .line 60
    .local v3, "personaList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 61
    .local v2, "personaInfo":Landroid/content/pm/PersonaInfo;
    iget-boolean v4, v2, Landroid/content/pm/PersonaInfo;->isEulaShown:Z

    if-eqz v4, :cond_0

    .line 62
    const-string v4, "KnoxUsageLogPro"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isEulaShownIncludedDying : True for PersonaID:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/content/pm/PersonaInfo;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const/4 v4, 0x1

    .line 67
    .end local v2    # "personaInfo":Landroid/content/pm/PersonaInfo;
    :goto_0
    return v4

    .line 66
    :cond_1
    const-string v4, "KnoxUsageLogPro"

    const-string v5, "isEulaShownIncludedDying : false"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isOnPremActivated(Landroid/content/Context;)Z
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    .line 35
    .local v0, "premActive":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "elm_skey_activation_state"

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 37
    .local v1, "premStatus":I
    const-string v2, "KnoxUsageLogPro"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "premStatus:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const/4 v2, 0x1

    if-ne v2, v1, :cond_0

    .line 39
    const/4 v0, 0x1

    .line 41
    :cond_0
    return v0
.end method

.method public static isProcessible(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    const/4 v0, 0x0

    .line 27
    .local v0, "processible":Z
    invoke-static {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isOnPremActivated(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isEulaShown(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    const/4 v0, 0x1

    .line 30
    :cond_0
    return v0
.end method

.method public static isProductShip(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    const/4 v0, 0x0

    .line 85
    .local v0, "shipProd":Z
    const-string v1, "ro.product_ship"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/knox/bridge/uploadmanager/SystemPropertiesProxy;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 86
    return v0
.end method

.method public static notifyLwcLaunchToKnoxusageMonitorService(Landroid/content/Context;I)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "userid"    # I

    .prologue
    .line 72
    invoke-static {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProcessible(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    const-string v1, "KnoxUsageLogPro"

    const-string v2, "notifyLwcLaunchToKnoxusageMonitorService :Not Processible, just return"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :goto_0
    return-void

    .line 77
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.user_handle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 79
    const-string v1, "com.samsung.knox.bridge.knoxusage.lwcsfolderlaunch"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static personasExistOnDevice(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 17
    const-string v3, "persona"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 18
    .local v0, "mPm":Landroid/os/PersonaManager;
    invoke-virtual {v0, v2}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v1

    .line 19
    .local v1, "personaList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 22
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
