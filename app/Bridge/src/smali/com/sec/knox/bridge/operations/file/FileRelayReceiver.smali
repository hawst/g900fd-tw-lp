.class public Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FileRelayReceiver.java"


# static fields
.field public static isCancelForGallery:Z

.field public static isCancelForMusic:Z

.field public static isCancelForMyfiles:Z

.field public static isCancelForSnote:Z

.field public static isCancelForVideo:Z

.field public static isCancelForVoicenote:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentUserId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    sput-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForVideo:Z

    .line 53
    sput-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForMusic:Z

    .line 54
    sput-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForMyfiles:Z

    .line 55
    sput-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForGallery:Z

    .line 56
    sput-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForSnote:Z

    .line 57
    sput-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForVoicenote:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mContext:Landroid/content/Context;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mCurrentUserId:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mCurrentUserId:I

    return v0
.end method

.method private fileMoveByThread(Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 2
    .param p1, "sndPackageName"    # Ljava/lang/String;
    .param p3, "destinationUserId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p2, "srcPathsOrig":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver$1;-><init>(Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;Ljava/lang/String;Ljava/util/ArrayList;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 339
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 340
    return-void
.end method

.method private sendErrorIntent(ILjava/lang/String;Ljava/util/ArrayList;I)V
    .locals 4
    .param p1, "errorCode"    # I
    .param p2, "sndPackageName"    # Ljava/lang/String;
    .param p4, "destinationUserId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p3, "srcPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "FileRelayReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendErrorIntent(): errorCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v1, "FileRelayReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " intent INTENT_FILE_RELAY_FAIL sndPackageName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; for destinationUserId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.knox.container.FileRelayFail"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 142
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "PATH"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 143
    const-string v1, "PACKAGENAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string v1, "DESTUSERID"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 145
    const-string v1, "ERRORCODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 146
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 147
    return-void
.end method


# virtual methods
.method public getIsCancelFor(Ljava/lang/String;)Z
    .locals 4
    .param p1, "sndPackageName"    # Ljava/lang/String;

    .prologue
    .line 394
    const/4 v0, 0x0

    .line 395
    .local v0, "result":Z
    const-string v1, "com.samsung.android.snote"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 396
    sget-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForSnote:Z

    .line 408
    :cond_0
    :goto_0
    const-string v1, "FileRelayReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getIsCancelFor(); sndPackageName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";  result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    return v0

    .line 397
    :cond_1
    const-string v1, "com.sec.android.gallery3d"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 398
    sget-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForGallery:Z

    goto :goto_0

    .line 399
    :cond_2
    const-string v1, "com.samsung.everglades.video"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 400
    sget-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForVideo:Z

    goto :goto_0

    .line 401
    :cond_3
    const-string v1, "com.sec.android.app.music"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 402
    sget-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForMusic:Z

    goto :goto_0

    .line 403
    :cond_4
    const-string v1, "com.sec.android.app.voicenote"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 404
    sget-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForVoicenote:Z

    goto :goto_0

    .line 405
    :cond_5
    const-string v1, "com.sec.android.app.myfiles"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 406
    sget-boolean v0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForMyfiles:Z

    goto :goto_0
.end method

.method public isAllowedApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 343
    const-string v0, "com.samsung.android.snote"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.gallery3d"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.samsung.everglades.video"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.app.music"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.app.voicenote"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.app.myfiles"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    :cond_0
    const-string v0, "FileRelayReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Allowed packageName   : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const/4 v0, 0x1

    .line 353
    :goto_0
    return v0

    .line 352
    :cond_1
    const-string v0, "FileRelayReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Not Allowed packageName   : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mContext:Landroid/content/Context;

    .line 88
    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    .line 89
    :cond_0
    const-string v4, "FileRelayReceiver"

    const-string v5, "Error onReceive(): intent == null || context == null"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_1
    :goto_0
    return-void

    .line 93
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 95
    const-string v4, "FileRelayReceiver"

    const-string v5, "Warning onReceive(): action == null"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_3
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive(): action="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v4, "PACKAGENAME"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "sndPackageName":Ljava/lang/String;
    const-string v4, "PATH"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 102
    .local v3, "srcPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "DESTUSERID"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 104
    .local v1, "destinationUserId":I
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive(): sndPackageName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; srcPaths="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; destinationUserId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v4

    iput v4, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mCurrentUserId:I

    .line 109
    const-string v4, "com.sec.knox.container.FileRelayRequest"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 110
    const-string v4, "FileRelayReceiver"

    const-string v5, "onReceive(): process INTENT_FILE_RELAY_REQUEST"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    if-gez v1, :cond_5

    .line 113
    :cond_4
    const v4, -0xf423f

    invoke-direct {p0, v4, v2, v3, v1}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->sendErrorIntent(ILjava/lang/String;Ljava/util/ArrayList;I)V

    .line 114
    const-string v4, "FileRelayReceiver"

    const-string v5, "Error onReceive(): sndPackageName == null || srcPaths == null || destinationUserId < 0"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119
    :cond_5
    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->setIsCancelFor(Ljava/lang/String;Z)V

    .line 120
    invoke-direct {p0, v2, v3, v1}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->fileMoveByThread(Ljava/lang/String;Ljava/util/ArrayList;I)V

    goto/16 :goto_0

    .line 122
    :cond_6
    const-string v4, "com.sec.knox.container.FileRelayCancel"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive(): process INTENT_FILE_RELAY_CANCEL; sndPackageName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    if-nez v2, :cond_7

    .line 127
    const-string v4, "FileRelayReceiver"

    const-string v5, "Error onReceive(): sndPackageName == null"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 130
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->setIsCancelFor(Ljava/lang/String;Z)V

    .line 131
    const-string v4, "FileRelayReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive(): after canceling; getIsCancelFor(sndPackageName)="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v2}, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->getIsCancelFor(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "sndPackageName"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/lang/String;
    .param p4, "extraInfo"    # I
    .param p5, "destinationUserId"    # I

    .prologue
    .line 358
    const-string v1, "FileRelayReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Snd Intent type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; sndPackageName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; file="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; extraInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; destinationUserId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 362
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "PATH"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 363
    const-string v1, "PACKAGENAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    const-string v1, "DESTUSERID"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 366
    const-string v1, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    const-string v1, "ERRORCODE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 369
    :cond_0
    const-string v1, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 370
    const-string v1, "PROGRESS"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 372
    :cond_1
    iget-object v1, p0, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 373
    return-void
.end method

.method public setIsCancelFor(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "sndPackageName"    # Ljava/lang/String;
    .param p2, "setValue"    # Z

    .prologue
    .line 376
    const-string v0, "com.samsung.android.snote"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    sput-boolean p2, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForSnote:Z

    .line 389
    :cond_0
    :goto_0
    const-string v0, "FileRelayReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setIsCancelFor(); sndPackageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";  isCancelForGallery="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForGallery:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    return-void

    .line 378
    :cond_1
    const-string v0, "com.sec.android.gallery3d"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 379
    sput-boolean p2, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForGallery:Z

    goto :goto_0

    .line 380
    :cond_2
    const-string v0, "com.samsung.everglades.video"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 381
    sput-boolean p2, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForVideo:Z

    goto :goto_0

    .line 382
    :cond_3
    const-string v0, "com.sec.android.app.music"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 383
    sput-boolean p2, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForMusic:Z

    goto :goto_0

    .line 384
    :cond_4
    const-string v0, "com.sec.android.app.voicenote"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 385
    sput-boolean p2, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForVoicenote:Z

    goto :goto_0

    .line 386
    :cond_5
    const-string v0, "com.sec.android.app.myfiles"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    sput-boolean p2, Lcom/sec/knox/bridge/operations/file/FileRelayReceiver;->isCancelForMyfiles:Z

    goto :goto_0
.end method
