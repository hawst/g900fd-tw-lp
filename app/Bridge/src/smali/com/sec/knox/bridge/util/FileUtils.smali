.class public Lcom/sec/knox/bridge/util/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# direct methods
.method public static getErrorCodeMatch(I)I
    .locals 4
    .param p0, "errCode"    # I

    .prologue
    .line 36
    const-string v1, "FileUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getErrorCodeMatch errCode  : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    sparse-switch p0, :sswitch_data_0

    .line 82
    const/4 v0, 0x7

    .line 85
    .local v0, "code":I
    :goto_0
    const-string v1, "FileUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getErrorCodeMatch code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return v0

    .line 41
    .end local v0    # "code":I
    :sswitch_0
    const/4 v0, 0x0

    .line 42
    .restart local v0    # "code":I
    goto :goto_0

    .line 45
    .end local v0    # "code":I
    :sswitch_1
    const/4 v0, 0x1

    .line 46
    .restart local v0    # "code":I
    goto :goto_0

    .line 49
    .end local v0    # "code":I
    :sswitch_2
    const/4 v0, 0x6

    .line 50
    .restart local v0    # "code":I
    goto :goto_0

    .line 54
    .end local v0    # "code":I
    :sswitch_3
    const/16 v0, 0x8

    .line 55
    .restart local v0    # "code":I
    goto :goto_0

    .line 58
    .end local v0    # "code":I
    :sswitch_4
    const/16 v0, 0xe

    .line 59
    .restart local v0    # "code":I
    goto :goto_0

    .line 62
    .end local v0    # "code":I
    :sswitch_5
    const/16 v0, 0xd

    .line 63
    .restart local v0    # "code":I
    goto :goto_0

    .line 66
    .end local v0    # "code":I
    :sswitch_6
    const/16 v0, 0xa

    .line 67
    .restart local v0    # "code":I
    goto :goto_0

    .line 70
    .end local v0    # "code":I
    :sswitch_7
    const/16 v0, 0xb

    .line 71
    .restart local v0    # "code":I
    goto :goto_0

    .line 74
    .end local v0    # "code":I
    :sswitch_8
    const/16 v0, 0xc

    .line 75
    .restart local v0    # "code":I
    goto :goto_0

    .line 78
    .end local v0    # "code":I
    :sswitch_9
    const/4 v0, 0x7

    .line 79
    .restart local v0    # "code":I
    goto :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        -0xf4242 -> :sswitch_6
        -0xf4241 -> :sswitch_8
        -0xf4240 -> :sswitch_7
        -0xf423f -> :sswitch_9
        -0x24 -> :sswitch_4
        -0x1c -> :sswitch_3
        -0x10 -> :sswitch_1
        -0xd -> :sswitch_5
        -0x2 -> :sswitch_2
        0x0 -> :sswitch_0
        0x1c -> :sswitch_3
    .end sparse-switch
.end method

.method public static getTotalFileSize(Ljava/util/ArrayList;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "srcPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-wide/16 v4, 0x0

    .line 92
    .local v4, "totalSize":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 93
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 94
    .local v2, "srcPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v1, "srcFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    goto :goto_1

    .line 99
    .end local v1    # "srcFile":Ljava/io/File;
    .end local v2    # "srcPath":Ljava/lang/String;
    :cond_1
    return-wide v4
.end method

.method public static isAllowedFileName(Ljava/lang/String;)Z
    .locals 6
    .param p0, "dstFileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 103
    const-string v3, "."

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 104
    .local v0, "dotPos":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 115
    :goto_0
    return v2

    .line 107
    :cond_0
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "fileExtName":Ljava/lang/String;
    const-string v3, "apk"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    const-string v2, "FileUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Not Allowed fileExtName   : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const/4 v2, 0x0

    goto :goto_0

    .line 114
    :cond_1
    const-string v3, "FileUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Allowed fileExtName   : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
