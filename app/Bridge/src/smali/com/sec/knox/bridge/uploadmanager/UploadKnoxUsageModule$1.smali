.class Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;
.super Ljava/lang/Object;
.source "UploadKnoxUsageModule.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->processUpload()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;->this$0:Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createAndUploadJsonRequest(Ljava/util/List;Z)V
    .locals 5
    .param p2, "isFirstTime"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "knoxUploadRequests":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;>;"
    const/4 v2, 0x0

    .line 111
    .local v2, "uploadRequestJSON":Lorg/json/JSONObject;
    :try_start_0
    invoke-static {p1}, Lcom/sec/knox/bridge/uploadmanager/DataManager;->compriseKnoxUsageUploadRequest(Ljava/util/List;)Lorg/json/JSONObject;

    move-result-object v2

    .line 113
    if-eqz v2, :cond_1

    .line 115
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;->getServerAdress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "SeverAddress":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createAndUploadJsonRequest:   "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 121
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "In UploadKnoxUsageModule, ELM HTTPS Server Addr : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 123
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HTTPS://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/knoxlog/api/containerusage/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, p2}, Lcom/sec/knox/bridge/uploadmanager/NetworkManager;->callRestAPI(Ljava/lang/String;Lorg/json/JSONObject;Z)V

    .line 141
    .end local v0    # "SeverAddress":Ljava/lang/String;
    :goto_0
    return-void

    .line 130
    .restart local v0    # "SeverAddress":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UploadKnoxUsageModule.processUpload(). Exception. Knox HTTPS Server Addr :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 131
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Can\'t get Server Address."

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v0    # "SeverAddress":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "UploadKnoxUsagelLogModule : processUpload() has Exception."

    invoke-static {v3}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 133
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    const-string v3, "In UploadKnoxUsagelLogModule, json REQUEST data is null "

    invoke-static {v3}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 134
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    throw v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private getServerAdress(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "severAddress":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;->isChinaModel()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    const-string v1, "UploadKnoxUsagelLogModule : getServerAdress() : China model"

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 149
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProductShip(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    const-string v0, "prod-knoxlog.secb2b.com.cn"

    .line 164
    :goto_0
    return-object v0

    .line 152
    :cond_0
    const-string v0, "stage-knoxlog.secb2b.com.cn"

    goto :goto_0

    .line 156
    :cond_1
    const-string v1, "UploadKnoxUsagelLogModule : getServerAdress() : Non China model"

    invoke-static {v1}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 157
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProductShip(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 158
    const-string v0, "prod-knoxlog.secb2b.com"

    goto :goto_0

    .line 160
    :cond_2
    const-string v0, "stage-knoxlog.secb2b.com"

    goto :goto_0
.end method

.method private isChinaModel()Z
    .locals 3

    .prologue
    .line 169
    const/4 v1, 0x0

    .line 170
    .local v1, "isChinaModel":Z
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "China"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    const-string v2, "UploadKnoxUsagelLogModule : isChinaModel() : true"

    invoke-static {v2}, Lcom/sec/knox/bridge/uploadmanager/KnoxLog;->d(Ljava/lang/String;)V

    .line 173
    const/4 v1, 0x1

    .line 175
    :cond_0
    return v1
.end method


# virtual methods
.method public run()V
    .locals 25

    .prologue
    .line 57
    const/4 v10, 0x1

    # invokes: Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->setUploadInProgress(Z)V
    invoke-static {v10}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->access$000(Z)V

    .line 58
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v21, "knoxUploadRequests":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;->this$0:Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;

    # getter for: Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->isFirstUpload:Z
    invoke-static {v10}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->access$100(Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 60
    const-string v10, "KnoxUsageLogPro"

    const-string v13, "processKnoxUsagelUploadRunnable : Not first time upload. Read from Pending table and upload"

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v19

    .line 62
    .local v19, "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    const-string v10, "uploads"

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getAllEntries(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 63
    .local v18, "cursor":Landroid/database/Cursor;
    if-eqz v18, :cond_1

    .line 64
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-lez v10, :cond_2

    .line 65
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    .line 67
    :cond_0
    const-string v10, "persona_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 68
    .local v3, "container_id":I
    const-string v10, "next_upload"

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 69
    .local v8, "uploadFrequency":J
    const-string v10, "track_start_ts"

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 70
    .local v6, "startOfWeek":J
    const-string v10, "login_count"

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 71
    .local v5, "login_count":I
    const-string v10, "persona_status"

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 72
    .local v4, "status":I
    const-string v10, "KnoxUsageLogPro"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Regular upload: Adding request for personaId:"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    new-instance v2, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;

    invoke-direct/range {v2 .. v9}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;-><init>(IIIJJ)V

    .line 74
    .local v2, "knoxUsageInfo":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-nez v10, :cond_0

    .line 80
    .end local v2    # "knoxUsageInfo":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    .end local v3    # "container_id":I
    .end local v4    # "status":I
    .end local v5    # "login_count":I
    .end local v6    # "startOfWeek":J
    .end local v8    # "uploadFrequency":J
    :goto_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 97
    .end local v18    # "cursor":Landroid/database/Cursor;
    .end local v19    # "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    :cond_1
    if-eqz v21, :cond_4

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_4

    .line 98
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;->this$0:Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;

    # getter for: Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->isFirstUpload:Z
    invoke-static {v10}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->access$100(Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;)Z

    move-result v10

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v10}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule$1;->createAndUploadJsonRequest(Ljava/util/List;Z)V

    .line 102
    :goto_1
    const-string v10, "KnoxUsageLogPro"

    const-string v13, "UploadKnoxUsageModule : processUpload() Completed."

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v10, 0x0

    # invokes: Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->setUploadInProgress(Z)V
    invoke-static {v10}, Lcom/sec/knox/bridge/uploadmanager/UploadKnoxUsageModule;->access$000(Z)V

    .line 104
    return-void

    .line 78
    .restart local v18    # "cursor":Landroid/database/Cursor;
    .restart local v19    # "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    :cond_2
    const-string v10, "Knox"

    const-string v13, "cursor not null but count 0: insert a row for  userId ="

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 84
    .end local v18    # "cursor":Landroid/database/Cursor;
    .end local v19    # "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    :cond_3
    const-string v10, "KnoxUsageLogPro"

    const-string v13, "processKnoxUsagelUploadRunnable : first time upload. Read from Persona list and upload"

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v13, "persona"

    invoke-virtual {v10, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/os/PersonaManager;

    .line 86
    .local v22, "mPm":Landroid/os/PersonaManager;
    const/4 v10, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v24

    .line 87
    .local v24, "personaList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 88
    .local v14, "currentTime":J
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/content/pm/PersonaInfo;

    .line 89
    .local v23, "personaInfo":Landroid/content/pm/PersonaInfo;
    invoke-virtual/range {v23 .. v23}, Landroid/content/pm/PersonaInfo;->getId()I

    move-result v11

    .line 90
    .local v11, "personaId":I
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/pm/PersonaState;->getKnox2_0State()I

    move-result v12

    .line 91
    .local v12, "state":I
    const-string v10, "KnoxUsageLogPro"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "state of Persona"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v16, " = "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v10, "KnoxUsageLogPro"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "First Time Upload: Adding request for personaId:"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    new-instance v2, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;

    const/4 v13, 0x1

    move-object v10, v2

    move-wide/from16 v16, v14

    invoke-direct/range {v10 .. v17}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;-><init>(IIIJJ)V

    .line 94
    .restart local v2    # "knoxUsageInfo":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 100
    .end local v2    # "knoxUsageInfo":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    .end local v11    # "personaId":I
    .end local v12    # "state":I
    .end local v14    # "currentTime":J
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v22    # "mPm":Landroid/os/PersonaManager;
    .end local v23    # "personaInfo":Landroid/content/pm/PersonaInfo;
    .end local v24    # "personaList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :cond_4
    const-string v10, "KnoxUsageLogPro"

    const-string v13, "UploadKnoxUsageModule : No entries in Pending table."

    invoke-static {v10, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
