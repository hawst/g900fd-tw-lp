.class Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;
.super Lcom/sec/knox/bridge/IBridgeService$Stub;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IBridgeBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/BridgeService;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/BridgeService;)V
    .locals 0

    .prologue
    .line 1753
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;->this$0:Lcom/sec/knox/bridge/BridgeService;

    invoke-direct {p0}, Lcom/sec/knox/bridge/IBridgeService$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/BridgeService;Lcom/sec/knox/bridge/BridgeService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/BridgeService;
    .param p2, "x1"    # Lcom/sec/knox/bridge/BridgeService$1;

    .prologue
    .line 1753
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;-><init>(Lcom/sec/knox/bridge/BridgeService;)V

    return-void
.end method


# virtual methods
.method public checkSAccount(Ljava/lang/String;Landroid/content/ISAccountCallback;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "callback"    # Landroid/content/ISAccountCallback;

    .prologue
    .line 1849
    const-string v0, "checkSAccount"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1850
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "checkSAccount() - going to tell system RCPService"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1851
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1, p2}, Landroid/os/RCPManager;->checkSAccount(Ljava/lang/String;Landroid/content/ISAccountCallback;)V

    .line 1852
    return-void
.end method

.method public checkSAccountPassword(Ljava/lang/String;Landroid/content/ISAccountCallback;)V
    .locals 2
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "callback"    # Landroid/content/ISAccountCallback;

    .prologue
    .line 1856
    const-string v0, "checkSAccountPassword"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1857
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "checkSAccountPassword() - going to tell system RCPService"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1858
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1, p2}, Landroid/os/RCPManager;->checkSAccountPassword(Ljava/lang/String;Landroid/content/ISAccountCallback;)V

    .line 1859
    return-void
.end method

.method public copyClipboardFile(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 2
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "destPath"    # Ljava/lang/String;
    .param p3, "permissions"    # I

    .prologue
    .line 1828
    const-string v0, "copyClipboardFile"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1829
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "copyClipboardFile() - going to tell system bridge"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RCPManager;->copyClipboardFile(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public deleteClipboardFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1835
    const-string v0, "deleteClipboardFile"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1836
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deleteClipboardFile() - going to tell system bridge"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1837
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1}, Landroid/os/RCPManager;->deleteClipboardFile(Ljava/lang/String;)V

    .line 1838
    return-void
.end method

.method public getUsersToSyncFrom(Ljava/lang/String;)[I
    .locals 4
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 1814
    const-string v1, "getUsersToSyncFrom"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v1}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1815
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "getUsersToSyncFrom() - going to tell system bridge"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1817
    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const-string v2, "Clipboard"

    const-string v3, "knox-import-data"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->getPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2, v3}, Lcom/sec/knox/bridge/BridgeService;->access$1700(Lcom/sec/knox/bridge/BridgeService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1819
    .local v0, "policy":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1820
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getUsersToSyncFrom: IMPORT not alloed for Clipboard = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1821
    const/4 v1, 0x0

    .line 1823
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v1, p1}, Landroid/os/RCPManager;->getUsersToSyncFrom(Ljava/lang/String;)[I

    move-result-object v1

    goto :goto_0
.end method

.method public queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "providerName"    # Ljava/lang/String;
    .param p2, "resourceName"    # Ljava/lang/String;
    .param p3, "containerId"    # I
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/CustomCursor;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1791
    const-string v0, "queryAllProviders"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1794
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Landroid/os/RCPManager;->queryAllProviders(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public queryProvider(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;
    .locals 8
    .param p1, "providerName"    # Ljava/lang/String;
    .param p2, "resourceName"    # Ljava/lang/String;
    .param p3, "containerId"    # I
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1768
    const-string v0, "queryProvider"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1771
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Landroid/os/RCPManager;->queryProvider(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/CustomCursor;

    move-result-object v0

    return-object v0
.end method

.method public refreshClipboard()V
    .locals 2

    .prologue
    .line 1842
    const-string v0, "refreshClipboard"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1843
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "refreshClipboard() - going to tell system bridge"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1844
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0}, Landroid/os/RCPManager;->refreshClipboard()V

    .line 1845
    return-void
.end method

.method public registerProvider(Ljava/lang/String;Landroid/content/IProviderCallBack;)V
    .locals 1
    .param p1, "providerName"    # Ljava/lang/String;
    .param p2, "mProvider"    # Landroid/content/IProviderCallBack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1758
    const-string v0, "registerProvider"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1759
    if-eqz p2, :cond_0

    .line 1760
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IBridgeBinder;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mBridgeProviderList:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$2000(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1761
    :cond_0
    return-void
.end method

.method public saveSharedSandbox(I)V
    .locals 2
    .param p1, "toUser"    # I

    .prologue
    .line 1807
    const-string v0, "saveSharedSandbox"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1808
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "saveSharedSandbox() - going to tell system bridge"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1809
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0, p1}, Landroid/os/RCPManager;->saveSharedSandbox(I)V

    .line 1810
    return-void
.end method

.method public setupClipbardSandbox()V
    .locals 2

    .prologue
    .line 1800
    const-string v0, "setupClipbardSandbox"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 1801
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setupClipbardSandbox() - going to tell system bridge"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1802
    sget-object v0, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    invoke-virtual {v0}, Landroid/os/RCPManager;->setupClipbardSandbox()V

    .line 1803
    return-void
.end method
