.class Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;
.super Ljava/lang/Object;
.source "MoveToKnoxActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 220
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cancel move to files. threadId = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToThreadId:J
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$700(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$800(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/content/IRCPInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToThreadId:J
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$700(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Landroid/content/IRCPInterface;->cancel(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_0
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 229
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "moveFiles throws remote exception"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 226
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$2;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-virtual {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->finish()V

    goto :goto_0
.end method
