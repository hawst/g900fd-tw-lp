.class public Lcom/sec/knox/bridge/util/NotificationUtil;
.super Ljava/lang/Object;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/util/NotificationUtil$tagId;,
        Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;
    }
.end annotation


# static fields
.field private static mCurrentPersona:I

.field private static mNotificationBuilder:Landroid/app/Notification$Builder;

.field private static mTagIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/knox/bridge/util/NotificationUtil$tagId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/content/pm/PersonaInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mPersona:Landroid/os/PersonaManager;

.field private mPersonaPolicy:Landroid/os/PersonaPolicyManager;

.field private snm:Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;

    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mNotificationBuilder:Landroid/app/Notification$Builder;

    .line 61
    const/4 v0, 0x0

    sput v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mCurrentPersona:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v1, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersona:Landroid/os/PersonaManager;

    .line 45
    iput-object v1, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersonaPolicy:Landroid/os/PersonaPolicyManager;

    .line 47
    iput-object v1, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;

    .line 55
    iput-object v1, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->snm:Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;

    .line 224
    new-instance v0, Lcom/sec/knox/bridge/util/NotificationUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/knox/bridge/util/NotificationUtil$1;-><init>(Lcom/sec/knox/bridge/util/NotificationUtil;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->comparator:Ljava/util/Comparator;

    .line 64
    iput-object p1, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;

    .line 65
    new-instance v0, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;-><init>(Lcom/sec/knox/bridge/util/NotificationUtil;Lcom/sec/knox/bridge/util/NotificationUtil$1;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->snm:Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;

    .line 66
    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/util/NotificationUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/util/NotificationUtil;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mTagIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getIconForKnoxModeSwitcher(I)I
    .locals 6
    .param p1, "userId"    # I

    .prologue
    .line 326
    invoke-direct {p0}, Lcom/sec/knox/bridge/util/NotificationUtil;->getPersonaManager()Landroid/os/PersonaManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v0

    .line 327
    .local v0, "pi":Landroid/content/pm/PersonaInfo;
    const/4 v3, 0x0

    .line 331
    .local v3, "userName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 332
    iget-object v4, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;

    const-string v5, "user"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/UserManager;

    .line 333
    .local v2, "um":Landroid/os/UserManager;
    iget v4, v0, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v2, v4}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v1

    .line 334
    .local v1, "ui":Landroid/content/pm/UserInfo;
    iget-object v3, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 338
    .end local v1    # "ui":Landroid/content/pm/UserInfo;
    .end local v2    # "um":Landroid/os/UserManager;
    :cond_0
    const-string v4, "KNOX"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 339
    const v4, 0x7f020002

    .line 341
    :goto_0
    return v4

    :cond_1
    const v4, 0x7f020003

    goto :goto_0
.end method

.method private getNotification(Landroid/content/Context;Landroid/content/pm/PersonaInfo;Z)Landroid/app/Notification;
    .locals 37
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pi"    # Landroid/content/pm/PersonaInfo;
    .param p3, "isLastItem"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/bridge/util/NotificationUtil;->getPersonaManager()Landroid/os/PersonaManager;

    move-result-object v27

    .line 87
    .local v27, "pm":Landroid/os/PersonaManager;
    sget v9, Lcom/sec/knox/bridge/util/NotificationUtil;->mCurrentPersona:I

    .line 89
    .local v9, "currentUser":I
    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v34

    invoke-static/range {v34 .. v34}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    .line 90
    .local v19, "isInPersona":Ljava/lang/Boolean;
    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v34, v0

    move/from16 v0, v34

    if-ne v0, v9, :cond_4

    const/16 v34, 0x1

    :goto_0
    invoke-static/range {v34 .. v34}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    .line 92
    .local v20, "isNotificationForHome":Ljava/lang/Boolean;
    new-instance v28, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v34

    const v35, 0x7f030004

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 98
    .local v28, "rv":Landroid/widget/RemoteViews;
    const-string v34, "user"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Landroid/os/UserManager;

    .line 99
    .local v31, "um":Landroid/os/UserManager;
    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v34, v0

    move-object/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v30

    .line 101
    .local v30, "ui":Landroid/content/pm/UserInfo;
    const-string v34, "NotificationUtil user"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "PErsona Name:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    move-object/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/16 v29, 0x0

    .line 103
    .local v29, "title1":Ljava/lang/String;
    const-string v34, "KNOX"

    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1

    .line 104
    const-string v34, "NotificationUtil user"

    const-string v35, "Knox 1 enabled:"

    invoke-static/range {v34 .. v35}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v34, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v29

    .line 106
    if-nez v29, :cond_0

    .line 107
    const-string v34, "NotificationUtil user"

    const-string v35, "No new name!"

    invoke-static/range {v34 .. v35}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 111
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 112
    .local v14, "iconDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v14, :cond_5

    .line 113
    check-cast v14, Landroid/graphics/drawable/BitmapDrawable;

    .end local v14    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v14}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    .line 114
    .local v13, "iconBitmap":Landroid/graphics/Bitmap;
    if-eqz v13, :cond_1

    .line 115
    const v34, 0x7f080010

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1, v13}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 122
    .end local v13    # "iconBitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_1
    const-string v34, "KNOX II"

    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    .line 123
    const-string v34, "NotificationUtil user"

    const-string v35, "Knox 2 enabled:"

    invoke-static/range {v34 .. v35}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 126
    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/util/NotificationUtil;->getIconForKnoxModeSwitcher(I)I

    move-result v12

    .line 127
    .local v12, "icon":I
    const v34, 0x7f080010

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1, v12}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 130
    .end local v12    # "icon":I
    :cond_2
    const v34, 0x7f080012

    move-object/from16 v0, v28

    move/from16 v1, v34

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 132
    const/4 v5, 0x0

    .line 134
    .local v5, "content1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersona:Landroid/os/PersonaManager;

    move-object/from16 v34, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v26

    .line 135
    .local v26, "personaState1":Landroid/content/pm/PersonaState;
    const/16 v21, 0x0

    .line 137
    .local v21, "isSessionExpired1":Z
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v34

    if-eqz v34, :cond_6

    .line 138
    const v34, 0x7f060023

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 146
    :goto_2
    const v34, 0x7f080013

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 148
    new-instance v16, Landroid/content/Intent;

    const-string v34, "com.sec.knox.bridge.receiver.INTENT_SWITCH_MODE"

    move-object/from16 v0, v16

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 149
    .local v16, "intent":Landroid/content/Intent;
    const-string v34, "changeMode"

    const/16 v35, 0x1

    move-object/from16 v0, v16

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 151
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v34

    if-eqz v34, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/bridge/util/NotificationUtil;->getParentUser()I

    move-result v15

    .line 152
    .local v15, "idToSwitch":I
    :goto_3
    const-string v34, "personaId"

    move-object/from16 v0, v16

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 153
    const-string v34, "NotificationUtil user"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "getNotificationGoToKnox /  personaId = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v24

    .line 155
    .local v24, "oldId":J
    add-int/lit16 v0, v15, 0x1388

    move/from16 v34, v0

    const/high16 v35, 0x8000000

    move-object/from16 v0, p1

    move/from16 v1, v34

    move-object/from16 v2, v16

    move/from16 v3, v35

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 157
    .local v6, "contentIntent1":Landroid/app/PendingIntent;
    invoke-static/range {v24 .. v25}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 158
    const v34, 0x7f08000e

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 160
    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/util/NotificationUtil;->isKeyGuardEnabledFromMDM(I)Z

    move-result v34

    if-eqz v34, :cond_b

    .line 161
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v34

    if-nez v34, :cond_a

    .line 162
    const-string v34, "NotificationUtil user"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "getPersonaState / 1 / personaId = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " : status = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " / isSessionExpired = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    sget-object v34, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    move-object/from16 v0, v26

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_3

    sget-object v34, Landroid/content/pm/PersonaState;->CREATING:Landroid/content/pm/PersonaState;

    move-object/from16 v0, v26

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_9

    :cond_3
    if-nez v21, :cond_9

    .line 166
    new-instance v17, Landroid/content/Intent;

    const-string v34, "com.sec.knox.bridge.receiver.INTENT_LOCK_IMMEDIATLY"

    move-object/from16 v0, v17

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .local v17, "intentLock1":Landroid/content/Intent;
    const-string v34, "personaId"

    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v35, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 169
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v24

    .line 170
    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v34, v0

    move/from16 v0, v34

    add-int/lit16 v0, v0, 0x1b58

    move/from16 v34, v0

    const/high16 v35, 0x8000000

    move-object/from16 v0, p1

    move/from16 v1, v34

    move-object/from16 v2, v17

    move/from16 v3, v35

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 172
    .local v7, "contentIntentLock1":Landroid/app/PendingIntent;
    invoke-static/range {v24 .. v25}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 174
    const v34, 0x7f080014

    const/16 v35, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 175
    const v34, 0x7f080014

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 196
    .end local v7    # "contentIntentLock1":Landroid/app/PendingIntent;
    .end local v17    # "intentLock1":Landroid/content/Intent;
    :goto_4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v34

    if-eqz v34, :cond_c

    if-eqz p3, :cond_c

    const/16 v22, 0x1

    .line 199
    .local v22, "notiPriority":I
    :goto_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 200
    .local v10, "currTime":J
    if-eqz p3, :cond_d

    move-wide/from16 v32, v10

    .line 202
    .local v32, "when":J
    :goto_6
    invoke-direct/range {p0 .. p1}, Lcom/sec/knox/bridge/util/NotificationUtil;->getNotificationBuilder(Landroid/content/Context;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 204
    .local v4, "builder":Landroid/app/Notification$Builder;
    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v34

    const v35, 0x7f020010

    invoke-virtual/range {v34 .. v35}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v34

    const/16 v35, 0x1

    invoke-virtual/range {v34 .. v35}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    move-result-object v34

    move-object/from16 v0, v34

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 208
    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v23

    .line 209
    .local v23, "notification":Landroid/app/Notification;
    const/16 v34, -0x1

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/app/Notification;->visibility:I

    .line 211
    const/16 v34, 0x80

    move/from16 v0, v34

    move-object/from16 v1, v23

    iput v0, v1, Landroid/app/Notification;->twQuickPanelEvent:I

    .line 213
    return-object v23

    .line 90
    .end local v4    # "builder":Landroid/app/Notification$Builder;
    .end local v5    # "content1":Ljava/lang/String;
    .end local v6    # "contentIntent1":Landroid/app/PendingIntent;
    .end local v10    # "currTime":J
    .end local v15    # "idToSwitch":I
    .end local v16    # "intent":Landroid/content/Intent;
    .end local v20    # "isNotificationForHome":Ljava/lang/Boolean;
    .end local v21    # "isSessionExpired1":Z
    .end local v22    # "notiPriority":I
    .end local v23    # "notification":Landroid/app/Notification;
    .end local v24    # "oldId":J
    .end local v26    # "personaState1":Landroid/content/pm/PersonaState;
    .end local v28    # "rv":Landroid/widget/RemoteViews;
    .end local v29    # "title1":Ljava/lang/String;
    .end local v30    # "ui":Landroid/content/pm/UserInfo;
    .end local v31    # "um":Landroid/os/UserManager;
    .end local v32    # "when":J
    :cond_4
    const/16 v34, 0x0

    goto/16 :goto_0

    .line 118
    .restart local v14    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v20    # "isNotificationForHome":Ljava/lang/Boolean;
    .restart local v28    # "rv":Landroid/widget/RemoteViews;
    .restart local v29    # "title1":Ljava/lang/String;
    .restart local v30    # "ui":Landroid/content/pm/UserInfo;
    .restart local v31    # "um":Landroid/os/UserManager;
    :cond_5
    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/sec/knox/bridge/util/NotificationUtil;->getIconForKnoxModeSwitcher(I)I

    move-result v12

    .line 119
    .restart local v12    # "icon":I
    const v34, 0x7f080010

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1, v12}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_1

    .line 140
    .end local v12    # "icon":I
    .end local v14    # "iconDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v5    # "content1":Ljava/lang/String;
    .restart local v21    # "isSessionExpired1":Z
    .restart local v26    # "personaState1":Landroid/content/pm/PersonaState;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersona:Landroid/os/PersonaManager;

    move-object/from16 v34, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v34

    sget-object v35, Landroid/content/pm/PersonaAttribute;->PASSWORD_CHANGE_REQUEST:Landroid/content/pm/PersonaAttribute;

    invoke-virtual/range {v34 .. v35}, Landroid/os/PersonaManager$StateManager;->isAttribute(Landroid/content/pm/PersonaAttribute;)Z

    move-result v34

    if-eqz v34, :cond_7

    .line 141
    const v34, 0x7f060024

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 143
    :cond_7
    const v34, 0x7f060022

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 151
    .restart local v16    # "intent":Landroid/content/Intent;
    :cond_8
    move-object/from16 v0, p2

    iget v15, v0, Landroid/content/pm/PersonaInfo;->id:I

    goto/16 :goto_3

    .line 177
    .restart local v6    # "contentIntent1":Landroid/app/PendingIntent;
    .restart local v15    # "idToSwitch":I
    .restart local v24    # "oldId":J
    :cond_9
    const v34, 0x7f080014

    const/16 v35, 0x8

    move-object/from16 v0, v28

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_4

    .line 180
    :cond_a
    new-instance v18, Landroid/content/Intent;

    const-string v34, "com.sec.knox.bridge.receiver.INTENT_SWITCH_LOCK"

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 182
    .local v18, "intentSwitchLock":Landroid/content/Intent;
    const-string v34, "personaId"

    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v35, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 183
    const-string v34, "NotificationUtil user"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "getNotificationGoToKnox /  personaId = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v24

    .line 185
    move-object/from16 v0, p2

    iget v0, v0, Landroid/content/pm/PersonaInfo;->id:I

    move/from16 v34, v0

    move/from16 v0, v34

    add-int/lit16 v0, v0, 0x1770

    move/from16 v34, v0

    const/high16 v35, 0x8000000

    move-object/from16 v0, p1

    move/from16 v1, v34

    move-object/from16 v2, v18

    move/from16 v3, v35

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 188
    .local v8, "contentIntentSwitchLock":Landroid/app/PendingIntent;
    invoke-static/range {v24 .. v25}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 189
    const v34, 0x7f080014

    const/16 v35, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 190
    const v34, 0x7f080014

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_4

    .line 193
    .end local v8    # "contentIntentSwitchLock":Landroid/app/PendingIntent;
    .end local v18    # "intentSwitchLock":Landroid/content/Intent;
    :cond_b
    const v34, 0x7f080014

    const/16 v35, 0x8

    move-object/from16 v0, v28

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_4

    .line 196
    :cond_c
    const/16 v22, -0x2

    goto/16 :goto_5

    .line 200
    .restart local v10    # "currTime":J
    .restart local v22    # "notiPriority":I
    :cond_d
    const-wide/16 v34, 0xa

    add-long v32, v10, v34

    goto/16 :goto_6
.end method

.method private getNotificationBuilder(Landroid/content/Context;)Landroid/app/Notification$Builder;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 217
    sget-object v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mNotificationBuilder:Landroid/app/Notification$Builder;

    if-nez v0, :cond_0

    .line 218
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mNotificationBuilder:Landroid/app/Notification$Builder;

    .line 221
    :cond_0
    sget-object v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mNotificationBuilder:Landroid/app/Notification$Builder;

    return-object v0
.end method

.method private getParentUser()I
    .locals 2

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/sec/knox/bridge/util/NotificationUtil;->getPersonaManager()Landroid/os/PersonaManager;

    move-result-object v0

    .line 310
    .local v0, "pm":Landroid/os/PersonaManager;
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 311
    invoke-virtual {v0}, Landroid/os/PersonaManager;->getParentUserForCurrentPersona()I

    move-result v1

    .line 313
    :goto_0
    return v1

    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    goto :goto_0
.end method

.method private getPersonaManager()Landroid/os/PersonaManager;
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersona:Landroid/os/PersonaManager;

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;

    const-string v1, "persona"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersona:Landroid/os/PersonaManager;

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersona:Landroid/os/PersonaManager;

    return-object v0
.end method

.method private getVisiblePersona(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PersonaInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PersonaInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v1, "personasToShow":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/PersonaInfo;>;"
    if-eqz p1, :cond_1

    .line 72
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 73
    .local v2, "pi":Landroid/content/pm/PersonaInfo;
    iget-object v3, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersonaPolicy:Landroid/os/PersonaPolicyManager;

    iget v4, v2, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v3, v4}, Landroid/os/PersonaPolicyManager;->getSwitchNotifEnabled(I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Landroid/content/pm/PersonaInfo;->isLightWeightContainer:Z

    if-nez v3, :cond_0

    .line 75
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "pi":Landroid/content/pm/PersonaInfo;
    :cond_1
    return-object v1
.end method

.method private isKeyGuardEnabledFromMDM(I)Z
    .locals 8
    .param p1, "personaId"    # I

    .prologue
    .line 346
    const/4 v4, 0x1

    .line 347
    .local v4, "returnValue":Z
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v3

    .line 349
    .local v3, "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    :try_start_0
    iget-object v5, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v5, p1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v1

    .line 350
    .local v1, "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerConfigurationPolicy()Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

    move-result-object v0

    .line 351
    .local v0, "ccp":Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getEnforceAuthForContainer()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 355
    .end local v0    # "ccp":Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;
    .end local v1    # "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    :goto_0
    const-string v5, "NotificationUtil user"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isKeyGuardEnabledFromMDM : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    return v4

    .line 352
    :catch_0
    move-exception v2

    .line 353
    .local v2, "e":Ljava/lang/SecurityException;
    const-string v5, "NotificationUtil user"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SecurityException:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2}, Lcom/sec/knox/bridge/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public cancalNotification()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->snm:Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;

    invoke-virtual {v0}, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->cancelAll()V

    .line 305
    return-void
.end method

.method public cancalNotification(I)V
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->snm:Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;

    add-int/lit16 v1, p1, 0x1388

    invoke-virtual {v0, v1}, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->cancel(I)V

    .line 301
    return-void
.end method

.method public getCurrentUserId()I
    .locals 1

    .prologue
    .line 242
    sget v0, Lcom/sec/knox/bridge/util/NotificationUtil;->mCurrentPersona:I

    return v0
.end method

.method public setCurrentUserId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 246
    sput p1, Lcom/sec/knox/bridge/util/NotificationUtil;->mCurrentPersona:I

    .line 247
    return-void
.end method

.method public showNotification()V
    .locals 11

    .prologue
    .line 252
    :try_start_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v9

    if-eqz v9, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    invoke-direct {p0}, Lcom/sec/knox/bridge/util/NotificationUtil;->getPersonaManager()Landroid/os/PersonaManager;

    move-result-object v7

    .line 258
    .local v7, "pm":Landroid/os/PersonaManager;
    if-eqz v7, :cond_0

    .line 261
    invoke-virtual {v7}, Landroid/os/PersonaManager;->isKioskContainerExistOnDevice()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 262
    const-string v9, "NotificationUtil user"

    const-string v10, "KIOSK mode is enabled for container . Igonoring showing notification.."

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 294
    .end local v7    # "pm":Landroid/os/PersonaManager;
    :catch_0
    move-exception v8

    .line 295
    .local v8, "re":Landroid/os/RemoteException;
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 266
    .end local v8    # "re":Landroid/os/RemoteException;
    .restart local v7    # "pm":Landroid/os/PersonaManager;
    :cond_2
    :try_start_1
    const-string v9, "persona_policy"

    invoke-virtual {v7, v9}, Landroid/os/PersonaManager;->getPersonaService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PersonaPolicyManager;

    iput-object v9, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mPersonaPolicy:Landroid/os/PersonaPolicyManager;

    .line 269
    invoke-direct {p0}, Lcom/sec/knox/bridge/util/NotificationUtil;->getParentUser()I

    move-result v4

    .line 271
    .local v4, "parentUser":I
    invoke-virtual {v7, v4}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v5

    .line 273
    .local v5, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-direct {p0, v5}, Lcom/sec/knox/bridge/util/NotificationUtil;->getVisiblePersona(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v5

    .line 275
    iget-object v9, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->comparator:Ljava/util/Comparator;

    invoke-static {v5, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 277
    const-string v9, "NotificationUtil user"

    const-string v10, "<<<<<< received notification >>>>>>>>>>"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 280
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 281
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PersonaInfo;

    .line 282
    .local v6, "pi":Landroid/content/pm/PersonaInfo;
    add-int/lit8 v9, v0, -0x1

    if-ne v1, v9, :cond_3

    const/4 v2, 0x1

    .line 284
    .local v2, "isLastItem":Z
    :goto_2
    iget-object v9, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v9, v6, v2}, Lcom/sec/knox/bridge/util/NotificationUtil;->getNotification(Landroid/content/Context;Landroid/content/pm/PersonaInfo;Z)Landroid/app/Notification;

    move-result-object v3

    .line 285
    .local v3, "notification":Landroid/app/Notification;
    if-eqz v3, :cond_4

    .line 286
    iget-object v9, p0, Lcom/sec/knox/bridge/util/NotificationUtil;->snm:Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;

    iget v10, v6, Landroid/content/pm/PersonaInfo;->id:I

    add-int/lit16 v10, v10, 0x1388

    invoke-virtual {v9, v10, v3}, Lcom/sec/knox/bridge/util/NotificationUtil$SyncedNotificationManager;->notify(ILandroid/app/Notification;)V

    .line 288
    const-string v9, "NotificationUtil user"

    const-string v10, "NotificationUtil inside receiver showing notification getNotificationGoToKnox"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 282
    .end local v2    # "isLastItem":Z
    .end local v3    # "notification":Landroid/app/Notification;
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 291
    .restart local v2    # "isLastItem":Z
    .restart local v3    # "notification":Landroid/app/Notification;
    :cond_4
    const-string v9, "NotificationUtil user"

    const-string v10, "getNotificationGoToKnox gets null"

    invoke-static {v9, v10}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method
