.class public Lcom/sec/knox/bridge/activity/SwitchB2BActivity;
.super Landroid/app/Activity;
.source "SwitchB2BActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/activity/SwitchB2BActivity$1;,
        Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;
    }
.end annotation


# static fields
.field public static isInLockChecking:Z


# instance fields
.field private mMigrationConn:Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->isInLockChecking:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->mMigrationConn:Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;

    .line 154
    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/activity/SwitchB2BActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/SwitchB2BActivity;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->switchAndFinish()V

    return-void
.end method

.method private connectToMigrationService()V
    .locals 5

    .prologue
    .line 140
    :try_start_0
    new-instance v2, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;-><init>(Lcom/sec/knox/bridge/activity/SwitchB2BActivity;Lcom/sec/knox/bridge/activity/SwitchB2BActivity$1;)V

    iput-object v2, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->mMigrationConn:Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;

    .line 141
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.knox.migrationagent.BIND_MIGRATION_STATE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->mMigrationConn:Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 144
    .local v1, "status":Z
    const-string v2, "SwitchB2BActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connectToMigrationService:status --- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    if-nez v1, :cond_0

    .line 146
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->switchAndFinish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .end local v1    # "status":Z
    :cond_0
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SwitchB2BActivity"

    const-string v3, "connectToMigrationService:UnableToStart"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->switchAndFinish()V

    goto :goto_0
.end method

.method public static processAfterKeyCheck(Landroid/content/Context;)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 117
    sget-boolean v0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->isInLockChecking:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-static {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->switchToB2BUser(Landroid/content/Context;)V

    .line 119
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->isInLockChecking:Z

    .line 121
    :cond_0
    return-void
.end method

.method private switchAndFinish()V
    .locals 0

    .prologue
    .line 126
    invoke-static {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->switchToB2BUser(Landroid/content/Context;)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->finish()V

    .line 128
    return-void
.end method

.method private static switchToB2BUser(Landroid/content/Context;)V
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 50
    const/4 v5, -0x1

    .line 51
    .local v5, "userid":I
    invoke-static {}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->getKnoxUser1()I

    move-result v5

    .line 53
    const-string v6, "SwitchB2BActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "switch to b2b container "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    if-gtz v5, :cond_0

    .line 56
    const-string v6, "SwitchB2BActivity"

    const-string v7, "switchToB2BUser() userid not found for knox2"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {p0}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->initialize(Landroid/content/Context;)V

    .line 59
    if-gtz v5, :cond_0

    .line 60
    const-string v6, "SwitchB2BActivity"

    const-string v7, "switchToB2BUser() userid not found for knox2"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v4

    .line 67
    .local v4, "status":I
    const-string v6, "SwitchB2BActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "switch to persona "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->isAdminLocked(Landroid/content/Context;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 69
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->startActivityAdminLocked(Landroid/content/Context;I)V

    goto :goto_0

    .line 73
    :cond_1
    const/16 v6, 0x5a

    if-eq v4, v6, :cond_3

    .line 74
    sget-boolean v6, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->isInLockChecking:Z

    if-nez v6, :cond_2

    .line 75
    const/4 v6, 0x1

    sput-boolean v6, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->isInLockChecking:Z

    .line 76
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->sendRequestKeyStatus(Landroid/content/Context;I)V

    goto :goto_0

    .line 80
    :cond_2
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v4

    .line 81
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->startActivity(Landroid/content/Context;I)V

    .line 82
    const-string v6, "SwitchB2BActivity"

    const-string v7, "KLMS status is not valid"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_3
    :try_start_0
    const-string v6, "persona"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    .line 92
    .local v2, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v2, v5}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v3

    .line 93
    .local v3, "pInfo":Landroid/content/pm/PersonaInfo;
    const-string v6, "SwitchB2BActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "PersonaInfo:getType() :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Landroid/content/pm/PersonaInfo;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v2, v5}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v6

    sget-object v7, Landroid/content/pm/PersonaState;->CREATING:Landroid/content/pm/PersonaState;

    invoke-virtual {v6, v7}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 96
    iget-boolean v6, v3, Landroid/content/pm/PersonaInfo;->isLightWeightContainer:Z

    if-eqz v6, :cond_4

    .line 97
    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 98
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "com.sec.knox.knoxsetupwizardclient"

    const-string v7, "com.sec.knox.knoxsetupwizardclient.shortcut.FolderContainer"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string v6, "userId"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 100
    const-string v6, "folderMode"

    const-string v7, "lwc"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const/high16 v6, 0x4000000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 103
    invoke-static {p0, v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->notifyLwcLaunchToKnoxusageMonitorService(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 110
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "mPersona":Landroid/os/PersonaManager;
    .end local v3    # "pInfo":Landroid/content/pm/PersonaInfo;
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "SwitchB2BActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "switch error message : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 105
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "mPersona":Landroid/os/PersonaManager;
    .restart local v3    # "pInfo":Landroid/content/pm/PersonaInfo;
    :cond_4
    :try_start_1
    invoke-virtual {v2, v5}, Landroid/os/PersonaManager;->launchPersonaHome(I)Z

    goto/16 :goto_0

    .line 108
    :cond_5
    const-string v6, "SwitchB2BActivity"

    const-string v7, "KNOX is not yet Created, State is intializing."

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const-string v0, "SwitchB2BActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->connectToMigrationService()V

    .line 46
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->mMigrationConn:Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->mMigrationConn:Lcom/sec/knox/bridge/activity/SwitchB2BActivity$KnoxMigrationServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/knox/bridge/activity/SwitchB2BActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 135
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 136
    return-void
.end method
