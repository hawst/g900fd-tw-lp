.class public Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;
.super Ljava/lang/Object;
.source "UploadPolicy.java"


# static fields
.field private static instance:Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->instance:Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method private static getBatteryLevel(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v1, 0x0

    .line 53
    .local v1, "level":I
    if-eqz p0, :cond_0

    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 55
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 57
    .local v0, "batteryIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 58
    const-string v2, "level"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 62
    .end local v0    # "batteryIntent":Landroid/content/Intent;
    :cond_0
    return v1
.end method

.method public static getInstance()Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->instance:Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;

    invoke-direct {v0}, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;-><init>()V

    sput-object v0, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->instance:Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;

    .line 26
    :cond_0
    sget-object v0, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->instance:Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;

    return-object v0
.end method

.method public static isBatteryOk(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 37
    invoke-static {p0}, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->getBatteryLevel(Landroid/content/Context;)I

    move-result v4

    const/16 v5, 0xf

    if-ge v4, v5, :cond_1

    move v0, v2

    .line 38
    .local v0, "mIsBatteryLow":Z
    :goto_0
    invoke-static {p0}, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->isTAConnected(Landroid/content/Context;)Z

    move-result v1

    .line 39
    .local v1, "mIsChargerConnected":Z
    if-eqz v1, :cond_2

    .line 46
    :cond_0
    :goto_1
    return v2

    .end local v0    # "mIsBatteryLow":Z
    .end local v1    # "mIsChargerConnected":Z
    :cond_1
    move v0, v3

    .line 37
    goto :goto_0

    .line 43
    .restart local v0    # "mIsBatteryLow":Z
    .restart local v1    # "mIsChargerConnected":Z
    :cond_2
    if-eqz v0, :cond_0

    move v2, v3

    .line 44
    goto :goto_1
.end method

.method public static isTAConnected(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 66
    const/4 v1, -0x1

    .line 67
    .local v1, "isConnected":I
    if-eqz p0, :cond_0

    .line 68
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 69
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 71
    .local v0, "batteryIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 72
    const-string v3, "plugged"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 77
    .end local v0    # "batteryIntent":Landroid/content/Intent;
    :cond_0
    if-eq v1, v2, :cond_1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 81
    :cond_1
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isAvailable(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    .line 31
    .local v0, "result":Z
    invoke-static {p1}, Lcom/sec/knox/bridge/uploadmanager/NetworkManager;->isWifiAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/knox/bridge/uploadmanager/UploadPolicy;->isBatteryOk(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUtils;->isProcessible(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const/4 v0, 0x1

    .line 33
    :cond_0
    return v0
.end method
