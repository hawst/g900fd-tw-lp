.class public Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;
.super Ljava/lang/Object;
.source "KnoxUsageUpdateManager.java"


# direct methods
.method private static UpdateEntryInCurrentTable(Landroid/database/Cursor;JJLandroid/content/Context;)V
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "currentTime"    # J
    .param p3, "nextUploadTime"    # J
    .param p5, "mContext"    # Landroid/content/Context;

    .prologue
    .line 72
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 73
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v2, "login_count"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 74
    const-string v2, "track_start_ts"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 75
    const-string v2, "next_upload"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 76
    const-string v2, "persona_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 77
    .local v1, "userId":I
    invoke-static {p5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->updateCurrentTable(Landroid/content/ContentValues;I)V

    .line 78
    return-void
.end method

.method public static addAnEntryForAnUser(Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;Landroid/content/Context;)V
    .locals 6
    .param p0, "knoxUsageInfo"    # Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v1

    .line 37
    .local v1, "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getContainerId()I

    move-result v2

    const-string v3, "currentlogs"

    invoke-virtual {v1, v2, v3}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->delete(ILjava/lang/String;)V

    .line 38
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 39
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v2, "persona_id"

    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getContainerId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 40
    const-string v2, "next_upload"

    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getUploadTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 41
    const-string v2, "login_count"

    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getEntranceCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 42
    const-string v2, "persona_status"

    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getStatus()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 43
    const-string v2, "track_start_ts"

    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;->getStartOfWeek()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 44
    const-string v2, "currentlogs"

    invoke-virtual {v1, v0, v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->insert(Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public static addEntriesForExistingUsers(JLandroid/content/Context;)V
    .locals 12
    .param p0, "next_uploadTime"    # J
    .param p2, "knoxUsageContext"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 127
    const-string v2, "persona"

    invoke-virtual {p2, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PersonaManager;

    .line 128
    .local v9, "mPm":Landroid/os/PersonaManager;
    invoke-virtual {v9, v3}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v11

    .line 129
    .local v11, "personaList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/PersonaInfo;

    .line 130
    .local v10, "personaInfo":Landroid/content/pm/PersonaInfo;
    invoke-virtual {v10}, Landroid/content/pm/PersonaInfo;->getId()I

    move-result v1

    .line 131
    .local v1, "personaId":I
    new-instance v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-wide v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;-><init>(IIIJJ)V

    .line 132
    .local v0, "knoxUsageInfo":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    invoke-static {v0, p2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;->addAnEntryForAnUser(Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;Landroid/content/Context;)V

    goto :goto_0

    .line 134
    .end local v0    # "knoxUsageInfo":Lcom/sec/knox/bridge/knoxusage/KnoxUsageInfo;
    .end local v1    # "personaId":I
    .end local v10    # "personaInfo":Landroid/content/pm/PersonaInfo;
    :cond_0
    return-void
.end method

.method private static addEntryInUploadsTable(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 81
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 82
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v1, "persona_id"

    const-string v2, "persona_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 86
    const-string v1, "next_upload"

    const-string v2, "next_upload"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 90
    const-string v1, "login_count"

    const-string v2, "login_count"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 94
    const-string v1, "persona_status"

    const-string v2, "persona_status"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 98
    const-string v1, "track_start_ts"

    const-string v2, "track_start_ts"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 102
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v1

    const-string v2, "uploads"

    invoke-virtual {v1, v0, v2}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->insert(Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public static moveDataFromCurrentToUploadTable(Landroid/content/Context;)V
    .locals 9
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v0

    .line 49
    .local v0, "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    const-string v6, "currentlogs"

    invoke-virtual {v0, v6}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getAllEntries(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 50
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_1

    .line 51
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_1

    .line 52
    const-string v6, "KnoxUsageLogPro"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getPendingUploadList size = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 55
    .local v2, "currentTime":J
    const-wide/32 v6, 0x240c8400

    add-long v4, v2, v6

    .line 56
    .local v4, "nextUploadTime":J
    const-string v6, "KnoxUsageLogPro"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "currentTime="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const-string v6, "KnoxUsageLogPro"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "nextUploadTime="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    invoke-static {v1, p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;->addEntryInUploadsTable(Landroid/database/Cursor;Landroid/content/Context;)V

    move-object v6, p0

    .line 63
    invoke-static/range {v1 .. v6}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageUpdateManager;->UpdateEntryInCurrentTable(Landroid/database/Cursor;JJLandroid/content/Context;)V

    .line 64
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 67
    .end local v2    # "currentTime":J
    .end local v4    # "nextUploadTime":J
    :cond_1
    return-void
.end method

.method public static moveDataOfDeletedPersona(ILandroid/content/Context;)V
    .locals 6
    .param p0, "personaId"    # I
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v1

    .line 108
    .local v1, "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    const-string v3, "currentlogs"

    invoke-virtual {v1, p0, v3}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->query(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 109
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 110
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 111
    const-string v3, "KnoxUsageLogPro"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moveDataOfDeletedPersona size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 113
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 114
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {v0, v2}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 116
    const-string v3, "persona_status"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 117
    const-string v3, "uploads"

    invoke-virtual {v1, v2, v3}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->insert(Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 120
    const-string v3, "currentlogs"

    invoke-virtual {v1, p0, v3}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->delete(ILjava/lang/String;)V

    .line 123
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method public static updateLoginCountForPersona(ILandroid/content/Context;)V
    .locals 7
    .param p0, "userId"    # I
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 18
    const-string v4, "KnoxUsageLogPro"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateTimeForUser: userId ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    invoke-static {p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v2

    .line 20
    .local v2, "databaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    const-string v4, "currentlogs"

    invoke-virtual {v2, p0, v4}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->query(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 21
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_1

    .line 22
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 23
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 24
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 25
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v4, "login_count"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 26
    .local v3, "loginCount":I
    const-string v4, "KnoxUsageLogPro"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateTimeForUser: userId ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " login count="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    const-string v4, "login_count"

    add-int/lit8 v5, v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 28
    invoke-virtual {v2, v0, p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->updateCurrentTable(Landroid/content/ContentValues;I)V

    .line 30
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v3    # "loginCount":I
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 33
    :cond_1
    return-void
.end method
