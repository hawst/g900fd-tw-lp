.class public Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KnoxNotificationReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;
    }
.end annotation


# static fields
.field public static isInLockChecking:Z

.field private static mPersona:Landroid/os/PersonaManager;

.field private static mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

.field static miUserIdForTrySwitch:I


# instance fields
.field private handlerThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    sput-boolean v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->isInLockChecking:Z

    .line 53
    sput v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->miUserIdForTrySwitch:I

    .line 55
    sput-object v1, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    .line 56
    sput-object v1, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handlerThread:Landroid/os/HandlerThread;

    .line 232
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleSwitchAction(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleSwitchLockAction(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleLockAction(Landroid/content/Context;I)V

    return-void
.end method

.method private getParentUser()I
    .locals 2

    .prologue
    .line 306
    sget-object v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    sget-object v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v0}, Landroid/os/PersonaManager;->getParentUserForCurrentPersona()I

    move-result v0

    .line 309
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    goto :goto_0
.end method

.method private handleLockAction(Landroid/content/Context;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "personaId"    # I

    .prologue
    const/4 v7, 0x0

    .line 193
    sget-object v4, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v4, p2}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v1

    .line 195
    .local v1, "pi":Landroid/content/pm/PersonaInfo;
    if-eqz v1, :cond_0

    .line 209
    const-string v4, "user"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 210
    .local v3, "um":Landroid/os/UserManager;
    iget v4, v1, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v3, v4}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v2

    .line 211
    .local v2, "ui":Landroid/content/pm/UserInfo;
    const v4, 0x7f06002a

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v2, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "message":Ljava/lang/String;
    sget-boolean v4, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->isDarkTheme:Z

    if-nez v4, :cond_1

    .line 213
    new-instance v4, Landroid/view/ContextThemeWrapper;

    const v5, 0x103012b

    invoke-direct {v4, p1, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 224
    .end local v0    # "message":Ljava/lang/String;
    .end local v2    # "ui":Landroid/content/pm/UserInfo;
    .end local v3    # "um":Landroid/os/UserManager;
    :cond_0
    :goto_0
    sget-object v4, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v4, p2}, Landroid/os/PersonaManager;->lockPersona(I)V

    .line 225
    return-void

    .line 217
    .restart local v0    # "message":Ljava/lang/String;
    .restart local v2    # "ui":Landroid/content/pm/UserInfo;
    .restart local v3    # "um":Landroid/os/UserManager;
    :cond_1
    new-instance v4, Landroid/view/ContextThemeWrapper;

    const v5, 0x1030128

    invoke-direct {v4, p1, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private static handleSwitchAction(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "personaId"    # I

    .prologue
    .line 120
    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->switchPersona(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 124
    :cond_0
    sget-object v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    if-eqz v0, :cond_1

    .line 126
    sget-object v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v0, p1}, Landroid/os/PersonaManager;->launchPersonaHome(I)Z

    .line 128
    :cond_1
    if-nez p1, :cond_2

    .line 129
    const-string v0, "dev.knoxapp.running"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_2
    const-string v0, "dev.knoxapp.running"

    const-string v1, "true"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleSwitchLockAction(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "personaId"    # I

    .prologue
    .line 188
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleLockAction(Landroid/content/Context;I)V

    .line 189
    invoke-direct {p0}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->getParentUser()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleSwitchAction(Landroid/content/Context;I)V

    .line 190
    return-void
.end method

.method public static processAfterKeyCheck(Landroid/content/Context;)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 179
    const-string v0, "KnoxNotificationReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processAfterKeyCheck "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->isInLockChecking:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->miUserIdForTrySwitch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    sget-boolean v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->isInLockChecking:Z

    if-eqz v0, :cond_0

    .line 182
    sget v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->miUserIdForTrySwitch:I

    invoke-static {p0, v0}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handleSwitchAction(Landroid/content/Context;I)V

    .line 183
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->isInLockChecking:Z

    .line 185
    :cond_0
    return-void
.end method

.method private static switchPersona(Landroid/content/Context;I)Z
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "userid"    # I

    .prologue
    const/16 v6, 0x5a

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 139
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v0

    .line 141
    .local v0, "status":I
    const-string v3, "KnoxNotificationReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switch to persona "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->isAdminLocked(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 144
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->startActivityAdminLocked(Landroid/content/Context;I)V

    .line 174
    :goto_0
    return v1

    .line 148
    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 149
    goto :goto_0

    .line 152
    :cond_1
    if-eq v0, v6, :cond_3

    .line 154
    sget-boolean v3, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->isInLockChecking:Z

    if-nez v3, :cond_2

    .line 155
    sput-boolean v2, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->isInLockChecking:Z

    .line 156
    sput p1, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->miUserIdForTrySwitch:I

    .line 158
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->sendRequestKeyStatus(Landroid/content/Context;I)V

    goto :goto_0

    .line 163
    :cond_2
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v0

    .line 165
    if-eq v0, v6, :cond_3

    .line 166
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->startActivity(Landroid/content/Context;I)V

    .line 168
    const-string v2, "KnoxNotificationReceiver"

    const-string v3, "KLMS status is not valid"

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 174
    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 61
    if-eqz p2, :cond_0

    .line 62
    const-string v6, "persona"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PersonaManager;

    sput-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mPersona:Landroid/os/PersonaManager;

    .line 63
    new-instance v6, Landroid/os/HandlerThread;

    const-string v7, "PersonaManagerService"

    const/16 v8, 0xa

    invoke-direct {v6, v7, v8}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v6, p0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handlerThread:Landroid/os/HandlerThread;

    .line 65
    iget-object v6, p0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->start()V

    .line 66
    new-instance v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    iget-object v7, p0, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, p0, v7}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;-><init>(Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;Landroid/os/Looper;)V

    sput-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    .line 68
    const-string v6, "personaId"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 70
    .local v4, "personaId":I
    const-string v6, "com.sec.knox.bridge.receiver.INTENT_SWITCH_MODE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 71
    const-string v6, "KnoxNotificationReceiver"

    const-string v7, "calling MSG_START_PERSONA_SWITCH handler"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    sget-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    invoke-virtual {v6, v10}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 73
    .local v3, "msg":Landroid/os/Message;
    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 74
    iput-object p1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 75
    sget-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    invoke-virtual {v6, v3}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->sendMessage(Landroid/os/Message;)Z

    .line 117
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "personaId":I
    :cond_0
    :goto_0
    return-void

    .line 76
    .restart local v4    # "personaId":I
    :cond_1
    const-string v6, "com.sec.knox.bridge.receiver.INTENT_SWITCH_LOCK"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 77
    const-string v6, "KnoxNotificationReceiver"

    const-string v7, "INTENT_SWITCH_LOCK is called"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v6, "KnoxNotificationReceiver"

    const-string v7, "calling MSG_START_PERSONA_SWITCH_LOCK handler"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 80
    .restart local v3    # "msg":Landroid/os/Message;
    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 81
    iput-object p1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 82
    sget-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    invoke-virtual {v6, v3}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 83
    .end local v3    # "msg":Landroid/os/Message;
    :cond_2
    const-string v6, "com.sec.android.knox.restrics"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 84
    const-string v6, "KNOX policy restrics the use of this action."

    invoke-static {p1, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 86
    :cond_3
    const-string v6, "com.sec.knox.bridge.receiver.INTENT_LOCK_IMMEDIATLY"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 87
    const-string v6, "KnoxNotificationReceiver"

    const-string v7, "ACTION_LOCK_IMMEDIATLY is called"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v6, "KnoxNotificationReceiver"

    const-string v7, "calling MSG_START_PERSONA_LOCK handler"

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v6, "activity"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    .line 93
    .local v2, "mActivityManager":Landroid/app/ActivityManager;
    if-eqz v2, :cond_4

    .line 94
    invoke-virtual {v2, v10}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v5

    .line 96
    .local v5, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 97
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v0, v6, Landroid/app/ActivityManager$RunningTaskInfo;->userId:I

    .line 98
    .local v0, "id":I
    if-ne v0, v4, :cond_4

    .line 99
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 100
    .local v1, "in":Landroid/content/Intent;
    const-string v6, "android.intent.action.MAIN"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v6, "android.intent.category.HOME"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const/high16 v6, 0x10000000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 103
    new-instance v6, Landroid/os/UserHandle;

    new-instance v7, Lcom/sec/knox/bridge/util/NotificationUtil;

    invoke-direct {v7, p1}, Lcom/sec/knox/bridge/util/NotificationUtil;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/sec/knox/bridge/util/NotificationUtil;->getCurrentUserId()I

    move-result v7

    invoke-direct {v6, v7}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p1, v1, v6}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 111
    .end local v0    # "id":I
    .end local v1    # "in":Landroid/content/Intent;
    .end local v5    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_4
    sget-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 112
    .restart local v3    # "msg":Landroid/os/Message;
    iput v4, v3, Landroid/os/Message;->arg1:I

    .line 113
    iput-object p1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 114
    sget-object v6, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver;->mSwitchHandler:Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;

    invoke-virtual {v6, v3}, Lcom/sec/knox/bridge/receiver/KnoxNotificationReceiver$KnoxSwitcherHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
