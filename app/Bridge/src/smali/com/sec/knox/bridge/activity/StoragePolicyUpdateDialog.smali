.class public Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog;
.super Landroid/app/Activity;
.source "StoragePolicyUpdateDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v5, 0x7f060045

    invoke-virtual {p0, v5}, Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 25
    .local v3, "message":Ljava/lang/String;
    const v5, 0x7f060044

    invoke-virtual {p0, v5}, Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 27
    .local v4, "title":Ljava/lang/String;
    const v2, 0x103012b

    .line 32
    .local v2, "dialogTheme":I
    const v2, 0x103012b

    .line 35
    new-instance v1, Landroid/app/AlertDialog$Builder;

    new-instance v5, Landroid/view/ContextThemeWrapper;

    invoke-direct {v5, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    .local v1, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 39
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x104000a

    invoke-virtual {p0, v6}, Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog$1;

    invoke-direct {v7, p0}, Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog$1;-><init>(Lcom/sec/knox/bridge/activity/StoragePolicyUpdateDialog;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 51
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 53
    return-void
.end method
