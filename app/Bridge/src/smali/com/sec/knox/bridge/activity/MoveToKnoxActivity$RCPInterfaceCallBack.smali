.class Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;
.super Landroid/content/IRCPInterfaceCallback$Stub;
.source "MoveToKnoxActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RCPInterfaceCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;


# direct methods
.method private constructor <init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V
    .locals 0

    .prologue
    .line 415
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-direct {p0}, Landroid/content/IRCPInterfaceCallback$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;
    .param p2, "x1"    # Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$1;

    .prologue
    .line 415
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;-><init>(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/util/List;II)V
    .locals 6
    .param p2, "destinationUserId"    # I
    .param p3, "successCnt"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .local p1, "srcPathsOrig":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v4, 0x32

    .line 419
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RCPInterfaceCallBack.onComplete successCnt = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # invokes: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->getSuccessMsg()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v0

    .line 421
    .local v0, "successMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mIsMoveFailed:Z
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 422
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 423
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 425
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 429
    :cond_0
    const-wide/16 v2, 0x12c

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 430
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCAN"

    const-string v4, "file:///mnt/sdcard"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    new-instance v3, Landroid/os/UserHandle;

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToSrcCId:I
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1500(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 438
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    invoke-virtual {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->finish()V

    .line 439
    return-void
.end method

.method public onDone(Ljava/lang/String;I)V
    .locals 6
    .param p1, "srcPathsOrig"    # Ljava/lang/String;
    .param p2, "destinationUserId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RCPInterfaceCallBack.onDone destinationUserId = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # operator++ for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$308(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    .line 446
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RCPInterfaceCallBack.onDone currentCnt = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    # setter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I
    invoke-static {v0, v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1602(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;I)I

    .line 448
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RCPInterfaceCallBack.onDone mCurrentProgress = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1600(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1600(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 452
    return-void
.end method

.method public onFail(Ljava/lang/String;II)V
    .locals 6
    .param p1, "srcPathsOrig"    # Ljava/lang/String;
    .param p2, "destinationUserId"    # I
    .param p3, "errorCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 457
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RCPInterfaceCallBack.onFail errorCode = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    const/4 v2, 0x1

    # setter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mIsMoveFailed:Z
    invoke-static {v1, v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1402(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;Z)Z

    .line 459
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$800(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/content/IRCPInterface;

    move-result-object v1

    if-nez v1, :cond_0

    .line 460
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "RCPInterfaceCallBack.onFail mRcpProxy is null"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "This means MoveToKnoxActivity is already destroyed & onFail callback is already handled. So just ignore this request"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :goto_0
    return-void

    .line 465
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mRcpProxy:Landroid/content/IRCPInterface;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$800(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/content/IRCPInterface;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/content/IRCPInterface;->getErrorMessage(I)Ljava/lang/String;

    move-result-object v0

    .line 466
    .local v0, "errMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 467
    iget-object v1, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public onProgress(Ljava/lang/String;II)V
    .locals 9
    .param p1, "srcPathsOrig"    # Ljava/lang/String;
    .param p2, "destinationUserId"    # I
    .param p3, "progress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 474
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RCPInterfaceCallBack.onProgress progress = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$100(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 477
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 479
    :cond_0
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToCurrnetCnt:I
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$300(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v0, v3

    .line 480
    .local v0, "temp1":I
    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mMoveToTotalCnt:I
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$400(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    int-to-float v4, p3

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 481
    .local v1, "temp2":I
    add-int v2, v0, v1

    .line 482
    .local v2, "tempProgress":I
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$000(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RCPInterfaceCallBack.onProgress tempProgress = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1600(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v3

    if-le v2, v3, :cond_1

    .line 484
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # setter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I
    invoke-static {v3, v2}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1602(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;I)I

    .line 485
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$200(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity$RCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;

    # getter for: Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->mCurrentProgress:I
    invoke-static {v6}, Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;->access$1600(Lcom/sec/knox/bridge/activity/MoveToKnoxActivity;)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v8, v7}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 488
    :cond_1
    return-void
.end method
