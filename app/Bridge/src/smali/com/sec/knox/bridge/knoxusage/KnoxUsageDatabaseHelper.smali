.class public Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "KnoxUsageDatabaseHelper.java"


# static fields
.field private static dbHelperInstance:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->dbHelperInstance:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const-string v0, "knoxusage.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 37
    return-void
.end method

.method private createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getCurrentLogTableUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getUploadTableUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public static getCurrentLogTableUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "CREATE TABLE IF NOT EXISTS currentlogs (_id INTEGER PRIMARY KEY AUTOINCREMENT,persona_id INTEGER,track_start_ts INTEGER,persona_status INTEGER,login_count INTEGER,next_upload INTEGER);"

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "getInstance - KnoxUsageDatabaseHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    sget-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->dbHelperInstance:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    if-nez v0, :cond_0

    .line 30
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "null == dbHelperInstance - KnoxUsageDatabaseHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    new-instance v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->dbHelperInstance:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    .line 33
    :cond_0
    sget-object v0, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->dbHelperInstance:Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    return-object v0
.end method

.method public static getUploadTableUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "CREATE TABLE IF NOT EXISTS uploads (_id INTEGER PRIMARY KEY AUTOINCREMENT,persona_id INTEGER,track_start_ts INTEGER,persona_status INTEGER,login_count INTEGER,next_upload INTEGER);"

    return-object v0
.end method


# virtual methods
.method public cleartable(Ljava/lang/String;)V
    .locals 2
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 108
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0, p1, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 109
    return-void
.end method

.method public delete(ILjava/lang/String;)V
    .locals 4
    .param p1, "personaId"    # I
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 118
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "persona_id= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "selection":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 120
    return-void
.end method

.method public getAllEntries(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "tablename"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 113
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 114
    .local v8, "c":Landroid/database/Cursor;
    return-object v8
.end method

.method public insert(Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 4
    .param p1, "contentValues"    # Landroid/content/ContentValues;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 87
    const-string v1, "KnoxUsageLogPro"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insert: in table "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " values ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 89
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 90
    return-void
.end method

.method public isUploadTableEmpty()Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 128
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 129
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "uploads"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 130
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 131
    const/4 v1, 0x0

    .line 133
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 41
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onCreate - KnoxUsageDatabaseHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 43
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 83
    const-string v0, "KnoxUsageLogPro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Downgrading database from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 74
    const-string v0, "KnoxUsageLogPro"

    const-string v1, "onUpgrade - KnoxUsageDatabaseHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string v0, "DROP TABLE IF EXISTS currentlogs;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 76
    const-string v0, "DROP TABLE IF EXISTS uploads;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 79
    return-void
.end method

.method public query(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "personaId"    # I
    .param p2, "tablename"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 101
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "persona_id= "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 102
    .local v3, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "c":Landroid/database/Cursor;
    move-object v1, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 103
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 104
    return-object v8
.end method

.method public updateCurrentTable(Landroid/content/ContentValues;I)V
    .locals 5
    .param p1, "contentValues"    # Landroid/content/ContentValues;
    .param p2, "personaId"    # I

    .prologue
    .line 93
    const-string v2, "KnoxUsageLogPro"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCurrentTable: contentValues ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "persona_id= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 96
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "currentlogs"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 97
    return-void
.end method

.method public updateUploadFrequencyInCurrentTable(J)V
    .locals 5
    .param p1, "uploadTime"    # J

    .prologue
    const/4 v4, 0x0

    .line 122
    invoke-virtual {p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 123
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 124
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "next_upload"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 125
    const-string v2, "currentlogs"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 126
    return-void
.end method
