.class public Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;
.super Ljava/lang/Thread;
.source "BridgeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "FileOperationTask"
.end annotation


# instance fields
.field TotalFileSize:J

.field TotalMoveSize:J

.field mCallback:Landroid/content/IRCPInterfaceCallback;

.field mDestContainerId:I

.field mDestFilePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsCanceled:Z

.field private mIsMoveOperation:Z

.field private mSessionId:J

.field mSrcContainerId:I

.field mSrcFilePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;ILjava/util/List;ILjava/util/List;ZLandroid/content/IRCPInterfaceCallback;)V
    .locals 6
    .param p2, "srcContainerId"    # I
    .param p4, "destContainerId"    # I
    .param p6, "isMoveOperation"    # Z
    .param p7, "callback"    # Landroid/content/IRCPInterfaceCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Landroid/content/IRCPInterfaceCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "srcFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "destFilePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2000
    iput-object p1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1983
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mIsMoveOperation:Z

    .line 1985
    iput-boolean v1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mIsCanceled:Z

    .line 1987
    iput-wide v4, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSessionId:J

    .line 1989
    iput-wide v4, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    .line 1992
    iput v1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcContainerId:I

    .line 1993
    iput-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    .line 1994
    iput v1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestContainerId:I

    .line 1995
    iput-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestFilePaths:Ljava/util/ArrayList;

    .line 1996
    iput-object v2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mCallback:Landroid/content/IRCPInterfaceCallback;

    .line 2001
    iput p2, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcContainerId:I

    .line 2002
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    .line 2003
    iput p4, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestContainerId:I

    .line 2004
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestFilePaths:Ljava/util/ArrayList;

    .line 2005
    iput-boolean p6, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mIsMoveOperation:Z

    .line 2006
    iput-object p7, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mCallback:Landroid/content/IRCPInterfaceCallback;

    .line 2007
    return-void
.end method

.method private fileOperation(ILjava/lang/String;ILjava/lang/String;ZLandroid/content/IRCPInterfaceCallback;)Z
    .locals 31
    .param p1, "srcContainerId"    # I
    .param p2, "srcFilePath"    # Ljava/lang/String;
    .param p3, "destContainerId"    # I
    .param p4, "destFilePath"    # Ljava/lang/String;
    .param p5, "isMoveOperation"    # Z
    .param p6, "callback"    # Landroid/content/IRCPInterfaceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2086
    const/16 v24, 0x0

    .line 2088
    .local v24, "retCode":I
    if-nez v24, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    move/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->isContainerIdIsValid(II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2089
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "fileOperation() containerID is not valid"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2090
    const/16 v24, -0x2

    .line 2093
    :cond_0
    if-nez v24, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    move/from16 v0, p3

    # invokes: Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->isContainerStateIsValid(I)Z
    invoke-static {v4, v0}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->access$2300(Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2094
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "fileOperation() container state is not valid"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2097
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    iget-object v4, v4, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    const-string v5, "user"

    invoke-virtual {v4, v5}, Lcom/sec/knox/bridge/BridgeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Landroid/os/UserManager;

    .line 2098
    .local v30, "um":Landroid/os/UserManager;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    iget-object v4, v4, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    iget-object v4, v4, Lcom/sec/knox/bridge/BridgeService;->mPm:Landroid/os/PersonaManager;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/PersonaInfo;->id:I

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v29

    .line 2099
    .local v29, "ui":Landroid/content/pm/UserInfo;
    move-object/from16 v0, v29

    iget-object v4, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    sput-object v4, Lcom/sec/knox/bridge/BridgeService;->mCurrentInvalidKnoxName:Ljava/lang/String;

    .line 2101
    const v24, -0xf4241

    .line 2104
    .end local v29    # "ui":Landroid/content/pm/UserInfo;
    .end local v30    # "um":Landroid/os/UserManager;
    :cond_1
    if-nez v24, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    move/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->isMoveFilesAllowed(II)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2105
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "fileOperation() policy is denied"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2106
    const v24, -0xf4240

    .line 2109
    :cond_2
    if-nez v24, :cond_9

    .line 2110
    new-instance v26, Ljava/io/File;

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2113
    .local v26, "srcFile":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2114
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2115
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " remove dir success: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2116
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-interface {v0, v1, v2}, Landroid/content/IRCPInterfaceCallback;->onDone(Ljava/lang/String;I)V

    .line 2122
    :goto_0
    const/16 v20, 0x0

    .line 2237
    .end local v26    # "srcFile":Ljava/io/File;
    :goto_1
    return v20

    .line 2118
    .restart local v26    # "srcFile":Ljava/io/File;
    :cond_3
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " remove dir failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    const/4 v4, 0x3

    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-interface {v0, v1, v2, v4}, Landroid/content/IRCPInterfaceCallback;->onFail(Ljava/lang/String;II)V

    goto :goto_0

    .line 2125
    :cond_4
    const-string v4, "/"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v21

    .line 2126
    .local v21, "lastindex":I
    const/4 v4, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v28

    .line 2127
    .local v28, "srcFolderPath":Ljava/lang/String;
    add-int/lit8 v4, v21, 0x1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v27

    .line 2128
    .local v27, "srcFileName":Ljava/lang/String;
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " srcFolderPath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; srcFileName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2131
    invoke-static/range {v27 .. v27}, Lcom/sec/knox/bridge/util/FileUtils;->isAllowedFileName(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2132
    const/4 v4, 0x5

    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-interface {v0, v1, v2, v4}, Landroid/content/IRCPInterfaceCallback;->onFail(Ljava/lang/String;II)V

    .line 2134
    const/16 v20, 0x0

    goto :goto_1

    .line 2137
    :cond_5
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_6

    .line 2138
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " srcFile is not Exist!! File Move Fail!! : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2139
    const/4 v4, 0x6

    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-interface {v0, v1, v2, v4}, Landroid/content/IRCPInterfaceCallback;->onFail(Ljava/lang/String;II)V

    .line 2141
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 2143
    :cond_6
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->length()J

    move-result-wide v16

    .line 2146
    .local v16, "currentFileSize":J
    const v11, 0xc8000

    .line 2147
    .local v11, "bufferSize":I
    const-wide/16 v9, 0x0

    .line 2148
    .local v9, "offset":J
    const/16 v25, 0x0

    .line 2150
    .local v25, "sendcnt":I
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalMoveSize:J

    move-wide/from16 v18, v0

    .line 2152
    .local v18, "currentTotalMoveSize":J
    :cond_7
    :goto_2
    cmp-long v4, v9, v16

    if-gtz v4, :cond_8

    .line 2155
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-eqz v4, :cond_c

    cmp-long v4, v9, v16

    if-nez v4, :cond_c

    .line 2156
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file moving is already done in empd. stop moving now"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2203
    .end local v18    # "currentTotalMoveSize":J
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalMoveSize:J

    add-long v4, v4, v16

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalMoveSize:J

    .line 2206
    .end local v9    # "offset":J
    .end local v11    # "bufferSize":I
    .end local v16    # "currentFileSize":J
    .end local v21    # "lastindex":I
    .end local v25    # "sendcnt":I
    .end local v26    # "srcFile":Ljava/io/File;
    .end local v27    # "srcFileName":Ljava/lang/String;
    .end local v28    # "srcFolderPath":Ljava/lang/String;
    :cond_9
    if-nez v24, :cond_10

    .line 2207
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_b

    .line 2208
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalMoveSize:J

    long-to-double v4, v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v22, v0

    .line 2209
    .local v22, "percentage":I
    const/16 v4, 0x64

    move/from16 v0, v22

    if-le v0, v4, :cond_a

    .line 2210
    const/16 v22, 0x64

    .line 2212
    :cond_a
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "End...  TotalMoveSize / TotalFileSize : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalMoveSize:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " percentage : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " %"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, v22

    invoke-interface {v0, v1, v2, v3}, Landroid/content/IRCPInterfaceCallback;->onProgress(Ljava/lang/String;II)V

    .line 2227
    .end local v22    # "percentage":I
    :cond_b
    :goto_4
    if-nez v24, :cond_11

    const/16 v20, 0x1

    .line 2228
    .local v20, "isSuccess":Z
    :goto_5
    if-eqz v20, :cond_12

    .line 2229
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-interface {v0, v1, v2}, Landroid/content/IRCPInterfaceCallback;->onDone(Ljava/lang/String;I)V

    .line 2234
    :goto_6
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v20, :cond_13

    const-string v4, "File Move Success"

    :goto_7
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "; retCode="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "; srcPath="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2159
    .end local v20    # "isSuccess":Z
    .restart local v9    # "offset":J
    .restart local v11    # "bufferSize":I
    .restart local v16    # "currentFileSize":J
    .restart local v18    # "currentTotalMoveSize":J
    .restart local v21    # "lastindex":I
    .restart local v25    # "sendcnt":I
    .restart local v26    # "srcFile":Ljava/io/File;
    .restart local v27    # "srcFileName":Ljava/lang/String;
    .restart local v28    # "srcFolderPath":Ljava/lang/String;
    :cond_c
    :try_start_1
    sget-object v4, Lcom/sec/knox/bridge/BridgeService;->mbridge:Landroid/os/RCPManager;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSessionId:J

    move/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v14, p5

    invoke-virtual/range {v4 .. v14}, Landroid/os/RCPManager;->copyChunks(ILjava/lang/String;ILjava/lang/String;JIJZ)I

    move-result v24

    .line 2163
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fileOperation() : retCode from epmd = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2165
    if-eqz v24, :cond_d

    .line 2166
    invoke-static/range {v24 .. v24}, Lcom/sec/knox/bridge/util/FileUtils;->getErrorCodeMatch(I)I

    move-result v24

    .line 2167
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "From moveFiles():fileOperation(): retCode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; message="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    move/from16 v0, v24

    invoke-virtual {v6, v0}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->getErrorMessage(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_3

    .line 2194
    .end local v18    # "currentTotalMoveSize":J
    :catch_0
    move-exception v23

    .line 2195
    .local v23, "re":Landroid/os/RemoteException;
    const/16 v24, 0xa

    .line 2196
    invoke-virtual/range {v23 .. v23}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2197
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File move fail with RemoteException for: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2172
    .end local v23    # "re":Landroid/os/RemoteException;
    .restart local v18    # "currentTotalMoveSize":J
    :cond_d
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mIsCanceled:Z

    if-eqz v4, :cond_e

    .line 2173
    const/16 v24, 0x2

    .line 2174
    goto/16 :goto_3

    .line 2177
    :cond_e
    int-to-long v4, v11

    add-long/2addr v9, v4

    .line 2179
    add-int/lit8 v25, v25, 0x1

    .line 2181
    int-to-long v4, v11

    add-long v18, v18, v4

    .line 2183
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_7

    .line 2184
    move-wide/from16 v0, v18

    long-to-double v4, v0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v22, v0

    .line 2185
    .restart local v22    # "percentage":I
    const/16 v4, 0x64

    move/from16 v0, v22

    if-le v0, v4, :cond_f

    .line 2186
    const/16 v22, 0x64

    .line 2191
    :cond_f
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, v22

    invoke-interface {v0, v1, v2, v3}, Landroid/content/IRCPInterfaceCallback;->onProgress(Ljava/lang/String;II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 2198
    .end local v18    # "currentTotalMoveSize":J
    .end local v22    # "percentage":I
    :catch_1
    move-exception v15

    .line 2199
    .local v15, "e":Ljava/lang/Exception;
    const/16 v24, 0x7

    .line 2200
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    .line 2201
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File move fail with Exception for: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2218
    .end local v9    # "offset":J
    .end local v11    # "bufferSize":I
    .end local v15    # "e":Ljava/lang/Exception;
    .end local v16    # "currentFileSize":J
    .end local v21    # "lastindex":I
    .end local v25    # "sendcnt":I
    .end local v26    # "srcFile":Ljava/io/File;
    .end local v27    # "srcFileName":Ljava/lang/String;
    .end local v28    # "srcFolderPath":Ljava/lang/String;
    :cond_10
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSessionId:J

    invoke-virtual {v4, v6, v7}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->cancelCopyChunks(J)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_4

    .line 2219
    :catch_2
    move-exception v23

    .line 2221
    .restart local v23    # "re":Landroid/os/RemoteException;
    invoke-virtual/range {v23 .. v23}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2222
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fileMove() cancelCopyChunks fail with RemoteException for: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2227
    .end local v23    # "re":Landroid/os/RemoteException;
    :cond_11
    const/16 v20, 0x0

    goto/16 :goto_5

    .line 2231
    .restart local v20    # "isSuccess":Z
    :cond_12
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, v24

    invoke-interface {v0, v1, v2, v3}, Landroid/content/IRCPInterfaceCallback;->onFail(Ljava/lang/String;II)V

    goto/16 :goto_6

    .line 2234
    :cond_13
    const-string v4, "File Move Failed"

    goto/16 :goto_7
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 2010
    const-string v0, "cancel"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2011
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mIsCanceled:Z

    .line 2012
    return-void
.end method

.method public run()V
    .locals 14

    .prologue
    .line 2020
    const/4 v11, 0x0

    .line 2023
    .local v11, "successCnt":I
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2024
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestFilePaths:Ljava/util/ArrayList;

    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2026
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 2027
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2028
    .local v2, "srcPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2029
    .local v4, "destPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    # invokes: Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->changeExtSdPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v2, v4}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->access$2200(Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2031
    .local v7, "chDestPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v9, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2026
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 2034
    .end local v2    # "srcPath":Ljava/lang/String;
    .end local v4    # "destPath":Ljava/lang/String;
    .end local v7    # "chDestPath":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/sec/knox/bridge/util/FileUtils;->getTotalFileSize(Ljava/util/ArrayList;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    .line 2036
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalMoveSize:J

    .line 2037
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " TotalFileSize : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v12, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->TotalFileSize:J

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039
    const/4 v9, 0x0

    :goto_1
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_2

    .line 2040
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2041
    .restart local v2    # "srcPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestFilePaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2043
    .restart local v4    # "destPath":Ljava/lang/String;
    iget-boolean v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mIsCanceled:Z

    if-eqz v0, :cond_1

    .line 2044
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    const-string v1, " process cancel intent: in \'for\' loop "

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 2049
    :cond_1
    :try_start_0
    iget v1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcContainerId:I

    iget v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestContainerId:I

    iget-boolean v5, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mIsMoveOperation:Z

    iget-object v6, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mCallback:Landroid/content/IRCPInterfaceCallback;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->fileOperation(ILjava/lang/String;ILjava/lang/String;ZLandroid/content/IRCPInterfaceCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_2

    .line 2051
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 2055
    :catch_0
    move-exception v10

    .line 2056
    .local v10, "re":Landroid/os/RemoteException;
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 2061
    .end local v2    # "srcPath":Ljava/lang/String;
    .end local v4    # "destPath":Ljava/lang/String;
    .end local v10    # "re":Landroid/os/RemoteException;
    :cond_2
    # getter for: Lcom/sec/knox/bridge/BridgeService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/bridge/BridgeService;->access$1500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " onComplete : successCnt : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2063
    const-wide/16 v0, 0x3e8

    :try_start_1
    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    .line 2064
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mCallback:Landroid/content/IRCPInterfaceCallback;

    iget-object v1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSrcFilePaths:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestContainerId:I

    invoke-interface {v0, v1, v3, v11}, Landroid/content/IRCPInterfaceCallback;->onComplete(Ljava/util/List;II)V

    .line 2067
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCAN"

    const-string v5, "file:///mnt/sdcard"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    new-instance v3, Landroid/os/UserHandle;

    iget v5, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mDestContainerId:I

    invoke-direct {v3, v5}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/knox/bridge/BridgeService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2076
    :goto_3
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 2077
    :try_start_2
    iget-object v0, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->this$1:Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;

    iget-object v0, v0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack;->this$0:Lcom/sec/knox/bridge/BridgeService;

    # getter for: Lcom/sec/knox/bridge/BridgeService;->mThreadMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$2100(Lcom/sec/knox/bridge/BridgeService;)Ljava/util/HashMap;

    move-result-object v0

    iget-wide v12, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSessionId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2078
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2079
    return-void

    .line 2071
    :catch_1
    move-exception v8

    .line 2073
    .local v8, "e":Landroid/os/RemoteException;
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 2078
    .end local v8    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public setSessionId(J)V
    .locals 1
    .param p1, "sessionId"    # J

    .prologue
    .line 2015
    const-string v0, "setSessionId"

    # invokes: Lcom/sec/knox/bridge/BridgeService;->checkCallerPermissionFor(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/sec/knox/bridge/BridgeService;->access$1300(Ljava/lang/String;)I

    .line 2016
    iput-wide p1, p0, Lcom/sec/knox/bridge/BridgeService$IRCPInterfaceCallBack$FileOperationTask;->mSessionId:J

    .line 2017
    return-void
.end method
