.class public Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ServiceStartReceiver.java"


# instance fields
.field private mPersona:Landroid/os/PersonaManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;->mPersona:Landroid/os/PersonaManager;

    return-void
.end method

.method private getParentUser(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;->getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;

    move-result-object v0

    .line 70
    .local v0, "pm":Landroid/os/PersonaManager;
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v0}, Landroid/os/PersonaManager;->getParentUserForCurrentPersona()I

    move-result v1

    .line 73
    :goto_0
    return v1

    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    goto :goto_0
.end method

.method private getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;->mPersona:Landroid/os/PersonaManager;

    if-nez v0, :cond_0

    .line 60
    const-string v0, "persona"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;->mPersona:Landroid/os/PersonaManager;

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;->mPersona:Landroid/os/PersonaManager;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    const-string v4, "ServiceStartReceiver  user"

    const-string v5, "on receive"

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/knox/bridge/handlers/NotificationHandler;

    invoke-direct {v3, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    .local v3, "service":Landroid/content/Intent;
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;->getPersonaManager(Landroid/content/Context;)Landroid/os/PersonaManager;

    move-result-object v2

    .line 28
    .local v2, "pm":Landroid/os/PersonaManager;
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.knox.container.action.containerremovalstarted"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 31
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    const-string v4, "android.intent.extra.user_handle"

    const-string v5, "com.sec.knox.container.category.observer.containerid"

    const/4 v6, -0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 35
    :try_start_0
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/SecurityException;
    goto :goto_0

    .line 40
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_1
    if-eqz v2, :cond_0

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/receiver/ServiceStartReceiver;->getParentUser(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v1

    .line 43
    .local v1, "personaInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 44
    if-eqz p2, :cond_2

    .line 45
    const-string v4, "ServiceStartReceiver  user"

    const-string v5, "<<< onReceive in ServiceStartREceiver >>>> "

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    :cond_2
    :try_start_1
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 51
    :catch_1
    move-exception v0

    .line 52
    .restart local v0    # "e":Ljava/lang/SecurityException;
    goto :goto_0
.end method
