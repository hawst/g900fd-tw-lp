.class public Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;
.super Landroid/app/Activity;
.source "ContainerKLMSLockedActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;
    }
.end annotation


# static fields
.field private static LOCK_TYPE:Landroid/content/pm/PersonaState;

.field public static mActivity:Landroid/app/Activity;

.field static mType:I

.field static mUserId:I


# instance fields
.field private btn_ok:Landroid/widget/Button;

.field isKioskEnabled:Z

.field mHandler:Landroid/os/Handler;

.field mObserver:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

.field private mPersona:Landroid/os/PersonaManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    const/4 v0, 0x2

    sput v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    .line 61
    const/4 v0, -0x1

    sput v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    .line 63
    sput-object v1, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    .line 67
    sput-object v1, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 68
    iput-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    .line 69
    iput-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mObserver:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    .line 70
    iput-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mHandler:Landroid/os/Handler;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    .line 349
    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;)Landroid/os/PersonaManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->blockIncomingCall(Z)V

    return-void
.end method

.method private blockIncomingCall(Z)V
    .locals 6
    .param p1, "block"    # Z

    .prologue
    .line 397
    const-string v3, "enterprise_policy"

    invoke-virtual {p0, v3}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 398
    .local v1, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getPhoneRestrictionPolicy()Landroid/app/enterprise/PhoneRestrictionPolicy;

    move-result-object v2

    .line 400
    .local v2, "phoneRestrictionPolicy":Landroid/app/enterprise/PhoneRestrictionPolicy;
    if-eqz p1, :cond_1

    .line 403
    :try_start_0
    const-string v3, ".*"

    invoke-virtual {v2, v3}, Landroid/app/enterprise/PhoneRestrictionPolicy;->setIncomingCallRestriction(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 404
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "success setting incoming call restriction"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    :goto_0
    return-void

    .line 406
    :cond_0
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "failed setting incoming call restriction"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v3, "ContainerKLMSLockedActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SecurityException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 412
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_1
    invoke-virtual {v2}, Landroid/app/enterprise/PhoneRestrictionPolicy;->removeIncomingCallRestriction()Z

    goto :goto_0
.end method

.method private blockStatusbar(Z)V
    .locals 3
    .param p1, "block"    # Z

    .prologue
    .line 419
    if-eqz p1, :cond_1

    const/high16 v0, 0x3ff0000

    .line 420
    .local v0, "flags":I
    :goto_0
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 421
    .local v1, "svc":Landroid/app/StatusBarManager;
    if-eqz v1, :cond_0

    .line 422
    invoke-virtual {v1, v0}, Landroid/app/StatusBarManager;->disable(I)V

    .line 424
    :cond_0
    return-void

    .line 419
    .end local v0    # "flags":I
    .end local v1    # "svc":Landroid/app/StatusBarManager;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getKioskContainerId()I
    .locals 5

    .prologue
    .line 111
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    if-eqz v3, :cond_1

    .line 112
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v1

    .line 113
    .local v1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 114
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 115
    .local v2, "pi":Landroid/content/pm/PersonaInfo;
    if-eqz v2, :cond_0

    iget-boolean v3, v2, Landroid/content/pm/PersonaInfo;->isKioskModeEnabled:Z

    if-eqz v3, :cond_0

    .line 116
    iget v3, v2, Landroid/content/pm/PersonaInfo;->id:I

    .line 121
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v2    # "pi":Landroid/content/pm/PersonaInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, -0x1

    goto :goto_0
.end method

.method private isAdminLocked(Landroid/content/Context;I)Z
    .locals 8
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "userid"    # I

    .prologue
    .line 74
    const/4 v3, 0x0

    .line 77
    .local v3, "ret":Z
    sget-object v4, Landroid/content/pm/PersonaState;->INVALID:Landroid/content/pm/PersonaState;

    sput-object v4, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    .line 79
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    if-eqz v4, :cond_2

    .line 80
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/PersonaManager;->getPersonasForUser(I)Ljava/util/List;

    move-result-object v1

    .line 81
    .local v1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 82
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 83
    .local v2, "pi":Landroid/content/pm/PersonaInfo;
    iget v4, v2, Landroid/content/pm/PersonaInfo;->id:I

    if-ne v4, p2, :cond_0

    .line 85
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v4, p2}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v4

    sget-object v5, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v4, v5}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 86
    const/4 v3, 0x1

    .line 89
    iget-object v4, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v4, p2}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PersonaManager$StateManager;->getState()Landroid/content/pm/PersonaState;

    move-result-object v4

    sput-object v4, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    .line 91
    :cond_1
    const-string v4, "ContainerKLMSLockedActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "persona info "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/content/pm/PersonaInfo;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v6, p2}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v6

    sget-object v7, Landroid/content/pm/PersonaState;->SUPER_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v6, v7}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v2    # "pi":Landroid/content/pm/PersonaInfo;
    :cond_2
    return v3
.end method

.method private isKioskContainerEnabled()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v0}, Landroid/os/PersonaManager;->isKioskContainerExistOnDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static startActivity(Landroid/content/Context;I)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "userid"    # I

    .prologue
    .line 331
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 333
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->getKnoxUser1()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 334
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".activity.klms.ContainerKLMSLockedActivity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    :goto_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 342
    const-string v1, "type"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 343
    const-string v1, "userid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 344
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 346
    const-string v1, "ContainerKLMSLockedActivity"

    const-string v2, "startActivity"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void

    .line 337
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".activity.klms.ContainerKLMSLockedActivity2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static startActivityAdminLocked(Landroid/content/Context;I)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "userid"    # I

    .prologue
    .line 313
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 315
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/knox/bridge/util/PersonaInfoMap;->getKnoxUser1()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".activity.klms.ContainerKLMSLockedActivity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    :goto_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 323
    const-string v1, "type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 324
    const-string v1, "userid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 325
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 327
    const-string v1, "ContainerKLMSLockedActivity"

    const-string v2, "startActivityAdminLocked"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    return-void

    .line 319
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".activity.klms.ContainerKLMSLockedActivity2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    if-eqz v0, :cond_0

    .line 292
    :goto_0
    return-void

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, -0x1

    .line 126
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x2000

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    .line 128
    invoke-static {p0}, Landroid/os/PersonaManager;->isKioskModeEnabled(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 129
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/high16 v7, 0x80000

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    .line 132
    :goto_0
    const-string v6, "persona"

    invoke-virtual {p0, v6}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PersonaManager;

    iput-object v6, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mPersona:Landroid/os/PersonaManager;

    .line 133
    const-string v6, "ro.build.characteristics"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "getDeviceType":Ljava/lang/String;
    const/4 v3, 0x0

    .line 139
    .local v3, "is_NON_AMOLEDTablet":Z
    const-string v6, "ContainerKLMSLockedActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "is_NON_AMOLEDTablet"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    if-eqz v3, :cond_0

    .line 142
    const v6, 0x7f070002

    invoke-virtual {p0, v6}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->setTheme(I)V

    .line 144
    :cond_0
    const/high16 v6, 0x7f030000

    invoke-virtual {p0, v6}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->setContentView(I)V

    .line 146
    if-eqz v3, :cond_1

    .line 147
    const v6, 0x7f080001

    invoke-virtual {p0, v6}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 148
    .local v0, "ImageView":Landroid/widget/ImageView;
    const v6, 0x7f020001

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 151
    .end local v0    # "ImageView":Landroid/widget/ImageView;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 153
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_2

    .line 156
    const-string v6, "type"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 157
    .local v5, "type_in":I
    const-string v6, "userid"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    sput v6, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    .line 159
    if-lez v5, :cond_5

    .line 160
    sput v5, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    .line 166
    .end local v5    # "type_in":I
    :cond_2
    :goto_1
    const-string v6, "ContainerKLMSLockedActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onCreate "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sput-object p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mActivity:Landroid/app/Activity;

    .line 169
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskContainerEnabled()Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    .line 170
    iget-boolean v6, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    if-eqz v6, :cond_3

    .line 171
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    iput-object v6, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mHandler:Landroid/os/Handler;

    .line 172
    invoke-direct {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getKioskContainerId()I

    move-result v4

    .line 173
    .local v4, "kioskContainerId":I
    new-instance v6, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    const/4 v7, 0x1

    invoke-direct {v6, p0, p0, v4, v7}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;-><init>(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;Landroid/content/Context;II)V

    iput-object v6, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mObserver:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    .line 175
    .end local v4    # "kioskContainerId":I
    :cond_3
    return-void

    .line 131
    .end local v1    # "getDeviceType":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "is_NON_AMOLEDTablet":Z
    :cond_4
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/high16 v7, 0x400000

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    goto/16 :goto_0

    .line 162
    .restart local v1    # "getDeviceType":Ljava/lang/String;
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "is_NON_AMOLEDTablet":Z
    .restart local v5    # "type_in":I
    :cond_5
    sput v9, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 275
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 276
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mActivity:Landroid/app/Activity;

    .line 277
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 267
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 268
    iget-boolean v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    if-eqz v0, :cond_0

    .line 269
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->blockStatusbar(Z)V

    .line 271
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 281
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 282
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 283
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const v8, 0x7f06003a

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 180
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 182
    const/16 v0, 0x5a

    .line 183
    .local v0, "klms_status":I
    const v3, 0x7f080003

    invoke-virtual {p0, v3}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 185
    .local v2, "textView":Landroid/widget/TextView;
    const/high16 v3, 0x7f080000

    invoke-virtual {p0, v3}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->btn_ok:Landroid/widget/Button;

    .line 186
    iget-boolean v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    if-eqz v3, :cond_0

    .line 187
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->btn_ok:Landroid/widget/Button;

    if-eqz v3, :cond_0

    .line 188
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->btn_ok:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 189
    invoke-direct {p0, v5}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->blockStatusbar(Z)V

    .line 190
    invoke-direct {p0, v5}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->blockIncomingCall(Z)V

    .line 194
    :cond_0
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_a

    .line 195
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    invoke-direct {p0, p0, v3}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isAdminLocked(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 196
    sget-object v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    sget-object v4, Landroid/content/pm/PersonaState;->ADMIN_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 197
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(I)V

    .line 217
    :cond_1
    :goto_0
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    if-eq v3, v6, :cond_2

    sget-object v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    if-eqz v3, :cond_3

    sget-object v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    sget-object v4, Landroid/content/pm/PersonaState;->LICENSE_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 218
    :cond_2
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    invoke-static {p0, v3}, Lcom/sec/knox/bridge/util/PreferenceManager;->getStoredKlmsState(Landroid/content/Context;I)I

    move-result v0

    .line 220
    invoke-static {p0, v0}, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->getKLMSErrorText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "KLMS Locked"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :cond_3
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 226
    const v3, 0x7f060041

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 229
    :cond_4
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    if-ne v3, v7, :cond_5

    .line 231
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "kiosk is being upgraded"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v3, "Knox is upgrading, please wait."

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    :cond_5
    iget-object v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->btn_ok:Landroid/widget/Button;

    new-instance v4, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$1;-><init>(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    const/16 v3, 0x5a

    if-ne v0, v3, :cond_c

    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    if-ne v3, v6, :cond_c

    .line 244
    iget-boolean v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    if-eqz v3, :cond_b

    .line 245
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "IPersonaObserver is taking care of finish"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_6
    :goto_1
    return-void

    .line 199
    :cond_7
    sget-object v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    sget-object v4, Landroid/content/pm/PersonaState;->TIMA_COMPROMISED:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 200
    invoke-virtual {p0, v8}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_8

    .line 203
    const-string v3, "KNOX"

    const-string v4, "KNOX(TIMA)"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 205
    :cond_8
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 206
    .end local v1    # "msg":Ljava/lang/String;
    :cond_9
    sget-object v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->LOCK_TYPE:Landroid/content/pm/PersonaState;

    sget-object v4, Landroid/content/pm/PersonaState;->LICENSE_LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/content/pm/PersonaState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 207
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    invoke-static {p0, v3}, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->sendRequestKeyStatus(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 211
    :cond_a
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "Container Locked, invalid userid"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 248
    :cond_b
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "currently license is okay.. finish this activity... "

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->finish()V

    .line 253
    :cond_c
    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    if-lez v3, :cond_6

    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mType:I

    if-ne v3, v5, :cond_6

    sget v3, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    invoke-direct {p0, p0, v3}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isAdminLocked(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 254
    iget-boolean v3, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->isKioskEnabled:Z

    if-eqz v3, :cond_d

    .line 255
    const-string v3, "ContainerKLMSLockedActivity"

    const-string v4, "IPersonaObserver is taking care of finish"

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 258
    :cond_d
    const-string v3, "ContainerKLMSLockedActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "currently admin lock freed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mUserId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-virtual {p0}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->finish()V

    goto :goto_1
.end method
