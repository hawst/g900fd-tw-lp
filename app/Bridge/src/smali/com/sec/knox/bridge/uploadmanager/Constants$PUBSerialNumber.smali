.class public final enum Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/uploadmanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PUBSerialNumber"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

.field public static final enum PROD_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

.field public static final enum QA_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;


# instance fields
.field private alias:Ljava/lang/String;

.field private mPUBSerialNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    const-string v1, "PROD_SN"

    const-string v2, "knoxlogcloud001"

    const-string v3, "knoxusagekeyprod"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->PROD_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    new-instance v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    const-string v1, "QA_SN"

    const-string v2, "knoxlogqatest001"

    const-string v3, "knoxusagekeyqa"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->QA_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    sget-object v1, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->PROD_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->QA_SN:Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->$VALUES:[Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3, "SerialNumber"    # Ljava/lang/String;
    .param p4, "mAlias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->alias:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->alias:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    return-object v0
.end method

.method public static values()[Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->$VALUES:[Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    invoke-virtual {v0}, [Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;

    return-object v0
.end method


# virtual methods
.method public getAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->alias:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNumebr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/knox/bridge/uploadmanager/Constants$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    return-object v0
.end method
