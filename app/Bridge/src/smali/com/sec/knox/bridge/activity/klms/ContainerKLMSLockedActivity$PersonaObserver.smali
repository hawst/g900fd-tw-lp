.class Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;
.super Landroid/content/pm/AbstractPersonaObserver;
.source "ContainerKLMSLockedActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonaObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;


# direct methods
.method public constructor <init>(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;Landroid/content/Context;II)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "containerId"    # I
    .param p4, "flags"    # I

    .prologue
    .line 351
    iput-object p1, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->this$0:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;

    .line 352
    invoke-direct {p0, p2, p3, p4}, Landroid/content/pm/AbstractPersonaObserver;-><init>(Landroid/content/Context;II)V

    .line 353
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    .prologue
    .line 349
    iget v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->containerId:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;

    .prologue
    .line 349
    iget v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->containerId:I

    return v0
.end method


# virtual methods
.method public onKeyGuardStateChanged(Z)V
    .locals 2
    .param p1, "state"    # Z

    .prologue
    .line 364
    const-string v0, "ContainerKLMSLockedActivity"

    const-string v1, "IPersonaObserver.onKeyGuardStateChanged()"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    return-void
.end method

.method public onSessionExpired()V
    .locals 2

    .prologue
    .line 359
    const-string v0, "ContainerKLMSLockedActivity"

    const-string v1, "IPersonaObserver.onSessionExpired()"

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    return-void
.end method

.method public onStateChange(Landroid/content/pm/PersonaState;Landroid/content/pm/PersonaState;)V
    .locals 4
    .param p1, "newState"    # Landroid/content/pm/PersonaState;
    .param p2, "previousState"    # Landroid/content/pm/PersonaState;

    .prologue
    .line 369
    const-string v0, "ContainerKLMSLockedActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IPersonaObserver.onStateChange() new state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and old state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " aand mContainerId-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->containerId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/bridge/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;->this$0:Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;

    iget-object v0, v0, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver$1;-><init>(Lcom/sec/knox/bridge/activity/klms/ContainerKLMSLockedActivity$PersonaObserver;Landroid/content/pm/PersonaState;Landroid/content/pm/PersonaState;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 388
    return-void
.end method
