.class final Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;
.super Landroid/os/Handler;
.source "KnoxUsageMonitorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 270
    iget v8, p1, Landroid/os/Message;->what:I

    packed-switch v8, :pswitch_data_0

    .line 376
    :goto_0
    return-void

    .line 272
    :pswitch_0
    const-string v8, "KnoxUsageLogPro"

    const-string v9, "Received msg MSG_UPLOAD_COMPLETE_FIRST_TIME"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 275
    .local v1, "data":Landroid/os/Bundle;
    const-string v8, "next_upload"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 277
    .local v6, "next_uploadTime":J
    const-string v8, "KnoxUsageLogPro"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Upload complete MSG_UPLOAD_COMPLETE_FIRST_TIME +nextUploadTime:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->saveNextUploadTime(JLandroid/content/Context;)V

    .line 282
    const/4 v8, 0x0

    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->saveIsFirstTime(ZLandroid/content/Context;)V

    .line 283
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$1;

    invoke-direct {v9, p0, v6, v7}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$1;-><init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;J)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    .line 292
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v8

    # invokes: Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->scheduleNextUpload(Landroid/content/Context;)V
    invoke-static {v8}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->access$000(Landroid/content/Context;)V

    goto :goto_0

    .line 295
    .end local v1    # "data":Landroid/os/Bundle;
    .end local v6    # "next_uploadTime":J
    :pswitch_1
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;

    move-result-object v2

    .line 297
    .local v2, "dataBaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 298
    .restart local v1    # "data":Landroid/os/Bundle;
    const-string v8, "next_upload"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 299
    .local v4, "nextUploadTime":J
    const-string v8, "KnoxUsageLogPro"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Upload complete MSG_UPLOAD_COMPLETE_REGULAR +nextUploadTime:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v4, v5, v8}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->saveNextUploadTime(JLandroid/content/Context;)V

    .line 304
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;

    invoke-direct {v9, p0, v2, v4, v5}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$2;-><init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;J)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 319
    .end local v1    # "data":Landroid/os/Bundle;
    .end local v2    # "dataBaseHelper":Lcom/sec/knox/bridge/knoxusage/KnoxUsageDatabaseHelper;
    .end local v4    # "nextUploadTime":J
    :pswitch_2
    const-string v8, "KnoxUsageLogPro"

    const-string v9, " Received MSG_UPDATE_USAGE"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 321
    .local v0, "currentUser":I
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageSharedPrefUtils;->loadIsFirstTime(Landroid/content/Context;)Z

    move-result v3

    .line 323
    .local v3, "isFirstTime":Z
    const/16 v8, 0x64

    if-ge v0, v8, :cond_0

    .line 324
    const-string v8, "KnoxUsageLogPro"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " MSG_UPDATE_USAGE: user is not persona. No need to update usage:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 329
    :cond_0
    if-eqz v3, :cond_1

    .line 330
    const-string v8, "KnoxUsageLogPro"

    const-string v9, " MSG_UPDATE_USAGE: Not registered with server yet.Try to register"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    # invokes: Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->startFirstUpload()V
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->access$100()V

    goto/16 :goto_0

    .line 336
    :cond_1
    invoke-static {}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService;->isUpdateInProgress()Z

    move-result v8

    if-nez v8, :cond_2

    .line 337
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$3;

    invoke-direct {v9, p0, v0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$3;-><init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;I)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 350
    :cond_2
    const-string v8, "KnoxUsageLogPro"

    const-string v9, " Update already in progress"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 357
    .end local v0    # "currentUser":I
    .end local v3    # "isFirstTime":Z
    :pswitch_3
    const-string v8, "KnoxUsageLogPro"

    const-string v9, " Received MSG_MOVE_AND_TRIGGER_UPDATE"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$4;

    invoke-direct {v9, p0}, Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3$4;-><init>(Lcom/sec/knox/bridge/knoxusage/KnoxUsageMonitorService$3;)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
