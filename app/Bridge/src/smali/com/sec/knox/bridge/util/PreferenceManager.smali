.class public Lcom/sec/knox/bridge/util/PreferenceManager;
.super Ljava/lang/Object;
.source "PreferenceManager.java"


# static fields
.field public static NOT_INIT:I

.field static mContext:Landroid/content/Context;

.field private static mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

.field private static m_FailureCnt:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/sec/knox/bridge/util/PreferenceManager;->m_FailureCnt:I

    .line 28
    const/16 v0, -0x3e7

    sput v0, Lcom/sec/knox/bridge/util/PreferenceManager;->NOT_INIT:I

    return-void
.end method

.method public static getStoredKlmsState(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "userid"    # I

    .prologue
    const/16 v1, 0x5a

    .line 59
    const/4 v0, -0x1

    .line 61
    .local v0, "state":I
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/util/PreferenceManager;->isUserManagedContainer(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    :goto_0
    return v1

    .line 65
    :cond_0
    sget-object v2, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    if-nez v2, :cond_1

    .line 66
    invoke-static {p0}, Lcom/sec/knox/bridge/util/PreferenceManager;->initPreference(Landroid/content/Context;)V

    .line 69
    :cond_1
    sget-object v2, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "status_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/sec/knox/bridge/util/PreferenceManager;->NOT_INIT:I

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 71
    sget v2, Lcom/sec/knox/bridge/util/PreferenceManager;->NOT_INIT:I

    if-ne v0, v2, :cond_2

    .line 72
    sget-object v2, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    const-string v3, "status"

    sget v4, Lcom/sec/knox/bridge/util/PreferenceManager;->NOT_INIT:I

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 74
    const-string v2, "PreferenceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no container based license, status : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_2
    if-eq v0, v1, :cond_3

    .line 78
    sget v1, Lcom/sec/knox/bridge/util/PreferenceManager;->m_FailureCnt:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/sec/knox/bridge/util/PreferenceManager;->m_FailureCnt:I

    .line 80
    sget v1, Lcom/sec/knox/bridge/util/PreferenceManager;->m_FailureCnt:I

    const/16 v2, 0xf

    if-le v1, v2, :cond_3

    .line 81
    const/4 v1, 0x0

    sput v1, Lcom/sec/knox/bridge/util/PreferenceManager;->m_FailureCnt:I

    .line 83
    invoke-static {p0, p1}, Lcom/sec/knox/bridge/activity/klms/KLMSUtils;->sendRequestKeyStatus(Landroid/content/Context;I)V

    .line 85
    const-string v1, "PreferenceManager"

    const-string v2, "requestUpdateStatus"

    invoke-static {v1, v2}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v1, v0

    .line 89
    goto :goto_0
.end method

.method private static initPreference(Landroid/content/Context;)V
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 33
    sget-object v0, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 34
    const-string v0, "KnoxModeSwitcher_shared_prefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    .line 37
    :cond_0
    sput-object p0, Lcom/sec/knox/bridge/util/PreferenceManager;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method public static isUserManagedContainer(Landroid/content/Context;I)Z
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "userid"    # I

    .prologue
    .line 41
    const-string v3, "persona"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 42
    .local v0, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v0, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v1

    .line 43
    .local v1, "pInfo":Landroid/content/pm/PersonaInfo;
    const/4 v2, 0x0

    .line 45
    .local v2, "ret":Z
    if-gez p1, :cond_0

    .line 46
    const/4 v3, 0x0

    .line 55
    :goto_0
    return v3

    .line 49
    :cond_0
    if-eqz v1, :cond_1

    iget-boolean v3, v1, Landroid/content/pm/PersonaInfo;->isUserManaged:Z

    if-eqz v3, :cond_1

    .line 50
    const/4 v2, 0x1

    .line 53
    :cond_1
    const-string v3, "PreferenceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isUserManagedContainer() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/bridge/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 55
    goto :goto_0
.end method

.method public static updateKlmsStatus(Landroid/content/Context;II)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "state"    # I
    .param p2, "user_id"    # I

    .prologue
    .line 93
    sget-object v1, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 94
    invoke-static {p0}, Lcom/sec/knox/bridge/util/PreferenceManager;->initPreference(Landroid/content/Context;)V

    .line 97
    :cond_0
    sget-object v1, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 100
    .local v0, "KnoxModeSwitcher_shared_prefsEditor":Landroid/content/SharedPreferences$Editor;
    const/16 v1, 0x64

    if-lt p2, v1, :cond_2

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 103
    sget-object v1, Lcom/sec/knox/bridge/util/PreferenceManager;->mKnoxModeSwitcher_shared_prefs:Landroid/content/SharedPreferences;

    const-string v2, "status"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    const-string v1, "status"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 110
    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 111
    return-void

    .line 107
    :cond_2
    const-string v1, "status"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method
