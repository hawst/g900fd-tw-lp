.class public interface abstract Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService;
.super Ljava/lang/Object;
.source "IMigrationRemoteService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/containeragent/migrationstate/IMigrationRemoteService$Stub;
    }
.end annotation


# virtual methods
.method public abstract finishMigration()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getMigrationCurrentState()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
