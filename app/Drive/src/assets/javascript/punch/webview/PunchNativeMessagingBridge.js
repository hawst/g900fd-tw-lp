/**
 * @fileoverview Implementation of the communication protocol between the Punch
 * webview and the native layer. See google3/javascript/apps/punch/viewer/
 * mobilenative/api/webview/externs/jstonative/webvieweventlistener.js for
 * more detail of what methods need to be defined.
 *
 * NOTE(vuillemin): do NOT use for comment the double forward slash, use instead
 *     forward slash + star, otherwise it fails to load the javascript
 *
 * @author brianpark@google.com (Brian Park)
 */

punch = {};
/**
 * Target density attribute name in the viewport HTML meta tag.
 */
punch.TARGET_DENSITY_DPI_ATTRIBUTE_NAME = 'target-densitydpi';
/**
 * Target density attribute value for the viewport HTML meta tag.
 */
punch.TARGET_DENSITY_DPI_DEVICE_DPI = 'device-dpi';
/**
 * Regex for detecting the target density attribute name.
 */
punch.TARGET_DENSITY_DPI_REGEX_PATTERN = new RegExp('^\s*' +
    punch.TARGET_DENSITY_DPI_ATTRIBUTE_NAME + '\s*=', 'i');

/**
 * Javascript -> Native callbacks.
 */
window.punchWebViewEventListener = (function() {
  var dataApi_;
  var interceptClicks_ = true;
  var fixTargetDensity_ = false;

  /**
   * Makes sure the viewport meta tag contains target-densitydpi=device-dpi.
   */
  var ensuresTargetDensity = function() {
    var allMeta = document.getElementsByTagName('meta');
    for (var metaElement, i = 0; metaElement = allMeta[i]; i++) {
      if (metaElement.name != 'viewport') {
        continue;
      }

      var originalContent = metaElement.content;
      var attributes = originalContent.replace(/\s*=\s*/g, '=')
          .split(/[\s,;]+/);
      var newAttributes = new Array();
      for (var attribute, j = 0; attribute = attributes[j]; j++) {
        if (!punch.TARGET_DENSITY_DPI_REGEX_PATTERN.test(attribute) &&
                attribute.length > 0) {
          newAttributes.push(attribute);
        }
      }

      newAttributes.push(punch.TARGET_DENSITY_DPI_ATTRIBUTE_NAME + '=' +
        punch.TARGET_DENSITY_DPI_DEVICE_DPI);
      metaElement.content = newAttributes.join(', ');
      break;
    }
  };

  return {
    /**
     * Called when the Punch APIs are exported.
     */
    onApiExported: function(dataApi, controlApi) {
      dataApi_ = dataApi;
      window.PUNCH_WEBVIEW_CONTROL_API = controlApi;
      var pageCount = dataApi_.getNumSlides();
      var pageWidth = dataApi_.getPageWidth();
      var pageHeight = dataApi_.getPageHeight();

      if (fixTargetDensity_) {
        ensuresTargetDensity();
      }

      /*
       * Sets a listener to detect tap action and report them to hosting
       * application: we do not report click action which relate to interaction
       * with the slide content, such as links or video control buttons.
       */
      document.addEventListener("click", function(event) {

        var supportsSVG = typeof SVGAElement != "undefined";
        /* Flag telling whether we should ignore this click event or not. */
        var ignore = false;
        for (var current = event.target;
            current != null;
            current = current.parentNode) {
          if (current == document) {
            break;
          }

          if (!!current.onclick) {
            ignore = true;
            break;
          }

          if (current instanceof HTMLAnchorElement) {
            ignore = true;
            break;
          }

          if (supportsSVG && current instanceof SVGAElement) {
            ignore = true;
            break;
          }
        }

        if (interceptClicks_ && !ignore) {
          event.stopPropagation();
          window.WebViewApi.onTap();
        }
      }, true);

      window.WebViewApi.onApiExported(pageCount, pageWidth, pageHeight);

      var containsNonSupportedTransition = false;
      if (dataApi_.containsNonSupportedTransition) {
        containsNonSupportedTransition =
          dataApi_.containsNonSupportedTransition();
      }

      var containsAnimationOrTransition = false;
      if (dataApi_.containsAnimationOrTransition) {
        containsAnimationOrTransition =
          dataApi_.containsAnimationOrTransition();
      }

      window.WebViewApi.onNotifyMissingFeatures(containsNonSupportedTransition,
                                                containsAnimationOrTransition);
    },

    /**
     * Called when there is a change in a slide's load state.
     * @see punch.viewer.mobilenative.LOAD_STATE_MAPPING
     */
    onSlideLoadStateChange: function(slideIndex, loadState) {
      /*
       * If the slide's load state is LOADED then fill in title and thumbnail
       * content.
       */
      var slideTitle = null;
      var slideSpeakerNotes = null;
      if (loadState == 2) {
        slideTitle = dataApi_.getSlideTitle(slideIndex);

        if (dataApi_.hasSpeakerNotes(slideIndex)) {
          slideSpeakerNotes = dataApi_.getSpeakerNotes(slideIndex);
        }
      }
      window.WebViewApi.onSlideLoadStateChange(slideIndex, loadState,
          slideTitle, slideSpeakerNotes);
    },

    /**
     * Called when requests for all slides have completed.
     */
    onFinishedLoading: function() {
      window.WebViewApi.onFinishedLoading(dataApi_.isLoaded());
    },

    /**
     * Called when it navigates to a different slide or a different part (when a
     * slide contains multiple animations) of a slide.
     */
    onNavigation: function() {
      window.WebViewApi.onNavigation(dataApi_.getCurrentSlideIndex());
    },

    /**
     * Called when there is a change in the action's state.
     */
    onActionEnabledStateChange: function(actionId) {
      window.WebViewApi.onActionEnabledStateChange(
          actionId, dataApi_.isActionEnabled(actionId));
    },

    /**
     * Called to notify when there is a change in previous preview.
     */
    onPreviousPreviewChange: function() {
      var prevContent = null;
      if (dataApi_.hasPrevContent()) {
        prevContent = dataApi_.getPrevContent();
      }
      window.WebViewApi.onPreviousPreviewChange(prevContent);
    },

    /**
     * Called to notify when there is a change in next preview.
     */
    onNextPreviewChange: function() {
      var nextContent = null;
      if (dataApi_.hasNextContent()) {
        nextContent = dataApi_.getNextContent();
      }
      window.WebViewApi.onNextPreviewChange(nextContent);
    },

    /**
     * Called to notify when there is a change in the current animation state.
     */
    onCurrentViewChange: function() {
      var currentContent = dataApi_.getCurrentContent();
      window.WebViewApi.onCurrentViewChange(currentContent);
    },

    /**
     * Called (from app) to access the content for the previous animation state.
     */
    requestPreviousContent: function(requestId) {
      var prevContent = null;
      if (dataApi_.hasPrevContent()) {
        /* This fails if !dataApi_.hasPrevContent() */
        prevContent = dataApi_.getPrevContent();
      }
      var slideContentType = dataApi_.getContentType();
      window.WebViewApi.onSlideContentRequestCompleted(requestId,
        dataApi_.getCurrentSlideIndex(), prevContent, slideContentType);
    },

    /**
     * Called (from app) to access the content for the next animation state.
     */
    requestNextContent: function(requestId) {
      var nextContent = null;
      if (dataApi_.hasNextContent()) {
        /* This fails if !dataApi_.hasPrevContent() */
        nextContent = dataApi_.getNextContent();
      }
      var slideContentType = dataApi_.getContentType();
      window.WebViewApi.onSlideContentRequestCompleted(requestId,
        dataApi_.getCurrentSlideIndex(), nextContent, slideContentType);
    },

    /**
     * Called (from app) to access the full content of a slide, including all
     * animated elements at once.
     */
    requestFullSlideContent: function(requestId, slideIndex) {
      var slideContent = dataApi_.getSlideContent(slideIndex);
      var slideContentType = dataApi_.getContentType();
      window.WebViewApi.onSlideContentRequestCompleted(
        requestId, slideIndex, slideContent, slideContentType);
    },

    /**
     * Called (from app) to access the content for the current state of the
     * current slide. If the slide has animated elements, this only returns the
     * content for the current state of the animations, instead of all animated
     * elements at once as would be returned by requestFullSlideContent.
     */
    requestCurrentStateContent: function(requestId) {
      var slideContent = dataApi_.getCurrentContent();
      var slideContentType = dataApi_.getContentType();
      window.WebViewApi.onSlideContentRequestCompleted(requestId,
        dataApi_.getCurrentSlideIndex(), slideContent, slideContentType);
    },

    /**
     * Configures the app to tell if we try to fix the target density.
     *
     * Intended to be called by the client app.
     *
     * @param {boolean} value to set fixTargetDensity_ with.
     */
    setFixTargetDensity: function(value) {
      fixTargetDensity_ = value;
    },

    /**
     * Configures the app to allow or block touch events from reaching punch.
     *
     * Intended to be called by the client app.
     */
    setInterceptClicks: function(interceptClicks) {
      interceptClicks_ = interceptClicks;
    }
  };
})();
