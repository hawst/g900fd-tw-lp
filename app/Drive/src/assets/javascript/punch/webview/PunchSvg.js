/**
 * @fileoverview  Customization of the Svg Punch webview behavior.
 *
 * NOTE(vuillemin): do NOT use for comment the double forward slash, use instead
 *     forward slash + star, otherwise it fails to load the javascript
 *
 * @author vuillemin@google.com (Alexis Vuillemin)
 */

/* Javascript -> Native callbacks. */
JAVA_INTERFACE = window.SvgLoader;

/**
 * Encapsulation in a singleton of the business logic for handling click
 * actions and resizing commands.
 * @constructor
 */
JAVA_INTERFACE.context = new function() {
  var myself = this;

  /**
   * Handler for the click action.
   * @param {Event} event This is the relating event.
   */
  this.onClicked = function(event) {
    /* Flag telling whether we should ignore this click event or not. */
    var ignore = false;

    for (var current = event.target;
        current != null;
        current = current.parentNode) {
      if (current == document) {
        break;
      }

      if (!!current.onclick) {
        JAVA_INTERFACE.log('ignoring as !!current.onclick');
        return;
      }

      if (current instanceof HTMLAnchorElement) {
        JAVA_INTERFACE.log(
          'ignoring as current instanceof HTMLAnchorElement');
        return;
      }

      if (current instanceof SVGAElement) {
        JAVA_INTERFACE.log(
          'ignoring as current instanceof SVGAElement');
        return;
      }
    }

    if (!ignore) {
      event.stopPropagation();
      JAVA_INTERFACE.onTap();
    }
  };

  /**
   * Changes the height of the container div.
   * @param {Number} new height to apply, expressed in pixels.
   */
  this.resizeHeight = function(newHeight) {
    var doc = document.getElementById('holder');
    doc.style.height = newHeight + 'px';
  };
}

document.addEventListener('click', JAVA_INTERFACE.context.onClicked, true);
