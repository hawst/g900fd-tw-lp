.class LDA;
.super Ljava/lang/Object;
.source "SlidingDrawerLayoutHelper.java"

# interfaces
.implements Lgz;


# instance fields
.field final synthetic a:LDB;

.field final synthetic a:LDz;

.field final synthetic a:Larh;

.field final synthetic a:Lcom/google/android/apps/docs/fragment/NavigationFragment;


# direct methods
.method constructor <init>(LDz;Lcom/google/android/apps/docs/fragment/NavigationFragment;Larh;LDB;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, LDA;->a:LDz;

    iput-object p2, p0, LDA;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iput-object p3, p0, LDA;->a:Larh;

    iput-object p4, p0, LDA;->a:LDB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, LDA;->a:LDz;

    invoke-static {v0}, LDz;->a(LDz;)Lf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf;->a(I)V

    .line 90
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, LDA;->a:LDz;

    invoke-static {v0}, LDz;->a(LDz;)Lf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf;->a(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, LDA;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, LDA;->a:LDB;

    invoke-interface {v0}, LDB;->p()V

    .line 110
    iget-object v0, p0, LDA;->a:LDz;

    invoke-static {v0}, LDz;->a(LDz;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, LDA;->a:LDz;

    invoke-static {v2, p1}, LDz;->a(LDz;Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerTitle(ILjava/lang/CharSequence;)V

    .line 112
    :cond_0
    iget-object v0, p0, LDA;->a:Larh;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Larh;->a(F)V

    .line 113
    iget-object v0, p0, LDA;->a:LDz;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, LDz;->a(LDz;F)F

    .line 114
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, LDA;->a:LDz;

    invoke-static {v0}, LDz;->a(LDz;)Lf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lf;->a(Landroid/view/View;F)V

    .line 95
    iget-object v0, p0, LDA;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, LDA;->a:Larh;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p2

    invoke-interface {v0, v1}, Larh;->a(F)V

    .line 97
    iget-object v0, p0, LDA;->a:LDz;

    invoke-virtual {v0}, LDz;->a()Z

    move-result v0

    .line 98
    iget-object v1, p0, LDA;->a:LDz;

    invoke-static {v1, p2}, LDz;->a(LDz;F)F

    .line 99
    if-eqz v0, :cond_0

    iget-object v0, p0, LDA;->a:LDz;

    invoke-virtual {v0}, LDz;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, LDA;->a:LDB;

    invoke-interface {v0}, LDB;->p()V

    .line 103
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, LDA;->a:LDz;

    invoke-static {v0}, LDz;->a(LDz;)Lf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf;->b(Landroid/view/View;)V

    .line 119
    iget-object v0, p0, LDA;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, LDA;->a:LDB;

    invoke-interface {v0}, LDB;->p()V

    .line 122
    :cond_0
    iget-object v0, p0, LDA;->a:LDz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LDz;->a(LDz;F)F

    .line 123
    iget-object v0, p0, LDA;->a:Larh;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Larh;->a(F)V

    .line 124
    return-void
.end method
