.class Laai;
.super Ljava/lang/Object;
.source "CachedAsyncSearchHandler.java"

# interfaces
.implements Laag;


# instance fields
.field final synthetic a:LaFM;

.field final synthetic a:LaFT;

.field final synthetic a:Laah;

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Laah;LaFM;LaFT;)V
    .locals 1

    .prologue
    .line 68
    iput-object p1, p0, Laai;->a:Laah;

    iput-object p2, p0, Laai;->a:LaFM;

    iput-object p3, p0, Laai;->a:LaFT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Laai;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public a(LaJT;)V
    .locals 3

    .prologue
    .line 77
    :try_start_0
    iget-object v0, p0, Laai;->a:Ljava/util/Set;

    invoke-interface {p1}, LaJT;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Laai;->a:Laah;

    invoke-static {v0}, Laah;->a(Laah;)Lagl;

    move-result-object v0

    iget-object v1, p0, Laai;->a:LaFM;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lagl;->a(LaFM;LaJT;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    const-string v1, "CachedOnlineSearchHandler"

    const-string v2, "Error inserting doc entry"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Laai;->a:Laah;

    invoke-static {v0}, Laah;->a(Laah;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    sget-object v1, Laat;->c:Laat;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Laai;->a:Laah;

    invoke-static {v0}, Laah;->a(Laah;)Laaj;

    move-result-object v0

    iget-object v1, p0, Laai;->a:LaFT;

    invoke-interface {v0, v1}, Laaj;->a(LaFT;)V

    .line 104
    iget-object v0, p0, Laai;->a:Laah;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laah;->a(Laah;Laaf;)Laaf;

    .line 105
    iget-object v0, p0, Laai;->a:LaFT;

    invoke-virtual {v0}, LaFT;->f()V

    .line 106
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Laai;->a:Laah;

    invoke-static {v0}, Laah;->a(Laah;)Laaj;

    move-result-object v0

    iget-object v1, p0, Laai;->a:LaFT;

    invoke-virtual {v1}, LaFT;->c()J

    move-result-wide v2

    iget-object v1, p0, Laai;->a:Ljava/util/Set;

    invoke-interface {v0, v2, v3, v1}, Laaj;->a(JLjava/util/Set;)Z

    move-result v0

    .line 87
    iget-object v1, p0, Laai;->a:Laah;

    invoke-static {v1}, Laah;->a(Laah;)Laaj;

    move-result-object v1

    iget-object v2, p0, Laai;->a:LaFT;

    invoke-interface {v1, v2}, Laaj;->a(LaFT;)V

    .line 88
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Laai;->a:Ljava/util/Set;

    .line 89
    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Laai;->a:Laah;

    invoke-static {v0}, Laah;->a(Laah;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    sget-object v1, Laat;->d:Laat;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Laai;->a:Laah;

    invoke-static {v0}, Laah;->a(Laah;)Laaj;

    move-result-object v0

    iget-object v1, p0, Laai;->a:LaFT;

    invoke-interface {v0, v1}, Laaj;->a(LaFT;)V

    .line 111
    iget-object v0, p0, Laai;->a:Laah;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laah;->a(Laah;Laaf;)Laaf;

    .line 112
    iget-object v0, p0, Laai;->a:LaFT;

    invoke-virtual {v0}, LaFT;->f()V

    .line 113
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Laai;->a:Laah;

    invoke-static {v0}, Laah;->a(Laah;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    sget-object v1, Laat;->e:Laat;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Laai;->a:Laah;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laah;->a(Laah;Laaf;)Laaf;

    .line 97
    iget-object v0, p0, Laai;->a:LaFT;

    invoke-virtual {v0}, LaFT;->a()V

    .line 98
    iget-object v0, p0, Laai;->a:LaFT;

    invoke-virtual {v0}, LaFT;->e()V

    .line 99
    return-void
.end method
