.class public LyI;
.super Landroid/widget/BaseAdapter;
.source "ActivityEntryAdapter.java"


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field private final a:LPD;

.field private final a:LPH;

.field private final a:LaGM;

.field private final a:LaGR;

.field private final a:LaGu;

.field private final a:LagZ;

.field private final a:LamF;

.field private final a:Lamw;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/graphics/Bitmap;

.field private final a:Landroid/view/LayoutInflater;

.field private final a:Landroid/view/View$OnClickListener;

.field private final a:LbsJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:LtK;

.field private a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    sget v0, Lxe;->detail_card_activity_row_info:I

    sput v0, LyI;->a:I

    .line 81
    sget v0, Lxe;->detail_card_activity_row_target:I

    sput v0, LyI;->b:I

    .line 82
    sget v0, Lxe;->detail_card_activity_row_extra:I

    sput v0, LyI;->c:I

    .line 83
    sget v0, Lxe;->detail_card_activity_row_expand:I

    sput v0, LyI;->d:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LaGM;LaGR;LagZ;LtK;LPD;LaGu;LamF;LPH;Lamw;LqK;)V
    .locals 2

    .prologue
    .line 250
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 221
    new-instance v0, LyJ;

    invoke-direct {v0, p0}, LyJ;-><init>(LyI;)V

    iput-object v0, p0, LyI;->a:Landroid/view/View$OnClickListener;

    .line 228
    new-instance v0, LyK;

    invoke-direct {v0, p0}, LyK;-><init>(LyI;)V

    iput-object v0, p0, LyI;->a:LbsJ;

    .line 251
    iput-object p1, p0, LyI;->a:Landroid/content/Context;

    .line 252
    iput-object p2, p0, LyI;->a:LaGM;

    .line 253
    iput-object p4, p0, LyI;->a:LagZ;

    .line 254
    iput-object p3, p0, LyI;->a:LaGR;

    .line 255
    iput-object p5, p0, LyI;->a:LtK;

    .line 256
    iput-object p7, p0, LyI;->a:LaGu;

    .line 257
    iput-object p6, p0, LyI;->a:LPD;

    .line 258
    iput-object p8, p0, LyI;->a:LamF;

    .line 259
    iput-object p9, p0, LyI;->a:LPH;

    .line 260
    iput-object p10, p0, LyI;->a:Lamw;

    .line 261
    iput-object p11, p0, LyI;->a:LqK;

    .line 262
    const-string v0, "layout_inflater"

    .line 263
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LyI;->a:Landroid/view/LayoutInflater;

    .line 265
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxb;->contact_android:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, LyI;->a:Landroid/graphics/Bitmap;

    .line 266
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LaGM;LaGR;LagZ;LtK;LPD;LaGu;LamF;LPH;Lamw;LqK;LyJ;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct/range {p0 .. p11}, LyI;-><init>(Landroid/content/Context;LaGM;LaGR;LagZ;LtK;LPD;LaGu;LamF;LPH;Lamw;LqK;)V

    return-void
.end method

.method private a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 329
    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1}, LPD;->a()LPL;

    move-result-object v1

    .line 330
    if-eqz v1, :cond_0

    .line 331
    iget-object v2, p0, LyI;->a:LPD;

    iget-object v3, p0, LyI;->a:LaGu;

    invoke-virtual {v1, v2, v3}, LPL;->a(LPD;LaGu;)LPK;

    move-result-object v1

    .line 332
    invoke-virtual {v1}, LPK;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LPK;->a()I

    move-result v0

    .line 334
    :cond_0
    return v0
.end method

.method static synthetic a(LyI;)LaGM;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:LaGM;

    return-object v0
.end method

.method static synthetic a(LyI;)LaGR;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:LaGR;

    return-object v0
.end method

.method static synthetic a(LyI;)LaGu;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:LaGu;

    return-object v0
.end method

.method static synthetic a(LyI;)LagZ;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:LagZ;

    return-object v0
.end method

.method static synthetic a(LyI;)Lamw;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:Lamw;

    return-object v0
.end method

.method static synthetic a(LyI;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(LbmF;)Landroid/graphics/drawable/Drawable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/api/services/appsactivity/model/User;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 545
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 547
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v4

    move v1, v2

    .line 549
    :goto_0
    if-ge v1, v3, :cond_2

    .line 550
    invoke-virtual {p1, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/User;

    .line 551
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/User;->getPhoto()Lcom/google/api/services/appsactivity/model/Photo;

    move-result-object v5

    .line 552
    new-instance v0, LakC;

    iget-object v6, p0, LyI;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v6}, LakC;-><init>(Landroid/graphics/Bitmap;)V

    .line 553
    if-eqz v5, :cond_0

    .line 555
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-virtual {v5}, Lcom/google/api/services/appsactivity/model/Photo;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 557
    iget-object v5, p0, LyI;->a:LPH;

    invoke-interface {v5, v6}, LPH;->a(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 558
    if-eqz v5, :cond_1

    .line 559
    new-instance v0, LakC;

    invoke-direct {v0, v5}, LakC;-><init>(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :cond_0
    :goto_1
    invoke-virtual {v4, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 549
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 561
    :cond_1
    :try_start_1
    iget-object v5, p0, LyI;->a:LPH;

    invoke-interface {v5, v6}, LPH;->a(Ljava/net/URL;)LbsU;

    move-result-object v5

    .line 562
    iget-object v6, p0, LyI;->a:LbsJ;

    invoke-static {v5, v6}, LbsK;->a(LbsU;LbsJ;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 564
    :catch_0
    move-exception v0

    .line 565
    const-string v5, "ActivityEntryAdapter"

    const-string v6, "Problem with photo URL for user"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v5, v0, v6, v7}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 572
    :cond_2
    invoke-virtual {v4}, LbmH;->a()LbmF;

    move-result-object v0

    invoke-direct {p0, v0}, LyI;->b(LbmF;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 370
    if-nez p1, :cond_0

    .line 371
    invoke-direct {p0, p2}, LyI;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 376
    :cond_0
    iget-object v0, p0, LyI;->a:LaGu;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    iget-object v0, p0, LyI;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 379
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyM;

    .line 381
    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v3

    .line 382
    invoke-virtual {v3}, Lcom/google/api/services/appsactivity/model/Event;->getPrimaryEventType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LPL;->a(Ljava/lang/String;)LPL;

    move-result-object v4

    .line 384
    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1}, LPD;->a()LbmF;

    move-result-object v5

    .line 385
    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1}, LPD;->b()I

    move-result v6

    .line 388
    invoke-virtual {v3}, Lcom/google/api/services/appsactivity/model/Event;->getTimestamp()Ljava/math/BigInteger;

    move-result-object v1

    .line 389
    iget-object v7, p0, LyI;->a:LamF;

    invoke-virtual {v1}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, LamF;->a(J)Ljava/lang/String;

    move-result-object v1

    .line 390
    iget-object v7, v0, LyM;->c:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 393
    const-string v1, ""

    .line 394
    if-lez v6, :cond_1

    .line 395
    invoke-direct {p0, v5}, LyI;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 397
    :cond_1
    iget-object v6, v0, LyM;->a:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    invoke-direct {p0, v5}, LyI;->a(LbmF;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 400
    iget-object v5, v0, LyM;->a:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 403
    if-eqz v4, :cond_2

    .line 404
    iget-object v1, p0, LyI;->a:LPD;

    iget-object v3, p0, LyI;->a:LaGu;

    invoke-virtual {v4, v1, v3}, LPL;->a(LPD;LaGu;)LPK;

    move-result-object v1

    .line 405
    iget-object v0, v0, LyM;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, LPK;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    :goto_0
    return-object p1

    .line 407
    :cond_2
    iget-object v0, v0, LyM;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/api/services/appsactivity/model/Event;->getPrimaryEventType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 438
    .line 439
    if-nez p1, :cond_0

    .line 440
    iget-object v0, p0, LyI;->a:Landroid/view/LayoutInflater;

    sget v1, LyI;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 443
    :cond_0
    iget-object v0, p0, LyI;->a:LPD;

    invoke-virtual {v0}, LPD;->a()LPL;

    move-result-object v0

    .line 444
    if-eqz v0, :cond_1

    .line 445
    iget-object v1, p0, LyI;->a:LPD;

    iget-object v2, p0, LyI;->a:LaGu;

    invoke-virtual {v0, v1, v2}, LPL;->a(LPD;LaGu;)LPK;

    move-result-object v0

    .line 446
    invoke-virtual {v0, p1, p3}, LPK;->a(Landroid/view/View;I)V

    .line 449
    :cond_1
    return-object p1
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/api/services/appsactivity/model/Target;)Landroid/view/View;
    .locals 4

    .prologue
    .line 454
    if-nez p1, :cond_0

    .line 455
    invoke-direct {p0, p2}, LyI;->b(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 460
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyQ;

    .line 462
    iget-object v1, v0, LyQ;->a:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/google/api/services/appsactivity/model/Target;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    invoke-virtual {p3}, Lcom/google/api/services/appsactivity/model/Target;->getMimeType()Ljava/lang/String;

    move-result-object v1

    .line 465
    invoke-static {v1}, LaGv;->b(Ljava/lang/String;)LaGv;

    move-result-object v2

    .line 467
    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v1

    .line 468
    iget-object v2, v0, LyQ;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 470
    invoke-virtual {p3}, Lcom/google/api/services/appsactivity/model/Target;->getId()Ljava/lang/String;

    move-result-object v1

    .line 472
    iget-object v0, v0, LyQ;->a:Landroid/view/View;

    new-instance v2, LyL;

    invoke-direct {v2, p0, v1}, LyL;-><init>(LyI;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481
    return-object p1
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 431
    iget-object v0, p0, LyI;->a:Landroid/view/LayoutInflater;

    sget v1, LyI;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 432
    invoke-static {v0}, LyM;->a(Landroid/view/View;)LyM;

    move-result-object v1

    .line 433
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 434
    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/User;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 414
    iget-object v0, p0, LyI;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 415
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/User;

    .line 416
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 417
    packed-switch v1, :pswitch_data_0

    .line 425
    sget v3, Lxg;->recent_activity_users_more:I

    add-int/lit8 v4, v1, -0x1

    new-array v5, v5, [Ljava/lang/Object;

    .line 426
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/User;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    add-int/lit8 v0, v1, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    .line 425
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 419
    :pswitch_0
    sget v1, Lxi;->recent_activity_users_one:I

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/User;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 421
    :pswitch_1
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/appsactivity/model/User;

    .line 422
    sget v3, Lxi;->recent_activity_users_two:I

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/User;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 423
    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/User;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 422
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 417
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(LyI;)LqK;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:LqK;

    return-object v0
.end method

.method static synthetic a(LyI;)LtK;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LyI;->a:LtK;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 535
    const-string v2, "ActivityEntryAdapter"

    const-string v3, "Toggle called! Expandable: %s; Expanded: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {p0}, LyI;->b()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    iget-boolean v5, p0, LyI;->a:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 536
    invoke-direct {p0}, LyI;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 537
    iget-object v2, p0, LyI;->a:LqK;

    const-string v3, "activityStream"

    const-string v4, "expandActivityEntryEvent"

    invoke-virtual {v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    iget-boolean v2, p0, LyI;->a:Z

    if-nez v2, :cond_1

    :goto_0
    iput-boolean v0, p0, LyI;->a:Z

    .line 540
    invoke-virtual {p0}, LyI;->notifyDataSetChanged()V

    .line 542
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 539
    goto :goto_0
.end method

.method static synthetic a(LyI;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, LyI;->a()V

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, LyI;->a:LPD;

    invoke-virtual {v0}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    .line 514
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getTarget()Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    .line 515
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Target;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LyI;->a:LaGu;

    invoke-interface {v1}, LaGu;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 519
    invoke-direct {p0}, LyI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const/4 v0, 0x0

    .line 526
    :goto_0
    return v0

    .line 523
    :cond_0
    iget-boolean v0, p0, LyI;->a:Z

    if-eqz v0, :cond_1

    .line 524
    iget-object v0, p0, LyI;->a:LPD;

    invoke-virtual {v0}, LPD;->a()I

    move-result v0

    goto :goto_0

    .line 526
    :cond_1
    const/4 v0, 0x3

    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1}, LPD;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private b(LbmF;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 576
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    .line 578
    if-nez v0, :cond_0

    .line 579
    new-instance v0, LakC;

    iget-object v1, p0, LyI;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, LakC;-><init>(Landroid/graphics/Bitmap;)V

    .line 586
    :goto_0
    return-object v0

    .line 580
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 581
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 583
    :cond_1
    new-instance v0, LyO;

    invoke-direct {v0, p1}, LyO;-><init>(LbmF;)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 493
    if-nez p1, :cond_0

    .line 494
    invoke-direct {p0, p2}, LyI;->c(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 499
    :cond_0
    sget v0, Lxc;->recent_event_expandButton:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 501
    invoke-direct {p0}, LyI;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LyI;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 503
    iget-object v0, p0, LyI;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 505
    return-object p1

    .line 501
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 485
    iget-object v0, p0, LyI;->a:Landroid/view/LayoutInflater;

    sget v1, LyI;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 486
    invoke-static {v0}, LyQ;->a(Landroid/view/View;)LyQ;

    move-result-object v1

    .line 487
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 488
    return-object v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, LyI;->a:LPD;

    invoke-virtual {v0}, LPD;->a()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 509
    iget-object v0, p0, LyI;->a:Landroid/view/LayoutInflater;

    sget v1, LyI;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 305
    invoke-direct {p0}, LyI;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, LyI;->b()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 306
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 275
    invoke-virtual {p0, p1}, LyI;->getItemViewType(I)I

    move-result v0

    .line 276
    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, LyI;->a:LPD;

    .line 286
    :goto_0
    return-object v0

    .line 278
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 279
    iget-object v0, p0, LyI;->a:Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 280
    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 281
    add-int/lit8 v0, p1, -0x1

    .line 282
    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1}, LPD;->a()LPL;

    move-result-object v1

    iget-object v2, p0, LyI;->a:LPD;

    iget-object v3, p0, LyI;->a:LaGu;

    invoke-virtual {v1, v2, v3}, LPL;->a(LPD;LaGu;)LPK;

    move-result-object v1

    .line 283
    invoke-virtual {v1, v0}, LPK;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 285
    :cond_2
    invoke-direct {p0}, LyI;->a()I

    move-result v0

    sub-int v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    .line 286
    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1, v0}, LPD;->a(I)Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0, p1}, LyI;->getItemViewType(I)I

    move-result v0

    .line 293
    if-nez v0, :cond_0

    .line 294
    const-wide/16 v0, 0x0

    .line 299
    :goto_0
    return-wide v0

    .line 295
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 297
    invoke-direct {p0}, LyI;->a()I

    move-result v0

    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1}, LPD;->a()I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    goto :goto_0

    .line 299
    :cond_1
    int-to-long v0, p1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 341
    const/4 v0, 0x0

    .line 347
    :goto_0
    return v0

    .line 342
    :cond_0
    invoke-direct {p0}, LyI;->a()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 343
    const/4 v0, 0x3

    goto :goto_0

    .line 344
    :cond_1
    invoke-direct {p0}, LyI;->a()I

    move-result v0

    invoke-direct {p0}, LyI;->b()I

    move-result v1

    add-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    .line 345
    const/4 v0, 0x1

    goto :goto_0

    .line 347
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 311
    invoke-virtual {p0, p1}, LyI;->getItemViewType(I)I

    move-result v0

    .line 313
    packed-switch v0, :pswitch_data_0

    .line 322
    :pswitch_0
    invoke-direct {p0}, LyI;->a()I

    move-result v0

    sub-int v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    .line 323
    iget-object v1, p0, LyI;->a:LPD;

    invoke-virtual {v1, v0}, LPD;->a(I)Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    .line 324
    invoke-direct {p0, p2, p3, v0}, LyI;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/api/services/appsactivity/model/Target;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 315
    :pswitch_1
    invoke-direct {p0, p2, p3}, LyI;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 317
    :pswitch_2
    add-int/lit8 v0, p1, -0x1

    .line 318
    invoke-direct {p0, p2, p3, v0}, LyI;->a(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 320
    :pswitch_3
    invoke-direct {p0, p2, p3}, LyI;->b(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 313
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x4

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 353
    invoke-virtual {p0, p1}, LyI;->getItemViewType(I)I

    move-result v1

    .line 354
    if-eq v1, v0, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 355
    invoke-direct {p0}, LyI;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, LyI;->a:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
