.class LLB;
.super Ljava/lang/Object;
.source "SelectionFunctionDoMoveToCurrentFolder.java"

# interfaces
.implements LLw;


# instance fields
.field private final a:LDa;

.field private final a:LJR;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Lcom/google/android/apps/docs/view/DocListView;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lapn;

.field private a:Lcom/google/android/apps/docs/doclist/SelectionItem;

.field private final b:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LJR;LDa;Laja;Laja;Lapn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LJR;",
            "LDa;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Laja",
            "<",
            "Lcom/google/android/apps/docs/view/DocListView;",
            ">;",
            "Lapn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, LLB;->a:LJR;

    .line 72
    iput-object p2, p0, LLB;->a:LDa;

    .line 73
    iput-object p3, p0, LLB;->b:Laja;

    .line 74
    iput-object p4, p0, LLB;->a:Laja;

    .line 75
    iput-object p5, p0, LLB;->a:Lapn;

    .line 76
    return-void
.end method

.method static synthetic a(LLB;)Laja;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LLB;->a:Laja;

    return-object v0
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    iput-object p3, p0, LLB;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 88
    return-void
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v1, p0, LLB;->a:LDa;

    iget-object v0, p0, LLB;->b:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v2, p0, LLB;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v1, v0, v2}, LDa;->a(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 94
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 95
    iget-object v0, p0, LLB;->a:Lapn;

    new-instance v1, LLC;

    invoke-direct {v1, p0, p3}, LLC;-><init>(LLB;LbmF;)V

    invoke-virtual {v0, v1}, Lapn;->b(Lapu;)V

    .line 96
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 81
    if-eqz p2, :cond_0

    iget-object v0, p0, LLB;->a:LJR;

    invoke-virtual {v0, p2}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
