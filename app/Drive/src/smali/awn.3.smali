.class public Lawn;
.super Ljava/lang/Object;
.source "TileBoard.java"


# static fields
.field static final a:Lcom/google/android/apps/viewer/client/Dimensions;


# instance fields
.field private final a:I

.field private final a:LavU;

.field private a:Lawq;

.field private final a:Ljava/lang/String;

.field private final a:[Landroid/graphics/Bitmap;

.field private final a:[Lawr;

.field public final b:Lcom/google/android/apps/viewer/client/Dimensions;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x320

    .line 28
    new-instance v0, Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/viewer/client/Dimensions;-><init>(II)V

    sput-object v0, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/viewer/client/Dimensions;LavU;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TileBoard #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lawn;->a:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lawn;->a:LavU;

    .line 51
    iput-object p2, p0, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    .line 53
    iget v0, p2, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    sget-object v1, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v1, v1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lawn;->a:I

    .line 54
    iget v0, p2, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    sget-object v1, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v1, v1, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 55
    iget v1, p0, Lawn;->a:I

    mul-int/2addr v1, v0

    new-array v1, v1, [Landroid/graphics/Bitmap;

    iput-object v1, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    .line 56
    iget v1, p0, Lawn;->a:I

    mul-int/2addr v0, v1

    new-array v0, v0, [Lawr;

    iput-object v0, p0, Lawn;->a:[Lawr;

    .line 57
    return-void
.end method

.method static synthetic a(Lawn;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lawn;->a:I

    return v0
.end method

.method private a(I)Lawr;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lawn;->a:[Lawr;

    aget-object v0, v0, p1

    .line 65
    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lawr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lawr;-><init>(Lawn;ILawo;)V

    .line 67
    iget-object v1, p0, Lawn;->a:[Lawr;

    aput-object v0, v1, p1

    .line 69
    :cond_0
    return-object v0
.end method

.method private a(Lawq;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lawq;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, Lawo;

    invoke-direct {v0, p0, p1}, Lawo;-><init>(Lawn;Lawq;)V

    return-object v0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lawn;->a()I

    move-result v0

    iget v1, p0, Lawn;->a:I

    div-int/2addr v0, v1

    return v0
.end method

.method static synthetic b(Lawn;)I
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lawn;->b()I

    move-result v0

    return v0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 217
    .line 220
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    iget-object v6, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v7, v6

    move v2, v3

    move v4, v3

    move v0, v3

    move v1, v3

    :goto_0
    if-ge v2, v7, :cond_1

    aget-object v8, v6, v2

    .line 222
    if-eqz v8, :cond_0

    .line 223
    add-int/lit8 v0, v0, 0x1

    .line 224
    invoke-static {v8}, LavU;->a(Landroid/graphics/Bitmap;)I

    move-result v8

    add-int/2addr v1, v8

    .line 225
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 221
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 229
    :cond_1
    iget-object v2, p0, Lawn;->a:Ljava/lang/String;

    const-string v4, "Tile Mem usage (%s): %d tiles (out of %d) / %d K. %s"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lawn;->a:Ljava/lang/String;

    aput-object v7, v6, v3

    const/4 v3, 0x1

    .line 230
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v3

    const/4 v0, 0x2

    iget-object v3, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v0, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x4

    aput-object v5, v6, v0

    .line 229
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v0, v0

    return v0
.end method

.method public a()V
    .locals 5

    .prologue
    .line 97
    iget-object v1, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 98
    iget-object v4, p0, Lawn;->a:LavU;

    invoke-virtual {v4, v3}, LavU;->a(Landroid/graphics/Bitmap;)V

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_0
    iget-object v0, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    invoke-direct {p0}, Lawn;->b()V

    .line 102
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v0, v0, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    if-eq v0, p1, :cond_0

    sget-boolean v0, LavX;->o:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/graphics/Rect;Laws;)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 109
    iget v0, p1, Landroid/graphics/Rect;->top:I

    if-ltz v0, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->left:I

    if-ltz v0, :cond_0

    .line 110
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v3, p0, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    if-gt v0, v3, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v3, p0, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    if-gt v0, v3, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewArea extends beyond our bounds, should be clipped."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 109
    invoke-static {v0, v3}, LauV;->b(ZLjava/lang/String;)V

    .line 112
    iget v0, p0, Lawn;->a:I

    invoke-direct {p0}, Lawn;->b()I

    move-result v3

    invoke-static {p1, v0, v3}, Lawq;->a(Landroid/graphics/Rect;II)Lawq;

    move-result-object v0

    .line 114
    iget-object v3, p0, Lawn;->a:Lawq;

    invoke-virtual {v0, v3}, Lawq;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 155
    :goto_1
    return v2

    :cond_0
    move v0, v2

    .line 110
    goto :goto_0

    .line 117
    :cond_1
    iput-object v0, p0, Lawn;->a:Lawq;

    .line 121
    iget-object v0, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v0, v0

    new-array v5, v0, [Landroid/graphics/Bitmap;

    .line 123
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lawn;->a:Lawq;

    invoke-virtual {v0}, Lawq;->a()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 124
    iget-object v0, p0, Lawn;->a:Lawq;

    invoke-direct {p0, v0}, Lawn;->a(Lawq;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 125
    iget-object v0, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v7

    .line 126
    if-nez v0, :cond_2

    .line 127
    invoke-direct {p0, v7}, Lawn;->a(I)Lawr;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v3

    :goto_3
    move v3, v0

    .line 133
    goto :goto_2

    .line 129
    :cond_2
    aput-object v0, v5, v7

    .line 130
    add-int/lit8 v0, v3, 0x1

    .line 131
    iget-object v3, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    const/4 v8, 0x0

    aput-object v8, v3, v7

    goto :goto_3

    .line 137
    :cond_3
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 138
    iget-object v8, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v9, v8

    move v0, v2

    move v4, v2

    :goto_4
    if-ge v0, v9, :cond_5

    aget-object v10, v8, v0

    .line 139
    if-eqz v10, :cond_4

    .line 140
    iget-object v11, p0, Lawn;->a:LavU;

    invoke-virtual {v11, v10}, LavU;->a(Landroid/graphics/Bitmap;)V

    .line 141
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 145
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 146
    invoke-interface {p2, v7}, Laws;->b(Ljava/lang/Iterable;)V

    .line 149
    :cond_6
    iget-object v0, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    iget-object v4, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v4, v4

    invoke-static {v5, v2, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 150
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 151
    iget-object v0, p0, Lawn;->a:Ljava/lang/String;

    const-string v4, "ViewArea has %d new tiles (had tiles: %s), discard: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    .line 152
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v2, 0x2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    .line 151
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-interface {p2, v6}, Laws;->a(Ljava/lang/Iterable;)V

    :cond_7
    move v2, v1

    .line 155
    goto/16 :goto_1
.end method

.method public a(Lawr;Landroid/graphics/Bitmap;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 87
    invoke-virtual {p1, p0}, Lawr;->a(Lawn;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    iget-object v2, p0, Lawn;->a:Ljava/lang/String;

    const-string v3, "Discard %s (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    iget-object v5, p0, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :goto_0
    return v0

    .line 91
    :cond_0
    iget-object v0, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Lawr;->a()I

    move-result v2

    aput-object p2, v0, v2

    .line 92
    invoke-direct {p0}, Lawn;->b()V

    move v0, v1

    .line 93
    goto :goto_0
.end method

.method protected finalize()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 235
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 237
    iget-object v2, p0, Lawn;->a:[Landroid/graphics/Bitmap;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 238
    if-eqz v4, :cond_0

    .line 239
    iget-object v4, p0, Lawn;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Finalize -- Memory leak candidate (bitmap not null) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lawn;->a:[Lawr;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 243
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lawn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (%s x %s), vis: %s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0}, Lawn;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lawn;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lawn;->a:Lawq;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
