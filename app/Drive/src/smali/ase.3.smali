.class public Lase;
.super Ljava/lang/Object;
.source "OnlineCatalog.java"

# interfaces
.implements Latj;


# instance fields
.field private final a:LTl;

.field private final a:LasR;

.field private a:Lbtk;

.field private final a:LwH;

.field private final a:Lyt;


# direct methods
.method public constructor <init>(LasR;LTl;LwH;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lase;->a:Lbtk;

    .line 56
    iput-object p1, p0, Lase;->a:LasR;

    .line 57
    iput-object p2, p0, Lase;->a:LTl;

    .line 58
    iput-object p3, p0, Lase;->a:LwH;

    .line 59
    new-instance v0, Lyt;

    invoke-static {}, Lyj;->a()Lyi;

    move-result-object v1

    invoke-direct {v0, v1}, Lyt;-><init>(Lyi;)V

    iput-object v0, p0, Lase;->a:Lyt;

    .line 60
    return-void
.end method

.method static synthetic a(Lase;Lath;)Ladj;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lase;->a(Lath;)Ladj;

    move-result-object v0

    return-object v0
.end method

.method private a(Lath;)Ladj;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 202
    invoke-virtual {p1}, Lath;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v6

    .line 226
    :goto_0
    return-object v0

    .line 206
    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    .line 207
    invoke-virtual {p1}, Lath;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, LbpU;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    const-wide/32 v2, 0x19bfcc00

    const-string v1, "0"

    .line 206
    invoke-static {v0, v2, v3, v1}, LwM;->a(Ljava/util/Set;JLjava/lang/String;)LwM;

    move-result-object v4

    .line 209
    :try_start_0
    iget-object v0, p0, Lase;->a:LwH;

    const-string v1, "welcome"

    const/4 v2, 0x0

    sget-object v3, LalT;->a:LaGs;

    iget-object v5, p0, Lase;->a:Lyt;

    invoke-interface/range {v0 .. v5}, LwH;->a(Ljava/lang/String;LaFO;LaGs;LwL;Lyt;)LaGL;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lase;->a:LwH;

    invoke-interface {v1, v0}, LwH;->a(LaGL;)LwI;

    move-result-object v0

    .line 212
    if-nez v0, :cond_1

    move-object v0, v6

    .line 213
    goto :goto_0

    .line 215
    :cond_1
    invoke-interface {v0}, LwI;->a()Ljava/util/List;

    move-result-object v0

    .line 216
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v0, v6

    .line 217
    goto :goto_0

    .line 219
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    .line 220
    const-string v1, "OnlineCatalog"

    const-string v2, "got Archive file %s (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 221
    invoke-interface {v0}, Ladj;->a()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {v0}, Ladj;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 220
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lwx; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 224
    :catch_0
    move-exception v0

    .line 225
    const-string v1, "OnlineCatalog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while getting the archived story "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    move-object v0, v6

    .line 226
    goto :goto_0
.end method

.method protected static a(Ljava/lang/String;)Lbtk;
    .locals 3

    .prologue
    .line 183
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    .line 185
    :try_start_0
    invoke-virtual {v0, p0}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lbth;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v0}, Lbth;->a()Lbtk;
    :try_end_0
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 193
    :goto_0
    return-object v0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    const-string v1, "OnlineCatalog"

    const-string v2, "Catalog file is not a valid JSON."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 193
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lath;)LasP;
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p1}, Lath;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbju;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lase;->a:LasR;

    invoke-virtual {v0}, LasR;->a()LasV;

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    .line 171
    :cond_0
    invoke-virtual {p1}, Lath;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    invoke-direct {p0, p1}, Lase;->a(Lath;)Ladj;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_1

    iget-object v1, p0, Lase;->a:LasR;

    .line 174
    invoke-virtual {p1}, Lath;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LasR;->a(Ladj;Ljava/lang/String;)LasP;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lase;->a:LasR;

    .line 175
    invoke-virtual {v0}, LasR;->a()LasV;

    move-result-object v0

    goto :goto_0

    .line 177
    :cond_2
    iget-object v0, p0, Lase;->a:LasR;

    invoke-virtual {p1}, Lath;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LasR;->a(Ljava/lang/String;)LasP;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lath;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lase;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lath;->a(Ljava/lang/String;)Lath;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)Latj;
    .locals 1

    .prologue
    .line 69
    invoke-static {p1}, Lase;->a(Ljava/lang/String;)Lbtk;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 71
    iput-object v0, p0, Lase;->a:Lbtk;

    .line 74
    :cond_0
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string v0, ""

    return-object v0
.end method

.method protected a(I)Ljava/lang/String;
    .locals 7

    .prologue
    .line 94
    iget-object v0, p0, Lase;->a:Lbtk;

    if-nez v0, :cond_1

    .line 95
    const-string v0, "OnlineCatalog"

    const-string v1, "Catalog is not initialized, should ensure loadFrom called."

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string v0, ""

    .line 135
    :cond_0
    :goto_0
    return-object v0

    .line 99
    :cond_1
    const/4 v2, 0x0

    .line 100
    const-string v0, ""

    .line 101
    const-string v0, ""

    .line 102
    iget-object v0, p0, Lase;->a:Lbtk;

    invoke-virtual {v0}, Lbtk;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 103
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 104
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbth;

    invoke-virtual {v0}, Lbth;->a()Ljava/lang/String;

    move-result-object v0

    .line 105
    const-string v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 107
    array-length v4, v3

    packed-switch v4, :pswitch_data_0

    .line 116
    const-string v0, "OnlineCatalog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to process version(s) in: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_0
    move-object v3, v1

    move-object v4, v1

    .line 121
    :goto_2
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-lt p1, v4, :cond_2

    .line 122
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-gt p1, v1, :cond_2

    .line 131
    :goto_3
    if-nez v0, :cond_0

    .line 132
    const-string v0, "OnlineCatalog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Catalog doesn\'t have our version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-string v0, ""

    goto :goto_0

    .line 112
    :pswitch_1
    const/4 v4, 0x0

    aget-object v4, v3, v4

    .line 113
    const/4 v6, 0x1

    aget-object v3, v3, v6

    goto :goto_2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    const-string v0, "OnlineCatalog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse number in this range: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_3

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lath;Latk;)Ljava/lang/Thread;
    .locals 3

    .prologue
    .line 140
    invoke-virtual {p1}, Lath;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p1}, Lath;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p2, v0}, Latk;->a(Landroid/net/Uri;)V

    .line 142
    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    .line 145
    :cond_0
    new-instance v0, Lasf;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fetch "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lasf;-><init>(Lase;Ljava/lang/String;Lath;Latk;)V

    .line 155
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Latj;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lase;->a:LTl;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LTl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-virtual {p0, v0}, Lase;->a(Ljava/lang/String;)Latj;

    move-result-object v0

    return-object v0
.end method
