.class public LabY;
.super Ljava/lang/Object;
.source "ModifySharingActivity.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Lacr;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .locals 0

    .prologue
    .line 465
    iput-object p1, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lacr;)V
    .locals 2

    .prologue
    .line 478
    const-string v0, "ModifySharingActivity"

    const-string v1, "Updated ACL."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iget-object v0, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Lacr;)Lacr;

    .line 480
    iget-object v0, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Lxi;->sharing_message_saved:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    .line 481
    iget-object v0, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v1, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Landroid/view/View;)V

    .line 482
    iget-object v0, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 483
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 465
    check-cast p1, Lacr;

    invoke-virtual {p0, p1}, LabY;->a(Lacr;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 468
    instance-of v0, p1, LQn;

    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Lxi;->sharing_list_offline:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 470
    :cond_1
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 471
    const-string v0, "ModifySharingActivity"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    const-string v2, "Update error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 472
    iget-object v0, p0, LabY;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Lxi;->sharing_message_unable_to_change:I

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;ILjava/lang/Throwable;)V

    goto :goto_0
.end method
