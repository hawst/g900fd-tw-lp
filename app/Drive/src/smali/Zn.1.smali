.class public LZn;
.super Ljava/lang/Object;
.source "PunchWebViewFragment.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LZa;)V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0, p1}, LZn;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    return-void
.end method


# virtual methods
.method public onActionEnabledStateChange(IZ)V
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 414
    const-string v0, "PunchWebViewFragment"

    const-string v1, "onActionEnabledStateChange(actionId=%s, isActionEnabled=%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 415
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 414
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 416
    return-void
.end method

.method public onApiExported(III)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 271
    const-string v2, "PunchWebViewFragment"

    const-string v3, "in onApiExported(slideCount=%s, slideWidth=%s, slideHeight=%s)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 272
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 271
    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 273
    iget-object v2, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v2}, LXD;->c()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 279
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZo;

    invoke-direct {v1, p0, p1, p2, p3}, LZo;-><init>(LZn;III)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 294
    return-void

    :cond_0
    move v0, v1

    .line 273
    goto :goto_0
.end method

.method public onCurrentViewChange(Ljava/lang/String;)V
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 469
    const-string v2, "PunchWebViewFragment"

    const-string v3, "onCurrentViewChange. Content is null: %s"

    new-array v4, v0, [Ljava/lang/Object;

    if-nez p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 471
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->g(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZu;

    invoke-direct {v1, p0, p1}, LZu;-><init>(LZn;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 482
    return-void

    :cond_0
    move v0, v1

    .line 469
    goto :goto_0
.end method

.method public onFinishedLoading(Z)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 375
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onFinishedLoading(isLoaded=%s) + Loadtime=%s ms"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 376
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v6}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 375
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 378
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    .line 379
    return-void
.end method

.method public onNavigation(I)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 389
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onNavigation(slideIndex=%s) took=%sms"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 390
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v6}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 389
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 392
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZr;

    invoke-direct {v1, p0, p1}, LZr;-><init>(LZn;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 405
    return-void
.end method

.method public onNextPreviewChange(Ljava/lang/String;)V
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 447
    const-string v2, "PunchWebViewFragment"

    const-string v3, "onNextPreviewChange. Content is null: %s"

    new-array v4, v0, [Ljava/lang/Object;

    if-nez p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 449
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->f(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZt;

    invoke-direct {v1, p0, p1}, LZt;-><init>(LZn;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 460
    return-void

    :cond_0
    move v0, v1

    .line 447
    goto :goto_0
.end method

.method public onNotifyMissingFeatures(ZZ)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 522
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onNotifyMissingFeatures()"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->i(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZw;

    invoke-direct {v1, p0, p2, p1}, LZw;-><init>(LZn;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 534
    return-void
.end method

.method public onPreviousPreviewChange(Ljava/lang/String;)V
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 425
    const-string v2, "PunchWebViewFragment"

    const-string v3, "onPreviousPreviewChange. Content is null: %s"

    new-array v4, v0, [Ljava/lang/Object;

    if-nez p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 427
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->e(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZs;

    invoke-direct {v1, p0, p1}, LZs;-><init>(LZn;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 438
    return-void

    :cond_0
    move v0, v1

    .line 425
    goto :goto_0
.end method

.method public onSlideContentRequestCompleted(IILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 492
    const-string v0, "PunchWebViewFragment"

    const-string v1, "onSlideContentRequestCompleted(requestId=%s slideIndex=%s slideContentType=%s)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 494
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    .line 492
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 496
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZv;

    invoke-direct {v1, p0, p1, p4, p3}, LZv;-><init>(LZn;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 512
    return-void
.end method

.method public onSlideLoadStateChange(IILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 334
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onSlideLoadStateChange(slideIndex=%s, loadStateCode=%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 335
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 334
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 337
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v6

    new-instance v0, LZq;

    move-object v1, p0

    move v2, p2

    move v3, p1

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LZq;-><init>(LZn;IILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 366
    return-void
.end method

.method public onTap()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 304
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onTap"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v0, p0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZp;

    invoke-direct {v1, p0}, LZp;-><init>(LZn;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 324
    return-void
.end method
