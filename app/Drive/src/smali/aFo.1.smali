.class LaFo;
.super Ljava/lang/Object;
.source "AbstractDatabaseInstance.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbjv",
        "<",
        "Landroid/database/sqlite/SQLiteDatabase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaFm;


# direct methods
.method constructor <init>(LaFm;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, LaFo;->a:LaFm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 456
    const-string v0, "AbstractDatabaseInstance"

    const-string v1, "About to open database"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v0, p0, LaFo;->a:LaFm;

    iget-object v0, v0, LaFm;->a:LaFu;

    invoke-virtual {v0}, LaFu;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 458
    const-string v1, "PRAGMA foreign_keys=ON;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 459
    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 453
    invoke-virtual {p0}, LaFo;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method
