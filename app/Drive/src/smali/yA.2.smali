.class public final LyA;
.super LyR;
.source "ActionCard.java"


# instance fields
.field private final a:LKf;

.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;LKf;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, LyR;-><init>()V

    .line 26
    iput-object p2, p0, LyA;->a:LKf;

    .line 28
    const-string v0, "layout_inflater"

    .line 29
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 30
    sget v1, Lxe;->action_card:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LyA;->a:Landroid/view/View;

    .line 32
    iget-object v0, p0, LyA;->a:Landroid/view/View;

    sget v1, Lxc;->action_card_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 33
    iget-object v1, p0, LyA;->a:Landroid/view/View;

    sget v2, Lxc;->action_card_popup_anchor:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 34
    invoke-virtual {p2, v0, v1}, LKf;->a(Landroid/widget/LinearLayout;Landroid/view/View;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LyA;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(LaGu;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, LyA;->a:LKf;

    invoke-virtual {v0, p1}, LKf;->a(LaGu;)V

    .line 45
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method
