.class public final Lyh;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lyd;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxU;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lyc;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxW;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxR;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxY;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lyf;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxQ;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxX;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lyb;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lye;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 49
    iput-object p1, p0, Lyh;->a:LbrA;

    .line 50
    const-class v0, Lyd;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->a:Lbsk;

    .line 53
    const-class v0, LxU;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->b:Lbsk;

    .line 56
    const-class v0, Lyc;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->c:Lbsk;

    .line 59
    const-class v0, LxW;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->d:Lbsk;

    .line 62
    const-class v0, LxR;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->e:Lbsk;

    .line 65
    const-class v0, LxY;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->f:Lbsk;

    .line 68
    const-class v0, Lyf;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->g:Lbsk;

    .line 71
    const-class v0, LxQ;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->h:Lbsk;

    .line 74
    const-class v0, LxX;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->i:Lbsk;

    .line 77
    const-class v0, Lyb;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->j:Lbsk;

    .line 80
    const-class v0, Lye;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->k:Lbsk;

    .line 83
    const-class v0, LxV;

    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyh;->l:Lbsk;

    .line 86
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 137
    packed-switch p1, :pswitch_data_0

    .line 269
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :pswitch_1
    new-instance v2, Lyd;

    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 142
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lyh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 140
    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lyh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->b:Lbsk;

    .line 148
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, Lyh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LqD;

    iget-object v3, v3, LqD;->b:Lbsk;

    .line 146
    invoke-static {v1, v3}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LqF;

    invoke-direct {v2, v0, v1}, Lyd;-><init>(Landroid/content/Context;LqF;)V

    move-object v0, v2

    .line 267
    :goto_0
    return-object v0

    .line 155
    :pswitch_2
    new-instance v1, LxU;

    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->f:Lbsk;

    .line 158
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lyh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->f:Lbsk;

    .line 156
    invoke-static {v0, v2}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, v0}, LxU;-><init>(I)V

    move-object v0, v1

    .line 163
    goto :goto_0

    .line 165
    :pswitch_3
    new-instance v1, Lyc;

    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 168
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lyh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 166
    invoke-static {v0, v2}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Lyc;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 173
    goto :goto_0

    .line 175
    :pswitch_4
    new-instance v1, LxW;

    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwG;

    iget-object v0, v0, LwG;->c:Lbsk;

    .line 178
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lyh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LwG;

    iget-object v2, v2, LwG;->c:Lbsk;

    .line 176
    invoke-static {v0, v2}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwH;

    invoke-direct {v1, v0}, LxW;-><init>(LwH;)V

    move-object v0, v1

    .line 183
    goto :goto_0

    .line 185
    :pswitch_5
    new-instance v2, LxR;

    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->a:Lbsk;

    .line 188
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lyh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->a:Lbsk;

    .line 186
    invoke-static {v0, v1}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTl;

    iget-object v1, p0, Lyh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lyh;

    iget-object v1, v1, Lyh;->b:Lbsk;

    .line 194
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, Lyh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lyh;

    iget-object v3, v3, Lyh;->b:Lbsk;

    .line 192
    invoke-static {v1, v3}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LxU;

    invoke-direct {v2, v0, v1}, LxR;-><init>(LTl;LxU;)V

    move-object v0, v2

    .line 199
    goto/16 :goto_0

    .line 201
    :pswitch_6
    new-instance v0, LxY;

    iget-object v1, p0, Lyh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->D:Lbsk;

    .line 204
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lyh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->D:Lbsk;

    .line 202
    invoke-static {v1, v2}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lyh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 210
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lyh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 208
    invoke-static {v2, v3}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, Lyh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lyh;

    iget-object v3, v3, Lyh;->l:Lbsk;

    .line 216
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lyh;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lyh;

    iget-object v4, v4, Lyh;->l:Lbsk;

    .line 214
    invoke-static {v3, v4}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LxV;

    iget-object v4, p0, Lyh;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lyh;

    iget-object v4, v4, Lyh;->h:Lbsk;

    .line 222
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lyh;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lyh;

    iget-object v5, v5, Lyh;->h:Lbsk;

    .line 220
    invoke-static {v4, v5}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LxQ;

    iget-object v5, p0, Lyh;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lyh;

    iget-object v5, v5, Lyh;->j:Lbsk;

    .line 228
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lyh;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lyh;

    iget-object v6, v6, Lyh;->j:Lbsk;

    .line 226
    invoke-static {v5, v6}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lyb;

    invoke-direct/range {v0 .. v5}, LxY;-><init>(Ljava/util/concurrent/Executor;LQr;LxV;LxQ;Lyb;)V

    goto/16 :goto_0

    .line 235
    :pswitch_7
    new-instance v0, Lyf;

    iget-object v1, p0, Lyh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->m:Lbsk;

    .line 238
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lyh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LarZ;

    iget-object v2, v2, LarZ;->m:Lbsk;

    .line 236
    invoke-static {v1, v2}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LasR;

    iget-object v2, p0, Lyh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 244
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lyh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 242
    invoke-static {v2, v3}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    iget-object v3, p0, Lyh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lyh;

    iget-object v3, v3, Lyh;->l:Lbsk;

    .line 250
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lyh;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lyh;

    iget-object v4, v4, Lyh;->l:Lbsk;

    .line 248
    invoke-static {v3, v4}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LxV;

    iget-object v4, p0, Lyh;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lyh;

    iget-object v4, v4, Lyh;->a:Lbsk;

    .line 256
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lyh;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lyh;

    iget-object v5, v5, Lyh;->a:Lbsk;

    .line 254
    invoke-static {v4, v5}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lyd;

    iget-object v5, p0, Lyh;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lyh;

    iget-object v5, v5, Lyh;->j:Lbsk;

    .line 262
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lyh;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lyh;

    iget-object v6, v6, Lyh;->j:Lbsk;

    .line 260
    invoke-static {v5, v6}, Lyh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lyb;

    invoke-direct/range {v0 .. v5}, Lyf;-><init>(LasR;LtK;LxV;Lyd;Lyb;)V

    goto/16 :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x75
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_4
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 284
    sparse-switch p2, :sswitch_data_0

    .line 331
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :sswitch_0
    check-cast p1, Lyg;

    .line 288
    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->e:Lbsk;

    .line 291
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxR;

    .line 288
    invoke-virtual {p1, v0}, Lyg;->provideCrossAppPromoCatalogFetcher(LxR;)LxQ;

    move-result-object v0

    .line 324
    :goto_0
    return-object v0

    .line 295
    :sswitch_1
    check-cast p1, Lyg;

    .line 297
    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->f:Lbsk;

    .line 300
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxY;

    .line 297
    invoke-virtual {p1, v0}, Lyg;->provideCrossAppPromoFetchingManager(LxY;)LxX;

    move-result-object v0

    goto :goto_0

    .line 304
    :sswitch_2
    check-cast p1, Lyg;

    .line 306
    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->c:Lbsk;

    .line 309
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyc;

    .line 306
    invoke-virtual {p1, v0}, Lyg;->provideCrossAppPromoInformationStore(Lyc;)Lyb;

    move-result-object v0

    goto :goto_0

    .line 313
    :sswitch_3
    check-cast p1, Lyg;

    .line 315
    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->g:Lbsk;

    .line 318
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyf;

    .line 315
    invoke-virtual {p1, v0}, Lyg;->provideCrossAppPromoManager(Lyf;)Lye;

    move-result-object v0

    goto :goto_0

    .line 322
    :sswitch_4
    check-cast p1, Lyg;

    .line 324
    iget-object v0, p0, Lyh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->d:Lbsk;

    .line 327
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxW;

    .line 324
    invoke-virtual {p1, v0}, Lyg;->provideCrossAppPromoFetcher(LxW;)LxV;

    move-result-object v0

    goto :goto_0

    .line 284
    nop

    :sswitch_data_0
    .sparse-switch
        0x74 -> :sswitch_0
        0x76 -> :sswitch_1
        0x78 -> :sswitch_2
        0x7e -> :sswitch_3
        0x82 -> :sswitch_4
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 93
    const-class v0, Lyd;

    iget-object v1, p0, Lyh;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 94
    const-class v0, LxU;

    iget-object v1, p0, Lyh;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 95
    const-class v0, Lyc;

    iget-object v1, p0, Lyh;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 96
    const-class v0, LxW;

    iget-object v1, p0, Lyh;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 97
    const-class v0, LxR;

    iget-object v1, p0, Lyh;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 98
    const-class v0, LxY;

    iget-object v1, p0, Lyh;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 99
    const-class v0, Lyf;

    iget-object v1, p0, Lyh;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 100
    const-class v0, LxQ;

    iget-object v1, p0, Lyh;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 101
    const-class v0, LxX;

    iget-object v1, p0, Lyh;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 102
    const-class v0, Lyb;

    iget-object v1, p0, Lyh;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 103
    const-class v0, Lye;

    iget-object v1, p0, Lyh;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 104
    const-class v0, LxV;

    iget-object v1, p0, Lyh;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyh;->a(Ljava/lang/Class;Lbsk;)V

    .line 105
    iget-object v0, p0, Lyh;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x7a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 107
    iget-object v0, p0, Lyh;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x7c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 109
    iget-object v0, p0, Lyh;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x79

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 111
    iget-object v0, p0, Lyh;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x80

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 113
    iget-object v0, p0, Lyh;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x75

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 115
    iget-object v0, p0, Lyh;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x77

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 117
    iget-object v0, p0, Lyh;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x7f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 119
    iget-object v0, p0, Lyh;->h:Lbsk;

    const-class v1, Lyg;

    const/16 v2, 0x74

    invoke-virtual {p0, v1, v2}, Lyh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 121
    iget-object v0, p0, Lyh;->i:Lbsk;

    const-class v1, Lyg;

    const/16 v2, 0x76

    invoke-virtual {p0, v1, v2}, Lyh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 123
    iget-object v0, p0, Lyh;->j:Lbsk;

    const-class v1, Lyg;

    const/16 v2, 0x78

    invoke-virtual {p0, v1, v2}, Lyh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 125
    iget-object v0, p0, Lyh;->k:Lbsk;

    const-class v1, Lyg;

    const/16 v2, 0x7e

    invoke-virtual {p0, v1, v2}, Lyh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 127
    iget-object v0, p0, Lyh;->l:Lbsk;

    const-class v1, Lyg;

    const/16 v2, 0x82

    invoke-virtual {p0, v1, v2}, Lyh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 129
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 276
    .line 278
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method
