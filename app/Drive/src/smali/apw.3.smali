.class public Lapw;
.super Ljava/lang/Object;
.source "DocListView.java"

# interfaces
.implements Lafy;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/view/DocListView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/view/DocListView;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lapw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lafx;)V
    .locals 4

    .prologue
    .line 246
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    .line 247
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;Lafx;)Lafx;

    .line 248
    sget-object v0, Lapz;->a:[I

    invoke-virtual {p1}, Lafx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 257
    const-string v0, "DocListView"

    const-string v1, "Unexpected sync more state: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 261
    :goto_0
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    .line 262
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 263
    return-void

    .line 251
    :pswitch_0
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v0, v0, Lcom/google/android/apps/docs/view/DocListView;->f:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBI;

    invoke-virtual {v0}, LBI;->b()V

    goto :goto_0

    .line 254
    :pswitch_1
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v0, v0, Lcom/google/android/apps/docs/view/DocListView;->f:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBI;

    invoke-virtual {v0}, LBI;->a()V

    goto :goto_0

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
