.class LbsP;
.super LbsC;
.source "Futures.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "LbsC",
        "<TO;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private a:LbsF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsF",
            "<-TI;+TO;>;"
        }
    .end annotation
.end field

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<+TI;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/CountDownLatch;

.field private volatile b:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<+TO;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LbsF;LbsU;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsF",
            "<-TI;+TO;>;",
            "LbsU",
            "<+TI;>;)V"
        }
    .end annotation

    .prologue
    .line 788
    invoke-direct {p0}, LbsC;-><init>()V

    .line 785
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    .line 789
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbsF;

    iput-object v0, p0, LbsP;->a:LbsF;

    .line 790
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbsU;

    iput-object v0, p0, LbsP;->a:LbsU;

    .line 791
    return-void
.end method

.method synthetic constructor <init>(LbsF;LbsU;LbsL;)V
    .locals 0

    .prologue
    .line 780
    invoke-direct {p0, p1, p2}, LbsP;-><init>(LbsF;LbsU;)V

    return-void
.end method

.method static synthetic a(LbsP;LbsU;)LbsU;
    .locals 0

    .prologue
    .line 780
    iput-object p1, p0, LbsP;->b:LbsU;

    return-object p1
.end method

.method private a(Ljava/util/concurrent/Future;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 810
    if-eqz p1, :cond_0

    .line 811
    invoke-interface {p1, p2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 813
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1

    .prologue
    .line 799
    invoke-super {p0, p1}, LbsC;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, LbsP;->a:LbsU;

    invoke-direct {p0, v0, p1}, LbsP;->a(Ljava/util/concurrent/Future;Z)V

    .line 803
    iget-object v0, p0, LbsP;->b:LbsU;

    invoke-direct {p0, v0, p1}, LbsP;->a(Ljava/util/concurrent/Future;Z)V

    .line 804
    const/4 v0, 0x1

    .line 806
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 819
    :try_start_0
    iget-object v0, p0, LbsP;->a:LbsU;

    invoke-static {v0}, Lbtf;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 832
    :try_start_1
    iget-object v1, p0, LbsP;->a:LbsF;

    invoke-interface {v1, v0}, LbsF;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    iput-object v0, p0, LbsP;->b:LbsU;

    .line 834
    invoke-virtual {p0}, LbsP;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 835
    invoke-virtual {p0}, LbsP;->a()Z

    move-result v1

    invoke-interface {v0, v1}, LbsU;->cancel(Z)Z

    .line 836
    const/4 v0, 0x0

    iput-object v0, p0, LbsP;->b:LbsU;
    :try_end_1
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 874
    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 879
    :goto_0
    return-void

    .line 820
    :catch_0
    move-exception v0

    .line 824
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {p0, v0}, LbsP;->cancel(Z)Z
    :try_end_2
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 874
    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 826
    :catch_1
    move-exception v0

    .line 828
    :try_start_3
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LbsP;->a(Ljava/lang/Throwable;)Z
    :try_end_3
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 874
    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 839
    :cond_0
    :try_start_4
    new-instance v1, LbsQ;

    invoke-direct {v1, p0, v0}, LbsQ;-><init>(LbsP;LbsU;)V

    invoke-static {}, LbsY;->a()LbsW;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LbsU;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_4
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 874
    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 862
    :catch_2
    move-exception v0

    .line 864
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/reflect/UndeclaredThrowableException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LbsP;->a(Ljava/lang/Throwable;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 874
    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 865
    :catch_3
    move-exception v0

    .line 868
    :try_start_6
    invoke-virtual {p0, v0}, LbsP;->a(Ljava/lang/Throwable;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 874
    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 869
    :catch_4
    move-exception v0

    .line 871
    :try_start_7
    invoke-virtual {p0, v0}, LbsP;->a(Ljava/lang/Throwable;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 874
    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v0, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 874
    :catchall_0
    move-exception v0

    iput-object v3, p0, LbsP;->a:LbsF;

    .line 875
    iput-object v3, p0, LbsP;->a:LbsU;

    .line 877
    iget-object v1, p0, LbsP;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
