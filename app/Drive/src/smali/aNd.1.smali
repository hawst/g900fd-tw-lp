.class public final LaNd;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final a:Landroid/location/Location;

.field private final a:Landroid/os/Bundle;

.field private final a:LayN;

.field private final a:Ljava/util/Date;

.field private final a:Ljava/util/Map;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:I

.field private final b:Ljava/lang/String;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "emulator"

    invoke-static {v0}, LaQb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LaNd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LaNe;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LaNd;-><init>(LaNe;LayN;)V

    return-void
.end method

.method public constructor <init>(LaNe;LayN;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, LaNe;->a(LaNe;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, LaNd;->a:Ljava/util/Date;

    invoke-static {p1}, LaNe;->a(LaNe;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaNd;->b:Ljava/lang/String;

    invoke-static {p1}, LaNe;->a(LaNe;)I

    move-result v0

    iput v0, p0, LaNd;->a:I

    invoke-static {p1}, LaNe;->a(LaNe;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaNd;->a:Ljava/util/Set;

    invoke-static {p1}, LaNe;->a(LaNe;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, LaNd;->a:Landroid/location/Location;

    invoke-static {p1}, LaNe;->a(LaNe;)Z

    move-result v0

    iput-boolean v0, p0, LaNd;->a:Z

    invoke-static {p1}, LaNe;->a(LaNe;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, LaNd;->a:Landroid/os/Bundle;

    invoke-static {p1}, LaNe;->a(LaNe;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LaNd;->a:Ljava/util/Map;

    invoke-static {p1}, LaNe;->b(LaNe;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaNd;->c:Ljava/lang/String;

    iput-object p2, p0, LaNd;->a:LayN;

    invoke-static {p1}, LaNe;->b(LaNe;)I

    move-result v0

    iput v0, p0, LaNd;->b:I

    invoke-static {p1}, LaNe;->b(LaNe;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaNd;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LaNd;->a:I

    return v0
.end method

.method public a()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, LaNd;->a:Landroid/location/Location;

    return-object v0
.end method

.method public a()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, LaNd;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public a(Ljava/lang/Class;)Landroid/os/Bundle;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LayD;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    iget-object v0, p0, LaNd;->a:Landroid/os/Bundle;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public a()LayN;
    .locals 1

    iget-object v0, p0, LaNd;->a:LayN;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaNd;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, LaNd;->a:Ljava/util/Date;

    return-object v0
.end method

.method public a()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, LaNd;->a:Ljava/util/Map;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, LaNd;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, LaNd;->a:Z

    return v0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 2

    iget-object v0, p0, LaNd;->b:Ljava/util/Set;

    invoke-static {p1}, LaQb;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LaNd;->b:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaNd;->c:Ljava/lang/String;

    return-object v0
.end method
