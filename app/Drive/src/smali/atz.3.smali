.class public Latz;
.super Ljava/lang/Object;
.source "WelcomeFragment.java"


# instance fields
.field private final a:Landroid/widget/ProgressBar;


# direct methods
.method private constructor <init>(Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Latz;->a:Landroid/widget/ProgressBar;

    .line 88
    return-void
.end method

.method public synthetic constructor <init>(Landroid/widget/ProgressBar;Latp;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Latz;-><init>(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    iget-object v0, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 108
    iget-object v0, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 109
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 92
    return-void
.end method

.method public static synthetic a(Latz;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Latz;->a()V

    return-void
.end method

.method public static synthetic a(Latz;I)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Latz;->a(I)V

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getSecondaryProgress()I

    move-result v0

    iget-object v1, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Latz;)Z
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Latz;->a()Z

    move-result v0

    return v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 95
    if-ge p1, v0, :cond_0

    move p1, v0

    .line 98
    :cond_0
    iget-object v0, p0, Latz;->a:Landroid/widget/ProgressBar;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 99
    iget-object v0, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 100
    return-void
.end method

.method static synthetic b(Latz;I)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Latz;->b(I)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 113
    const-string v0, "%s of %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getSecondaryProgress()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Latz;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
