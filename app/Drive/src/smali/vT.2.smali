.class public final LvT;
.super Ljava/lang/Object;
.source "EntryCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;
    .locals 2

    .prologue
    .line 47
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 47
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 49
    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;
    .locals 1

    .prologue
    .line 54
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, LvT;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, LvT;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    move-result-object v0

    return-object v0
.end method
