.class public final LTk;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTl;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTe;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTj;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTb;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTg;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTF;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSZ;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTh;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTO;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTc;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 48
    iput-object p1, p0, LTk;->a:LbrA;

    .line 49
    const-class v0, LTl;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->a:Lbsk;

    .line 52
    const-class v0, LTe;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->b:Lbsk;

    .line 55
    const-class v0, LTj;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->c:Lbsk;

    .line 58
    const-class v0, LTb;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->d:Lbsk;

    .line 61
    const-class v0, LTg;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->e:Lbsk;

    .line 64
    const-class v0, LTF;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->f:Lbsk;

    .line 67
    const-class v0, LSZ;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->g:Lbsk;

    .line 70
    const-class v0, LTh;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->h:Lbsk;

    .line 73
    const-class v0, LTO;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->i:Lbsk;

    .line 76
    const-class v0, LTc;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->j:Lbsk;

    .line 79
    const-class v0, LTd;

    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTk;->k:Lbsk;

    .line 82
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 130
    sparse-switch p1, :sswitch_data_0

    .line 206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :sswitch_0
    new-instance v1, LTl;

    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->f:Lbsk;

    .line 135
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LTk;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->f:Lbsk;

    .line 133
    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTF;

    invoke-direct {v1, v0}, LTl;-><init>(LTF;)V

    move-object v0, v1

    .line 204
    :goto_0
    return-object v0

    .line 142
    :sswitch_1
    new-instance v4, LTe;

    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->g:Lbsk;

    .line 145
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LTk;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->g:Lbsk;

    .line 143
    invoke-static {v0, v1}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSZ;

    iget-object v1, p0, LTk;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->f:Lbsk;

    .line 151
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LTk;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->f:Lbsk;

    .line 149
    invoke-static {v1, v2}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTF;

    iget-object v2, p0, LTk;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 157
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LTk;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 155
    invoke-static {v2, v3}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, LTk;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LqD;

    iget-object v3, v3, LqD;->c:Lbsk;

    .line 163
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LTk;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LqD;

    iget-object v5, v5, LqD;->c:Lbsk;

    .line 161
    invoke-static {v3, v5}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LqK;

    invoke-direct {v4, v0, v1, v2, v3}, LTe;-><init>(LSZ;LTF;LQr;LqK;)V

    move-object v0, v4

    .line 168
    goto :goto_0

    .line 170
    :sswitch_2
    new-instance v2, LTj;

    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->k:Lbsk;

    .line 173
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LTk;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->k:Lbsk;

    .line 171
    invoke-static {v0, v1}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTd;

    iget-object v1, p0, LTk;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->f:Lbsk;

    .line 179
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LTk;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTk;

    iget-object v3, v3, LTk;->f:Lbsk;

    .line 177
    invoke-static {v1, v3}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTF;

    invoke-direct {v2, v0, v1}, LTj;-><init>(LTd;LTF;)V

    move-object v0, v2

    .line 184
    goto/16 :goto_0

    .line 186
    :sswitch_3
    new-instance v1, LTb;

    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 189
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LTk;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 187
    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, LTb;-><init>(Laja;)V

    move-object v0, v1

    .line 194
    goto/16 :goto_0

    .line 196
    :sswitch_4
    new-instance v1, LTg;

    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->h:Lbsk;

    .line 199
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LTk;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->h:Lbsk;

    .line 197
    invoke-static {v0, v2}, LTk;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTh;

    invoke-direct {v1, v0}, LTg;-><init>(LTh;)V

    move-object v0, v1

    .line 204
    goto/16 :goto_0

    .line 130
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_2
        0x81 -> :sswitch_0
        0x98 -> :sswitch_1
        0x9a -> :sswitch_3
        0x9e -> :sswitch_4
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 221
    sparse-switch p2, :sswitch_data_0

    .line 273
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :sswitch_0
    check-cast p1, LTn;

    .line 225
    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTz;

    iget-object v0, v0, LTz;->b:Lbsk;

    .line 228
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTw;

    .line 225
    invoke-virtual {p1, v0}, LTn;->provideHttpIssuer(LTw;)LTF;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    .line 232
    :sswitch_1
    check-cast p1, LTn;

    .line 234
    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->d:Lbsk;

    .line 237
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTb;

    .line 234
    invoke-virtual {p1, v0}, LTn;->provideAccountAuthenticator(LTb;)LSZ;

    move-result-object v0

    goto :goto_0

    .line 241
    :sswitch_2
    check-cast p1, LTn;

    .line 243
    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->c:Lbsk;

    .line 246
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTj;

    .line 243
    invoke-virtual {p1, v0}, LTn;->provideAuthenticatedHttpIssuer(LTj;)LTh;

    move-result-object v0

    goto :goto_0

    .line 250
    :sswitch_3
    check-cast p1, LTn;

    .line 252
    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTz;

    iget-object v0, v0, LTz;->c:Lbsk;

    .line 255
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTQ;

    .line 252
    invoke-virtual {p1, v0}, LTn;->provideUserAgentStringGenerator(LTQ;)LTO;

    move-result-object v0

    goto :goto_0

    .line 259
    :sswitch_4
    check-cast p1, LTn;

    .line 261
    invoke-virtual {p1}, LTn;->provideApplicationContextProvider()LTc;

    move-result-object v0

    goto :goto_0

    .line 264
    :sswitch_5
    check-cast p1, LTn;

    .line 266
    iget-object v0, p0, LTk;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->b:Lbsk;

    .line 269
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTe;

    .line 266
    invoke-virtual {p1, v0}, LTn;->provideAuthTokenManager(LTe;)LTd;

    move-result-object v0

    goto :goto_0

    .line 221
    :sswitch_data_0
    .sparse-switch
        0x44 -> :sswitch_2
        0x8c -> :sswitch_0
        0x94 -> :sswitch_5
        0x99 -> :sswitch_1
        0x9b -> :sswitch_3
        0x9d -> :sswitch_4
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 89
    const-class v0, LTl;

    iget-object v1, p0, LTk;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 90
    const-class v0, LTe;

    iget-object v1, p0, LTk;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 91
    const-class v0, LTj;

    iget-object v1, p0, LTk;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 92
    const-class v0, LTb;

    iget-object v1, p0, LTk;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 93
    const-class v0, LTg;

    iget-object v1, p0, LTk;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 94
    const-class v0, LTF;

    iget-object v1, p0, LTk;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 95
    const-class v0, LSZ;

    iget-object v1, p0, LTk;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 96
    const-class v0, LTh;

    iget-object v1, p0, LTk;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 97
    const-class v0, LTO;

    iget-object v1, p0, LTk;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 98
    const-class v0, LTc;

    iget-object v1, p0, LTk;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 99
    const-class v0, LTd;

    iget-object v1, p0, LTk;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, LTk;->a(Ljava/lang/Class;Lbsk;)V

    .line 100
    iget-object v0, p0, LTk;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x81

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 102
    iget-object v0, p0, LTk;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x98

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 104
    iget-object v0, p0, LTk;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x64

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 106
    iget-object v0, p0, LTk;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x9a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 108
    iget-object v0, p0, LTk;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x9e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 110
    iget-object v0, p0, LTk;->f:Lbsk;

    const-class v1, LTn;

    const/16 v2, 0x8c

    invoke-virtual {p0, v1, v2}, LTk;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 112
    iget-object v0, p0, LTk;->g:Lbsk;

    const-class v1, LTn;

    const/16 v2, 0x99

    invoke-virtual {p0, v1, v2}, LTk;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 114
    iget-object v0, p0, LTk;->h:Lbsk;

    const-class v1, LTn;

    const/16 v2, 0x44

    invoke-virtual {p0, v1, v2}, LTk;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 116
    iget-object v0, p0, LTk;->i:Lbsk;

    const-class v1, LTn;

    const/16 v2, 0x9b

    invoke-virtual {p0, v1, v2}, LTk;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 118
    iget-object v0, p0, LTk;->j:Lbsk;

    const-class v1, LTn;

    const/16 v2, 0x9d

    invoke-virtual {p0, v1, v2}, LTk;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 120
    iget-object v0, p0, LTk;->k:Lbsk;

    const-class v1, LTn;

    const/16 v2, 0x94

    invoke-virtual {p0, v1, v2}, LTk;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 122
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 213
    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method
