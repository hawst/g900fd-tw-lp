.class LWC;
.super Landroid/os/AsyncTask;
.source "KitKatPrintActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LWB;

.field final synthetic a:Landroid/os/CancellationSignal;

.field final synthetic a:Landroid/os/ParcelFileDescriptor;

.field final synthetic a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;


# direct methods
.method constructor <init>(LWB;Landroid/os/CancellationSignal;Landroid/os/ParcelFileDescriptor;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, LWC;->a:LWB;

    iput-object p2, p0, LWC;->a:Landroid/os/CancellationSignal;

    iput-object p3, p0, LWC;->a:Landroid/os/ParcelFileDescriptor;

    iput-object p4, p0, LWC;->a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 115
    :try_start_0
    iget-object v0, p0, LWC;->a:Landroid/os/CancellationSignal;

    new-instance v1, LWD;

    invoke-direct {v1, p0}, LWD;-><init>(Landroid/os/AsyncTask;)V

    invoke-virtual {v0, v1}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 116
    iget-object v0, p0, LWC;->a:LWB;

    iget-object v0, v0, LWB;->a:Lcom/google/android/apps/docs/print/KitKatPrintActivity;

    iget-object v1, p0, LWC;->a:LWB;

    iget-object v1, v1, LWB;->a:Landroid/net/Uri;

    iget-object v2, p0, LWC;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->a(Lcom/google/android/apps/docs/print/KitKatPrintActivity;Landroid/net/Uri;Landroid/os/ParcelFileDescriptor;)V

    .line 117
    invoke-virtual {p0}, LWC;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    const-string v0, "PrintActivity"

    const-string v1, "onWriteFinished"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, LWC;->a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/print/PageRange;

    const/4 v2, 0x0

    sget-object v3, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V

    .line 132
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 125
    :cond_0
    const-string v0, "PrintActivity"

    const-string v1, "Write task cancelled, calling onWriteFailed."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v0, p0, LWC;->a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    const-string v1, "Print job cancelled."

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const-string v1, "PrintActivity"

    const-string v2, "Printing document failed."

    invoke-static {v1, v2, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 130
    iget-object v1, p0, LWC;->a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LWC;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
