.class public Laua;
.super Ljava/lang/Object;
.source "ProjectorChrome.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private a:I

.field private final a:Landroid/animation/ValueAnimator;

.field private a:Laue;

.field private final a:Laur;

.field private final a:LhJ;

.field private final a:LhN;

.field private a:Z

.field private b:I

.field private b:Z


# direct methods
.method public constructor <init>(LhN;Laur;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Laua;->a:LhN;

    .line 59
    invoke-virtual {p1}, LhN;->a()LhJ;

    move-result-object v0

    iput-object v0, p0, Laua;->a:LhJ;

    .line 60
    iput-object p2, p0, Laua;->a:Laur;

    .line 61
    const/4 v0, 0x0

    new-array v0, v0, [F

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Laua;->a:Landroid/animation/ValueAnimator;

    .line 62
    invoke-direct {p0}, Laua;->b()V

    .line 63
    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Laua;->a:LhJ;

    invoke-virtual {v0}, LhJ;->b()I

    move-result v0

    .line 274
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 275
    invoke-direct {p0}, Laua;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    :cond_0
    return v0
.end method

.method static synthetic a(Laua;)I
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Laua;->d()I

    move-result v0

    return v0
.end method

.method private a()Landroid/view/View;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Laua;->a:LhN;

    invoke-virtual {v0}, LhN;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laua;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Laua;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laua;)Laue;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Laua;->a:Laue;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 380
    iget-object v0, p0, Laua;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    invoke-direct {p0}, Laua;->b()Landroid/view/View;

    move-result-object v0

    .line 384
    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 388
    if-eq v0, p1, :cond_0

    .line 392
    iget-object v1, p0, Laua;->a:Landroid/animation/ValueAnimator;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    int-to-float v0, v0

    aput v0, v2, v3

    const/4 v0, 0x1

    int-to-float v3, p1

    aput v3, v2, v0

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 393
    iget-object v0, p0, Laua;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method static synthetic a(Laua;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Laua;->d()V

    return-void
.end method

.method static synthetic a(Laua;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Laua;->a(I)V

    return-void
.end method

.method static synthetic a(Laua;Laue;D)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Laua;->a(Laue;D)V

    return-void
.end method

.method private a(Laue;D)V
    .locals 2

    .prologue
    .line 263
    invoke-direct {p0}, Laua;->a()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, p2

    double-to-int v0, v0

    .line 266
    if-lez v0, :cond_0

    .line 267
    invoke-interface {p1, v0}, Laue;->a(I)V

    .line 269
    :cond_0
    return-void
.end method

.method static synthetic a(Laua;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Laua;->a:Z

    return v0
.end method

.method static synthetic a(Laua;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Laua;->a:Z

    return p1
.end method

.method private b()I
    .locals 2

    .prologue
    .line 283
    iget v0, p0, Laua;->a:I

    if-nez v0, :cond_0

    .line 284
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 285
    iget-object v1, p0, Laua;->a:LhN;

    invoke-virtual {v1}, LhN;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 286
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 287
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p0, Laua;->a:I

    .line 289
    :cond_0
    iget v0, p0, Laua;->a:I

    return v0
.end method

.method static synthetic b(Laua;)I
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Laua;->e()I

    move-result v0

    return v0
.end method

.method private b()Landroid/view/View;
    .locals 4

    .prologue
    .line 223
    iget-object v0, p0, Laua;->a:LhN;

    sget v1, Lauj;->action_bar_container:I

    invoke-virtual {v0, v1}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 224
    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Laua;->a:LhN;

    invoke-virtual {v0}, LhN;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 228
    const-string v1, "action_bar_container"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 229
    iget-object v1, p0, Laua;->a:LhN;

    invoke-virtual {v1, v0}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 230
    if-nez v0, :cond_0

    .line 231
    const/4 v0, 0x0

    .line 240
    :goto_0
    return-object v0

    :cond_0
    move-object v1, v0

    .line 236
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 237
    instance-of v2, v0, Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 238
    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 240
    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-direct {p0}, Laua;->a()Landroid/view/View;

    move-result-object v0

    new-instance v1, Laub;

    invoke-direct {v1, p0}, Laub;-><init>(Laua;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 81
    iget-object v0, p0, Laua;->a:Landroid/animation/ValueAnimator;

    new-instance v1, Lauc;

    invoke-direct {v1, p0}, Lauc;-><init>(Laua;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 88
    iget-object v0, p0, Laua;->a:LhJ;

    iget-object v1, p0, Laua;->a:Laur;

    invoke-virtual {v1}, Laur;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LhJ;->a(Z)V

    .line 89
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Laua;->a:LhJ;

    iget-object v1, p0, Laua;->a:LhN;

    invoke-virtual {v1}, LhN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Laui;->back_white:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LhJ;->a(Landroid/graphics/drawable/Drawable;)V

    .line 93
    :cond_0
    invoke-direct {p0}, Laua;->c()Landroid/view/View;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_1

    .line 95
    iget-object v1, p0, Laua;->a:LhN;

    .line 96
    invoke-virtual {v1}, LhN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lauh;->viewer_action_bar_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 97
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 101
    :cond_1
    invoke-virtual {p0}, Laua;->a()V

    .line 102
    iget-object v0, p0, Laua;->a:LhJ;

    invoke-virtual {v0}, LhJ;->a()V

    .line 103
    return-void
.end method

.method static synthetic b(Laua;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Laua;->c()V

    return-void
.end method

.method static synthetic b(Laua;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Laua;->b:Z

    return v0
.end method

.method private c()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Laua;->b:I

    if-nez v0, :cond_0

    .line 295
    invoke-direct {p0}, Laua;->b()Landroid/view/View;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Laua;->b:I

    .line 300
    :cond_0
    iget v0, p0, Laua;->b:I

    return v0
.end method

.method private c()Landroid/view/View;
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Laua;->a:LhN;

    invoke-virtual {v0}, LhN;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 254
    const-string v1, "action_bar"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 256
    if-nez v0, :cond_0

    .line 257
    const/4 v0, 0x0

    .line 259
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Laua;->a:LhN;

    invoke-virtual {v1, v0}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 359
    invoke-direct {p0}, Laua;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x505

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 363
    return-void
.end method

.method private d()I
    .locals 2

    .prologue
    .line 398
    invoke-direct {p0}, Laua;->b()I

    move-result v0

    invoke-direct {p0}, Laua;->c()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 375
    invoke-direct {p0}, Laua;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x500

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 376
    return-void
.end method

.method private e()I
    .locals 1

    .prologue
    .line 403
    invoke-direct {p0}, Laua;->b()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Lmv;)Lmu;
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Laua;->a:LhN;

    invoke-virtual {v0, p1}, LhN;->a(Lmv;)Lmu;

    move-result-object v0

    .line 354
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Laua;->a(Z)V

    .line 355
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 370
    invoke-direct {p0}, Laua;->d()V

    .line 371
    invoke-direct {p0}, Laua;->e()I

    move-result v0

    invoke-direct {p0, v0}, Laua;->a(I)V

    .line 372
    return-void
.end method

.method public a(Laue;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    if-nez p1, :cond_1

    .line 134
    iget-object v0, p0, Laua;->a:Laue;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Laua;->a:Laue;

    invoke-interface {v0, v1}, Laue;->a(Lawf;)V

    .line 136
    iput-object v1, p0, Laua;->a:Laue;

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iput-object p1, p0, Laua;->a:Laue;

    .line 142
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, p1, v0, v1}, Laua;->a(Laue;D)V

    .line 143
    new-instance v0, Laud;

    invoke-direct {v0, p0}, Laud;-><init>(Laua;)V

    invoke-interface {p1, v0}, Laue;->a(Lawf;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Laua;->a:LhJ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LhJ;->b(Z)V

    .line 116
    iget-object v0, p0, Laua;->a:LhJ;

    invoke-virtual {v0, p1}, LhJ;->a(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Laua;->a:LhJ;

    invoke-virtual {v0, p2}, LhJ;->a(I)V

    .line 118
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 121
    iput-boolean p1, p0, Laua;->b:Z

    .line 122
    if-eqz p1, :cond_0

    .line 123
    invoke-virtual {p0}, Laua;->a()V

    .line 125
    :cond_0
    return-void
.end method

.method public a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 324
    const v1, 0x102002c

    if-ne p1, v1, :cond_1

    .line 325
    iget-object v1, p0, Laua;->a:LhN;

    invoke-virtual {v1}, LhN;->finish()V

    .line 326
    invoke-static {}, LavO;->a()LavT;

    move-result-object v1

    const-string v2, "home"

    invoke-virtual {v1, v2}, LavT;->a(Ljava/lang/String;)V

    .line 348
    :cond_0
    :goto_0
    return v0

    .line 328
    :cond_1
    sget v1, Lauj;->action_find:I

    if-eq p1, v1, :cond_0

    .line 330
    sget v1, Lauj;->action_edit:I

    if-ne p1, v1, :cond_2

    .line 331
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->j()Z

    move-result v0

    goto :goto_0

    .line 332
    :cond_2
    sget v1, Lauj;->action_add_to_drive:I

    if-ne p1, v1, :cond_3

    .line 333
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->b()Z

    move-result v0

    goto :goto_0

    .line 334
    :cond_3
    sget v1, Lauj;->action_send:I

    if-ne p1, v1, :cond_4

    .line 335
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->a()Z

    move-result v0

    goto :goto_0

    .line 336
    :cond_4
    sget v1, Lauj;->action_print:I

    if-ne p1, v1, :cond_5

    .line 337
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->g()Z

    move-result v0

    goto :goto_0

    .line 338
    :cond_5
    sget v1, Lauj;->action_details:I

    if-eq p1, v1, :cond_0

    .line 340
    sget v0, Lauj;->action_download:I

    if-ne p1, v0, :cond_6

    .line 341
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->i()Z

    move-result v0

    goto :goto_0

    .line 342
    :cond_6
    sget v0, Lauj;->action_share_link:I

    if-ne p1, v0, :cond_7

    .line 343
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->k()Z

    move-result v0

    goto :goto_0

    .line 344
    :cond_7
    sget v0, Lauj;->action_add_people:I

    if-ne p1, v0, :cond_8

    .line 345
    iget-object v0, p0, Laua;->a:Laur;

    invoke-virtual {v0}, Laur;->l()Z

    move-result v0

    goto :goto_0

    .line 348
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method
