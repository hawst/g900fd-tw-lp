.class public final LvI;
.super Ljava/lang/Object;
.source "AccountCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;-><init>(Landroid/os/Parcel;LvI;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;
    .locals 1

    .prologue
    .line 82
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0, p1}, LvI;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0, p1}, LvI;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    move-result-object v0

    return-object v0
.end method
