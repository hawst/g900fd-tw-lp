.class public LaHs;
.super Ljava/lang/Object;
.source "OperationQueueImpl.java"

# interfaces
.implements LaHr;
.implements Lagw;
.implements Lvk;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:I

.field private final a:LUx;

.field private final a:LZT;

.field private final a:LaGg;

.field private final a:LaHD;

.field private final a:LaKR;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LaHo;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LaFO;",
            "Ljava/util/Queue",
            "<",
            "LaGX;",
            ">;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/concurrent/Executor;

.field private final a:LtK;

.field private final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LaFO;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGg;Ljava/util/concurrent/Executor;LQr;LaHx;Landroid/content/Context;LUx;LaHD;LaKR;LtK;)V
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LaHs;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 109
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LaHs;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 115
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LaHs;->a:Ljava/util/Queue;

    .line 92
    iput-object p1, p0, LaHs;->a:LaGg;

    .line 93
    iput-object p2, p0, LaHs;->a:Ljava/util/concurrent/Executor;

    .line 94
    const-string v0, "operationQueueMaxAttempts"

    const/4 v1, 0x4

    invoke-interface {p3, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LaHs;->a:I

    .line 95
    iput-object p4, p0, LaHs;->a:LZT;

    .line 96
    iput-object p5, p0, LaHs;->a:Landroid/content/Context;

    .line 97
    iput-object p6, p0, LaHs;->a:LUx;

    .line 98
    iput-object p7, p0, LaHs;->a:LaHD;

    .line 99
    iput-object p8, p0, LaHs;->a:LaKR;

    .line 100
    iput-object p9, p0, LaHs;->a:LtK;

    .line 101
    return-void
.end method

.method static a(LaFM;LaGg;LaGX;)LaHi;
    .locals 2

    .prologue
    .line 522
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p2}, LaGX;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 523
    invoke-static {p0, p1, v0}, LaHi;->a(LaFM;LaGg;Lorg/json/JSONObject;)LaHi;

    move-result-object v0

    .line 524
    return-object v0
.end method

.method private a(LaFM;)V
    .locals 1

    .prologue
    .line 322
    new-instance v0, LaHt;

    invoke-direct {v0, p0}, LaHt;-><init>(LaHs;)V

    .line 328
    invoke-virtual {p0, p1, v0}, LaHs;->a(LaFM;LaHy;)V

    .line 329
    return-void
.end method

.method private static a(LaGX;LaGe;LaHo;)V
    .locals 3

    .prologue
    .line 304
    const-string v0, "OperationQueueImpl"

    const-string v1, "The eTag we made the operation against is different to the current local etag."

    invoke-static {v0, v1}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    invoke-interface {p2, p1}, LaHo;->a(LaGe;)LaHo;

    move-result-object v0

    .line 306
    if-eqz p0, :cond_0

    .line 307
    new-instance v1, LaHi;

    .line 308
    invoke-virtual {p1}, LaGe;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p2, v0, v2}, LaHi;-><init>(LaHo;LaHo;Ljava/lang/String;)V

    .line 311
    :try_start_0
    invoke-virtual {v1}, LaHi;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    invoke-virtual {p0, v0}, LaGX;->a(Ljava/lang/String;)V

    .line 313
    invoke-virtual {p0}, LaGX;->e()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(LaHi;)V
    .locals 8

    .prologue
    .line 504
    invoke-virtual {p1}, LaHi;->a()LaHo;

    move-result-object v0

    .line 505
    invoke-virtual {p1}, LaHi;->a()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 506
    invoke-interface {v0}, LaHo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 507
    iget-object v3, p0, LaHs;->a:LaGg;

    invoke-interface {v3, v2}, LaGg;->a(LaFO;)LaFM;

    move-result-object v3

    .line 508
    iget-object v4, p0, LaHs;->a:LaGg;

    .line 509
    invoke-interface {v0}, LaHo;->a()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 508
    invoke-interface {v4, v3, v1, v6, v7}, LaGg;->a(LaFM;Ljava/lang/String;J)LaGX;

    move-result-object v0

    .line 510
    const-string v1, "OperationQueueImpl"

    const-string v3, "Save entry in local database:%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 513
    invoke-virtual {p0, v2}, LaHs;->a(LaFO;)Ljava/util/Queue;

    move-result-object v1

    .line 515
    invoke-virtual {v0}, LaGX;->e()V

    .line 516
    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 517
    return-void
.end method

.method private static a(LaHi;LaFM;LaGg;)V
    .locals 3

    .prologue
    .line 260
    invoke-interface {p2}, LaGg;->a()V

    .line 262
    :try_start_0
    invoke-virtual {p0}, LaHi;->b()LaHo;

    move-result-object v0

    .line 263
    invoke-interface {v0}, LaHo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {p2, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v1

    .line 265
    if-eqz v1, :cond_0

    .line 266
    invoke-virtual {v1}, LaGd;->a()LaGe;

    move-result-object v1

    .line 267
    invoke-interface {v0, v1}, LaHo;->a(LaGe;)LaHo;

    .line 268
    invoke-virtual {v1}, LaGe;->e()V

    .line 271
    :cond_0
    invoke-interface {p2}, LaGg;->c()V

    .line 272
    invoke-interface {p2, p1}, LaGg;->b(LaFM;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    invoke-interface {p2}, LaGg;->b()V

    .line 278
    :goto_0
    return-void

    .line 273
    :catch_0
    move-exception v0

    .line 274
    :try_start_1
    const-string v1, "OperationQueueImpl"

    const-string v2, "Failed to save reverted operation in database"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 276
    invoke-interface {p2}, LaGg;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p2}, LaGg;->b()V

    throw v0
.end method

.method static synthetic a(LaHs;LaFM;LaHy;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, LaHs;->b(LaFM;LaHy;)V

    return-void
.end method

.method private static a(LaFM;LaHy;LaGg;Ljava/util/Queue;ILZT;LaHD;LaKR;LtK;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LaHy;",
            "LaGg;",
            "Ljava/util/Queue",
            "<",
            "LaGX;",
            ">;I",
            "LZT;",
            "LaHD;",
            "LaKR;",
            "LtK;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 153
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 157
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 158
    invoke-static {v1}, LbnG;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGX;

    .line 160
    :try_start_0
    invoke-static {p2, p0, v1}, LaHs;->a(LaGg;LaFM;LaGX;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 161
    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaGC; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 163
    :catch_0
    move-exception v4

    .line 167
    const-string v4, "OperationQueueImpl"

    const-string v5, "Failed to get applied operation from pending operation:%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 169
    invoke-virtual {v1}, LaGX;->f()V

    .line 170
    invoke-interface {p3, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 171
    :catch_1
    move-exception v4

    .line 172
    invoke-virtual {v1}, LaGX;->f()V

    .line 173
    invoke-interface {p3, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    :cond_1
    invoke-interface/range {p5 .. p5}, LZT;->a()LZS;

    move-result-object v6

    .line 179
    const/4 v2, 0x0

    .line 180
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 181
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGX;

    .line 182
    :cond_2
    :goto_1
    if-eqz v1, :cond_c

    .line 183
    invoke-interface/range {p7 .. p7}, LaKR;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 184
    const/4 v1, 0x0

    .line 242
    :goto_2
    return v1

    .line 181
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 188
    :cond_4
    :try_start_1
    invoke-static {p2, p0, v1}, LaHs;->a(LaGg;LaFM;LaGX;)Z

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PendingOperation not applicable on Server: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LbiT;->b(ZLjava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch LaGC; {:try_start_1 .. :try_end_1} :catch_3

    .line 199
    invoke-virtual {v1}, LaGX;->b()I

    move-result v3

    .line 200
    const-string v4, "OperationQueueImpl"

    const-string v5, "batchSynchronize attemptCount=%d maxAttempts=%d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    .line 201
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    .line 200
    invoke-static {v4, v5, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 202
    move/from16 v0, p4

    if-le v3, v0, :cond_6

    .line 203
    const/4 v1, 0x1

    .line 204
    goto :goto_2

    .line 190
    :catch_2
    move-exception v1

    .line 192
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Find invalid PendingOperation again?!"

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 193
    :catch_3
    move-exception v3

    .line 194
    invoke-virtual {v1}, LaGX;->f()V

    .line 195
    invoke-interface {p3, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 196
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGX;

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 207
    :cond_6
    const/4 v5, 0x0

    .line 208
    const/4 v4, 0x0

    .line 211
    :try_start_2
    invoke-static {p0, p2, v1}, LaHs;->a(LaFM;LaGg;LaGX;)LaHi;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_4
    .catch LaGC; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 218
    :goto_3
    if-eqz v5, :cond_7

    .line 219
    :try_start_3
    move-object/from16 v0, p6

    invoke-static {v5, p1, v0}, LaHs;->a(LaHi;LaHy;LaHD;)Z
    :try_end_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-nez v3, :cond_7

    .line 220
    const/4 v4, 0x1

    .line 222
    :try_start_4
    invoke-interface {v6}, LZS;->b()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 234
    :cond_7
    :goto_4
    if-nez v4, :cond_2

    .line 235
    invoke-virtual {v1}, LaGX;->f()V

    .line 236
    invoke-interface {p3, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 237
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGX;

    goto/16 :goto_1

    .line 212
    :catch_4
    move-exception v3

    .line 213
    :try_start_5
    new-instance v8, Ljava/lang/RuntimeException;

    const-string v9, "Find invalid PendingOperation again?!"

    invoke-direct {v8, v9, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_5
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 227
    :catch_5
    move-exception v3

    .line 228
    :try_start_6
    const-string v8, "OperationQueueImpl"

    const-string v9, "Applying on the server is not supported by: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-static {v8, v9, v10}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 229
    sget-object v5, Lry;->ab:Lry;

    move-object/from16 v0, p8

    invoke-interface {v0, v5}, LtK;->a(LtJ;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 230
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 234
    :catchall_0
    move-exception v2

    if-nez v4, :cond_8

    .line 235
    invoke-virtual {v1}, LaGX;->f()V

    .line 236
    invoke-interface {p3, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 237
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGX;

    :cond_8
    throw v2

    .line 214
    :catch_6
    move-exception v3

    .line 215
    :try_start_7
    const-string v8, "OperationQueueImpl"

    const-string v9, "Failed to find entry when deserealizing an operation"

    invoke-static {v8, v3, v9}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 237
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 235
    :cond_a
    invoke-virtual {v1}, LaGX;->f()V

    .line 236
    invoke-interface {p3, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 237
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGX;

    goto/16 :goto_1

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 223
    :catch_7
    move-exception v3

    goto :goto_4

    :cond_c
    move v1, v2

    goto/16 :goto_2
.end method

.method private static a(LaGg;LaFM;LaGX;)Z
    .locals 1

    .prologue
    .line 248
    .line 249
    invoke-static {p1, p0, p2}, LaHs;->a(LaFM;LaGg;LaGX;)LaHi;

    move-result-object v0

    .line 250
    invoke-virtual {v0}, LaHi;->a()LaHo;

    move-result-object v0

    .line 251
    invoke-interface {v0}, LaHo;->a()Z

    move-result v0

    return v0
.end method

.method private static a(LaHi;LaHy;LaHD;)Z
    .locals 5

    .prologue
    .line 291
    invoke-virtual {p0}, LaHi;->a()LaHo;

    move-result-object v0

    .line 292
    const-string v1, "OperationQueueImpl"

    const-string v2, "Going to apply operation %s on server."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 293
    invoke-interface {v0, p1, p2}, LaHo;->a(LaHy;LaHD;)Z

    move-result v0

    return v0
.end method

.method private b(LaFM;LaHy;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 418
    .line 420
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v0

    .line 421
    invoke-virtual {p0, v0}, LaHs;->a(LaFO;)Ljava/util/Queue;

    move-result-object v3

    .line 422
    iget-object v1, p0, LaHs;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    invoke-interface {v1, v0, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    :try_start_0
    iget-object v1, p0, LaHs;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    monitor-enter v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 425
    :try_start_1
    iget-object v2, p0, LaHs;->a:LaGg;

    iget v4, p0, LaHs;->a:I

    iget-object v5, p0, LaHs;->a:LZT;

    iget-object v6, p0, LaHs;->a:LaHD;

    iget-object v7, p0, LaHs;->a:LaKR;

    iget-object v8, p0, LaHs;->a:LtK;

    move-object v0, p1

    move-object v1, p2

    invoke-static/range {v0 .. v8}, LaHs;->a(LaFM;LaHy;LaGg;Ljava/util/Queue;ILZT;LaHD;LaKR;LtK;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 428
    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 430
    if-eqz v1, :cond_0

    .line 431
    iget-object v0, p0, LaHs;->a:Landroid/content/Context;

    iget-object v1, p0, LaHs;->a:LUx;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a(Landroid/content/Context;LUx;)V

    .line 435
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, v9, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 436
    return-void

    .line 428
    :catchall_0
    move-exception v0

    move v1, v9

    :goto_0
    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 430
    :catchall_1
    move-exception v0

    move v9, v1

    :goto_1
    if-eqz v9, :cond_1

    .line 431
    iget-object v1, p0, LaHs;->a:Landroid/content/Context;

    iget-object v2, p0, LaHs;->a:LUx;

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a(Landroid/content/Context;LUx;)V

    :cond_1
    throw v0

    .line 430
    :catchall_2
    move-exception v0

    goto :goto_1

    .line 428
    :catchall_3
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method a(LaFO;)Ljava/util/Queue;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            ")",
            "Ljava/util/Queue",
            "<",
            "LaGX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, LaHs;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 125
    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    .line 129
    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v2, p0, LaHs;->a:LaGg;

    .line 130
    invoke-interface {v2, v0}, LaGg;->a(LaFM;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>(Ljava/util/Collection;)V

    .line 131
    iget-object v0, p0, LaHs;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, LaHs;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 134
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 9

    .prologue
    .line 356
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 357
    iget-object v2, p0, LaHs;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(LaFO;)LaFM;

    move-result-object v2

    .line 358
    invoke-virtual {p0, v0}, LaHs;->a(LaFO;)Ljava/util/Queue;

    move-result-object v3

    .line 359
    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGX;

    .line 360
    invoke-virtual {v0}, LaGX;->a()I

    move-result v5

    iget v6, p0, LaHs;->a:I

    if-le v5, v6, :cond_1

    .line 361
    invoke-virtual {v0}, LaGX;->f()V

    .line 362
    invoke-interface {v3, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 365
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-virtual {v0}, LaGX;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 366
    iget-object v6, p0, LaHs;->a:LaGg;

    .line 367
    invoke-static {v2, v6, v5}, LaHi;->a(LaFM;LaGg;Lorg/json/JSONObject;)LaHi;

    move-result-object v5

    .line 368
    iget-object v6, p0, LaHs;->a:LaGg;

    invoke-static {v5, v2, v6}, LaHs;->a(LaHi;LaFM;LaGg;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaGC; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 369
    :catch_0
    move-exception v5

    .line 371
    const-string v5, "OperationQueueImpl"

    const-string v6, "JSONException: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, LaGX;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 372
    :catch_1
    move-exception v0

    .line 374
    const-string v5, "OperationQueueImpl"

    const-string v6, "Failed to find entry when deserealizing an operation"

    invoke-static {v5, v0, v6}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    :cond_2
    return-void
.end method

.method public a(LaFM;LaHy;)V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, LaHs;->a:Ljava/util/concurrent/Executor;

    new-instance v1, LaHu;

    invoke-direct {v1, p0, p1, p2}, LaHu;-><init>(LaHs;LaFM;LaHy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 455
    return-void
.end method

.method public a(LaGu;)V
    .locals 9

    .prologue
    .line 385
    instance-of v1, p1, LaGd;

    if-nez v1, :cond_1

    .line 386
    const-string v1, "OperationQueueImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Entry ignored: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_0
    return-void

    .line 389
    :cond_1
    invoke-interface {p1}, LaGu;->a()LaFM;

    move-result-object v4

    .line 390
    invoke-virtual {v4}, LaFM;->a()LaFO;

    move-result-object v1

    .line 391
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    .line 392
    invoke-virtual {p0, v1}, LaHs;->a(LaFO;)Ljava/util/Queue;

    move-result-object v6

    .line 393
    invoke-interface {v6}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, p1

    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGX;

    .line 395
    :try_start_0
    iget-object v2, p0, LaHs;->a:LaGg;

    invoke-static {v4, v2, v1}, LaHs;->a(LaFM;LaGg;LaGX;)LaHi;

    move-result-object v8

    .line 397
    invoke-virtual {v8}, LaHi;->a()LaHo;

    move-result-object v2

    invoke-interface {v2}, LaHo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 398
    move-object v0, v3

    check-cast v0, LaGd;

    move-object v2, v0

    invoke-virtual {v2}, LaGd;->a()LaGe;

    move-result-object v2

    .line 402
    invoke-virtual {v8}, LaHi;->a()LaHo;

    move-result-object v8

    .line 399
    invoke-static {v1, v2, v8}, LaHs;->a(LaGX;LaGe;LaHo;)V

    .line 403
    invoke-virtual {v2}, LaGe;->b()LaGd;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaGC; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto :goto_0

    .line 405
    :catch_0
    move-exception v2

    .line 406
    invoke-virtual {v1}, LaGX;->f()V

    .line 407
    invoke-interface {v6, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 408
    const-string v1, "OperationQueueImpl"

    const-string v2, "JSONException when reapplying operations on sync"

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 409
    :catch_1
    move-exception v2

    .line 410
    invoke-virtual {v1}, LaGX;->f()V

    .line 411
    invoke-interface {v6, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 412
    const-string v1, "OperationQueueImpl"

    const-string v8, "EntryNotFoundException when reapplying operations on sync"

    invoke-static {v1, v2, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(LaHo;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, LaHs;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 120
    return-void
.end method

.method public a()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 459
    iget-object v0, p0, LaHs;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 500
    :goto_0
    return v0

    .line 462
    :cond_0
    const/4 v1, 0x0

    .line 463
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 464
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 467
    :goto_1
    :try_start_0
    iget-object v0, p0, LaHs;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    .line 469
    :try_start_1
    invoke-interface {v0}, LaHo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 470
    iget-object v5, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaHo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    invoke-interface {v5, v6}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v5

    .line 471
    if-eqz v5, :cond_1

    .line 472
    invoke-virtual {v5}, LaGd;->a()LaGe;

    move-result-object v1

    .line 473
    invoke-interface {v0, v1}, LaHo;->a(LaGe;)LaHo;

    move-result-object v5

    .line 474
    new-instance v6, LaHi;

    .line 475
    invoke-virtual {v1}, LaGe;->j()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v0, v5, v7}, LaHi;-><init>(LaHo;LaHo;Ljava/lang/String;)V

    .line 476
    invoke-direct {p0, v6}, LaHs;->a(LaHi;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    move-object v0, v1

    .line 483
    if-eqz v0, :cond_2

    .line 484
    :try_start_2
    invoke-virtual {v0}, LaGe;->e()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    move-object v1, v0

    .line 489
    goto :goto_1

    .line 478
    :catch_0
    move-exception v0

    .line 479
    :try_start_3
    const-string v1, "OperationQueueImpl"

    const-string v2, "Failed to save operation in local database."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 493
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    move v0, v3

    goto :goto_0

    .line 486
    :catch_1
    move-exception v0

    .line 487
    :try_start_4
    const-string v1, "OperationQueueImpl"

    const-string v2, "Failed to save entry change in local database."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 493
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    move v0, v3

    goto :goto_0

    .line 491
    :cond_3
    :try_start_5
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 493
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 497
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 498
    iget-object v3, p0, LaHs;->a:LaGg;

    iget-object v4, p0, LaHs;->a:LaGg;

    invoke-interface {v4, v0}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    invoke-interface {v3, v0}, LaGg;->b(LaFM;)V

    goto :goto_2

    .line 493
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaHs;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    :cond_4
    move v0, v2

    .line 500
    goto/16 :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    .line 334
    iget-object v0, p0, LaHs;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 335
    iget-object v2, p0, LaHs;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(LaFO;)LaFM;

    move-result-object v2

    .line 336
    invoke-virtual {p0, v0}, LaHs;->a(LaFO;)Ljava/util/Queue;

    move-result-object v3

    .line 337
    const/4 v0, 0x0

    .line 339
    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGX;

    .line 340
    invoke-virtual {v0}, LaGX;->a()V

    .line 341
    invoke-virtual {v0}, LaGX;->e()V

    .line 342
    const/4 v0, 0x1

    .line 343
    goto :goto_1

    .line 344
    :cond_1
    if-eqz v0, :cond_0

    .line 347
    invoke-direct {p0, v2}, LaHs;->a(LaFM;)V

    goto :goto_0

    .line 350
    :cond_2
    return-void
.end method
