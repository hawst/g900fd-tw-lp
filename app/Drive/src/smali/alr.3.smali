.class public Lalr;
.super Ljava/lang/Object;
.source "FormatUtilities.java"


# direct methods
.method public static a(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 26
    long-to-double v0, p0

    .line 29
    const-wide/32 v2, 0x40000000

    cmp-long v2, p0, v2

    if-lez v2, :cond_0

    .line 30
    const-wide/high16 v2, 0x41d0000000000000L    # 1.073741824E9

    div-double v2, v0, v2

    .line 31
    const-string v0, "GB"

    .line 39
    :goto_0
    const-string v1, "%.1f %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v0, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 32
    :cond_0
    const-wide/32 v2, 0x100000

    cmp-long v2, p0, v2

    if-lez v2, :cond_1

    .line 33
    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double v2, v0, v2

    .line 34
    const-string v0, "MB"

    goto :goto_0

    .line 36
    :cond_1
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    div-double v2, v0, v2

    .line 37
    const-string v0, "KB"

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/Long;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 57
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 58
    if-eqz p2, :cond_0

    .line 59
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    .line 61
    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    sget v1, Lxi;->quota_zero:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 64
    :cond_1
    if-eqz p2, :cond_2

    .line 65
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 67
    :cond_2
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
