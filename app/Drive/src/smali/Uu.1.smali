.class public LUu;
.super Ljava/lang/Object;
.source "Observables.java"

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<TO;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LUu;->a:Ljava/util/Set;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, LUu;->a:LbmF;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TO;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v1, p0, LUu;->a:Ljava/util/Set;

    monitor-enter v1

    .line 40
    :try_start_0
    iget-object v0, p0, LUu;->a:Ljava/util/Set;

    .line 41
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "Observer %s previously registered."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 40
    invoke-static {v0, v2, v3}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, LUu;->a:LbmF;

    .line 43
    monitor-exit v1

    .line 44
    return-object p1

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 49
    iget-object v1, p0, LUu;->a:Ljava/util/Set;

    monitor-enter v1

    .line 50
    :try_start_0
    iget-object v0, p0, LUu;->a:Ljava/util/Set;

    .line 51
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "Trying to remove inexistant Observer %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 50
    invoke-static {v0, v2, v3}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, LUu;->a:LbmF;

    .line 53
    monitor-exit v1

    .line 54
    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected finalize()V
    .locals 6

    .prologue
    .line 66
    iget-object v0, p0, LUu;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    const-string v0, "Observable"

    const-string v1, "Leaking %d observers, e.g. %s "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LUu;->a:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LUu;->a:Ljava/util/Set;

    const-string v5, ""

    .line 68
    invoke-static {v4, v5}, Lbnm;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 67
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 71
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v1, p0, LUu;->a:Ljava/util/Set;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v0, p0, LUu;->a:LbmF;

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, LUu;->a:Ljava/util/Set;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, LUu;->a:LbmF;

    .line 80
    :cond_0
    iget-object v0, p0, LUu;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v0

    .line 81
    monitor-exit v1

    .line 82
    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
