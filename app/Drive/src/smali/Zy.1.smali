.class public LZy;
.super Lbuo;
.source "PunchWebViewModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lbuo;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 81
    const-class v0, LWS;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LZA;

    invoke-interface {v0, v1}, LbuQ;->b(Ljava/lang/Class;)LbuU;

    .line 83
    const-class v0, LXt;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LZB;

    invoke-interface {v0, v1}, LbuQ;->b(Ljava/lang/Class;)LbuU;

    .line 84
    const-class v0, LXD;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LaiC;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)V

    .line 85
    const-class v0, LYO;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LYS;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 86
    const-class v0, LYS;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LaiC;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)V

    .line 87
    const-class v0, LXJ;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LXD;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 88
    const-class v0, LXz;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LXJ;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 89
    const-class v0, LXC;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LYZ;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 90
    const-class v0, LXx;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LXy;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 91
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-string v1, "punchThumbnailCacheSize"

    invoke-static {v1}, Lbwo;->a(Ljava/lang/String;)Lbwm;

    move-result-object v1

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/annotation/Annotation;)LbuT;

    move-result-object v1

    .line 92
    invoke-static {}, LakQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, LbuT;->a(Ljava/lang/Object;)V

    .line 94
    const-class v0, LWR;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LZG;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 95
    const-class v0, LYw;

    invoke-virtual {p0, v0}, LZy;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LYy;

    .line 96
    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 97
    return-void

    .line 92
    :cond_0
    const/16 v0, 0x50

    goto :goto_0
.end method

.method provideDragKnobFragmentListener(Landroid/content/Context;)LWY;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 56
    const-class v0, LWY;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWY;

    return-object v0
.end method

.method providePunchWebViewOwner(Landroid/content/Context;)LZC;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 71
    const-class v0, LZC;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZC;

    return-object v0
.end method

.method provideSlidePickerController(Landroid/content/Context;)LZG;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 66
    const-class v0, LZG;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZG;

    return-object v0
.end method

.method provideUiRequestListener(Landroid/content/Context;)LXR;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 76
    const-class v0, LXR;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXR;

    return-object v0
.end method

.method provideVideoViewListener(Landroid/content/Context;)LZm;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 61
    const-class v0, LZm;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZm;

    return-object v0
.end method
