.class public LQV;
.super Ljava/lang/Object;
.source "DocListConfigurationStore.java"


# instance fields
.field private final a:LCo;

.field private final a:LpW;

.field private final a:LzQ;


# direct methods
.method public constructor <init>(LpW;LCo;LzQ;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, LQV;->a:LpW;

    .line 65
    iput-object p2, p0, LQV;->a:LCo;

    .line 66
    iput-object p3, p0, LQV;->a:LzQ;

    .line 67
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LQW;
    .locals 3

    .prologue
    .line 144
    if-eqz p1, :cond_1

    .line 145
    invoke-interface {p1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, LQV;->a:LCo;

    sget-object v1, LCn;->a:LCn;

    .line 147
    invoke-interface {v0, v1}, LCo;->a(LCn;)LCl;

    move-result-object v1

    .line 148
    new-instance v0, LQW;

    invoke-interface {v1}, LCl;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, LCl;->a()LIK;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LQW;-><init>(Ljava/lang/String;LIK;)V

    .line 157
    :goto_0
    return-object v0

    .line 150
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LCl;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_1

    .line 152
    new-instance v0, LQW;

    invoke-interface {v1}, LCl;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, LCl;->a()LIK;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LQW;-><init>(Ljava/lang/String;LIK;)V

    goto :goto_0

    .line 157
    :cond_1
    new-instance v0, LQW;

    const-string v1, "default"

    sget-object v2, LIK;->c:LIK;

    invoke-direct {v0, v1, v2}, LQW;-><init>(Ljava/lang/String;LIK;)V

    goto :goto_0
.end method

.method private a(LaFO;Ljava/lang/String;LIK;)V
    .locals 3

    .prologue
    .line 123
    :try_start_0
    iget-object v0, p0, LQV;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sorting-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-virtual {p3}, LIK;->name()Ljava/lang/String;

    move-result-object v2

    .line 124
    invoke-interface {v0, v1, v2}, LpU;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v1, p0, LQV;->a:LpW;

    invoke-interface {v1, v0}, LpW;->a(LpU;)V
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v0

    .line 128
    const-string v1, "DocListLoadingUtils"

    const-string v2, "Error while trying to save sorting preference"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public a(LaFO;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LIK;
    .locals 2

    .prologue
    .line 134
    invoke-direct {p0, p2}, LQV;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LQW;

    move-result-object v0

    .line 135
    invoke-static {v0}, LQW;->a(LQW;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, LQW;->a(LQW;)LIK;

    move-result-object v0

    invoke-virtual {p0, p1, v1, v0}, LQV;->a(LaFO;Ljava/lang/String;LIK;)LIK;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;Ljava/lang/String;LIK;)LIK;
    .locals 3

    .prologue
    .line 104
    :try_start_0
    iget-object v0, p0, LQV;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sorting-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LpU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    sget-object v1, LIK;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    .line 113
    if-nez v0, :cond_0

    .line 117
    :goto_0
    return-object p3

    .line 105
    :catch_0
    move-exception v0

    .line 106
    const-string v1, "DocListLoadingUtils"

    const-string v2, "Error while trying to load sorting preference"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    move-object p3, v0

    goto :goto_0
.end method

.method public a(LaFO;LCl;)LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "LCl;",
            ")",
            "LbmY",
            "<",
            "LzO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, LQV;->a:LzQ;

    invoke-interface {v0, p2, p1}, LzQ;->a(LCl;LaFO;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;)LzO;
    .locals 4

    .prologue
    .line 75
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 76
    iget-object v0, p0, LQV;->a:LzQ;

    invoke-interface {v0}, LzQ;->a()LzO;

    move-result-object v0

    .line 78
    :try_start_0
    iget-object v1, p0, LQV;->a:LpW;

    invoke-interface {v1, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v1

    .line 79
    iget-object v2, p0, LQV;->a:LzQ;

    invoke-interface {v2, v1, v0}, LzQ;->a(LpU;LzO;)LzO;
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 84
    :goto_1
    return-object v0

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :catch_0
    move-exception v1

    .line 81
    const-string v2, "DocListLoadingUtils"

    const-string v3, "Error while trying to load arrangement mode preferences."

    invoke-static {v2, v1, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public a(LaFO;LIK;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0, p3}, LQV;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LQW;

    move-result-object v0

    .line 140
    invoke-static {v0}, LQW;->a(LQW;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, LQV;->a(LaFO;Ljava/lang/String;LIK;)V

    .line 141
    return-void
.end method

.method public a(LaFO;LzO;)V
    .locals 3

    .prologue
    .line 88
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 90
    :try_start_0
    iget-object v0, p0, LQV;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 91
    invoke-virtual {p2, v0}, LzO;->a(LpU;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, LQV;->a:LpW;

    invoke-interface {v1, v0}, LpW;->a(LpU;)V
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_0
    :goto_1
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string v1, "DocListLoadingUtils"

    const-string v2, "Error while trying to save arrangement mode preferences."

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method
