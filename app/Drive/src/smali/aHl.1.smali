.class public LaHl;
.super Ljava/lang/Object;
.source "LoggingOperationSyncReporter.java"

# interfaces
.implements LaHy;


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LaHl;->a:Ljava/util/concurrent/CountDownLatch;

    .line 18
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaHl;->a:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 29
    :try_start_0
    iget-object v0, p0, LaHl;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    .line 31
    const-string v1, "LoggingOperationSyncReporter"

    const-string v2, "%s: operation has failed"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LaHl;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public a(ILjava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 23
    const-string v0, "LoggingOperationSyncReporter"

    const-string v1, "%s: operation has failed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LaHl;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, p2, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 24
    iget-object v0, p0, LaHl;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 25
    return-void
.end method
