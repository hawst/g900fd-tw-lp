.class public LwQ;
.super Ljava/lang/Object;
.source "DexInstaller.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method private a(Lorg/w3c/dom/NamedNodeMap;LwR;Lorg/w3c/dom/Element;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 180
    const-string v2, "name"

    invoke-interface {p1, v2}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    .line 181
    const-string v3, "process_name"

    invoke-interface {p1, v3}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    .line 182
    invoke-static {p4, v3}, LVZ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 183
    const-string v4, "DexInstaller"

    const-string v5, "Process name mismatch \'%s\'!=\'%s\' for %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p4, v6, v0

    aput-object v3, v6, v1

    aput-object v2, v6, v7

    invoke-static {v4, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 202
    :goto_0
    return-void

    .line 188
    :cond_0
    const-string v4, "DexInstaller"

    const-string v5, "Process name match \'%s\'==\'%s\' for %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p4, v6, v0

    aput-object v3, v6, v1

    aput-object v2, v6, v7

    invoke-static {v4, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 191
    const-string v3, "require_injection"

    invoke-interface {p1, v3}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    .line 192
    if-eqz v3, :cond_1

    const-string v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_1
    move v0, v1

    .line 201
    :cond_2
    invoke-virtual {p2, v2, v0}, LwR;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 194
    :cond_3
    const-string v1, "false"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DEX injection attribute should be either \'true\' or \'false\', but is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 197
    const-string v1, "DexInstaller"

    invoke-static {v1, v0}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LwR;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 139
    new-instance v3, LwR;

    invoke-direct {v3}, LwR;-><init>()V

    .line 140
    const/4 v0, 0x0

    .line 142
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 143
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v4

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 145
    :try_start_1
    invoke-virtual {v4, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 147
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 148
    const-string v4, "dexfile"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 149
    :goto_0
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 150
    invoke-interface {v4, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 151
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v5

    .line 152
    check-cast v0, Lorg/w3c/dom/Element;

    invoke-direct {p0, v5, v3, v0, p3}, LwQ;->a(Lorg/w3c/dom/NamedNodeMap;LwR;Lorg/w3c/dom/Element;Ljava/lang/String;)V

    .line 149
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 155
    :cond_0
    new-instance v0, Lpf;

    new-instance v2, Lpw;

    invoke-direct {v2}, Lpw;-><init>()V

    invoke-direct {v0, v3, v2}, Lpf;-><init>(Lpi;Lpw;)V

    .line 156
    invoke-virtual {v0, p1}, Lpf;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    if-eqz v1, :cond_1

    .line 169
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 175
    :cond_1
    :goto_1
    return-object v3

    .line 171
    :catch_0
    move-exception v0

    .line 172
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed installing dex configuration"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 157
    :catch_1
    move-exception v1

    .line 158
    :goto_2
    :try_start_3
    const-string v1, "DexInstaller"

    const-string v2, "Dex configuration file %s not found. Skipping dex installation."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v1, v2, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 168
    if-eqz v0, :cond_1

    .line 169
    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 171
    :catch_2
    move-exception v0

    .line 172
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed installing dex configuration"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 160
    :catch_3
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 161
    :goto_3
    :try_start_5
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed installing dex configuration"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 167
    :catchall_0
    move-exception v0

    .line 168
    :goto_4
    if-eqz v1, :cond_2

    .line 169
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 172
    :cond_2
    throw v0

    .line 162
    :catch_4
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 163
    :goto_5
    :try_start_7
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed installing dex configuration"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 164
    :catch_5
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 165
    :goto_6
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed installing dex configuration"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 171
    :catch_6
    move-exception v0

    .line 172
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed installing dex configuration"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 167
    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    :catchall_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    .line 164
    :catch_7
    move-exception v0

    goto :goto_6

    .line 162
    :catch_8
    move-exception v0

    goto :goto_5

    .line 160
    :catch_9
    move-exception v0

    goto :goto_3

    .line 157
    :catch_a
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method
