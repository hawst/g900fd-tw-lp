.class public LAE;
.super Landroid/widget/BaseAdapter;
.source "DocListAdapter.java"

# interfaces
.implements LDD;
.implements LEc;
.implements LIz;
.implements LKe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/BaseAdapter;",
        "LDD;",
        "LEc;",
        "LIz;",
        "LKe",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private a:LAJ;

.field private final a:LBf;

.field private final a:LCr;

.field private final a:LCt;

.field private final a:LDU;

.field private final a:LDY;

.field private final a:LDk;

.field private final a:LDn;

.field private final a:LHp;

.field private a:LaGA;

.field private final a:LabF;

.field private final a:LamF;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/support/v4/app/Fragment;

.field private final a:Landroid/view/LayoutInflater;

.field private final a:Landroid/view/View$OnClickListener;

.field private final a:Landroid/view/View$OnLongClickListener;

.field private final a:Landroid/widget/ListView;

.field private final a:LapF;

.field private final a:Lapd;

.field private final a:Lapn;

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:LtK;

.field private final a:LzN;

.field private a:LzO;

.field private final a:Z

.field private final b:I

.field private final b:Z

.field private c:I


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LtK;LapF;LamF;LCt;Landroid/widget/ListView;LQX;LBf;LCr;Lapd;LDU;LJR;LCU;LDn;LDk;LzN;LHp;Lapn;ILabF;Z)V
    .locals 2

    .prologue
    .line 230
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 231
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, LAE;->a:Landroid/content/Context;

    .line 232
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    iput-object v1, p0, LAE;->a:Landroid/support/v4/app/Fragment;

    .line 233
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, LAE;->a:Landroid/view/LayoutInflater;

    .line 234
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    iput-object v1, p0, LAE;->a:LtK;

    .line 235
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LapF;

    iput-object v1, p0, LAE;->a:LapF;

    .line 236
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LamF;

    iput-object v1, p0, LAE;->a:LamF;

    .line 237
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, LAE;->a:Landroid/widget/ListView;

    .line 238
    iput-object p9, p0, LAE;->a:LBf;

    .line 239
    invoke-static {p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LCr;

    iput-object v1, p0, LAE;->a:LCr;

    .line 240
    iput-object p11, p0, LAE;->a:Lapd;

    .line 241
    invoke-static {p12}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDU;

    iput-object v1, p0, LAE;->a:LDU;

    .line 242
    invoke-static/range {p15 .. p15}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDn;

    iput-object v1, p0, LAE;->a:LDn;

    .line 243
    invoke-virtual/range {p14 .. p14}, LCU;->a()Z

    move-result v1

    iput-boolean v1, p0, LAE;->a:Z

    .line 244
    move-object/from16 v0, p16

    iput-object v0, p0, LAE;->a:LDk;

    .line 245
    invoke-static/range {p17 .. p17}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LzN;

    iput-object v1, p0, LAE;->a:LzN;

    .line 246
    invoke-static/range {p18 .. p18}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LHp;

    iput-object v1, p0, LAE;->a:LHp;

    .line 247
    move-object/from16 v0, p19

    iput-object v0, p0, LAE;->a:Lapn;

    .line 248
    move/from16 v0, p20

    iput v0, p0, LAE;->b:I

    .line 249
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LCt;

    iput-object v1, p0, LAE;->a:LCt;

    .line 250
    new-instance v1, LDY;

    invoke-direct {v1, p11, p0}, LDY;-><init>(Lapd;LEc;)V

    iput-object v1, p0, LAE;->a:LDY;

    .line 251
    move-object/from16 v0, p21

    iput-object v0, p0, LAE;->a:LabF;

    .line 252
    move/from16 v0, p22

    iput-boolean v0, p0, LAE;->b:Z

    .line 254
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LAE;->a(Landroid/content/res/Resources;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v1

    iput-object v1, p0, LAE;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 255
    new-instance v1, LAF;

    invoke-direct {v1, p0, p6}, LAF;-><init>(LAE;LCt;)V

    iput-object v1, p0, LAE;->a:Landroid/view/View$OnClickListener;

    .line 276
    sget-object v1, Lry;->S:Lry;

    invoke-interface {p3, v1}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    new-instance v1, LAG;

    invoke-direct {v1, p0, p6}, LAG;-><init>(LAE;LCt;)V

    iput-object v1, p0, LAE;->a:Landroid/view/View$OnLongClickListener;

    .line 299
    :goto_0
    invoke-virtual {p13, p0}, LJR;->a(LKe;)V

    .line 300
    invoke-virtual {p0, p8}, LAE;->a(LQX;)V

    .line 301
    return-void

    .line 297
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, LAE;->a:Landroid/view/View$OnLongClickListener;

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LtK;LapF;LamF;LCt;Landroid/widget/ListView;LQX;LBf;LCr;Lapd;LDU;LJR;LCU;LDn;LDk;LzN;LHp;Lapn;ILabF;ZLAF;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct/range {p0 .. p22}, LAE;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LtK;LapF;LamF;LCt;Landroid/widget/ListView;LQX;LBf;LCr;Lapd;LDU;LJR;LCU;LDn;LDk;LzN;LHp;Lapn;ILabF;Z)V

    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, LAE;->a:LaGA;

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, LAE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()I

    move-result v0

    .line 520
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 420
    invoke-direct {p0}, LAE;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 421
    sget v0, Lxe;->incoming_card:I

    .line 430
    :goto_0
    iget-object v2, p0, LAE;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 432
    iget-object v3, p0, LAE;->a:Landroid/view/LayoutInflater;

    iget v4, p0, LAE;->b:I

    sget v0, Lxc;->more_actions_button_container:I

    .line 434
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 432
    invoke-virtual {v3, v4, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 436
    iget-boolean v0, p0, LAE;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LAE;->a:LtK;

    sget-object v3, Lry;->S:Lry;

    .line 437
    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    .line 439
    :goto_1
    if-nez v0, :cond_1

    invoke-direct {p0}, LAE;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 440
    sget v0, Lxc;->doc_entry_root:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 442
    iget-object v1, p0, LAE;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;)V

    .line 445
    :cond_1
    invoke-direct {p0}, LAE;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, LCA;

    invoke-direct {v0, v2}, LCA;-><init>(Landroid/view/View;)V

    move-object v1, v0

    .line 447
    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 449
    iget-object v0, v1, LBe;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v3

    :cond_2
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 450
    if-eqz v0, :cond_2

    .line 451
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 452
    iget-object v4, p0, LAE;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 453
    iget-object v4, p0, LAE;->a:Landroid/view/View$OnLongClickListener;

    if-eqz v4, :cond_2

    .line 454
    iget-object v4, p0, LAE;->a:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_3

    .line 422
    :cond_3
    sget-object v0, LapG;->c:LapG;

    iget-object v2, p0, LAE;->a:LapF;

    invoke-interface {v2}, LapF;->a()LapG;

    move-result-object v2

    invoke-virtual {v0, v2}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 424
    sget v0, Lxe;->doc_entry_row_onecolumn:I

    goto :goto_0

    .line 427
    :cond_4
    sget v0, Lxe;->doc_entry_row:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 437
    goto :goto_1

    .line 445
    :cond_6
    new-instance v0, LBy;

    iget-object v1, p0, LAE;->a:Lapd;

    iget-object v3, p0, LAE;->a:LDk;

    invoke-direct {v0, v2, v1, v3}, LBy;-><init>(Landroid/view/View;Lapd;LDk;)V

    move-object v1, v0

    goto :goto_2

    .line 458
    :cond_7
    return-object v2
.end method

.method static synthetic a(LAE;)Lapn;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, LAE;->a:Lapn;

    return-object v0
.end method

.method static a(Landroid/content/res/Resources;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 2

    .prologue
    .line 392
    sget v0, Lxa;->doclist_entry_height:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 393
    new-instance v1, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v1, v0, v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    return-object v1
.end method

.method private a(LaGA;)V
    .locals 0

    .prologue
    .line 360
    if-eqz p1, :cond_0

    .line 362
    invoke-virtual {p0}, LAE;->notifyDataSetChanged()V

    .line 367
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-virtual {p0}, LAE;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, LAE;->a:LzO;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must not be called before setState()"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 305
    iget-object v0, p0, LAE;->a:LzO;

    sget-object v1, LzO;->c:LzO;

    invoke-virtual {v0, v1}, LzO;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 497
    invoke-direct {p0}, LAE;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    iget-object v0, p0, LAE;->a:LDY;

    invoke-virtual {v0}, LDY;->a()V

    .line 500
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)LCv;
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, LAE;->a:LaGA;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, LAE;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    .line 529
    iget-object v0, p0, LAE;->a:LaGA;

    .line 531
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)LEd;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 588
    iget-object v0, p0, LAE;->a:LHp;

    invoke-interface {v0, p1}, LHp;->a(I)Landroid/view/View;

    move-result-object v0

    .line 589
    if-nez v0, :cond_0

    move-object v0, v1

    .line 600
    :goto_0
    return-object v0

    .line 593
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 594
    instance-of v2, v0, LEg;

    if-eqz v2, :cond_1

    .line 595
    check-cast v0, LEg;

    .line 598
    invoke-interface {v0}, LEg;->a()LbmF;

    move-result-object v0

    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEd;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 600
    goto :goto_0
.end method

.method public a(I)LIL;
    .locals 2

    .prologue
    .line 568
    iget-object v0, p0, LAE;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    .line 569
    iget-object v0, p0, LAE;->a:LAJ;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, LAE;->a:LAJ;

    invoke-virtual {v0}, LAJ;->a()LIf;

    move-result-object v0

    iget-object v1, p0, LAE;->a:LaGA;

    invoke-virtual {v0, v1}, LIf;->a(LCv;)LIL;

    move-result-object v0

    .line 572
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v0}, LIL;->a([Ljava/lang/Object;)LIL;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)LIy;
    .locals 5

    .prologue
    .line 611
    iget-object v0, p0, LAE;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    move-result v0

    .line 612
    const-string v1, "Failed to move to position %s for cursor %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 613
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LAE;->a:LaGA;

    aput-object v4, v2, v3

    .line 612
    invoke-static {v0, v1, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 614
    iget-object v0, p0, LAE;->a:LAJ;

    invoke-virtual {v0}, LAJ;->a()LIf;

    move-result-object v0

    iget-object v1, p0, LAE;->a:LaGA;

    invoke-virtual {v0, v1}, LIf;->b(LCv;)LIy;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGA;)LaGA;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, LAE;->a:LAJ;

    invoke-virtual {v0, p1}, LAJ;->a(LaGA;)V

    .line 349
    iget-object v0, p0, LAE;->a:LaGA;

    if-ne p1, v0, :cond_0

    .line 350
    const/4 v0, 0x0

    .line 356
    :goto_0
    return-object v0

    .line 352
    :cond_0
    iget-object v0, p0, LAE;->a:LaGA;

    .line 353
    iput-object p1, p0, LAE;->a:LaGA;

    .line 354
    invoke-direct {p0, p1}, LAE;->a(LaGA;)V

    .line 355
    invoke-direct {p0}, LAE;->d()V

    goto :goto_0
.end method

.method public a(I)Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 5

    .prologue
    .line 578
    iget-object v0, p0, LAE;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    move-result v0

    .line 579
    const-string v1, "cursor.getCount()=%s, position=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LAE;->a:LaGA;

    invoke-interface {v4}, LaGA;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 580
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 579
    invoke-static {v0, v1, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 581
    iget-object v0, p0, LAE;->a:LaGA;

    iget-object v1, p0, LAE;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a(LaGA;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    .line 582
    return-object v0
.end method

.method public a()LzN;
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, LAE;->a:LzN;

    return-object v0
.end method

.method public a(LQX;)V
    .locals 21

    .prologue
    .line 310
    invoke-virtual/range {p1 .. p1}, LQX;->a()LaFX;

    move-result-object v2

    const-class v3, LaGA;

    invoke-virtual {v2, v3}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v2

    move-object/from16 v20, v2

    check-cast v20, LaGA;

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, LAE;->a:LaGA;

    move-object/from16 v0, v20

    if-ne v0, v2, :cond_0

    .line 345
    :goto_0
    return-void

    .line 314
    :cond_0
    invoke-virtual/range {p1 .. p1}, LQX;->a()LzO;

    move-result-object v19

    .line 315
    invoke-virtual/range {v19 .. v19}, LzO;->a()LzP;

    move-result-object v2

    sget-object v3, LzP;->a:LzP;

    invoke-virtual {v2, v3}, LzP;->equals(Ljava/lang/Object;)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported ArrangementMode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LbiT;->a(ZLjava/lang/Object;)V

    .line 317
    move-object/from16 v0, p0

    iget-object v2, v0, LAE;->a:LaGA;

    if-eqz v2, :cond_1

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, LAE;->a:LaGA;

    invoke-interface {v2}, LaGA;->a()V

    .line 320
    :cond_1
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, LAE;->a:LaGA;

    .line 321
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, LAE;->a:LzO;

    .line 322
    new-instance v2, LAJ;

    move-object/from16 v0, p0

    iget-object v3, v0, LAE;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, LAE;->a:LaGA;

    .line 325
    invoke-virtual/range {p1 .. p1}, LQX;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    .line 326
    invoke-virtual/range {p1 .. p1}, LQX;->a()LIf;

    move-result-object v6

    .line 327
    invoke-virtual/range {p1 .. p1}, LQX;->a()LIK;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LAE;->a:LtK;

    .line 329
    invoke-virtual/range {p1 .. p1}, LQX;->a()LCl;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LAE;->a:LapF;

    move-object/from16 v0, p0

    iget-object v11, v0, LAE;->a:LamF;

    move-object/from16 v0, p0

    iget-object v12, v0, LAE;->a:LBf;

    move-object/from16 v0, p0

    iget-object v13, v0, LAE;->a:LCr;

    move-object/from16 v0, p0

    iget-object v14, v0, LAE;->a:LDU;

    move-object/from16 v0, p0

    iget-object v15, v0, LAE;->a:LDn;

    move-object/from16 v0, p0

    iget-object v0, v0, LAE;->a:LCt;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LAE;->a:LabF;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LAE;->b:Z

    move/from16 v18, v0

    invoke-direct/range {v2 .. v19}, LAJ;-><init>(Landroid/content/Context;LaGA;Lcom/google/android/gms/drive/database/data/EntrySpec;LIf;LIK;LtK;LCl;LapF;LamF;LBf;LCr;LDU;LDn;LCt;LabF;ZLzO;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LAE;->a:LAJ;

    .line 340
    invoke-direct/range {p0 .. p0}, LAE;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, LAE;->a:LDY;

    invoke-virtual {v2}, LDY;->b()V

    .line 343
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, LAE;->a(LaGA;)V

    .line 344
    invoke-direct/range {p0 .. p0}, LAE;->d()V

    goto/16 :goto_0
.end method

.method public a(LaFX;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 547
    const-class v0, LaGA;

    invoke-virtual {p1, v0}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v0

    check-cast v0, LaGA;

    .line 548
    invoke-virtual {p0, v0}, LAE;->a(LaGA;)LaGA;

    move-result-object v0

    .line 549
    if-eqz v0, :cond_0

    .line 550
    invoke-interface {v0}, LaGA;->a()V

    .line 552
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, LAE;->a:LDY;

    invoke-virtual {v0}, LDY;->b()V

    .line 509
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 514
    return-void
.end method

.method public c_()V
    .locals 3

    .prologue
    .line 556
    iget-object v0, p0, LAE;->a:LAJ;

    if-eqz v0, :cond_1

    .line 557
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LAE;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 558
    iget-object v1, p0, LAE;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 559
    instance-of v2, v1, Lcom/google/android/apps/docs/view/DocEntryRowRelativeLayout;

    if-eqz v2, :cond_0

    .line 560
    iget-object v2, p0, LAE;->a:LAJ;

    invoke-virtual {v2, v1}, LAJ;->a(Landroid/view/View;)V

    .line 557
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 564
    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 371
    invoke-direct {p0}, LAE;->a()I

    move-result v0

    iput v0, p0, LAE;->a:I

    .line 372
    iget v0, p0, LAE;->a:I

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1}, LAE;->a(I)LCv;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 537
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 382
    invoke-direct {p0}, LAE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 398
    iget-object v0, p0, LAE;->a:LaGA;

    if-nez v0, :cond_0

    .line 399
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this should only be called when the cursor is valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 401
    :cond_0
    iget-object v0, p0, LAE;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 402
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "couldn\'t move cursor to position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :cond_1
    instance-of v0, p2, Lcom/google/android/apps/docs/view/DocEntryRowRelativeLayout;

    if-eqz v0, :cond_2

    .line 407
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LBe;

    if-eqz v0, :cond_2

    .line 413
    :goto_0
    iget-object v0, p0, LAE;->a:LAJ;

    iget-object v1, p0, LAE;->a:LaGA;

    invoke-virtual {v0, p2, v1}, LAJ;->a(Landroid/view/View;LaGA;)V

    .line 415
    return-object p2

    .line 410
    :cond_2
    invoke-direct {p0, p3}, LAE;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 483
    const/4 v0, 0x0

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 387
    invoke-direct {p0}, LAE;->a()I

    move-result v0

    iput v0, p0, LAE;->a:I

    .line 388
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 389
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 489
    iget v0, p0, LAE;->c:I

    if-eq v0, p2, :cond_0

    .line 490
    iput p2, p0, LAE;->c:I

    .line 491
    iget-object v0, p0, LAE;->a:Lapd;

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v1, p2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lapd;->a(J)V

    .line 492
    invoke-direct {p0}, LAE;->d()V

    .line 494
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 504
    return-void
.end method
