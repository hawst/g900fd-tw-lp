.class public LVg;
.super Ljava/lang/Object;
.source "BatchOperationQueue.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LVB;

.field private final a:LVL;

.field private final a:LVl;

.field private final a:LaGM;

.field private final a:Ljava/util/concurrent/ExecutorService;

.field private a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "LVd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LVl;LVB;LaGM;)V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, LVg;-><init>(LVl;LVB;LaGM;Ljava/util/concurrent/ExecutorService;)V

    .line 78
    return-void
.end method

.method constructor <init>(LVl;LVB;LaGM;Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, LVh;

    invoke-direct {v0, p0}, LVh;-><init>(LVg;)V

    iput-object v0, p0, LVg;->a:LVL;

    .line 69
    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    iput-object v0, p0, LVg;->a:Ljava/util/concurrent/Future;

    .line 86
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, LVg;->a:Ljava/util/concurrent/ExecutorService;

    .line 87
    iput-object p2, p0, LVg;->a:LVB;

    .line 88
    iput-object p3, p0, LVg;->a:LaGM;

    .line 89
    iput-object p1, p0, LVg;->a:LVl;

    .line 90
    return-void
.end method

.method static synthetic a(LVg;)LVB;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LVg;->a:LVB;

    return-object v0
.end method

.method static synthetic a(LVg;)LVL;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LVg;->a:LVL;

    return-object v0
.end method

.method static synthetic a(LVg;LVd;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, LVg;->c(LVd;)V

    return-void
.end method

.method static synthetic a(LVg;LaFM;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, LVg;->a(LaFM;)V

    return-void
.end method

.method private a(LaFM;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, LVg;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(LaFM;)V

    .line 164
    return-void
.end method

.method private c(LVd;)V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p1}, LVd;->a()LbmF;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVt;

    .line 117
    invoke-interface {v0}, LVt;->a()V

    goto :goto_0

    .line 119
    :cond_0
    invoke-virtual {p1}, LVd;->a()LaFM;

    move-result-object v0

    invoke-direct {p0, v0}, LVg;->a(LaFM;)V

    .line 120
    return-void
.end method


# virtual methods
.method public a(LaFO;)LVf;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, LVg;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 94
    iget-object v1, p0, LVg;->a:LVl;

    invoke-virtual {v1, v0}, LVl;->a(LaFM;)LVf;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 135
    monitor-enter p0

    .line 136
    :try_start_0
    iget-object v0, p0, LVg;->a:Ljava/util/concurrent/Future;

    .line 137
    const/4 v1, 0x0

    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v1

    iput-object v1, p0, LVg;->a:Ljava/util/concurrent/Future;

    .line 138
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    iget-object v1, p0, LVg;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, LVk;

    invoke-direct {v2, p0, v0}, LVk;-><init>(LVg;Ljava/util/concurrent/Future;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 160
    return-void

    .line 138
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(LVd;)V
    .locals 2

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LVg;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, LVi;

    invoke-direct {v1, p0, p1}, LVi;-><init>(LVg;LVd;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LVg;->a:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LVd;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0, p1}, LVg;->b(LVd;)V

    .line 124
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LVj;

    invoke-direct {v1, p0, p2}, LVj;-><init>(LVg;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 131
    return-void
.end method

.method public b(LVd;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1}, LVg;->c(LVd;)V

    .line 109
    monitor-enter p0

    .line 110
    :try_start_0
    invoke-static {p1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    iput-object v0, p0, LVg;->a:Ljava/util/concurrent/Future;

    .line 111
    monitor-exit p0

    .line 112
    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
