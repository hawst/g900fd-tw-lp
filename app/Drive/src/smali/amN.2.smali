.class public LamN;
.super Ljava/lang/Object;
.source "StreamToWebViewUtils.java"


# direct methods
.method public static a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 80
    const/16 v1, 0x4000

    new-array v1, v1, [B

    .line 82
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 83
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 85
    :cond_0
    const-string v1, "UTF8"

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, LamN;->a(Ljava/lang/String;Ljava/io/InputStream;Landroid/webkit/WebView;)V

    .line 44
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/io/InputStream;Landroid/webkit/WebView;)V
    .locals 6

    .prologue
    .line 56
    const-string v0, "The input stream is null."

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v0, "The WebView is null."

    invoke-static {p2, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const/4 v2, 0x0

    .line 60
    :try_start_0
    invoke-static {p1}, LamN;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 64
    :goto_0
    if-eqz v2, :cond_0

    .line 65
    if-eqz p0, :cond_1

    .line 66
    const-string v3, "text/html"

    const-string v4, "UTF8"

    const-string v5, ""

    move-object v0, p2

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    :goto_1
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 76
    :goto_2
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    const-string v0, "StreamToWebViewUtils"

    const-string v1, "Unable to load data from the stream."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 68
    :cond_1
    const-string v0, "text/html"

    const-string v1, "UTF8"

    invoke-virtual {p2, v2, v0, v1}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 73
    :catch_1
    move-exception v0

    goto :goto_2
.end method
