.class public final enum LaHW;
.super Ljava/lang/Enum;
.source "DocumentCursorRowFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaHW;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaHW;

.field private static final synthetic a:[LaHW;

.field public static final enum b:LaHW;


# instance fields
.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, LaHW;

    const-string v1, "READ_WRITE"

    invoke-direct {v0, v1, v2, v3}, LaHW;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LaHW;->a:LaHW;

    .line 64
    new-instance v0, LaHW;

    const-string v1, "READ_ONLY"

    invoke-direct {v0, v1, v3, v2}, LaHW;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LaHW;->b:LaHW;

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [LaHW;

    sget-object v1, LaHW;->a:LaHW;

    aput-object v1, v0, v2

    sget-object v1, LaHW;->b:LaHW;

    aput-object v1, v0, v3

    sput-object v0, LaHW;->a:[LaHW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 69
    iput-boolean p3, p0, LaHW;->a:Z

    .line 70
    return-void
.end method

.method public static a(Z)LaHW;
    .locals 1

    .prologue
    .line 77
    if-eqz p0, :cond_0

    sget-object v0, LaHW;->a:LaHW;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LaHW;->b:LaHW;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LaHW;
    .locals 1

    .prologue
    .line 62
    const-class v0, LaHW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaHW;

    return-object v0
.end method

.method public static values()[LaHW;
    .locals 1

    .prologue
    .line 62
    sget-object v0, LaHW;->a:[LaHW;

    invoke-virtual {v0}, [LaHW;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaHW;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, LaHW;->a:Z

    return v0
.end method
