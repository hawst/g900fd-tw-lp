.class LadU;
.super Ljava/lang/Object;
.source "LocalFileManagerImpl.java"

# interfaces
.implements LadR;


# instance fields
.field private a:I

.field final synthetic a:LadS;

.field private final a:Ladj;

.field private volatile a:Ljava/lang/String;

.field private volatile a:Z

.field private volatile b:Z


# direct methods
.method private constructor <init>(LadS;Ladj;ZLjava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 58
    iput-object p1, p0, LadU;->a:LadS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, LadU;->a:Ladj;

    .line 60
    iput-boolean p3, p0, LadU;->b:Z

    .line 61
    iput-object p4, p0, LadU;->a:Ljava/lang/String;

    .line 62
    iput-boolean v0, p0, LadU;->a:Z

    .line 63
    iput v0, p0, LadU;->a:I

    .line 64
    return-void
.end method

.method synthetic constructor <init>(LadS;Ladj;ZLjava/lang/String;LadT;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, LadU;-><init>(LadS;Ladj;ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(LadU;)I
    .locals 2

    .prologue
    .line 47
    iget v0, p0, LadU;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LadU;->a:I

    return v0
.end method

.method static synthetic a(LadU;)Ladj;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, LadU;->a:Ladj;

    return-object v0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, LadU;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()LaGp;

    move-result-object v0

    .line 291
    if-nez v0, :cond_0

    .line 292
    const-string v0, "LocalFileManagerImpl"

    const-string v1, "Document content does not exist"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, LaGp;->c()Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(LadU;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, LadU;->b:Z

    return v0
.end method

.method static synthetic b(LadU;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, LadU;->a:I

    return v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, LadU;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()LaGp;

    move-result-object v0

    .line 333
    if-nez v0, :cond_0

    .line 334
    const-string v0, "LocalFileManagerImpl"

    const-string v1, "Document content does not exist"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const/4 v0, 0x0

    .line 338
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, LaGp;->d()Z

    move-result v0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 67
    iget-boolean v0, p0, LadU;->a:Z

    const-string v1, "LocalFile is no longer valid"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 68
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, LadU;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, LadU;->d()V

    .line 102
    iget-object v0, p0, LadU;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, LadU;->b:Z

    .line 104
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, LadU;->d()V

    .line 109
    iget-object v0, p0, LadU;->a:Ladj;

    invoke-interface {v0, p1}, Ladj;->a(Ljava/util/Date;)V

    .line 110
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 422
    invoke-direct {p0}, LadU;->d()V

    .line 423
    const-string v0, "LocalFileManagerImpl"

    const-string v1, "requestPagingOnClose"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    iget-object v0, p0, LadU;->a:Ladj;

    invoke-interface {v0}, Ladj;->b()V

    .line 425
    return-void
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 477
    const-string v2, "LocalFileManagerImpl"

    const-string v3, "Setting a new doc (%s) as created on the server"

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v5, p0, LadU;->a:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 478
    iget-object v2, p0, LadU;->a:Ljava/lang/String;

    if-eqz v2, :cond_2

    :goto_0
    const-string v1, "setNewDocumentIdAndUri must be called before setNewDocumentIsCreatedOnServer."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 480
    const/4 v0, 0x0

    .line 481
    iget-object v1, p0, LadU;->a:LadS;

    invoke-static {v1}, LadS;->a(LadS;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->a()V

    .line 483
    :try_start_0
    iget-object v1, p0, LadU;->a:Ladj;

    invoke-interface {v1}, Ladj;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 484
    iget-object v2, p0, LadU;->a:LadS;

    invoke-static {v2}, LadS;->a(LadS;)LaGg;

    move-result-object v2

    invoke-interface {v2, v1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v1

    .line 485
    invoke-virtual {v1}, LaGb;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 486
    invoke-virtual {v1}, LaGb;->a()LaFM;

    move-result-object v0

    .line 487
    invoke-virtual {v1}, LaGb;->a()LaGc;

    move-result-object v1

    iget-object v2, p0, LadU;->a:Ljava/lang/String;

    .line 488
    invoke-virtual {v1, v2}, LaGc;->h(Ljava/lang/String;)LaGe;

    move-result-object v1

    .line 489
    invoke-virtual {v1}, LaGe;->e()V

    .line 491
    :cond_0
    iget-object v1, p0, LadU;->a:LadS;

    invoke-static {v1}, LadS;->a(LadS;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 493
    iget-object v1, p0, LadU;->a:LadS;

    invoke-static {v1}, LadS;->a(LadS;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->b()V

    .line 500
    if-eqz v0, :cond_1

    .line 501
    iget-object v1, p0, LadU;->a:LadS;

    invoke-static {v1}, LadS;->a(LadS;)LaHr;

    move-result-object v1

    sget-object v2, LUo;->a:LaHy;

    invoke-interface {v1, v0, v2}, LaHr;->a(LaFM;LaHy;)V

    .line 503
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 478
    goto :goto_0

    .line 493
    :catchall_0
    move-exception v0

    iget-object v1, p0, LadU;->a:LadS;

    invoke-static {v1}, LadS;->a(LadS;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method

.method public close()V
    .locals 8

    .prologue
    .line 114
    iget-object v1, p0, LadU;->a:LadS;

    monitor-enter v1

    .line 115
    :try_start_0
    invoke-direct {p0}, LadU;->d()V

    .line 117
    iget-object v0, p0, LadU;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()LaGp;

    move-result-object v0

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v2

    .line 118
    iget v0, p0, LadU;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LadU;->a:I

    .line 119
    const-string v0, "LocalFileManagerImpl"

    const-string v4, "Closing document file [%s]. Number of references left: %s."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 120
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, p0, LadU;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 119
    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 122
    iget v0, p0, LadU;->a:I

    if-lez v0, :cond_0

    .line 123
    monitor-exit v1

    .line 156
    :goto_0
    return-void

    .line 126
    :cond_0
    const-string v0, "LocalFileManagerImpl"

    const-string v4, "Calling close() of [%s]."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 127
    iget-object v0, p0, LadU;->a:Ladj;

    invoke-interface {v0}, Ladj;->close()V

    .line 128
    iget-object v0, p0, LadU;->a:LadS;

    invoke-static {v0}, LadS;->a(LadS;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, LadU;->a:Z

    .line 131
    iget-object v0, p0, LadU;->a:LadS;

    invoke-static {v0}, LadS;->a(LadS;)LaGg;

    move-result-object v0

    iget-object v4, p0, LadU;->a:Ladj;

    .line 132
    invoke-interface {v4}, Ladj;->a()LaGp;

    move-result-object v4

    .line 131
    invoke-interface {v0, v4}, LaGg;->a(LaGp;)LaGo;

    move-result-object v0

    .line 133
    if-nez v0, :cond_1

    .line 134
    const-string v0, "LocalFileManagerImpl"

    const-string v4, "Closing document file [%s] without parent document"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 135
    monitor-exit v1

    goto :goto_0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 138
    :cond_1
    :try_start_1
    invoke-direct {p0}, LadU;->b()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v0}, LaGo;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 141
    :cond_2
    monitor-exit v1

    goto :goto_0

    .line 142
    :cond_3
    invoke-interface {v0}, LaGo;->f()Z

    move-result v2

    if-nez v2, :cond_5

    .line 145
    const-string v2, "LocalFileManagerImpl"

    const-string v3, "Requesting metadata sync of non-pinned %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, LaGo;->i()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 146
    iget-object v2, p0, LadU;->a:LadS;

    invoke-static {v2}, LadS;->a(LadS;)LagZ;

    move-result-object v2

    invoke-interface {v2, v0}, LagZ;->a(LaGo;)V

    .line 155
    :cond_4
    :goto_1
    monitor-exit v1

    goto :goto_0

    .line 147
    :cond_5
    iget-object v2, p0, LadU;->a:LadS;

    invoke-static {v2}, LadS;->a(LadS;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, LadU;->a()Z

    move-result v2

    if-nez v2, :cond_4

    .line 152
    const-string v2, "LocalFileManagerImpl"

    const-string v3, "Requesting sync of pinned %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, LaGo;->i()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 153
    iget-object v2, p0, LadU;->a:LadS;

    invoke-static {v2}, LadS;->a(LadS;)LagZ;

    move-result-object v2

    invoke-interface {v2, v0}, LagZ;->b(LaGo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
