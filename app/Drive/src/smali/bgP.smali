.class public LbgP;
.super Ljava/lang/Object;
.source "PdfWriter.java"


# static fields
.field private static final a:[B

.field private static final b:[B


# instance fields
.field private a:I

.field private final a:LbgD;

.field private a:LbgL;

.field private a:LbgM;

.field private final a:LbgQ;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "%PDF-1.4\n"

    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LbgP;->a:[B

    .line 15
    const-string v0, "%\u00e2\u00e3\u00cf\u00d3\n"

    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LbgP;->b:[B

    return-void
.end method

.method constructor <init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, LbgQ;

    invoke-direct {v0}, LbgQ;-><init>()V

    iput-object v0, p0, LbgP;->a:LbgQ;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, LbgP;->a:I

    .line 24
    sget-object v0, LbgM;->a:LbgM;

    iput-object v0, p0, LbgP;->a:LbgM;

    .line 35
    new-instance v0, LbgD;

    invoke-direct {v0, p1}, LbgD;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, LbgP;->a:LbgD;

    .line 36
    iput-object p2, p0, LbgP;->a:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;)LbgP;
    .locals 2

    .prologue
    .line 51
    new-instance v0, LbgP;

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1, p1}, LbgP;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    return-object v0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 164
    iget v0, p0, LbgP;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LbgP;->a:I

    return v0
.end method

.method private b(LbgF;)V
    .locals 2

    .prologue
    .line 109
    invoke-direct {p0}, LbgP;->b()I

    move-result v0

    invoke-virtual {p1, v0}, LbgF;->a(I)V

    .line 110
    invoke-virtual {p0}, LbgP;->a()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, LbgF;->a(J)V

    .line 111
    iget-object v0, p0, LbgP;->a:LbgQ;

    invoke-virtual {v0, p1}, LbgQ;->a(LbgG;)V

    .line 112
    iget-object v0, p0, LbgP;->a:LbgD;

    invoke-virtual {p1, v0}, LbgF;->d(Ljava/io/OutputStream;)I

    .line 113
    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, LbgP;->a:LbgD;

    invoke-virtual {v0}, LbgD;->a()I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, LbgP;->a:LbgD;

    sget-object v1, LbgP;->a:[B

    invoke-virtual {v0, v1}, LbgD;->write([B)V

    .line 71
    iget-object v0, p0, LbgP;->a:LbgD;

    sget-object v1, LbgP;->b:[B

    invoke-virtual {v0, v1}, LbgD;->write([B)V

    .line 72
    return-void
.end method

.method public a(LbgF;)V
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0, p1}, LbgP;->b(LbgF;)V

    .line 85
    new-instance v0, LbgK;

    invoke-direct {v0, p1}, LbgK;-><init>(LbgF;)V

    .line 86
    invoke-virtual {p0}, LbgP;->a()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LbgK;->a(J)V

    .line 87
    iget-object v1, p0, LbgP;->a:LbgQ;

    invoke-virtual {v1, v0}, LbgQ;->a(LbgG;)V

    .line 88
    iget-object v1, p0, LbgP;->a:LbgD;

    invoke-virtual {v0, v1}, LbgK;->d(Ljava/io/OutputStream;)I

    .line 90
    iget-object v1, p0, LbgP;->a:LbgL;

    if-nez v1, :cond_0

    .line 91
    new-instance v1, LbgL;

    invoke-direct {v1}, LbgL;-><init>()V

    iput-object v1, p0, LbgP;->a:LbgL;

    .line 92
    iget-object v1, p0, LbgP;->a:LbgQ;

    iget-object v2, p0, LbgP;->a:LbgL;

    invoke-virtual {v1, v2}, LbgQ;->a(LbgG;)V

    .line 95
    :cond_0
    new-instance v1, LbgJ;

    iget-object v2, p0, LbgP;->a:LbgL;

    iget-object v3, p0, LbgP;->a:LbgM;

    invoke-direct {v1, v2, p1, v0, v3}, LbgJ;-><init>(LbgL;LbgF;LbgK;LbgM;)V

    .line 96
    invoke-virtual {p0}, LbgP;->a()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, LbgJ;->a(J)V

    .line 97
    iget-object v0, p0, LbgP;->a:LbgQ;

    invoke-virtual {v0, v1}, LbgQ;->a(LbgG;)V

    .line 98
    iget-object v0, p0, LbgP;->a:LbgD;

    invoke-virtual {v1, v0}, LbgJ;->d(Ljava/io/OutputStream;)I

    .line 99
    iget-object v0, p0, LbgP;->a:LbgL;

    invoke-virtual {v0, v1}, LbgL;->a(LbgJ;)V

    .line 100
    return-void
.end method

.method public a(LbgM;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, LbgP;->a:LbgM;

    .line 62
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 124
    iget-object v0, p0, LbgP;->a:LbgL;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, LbgP;->a:LbgL;

    invoke-virtual {p0}, LbgP;->a()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LbgL;->a(J)V

    .line 126
    iget-object v0, p0, LbgP;->a:LbgL;

    iget-object v1, p0, LbgP;->a:LbgD;

    invoke-virtual {v0, v1}, LbgL;->d(Ljava/io/OutputStream;)I

    .line 130
    :cond_0
    new-instance v0, LbgE;

    iget-object v1, p0, LbgP;->a:LbgL;

    invoke-direct {v0, v1}, LbgE;-><init>(LbgL;)V

    .line 131
    invoke-virtual {p0}, LbgP;->a()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LbgE;->a(J)V

    .line 132
    iget-object v1, p0, LbgP;->a:LbgQ;

    invoke-virtual {v1, v0}, LbgQ;->a(LbgG;)V

    .line 133
    iget-object v1, p0, LbgP;->a:LbgD;

    invoke-virtual {v0, v1}, LbgE;->d(Ljava/io/OutputStream;)I

    .line 136
    new-instance v1, LbgH;

    iget-object v2, p0, LbgP;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, LbgH;-><init>(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, LbgP;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, LbgH;->a(J)V

    .line 138
    iget-object v2, p0, LbgP;->a:LbgQ;

    invoke-virtual {v2, v1}, LbgQ;->a(LbgG;)V

    .line 139
    iget-object v2, p0, LbgP;->a:LbgD;

    invoke-virtual {v1, v2}, LbgH;->d(Ljava/io/OutputStream;)I

    .line 142
    iget-object v2, p0, LbgP;->a:LbgQ;

    invoke-virtual {p0}, LbgP;->a()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, LbgQ;->a(J)V

    .line 143
    iget-object v2, p0, LbgP;->a:LbgQ;

    iget-object v3, p0, LbgP;->a:LbgD;

    invoke-virtual {v2, v3}, LbgQ;->d(Ljava/io/OutputStream;)I

    .line 144
    new-instance v2, LbgO;

    iget-object v3, p0, LbgP;->a:LbgQ;

    invoke-direct {v2, v0, v1, v3}, LbgO;-><init>(LbgE;LbgH;LbgQ;)V

    .line 145
    iget-object v0, p0, LbgP;->a:LbgD;

    invoke-virtual {v2, v0}, LbgO;->d(Ljava/io/OutputStream;)I

    .line 147
    iget-object v0, p0, LbgP;->a:LbgD;

    invoke-virtual {v0}, LbgD;->close()V

    .line 148
    return-void
.end method
