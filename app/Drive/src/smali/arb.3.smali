.class public abstract Larb;
.super Ljava/lang/Object;
.source "ActionBarHelperFactory.java"

# interfaces
.implements LaqZ;


# instance fields
.field private final a:LqK;

.field private final a:LsI;


# direct methods
.method protected constructor <init>(LqK;LsI;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p0, Larb;->a:LqK;

    .line 25
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsI;

    iput-object v0, p0, Larb;->a:LsI;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)LaqY;
    .locals 3

    .prologue
    .line 45
    instance-of v0, p1, Laju;

    invoke-static {v0}, LbiT;->a(Z)V

    .line 46
    new-instance v0, Lsd;

    iget-object v1, p0, Larb;->a:LsI;

    iget-object v2, p0, Larb;->a:LqK;

    invoke-direct {v0, p1, v1, v2}, Lsd;-><init>(Landroid/app/Activity;LsI;LqK;)V

    .line 48
    invoke-virtual {p0, p1, v0}, Larb;->a(Landroid/app/Activity;LarB;)LaqY;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;LarB;)LaqY;
    .locals 2

    .prologue
    .line 34
    instance-of v0, p1, Lrm;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 35
    check-cast v0, Lrm;

    invoke-virtual {v0}, Lrm;->a()LaFO;

    move-result-object v0

    .line 40
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Larb;->a(Landroid/app/Activity;LarB;LaFO;)LaqY;

    move-result-object v0

    return-object v0

    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Landroid/app/Activity;LarB;LaFO;)LaqY;
.end method
