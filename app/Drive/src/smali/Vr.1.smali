.class public LVr;
.super Ljava/lang/Object;
.source "MoveOperation.java"

# interfaces
.implements LVt;


# instance fields
.field private final a:LUi;

.field private final a:LaGM;

.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:LqK;

.field private final b:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method constructor <init>(LqK;LUi;LaGM;Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LqK;",
            "LUi;",
            "LaGM;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, LVr;->a:LqK;

    .line 63
    invoke-virtual {p5, p6}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 64
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LVr;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 65
    iput-object p5, p0, LVr;->a:LbmY;

    .line 66
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LVr;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 67
    iput-object p2, p0, LVr;->a:LUi;

    .line 68
    iput-object p3, p0, LVr;->a:LaGM;

    .line 69
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 73
    new-instance v0, LaHl;

    const-string v1, "MoveOperation"

    invoke-direct {v0, v1}, LaHl;-><init>(Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, LVr;->a:LUi;

    iget-object v2, p0, LVr;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, p0, LVr;->a:LbmY;

    iget-object v4, p0, LVr;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v4}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4, v0}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;LbmY;LaHy;)V

    .line 75
    invoke-virtual {v0}, LaHl;->a()V

    .line 77
    iget-object v0, p0, LVr;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "moveEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, LVr;->a:LaGM;

    iget-object v1, p0, LVr;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 83
    invoke-interface {v0, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 85
    iget-object v1, p0, LVr;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v1}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v1

    invoke-virtual {v1, v0}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, LaHl;

    const-string v1, "MoveOperation.Undo"

    invoke-direct {v0, v1}, LaHl;-><init>(Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, LVr;->a:LUi;

    iget-object v2, p0, LVr;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, p0, LVr;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v3}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v3

    iget-object v4, p0, LVr;->a:LbmY;

    invoke-interface {v1, v2, v3, v4, v0}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;LbmY;LaHy;)V

    .line 89
    invoke-virtual {v0}, LaHl;->a()V

    .line 90
    iget-object v0, p0, LVr;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "moveUndoEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    const-string v0, "MoveOperation"

    const-string v1, "Undo failed because parent folder has changed."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
