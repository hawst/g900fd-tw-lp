.class public LGD;
.super LafH;
.source "ProgressSyncMonitor.java"


# instance fields
.field private final a:Lamr;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lamr;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, LafH;-><init>()V

    .line 30
    sget v0, Lxi;->download_bytes_format:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGD;->a:Ljava/lang/String;

    .line 31
    sget v0, Lxi;->download_bytes_format_short:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGD;->b:Ljava/lang/String;

    .line 32
    sget v0, Lxi;->download_kilobytes_format:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGD;->c:Ljava/lang/String;

    .line 33
    sget v0, Lxi;->download_kilobytes_format_short:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGD;->d:Ljava/lang/String;

    .line 34
    sget v0, Lxi;->download_megabytes_format:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGD;->e:Ljava/lang/String;

    .line 35
    sget v0, Lxi;->download_megabytes_format_short:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGD;->f:Ljava/lang/String;

    .line 36
    iput-object p2, p0, LGD;->a:Lamr;

    .line 37
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .locals 9

    .prologue
    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 66
    long-to-double v0, p1

    cmpg-double v0, v0, v4

    if-gez v0, :cond_0

    .line 67
    iget-object v0, p0, LGD;->b:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    .line 68
    :cond_0
    long-to-double v0, p1

    cmpg-double v0, v0, v6

    if-gez v0, :cond_1

    .line 69
    iget-object v0, p0, LGD;->d:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v4, v5}, LGD;->a(JD)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_1
    iget-object v0, p0, LGD;->f:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v6, v7}, LGD;->a(JD)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(JD)Ljava/lang/String;
    .locals 5

    .prologue
    .line 76
    .line 77
    long-to-double v0, p1

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    mul-double/2addr v2, p3

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 78
    const/4 v0, 0x2

    .line 85
    :goto_0
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    .line 86
    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 87
    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 88
    long-to-double v2, p1

    div-double/2addr v2, p3

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 79
    :cond_0
    long-to-double v0, p1

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, p3

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 80
    const/4 v0, 0x1

    goto :goto_0

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JJ)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 53
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    long-to-double v2, p1

    mul-double/2addr v0, v2

    long-to-double v2, p3

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 54
    long-to-double v2, p3

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 55
    iget-object v1, p0, LGD;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    :goto_0
    return-object v0

    .line 56
    :cond_0
    long-to-double v2, p3

    cmpg-double v1, v2, v6

    if-gez v1, :cond_1

    .line 57
    iget-object v1, p0, LGD;->c:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v4, v5}, LGD;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    .line 58
    invoke-direct {p0, p3, p4, v4, v5}, LGD;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v10

    .line 57
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, LGD;->e:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v6, v7}, LGD;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    .line 61
    invoke-direct {p0, p3, p4, v6, v7}, LGD;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v10

    .line 60
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(JJ)V
    .locals 7

    .prologue
    .line 41
    iget-object v0, p0, LGD;->a:Lamr;

    if-nez v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 45
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, LGD;->a(JJ)Ljava/lang/String;

    move-result-object v6

    .line 49
    :goto_1
    iget-object v1, p0, LGD;->a:Lamr;

    move-wide v2, p1

    move-wide v4, p3

    invoke-interface/range {v1 .. v6}, Lamr;->a(JJLjava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_1
    invoke-direct {p0, p1, p2}, LGD;->a(J)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method
