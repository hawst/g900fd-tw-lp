.class public final LbnA;
.super LblS;
.source "LinkedHashMultimap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LblS",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field transient a:I

.field private transient a:LbnB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnB",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 230
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-direct {p0, v0}, LblS;-><init>(Ljava/util/Map;)V

    .line 225
    const/4 v0, 0x2

    iput v0, p0, LbnA;->a:I

    .line 232
    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expectedValuesPerKey must be >= 0 but was %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 235
    iput p2, p0, LbnA;->a:I

    .line 236
    new-instance v0, LbnB;

    invoke-direct {v0, v5, v5, v2, v5}, LbnB;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILbnB;)V

    iput-object v0, p0, LbnA;->a:LbnB;

    .line 237
    iget-object v0, p0, LbnA;->a:LbnB;

    iget-object v1, p0, LbnA;->a:LbnB;

    invoke-static {v0, v1}, LbnA;->b(LbnB;LbnB;)V

    .line 238
    return-void

    :cond_0
    move v0, v2

    .line 232
    goto :goto_0
.end method

.method public static a()LbnA;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LbnA",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, LbnA;

    const/16 v1, 0x10

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LbnA;-><init>(II)V

    return-object v0
.end method

.method static synthetic a(LbnA;)LbnB;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, LbnA;->a:LbnB;

    return-object v0
.end method

.method static synthetic a(LbnB;)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0}, LbnA;->b(LbnB;)V

    return-void
.end method

.method static synthetic a(LbnB;LbnB;)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0, p1}, LbnA;->b(LbnB;LbnB;)V

    return-void
.end method

.method static synthetic a(LbnE;)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0}, LbnA;->b(LbnE;)V

    return-void
.end method

.method static synthetic a(LbnE;LbnE;)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0, p1}, LbnA;->b(LbnE;LbnE;)V

    return-void
.end method

.method private static b(LbnB;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LbnB",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p0}, LbnB;->a()LbnB;

    move-result-object v0

    invoke-virtual {p0}, LbnB;->b()LbnB;

    move-result-object v1

    invoke-static {v0, v1}, LbnA;->b(LbnB;LbnB;)V

    .line 146
    return-void
.end method

.method private static b(LbnB;LbnB;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LbnB",
            "<TK;TV;>;",
            "LbnB",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0, p1}, LbnB;->a(LbnB;)V

    .line 137
    invoke-virtual {p1, p0}, LbnB;->b(LbnB;)V

    .line 138
    return-void
.end method

.method private static b(LbnE;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LbnE",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-interface {p0}, LbnE;->a()LbnE;

    move-result-object v0

    invoke-interface {p0}, LbnE;->b()LbnE;

    move-result-object v1

    invoke-static {v0, v1}, LbnA;->b(LbnE;LbnE;)V

    .line 142
    return-void
.end method

.method private static b(LbnE;LbnE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LbnE",
            "<TK;TV;>;",
            "LbnE",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 131
    invoke-interface {p0, p1}, LbnE;->b(LbnE;)V

    .line 132
    invoke-interface {p1, p0}, LbnE;->a(LbnE;)V

    .line 133
    return-void
.end method


# virtual methods
.method public bridge synthetic a()I
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, LblS;->a()I

    move-result v0

    return v0
.end method

.method synthetic a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, LbnA;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 267
    new-instance v0, LbnC;

    iget v1, p0, LbnA;->a:I

    invoke-direct {v0, p0, p1, v1}, LbnC;-><init>(LbnA;Ljava/lang/Object;I)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, LblS;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 540
    invoke-super {p0}, LblS;->a()V

    .line 541
    iget-object v0, p0, LbnA;->a:LbnB;

    iget-object v1, p0, LbnA;->a:LbnB;

    invoke-static {v0, v1}, LbnA;->b(LbnB;LbnB;)V

    .line 542
    return-void
.end method

.method public bridge synthetic a()Z
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, LblS;->a()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, LblS;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, LblS;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, LblS;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, LblS;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method c()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 252
    new-instance v0, Ljava/util/LinkedHashSet;

    iget v1, p0, LbnA;->a:I

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    return-object v0
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, LblS;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, LblS;->hashCode()I

    move-result v0

    return v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, LblS;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
