.class public LxK;
.super Ljava/lang/Object;
.source "DocsCentricTaskCompatImpl.java"

# interfaces
.implements LsF;


# instance fields
.field a:LaIm;

.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LxK;->a:LaIm;

    invoke-interface {v0, p1}, LaIm;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, LxK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v0

    .line 36
    iget-object v1, p0, LxK;->a:Landroid/content/Context;

    invoke-static {v1, v0, p2, p3}, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
