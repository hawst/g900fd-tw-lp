.class public LKZ;
.super Ljava/lang/Object;
.source "SelectionCardAnimator.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:I

.field private final a:LJR;

.field private final a:LKp;

.field private final a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(LKp;LJR;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, LKZ;->a:LKp;

    .line 55
    iput-object p2, p0, LKZ;->a:LJR;

    .line 56
    iput-object p3, p0, LKZ;->a:Landroid/app/Activity;

    .line 57
    invoke-virtual {p3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 58
    sget v1, Lxa;->shadow_border_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LKZ;->a:I

    .line 59
    return-void
.end method

.method static synthetic a(LKZ;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, LKZ;->a:I

    return v0
.end method

.method static synthetic a(LKZ;)LJR;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LKZ;->a:LJR;

    return-object v0
.end method

.method static synthetic a(LKZ;)LKp;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LKZ;->a:LKp;

    return-object v0
.end method

.method private a()Landroid/view/View;
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, LKZ;->a:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    .line 259
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 261
    sget v1, Lxe;->selected_entry_card:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, LKZ;->a:Landroid/app/Activity;

    sget v1, Lxc;->selection_floating_handle:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    return-object v0
.end method

.method static synthetic a(LKZ;Landroid/view/View;IIIII)V
    .locals 0

    .prologue
    .line 37
    invoke-direct/range {p0 .. p6}, LKZ;->a(Landroid/view/View;IIIII)V

    return-void
.end method

.method static synthetic a(LKZ;Landroid/view/View;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, LKZ;->a(Landroid/view/View;Landroid/view/View;I)V

    return-void
.end method

.method private a(Landroid/view/View;IIIII)V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 187
    sget v0, Lxc;->title_background:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 188
    sget v1, Lxc;->shadow:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 190
    invoke-static {}, Lxm;->a()Lxr;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    .line 192
    invoke-static {v0, v3}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    .line 191
    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    const/16 v3, 0x82

    .line 193
    invoke-virtual {v0, v3}, Lxr;->a(I)Lxr;

    move-result-object v0

    .line 191
    invoke-virtual {v2, v0}, Lxr;->a(Lxr;)Lxr;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_1

    .line 195
    invoke-static {v1, v2}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v1

    .line 194
    invoke-static {v1}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    const/16 v2, 0x64

    .line 196
    invoke-virtual {v1, v2}, Lxr;->a(I)Lxr;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, Lxr;->a(Lxr;)Lxr;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v1

    .line 199
    iget v0, p0, LKZ;->a:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p4, v0

    sub-int v2, v0, p6

    .line 201
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v3, 0x0

    aput p4, v0, v3

    const/4 v3, 0x1

    sub-int v4, p4, v2

    aput v4, v0, v3

    .line 202
    invoke-static {p1, v0}, Lxm;->b(Landroid/view/View;[I)Landroid/animation/Animator;

    move-result-object v0

    .line 201
    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    add-int v5, p3, v2

    int-to-float v5, v5

    aput v5, v3, v4

    .line 203
    invoke-static {p1, v3}, Lxm;->c(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v0, v3}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v3

    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 204
    :goto_0
    invoke-virtual {v3, v0}, Lxr;->a(I)Lxr;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v3

    .line 207
    invoke-direct {p0}, LKZ;->a()Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    move-result-object v4

    .line 208
    invoke-virtual {v4}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getWidth()I

    move-result v0

    iget v5, p0, LKZ;->a:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    .line 209
    invoke-virtual {v4}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getHeight()I

    move-result v5

    iget v6, p0, LKZ;->a:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    .line 210
    invoke-virtual {v4}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getX()F

    move-result v6

    iget v7, p0, LKZ;->a:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    .line 211
    invoke-virtual {v4}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getY()F

    move-result v7

    iget v8, p0, LKZ;->a:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    .line 212
    float-to-double v8, v6

    float-to-double v10, v7

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v8

    .line 214
    const/4 v10, 0x2

    new-array v10, v10, [I

    const/4 v11, 0x0

    aput p5, v10, v11

    const/4 v11, 0x1

    aput v0, v10, v11

    .line 215
    invoke-static {p1, v10}, Lxm;->a(Landroid/view/View;[I)Landroid/animation/Animator;

    move-result-object v0

    .line 214
    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    const/4 v10, 0x2

    new-array v10, v10, [I

    const/4 v11, 0x0

    sub-int v12, p4, v2

    aput v12, v10, v11

    const/4 v11, 0x1

    aput v5, v10, v11

    .line 216
    invoke-static {p1, v10}, Lxm;->b(Landroid/view/View;[I)Landroid/animation/Animator;

    move-result-object v5

    invoke-virtual {v0, v5}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v10, 0x0

    int-to-float v11, p2

    aput v11, v5, v10

    const/4 v10, 0x1

    aput v6, v5, v10

    .line 217
    invoke-static {p1, v5}, Lxm;->c(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v5

    invoke-virtual {v0, v5}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    add-int v2, v2, p3

    int-to-float v2, v2

    aput v2, v5, v6

    const/4 v2, 0x1

    aput v7, v5, v2

    .line 218
    invoke-static {p1, v5}, Lxm;->b(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v0, v2}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    const-wide/16 v6, 0x0

    cmpl-double v0, v8, v6

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 219
    :goto_1
    invoke-virtual {v2, v0}, Lxr;->a(I)Lxr;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 222
    invoke-static {v1}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    .line 223
    invoke-virtual {v1, v3}, Lxr;->b(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    .line 224
    invoke-virtual {v1, v0}, Lxr;->b(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    new-instance v1, LLd;

    invoke-direct {v1, p0, v4}, LLd;-><init>(LKZ;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    .line 225
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 232
    iget-object v1, p0, LKZ;->a:LKp;

    invoke-virtual {v1, p1, v0}, LKp;->a(Landroid/view/View;Landroid/animation/Animator;)V

    .line 233
    return-void

    .line 203
    :cond_0
    const/16 v0, 0x64

    goto/16 :goto_0

    .line 218
    :cond_1
    const/16 v0, 0xc8

    goto :goto_1

    .line 190
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 191
    :array_1
    .array-data 4
        0x0
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method private a(Landroid/view/View;Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 169
    sget v0, Lxc;->title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 170
    sget v1, Lxc;->card_title:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 172
    iput p3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 173
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    sget v1, Lxc;->title:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 175
    if-eqz v1, :cond_0

    .line 176
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 177
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    .line 178
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 181
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 236
    invoke-direct {p0}, LKZ;->a()Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getX()F

    move-result v1

    .line 239
    new-array v2, v5, [F

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    const/high16 v4, 0x43960000    # 300.0f

    sub-float v4, v1, v4

    aput v4, v2, v3

    .line 240
    invoke-static {v0, v2}, Lxm;->c(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v2

    .line 239
    invoke-static {v2}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    new-array v3, v5, [F

    fill-array-data v3, :array_0

    .line 241
    invoke-static {v0, v3}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v2, v3}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    const/16 v3, 0x12c

    .line 242
    invoke-virtual {v2, v3}, Lxr;->a(I)Lxr;

    move-result-object v2

    new-instance v3, LLe;

    invoke-direct {v3, p0, v0, v1}, LLe;-><init>(LKZ;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;F)V

    .line 243
    invoke-virtual {v2, v3}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 250
    invoke-virtual {v0}, Lxr;->a()Landroid/animation/Animator;

    .line 251
    return-void

    .line 239
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public a(Landroid/view/View;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, LKZ;->a()Landroid/view/View;

    move-result-object v0

    .line 64
    invoke-direct {p0}, LKZ;->a()Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->g()V

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, LLa;

    invoke-direct {v3, p0, p1, v1, v0}, LLa;-><init>(LKZ;Landroid/view/View;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 109
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0}, LKZ;->a()Landroid/view/View;

    move-result-object v0

    .line 115
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LLc;

    invoke-direct {v2, p0, p1, v0}, LLc;-><init>(LKZ;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 166
    return-void
.end method
