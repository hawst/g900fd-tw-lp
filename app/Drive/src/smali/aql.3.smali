.class public final enum Laql;
.super Ljava/lang/Enum;
.source "LinearLayoutListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laql;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laql;

.field private static final synthetic a:[Laql;

.field public static final enum b:Laql;


# instance fields
.field private final a:I

.field private final a:Laqq;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 107
    new-instance v0, Laql;

    const-string v1, "HORIZONTAL"

    new-instance v5, Laqm;

    invoke-direct {v5}, Laqm;-><init>()V

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Laql;-><init>(Ljava/lang/String;IIILaqq;)V

    sput-object v0, Laql;->a:Laql;

    .line 129
    new-instance v3, Laql;

    const-string v4, "VERTICAL"

    new-instance v8, Laqo;

    invoke-direct {v8}, Laqo;-><init>()V

    move v5, v9

    move v6, v9

    move v7, v9

    invoke-direct/range {v3 .. v8}, Laql;-><init>(Ljava/lang/String;IIILaqq;)V

    sput-object v3, Laql;->b:Laql;

    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [Laql;

    sget-object v1, Laql;->a:Laql;

    aput-object v1, v0, v2

    sget-object v1, Laql;->b:Laql;

    aput-object v1, v0, v9

    sput-object v0, Laql;->a:[Laql;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILaqq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Laqq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 162
    iput p3, p0, Laql;->a:I

    .line 163
    iput p4, p0, Laql;->b:I

    .line 164
    iput-object p5, p0, Laql;->a:Laqq;

    .line 165
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Laql;->a:I

    return v0
.end method

.method private a(II)I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Laql;->b:I

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    move p1, p2

    goto :goto_0
.end method

.method public static synthetic a(Laql;)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Laql;->b()I

    move-result v0

    return v0
.end method

.method public static synthetic a(Laql;II)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Laql;->a(II)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Laql;Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Laqr;
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Laql;->a(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Laqr;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Laqr;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Laql;->a:Laqq;

    invoke-interface {v0, p1, p2}, Laqq;->a(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Laqr;

    move-result-object v0

    return-object v0
.end method

.method private b()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Laql;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Laql;)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Laql;->c()I

    move-result v0

    return v0
.end method

.method private c()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 184
    iget v1, p0, Laql;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Laql;)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Laql;->a()I

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Laql;
    .locals 1

    .prologue
    .line 106
    const-class v0, Laql;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laql;

    return-object v0
.end method

.method public static values()[Laql;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Laql;->a:[Laql;

    invoke-virtual {v0}, [Laql;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laql;

    return-object v0
.end method
