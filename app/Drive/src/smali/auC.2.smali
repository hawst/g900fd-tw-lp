.class public LauC;
.super Ljava/lang/Object;
.source "CastManager.java"


# static fields
.field private static a:LauC;


# instance fields
.field private a:I

.field private a:LaCV;

.field private final a:LaCX;

.field private final a:LaCY;

.field private final a:LaCd;

.field private a:LaCh;

.field private a:LaCn;

.field private a:Landroid/content/Context;

.field private a:LauB;

.field private a:LauJ;

.field private a:LauM;

.field private a:Lcom/google/android/gms/cast/CastDevice;

.field public a:Ljava/lang/Integer;

.field private a:Ljava/lang/String;

.field private a:Llj;

.field private a:Z

.field private a:[LauM;

.field private b:I

.field private b:Z


# direct methods
.method private a(LauM;Z)LaCh;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 443
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 446
    :try_start_0
    iget-object v0, p1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    if-eqz v0, :cond_0

    .line 447
    const-string v0, "extractUrl"

    iget-object v2, p1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v2, v2, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 449
    :cond_0
    iget-object v0, p1, LauM;->b:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    if-eqz v0, :cond_1

    .line 450
    const-string v0, "loResUrl"

    iget-object v2, p1, LauM;->b:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v2, v2, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 452
    :cond_1
    const-string v0, "preload"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    :goto_0
    iget-object v0, p1, LauM;->c:Ljava/lang/String;

    const-string v2, "[/]"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    .line 459
    const-string v2, "video"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 460
    new-instance v0, LaCj;

    invoke-direct {v0, v4}, LaCj;-><init>(I)V

    .line 466
    :goto_1
    const-string v2, "com.google.android.gms.cast.metadata.TITLE"

    iget-object v3, p1, LauM;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LaCj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    new-instance v2, LaCi;

    iget-object v3, p1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v3, v3, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LaCi;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LauM;->c:Ljava/lang/String;

    .line 469
    invoke-virtual {v2, v3}, LaCi;->a(Ljava/lang/String;)LaCi;

    move-result-object v2

    .line 470
    invoke-virtual {v2, v4}, LaCi;->a(I)LaCi;

    move-result-object v2

    .line 471
    invoke-virtual {v2, v0}, LaCi;->a(LaCj;)LaCi;

    move-result-object v0

    .line 472
    invoke-virtual {v0, v1}, LaCi;->a(Lorg/json/JSONObject;)LaCi;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, LaCi;->a()LaCh;

    move-result-object v0

    .line 474
    return-object v0

    .line 453
    :catch_0
    move-exception v0

    .line 454
    const-string v0, "Casting"

    const-string v2, "Fail to construct JSON Object"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 461
    :cond_2
    const-string v2, "audio"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 462
    new-instance v0, LaCj;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, LaCj;-><init>(I)V

    goto :goto_1

    .line 464
    :cond_3
    new-instance v0, LaCj;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, LaCj;-><init>(I)V

    goto :goto_1
.end method

.method public static a()LauC;
    .locals 2

    .prologue
    .line 109
    sget-object v0, LauC;->a:LauC;

    if-nez v0, :cond_0

    .line 110
    const-string v0, "Casting"

    const-string v1, "No Cast Manager was built!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x0

    .line 113
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LauC;->a:LauC;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, LauC;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v1, p0, LauC;->a:LaCd;

    .line 184
    invoke-static {v0, v1}, LaCb;->a(Lcom/google/android/gms/cast/CastDevice;LaCd;)LaCc;

    move-result-object v0

    invoke-virtual {v0}, LaCc;->a()LaCb;

    move-result-object v0

    .line 185
    new-instance v1, LaCW;

    invoke-direct {v1, p1}, LaCW;-><init>(Landroid/content/Context;)V

    sget-object v2, LaBU;->a:LaCM;

    invoke-virtual {v1, v2, v0}, LaCW;->a(LaCM;LaCO;)LaCW;

    move-result-object v0

    iget-object v1, p0, LauC;->a:LaCX;

    .line 186
    invoke-virtual {v0, v1}, LaCW;->a(LaCX;)LaCW;

    move-result-object v0

    iget-object v1, p0, LauC;->a:LaCY;

    .line 187
    invoke-virtual {v0, v1}, LaCW;->a(LaCY;)LaCW;

    move-result-object v0

    invoke-virtual {v0}, LaCW;->a()LaCV;

    move-result-object v0

    iput-object v0, p0, LauC;->a:LaCV;

    .line 188
    iget-object v0, p0, LauC;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()V

    .line 189
    return-void
.end method

.method static synthetic a(LauC;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, LauC;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 3

    .prologue
    .line 164
    iput-object p1, p0, LauC;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 165
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LauC;->a:Ljava/lang/String;

    .line 167
    iget-object v0, p0, LauC;->a:Lcom/google/android/gms/cast/CastDevice;

    if-eqz v0, :cond_1

    .line 169
    :try_start_0
    invoke-direct {p0}, LauC;->e()V

    .line 170
    invoke-direct {p0}, LauC;->d()V

    .line 171
    iget-object v0, p0, LauC;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, LauC;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_1
    return-void

    .line 165
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    const-string v1, "Casting"

    const-string v2, "Exception while connecting API client"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 174
    invoke-direct {p0}, LauC;->d()V

    goto :goto_1

    .line 177
    :cond_1
    invoke-direct {p0}, LauC;->d()V

    .line 178
    iget-object v0, p0, LauC;->a:Llj;

    iget-object v1, p0, LauC;->a:Llj;

    invoke-virtual {v1}, Llj;->a()Llv;

    move-result-object v1

    invoke-virtual {v0, v1}, Llj;->a(Llv;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 480
    iget-object v0, p0, LauC;->a:LaCV;

    if-eqz v0, :cond_0

    iget-object v0, p0, LauC;->a:LauJ;

    if-eqz v0, :cond_0

    .line 482
    :try_start_0
    sget-object v0, LaBU;->a:LaBX;

    iget-object v1, p0, LauC;->a:LaCV;

    const-string v2, "urn:x-cast:com.google.android.apps.viewer"

    invoke-interface {v0, v1, v2, p1}, LaBX;->a(LaCV;Ljava/lang/String;Ljava/lang/String;)LaCZ;

    move-result-object v0

    new-instance v1, LauE;

    invoke-direct {v1, p0}, LauE;-><init>(LauC;)V

    .line 483
    invoke-interface {v0, v1}, LaCZ;->a(LaDd;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 491
    :catch_0
    move-exception v0

    .line 492
    const-string v1, "Casting"

    const-string v2, "Exception while sending message"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 336
    iput-boolean p1, p0, LauC;->b:Z

    .line 337
    const-string v0, "Casting"

    const-string v1, "Casting status changed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 339
    iget-object v0, p0, LauC;->a:LauM;

    if-eqz v0, :cond_0

    .line 340
    const-string v0, "Casting"

    const-string v1, "Try casting the last file"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    iget-object v0, p0, LauC;->a:LauM;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LauC;->a(LauM;Z)V

    .line 343
    :cond_0
    invoke-direct {p0}, LauC;->g()V

    .line 347
    :goto_0
    return-void

    .line 345
    :cond_1
    invoke-direct {p0}, LauC;->f()V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, LauC;->a:LaCV;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, LauC;->a:LaCV;

    invoke-interface {v0}, LaCV;->b()V

    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, LauC;->a:LaCV;

    .line 315
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, LauC;->a:LaCV;

    if-nez v0, :cond_1

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    iget-boolean v0, p0, LauC;->a:Z

    if-eqz v0, :cond_0

    .line 324
    sget-object v0, LaBU;->a:LaBX;

    iget-object v1, p0, LauC;->a:LaCV;

    invoke-interface {v0, v1}, LaBX;->a(LaCV;)LaCZ;

    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, LauC;->a:Z

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 350
    const-string v0, "Casting"

    const-string v1, "Stoping Notifications."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LauC;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/viewer/cast/CastNotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 352
    iget-object v1, p0, LauC;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 353
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 356
    const-string v0, "Casting"

    const-string v1, "Creating Notifications."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LauC;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/viewer/cast/CastNotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 358
    iget-object v1, p0, LauC;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 359
    return-void
.end method


# virtual methods
.method public a()LaCh;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, LauC;->a:LaCh;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, LauC;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 305
    invoke-direct {p0}, LauC;->e()V

    .line 306
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LauC;->a(Lcom/google/android/gms/cast/CastDevice;)V

    .line 307
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LauC;->b(Z)V

    .line 308
    return-void
.end method

.method public a(LaCh;)V
    .locals 3

    .prologue
    .line 405
    iget-object v0, p0, LauC;->a:LaCn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LauC;->a:LaCV;

    if-nez v0, :cond_1

    .line 406
    :cond_0
    const-string v0, "Casting"

    const-string v1, "Remote player is null of apiclient is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    :goto_0
    return-void

    .line 410
    :cond_1
    iget-object v0, p0, LauC;->a:LaCn;

    iget-object v1, p0, LauC;->a:LaCV;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, LaCn;->a(LaCV;LaCh;Z)LaCZ;

    move-result-object v0

    new-instance v1, LauD;

    invoke-direct {v1, p0}, LauD;-><init>(LauC;)V

    invoke-interface {v0, v1}, LaCZ;->a(LaDd;)V

    .line 432
    invoke-direct {p0}, LauC;->g()V

    goto :goto_0
.end method

.method public a(LauB;)V
    .locals 0

    .prologue
    .line 545
    iput-object p1, p0, LauC;->a:LauB;

    .line 546
    return-void
.end method

.method public a(LauM;Z)V
    .locals 2

    .prologue
    .line 388
    iget-boolean v0, p0, LauC;->b:Z

    if-nez v0, :cond_0

    .line 389
    const-string v0, "Casting"

    const-string v1, "Casting is not set up"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    :goto_0
    return-void

    .line 392
    :cond_0
    if-nez p1, :cond_1

    .line 393
    const-string v0, "Casting"

    const-string v1, "File is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 397
    :cond_1
    invoke-direct {p0, p1, p2}, LauC;->a(LauM;Z)LaCh;

    move-result-object v0

    .line 398
    if-nez p2, :cond_2

    .line 399
    iput-object v0, p0, LauC;->a:LaCh;

    .line 401
    :cond_2
    invoke-virtual {p0, v0}, LauC;->a(LaCh;)V

    goto :goto_0
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 599
    const-string v0, "Casting"

    const-string v1, "attempting to play media"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    iget-object v0, p0, LauC;->a:LaCn;

    if-nez v0, :cond_0

    .line 601
    const-string v0, "Casting"

    const-string v1, "Trying to play a video with no active media session"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    :goto_0
    return-void

    .line 604
    :cond_0
    iget-object v0, p0, LauC;->a:LaCn;

    iget-object v1, p0, LauC;->a:LaCV;

    invoke-virtual {v0, v1, p1}, LaCn;->b(LaCV;Lorg/json/JSONObject;)LaCZ;

    move-result-object v0

    new-instance v1, LauF;

    invoke-direct {v1, p0}, LauF;-><init>(LauC;)V

    invoke-interface {v0, v1}, LaCZ;->a(LaDd;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 573
    iget-object v1, p0, LauC;->a:[LauM;

    if-eqz v1, :cond_0

    iget-object v1, p0, LauC;->a:Ljava/lang/Integer;

    if-nez v1, :cond_2

    .line 574
    :cond_0
    const-string v0, "Casting"

    const-string v1, "Filelist not exist."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :cond_1
    :goto_0
    return-void

    .line 576
    :cond_2
    iget-object v1, p0, LauC;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p1, v0, :cond_3

    :goto_1
    add-int/2addr v0, v1

    .line 577
    iget-object v1, p0, LauC;->a:[LauM;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 578
    iget-object v1, p0, LauC;->a:[LauM;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LauC;->a([LauM;Ljava/lang/Integer;)V

    goto :goto_0

    .line 576
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a([LauM;Ljava/lang/Integer;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 369
    const-string v1, "Casting"

    const-string v2, "try to cast now"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, p1, v1

    .line 371
    iput-object p1, p0, LauC;->a:[LauM;

    .line 372
    iput-object p2, p0, LauC;->a:Ljava/lang/Integer;

    .line 373
    iput-object v1, p0, LauC;->a:LauM;

    .line 374
    invoke-virtual {p0, v1, v0}, LauC;->a(LauM;Z)V

    .line 377
    :goto_0
    if-ge v0, v3, :cond_0

    .line 378
    iget-object v1, p0, LauC;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    .line 379
    array-length v2, p1

    if-lt v1, v2, :cond_1

    .line 385
    :cond_0
    return-void

    .line 382
    :cond_1
    aget-object v1, p1, v1

    invoke-virtual {p0, v1, v3}, LauC;->a(LauM;Z)V

    .line 377
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 561
    iget v0, p0, LauC;->a:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, LauC;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 549
    const/4 v0, 0x0

    iput-object v0, p0, LauC;->a:LauB;

    .line 550
    return-void
.end method

.method public b(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 617
    const-string v0, "Casting"

    const-string v1, "attempting to pause media"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    iget-object v0, p0, LauC;->a:LaCn;

    if-nez v0, :cond_0

    .line 619
    const-string v0, "Casting"

    const-string v1, "Trying to pause a video with no active media session"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    :goto_0
    return-void

    .line 622
    :cond_0
    iget-object v0, p0, LauC;->a:LaCn;

    iget-object v1, p0, LauC;->a:LaCV;

    invoke-virtual {v0, v1, p1}, LaCn;->a(LaCV;Lorg/json/JSONObject;)LaCZ;

    move-result-object v0

    new-instance v1, LauG;

    invoke-direct {v1, p0}, LauG;-><init>(LauC;)V

    invoke-interface {v0, v1}, LaCZ;->a(LaDd;)V

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 585
    invoke-virtual {p0}, LauC;->a()Z

    move-result v0

    .line 586
    if-eqz v0, :cond_0

    .line 587
    invoke-virtual {p0, v2}, LauC;->b(Lorg/json/JSONObject;)V

    .line 596
    :goto_0
    return-void

    .line 589
    :cond_0
    iget v0, p0, LauC;->a:I

    if-ne v0, v1, :cond_1

    iget v0, p0, LauC;->b:I

    if-ne v0, v1, :cond_1

    .line 591
    invoke-virtual {p0}, LauC;->a()LaCh;

    move-result-object v0

    invoke-virtual {p0, v0}, LauC;->a(LaCh;)V

    goto :goto_0

    .line 593
    :cond_1
    invoke-virtual {p0, v2}, LauC;->a(Lorg/json/JSONObject;)V

    goto :goto_0
.end method
