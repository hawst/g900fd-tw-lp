.class public LHF;
.super Ljava/lang/Object;
.source "DocGridCellAdapter.java"

# interfaces
.implements LEc;
.implements LHI;
.implements LIz;


# instance fields
.field private final a:LDY;

.field private a:LHK;

.field private final a:LHL;

.field private final a:LHp;

.field private a:LIf;

.field private a:LaGA;

.field private final a:LzN;


# direct methods
.method constructor <init>(LQX;Lapd;LHp;LzN;LHL;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHp;

    iput-object v0, p0, LHF;->a:LHp;

    .line 73
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzN;

    iput-object v0, p0, LHF;->a:LzN;

    .line 74
    new-instance v0, LDY;

    invoke-direct {v0, p2, p0}, LDY;-><init>(Lapd;LEc;)V

    iput-object v0, p0, LHF;->a:LDY;

    .line 75
    iput-object p5, p0, LHF;->a:LHL;

    .line 76
    invoke-virtual {p0, p1}, LHF;->a(LQX;)V

    .line 77
    return-void
.end method


# virtual methods
.method public a(I)LEd;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 166
    iget-object v0, p0, LHF;->a:LHp;

    invoke-interface {v0, p1}, LHp;->a(I)Landroid/view/View;

    move-result-object v0

    .line 167
    if-nez v0, :cond_0

    move-object v0, v1

    .line 175
    :goto_0
    return-object v0

    .line 170
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 171
    instance-of v2, v0, LHq;

    if-eqz v2, :cond_1

    .line 172
    check-cast v0, LHq;

    .line 173
    invoke-virtual {v0}, LHq;->a()LEd;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 175
    goto :goto_0
.end method

.method public a(I)LIL;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, LHF;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    .line 138
    iget-object v0, p0, LHF;->a:LIf;

    iget-object v1, p0, LHF;->a:LaGA;

    invoke-virtual {v0, v1}, LIf;->a(LCv;)LIL;

    move-result-object v0

    return-object v0
.end method

.method public a(I)LIy;
    .locals 5

    .prologue
    .line 143
    iget-object v0, p0, LHF;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    move-result v0

    .line 144
    const-string v1, "Failed to move to position %s for cursor %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 145
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LHF;->a:LaGA;

    aput-object v4, v2, v3

    .line 144
    invoke-static {v0, v1, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 146
    iget-object v0, p0, LHF;->a:LIf;

    iget-object v1, p0, LHF;->a:LaGA;

    invoke-virtual {v0, v1}, LIf;->b(LCv;)LIy;

    move-result-object v0

    return-object v0
.end method

.method public a(I)LaGv;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, LHF;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    .line 88
    iget-object v0, p0, LHF;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()LaGv;

    move-result-object v0

    return-object v0
.end method

.method public a(IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 82
    iget-object v0, p0, LHF;->a:LHK;

    iget-object v1, p0, LHF;->a:LaGA;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, LHK;->a(LaGA;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, LHF;->a:LHK;

    iget-object v1, p0, LHF;->a:LaGA;

    invoke-interface {v0, v1, p1}, LHK;->a(LaGA;I)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    return-object v0
.end method

.method public a()LzN;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, LHF;->a:LzN;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, LHF;->a:LDY;

    invoke-virtual {v0}, LDY;->b()V

    .line 123
    return-void
.end method

.method public a(LQX;)V
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p1}, LQX;->a()LaFX;

    move-result-object v0

    const-class v1, LaGA;

    invoke-virtual {v0, v1}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v0

    check-cast v0, LaGA;

    .line 94
    iget-object v1, p0, LHF;->a:LaGA;

    if-ne v0, v1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v1, p0, LHF;->a:LaGA;

    if-eqz v1, :cond_1

    .line 99
    iget-object v1, p0, LHF;->a:LaGA;

    invoke-interface {v1}, LaGA;->a()V

    .line 101
    :cond_1
    iput-object v0, p0, LHF;->a:LaGA;

    .line 103
    invoke-virtual {p1}, LQX;->a()LIf;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIf;

    iput-object v0, p0, LHF;->a:LIf;

    .line 104
    iget-object v0, p0, LHF;->a:LDY;

    invoke-virtual {v0}, LDY;->a()V

    .line 105
    iget-object v0, p0, LHF;->a:LHK;

    if-eqz v0, :cond_2

    .line 106
    iget-object v0, p0, LHF;->a:LHK;

    invoke-interface {v0}, LHK;->a()V

    .line 108
    :cond_2
    iget-object v0, p0, LHF;->a:LHL;

    invoke-virtual {v0, p1}, LHL;->a(LQX;)LHK;

    move-result-object v0

    iput-object v0, p0, LHF;->a:LHK;

    goto :goto_0
.end method

.method public a(LaFX;)V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, LHF;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()V

    .line 114
    const-class v0, LaGA;

    invoke-virtual {p1, v0}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v0

    check-cast v0, LaGA;

    .line 115
    iget-object v1, p0, LHF;->a:LaGA;

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 116
    iput-object v0, p0, LHF;->a:LaGA;

    .line 117
    iget-object v0, p0, LHF;->a:LDY;

    invoke-virtual {v0}, LDY;->a()V

    .line 118
    return-void

    .line 115
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, LHF;->a:LHK;

    invoke-interface {v0, p1}, LHK;->a(Landroid/view/View;)V

    .line 187
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, LHF;->a:LHK;

    invoke-interface {v0}, LHK;->a()V

    .line 128
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, LHF;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()I

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, LHF;->a:LDY;

    invoke-virtual {v0}, LDY;->a()V

    .line 153
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method
