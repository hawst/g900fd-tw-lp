.class public Lbgf;
.super Lbgg;
.source "ActionBarCompatActivity.java"


# instance fields
.field private final a:Lbhm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lbgg;-><init>()V

    .line 18
    invoke-static {p0}, Lbhm;->a(Landroid/app/Activity;)Lbhm;

    move-result-object v0

    iput-object v0, p0, Lbgf;->a:Lbhm;

    return-void
.end method


# virtual methods
.method public a()Lbhm;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lbgf;->a:Lbhm;

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lbgf;->a:Lbhm;

    invoke-super {p0}, Lbgg;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbhm;->a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbgf;->a:Lbhm;

    invoke-virtual {v0, p1}, Lbhm;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lbgg;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, Lbgg;->onCreate(Landroid/os/Bundle;)V

    .line 37
    iget-object v0, p0, Lbgf;->a:Lbhm;

    invoke-virtual {v0, p1}, Lbhm;->a(Landroid/os/Bundle;)V

    .line 38
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lbgf;->a:Lbhm;

    invoke-virtual {v0, p1, p2, p3}, Lbhm;->a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 65
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    invoke-super {p0, p1}, Lbgg;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 57
    iget-object v1, p0, Lbgf;->a:Lbhm;

    invoke-virtual {v1, p1}, Lbhm;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 58
    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Lbgg;->onPostCreate(Landroid/os/Bundle;)V

    .line 44
    iget-object v0, p0, Lbgf;->a:Lbhm;

    invoke-virtual {v0, p1}, Lbhm;->b(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lbgf;->a:Lbhm;

    invoke-virtual {v0, p1, p2}, Lbhm;->a(Ljava/lang/CharSequence;I)V

    .line 77
    invoke-super {p0, p1, p2}, Lbgg;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 78
    return-void
.end method
