.class public LVX;
.super Ljava/lang/Object;
.source "PluginInstaller.java"


# instance fields
.field private a:LVV;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LVU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ClassLoader;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    :try_start_0
    invoke-static {p1, p3}, LVX;->a(Ljava/lang/ClassLoader;Ljava/lang/String;)LVV;

    move-result-object v0

    iput-object v0, p0, LVX;->a:LVV;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    iget-object v0, p0, LVX;->a:LVV;

    invoke-interface {v0}, LVV;->createPlugins()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LVX;->a:Ljava/util/List;

    .line 59
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    new-instance v0, LVS;

    invoke-direct {v0, p2, p1}, LVS;-><init>(Landroid/content/Context;Ljava/lang/ClassLoader;)V

    iput-object v0, p0, LVX;->a:LVV;

    goto :goto_0
.end method

.method private static final a(Ljava/lang/ClassLoader;Ljava/lang/String;)LVV;
    .locals 4

    .prologue
    .line 130
    :try_start_0
    const-string v0, "com.google.android.apps.docs.plugins.StaticPluginFactory"

    .line 131
    invoke-virtual {p0, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 133
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 135
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVV;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to instantiate plugin installer"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 138
    :catch_1
    move-exception v0

    .line 139
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to instantiate plugin installer"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 140
    :catch_2
    move-exception v0

    .line 141
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to instantiate plugin installer"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 142
    :catch_3
    move-exception v0

    .line 143
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to instantiate plugin installer"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LbuC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 77
    new-instance v0, LVY;

    invoke-direct {v0, p0}, LVY;-><init>(LVX;)V

    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 84
    iget-object v0, p0, LVX;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVU;

    .line 85
    invoke-interface {v0}, LVU;->a()Ljava/util/List;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    goto :goto_0

    .line 91
    :cond_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, LVX;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVU;

    .line 68
    invoke-interface {v0, p1}, LVU;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method
