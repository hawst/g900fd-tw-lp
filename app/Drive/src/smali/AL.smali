.class abstract LAL;
.super Ljava/lang/Object;
.source "DocListAdapterState.java"

# interfaces
.implements LAO;


# instance fields
.field final synthetic a:LAJ;


# direct methods
.method private constructor <init>(LAJ;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, LAL;->a:LAJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LAJ;LAK;)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0, p1}, LAL;-><init>(LAJ;)V

    return-void
.end method

.method private a(LBe;)V
    .locals 4

    .prologue
    .line 240
    iget-object v0, p1, LBe;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 241
    iget-object v1, p0, LAL;->a:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LCv;

    move-result-object v1

    invoke-interface {v1}, LCv;->a()Ljava/lang/String;

    move-result-object v1

    .line 242
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setTextAndTypefaceNoLayout(Ljava/lang/String;Landroid/graphics/Typeface;)V

    .line 248
    iget-object v2, p0, LAL;->a:LAJ;

    invoke-static {v2}, LAJ;->a(LAJ;)LCv;

    move-result-object v2

    invoke-interface {v2}, LCv;->a()LaGv;

    move-result-object v2

    .line 249
    iget-object v3, p0, LAL;->a:LAJ;

    invoke-static {v3}, LAJ;->a(LAJ;)LCv;

    move-result-object v3

    invoke-interface {v3}, LCv;->e()Ljava/lang/String;

    move-result-object v3

    .line 250
    invoke-static {v2, v3}, LaGt;->b(LaGv;Ljava/lang/String;)I

    move-result v2

    .line 251
    iget-object v3, p1, LBe;->a:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 252
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 254
    invoke-virtual {p0}, LAL;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setEnabled(Z)V

    .line 255
    return-void
.end method

.method private a(LBe;LaGA;Z)V
    .locals 2

    .prologue
    .line 203
    invoke-interface {p2}, LaGA;->b()I

    move-result v0

    invoke-virtual {p1, p2, v0}, LBe;->a(LaGA;I)V

    .line 204
    invoke-direct {p0, p1}, LAL;->a(LBe;)V

    .line 205
    invoke-direct {p0, p1, p3}, LAL;->a(LBe;Z)V

    .line 206
    iget-object v0, p1, LBe;->e:Landroid/view/View;

    invoke-virtual {p0}, LAL;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 207
    return-void
.end method

.method private a(LBe;Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 259
    iget-object v0, p1, LBe;->c:Landroid/view/View;

    .line 260
    if-eqz v0, :cond_1

    .line 263
    invoke-virtual {p0}, LAL;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 264
    invoke-virtual {v0, p2}, Landroid/view/View;->setActivated(Z)V

    .line 267
    :cond_0
    invoke-virtual {p0}, LAL;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 269
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LBe;Landroid/view/View;LaGA;Z)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0, p1, p3, p4}, LAL;->a(LBe;LaGA;Z)V

    .line 198
    invoke-virtual {p0, p2, p3, p4}, LAL;->a(Landroid/view/View;LaGA;Z)V

    .line 199
    return-void
.end method

.method protected abstract a(Landroid/view/View;LaGA;Z)V
.end method

.method protected a(Lcom/google/android/apps/docs/view/FixedSizeImageView;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/view/FixedSizeImageView;->setVisibility(I)V

    .line 218
    iget-object v0, p0, LAL;->a:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LCv;

    move-result-object v0

    invoke-interface {v0}, LCv;->a()LaGv;

    move-result-object v0

    .line 219
    iget-object v1, p0, LAL;->a:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LCv;

    move-result-object v1

    invoke-interface {v1}, LCv;->e()Ljava/lang/String;

    move-result-object v1

    .line 220
    iget-object v2, p0, LAL;->a:LAJ;

    invoke-static {v2}, LAJ;->a(LAJ;)LCv;

    move-result-object v2

    invoke-interface {v2}, LCv;->c()Z

    move-result v2

    .line 223
    invoke-static {v0, v1, v2}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v0

    .line 222
    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/view/FixedSizeImageView;->setImageResource(I)V

    .line 224
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {p0}, LAL;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/view/FixedSizeImageView;->setAlpha(F)V

    .line 227
    :cond_0
    return-void

    .line 225
    :cond_1
    const v0, 0x3f19999a    # 0.6f

    goto :goto_0
.end method

.method protected a()Z
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, LAL;->a:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LCv;

    move-result-object v0

    invoke-interface {v0}, LCv;->a()LaGv;

    move-result-object v0

    .line 231
    iget-object v1, p0, LAL;->a:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LCv;

    move-result-object v1

    invoke-interface {v1}, LCv;->c()Ljava/lang/String;

    move-result-object v1

    .line 232
    iget-object v2, p0, LAL;->a:LAJ;

    invoke-static {v2}, LAJ;->a(LAJ;)LCr;

    move-result-object v2

    invoke-interface {v2, v0, v1}, LCr;->a(LaGv;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected b()Z
    .locals 2

    .prologue
    .line 236
    sget-object v0, LapG;->c:LapG;

    iget-object v1, p0, LAL;->a:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LapF;

    move-result-object v1

    invoke-interface {v1}, LapF;->a()LapG;

    move-result-object v1

    invoke-virtual {v0, v1}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
