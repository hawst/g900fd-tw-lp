.class public LyS;
.super Ljava/lang/Object;
.source "DetailCardAdapterFactory.java"

# interfaces
.implements Lza;


# instance fields
.field private final a:Lacj;

.field private final a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

.field private final a:LtK;

.field private final a:LvU;

.field private final a:LyA;

.field private final a:LyB;

.field private final a:LzD;

.field private final a:LzJ;

.field private final a:Lzc;

.field private final a:Lzd;

.field private final a:Lzf;

.field private final a:Lzn;

.field private final a:Lzq;

.field private final a:Lzz;


# direct methods
.method public constructor <init>(LtK;LvU;LzJ;Lacj;Lzz;Lzq;LyA;Lzn;Lzc;Lzd;Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;LzD;LyB;Lzg;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p2, p0, LyS;->a:LvU;

    .line 61
    iput-object p1, p0, LyS;->a:LtK;

    .line 62
    iput-object p3, p0, LyS;->a:LzJ;

    .line 63
    iput-object p4, p0, LyS;->a:Lacj;

    .line 64
    iput-object p5, p0, LyS;->a:Lzz;

    .line 65
    iput-object p6, p0, LyS;->a:Lzq;

    .line 66
    iput-object p7, p0, LyS;->a:LyA;

    .line 67
    iput-object p8, p0, LyS;->a:Lzn;

    .line 68
    iput-object p9, p0, LyS;->a:Lzc;

    .line 69
    iput-object p10, p0, LyS;->a:Lzd;

    .line 70
    iput-object p11, p0, LyS;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    .line 71
    iput-object p12, p0, LyS;->a:LzD;

    .line 72
    iput-object p13, p0, LyS;->a:LyB;

    .line 73
    invoke-virtual {p14}, Lzg;->a()Lzf;

    move-result-object v0

    iput-object v0, p0, LyS;->a:Lzf;

    .line 75
    new-instance v0, LyT;

    invoke-direct {v0, p0}, LyT;-><init>(LyS;)V

    invoke-interface {p2, v0}, LvU;->a(LvV;)V

    .line 87
    invoke-virtual {p4, p12}, Lacj;->a(Laco;)V

    .line 88
    invoke-virtual {p4, p11}, Lacj;->a(Laco;)V

    .line 89
    return-void
.end method

.method static synthetic a(LyS;Z)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, LyS;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, LyS;->a:LvU;

    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    .line 94
    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v1, p0, LyS;->a:Lacj;

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 100
    iget-object v1, p0, LyS;->a:Lzz;

    invoke-virtual {v1, v0}, Lzz;->a(LaGu;)V

    .line 101
    iget-object v1, p0, LyS;->a:LyA;

    invoke-virtual {v1, v0}, LyA;->a(LaGu;)V

    .line 102
    iget-object v1, p0, LyS;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(LaGu;)V

    .line 103
    iget-object v1, p0, LyS;->a:LzD;

    invoke-virtual {v1, v0}, LzD;->a(LaGu;)V

    .line 104
    iget-object v1, p0, LyS;->a:Lzn;

    invoke-virtual {v1, v0}, Lzn;->a(LaGu;)V

    .line 106
    iget-object v1, p0, LyS;->a:Lzd;

    invoke-virtual {v1, v0, p1}, Lzd;->a(LaGu;Z)V

    .line 108
    iget-object v1, p0, LyS;->a:LyB;

    if-eqz v1, :cond_2

    .line 109
    iget-object v1, p0, LyS;->a:LyB;

    invoke-virtual {v1, v0}, LyB;->a(LaGu;)V

    .line 112
    :cond_2
    iget-object v1, p0, LyS;->a:Lzf;

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, LyS;->a:Lzf;

    invoke-virtual {v1, v0}, Lzf;->a(LaGu;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Laqs;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 119
    new-instance v0, LbmH;

    invoke-direct {v0}, LbmH;-><init>()V

    .line 120
    const/16 v1, 0x9

    new-array v1, v1, [Landroid/widget/ListAdapter;

    iget-object v2, p0, LyS;->a:Lzq;

    aput-object v2, v1, v6

    iget-object v2, p0, LyS;->a:LyA;

    aput-object v2, v1, v5

    iget-object v2, p0, LyS;->a:LzJ;

    sget v3, Lxe;->detail_card_divider_row:I

    .line 123
    invoke-virtual {v2, v3}, LzJ;->a(I)LzH;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x3

    iget-object v3, p0, LyS;->a:Lzn;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, LyS;->a:Lzc;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, LyS;->a:LzJ;

    sget v4, Lxe;->detail_card_divider_row:I

    .line 126
    invoke-virtual {v3, v4}, LzJ;->a(I)LzH;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, LyS;->a:LzJ;

    sget v4, Lxe;->detail_card_sharing_header:I

    .line 127
    invoke-virtual {v3, v4}, LzJ;->a(I)LzH;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, LyS;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, LyS;->a:LzD;

    aput-object v3, v1, v2

    .line 120
    invoke-virtual {v0, v1}, LbmH;->a([Ljava/lang/Object;)LbmH;

    .line 131
    iget-object v1, p0, LyS;->a:LtK;

    sget-object v2, Lry;->as:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    new-array v1, v7, [Landroid/widget/ListAdapter;

    iget-object v2, p0, LyS;->a:LzJ;

    sget v3, Lxe;->detail_card_divider_row:I

    .line 133
    invoke-virtual {v2, v3}, LzJ;->a(I)LzH;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, LyS;->a:LyB;

    aput-object v2, v1, v5

    .line 132
    invoke-virtual {v0, v1}, LbmH;->a([Ljava/lang/Object;)LbmH;

    .line 138
    :cond_0
    iget-object v1, p0, LyS;->a:Lzf;

    if-eqz v1, :cond_1

    .line 139
    iget-object v1, p0, LyS;->a:Lzf;

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 142
    :cond_1
    invoke-direct {p0, v5}, LyS;->a(Z)V

    .line 143
    new-instance v1, Laqs;

    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    invoke-direct {v1, v0}, Laqs;-><init>(LbmF;)V

    return-object v1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 148
    invoke-static {}, LamV;->a()V

    .line 149
    iget-object v0, p0, LyS;->a:Lzq;

    invoke-virtual {v0}, Lzq;->a()V

    .line 150
    return-void
.end method
