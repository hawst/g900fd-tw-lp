.class interface abstract Lbox;
.super Ljava/lang/Object;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()I
.end method

.method public abstract a()J
.end method

.method public abstract a()LboK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LboK",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract a()Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation
.end method

.method public abstract a(J)V
.end method

.method public abstract a(LboK;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LboK",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract a(Lbox;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract b()Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract b(Lbox;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract c()Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract c(Lbox;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract d()Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract d(Lbox;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract e()Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end method
