.class public Lamd;
.super Ljava/lang/Object;
.source "ModelLoaderAsync.java"


# instance fields
.field private final a:LaGM;

.field private final a:LbsW;


# direct methods
.method public constructor <init>(LaGM;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    iput-object v0, p0, Lamd;->a:LbsW;

    .line 29
    iput-object p1, p0, Lamd;->a:LaGM;

    .line 30
    return-void
.end method

.method static synthetic a(Lamd;)LaGM;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lamd;->a:LaGM;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "LbsU",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lamd;->a:LbsW;

    new-instance v1, Lamf;

    invoke-direct {v1, p0, p1}, Lamf;-><init>(Lamd;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public a(Lwm;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lwm;",
            ")",
            "LbsU",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-interface {p1}, Lwm;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 36
    if-eqz v1, :cond_0

    .line 37
    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    .line 38
    :cond_0
    sget-object v1, LCe;->q:LCe;

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LCl;

    move-result-object v2

    invoke-virtual {v1, v2}, LCe;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LaFO;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lamd;->a:LbsW;

    new-instance v2, Lame;

    invoke-direct {v2, p0, v0}, Lame;-><init>(Lamd;LaFO;)V

    invoke-interface {v1, v2}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
