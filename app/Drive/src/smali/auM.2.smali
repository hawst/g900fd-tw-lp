.class public final LauM;
.super Ljava/lang/Object;
.source "FileInfo.java"


# instance fields
.field public final a:Landroid/content/Intent;

.field public final a:Landroid/net/Uri;

.field public final a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

.field private a:Lcom/google/android/apps/viewer/client/Dimensions;

.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Landroid/net/Uri;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, LauM;->a:Ljava/lang/String;

    .line 85
    iput-object p2, p0, LauM;->b:Ljava/lang/String;

    .line 86
    iput-object p3, p0, LauM;->c:Ljava/lang/String;

    .line 87
    iput-object p4, p0, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    .line 88
    iput-object p5, p0, LauM;->b:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    .line 89
    iput-object p6, p0, LauM;->a:Landroid/net/Uri;

    .line 90
    iput-object p7, p0, LauM;->a:Landroid/content/Intent;

    .line 91
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LauM;
    .locals 8

    .prologue
    .line 131
    const-class v0, LauM;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 132
    new-instance v0, LauM;

    const-string v1, "id"

    .line 133
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "n"

    .line 134
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mt"

    .line 135
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "url"

    .line 136
    invoke-virtual {p0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    const-string v5, "lru"

    .line 137
    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    const-string v6, "stream"

    .line 138
    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    const-string v7, "editIntent"

    .line 139
    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/content/Intent;

    invoke-direct/range {v0 .. v7}, LauM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Landroid/net/Uri;Landroid/content/Intent;)V

    .line 140
    const-string v1, "dim"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/viewer/client/Dimensions;

    iput-object v1, v0, LauM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    .line 141
    return-object v0
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 112
    const-string v1, "id"

    iget-object v2, p0, LauM;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v1, "n"

    iget-object v2, p0, LauM;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v1, "mt"

    iget-object v2, p0, LauM;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v1, "url"

    iget-object v2, p0, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 116
    const-string v1, "lru"

    iget-object v2, p0, LauM;->b:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 117
    const-string v1, "stream"

    iget-object v2, p0, LauM;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 118
    const-string v1, "editIntent"

    iget-object v2, p0, LauM;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 119
    iget-object v1, p0, LauM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    if-eqz v1, :cond_0

    .line 120
    const-string v1, "dim"

    iget-object v2, p0, LauM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 122
    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 146
    const-string v0, "File %s (%s) @%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LauM;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LauM;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v3, v3, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
