.class public final LaHG;
.super LaHp;
.source "TrashOp.java"


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V
    .locals 1

    .prologue
    .line 30
    const-string v0, "trash"

    invoke-direct {p0, p1, p2, v0}, LaHp;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHG;
    .locals 1

    .prologue
    .line 36
    new-instance v0, LaHG;

    invoke-direct {v0, p0, p1}, LaHG;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V

    return-object v0
.end method


# virtual methods
.method public a(LaGe;)LaHo;
    .locals 3

    .prologue
    .line 47
    new-instance v0, LaHH;

    iget-object v1, p0, LaHG;->a:LaGg;

    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LaHH;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V

    .line 48
    invoke-virtual {p1}, LaGe;->a()V

    .line 49
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 54
    invoke-super {p0}, LaHp;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 55
    const-string v1, "operationName"

    const-string v2, "trash"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 57
    return-object v0
.end method

.method public a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p2}, LaHD;->a()LaHE;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p3, p1, v1}, LaHE;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHy;Z)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 62
    instance-of v0, p1, LaHG;

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 66
    :goto_0
    return v0

    .line 65
    :cond_0
    check-cast p1, LaHG;

    .line 66
    invoke-virtual {p0, p1}, LaHG;->a(LaHp;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, LaHG;->b()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    const-string v0, "TrashOp[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LaHG;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
