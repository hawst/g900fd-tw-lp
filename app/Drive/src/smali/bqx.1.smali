.class final Lbqx;
.super Lbmm;
.source "WellBehavedMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lbmm",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lbmm;-><init>()V

    .line 42
    iput-object p1, p0, Lbqx;->a:Ljava/util/Map;

    .line 43
    return-void
.end method

.method static a(Ljava/util/Map;)Lbqx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV;>;)",
            "Lbqx",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lbqx;

    invoke-direct {v0, p0}, Lbqx;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lbqx;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lbqx;->a:Ljava/util/Map;

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lbqx;->a:Ljava/util/Set;

    .line 63
    if-eqz v0, :cond_0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbqz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbqz;-><init>(Lbqx;Lbqy;)V

    iput-object v0, p0, Lbqx;->a:Ljava/util/Set;

    goto :goto_0
.end method
