.class public LIU;
.super Ljava/lang/Object;
.source "BaseHelpCard.java"

# interfaces
.implements LJw;


# instance fields
.field private final a:I

.field private final a:LJb;

.field private a:LJd;

.field private final a:LaFO;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Lcom/google/android/apps/docs/view/DocListView;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/content/Context;

.field private a:Landroid/view/View;

.field private a:Ljava/lang/Runnable;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LJx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:Z

.field private final b:I

.field private b:Landroid/view/View;

.field private b:Ljava/lang/Runnable;

.field private final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method private constructor <init>(Landroid/content/Context;LaFO;LqK;Laja;ILjava/lang/String;IIZLJb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LaFO;",
            "LqK;",
            "Laja",
            "<",
            "Lcom/google/android/apps/docs/view/DocListView;",
            ">;I",
            "Ljava/lang/String;",
            "IIZ",
            "LJb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    sget-object v0, LJd;->c:LJd;

    iput-object v0, p0, LIU;->a:LJd;

    .line 146
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 147
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LIU;->a:Ljava/util/Set;

    .line 170
    iput-object p3, p0, LIU;->a:LqK;

    .line 171
    iput-object p1, p0, LIU;->a:Landroid/content/Context;

    .line 172
    iput-object p2, p0, LIU;->a:LaFO;

    .line 173
    iput-object p4, p0, LIU;->a:Laja;

    .line 174
    iput p5, p0, LIU;->a:I

    .line 175
    iput-object p6, p0, LIU;->a:Ljava/lang/String;

    .line 176
    iput p7, p0, LIU;->b:I

    .line 177
    iput p8, p0, LIU;->c:I

    .line 178
    iput-boolean p9, p0, LIU;->a:Z

    .line 179
    iput-object p10, p0, LIU;->a:LJb;

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LIU;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "__Counter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LIU;->b:Ljava/lang/String;

    .line 181
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LaFO;LqK;Laja;ILjava/lang/String;IIZLJb;LIV;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct/range {p0 .. p10}, LIU;-><init>(Landroid/content/Context;LaFO;LqK;Laja;ILjava/lang/String;IIZLJb;)V

    return-void
.end method

.method static synthetic a(LIU;)LJd;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LIU;->a:LJd;

    return-object v0
.end method

.method static synthetic a(LIU;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LIU;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 359
    sget-object v0, LJd;->c:LJd;

    iput-object v0, p0, LIU;->a:LJd;

    .line 360
    return-void
.end method

.method private a(FF)V
    .locals 2

    .prologue
    .line 352
    sget-object v0, LJd;->c:LJd;

    iget-object v1, p0, LIU;->a:LJd;

    invoke-virtual {v0, v1}, LJd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    invoke-direct {p0, p1, p2}, LIU;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LJd;->a:LJd;

    :goto_0
    iput-object v0, p0, LIU;->a:LJd;

    .line 356
    :cond_0
    return-void

    .line 354
    :cond_1
    sget-object v0, LJd;->b:LJd;

    goto :goto_0
.end method

.method static synthetic a(LIU;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, LIU;->a()V

    return-void
.end method

.method static synthetic a(LIU;FF)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, LIU;->a(FF)V

    return-void
.end method

.method static synthetic a(LIU;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, LIU;->c(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(LIU;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, LIU;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(LIU;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, LIU;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 432
    invoke-direct {p0, p1}, LIU;->b(Landroid/content/Context;)I

    move-result v0

    .line 433
    iget-object v1, p0, LIU;->a:LqK;

    const-string v2, "helpCard"

    invoke-virtual {p0}, LIU;->a()Ljava/lang/String;

    move-result-object v3

    int-to-long v4, v0

    .line 434
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 433
    invoke-virtual {v1, v2, p2, v3, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 435
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 408
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 409
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 412
    neg-int v3, v2

    div-int/lit8 v3, v3, 0x2

    if-ge v0, v3, :cond_0

    .line 413
    sub-int v0, v2, v0

    neg-int v0, v0

    move v2, v0

    move v0, v1

    .line 420
    :goto_0
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v2

    invoke-direct {v3, v4, v2, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 421
    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 422
    invoke-virtual {v3, v1}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    .line 423
    if-eqz v0, :cond_2

    .line 424
    new-instance v0, LJa;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LJa;-><init>(LIU;Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 428
    :goto_1
    invoke-virtual {p1, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 429
    return-void

    .line 414
    :cond_0
    div-int/lit8 v3, v2, 0x2

    if-ge v0, v3, :cond_1

    .line 415
    neg-int v2, v0

    .line 416
    const/4 v0, 0x0

    goto :goto_0

    .line 418
    :cond_1
    sub-int v0, v2, v0

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 426
    :cond_2
    new-instance v0, LJe;

    invoke-direct {v0, p0, p1}, LJe;-><init>(LIU;Landroid/view/View;)V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_1
.end method

.method private a(Landroid/widget/Button;Landroid/content/Context;IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 193
    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 194
    if-eqz p4, :cond_0

    sget v0, LwZ;->helpcard_primary:I

    .line 195
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 196
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 198
    invoke-virtual {p1, v2, v3, v3, v3}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 200
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->help_card_button_compound_drawable_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 201
    float-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setCompoundDrawablePadding(I)V

    .line 202
    return-void

    .line 194
    :cond_0
    sget v0, LwZ;->helpcard_secondary:I

    goto :goto_0
.end method

.method private a(FF)Z
    .locals 2

    .prologue
    .line 363
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 364
    :goto_0
    return v0

    .line 363
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LIU;FF)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, LIU;->a(FF)Z

    move-result v0

    return v0
.end method

.method private b(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 456
    const-string v0, "HelpCard"

    .line 457
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 458
    iget-object v1, p0, LIU;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static synthetic b(LIU;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LIU;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method private b(Landroid/content/Context;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 471
    iget-object v0, p0, LIU;->a:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget-object v3, p0, LIU;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    aput v3, v1, v2

    aput v4, v1, v4

    invoke-static {v0, v1}, Lxm;->b(Landroid/view/View;[I)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    const/16 v1, 0x12c

    .line 472
    invoke-virtual {v0, v1}, Lxr;->a(I)Lxr;

    move-result-object v0

    .line 473
    invoke-virtual {v0, p1}, Lxr;->c(Landroid/content/Context;)Lxr;

    move-result-object v0

    new-instance v1, LIZ;

    invoke-direct {v1, p0, p1}, LIZ;-><init>(LIU;Landroid/content/Context;)V

    .line 474
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 479
    invoke-virtual {v0}, Lxr;->a()Landroid/animation/Animator;

    .line 480
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 483
    const-string v0, "HelpCard"

    const/4 v1, 0x0

    .line 484
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 485
    invoke-virtual {p0}, LIU;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 486
    iget-object v0, p0, LIU;->a:Ljava/util/Set;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 487
    iget-object v1, p0, LIU;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 488
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJx;

    .line 489
    invoke-interface {v0}, LJx;->a()V

    goto :goto_0

    .line 491
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 448
    const-string v0, "HelpCard"

    .line 449
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 450
    iget-object v1, p0, LIU;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 451
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, LIU;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 452
    return v1
.end method

.method public a()LaFO;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, LIU;->a:LaFO;

    return-object v0
.end method

.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, LIU;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 206
    iget-object v0, p0, LIU;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LIU;->b:Landroid/view/View;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, LIU;->a:Landroid/view/View;

    .line 347
    :goto_0
    return-object v0

    .line 209
    :cond_0
    const-string v0, "layout_inflater"

    .line 210
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 211
    iget v1, p0, LIU;->a:I

    invoke-virtual {v0, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 212
    new-instance v1, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;-><init>(Landroid/content/Context;)V

    .line 214
    iget-boolean v0, p0, LIU;->a:Z

    if-eqz v0, :cond_3

    sget v0, Lxc;->secondary_button:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 216
    new-instance v3, LIV;

    invoke-direct {v3, p0, p1}, LIV;-><init>(LIU;Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget v3, p0, LIU;->b:I

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 227
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setMinWidth(I)V

    .line 228
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 229
    iget-boolean v3, p0, LIU;->a:Z

    if-nez v3, :cond_1

    iget v3, p0, LIU;->c:I

    if-eqz v3, :cond_1

    .line 230
    iget v3, p0, LIU;->c:I

    const/4 v4, 0x1

    invoke-direct {p0, v0, p1, v3, v4}, LIU;->a(Landroid/widget/Button;Landroid/content/Context;IZ)V

    .line 233
    :cond_1
    iget-boolean v0, p0, LIU;->a:Z

    if-eqz v0, :cond_4

    sget v0, Lxc;->primary_button:I

    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 236
    iget-object v3, p0, LIU;->a:LJb;

    sget-object v4, LJb;->d:LJb;

    invoke-virtual {v3, v4}, LJb;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 237
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 265
    :cond_2
    :goto_3
    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->addView(Landroid/view/View;)V

    .line 267
    new-instance v0, LIX;

    invoke-direct {v0, p0, v1, v2, p1}, LIX;-><init>(LIU;Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;Landroid/view/View;Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->setGestureListener(Landroid/view/GestureDetector$OnGestureListener;)V

    .line 335
    new-instance v0, LIY;

    invoke-direct {v0, p0, v2}, LIY;-><init>(LIU;Landroid/view/View;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->setOnUpListener(LJv;)V

    .line 343
    iget-object v0, p0, LIU;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->setDocListView(Lcom/google/android/apps/docs/view/DocListView;)V

    .line 344
    iput-object v1, p0, LIU;->a:Landroid/view/View;

    .line 345
    iput-object p2, p0, LIU;->b:Landroid/view/View;

    move-object v0, v1

    .line 347
    goto :goto_0

    .line 214
    :cond_3
    sget v0, Lxc;->primary_button:I

    goto :goto_1

    .line 233
    :cond_4
    sget v0, Lxc;->secondary_button:I

    goto :goto_2

    .line 239
    :cond_5
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setMinWidth(I)V

    .line 240
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 241
    iget-object v3, p0, LIU;->a:LJb;

    invoke-virtual {v3}, LJb;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 242
    new-instance v3, LIW;

    invoke-direct {v3, p0, p1}, LIW;-><init>(LIU;Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-object v3, p0, LIU;->a:LJb;

    invoke-virtual {v3}, LJb;->b()I

    move-result v3

    .line 254
    if-lez v3, :cond_2

    .line 259
    iget-object v4, p0, LIU;->a:LJb;

    sget-object v5, LJb;->a:LJb;

    if-eq v4, v5, :cond_6

    iget-boolean v4, p0, LIU;->a:Z

    if-eqz v4, :cond_2

    .line 260
    :cond_6
    iget-boolean v4, p0, LIU;->a:Z

    invoke-direct {p0, v0, p1, v3, v4}, LIU;->a(Landroid/widget/Button;Landroid/content/Context;IZ)V

    goto :goto_3
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, LIU;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(LJx;)V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, LIU;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 520
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 462
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LIU;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 463
    invoke-direct {p0, p1}, LIU;->b(Landroid/content/Context;)V

    .line 467
    :goto_0
    return-void

    .line 465
    :cond_0
    invoke-direct {p0, p1}, LIU;->c(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, LIU;->a:Ljava/lang/Runnable;

    .line 505
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 439
    const-string v0, "HelpCard"

    .line 440
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 441
    invoke-virtual {p0}, LIU;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 443
    return v0
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, LIU;->b:Ljava/lang/Runnable;

    .line 509
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, LIU;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
