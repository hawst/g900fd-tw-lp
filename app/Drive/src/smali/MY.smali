.class public final LMY;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LMV;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LNc;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LNe;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LMU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LMY;->a:LbrA;

    .line 42
    const-class v0, LMV;

    invoke-static {v0, v1}, LMY;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LMY;->a:Lbsk;

    .line 45
    const-class v0, LNc;

    invoke-static {v0, v1}, LMY;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LMY;->b:Lbsk;

    .line 48
    const-class v0, LNe;

    invoke-static {v0, v1}, LMY;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LMY;->c:Lbsk;

    .line 51
    const-class v0, LMU;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LMY;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LMY;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 81
    sparse-switch p1, :sswitch_data_0

    .line 191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :sswitch_0
    new-instance v0, LMV;

    iget-object v1, p0, LMY;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->h:Lbsk;

    .line 86
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LMY;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->h:Lbsk;

    .line 84
    invoke-static {v1, v2}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTh;

    iget-object v2, p0, LMY;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 92
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LMY;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 90
    invoke-static {v2, v3}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, LMY;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laek;

    iget-object v3, v3, Laek;->d:Lbsk;

    .line 98
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LMY;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Laek;

    iget-object v4, v4, Laek;->d:Lbsk;

    .line 96
    invoke-static {v3, v4}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LbwT;

    iget-object v4, p0, LMY;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 104
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LMY;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->l:Lbsk;

    .line 102
    invoke-static {v4, v5}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LaGM;

    iget-object v5, p0, LMY;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Laek;

    iget-object v5, v5, Laek;->h:Lbsk;

    .line 110
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LMY;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Laek;

    iget-object v6, v6, Laek;->h:Lbsk;

    .line 108
    invoke-static {v5, v6}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Laer;

    iget-object v6, p0, LMY;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LqD;

    iget-object v6, v6, LqD;->c:Lbsk;

    .line 116
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LMY;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LqD;

    iget-object v7, v7, LqD;->c:Lbsk;

    .line 114
    invoke-static {v6, v7}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LqK;

    iget-object v7, p0, LMY;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LahE;

    iget-object v7, v7, LahE;->k:Lbsk;

    .line 122
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LMY;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LahE;

    iget-object v8, v8, LahE;->k:Lbsk;

    .line 120
    invoke-static {v7, v8}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LahK;

    invoke-direct/range {v0 .. v7}, LMV;-><init>(LTh;LQr;LbwT;LaGM;Laer;LqK;LahK;)V

    .line 189
    :goto_0
    return-object v0

    .line 129
    :sswitch_1
    new-instance v4, LNc;

    iget-object v0, p0, LMY;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 132
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMY;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 130
    invoke-static {v0, v1}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LMY;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->K:Lbsk;

    .line 138
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LMY;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->K:Lbsk;

    .line 136
    invoke-static {v1, v2}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lalo;

    iget-object v2, p0, LMY;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->q:Lbsk;

    .line 144
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LMY;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->q:Lbsk;

    .line 142
    invoke-static {v2, v3}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LalY;

    iget-object v3, p0, LMY;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->p:Lbsk;

    .line 150
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LMY;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LadM;

    iget-object v5, v5, LadM;->p:Lbsk;

    .line 148
    invoke-static {v3, v5}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laeb;

    invoke-direct {v4, v0, v1, v2, v3}, LNc;-><init>(Laja;Lalo;LalY;Laeb;)V

    move-object v0, v4

    .line 155
    goto :goto_0

    .line 157
    :sswitch_2
    new-instance v0, LNe;

    iget-object v1, p0, LMY;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->h:Lbsk;

    .line 160
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LMY;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->h:Lbsk;

    .line 158
    invoke-static {v1, v2}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTh;

    iget-object v2, p0, LMY;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 166
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LMY;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 164
    invoke-static {v2, v3}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, LMY;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laek;

    iget-object v3, v3, Laek;->d:Lbsk;

    .line 172
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LMY;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Laek;

    iget-object v4, v4, Laek;->d:Lbsk;

    .line 170
    invoke-static {v3, v4}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LbwT;

    iget-object v4, p0, LMY;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LqD;

    iget-object v4, v4, LqD;->c:Lbsk;

    .line 178
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LMY;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LqD;

    iget-object v5, v5, LqD;->c:Lbsk;

    .line 176
    invoke-static {v4, v5}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LqK;

    iget-object v5, p0, LMY;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->l:Lbsk;

    .line 184
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LMY;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaGH;

    iget-object v6, v6, LaGH;->l:Lbsk;

    .line 182
    invoke-static {v5, v6}, LMY;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaGM;

    invoke-direct/range {v0 .. v5}, LNe;-><init>(LTh;LQr;LbwT;LqK;LaGM;)V

    goto/16 :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x159 -> :sswitch_1
        0x162 -> :sswitch_2
        0x1ab -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 206
    packed-switch p2, :pswitch_data_0

    .line 217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :pswitch_0
    check-cast p1, LMX;

    .line 210
    iget-object v0, p0, LMY;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMY;

    iget-object v0, v0, LMY;->a:Lbsk;

    .line 213
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMV;

    .line 210
    invoke-virtual {p1, v0}, LMX;->provideDocsUploader(LMV;)LMU;

    move-result-object v0

    return-object v0

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x1ac
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 61
    const-class v0, LMV;

    iget-object v1, p0, LMY;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LMY;->a(Ljava/lang/Class;Lbsk;)V

    .line 62
    const-class v0, LNc;

    iget-object v1, p0, LMY;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LMY;->a(Ljava/lang/Class;Lbsk;)V

    .line 63
    const-class v0, LNe;

    iget-object v1, p0, LMY;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LMY;->a(Ljava/lang/Class;Lbsk;)V

    .line 64
    const-class v0, LMU;

    iget-object v1, p0, LMY;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LMY;->a(Ljava/lang/Class;Lbsk;)V

    .line 65
    iget-object v0, p0, LMY;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1ab

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    iget-object v0, p0, LMY;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x159

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 69
    iget-object v0, p0, LMY;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x162

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 71
    iget-object v0, p0, LMY;->d:Lbsk;

    const-class v1, LMX;

    const/16 v2, 0x1ac

    invoke-virtual {p0, v1, v2}, LMY;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 73
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 198
    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
