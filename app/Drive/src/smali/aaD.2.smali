.class public LaaD;
.super Ljava/lang/Object;
.source "DatabaseBlobFileUploader.java"

# interfaces
.implements LaaA;


# instance fields
.field a:Ladi;

.field a:LahB;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    return v0
.end method

.method private b(Ljava/util/List;)LbmF;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LMZ;",
            ">;)",
            "LbmF",
            "<",
            "LMZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 52
    new-instance v2, LbmH;

    invoke-direct {v2}, LbmH;-><init>()V

    .line 55
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMZ;

    .line 57
    :try_start_0
    iget-object v1, p0, LaaD;->a:Ladi;

    invoke-virtual {v0, v1}, LMZ;->a(Ladi;)V

    .line 58
    invoke-virtual {v2, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 59
    invoke-direct {p0}, LaaD;->a()Z
    :try_end_0
    .catch LQm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    :cond_1
    invoke-virtual {v2}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0

    .line 62
    :catch_0
    move-exception v1

    .line 63
    const-string v4, "DbBasedBlobFileUploader"

    const-string v5, "Failed to encrypt document for item: %s Cause: %s"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v0, v6, v7

    aput-object v1, v6, v8

    invoke-static {v4, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 64
    :catch_1
    move-exception v1

    .line 65
    const-string v4, "DbBasedBlobFileUploader"

    const-string v5, "Failed to copy document for item: %s Cause: %s"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v0, v6, v7

    aput-object v1, v6, v8

    invoke-static {v4, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private c(Ljava/util/List;)LbmF;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LMZ;",
            ">;)",
            "LbmF",
            "<",
            "LaGo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v2, LbmH;

    invoke-direct {v2}, LbmH;-><init>()V

    .line 78
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMZ;

    .line 80
    :try_start_0
    iget-object v1, p0, LaaD;->a:LahB;

    invoke-interface {v1, v0}, LahB;->a(LMZ;)LaGb;

    move-result-object v1

    .line 81
    invoke-virtual {v2, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 83
    invoke-direct {p0}, LaaD;->a()Z
    :try_end_0
    .catch LNh; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    :cond_1
    invoke-virtual {v2}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0

    .line 86
    :catch_0
    move-exception v1

    .line 87
    const-string v4, "DbBasedBlobFileUploader"

    const-string v5, "Fail to create document for item: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v4, v1, v5, v6}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;)LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LMZ;",
            ">;)",
            "LbmF",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1}, LaaD;->b(Ljava/util/List;)LbmF;

    move-result-object v0

    .line 35
    invoke-direct {p0, v0}, LaaD;->c(Ljava/util/List;)LbmF;

    move-result-object v0

    .line 37
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 38
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGo;

    .line 39
    invoke-interface {v0}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_0

    .line 42
    :cond_0
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method
