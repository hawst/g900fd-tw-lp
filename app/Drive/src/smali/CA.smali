.class public LCA;
.super LBe;
.source "IncomingCardViewHolder.java"


# instance fields
.field final a:Landroid/widget/Button;

.field final a:Landroid/widget/ImageView;

.field final a:Lcom/google/android/apps/docs/view/FixedSizeImageView;

.field final d:Lcom/google/android/apps/docs/view/FixedSizeTextView;

.field final e:Lcom/google/android/apps/docs/view/FixedSizeTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1}, LBe;-><init>(Landroid/view/View;)V

    .line 25
    sget v0, Lxc;->owner_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LCA;->a:Landroid/widget/ImageView;

    .line 26
    sget v0, Lxc;->owner_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LCA;->d:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 27
    sget v0, Lxc;->sort_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LCA;->e:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 28
    sget v0, Lxc;->open_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LCA;->a:Landroid/widget/Button;

    .line 29
    sget v0, Lxc;->doc_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeImageView;

    iput-object v0, p0, LCA;->a:Lcom/google/android/apps/docs/view/FixedSizeImageView;

    .line 30
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    .line 31
    iget-object v1, p0, LCA;->a:LbmF;

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    iget-object v1, p0, LCA;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LCA;->a:LbmF;

    .line 32
    return-void
.end method
