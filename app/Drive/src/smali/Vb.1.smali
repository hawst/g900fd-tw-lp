.class public abstract LVb;
.super Ljava/lang/Object;
.source "UrlPatternMatcher.java"


# instance fields
.field private final a:LVc;

.field protected final a:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(LVc;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LVb;-><init>(LVc;Ljava/util/regex/Pattern;)V

    .line 26
    return-void
.end method

.method public constructor <init>(LVc;Ljava/util/regex/Pattern;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVc;

    iput-object v0, p0, LVb;->a:LVc;

    .line 21
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    iput-object v0, p0, LVb;->a:Ljava/util/regex/Pattern;

    .line 22
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;)LVa;
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, LVb;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 50
    invoke-virtual {p0, v1, p1}, LVb;->a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, LVa;

    invoke-virtual {p0, v1, p1}, LVb;->a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LVb;->a:LVc;

    invoke-virtual {p0, p1}, LVb;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LVa;-><init>(Ljava/lang/String;LVc;Landroid/net/Uri;)V

    .line 54
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 38
    return-object p1
.end method

.method protected abstract a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Ljava/lang/String;
.end method

.method protected a(Landroid/net/Uri;Ljava/lang/String;)Ljava/util/regex/Matcher;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LVb;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 34
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    const-string v0, "pattern=[%s] type=[%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LVb;->a:Ljava/util/regex/Pattern;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LVb;->a:LVc;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
