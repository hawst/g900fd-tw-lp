.class public LFy;
.super Landroid/widget/ArrayAdapter;
.source "SelectNewDocTypeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LCN;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/LayoutInflater;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, LFy;->a:Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;

    iput-object p5, p0, LFy;->a:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    invoke-virtual {p0, p1}, LFy;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCN;

    .line 117
    iget-object v1, p0, LFy;->a:Landroid/view/LayoutInflater;

    sget v2, Lxe;->create_entry_dialog_row:I

    .line 118
    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 119
    invoke-interface {v0}, LCN;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 120
    invoke-interface {v0}, LCN;->b()I

    move-result v0

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 121
    return-object v1
.end method
