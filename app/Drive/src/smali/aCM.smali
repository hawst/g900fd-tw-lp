.class public final LaCM;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "LaCN;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LaCS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaCS",
            "<*TO;>;"
        }
    .end annotation
.end field

.field private final a:LaCT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaCT",
            "<*>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LaDe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(LaCS;LaCT;[LaDe;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "LaCR;",
            ">(",
            "LaCS",
            "<TC;TO;>;",
            "LaCT",
            "<TC;>;[",
            "LaDe;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaCM;->a:LaCS;

    iput-object p2, p0, LaCM;->a:LaCT;

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LaCM;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()LaCS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LaCS",
            "<*TO;>;"
        }
    .end annotation

    iget-object v0, p0, LaCM;->a:LaCS;

    return-object v0
.end method

.method public a()LaCT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LaCT",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, LaCM;->a:LaCT;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LaDe;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, LaCM;->a:Ljava/util/ArrayList;

    return-object v0
.end method
