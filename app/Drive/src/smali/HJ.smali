.class LHJ;
.super Ljava/lang/Object;
.source "GridCellAdapterVisibleEntryViewLookup.java"

# interfaces
.implements LHp;


# instance fields
.field private final a:LHp;

.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "LBC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LHp;Lbjv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LHp;",
            "Lbjv",
            "<",
            "LBC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHp;

    iput-object v0, p0, LHJ;->a:LHp;

    .line 23
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    iput-object v0, p0, LHJ;->a:Lbjv;

    .line 24
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, LHJ;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    .line 29
    invoke-virtual {v0, p1}, LBC;->a(I)I

    move-result v2

    .line 30
    iget-object v1, p0, LHJ;->a:LHp;

    invoke-interface {v1, v2}, LHp;->a(I)Landroid/view/View;

    move-result-object v1

    .line 31
    instance-of v3, v1, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    if-nez v3, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    .line 35
    :cond_0
    check-cast v1, Landroid/view/ViewGroup;

    .line 37
    invoke-virtual {v0, v2}, LBC;->a(I)LBF;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, LBF;->a()I

    move-result v0

    sub-int v0, p1, v0

    .line 39
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    const-class v0, LHJ;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "parent"

    iget-object v2, p0, LHJ;->a:LHp;

    .line 46
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
