.class public LaZG;
.super LaZJ;
.source "OwnersCoverPhotoManager.java"


# static fields
.field private static a:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;LaCV;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LaZJ;-><init>(Landroid/content/Context;LaCV;Z)V

    .line 30
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 72
    sget-object v0, LaZG;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LaZW;->account_switcher_blue:I

    .line 73
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, LaZG;->a:Landroid/graphics/Bitmap;

    .line 76
    :cond_0
    sget-object v0, LaZG;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 67
    new-instance v0, LaZH;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LaZH;-><init>(LaZG;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 68
    invoke-virtual {p0}, LaZG;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LaZG;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 67
    invoke-virtual {p0, v0, v1}, LaZG;->a(LaZK;Landroid/graphics/Bitmap;)V

    .line 69
    return-void
.end method
