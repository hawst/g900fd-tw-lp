.class public abstract LaFm;
.super Ljava/lang/Object;
.source "AbstractDatabaseInstance.java"

# interfaces
.implements LaFx;


# instance fields
.field private a:I

.field private final a:LQr;

.field protected final a:LaFu;

.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LaFq;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Thread;",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/atomic/AtomicLong;

.field protected final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lbjv",
            "<",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method public constructor <init>(LQr;LtK;LaFu;)V
    .locals 4

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 74
    new-instance v0, LaFn;

    invoke-direct {v0, p0}, LaFn;-><init>(LaFm;)V

    iput-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 83
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LaFm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 97
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFu;

    iput-object v0, p0, LaFm;->a:LaFu;

    .line 98
    iput-object p1, p0, LaFm;->a:LQr;

    .line 99
    iput-object p2, p0, LaFm;->a:LtK;

    .line 100
    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    .line 516
    iget-object v0, p0, LaFm;->a:LQr;

    const-string v1, "syncDbBackoff"

    const/16 v2, 0x258

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private a(LaFy;Landroid/content/ContentValues;Landroid/net/Uri;)J
    .locals 7

    .prologue
    .line 123
    invoke-direct {p0}, LaFm;->e()V

    .line 127
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p1}, LaFy;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 128
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 129
    const-string v2, "AbstractDatabaseInstance"

    const-string v3, "Failed to insert %s object"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, LaFy;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_0
    :goto_0
    invoke-direct {p0}, LaFm;->g()V

    return-wide v0

    .line 131
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1, v0, v1}, LaFm;->a(LaFy;J)V

    .line 133
    if-eqz p3, :cond_0

    .line 134
    iget-object v2, p0, LaFm;->a:LaFu;

    invoke-static {p3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LaFu;->a(Landroid/net/Uri;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    :try_start_2
    const-string v1, "AbstractDatabaseInstance"

    const-string v2, "Failed to save into %s object: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, LaFy;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 141
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 143
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LaFm;->g()V

    throw v0
.end method

.method private e()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 490
    invoke-virtual {p0}, LaFm;->a()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 491
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    .line 492
    iget-wide v2, v0, LaFq;->a:J

    add-long/2addr v2, v4

    iput-wide v2, v0, LaFq;->a:J

    .line 494
    iget-wide v2, v0, LaFq;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 495
    iget-wide v2, v0, LaFq;->b:J

    add-long/2addr v2, v4

    iput-wide v2, v0, LaFq;->b:J

    .line 496
    invoke-direct {p0}, LaFm;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2}, Ljava/lang/RuntimeException;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    :cond_0
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 502
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 560
    invoke-direct {p0}, LaFm;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-le v0, v6, :cond_1

    .line 563
    const-string v0, "AbstractDatabaseInstance"

    const-string v1, "Yielding for %d threads"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, LaFm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 564
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 565
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-eq v2, v3, :cond_0

    .line 566
    const-string v2, "AbstractDatabaseInstance"

    const-string v3, "Thread: %s\nStack:\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v7

    .line 567
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, LalV;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 566
    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 572
    :cond_1
    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    .line 607
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    iget-wide v2, v0, LaFq;->a:J

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    iput-wide v2, v0, LaFq;->a:J

    .line 609
    invoke-direct {p0}, LaFm;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    iget-wide v0, v0, LaFq;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 610
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    :cond_0
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->decrementAndGet()J

    .line 613
    return-void
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, LaFm;->a:LtK;

    sget-object v1, Lry;->g:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(JLaFy;Landroid/net/Uri;)I
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 205
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid rowId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 207
    invoke-direct {p0}, LaFm;->e()V

    .line 209
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 210
    invoke-virtual {p3}, LaFy;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, LaFy;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 209
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 211
    if-eqz p4, :cond_0

    .line 212
    iget-object v1, p0, LaFm;->a:LaFu;

    invoke-static {p4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LaFu;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :cond_0
    invoke-direct {p0}, LaFm;->g()V

    return v0

    :cond_1
    move v0, v1

    .line 205
    goto :goto_0

    .line 215
    :catch_0
    move-exception v0

    .line 216
    :try_start_1
    const-string v1, "AbstractDatabaseInstance"

    const-string v2, "Failed to delete %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p3}, LaFy;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 217
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LaFm;->g()V

    throw v0
.end method

.method public a(LaFy;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 255
    invoke-direct {p0}, LaFm;->e()V

    .line 257
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p1}, LaFy;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 262
    invoke-direct {p0}, LaFm;->g()V

    return v0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    :try_start_1
    const-string v1, "AbstractDatabaseInstance"

    const-string v2, "Failed to update %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, LaFy;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 260
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 262
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LaFm;->g()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 300
    invoke-direct {p0}, LaFm;->e()V

    .line 301
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT COUNT(*) FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 302
    const/4 v1, 0x0

    .line 304
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 305
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 313
    if-eqz v1, :cond_0

    .line 314
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 316
    :cond_0
    invoke-direct {p0}, LaFm;->g()V

    :goto_0
    return v0

    .line 308
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 313
    if-eqz v1, :cond_2

    .line 314
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 316
    :cond_2
    invoke-direct {p0}, LaFm;->g()V

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    :try_start_2
    const-string v2, "AbstractDatabaseInstance"

    const-string v3, "Failed to query %s object"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 311
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 313
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 314
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 316
    :cond_3
    invoke-direct {p0}, LaFm;->g()V

    throw v0
.end method

.method public a(JLaFy;Landroid/content/ContentValues;Landroid/net/Uri;)J
    .locals 3

    .prologue
    .line 236
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 237
    invoke-direct {p0, p3, p4, p5}, LaFm;->a(LaFy;Landroid/content/ContentValues;Landroid/net/Uri;)J

    move-result-wide p1

    .line 240
    :goto_0
    return-wide p1

    .line 239
    :cond_0
    invoke-virtual/range {p0 .. p5}, LaFm;->a(JLaFy;Landroid/content/ContentValues;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public a()LQr;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, LaFm;->a:LQr;

    return-object v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 271
    invoke-direct {p0}, LaFm;->e()V

    .line 273
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    :try_start_0
    invoke-virtual/range {v0 .. v8}, LaFm;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 275
    invoke-direct {p0}, LaFm;->g()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, LaFm;->g()V

    throw v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 284
    invoke-direct {p0}, LaFm;->e()V

    .line 286
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 287
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 292
    invoke-direct {p0}, LaFm;->g()V

    return-object v0

    .line 288
    :catch_0
    move-exception v0

    .line 289
    :try_start_1
    const-string v1, "AbstractDatabaseInstance"

    const-string v2, "Failed to query %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 290
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LaFm;->g()V

    throw v0
.end method

.method public a()LtK;
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, LaFm;->a:LtK;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 349
    invoke-virtual {p0}, LaFm;->c()Z

    move-result v0

    .line 350
    const-string v1, "database was already open"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 351
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 509
    iget-object v0, p0, LaFm;->a:LtK;

    sget-object v1, Lry;->ab:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    iget-wide v0, v0, LaFq;->b:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 512
    :cond_0
    return-void

    .line 510
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(JLaFy;Landroid/content/ContentValues;Landroid/net/Uri;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 158
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 160
    invoke-direct {p0}, LaFm;->e()V

    .line 162
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 163
    invoke-virtual {p3}, LaFy;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, LaFy;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 164
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 162
    invoke-virtual {v0, v2, p4, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 165
    if-eq v0, v1, :cond_1

    .line 166
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Database updates failed"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    :try_start_1
    const-string v1, "AbstractDatabaseInstance"

    const-string v2, "Failed to update %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p3}, LaFy;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 173
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LaFm;->g()V

    throw v0

    .line 168
    :cond_1
    if-eqz p5, :cond_2

    .line 169
    :try_start_2
    invoke-virtual {p0, p5, p1, p2}, LaFm;->a(Landroid/net/Uri;J)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    :cond_2
    invoke-direct {p0}, LaFm;->g()V

    .line 177
    return-void
.end method

.method public a(LZS;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 526
    invoke-virtual {p0}, LaFm;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    :cond_0
    :goto_0
    return-void

    .line 529
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 530
    invoke-direct {p0}, LaFm;->a()I

    move-result v3

    .line 531
    iget-object v0, p0, LaFm;->a:LQr;

    const-string v2, "syncMaxBackoff"

    const/4 v6, 0x2

    .line 532
    invoke-interface {v0, v2, v6}, LQr;->a(Ljava/lang/String;I)I

    move-result v6

    .line 535
    :try_start_0
    invoke-interface {p1}, LZS;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v1

    .line 541
    :goto_2
    if-lez v3, :cond_2

    add-int/lit8 v2, v0, 0x1

    if-ge v0, v6, :cond_2

    invoke-virtual {p0}, LaFm;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 543
    :try_start_1
    const-string v0, "AbstractDatabaseInstance"

    const-string v7, "Database already locked, backing off"

    invoke-static {v0, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    invoke-direct {p0}, LaFm;->f()V

    .line 545
    int-to-long v8, v3

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v2

    .line 551
    goto :goto_2

    .line 536
    :catch_0
    move-exception v0

    .line 537
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 546
    :catch_1
    move-exception v0

    .line 549
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 553
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    .line 554
    const-wide/16 v4, 0x32

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 555
    const-string v0, "AbstractDatabaseInstance"

    const-string v4, "Backed off for %d msec"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method protected a(LaFy;J)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 4

    .prologue
    .line 190
    const-string v0, "null uri"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid rowId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 192
    iget-object v0, p0, LaFm;->a:LaFu;

    invoke-static {p1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LaFu;->a(Landroid/net/Uri;)V

    .line 193
    return-void

    .line 191
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LZS;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 575
    invoke-direct {p0}, LaFm;->f()V

    .line 576
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely()Z

    move-result v2

    if-nez v2, :cond_0

    .line 577
    iget v1, p0, LaFm;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LaFm;->a:I

    .line 600
    :goto_0
    return v0

    .line 581
    :cond_0
    invoke-direct {p0}, LaFm;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 582
    const-string v2, "AbstractDatabaseInstance"

    const-string v3, "yieldIfContendedSafely returned true %d"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, LaFm;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 584
    :cond_1
    iput v0, p0, LaFm;->a:I

    .line 587
    invoke-virtual {p0}, LaFm;->c()V

    .line 589
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    invoke-static {v2}, LbiT;->b(Z)V

    .line 590
    invoke-direct {p0}, LaFm;->f()V

    .line 591
    invoke-direct {p0}, LaFm;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 592
    invoke-virtual {p0, p1}, LaFm;->a(LZS;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 597
    const-string v2, "AbstractDatabaseInstance"

    const-string v3, "yield finished"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 598
    invoke-virtual {p0}, LaFm;->b()V

    :goto_2
    move v0, v1

    .line 600
    goto :goto_0

    :cond_2
    move v2, v0

    .line 589
    goto :goto_1

    .line 593
    :catch_0
    move-exception v2

    .line 594
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597
    const-string v2, "AbstractDatabaseInstance"

    const-string v3, "yield finished"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 598
    invoke-virtual {p0}, LaFm;->b()V

    goto :goto_2

    .line 597
    :catchall_0
    move-exception v1

    const-string v2, "AbstractDatabaseInstance"

    const-string v3, "yield finished"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 598
    invoke-virtual {p0}, LaFm;->b()V

    throw v1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 324
    invoke-direct {p0}, LaFm;->e()V

    .line 326
    :try_start_0
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 331
    invoke-direct {p0}, LaFm;->g()V

    return v0

    .line 327
    :catch_0
    move-exception v0

    .line 328
    :try_start_1
    const-string v1, "AbstractDatabaseInstance"

    const-string v2, "Failed to delete from %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 329
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LaFm;->g()V

    throw v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 505
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    iget-wide v0, v0, LaFq;->b:J

    return-wide v0
.end method

.method protected b()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    .line 108
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 109
    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    return-object v0

    .line 108
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 377
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    .line 378
    iget-wide v2, v0, LaFq;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 379
    const/4 v1, 0x0

    iput-boolean v1, v0, LaFq;->a:Z

    .line 381
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, LaFq;->b:Z

    .line 383
    invoke-direct {p0}, LaFm;->e()V

    .line 384
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 385
    return-void
.end method

.method protected final b()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 412
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isDbLockedByOtherThreads()Z

    move-result v0

    .line 413
    if-nez v0, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    iget-object v3, p0, LaFm;->a:LQr;

    const-string v4, "enableDatabaseTransactionCounterCheckMinApi"

    invoke-interface {v3, v4, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 415
    iget-object v0, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 416
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    iget-wide v4, v0, LaFq;->a:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 418
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 416
    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 389
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 390
    invoke-direct {p0}, LaFm;->g()V

    .line 391
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    .line 392
    iget-boolean v2, v0, LaFq;->b:Z

    if-eqz v2, :cond_0

    .line 393
    iput-boolean v1, v0, LaFq;->a:Z

    .line 395
    :cond_0
    iget-wide v2, v0, LaFq;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 396
    iget-boolean v0, v0, LaFq;->a:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, LaFm;->a(Z)V

    .line 398
    :cond_1
    return-void

    .line 396
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 447
    invoke-virtual {p0}, LaFm;->d()Z

    move-result v0

    .line 448
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    .line 449
    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 435
    invoke-virtual {p0}, LaFm;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 436
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    const/4 v1, 0x0

    iput-boolean v1, v0, LaFq;->b:Z

    .line 437
    return-void
.end method

.method public d()Z
    .locals 4

    .prologue
    .line 462
    new-instance v0, LaFo;

    invoke-direct {v0, p0}, LaFo;-><init>(LaFm;)V

    invoke-static {v0}, Lbjw;->a(Lbjv;)Lbjv;

    move-result-object v0

    .line 463
    iget-object v1, p0, LaFm;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 464
    if-eqz v1, :cond_0

    .line 465
    new-instance v2, LaFp;

    const-string v3, "Open database in background"

    invoke-direct {v2, p0, v3, v0}, LaFp;-><init>(LaFm;Ljava/lang/String;Lbjv;)V

    .line 473
    invoke-virtual {v2}, LaFp;->start()V

    .line 475
    :cond_0
    return v1
.end method

.method protected final e()Z
    .locals 4

    .prologue
    .line 482
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    iget-wide v0, v0, LaFq;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 4

    .prologue
    .line 629
    iget-object v0, p0, LaFm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFq;

    iget-wide v0, v0, LaFq;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
