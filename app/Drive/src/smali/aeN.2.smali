.class public LaeN;
.super Ljava/lang/Object;
.source "DocsListGDataParserFactory.java"

# interfaces
.implements LbwT;


# instance fields
.field private final a:Lbxr;


# direct methods
.method public constructor <init>(Lbxr;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, LaeN;->a:Lbxr;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lbxj;
    .locals 3

    .prologue
    .line 41
    :try_start_0
    iget-object v0, p0, LaeN;->a:Lbxr;

    invoke-interface {v0}, Lbxr;->a()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 45
    new-instance v1, LaeM;

    invoke-direct {v1, p1, v0}, LaeM;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 43
    new-instance v1, Lbxk;

    const-string v2, "Could not create XmlPullParser"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxj;
    .locals 3

    .prologue
    .line 51
    const-class v0, LaeF;

    if-eq p1, v0, :cond_0

    const-class v0, LaeC;

    if-ne p1, v0, :cond_1

    .line 52
    :cond_0
    invoke-virtual {p0, p2}, LaeN;->a(Ljava/io/InputStream;)Lbxj;

    move-result-object v0

    return-object v0

    .line 55
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown entry class \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 56
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' specified."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lbxb;)Lbxs;
    .locals 3

    .prologue
    .line 61
    instance-of v0, p1, LaeC;

    if-eqz v0, :cond_0

    .line 62
    check-cast p1, LaeC;

    .line 63
    new-instance v0, LaeK;

    iget-object v1, p0, LaeN;->a:Lbxr;

    invoke-direct {v0, v1, p1}, LaeK;-><init>(Lbxr;LaeC;)V

    .line 66
    :goto_0
    return-object v0

    .line 64
    :cond_0
    instance-of v0, p1, LaeF;

    if-eqz v0, :cond_1

    .line 65
    check-cast p1, LaeF;

    .line 66
    new-instance v0, LaeS;

    iget-object v1, p0, LaeN;->a:Lbxr;

    invoke-direct {v0, v1, p1}, LaeS;-><init>(Lbxr;LaeF;)V

    goto :goto_0

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected Entry class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/util/Enumeration;)Lbxs;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lbxt;

    iget-object v1, p0, LaeN;->a:Lbxr;

    invoke-direct {v0, p0, v1, p1}, Lbxt;-><init>(LbwT;Lbxr;Ljava/util/Enumeration;)V

    return-object v0
.end method
