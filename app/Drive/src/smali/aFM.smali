.class public final LaFM;
.super Ljava/lang/Object;
.source "Account.java"


# instance fields
.field private final a:J

.field private final a:LaFO;


# direct methods
.method public constructor <init>(LaFO;J)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, LaFM;->a:LaFO;

    .line 25
    iput-wide p2, p0, LaFM;->a:J

    .line 26
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, LaFM;->a:J

    return-wide v0
.end method

.method public a()LaFO;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LaFM;->a:LaFO;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44
    if-ne p0, p1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    instance-of v2, p1, LaFM;

    if-nez v2, :cond_2

    move v0, v1

    .line 48
    goto :goto_0

    .line 50
    :cond_2
    check-cast p1, LaFM;

    .line 51
    iget-object v2, p0, LaFM;->a:LaFO;

    iget-object v3, p1, LaFM;->a:LaFO;

    invoke-virtual {v2, v3}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, LaFM;->a:J

    iget-wide v4, p1, LaFM;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LaFM;->a:LaFO;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, LaFM;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 62
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Account[%s, %d]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LaFM;->a:LaFO;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, LaFM;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
