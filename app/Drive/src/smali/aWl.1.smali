.class public interface abstract LaWl;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()F
.end method

.method public abstract a()I
.end method

.method public abstract a()LaLm;
.end method

.method public abstract a()LaWJ;
.end method

.method public abstract a()LaWx;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/CircleOptions;)LaYD;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)LaYG;
.end method

.method public abstract a()LaYJ;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/MarkerOptions;)LaYM;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolygonOptions;)LaYP;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)LaYS;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolylineOptions;)LaYx;
.end method

.method public abstract a()Landroid/location/Location;
.end method

.method public abstract a()Lcom/google/android/gms/maps/model/CameraPosition;
.end method

.method public abstract a()V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(IIII)V
.end method

.method public abstract a(LaLm;)V
.end method

.method public abstract a(LaLm;ILaWN;)V
.end method

.method public abstract a(LaLm;LaWN;)V
.end method

.method public abstract a(LaWT;)V
.end method

.method public abstract a(LaWW;)V
.end method

.method public abstract a(LaWZ;)V
.end method

.method public abstract a(LaWo;)V
.end method

.method public abstract a(LaXA;)V
.end method

.method public abstract a(LaXM;LaLm;)V
.end method

.method public abstract a(LaXc;)V
.end method

.method public abstract a(LaXi;)V
.end method

.method public abstract a(LaXl;)V
.end method

.method public abstract a(LaXo;)V
.end method

.method public abstract a(LaXr;)V
.end method

.method public abstract a(LaXu;)V
.end method

.method public abstract a(LaXx;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract a(Z)Z
.end method

.method public abstract b()F
.end method

.method public abstract b()V
.end method

.method public abstract b(LaLm;)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(Z)V
.end method

.method public abstract c()Z
.end method

.method public abstract d(Z)V
.end method

.method public abstract d()Z
.end method
