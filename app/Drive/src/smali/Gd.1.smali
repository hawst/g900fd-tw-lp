.class public LGd;
.super Ljava/lang/Object;
.source "DownloadFileDocumentOpenerImpl.java"


# instance fields
.field private final a:LFR;

.field private final a:LaGM;

.field private final a:Ladb;

.field private final a:LagG;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LGa;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:Lrm;


# direct methods
.method constructor <init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;Laja;)V
    .locals 0
    .param p5    # LFR;
        .annotation runtime Lbxv;
            a = "DefaultLocal"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrm;",
            "LagG;",
            "LaGM;",
            "LqK;",
            "LFR;",
            "Ladb;",
            "Laja",
            "<",
            "LGa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-object p1, p0, LGd;->a:Lrm;

    .line 150
    iput-object p2, p0, LGd;->a:LagG;

    .line 151
    iput-object p3, p0, LGd;->a:LaGM;

    .line 152
    iput-object p4, p0, LGd;->a:LqK;

    .line 153
    iput-object p5, p0, LGd;->a:LFR;

    .line 154
    iput-object p6, p0, LGd;->a:Ladb;

    .line 155
    iput-object p7, p0, LGd;->a:Lbxw;

    .line 156
    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, LGd;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGb;

    invoke-virtual {p0, v0, p1}, LGd;->a(LGb;I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    move-result-object v0

    return-object v0
.end method

.method a(LGb;I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;
    .locals 10

    .prologue
    .line 178
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    iget-object v1, p0, LGd;->a:Lrm;

    iget-object v2, p0, LGd;->a:LagG;

    iget-object v3, p0, LGd;->a:LaGM;

    iget-object v4, p0, LGd;->a:LqK;

    iget-object v5, p0, LGd;->a:LFR;

    iget-object v6, p0, LGd;->a:Ladb;

    const/4 v9, 0x0

    move-object v7, p1

    move v8, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;-><init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;LGb;ILFZ;)V

    return-object v0
.end method

.method public a(LGj;)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;
    .locals 1

    .prologue
    .line 167
    invoke-static {}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a()I

    move-result v0

    invoke-virtual {p0, p1, v0}, LGd;->a(LGj;I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    move-result-object v0

    return-object v0
.end method

.method public a(LGj;I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;
    .locals 3

    .prologue
    .line 172
    new-instance v1, LGc;

    iget-object v0, p0, LGd;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGa;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, v2}, LGc;-><init>(LGa;LGj;LFZ;)V

    invoke-virtual {p0, v1, p2}, LGd;->a(LGb;I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    move-result-object v0

    return-object v0
.end method
