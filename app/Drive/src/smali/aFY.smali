.class public LaFY;
.super Ljava/lang/Object;
.source "DataModelModule.java"

# interfaces
.implements LbuC;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "AES"

    iput-object v0, p0, LaFY;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 34
    const-class v0, LaGM;

    const-class v1, LQR;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 36
    return-void
.end method

.method provideAppCacheManifestModelLoader(LaGg;)LaEi;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 71
    return-object p1
.end method

.method provideDatabaseModelLoader(LaGh;)LaGg;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 65
    return-object p1
.end method

.method provideGDataConverter(LaGG;)LaGF;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 59
    return-object p1
.end method

.method provideKeyGenerator()Ljavax/crypto/KeyGenerator;
    .locals 2
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 50
    :try_start_0
    const-string v0, "AES"

    invoke-static {v0}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Phone does not support AES"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method provideModelLoader(LbiP;LaGg;)LaGM;
    .locals 1
    .param p1    # LbiP;
        .annotation runtime LQR;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "LaGM;",
            ">;",
            "LaGg;",
            ")",
            "LaGM;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p1, p2}, LbiP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    return-object v0
.end method

.method provideNotificationManager()Lake;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 85
    new-instance v0, Lakd;

    invoke-direct {v0}, Lakd;-><init>()V

    return-object v0
.end method

.method provideObsoleteDataCleaner(LaGV;)LaGU;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 78
    return-object p1
.end method

.method provideResourceIdGenerator(LaGl;)LaHa;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 91
    return-object p1
.end method
