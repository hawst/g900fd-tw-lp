.class public LTY;
.super Ljava/lang/Object;
.source "PartnerAppInfo.java"


# static fields
.field static final a:LTY;


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, LTY;

    invoke-direct {v0}, LTY;-><init>()V

    sput-object v0, LTY;->a:LTY;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v0, p0, LTY;->a:Ljava/lang/String;

    .line 42
    iput-object v0, p0, LTY;->b:Ljava/lang/String;

    .line 43
    iput-object v0, p0, LTY;->c:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, LTY;->a:Z

    .line 45
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LTY;->a:Ljava/lang/String;

    .line 60
    iput-object p2, p0, LTY;->b:Ljava/lang/String;

    .line 61
    iput-object p3, p0, LTY;->c:Ljava/lang/String;

    .line 62
    iput-boolean p4, p0, LTY;->a:Z

    .line 63
    return-void
.end method


# virtual methods
.method public a()Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p0}, LTY;->c()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 114
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, LTY;->a:Ljava/lang/String;

    iget-object v2, p0, LTY;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, LTY;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 121
    invoke-virtual {p0}, LTY;->d()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 122
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, LTY;->a:Ljava/lang/String;

    iget-object v2, p0, LTY;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, LTY;->b()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 77
    iget-object v0, p0, LTY;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, LTY;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, LTY;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, LTY;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, LTY;->a:Z

    return v0
.end method
