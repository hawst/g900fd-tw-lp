.class public LJy;
.super Landroid/widget/BaseAdapter;
.source "HelpCardAdapter.java"

# interfaces
.implements LJt;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:LCl;

.field private final a:LJC;

.field private final a:LJj;

.field private a:LJw;

.field private final a:LJx;

.field private a:Lakv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakv",
            "<",
            "LJw;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/content/Context;

.field private a:LapF;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LJC;LJj;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 58
    new-instance v0, LJz;

    invoke-direct {v0, p0}, LJz;-><init>(LJy;)V

    iput-object v0, p0, LJy;->a:LJx;

    .line 70
    invoke-static {}, Lakv;->a()Lakv;

    move-result-object v0

    iput-object v0, p0, LJy;->a:Lakv;

    .line 77
    iput-object p1, p0, LJy;->a:Landroid/content/Context;

    .line 78
    iput-object p3, p0, LJy;->a:LJj;

    .line 79
    iput-object p2, p0, LJy;->a:LJC;

    .line 81
    invoke-virtual {p3, p0}, LJj;->a(LJt;)V

    .line 82
    return-void
.end method

.method static synthetic a(LJy;)LCl;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LJy;->a:LCl;

    return-object v0
.end method

.method static synthetic a(LJy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LJy;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(LJw;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, LJy;->a:LJC;

    invoke-virtual {v0, p1}, LJC;->a(LJw;)Z

    move-result v0

    .line 174
    if-eqz v0, :cond_1

    .line 175
    iput-object p1, p0, LJy;->a:LJw;

    .line 176
    iput-boolean v0, p0, LJy;->a:Z

    .line 177
    if-eqz p1, :cond_0

    .line 178
    iget-object v0, p0, LJy;->a:LJx;

    invoke-interface {p1, v0}, LJw;->a(LJx;)V

    .line 180
    :cond_0
    invoke-virtual {p0}, LJy;->notifyDataSetChanged()V

    .line 182
    :cond_1
    return-void
.end method

.method static synthetic a(LJy;LJw;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, LJy;->a(LJw;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LJA;

    invoke-direct {v1, p0, p1}, LJA;-><init>(LJy;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 132
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, LJy;->a:LapF;

    if-eqz v0, :cond_0

    sget-object v0, LapG;->c:LapG;

    iget-object v1, p0, LJy;->a:LapF;

    invoke-interface {v1}, LapF;->a()LapG;

    move-result-object v1

    invoke-virtual {v0, v1}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LJy;->a(LJw;)V

    .line 170
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, LJy;->a:Lakv;

    invoke-virtual {v0}, Lakv;->a()V

    .line 155
    new-instance v0, LJB;

    invoke-direct {v0, p0}, LJB;-><init>(LJy;)V

    invoke-static {v0}, Lakv;->a(LbsJ;)Lakv;

    move-result-object v0

    iput-object v0, p0, LJy;->a:Lakv;

    .line 167
    iget-object v0, p0, LJy;->a:LJj;

    iget-object v1, p0, LJy;->a:LCl;

    invoke-virtual {v0, v1}, LJj;->a(LCl;)LbsU;

    move-result-object v0

    .line 168
    iget-object v1, p0, LJy;->a:Lakv;

    invoke-static {v0, v1}, Lanj;->a(LbsU;LbsJ;)V

    goto :goto_0
.end method

.method public a(LCl;)V
    .locals 1

    .prologue
    .line 146
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    iput-object v0, p0, LJy;->a:LCl;

    .line 147
    invoke-virtual {p0}, LJy;->a()V

    .line 148
    return-void
.end method

.method public a(LapF;)V
    .locals 1

    .prologue
    .line 85
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LapF;

    iput-object v0, p0, LJy;->a:LapF;

    .line 86
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LJy;->a:LJw;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 96
    const/4 v0, 0x0

    return-object v0

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 101
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 102
    const-wide/16 v0, 0x0

    return-wide v0

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 137
    return v1

    :cond_0
    move v0, v1

    .line 136
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 107
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 108
    iget-object v0, p0, LJy;->a:LJw;

    if-eqz v0, :cond_2

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 109
    iget-object v0, p0, LJy;->a:LJw;

    iget-object v1, p0, LJy;->a:Landroid/content/Context;

    invoke-interface {v0, v1, p3}, LJw;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 111
    iget-boolean v1, p0, LJy;->a:Z

    if-eqz v1, :cond_0

    invoke-static {}, LakQ;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    invoke-direct {p0, v0}, LJy;->a(Landroid/view/View;)V

    .line 113
    iput-boolean v2, p0, LJy;->a:Z

    .line 115
    :cond_0
    return-object v0

    :cond_1
    move v0, v2

    .line 107
    goto :goto_0

    :cond_2
    move v1, v2

    .line 108
    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method
