.class public final LanN;
.super Lanv;
.source "CachingImageDownloadFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanv",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Laon;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LakR;

.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(LakR;Ljava/lang/String;Laoo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakR;",
            "Ljava/lang/String;",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p3}, Lanv;-><init>(Laoo;)V

    .line 27
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakR;

    iput-object v0, p0, LanN;->a:LakR;

    .line 28
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LanN;->a:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static a(LakR;Ljava/lang/String;Laoo;)LanN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakR;",
            "Ljava/lang/String;",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;)",
            "LanN;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, LanN;

    invoke-direct {v0, p0, p1, p2}, LanN;-><init>(LakR;Ljava/lang/String;Laoo;)V

    return-object v0
.end method

.method private a(Laon;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 71
    invoke-virtual {p1}, Laon;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    .line 73
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s-%d-%d-d_downloaded_image_%s_%s"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LanN;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 76
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 77
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x3

    .line 78
    invoke-virtual {p1}, Laon;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/data/EntrySpec;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    .line 79
    invoke-virtual {p1}, Laon;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    .line 73
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 80
    return-object v0
.end method


# virtual methods
.method protected a(Laon;)LakD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laon;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1}, LanN;->a(Laon;)Ljava/lang/String;

    move-result-object v0

    .line 45
    iget-object v1, p0, LanN;->a:LakR;

    invoke-virtual {p1}, Laon;->a()LaFO;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LakR;->a(LaFO;Ljava/lang/String;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Laon;

    move-result-object v0

    return-object v0
.end method

.method protected a(Laon;LakD;I)LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laon;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;I)",
            "LbmF",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 59
    :try_start_0
    invoke-direct {p0, p1}, LanN;->a(Laon;)Ljava/lang/String;

    move-result-object v1

    .line 60
    iget-object v2, p0, LanN;->a:LakR;

    .line 61
    invoke-virtual {p2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {p1}, Laon;->a()LaFO;

    move-result-object v3

    .line 60
    invoke-virtual {v2, v0, v3, v1}, LakR;->a(Ljava/io/File;LaFO;Ljava/lang/String;)LakD;

    move-result-object v0

    .line 62
    invoke-static {v0, p3}, LakD;->a(LakD;I)LbmF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 66
    invoke-virtual {p2}, LakD;->close()V

    return-object v0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    new-instance v1, LaoK;

    const-string v2, "An exception when saving image to cache"

    invoke-direct {v1, v2, v0}, LaoK;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    :catchall_0
    move-exception v0

    invoke-virtual {p2}, LakD;->close()V

    throw v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;I)LbmF;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Laon;

    check-cast p2, LakD;

    invoke-virtual {p0, p1, p2, p3}, LanN;->a(Laon;LakD;I)LbmF;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Laon;

    invoke-virtual {p0, p1}, LanN;->a(Laon;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p1}, LakD;->close()V

    .line 86
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, LanN;->a(LakD;)V

    return-void
.end method

.method protected a(Laon;)Z
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0, p1}, LanN;->a(Laon;)Ljava/lang/String;

    move-result-object v0

    .line 51
    iget-object v1, p0, LanN;->a:LakR;

    invoke-virtual {p1}, Laon;->a()LaFO;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LakR;->a(LaFO;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p1, Laon;

    invoke-virtual {p0, p1}, LanN;->a(Laon;)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, LanN;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;

    move-result-object v0

    return-object v0
.end method
