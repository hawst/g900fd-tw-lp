.class public LaKe;
.super Ljava/lang/Object;
.source "ApiaryRemoteResourceAccessor.java"

# interfaces
.implements LaJY;


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LQr;

.field private final a:LaFO;

.field private final a:LaJR;

.field private final a:LaKi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    const-string v0, "https://www.googleapis.com/auth/drive"

    const-string v1, "https://www.googleapis.com/auth/drive.apps.readonly"

    invoke-static {v0, v1}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    sput-object v0, LaKe;->a:Ljava/util/List;

    return-void
.end method

.method constructor <init>(LaFO;LaKi;LaJR;LQr;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, LaKe;->a:LaFO;

    .line 79
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKi;

    iput-object v0, p0, LaKe;->a:LaKi;

    .line 80
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJR;

    iput-object v0, p0, LaKe;->a:LaJR;

    .line 81
    iput-object p4, p0, LaKe;->a:LQr;

    .line 82
    return-void
.end method

.method private a()Lcom/google/api/services/drive/Drive;
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, LaKe;->a:LaKi;

    iget-object v1, p0, LaKe;->a:LaFO;

    sget-object v2, LaKe;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LaKi;->a(LaFO;Ljava/util/List;)Lcom/google/api/services/drive/Drive;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(JJ)LaJQ;
    .locals 9

    .prologue
    const/4 v7, 0x2

    .line 88
    invoke-direct {p0}, LaKe;->a()Lcom/google/api/services/drive/Drive;

    move-result-object v0

    .line 90
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v7}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 91
    new-instance v2, LaKh;

    invoke-direct {v2, p0, v1}, LaKh;-><init>(LaKe;Ljava/util/concurrent/CountDownLatch;)V

    .line 92
    new-instance v3, LaKh;

    invoke-direct {v3, p0, v1}, LaKh;-><init>(LaKe;Ljava/util/concurrent/CountDownLatch;)V

    .line 94
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive;->batch()Lbdm;

    move-result-object v4

    .line 95
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$About;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/api/services/drive/Drive$About;->a()Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v5

    .line 96
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/api/services/drive/Drive$About$Get;->b(Ljava/lang/Long;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v5

    .line 97
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/api/services/drive/Drive$About$Get;->a(Ljava/lang/Long;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v5

    .line 98
    invoke-virtual {v5, v4, v2}, Lcom/google/api/services/drive/Drive$About$Get;->queue(Lbdm;Lbdv;)V

    .line 99
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive$Apps;->a()Lcom/google/api/services/drive/Drive$Apps$List;

    move-result-object v0

    invoke-virtual {v0, v4, v3}, Lcom/google/api/services/drive/Drive$Apps$List;->queue(Lbdm;Lbdv;)V

    .line 101
    const-string v0, "ApiaryRemoteResourceAccessor"

    const-string v5, "Issuing a request for AccountMetadata(%d,%d) and Apps"

    new-array v6, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 102
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    .line 101
    invoke-static {v0, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 103
    invoke-virtual {v4}, Lbdm;->a()V

    .line 106
    :try_start_0
    iget-object v0, p0, LaKe;->a:LQr;

    const-string v4, "accountMetadataTimeoutSeconds"

    const/16 v5, 0x258

    invoke-interface {v0, v4, v5}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 108
    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5, v0}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 109
    if-nez v0, :cond_0

    .line 110
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Timeout waiting for batchRequest to execute()"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 116
    :cond_0
    invoke-virtual {v2}, LaKh;->a()Lbdw;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 117
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {v2}, LaKh;->a()Lbdw;

    move-result-object v1

    invoke-virtual {v1}, Lbdw;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_1
    invoke-virtual {v3}, LaKh;->a()Lbdw;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 121
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {v3}, LaKh;->a()Lbdw;

    move-result-object v1

    invoke-virtual {v1}, Lbdw;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_2
    iget-object v4, p0, LaKe;->a:LaJR;

    .line 125
    invoke-virtual {v2}, LaKh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About;

    invoke-virtual {v3}, LaKh;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/drive/model/AppList;

    .line 124
    invoke-interface {v4, v0, v1}, LaJR;->a(Lcom/google/api/services/drive/model/About;Lcom/google/api/services/drive/model/AppList;)LaJQ;

    move-result-object v0

    return-object v0
.end method
