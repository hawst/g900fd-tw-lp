.class public LawN;
.super Ljava/lang/Object;
.source "MosaicView.java"

# interfaces
.implements Laws;


# instance fields
.field final synthetic a:Laws;

.field final synthetic a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;Laws;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    iput-object p2, p0, LawN;->a:Laws;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lawr;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 312
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 314
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawr;

    .line 315
    iget-object v5, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-virtual {v0}, Lawr;->a()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;I)Laxx;

    move-result-object v5

    if-nez v5, :cond_1

    .line 316
    iget-object v5, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-static {v5, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;Lawr;)V

    .line 317
    invoke-virtual {v0}, Lawr;->a()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 320
    goto :goto_0

    .line 321
    :cond_0
    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    iget-object v0, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "Add %d tiles %s (%d) "

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v1, 0x1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x2

    iget-object v2, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    .line 323
    invoke-static {v2}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    .line 322
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    iget-object v0, p0, LawN;->a:Laws;

    invoke-interface {v0, p1}, Laws;->a(Ljava/lang/Iterable;)V

    .line 325
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public b(Ljava/lang/Iterable;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 328
    iget-object v0, p0, LawN;->a:Laws;

    invoke-interface {v0, p1}, Laws;->b(Ljava/lang/Iterable;)V

    .line 329
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 331
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 332
    iget-object v0, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-static {v0, v5}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;I)Laxx;

    move-result-object v6

    .line 333
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    add-int/lit8 v0, v1, 0x1

    .line 335
    if-eqz v6, :cond_0

    .line 336
    invoke-virtual {v6}, Laxx;->a()V

    .line 337
    iget-object v1, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->removeView(Landroid/view/View;)V

    .line 338
    iget-object v1, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Landroid/util/SparseArray;

    move-result-object v1

    iget-object v5, v6, Laxx;->a:Lawr;

    invoke-virtual {v5}, Lawr;->a()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/util/SparseArray;->remove(I)V

    :goto_1
    move v1, v0

    .line 342
    goto :goto_0

    .line 340
    :cond_0
    iget-object v1, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Dispose NULL Tile View @ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 343
    :cond_1
    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    iget-object v0, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "Remove %d tiles %s (%d) "

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v1, 0x1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x2

    iget-object v2, p0, LawN;->a:Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    .line 345
    invoke-static {v2}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    .line 344
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    return-void
.end method
