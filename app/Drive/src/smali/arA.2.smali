.class public LarA;
.super Larb;
.source "HoneycombActionBarHelper.java"


# instance fields
.field private final a:LabF;

.field private final a:LalO;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Larg;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method public constructor <init>(LqK;LsI;LabF;LalO;Laja;LtK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LqK;",
            "LsI;",
            "LabF;",
            "LalO;",
            "Laja",
            "<",
            "Larg;",
            ">;",
            "LtK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Larb;-><init>(LqK;LsI;)V

    .line 92
    iput-object p3, p0, LarA;->a:LabF;

    .line 93
    iput-object p4, p0, LarA;->a:LalO;

    .line 94
    iput-object p6, p0, LarA;->a:LtK;

    .line 95
    iput-object p5, p0, LarA;->a:Lbxw;

    .line 96
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;LarB;LaFO;)LaqY;
    .locals 9

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Larr;

    invoke-direct {v0}, Larr;-><init>()V

    .line 104
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lart;

    sget v3, Lxi;->logo_title:I

    iget-object v5, p0, LarA;->a:LabF;

    iget-object v6, p0, LarA;->a:LalO;

    iget-object v7, p0, LarA;->a:Lbxw;

    iget-object v8, p0, LarA;->a:LtK;

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    invoke-direct/range {v0 .. v8}, Lart;-><init>(Landroid/app/Activity;LaFO;ILarB;LabF;LalO;Lbxw;LtK;)V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 117
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    .line 118
    return-void
.end method
