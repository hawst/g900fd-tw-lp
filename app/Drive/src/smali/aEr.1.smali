.class public final enum LaEr;
.super Ljava/lang/Enum;
.source "CachedSearchResultTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEr;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEr;

.field private static final synthetic a:[LaEr;

.field public static final enum b:LaEr;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 44
    new-instance v0, LaEr;

    const-string v1, "CACHED_SEARCH_ID"

    invoke-static {}, LaEq;->b()LaEq;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "cachedSearchId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 45
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    .line 46
    invoke-static {}, LaEs;->a()LaEs;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 45
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LaEr;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEr;->a:LaEr;

    .line 51
    new-instance v0, LaEr;

    const-string v1, "RESOURCE_ID"

    invoke-static {}, LaEq;->b()LaEq;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "resourceId"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 52
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaEr;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEr;->b:LaEr;

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [LaEr;

    sget-object v1, LaEr;->a:LaEr;

    aput-object v1, v0, v6

    sget-object v1, LaEr;->b:LaEr;

    aput-object v1, v0, v7

    sput-object v0, LaEr;->a:[LaEr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEr;->a:LaFr;

    .line 60
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEr;
    .locals 1

    .prologue
    .line 43
    const-class v0, LaEr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEr;

    return-object v0
.end method

.method public static values()[LaEr;
    .locals 1

    .prologue
    .line 43
    sget-object v0, LaEr;->a:[LaEr;

    invoke-virtual {v0}, [LaEr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEr;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, LaEr;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, LaEr;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
