.class LPA;
.super Ljava/lang/Object;
.source "ActivityApi.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "LPD;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LPy;


# direct methods
.method constructor <init>(LPy;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, LPA;->a:LPy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, LPA;->a:LPy;

    invoke-static {v0}, LPy;->a(LPy;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 141
    iget-object v1, p0, LPA;->a:LPy;

    invoke-static {v1}, LPy;->a(LPy;)LPC;

    move-result-object v1

    iget-object v2, p0, LPA;->a:LPy;

    invoke-static {v2}, LPy;->a(LPy;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LPC;->a(LaGu;Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    move-result-object v0

    .line 142
    iget-object v1, p0, LPA;->a:LPy;

    invoke-static {v1, v0}, LPy;->a(LPy;Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, LPA;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
