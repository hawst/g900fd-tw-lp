.class public LOf;
.super Ljava/lang/Object;
.source "VideoSubtitleFetcher.java"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;


# instance fields
.field private a:LTh;

.field private a:Lalo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "https://docs.google.com/timedtext?fmts=1&tlangs=1&type=list"

    .line 38
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LOf;->a:Landroid/net/Uri;

    .line 40
    const-string v0, "https://docs.google.com/timedtext?format=vtt&type=track"

    .line 41
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LOf;->b:Landroid/net/Uri;

    .line 40
    return-void
.end method

.method public constructor <init>(LTh;Lalo;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, LOf;->a:LTh;

    .line 51
    iput-object p2, p0, LOf;->a:Lalo;

    .line 52
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;)LNP;
    .locals 5

    .prologue
    .line 115
    sget-object v0, LOf;->a:Landroid/net/Uri;

    .line 116
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "v"

    .line 117
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "vid"

    .line 118
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hl"

    .line 119
    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 122
    :try_start_0
    iget-object v1, p0, LOf;->a:LTh;

    iget-object v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 123
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    const/4 v3, 0x5

    const/4 v4, 0x1

    .line 122
    invoke-static {v1, v2, v0, v3, v4}, LTs;->a(LTh;LaFO;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 125
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    iget-object v1, p0, LOf;->a:LTh;

    invoke-interface {v1, v0}, LTh;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, LNO;->a(Ljava/io/InputStream;)LNO;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, LNO;->a()LNP;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 133
    iget-object v1, p0, LOf;->a:LTh;

    invoke-interface {v1}, LTh;->a()V

    .line 134
    iget-object v1, p0, LOf;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    return-object v0

    .line 130
    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Response without entity."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    :catchall_0
    move-exception v0

    iget-object v1, p0, LOf;->a:LTh;

    invoke-interface {v1}, LTh;->a()V

    .line 134
    iget-object v1, p0, LOf;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    throw v0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;LNP;)Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 81
    sget-object v0, LOf;->b:Landroid/net/Uri;

    .line 82
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "v"

    .line 83
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "vid"

    .line 84
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hl"

    .line 85
    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "lang"

    iget-object v2, p4, LNP;->c:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "name"

    iget-object v0, p4, LNP;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    .line 87
    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "kind"

    iget-object v0, p4, LNP;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    .line 88
    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 91
    :try_start_0
    iget-object v1, p0, LOf;->a:LTh;

    iget-object v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 92
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    const/4 v3, 0x5

    const/4 v4, 0x1

    .line 91
    invoke-static {v1, v2, v0, v3, v4}, LTs;->a(LTh;LaFO;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 93
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_2

    .line 95
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, LOf;->a:Lalo;

    iget-object v3, p0, LOf;->a:LTh;

    .line 96
    invoke-interface {v3, v0}, LTh;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v0

    invoke-interface {v2, v0}, Lalo;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    iget-object v0, p0, LOf;->a:LTh;

    invoke-interface {v0}, LTh;->a()V

    .line 102
    iget-object v0, p0, LOf;->a:LTh;

    invoke-interface {v0}, LTh;->b()V

    return-object v1

    .line 86
    :cond_0
    iget-object v0, p4, LNP;->a:Ljava/lang/String;

    goto :goto_0

    .line 87
    :cond_1
    iget-object v0, p4, LNP;->b:Ljava/lang/String;

    goto :goto_1

    .line 98
    :cond_2
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Response without entity."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    :catchall_0
    move-exception v0

    iget-object v1, p0, LOf;->a:LTh;

    invoke-interface {v1}, LTh;->a()V

    .line 102
    iget-object v1, p0, LOf;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 62
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, LOf;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;)LNP;

    move-result-object v1

    .line 63
    if-nez v1, :cond_0

    .line 69
    :goto_0
    return-object v0

    .line 66
    :cond_0
    invoke-direct {p0, p1, p2, p3, v1}, LOf;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;LNP;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    const-string v2, "VideoSubtitleFetcher"

    const-string v3, "Error creating subtitle uri"

    invoke-static {v2, v1, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
