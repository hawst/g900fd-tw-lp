.class Lbko;
.super Ljava/lang/Object;
.source "LocalCache.java"

# interfaces
.implements LbjF;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbjF",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field final a:LbjQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbjG;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbjG",
            "<-TK;-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4619
    new-instance v0, LbjQ;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LbjQ;-><init>(LbjG;LbjM;)V

    invoke-direct {p0, v0}, Lbko;-><init>(LbjQ;)V

    .line 4620
    return-void
.end method

.method private constructor <init>(LbjQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbjQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4623
    iput-object p1, p0, Lbko;->a:LbjQ;

    .line 4624
    return-void
.end method

.method synthetic constructor <init>(LbjQ;LbjR;)V
    .locals 0

    .prologue
    .line 4615
    invoke-direct {p0, p1}, Lbko;-><init>(LbjQ;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/concurrent/Callable",
            "<+TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 4634
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4635
    iget-object v0, p0, Lbko;->a:LbjQ;

    new-instance v1, Lbkp;

    invoke-direct {v1, p0, p2}, Lbkp;-><init>(Lbko;Ljava/util/concurrent/Callable;)V

    invoke-virtual {v0, p1, v1}, LbjQ;->a(Ljava/lang/Object;LbjM;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 4657
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4658
    iget-object v0, p0, Lbko;->a:LbjQ;

    invoke-virtual {v0, p1}, LbjQ;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4659
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 4649
    iget-object v0, p0, Lbko;->a:LbjQ;

    invoke-virtual {v0, p1, p2}, LbjQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4650
    return-void
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 4630
    iget-object v0, p0, Lbko;->a:LbjQ;

    invoke-virtual {v0, p1}, LbjQ;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
