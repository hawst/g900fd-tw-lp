.class final Lboq;
.super Ljava/util/AbstractQueue;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "Lbox",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final a:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3140
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3141
    new-instance v0, Lbor;

    invoke-direct {v0, p0}, Lbor;-><init>(Lboq;)V

    iput-object v0, p0, Lboq;->a:Lbox;

    return-void
.end method


# virtual methods
.method public a()Lbox;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3190
    iget-object v0, p0, Lboq;->a:Lbox;

    invoke-interface {v0}, Lbox;->b()Lbox;

    move-result-object v0

    .line 3191
    iget-object v1, p0, Lboq;->a:Lbox;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a(Lbox;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 3180
    invoke-interface {p1}, Lbox;->c()Lbox;

    move-result-object v0

    invoke-interface {p1}, Lbox;->b()Lbox;

    move-result-object v1

    invoke-static {v0, v1}, LbnY;->a(Lbox;Lbox;)V

    .line 3183
    iget-object v0, p0, Lboq;->a:Lbox;

    invoke-interface {v0}, Lbox;->c()Lbox;

    move-result-object v0

    invoke-static {v0, p1}, LbnY;->a(Lbox;Lbox;)V

    .line 3184
    iget-object v0, p0, Lboq;->a:Lbox;

    invoke-static {p1, v0}, LbnY;->a(Lbox;Lbox;)V

    .line 3186
    const/4 v0, 0x1

    return v0
.end method

.method public b()Lbox;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3195
    iget-object v0, p0, Lboq;->a:Lbox;

    invoke-interface {v0}, Lbox;->b()Lbox;

    move-result-object v0

    .line 3196
    iget-object v1, p0, Lboq;->a:Lbox;

    if-ne v0, v1, :cond_0

    .line 3197
    const/4 v0, 0x0

    .line 3201
    :goto_0
    return-object v0

    .line 3200
    :cond_0
    invoke-virtual {p0, v0}, Lboq;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 3239
    iget-object v0, p0, Lboq;->a:Lbox;

    invoke-interface {v0}, Lbox;->b()Lbox;

    move-result-object v0

    .line 3240
    :goto_0
    iget-object v1, p0, Lboq;->a:Lbox;

    if-eq v0, v1, :cond_0

    .line 3241
    invoke-interface {v0}, Lbox;->b()Lbox;

    move-result-object v1

    .line 3242
    invoke-static {v0}, LbnY;->b(Lbox;)V

    move-object v0, v1

    .line 3244
    goto :goto_0

    .line 3246
    :cond_0
    iget-object v0, p0, Lboq;->a:Lbox;

    iget-object v1, p0, Lboq;->a:Lbox;

    invoke-interface {v0, v1}, Lbox;->a(Lbox;)V

    .line 3247
    iget-object v0, p0, Lboq;->a:Lbox;

    iget-object v1, p0, Lboq;->a:Lbox;

    invoke-interface {v0, v1}, Lbox;->b(Lbox;)V

    .line 3248
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3219
    check-cast p1, Lbox;

    .line 3220
    invoke-interface {p1}, Lbox;->b()Lbox;

    move-result-object v0

    sget-object v1, Lbow;->a:Lbow;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 3225
    iget-object v0, p0, Lboq;->a:Lbox;

    invoke-interface {v0}, Lbox;->b()Lbox;

    move-result-object v0

    iget-object v1, p0, Lboq;->a:Lbox;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lbox",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3252
    new-instance v0, Lbos;

    invoke-virtual {p0}, Lboq;->a()Lbox;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbos;-><init>(Lboq;Lbox;)V

    return-object v0
.end method

.method public synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3140
    check-cast p1, Lbox;

    invoke-virtual {p0, p1}, Lboq;->a(Lbox;)Z

    move-result v0

    return v0
.end method

.method public synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3140
    invoke-virtual {p0}, Lboq;->a()Lbox;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3140
    invoke-virtual {p0}, Lboq;->b()Lbox;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3207
    check-cast p1, Lbox;

    .line 3208
    invoke-interface {p1}, Lbox;->c()Lbox;

    move-result-object v0

    .line 3209
    invoke-interface {p1}, Lbox;->b()Lbox;

    move-result-object v1

    .line 3210
    invoke-static {v0, v1}, LbnY;->a(Lbox;Lbox;)V

    .line 3211
    invoke-static {p1}, LbnY;->b(Lbox;)V

    .line 3213
    sget-object v0, Lbow;->a:Lbow;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 3

    .prologue
    .line 3230
    const/4 v1, 0x0

    .line 3231
    iget-object v0, p0, Lboq;->a:Lbox;

    invoke-interface {v0}, Lbox;->b()Lbox;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lboq;->a:Lbox;

    if-eq v0, v2, :cond_0

    .line 3232
    add-int/lit8 v1, v1, 0x1

    .line 3231
    invoke-interface {v0}, Lbox;->b()Lbox;

    move-result-object v0

    goto :goto_0

    .line 3234
    :cond_0
    return v1
.end method
