.class public LacF;
.super Ljava/lang/Object;
.source "SharingRequestFlow.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:LacG;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LbqD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, LacG;->a:LacG;

    iput-object v0, p0, LacF;->a:LacG;

    .line 22
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, LacF;->a:LbmF;

    .line 26
    return-void
.end method

.method private a(Ljava/lang/String;)LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbmF",
            "<",
            "LbqD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 30
    const/16 v0, 0x2c

    invoke-static {v0}, Lbjk;->a(C)Lbjk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbjk;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32
    invoke-static {v0}, LbqC;->a(Ljava/lang/String;)LbqD;

    move-result-object v0

    .line 33
    if-nez v0, :cond_0

    .line 34
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    .line 40
    :goto_1
    return-object v0

    .line 36
    :cond_0
    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "LbqD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, LacF;->a:LbmF;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, LacF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    sget-object v0, LacG;->c:LacG;

    iput-object v0, p0, LacF;->a:LacG;

    .line 66
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, LacF;->a:LbmF;

    .line 68
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 44
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v0, p0, LacF;->a:LacG;

    sget-object v1, LacG;->a:LacG;

    invoke-virtual {v0, v1}, LacG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-direct {p0, p1}, LacF;->a(Ljava/lang/String;)LbmF;

    move-result-object v0

    iput-object v0, p0, LacF;->a:LbmF;

    .line 47
    iget-object v0, p0, LacF;->a:LbmF;

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    sget-object v0, LacG;->b:LacG;

    iput-object v0, p0, LacF;->a:LacG;

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    sget-object v0, LacG;->c:LacG;

    iput-object v0, p0, LacF;->a:LacG;

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, LacF;->a:LacG;

    sget-object v1, LacG;->b:LacG;

    invoke-virtual {v0, v1}, LacG;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
