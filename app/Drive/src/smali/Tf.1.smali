.class LTf;
.super Ljava/lang/Object;
.source "AuthTokenManagerImpl.java"


# instance fields
.field final synthetic a:LTe;

.field private final a:LaFO;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LTe;LaFO;)V
    .locals 1

    .prologue
    .line 81
    iput-object p1, p0, LTf;->a:LTe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LTf;->a:Ljava/util/Map;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LTf;->b:Ljava/util/Map;

    .line 82
    iput-object p2, p0, LTf;->a:LaFO;

    .line 83
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LTf;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91
    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, LTf;->a:LTe;

    invoke-static {v0}, LTe;->a(LTe;)LSZ;

    move-result-object v0

    invoke-interface {v0}, LSZ;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 94
    :try_start_1
    iget-object v0, p0, LTf;->a:LTe;

    invoke-static {v0}, LTe;->a(LTe;)LSZ;

    move-result-object v0

    iget-object v1, p0, LTf;->a:LaFO;

    invoke-interface {v0, v1, p1}, LSZ;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    iget-object v1, p0, LTf;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch LTr; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :try_start_2
    iget-object v1, p0, LTf;->a:LTe;

    invoke-static {v1}, LTe;->a(LTe;)LSZ;

    move-result-object v1

    invoke-interface {v1}, LSZ;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 108
    :cond_0
    monitor-exit p0

    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v1, p0, LTf;->a:LTe;

    invoke-static {v1}, LTe;->a(LTe;)LSZ;

    move-result-object v1

    invoke-interface {v1}, LSZ;->b()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 90
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 98
    :catch_1
    move-exception v0

    .line 99
    :try_start_5
    throw v0

    .line 100
    :catch_2
    move-exception v0

    .line 101
    throw v0

    .line 102
    :catch_3
    move-exception v0

    .line 103
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LTf;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 131
    iget-object v1, p0, LTf;->a:LTe;

    invoke-static {v1}, LTe;->a(LTe;)LSZ;

    move-result-object v1

    invoke-interface {v1}, LSZ;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 134
    :try_start_1
    iget-object v1, p0, LTf;->a:LTe;

    invoke-static {v1}, LTe;->a(LTe;)LSZ;

    move-result-object v1

    iget-object v2, p0, LTf;->a:LaFO;

    invoke-interface {v1, v2, v0}, LSZ;->a(LaFO;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, LTf;->b:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    :try_start_2
    iget-object v0, p0, LTf;->a:LTe;

    invoke-static {v0}, LTe;->a(LTe;)LSZ;

    move-result-object v0

    invoke-interface {v0}, LSZ;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 139
    monitor-exit p0

    return-void

    .line 137
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, LTf;->a:LTe;

    invoke-static {v1}, LTe;->a(LTe;)LSZ;

    move-result-object v1

    invoke-interface {v1}, LSZ;->b()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 130
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LTf;->a:LTe;

    invoke-static {v0}, LTe;->a(LTe;)LSZ;

    move-result-object v0

    invoke-interface {v0}, LSZ;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119
    :try_start_1
    invoke-virtual {p0, p1}, LTf;->a(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, p1}, LTf;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 122
    :try_start_2
    iget-object v1, p0, LTf;->a:LTe;

    invoke-static {v1}, LTe;->a(LTe;)LSZ;

    move-result-object v1

    invoke-interface {v1}, LSZ;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, LTf;->a:LTe;

    invoke-static {v1}, LTe;->a(LTe;)LSZ;

    move-result-object v1

    invoke-interface {v1}, LSZ;->b()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 116
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
