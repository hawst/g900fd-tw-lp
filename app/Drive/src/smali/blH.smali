.class abstract LblH;
.super LblO;
.source "AbstractMapBasedMultiset.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LblO",
        "<TE;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private transient a:J

.field private transient a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TE;",
            "Lbmc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TE;",
            "Lbmc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, LblO;-><init>()V

    .line 61
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, LblH;->a:Ljava/util/Map;

    .line 62
    invoke-super {p0}, LblO;->size()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LblH;->a:J

    .line 63
    return-void
.end method

.method private static a(Lbmc;I)I
    .locals 1

    .prologue
    .line 280
    if-nez p0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 284
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lbmc;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(LblH;)J
    .locals 4

    .prologue
    .line 48
    iget-wide v0, p0, LblH;->a:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, LblH;->a:J

    return-wide v0
.end method

.method static synthetic a(LblH;J)J
    .locals 3

    .prologue
    .line 48
    iget-wide v0, p0, LblH;->a:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, LblH;->a:J

    return-wide v0
.end method

.method static synthetic a(LblH;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-static {v0, p1}, LboS;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    .line 197
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lbmc;->a()I

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;I)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 211
    if-nez p2, :cond_0

    .line 212
    invoke-virtual {p0, p1}, LblH;->a(Ljava/lang/Object;)I

    move-result v2

    .line 227
    :goto_0
    return v2

    .line 214
    :cond_0
    if-lez p2, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 215
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    .line 217
    if-nez v0, :cond_2

    .line 219
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    new-instance v1, Lbmc;

    invoke-direct {v1, p2}, Lbmc;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :goto_2
    iget-wide v0, p0, LblH;->a:J

    int-to-long v4, p2

    add-long/2addr v0, v4

    iput-wide v0, p0, LblH;->a:J

    goto :goto_0

    :cond_1
    move v0, v2

    .line 214
    goto :goto_1

    .line 221
    :cond_2
    invoke-virtual {v0}, Lbmc;->a()I

    move-result v4

    .line 222
    int-to-long v6, v4

    int-to-long v8, p2

    add-long/2addr v6, v8

    .line 223
    const-wide/32 v8, 0x7fffffff

    cmp-long v3, v6, v8

    if-gtz v3, :cond_3

    move v3, v1

    :goto_3
    const-string v5, "too many occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v3, v5, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 224
    invoke-virtual {v0, p2}, Lbmc;->a(I)I

    move v2, v4

    goto :goto_2

    :cond_3
    move v3, v2

    .line 223
    goto :goto_3
.end method

.method a()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lbpj",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 88
    new-instance v1, LblI;

    invoke-direct {v1, p0, v0}, LblI;-><init>(LblH;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lbpj",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-super {p0}, LblO;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;I)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    if-nez p2, :cond_1

    .line 233
    invoke-virtual {p0, p1}, LblH;->a(Ljava/lang/Object;)I

    move-result v2

    .line 253
    :cond_0
    :goto_0
    return v2

    .line 235
    :cond_1
    if-lez p2, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    .line 237
    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {v0}, Lbmc;->a()I

    move-result v1

    .line 244
    if-le v1, p2, :cond_3

    .line 251
    :goto_2
    neg-int v2, p2

    invoke-virtual {v0, v2}, Lbmc;->b(I)I

    .line 252
    iget-wide v2, p0, LblH;->a:J

    int-to-long v4, p2

    sub-long/2addr v2, v4

    iput-wide v2, p0, LblH;->a:J

    move v2, v1

    .line 253
    goto :goto_0

    :cond_2
    move v0, v2

    .line 235
    goto :goto_1

    .line 248
    :cond_3
    iget-object v2, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move p2, v1

    goto :goto_2
.end method

.method public c(Ljava/lang/Object;I)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 259
    const-string v0, "count"

    invoke-static {p2, v0}, Lbpk;->a(ILjava/lang/String;)V

    .line 263
    if-nez p2, :cond_0

    .line 264
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    .line 265
    invoke-static {v0, p2}, LblH;->a(Lbmc;I)I

    move-result v0

    .line 275
    :goto_0
    iget-wide v2, p0, LblH;->a:J

    sub-int v1, p2, v0

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LblH;->a:J

    .line 276
    return v0

    .line 267
    :cond_0
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    .line 268
    invoke-static {v0, p2}, LblH;->a(Lbmc;I)I

    move-result v1

    .line 270
    if-nez v0, :cond_1

    .line 271
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    new-instance v2, Lbmc;

    invoke-direct {v2, p2}, Lbmc;-><init>(I)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    .line 129
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lbmc;->a(I)V

    goto :goto_0

    .line 131
    :cond_0
    iget-object v0, p0, LblH;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 132
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LblH;->a:J

    .line 133
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v0, LblK;

    invoke-direct {v0, p0}, LblK;-><init>(LblH;)V

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 144
    iget-wide v0, p0, LblH;->a:J

    invoke-static {v0, v1}, Lbsy;->b(J)I

    move-result v0

    return v0
.end method
