.class public LEH;
.super Ljava/lang/Object;
.source "DialogRegistryImpl.java"

# interfaces
.implements LEE;


# instance fields
.field private final a:I

.field private final a:Landroid/app/Activity;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LEI;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LEB;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0, p1, p2, v0}, LEH;-><init>(Landroid/app/Activity;II)V

    .line 97
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;II)V
    .locals 3

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LEH;->a:Ljava/util/Map;

    .line 72
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LEH;->a:Ljava/util/Set;

    .line 107
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid dialogId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 108
    const-string v0, "null activity"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, LEH;->a:Landroid/app/Activity;

    .line 109
    iput p2, p0, LEH;->b:I

    .line 110
    iput p3, p0, LEH;->a:I

    .line 111
    return-void

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 160
    iget v0, p0, LEH;->b:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 161
    iget v0, p0, LEH;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LEH;->b:I

    return v0

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(LEB;)LFr;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, LEH;->a(LEB;)LFs;

    move-result-object v0

    return-object v0
.end method

.method public a(LEB;)LFs;
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, LEH;->a:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already registered instance of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 122
    invoke-virtual {p0}, LEH;->a()I

    move-result v0

    .line 123
    new-instance v1, LEI;

    invoke-direct {v1, p1}, LEI;-><init>(LEB;)V

    .line 124
    iget-object v2, p0, LEH;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    new-instance v2, LFs;

    iget-object v3, p0, LEH;->a:Landroid/app/Activity;

    invoke-direct {v2, v3, v0, v1}, LFs;-><init>(Landroid/app/Activity;ILFt;)V

    .line 126
    return-object v2
.end method

.method public a(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 131
    iget-object v0, p0, LEH;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEI;

    .line 132
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dialog "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isn\'t managed by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LbiT;->a(ZLjava/lang/Object;)V

    .line 134
    invoke-virtual {v0}, LEI;->a()LEB;

    move-result-object v1

    iget-object v2, p0, LEH;->a:Landroid/app/Activity;

    invoke-virtual {v0}, LEI;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LEB;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 139
    if-nez v0, :cond_0

    iget v1, p0, LEH;->a:I

    const/4 v2, 0x7

    if-gt v1, v2, :cond_0

    .line 140
    new-instance v0, LEJ;

    iget-object v1, p0, LEH;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, LEJ;-><init>(Landroid/content/Context;)V

    .line 142
    :cond_0
    return-object v0

    .line 132
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/app/Dialog;)V
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, LEH;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEI;

    .line 149
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dialog "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isn\'t managed by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LbiT;->a(ZLjava/lang/Object;)V

    .line 151
    instance-of v1, p2, LEJ;

    if-eqz v1, :cond_1

    .line 152
    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    .line 156
    :goto_1
    return-void

    .line 149
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 154
    :cond_1
    invoke-virtual {v0}, LEI;->a()LEB;

    move-result-object v1

    iget-object v2, p0, LEH;->a:Landroid/app/Activity;

    invoke-virtual {v0}, LEI;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {v1, v2, p2, v0}, LEB;->a(Landroid/content/Context;Landroid/app/Dialog;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, LEH;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 200
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "DialogRegistry[%d managed dialogs]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LEH;->a:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
