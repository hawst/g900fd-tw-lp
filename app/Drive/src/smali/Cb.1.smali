.class public final LCb;
.super Ljava/lang/Object;
.source "DocumentTypeFilter.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
    .locals 4

    .prologue
    .line 129
    const-class v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 130
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 129
    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    .line 131
    const-class v1, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 132
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v1

    .line 131
    invoke-static {v1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v1

    .line 133
    new-instance v2, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;-><init>(LbmY;LbmY;LCb;)V

    return-object v2
.end method

.method public a(I)[Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
    .locals 1

    .prologue
    .line 138
    new-array v0, p1, [Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0, p1}, LCb;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0, p1}, LCb;->a(I)[Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v0

    return-object v0
.end method
