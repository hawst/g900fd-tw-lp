.class public LsW;
.super LpD;
.source "DocumentOpenerActivityDelegate.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, LsW;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iput-object p2, p0, LsW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0}, LpD;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 488
    const/4 v5, 0x5

    .line 489
    const/16 v6, 0x3c

    move v0, v2

    move v4, v3

    .line 494
    :goto_0
    const/4 v1, 0x2

    if-ge v4, v1, :cond_4

    iget-object v1, p0, LsW;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LaKR;

    .line 495
    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 497
    :try_start_0
    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_1

    int-to-long v0, v5

    :goto_1
    invoke-virtual {v7, v0, v1}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 503
    add-int/lit8 v0, v4, 0x1

    .line 504
    const-string v1, "DocumentOpenerActivityDelegate"

    const-string v4, "Trying sync doc after opened. Checking count: %s"

    new-array v7, v2, [Ljava/lang/Object;

    .line 505
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    .line 504
    invoke-static {v1, v4, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 507
    iget-object v1, p0, LsW;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LaGM;

    iget-object v4, p0, LsW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v4}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v1

    .line 508
    if-nez v1, :cond_2

    .line 524
    :cond_0
    :goto_2
    return-void

    .line 497
    :cond_1
    int-to-long v0, v6

    goto :goto_1

    .line 499
    :catch_0
    move-exception v0

    .line 500
    invoke-virtual {p0}, LsW;->a()V

    goto :goto_2

    .line 511
    :cond_2
    iget-object v4, p0, LsW;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v4, v4, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LagZ;

    invoke-interface {v4, v1}, LagZ;->d(LaGo;)V

    .line 513
    iget-object v1, p0, LsW;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LaGM;

    iget-object v4, p0, LsW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v4}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v1

    .line 514
    if-eqz v1, :cond_0

    .line 517
    iget-object v4, p0, LsW;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v4, v4, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Ladi;

    sget-object v7, LacY;->a:LacY;

    invoke-interface {v4, v1, v7}, Ladi;->b(LaGo;LacY;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 518
    const-string v4, "DocumentOpenerActivityDelegate"

    const-string v5, "Doc synced after opened. Checking count: %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v4, v5, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 519
    iget-object v0, p0, LsW;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LagZ;

    invoke-interface {v0, v1}, LagZ;->c(LaGo;)V

    goto :goto_2

    :cond_3
    move v4, v0

    move v0, v3

    .line 522
    goto :goto_0

    .line 523
    :cond_4
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "No change detected. Checking count: %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2
.end method
