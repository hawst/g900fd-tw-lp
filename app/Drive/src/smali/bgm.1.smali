.class public final enum Lbgm;
.super Ljava/lang/Enum;
.source "CaptureActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbgm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbgm;

.field private static final synthetic a:[Lbgm;

.field public static final enum b:Lbgm;

.field public static final enum c:Lbgm;

.field public static final enum d:Lbgm;

.field public static final enum e:Lbgm;

.field public static final enum f:Lbgm;

.field public static final enum g:Lbgm;

.field public static final enum h:Lbgm;

.field public static final enum i:Lbgm;

.field public static final enum j:Lbgm;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    new-instance v0, Lbgm;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v3}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->a:Lbgm;

    .line 87
    new-instance v0, Lbgm;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->b:Lbgm;

    .line 88
    new-instance v0, Lbgm;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v5}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->c:Lbgm;

    .line 89
    new-instance v0, Lbgm;

    const-string v1, "QUERY_FOCUS"

    invoke-direct {v0, v1, v6}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->d:Lbgm;

    .line 90
    new-instance v0, Lbgm;

    const-string v1, "FOCUS_ONLY"

    invoke-direct {v0, v1, v7}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->e:Lbgm;

    .line 91
    new-instance v0, Lbgm;

    const-string v1, "HOLDING_FOCUS_BUTTONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->f:Lbgm;

    .line 92
    new-instance v0, Lbgm;

    const-string v1, "PIC_AFTER_FOCUS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->g:Lbgm;

    .line 93
    new-instance v0, Lbgm;

    const-string v1, "PROCESSING_RESULT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->h:Lbgm;

    .line 94
    new-instance v0, Lbgm;

    const-string v1, "TAKING_PICTURE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->i:Lbgm;

    .line 95
    new-instance v0, Lbgm;

    const-string v1, "STORAGE_ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgm;->j:Lbgm;

    .line 85
    const/16 v0, 0xa

    new-array v0, v0, [Lbgm;

    sget-object v1, Lbgm;->a:Lbgm;

    aput-object v1, v0, v3

    sget-object v1, Lbgm;->b:Lbgm;

    aput-object v1, v0, v4

    sget-object v1, Lbgm;->c:Lbgm;

    aput-object v1, v0, v5

    sget-object v1, Lbgm;->d:Lbgm;

    aput-object v1, v0, v6

    sget-object v1, Lbgm;->e:Lbgm;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lbgm;->f:Lbgm;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbgm;->g:Lbgm;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbgm;->h:Lbgm;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbgm;->i:Lbgm;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbgm;->j:Lbgm;

    aput-object v2, v0, v1

    sput-object v0, Lbgm;->a:[Lbgm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbgm;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lbgm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbgm;

    return-object v0
.end method

.method public static values()[Lbgm;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lbgm;->a:[Lbgm;

    invoke-virtual {v0}, [Lbgm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbgm;

    return-object v0
.end method
