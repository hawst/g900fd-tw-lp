.class public final LBF;
.super Ljava/lang/Object;
.source "DocListRowSectionIndexer.java"


# instance fields
.field private final a:I

.field private final a:LIB;

.field private final b:I


# direct methods
.method private constructor <init>(LIB;II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 35
    if-ltz p3, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->a(Z)V

    .line 36
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIB;

    iput-object v0, p0, LBF;->a:LIB;

    .line 37
    iput p2, p0, LBF;->a:I

    .line 38
    iput p3, p0, LBF;->b:I

    .line 39
    return-void

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0

    :cond_1
    move v1, v2

    .line 35
    goto :goto_1
.end method

.method synthetic constructor <init>(LIB;IILBD;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, LBF;-><init>(LIB;II)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, LBF;->a:I

    return v0
.end method

.method public a()LIB;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LBF;->a:LIB;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, LBF;->b:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 61
    instance-of v1, p1, LBF;

    if-nez v1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    check-cast p1, LBF;

    .line 65
    iget-object v1, p0, LBF;->a:LIB;

    iget-object v2, p1, LBF;->a:LIB;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LBF;->a:I

    iget v2, p1, LBF;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LBF;->b:I

    iget v2, p1, LBF;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LBF;->a:LIB;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LBF;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, LBF;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    const-class v0, LBC;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "groupedEntry"

    iget-object v2, p0, LBF;->a:LIB;

    .line 77
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    const-string v1, "startCursorPosition"

    iget v2, p0, LBF;->a:I

    .line 78
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    move-result-object v0

    const-string v1, "numberOfEntriesInTheRow"

    iget v2, p0, LBF;->b:I

    .line 79
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
