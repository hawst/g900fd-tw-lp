.class public LahR;
.super Ljava/lang/Object;
.source "TaskInfo.java"


# instance fields
.field private final a:J

.field private final a:LafT;

.field private final a:LahS;

.field private a:Z

.field private final b:J

.field private final b:LafT;


# direct methods
.method private constructor <init>(LahS;LafT;JJ)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, LafT;->s:LafT;

    iput-object v0, p0, LahR;->b:LafT;

    .line 40
    iput-object p1, p0, LahR;->a:LahS;

    .line 41
    iput-object p2, p0, LahR;->a:LafT;

    .line 42
    iput-wide p3, p0, LahR;->a:J

    .line 43
    iput-wide p5, p0, LahR;->b:J

    .line 44
    return-void
.end method

.method public static a()LahR;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 58
    new-instance v1, LahR;

    const/4 v2, 0x0

    sget-object v3, LafT;->n:LafT;

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    return-object v1
.end method

.method static a(JJ)LahR;
    .locals 8

    .prologue
    .line 63
    new-instance v1, LahR;

    const/4 v2, 0x0

    sget-object v3, LafT;->m:LafT;

    move-wide v4, p0

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    return-object v1
.end method

.method static a(LafT;)LahR;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 51
    sget-object v0, Lagg;->b:Lagg;

    .line 52
    invoke-virtual {p0}, LafT;->a()Lagg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lagg;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 51
    invoke-static {v0}, LbiT;->a(Z)V

    .line 53
    new-instance v1, LahR;

    const/4 v2, 0x0

    move-object v3, p0

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    return-object v1
.end method

.method static a(LahR;LahR;)LahR;
    .locals 8

    .prologue
    .line 128
    invoke-virtual {p0}, LahR;->a()LahS;

    move-result-object v2

    .line 129
    invoke-virtual {p1}, LahR;->a()LafT;

    move-result-object v3

    .line 130
    invoke-virtual {p1}, LahR;->a()Lagg;

    move-result-object v0

    .line 133
    sget-object v1, Lagg;->e:Lagg;

    if-eq v0, v1, :cond_0

    sget-object v1, Lagg;->b:Lagg;

    if-eq v0, v1, :cond_0

    sget-object v1, Lagg;->g:Lagg;

    if-eq v0, v1, :cond_0

    sget-object v1, Lagg;->f:Lagg;

    if-ne v0, v1, :cond_1

    .line 137
    :cond_0
    new-instance v1, LahR;

    .line 138
    invoke-virtual {p0}, LahR;->a()J

    move-result-wide v4

    invoke-virtual {p0}, LahR;->b()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    .line 143
    :goto_0
    invoke-virtual {p0}, LahR;->a()Z

    move-result v0

    invoke-virtual {v1, v0}, LahR;->a(Z)V

    .line 144
    return-object v1

    .line 140
    :cond_1
    new-instance v1, LahR;

    .line 141
    invoke-virtual {p1}, LahR;->a()J

    move-result-wide v4

    invoke-virtual {p1}, LahR;->b()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    goto :goto_0
.end method

.method static a(LahS;)LahR;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 47
    new-instance v1, LahR;

    sget-object v3, LafT;->l:LafT;

    move-object v2, p0

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    return-object v1
.end method

.method static b()LahR;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 67
    new-instance v1, LahR;

    const/4 v2, 0x0

    sget-object v3, LafT;->d:LafT;

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    return-object v1
.end method

.method static b(LafT;)LahR;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 71
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lagg;->f:Lagg;

    .line 73
    invoke-virtual {p0}, LafT;->a()Lagg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lagg;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 72
    invoke-static {v0}, LbiT;->a(Z)V

    .line 74
    new-instance v1, LahR;

    const/4 v2, 0x0

    move-object v3, p0

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    return-object v1
.end method

.method static c()LahR;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 78
    new-instance v1, LahR;

    const/4 v2, 0x0

    sget-object v3, LafT;->c:LafT;

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LahR;-><init>(LahS;LafT;JJ)V

    return-object v1
.end method


# virtual methods
.method public declared-synchronized a()J
    .locals 2

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LahR;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()LafT;
    .locals 1

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahR;->a:LafT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Lagg;
    .locals 1

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahR;->a:LafT;

    invoke-virtual {v0}, LafT;->a()Lagg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()LahS;
    .locals 1

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahR;->a:LahS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 110
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LahR;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    monitor-exit p0

    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Z
    .locals 1

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LahR;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()J
    .locals 2

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LahR;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()LafT;
    .locals 1

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahR;->b:LafT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "TaskInfo[%s, %d of %d bytes loaded]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 116
    invoke-virtual {p0}, LahR;->a()LafT;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, LahR;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 117
    invoke-virtual {p0}, LahR;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 115
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
