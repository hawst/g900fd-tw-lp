.class public LHM;
.super Ljava/lang/Object;
.source "GridViewBinderFactory.java"


# instance fields
.field private final a:LBf;

.field private final a:LCU;

.field private final a:LDU;

.field private final a:LDk;

.field private final a:LDn;

.field private final a:LaKM;

.field private final a:LaoY;

.field private final a:Lapd;

.field private final a:Laqw;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Laja;LBf;LDU;LCU;LDk;Laqw;LtK;LaKM;LDn;Lapd;LaoY;LsC;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LBf;",
            "LDU;",
            "LCU;",
            "LDk;",
            "Laqw;",
            "LtK;",
            "LaKM;",
            "LDn;",
            "Lapd;",
            "LaoY;",
            "LsC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, LHM;->a:LbuE;

    .line 71
    iput-object p2, p0, LHM;->a:LBf;

    .line 72
    iput-object p3, p0, LHM;->a:LDU;

    .line 73
    iput-object p4, p0, LHM;->a:LCU;

    .line 74
    iput-object p5, p0, LHM;->a:LDk;

    .line 75
    iput-object p6, p0, LHM;->a:Laqw;

    .line 76
    iput-object p7, p0, LHM;->a:LtK;

    .line 77
    iput-object p8, p0, LHM;->a:LaKM;

    .line 78
    iput-object p9, p0, LHM;->a:LDn;

    .line 79
    iput-object p10, p0, LHM;->a:Lapd;

    .line 80
    iput-object p11, p0, LHM;->a:LaoY;

    .line 81
    invoke-virtual {p1}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 82
    invoke-interface {p12, v0}, LsC;->a(Landroid/content/res/Resources;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    iput-object v0, p0, LHM;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 83
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/Fragment;LCt;)LHL;
    .locals 15

    .prologue
    .line 88
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 89
    iget-object v0, p0, LHM;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 90
    new-instance v6, LamF;

    iget-object v0, p0, LHM;->a:LbuE;

    .line 91
    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v6, v0, v1}, LamF;-><init>(Landroid/content/Context;Landroid/text/format/Time;)V

    .line 93
    new-instance v0, LHL;

    iget-object v1, p0, LHM;->a:LbuE;

    .line 94
    invoke-interface {v1}, LbuE;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v3, p0, LHM;->a:LBf;

    iget-object v4, p0, LHM;->a:LDU;

    iget-object v5, p0, LHM;->a:LCU;

    iget-object v7, p0, LHM;->a:LDk;

    iget-object v2, p0, LHM;->a:Laqw;

    .line 101
    invoke-interface {v2}, Laqw;->b()I

    move-result v8

    iget-object v9, p0, LHM;->a:LtK;

    iget-object v10, p0, LHM;->a:Lapd;

    iget-object v11, p0, LHM;->a:LaoY;

    iget-object v12, p0, LHM;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iget-object v14, p0, LHM;->a:LDn;

    move-object/from16 v2, p1

    move-object/from16 v13, p2

    invoke-direct/range {v0 .. v14}, LHL;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LBf;LDU;LCU;LamF;LDk;ILtK;Lapd;LaoY;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LCt;LDn;)V

    return-object v0
.end method
