.class LaEQ;
.super LaGz;
.source "EntryChangeNotifier.java"


# instance fields
.field final synthetic a:LaEC;

.field final synthetic a:LaEP;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method constructor <init>(LaEP;Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, LaEQ;->a:LaEP;

    iput-object p3, p0, LaEQ;->a:LaEC;

    iput-object p4, p0, LaEQ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0, p2}, LaGz;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 59
    const-string v0, "EntryChangeNotifier"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entry not found for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaEQ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-void
.end method

.method protected a(LaGu;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, LaEQ;->a:LaEC;

    invoke-interface {v0, p1}, LaEC;->a(LaGu;)V

    .line 54
    return-void
.end method
