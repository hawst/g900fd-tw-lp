.class public Lavy;
.super Ljava/lang/Object;
.source "FilmScrollView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:F

.field final synthetic a:Lcom/google/android/apps/viewer/film/FilmScrollView;

.field private a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V
    .locals 1

    .prologue
    .line 388
    iput-object p1, p0, Lavy;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavy;->a:Z

    .line 392
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lavy;->a:F

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v0, 0x3f333333    # 0.7f

    const v7, 0x3e99999a    # 0.3f

    .line 396
    iget-object v1, p0, Lavy;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(Lcom/google/android/apps/viewer/film/FilmScrollView;)Lcom/google/android/apps/viewer/film/FilmView;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/viewer/film/FilmView;->a(Landroid/view/View;)I

    move-result v1

    .line 397
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 398
    const-string v0, "FilmScrollView"

    const-string v1, "Click on a non-frame of the film view ??"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :goto_0
    return-void

    .line 402
    :cond_0
    iget-object v2, p0, Lavy;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-virtual {v2}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a()F

    move-result v2

    .line 403
    const-string v3, "FilmScrollView"

    const-string v4, "onClick on %s (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    iget-object v3, p0, Lavy;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v3}, Lcom/google/android/apps/viewer/film/FilmScrollView;->b(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    .line 405
    iget-boolean v3, p0, Lavy;->a:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lavy;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-virtual {v3}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 407
    :cond_1
    iput-boolean v8, p0, Lavy;->a:Z

    .line 408
    sub-float v1, v2, v7

    .line 409
    cmpg-float v2, v1, v0

    if-gtz v2, :cond_3

    .line 411
    iput-boolean v9, p0, Lavy;->a:Z

    .line 414
    :goto_1
    iget-object v1, p0, Lavy;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(FF)V

    goto :goto_0

    .line 417
    :cond_2
    iget-object v0, p0, Lavy;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    add-float/2addr v2, v7

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(FLjava/lang/Integer;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
