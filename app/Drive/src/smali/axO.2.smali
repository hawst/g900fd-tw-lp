.class LaxO;
.super Laxy;
.source "PdfPageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Lcom/google/android/apps/viewer/pdflib/Selection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaxI;

.field private final a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

.field private final b:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;


# direct methods
.method constructor <init>(LaxI;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V
    .locals 2

    .prologue
    .line 370
    iput-object p1, p0, LaxO;->a:LaxI;

    .line 371
    invoke-static {p1}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    sget-object v1, LaxR;->e:LaxR;

    invoke-direct {p0, v0, v1}, Laxy;-><init>(Laxz;LaxR;)V

    .line 372
    iput-object p2, p0, LaxO;->a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 373
    iput-object p3, p0, LaxO;->b:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 374
    return-void
.end method


# virtual methods
.method protected a(LavE;)Lcom/google/android/apps/viewer/pdflib/Selection;
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, LaxO;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    iget-object v1, p0, LaxO;->a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    iget-object v2, p0, LaxO;->b:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    invoke-interface {p1, v0, v1, v2}, LavE;->a(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)Lcom/google/android/apps/viewer/pdflib/Selection;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 366
    invoke-virtual {p0, p1}, LaxO;->a(LavE;)Lcom/google/android/apps/viewer/pdflib/Selection;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;Lcom/google/android/apps/viewer/pdflib/Selection;)V
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, LaxO;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    invoke-interface {p1, v0, p2}, LaxH;->a(ILcom/google/android/apps/viewer/pdflib/Selection;)V

    .line 383
    iget-object v0, p0, LaxO;->a:LaxI;

    const/4 v1, 0x0

    iput-object v1, v0, LaxI;->a:LaxO;

    .line 384
    return-void
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 366
    check-cast p2, Lcom/google/android/apps/viewer/pdflib/Selection;

    invoke-virtual {p0, p1, p2}, LaxO;->a(LaxH;Lcom/google/android/apps/viewer/pdflib/Selection;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 387
    const-string v0, "SelectionTask(page=%d, start=%s, stop=%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaxO;->a:LaxI;

    invoke-static {v3}, LaxI;->a(LaxI;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaxO;->a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LaxO;->b:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
