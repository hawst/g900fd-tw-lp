.class public Labj;
.super Ljava/lang/Object;
.source "AddCollaboratorTextDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Labj;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    iput-object p2, p0, Labj;->a:Ljava/lang/String;

    iput-object p3, p0, Labj;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 293
    check-cast p1, Landroid/app/AlertDialog;

    .line 294
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/app/AlertDialog;)V

    .line 295
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    iget-object v1, p0, Labj;->a:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/app/AlertDialog;Ljava/lang/String;)V

    .line 296
    invoke-virtual {p1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    .line 297
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 298
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()LH;

    move-result-object v0

    .line 299
    if-eqz v0, :cond_0

    .line 300
    iget-object v1, p0, Labj;->a:Landroid/view/View;

    iget-object v2, p0, Labj;->a:Landroid/view/View;

    .line 301
    invoke-virtual {v2}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    .line 300
    invoke-static {v0, v1, v2}, LUs;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 303
    :cond_0
    return-void
.end method
