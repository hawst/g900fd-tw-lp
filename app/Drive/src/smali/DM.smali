.class public LDM;
.super Ljava/lang/Object;
.source "SyncViewState.java"


# instance fields
.field private final a:LBZ;

.field private final a:LBf;

.field private final a:LaGM;

.field private final a:LahB;

.field private final a:Ljava/lang/String;

.field private final a:LvU;

.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final b:Z

.field private c:Z


# direct methods
.method public constructor <init>(LaGM;LvU;LBf;LahB;LBZ;LCn;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LDM;->a:LaGM;

    .line 87
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p0, LDM;->a:LvU;

    .line 88
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBf;

    iput-object v0, p0, LDM;->a:LBf;

    .line 89
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahB;

    iput-object v0, p0, LDM;->a:LahB;

    .line 90
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBZ;

    iput-object v0, p0, LDM;->a:LBZ;

    .line 91
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LDM;->a:Ljava/lang/String;

    .line 92
    invoke-static {p8}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LDM;->b:Ljava/lang/String;

    .line 93
    sget-object v0, LCn;->h:LCn;

    invoke-virtual {p6, v0}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LDM;->a:Z

    .line 94
    sget-object v0, LCn;->i:LCn;

    invoke-virtual {p6, v0}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LDM;->b:Z

    .line 95
    return-void
.end method

.method private a(LDT;)V
    .locals 2

    .prologue
    .line 603
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 604
    invoke-static {p1}, LDT;->b(LDT;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 605
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-static {p1}, LDT;->b(LDT;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 607
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 608
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusRightId(I)V

    .line 609
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 612
    invoke-static {p1}, LDT;->b(LDT;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 613
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-static {p1}, LDT;->b(LDT;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    goto :goto_0
.end method

.method private a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 411
    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 412
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eq p2, v0, :cond_0

    invoke-static {p1}, LDT;->a(LDT;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 413
    invoke-static {p1}, LDT;->a(LDT;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    :cond_0
    return-void
.end method

.method private a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 4

    .prologue
    .line 418
    invoke-static {p1}, LDT;->a(LDT;)Landroid/content/res/Resources;

    move-result-object v0

    .line 419
    if-eqz p3, :cond_0

    .line 420
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 421
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    sget v2, Lxi;->download_resume:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 422
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, LDR;

    iget-object v2, p0, LDM;->a:LahB;

    iget-object v3, p0, LDM;->a:LvU;

    invoke-direct {v1, v2, p2, v3}, LDR;-><init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v0, v1}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 427
    :goto_0
    return-void

    .line 425
    :cond_0
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(LDT;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 477
    if-eqz p2, :cond_1

    .line 478
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 479
    if-eqz p3, :cond_0

    .line 480
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 392
    invoke-static {p1}, LDT;->a(LDT;)Landroid/content/res/Resources;

    move-result-object v1

    .line 395
    invoke-direct {p0, p1, p2, v0}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 396
    invoke-direct {p0, p1, p2, v0}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 397
    invoke-direct {p0, p1, p2, v0}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 398
    invoke-direct {p0, p1, p2, v0}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 400
    iget-object v2, p0, LDM;->a:LBf;

    invoke-interface {v2}, LBf;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 401
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 402
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    sget v2, Lxi;->pin_downloading:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    const/4 v0, 0x1

    .line 406
    :goto_0
    return v0

    .line 405
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LDM;->a(LDT;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method private b(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 301
    iget-object v0, p0, LDM;->a:LBf;

    invoke-interface {v0}, LBf;->a()Z

    move-result v0

    .line 302
    iget-object v1, p0, LDM;->a:LBf;

    invoke-interface {v1}, LBf;->b()Z

    move-result v1

    .line 304
    iget-object v2, p0, LDM;->a:LBf;

    invoke-interface {v2}, LBf;->a()LBh;

    move-result-object v2

    .line 306
    invoke-static {p1}, LDT;->a(LDT;)Landroid/content/res/Resources;

    move-result-object v3

    .line 307
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 308
    iget-object v4, p0, LDM;->a:LBf;

    invoke-interface {v4}, LBf;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 309
    invoke-direct {p0, p1, p2}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    iput-boolean v0, p0, LDM;->c:Z

    .line 389
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v4, p0, LDM;->a:LBf;

    invoke-interface {v4}, LBf;->a()LBg;

    move-result-object v4

    sget-object v5, LBg;->a:LBg;

    invoke-virtual {v4, v5}, LBg;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 314
    invoke-direct {p0, p1, p2, v6}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 315
    invoke-direct {p0, p1, p2, v7}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 316
    invoke-direct {p0, p1, p2, v6}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 317
    sget v0, Lxi;->pin_update:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, LDM;->a(LDT;ZLjava/lang/String;)V

    .line 318
    invoke-direct {p0, p1, p2, v7}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 319
    iput-boolean v6, p0, LDM;->c:Z

    goto :goto_0

    .line 320
    :cond_1
    if-eqz v0, :cond_5

    .line 321
    sget-object v0, LBh;->e:LBh;

    sget-object v1, LBh;->b:LBh;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 323
    invoke-direct {p0, p1, p2, v7}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 324
    invoke-direct {p0, p1, p2, v6}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 325
    invoke-direct {p0, p1, p2, v7}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 326
    sget-object v0, LDN;->a:[I

    invoke-virtual {v2}, LBh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 335
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "What are we waiting for?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :pswitch_0
    sget v0, Lxi;->pin_waiting:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, LDM;->a(LDT;ZLjava/lang/String;)V

    .line 337
    :goto_1
    invoke-direct {p0, p1, p2, v7}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 368
    :goto_2
    iput-boolean v6, p0, LDM;->c:Z

    goto :goto_0

    .line 331
    :pswitch_1
    sget v0, Lxi;->pin_waiting_for_network:I

    .line 332
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 331
    invoke-direct {p0, p1, v6, v0}, LDM;->a(LDT;ZLjava/lang/String;)V

    goto :goto_1

    .line 338
    :cond_2
    sget-object v0, LBh;->c:LBh;

    invoke-virtual {v0, v2}, LBh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 339
    invoke-direct {p0, p1, p2, v6}, LDM;->b(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 340
    invoke-direct {p0, p1, p2, v7}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 341
    invoke-direct {p0, p1, p2, v6}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 342
    sget v0, Lxi;->upload_waiting_for_wifi:I

    .line 343
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 342
    invoke-direct {p0, p1, v6, v0}, LDM;->a(LDT;ZLjava/lang/String;)V

    .line 344
    invoke-direct {p0, p1, p2, v7}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    goto :goto_2

    .line 345
    :cond_3
    sget-object v0, LBh;->a:LBh;

    sget-object v1, LBh;->d:LBh;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 347
    invoke-direct {p0, p1, p2, v6}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 348
    invoke-direct {p0, p1, p2, v7}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 349
    invoke-direct {p0, p1, p2, v6}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 350
    sget-object v0, LDN;->a:[I

    invoke-virtual {v2}, LBh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 358
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No status set for this upload state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :pswitch_2
    sget v0, Lxi;->pin_update:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, LDM;->a(LDT;ZLjava/lang/String;)V

    .line 360
    :goto_3
    invoke-direct {p0, p1, p2, v7}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    goto :goto_2

    .line 355
    :pswitch_3
    sget v0, Lxi;->pin_paused:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, LDM;->a(LDT;ZLjava/lang/String;)V

    goto :goto_3

    .line 362
    :cond_4
    invoke-direct {p0, p1, p2, v7}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 363
    invoke-direct {p0, p1, p2, v6}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 364
    invoke-direct {p0, p1, p2, v7}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 365
    sget v0, Lxi;->pin_waiting:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, LDM;->a(LDT;ZLjava/lang/String;)V

    .line 366
    invoke-direct {p0, p1, p2, v7}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    goto/16 :goto_2

    .line 369
    :cond_5
    if-eqz v1, :cond_6

    .line 371
    invoke-direct {p0, p1, p2, v7}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 372
    invoke-direct {p0, p1, p2, v6}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 373
    invoke-direct {p0, p1, p2, v7}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 376
    invoke-direct {p0, p1, v6, v8}, LDM;->a(LDT;ZLjava/lang/String;)V

    .line 377
    invoke-direct {p0, p1, p2, v6}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 378
    iput-boolean v6, p0, LDM;->c:Z

    goto/16 :goto_0

    .line 380
    :cond_6
    invoke-direct {p0, p1, p2, v7}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 381
    invoke-direct {p0, p1, p2, v7}, LDM;->d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 382
    invoke-direct {p0, p1, p2, v7}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 383
    invoke-direct {p0, p1, v7, v8}, LDM;->a(LDT;ZLjava/lang/String;)V

    .line 384
    invoke-direct {p0, p1, p2, v7}, LDM;->e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 386
    iput-boolean v7, p0, LDM;->c:Z

    goto/16 :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 350
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 4

    .prologue
    .line 430
    invoke-static {p1}, LDT;->a(LDT;)Landroid/content/res/Resources;

    move-result-object v0

    .line 431
    if-eqz p3, :cond_0

    .line 432
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 433
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    sget v2, Lxi;->download_resume:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 434
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, LDP;

    iget-object v2, p0, LDM;->a:LahB;

    iget-object v3, p0, LDM;->a:LvU;

    invoke-direct {v1, v2, p2, v3}, LDP;-><init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v0, v1}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 439
    :goto_0
    return-void

    .line 437
    :cond_0
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 488
    invoke-static {p1}, LDT;->a(LDT;)Landroid/content/res/Resources;

    move-result-object v1

    .line 489
    iget-object v0, p0, LDM;->a:LBf;

    invoke-interface {v0}, LBf;->a()LBh;

    move-result-object v0

    .line 491
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 492
    sget-object v2, LBh;->a:LBh;

    sget-object v3, LBh;->d:LBh;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 494
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 495
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    sget v3, Lxi;->upload_resume:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 496
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    new-instance v3, LDR;

    iget-object v4, p0, LDM;->a:LahB;

    iget-object v5, p0, LDM;->a:LvU;

    invoke-direct {v3, v4, p2, v5}, LDR;-><init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v2, v3}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 499
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 501
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 502
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    sget v3, Lxi;->upload_remove:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 503
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    new-instance v3, LDW;

    iget-object v4, p0, LDM;->a:LaGM;

    iget-object v5, p0, LDM;->a:LahB;

    invoke-direct {v3, v4, v5, p2}, LDW;-><init>(LaGM;LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-direct {p0, p1, v2, v3}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 507
    sget-object v2, LDN;->a:[I

    invoke-virtual {v0}, LBh;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 517
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No status set for this upload state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 509
    :pswitch_0
    sget v0, Lxi;->upload_incomplete:I

    .line 510
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 519
    :goto_0
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LDM;->c:Z

    .line 600
    :goto_2
    return-void

    .line 513
    :pswitch_1
    sget v0, Lxi;->upload_paused:I

    .line 514
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 524
    :cond_0
    sget-object v2, LBh;->e:LBh;

    sget-object v3, LBh;->b:LBh;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 527
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 529
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 530
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    sget v3, Lxi;->upload_pause:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 531
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    new-instance v3, LDO;

    iget-object v4, p0, LDM;->a:LahB;

    iget-object v5, p0, LDM;->a:LvU;

    invoke-direct {v3, v4, p2, v5}, LDO;-><init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v2, v3}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 534
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 536
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 538
    sget-object v2, LDN;->a:[I

    invoke-virtual {v0}, LBh;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    .line 546
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "What are we waiting for?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 540
    :pswitch_2
    sget v0, Lxi;->upload_waiting:I

    .line 548
    :goto_3
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 549
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 551
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, LDM;->a:Ljava/lang/String;

    invoke-static {p2, v0, v1, v2, v8}, LDQ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/widget/TextView;Landroid/widget/ProgressBar;Ljava/lang/String;LahR;)V

    goto/16 :goto_1

    .line 543
    :pswitch_3
    sget v0, Lxi;->upload_waiting_for_network:I

    goto :goto_3

    .line 554
    :cond_1
    sget-object v2, LBh;->f:LBh;

    invoke-virtual {v0, v2}, LBh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 556
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 558
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 559
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v2, Lxi;->upload_pause:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 560
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, LDO;

    iget-object v2, p0, LDM;->a:LahB;

    iget-object v3, p0, LDM;->a:LvU;

    invoke-direct {v1, v2, p2, v3}, LDO;-><init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v0, v1}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 563
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 565
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 566
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 568
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, LDM;->a:Ljava/lang/String;

    iget-object v3, p0, LDM;->a:LBf;

    .line 569
    invoke-interface {v3}, LBf;->a()LahR;

    move-result-object v3

    .line 568
    invoke-static {p2, v0, v1, v2, v3}, LDQ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/widget/TextView;Landroid/widget/ProgressBar;Ljava/lang/String;LahR;)V

    goto/16 :goto_1

    .line 571
    :cond_2
    sget-object v2, LBh;->c:LBh;

    invoke-virtual {v2, v0}, LBh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 573
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 574
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v2, Lxi;->upload_resume:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 575
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v2, LDP;

    iget-object v3, p0, LDM;->a:LahB;

    iget-object v4, p0, LDM;->a:LvU;

    invoke-direct {v2, v3, p2, v4}, LDP;-><init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v0, v2}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 578
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 580
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 581
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v2, Lxi;->upload_remove:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 582
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v2, LDW;

    iget-object v3, p0, LDM;->a:LaGM;

    iget-object v4, p0, LDM;->a:LahB;

    invoke-direct {v2, v3, v4, p2}, LDW;-><init>(LaGM;LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-direct {p0, p1, v0, v2}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 585
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 586
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    sget v2, Lxi;->upload_waiting_for_wifi:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 589
    :cond_3
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 590
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 591
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 592
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 593
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 595
    iput-boolean v7, p0, LDM;->c:Z

    goto/16 :goto_2

    .line 507
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 538
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 5

    .prologue
    .line 442
    if-eqz p3, :cond_0

    .line 443
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 444
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 445
    invoke-static {p1}, LDT;->a(LDT;)Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxi;->download_remove:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 444
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 446
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, LDV;

    iget-object v2, p0, LDM;->a:LaGM;

    iget-object v3, p0, LDM;->a:LBZ;

    iget-object v4, p0, LDM;->a:LvU;

    invoke-direct {v1, v2, v3, p2, v4}, LDV;-><init>(LaGM;LBZ;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v0, v1}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 451
    :goto_0
    return-void

    .line 449
    :cond_0
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private d(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 4

    .prologue
    .line 454
    invoke-static {p1}, LDT;->a(LDT;)Landroid/content/res/Resources;

    move-result-object v0

    .line 455
    if-eqz p3, :cond_0

    .line 456
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 457
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v1

    sget v2, Lxi;->download_pause:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 458
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, LDO;

    iget-object v2, p0, LDM;->a:LahB;

    iget-object v3, p0, LDM;->a:LvU;

    invoke-direct {v1, v2, p2, v3}, LDO;-><init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V

    invoke-direct {p0, p1, v0, v1}, LDM;->a(LDT;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 463
    :goto_0
    return-void

    .line 461
    :cond_0
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private e(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 4

    .prologue
    .line 466
    if-eqz p3, :cond_0

    .line 467
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 468
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, LDM;->b:Ljava/lang/String;

    iget-object v3, p0, LDM;->a:LBf;

    .line 469
    invoke-interface {v3}, LBf;->a()LahR;

    move-result-object v3

    .line 468
    invoke-static {p2, v0, v1, v2, v3}, LDQ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/widget/TextView;Landroid/widget/ProgressBar;Ljava/lang/String;LahR;)V

    .line 473
    :goto_0
    return-void

    .line 471
    :cond_0
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 618
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    iget-object v0, p0, LDM;->a:LBf;

    invoke-interface {v0}, LBf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LDM;->a:LBf;

    invoke-interface {v0}, LBf;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 625
    :goto_0
    iget-boolean v2, p0, LDM;->b:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, LDM;->a:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 626
    :cond_1
    invoke-direct {p0, p1, p2}, LDM;->b(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 627
    invoke-direct {p0, p1}, LDM;->a(LDT;)V

    .line 641
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 621
    goto :goto_0

    .line 628
    :cond_3
    iget-boolean v0, p0, LDM;->a:Z

    if-eqz v0, :cond_4

    .line 629
    invoke-direct {p0, p1, p2}, LDM;->c(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 630
    invoke-direct {p0, p1}, LDM;->a(LDT;)V

    goto :goto_1

    .line 632
    :cond_4
    invoke-static {p1}, LDT;->b(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 633
    invoke-static {p1}, LDT;->c(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 634
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 635
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 636
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 637
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 638
    invoke-static {p1}, LDT;->a(LDT;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setTag(Ljava/lang/Object;)V

    .line 639
    iput-boolean v1, p0, LDM;->c:Z

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 644
    iget-boolean v0, p0, LDM;->c:Z

    return v0
.end method
