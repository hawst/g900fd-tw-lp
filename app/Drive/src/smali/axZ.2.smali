.class public LaxZ;
.super Ljava/lang/Object;
.source "SwipeySwitcher.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field final synthetic a:Lcom/google/android/common/SwipeySwitcher;


# direct methods
.method public constructor <init>(Lcom/google/android/common/SwipeySwitcher;)V
    .locals 0

    .prologue
    .line 473
    iput-object p1, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 370
    iget-object v2, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v2}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Layb;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v2}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Layb;

    move-result-object v2

    invoke-interface {v2}, Layb;->a()I

    move-result v2

    .line 371
    :goto_0
    if-le v2, v0, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 370
    goto :goto_0

    :cond_1
    move v0, v1

    .line 371
    goto :goto_1
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 375
    iget-object v2, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v2}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Layb;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v2}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Layb;

    move-result-object v2

    invoke-interface {v2}, Layb;->a()I

    move-result v2

    .line 376
    :goto_0
    if-le v2, v0, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 375
    goto :goto_0

    :cond_1
    move v0, v1

    .line 376
    goto :goto_1
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, v1}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;I)I

    .line 366
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 381
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v0}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 417
    :cond_0
    :goto_0
    return v2

    .line 388
    :cond_1
    cmpl-float v0, p3, v3

    if-eqz v0, :cond_0

    .line 392
    cmpl-float v0, p3, v3

    if-lez v0, :cond_2

    invoke-direct {p0}, LaxZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    :cond_2
    cmpg-float v0, p3, v3

    if-gez v0, :cond_3

    invoke-direct {p0}, LaxZ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    :cond_3
    cmpg-float v0, p3, v3

    if-gez v0, :cond_5

    move v0, v1

    .line 404
    :goto_1
    iget-object v3, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v3}, Lcom/google/android/common/SwipeySwitcher;->getScrollX()I

    move-result v3

    iget-object v4, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v4}, Lcom/google/android/common/SwipeySwitcher;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    if-lez v3, :cond_4

    move v2, v1

    .line 406
    :cond_4
    if-eq v0, v2, :cond_6

    .line 407
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v0}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)V

    :goto_2
    move v2, v1

    .line 417
    goto :goto_0

    :cond_5
    move v0, v2

    .line 403
    goto :goto_1

    .line 409
    :cond_6
    if-eqz v0, :cond_7

    .line 410
    iget-object v2, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v2}, Lcom/google/android/common/SwipeySwitcher;->b(Lcom/google/android/common/SwipeySwitcher;)V

    .line 415
    :goto_3
    iget-object v2, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v2, v0}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;Z)V

    goto :goto_2

    .line 412
    :cond_7
    iget-object v2, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v2}, Lcom/google/android/common/SwipeySwitcher;->c(Lcom/google/android/common/SwipeySwitcher;)V

    goto :goto_3
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 422
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 426
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v0}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 467
    :goto_0
    return v0

    .line 430
    :cond_0
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v0}, Lcom/google/android/common/SwipeySwitcher;->getScrollX()I

    move-result v0

    float-to-int v3, p3

    add-int/2addr v0, v3

    .line 432
    iget-object v3, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v3}, Lcom/google/android/common/SwipeySwitcher;->getWidth()I

    move-result v3

    if-ge v0, v3, :cond_4

    invoke-direct {p0}, LaxZ;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 433
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v0}, Lcom/google/android/common/SwipeySwitcher;->getWidth()I

    move-result v0

    .line 438
    :cond_1
    :goto_1
    iget-object v3, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v3, v0, v1}, Lcom/google/android/common/SwipeySwitcher;->scrollTo(II)V

    .line 440
    iget-object v3, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v3}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Layb;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v3}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)Layb;

    move-result-object v3

    invoke-interface {v3}, Layb;->a()I

    move-result v3

    if-ne v3, v5, :cond_3

    .line 457
    iget-object v3, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v3}, Lcom/google/android/common/SwipeySwitcher;->getWidth()I

    move-result v3

    .line 458
    if-ge v0, v3, :cond_5

    iget-object v4, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v4}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)[Layd;

    move-result-object v4

    aget-object v4, v4, v1

    instance-of v4, v4, Layh;

    if-eqz v4, :cond_5

    move v1, v2

    .line 463
    :cond_2
    :goto_2
    if-eqz v1, :cond_3

    .line 464
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v0}, Lcom/google/android/common/SwipeySwitcher;->d(Lcom/google/android/common/SwipeySwitcher;)V

    :cond_3
    move v0, v2

    .line 467
    goto :goto_0

    .line 434
    :cond_4
    iget-object v3, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v3}, Lcom/google/android/common/SwipeySwitcher;->getWidth()I

    move-result v3

    if-le v0, v3, :cond_1

    invoke-direct {p0}, LaxZ;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 435
    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-virtual {v0}, Lcom/google/android/common/SwipeySwitcher;->getWidth()I

    move-result v0

    goto :goto_1

    .line 460
    :cond_5
    if-le v0, v3, :cond_2

    iget-object v0, p0, LaxZ;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-static {v0}, Lcom/google/android/common/SwipeySwitcher;->a(Lcom/google/android/common/SwipeySwitcher;)[Layd;

    move-result-object v0

    aget-object v0, v0, v5

    instance-of v0, v0, Layh;

    if-eqz v0, :cond_2

    move v1, v2

    .line 461
    goto :goto_2
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 471
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    return v0
.end method
