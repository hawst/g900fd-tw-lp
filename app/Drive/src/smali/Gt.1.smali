.class public LGt;
.super Ljava/lang/Object;
.source "GDocDocumentOpener.java"

# interfaces
.implements LFR;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

.field private final a:LtK;

.field private final a:Lye;


# direct methods
.method public constructor <init>(Landroid/content/Context;LtK;Lye;Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LGt;->a:Landroid/content/Context;

    .line 46
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LGt;->a:LtK;

    .line 47
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye;

    iput-object v0, p0, LGt;->a:Lye;

    .line 48
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    iput-object v0, p0, LGt;->a:Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    .line 49
    return-void
.end method

.method private a(LFT;LaGo;Landroid/content/Intent;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/content/Intent;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-interface {p2}, LaGo;->a()LaFM;

    move-result-object v0

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    .line 85
    new-instance v1, LGy;

    iget-object v2, p0, LGt;->a:Landroid/content/Context;

    .line 86
    invoke-virtual {v0}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, p1, v0, p3}, LGy;-><init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;)V

    .line 87
    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LaGo;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, LGt;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/content/Context;LaGu;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    const-string v0, "editMode"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 55
    :goto_0
    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p0, p2, p3}, LGt;->a(LaGo;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_1

    .line 58
    invoke-direct {p0, p1, p2, v0}, LGt;->a(LFT;LaGo;Landroid/content/Intent;)LbsU;

    move-result-object v0

    .line 73
    :goto_1
    return-object v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :cond_1
    iget-object v0, p0, LGt;->a:LtK;

    sget-object v1, Lry;->j:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    invoke-interface {p2}, LaGo;->g()Ljava/lang/String;

    move-result-object v0

    .line 64
    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 65
    iget-object v2, p0, LGt;->a:Lye;

    invoke-interface {v2, v0, v1}, Lye;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_2

    .line 67
    iget-object v1, p0, LGt;->a:Landroid/content/Context;

    sget v2, Lxi;->cross_app_promo_view_only_button_text:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object v2, p0, LGt;->a:Lye;

    invoke-interface {v2, v0, v1}, Lye;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    invoke-direct {p0, p1, p2, v0}, LGt;->a(LFT;LaGo;Landroid/content/Intent;)LbsU;

    move-result-object v0

    goto :goto_1

    .line 73
    :cond_2
    iget-object v0, p0, LGt;->a:Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    goto :goto_1
.end method
