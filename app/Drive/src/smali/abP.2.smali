.class public final LabP;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laby;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laby;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lact;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Labt;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lacj;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LabG;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LacF;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lach;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lacq;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lace;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LabQ;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Labh;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Labw;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lacs;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Labe;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQT;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lacz;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LabF;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Labg;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lacr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 57
    iput-object p1, p0, LabP;->a:LbrA;

    .line 58
    const-class v0, Laby;

    new-instance v1, LbvN;

    const-string v2, ""

    const/4 v3, 0x3

    const-string v4, ""

    invoke-direct {v1, v2, v3, v6, v4}, LbvN;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 59
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 58
    invoke-static {v0, v5}, LabP;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->a:Lbsk;

    .line 61
    const-class v0, Laby;

    new-instance v1, LbvN;

    const-string v2, ""

    const/4 v3, 0x2

    const-string v4, ""

    invoke-direct {v1, v2, v3, v6, v4}, LbvN;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 62
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 61
    invoke-static {v0, v5}, LabP;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->b:Lbsk;

    .line 64
    const-class v0, Lact;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->c:Lbsk;

    .line 67
    const-class v0, Labt;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->d:Lbsk;

    .line 70
    const-class v0, Lacj;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->e:Lbsk;

    .line 73
    const-class v0, LabG;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->f:Lbsk;

    .line 76
    const-class v0, LacF;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->g:Lbsk;

    .line 79
    const-class v0, Lach;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->h:Lbsk;

    .line 82
    const-class v0, Lacq;

    const-class v1, LaiL;

    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->i:Lbsk;

    .line 85
    const-class v0, Lace;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->j:Lbsk;

    .line 88
    const-class v0, LabQ;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->k:Lbsk;

    .line 91
    const-class v0, Labh;

    const-class v1, LaiL;

    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->l:Lbsk;

    .line 94
    const-class v0, Labw;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->m:Lbsk;

    .line 97
    const-class v0, Lacs;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->n:Lbsk;

    .line 100
    const-class v0, Labe;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->o:Lbsk;

    .line 103
    const-class v0, LQT;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->p:Lbsk;

    .line 106
    const-class v0, Lacz;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->q:Lbsk;

    .line 109
    const-class v0, LabF;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->r:Lbsk;

    .line 112
    const-class v0, Labg;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->s:Lbsk;

    .line 115
    const-class v0, Lacr;

    invoke-static {v0, v5}, LabP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LabP;->t:Lbsk;

    .line 118
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 582
    sparse-switch p1, :sswitch_data_0

    .line 706
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 584
    :sswitch_0
    new-instance v0, Lact;

    invoke-direct {v0}, Lact;-><init>()V

    .line 586
    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    .line 587
    invoke-virtual {v1, v0}, LabP;->a(Lact;)V

    .line 704
    :goto_0
    return-object v0

    .line 590
    :sswitch_1
    new-instance v0, Labt;

    invoke-direct {v0}, Labt;-><init>()V

    .line 592
    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    .line 593
    invoke-virtual {v1, v0}, LabP;->a(Labt;)V

    goto :goto_0

    .line 596
    :sswitch_2
    new-instance v1, Lacj;

    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 599
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LabP;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 597
    invoke-static {v0, v2}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Lacj;-><init>(Landroid/content/Context;)V

    .line 604
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 605
    invoke-virtual {v0, v1}, LabP;->a(Lacj;)V

    move-object v0, v1

    .line 606
    goto :goto_0

    .line 608
    :sswitch_3
    new-instance v1, LabG;

    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 611
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LabP;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 609
    invoke-static {v0, v2}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, LabG;-><init>(Laja;)V

    move-object v0, v1

    .line 616
    goto :goto_0

    .line 618
    :sswitch_4
    new-instance v0, LacF;

    invoke-direct {v0}, LacF;-><init>()V

    goto :goto_0

    .line 622
    :sswitch_5
    new-instance v2, Lach;

    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 625
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 623
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 631
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LabP;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 629
    invoke-static {v1, v3}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    invoke-direct {v2, v0, v1}, Lach;-><init>(LtK;LQr;)V

    move-object v0, v2

    .line 636
    goto/16 :goto_0

    .line 638
    :sswitch_6
    new-instance v0, Lacq;

    invoke-direct {v0}, Lacq;-><init>()V

    goto/16 :goto_0

    .line 642
    :sswitch_7
    new-instance v1, Lace;

    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->r:Lbsk;

    .line 645
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LabP;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->r:Lbsk;

    .line 643
    invoke-static {v0, v2}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, Lace;-><init>(Laja;)V

    move-object v0, v1

    .line 650
    goto/16 :goto_0

    .line 652
    :sswitch_8
    new-instance v0, LabQ;

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->k:Lbsk;

    .line 655
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LabP;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->k:Lbsk;

    .line 653
    invoke-static {v1, v2}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTd;

    iget-object v2, p0, LabP;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 661
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LabP;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 659
    invoke-static {v2, v3}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, LabP;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 667
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LabP;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 665
    invoke-static {v3, v4}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LtK;

    iget-object v4, p0, LabP;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LagN;

    iget-object v4, v4, LagN;->E:Lbsk;

    .line 673
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LabP;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LagN;

    iget-object v5, v5, LagN;->E:Lbsk;

    .line 671
    invoke-static {v4, v5}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LagZ;

    iget-object v5, p0, LabP;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LqD;

    iget-object v5, v5, LqD;->c:Lbsk;

    .line 679
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LabP;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LqD;

    iget-object v6, v6, LqD;->c:Lbsk;

    .line 677
    invoke-static {v5, v6}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LqK;

    invoke-direct/range {v0 .. v5}, LabQ;-><init>(LTd;LQr;LtK;LagZ;LqK;)V

    goto/16 :goto_0

    .line 686
    :sswitch_9
    new-instance v0, Labh;

    invoke-direct {v0}, Labh;-><init>()V

    goto/16 :goto_0

    .line 690
    :sswitch_a
    new-instance v2, Labw;

    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lbrx;

    iget-object v0, v0, Lbrx;->f:Lbsk;

    .line 693
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lbrx;

    iget-object v1, v1, Lbrx;->f:Lbsk;

    .line 691
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->a:Lbsk;

    .line 699
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LabP;->a:LbrA;

    iget-object v3, v3, LbrA;->a:La;

    iget-object v3, v3, La;->a:Lbsk;

    .line 697
    invoke-static {v1, v3}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v2, v0, v1}, Labw;-><init>(Ljava/util/Set;Landroid/app/Activity;)V

    move-object v0, v2

    .line 704
    goto/16 :goto_0

    .line 582
    :sswitch_data_0
    .sparse-switch
        0x104 -> :sswitch_5
        0x1b7 -> :sswitch_0
        0x1b8 -> :sswitch_7
        0x1b9 -> :sswitch_1
        0x1bb -> :sswitch_2
        0x1bd -> :sswitch_3
        0x1be -> :sswitch_9
        0x1bf -> :sswitch_4
        0x1c4 -> :sswitch_a
        0x1c7 -> :sswitch_6
        0x1ca -> :sswitch_8
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 793
    sparse-switch p2, :sswitch_data_0

    .line 858
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795
    :sswitch_0
    check-cast p1, LacE;

    .line 797
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->c:Lbsk;

    .line 800
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lact;

    .line 797
    invoke-virtual {p1, v0}, LacE;->provideSharingInfoManager(Lact;)Lacs;

    move-result-object v0

    .line 851
    :goto_0
    return-object v0

    .line 804
    :sswitch_1
    check-cast p1, LacE;

    .line 806
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->j:Lbsk;

    .line 809
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lace;

    .line 806
    invoke-virtual {p1, v0}, LacE;->provideAclManager(Lace;)Labe;

    move-result-object v0

    goto :goto_0

    .line 813
    :sswitch_2
    check-cast p1, LacE;

    .line 815
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->e:Lbsk;

    .line 818
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    .line 815
    invoke-virtual {p1, v0}, LacE;->provideContactSharingConfirmationListener(Lacj;)LQT;

    move-result-object v0

    goto :goto_0

    .line 822
    :sswitch_3
    check-cast p1, LacE;

    .line 824
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->e:Lbsk;

    .line 827
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    .line 824
    invoke-virtual {p1, v0}, LacE;->provideSharingInfoProvider(Lacj;)Lacz;

    move-result-object v0

    goto :goto_0

    .line 831
    :sswitch_4
    check-cast p1, LacE;

    .line 833
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->f:Lbsk;

    .line 836
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabG;

    .line 833
    invoke-virtual {p1, v0}, LacE;->provideContactManager(LabG;)LabF;

    move-result-object v0

    goto :goto_0

    .line 840
    :sswitch_5
    check-cast p1, LacE;

    .line 842
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->e:Lbsk;

    .line 845
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    .line 842
    invoke-virtual {p1, v0}, LacE;->provideAddCollaboratorClient(Lacj;)Labg;

    move-result-object v0

    goto :goto_0

    .line 849
    :sswitch_6
    check-cast p1, LacE;

    .line 851
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 854
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 851
    invoke-virtual {p1, v0}, LacE;->provideSharingInfo(Landroid/content/Context;)Lacr;

    move-result-object v0

    goto :goto_0

    .line 793
    nop

    :sswitch_data_0
    .sparse-switch
        0x2c -> :sswitch_4
        0x1b4 -> :sswitch_0
        0x1b6 -> :sswitch_1
        0x1ba -> :sswitch_2
        0x1bc -> :sswitch_3
        0x1c6 -> :sswitch_5
        0x1cb -> :sswitch_6
    .end sparse-switch
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 478
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x2d

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 481
    const-class v0, Labt;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x2e

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 484
    const-class v0, Lact;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x2f

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 487
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x30

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 490
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x32

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 493
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x35

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 496
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x36

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 499
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x38

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 502
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x31

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 505
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x33

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 508
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x37

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 511
    const-class v0, Lacj;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x34

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 514
    const-class v0, Laby;

    new-instance v1, LbvN;

    const-string v2, ""

    const/4 v3, 0x3

    const-string v4, ""

    invoke-direct {v1, v2, v3, v5, v4}, LbvN;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LabP;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 515
    const-class v0, Laby;

    new-instance v1, LbvN;

    const-string v2, ""

    const/4 v3, 0x2

    const-string v4, ""

    invoke-direct {v1, v2, v3, v5, v4}, LbvN;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LabP;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 516
    const-class v0, Lact;

    iget-object v1, p0, LabP;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 517
    const-class v0, Labt;

    iget-object v1, p0, LabP;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 518
    const-class v0, Lacj;

    iget-object v1, p0, LabP;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 519
    const-class v0, LabG;

    iget-object v1, p0, LabP;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 520
    const-class v0, LacF;

    iget-object v1, p0, LabP;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 521
    const-class v0, Lach;

    iget-object v1, p0, LabP;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 522
    const-class v0, Lacq;

    iget-object v1, p0, LabP;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 523
    const-class v0, Lace;

    iget-object v1, p0, LabP;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 524
    const-class v0, LabQ;

    iget-object v1, p0, LabP;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 525
    const-class v0, Labh;

    iget-object v1, p0, LabP;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 526
    const-class v0, Labw;

    iget-object v1, p0, LabP;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 527
    const-class v0, Lacs;

    iget-object v1, p0, LabP;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 528
    const-class v0, Labe;

    iget-object v1, p0, LabP;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 529
    const-class v0, LQT;

    iget-object v1, p0, LabP;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 530
    const-class v0, Lacz;

    iget-object v1, p0, LabP;->q:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 531
    const-class v0, LabF;

    iget-object v1, p0, LabP;->r:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 532
    const-class v0, Labg;

    iget-object v1, p0, LabP;->s:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 533
    const-class v0, Lacr;

    iget-object v1, p0, LabP;->t:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 534
    iget-object v0, p0, LabP;->a:Lbsk;

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LOs;

    iget-object v1, v1, LOs;->e:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 536
    iget-object v0, p0, LabP;->b:Lbsk;

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->d:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 538
    iget-object v0, p0, LabP;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1b7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 540
    iget-object v0, p0, LabP;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1b9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 542
    iget-object v0, p0, LabP;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1bb

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 544
    iget-object v0, p0, LabP;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1bd

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 546
    iget-object v0, p0, LabP;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1bf

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 548
    iget-object v0, p0, LabP;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x104

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 550
    iget-object v0, p0, LabP;->i:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1c7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 552
    iget-object v0, p0, LabP;->j:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1b8

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 554
    iget-object v0, p0, LabP;->k:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1ca

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 556
    iget-object v0, p0, LabP;->l:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1be

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 558
    iget-object v0, p0, LabP;->m:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1c4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 560
    iget-object v0, p0, LabP;->n:Lbsk;

    const-class v1, LacE;

    const/16 v2, 0x1b4

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 562
    iget-object v0, p0, LabP;->o:Lbsk;

    const-class v1, LacE;

    const/16 v2, 0x1b6

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 564
    iget-object v0, p0, LabP;->p:Lbsk;

    const-class v1, LacE;

    const/16 v2, 0x1ba

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 566
    iget-object v0, p0, LabP;->q:Lbsk;

    const-class v1, LacE;

    const/16 v2, 0x1bc

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 568
    iget-object v0, p0, LabP;->r:Lbsk;

    const-class v1, LacE;

    const/16 v2, 0x2c

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 570
    iget-object v0, p0, LabP;->s:Lbsk;

    const-class v1, LacE;

    const/16 v2, 0x1c6

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 572
    iget-object v0, p0, LabP;->t:Lbsk;

    const-class v1, LacE;

    const/16 v2, 0x1cb

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 574
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 713
    packed-switch p1, :pswitch_data_0

    .line 787
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 715
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    .line 717
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 718
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 789
    :goto_0
    return-void

    .line 721
    :pswitch_1
    check-cast p2, Labt;

    .line 723
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 724
    invoke-virtual {v0, p2}, LabP;->a(Labt;)V

    goto :goto_0

    .line 727
    :pswitch_2
    check-cast p2, Lact;

    .line 729
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 730
    invoke-virtual {v0, p2}, LabP;->a(Lact;)V

    goto :goto_0

    .line 733
    :pswitch_3
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;

    .line 735
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 736
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;)V

    goto :goto_0

    .line 739
    :pswitch_4
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;

    .line 741
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 742
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;)V

    goto :goto_0

    .line 745
    :pswitch_5
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;

    .line 747
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 748
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;)V

    goto :goto_0

    .line 751
    :pswitch_6
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;

    .line 753
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 754
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;)V

    goto :goto_0

    .line 757
    :pswitch_7
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    .line 759
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 760
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    goto :goto_0

    .line 763
    :pswitch_8
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;

    .line 765
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 766
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;)V

    goto :goto_0

    .line 769
    :pswitch_9
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;

    .line 771
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 772
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;)V

    goto :goto_0

    .line 775
    :pswitch_a
    check-cast p2, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;

    .line 777
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 778
    invoke-virtual {v0, p2}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;)V

    goto :goto_0

    .line 781
    :pswitch_b
    check-cast p2, Lacj;

    .line 783
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 784
    invoke-virtual {v0, p2}, LabP;->a(Lacj;)V

    goto :goto_0

    .line 713
    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_8
        :pswitch_4
        :pswitch_9
        :pswitch_b
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_7
    .end packed-switch
.end method

.method public a(Labt;)V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->z:Lbsk;

    .line 168
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->z:Lbsk;

    .line 166
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Labt;->a:Laja;

    .line 172
    return-void
.end method

.method public a(Lacj;)V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->n:Lbsk;

    .line 406
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->n:Lbsk;

    .line 404
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacs;

    iput-object v0, p1, Lacj;->a:Lacs;

    .line 410
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 413
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 411
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lacj;->a:Lald;

    .line 417
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 420
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 418
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lacj;->a:LaGM;

    .line 424
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->k:Lbsk;

    .line 427
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->k:Lbsk;

    .line 425
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabQ;

    iput-object v0, p1, Lacj;->a:LabQ;

    .line 431
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->O:Lbsk;

    .line 434
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->O:Lbsk;

    .line 432
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajO;

    iput-object v0, p1, Lacj;->a:LajO;

    .line 438
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->i:Lbsk;

    .line 441
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->i:Lbsk;

    .line 439
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacq;

    iput-object v0, p1, Lacj;->a:Lacq;

    .line 445
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 448
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 446
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lacj;->a:LtK;

    .line 452
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 455
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 453
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, Lacj;->a:LaKR;

    .line 459
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lak;

    iget-object v0, v0, Lak;->a:Lbsk;

    .line 462
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lak;

    iget-object v1, v1, Lak;->a:Lbsk;

    .line 460
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM;

    iput-object v0, p1, Lacj;->a:LM;

    .line 466
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 469
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 467
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, Lacj;->a:LaGR;

    .line 473
    return-void
.end method

.method public a(Lact;)V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->o:Lbsk;

    .line 179
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->o:Lbsk;

    .line 177
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labe;

    iput-object v0, p1, Lact;->a:Labe;

    .line 183
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 186
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 184
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, Lact;->a:LaKR;

    .line 190
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 193
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 191
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, Lact;->a:Landroid/content/Context;

    .line 197
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->r:Lbsk;

    .line 200
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->r:Lbsk;

    .line 198
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabF;

    iput-object v0, p1, Lact;->a:LabF;

    .line 204
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 221
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 222
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 225
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 223
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lald;

    .line 229
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lak;

    iget-object v0, v0, Lak;->a:Lbsk;

    .line 232
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lak;

    iget-object v1, v1, Lak;->a:Lbsk;

    .line 230
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:LM;

    .line 236
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->l:Lbsk;

    .line 239
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->l:Lbsk;

    .line 237
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labh;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labh;

    .line 243
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 246
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 244
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:LaKR;

    .line 250
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->e:Lbsk;

    .line 253
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->e:Lbsk;

    .line 251
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    .line 257
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->j:Lbsk;

    .line 260
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->j:Lbsk;

    .line 258
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:LvU;

    .line 264
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 275
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 276
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->i:Lbsk;

    .line 279
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->i:Lbsk;

    .line 277
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->c:Laja;

    .line 283
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->m:Lbsk;

    .line 286
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->m:Lbsk;

    .line 284
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labw;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Labw;

    .line 290
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->q:Lbsk;

    .line 293
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->q:Lbsk;

    .line 291
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacz;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacz;

    .line 297
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 300
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 298
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Laja;

    .line 304
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 307
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 305
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LtK;

    .line 311
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 314
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 312
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LQr;

    .line 318
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 321
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 319
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:LqK;

    .line 325
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->g:Lbsk;

    .line 328
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->g:Lbsk;

    .line 326
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacF;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LacF;

    .line 332
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->O:Lbsk;

    .line 335
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->O:Lbsk;

    .line 333
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajO;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LajO;

    .line 339
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 342
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 340
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LaGM;

    .line 346
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->ao:Lbsk;

    .line 349
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->ao:Lbsk;

    .line 347
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:Laja;

    .line 353
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->r:Lbsk;

    .line 356
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->r:Lbsk;

    .line 354
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabF;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LabF;

    .line 360
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 215
    invoke-virtual {v0, p1}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;)V

    .line 216
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;)V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 385
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 386
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->m:Lbsk;

    .line 389
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->m:Lbsk;

    .line 387
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labw;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a:Labw;

    .line 393
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;)V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 365
    invoke-virtual {v0, p1}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;)V

    .line 366
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->ai:Lbsk;

    .line 369
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->ai:Lbsk;

    .line 367
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b:Laja;

    .line 373
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 376
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 374
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b:LqK;

    .line 380
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 209
    invoke-virtual {v0, p1}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;)V

    .line 210
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 269
    invoke-virtual {v0, p1}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;)V

    .line 270
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 125
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 126
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->O:Lbsk;

    .line 129
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->O:Lbsk;

    .line 127
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajO;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LajO;

    .line 133
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 136
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 134
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LaGM;

    .line 140
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->n:Lbsk;

    .line 143
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->n:Lbsk;

    .line 141
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacs;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacs;

    .line 147
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 150
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 148
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lald;

    .line 154
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 157
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LabP;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 155
    invoke-static {v0, v1}, LabP;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LtK;

    .line 161
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;)V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, LabP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 398
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 399
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 578
    return-void
.end method
