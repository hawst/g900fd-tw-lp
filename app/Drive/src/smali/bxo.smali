.class public Lbxo;
.super Lbxm;
.source "SafeXmlPullParser.java"


# direct methods
.method public constructor <init>(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lbxm;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 39
    return-void
.end method


# virtual methods
.method public getEventType()I
    .locals 3

    .prologue
    .line 44
    :try_start_0
    invoke-super {p0}, Lbxm;->getEventType()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "ArrayIndexOutOfBoundsException encountered while parsing"

    invoke-direct {v1, v2, p0, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public next()I
    .locals 3

    .prologue
    .line 53
    :try_start_0
    invoke-super {p0}, Lbxm;->next()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "ArrayIndexOutOfBoundsException encountered while parsing"

    invoke-direct {v1, v2, p0, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public nextTag()I
    .locals 3

    .prologue
    .line 90
    :try_start_0
    invoke-super {p0}, Lbxm;->nextTag()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "ArrayIndexOutOfBoundsException encountered while parsing"

    invoke-direct {v1, v2, p0, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public nextText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    :try_start_0
    invoke-super {p0}, Lbxm;->nextText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "ArrayIndexOutOfBoundsException encountered while parsing"

    invoke-direct {v1, v2, p0, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public nextToken()I
    .locals 3

    .prologue
    .line 62
    :try_start_0
    invoke-super {p0}, Lbxm;->nextToken()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "ArrayIndexOutOfBoundsException encountered while parsing"

    invoke-direct {v1, v2, p0, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public require(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 72
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lbxm;->require(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "ArrayIndexOutOfBoundsException encountered while parsing"

    invoke-direct {v1, v2, p0, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    throw v1
.end method
