.class LawR;
.super Ljava/lang/Object;
.source "PageIndicator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:LawP;


# direct methods
.method constructor <init>(LawP;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, LawR;->a:LawP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 60
    iget-object v1, p0, LawR;->a:LawP;

    invoke-static {v1}, LawP;->a(LawP;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 61
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, LawR;->a:LawP;

    invoke-static {v0}, LawP;->a(LawP;)V

    .line 64
    :cond_0
    return-void
.end method
