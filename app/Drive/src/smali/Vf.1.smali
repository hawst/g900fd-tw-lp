.class public LVf;
.super Ljava/lang/Object;
.source "BatchOperation.java"


# instance fields
.field private final a:LVA;

.field private final a:LVs;

.field private final a:LVv;

.field private final a:LVx;

.field private final a:LaFM;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LVt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LVs;LVx;LVv;LVA;LaFM;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LVf;->a:Ljava/util/List;

    .line 36
    iput-object p1, p0, LVf;->a:LVs;

    .line 37
    iput-object p2, p0, LVf;->a:LVx;

    .line 38
    iput-object p3, p0, LVf;->a:LVv;

    .line 39
    iput-object p4, p0, LVf;->a:LVA;

    .line 40
    iput-object p5, p0, LVf;->a:LaFM;

    .line 41
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    iget-object v1, p0, LVf;->a:LaFM;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 109
    return-void
.end method


# virtual methods
.method public a()LVd;
    .locals 4

    .prologue
    .line 103
    new-instance v0, LVd;

    iget-object v1, p0, LVf;->a:LaFM;

    iget-object v2, p0, LVf;->a:Ljava/util/List;

    invoke-static {v2}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LVd;-><init>(LaFM;LbmF;LVe;)V

    .line 104
    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0, p1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 58
    iget-object v0, p0, LVf;->a:LVv;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LVv;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Z)LVu;

    move-result-object v0

    .line 59
    iget-object v1, p0, LVf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    return-object p0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "LVf;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 46
    invoke-direct {p0, p3}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 47
    invoke-virtual {p2, p3}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 48
    invoke-virtual {p2}, LbmY;->a()Lbqv;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 49
    invoke-direct {p0, v0}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_1

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, LVf;->a:LVs;

    invoke-virtual {v0, p1, p2, p3}, LVs;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;Lcom/google/android/gms/drive/database/data/EntrySpec;)LVr;

    move-result-object v0

    .line 52
    iget-object v1, p0, LVf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    return-object p0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0, p1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 80
    if-eqz p2, :cond_0

    .line 81
    invoke-direct {p0, p2}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 83
    :cond_0
    iget-object v0, p0, LVf;->a:LVx;

    invoke-virtual {v0, p1, p2}, LVx;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)LVw;

    move-result-object v0

    .line 84
    iget-object v1, p0, LVf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    return-object p0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0, p1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 65
    iget-object v0, p0, LVf;->a:LVv;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LVv;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Z)LVu;

    move-result-object v0

    .line 66
    iget-object v1, p0, LVf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    return-object p0
.end method

.method public c(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0, p1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 90
    iget-object v0, p0, LVf;->a:LVA;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LVA;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Z)LVy;

    move-result-object v0

    .line 91
    iget-object v1, p0, LVf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    return-object p0
.end method

.method public d(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0, p1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 97
    iget-object v0, p0, LVf;->a:LVA;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LVA;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Z)LVy;

    move-result-object v0

    .line 98
    iget-object v1, p0, LVf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    return-object p0
.end method
