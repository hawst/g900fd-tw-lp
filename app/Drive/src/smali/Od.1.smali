.class public LOd;
.super Landroid/os/AsyncTask;
.source "VideoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, LOd;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    iput-object p2, p0, LOd;->a:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 318
    const-string v0, "VideoPlayerActivity"

    const-string v1, "Attempting to load subtitles."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget-object v0, p0, LOd;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOf;

    iget-object v1, p0, LOd;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    .line 320
    invoke-static {v1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    iget-object v2, p0, LOd;->a:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 319
    invoke-virtual {v0, v1, v2, v3}, LOf;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 325
    if-eqz p1, :cond_0

    iget-object v0, p0, LOd;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    const-string v0, "VideoPlayerActivity"

    const-string v1, "Found a stream. Attempting to add as vtt."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v0, p0, LOd;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Landroid/widget/VideoView;

    move-result-object v0

    const-string v1, "text/vtt"

    .line 328
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 327
    invoke-static {v1, v2}, Landroid/media/MediaFormat;->createSubtitleFormat(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaFormat;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/widget/VideoView;->addSubtitleSource(Ljava/io/InputStream;Landroid/media/MediaFormat;)V

    .line 330
    :cond_0
    iget-object v0, p0, LOd;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->invalidateOptionsMenu()V

    .line 331
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 315
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LOd;->a([Ljava/lang/Void;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 315
    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1}, LOd;->a(Ljava/io/InputStream;)V

    return-void
.end method
