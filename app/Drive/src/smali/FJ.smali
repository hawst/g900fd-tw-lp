.class public final LFJ;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFF;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 39
    iput-object p1, p0, LFJ;->a:LbrA;

    .line 40
    const-class v0, LFF;

    invoke-static {v0, v1}, LFJ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFJ;->a:Lbsk;

    .line 43
    const-class v0, LFG;

    invoke-static {v0, v1}, LFJ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFJ;->b:Lbsk;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 67
    packed-switch p1, :pswitch_data_0

    .line 103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :pswitch_0
    new-instance v0, LFG;

    iget-object v1, p0, LFJ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->a:Lbsk;

    .line 72
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LFJ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:La;

    iget-object v2, v2, La;->a:Lbsk;

    .line 70
    invoke-static {v1, v2}, LFJ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, LFJ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->j:Lbsk;

    .line 78
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LFJ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->j:Lbsk;

    .line 76
    invoke-static {v2, v3}, LFJ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaFO;

    iget-object v3, p0, LFJ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lwc;

    iget-object v3, v3, Lwc;->g:Lbsk;

    .line 84
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LFJ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lwc;

    iget-object v4, v4, Lwc;->g:Lbsk;

    .line 82
    invoke-static {v3, v4}, LFJ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwm;

    iget-object v4, p0, LFJ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->X:Lbsk;

    .line 90
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LFJ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LCw;

    iget-object v5, v5, LCw;->X:Lbsk;

    .line 88
    invoke-static {v4, v5}, LFJ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LsI;

    iget-object v5, p0, LFJ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->f:Lbsk;

    .line 96
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LFJ;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaGH;

    iget-object v6, v6, LaGH;->f:Lbsk;

    .line 94
    invoke-static {v5, v6}, LFJ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaGR;

    invoke-direct/range {v0 .. v5}, LFG;-><init>(Landroid/app/Activity;LaFO;Lwm;LsI;LaGR;)V

    .line 101
    return-object v0

    .line 67
    :pswitch_data_0
    .packed-switch 0x222
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 118
    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 53
    const-class v0, LFF;

    iget-object v1, p0, LFJ;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LFJ;->a(Ljava/lang/Class;Lbsk;)V

    .line 54
    const-class v0, LFG;

    iget-object v1, p0, LFJ;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LFJ;->a(Ljava/lang/Class;Lbsk;)V

    .line 55
    iget-object v0, p0, LFJ;->a:Lbsk;

    iget-object v1, p0, LFJ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LFJ;

    iget-object v1, v1, LFJ;->b:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 57
    iget-object v0, p0, LFJ;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x222

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 59
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 110
    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method
