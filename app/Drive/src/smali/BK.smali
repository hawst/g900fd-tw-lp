.class public LBK;
.super LzK;
.source "DocListViewGridArrangementModeDelegate.java"


# instance fields
.field private final a:I

.field private final a:LBM;

.field private final a:LBa;

.field private a:LHu;

.field private final a:LHz;

.field private final a:Landroid/support/v4/app/Fragment;

.field private final a:LzZ;

.field private final b:LBM;


# direct methods
.method private constructor <init>(LHz;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;ILarF;LzZ;LBM;LBM;LBa;)V
    .locals 9

    .prologue
    .line 209
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, LzK;-><init>(LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;LarF;)V

    .line 216
    if-lez p9, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->a(Z)V

    .line 218
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LHz;

    iput-object v1, p0, LBK;->a:LHz;

    .line 219
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    iput-object v1, p0, LBK;->a:Landroid/support/v4/app/Fragment;

    .line 220
    move/from16 v0, p9

    iput v0, p0, LBK;->a:I

    .line 221
    move-object/from16 v0, p11

    iput-object v0, p0, LBK;->a:LzZ;

    .line 222
    move-object/from16 v0, p13

    iput-object v0, p0, LBK;->a:LBM;

    .line 223
    move-object/from16 v0, p12

    iput-object v0, p0, LBK;->b:LBM;

    .line 224
    move-object/from16 v0, p14

    iput-object v0, p0, LBK;->a:LBa;

    .line 225
    return-void

    .line 216
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(LHz;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;ILarF;LzZ;LBM;LBM;LBa;LBL;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct/range {p0 .. p14}, LBK;-><init>(LHz;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;ILarF;LzZ;LBM;LBM;LBa;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, LBK;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 295
    iget-object v1, p0, LBK;->a:LzZ;

    invoke-interface {v1, v0}, LzZ;->a(I)I

    move-result v0

    .line 296
    return v0
.end method

.method protected a()LzZ;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, LBK;->a:LzZ;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, LBK;->a:LzZ;

    invoke-interface {v0, p1}, LzZ;->b(I)I

    move-result v0

    .line 302
    iget-object v1, p0, LBK;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 303
    return-void
.end method

.method public a(LQX;)V
    .locals 7

    .prologue
    .line 229
    invoke-super {p0, p1}, LzK;->a(LQX;)V

    .line 230
    iget-object v0, p0, LBK;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 231
    iget-object v0, p0, LBK;->a:LHu;

    if-nez v0, :cond_2

    .line 232
    new-instance v5, LzY;

    new-instance v0, LCG;

    iget-object v1, p0, LBK;->a:Landroid/widget/ListView;

    invoke-direct {v0, v1}, LCG;-><init>(Landroid/widget/ListView;)V

    iget-object v1, p0, LBK;->a:LzZ;

    invoke-direct {v5, v0, v1}, LzY;-><init>(LzN;LzZ;)V

    .line 234
    new-instance v4, LzX;

    new-instance v0, LCH;

    iget-object v1, p0, LBK;->a:Landroid/widget/ListView;

    invoke-direct {v0, v1}, LCH;-><init>(Landroid/widget/ListView;)V

    iget-object v1, p0, LBK;->a:LzZ;

    invoke-direct {v4, v0, v1}, LzX;-><init>(LHp;LzZ;)V

    .line 238
    new-instance v6, LBN;

    iget-object v0, p0, LBK;->a:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, LBK;->a:LBM;

    invoke-direct {v6, v0, p1, v1}, LBN;-><init>(Landroid/support/v4/app/Fragment;LQX;LBM;)V

    .line 241
    iget-object v0, p0, LBK;->a:LtK;

    sget-object v1, Lry;->ax:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LBK;->b:LBM;

    if-eqz v0, :cond_0

    .line 243
    new-instance v0, LBN;

    iget-object v1, p0, LBK;->a:Landroid/support/v4/app/Fragment;

    iget-object v2, p0, LBK;->b:LBM;

    invoke-direct {v0, v1, p1, v2}, LBN;-><init>(Landroid/support/v4/app/Fragment;LQX;LBM;)V

    .line 247
    invoke-static {v6, v0}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    .line 248
    new-instance v6, LHU;

    invoke-direct {v6, v0}, LHU;-><init>(Ljava/util/List;)V

    .line 251
    :cond_0
    iget-object v0, p0, LBK;->a:LHz;

    iget-object v2, p0, LBK;->a:Landroid/widget/ListView;

    iget v3, p0, LBK;->a:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LHz;->a(LQX;Landroid/widget/ListView;ILHp;LzN;LHy;)LHu;

    move-result-object v0

    iput-object v0, p0, LBK;->a:LHu;

    .line 258
    iget-object v0, p0, LBK;->a:LzZ;

    check-cast v0, LAa;

    iget-object v1, p0, LBK;->a:LHu;

    iget-object v2, p0, LBK;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, v1, v2}, LAa;->a(LAD;LapF;)V

    .line 260
    iget-object v0, p0, LBK;->a:LBa;

    iget-object v1, p0, LBK;->a:LHu;

    invoke-virtual {v0, v1}, LBa;->a(LAD;)V

    .line 261
    iget-object v0, p0, LBK;->a:LzZ;

    invoke-interface {v0, p1}, LzZ;->a(LQX;)V

    .line 266
    :goto_0
    iget-object v0, p0, LBK;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, LBK;->a:LzZ;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 268
    iget-object v0, p0, LBK;->a:LzZ;

    invoke-virtual {p0, p1, v0}, LBK;->a(LQX;LCq;)V

    .line 270
    iget-object v0, p0, LBK;->a:LzZ;

    iget-object v1, p0, LBK;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 271
    iget-object v0, p0, LBK;->a:Landroid/widget/ListView;

    iget-object v1, p0, LBK;->a:LzZ;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 274
    :cond_1
    iget-object v0, p0, LBK;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->c()V

    .line 275
    return-void

    .line 263
    :cond_2
    iget-object v0, p0, LBK;->a:LBa;

    iget-object v1, p0, LBK;->a:LHu;

    invoke-virtual {v0, v1}, LBa;->a(LAD;)V

    .line 264
    iget-object v0, p0, LBK;->a:LzZ;

    invoke-interface {v0, p1}, LzZ;->a(LQX;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, LBK;->a:LzZ;

    invoke-interface {v0, p1}, LzZ;->c(I)I

    move-result v0

    .line 308
    iget-object v1, p0, LBK;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 309
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, LBK;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, LBK;->a:LzZ;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->b(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 280
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 290
    return-void
.end method
