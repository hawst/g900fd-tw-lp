.class LLM;
.super LLQ;
.source "SelectionFunctionDoShare.java"


# instance fields
.field private final a:LM;

.field private final a:LaKR;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LaKR;LM;LtK;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, LLQ;-><init>()V

    .line 30
    iput-object p1, p0, LLM;->a:LaKR;

    .line 31
    iput-object p2, p0, LLM;->a:LM;

    .line 32
    iput-object p3, p0, LLM;->a:LtK;

    .line 33
    return-void
.end method

.method private a(LaGu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 55
    iget-object v1, p0, LLM;->a:LtK;

    sget-object v2, Lry;->F:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    invoke-virtual {v1}, LaGv;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {p3}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 50
    iget-object v1, p0, LLM;->a:LM;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 51
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 52
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-super {p0, p1, p2}, LLQ;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    :goto_0
    return v1

    .line 41
    :cond_0
    invoke-virtual {p1, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 42
    invoke-interface {v0}, LaGu;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LLM;->a:LaKR;

    invoke-interface {v2}, LaKR;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, LaGu;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 43
    invoke-direct {p0, v0}, LLM;->a(LaGu;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
