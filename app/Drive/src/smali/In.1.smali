.class public LIn;
.super Ljava/lang/Object;
.source "FastScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field final synthetic a:LIk;

.field b:J


# direct methods
.method public constructor <init>(LIk;)V
    .locals 0

    .prologue
    .line 603
    iput-object p1, p0, LIn;->a:LIk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()I
    .locals 8

    .prologue
    const-wide/16 v6, 0xd0

    .line 617
    iget-object v0, p0, LIn;->a:LIk;

    invoke-virtual {v0}, LIk;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 618
    const/16 v0, 0xd0

    .line 627
    :goto_0
    return v0

    .line 621
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 622
    iget-wide v2, p0, LIn;->a:J

    iget-wide v4, p0, LIn;->b:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 623
    const/4 v0, 0x0

    goto :goto_0

    .line 625
    :cond_1
    iget-wide v2, p0, LIn;->a:J

    sub-long/2addr v0, v2

    mul-long/2addr v0, v6

    iget-wide v2, p0, LIn;->b:J

    div-long/2addr v0, v2

    sub-long v0, v6, v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method a()V
    .locals 2

    .prologue
    .line 611
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, LIn;->b:J

    .line 612
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LIn;->a:J

    .line 613
    iget-object v0, p0, LIn;->a:LIk;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LIk;->a(I)V

    .line 614
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 632
    iget-object v0, p0, LIn;->a:LIk;

    invoke-virtual {v0}, LIk;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 633
    invoke-virtual {p0}, LIn;->a()V

    .line 642
    :goto_0
    return-void

    .line 637
    :cond_0
    invoke-virtual {p0}, LIn;->a()I

    move-result v0

    if-lez v0, :cond_1

    .line 638
    iget-object v0, p0, LIn;->a:LIk;

    invoke-static {v0}, LIk;->a(LIk;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 640
    :cond_1
    iget-object v0, p0, LIn;->a:LIk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LIk;->a(I)V

    goto :goto_0
.end method
