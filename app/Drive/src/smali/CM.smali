.class public LCM;
.super Ljava/lang/Object;
.source "MenuItemsState.java"


# instance fields
.field private final a:LWL;

.field private final a:LaGM;

.field private final a:LaKR;

.field private final a:Lach;

.field private final a:LsC;

.field private final a:LtB;

.field private final a:LtK;

.field a:Lvq;

.field private final a:Lwa;


# direct methods
.method public constructor <init>(LaGM;LtK;LaKR;Lwa;LsC;LtB;Lach;LWL;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-object p1, p0, LCM;->a:LaGM;

    .line 150
    iput-object p2, p0, LCM;->a:LtK;

    .line 151
    iput-object p3, p0, LCM;->a:LaKR;

    .line 152
    iput-object p4, p0, LCM;->a:Lwa;

    .line 153
    iput-object p5, p0, LCM;->a:LsC;

    .line 154
    iput-object p6, p0, LCM;->a:LtB;

    .line 155
    iput-object p7, p0, LCM;->a:Lach;

    .line 156
    iput-object p8, p0, LCM;->a:LWL;

    .line 157
    return-void
.end method


# virtual methods
.method public a(LaGu;)LCI;
    .locals 1

    .prologue
    .line 161
    invoke-static {}, LamV;->a()V

    .line 173
    iget-object v0, p0, LCM;->a:Lwa;

    invoke-interface {v0}, Lwa;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 174
    :goto_0
    invoke-virtual {p0, p1, v0}, LCM;->a(LaGu;Z)LCI;

    move-result-object v0

    return-object v0

    .line 173
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGu;Z)LCI;
    .locals 19

    .prologue
    .line 178
    .line 180
    if-nez p1, :cond_0

    .line 181
    invoke-static {}, LCI;->a()LCI;

    move-result-object v1

    .line 213
    :goto_0
    return-object v1

    .line 183
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v2

    .line 184
    invoke-interface/range {p1 .. p1}, LaGu;->h()Z

    move-result v3

    .line 185
    invoke-interface/range {p1 .. p1}, LaGu;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v4, 0x1

    .line 188
    :goto_1
    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->d:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-interface/range {p1 .. p1}, LaGu;->a()LaGv;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v5

    .line 189
    move-object/from16 v0, p1

    instance-of v1, v0, LaGo;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v6, v0, LCM;->a:LaGM;

    move-object/from16 v1, p1

    check-cast v1, LaGo;

    .line 190
    invoke-interface {v6, v1, v5}, LaGM;->b(LaGo;LacY;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v5, 0x1

    .line 191
    :goto_2
    invoke-interface/range {p1 .. p1}, LaGu;->g()Z

    move-result v6

    .line 192
    invoke-interface/range {p1 .. p1}, LaGu;->f()Z

    move-result v7

    .line 193
    invoke-interface/range {p1 .. p1}, LaGu;->e()Z

    move-result v8

    .line 194
    invoke-interface/range {p1 .. p1}, LaGu;->b()Z

    move-result v13

    .line 195
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:Lach;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lach;->a(LaGu;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v9, 0x1

    .line 197
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LWL;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, LWL;->a(LaGu;)Z

    move-result v12

    .line 198
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LtK;

    sget-object v10, Lry;->T:Lry;

    invoke-interface {v1, v10}, LtK;->a(LtJ;)Z

    move-result v10

    .line 199
    invoke-interface/range {p1 .. p1}, LaGu;->a()LaGv;

    move-result-object v1

    sget-object v11, LaGv;->a:LaGv;

    invoke-virtual {v1, v11}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 200
    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LtK;

    sget-object v14, Lry;->A:Lry;

    .line 201
    invoke-interface {v1, v14}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v14, 0x1

    .line 202
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LtK;

    sget-object v15, Lry;->i:Lry;

    .line 203
    invoke-interface {v1, v15}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:Lvq;

    if-eqz v1, :cond_5

    const/4 v15, 0x1

    .line 208
    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LtK;

    sget-object v16, Lry;->aS:Lry;

    .line 209
    move-object/from16 v0, v16

    invoke-interface {v1, v0}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 210
    invoke-interface/range {p1 .. p1}, LaGu;->a()LaGv;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v0, v0, LCM;->a:LtB;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, LtB;->a()LaGv;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v17, 0x1

    .line 211
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LtK;

    sget-object v16, Lry;->af:Lry;

    move-object/from16 v0, v16

    invoke-interface {v1, v0}, LtK;->a(LtJ;)Z

    move-result v18

    .line 213
    move-object/from16 v0, p0

    iget-object v1, v0, LCM;->a:LsC;

    .line 214
    invoke-interface {v1}, LsC;->a()Ljava/util/Set;

    move-result-object v1

    move/from16 v16, p2

    .line 213
    invoke-static/range {v1 .. v18}, LCI;->a(Ljava/util/Set;ZZZZZZZZZZZZZZZZZ)LCI;

    move-result-object v1

    goto/16 :goto_0

    .line 185
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 190
    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 195
    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 201
    :cond_4
    const/4 v14, 0x0

    goto :goto_4

    .line 203
    :cond_5
    const/4 v15, 0x0

    goto :goto_5

    .line 210
    :cond_6
    const/16 v17, 0x0

    goto :goto_6
.end method
