.class public final enum Lagg;
.super Ljava/lang/Enum;
.source "ContentSyncStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lagg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lagg;

.field private static final synthetic a:[Lagg;

.field public static final enum b:Lagg;

.field public static final enum c:Lagg;

.field public static final enum d:Lagg;

.field public static final enum e:Lagg;

.field public static final enum f:Lagg;

.field public static final enum g:Lagg;

.field public static final enum h:Lagg;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lagg;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v3}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->a:Lagg;

    .line 10
    new-instance v0, Lagg;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v4}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->b:Lagg;

    .line 12
    new-instance v0, Lagg;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v5}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->c:Lagg;

    .line 14
    new-instance v0, Lagg;

    const-string v1, "PROCESSING"

    invoke-direct {v0, v1, v6}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->d:Lagg;

    .line 16
    new-instance v0, Lagg;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v7}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->e:Lagg;

    .line 18
    new-instance v0, Lagg;

    const-string v1, "ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->f:Lagg;

    .line 20
    new-instance v0, Lagg;

    const-string v1, "CANCELED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->g:Lagg;

    .line 22
    new-instance v0, Lagg;

    const-string v1, "UNSET"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lagg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagg;->h:Lagg;

    .line 6
    const/16 v0, 0x8

    new-array v0, v0, [Lagg;

    sget-object v1, Lagg;->a:Lagg;

    aput-object v1, v0, v3

    sget-object v1, Lagg;->b:Lagg;

    aput-object v1, v0, v4

    sget-object v1, Lagg;->c:Lagg;

    aput-object v1, v0, v5

    sget-object v1, Lagg;->d:Lagg;

    aput-object v1, v0, v6

    sget-object v1, Lagg;->e:Lagg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lagg;->f:Lagg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lagg;->g:Lagg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lagg;->h:Lagg;

    aput-object v2, v0, v1

    sput-object v0, Lagg;->a:[Lagg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lagg;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lagg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lagg;

    return-object v0
.end method

.method public static values()[Lagg;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lagg;->a:[Lagg;

    invoke-virtual {v0}, [Lagg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lagg;

    return-object v0
.end method
