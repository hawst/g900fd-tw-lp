.class public LarX;
.super Ljava/lang/Object;
.source "Dialogs.java"


# instance fields
.field a:LM;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public a(LarY;Ljava/lang/String;)Landroid/support/v4/app/DialogFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/support/v4/app/DialogFragment;",
            ">(",
            "LarY",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {p1}, LarY;->a(LarY;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LarX;->a(Ljava/lang/Class;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 58
    if-nez v0, :cond_0

    .line 60
    :try_start_0
    invoke-static {p1}, LarY;->a(LarY;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 61
    invoke-virtual {p1, v0}, LarY;->a(Landroid/support/v4/app/DialogFragment;)V

    .line 62
    iget-object v1, p0, LarX;->a:LM;

    invoke-virtual {v0, v1, p2}, Landroid/support/v4/app/DialogFragment;->a(LM;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 70
    :cond_0
    return-object v0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-static {v0}, Lbjz;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    invoke-static {v0}, Lbjz;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/support/v4/app/Fragment;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, LarX;->a:LM;

    invoke-virtual {v0, p2}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method
