.class public LOp;
.super Ljava/lang/Object;
.source "ApiaryPsynchoConfigurationAccessor.java"

# interfaces
.implements LVN;


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LVR;",
            "LOq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LaKi;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 86
    sget-object v0, LVR;->a:LVR;

    sget-object v1, LOq;->b:LOq;

    sget-object v2, LVR;->b:LVR;

    sget-object v3, LOq;->c:LOq;

    sget-object v4, LVR;->c:LVR;

    sget-object v5, LOq;->d:LOq;

    invoke-static/range {v0 .. v5}, LbmL;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmL;

    move-result-object v0

    sput-object v0, LOp;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LaKi;LtK;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const-string v0, "https://www.googleapis.com/auth/drive"

    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, LOp;->a:Ljava/util/List;

    .line 100
    iput-object p1, p0, LOp;->a:LaKi;

    .line 101
    iput-object p2, p0, LOp;->a:LtK;

    .line 102
    return-void
.end method

.method private a(LaFO;LOq;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 136
    invoke-static {}, LamV;->b()V

    .line 138
    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, LOp;->a:LaKi;

    iget-object v1, p0, LOp;->a:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, LaKi;->a(LaFO;Ljava/util/List;)Lcom/google/api/services/drive/Drive;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$Settings;

    move-result-object v0

    iget-object v1, p2, LOq;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/Drive$Settings;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Get;

    move-result-object v0

    iget-object v1, p2, LOq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/Drive$Settings$Get;->b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Get;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/google/api/services/drive/DriveRequest;->execute()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Setting;

    .line 143
    invoke-direct {p0, v0}, LOp;->a(Lcom/google/api/services/drive/model/Setting;)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 146
    :catch_1
    move-exception v0

    .line 147
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lcom/google/api/services/drive/model/Setting;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 107
    if-nez p1, :cond_1

    .line 119
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 124
    iget-object v2, p0, LOp;->a:LtK;

    sget-object v3, Lry;->ab:Lry;

    invoke-interface {v2, v3}, LtK;->a(LtJ;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 125
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_1
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/Setting;->a()Ljava/lang/String;

    move-result-object v1

    .line 111
    const-string v2, "true"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 113
    :cond_2
    const-string v2, "false"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_3
    const-string v2, "ApiaryPsynchoConfigurationAccessor"

    invoke-static {v2, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_4
    return-object v0
.end method

.method private a(LaFO;LOq;Z)V
    .locals 4

    .prologue
    .line 166
    invoke-static {}, LamV;->b()V

    .line 167
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    if-eqz p3, :cond_0

    const-string v0, "true"

    .line 170
    :goto_0
    new-instance v1, Lcom/google/api/services/drive/model/Setting;

    invoke-direct {v1}, Lcom/google/api/services/drive/model/Setting;-><init>()V

    iget-object v2, p2, LOq;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/api/services/drive/model/Setting;->a(Ljava/lang/String;)Lcom/google/api/services/drive/model/Setting;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/drive/model/Setting;->b(Ljava/lang/String;)Lcom/google/api/services/drive/model/Setting;

    move-result-object v0

    .line 173
    :try_start_0
    iget-object v1, p0, LOp;->a:LaKi;

    iget-object v2, p0, LOp;->a:Ljava/util/List;

    invoke-virtual {v1, p1, v2}, LaKi;->a(LaFO;Ljava/util/List;)Lcom/google/api/services/drive/Drive;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 185
    invoke-virtual {v1}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$Settings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/drive/Drive$Settings;->a(Lcom/google/api/services/drive/model/Setting;)Lcom/google/api/services/drive/Drive$Settings$Insert;

    move-result-object v0

    iget-object v1, p2, LOq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/Drive$Settings$Insert;->b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Insert;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive$Settings$Insert;->execute()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Setting;

    .line 187
    invoke-direct {p0, v0}, LOp;->a(Lcom/google/api/services/drive/model/Setting;)Ljava/lang/Boolean;

    move-result-object v0

    .line 188
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Lbfc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 189
    const-string v1, "Server returned successful result %s although we set %s for %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    .line 190
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    aput-object p2, v2, v0

    .line 189
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 191
    iget-object v1, p0, LOp;->a:LtK;

    sget-object v2, Lry;->ab:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 169
    :cond_0
    const-string v0, "false"

    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 176
    :catch_1
    move-exception v0

    .line 177
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 194
    :cond_1
    const-string v1, "ApiaryPsynchoConfigurationAccessor"

    invoke-static {v1, v0}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_2
    return-void
.end method


# virtual methods
.method public a(LaFO;)LVO;
    .locals 1

    .prologue
    .line 154
    invoke-static {}, LamV;->b()V

    .line 155
    sget-object v0, LOq;->a:LOq;

    invoke-direct {p0, p1, v0}, LOp;->a(LaFO;LOq;)Ljava/lang/Boolean;

    move-result-object v0

    .line 156
    if-nez v0, :cond_0

    .line 157
    sget-object v0, LVO;->a:LVO;

    .line 159
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LVO;->b:LVO;

    goto :goto_0

    :cond_1
    sget-object v0, LVO;->c:LVO;

    goto :goto_0
.end method

.method public a(LaFO;LVO;)V
    .locals 2

    .prologue
    .line 203
    invoke-static {}, LamV;->b()V

    .line 204
    sget-object v0, LVO;->a:LVO;

    invoke-virtual {p2, v0}, LVO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 205
    sget-object v0, LOq;->a:LOq;

    sget-object v1, LVO;->b:LVO;

    invoke-virtual {p2, v1}, LVO;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LOp;->a(LaFO;LOq;Z)V

    .line 206
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaFO;LVR;)Z
    .locals 2

    .prologue
    .line 210
    invoke-static {}, LamV;->b()V

    .line 211
    sget-object v0, LOp;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LOq;

    .line 212
    if-nez v0, :cond_0

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p2}, LVR;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    invoke-direct {p0, p1, v0}, LOp;->a(LaFO;LOq;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
