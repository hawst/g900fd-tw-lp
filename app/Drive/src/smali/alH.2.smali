.class public LalH;
.super Ljava/lang/Object;
.source "GifUtilities.java"


# static fields
.field private static final a:[B

.field private static final b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LalH;->a:[B

    .line 13
    const/16 v0, 0xb

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, LalH;->b:[B

    return-void

    .line 12
    :array_0
    .array-data 1
        0x47t
        0x49t
        0x46t
    .end array-data

    .line 13
    :array_1
    .array-data 1
        0x4et
        0x45t
        0x54t
        0x53t
        0x43t
        0x41t
        0x50t
        0x45t
        0x32t
        0x2et
        0x30t
    .end array-data
.end method

.method public static a(Ljava/io/InputStream;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 27
    :try_start_0
    sget-object v2, LalH;->a:[B

    array-length v2, v2

    new-array v2, v2, [B

    .line 28
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    sget-object v4, LalH;->a:[B

    array-length v4, v4

    if-ne v3, v4, :cond_0

    sget-object v3, LalH;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_1

    .line 97
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 33
    :cond_1
    const/4 v2, 0x7

    invoke-static {p0, v2}, LalH;->a(Ljava/io/InputStream;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 39
    if-eq v3, v5, :cond_0

    .line 43
    and-int/lit16 v2, v3, 0x80

    if-eqz v2, :cond_4

    move v2, v1

    .line 44
    :goto_1
    and-int/lit8 v3, v3, 0x7

    shl-int v3, v6, v3

    .line 46
    const/4 v4, 0x2

    invoke-static {p0, v4}, LalH;->a(Ljava/io/InputStream;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 51
    if-eqz v2, :cond_2

    mul-int/lit8 v2, v3, 0x3

    invoke-static {p0, v2}, LalH;->a(Ljava/io/InputStream;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    :cond_2
    :goto_2
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 58
    const/16 v3, 0x21

    if-ne v2, v3, :cond_0

    .line 62
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 63
    if-eq v2, v5, :cond_0

    .line 67
    sparse-switch v2, :sswitch_data_0

    .line 89
    :cond_3
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 90
    if-eq v2, v5, :cond_0

    invoke-static {p0, v2}, LalH;->a(Ljava/io/InputStream;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 93
    if-gtz v2, :cond_3

    goto :goto_2

    :cond_4
    move v2, v0

    .line 43
    goto :goto_1

    .line 69
    :sswitch_1
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 70
    sget-object v3, LalH;->b:[B

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 75
    new-array v3, v2, [B

    .line 76
    invoke-virtual {p0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-ne v4, v2, :cond_0

    sget-object v2, LalH;->b:[B

    .line 77
    invoke-static {v3, v2}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 78
    goto :goto_0

    .line 96
    :catch_0
    move-exception v1

    goto :goto_0

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0xf9 -> :sswitch_0
        0xff -> :sswitch_1
    .end sparse-switch
.end method

.method private static a(Ljava/io/InputStream;I)Z
    .locals 4

    .prologue
    .line 17
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 18
    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
