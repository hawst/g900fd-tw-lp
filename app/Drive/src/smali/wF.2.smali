.class LwF;
.super Ljava/lang/Object;
.source "FileCacheStore.java"

# interfaces
.implements LwC;


# instance fields
.field private final a:Ljava/io/File;

.field private final a:Ljava/lang/String;

.field final synthetic a:LwD;


# direct methods
.method private constructor <init>(LwD;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 32
    iput-object p1, p0, LwF;->a:LwD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p2, p0, LwF;->a:Ljava/lang/String;

    .line 34
    const-string v0, "app"

    const-string v1, ".cache"

    invoke-static {p1}, LwD;->a(LwD;)Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LwF;->a:Ljava/io/File;

    .line 35
    return-void
.end method

.method synthetic constructor <init>(LwD;Ljava/lang/String;LwE;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, LwF;-><init>(LwD;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LwF;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LwF;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 41
    return-void
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, LwF;->a:LwD;

    invoke-static {v0}, LwD;->a(LwD;)Lalo;

    move-result-object v0

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, LwF;->a:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-interface {v0, p1, v1}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 46
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LwF;->a:Ljava/lang/String;

    return-object v0
.end method
