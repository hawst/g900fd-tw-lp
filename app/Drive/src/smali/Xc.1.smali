.class public final LXc;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXC;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXJ;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LYw;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXz;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWR;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXx;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LYO;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXt;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWS;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXS;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LYc;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZB;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXD;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LYy;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LYZ;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LYS;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LXR;",
            ">;>;"
        }
    .end annotation
.end field

.field public r:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXq;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZA;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWU;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXy;",
            ">;"
        }
    .end annotation
.end field

.field public v:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZm;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LXR;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWY;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZG;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 62
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 63
    iput-object p1, p0, LXc;->a:LbrA;

    .line 64
    const-class v0, LXC;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->a:Lbsk;

    .line 67
    const-class v0, LXJ;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->b:Lbsk;

    .line 70
    const-class v0, LYw;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->c:Lbsk;

    .line 73
    const-class v0, LXz;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->d:Lbsk;

    .line 76
    const-class v0, LWR;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->e:Lbsk;

    .line 79
    const-class v0, LXx;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->f:Lbsk;

    .line 82
    const-class v0, LYO;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->g:Lbsk;

    .line 85
    const-class v0, LXt;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->h:Lbsk;

    .line 88
    const-class v0, LWS;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->i:Lbsk;

    .line 91
    const-class v0, LXS;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->j:Lbsk;

    .line 94
    const-class v0, LYc;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->k:Lbsk;

    .line 97
    const-class v0, LZB;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->l:Lbsk;

    .line 100
    const-class v0, LXD;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->m:Lbsk;

    .line 103
    const-class v0, LYy;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->n:Lbsk;

    .line 106
    const-class v0, LYZ;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->o:Lbsk;

    .line 109
    const-class v0, LYS;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->p:Lbsk;

    .line 112
    const-class v0, Laja;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, LXR;

    aput-object v3, v1, v2

    .line 113
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 112
    invoke-static {v0, v4}, LXc;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->q:Lbsk;

    .line 115
    const-class v0, LXq;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->r:Lbsk;

    .line 118
    const-class v0, LZA;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->s:Lbsk;

    .line 121
    const-class v0, LWU;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->t:Lbsk;

    .line 124
    const-class v0, LXy;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->u:Lbsk;

    .line 127
    const-class v0, LZm;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->v:Lbsk;

    .line 130
    const-class v0, LXR;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->w:Lbsk;

    .line 133
    const-class v0, LWY;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->x:Lbsk;

    .line 136
    const-class v0, LZG;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->y:Lbsk;

    .line 139
    const-class v0, LZC;

    invoke-static {v0, v4}, LXc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LXc;->z:Lbsk;

    .line 142
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 850
    sparse-switch p1, :sswitch_data_0

    .line 955
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 852
    :sswitch_0
    new-instance v0, LXS;

    invoke-direct {v0}, LXS;-><init>()V

    .line 854
    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    .line 855
    invoke-virtual {v1, v0}, LXc;->a(LXS;)V

    .line 953
    :goto_0
    return-object v0

    .line 858
    :sswitch_1
    new-instance v0, LYc;

    invoke-direct {v0}, LYc;-><init>()V

    .line 860
    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    .line 861
    invoke-virtual {v1, v0}, LXc;->a(LYc;)V

    goto :goto_0

    .line 864
    :sswitch_2
    new-instance v1, LZB;

    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 867
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LXc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 865
    invoke-static {v0, v2}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LZB;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 872
    goto :goto_0

    .line 874
    :sswitch_3
    new-instance v0, LXD;

    invoke-direct {v0}, LXD;-><init>()V

    goto :goto_0

    .line 878
    :sswitch_4
    new-instance v3, LYy;

    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 881
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 879
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->d:Lbsk;

    .line 887
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LXc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LXc;

    iget-object v2, v2, LXc;->d:Lbsk;

    .line 885
    invoke-static {v1, v2}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LXz;

    iget-object v2, p0, LXc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 893
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LXc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 891
    invoke-static {v2, v4}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    invoke-direct {v3, v0, v1, v2}, LYy;-><init>(Laja;LXz;LQr;)V

    .line 898
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 899
    invoke-virtual {v0, v3}, LXc;->a(LYy;)V

    move-object v0, v3

    .line 900
    goto :goto_0

    .line 902
    :sswitch_5
    new-instance v0, LYZ;

    invoke-direct {v0}, LYZ;-><init>()V

    goto/16 :goto_0

    .line 906
    :sswitch_6
    new-instance v0, LYS;

    invoke-direct {v0}, LYS;-><init>()V

    goto/16 :goto_0

    .line 910
    :sswitch_7
    new-instance v2, Laja;

    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->w:Lbsk;

    .line 911
    invoke-static {v0}, LXc;->a(LbuE;)LbuE;

    move-result-object v3

    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 918
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->a:Lbsk;

    .line 916
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 924
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v4, p0, LXc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->c:Lbsk;

    .line 922
    invoke-static {v1, v4}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    invoke-direct {v2, v3, v0, v1}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    move-object v0, v2

    .line 929
    goto/16 :goto_0

    .line 931
    :sswitch_8
    new-instance v0, LXq;

    invoke-direct {v0}, LXq;-><init>()V

    .line 933
    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    .line 934
    invoke-virtual {v1, v0}, LXc;->a(LXq;)V

    goto/16 :goto_0

    .line 937
    :sswitch_9
    new-instance v1, LZA;

    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->ac:Lbsk;

    .line 940
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LXc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->ac:Lbsk;

    .line 938
    invoke-static {v0, v2}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, LZA;-><init>(Laja;)V

    move-object v0, v1

    .line 945
    goto/16 :goto_0

    .line 947
    :sswitch_a
    new-instance v0, LWU;

    invoke-direct {v0}, LWU;-><init>()V

    goto/16 :goto_0

    .line 951
    :sswitch_b
    new-instance v0, LXy;

    invoke-direct {v0}, LXy;-><init>()V

    goto/16 :goto_0

    .line 850
    nop

    :sswitch_data_0
    .sparse-switch
        0x40d -> :sswitch_a
        0x553 -> :sswitch_0
        0x554 -> :sswitch_1
        0x555 -> :sswitch_2
        0x556 -> :sswitch_3
        0x560 -> :sswitch_4
        0x562 -> :sswitch_5
        0x563 -> :sswitch_7
        0x564 -> :sswitch_6
        0x568 -> :sswitch_8
        0x569 -> :sswitch_9
        0x56a -> :sswitch_b
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1096
    sparse-switch p2, :sswitch_data_0

    .line 1143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1098
    :sswitch_0
    check-cast p1, LZy;

    .line 1100
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 1103
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1100
    invoke-virtual {p1, v0}, LZy;->provideVideoViewListener(Landroid/content/Context;)LZm;

    move-result-object v0

    .line 1136
    :goto_0
    return-object v0

    .line 1107
    :sswitch_1
    check-cast p1, LZy;

    .line 1109
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 1112
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1109
    invoke-virtual {p1, v0}, LZy;->provideUiRequestListener(Landroid/content/Context;)LXR;

    move-result-object v0

    goto :goto_0

    .line 1116
    :sswitch_2
    check-cast p1, LZy;

    .line 1118
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 1121
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1118
    invoke-virtual {p1, v0}, LZy;->provideDragKnobFragmentListener(Landroid/content/Context;)LWY;

    move-result-object v0

    goto :goto_0

    .line 1125
    :sswitch_3
    check-cast p1, LZy;

    .line 1127
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 1130
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1127
    invoke-virtual {p1, v0}, LZy;->provideSlidePickerController(Landroid/content/Context;)LZG;

    move-result-object v0

    goto :goto_0

    .line 1134
    :sswitch_4
    check-cast p1, LZy;

    .line 1136
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 1139
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1136
    invoke-virtual {p1, v0}, LZy;->providePunchWebViewOwner(Landroid/content/Context;)LZC;

    move-result-object v0

    goto :goto_0

    .line 1096
    nop

    :sswitch_data_0
    .sparse-switch
        0x2e0 -> :sswitch_4
        0x558 -> :sswitch_1
        0x561 -> :sswitch_0
        0x565 -> :sswitch_2
        0x566 -> :sswitch_3
    .end sparse-switch
.end method

.method public a()V
    .locals 4

    .prologue
    .line 699
    const-class v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb6

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 702
    const-class v0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xba

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 705
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xbb

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 708
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xbc

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 711
    const-class v0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xbe

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 714
    const-class v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb7

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 717
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xbd

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 720
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc1

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 723
    const-class v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc2

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 726
    const-class v0, LYc;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb9

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 729
    const-class v0, LYy;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xbf

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 732
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc4

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 735
    const-class v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc5

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 738
    const-class v0, LYs;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc3

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 741
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc7

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 744
    const-class v0, LXS;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb8

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 747
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc0

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 750
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc8

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 753
    const-class v0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc9

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 756
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xca

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 759
    const-class v0, LXq;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc6

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 762
    const-class v0, LXC;

    iget-object v1, p0, LXc;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 763
    const-class v0, LXJ;

    iget-object v1, p0, LXc;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 764
    const-class v0, LYw;

    iget-object v1, p0, LXc;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 765
    const-class v0, LXz;

    iget-object v1, p0, LXc;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 766
    const-class v0, LWR;

    iget-object v1, p0, LXc;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 767
    const-class v0, LXx;

    iget-object v1, p0, LXc;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 768
    const-class v0, LYO;

    iget-object v1, p0, LXc;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 769
    const-class v0, LXt;

    iget-object v1, p0, LXc;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 770
    const-class v0, LWS;

    iget-object v1, p0, LXc;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 771
    const-class v0, LXS;

    iget-object v1, p0, LXc;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 772
    const-class v0, LYc;

    iget-object v1, p0, LXc;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 773
    const-class v0, LZB;

    iget-object v1, p0, LXc;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 774
    const-class v0, LXD;

    iget-object v1, p0, LXc;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 775
    const-class v0, LYy;

    iget-object v1, p0, LXc;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 776
    const-class v0, LYZ;

    iget-object v1, p0, LXc;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 777
    const-class v0, LYS;

    iget-object v1, p0, LXc;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 778
    const-class v0, Laja;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, LXR;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LXc;->q:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 779
    const-class v0, LXq;

    iget-object v1, p0, LXc;->r:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 780
    const-class v0, LZA;

    iget-object v1, p0, LXc;->s:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 781
    const-class v0, LWU;

    iget-object v1, p0, LXc;->t:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 782
    const-class v0, LXy;

    iget-object v1, p0, LXc;->u:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 783
    const-class v0, LZm;

    iget-object v1, p0, LXc;->v:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 784
    const-class v0, LXR;

    iget-object v1, p0, LXc;->w:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 785
    const-class v0, LWY;

    iget-object v1, p0, LXc;->x:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 786
    const-class v0, LZG;

    iget-object v1, p0, LXc;->y:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 787
    const-class v0, LZC;

    iget-object v1, p0, LXc;->z:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 788
    iget-object v0, p0, LXc;->a:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->o:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 790
    iget-object v0, p0, LXc;->b:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->m:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 792
    iget-object v0, p0, LXc;->c:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->n:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 794
    iget-object v0, p0, LXc;->d:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->b:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 796
    iget-object v0, p0, LXc;->e:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->y:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 798
    iget-object v0, p0, LXc;->f:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->u:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 800
    iget-object v0, p0, LXc;->g:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->p:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 802
    iget-object v0, p0, LXc;->h:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->l:Lbsk;

    invoke-static {v1}, LXc;->a(Lbxw;)LbuE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 805
    iget-object v0, p0, LXc;->i:Lbsk;

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->s:Lbsk;

    invoke-static {v1}, LXc;->a(Lbxw;)LbuE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 808
    iget-object v0, p0, LXc;->j:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x553

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 810
    iget-object v0, p0, LXc;->k:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x554

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 812
    iget-object v0, p0, LXc;->l:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x555

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 814
    iget-object v0, p0, LXc;->m:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x556

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 816
    iget-object v0, p0, LXc;->n:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x560

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 818
    iget-object v0, p0, LXc;->o:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x562

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 820
    iget-object v0, p0, LXc;->p:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x564

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 822
    iget-object v0, p0, LXc;->q:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x563

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 824
    iget-object v0, p0, LXc;->r:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x568

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 826
    iget-object v0, p0, LXc;->s:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x569

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 828
    iget-object v0, p0, LXc;->t:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x40d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 830
    iget-object v0, p0, LXc;->u:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x56a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 832
    iget-object v0, p0, LXc;->v:Lbsk;

    const-class v1, LZy;

    const/16 v2, 0x561

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 834
    iget-object v0, p0, LXc;->w:Lbsk;

    const-class v1, LZy;

    const/16 v2, 0x558

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 836
    iget-object v0, p0, LXc;->x:Lbsk;

    const-class v1, LZy;

    const/16 v2, 0x565

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 838
    iget-object v0, p0, LXc;->y:Lbsk;

    const-class v1, LZy;

    const/16 v2, 0x566

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 840
    iget-object v0, p0, LXc;->z:Lbsk;

    const-class v1, LZy;

    const/16 v2, 0x2e0

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 842
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 962
    packed-switch p1, :pswitch_data_0

    .line 1090
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 964
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    .line 966
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 967
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;)V

    .line 1092
    :goto_0
    return-void

    .line 970
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    .line 972
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 973
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;)V

    goto :goto_0

    .line 976
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    .line 978
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 979
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    goto :goto_0

    .line 982
    :pswitch_3
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    .line 984
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 985
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;)V

    goto :goto_0

    .line 988
    :pswitch_4
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    .line 990
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 991
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;)V

    goto :goto_0

    .line 994
    :pswitch_5
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    .line 996
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 997
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;)V

    goto :goto_0

    .line 1000
    :pswitch_6
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;

    .line 1002
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1003
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;)V

    goto :goto_0

    .line 1006
    :pswitch_7
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    .line 1008
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1009
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V

    goto :goto_0

    .line 1012
    :pswitch_8
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    .line 1014
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1015
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;)V

    goto :goto_0

    .line 1018
    :pswitch_9
    check-cast p2, LYc;

    .line 1020
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1021
    invoke-virtual {v0, p2}, LXc;->a(LYc;)V

    goto :goto_0

    .line 1024
    :pswitch_a
    check-cast p2, LYy;

    .line 1026
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1027
    invoke-virtual {v0, p2}, LXc;->a(LYy;)V

    goto :goto_0

    .line 1030
    :pswitch_b
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    .line 1032
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1033
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V

    goto :goto_0

    .line 1036
    :pswitch_c
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    .line 1038
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1039
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;)V

    goto :goto_0

    .line 1042
    :pswitch_d
    check-cast p2, LYs;

    .line 1044
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1045
    invoke-virtual {v0, p2}, LXc;->a(LYs;)V

    goto/16 :goto_0

    .line 1048
    :pswitch_e
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;

    .line 1050
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1051
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V

    goto/16 :goto_0

    .line 1054
    :pswitch_f
    check-cast p2, LXS;

    .line 1056
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1057
    invoke-virtual {v0, p2}, LXc;->a(LXS;)V

    goto/16 :goto_0

    .line 1060
    :pswitch_10
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    .line 1062
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1063
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;)V

    goto/16 :goto_0

    .line 1066
    :pswitch_11
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    .line 1068
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1069
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    goto/16 :goto_0

    .line 1072
    :pswitch_12
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    .line 1074
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1075
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;)V

    goto/16 :goto_0

    .line 1078
    :pswitch_13
    check-cast p2, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    .line 1080
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1081
    invoke-virtual {v0, p2}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;)V

    goto/16 :goto_0

    .line 1084
    :pswitch_14
    check-cast p2, LXq;

    .line 1086
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 1087
    invoke-virtual {v0, p2}, LXc;->a(LXq;)V

    goto/16 :goto_0

    .line 962
    :pswitch_data_0
    .packed-switch 0xb6
        :pswitch_0
        :pswitch_5
        :pswitch_f
        :pswitch_9
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_a
        :pswitch_10
        :pswitch_7
        :pswitch_8
        :pswitch_d
        :pswitch_b
        :pswitch_c
        :pswitch_14
        :pswitch_e
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public a(LXS;)V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->m:Lbsk;

    .line 512
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->m:Lbsk;

    .line 510
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXD;

    iput-object v0, p1, LXS;->a:LXD;

    .line 516
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->q:Lbsk;

    .line 519
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->q:Lbsk;

    .line 517
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LXS;->a:Laja;

    .line 523
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->i:Lbsk;

    .line 526
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->i:Lbsk;

    .line 524
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    iput-object v0, p1, LXS;->a:LTO;

    .line 530
    return-void
.end method

.method public a(LXq;)V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 693
    invoke-virtual {v0, p1}, LXc;->a(LYs;)V

    .line 694
    return-void
.end method

.method public a(LYc;)V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 417
    invoke-virtual {v0, p1}, LXc;->a(LYs;)V

    .line 418
    return-void
.end method

.method public a(LYs;)V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->i:Lbsk;

    .line 460
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->i:Lbsk;

    .line 458
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    iput-object v0, p1, LYs;->a:LTO;

    .line 464
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->m:Lbsk;

    .line 467
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->m:Lbsk;

    .line 465
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXD;

    iput-object v0, p1, LYs;->a:LXD;

    .line 471
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->q:Lbsk;

    .line 474
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->q:Lbsk;

    .line 472
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LYs;->a:Laja;

    .line 478
    return-void
.end method

.method public a(LYy;)V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lbrx;

    iget-object v0, v0, Lbrx;->c:Lbsk;

    .line 425
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lbrx;

    iget-object v1, v1, Lbrx;->c:Lbsk;

    .line 423
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, LYy;->a:I

    .line 429
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;)V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->e:Lbsk;

    .line 296
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->e:Lbsk;

    .line 294
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWR;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWR;

    .line 300
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 303
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 301
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LqK;

    .line 307
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->d:Lbsk;

    .line 310
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->d:Lbsk;

    .line 308
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    .line 314
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->c:Lbsk;

    .line 317
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->c:Lbsk;

    .line 315
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYw;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYw;

    .line 321
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 324
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 322
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQr;

    .line 328
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;)V
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->x:Lbsk;

    .line 678
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->x:Lbsk;

    .line 676
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWY;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:LWY;

    .line 682
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;)V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 288
    invoke-virtual {v0, p1}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;)V

    .line 289
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 149
    invoke-virtual {v0, p1}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;)V

    .line 150
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->g:Lbsk;

    .line 157
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->g:Lbsk;

    .line 155
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYO;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LYO;

    .line 161
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->m:Lbsk;

    .line 164
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->m:Lbsk;

    .line 162
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXD;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXD;

    .line 168
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 171
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 169
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LqK;

    .line 175
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->j:Lbsk;

    .line 178
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->j:Lbsk;

    .line 176
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b:LXS;

    .line 182
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->w:Lbsk;

    .line 185
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXR;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXR;

    .line 187
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->j:Lbsk;

    .line 190
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->j:Lbsk;

    .line 188
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXS;

    .line 194
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;)V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 333
    invoke-virtual {v0, p1}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;)V

    .line 334
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 337
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 335
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Lald;

    .line 341
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->f:Lbsk;

    .line 344
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->f:Lbsk;

    .line 342
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Laja;

    .line 348
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->d:Lbsk;

    .line 351
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->d:Lbsk;

    .line 349
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LXz;

    .line 355
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 358
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 356
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LQr;

    .line 362
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->q:Lbsk;

    .line 365
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->q:Lbsk;

    .line 363
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Laja;

    .line 369
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 282
    invoke-virtual {v0, p1}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;)V

    .line 283
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;)V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->f:Lbsk;

    .line 537
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->f:Lbsk;

    .line 535
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXx;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a:LXx;

    .line 541
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->g:Lbsk;

    .line 544
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->g:Lbsk;

    .line 542
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYO;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a:LYO;

    .line 548
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 434
    invoke-virtual {v0, p1}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;)V

    .line 435
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->i:Lbsk;

    .line 438
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->i:Lbsk;

    .line 436
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWS;

    .line 442
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;)V
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 687
    invoke-virtual {v0, p1}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;)V

    .line 688
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->g:Lbsk;

    .line 376
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->g:Lbsk;

    .line 374
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYO;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LYO;

    .line 380
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->q:Lbsk;

    .line 383
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->q:Lbsk;

    .line 381
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Laja;

    .line 387
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->d:Lbsk;

    .line 390
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->d:Lbsk;

    .line 388
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    .line 394
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 397
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 395
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LqK;

    .line 401
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    .line 483
    invoke-virtual {v0, p1}, LXc;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;)V

    .line 484
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->i:Lbsk;

    .line 487
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->i:Lbsk;

    .line 485
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWS;

    .line 491
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 494
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 492
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LtK;

    .line 498
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->m:Lbsk;

    .line 501
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->m:Lbsk;

    .line 499
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXD;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXD;

    .line 505
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 199
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 200
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->f:Lbsk;

    .line 203
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->f:Lbsk;

    .line 201
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXx;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXx;

    .line 207
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->h:Lbsk;

    .line 210
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->h:Lbsk;

    .line 208
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXt;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXt;

    .line 214
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->b:Lbsk;

    .line 217
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->b:Lbsk;

    .line 215
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXJ;

    .line 221
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 224
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 222
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQr;

    .line 228
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTV;

    iget-object v0, v0, LTV;->a:Lbsk;

    .line 231
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTV;

    iget-object v1, v1, LTV;->a:Lbsk;

    .line 229
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTT;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LTT;

    .line 235
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->a:Lbsk;

    .line 238
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->a:Lbsk;

    .line 236
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXC;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXC;

    .line 242
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->w:Lbsk;

    .line 245
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->w:Lbsk;

    .line 243
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsP;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LsP;

    .line 249
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->k:Lbsk;

    .line 252
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lyh;

    iget-object v1, v1, Lyh;->k:Lbsk;

    .line 250
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lye;

    .line 256
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 259
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 257
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LaGR;

    .line 263
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->g:Lbsk;

    .line 266
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->g:Lbsk;

    .line 264
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYO;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    .line 270
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 273
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 271
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LaGM;

    .line 277
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 555
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 553
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lald;

    .line 559
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->f:Lbsk;

    .line 562
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->f:Lbsk;

    .line 560
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUT;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUT;

    .line 566
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->v:Lbsk;

    .line 569
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->v:Lbsk;

    .line 567
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZm;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZm;

    .line 573
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 576
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 574
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:LQr;

    .line 580
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->i:Lbsk;

    .line 583
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->i:Lbsk;

    .line 581
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LTO;

    .line 587
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 590
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 588
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LtK;

    .line 594
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->k:Lbsk;

    .line 597
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lyh;

    iget-object v1, v1, Lyh;->k:Lbsk;

    .line 595
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lye;

    .line 601
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 604
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 602
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    .line 608
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->k:Lbsk;

    .line 611
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->k:Lbsk;

    .line 609
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTd;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LTd;

    .line 615
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->g:Lbsk;

    .line 618
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->g:Lbsk;

    .line 616
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYO;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    .line 622
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->K:Lbsk;

    .line 625
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->K:Lbsk;

    .line 623
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalo;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lalo;

    .line 629
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->m:Lbsk;

    .line 632
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->m:Lbsk;

    .line 630
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXD;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    .line 636
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 639
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 637
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LaKR;

    .line 643
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 646
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 644
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQr;

    .line 650
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->r:Lbsk;

    .line 653
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->r:Lbsk;

    .line 651
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXq;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXq;

    .line 657
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->l:Lbsk;

    .line 660
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->l:Lbsk;

    .line 658
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Class;

    .line 664
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->k:Lbsk;

    .line 667
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->k:Lbsk;

    .line 665
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYc;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYc;

    .line 671
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;)V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->d:Lbsk;

    .line 449
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->d:Lbsk;

    .line 447
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LXz;

    .line 453
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;)V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, LXc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LXc;

    iget-object v0, v0, LXc;->d:Lbsk;

    .line 408
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LXc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LXc;

    iget-object v1, v1, LXc;->d:Lbsk;

    .line 406
    invoke-static {v0, v1}, LXc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LXz;

    .line 412
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 846
    return-void
.end method
