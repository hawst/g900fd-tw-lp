.class public LsB;
.super Ljava/lang/Object;
.source "DocListActivity.java"

# interfaces
.implements LJJ;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocListActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocListActivity;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()LCl;
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwa;

    invoke-interface {v0}, Lwa;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 207
    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 208
    instance-of v2, v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    if-eqz v2, :cond_0

    .line 209
    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    .line 210
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a()LCl;

    move-result-object v0

    .line 217
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCo;

    sget-object v1, LCn;->l:LCn;

    invoke-interface {v0, v1}, LCo;->a(LCn;)LCl;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocListActivity;->j()V

    .line 193
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0, p1}, LAP;->a(Landroid/content/Intent;)V

    .line 253
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0, p1}, LAP;->a(Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()LQX;

    move-result-object v0

    .line 198
    if-nez v0, :cond_0

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v1, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v1

    .line 202
    invoke-virtual {v0}, LQX;->a()LIK;

    move-result-object v2

    invoke-virtual {v0}, LQX;->a()LCl;

    move-result-object v0

    invoke-interface {v0}, LCl;->a()Ljava/util/EnumSet;

    move-result-object v0

    .line 201
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;->a(LM;LIK;Ljava/util/EnumSet;)V

    goto :goto_0
.end method

.method public c()V
    .locals 7

    .prologue
    .line 222
    sget-object v0, LCe;->e:LCe;

    sget-object v1, LCe;->h:LCe;

    sget-object v2, LCe;->g:LCe;

    sget-object v3, LCe;->f:LCe;

    sget-object v4, LCe;->j:LCe;

    sget-object v5, LCe;->k:LCe;

    sget-object v6, LCe;->l:LCe;

    invoke-static/range {v0 .. v6}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    .line 230
    iget-object v1, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    .line 231
    invoke-virtual {v1}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v1

    invoke-direct {p0}, LsB;->a()LCl;

    move-result-object v2

    .line 230
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a(LM;LCl;LbmF;)V

    .line 232
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocListActivity;->a(Lcom/google/android/apps/docs/app/DocListActivity;)LqK;

    move-result-object v0

    const-string v1, "doclist"

    const-string v2, "arrangementModeList"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    sget-object v1, LzO;->a:LzO;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LzO;)V

    .line 240
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 244
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocListActivity;->b(Lcom/google/android/apps/docs/app/DocListActivity;)LqK;

    move-result-object v0

    const-string v1, "doclist"

    const-string v2, "arrangementModeGrid"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    sget-object v1, LzO;->b:LzO;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LzO;)V

    .line 248
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LKZ;

    invoke-virtual {v0}, LKZ;->a()V

    .line 258
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJR;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LJR;->a(Z)V

    .line 263
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJR;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LJR;->a(Z)V

    .line 268
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Lcom/google/android/apps/docs/view/DocListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()LQX;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v0}, LQX;->a()LaFX;

    move-result-object v0

    const-class v1, LaGA;

    invoke-virtual {v0, v1}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v0

    check-cast v0, LaGA;

    .line 271
    if-eqz v0, :cond_0

    .line 272
    iget-object v1, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCU;

    invoke-virtual {v1, v0}, LCU;->a(LaGA;)V

    .line 275
    :cond_0
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, LsB;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0}, LAP;->i()V

    .line 285
    return-void
.end method
