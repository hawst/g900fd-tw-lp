.class public LakD;
.super Ljava/lang/Object;
.source "CloseableReference.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field private final a:LakE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakE",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method private constructor <init>(LakD;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, LakD;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 134
    iget-object v1, p1, LakD;->a:LakE;

    iput-object v1, p0, LakD;->a:LakE;

    .line 135
    iget-object v1, p0, LakD;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v2, p0, LakD;->a:LakE;

    invoke-virtual {v2}, LakE;->a()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 136
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;LakG;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LakG",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LakD;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 130
    new-instance v0, LakE;

    invoke-direct {v0, p1, p2}, LakE;-><init>(Ljava/lang/Object;LakG;)V

    iput-object v0, p0, LakD;->a:LakE;

    .line 131
    return-void
.end method

.method public static a(LakD;)LakD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LakD",
            "<+TT;>;)",
            "LakD",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, LakD;

    invoke-direct {v0, p0}, LakD;-><init>(LakD;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;LakG;)LakD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "LakG",
            "<TT;>;)",
            "LakD",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, LakD;

    invoke-direct {v0, p0, p1}, LakD;-><init>(Ljava/lang/Object;LakG;)V

    return-object v0
.end method

.method public static a(LakD;I)LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LakD",
            "<+TT;>;I)",
            "LbmF",
            "<",
            "LakD",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 121
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 122
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    .line 123
    :goto_1
    if-ge v1, p1, :cond_1

    .line 124
    invoke-static {p0}, LakD;->a(LakD;)LakD;

    move-result-object v2

    invoke-virtual {v0, v2}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 123
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 121
    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, LakD;->a:LakE;

    invoke-virtual {v0}, LakE;->a()Ljava/lang/Object;

    move-result-object v0

    .line 141
    iget-object v1, p0, LakD;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, LakD;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, LakD;->a:LakE;

    invoke-virtual {v0}, LakE;->a()V

    .line 149
    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 4

    .prologue
    .line 154
    :try_start_0
    iget-object v0, p0, LakD;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    const-string v0, "CloseableReference"

    const-string v1, "CloseableReference was not released."

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    const-string v1, "CloseableReference"

    const-string v2, "An error occured in finalizer."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
