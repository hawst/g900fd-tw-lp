.class public LsZ;
.super Ljava/lang/Object;
.source "DocumentOpenerActivityDelegate.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LFV;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Ljava/lang/String;Ljava/lang/String;LFV;)V
    .locals 0

    .prologue
    .line 596
    iput-object p1, p0, LsZ;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iput-object p2, p0, LsZ;->a:Ljava/lang/String;

    iput-object p3, p0, LsZ;->b:Ljava/lang/String;

    iput-object p4, p0, LsZ;->a:LFV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 599
    iget-object v0, p0, LsZ;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, LsZ;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    .line 601
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v2

    .line 602
    iget-object v0, p0, LsZ;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a()LM;

    move-result-object v0

    iget-object v1, p0, LsZ;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    .line 603
    invoke-static {v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    iget-object v3, p0, LsZ;->a:Ljava/lang/String;

    iget-object v4, p0, LsZ;->b:Ljava/lang/String;

    iget-object v5, p0, LsZ;->a:LFV;

    .line 607
    invoke-virtual {v5}, LFV;->b()Z

    move-result v5

    .line 602
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 613
    :goto_0
    return-void

    .line 611
    :cond_0
    iget-object v0, p0, LsZ;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    goto :goto_0
.end method
