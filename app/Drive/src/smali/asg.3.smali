.class public Lasg;
.super Landroid/webkit/WebViewClient;
.source "Page.java"


# instance fields
.field final synthetic a:Lasq;

.field final synthetic a:Lcom/google/android/apps/docs/welcome/Page;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/welcome/Page;Lasq;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lasg;->a:Lcom/google/android/apps/docs/welcome/Page;

    iput-object p2, p0, Lasg;->a:Lasq;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 193
    iget-object v0, p0, Lasg;->a:Lasq;

    invoke-virtual {v0}, Lasq;->onWindowLoaded()V

    .line 195
    iget-object v0, p0, Lasg;->a:Lcom/google/android/apps/docs/welcome/Page;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/Page;->a(Lcom/google/android/apps/docs/welcome/Page;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lasg;->a:Lcom/google/android/apps/docs/welcome/Page;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/Page;->a(Lcom/google/android/apps/docs/welcome/Page;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LUs;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lasg;->a:Lcom/google/android/apps/docs/welcome/Page;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/Page;->a(Lcom/google/android/apps/docs/welcome/Page;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lash;

    invoke-direct {v1, p0}, Lash;-><init>(Lasg;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 206
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 181
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 183
    :try_start_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 184
    :catch_0
    move-exception v0

    goto :goto_0
.end method
