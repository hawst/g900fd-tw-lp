.class public abstract Lbng;
.super Lbnj;
.source "ImmutableSortedMap.java"

# interfaces
.implements Ljava/util/SortedMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lbnj",
        "<TK;TV;>;",
        "Ljava/util/SortedMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final a:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Comparable;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lbpw;->d()Lbpw;

    move-result-object v0

    sput-object v0, Lbng;->a:Ljava/util/Comparator;

    .line 67
    new-instance v0, Lbmi;

    sget-object v1, Lbng;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lbmi;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lbng;->a:Lbng;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0}, Lbnj;-><init>()V

    return-void
.end method

.method public static a()Lbng;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lbng;->a:Lbng;

    return-object v0
.end method

.method static a(Lbnk;LbmF;)Lbng;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbnk",
            "<TK;>;",
            "LbmF",
            "<TV;>;)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p0}, Lbnk;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lbnk;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lbng;->a(Ljava/util/Comparator;)Lbng;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LbpM;

    check-cast p0, LbpQ;

    invoke-direct {v0, p0, p1}, LbpM;-><init>(LbpQ;LbmF;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Comparable;Ljava/lang/Object;)Lbng;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K::",
            "Ljava/lang/Comparable",
            "<-TK;>;V:",
            "Ljava/lang/Object;",
            ">(TK;TV;)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 118
    invoke-static {p0}, Lbnk;->a(Ljava/lang/Comparable;)Lbnk;

    move-result-object v0

    invoke-static {p1}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v1

    invoke-static {v0, v1}, Lbng;->a(Lbnk;LbmF;)Lbng;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Comparator;)Lbng;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TK;>;)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {}, Lbpw;->d()Lbpw;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-static {}, Lbng;->a()Lbng;

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbmi;

    invoke-direct {v0, p0}, Lbmi;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method static a(Ljava/util/Comparator;Ljava/util/Collection;)Lbng;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TK;>;",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;>;)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 80
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-static {p0}, Lbng;->a(Ljava/util/Comparator;)Lbng;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    .line 84
    :cond_0
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 85
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v2

    .line 86
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 87
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 88
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_1

    .line 91
    :cond_1
    new-instance v0, LbpM;

    new-instance v3, LbpQ;

    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v1

    invoke-direct {v3, v1, p0}, LbpQ;-><init>(LbmF;Ljava/util/Comparator;)V

    invoke-virtual {v2}, LbmH;->a()LbmF;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LbpM;-><init>(LbpQ;LbmF;)V

    goto :goto_0
.end method

.method public static a()Lbni;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K::",
            "Ljava/lang/Comparable",
            "<*>;V:",
            "Ljava/lang/Object;",
            ">()",
            "Lbni",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 303
    new-instance v0, Lbni;

    invoke-static {}, Lbpw;->d()Lbpw;

    move-result-object v1

    invoke-direct {v0, v1}, Lbni;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method static synthetic a(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 59
    invoke-static {p0, p1}, Lbng;->c(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method static synthetic b(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 59
    invoke-static {p0, p1}, Lbng;->d(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method private static c(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;",
            "Ljava/util/Comparator",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    .line 277
    new-instance v0, Lbnh;

    invoke-direct {v0, p1}, Lbnh;-><init>(Ljava/util/Comparator;)V

    .line 284
    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 285
    return-void
.end method

.method private static d(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;",
            "Ljava/util/Comparator",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    .line 289
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 290
    add-int/lit8 v0, v1, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    .line 291
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duplicate keys in mappings "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, -0x1

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 289
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 295
    :cond_1
    return-void
.end method


# virtual methods
.method public a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 437
    invoke-super {p0}, Lbnj;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()Lbmv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbmv",
            "<TV;>;"
        }
    .end annotation
.end method

.method public a(Ljava/lang/Object;)Lbng;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 484
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbng;->a(Ljava/lang/Object;Z)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lbng;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TK;)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 517
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lbng;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;Z)Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lbng;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ZTK;Z)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 538
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    invoke-virtual {p0}, Lbng;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expected fromKey <= toKey but %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 542
    invoke-virtual {p0, p3, p4}, Lbng;->a(Ljava/lang/Object;Z)Lbng;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbng;->b(Ljava/lang/Object;Z)Lbng;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 540
    goto :goto_0
.end method

.method public abstract a()Lbnk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbnk",
            "<TK;>;"
        }
    .end annotation
.end method

.method public a()Ljava/util/Map$Entry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 613
    invoke-virtual {p0}, Lbng;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lbng;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()LbmF;

    move-result-object v0

    invoke-virtual {p0}, Lbng;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 428
    invoke-virtual {p0}, Lbng;->a()Lbnk;

    move-result-object v0

    invoke-virtual {v0}, Lbnk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbng;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0}, Lbmv;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;)Lbng;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 557
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lbng;->b(Ljava/lang/Object;Z)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(Ljava/lang/Object;Z)Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public synthetic c()LbmY;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbng;->a()Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 461
    invoke-virtual {p0}, Lbng;->a()Lbnk;

    move-result-object v0

    invoke-virtual {v0}, Lbnk;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 423
    invoke-virtual {p0}, Lbng;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbmv;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbng;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 465
    invoke-virtual {p0}, Lbng;->a()Lbnk;

    move-result-object v0

    invoke-virtual {v0}, Lbnk;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lbng;->a(Ljava/lang/Object;)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbng;->a()Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 469
    invoke-virtual {p0}, Lbng;->a()Lbnk;

    move-result-object v0

    invoke-virtual {v0}, Lbnk;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p0}, Lbng;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0}, Lbmv;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0, p1, p2}, Lbng;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lbng;->b(Ljava/lang/Object;)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbng;->a()Lbmv;

    move-result-object v0

    return-object v0
.end method
