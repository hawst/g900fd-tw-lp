.class public LXD;
.super Ljava/lang/Object;
.source "PunchModelImpl.java"

# interfaces
.implements LXJ;


# instance fields
.field private a:I

.field private a:LXE;

.field private a:Ljava/lang/Boolean;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LXF;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LXA;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private b:Ljava/lang/Boolean;

.field private b:Ljava/lang/String;

.field private c:I

.field private c:Ljava/lang/String;

.field private d:I

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LXD;->a:Ljava/util/Set;

    .line 75
    iput v1, p0, LXD;->a:I

    .line 79
    iput v1, p0, LXD;->d:I

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, LXD;->a:Z

    return-void
.end method

.method private a(I)LXF;
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, LbiT;->b(II)I

    .line 181
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXF;

    .line 182
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LXF;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, LXD;->b:I

    return v0
.end method

.method public a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, LXD;->a:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, LXD;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0, p1}, LXD;->a(I)LXF;

    move-result-object v0

    .line 222
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LXF;->a(LXF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method a()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 256
    invoke-interface {v0}, LXA;->b()V

    goto :goto_0

    .line 258
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, LbiT;->b(II)I

    .line 235
    iget-object v0, p0, LXD;->a:LXE;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, LXD;->a:LXE;

    invoke-interface {v0, p1}, LXE;->a(I)V

    .line 238
    :cond_0
    return-void
.end method

.method public a(ILXB;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, LXD;->a:LXE;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, LXD;->a:LXE;

    invoke-interface {v0, p1, p2}, LXE;->a(ILXB;)V

    .line 196
    :cond_0
    return-void
.end method

.method a(ILXF;)V
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXF;

    .line 358
    if-nez v0, :cond_0

    .line 359
    iget v0, p0, LXD;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LXD;->d:I

    .line 362
    :cond_0
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 363
    invoke-virtual {p2}, LXF;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 364
    invoke-virtual {p0, p1}, LXD;->b(I)V

    .line 367
    :cond_1
    iget v0, p0, LXD;->d:I

    iget-object v1, p0, LXD;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 368
    invoke-virtual {p0}, LXD;->g()V

    .line 370
    :cond_2
    return-void
.end method

.method public a(LXA;)V
    .locals 1

    .prologue
    .line 116
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 118
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 119
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LXB;)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, LXD;->a:LXE;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, LXD;->a:LXE;

    invoke-interface {v0, p1}, LXE;->a(LXB;)V

    .line 203
    :cond_0
    return-void
.end method

.method public a(LXE;)V
    .locals 0

    .prologue
    .line 91
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iput-object p1, p0, LXD;->a:LXE;

    .line 93
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 298
    iput-object p1, p0, LXD;->b:Ljava/lang/String;

    .line 299
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 300
    invoke-interface {v0, p1}, LXA;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;IIII)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 327
    if-ge p5, p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 329
    iput-object p1, p0, LXD;->a:Ljava/lang/String;

    .line 330
    iput p5, p0, LXD;->e:I

    .line 331
    iput p4, p0, LXD;->a:I

    .line 332
    iput p2, p0, LXD;->b:I

    .line 333
    iput p3, p0, LXD;->c:I

    .line 335
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LXD;->a:Ljava/util/List;

    move v0, v1

    .line 336
    :goto_1
    if-ge v0, p4, :cond_1

    .line 337
    iget-object v2, p0, LXD;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 327
    goto :goto_0

    .line 340
    :cond_1
    iput v1, p0, LXD;->d:I

    .line 342
    invoke-virtual {p0}, LXD;->f()V

    .line 343
    invoke-virtual {p0}, LXD;->b()V

    .line 344
    invoke-virtual {p0}, LXD;->c()V

    .line 346
    iget v0, p0, LXD;->e:I

    if-ltz v0, :cond_2

    .line 347
    invoke-virtual {p0}, LXD;->d()V

    .line 349
    :cond_2
    return-void
.end method

.method public a(ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 417
    const/4 v0, 0x0

    .line 418
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, LXD;->b:Ljava/lang/Boolean;

    .line 419
    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 420
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LXD;->b:Ljava/lang/Boolean;

    move v0, v1

    .line 424
    :cond_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, LXD;->a:Ljava/lang/Boolean;

    .line 425
    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 426
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LXD;->a:Ljava/lang/Boolean;

    .line 430
    :goto_0
    if-eqz v1, :cond_1

    .line 431
    invoke-virtual {p0}, LXD;->a()V

    .line 433
    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, LXD;->a:Z

    return v0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0, p1}, LXD;->a(I)LXF;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, LXD;->c:I

    return v0
.end method

.method public b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, LXD;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, LXD;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0, p1}, LXD;->a(I)LXF;

    move-result-object v0

    .line 228
    invoke-static {v0}, LXF;->b(LXF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 262
    invoke-interface {v0}, LXA;->a()V

    goto :goto_0

    .line 264
    :cond_0
    return-void
.end method

.method b(I)V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 274
    invoke-interface {v0, p1}, LXA;->a(I)V

    goto :goto_0

    .line 276
    :cond_0
    return-void
.end method

.method public b(LXA;)V
    .locals 1

    .prologue
    .line 123
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 125
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 126
    return-void
.end method

.method public b(LXB;)V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, LXD;->a:LXE;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, LXD;->a:LXE;

    invoke-interface {v0, p1}, LXE;->b(LXB;)V

    .line 210
    :cond_0
    return-void
.end method

.method public b(LXE;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, LXD;->a:LXE;

    if-ne v0, p1, :cond_0

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, LXD;->a:LXE;

    .line 112
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 306
    iput-object p1, p0, LXD;->c:Ljava/lang/String;

    .line 307
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 308
    invoke-interface {v0, p1}, LXA;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_0
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, LXD;->a:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, LXD;->c:Ljava/lang/String;

    return-object v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 268
    invoke-interface {v0}, LXA;->d()V

    goto :goto_0

    .line 270
    :cond_0
    return-void
.end method

.method c(I)V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    iget-object v0, p0, LXD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p1, v0}, LbiT;->a(II)I

    .line 396
    iget v0, p0, LXD;->e:I

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    .line 397
    :goto_0
    iput p1, p0, LXD;->e:I

    .line 401
    invoke-virtual {p0}, LXD;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 402
    if-eqz v0, :cond_2

    .line 403
    invoke-virtual {p0}, LXD;->d()V

    .line 407
    :cond_0
    :goto_1
    return-void

    .line 396
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 404
    :cond_2
    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {p0}, LXD;->e()V

    goto :goto_1
.end method

.method public c(LXB;)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, LXD;->a:LXE;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, LXD;->a:LXE;

    invoke-interface {v0, p1}, LXE;->c(LXB;)V

    .line 217
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 314
    iput-object p1, p0, LXD;->d:Ljava/lang/String;

    .line 315
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 316
    invoke-interface {v0, p1}, LXA;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 318
    :cond_0
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, LXD;->e:I

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, LXD;->d:Ljava/lang/String;

    return-object v0
.end method

.method d()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 280
    invoke-interface {v0}, LXA;->e()V

    goto :goto_0

    .line 282
    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, LXD;->a:Ljava/lang/String;

    .line 440
    invoke-virtual {p0}, LXD;->b()V

    .line 441
    return-void
.end method

.method e()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 286
    invoke-interface {v0}, LXA;->f()V

    goto :goto_0

    .line 288
    :cond_0
    return-void
.end method

.method f()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, LXD;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXA;

    .line 292
    invoke-interface {v0}, LXA;->c()V

    goto :goto_0

    .line 294
    :cond_0
    return-void
.end method

.method g()V
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    iput-boolean v0, p0, LXD;->a:Z

    .line 377
    return-void
.end method
