.class public LanU;
.super Ljava/lang/Object;
.source "ChainedImageDownloadFetcher.java"


# instance fields
.field private final a:LQr;

.field private final a:LZS;

.field private final a:LakR;

.field private final a:LaoG;

.field private final a:LaoJ;

.field private final a:Laom;

.field private final a:Lapl;

.field private final b:LZS;


# direct methods
.method public constructor <init>(LQr;LaoJ;Laom;Lapl;LaoG;LakR;LZS;LZS;)V
    .locals 0
    .param p6    # LakR;
        .annotation runtime LaoC;
        .end annotation
    .end param
    .param p7    # LZS;
        .annotation runtime LaoD;
        .end annotation
    .end param
    .param p8    # LZS;
        .annotation runtime LaoE;
        .end annotation
    .end param

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, LanU;->a:LQr;

    .line 85
    iput-object p2, p0, LanU;->a:LaoJ;

    .line 86
    iput-object p3, p0, LanU;->a:Laom;

    .line 87
    iput-object p4, p0, LanU;->a:Lapl;

    .line 88
    iput-object p5, p0, LanU;->a:LaoG;

    .line 89
    iput-object p6, p0, LanU;->a:LakR;

    .line 90
    iput-object p8, p0, LanU;->b:LZS;

    .line 91
    iput-object p7, p0, LanU;->a:LZS;

    .line 92
    return-void
.end method

.method static synthetic a(LanU;)LZS;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LanU;->a:LZS;

    return-object v0
.end method

.method static synthetic a(LanU;)LakR;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LanU;->a:LakR;

    return-object v0
.end method

.method static synthetic a(LanU;)LaoG;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LanU;->a:LaoG;

    return-object v0
.end method

.method static synthetic a(LanU;)LaoJ;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LanU;->a:LaoJ;

    return-object v0
.end method

.method static synthetic a(LanU;)Laom;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LanU;->a:Laom;

    return-object v0
.end method

.method static synthetic a(LanU;)Lapl;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LanU;->a:Lapl;

    return-object v0
.end method

.method static synthetic b(LanU;)LZS;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LanU;->b:LZS;

    return-object v0
.end method


# virtual methods
.method public a()LanS;
    .locals 6

    .prologue
    .line 230
    new-instance v0, LanV;

    const-string v2, "storageApiThumbnail"

    sget-object v3, LanX;->a:LanX;

    const-string v4, "storageApiThumbnailFetcher"

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LanV;-><init>(LanU;Ljava/lang/String;LanX;Ljava/lang/String;I)V

    .line 234
    invoke-virtual {v0}, LanV;->a()LanS;

    move-result-object v0

    return-object v0
.end method

.method public b()LanS;
    .locals 6

    .prologue
    .line 242
    iget-object v0, p0, LanU;->a:LQr;

    const-string v1, "maxNumberOfThumbnailDownloadRetries"

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v5

    .line 246
    new-instance v0, LanV;

    const-string v2, "previewImage"

    sget-object v3, LanX;->b:LanX;

    const-string v4, "documentPreview"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LanV;-><init>(LanU;Ljava/lang/String;LanX;Ljava/lang/String;I)V

    .line 250
    invoke-virtual {v0}, LanV;->a()LanS;

    move-result-object v0

    return-object v0
.end method
