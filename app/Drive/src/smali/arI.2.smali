.class public LarI;
.super Ljava/lang/Object;
.source "CompoundCascadingAnimator.java"

# interfaces
.implements LarE;


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "LarE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbmY;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LarE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, LarI;->a:LbmY;

    .line 20
    return-void
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 39
    iget-object v0, p0, LarI;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarE;

    .line 40
    invoke-interface {v0}, LarE;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 42
    :cond_0
    return-object v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 32
    invoke-virtual {p0}, LarI;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 33
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 34
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, LarI;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarE;

    .line 25
    invoke-interface {v0, p1}, LarE;->a(Landroid/view/View;)V

    goto :goto_0

    .line 27
    :cond_0
    return-void
.end method
