.class public LahA;
.super Ljava/lang/Object;
.source "ContentSyncModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 62
    const-class v0, Lahy;

    const-class v1, LQR;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 64
    return-void
.end method

.method provideContentSyncInformation(LahL;LbiP;)Lahy;
    .locals 1
    .param p2    # LbiP;
        .annotation runtime LQR;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LahL;",
            "LbiP",
            "<",
            "Lahy;",
            ">;)",
            "Lahy;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p2, p1}, LbiP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahy;

    return-object v0
.end method

.method provideContentSyncScheduler(LahC;)LahB;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 49
    return-object p1
.end method

.method provideSyncRequestModelLoader(LaGg;)LahO;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 43
    return-object p1
.end method

.method provideSyncTaskQueue(LahL;)LahK;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 30
    return-object p1
.end method

.method provideTransactionSafeContentSyncScheduler(LahC;)LahT;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 37
    return-object p1
.end method
