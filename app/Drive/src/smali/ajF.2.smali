.class LajF;
.super Ljava/lang/Object;
.source "WrappedBinder.java"

# interfaces
.implements Lcom/google/inject/Binder;


# instance fields
.field private final a:Lcom/google/inject/Binder;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbuv",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/inject/Binder;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LajF;->a:Ljava/util/Set;

    .line 38
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/inject/Binder;

    iput-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Lbuv;)LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuE;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Ljava/lang/Class;)LbuE;

    move-result-object v0

    return-object v0
.end method

.method public a(LbuP;)LbuQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, LajE;

    iget-object v1, p0, LajF;->a:Ljava/util/Set;

    iget-object v2, p0, LajF;->a:Lcom/google/inject/Binder;

    .line 77
    invoke-interface {v2, p1}, Lcom/google/inject/Binder;->a(LbuP;)LbuQ;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LajE;-><init>(LbuP;Ljava/util/Set;LbuQ;)V

    return-object v0
.end method

.method public a(Ljava/lang/Class;)LbuQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, LajE;

    invoke-static {p1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    iget-object v2, p0, LajF;->a:Ljava/util/Set;

    iget-object v3, p0, LajF;->a:Lcom/google/inject/Binder;

    .line 63
    invoke-interface {v3, p1}, Lcom/google/inject/Binder;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LajE;-><init>(LbuP;Ljava/util/Set;LbuQ;)V

    return-object v0
.end method

.method public a()LbuR;
    .locals 3

    .prologue
    .line 82
    new-instance v0, LajG;

    iget-object v1, p0, LajF;->a:Lcom/google/inject/Binder;

    invoke-interface {v1}, Lcom/google/inject/Binder;->a()LbuR;

    move-result-object v1

    iget-object v2, p0, LajF;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, LajG;-><init>(LbuR;Ljava/util/Set;)V

    return-object v0
.end method

.method public a(Lbuv;)LbuT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 69
    :cond_0
    iget-object v0, p0, LajF;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_1
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lcom/google/inject/Binder;
    .locals 0

    .prologue
    .line 195
    return-object p0
.end method

.method public varargs a([Ljava/lang/Class;)Lcom/google/inject/Binder;
    .locals 0

    .prologue
    .line 190
    return-object p0
.end method

.method a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lbuv",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, LajF;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(LbuC;)V
    .locals 1

    .prologue
    .line 137
    invoke-interface {p1, p0}, LbuC;->a(Lcom/google/inject/Binder;)V

    .line 138
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    instance-of v0, v0, LbrB;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    check-cast v0, LbrB;

    invoke-interface {v0, p1}, LbrB;->b(LbuC;)V

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-static {p1}, Lbvz;->a(LbuC;)LbuC;

    move-result-object v0

    invoke-interface {v0, p0}, LbuC;->a(Lcom/google/inject/Binder;)V

    goto :goto_0
.end method

.method public a(Lbwy;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Lbwy;)V

    .line 48
    return-void
.end method

.method public a(Ljava/lang/Class;LbuH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LbuH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    invoke-interface {v0, p1, p2}, Lcom/google/inject/Binder;->a(Ljava/lang/Class;LbuH;)V

    .line 98
    return-void
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LajF;->a:Lcom/google/inject/Binder;

    invoke-interface {v0, p1, p2}, Lcom/google/inject/Binder;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    return-void
.end method
