.class public abstract LafC;
.super Ljava/lang/Object;
.source "AbstractSyncable.java"

# interfaces
.implements Lahm;


# instance fields
.field protected final a:LagH;

.field private final a:LahJ;


# direct methods
.method public constructor <init>(LahJ;LagH;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahJ;

    iput-object v0, p0, LafC;->a:LahJ;

    .line 33
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagH;

    iput-object v0, p0, LafC;->a:LagH;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()LahJ;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, LafC;->a:LahJ;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LafC;->a:LahJ;

    invoke-interface {v0}, LahJ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Exception;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, LafC;->a:LagH;

    sget-object v1, LafT;->i:LafT;

    invoke-interface {v0, v1}, LagH;->a(LafT;)V

    .line 82
    new-instance v0, LafD;

    invoke-direct {v0, p1}, LafD;-><init>(Ljava/lang/Exception;)V

    return-object v0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LafC;->a:LagH;

    invoke-interface {v0}, LagH;->c()V

    .line 62
    return-void
.end method

.method protected a(JJ)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, LafC;->a:LagH;

    invoke-interface {v0, p1, p2, p3, p4}, LagH;->a(JJ)V

    .line 70
    return-void
.end method

.method protected a(LafT;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, LafC;->a:LagH;

    invoke-interface {v0, p1}, LagH;->a(LafT;)V

    .line 78
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LafC;->a:LagH;

    invoke-interface {v0}, LagH;->a()V

    .line 66
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, LafC;->a:LagH;

    invoke-interface {v0}, LagH;->b()V

    .line 74
    return-void
.end method
