.class public Lwj;
.super Ljava/lang/Object;
.source "MimeTypeFilter.java"


# direct methods
.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 5

    .prologue
    .line 73
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {}, Lwk;->values()[Lwk;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 75
    invoke-static {v3, p0}, Lwk;->a(Lwk;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    invoke-virtual {v3}, Lwk;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 80
    :goto_1
    return-object v0

    .line 74
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_1
    invoke-static {p0}, Lwj;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaES;->r:LaES;

    .line 59
    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "text/html"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    invoke-static {}, Lwk;->values()[Lwk;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v3, v4, v1

    .line 62
    sget-object v6, LaFL;->a:LaFL;

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-static {v3}, Lwk;->a(Lwk;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v3

    aput-object v3, v7, v2

    invoke-virtual {v6, v0, v7}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v3

    .line 61
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto :goto_0

    .line 66
    :cond_0
    return-object v0
.end method
