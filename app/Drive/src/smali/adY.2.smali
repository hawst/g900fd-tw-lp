.class public LadY;
.super Ljava/lang/Object;
.source "ReadOnlyFile.java"


# instance fields
.field private final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LadY;->a:Ljava/io/File;

    .line 24
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LbmY;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "LadY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, LadY;->b()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 60
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 61
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v2

    .line 62
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 63
    new-instance v5, LadY;

    invoke-direct {v5, v4}, LadY;-><init>(Ljava/io/File;)V

    invoke-virtual {v2, v5}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    invoke-virtual {v2}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, LadY;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-static {v0}, LadF;->b(Ljava/io/File;)J

    move-result-wide v0

    .line 38
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 70
    if-ne p0, p1, :cond_0

    .line 71
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    .line 73
    :cond_0
    instance-of v0, p1, LadY;

    if-nez v0, :cond_1

    .line 74
    const/4 v0, 0x0

    goto :goto_0

    .line 76
    :cond_1
    check-cast p1, LadY;

    .line 77
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    iget-object v1, p1, LadY;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, LadY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->hashCode()I

    move-result v0

    return v0
.end method
