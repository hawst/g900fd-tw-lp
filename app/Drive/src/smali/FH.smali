.class LFH;
.super LaGN;
.source "DocListDocumentCreatorImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LFG;

.field final synthetic a:Ljava/util/List;


# direct methods
.method constructor <init>(LFG;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, LFH;->a:LFG;

    iput-object p2, p0, LFH;->a:Ljava/util/List;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, LFH;->a:LFG;

    iget-object v1, p0, LFH;->a:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, LFG;->a(LaGM;Ljava/util/List;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1}, LFH;->a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, LFH;->a:LFG;

    invoke-static {v0}, LFG;->a(LFG;)LsI;

    move-result-object v0

    iget-object v1, p0, LFH;->a:LFG;

    .line 77
    invoke-static {v1}, LFG;->a(LFG;)LaFO;

    move-result-object v1

    const/4 v2, 0x0

    .line 76
    invoke-interface {v0, v1, v2, p1}, LsI;->a(LaFO;LaGv;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 78
    iget-object v1, p0, LFH;->a:LFG;

    invoke-static {v1}, LFG;->a(LFG;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 79
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, p1}, LFH;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method
