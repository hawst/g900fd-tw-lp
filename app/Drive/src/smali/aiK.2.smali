.class LaiK;
.super Ljava/lang/Object;
.source "ActivitySessionScopeImpl.java"

# interfaces
.implements LbuE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbuE",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LaiH;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaiH;LbuE;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuE",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 54
    iput-object p1, p0, LaiK;->a:LaiH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    iput-object v0, p0, LaiK;->a:LbuE;

    .line 56
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, LaiK;->a:LaiH;

    invoke-static {v0}, LaiH;->a(LaiH;)LaiU;

    move-result-object v0

    invoke-interface {v0}, LaiU;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiW;

    .line 61
    const/4 v1, 0x0

    .line 63
    if-eqz v0, :cond_2

    .line 64
    invoke-virtual {v0}, LaiW;->a()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, LaiJ;

    if-nez v2, :cond_2

    .line 65
    new-instance v1, LaiW;

    new-instance v2, LaiJ;

    iget-object v3, p0, LaiK;->a:LaiH;

    .line 66
    invoke-static {v3}, LaiH;->a(LaiH;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LaiK;->a:LaiH;

    invoke-virtual {v4}, LaiH;->a()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LaiJ;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, LaiK;->a:LaiH;

    .line 67
    invoke-virtual {v3}, LaiH;->a()LajA;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LaiW;-><init>(Landroid/content/Context;LajA;)V

    .line 68
    iget-object v2, p0, LaiK;->a:LaiH;

    invoke-static {v2}, LaiH;->a(LaiH;)LaiU;

    move-result-object v2

    invoke-interface {v2, v0}, LaiU;->b(Ljava/lang/Object;)V

    .line 69
    iget-object v2, p0, LaiK;->a:LaiH;

    invoke-static {v2}, LaiH;->a(LaiH;)LaiU;

    move-result-object v2

    invoke-interface {v2, v1}, LaiU;->a(Ljava/lang/Object;)V

    move-object v2, v1

    .line 73
    :goto_0
    :try_start_0
    iget-object v1, p0, LaiK;->a:LbuE;

    invoke-interface {v1}, LbuE;->a()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 75
    if-eqz v2, :cond_0

    .line 76
    iget-object v3, p0, LaiK;->a:LaiH;

    invoke-static {v3}, LaiH;->a(LaiH;)LaiU;

    move-result-object v3

    invoke-interface {v3, v2}, LaiU;->b(Ljava/lang/Object;)V

    .line 77
    iget-object v2, p0, LaiK;->a:LaiH;

    invoke-static {v2}, LaiH;->a(LaiH;)LaiU;

    move-result-object v2

    invoke-interface {v2, v0}, LaiU;->a(Ljava/lang/Object;)V

    :cond_0
    return-object v1

    .line 75
    :catchall_0
    move-exception v1

    if-eqz v2, :cond_1

    .line 76
    iget-object v3, p0, LaiK;->a:LaiH;

    invoke-static {v3}, LaiH;->a(LaiH;)LaiU;

    move-result-object v3

    invoke-interface {v3, v2}, LaiU;->b(Ljava/lang/Object;)V

    .line 77
    iget-object v2, p0, LaiK;->a:LaiH;

    invoke-static {v2}, LaiH;->a(LaiH;)LaiU;

    move-result-object v2

    invoke-interface {v2, v0}, LaiU;->a(Ljava/lang/Object;)V

    :cond_1
    throw v1

    :cond_2
    move-object v2, v1

    goto :goto_0
.end method
