.class public Lwu;
.super Ljava/lang/Object;
.source "SqlWhereClauseCriterionVisitor.java"

# interfaces
.implements LvR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LvR",
        "<",
        "Lwv;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LCl;

.field private a:LaFM;

.field private final a:LaGg;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>(LaGg;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lwu;->a:Z

    .line 54
    iput-object p1, p0, Lwu;->a:LaGg;

    .line 55
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lwu;->a:Ljava/util/List;

    .line 56
    return-void
.end method


# virtual methods
.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lwu;->a()Lwv;

    move-result-object v0

    return-object v0
.end method

.method public a()Lwv;
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lwu;->a:LaFM;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 136
    iget-boolean v0, p0, Lwu;->a:Z

    if-nez v0, :cond_1

    .line 137
    new-instance v0, Lwv;

    iget-object v1, p0, Lwu;->a:LaFM;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lwv;-><init>(LaFO;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)V

    .line 143
    :goto_1
    return-object v0

    .line 135
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :cond_1
    iget-object v0, p0, Lwu;->a:LCl;

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    iget-object v1, p0, Lwu;->a:LCl;

    iget-object v2, p0, Lwu;->a:LaFM;

    invoke-interface {v1, v2}, LCl;->a(LaFM;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_2
    new-instance v0, Lwv;

    iget-object v1, p0, Lwu;->a:LaFM;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    sget-object v2, LaFL;->a:LaFL;

    iget-object v3, p0, Lwu;->a:Ljava/util/List;

    invoke-virtual {v2, v3}, LaFL;->a(Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lwv;-><init>(LaFO;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)V

    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    sget-object v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method public a(LCl;Z)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lwu;->a:LCl;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 68
    iput-object p1, p0, Lwu;->a:LCl;

    .line 69
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaFO;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lwu;->a:LaFM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lwu;->a:LaFM;

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-virtual {p1, v0}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 61
    iget-object v0, p0, Lwu;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    iput-object v0, p0, Lwu;->a:LaFM;

    .line 62
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    iget-object v1, p0, Lwu;->a:LaFM;

    invoke-static {v1}, LDK;->a(LaFM;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    return-void

    .line 60
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Laay;)V
    .locals 2

    .prologue
    .line 78
    invoke-static {p1}, LDK;->a(Laay;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lwu;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public a(LbmY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    invoke-static {p1}, LDK;->a(LbmY;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public a(LbmY;LbmY;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    invoke-static {p1, p2, p3}, LDK;->a(LbmY;LbmY;Z)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public a(LbmY;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    invoke-static {p1, p2}, LDK;->a(LbmY;Z)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lwu;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 85
    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lwu;->a:Z

    .line 90
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lwu;->a:Ljava/util/List;

    invoke-virtual {v0}, LaFZ;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, LDK;->a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-static {}, LaER;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    return-void
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 6

    .prologue
    .line 127
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 128
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {p0, v1}, Lwu;->a(LaFO;)V

    .line 129
    iget-object v1, p0, Lwu;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v4}, LaER;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    sget-object v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    .line 121
    iget-object v0, p0, Lwu;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaES;->H:LaES;

    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LaGw;->b:LaGw;

    .line 122
    invoke-virtual {v3}, LaGw;->a()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    return-void
.end method
