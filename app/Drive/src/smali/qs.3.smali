.class public Lqs;
.super Ljava/lang/Object;
.source "AclType.java"


# instance fields
.field private a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lqv;

.field private a:Lqx;

.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342
    iput-object v0, p0, Lqs;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 343
    iput-object v0, p0, Lqs;->a:Ljava/lang/String;

    .line 344
    sget-object v0, Lqx;->e:Lqx;

    iput-object v0, p0, Lqs;->a:Lqx;

    .line 345
    sget-object v0, Lqv;->f:Lqv;

    iput-object v0, p0, Lqs;->a:Lqv;

    .line 346
    const-class v0, Lqq;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Ljava/util/Set;

    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lqs;->a:Z

    .line 358
    return-void
.end method

.method private a(Lqt;Lqx;Z)Lqs;
    .locals 2

    .prologue
    .line 436
    invoke-virtual {p1}, Lqt;->a()Lqv;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Lqv;

    .line 437
    iput-object p2, p0, Lqs;->a:Lqx;

    .line 438
    iput-boolean p3, p0, Lqs;->a:Z

    .line 439
    iget-object v0, p0, Lqs;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 440
    iget-object v0, p0, Lqs;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lqt;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 441
    return-object p0
.end method


# virtual methods
.method public a()Lqo;
    .locals 7

    .prologue
    .line 429
    new-instance v0, Lqo;

    iget-object v1, p0, Lqs;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v2, p0, Lqs;->a:Ljava/lang/String;

    iget-object v3, p0, Lqs;->a:Lqx;

    iget-object v4, p0, Lqs;->a:Lqv;

    iget-object v5, p0, Lqs;->a:Ljava/util/Set;

    .line 430
    invoke-static {v4, v5}, Lqt;->a(Lqv;Ljava/util/Set;)Lqt;

    move-result-object v4

    iget-boolean v5, p0, Lqs;->a:Z

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lqo;-><init>(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Lqx;Lqt;ZLqp;)V

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lqs;
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lqs;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 362
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lqs;
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lqs;->a:Ljava/lang/String;

    .line 367
    return-object p0
.end method

.method public a(Ljava/util/Set;)Lqs;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;)",
            "Lqs;"
        }
    .end annotation

    .prologue
    .line 381
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    const-class v0, Lqq;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Ljava/util/Set;

    .line 386
    :goto_0
    return-object p0

    .line 384
    :cond_0
    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Ljava/util/Set;

    goto :goto_0
.end method

.method public a(Lqo;)Lqs;
    .locals 2

    .prologue
    .line 412
    invoke-static {p1}, Lqo;->a(Lqo;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 413
    invoke-static {p1}, Lqo;->a(Lqo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Ljava/lang/String;

    .line 414
    invoke-static {p1}, Lqo;->a(Lqo;)Lqx;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Lqx;

    .line 415
    invoke-virtual {p1}, Lqo;->a()Lqv;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Lqv;

    .line 416
    invoke-static {p1}, Lqo;->a(Lqo;)Z

    move-result v0

    iput-boolean v0, p0, Lqs;->a:Z

    .line 417
    iget-object v0, p0, Lqs;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 418
    iget-object v0, p0, Lqs;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lqo;->a()Lqt;

    move-result-object v1

    invoke-virtual {v1}, Lqt;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 419
    return-object p0
.end method

.method public a(Lqt;)Lqs;
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p1}, Lqt;->a()Lqv;

    move-result-object v0

    iput-object v0, p0, Lqs;->a:Lqv;

    .line 401
    invoke-virtual {p1}, Lqt;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lqs;->a(Ljava/util/Set;)Lqs;

    .line 402
    return-object p0
.end method

.method public a(Lqu;)Lqs;
    .locals 3

    .prologue
    .line 423
    invoke-virtual {p1}, Lqu;->a()Lqt;

    move-result-object v0

    .line 424
    invoke-virtual {p1}, Lqu;->a()Lqx;

    move-result-object v1

    .line 425
    invoke-virtual {p1}, Lqu;->a()Z

    move-result v2

    .line 423
    invoke-direct {p0, v0, v1, v2}, Lqs;->a(Lqt;Lqx;Z)Lqs;

    move-result-object v0

    return-object v0
.end method

.method public a(Lqv;)Lqs;
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lqs;->a:Lqv;

    .line 377
    return-object p0
.end method

.method public a(Lqx;)Lqs;
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lqs;->a:Lqx;

    .line 372
    return-object p0
.end method

.method public a(Z)Lqs;
    .locals 0

    .prologue
    .line 390
    iput-boolean p1, p0, Lqs;->a:Z

    .line 391
    return-object p0
.end method
