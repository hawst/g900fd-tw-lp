.class LaxG;
.super Laxy;
.source "PdfLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "LavJ;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Laxz;

.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(Laxz;)V
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LaxG;-><init>(Laxz;Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method constructor <init>(Laxz;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 305
    iput-object p1, p0, LaxG;->a:Laxz;

    .line 306
    sget-object v0, LaxR;->a:LaxR;

    invoke-direct {p0, p1, v0}, Laxy;-><init>(Laxz;LaxR;)V

    .line 307
    iput-object p2, p0, LaxG;->a:Ljava/lang/String;

    .line 308
    return-void
.end method


# virtual methods
.method protected a(LavE;)LavJ;
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, LaxG;->a:Laxz;

    invoke-static {v0}, Laxz;->a(Laxz;)LauW;

    move-result-object v0

    if-nez v0, :cond_0

    .line 314
    const-string v0, "PdfLoader"

    const-string v1, "Can\'t load file (data unavailable)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    sget-object v0, LavJ;->a:LavJ;

    .line 324
    :goto_0
    return-object v0

    .line 317
    :cond_0
    iget-object v0, p0, LaxG;->a:Laxz;

    invoke-static {v0}, Laxz;->a(Laxz;)LauW;

    move-result-object v0

    invoke-virtual {v0}, LauW;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 318
    if-nez v0, :cond_1

    .line 319
    const-string v0, "PdfLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t load file (doesn\'t open) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaxG;->a:Laxz;

    invoke-static {v2}, Laxz;->a(Laxz;)LauW;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    sget-object v0, LavJ;->a:LavJ;

    goto :goto_0

    .line 322
    :cond_1
    iget-object v1, p0, LaxG;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LavE;->a(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V

    .line 323
    invoke-static {}, LavJ;->values()[LavJ;

    move-result-object v0

    invoke-interface {p1}, LavE;->a()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 298
    invoke-virtual {p0, p1}, LaxG;->a(LavE;)LavJ;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;LavJ;)V
    .locals 2

    .prologue
    .line 329
    sget-object v0, LaxB;->a:[I

    invoke-virtual {p2}, LavJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 341
    :goto_0
    return-void

    .line 331
    :pswitch_0
    iget-object v0, p0, LaxG;->a:Laxz;

    invoke-virtual {v0}, Laxz;->a()I

    move-result v0

    invoke-interface {p1, v0}, LaxH;->a(I)V

    goto :goto_0

    .line 334
    :pswitch_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LaxH;->a(Z)V

    goto :goto_0

    .line 338
    :pswitch_2
    invoke-interface {p1}, LaxH;->a()V

    goto :goto_0

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 298
    check-cast p2, LavJ;

    invoke-virtual {p0, p1, p2}, LaxG;->a(LaxH;LavJ;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoadDocumentTask("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaxG;->a:Laxz;

    invoke-static {v1}, Laxz;->a(Laxz;)LauW;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
