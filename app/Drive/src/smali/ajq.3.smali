.class public abstract Lajq;
.super Landroid/content/BroadcastReceiver;
.source "GuiceBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 26
    :try_start_0
    invoke-static {p1}, LaiZ;->b(Landroid/content/Context;)LaiY;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 35
    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 36
    invoke-virtual {p0, p1, p2}, Lajq;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 37
    return-void

    .line 27
    :catch_0
    move-exception v0

    .line 30
    const-string v1, "GuiceBroadcastReceiver"

    const-string v2, "Dumping applicationInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    new-instance v1, Landroid/util/LogPrinter;

    const/4 v2, 0x6

    const-string v3, "GuiceBroadcastReceiver"

    invoke-direct {v1, v2, v3}, Landroid/util/LogPrinter;-><init>(ILjava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/ApplicationInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    .line 33
    throw v0
.end method
