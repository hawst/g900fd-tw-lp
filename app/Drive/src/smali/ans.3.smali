.class public abstract Lans;
.super Ljava/lang/Object;
.source "AbstractDirectExecutorFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoo",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<-TK;>;"
        }
    .end annotation
.end field

.field private final a:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Laop;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lant;

    invoke-direct {v0, p0}, Lant;-><init>(Lans;)V

    iput-object v0, p0, Lans;->a:Laoy;

    .line 33
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laop;

    iput-object v0, p0, Lans;->a:Laop;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lans;->a:Laoy;

    invoke-static {v0}, Laou;->a(Laoy;)Laou;

    move-result-object v0

    .line 39
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v1, p0, Lans;->a:Laop;

    new-instance v2, Lanu;

    invoke-direct {v2, p0, p1, v0}, Lanu;-><init>(Lans;Ljava/lang/Object;Laou;)V

    invoke-interface {v1, p1, v2}, Laop;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Laou;->b(LbsU;)V

    .line 50
    return-object v1
.end method

.method protected abstract a(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 66
    return-void
.end method
