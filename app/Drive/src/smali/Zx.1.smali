.class public final enum LZx;
.super Ljava/lang/Enum;
.source "PunchWebViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LZx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LZx;

.field private static final synthetic a:[LZx;

.field public static final enum b:LZx;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 133
    new-instance v0, LZx;

    const-string v1, "FULLSCREEN"

    const-string v2, "webViewPunchFullscreenDuration"

    invoke-direct {v0, v1, v3, v2}, LZx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LZx;->a:LZx;

    .line 134
    new-instance v0, LZx;

    const-string v1, "EMBEDDED"

    const-string v2, "webViewPunchEmbeddedDuration"

    invoke-direct {v0, v1, v4, v2}, LZx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LZx;->b:LZx;

    .line 132
    const/4 v0, 0x2

    new-array v0, v0, [LZx;

    sget-object v1, LZx;->a:LZx;

    aput-object v1, v0, v3

    sget-object v1, LZx;->b:LZx;

    aput-object v1, v0, v4

    sput-object v0, LZx;->a:[LZx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 139
    iput-object p3, p0, LZx;->a:Ljava/lang/String;

    .line 140
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, LZx;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(LZx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, LZx;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LZx;
    .locals 1

    .prologue
    .line 132
    const-class v0, LZx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LZx;

    return-object v0
.end method

.method public static values()[LZx;
    .locals 1

    .prologue
    .line 132
    sget-object v0, LZx;->a:[LZx;

    invoke-virtual {v0}, [LZx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LZx;

    return-object v0
.end method
