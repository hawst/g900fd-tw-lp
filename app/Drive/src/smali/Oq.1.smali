.class final enum LOq;
.super Ljava/lang/Enum;
.source "ApiaryPsynchoConfigurationAccessor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LOq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LOq;

.field private static final synthetic a:[LOq;

.field public static final enum b:LOq;

.field public static final enum c:LOq;

.field public static final enum d:LOq;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 50
    new-instance v0, LOq;

    const-string v1, "AUTO_ADD"

    const-string v2, "PSYNCHO"

    const-string v3, "ShowInMyDrive"

    invoke-direct {v0, v1, v4, v2, v3}, LOq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LOq;->a:LOq;

    .line 57
    new-instance v0, LOq;

    const-string v1, "PHOTO_BACKUP_ANNOUNCEMENT_WHITELIST"

    const-string v2, "FEATURE_SWITCH"

    const-string v3, "psyncho_auto_backup_promo"

    invoke-direct {v0, v1, v5, v2, v3}, LOq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LOq;->b:LOq;

    .line 63
    new-instance v0, LOq;

    const-string v1, "PHOTO_BACKUP_SETTING_WHITELIST"

    const-string v2, "FEATURE_SWITCH"

    const-string v3, "psyncho_auto_backup_whitelisted"

    invoke-direct {v0, v1, v6, v2, v3}, LOq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LOq;->c:LOq;

    .line 70
    new-instance v0, LOq;

    const-string v1, "PSYNCHO_UI_WHITELIST"

    const-string v2, "FEATURE_SWITCH"

    const-string v3, "psyncho_whitelisted"

    invoke-direct {v0, v1, v7, v2, v3}, LOq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LOq;->d:LOq;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [LOq;

    sget-object v1, LOq;->a:LOq;

    aput-object v1, v0, v4

    sget-object v1, LOq;->b:LOq;

    aput-object v1, v0, v5

    sget-object v1, LOq;->c:LOq;

    aput-object v1, v0, v6

    sget-object v1, LOq;->d:LOq;

    aput-object v1, v0, v7

    sput-object v0, LOq;->a:[LOq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LOq;->a:Ljava/lang/String;

    .line 77
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LOq;->b:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LOq;
    .locals 1

    .prologue
    .line 43
    const-class v0, LOq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LOq;

    return-object v0
.end method

.method public static values()[LOq;
    .locals 1

    .prologue
    .line 43
    sget-object v0, LOq;->a:[LOq;

    invoke-virtual {v0}, [LOq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LOq;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 82
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s SettingSpec[%s, %s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, LOq;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LOq;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LOq;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
