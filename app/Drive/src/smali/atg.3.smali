.class Latg;
.super LpD;
.source "WelcomeController.java"


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic a:Latd;

.field final synthetic a:Latn;


# direct methods
.method constructor <init>(Latd;Ljava/lang/String;Latn;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 655
    iput-object p1, p0, Latg;->a:Latd;

    iput-object p3, p0, Latg;->a:Latn;

    iput-object p4, p0, Latg;->a:Landroid/content/Context;

    invoke-direct {p0, p2}, LpD;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 657
    const/4 v0, 0x1

    .line 659
    :try_start_0
    new-instance v1, Lath;

    iget-object v2, p0, Latg;->a:Latd;

    invoke-static {v2}, Latd;->a(Latd;)Latm;

    move-result-object v2

    invoke-static {v2}, Latm;->b(Latm;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lath;-><init>(Ljava/lang/String;Late;)V

    .line 660
    iget-object v2, p0, Latg;->a:Latd;

    invoke-static {v2}, Latd;->a(Latd;)Latj;

    move-result-object v2

    invoke-interface {v2, v1}, Latj;->a(Lath;)LasP;

    move-result-object v1

    .line 661
    iget-object v2, p0, Latg;->a:Latd;

    invoke-static {v2}, Latd;->a(Latd;)Latl;

    move-result-object v2

    invoke-static {v2, v1}, Latl;->a(Latl;LasP;)LasP;

    .line 662
    sget-object v2, LasO;->c:LasO;

    invoke-static {v1, v2}, LasJ;->a(LasP;LasO;)LasJ;

    move-result-object v2

    .line 663
    invoke-virtual {p0}, Latg;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 664
    const-string v0, "WelcomeController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Canceling launch of announce "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Latg;->a:Latd;

    invoke-static {v2}, Latd;->a(Latd;)Latm;

    move-result-object v2

    invoke-static {v2}, Latm;->b(Latm;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    iget-object v0, p0, Latg;->a:Latd;

    invoke-static {v0}, Latd;->a(Latd;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0, p0, v4}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 679
    iget-object v0, p0, Latg;->a:Latd;

    invoke-virtual {v0}, Latd;->c()V

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 668
    :cond_1
    :try_start_1
    invoke-virtual {v2}, LasJ;->a()I

    move-result v3

    if-lez v3, :cond_2

    .line 669
    iget-object v0, p0, Latg;->a:Latn;

    iget-object v3, p0, Latg;->a:Latd;

    invoke-static {v3}, Latd;->a(Latd;)Latm;

    move-result-object v3

    invoke-static {v3}, Latm;->b(Latm;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Latn;->a(Latn;Ljava/lang/String;)Ljava/lang/String;

    .line 670
    iget-object v0, p0, Latg;->a:Landroid/content/Context;

    invoke-interface {v1}, LasP;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v2, v1}, Latd;->a(Landroid/content/Context;LasJ;Landroid/net/Uri;)V

    .line 671
    iget-object v0, p0, Latg;->a:Latd;

    invoke-static {v0}, Latd;->a(Latd;)Lato;

    move-result-object v0

    iget-object v1, p0, Latg;->a:Latn;

    invoke-virtual {v0, v1}, Lato;->a(Latn;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 672
    const/4 v0, 0x0

    .line 677
    :goto_1
    iget-object v1, p0, Latg;->a:Latd;

    invoke-static {v1}, Latd;->a(Latd;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1, p0, v4}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 678
    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Latg;->a:Latd;

    invoke-virtual {v0}, Latd;->c()V

    goto :goto_0

    .line 674
    :cond_2
    :try_start_2
    const-string v1, "WelcomeController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got an empty announce from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Latg;->a:Latd;

    invoke-static {v3}, Latd;->a(Latd;)Latm;

    move-result-object v3

    invoke-static {v3}, Latm;->b(Latm;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 677
    :catchall_0
    move-exception v0

    iget-object v1, p0, Latg;->a:Latd;

    invoke-static {v1}, Latd;->a(Latd;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1, p0, v4}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 679
    iget-object v1, p0, Latg;->a:Latd;

    invoke-virtual {v1}, Latd;->c()V

    throw v0
.end method
