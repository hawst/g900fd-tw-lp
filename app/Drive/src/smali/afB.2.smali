.class public LafB;
.super Ljava/lang/Object;
.source "SyncMoreFeedsLoader.java"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final a:LaGF;

.field private final a:LaGM;

.field private final a:Lafa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, LafB;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LafB;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LaGM;LaGF;Lafa;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, LafB;->a:LaGM;

    .line 31
    iput-object p2, p0, LafB;->a:LaGF;

    .line 32
    iput-object p3, p0, LafB;->a:Lafa;

    .line 33
    return-void
.end method


# virtual methods
.method a(LaFM;Lafi;Landroid/net/Uri;Ljava/util/Date;)LaGW;
    .locals 4

    .prologue
    .line 39
    invoke-virtual {p2}, Lafi;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    new-instance p4, Ljava/util/Date;

    const-wide v0, 0x7fffffffffffffffL

    invoke-direct {p4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 62
    :goto_0
    if-nez p3, :cond_1

    .line 63
    const/4 v0, 0x0

    .line 68
    :goto_1
    return-object v0

    .line 59
    :cond_0
    iget-object v0, p0, LafB;->a:LaGF;

    invoke-static {p4, v0}, Lafi;->a(Ljava/util/Date;LaGF;)Lafi;

    move-result-object v0

    invoke-virtual {v0, p3}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p3

    goto :goto_0

    .line 65
    :cond_1
    sget-boolean v0, LafB;->a:Z

    if-nez v0, :cond_2

    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_2
    iget-object v0, p0, LafB;->a:LaGM;

    .line 67
    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {v0, p1, v1, v2, v3}, LaGM;->a(LaFM;Ljava/lang/String;J)LaGW;

    move-result-object v0

    goto :goto_1
.end method

.method public a(LaFM;LaeZ;)Lafe;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LaeZ;",
            ")",
            "Lafe",
            "<",
            "LaGW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, LafB;->a:LaGM;

    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFQ;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, LaFQ;->a()Lafe;

    move-result-object v3

    .line 78
    iget-object v0, p0, LafB;->a:Lafa;

    invoke-interface {v0, p2}, Lafa;->a(LaeZ;)Lafe;

    move-result-object v4

    .line 79
    iget-object v0, p2, LaeZ;->a:Ljava/lang/Object;

    check-cast v0, Lafi;

    iget-object v1, v4, Lafe;->a:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    iget-object v2, v3, Lafe;->a:Ljava/lang/Object;

    check-cast v2, Ljava/util/Date;

    invoke-virtual {p0, p1, v0, v1, v2}, LafB;->a(LaFM;Lafi;Landroid/net/Uri;Ljava/util/Date;)LaGW;

    move-result-object v5

    .line 81
    iget-object v0, p2, LaeZ;->b:Ljava/lang/Object;

    check-cast v0, Lafi;

    iget-object v1, v4, Lafe;->b:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    iget-object v2, v3, Lafe;->b:Ljava/lang/Object;

    check-cast v2, Ljava/util/Date;

    invoke-virtual {p0, p1, v0, v1, v2}, LafB;->a(LaFM;Lafi;Landroid/net/Uri;Ljava/util/Date;)LaGW;

    move-result-object v0

    .line 83
    invoke-static {v5, v0}, Lafe;->a(Ljava/lang/Object;Ljava/lang/Object;)Lafe;

    move-result-object v0

    return-object v0
.end method
