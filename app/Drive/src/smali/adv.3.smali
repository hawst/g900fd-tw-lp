.class public Ladv;
.super Ljava/lang/Object;
.source "ExposingDocumentUriCreatorImpl.java"

# interfaces
.implements Ladu;


# instance fields
.field private final a:LaGM;

.field private final a:Ladf;

.field private final a:Ladi;


# direct methods
.method public constructor <init>(LaGM;Ladf;Ladi;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Ladv;->a:LaGM;

    .line 29
    iput-object p2, p0, Ladv;->a:Ladf;

    .line 30
    iput-object p3, p0, Ladv;->a:Ladi;

    .line 31
    return-void
.end method


# virtual methods
.method a(LaGo;Ljava/lang/Exception;)Ladc;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Ladc;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Document id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 70
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ladc;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Ladv;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v1

    .line 37
    if-nez v1, :cond_0

    .line 38
    new-instance v0, Ladt;

    invoke-direct {v0}, Ladt;-><init>()V

    throw v0

    .line 44
    :cond_0
    :try_start_0
    iget-object v0, p0, Ladv;->a:Ladi;

    invoke-interface {v0, v1, p2}, Ladi;->a(LaGo;LacY;)LbsU;

    move-result-object v0

    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 52
    :try_start_1
    iget-object v2, p0, Ladv;->a:LaGM;

    invoke-interface {v2, v1, p2}, LaGM;->a(LaGo;LacY;)LaGp;

    move-result-object v1

    .line 53
    if-nez v1, :cond_1

    .line 54
    new-instance v1, Ladc;

    invoke-direct {v1}, Ladc;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Ladj;->close()V

    throw v1

    .line 45
    :catch_0
    move-exception v0

    .line 46
    invoke-virtual {p0, v1, v0}, Ladv;->a(LaGo;Ljava/lang/Exception;)Ladc;

    move-result-object v0

    throw v0

    .line 47
    :catch_1
    move-exception v0

    .line 48
    invoke-virtual {p0, v1, v0}, Ladv;->a(LaGo;Ljava/lang/Exception;)Ladc;

    move-result-object v0

    throw v0

    .line 57
    :cond_1
    :try_start_2
    iget-object v2, p0, Ladv;->a:Ladf;

    .line 58
    invoke-virtual {v2, v1}, Ladf;->a(LaGp;)Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-static {v1}, LacV;->a(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 63
    invoke-interface {v0}, Ladj;->close()V

    return-object v1
.end method
