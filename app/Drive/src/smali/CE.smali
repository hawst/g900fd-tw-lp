.class public LCE;
.super Ljava/lang/Object;
.source "LabelViewState.java"


# instance fields
.field private final a:I

.field private final a:LBf;

.field private a:LCv;

.field private final a:LIK;

.field private final a:LIf;

.field private final a:LamF;

.field private final a:Landroid/text/SpannableString;

.field private final a:Z

.field private final b:Landroid/text/SpannableString;

.field private final b:Z

.field private final c:Landroid/text/SpannableString;

.field private final c:Z

.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LIK;LamF;LBf;LCn;LIf;Z)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    iput-object v0, p0, LCE;->a:LIK;

    .line 68
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamF;

    iput-object v0, p0, LCE;->a:LamF;

    .line 69
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBf;

    iput-object v0, p0, LCE;->a:LBf;

    .line 70
    invoke-virtual {p2}, LIK;->c()I

    move-result v0

    iput v0, p0, LCE;->a:I

    .line 72
    sget-object v0, LCn;->h:LCn;

    invoke-virtual {p5, v0}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LCE;->a:Z

    .line 73
    sget-object v0, LCn;->f:LCn;

    invoke-virtual {p5, v0}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LCE;->b:Z

    .line 74
    sget-object v0, LCn;->i:LCn;

    invoke-virtual {p5, v0}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LCE;->c:Z

    .line 75
    iput-object p6, p0, LCE;->a:LIf;

    .line 76
    iput-boolean p7, p0, LCE;->d:Z

    .line 78
    sget v0, Lxi;->pin_offline:I

    sget v1, Lxb;->ic_offline_small_alpha:I

    .line 79
    invoke-direct {p0, p1, v0, v1}, LCE;->a(Landroid/content/Context;II)Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, p0, LCE;->a:Landroid/text/SpannableString;

    .line 80
    sget v0, Lxi;->shared_status:I

    sget v1, Lxb;->ic_shared_small_alpha:I

    .line 81
    invoke-direct {p0, p1, v0, v1}, LCE;->a(Landroid/content/Context;II)Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, p0, LCE;->b:Landroid/text/SpannableString;

    .line 82
    sget v0, Lxi;->doclist_starred_state:I

    sget v1, Lxb;->ic_starred_small_alpha:I

    .line 83
    invoke-direct {p0, p1, v0, v1}, LCE;->a(Landroid/content/Context;II)Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, p0, LCE;->c:Landroid/text/SpannableString;

    .line 85
    return-void
.end method

.method private a(Landroid/content/Context;II)Landroid/text/SpannableString;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 317
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 319
    const-string v1, "%s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 320
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 322
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 324
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 323
    invoke-virtual {v0, v5, v5, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 325
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, LwZ;->m_app_secondary_text:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 326
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 327
    new-instance v1, Landroid/text/style/ImageSpan;

    invoke-direct {v1, v0, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 329
    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v0

    const/16 v3, 0x21

    invoke-virtual {v2, v1, v5, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 330
    return-object v2
.end method

.method private a(LCF;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCF;",
            "Ljava/util/List",
            "<",
            "Landroid/text/SpannableString;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 246
    invoke-static {p1}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LCE;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LCE;->a:LBf;

    invoke-interface {v0}, LBf;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, LCE;->c:Landroid/text/SpannableString;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    :cond_0
    return-void
.end method

.method private a(LCF;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCF;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/text/SpannableString;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 265
    iget-object v0, p0, LCE;->a:LCv;

    invoke-interface {v0}, LCv;->c()Z

    move-result v0

    .line 266
    iget-boolean v1, p0, LCE;->b:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 267
    :goto_0
    invoke-static {p1}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, LCE;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 268
    iget-object v0, p0, LCE;->b:Landroid/text/SpannableString;

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    :cond_0
    :goto_1
    return-void

    .line 266
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 269
    :cond_2
    if-eqz v0, :cond_0

    .line 270
    invoke-static {p1}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->shared_status:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private a(Landroid/widget/TextView;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Ljava/util/List",
            "<",
            "Landroid/text/SpannableString;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 334
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 335
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 337
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    .line 338
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 339
    const-string v4, "  "

    invoke-virtual {v1, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 340
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 341
    const-string v0, "  "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 344
    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 345
    invoke-virtual {p1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 347
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 348
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 349
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;LCF;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LCF;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/text/SpannableString;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 209
    iget-object v0, p0, LCE;->a:LBf;

    invoke-interface {v0}, LBf;->a()LBg;

    move-result-object v0

    .line 210
    iget-object v2, p0, LCE;->a:LBf;

    invoke-interface {v2}, LBf;->a()LBh;

    move-result-object v2

    .line 212
    iget-boolean v3, p0, LCE;->c:Z

    if-eqz v3, :cond_5

    .line 213
    sget-object v2, LBg;->d:LBg;

    invoke-virtual {v0, v2}, LBg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unpinned entry "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in offline folder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_0
    sget-object v2, LBg;->a:LBg;

    invoke-virtual {v0, v2}, LBg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 232
    :goto_0
    invoke-static {p2}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, LCE;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, LCE;->c:Z

    if-nez v2, :cond_2

    .line 233
    sget v2, Lxi;->pin_offline:I

    if-ne v0, v2, :cond_1

    .line 234
    iget-object v0, p0, LCE;->a:Landroid/text/SpannableString;

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v1

    .line 239
    :cond_2
    if-eq v0, v1, :cond_3

    .line 241
    invoke-static {p2}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    :cond_3
    return-void

    .line 221
    :cond_4
    iget v0, v0, LBg;->a:I

    goto :goto_0

    .line 224
    :cond_5
    iget-boolean v0, v0, LBg;->a:Z

    if-eqz v0, :cond_6

    sget-object v0, LBh;->e:LBh;

    sget-object v3, LBh;->f:LBh;

    invoke-static {v0, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 225
    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 226
    sget v0, Lxi;->pin_offline:I

    goto :goto_0

    :cond_6
    move v0, v1

    .line 228
    goto :goto_0
.end method

.method private a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 313
    invoke-static {p1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LCF;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, LCE;->a:LIK;

    sget-object v1, LIK;->g:LIK;

    if-ne v0, v1, :cond_0

    .line 156
    invoke-direct {p0, p1}, LCE;->d(LCF;)V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-direct {p0, p1}, LCE;->c(LCF;)V

    goto :goto_0
.end method

.method private b(LCF;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCF;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    iget-boolean v0, p0, LCE;->a:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LCE;->c:Z

    if-nez v0, :cond_1

    sget-object v0, LBh;->e:LBh;

    sget-object v1, LBh;->f:LBh;

    .line 254
    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iget-object v1, p0, LCE;->a:LBf;

    .line 255
    invoke-interface {v1}, LBf;->a()LBh;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LahS;->b:LahS;

    iget-object v1, p0, LCE;->a:LBf;

    .line 256
    invoke-interface {v1}, LBf;->a()LahS;

    move-result-object v1

    invoke-virtual {v0, v1}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 257
    :goto_0
    if-eqz v0, :cond_0

    .line 259
    invoke-static {p1}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->upload_status_in_doc_list:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    :cond_0
    return-void

    .line 256
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/widget/TextView;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Ljava/util/List",
            "<",
            "Landroid/text/SpannableString;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 352
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 353
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 355
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    .line 356
    const-string v4, "  "

    invoke-virtual {v1, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 357
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 358
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 362
    :cond_0
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 363
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 364
    return-void
.end method

.method private c(LCF;)V
    .locals 5

    .prologue
    .line 165
    iget-object v0, p0, LCE;->a:LIf;

    iget-object v1, p0, LCE;->a:LCv;

    invoke-virtual {v0, v1}, LIf;->a(LCv;)Ljava/lang/Long;

    move-result-object v0

    .line 166
    if-nez v0, :cond_0

    .line 167
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 169
    :cond_0
    iget-object v1, p0, LCE;->a:LamF;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LamF;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-static {p1}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v1

    iget v2, p0, LCE;->a:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 173
    iget-boolean v2, p0, LCE;->d:Z

    if-eqz v2, :cond_1

    .line 180
    :goto_0
    invoke-static {p1}, LCF;->a(LCF;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    invoke-static {p1}, LCF;->a(LCF;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 182
    return-void

    :cond_1
    move-object v0, v1

    .line 177
    goto :goto_0
.end method

.method private d(LCF;)V
    .locals 5

    .prologue
    .line 185
    iget-object v0, p0, LCE;->a:LCv;

    invoke-interface {v0}, LCv;->g()J

    move-result-wide v0

    .line 187
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 188
    invoke-static {v0, v1}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 193
    :goto_0
    invoke-static {p1}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v1

    iget v2, p0, LCE;->a:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 196
    iget-boolean v2, p0, LCE;->d:Z

    if-eqz v2, :cond_1

    .line 202
    :goto_1
    invoke-static {p1}, LCF;->a(LCF;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    invoke-static {p1}, LCF;->a(LCF;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 204
    return-void

    .line 190
    :cond_0
    invoke-static {p1}, LCF;->a(LCF;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->quota_zero:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 200
    goto :goto_1
.end method


# virtual methods
.method public a(LCF;)V
    .locals 0

    .prologue
    .line 306
    invoke-virtual {p1}, LCF;->a()V

    .line 307
    invoke-virtual {p1}, LCF;->c()V

    .line 308
    return-void
.end method

.method public a(LCF;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 3

    .prologue
    .line 280
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 284
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 285
    invoke-direct {p0, p1}, LCE;->b(LCF;)V

    .line 286
    invoke-direct {p0, p2, p1, v0, v1}, LCE;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LCF;Ljava/util/List;Ljava/util/List;)V

    .line 287
    invoke-direct {p0, p1, v0, v1}, LCE;->a(LCF;Ljava/util/List;Ljava/util/List;)V

    .line 288
    invoke-direct {p0, p1, v1}, LCE;->a(LCF;Ljava/util/List;)V

    .line 289
    invoke-direct {p0, p1, v0}, LCE;->b(LCF;Ljava/util/List;)V

    .line 291
    const-string v2, "    "

    invoke-static {v2}, LbiI;->a(Ljava/lang/String;)LbiI;

    move-result-object v2

    invoke-virtual {v2, v0}, LbiI;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 292
    invoke-static {p1}, LCF;->b(LCF;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 293
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 294
    invoke-static {p1}, LCF;->b(LCF;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    :cond_0
    if-eqz p3, :cond_1

    .line 299
    invoke-static {p1}, LCF;->c(LCF;)Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0, v0, v1}, LCE;->b(Landroid/widget/TextView;Ljava/util/List;)V

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_1
    invoke-static {p1}, LCF;->a(LCF;)Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0, v0, v1}, LCE;->a(Landroid/widget/TextView;Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(LCv;)V
    .locals 0

    .prologue
    .line 275
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    iput-object p1, p0, LCE;->a:LCv;

    .line 277
    return-void
.end method
