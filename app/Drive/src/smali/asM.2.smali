.class public LasM;
.super Ljava/lang/Object;
.source "Story.java"


# instance fields
.field private a:Landroid/net/Uri;

.field private a:LasO;

.field private a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LasM;->a:Ljava/lang/Iterable;

    return-void
.end method

.method static synthetic a(LasM;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LasM;->a:Landroid/net/Uri;

    return-object v0
.end method

.method private a(Lbth;)V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p1}, Lbth;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p1}, Lbth;->a()Lbtk;

    move-result-object v0

    .line 96
    const-string v1, "pages"

    invoke-virtual {v0, v1}, Lbtk;->a(Ljava/lang/String;)Lbtg;

    move-result-object v0

    .line 97
    new-instance v1, LasN;

    invoke-direct {v1, p0}, LasN;-><init>(LasM;)V

    invoke-static {v0, v1}, Lbnm;->a(Ljava/lang/Iterable;LbiG;)Ljava/lang/Iterable;

    move-result-object v0

    iput-object v0, p0, LasM;->a:Ljava/lang/Iterable;

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public a()LasJ;
    .locals 3

    .prologue
    .line 108
    new-instance v0, LasJ;

    iget-object v1, p0, LasM;->a:LasO;

    iget-object v2, p0, LasM;->a:Ljava/lang/Iterable;

    invoke-static {v2}, LbmF;->a(Ljava/lang/Iterable;)LbmF;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LasJ;-><init>(LasO;LbmF;)V

    return-object v0
.end method

.method public a(Landroid/net/Uri;)LasM;
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, LasM;->a:Landroid/net/Uri;

    .line 77
    return-object p0
.end method

.method public a(LasO;)LasM;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, LasM;->a:LasO;

    .line 68
    return-object p0
.end method

.method public a(Ljava/lang/String;)LasM;
    .locals 5

    .prologue
    .line 82
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    .line 85
    :try_start_0
    invoke-virtual {v0, p1}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    .line 86
    invoke-direct {p0, v0}, LasM;->a(Lbth;)V
    :try_end_0
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-object p0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "Story"

    const-string v2, "Provided toc file is not a valid JSON %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
