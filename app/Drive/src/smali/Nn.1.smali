.class public enum LNn;
.super Ljava/lang/Enum;
.source "DriveFeature.java"

# interfaces
.implements LtJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LNn;",
        ">;",
        "LtJ;"
    }
.end annotation


# static fields
.field public static final enum a:LNn;

.field private static final synthetic a:[LNn;

.field public static final enum b:LNn;

.field public static final enum c:LNn;

.field public static final enum d:LNn;


# instance fields
.field public final a:Lrx;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 35
    new-instance v0, LNo;

    const-string v1, "CREATE_BAR"

    sget-object v2, Lrx;->a:Lrx;

    invoke-direct {v0, v1, v3, v2}, LNo;-><init>(Ljava/lang/String;ILrx;)V

    sput-object v0, LNn;->a:LNn;

    .line 45
    new-instance v0, LNn;

    const-string v1, "DRIVE_HELP_CARDS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, LNn;-><init>(Ljava/lang/String;ILrx;)V

    sput-object v0, LNn;->b:LNn;

    .line 50
    new-instance v0, LNn;

    const-string v1, "PUNCH_PREVIEW_SHOWS_FIRST_TIME_PROMO"

    sget-object v2, Lrx;->a:Lrx;

    invoke-direct {v0, v1, v5, v2}, LNn;-><init>(Ljava/lang/String;ILrx;)V

    sput-object v0, LNn;->c:LNn;

    .line 55
    new-instance v0, LNp;

    const-string v1, "TOPHAT"

    sget-object v2, Lrx;->d:Lrx;

    invoke-direct {v0, v1, v6, v2}, LNp;-><init>(Ljava/lang/String;ILrx;)V

    sput-object v0, LNn;->d:LNn;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [LNn;

    sget-object v1, LNn;->a:LNn;

    aput-object v1, v0, v3

    sget-object v1, LNn;->b:LNn;

    aput-object v1, v0, v4

    sget-object v1, LNn;->c:LNn;

    aput-object v1, v0, v5

    sget-object v1, LNn;->d:LNn;

    aput-object v1, v0, v6

    sput-object v0, LNn;->a:[LNn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILrx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput-object p3, p0, LNn;->a:Lrx;

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILrx;LNo;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, LNn;-><init>(Ljava/lang/String;ILrx;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LNn;
    .locals 1

    .prologue
    .line 29
    const-class v0, LNn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LNn;

    return-object v0
.end method

.method public static values()[LNn;
    .locals 1

    .prologue
    .line 29
    sget-object v0, LNn;->a:[LNn;

    invoke-virtual {v0}, [LNn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LNn;

    return-object v0
.end method


# virtual methods
.method public a()Lrx;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, LNn;->a:Lrx;

    return-object v0
.end method

.method public a(LtK;LQr;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method
