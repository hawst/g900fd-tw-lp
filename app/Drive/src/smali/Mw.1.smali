.class LMw;
.super Ljava/lang/Object;
.source "DocListViewSnapshotter.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:LMv;

.field final synthetic a:Lcom/google/android/apps/docs/view/DocListView;


# direct methods
.method constructor <init>(LMv;Lcom/google/android/apps/docs/view/DocListView;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, LMw;->a:LMv;

    iput-object p2, p0, LMw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, LMw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, LMw;->a:Lcom/google/android/apps/docs/view/DocListView;

    new-instance v1, LMx;

    invoke-direct {v1, p0}, LMx;-><init>(LMw;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->post(Ljava/lang/Runnable;)Z

    .line 52
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
