.class public final LPI;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPH;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPG;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPE;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPF;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPB;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 43
    iput-object p1, p0, LPI;->a:LbrA;

    .line 44
    const-class v0, LPH;

    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPI;->a:Lbsk;

    .line 47
    const-class v0, LPG;

    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPI;->b:Lbsk;

    .line 50
    const-class v0, LPE;

    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPI;->c:Lbsk;

    .line 53
    const-class v0, LPF;

    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPI;->d:Lbsk;

    .line 56
    const-class v0, LPB;

    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPI;->e:Lbsk;

    .line 59
    const-class v0, LPC;

    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPI;->f:Lbsk;

    .line 62
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 95
    packed-switch p1, :pswitch_data_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :pswitch_0
    new-instance v3, LPG;

    iget-object v0, p0, LPI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->f:Lbsk;

    .line 100
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->f:Lbsk;

    .line 98
    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTF;

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 106
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LPI;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 104
    invoke-static {v1, v2}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, LPI;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->S:Lbsk;

    .line 112
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LPI;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->S:Lbsk;

    .line 110
    invoke-static {v2, v4}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaKM;

    invoke-direct {v3, v0, v1, v2}, LPG;-><init>(LTF;LQr;LaKM;)V

    move-object v0, v3

    .line 181
    :goto_0
    return-object v0

    .line 119
    :pswitch_1
    new-instance v1, LPE;

    iget-object v0, p0, LPI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPI;

    iget-object v0, v0, LPI;->b:Lbsk;

    .line 122
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LPI;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LPI;

    iget-object v2, v2, LPI;->b:Lbsk;

    .line 120
    invoke-static {v0, v2}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPG;

    invoke-direct {v1, v0}, LPE;-><init>(LPG;)V

    move-object v0, v1

    .line 127
    goto :goto_0

    .line 129
    :pswitch_2
    new-instance v2, LPF;

    iget-object v0, p0, LPI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPI;

    iget-object v0, v0, LPI;->c:Lbsk;

    .line 132
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPI;

    iget-object v1, v1, LPI;->c:Lbsk;

    .line 130
    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPE;

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 138
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LPI;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 136
    invoke-static {v1, v3}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    invoke-direct {v2, v0, v1}, LPF;-><init>(LPE;LQr;)V

    move-object v0, v2

    .line 143
    goto :goto_0

    .line 145
    :pswitch_3
    new-instance v3, LPB;

    iget-object v0, p0, LPI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 148
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 146
    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 154
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LPI;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 152
    invoke-static {v1, v2}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, LPI;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LPI;

    iget-object v2, v2, LPI;->f:Lbsk;

    .line 160
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LPI;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LPI;

    iget-object v4, v4, LPI;->f:Lbsk;

    .line 158
    invoke-static {v2, v4}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LPC;

    invoke-direct {v3, v0, v1, v2}, LPB;-><init>(LaGM;LQr;LPC;)V

    move-object v0, v3

    .line 165
    goto/16 :goto_0

    .line 167
    :pswitch_4
    new-instance v2, LPC;

    iget-object v0, p0, LPI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 170
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 168
    invoke-static {v0, v1}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->k:Lbsk;

    .line 176
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LPI;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTk;

    iget-object v3, v3, LTk;->k:Lbsk;

    .line 174
    invoke-static {v1, v3}, LPI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTd;

    invoke-direct {v2, v0, v1}, LPC;-><init>(LQr;LTd;)V

    move-object v0, v2

    .line 181
    goto/16 :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x8f
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 198
    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 69
    const-class v0, LPH;

    iget-object v1, p0, LPI;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LPI;->a(Ljava/lang/Class;Lbsk;)V

    .line 70
    const-class v0, LPG;

    iget-object v1, p0, LPI;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LPI;->a(Ljava/lang/Class;Lbsk;)V

    .line 71
    const-class v0, LPE;

    iget-object v1, p0, LPI;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LPI;->a(Ljava/lang/Class;Lbsk;)V

    .line 72
    const-class v0, LPF;

    iget-object v1, p0, LPI;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LPI;->a(Ljava/lang/Class;Lbsk;)V

    .line 73
    const-class v0, LPB;

    iget-object v1, p0, LPI;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LPI;->a(Ljava/lang/Class;Lbsk;)V

    .line 74
    const-class v0, LPC;

    iget-object v1, p0, LPI;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LPI;->a(Ljava/lang/Class;Lbsk;)V

    .line 75
    iget-object v0, p0, LPI;->a:Lbsk;

    iget-object v1, p0, LPI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPI;

    iget-object v1, v1, LPI;->d:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 77
    iget-object v0, p0, LPI;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x90

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 79
    iget-object v0, p0, LPI;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x91

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 81
    iget-object v0, p0, LPI;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x8f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 83
    iget-object v0, p0, LPI;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x92

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 85
    iget-object v0, p0, LPI;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x93

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 87
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 190
    .line 192
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
