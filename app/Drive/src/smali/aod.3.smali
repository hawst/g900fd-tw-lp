.class Laod;
.super Ljava/lang/Object;
.source "DocumentFileStoringFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laoo",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LaGM;

.field private final a:Lago;


# direct methods
.method constructor <init>(LaGM;Lago;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Laod;->a:LaGM;

    .line 48
    iput-object p2, p0, Laod;->a:Lago;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Laod;->a:LaGM;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    .line 57
    :try_start_0
    iget-object v1, p0, Laod;->a:Lago;

    sget-object v2, LacY;->a:LacY;

    invoke-virtual {v1, v0, v2}, Lago;->a(LaGo;LacY;)Lagq;

    move-result-object v0

    .line 58
    iget-object v0, v0, Lagq;->a:Landroid/net/Uri;

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_4
    .catch LTt; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_6

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    invoke-static {v0}, Laoc;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 61
    :catch_1
    move-exception v0

    .line 62
    invoke-static {v0}, Laoc;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 63
    :catch_2
    move-exception v0

    .line 64
    invoke-static {v0}, Laoc;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 65
    :catch_3
    move-exception v0

    .line 66
    invoke-static {v0}, Laoc;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 67
    :catch_4
    move-exception v0

    .line 68
    invoke-static {v0}, Laoc;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 69
    :catch_5
    move-exception v0

    .line 70
    invoke-static {v0}, Laoc;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 71
    :catch_6
    move-exception v0

    .line 72
    invoke-static {v0}, Laoc;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_0
    const-string v0, "DocumentFileStoringFetcher"

    const-string v1, "Failed to find document for fetchSpec: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 76
    new-instance v0, Ladt;

    invoke-direct {v0}, Ladt;-><init>()V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)LbsU;
    .locals 1

    .prologue
    .line 42
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, Laod;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    return-object v0
.end method
