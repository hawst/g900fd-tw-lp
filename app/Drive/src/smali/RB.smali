.class public LRB;
.super Ljava/lang/Object;
.source "NavigationFragment.java"

# interfaces
.implements Lqc;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/fragment/NavigationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, LRB;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 229
    const-string v0, "NavigationFragment"

    const-string v1, "Switched to: %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 231
    invoke-static {p1}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 232
    iget-object v1, p0, LRB;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    iget-object v1, p0, LRB;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v2, p0, LRB;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v2}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LaqX;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;LaFO;LaqX;)V

    .line 234
    iget-object v0, p0, LRB;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    sget v1, Lxi;->announce_account_switch:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 235
    iget-object v1, p0, LRB;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v1

    iget-object v2, p0, LRB;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v2

    invoke-static {v1, v2, v0}, LUs;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 237
    :cond_0
    return-void
.end method
