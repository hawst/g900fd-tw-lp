.class public LaaK;
.super Ljava/lang/Object;
.source "MimeTypeHelper.java"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LQr;

.field private final a:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-string v0, ".3gp"

    const-string v1, "video/3gpp"

    .line 36
    invoke-static {v0, v1}, LbmL;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmL;

    move-result-object v0

    sput-object v0, LaaK;->a:Ljava/util/Map;

    .line 37
    const-string v0, "application/vnd.google.panorama360+jpg"

    const-string v1, "image/jpeg"

    .line 38
    invoke-static {v0, v1}, LbmL;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmL;

    move-result-object v0

    sput-object v0, LaaK;->b:Ljava/util/Map;

    .line 37
    return-void
.end method

.method public constructor <init>(LQr;Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, LaaK;->a:LQr;

    .line 46
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, LaaK;->a:Landroid/content/ContentResolver;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 59
    .line 62
    sget-object v0, LaaK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 63
    invoke-virtual {p3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    sget-object v2, LaaK;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    const-string v2, "MimeTypeHelper"

    const-string v3, "Hardcoding mimetype to %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object p2, v0

    goto :goto_0

    .line 68
    :cond_1
    sget-object v0, LaaK;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 69
    sget-object v0, LaaK;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    :goto_1
    if-eqz v0, :cond_2

    const-string v1, "/*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 75
    :cond_2
    iget-object v1, p0, LaaK;->a:Landroid/content/ContentResolver;

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 76
    const-string v2, "MimeTypeHelper"

    const-string v3, "MIME type %s too generic; inferring from %s yields %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object p1, v4, v6

    const/4 v0, 0x2

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    .line 80
    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, LaaK;->a:LQr;

    invoke-static {v1, v0}, Lamc;->a(LQr;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 81
    :cond_4
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, p0, LaaK;->a:LQr;

    invoke-static {v1, v0}, Lamc;->a(LQr;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 85
    :cond_6
    iget-object v0, p0, LaaK;->a:LQr;

    const-string v1, "defaultUploadMimeType"

    const-string v2, "application/octet-stream"

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_7
    return-object v0

    :cond_8
    move-object v0, p2

    goto :goto_1
.end method
