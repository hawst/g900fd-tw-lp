.class LazS;
.super Ljava/lang/Object;

# interfaces
.implements Lazn;
.implements Lazt;
.implements Lazu;


# instance fields
.field private volatile a:I

.field private volatile a:J

.field private final a:Landroid/content/Context;

.field private final a:LayV;

.field private final a:LazA;

.field private a:LazE;

.field private volatile a:LazW;

.field private volatile a:Lazq;

.field private a:Lazv;

.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LazZ;",
            ">;"
        }
    .end annotation
.end field

.field private volatile a:Ljava/util/Timer;

.field private a:Z

.field private b:J

.field private b:Lazv;

.field private volatile b:Ljava/util/Timer;

.field private b:Z

.field private volatile c:Ljava/util/Timer;

.field private c:Z

.field private d:Z


# direct methods
.method constructor <init>(Landroid/content/Context;LazA;)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {p1}, LayV;->a(Landroid/content/Context;)LayV;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, LazS;-><init>(Landroid/content/Context;LazA;Lazv;LayV;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;LazA;Lazv;LayV;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LazS;->a:Ljava/util/Queue;

    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, LazS;->b:J

    iput-object p3, p0, LazS;->b:Lazv;

    iput-object p1, p0, LazS;->a:Landroid/content/Context;

    iput-object p2, p0, LazS;->a:LazA;

    iput-object p4, p0, LazS;->a:LayV;

    new-instance v0, LazT;

    invoke-direct {v0, p0}, LazT;-><init>(LazS;)V

    iput-object v0, p0, LazS;->a:LazE;

    const/4 v0, 0x0

    iput v0, p0, LazS;->a:I

    sget-object v0, LazW;->g:LazW;

    iput-object v0, p0, LazS;->a:LazW;

    return-void
.end method

.method static synthetic a(LazS;)J
    .locals 2

    iget-wide v0, p0, LazS;->a:J

    return-wide v0
.end method

.method static synthetic a(LazS;)LazE;
    .locals 1

    iget-object v0, p0, LazS;->a:LazE;

    return-object v0
.end method

.method static synthetic a(LazS;)LazW;
    .locals 1

    iget-object v0, p0, LazS;->a:LazW;

    return-object v0
.end method

.method static synthetic a(LazS;)Ljava/util/Queue;
    .locals 1

    iget-object v0, p0, LazS;->a:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic a(LazS;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, LazS;->c:Ljava/util/Timer;

    return-object v0
.end method

.method private a(Ljava/util/Timer;)Ljava/util/Timer;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/Timer;->cancel()V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic a(LazS;)V
    .locals 0

    invoke-direct {p0}, LazS;->h()V

    return-void
.end method

.method static synthetic b(LazS;)J
    .locals 2

    iget-wide v0, p0, LazS;->b:J

    return-wide v0
.end method

.method static synthetic b(LazS;)V
    .locals 0

    invoke-direct {p0}, LazS;->j()V

    return-void
.end method

.method static synthetic c(LazS;)V
    .locals 0

    invoke-direct {p0}, LazS;->k()V

    return-void
.end method

.method static synthetic d(LazS;)V
    .locals 0

    invoke-direct {p0}, LazS;->l()V

    return-void
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, LazS;->a:Ljava/util/Timer;

    invoke-direct {p0, v0}, LazS;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, LazS;->a:Ljava/util/Timer;

    iget-object v0, p0, LazS;->b:Ljava/util/Timer;

    invoke-direct {p0, v0}, LazS;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, LazS;->b:Ljava/util/Timer;

    iget-object v0, p0, LazS;->c:Ljava/util/Timer;

    invoke-direct {p0, v0}, LazS;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, LazS;->c:Ljava/util/Timer;

    return-void
.end method

.method private declared-synchronized h()V
    .locals 8

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, LazS;->a:LazA;

    invoke-interface {v3}, LazA;->a()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LazS;->a:LazA;

    invoke-interface {v2}, LazA;->a()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v2

    new-instance v3, LazU;

    invoke-direct {v3, p0}, LazU;-><init>(LazS;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v2, p0, LazS;->b:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, LazS;->f()V

    :cond_2
    sget-object v2, LazV;->a:[I

    iget-object v3, p0, LazS;->a:LazW;

    invoke-virtual {v3}, LazW;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :goto_1
    :pswitch_1
    iget-object v2, p0, LazS;->a:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, LazS;->a:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LazZ;

    move-object v7, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending hit to store  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LaAj;->c(Ljava/lang/String;)V

    iget-object v2, p0, LazS;->a:Lazv;

    invoke-virtual {v7}, LazZ;->a()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v7}, LazZ;->a()J

    move-result-wide v4

    invoke-virtual {v7}, LazZ;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, LazZ;->a()Ljava/util/List;

    move-result-object v7

    invoke-interface/range {v2 .. v7}, Lazv;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_3
    :try_start_2
    iget-boolean v2, p0, LazS;->a:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, LazS;->i()V

    goto :goto_0

    :goto_2
    :pswitch_2
    iget-object v2, p0, LazS;->a:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, LazS;->a:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LazZ;

    move-object v7, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending hit to service   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LaAj;->c(Ljava/lang/String;)V

    iget-object v2, p0, LazS;->a:LayV;

    invoke-virtual {v2}, LayV;->a()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, LazS;->a:Lazq;

    invoke-virtual {v7}, LazZ;->a()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v7}, LazZ;->a()J

    move-result-wide v4

    invoke-virtual {v7}, LazZ;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, LazZ;->a()Ljava/util/List;

    move-result-object v7

    invoke-interface/range {v2 .. v7}, Lazq;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    :goto_3
    iget-object v2, p0, LazS;->a:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_2

    :cond_4
    const-string v2, "Dry run enabled. Hit not actually sent to service."

    invoke-static {v2}, LaAj;->c(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    iget-object v2, p0, LazS;->a:LazE;

    invoke-interface {v2}, LazE;->a()J

    move-result-wide v2

    iput-wide v2, p0, LazS;->a:J

    goto/16 :goto_0

    :pswitch_3
    const-string v2, "Need to reconnect"

    invoke-static {v2}, LaAj;->c(Ljava/lang/String;)V

    iget-object v2, p0, LazS;->a:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, LazS;->k()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, LazS;->a:Lazv;

    invoke-interface {v0}, Lazv;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LazS;->a:Z

    return-void
.end method

.method private declared-synchronized j()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LazS;->a:LazW;

    sget-object v1, LazW;->c:LazW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, LazS;->g()V

    const-string v0, "falling back to local store"

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    iget-object v0, p0, LazS;->b:Lazv;

    if-eqz v0, :cond_1

    iget-object v0, p0, LazS;->b:Lazv;

    iput-object v0, p0, LazS;->a:Lazv;

    :goto_1
    sget-object v0, LazW;->c:LazW;

    iput-object v0, p0, LazS;->a:LazW;

    invoke-direct {p0}, LazS;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-static {}, LayO;->a()LayO;

    move-result-object v0

    iget-object v1, p0, LazS;->a:Landroid/content/Context;

    iget-object v2, p0, LazS;->a:LazA;

    invoke-virtual {v0, v1, v2}, LayO;->a(Landroid/content/Context;LazA;)V

    invoke-virtual {v0}, LayO;->a()Lazv;

    move-result-object v0

    iput-object v0, p0, LazS;->a:Lazv;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized k()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LazS;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LazS;->a:Lazq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LazS;->a:LazW;

    sget-object v1, LazW;->c:LazW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    :try_start_1
    iget v0, p0, LazS;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LazS;->a:I

    iget-object v0, p0, LazS;->b:Ljava/util/Timer;

    invoke-direct {p0, v0}, LazS;->a(Ljava/util/Timer;)Ljava/util/Timer;

    sget-object v0, LazW;->a:LazW;

    iput-object v0, p0, LazS;->a:LazW;

    new-instance v0, Ljava/util/Timer;

    const-string v1, "Failed Connect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LazS;->b:Ljava/util/Timer;

    iget-object v0, p0, LazS;->b:Ljava/util/Timer;

    new-instance v1, LazY;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LazY;-><init>(LazS;LazT;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    const-string v0, "connecting to Analytics service"

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    iget-object v0, p0, LazS;->a:Lazq;

    invoke-interface {v0}, Lazq;->b()V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "security exception on connectToService"

    invoke-static {v0}, LaAj;->d(Ljava/lang/String;)V

    invoke-direct {p0}, LazS;->j()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    const-string v0, "client not initialized."

    invoke-static {v0}, LaAj;->d(Ljava/lang/String;)V

    invoke-direct {p0}, LazS;->j()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized l()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LazS;->a:Lazq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LazS;->a:LazW;

    sget-object v1, LazW;->b:LazW;

    if-ne v0, v1, :cond_0

    sget-object v0, LazW;->f:LazW;

    iput-object v0, p0, LazS;->a:LazW;

    iget-object v0, p0, LazS;->a:Lazq;

    invoke-interface {v0}, Lazq;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private m()V
    .locals 4

    iget-object v0, p0, LazS;->a:Ljava/util/Timer;

    invoke-direct {p0, v0}, LazS;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, LazS;->a:Ljava/util/Timer;

    new-instance v0, Ljava/util/Timer;

    const-string v1, "Service Reconnect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LazS;->a:Ljava/util/Timer;

    iget-object v0, p0, LazS;->a:Ljava/util/Timer;

    new-instance v1, LaAa;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LaAa;-><init>(LazS;LazT;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LazS;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    :pswitch_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "setForceLocalDispatch called."

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LazS;->d:Z

    sget-object v0, LazV;->a:[I

    iget-object v1, p0, LazS;->a:LazW;

    invoke-virtual {v1}, LazW;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, LazS;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_2
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LazS;->c:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized a(ILandroid/content/Intent;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, LazW;->e:LazW;

    iput-object v0, p0, LazS;->a:LazW;

    iget v0, p0, LazS;->a:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Service unavailable (code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), will retry."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaAj;->d(Ljava/lang/String;)V

    invoke-direct {p0}, LazS;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Service unavailable (code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), using local store."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaAj;->d(Ljava/lang/String;)V

    invoke-direct {p0}, LazS;->j()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/fi;",
            ">;)V"
        }
    .end annotation

    const-string v0, "putHit called"

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    iget-object v6, p0, LazS;->a:Ljava/util/Queue;

    new-instance v0, LazZ;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LazZ;-><init>(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, LazS;->h()V

    return-void
.end method

.method public b()V
    .locals 2

    sget-object v0, LazV;->a:[I

    iget-object v1, p0, LazS;->a:LazW;

    invoke-virtual {v1}, LazW;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LazS;->a:Z

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, LazS;->i()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, LazS;->a:Lazq;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lazr;

    iget-object v1, p0, LazS;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Lazr;-><init>(Landroid/content/Context;Lazt;Lazu;)V

    iput-object v0, p0, LazS;->a:Lazq;

    invoke-direct {p0}, LazS;->k()V

    goto :goto_0
.end method

.method public declared-synchronized d()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LazS;->b:Ljava/util/Timer;

    invoke-direct {p0, v0}, LazS;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, LazS;->b:Ljava/util/Timer;

    const/4 v0, 0x0

    iput v0, p0, LazS;->a:I

    const-string v0, "Connected to service"

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    sget-object v0, LazW;->b:LazW;

    iput-object v0, p0, LazS;->a:LazW;

    iget-boolean v0, p0, LazS;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, LazS;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LazS;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, LazS;->h()V

    iget-object v0, p0, LazS;->c:Ljava/util/Timer;

    invoke-direct {p0, v0}, LazS;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, LazS;->c:Ljava/util/Timer;

    new-instance v0, Ljava/util/Timer;

    const-string v1, "disconnect check"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LazS;->c:Ljava/util/Timer;

    iget-object v0, p0, LazS;->c:Ljava/util/Timer;

    new-instance v1, LazX;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LazX;-><init>(LazS;LazT;)V

    iget-wide v2, p0, LazS;->b:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LazS;->a:LazW;

    sget-object v1, LazW;->f:LazW;

    if-ne v0, v1, :cond_0

    const-string v0, "Disconnected from service"

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    invoke-direct {p0}, LazS;->g()V

    sget-object v0, LazW;->g:LazW;

    iput-object v0, p0, LazS;->a:LazW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "Unexpected disconnect."

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    sget-object v0, LazW;->e:LazW;

    iput-object v0, p0, LazS;->a:LazW;

    iget v0, p0, LazS;->a:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    invoke-direct {p0}, LazS;->m()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-direct {p0}, LazS;->j()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public f()V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "clearHits called"

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    iget-object v0, p0, LazS;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    sget-object v0, LazV;->a:[I

    iget-object v1, p0, LazS;->a:LazW;

    invoke-virtual {v1}, LazW;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LazS;->b:Z

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, LazS;->a:Lazv;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lazv;->a(J)V

    iput-boolean v4, p0, LazS;->b:Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, LazS;->a:Lazq;

    invoke-interface {v0}, Lazq;->a()V

    iput-boolean v4, p0, LazS;->b:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
