.class final LaPu;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaPx;

.field final synthetic a:LaQw;

.field final synthetic a:Landroid/content/Context;

.field final synthetic a:Lcom/google/android/gms/internal/dx;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPx;LaQw;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LaPu;->a:Landroid/content/Context;

    iput-object p2, p0, LaPu;->a:Lcom/google/android/gms/internal/dx;

    iput-object p3, p0, LaPu;->a:LaPx;

    iput-object p4, p0, LaPu;->a:LaQw;

    iput-object p5, p0, LaPu;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, LaPu;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/internal/aq;

    invoke-direct {v1}, Lcom/google/android/gms/internal/aq;-><init>()V

    const/4 v4, 0x0

    iget-object v3, p0, LaPu;->a:Lcom/google/android/gms/internal/dx;

    iget-object v5, v3, Lcom/google/android/gms/internal/dx;->a:Lcom/google/android/gms/internal/fa;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/fc;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;ZZLaSP;Lcom/google/android/gms/internal/fa;)Lcom/google/android/gms/internal/fc;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fc;->setWillNotDraw(Z)V

    iget-object v1, p0, LaPu;->a:LaPx;

    invoke-virtual {v1, v0}, LaPx;->a(Lcom/google/android/gms/internal/fc;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v1

    const-string v2, "/invalidRequest"

    iget-object v3, p0, LaPu;->a:LaPx;

    iget-object v3, v3, LaPx;->a:LaNu;

    invoke-virtual {v1, v2, v3}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v2, "/loadAdURL"

    iget-object v3, p0, LaPu;->a:LaPx;

    iget-object v3, v3, LaPx;->b:LaNu;

    invoke-virtual {v1, v2, v3}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v2, "/log"

    sget-object v3, LaNl;->g:LaNu;

    invoke-virtual {v1, v2, v3}, LaQu;->a(Ljava/lang/String;LaNu;)V

    iget-object v2, p0, LaPu;->a:LaQw;

    invoke-virtual {v1, v2}, LaQu;->a(LaQw;)V

    const-string v1, "Loading the JS library."

    invoke-static {v1}, LaQc;->a(Ljava/lang/String;)V

    iget-object v1, p0, LaPu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fc;->loadUrl(Ljava/lang/String;)V

    return-void
.end method
