.class public LaeR;
.super LaeP;
.source "XmlAclGDataSerializer.java"


# direct methods
.method constructor <init>(Lbxr;LaeB;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, LaeP;-><init>(Lbxr;Lbxb;)V

    .line 36
    return-void
.end method

.method private a()LaeB;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LaeR;->a()Lbxb;

    move-result-object v0

    check-cast v0, LaeB;

    return-object v0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, Lqq;->b:Lqq;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 87
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqq;

    .line 88
    const-string v2, "http://schemas.google.com/acl/2007"

    const-string v3, "additionalRole"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 89
    const/4 v2, 0x0

    const-string v3, "value"

    invoke-virtual {v0}, Lqq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v2, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 90
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v2, "additionalRole"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_1

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :cond_1
    return-void
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Lqv;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    sget-object v0, Lqv;->f:Lqv;

    if-eq p1, v0, :cond_0

    sget-object v0, Lqv;->e:Lqv;

    if-ne p1, v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 61
    const-string v0, "value"

    invoke-virtual {p1}, Lqv;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 62
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 64
    if-eqz p2, :cond_0

    .line 65
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 66
    const-string v0, "value"

    sget-object v1, Lqv;->d:Lqv;

    invoke-virtual {v1}, Lqv;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 67
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Lqx;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    sget-object v0, Lqx;->e:Lqx;

    if-ne p1, v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 76
    :cond_0
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "scope"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 77
    sget-object v0, Lbxq;->l:Ljava/lang/String;

    invoke-virtual {p1}, Lqx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 78
    if-eqz p2, :cond_1

    .line 79
    const-string v0, "value"

    invoke-interface {p0, v2, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 81
    :cond_1
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "scope"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method


# virtual methods
.method protected a(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2

    .prologue
    .line 97
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v0, "gd"

    const-string v1, "http://schemas.google.com/g/2005"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v0, "gAcl"

    const-string v1, "http://schemas.google.com/acl/2007"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method protected b(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 5

    .prologue
    .line 46
    invoke-direct {p0}, LaeR;->a()LaeB;

    move-result-object v0

    .line 48
    const-string v1, "XmlAclGDataSerializer"

    const-string v2, "Serializing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 50
    invoke-virtual {v0}, LaeB;->a()Lqv;

    move-result-object v1

    invoke-virtual {v0}, LaeB;->a()Z

    move-result v2

    invoke-static {p1, v1, v2}, LaeR;->a(Lorg/xmlpull/v1/XmlSerializer;Lqv;Z)V

    .line 51
    invoke-virtual {v0}, LaeB;->a()Lqx;

    move-result-object v1

    invoke-virtual {v0}, LaeB;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, LaeR;->a(Lorg/xmlpull/v1/XmlSerializer;Lqx;Ljava/lang/String;)V

    .line 52
    invoke-virtual {v0}, LaeB;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, v0}, LaeR;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V

    .line 53
    return-void
.end method
