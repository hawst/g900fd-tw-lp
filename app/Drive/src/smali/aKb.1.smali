.class public LaKb;
.super Ljava/lang/Object;
.source "ApiaryAccountMetadataEntry.java"

# interfaces
.implements LaJQ;


# instance fields
.field private a:J

.field private a:LaHK;

.field private a:LbpT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpT",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/api/services/drive/model/About;

.field private final a:Lcom/google/api/services/drive/model/AppList;

.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaGv;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lqt;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/api/services/drive/model/About;Lcom/google/api/services/drive/model/AppList;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About;

    iput-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    .line 99
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/AppList;

    iput-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/AppList;

    .line 100
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/api/services/drive/model/About;Lcom/google/api/services/drive/model/AppList;LaKc;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, LaKb;-><init>(Lcom/google/api/services/drive/model/About;Lcom/google/api/services/drive/model/AppList;)V

    return-void
.end method

.method private a(Lcom/google/api/services/drive/model/AppList;)LaHK;
    .locals 8

    .prologue
    .line 254
    new-instance v2, LaHM;

    invoke-direct {v2}, LaHM;-><init>()V

    .line 255
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/AppList;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/App;

    .line 256
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->a()Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->b()Ljava/lang/String;

    move-result-object v4

    .line 258
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->c()Ljava/lang/String;

    move-result-object v5

    .line 259
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->a()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 260
    new-instance v7, LaHJ;

    invoke-direct {v7, v1, v4, v5, v6}, LaHJ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 262
    const-string v1, "installedAppSupportsMobileBrowser"

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/model/App;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 263
    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 264
    const-string v0, "ApiaryAccountMetadataEntry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t support mobile browsers; ignoring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const/4 v0, 0x0

    .line 296
    :goto_1
    return-object v0

    .line 268
    :cond_0
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, LaHJ;->e(Ljava/lang/String;)V

    .line 270
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 271
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 272
    invoke-virtual {v7, v1}, LaHJ;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 276
    :cond_1
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->d()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 277
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 278
    invoke-virtual {v7, v1}, LaHJ;->b(Ljava/lang/String;)V

    goto :goto_3

    .line 282
    :cond_2
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->a()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 283
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 284
    invoke-virtual {v7, v1}, LaHJ;->c(Ljava/lang/String;)V

    goto :goto_4

    .line 288
    :cond_3
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 289
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/App;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 290
    invoke-virtual {v7, v0}, LaHJ;->d(Ljava/lang/String;)V

    goto :goto_5

    .line 294
    :cond_4
    invoke-interface {v2}, LaHK;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    move-object v0, v2

    .line 296
    goto/16 :goto_1
.end method

.method private a(Lcom/google/api/services/drive/model/About;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/drive/model/About;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lqt;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 222
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 223
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    .line 225
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, LaKb;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 228
    :cond_0
    return-object v1
.end method

.method private a(Ljava/util/List;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lqt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 233
    if-eqz p1, :cond_0

    .line 234
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    .line 235
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lqv;->a(Ljava/lang/String;)Lqv;

    move-result-object v3

    .line 236
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, LaKb;->b(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    .line 237
    invoke-static {v3, v0}, Lqt;->a(Lqv;Ljava/util/Set;)Lqt;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 240
    :cond_0
    return-object v1
.end method

.method private b(Ljava/util/List;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 245
    if-eqz p1, :cond_0

    .line 246
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 247
    invoke-static {v0}, Lqq;->a(Ljava/lang/String;)Lqq;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 250
    :cond_0
    return-object v1
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->b()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGv;)J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 151
    iget-object v0, p0, LaKb;->a:Ljava/util/Map;

    if-nez v0, :cond_3

    .line 152
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LaKb;->a:Ljava/util/Map;

    .line 153
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 154
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    .line 155
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 156
    :goto_1
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Ljava/lang/String;

    move-result-object v6

    const-string v7, "*"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 157
    iget-wide v2, p0, LaKb;->a:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 158
    const-string v2, "ApiaryAccountMetadataEntry"

    const-string v3, "Multiple wildcard import sizes logged"

    invoke-static {v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, LaKb;->a:J

    goto :goto_0

    :cond_1
    move-wide v2, v4

    .line 155
    goto :goto_1

    .line 163
    :cond_2
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaGv;->b(Ljava/lang/String;)LaGv;

    move-result-object v0

    .line 164
    iget-object v6, p0, LaKb;->a:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 170
    :cond_3
    iget-object v0, p0, LaKb;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 171
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_2
    return-wide v0

    :cond_4
    iget-wide v0, p0, LaKb;->a:J

    goto :goto_2
.end method

.method public a()LaHK;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, LaKb;->a:LaHK;

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/AppList;

    invoke-direct {p0, v0}, LaKb;->a(Lcom/google/api/services/drive/model/AppList;)LaHK;

    move-result-object v0

    iput-object v0, p0, LaKb;->a:LaHK;

    .line 210
    :cond_0
    iget-object v0, p0, LaKb;->a:LaHK;

    invoke-static {v0}, LaHR;->a(LaHK;)LaHR;

    move-result-object v0

    return-object v0
.end method

.method public a()LaJS;
    .locals 1

    .prologue
    .line 302
    sget-object v0, LaJS;->a:LaJS;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 215
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 216
    iget-object v1, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v1}, Lcom/google/api/services/drive/model/About;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 217
    iget-object v1, p0, LaKb;->a:Lcom/google/api/services/drive/model/AppList;

    invoke-virtual {v1}, Lcom/google/api/services/drive/model/AppList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 218
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, LaKb;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 105
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaKb;->a:Ljava/util/Set;

    .line 106
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$Features;

    .line 108
    iget-object v2, p0, LaKb;->a:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$Features;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, LaKb;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, LaKb;->a:LbpT;

    if-nez v0, :cond_2

    .line 128
    invoke-static {}, LbnA;->a()LbnA;

    move-result-object v0

    iput-object v0, p0, LaKb;->a:LbpT;

    .line 129
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$ImportFormats;

    .line 131
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    .line 132
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$ImportFormats;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 133
    invoke-static {v1}, LaGv;->b(Ljava/lang/String;)LaGv;

    move-result-object v4

    .line 134
    sget-object v5, LaGv;->k:LaGv;

    invoke-virtual {v4, v5}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 135
    const-string v4, "ApiaryAccountMetadataEntry"

    const-string v5, "Unknown mime type (%s) returned in supported import map"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 139
    :cond_1
    iget-object v1, p0, LaKb;->a:LbpT;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About$ImportFormats;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, LaGv;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v5, v4}, LbpT;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_2
    iget-object v0, p0, LaKb;->a:LbpT;

    invoke-interface {v0, p1}, LbpT;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_3

    :goto_1
    return-object v0

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->d()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->c()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->c()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lqt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, LaKb;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-direct {p0, v0}, LaKb;->a(Lcom/google/api/services/drive/model/About;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LaKb;->b:Ljava/util/Map;

    .line 180
    :cond_0
    iget-object v0, p0, LaKb;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 181
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public c()J
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->a()Ljava/lang/Long;

    move-result-object v0

    .line 198
    iget-object v1, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v1}, Lcom/google/api/services/drive/model/About;->a()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_0

    .line 199
    const-string v0, "ApiaryAccountMetadataEntry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Largest change id should not be null for About: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 202
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 191
    invoke-virtual {p0}, LaKb;->a()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 192
    iget-object v0, p0, LaKb;->a:Lcom/google/api/services/drive/model/About;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->d()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
