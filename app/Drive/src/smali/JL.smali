.class LJL;
.super Ljava/lang/Object;
.source "DocListExtraActionsHelperImpl.java"

# interfaces
.implements LJK;


# instance fields
.field a:LQa;

.field private final a:LQr;

.field a:LSv;

.field private final a:Landroid/app/Activity;

.field private final a:Latd;

.field private final a:LqK;

.field private final a:LqV;

.field private final a:LsC;

.field private final a:Lwm;


# direct methods
.method constructor <init>(LqK;Landroid/app/Activity;LQr;Lwm;LqV;Latd;LsC;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, LJL;->a:LqK;

    .line 54
    iput-object p2, p0, LJL;->a:Landroid/app/Activity;

    .line 55
    iput-object p3, p0, LJL;->a:LQr;

    .line 56
    iput-object p4, p0, LJL;->a:Lwm;

    .line 57
    iput-object p5, p0, LJL;->a:LqV;

    .line 58
    iput-object p6, p0, LJL;->a:Latd;

    .line 59
    iput-object p7, p0, LJL;->a:LsC;

    .line 60
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, LJL;->a:LQa;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, LJL;->a:LQa;

    iget-object v1, p0, LJL;->a:LqV;

    invoke-interface {v1}, LqV;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LQa;->a(LaFO;)V

    .line 67
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, LJL;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "settingsEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, LJL;->a:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LJL;->a:Landroid/app/Activity;

    const-class v3, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 74
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, LJL;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "helpEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, LJL;->a:LQr;

    iget-object v2, p0, LJL;->a:LsC;

    invoke-interface {v2}, LsC;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LJL;->a:LsC;

    .line 82
    invoke-interface {v3}, LsC;->b()Ljava/lang/String;

    move-result-object v3

    .line 81
    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 84
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 86
    iget-object v1, p0, LJL;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, LJL;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "feedBackEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v0, Lali;

    iget-object v1, p0, LJL;->a:Landroid/app/Activity;

    const-string v2, "android_docs"

    invoke-direct {v0, v1, v2}, Lali;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0}, Lali;->a()V

    .line 96
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, LJL;->a:LSv;

    iget-object v1, p0, LJL;->a:Landroid/app/Activity;

    iget-object v2, p0, LJL;->a:LsC;

    iget-object v3, p0, LJL;->a:Lwm;

    .line 101
    invoke-interface {v2, v3}, LsC;->a(Lwm;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LJL;->a:LsC;

    .line 102
    invoke-interface {v3}, LsC;->a()Landroid/net/Uri;

    move-result-object v3

    .line 100
    invoke-interface {v0, v1, v2, v3}, LSv;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;)V

    .line 103
    return-void
.end method
