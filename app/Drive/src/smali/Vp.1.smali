.class LVp;
.super Ljava/lang/Object;
.source "DelayedRemoveEntriesOperation.java"

# interfaces
.implements LVL;


# instance fields
.field final synthetic a:LVm;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LVm;LbmF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 72
    iput-object p1, p0, LVp;->a:LVm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmF;

    iput-object v0, p0, LVp;->a:LbmF;

    .line 74
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, LVp;->a:LVm;

    iget-object v1, p0, LVp;->a:LbmF;

    invoke-static {v0, v1}, LVm;->a(LVm;LbmF;)V

    .line 85
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 78
    new-instance v0, LVo;

    iget-object v1, p0, LVp;->a:LVm;

    iget-object v2, p0, LVp;->a:LbmF;

    invoke-direct {v0, v1, v2}, LVo;-><init>(LVm;LbmF;)V

    .line 79
    iget-object v1, p0, LVp;->a:LVm;

    invoke-static {v1}, LVm;->a(LVm;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 80
    return-void
.end method
