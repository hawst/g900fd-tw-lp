.class public LGy;
.super LFM;
.source "IntentStateMachine.java"


# instance fields
.field private final a:LGx;

.field private final a:Landroid/content/Intent;

.field private final a:Lcom/google/android/apps/docs/utils/ParcelableTask;


# direct methods
.method public constructor <init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 31
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LGy;-><init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/docs/utils/ParcelableTask;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/docs/utils/ParcelableTask;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p3}, LFM;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 47
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGx;

    iput-object v0, p0, LGy;->a:LGx;

    .line 48
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, LGy;->a:Landroid/content/Intent;

    .line 49
    iput-object p5, p0, LGy;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, LGy;->a:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 55
    iget-object v0, p0, LGy;->a:LGx;

    iget-object v1, p0, LGy;->a:Landroid/content/Intent;

    iget-object v2, p0, LGy;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    invoke-interface {v0, v1, v2}, LGx;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/utils/ParcelableTask;)V

    .line 56
    const/4 v0, -0x1

    return v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method
