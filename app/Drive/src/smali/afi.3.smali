.class public abstract Lafi;
.super Ljava/lang/Object;
.source "SingleFeedFilter.java"


# static fields
.field public static final a:Lafi;

.field private static final a:Landroid/net/Uri;

.field public static final b:Lafi;


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "https://docs.google.com/feeds/default/private/full?showdeleted=true&showroot=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lafi;->a:Landroid/net/Uri;

    .line 161
    new-instance v0, Lafm;

    const-string v1, "all"

    invoke-direct {v0, v1}, Lafm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lafi;->a:Lafi;

    .line 165
    new-instance v0, Lafn;

    const-string v1, "none"

    invoke-direct {v0, v1}, Lafn;-><init>(Ljava/lang/String;)V

    sput-object v0, Lafi;->b:Lafi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lafi;-><init>(Ljava/lang/String;Z)V

    .line 59
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lafj;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lafi;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lafi;->a:Ljava/lang/String;

    .line 70
    iput-boolean p2, p0, Lafi;->a:Z

    .line 71
    return-void
.end method

.method private static a(Lafi;Lafi;)Lafi;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    sget-object v0, Lafi;->b:Lafi;

    invoke-virtual {p0, v0}, Lafi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lafi;->b:Lafi;

    invoke-virtual {p1, v0}, Lafi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    :cond_0
    sget-object p1, Lafi;->b:Lafi;

    .line 145
    :cond_1
    :goto_0
    return-object p1

    .line 137
    :cond_2
    sget-object v0, Lafi;->a:Lafi;

    invoke-virtual {p0, v0}, Lafi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 139
    sget-object v0, Lafi;->a:Lafi;

    invoke-virtual {p1, v0}, Lafi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object p1, p0

    .line 140
    goto :goto_0

    .line 144
    :cond_3
    iget-boolean v0, p0, Lafi;->a:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lafi;->a:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 145
    :goto_1
    new-instance v3, Lafj;

    const-string v4, "Compose[%s, %s]"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v2

    aput-object p1, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1, v0, p0, p1}, Lafj;-><init>(Ljava/lang/String;ZLafi;Lafi;)V

    move-object p1, v3

    goto :goto_0

    :cond_4
    move v0, v2

    .line 144
    goto :goto_1
.end method

.method public static a(LbmY;)Lafi;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Lafu;",
            ">;)",
            "Lafi;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 194
    invoke-virtual {p0}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 195
    new-instance v0, Lafo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sources: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1, p0}, Lafo;-><init>(Ljava/lang/String;ZLbmY;)V

    return-object v0

    :cond_0
    move v0, v1

    .line 194
    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)Lafi;
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lafi;->a(Ljava/lang/String;ZZ)Lafi;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lafi;
    .locals 3

    .prologue
    .line 249
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    new-instance v0, Lafq;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "category:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lafq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;ZZ)Lafi;
    .locals 3

    .prologue
    .line 280
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    new-instance v0, Lafr;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "collection:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0, p2}, Lafr;-><init>(Ljava/lang/String;ZLjava/lang/String;Z)V

    return-object v0
.end method

.method public static final a(Ljava/util/Date;LaGF;)Lafi;
    .locals 4

    .prologue
    .line 222
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    if-nez p0, :cond_0

    .line 224
    sget-object v0, Lafi;->b:Lafi;

    .line 228
    :goto_0
    return-object v0

    .line 225
    :cond_0
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 226
    sget-object v0, Lafi;->a:Lafi;

    goto :goto_0

    .line 228
    :cond_1
    new-instance v0, Lafp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "syncClipTime: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lafp;-><init>(Ljava/lang/String;Ljava/util/Date;LaGF;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Set;)Lafi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LaGv;",
            ">;)",
            "Lafi;"
        }
    .end annotation

    .prologue
    .line 321
    new-instance v0, Laft;

    const-string v1, ""

    invoke-direct {v0, v1, p0}, Laft;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public static a(Ljava/util/Set;Ljava/util/Set;)Lafi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LaGv;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lafi;"
        }
    .end annotation

    .prologue
    .line 361
    new-instance v0, Lafl;

    const-string v1, ""

    invoke-direct {v0, v1, p0, p1}, Lafl;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V

    return-object v0
.end method

.method public static a(Z)Lafi;
    .locals 2

    .prologue
    .line 270
    const-string v0, "root"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lafi;->a(Ljava/lang/String;ZZ)Lafi;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(",
            "Ljava/util/Collection",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 382
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 383
    return-object v0
.end method

.method static synthetic a(Landroid/net/Uri$Builder;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 39
    invoke-static {p0, p1}, Lafi;->b(Landroid/net/Uri$Builder;Ljava/util/List;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)Lafi;
    .locals 3

    .prologue
    .line 311
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    new-instance v0, Lafs;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Query: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lafs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/util/Set;)Lafi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lafi;"
        }
    .end annotation

    .prologue
    .line 339
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 341
    new-instance v0, Lafk;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MIME types: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lafk;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0

    .line 339
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/net/Uri$Builder;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 265
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 267
    :cond_0
    return-void
.end method

.method static c(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 392
    if-nez p0, :cond_0

    .line 416
    :goto_0
    return-object v0

    .line 395
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 397
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 398
    const-string v1, "-"

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 399
    if-ltz v1, :cond_1

    .line 401
    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v0, v1, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 402
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 403
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 404
    invoke-static {v3}, Lafi;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 406
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 407
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 409
    :cond_2
    invoke-static {p0}, Lanl;->a(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lafi;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 410
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 411
    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lafi;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 412
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 413
    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_2

    .line 416
    :cond_4
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lafi;)Lafi;
    .locals 1

    .prologue
    .line 158
    invoke-static {p0, p1}, Lafi;->a(Lafi;Lafi;)Lafi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 103
    if-nez p1, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 108
    :goto_0
    return-object v0

    .line 106
    :cond_0
    invoke-virtual {p0, p1}, Lafi;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 107
    invoke-static {v0}, Lafi;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lafi;->a:Z

    return v0
.end method

.method protected abstract b(Landroid/net/Uri;)Landroid/net/Uri;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    instance-of v2, p1, Lafi;

    if-nez v2, :cond_2

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    check-cast p1, Lafi;

    .line 81
    iget-boolean v2, p0, Lafi;->a:Z

    iget-boolean v3, p1, Lafi;->a:Z

    if-ne v2, v3, :cond_3

    sget-object v2, Lafi;->a:Landroid/net/Uri;

    .line 82
    invoke-virtual {p0, v2}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lafi;->a:Landroid/net/Uri;

    invoke-virtual {p1, v3}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 89
    sget-object v0, Lafi;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 90
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    iget-boolean v2, p0, Lafi;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 95
    const-string v1, "Filter[%s%s]"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v3, p0, Lafi;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget-boolean v0, p0, Lafi;->a:Z

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ", outside Drive"

    goto :goto_0
.end method
