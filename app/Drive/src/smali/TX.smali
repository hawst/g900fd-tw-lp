.class public LTX;
.super LTY;
.source "LegacyPartnerAppInfo.java"


# static fields
.field public static final a:LTX;

.field public static final b:LTX;

.field public static final c:LTX;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 10
    new-instance v0, LTX;

    const-string v1, "com.google.android.apps.docs"

    const-class v2, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    .line 12
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "com.google.android.apps.docs.statesyncer"

    invoke-direct {v0, v1, v2, v3, v4}, LTX;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LTX;->a:LTX;

    .line 17
    new-instance v0, LTX;

    const-string v1, "com.google.android.apps.docs.editors.docs"

    const-class v2, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    .line 19
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.apps.docs.editors.kix.KixDocumentCreatorActivity"

    const-string v4, "com.google.android.apps.docs.editors.kix.statesyncer"

    invoke-direct {v0, v1, v2, v3, v4}, LTX;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LTX;->b:LTX;

    .line 24
    new-instance v0, LTX;

    const-string v1, "com.google.android.apps.docs.editors.sheets"

    const-class v2, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    .line 26
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.apps.docs.editors.ritz.RitzDocumentCreatorActivity"

    const-string v4, "com.google.android.apps.docs.editors.trix.statesyncer"

    invoke-direct {v0, v1, v2, v3, v4}, LTX;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LTX;->c:LTX;

    .line 24
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LTY;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 35
    iput-object p4, p0, LTX;->a:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LTX;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method
