.class public LaHc;
.super LaGE;
.source "SharedEntryCursor.java"


# instance fields
.field private a:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method private constructor <init>(LaGA;)V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0, p1}, LaGE;-><init>(LaGA;)V

    .line 28
    invoke-interface {p1}, LaGA;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 33
    :goto_0
    return-void

    .line 31
    :cond_0
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    goto :goto_0
.end method

.method private constructor <init>(LaHc;)V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p1, LaHc;->a:LaGA;

    invoke-direct {p0, v0}, LaGE;-><init>(LaGA;)V

    .line 18
    invoke-virtual {p1}, LaHc;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 24
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v0, p1, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 22
    iget-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    goto :goto_0
.end method

.method public static a(LaGA;)LaHc;
    .locals 1

    .prologue
    .line 36
    instance-of v0, p0, LaHc;

    if-eqz v0, :cond_0

    .line 37
    check-cast p0, LaHc;

    .line 38
    new-instance v0, LaHc;

    invoke-direct {v0, p0}, LaHc;-><init>(LaHc;)V

    .line 40
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LaHc;

    invoke-direct {v0, p0}, LaHc;-><init>(LaGA;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v0, :cond_0

    iget-object v0, p0, LaHc;->a:LaGA;

    invoke-interface {v0}, LaGA;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "The cursor is already closed."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 48
    iget-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->decrementAndGet()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 49
    invoke-super {p0}, LaGE;->a()V

    .line 51
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LaHc;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 52
    return-void

    .line 46
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
