.class public LKs;
.super Ljava/lang/Object;
.source "DropToThisFolderListener.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LCU;

.field private final a:LJR;

.field private a:LKA;

.field private final a:LKB;

.field private final a:LKe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LKe",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LKz;

.field private final a:Lamd;

.field private final a:Landroid/os/Handler;

.field private final a:LbsF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsF",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Runnable;

.field private final a:Lwm;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(LKB;LCU;LJR;Lwm;Lamd;)V
    .locals 2

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-object v0, LKA;->a:LKA;

    iput-object v0, p0, LKs;->a:LKA;

    .line 43
    new-instance v0, LKt;

    invoke-direct {v0, p0}, LKt;-><init>(LKs;)V

    iput-object v0, p0, LKs;->a:Ljava/lang/Runnable;

    .line 62
    new-instance v0, LKu;

    invoke-direct {v0, p0}, LKu;-><init>(LKs;)V

    iput-object v0, p0, LKs;->b:Ljava/lang/Runnable;

    .line 124
    new-instance v0, LKz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LKz;-><init>(LKs;LKt;)V

    iput-object v0, p0, LKs;->a:LKz;

    .line 126
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, LKs;->a:Landroid/os/Handler;

    .line 132
    new-instance v0, LKv;

    invoke-direct {v0, p0}, LKv;-><init>(LKs;)V

    iput-object v0, p0, LKs;->a:LbsF;

    .line 157
    new-instance v0, LKx;

    invoke-direct {v0, p0}, LKx;-><init>(LKs;)V

    iput-object v0, p0, LKs;->a:LKe;

    .line 176
    iput-object p1, p0, LKs;->a:LKB;

    .line 177
    iput-object p2, p0, LKs;->a:LCU;

    .line 178
    iput-object p3, p0, LKs;->a:LJR;

    .line 179
    iput-object p5, p0, LKs;->a:Lamd;

    .line 180
    iput-object p4, p0, LKs;->a:Lwm;

    .line 182
    iget-object v0, p0, LKs;->a:LKe;

    invoke-virtual {p3, v0}, LJR;->a(LKe;)V

    .line 184
    new-instance v0, LKy;

    invoke-direct {v0, p0}, LKy;-><init>(LKs;)V

    invoke-interface {p4, v0}, Lwm;->a(Lwn;)V

    .line 193
    return-void
.end method

.method static synthetic a(LKs;)LCU;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->a:LCU;

    return-object v0
.end method

.method static synthetic a(LKs;)LJR;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->a:LJR;

    return-object v0
.end method

.method static synthetic a(LKs;)LKA;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->a:LKA;

    return-object v0
.end method

.method static synthetic a(LKs;LKA;)LKA;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, LKs;->a:LKA;

    return-object p1
.end method

.method static synthetic a(LKs;)LKB;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->a:LKB;

    return-object v0
.end method

.method static synthetic a(LKs;)Lamd;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->a:Lamd;

    return-object v0
.end method

.method static synthetic a(LKs;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a()LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbsU",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, LKs;->a:LbsU;

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, LKs;->a:Lamd;

    iget-object v1, p0, LKs;->a:Lwm;

    .line 206
    invoke-virtual {v0, v1}, Lamd;->a(Lwm;)LbsU;

    move-result-object v0

    .line 208
    iget-object v1, p0, LKs;->a:LbsF;

    .line 209
    invoke-static {v0, v1}, LbsK;->a(LbsU;LbsF;)LbsU;

    move-result-object v0

    iput-object v0, p0, LKs;->a:LbsU;

    .line 211
    :cond_0
    iget-object v0, p0, LKs;->a:LbsU;

    return-object v0
.end method

.method static synthetic a(LKs;)LbsU;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, LKs;->a()LbsU;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LKs;LbsU;)LbsU;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, LKs;->a:LbsU;

    return-object p1
.end method

.method static synthetic a(LKs;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(LKs;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKs;->b:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, LKs;->a:LKB;

    invoke-virtual {v0}, LKB;->a()V

    .line 201
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, LKs;->a:LKz;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 197
    return-void
.end method
