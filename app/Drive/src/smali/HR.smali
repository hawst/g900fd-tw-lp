.class public LHR;
.super LHt;
.source "MainGridViewBinder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LHt",
        "<",
        "LHQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final a:LCt;

.field private final a:LDk;

.field private final a:Landroid/support/v4/app/Fragment;

.field private final a:Landroid/view/LayoutInflater;

.field private final a:LtK;

.field private final a:Z

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget v0, Lxe;->doc_grid_item:I

    sput v0, LHR;->a:I

    .line 38
    sget v0, Lxe;->doc_grid_folder:I

    sput v0, LHR;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LBf;LDU;LCU;LamF;LDk;ILtK;LQX;Lapd;Lapc;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LCt;LDg;)V
    .locals 11

    .prologue
    .line 63
    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p10

    move-object/from16 v7, p11

    move-object/from16 v8, p13

    move-object/from16 v9, p12

    move-object/from16 v10, p15

    invoke-direct/range {v1 .. v10}, LHt;-><init>(Landroid/content/Context;LBf;LDU;LamF;LQX;Lapd;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lapc;LDg;)V

    .line 72
    iput-object p2, p0, LHR;->a:Landroid/support/v4/app/Fragment;

    .line 73
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, LHR;->a:Landroid/view/LayoutInflater;

    .line 74
    invoke-static/range {p7 .. p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDk;

    iput-object v1, p0, LHR;->a:LDk;

    .line 75
    move/from16 v0, p8

    iput v0, p0, LHR;->c:I

    .line 76
    move-object/from16 v0, p9

    iput-object v0, p0, LHR;->a:LtK;

    .line 77
    invoke-virtual/range {p5 .. p5}, LCU;->a()Z

    move-result v1

    iput-boolean v1, p0, LHR;->a:Z

    .line 78
    move-object/from16 v0, p14

    iput-object v0, p0, LHR;->a:LCt;

    .line 79
    return-void
.end method

.method private a(LCv;)I
    .locals 3

    .prologue
    .line 134
    invoke-interface {p1}, LCv;->a()LaGv;

    move-result-object v0

    .line 135
    invoke-interface {p1}, LCv;->e()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-interface {p1}, LCv;->c()Z

    move-result v2

    .line 138
    invoke-static {v0, v1, v2}, LaGt;->a(LaGv;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method private a(Z)I
    .locals 1

    .prologue
    .line 94
    if-eqz p1, :cond_0

    sget v0, LHR;->b:I

    :goto_0
    return v0

    :cond_0
    sget v0, LHR;->a:I

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 142
    instance-of v0, p1, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LHQ;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 153
    :goto_0
    return v0

    .line 147
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 150
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHQ;

    .line 151
    invoke-virtual {v0}, LHQ;->a()Z

    move-result v0

    if-ne p2, v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 153
    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHQ;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 102
    invoke-direct {p0, p2, p4}, LHR;->a(Landroid/view/View;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    check-cast p2, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    .line 104
    invoke-virtual {p2}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHQ;

    .line 105
    invoke-virtual {v0}, LHQ;->a()V

    .line 129
    :cond_0
    :goto_0
    invoke-virtual {v0, p4}, LHQ;->e(Z)V

    .line 130
    return-object v0

    .line 107
    :cond_1
    invoke-direct {p0, p4}, LHR;->a(Z)I

    move-result v0

    .line 108
    iget-object v1, p0, LHR;->a:Landroid/view/LayoutInflater;

    .line 109
    invoke-virtual {v1, v0, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    .line 110
    iget-object v1, p0, LHR;->a:Landroid/view/LayoutInflater;

    iget v2, p0, LHR;->c:I

    sget v0, Lxc;->more_actions_button_container:I

    .line 112
    invoke-virtual {v4, v0}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 110
    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 114
    new-instance v0, LHQ;

    iget-object v1, p0, LHR;->a:Lapd;

    iget-object v2, p0, LHR;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iget-object v3, p0, LHR;->a:Lapc;

    iget-object v5, p0, LHR;->a:LDk;

    invoke-direct/range {v0 .. v5}, LHQ;-><init>(Lapd;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lapc;Landroid/view/View;LDk;)V

    .line 116
    iget-object v1, p0, LHR;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 117
    invoke-virtual {v4, v0}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setTag(Ljava/lang/Object;)V

    .line 118
    iget-object v1, p0, LHR;->a:LtK;

    iget-object v2, p0, LHR;->a:LCt;

    invoke-virtual {v0, v1, v2}, LHQ;->a(LtK;LCt;)V

    .line 120
    iget-boolean v1, p0, LHR;->a:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LHR;->a:LtK;

    sget-object v2, Lry;->S:Lry;

    .line 121
    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 123
    :goto_1
    if-nez v1, :cond_0

    .line 125
    iget-object v1, p0, LHR;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v4}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    move v1, v6

    .line 121
    goto :goto_1
.end method

.method public bridge synthetic a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHq;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2, p3, p4}, LHR;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHQ;

    move-result-object v0

    return-object v0
.end method

.method public a(LCv;LHQ;)V
    .locals 2

    .prologue
    .line 83
    invoke-interface {p1}, LCv;->a()Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-virtual {p2, v0}, LHQ;->a(Ljava/lang/String;)V

    .line 85
    invoke-direct {p0, p1}, LHR;->a(LCv;)I

    move-result v0

    invoke-virtual {p2, v0}, LHQ;->a(I)V

    .line 86
    invoke-interface {p1}, LCv;->a()LaGv;

    move-result-object v0

    .line 88
    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p2}, LHQ;->b()V

    .line 91
    :cond_0
    return-void
.end method

.method public bridge synthetic a(LCv;LHq;)V
    .locals 0

    .prologue
    .line 36
    check-cast p2, LHQ;

    invoke-virtual {p0, p1, p2}, LHR;->a(LCv;LHQ;)V

    return-void
.end method
