.class public LZN;
.super Ljava/lang/Object;
.source "BucketWaitingRateLimiter.java"

# interfaces
.implements LQs;
.implements LZS;


# instance fields
.field private a:I

.field private a:J

.field private final a:LQr;

.field private final a:LaKM;

.field private final a:Ljava/lang/String;

.field private b:I

.field private final b:Ljava/lang/String;

.field private c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(LQr;LaKM;)V
    .locals 2

    .prologue
    .line 79
    const-string v0, "maxTokens"

    const-string v1, "tokenPeriodMilli"

    invoke-direct {p0, p1, p2, v0, v1}, LZN;-><init>(LQr;LaKM;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public constructor <init>(LQr;LaKM;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 89
    const/16 v5, 0xa

    const/16 v6, 0x3e8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, LZN;-><init>(LQr;LaKM;Ljava/lang/String;Ljava/lang/String;II)V

    .line 95
    return-void
.end method

.method public constructor <init>(LQr;LaKM;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p4, p0, LZN;->b:Ljava/lang/String;

    .line 111
    iput-object p3, p0, LZN;->a:Ljava/lang/String;

    .line 112
    iput-object p1, p0, LZN;->a:LQr;

    .line 113
    iput-object p2, p0, LZN;->a:LaKM;

    .line 114
    invoke-interface {p2}, LaKM;->a()J

    move-result-wide v0

    iput-wide v0, p0, LZN;->a:J

    .line 115
    iput p5, p0, LZN;->d:I

    .line 116
    iput p6, p0, LZN;->e:I

    .line 118
    invoke-interface {p1, p0}, LQr;->a(LQs;)V

    .line 120
    invoke-virtual {p0}, LZN;->a()V

    .line 121
    return-void
.end method


# virtual methods
.method declared-synchronized a()J
    .locals 8

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LZN;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v2

    .line 131
    iget-wide v0, p0, LZN;->a:J

    sub-long v0, v2, v0

    .line 137
    iget v4, p0, LZN;->b:I

    neg-int v4, v4

    iget v5, p0, LZN;->a:I

    mul-int/2addr v4, v5

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 138
    iget v0, p0, LZN;->b:I

    iget v1, p0, LZN;->a:I

    mul-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, LZN;->a:J

    .line 139
    iget v0, p0, LZN;->b:I

    neg-int v0, v0

    int-to-long v0, v0

    .line 142
    :cond_0
    iget v4, p0, LZN;->b:I

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-ltz v4, :cond_1

    .line 143
    iget v4, p0, LZN;->c:I

    int-to-long v4, v4

    iget v6, p0, LZN;->b:I

    int-to-long v6, v6

    div-long v6, v0, v6

    add-long/2addr v4, v6

    long-to-int v4, v4

    iput v4, p0, LZN;->c:I

    .line 144
    iget v4, p0, LZN;->b:I

    int-to-long v4, v4

    rem-long/2addr v0, v4

    sub-long v0, v2, v0

    iput-wide v0, p0, LZN;->a:J

    .line 147
    iget v0, p0, LZN;->c:I

    iget v1, p0, LZN;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LZN;->c:I

    .line 150
    :cond_1
    iget v0, p0, LZN;->c:I

    if-lez v0, :cond_2

    .line 151
    iget v0, p0, LZN;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LZN;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    const-wide/16 v0, 0x0

    .line 155
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 154
    :cond_2
    :try_start_1
    iget-wide v0, p0, LZN;->a:J

    iget v4, p0, LZN;->b:I

    int-to-long v4, v4

    add-long/2addr v0, v4

    iput-wide v0, p0, LZN;->a:J

    .line 155
    iget-wide v0, p0, LZN;->a:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long/2addr v0, v2

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 6

    .prologue
    .line 181
    monitor-enter p0

    :try_start_0
    iget v0, p0, LZN;->a:I

    .line 182
    iget v1, p0, LZN;->b:I

    .line 183
    iget-object v2, p0, LZN;->a:LQr;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 184
    :try_start_1
    iget-object v3, p0, LZN;->a:LQr;

    iget-object v4, p0, LZN;->a:Ljava/lang/String;

    iget v5, p0, LZN;->d:I

    invoke-interface {v3, v4, v5}, LQr;->a(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, LZN;->a:I

    .line 185
    iget-object v3, p0, LZN;->a:LQr;

    iget-object v4, p0, LZN;->b:Ljava/lang/String;

    iget v5, p0, LZN;->e:I

    invoke-interface {v3, v4, v5}, LQr;->a(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, LZN;->b:I

    .line 186
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :try_start_2
    iget v2, p0, LZN;->a:I

    if-ne v0, v2, :cond_0

    iget v0, p0, LZN;->b:I

    if-eq v1, v0, :cond_1

    .line 189
    :cond_0
    invoke-virtual {p0}, LZN;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 191
    :cond_1
    monitor-exit p0

    return-void

    .line 186
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 181
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 6

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LZN;->a()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 163
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 164
    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    :cond_0
    monitor-exit p0

    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 168
    :try_start_2
    iget-wide v2, p0, LZN;->a:J

    iget v1, p0, LZN;->b:I

    int-to-long v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, LZN;->a:J

    .line 169
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 175
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, LZN;->c:I

    .line 176
    iget-object v0, p0, LZN;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v0

    iput-wide v0, p0, LZN;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    monitor-exit p0

    return-void

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
