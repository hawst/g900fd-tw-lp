.class LEV;
.super Ljava/lang/Object;
.source "DriveDialogBuilder.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:LEU;

.field final synthetic a:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(LEU;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, LEV;->a:LEU;

    iput-object p2, p0, LEV;->a:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 248
    .line 249
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getMinimumHeight()I

    move-result v0

    .line 262
    :goto_0
    return v0

    .line 255
    :cond_0
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v2, "getSuggestedMinimumHeight"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 256
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 257
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, LEV;->a:LEU;

    invoke-virtual {v0}, LEU;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "android"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 244
    iget-object v1, p0, LEV;->a:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 109
    iget-object v0, p0, LEV;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 110
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 111
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 112
    iget-object v1, p0, LEV;->a:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 114
    const-string v0, "topPanel"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)V

    .line 115
    const-string v0, "contentPanel"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)V

    .line 116
    const-string v0, "customPanel"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)V

    .line 117
    const-string v0, "buttonPanel"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)V

    .line 118
    const-string v0, "parentPanel"

    invoke-direct {p0, v0}, LEV;->b(Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 204
    invoke-direct {p0, v0}, LEV;->a(Landroid/view/View;)I

    move-result v0

    .line 205
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {p1, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 208
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    invoke-direct {p0, p1}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 125
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 127
    :cond_0
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 149
    const-string v0, "titleDivider"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_0

    .line 151
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    :cond_0
    const-string v0, "title_template"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 157
    if-eqz v0, :cond_1

    .line 159
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 160
    iget-object v2, p0, LEV;->a:LEU;

    .line 161
    invoke-virtual {v2}, LEU;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lxa;->m_dialog_content_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 162
    invoke-virtual {v0, v2, v4, v2, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 163
    sget v2, LwZ;->m_app_background:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 164
    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 165
    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 167
    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 130
    invoke-direct {p0, p1}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 131
    if-nez v1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 135
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 136
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 137
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 138
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 140
    invoke-static {}, LakQ;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 141
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 142
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 144
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, LEV;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 173
    sget v1, LwZ;->m_app_background:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 177
    :cond_0
    const-string v0, "scrollView"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_1

    .line 179
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 180
    invoke-direct {p0, v0}, LEV;->a(Landroid/view/View;)V

    .line 181
    sget v1, LwZ;->m_app_background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 185
    :cond_1
    const-string v0, "custom"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_2

    .line 187
    invoke-direct {p0, v0}, LEV;->a(Landroid/view/View;)V

    .line 188
    sget v1, LwZ;->m_app_background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 192
    :cond_2
    const-string v0, "message"

    invoke-direct {p0, v0}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_3

    .line 194
    iget-object v1, p0, LEV;->a:LEU;

    .line 195
    invoke-virtual {v1}, LEU;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxa;->m_dialog_content_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 196
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 197
    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 196
    invoke-virtual {v0, v1, v2, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 198
    const-string v0, "message"

    invoke-direct {p0, v0}, LEV;->b(Ljava/lang/String;)V

    .line 200
    :cond_3
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 220
    invoke-direct {p0, p1}, LEV;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 221
    if-nez v0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    iget-object v1, p0, LEV;->a:LEU;

    invoke-virtual {v1}, LEU;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 225
    const-string v1, "button1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 226
    sget v1, LwZ;->m_dialog_primary_button_text:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 228
    :cond_2
    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 229
    const/4 v3, 0x0

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 230
    sget v3, Lxa;->m_dialog_button_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 231
    const/4 v2, -0x2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 233
    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 234
    if-eqz v0, :cond_0

    .line 237
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setMeasureWithLargestChildEnabled(Z)V

    .line 238
    const v1, 0x800005

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 211
    const-string v0, "button1"

    invoke-direct {p0, v0}, LEV;->c(Ljava/lang/String;)V

    .line 212
    const-string v0, "button2"

    invoke-direct {p0, v0}, LEV;->c(Ljava/lang/String;)V

    .line 213
    const-string v0, "button3"

    invoke-direct {p0, v0}, LEV;->c(Ljava/lang/String;)V

    .line 214
    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, LEV;->a:LEU;

    invoke-static {v0}, LEU;->a(LEU;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, LEV;->a:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setIcon(I)V

    .line 91
    invoke-direct {p0}, LEV;->b()V

    .line 92
    invoke-direct {p0}, LEV;->c()V

    .line 93
    invoke-direct {p0}, LEV;->d()V

    .line 96
    :cond_0
    iget-object v0, p0, LEV;->a:LEU;

    invoke-static {v0}, LEU;->b(LEU;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    invoke-direct {p0}, LEV;->a()V

    .line 100
    :cond_1
    iget-object v0, p0, LEV;->a:LEU;

    invoke-static {v0}, LEU;->a(LEU;)Landroid/content/DialogInterface$OnShowListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, LEV;->a:LEU;

    invoke-static {v0}, LEU;->a(LEU;)Landroid/content/DialogInterface$OnShowListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnShowListener;->onShow(Landroid/content/DialogInterface;)V

    .line 103
    :cond_2
    return-void
.end method
