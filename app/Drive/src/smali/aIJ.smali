.class public final enum LaIJ;
.super Ljava/lang/Enum;
.source "ViewGuid.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaIJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaIJ;

.field private static final synthetic a:[LaIJ;

.field public static final enum b:LaIJ;

.field public static final enum c:LaIJ;


# instance fields
.field private final a:I

.field private final a:LCl;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, LaIJ;

    const-string v1, "MY_DRIVE"

    const-string v3, "mydrive"

    sget-object v4, LCe;->q:LCe;

    sget v5, Lxb;->ic_drive_my_drive:I

    invoke-direct/range {v0 .. v5}, LaIJ;-><init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V

    sput-object v0, LaIJ;->a:LaIJ;

    .line 48
    new-instance v3, LaIJ;

    const-string v4, "SHARED_WITH_ME"

    const-string v6, "shared_with_me"

    sget-object v7, LCe;->o:LCe;

    sget v8, Lxb;->ic_drive_shared_with_me:I

    move v5, v9

    invoke-direct/range {v3 .. v8}, LaIJ;-><init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V

    sput-object v3, LaIJ;->b:LaIJ;

    .line 50
    new-instance v3, LaIJ;

    const-string v4, "STARRED"

    const-string v6, "starred"

    sget-object v7, LCe;->b:LCe;

    sget v8, Lxb;->ic_drive_starred:I

    move v5, v10

    invoke-direct/range {v3 .. v8}, LaIJ;-><init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V

    sput-object v3, LaIJ;->c:LaIJ;

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [LaIJ;

    sget-object v1, LaIJ;->a:LaIJ;

    aput-object v1, v0, v2

    sget-object v1, LaIJ;->b:LaIJ;

    aput-object v1, v0, v9

    sget-object v1, LaIJ;->c:LaIJ;

    aput-object v1, v0, v10

    sput-object v0, LaIJ;->a:[LaIJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LCl;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput-object p3, p0, LaIJ;->a:Ljava/lang/String;

    .line 58
    iput-object p4, p0, LaIJ;->a:LCl;

    .line 59
    iput p5, p0, LaIJ;->a:I

    .line 60
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaIJ;
    .locals 1

    .prologue
    .line 46
    const-class v0, LaIJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaIJ;

    return-object v0
.end method

.method public static values()[LaIJ;
    .locals 1

    .prologue
    .line 46
    sget-object v0, LaIJ;->a:[LaIJ;

    invoke-virtual {v0}, [LaIJ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaIJ;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, LaIJ;->a:I

    return v0
.end method

.method a()LCl;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LaIJ;->a:LCl;

    return-object v0
.end method

.method a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, LaIJ;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LaIJ;->a:LCl;

    invoke-interface {v0}, LCl;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 83
    sget-object v0, LaIJ;->a:LaIJ;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 79
    const-string v0, "%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "view="

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaIJ;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
