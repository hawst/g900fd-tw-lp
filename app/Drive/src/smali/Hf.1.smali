.class public abstract LHf;
.super Ljava/lang/Object;
.source "OpenerOption.java"

# interfaces
.implements LFR;


# static fields
.field private static final a:Landroid/widget/SimpleAdapter$ViewBinder;


# instance fields
.field protected final a:LHh;

.field private final a:Ljava/lang/String;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, LHg;

    invoke-direct {v0}, LHg;-><init>()V

    sput-object v0, LHf;->a:Landroid/widget/SimpleAdapter$ViewBinder;

    return-void
.end method

.method protected constructor <init>(LHh;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHh;

    iput-object v0, p0, LHf;->a:LHh;

    .line 128
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LHf;->a:Ljava/lang/String;

    .line 129
    iput-boolean p3, p0, LHf;->a:Z

    .line 130
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/BaseAdapter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "LHf;",
            ">;)",
            "Landroid/widget/BaseAdapter;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 111
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 112
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHf;

    .line 113
    const-string v3, "key"

    invoke-static {v3, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_0
    new-instance v0, Landroid/widget/SimpleAdapter;

    sget v3, Lxe;->opener_option:I

    new-array v4, v5, [Ljava/lang/String;

    const-string v1, "key"

    aput-object v1, v4, v6

    new-array v5, v5, [I

    sget v1, Lxc;->document_opener_option:I

    aput v1, v5, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 118
    sget-object v1, LHf;->a:Landroid/widget/SimpleAdapter$ViewBinder;

    invoke-virtual {v0, v1}, Landroid/widget/SimpleAdapter;->setViewBinder(Landroid/widget/SimpleAdapter$ViewBinder;)V

    .line 119
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, LHf;->a:Ljava/lang/String;

    return-object v0
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, LHf;->a:Z

    return v0
.end method

.method public abstract a(ZZ)Z
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, LHf;->a:LHh;

    invoke-virtual {v0}, LHh;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
