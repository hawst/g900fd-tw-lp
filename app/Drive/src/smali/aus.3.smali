.class public Laus;
.super Landroid/webkit/WebViewClient;
.source "LoginWebViewClient.java"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "https://accounts\\.(google|youtube)(\\.co(m?))?(\\.\\w{2})?/.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Laus;->a:Ljava/util/regex/Pattern;

    .line 42
    const-string v0, ".*/ServiceLogin$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Laus;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 71
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Require Google account"

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 73
    iput-object p1, p0, Laus;->a:Landroid/accounts/Account;

    .line 74
    return-void

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/accounts/Account;Landroid/webkit/WebView;Landroid/net/Uri;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 110
    const-string v0, "service"

    invoke-virtual {p3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    const-string v1, "weblogin:service=%s&continue=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    const/4 v0, 0x1

    const-string v5, "https://viewer.google.com/?androidweblogin"

    aput-object v5, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 113
    new-instance v0, LavN;

    invoke-virtual {p2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LavN;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LavN;->a()Landroid/accounts/AccountManager;

    move-result-object v0

    move-object v1, p1

    move-object v5, v3

    move-object v6, v3

    .line 115
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 117
    new-instance v1, Laut;

    invoke-direct {v1, p0, v0, p2, p3}, Laut;-><init>(Laus;Landroid/accounts/AccountManagerFuture;Landroid/webkit/WebView;Landroid/net/Uri;)V

    new-array v0, v4, [Ljava/lang/Void;

    .line 141
    invoke-virtual {v1, v0}, Laut;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 142
    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 78
    iget-object v2, p0, Laus;->a:Landroid/accounts/Account;

    if-nez v2, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 85
    const-string v3, "https://viewer.google.com/?androidweblogin"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 87
    iget-object v0, p0, Laus;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    move v0, v1

    .line 88
    goto :goto_0

    .line 89
    :cond_2
    iget-object v3, p0, Laus;->a:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 92
    sget-object v3, Laus;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 94
    const-string v0, "continue"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laus;->a:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Laus;->a:Landroid/accounts/Account;

    invoke-direct {p0, v0, p1, v2}, Laus;->a(Landroid/accounts/Account;Landroid/webkit/WebView;Landroid/net/Uri;)V

    move v0, v1

    .line 96
    goto :goto_0

    .line 97
    :cond_3
    sget-object v2, Laus;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_0

    .line 101
    const-string v0, "LoginWebViewClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempt to load non-whitelisted URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 102
    goto :goto_0
.end method
