.class LLI;
.super LLQ;
.source "SelectionFunctionDoRemoveSingleItem.java"


# instance fields
.field private final a:LPp;

.field private final a:LaGM;

.field private final a:LaKR;

.field private final a:LtK;

.field private final a:LvY;


# direct methods
.method public constructor <init>(LaKR;LPp;LaGM;LvY;LtK;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, LLQ;-><init>()V

    .line 39
    iput-object p1, p0, LLI;->a:LaKR;

    .line 40
    iput-object p2, p0, LLI;->a:LPp;

    .line 41
    iput-object p3, p0, LLI;->a:LaGM;

    .line 42
    iput-object p4, p0, LLI;->a:LvY;

    .line 43
    iput-object p5, p0, LLI;->a:LtK;

    .line 44
    return-void
.end method

.method static synthetic a(LLI;)LvY;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LLI;->a:LvY;

    return-object v0
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 80
    invoke-virtual {p2}, LbmF;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 81
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 86
    invoke-virtual {p3}, LbmF;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p4}, LbmF;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 87
    invoke-virtual {p4, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 89
    iget-object v1, p0, LLI;->a:LvY;

    invoke-interface {v1}, LvY;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    .line 90
    if-nez v1, :cond_2

    .line 92
    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 96
    :goto_1
    iget-object v2, p0, LLI;->a:LPp;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v0}, LaGD;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGD;

    move-result-object v0

    invoke-interface {v2, v0, v1}, LPp;->a(LaGD;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 98
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 99
    return-void

    :cond_0
    move v0, v1

    .line 86
    goto :goto_0

    .line 92
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_1

    .line 94
    :cond_2
    invoke-interface {v1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    goto :goto_1
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, LLI;->a:LtK;

    sget-object v1, Lry;->V:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 74
    :goto_0
    return v0

    .line 53
    :cond_0
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 54
    invoke-virtual {p1, v2}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 55
    iget-object v1, p0, LLI;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LaGu;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    invoke-interface {v0}, LaGu;->e()Z

    move-result v1

    if-nez v1, :cond_3

    .line 56
    new-instance v1, LLJ;

    invoke-direct {v1, p0}, LLJ;-><init>(LLI;)V

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lanj;->a(Ljava/util/concurrent/Callable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 64
    if-nez v1, :cond_2

    invoke-interface {v0}, LaGu;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, LaGu;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 65
    iget-object v1, p0, LLI;->a:LaGM;

    .line 66
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {v1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 68
    goto :goto_0

    :cond_2
    move v0, v3

    .line 71
    goto :goto_0

    :cond_3
    move v0, v2

    .line 74
    goto :goto_0
.end method
