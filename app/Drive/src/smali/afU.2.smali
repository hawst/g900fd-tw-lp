.class public LafU;
.super Ljava/lang/Object;
.source "ContentSyncOverallStatusNotifier.java"

# interfaces
.implements Lahz;


# instance fields
.field private final a:LCo;

.field private final a:LQr;

.field private final a:LUx;

.field private a:LaFO;

.field private final a:LaGM;

.field private final a:LahK;

.field private final a:Lamw;

.field private final a:Landroid/content/Context;

.field private final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "LahS;",
            "LafZ;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Runnable;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LafY;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;LaGM;LUx;Lamx;LahK;LCo;LQr;)V
    .locals 4

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    new-instance v0, LafV;

    invoke-direct {v0, p0}, LafV;-><init>(LafU;)V

    iput-object v0, p0, LafU;->a:Ljava/lang/Runnable;

    .line 226
    invoke-static {}, LafU;->a()LbmL;

    move-result-object v0

    iput-object v0, p0, LafU;->a:LbmL;

    .line 246
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LafU;->a:Landroid/content/Context;

    .line 247
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LafU;->a:LaGM;

    .line 248
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUx;

    iput-object v0, p0, LafU;->a:LUx;

    .line 249
    iput-object p5, p0, LafU;->a:LahK;

    .line 250
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCo;

    iput-object v0, p0, LafU;->a:LCo;

    .line 251
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p0, LafU;->a:LQr;

    .line 253
    new-instance v0, LafW;

    invoke-direct {v0, p0}, LafW;-><init>(LafU;)V

    .line 260
    const/16 v1, 0x3e8

    new-instance v2, LalI;

    .line 261
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, v3}, LalI;-><init>(Landroid/os/Handler;)V

    const-string v3, "ContentSyncOverallStatusNotifier"

    .line 260
    invoke-interface {p4, v0, v1, v2, v3}, Lamx;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lamw;

    move-result-object v0

    iput-object v0, p0, LafU;->a:Lamw;

    .line 263
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LafY;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LafU;->a:Ljava/util/Map;

    .line 264
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LaGM;LUx;Lamx;LahK;LCo;LQr;LafV;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct/range {p0 .. p7}, LafU;-><init>(Landroid/content/Context;LaGM;LUx;Lamx;LahK;LCo;LQr;)V

    return-void
.end method

.method static synthetic a(LafU;)Lamw;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LafU;->a:Lamw;

    return-object v0
.end method

.method private a(Landroid/content/Context;IIJ)Landroid/app/Notification;
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 468
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 470
    sget v1, Lxb;->ic_offline_notification:I

    .line 473
    if-nez p2, :cond_0

    .line 474
    sget v2, Lxi;->pin_notification_sync_fail:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 487
    :goto_0
    invoke-static {p4, p5}, LafU;->a(J)Ljava/lang/String;

    move-result-object v2

    .line 489
    invoke-direct {p0, p1, v1, v0, v2}, LafU;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 475
    :cond_0
    if-nez p3, :cond_1

    .line 476
    sget v2, Lxg;->pin_notification_sync_completed_all:I

    new-array v3, v8, [Ljava/lang/Object;

    .line 477
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 476
    invoke-virtual {v0, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 479
    :cond_1
    add-int v2, p2, p3

    .line 480
    sget v3, Lxg;->pin_notification_sync_queued_files:I

    new-array v4, v8, [Ljava/lang/Object;

    .line 481
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 480
    invoke-virtual {v0, v3, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 482
    sget v4, Lxg;->pin_notification_sync_completed:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 483
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v3, v5, v8

    .line 482
    invoke-virtual {v0, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;IJJLafY;)Landroid/app/Notification;
    .locals 11

    .prologue
    .line 441
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p7

    iget v3, v0, LafY;->d:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 442
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 441
    invoke-virtual {v2, v3, p2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 444
    move-object/from16 v0, p7

    iget v4, v0, LafY;->c:I

    move-object v2, p0

    move-object v3, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v2 .. v9}, LafU;->a(Landroid/content/Context;ILjava/lang/String;JJ)Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method private a(Landroid/content/Context;ILafY;)Landroid/app/Notification;
    .locals 6

    .prologue
    .line 398
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p3, LafY;->e:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 399
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 398
    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 400
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->transfer_notification_waiting_content:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 403
    invoke-static {}, LakQ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v5, p3, LafY;->a:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, LafU;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v0

    .line 407
    :goto_0
    return-object v0

    :cond_0
    sget v0, Lxb;->ic_notification_paused:I

    invoke-direct {p0, p1, v0, v3, v4}, LafU;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;JJ)Landroid/app/Notification;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 450
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 452
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 454
    invoke-static {v0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v2, Lxb;->notification_icon:I

    .line 455
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 456
    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 457
    invoke-static {p4, p5}, LafU;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/16 v2, 0x64

    const/4 v3, 0x0

    .line 458
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 459
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 460
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 461
    invoke-virtual {v0, p6, p7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 463
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/Notification;
    .locals 4

    .prologue
    .line 518
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 520
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 521
    sget v2, Lxb;->notification_icon:I

    .line 522
    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 523
    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 524
    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 525
    invoke-virtual {v0, p4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v2, 0x1

    .line 526
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 527
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 529
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/Notification;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 415
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxg;->transfer_notification_waiting_ticker:I

    invoke-virtual {v0, v1, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 417
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxg;->transfer_notification_waiting_resume:I

    invoke-virtual {v1, v2, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    .line 420
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 421
    invoke-virtual {v2, p5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 422
    invoke-static {p1, v4, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 424
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lxb;->ic_notification_paused:I

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 427
    new-instance v4, Landroid/app/Notification$Builder;

    invoke-direct {v4, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 428
    invoke-virtual {v4, v3}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v3

    sget v4, Lxb;->notification_icon:I

    .line 429
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 430
    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 431
    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 432
    invoke-virtual {v0, p4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 433
    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 434
    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v3, Lxb;->ic_retry:I

    .line 435
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 436
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;LaFO;LafY;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 537
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    iget-object v0, p3, LafY;->a:LCn;

    .line 539
    iget-object v1, p0, LafU;->a:LCo;

    invoke-interface {v1, v0}, LCo;->a(LCn;)LCl;

    move-result-object v0

    .line 541
    invoke-static {p1, p2, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;LCl;)Landroid/content/Intent;

    move-result-object v1

    .line 542
    const/high16 v2, 0x34000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 547
    iget-object v2, p0, LafU;->a:LCo;

    .line 548
    invoke-interface {v2}, LCo;->a()LbmF;

    move-result-object v2

    invoke-virtual {v2, v0}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/high16 v2, 0x8000000

    .line 547
    invoke-static {p1, v0, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a()LbmL;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmL",
            "<",
            "LahS;",
            "LafZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    const-class v0, LahS;

    invoke-static {v0}, LboS;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v1

    .line 231
    invoke-static {}, LahS;->values()[LahS;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 232
    new-instance v5, LafZ;

    invoke-direct {v5, v4}, LafZ;-><init>(LahS;)V

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 235
    :cond_0
    invoke-static {v1}, LboS;->a(Ljava/util/Map;)LbmL;

    move-result-object v0

    return-object v0
.end method

.method private static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 557
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    invoke-static {p0, p1}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 273
    invoke-direct {p0}, LafU;->b()V

    .line 274
    iget-object v0, p0, LafU;->a:Landroid/content/Context;

    sget-object v1, LafY;->a:LafY;

    invoke-direct {p0, v0, v1}, LafU;->a(Landroid/content/Context;LafY;)Z

    move-result v0

    .line 275
    iget-object v1, p0, LafU;->a:LCo;

    sget-object v2, LCn;->h:LCn;

    invoke-interface {v1, v2}, LCo;->a(LCn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, LafU;->a:Landroid/content/Context;

    sget-object v2, LafY;->b:LafY;

    .line 277
    invoke-direct {p0, v1, v2}, LafU;->a(Landroid/content/Context;LafY;)Z

    move-result v1

    .line 278
    or-int/2addr v0, v1

    .line 281
    :cond_0
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v1

    .line 282
    iget-object v2, p0, LafU;->a:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 284
    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, LafU;->a:LQr;

    const-string v2, "contentSyncNotificationRefreshPeriodSeconds"

    const/16 v3, 0x1e

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    .line 289
    iget-object v0, p0, LafU;->a:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 291
    :cond_1
    return-void
.end method

.method static synthetic a(LafU;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, LafU;->a()V

    return-void
.end method

.method private a(Landroid/content/Context;Lbpi;LafY;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbpi",
            "<",
            "LafT;",
            ">;",
            "LafY;",
            ")V"
        }
    .end annotation

    .prologue
    .line 385
    sget-object v0, LafT;->k:LafT;

    invoke-interface {p2, v0}, Lbpi;->a(Ljava/lang/Object;)I

    move-result v0

    .line 386
    if-lez v0, :cond_0

    .line 387
    invoke-direct {p0, p1, v0, p3}, LafU;->a(Landroid/content/Context;ILafY;)Landroid/app/Notification;

    move-result-object v0

    .line 389
    iget-object v1, p0, LafU;->a:LaFO;

    invoke-direct {p0, p1, v1, p3}, LafU;->a(Landroid/content/Context;LaFO;LafY;)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 390
    iget-object v1, p0, LafU;->a:LUx;

    iget v2, p3, LafY;->b:I

    invoke-interface {v1, v2, v0}, LUx;->a(ILandroid/app/Notification;)V

    .line 394
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v0, p0, LafU;->a:LUx;

    iget v1, p3, LafY;->b:I

    invoke-interface {v0, v1}, LUx;->a(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;LafY;)Z
    .locals 10

    .prologue
    .line 323
    iget-object v0, p2, LafY;->a:LahS;

    .line 324
    iget-object v1, p0, LafU;->a:LbmL;

    invoke-virtual {v1, v0}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LafZ;

    invoke-virtual {v0}, LafZ;->a()Lahl;

    move-result-object v0

    .line 326
    iget v9, v0, Lahl;->a:I

    .line 327
    iget v2, v0, Lahl;->b:I

    .line 328
    iget v3, v0, Lahl;->c:I

    .line 329
    iget-object v1, v0, Lahl;->a:LbmV;

    invoke-virtual {v1}, LbmV;->size()I

    move-result v1

    .line 330
    iget-wide v4, v0, Lahl;->a:J

    .line 332
    iget-object v6, v0, Lahl;->a:LbmV;

    invoke-direct {p0, p1, v6, p2}, LafU;->a(Landroid/content/Context;Lbpi;LafY;)V

    .line 334
    add-int v6, v9, v2

    add-int/2addr v6, v3

    if-nez v6, :cond_0

    .line 338
    iget-object v0, p0, LafU;->a:LUx;

    iget v1, p2, LafY;->a:I

    invoke-interface {v0, v1}, LUx;->a(I)V

    .line 378
    :goto_0
    if-lez v9, :cond_5

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 341
    :cond_0
    iget-object v6, p0, LafU;->a:LaFO;

    invoke-static {v6, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    add-int v0, v2, v3

    .line 344
    sget-object v6, LafY;->b:LafY;

    invoke-virtual {p2, v6}, LafY;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 345
    add-int/2addr v0, v1

    move v1, v0

    .line 349
    :goto_2
    iget-object v0, p0, LafU;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 350
    if-nez v9, :cond_3

    .line 351
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 352
    sget-object v0, LafY;->b:LafY;

    invoke-virtual {p2, v0}, LafY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    move-object v1, p1

    .line 353
    invoke-direct/range {v0 .. v5}, LafU;->b(Landroid/content/Context;IIJ)Landroid/app/Notification;

    move-result-object v0

    move-object v1, v0

    move-object v0, v6

    .line 367
    :goto_3
    iget-object v2, p0, LafU;->a:Ljava/util/Map;

    invoke-interface {v2, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-object v0, p0, LafU;->a:LaFO;

    invoke-direct {p0, p1, v0, p2}, LafU;->a(Landroid/content/Context;LaFO;LafY;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 371
    if-nez v9, :cond_1

    .line 372
    iget-object v0, p0, LafU;->a:LUx;

    iget v2, p2, LafY;->a:I

    invoke-interface {v0, v2}, LUx;->a(I)V

    .line 375
    :cond_1
    iget-object v0, p0, LafU;->a:LUx;

    iget v2, p2, LafY;->a:I

    invoke-interface {v0, v2, v1}, LUx;->a(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    .line 356
    invoke-direct/range {v0 .. v5}, LafU;->a(Landroid/content/Context;IIJ)Landroid/app/Notification;

    move-result-object v0

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    .line 361
    :cond_3
    if-nez v0, :cond_4

    .line 362
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 364
    :cond_4
    add-int v3, v9, v1

    .line 365
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v1, p0

    move-object v2, p1

    move-object v8, p2

    .line 364
    invoke-direct/range {v1 .. v8}, LafU;->a(Landroid/content/Context;IJJLafY;)Landroid/app/Notification;

    move-result-object v1

    goto :goto_3

    .line 378
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    move v1, v0

    goto :goto_2
.end method

.method private b(Landroid/content/Context;IIJ)Landroid/app/Notification;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 494
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 496
    if-nez p3, :cond_0

    sget v0, Lxb;->ic_upload_notification:I

    move v1, v0

    .line 500
    :goto_0
    if-nez p3, :cond_1

    .line 501
    sget v0, Lxg;->upload_notification_sync_completed_successfully:I

    new-array v3, v7, [Ljava/lang/Object;

    .line 503
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 501
    invoke-virtual {v2, v0, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 511
    :goto_1
    invoke-static {p4, p5}, LafU;->a(J)Ljava/lang/String;

    move-result-object v2

    .line 513
    invoke-direct {p0, p1, v1, v0, v2}, LafU;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 496
    :cond_0
    sget v0, Lxb;->ic_upload_notification_error:I

    move v1, v0

    goto :goto_0

    .line 505
    :cond_1
    add-int v0, p2, p3

    .line 506
    sget v3, Lxg;->upload_notification_sync_completed_with_failures:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 507
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 508
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 506
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 294
    invoke-static {}, LahS;->values()[LahS;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 295
    iget-object v5, p0, LafU;->a:LbmL;

    invoke-virtual {v5, v0}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LafZ;

    invoke-virtual {v0}, LafZ;->a()V

    .line 294
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 298
    :cond_0
    iget-object v0, p0, LafU;->a:LaGM;

    invoke-interface {v0}, LaGM;->a()LbmY;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 300
    iget-object v3, p0, LafU;->a:LahK;

    invoke-interface {v3, v0}, LahK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v3

    .line 301
    if-eqz v3, :cond_1

    .line 305
    invoke-interface {v3}, LahF;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    invoke-interface {v3}, LahF;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 307
    iget-object v0, p0, LafU;->a:LbmL;

    sget-object v4, LahS;->b:LahS;

    invoke-virtual {v0, v4}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LafZ;

    invoke-virtual {v0, v3}, LafZ;->a(LahF;)V

    .line 310
    :cond_2
    invoke-interface {v3}, LahF;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, LafU;->a:LbmL;

    sget-object v4, LahS;->a:LahS;

    invoke-virtual {v0, v4}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LafZ;

    invoke-virtual {v0, v3}, LafZ;->a(LahF;)V

    goto :goto_1

    .line 316
    :cond_3
    const-string v0, "ContentSyncOverallStatusNotifier"

    const-string v1, "Statistics overall: %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LafU;->a:LbmL;

    aput-object v4, v3, v2

    invoke-static {v0, v1, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 317
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LahR;)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, LafU;->a:LaFO;

    .line 269
    iget-object v0, p0, LafU;->a:Lamw;

    invoke-interface {v0}, Lamw;->a()V

    .line 270
    return-void
.end method
