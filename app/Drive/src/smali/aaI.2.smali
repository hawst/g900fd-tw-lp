.class public final LaaI;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaaP;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaaD;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaaL;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaaA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 42
    iput-object p1, p0, LaaI;->a:LbrA;

    .line 43
    const-class v0, LaaP;

    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaaI;->a:Lbsk;

    .line 46
    const-class v0, LaaD;

    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaaI;->b:Lbsk;

    .line 49
    const-class v0, LaaL;

    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaaI;->c:Lbsk;

    .line 52
    const-class v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaaI;->d:Lbsk;

    .line 55
    const-class v0, LaaA;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaaI;->e:Lbsk;

    .line 58
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 255
    sparse-switch p1, :sswitch_data_0

    .line 285
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :sswitch_0
    new-instance v0, LaaD;

    invoke-direct {v0}, LaaD;-><init>()V

    .line 259
    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaaI;

    .line 260
    invoke-virtual {v1, v0}, LaaI;->a(LaaD;)V

    .line 283
    :goto_0
    return-object v0

    .line 263
    :sswitch_1
    new-instance v2, LaaL;

    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:La;

    iget-object v0, v0, La;->a:Lbsk;

    .line 266
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->a:Lbsk;

    .line 264
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->o:Lbsk;

    .line 272
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaaI;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->o:Lbsk;

    .line 270
    invoke-static {v1, v3}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lamj;

    invoke-direct {v2, v0, v1}, LaaL;-><init>(Landroid/app/Activity;Lamj;)V

    move-object v0, v2

    .line 277
    goto :goto_0

    .line 279
    :sswitch_2
    new-instance v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-direct {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;-><init>()V

    .line 281
    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaaI;

    .line 282
    invoke-virtual {v1, v0}, LaaI;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V

    goto :goto_0

    .line 255
    nop

    :sswitch_data_0
    .sparse-switch
        0x226 -> :sswitch_0
        0x22c -> :sswitch_1
        0x22e -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 324
    packed-switch p2, :pswitch_data_0

    .line 339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :pswitch_0
    check-cast p1, LaaB;

    .line 328
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaaI;

    iget-object v0, v0, LaaI;->b:Lbsk;

    .line 331
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaD;

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->bl:Lbsk;

    .line 335
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LbiP;

    .line 328
    invoke-virtual {p1, v0, v1}, LaaB;->provideBlobFileUploader(LaaD;LbiP;)LaaA;

    move-result-object v0

    return-object v0

    .line 324
    nop

    :pswitch_data_0
    .packed-switch 0x229
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 220
    const-class v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x4b

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LaaI;->a(LbuP;LbuB;)V

    .line 223
    const-class v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x4c

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LaaI;->a(LbuP;LbuB;)V

    .line 226
    const-class v0, LaaD;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x4a

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LaaI;->a(LbuP;LbuB;)V

    .line 229
    const-class v0, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x4d

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LaaI;->a(LbuP;LbuB;)V

    .line 232
    const-class v0, LaaP;

    iget-object v1, p0, LaaI;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaaI;->a(Ljava/lang/Class;Lbsk;)V

    .line 233
    const-class v0, LaaD;

    iget-object v1, p0, LaaI;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaaI;->a(Ljava/lang/Class;Lbsk;)V

    .line 234
    const-class v0, LaaL;

    iget-object v1, p0, LaaI;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaaI;->a(Ljava/lang/Class;Lbsk;)V

    .line 235
    const-class v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaI;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaaI;->a(Ljava/lang/Class;Lbsk;)V

    .line 236
    const-class v0, LaaA;

    iget-object v1, p0, LaaI;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LaaI;->a(Ljava/lang/Class;Lbsk;)V

    .line 237
    iget-object v0, p0, LaaI;->a:Lbsk;

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaaI;

    iget-object v1, v1, LaaI;->c:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 239
    iget-object v0, p0, LaaI;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x226

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 241
    iget-object v0, p0, LaaI;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x22c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 243
    iget-object v0, p0, LaaI;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x22e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 245
    iget-object v0, p0, LaaI;->e:Lbsk;

    const-class v1, LaaB;

    const/16 v2, 0x229

    invoke-virtual {p0, v1, v2}, LaaI;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 247
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 292
    packed-switch p1, :pswitch_data_0

    .line 318
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;

    .line 296
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaaI;

    .line 297
    invoke-virtual {v0, p2}, LaaI;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)V

    .line 320
    :goto_0
    return-void

    .line 300
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    .line 302
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaaI;

    .line 303
    invoke-virtual {v0, p2}, LaaI;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V

    goto :goto_0

    .line 306
    :pswitch_2
    check-cast p2, LaaD;

    .line 308
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaaI;

    .line 309
    invoke-virtual {v0, p2}, LaaI;->a(LaaD;)V

    goto :goto_0

    .line 312
    :pswitch_3
    check-cast p2, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;

    .line 314
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaaI;

    .line 315
    invoke-virtual {v0, p2}, LaaI;->a(Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;)V

    goto :goto_0

    .line 292
    :pswitch_data_0
    .packed-switch 0x4a
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public a(LaaD;)V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->h:Lbsk;

    .line 198
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LahE;

    iget-object v1, v1, LahE;->h:Lbsk;

    .line 196
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahB;

    iput-object v0, p1, LaaD;->a:LahB;

    .line 202
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->m:Lbsk;

    .line 205
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->m:Lbsk;

    .line 203
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    iput-object v0, p1, LaaD;->a:Ladi;

    .line 209
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;)V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 214
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 215
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 65
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 66
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 71
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 72
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 75
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 73
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaGR;

    .line 79
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 82
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 80
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LtK;

    .line 86
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaaI;

    iget-object v0, v0, LaaI;->a:Lbsk;

    .line 89
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaaI;

    iget-object v1, v1, LaaI;->a:Lbsk;

    .line 87
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaP;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaP;

    .line 93
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMY;

    iget-object v0, v0, LMY;->b:Lbsk;

    .line 96
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LMY;

    iget-object v1, v1, LMY;->b:Lbsk;

    .line 94
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNc;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LNc;

    .line 100
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->q:Lbsk;

    .line 103
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->q:Lbsk;

    .line 101
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalY;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LalY;

    .line 107
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUw;

    iget-object v0, v0, LUw;->b:Lbsk;

    .line 110
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUw;

    iget-object v1, v1, LUw;->b:Lbsk;

    .line 108
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUx;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LUx;

    .line 114
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 117
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 115
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lald;

    .line 121
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->N:Lbsk;

    .line 124
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->N:Lbsk;

    .line 122
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCo;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LCo;

    .line 128
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaaI;

    iget-object v0, v0, LaaI;->e:Lbsk;

    .line 131
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaaI;

    iget-object v1, v1, LaaI;->e:Lbsk;

    .line 129
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaA;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaA;

    .line 135
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 138
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 136
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSF;

    .line 142
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lqa;

    iget-object v0, v0, Lqa;->b:Lbsk;

    .line 145
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lqa;

    iget-object v1, v1, Lqa;->b:Lbsk;

    .line 143
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpW;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LpW;

    .line 149
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->O:Lbsk;

    .line 152
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->O:Lbsk;

    .line 150
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajO;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LajO;

    .line 156
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->V:Lbsk;

    .line 159
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->V:Lbsk;

    .line 157
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/BitmapUtilities;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

    .line 163
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 166
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 164
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LQr;

    .line 170
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->j:Lbsk;

    .line 173
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->j:Lbsk;

    .line 171
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladu;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ladu;

    .line 177
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHX;

    iget-object v0, v0, LaHX;->n:Lbsk;

    .line 180
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHX;

    iget-object v1, v1, LaHX;->n:Lbsk;

    .line 178
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIa;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaIa;

    .line 184
    iget-object v0, p0, LaaI;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 187
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaaI;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 185
    invoke-static {v0, v1}, LaaI;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaGM;

    .line 191
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 251
    return-void
.end method
