.class public final enum Lajv;
.super Ljava/lang/Enum;
.source "InjectorRegistry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lajv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lajv;

.field private static final synthetic a:[Lajv;


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/content/Context;",
            "Lbuu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    new-instance v0, Lajv;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lajv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lajv;->a:Lajv;

    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [Lajv;

    sget-object v1, Lajv;->a:Lajv;

    aput-object v1, v0, v2

    sput-object v0, Lajv;->a:[Lajv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lajv;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lajv;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lajv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lajv;

    return-object v0
.end method

.method public static values()[Lajv;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lajv;->a:[Lajv;

    invoke-virtual {v0}, [Lajv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lajv;

    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Context;)Lbuu;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lajv;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuu;

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/util/List;)Lbuu;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LbuC;",
            ">;)",
            "Lbuu;"
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x1

    new-array v0, v0, [LbuC;

    const/4 v1, 0x0

    new-instance v2, Lajc;

    invoke-direct {v2, p2}, Lajc;-><init>(Ljava/lang/Iterable;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Lbrw;->a([LbuC;)Lbuu;

    move-result-object v0

    .line 69
    invoke-static {p1, v0}, LaiZ;->a(Landroid/content/Context;Lbuu;)LaiY;

    move-result-object v0

    .line 70
    invoke-virtual {p0, p1, v0}, Lajv;->a(Landroid/content/Context;Lbuu;)V

    .line 71
    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lajv;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method

.method public a(Landroid/content/Context;Lbuu;)V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lajv;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuu;

    .line 34
    if-eqz v0, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already associated with an injector"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    return-void
.end method
