.class public LOy;
.super Ljava/lang/Object;
.source "PhotoBackupHelpCardFlow.java"

# interfaces
.implements LJs;


# annotations
.annotation runtime LaiC;
.end annotation


# static fields
.field private static final a:LCl;


# instance fields
.field private a:LJt;

.field private final a:LOC;

.field private final a:LSu;

.field private final a:LaFO;

.field private final a:Landroid/content/Context;

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, LCe;->r:LCe;

    sput-object v0, LOy;->a:LCl;

    return-void
.end method

.method public constructor <init>(LbiP;Landroid/content/Context;LaFO;LtK;LOC;)V
    .locals 1
    .param p2    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "LSu;",
            ">;",
            "Landroid/content/Context;",
            "LaFO;",
            "LtK;",
            "LOC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-virtual {p1}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSu;

    :goto_0
    iput-object v0, p0, LOy;->a:LSu;

    .line 83
    iput-object p2, p0, LOy;->a:Landroid/content/Context;

    .line 84
    iput-object p3, p0, LOy;->a:LaFO;

    .line 85
    iput-object p4, p0, LOy;->a:LtK;

    .line 86
    iput-object p5, p0, LOy;->a:LOC;

    .line 87
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()LOz;
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, LOy;->a:Landroid/content/Context;

    const-string v1, "com.google.android.apps.docs.doclist.helpcard.PhotoBackupHelpCardFlow"

    const/4 v2, 0x0

    .line 128
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 129
    const-string v1, "com.google.android.apps.docs.doclist.helpcard.State"

    sget-object v2, LOz;->a:LOz;

    .line 130
    invoke-virtual {v2}, LOz;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 131
    invoke-static {v0}, LOz;->a(I)LOz;

    move-result-object v0

    return-object v0
.end method

.method private a(LOz;)V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, LOy;->a:Landroid/content/Context;

    const-string v1, "com.google.android.apps.docs.doclist.helpcard.PhotoBackupHelpCardFlow"

    const/4 v2, 0x0

    .line 122
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.apps.docs.doclist.helpcard.State"

    invoke-virtual {p1}, LOz;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 124
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, LOy;->a:LSu;

    if-eqz v0, :cond_0

    iget-object v0, p0, LOy;->a:LtK;

    sget-object v1, LOE;->a:Lrg;

    iget-object v2, p0, LOy;->a:LaFO;

    .line 113
    invoke-interface {v0, v1, v2}, LtK;->a(Lrg;LaFO;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LCl;)LJw;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 92
    invoke-direct {p0}, LOy;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LOy;->a:LCl;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-object v0

    .line 96
    :cond_1
    invoke-direct {p0}, LOy;->a()LOz;

    move-result-object v1

    .line 97
    sget-object v2, LOz;->a:LOz;

    invoke-virtual {v1, v2}, LOz;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    iget-object v0, p0, LOy;->a:LSu;

    iget-object v1, p0, LOy;->a:LaFO;

    .line 99
    invoke-virtual {v1}, LaFO;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LSu;->a(Ljava/lang/String;)LbsU;

    move-result-object v0

    .line 100
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lalv;->a(Ljava/util/concurrent/Future;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 101
    if-eqz v0, :cond_2

    .line 102
    sget-object v0, LOz;->b:LOz;

    invoke-direct {p0, v0}, LOy;->a(LOz;)V

    .line 103
    invoke-virtual {p0, p1}, LOy;->a(LCl;)LJw;

    move-result-object v0

    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, LOy;->a:LOC;

    invoke-virtual {v0, p0}, LOC;->a(LOy;)LOA;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 117
    sget-object v0, LOz;->b:LOz;

    invoke-direct {p0, v0}, LOy;->a(LOz;)V

    .line 118
    return-void
.end method

.method public a(LJt;)V
    .locals 1

    .prologue
    .line 136
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJt;

    iput-object v0, p0, LOy;->a:LJt;

    .line 137
    return-void
.end method
