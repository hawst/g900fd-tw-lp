.class abstract LajD;
.super Ljava/lang/Object;
.source "ScopedWrapper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lajh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajh",
            "<TS;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lajh;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lajh",
            "<TS;>;TS;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajh;

    iput-object v0, p0, LajD;->a:Lajh;

    .line 20
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LajD;->a:Ljava/lang/Object;

    .line 21
    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, LajD;->a:Lajh;

    invoke-interface {v0}, Lajh;->b()Ljava/lang/Object;

    move-result-object v0

    .line 30
    iget-object v1, p0, LajD;->a:Lajh;

    iget-object v2, p0, LajD;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lajh;->a(Ljava/lang/Object;)V

    .line 31
    return-object v0
.end method

.method protected final b(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, LajD;->a:Lajh;

    invoke-interface {v0}, Lajh;->b()Ljava/lang/Object;

    move-result-object v0

    .line 41
    if-eq v0, p1, :cond_0

    .line 42
    if-eqz p1, :cond_1

    .line 43
    iget-object v0, p0, LajD;->a:Lajh;

    invoke-interface {v0, p1}, Lajh;->a(Ljava/lang/Object;)V

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    iget-object v1, p0, LajD;->a:Lajh;

    invoke-interface {v1, v0}, Lajh;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method
