.class public final LawJ;
.super Ljava/lang/Object;
.source "HighlightPaint.java"


# static fields
.field public static final a:Landroid/graphics/Paint;

.field public static final b:Landroid/graphics/Paint;

.field public static final c:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 11
    const/16 v0, 0xa0

    const/16 v1, 0xd7

    invoke-static {v0, v1, v2}, LawJ;->a(III)Landroid/graphics/Paint;

    move-result-object v0

    sput-object v0, LawJ;->a:Landroid/graphics/Paint;

    .line 14
    const/4 v0, 0x0

    invoke-static {v2, v2, v0}, LawJ;->a(III)Landroid/graphics/Paint;

    move-result-object v0

    sput-object v0, LawJ;->b:Landroid/graphics/Paint;

    .line 20
    const/16 v0, 0x96

    const/16 v1, 0x32

    invoke-static {v2, v0, v1}, LawJ;->a(III)Landroid/graphics/Paint;

    move-result-object v0

    sput-object v0, LawJ;->c:Landroid/graphics/Paint;

    return-void
.end method

.method private static a(III)Landroid/graphics/Paint;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 29
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 30
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 31
    const/16 v1, 0xff

    invoke-virtual {v0, v1, p0, p1, p2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 32
    return-object v0
.end method
