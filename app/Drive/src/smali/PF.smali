.class public LPF;
.super Lanv;
.source "AvatarCachingFetcher.java"

# interfaces
.implements LPH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanv",
        "<",
        "Ljava/net/URL;",
        "Ljava/net/URL;",
        "Landroid/graphics/Bitmap;",
        ">;",
        "LPH;"
    }
.end annotation


# instance fields
.field private final a:LbjF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjF",
            "<",
            "Ljava/net/URL;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LPE;LQr;)V
    .locals 5

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lanv;-><init>(Laoo;)V

    .line 34
    const-string v0, "recentActivityMaximumNumberAvatars"

    const/16 v1, 0x64

    .line 35
    invoke-interface {p2, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 36
    const-string v2, "recentActivityAvatarExpiryMinutes"

    const/16 v3, 0xa

    invoke-interface {p2, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v2

    int-to-long v2, v2

    .line 39
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, LbjG;->a(J)LbjG;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    .line 40
    invoke-virtual {v0, v2, v3, v1}, LbjG;->a(JLjava/util/concurrent/TimeUnit;)LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->a()LbjF;

    move-result-object v0

    iput-object v0, p0, LPF;->a:LbjF;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Ljava/net/URL;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LPF;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;I)LbmF;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/net/URL;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, p3}, LPF;->a(Ljava/net/URL;Landroid/graphics/Bitmap;I)LbmF;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/net/URL;Landroid/graphics/Bitmap;I)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            "Landroid/graphics/Bitmap;",
            "I)",
            "LbmF",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, LPF;->a:LbjF;

    invoke-interface {v0, p1, p2}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 70
    new-instance v1, LbmH;

    invoke-direct {v1}, LbmH;-><init>()V

    .line 71
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 72
    invoke-virtual {v1, p2}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_0
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/net/URL;)LbsU;
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1}, Lanv;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/net/URL;

    invoke-virtual {p0, p1}, LPF;->a(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/net/URL;)Ljava/net/URL;
    .locals 0

    .prologue
    .line 50
    return-object p1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/net/URL;

    invoke-virtual {p0, p1}, LPF;->a(Ljava/net/URL;)Z

    move-result v0

    return v0
.end method

.method protected a(Ljava/net/URL;)Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LPF;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/net/URL;

    invoke-virtual {p0, p1}, LPF;->a(Ljava/net/URL;)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method
