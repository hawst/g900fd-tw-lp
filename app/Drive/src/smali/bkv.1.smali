.class abstract enum Lbkv;
.super Ljava/lang/Enum;
.source "LocalCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbkv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbkv;

.field private static final synthetic a:[Lbkv;

.field public static final enum b:Lbkv;

.field public static final enum c:Lbkv;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 376
    new-instance v0, Lbkw;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, Lbkw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbkv;->a:Lbkv;

    .line 391
    new-instance v0, Lbkx;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, Lbkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbkv;->b:Lbkv;

    .line 407
    new-instance v0, Lbky;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, Lbky;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbkv;->c:Lbkv;

    .line 370
    const/4 v0, 0x3

    new-array v0, v0, [Lbkv;

    sget-object v1, Lbkv;->a:Lbkv;

    aput-object v1, v0, v2

    sget-object v1, Lbkv;->b:Lbkv;

    aput-object v1, v0, v3

    sget-object v1, Lbkv;->c:Lbkv;

    aput-object v1, v0, v4

    sput-object v0, Lbkv;->a:[Lbkv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbjR;)V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0, p1, p2}, Lbkv;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbkv;
    .locals 1

    .prologue
    .line 370
    const-class v0, Lbkv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbkv;

    return-object v0
.end method

.method public static values()[Lbkv;
    .locals 1

    .prologue
    .line 370
    sget-object v0, Lbkv;->a:[Lbkv;

    invoke-virtual {v0}, [Lbkv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbkv;

    return-object v0
.end method


# virtual methods
.method abstract a()LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method abstract a(Lbks;Lbkr;Ljava/lang/Object;I)LbkF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbks",
            "<TK;TV;>;",
            "Lbkr",
            "<TK;TV;>;TV;I)",
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation
.end method
