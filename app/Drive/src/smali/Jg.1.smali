.class public LJg;
.super Ljava/lang/Object;
.source "CentralHelpCardsSupplierImpl.java"

# interfaces
.implements LJf;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:LCl;

.field private a:LJw;

.field private final a:LJx;

.field a:LaKM;

.field a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "Lbjv",
            "<",
            "LJw;",
            ">;>;"
        }
    .end annotation
.end field

.field a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private b:Z

.field private c:Z

.field private final d:Z


# direct methods
.method public constructor <init>(Laja;LtK;LaKM;Lbjv;LbiP;)V
    .locals 1
    .param p4    # Lbjv;
        .annotation runtime Lbxv;
            a = "docListActivityVisitCountSupplier"
        .end annotation
    .end param
    .param p5    # LbiP;
        .annotation runtime Lbxv;
            a = "appHelpCardSupplier"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LtK;",
            "LaKM;",
            "Lbjv",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LbiP",
            "<",
            "Lbjv",
            "<",
            "LJw;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, LJg;->c:Z

    .line 67
    new-instance v0, LJh;

    invoke-direct {v0, p0}, LJh;-><init>(LJg;)V

    iput-object v0, p0, LJg;->a:LJx;

    .line 86
    sget-object v0, Lry;->N:Lry;

    invoke-interface {p2, v0}, LtK;->a(LtJ;)Z

    move-result v0

    iput-boolean v0, p0, LJg;->a:Z

    .line 87
    iput-object p1, p0, LJg;->a:Lbxw;

    .line 88
    iput-object p3, p0, LJg;->a:LaKM;

    .line 89
    iput-object p4, p0, LJg;->a:Lbjv;

    .line 90
    sget-object v0, Lry;->O:Lry;

    invoke-interface {p2, v0}, LtK;->a(LtJ;)Z

    move-result v0

    iput-boolean v0, p0, LJg;->d:Z

    .line 91
    iput-object p5, p0, LJg;->a:LbiP;

    .line 92
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 160
    const-string v0, "HelpCard"

    .line 161
    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 162
    const-string v1, "docListActivityVisitCount"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static synthetic a(LJg;LJw;)LJw;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, LJg;->a:LJw;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 95
    iput-boolean v2, p0, LJg;->c:Z

    .line 97
    iget-boolean v0, p0, LJg;->a:Z

    if-nez v0, :cond_0

    .line 98
    const-string v0, "CentralHelpCardsSupplierImpl"

    const-string v1, "Help Card feature is disabled"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iput-boolean v2, p0, LJg;->b:Z

    .line 102
    :cond_0
    iget-object v0, p0, LJg;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    const-string v0, "CentralHelpCardsSupplierImpl"

    const-string v1, "get returns null as appHelpCardsSupplier is null"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iput-boolean v2, p0, LJg;->b:Z

    .line 106
    :cond_1
    return-void
.end method

.method static synthetic a(LJg;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LJg;->b()V

    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, LJg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LJg;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, LJg;->d:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 140
    iget-object v0, p0, LJg;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v2

    .line 141
    iget-object v0, p0, LJg;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string v1, "HelpCard"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 143
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastDismissMilli"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "docListActivityVisitCount"

    .line 144
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 145
    return-void
.end method

.method private b()Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, LJg;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string v2, "HelpCard"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 174
    iget-object v2, p0, LJg;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    .line 175
    const-string v4, "lastDismissMilli"

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 176
    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    .line 177
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "lastDismissMilli"

    invoke-interface {v0, v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 182
    :goto_0
    return v1

    .line 181
    :cond_0
    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 182
    goto :goto_0

    :cond_1
    move v0, v1

    .line 181
    goto :goto_1
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, LJg;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LJg;->a(Landroid/content/Context;)I

    move-result v1

    .line 187
    iget-object v0, p0, LJg;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    .line 188
    :goto_0
    return v0

    .line 187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 192
    sget-object v0, LCe;->q:LCe;

    iget-object v1, p0, LJg;->a:LCl;

    invoke-virtual {v0, v1}, LCe;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, LJg;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;

    return v0
.end method


# virtual methods
.method public a()LJw;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-boolean v0, p0, LJg;->c:Z

    if-nez v0, :cond_0

    .line 111
    invoke-direct {p0}, LJg;->a()V

    .line 114
    :cond_0
    iget-boolean v0, p0, LJg;->b:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, LJg;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, LJg;->d()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 136
    :goto_0
    return-object v0

    .line 118
    :cond_2
    iget-object v0, p0, LJg;->a:LJw;

    if-eqz v0, :cond_3

    .line 119
    iget-object v0, p0, LJg;->a:LJw;

    goto :goto_0

    .line 122
    :cond_3
    invoke-direct {p0}, LJg;->a()Z

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v1

    .line 123
    goto :goto_0

    .line 126
    :cond_4
    iget-object v0, p0, LJg;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJw;

    .line 127
    if-nez v0, :cond_5

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, LJg;->b:Z

    move-object v0, v1

    .line 129
    goto :goto_0

    .line 132
    :cond_5
    iput-object v0, p0, LJg;->a:LJw;

    .line 133
    iget-object v1, p0, LJg;->a:LJw;

    iget-object v2, p0, LJg;->a:LJx;

    invoke-interface {v1, v2}, LJw;->a(LJx;)V

    .line 134
    iget-object v2, p0, LJg;->a:LJw;

    iget-object v1, p0, LJg;->a:Lbxw;

    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-interface {v2, v1}, LJw;->a(Landroid/content/Context;)I

    goto :goto_0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, LJg;->a()LJw;

    move-result-object v0

    return-object v0
.end method

.method public a(LCl;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, LJg;->a:LCl;

    .line 198
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    const-string v0, "HelpCard"

    .line 150
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 151
    const-string v1, "docListActivityVisitCount"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 152
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "docListActivityVisitCount"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 154
    iget-object v0, p0, LJg;->a:LJw;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, LJg;->a:LJw;

    invoke-interface {v0, p1}, LJw;->a(Landroid/content/Context;)I

    .line 157
    :cond_0
    return-void
.end method
