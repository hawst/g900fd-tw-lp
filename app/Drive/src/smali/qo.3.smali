.class public Lqo;
.super Ljava/lang/Object;
.source "AclType.java"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private final a:Ljava/lang/String;

.field private final a:Lqt;

.field private final a:Lqx;

.field private final a:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Lqx;Lqt;Z)V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    iput-object p1, p0, Lqo;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 451
    iput-object p2, p0, Lqo;->a:Ljava/lang/String;

    .line 452
    iput-object p3, p0, Lqo;->a:Lqx;

    .line 453
    iput-object p4, p0, Lqo;->a:Lqt;

    .line 454
    iput-boolean p5, p0, Lqo;->a:Z

    .line 455
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Lqx;Lqt;ZLqp;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct/range {p0 .. p5}, Lqo;-><init>(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Lqx;Lqt;Z)V

    return-void
.end method

.method static synthetic a(Lqo;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lqo;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method static synthetic a(Lqo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lqo;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lqo;)Lqx;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lqo;->a:Lqx;

    return-object v0
.end method

.method static synthetic a(Lqo;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lqo;->a:Z

    return v0
.end method


# virtual methods
.method public a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lqo;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lqo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481
    iget-object v0, p0, Lqo;->a:Lqt;

    invoke-virtual {v0}, Lqt;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqt;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lqo;->a:Lqt;

    return-object v0
.end method

.method public a()Lqu;
    .locals 3

    .prologue
    .line 493
    iget-object v0, p0, Lqo;->a:Lqt;

    iget-object v1, p0, Lqo;->a:Lqx;

    iget-boolean v2, p0, Lqo;->a:Z

    invoke-static {v0, v1, v2}, Lqu;->a(Lqt;Lqx;Z)Lqu;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqv;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lqo;->a:Lqt;

    invoke-virtual {v0}, Lqt;->a()Lqv;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqx;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lqo;->a:Lqx;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 489
    iget-boolean v0, p0, Lqo;->a:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 322
    instance-of v1, p1, Lqo;

    if-nez v1, :cond_1

    .line 329
    :cond_0
    :goto_0
    return v0

    .line 325
    :cond_1
    check-cast p1, Lqo;

    .line 326
    iget-object v1, p0, Lqo;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v2, p1, Lqo;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lqo;->a:Ljava/lang/String;

    iget-object v2, p1, Lqo;->a:Ljava/lang/String;

    .line 327
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lqo;->a:Lqx;

    iget-object v2, p1, Lqo;->a:Lqx;

    .line 328
    invoke-virtual {v1, v2}, Lqx;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lqo;->a:Lqt;

    iget-object v2, p1, Lqo;->a:Lqt;

    .line 329
    invoke-virtual {v1, v2}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lqo;->a:Z

    iget-boolean v2, p1, Lqo;->a:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 334
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lqo;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lqo;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lqo;->a:Lqx;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lqo;->a:Lqt;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lqo;->a:Z

    .line 335
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 334
    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 498
    const-string v1, "AclType[%s %s %s%s]"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v3, p0, Lqo;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget-object v3, p0, Lqo;->a:Lqt;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-object v3, p0, Lqo;->a:Lqx;

    aput-object v3, v2, v0

    const/4 v3, 0x3

    iget-boolean v0, p0, Lqo;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "+link"

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
