.class public LbuG;
.super Ljava/lang/RuntimeException;
.source "ProvisionException.java"


# instance fields
.field protected final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lbwy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lbwy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 46
    invoke-static {p1}, LbmY;->a(Ljava/lang/Iterable;)LbmY;

    move-result-object v0

    iput-object v0, p0, LbuG;->a:LbmY;

    .line 47
    iget-object v0, p0, LbuG;->a:LbmY;

    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 48
    iget-object v0, p0, LbuG;->a:LbmY;

    invoke-static {v0}, Lbvd;->a(Ljava/util/Collection;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LbuG;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 49
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 57
    new-instance v0, Lbwy;

    invoke-direct {v0, p1}, Lbwy;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    iput-object v0, p0, LbuG;->a:LbmY;

    .line 58
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    const-string v0, "Unable to provision, see the following errors"

    iget-object v1, p0, LbuG;->a:LbmY;

    invoke-static {v0, v1}, Lbvd;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
