.class LaZL;
.super Landroid/os/AsyncTask;
.source "OwnersImageManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final a:I

.field final synthetic a:LaZJ;

.field final a:LaZK;

.field final a:Landroid/graphics/Bitmap;

.field final a:Landroid/os/ParcelFileDescriptor;


# direct methods
.method constructor <init>(LaZJ;LaZK;Landroid/os/ParcelFileDescriptor;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, LaZL;->a:LaZJ;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 204
    iput-object p2, p0, LaZL;->a:LaZK;

    .line 205
    iput-object p3, p0, LaZL;->a:Landroid/os/ParcelFileDescriptor;

    .line 206
    iput-object p4, p0, LaZL;->a:Landroid/graphics/Bitmap;

    .line 207
    iput p5, p0, LaZL;->a:I

    .line 208
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 212
    .line 214
    :try_start_0
    iget-object v0, p0, LaZL;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, LaZt;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 215
    if-nez v0, :cond_1

    .line 216
    iget-object v0, p0, LaZL;->a:Landroid/graphics/Bitmap;

    move-object v1, v0

    .line 223
    :goto_0
    iget-object v0, p0, LaZL;->a:LaZJ;

    invoke-static {v0}, LaZJ;->a(LaZJ;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    iget-object v2, p0, LaZL;->a:LaZK;

    iget-object v2, v2, LaZK;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    iget-object v0, p0, LaZL;->a:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 227
    :try_start_1
    iget-object v0, p0, LaZL;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 233
    :cond_0
    :goto_1
    return-object v1

    .line 217
    :cond_1
    :try_start_2
    iget-object v1, p0, LaZL;->a:LaZJ;

    invoke-static {v1}, LaZJ;->a(LaZJ;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 218
    invoke-static {v0}, LaZC;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 220
    :cond_2
    iget v1, p0, LaZL;->a:I

    iget-object v2, p0, LaZL;->a:LaZJ;

    invoke-static {v2}, LaZJ;->a(LaZJ;)F

    move-result v2

    iget-object v3, p0, LaZL;->a:LaZJ;

    invoke-static {v3}, LaZJ;->b(LaZJ;)F

    move-result v3

    div-float/2addr v2, v3

    invoke-static {v0, v1, v2}, LaZJ;->a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    const-string v2, "OwnersImageManager"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 225
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaZL;->a:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_3

    .line 227
    :try_start_3
    iget-object v1, p0, LaZL;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 230
    :cond_3
    :goto_2
    throw v0

    .line 228
    :catch_1
    move-exception v1

    .line 229
    const-string v2, "OwnersImageManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, LaZL;->a:LaZK;

    iget-object v0, v0, LaZK;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaZL;->a:LaZK;

    if-eq v0, v1, :cond_0

    .line 242
    :goto_0
    return-void

    .line 241
    :cond_0
    iget-object v0, p0, LaZL;->a:LaZJ;

    iget-object v1, p0, LaZL;->a:LaZK;

    iget-object v1, v1, LaZK;->a:Landroid/widget/ImageView;

    invoke-static {v0, v1, p1}, LaZJ;->a(LaZJ;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaZL;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 197
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, LaZL;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
