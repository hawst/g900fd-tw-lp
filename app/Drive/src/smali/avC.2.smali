.class public LavC;
.super Ljava/util/AbstractList;
.source "MatchRects.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/viewer/pdflib/MatchRects;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, LavC;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, LavC;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->getFirstRect(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0, p1}, LavC;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, LavC;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    # getter for: Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->access$100(Lcom/google/android/apps/viewer/pdflib/MatchRects;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
