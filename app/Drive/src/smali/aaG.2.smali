.class public LaaG;
.super Ljava/lang/Object;
.source "ExtraStreamDataSource.java"


# instance fields
.field private a:LaaK;

.field private a:LaaP;

.field private a:LaaR;

.field private a:Landroid/net/Uri;

.field private a:Ljava/lang/String;

.field private a:Z

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, LaaG;->a:Z

    return-void
.end method

.method static synthetic a(LaaG;)LaaK;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LaaG;->a:LaaK;

    return-object v0
.end method

.method static synthetic a(LaaG;)LaaP;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LaaG;->a:LaaP;

    return-object v0
.end method

.method static synthetic a(LaaG;)LaaR;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LaaG;->a:LaaR;

    return-object v0
.end method

.method static synthetic a(LaaG;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LaaG;->a:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic a(LaaG;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LaaG;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LaaG;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, LaaG;->a:Z

    return v0
.end method

.method static synthetic b(LaaG;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LaaG;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()LaaE;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, LaaG;->a:Landroid/net/Uri;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, LaaG;->a:Ljava/lang/String;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, LaaG;->a:LaaR;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, LaaG;->a:LaaP;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, LaaG;->a:LaaK;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    new-instance v0, LaaE;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaaE;-><init>(LaaG;LaaF;)V

    return-object v0
.end method

.method public a(LaaK;)LaaG;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, LaaG;->a:LaaK;

    .line 66
    return-object p0
.end method

.method public a(LaaP;)LaaG;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, LaaG;->a:LaaP;

    .line 56
    return-object p0
.end method

.method public a(LaaR;)LaaG;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, LaaG;->a:LaaR;

    .line 51
    return-object p0
.end method

.method public a(Landroid/net/Uri;)LaaG;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, LaaG;->a:Landroid/net/Uri;

    .line 36
    return-object p0
.end method

.method public a(Ljava/lang/String;)LaaG;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, LaaG;->a:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public a(Z)LaaG;
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, LaaG;->a:Z

    .line 46
    return-object p0
.end method

.method public b(Ljava/lang/String;)LaaG;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, LaaG;->b:Ljava/lang/String;

    .line 61
    return-object p0
.end method
