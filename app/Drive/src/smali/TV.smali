.class public final LTV;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTT;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTZ;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTR;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTS;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 42
    iput-object p1, p0, LTV;->a:LbrA;

    .line 43
    const-class v0, LTT;

    invoke-static {v0, v1}, LTV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTV;->a:Lbsk;

    .line 46
    const-class v0, LTZ;

    invoke-static {v0, v1}, LTV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTV;->b:Lbsk;

    .line 49
    const-class v0, LTR;

    invoke-static {v0, v1}, LTV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTV;->c:Lbsk;

    .line 52
    const-class v0, LTS;

    invoke-static {v0, v1}, LTV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTV;->d:Lbsk;

    .line 55
    const-class v0, LTU;

    const-class v1, LbuO;

    invoke-static {v0, v1}, LTV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTV;->e:Lbsk;

    .line 58
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 88
    sparse-switch p1, :sswitch_data_0

    .line 153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :sswitch_0
    new-instance v1, LTZ;

    iget-object v0, p0, LTV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->r:Lbsk;

    .line 93
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LTV;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->r:Lbsk;

    .line 91
    invoke-static {v0, v2}, LTV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakk;

    invoke-direct {v1, v0}, LTZ;-><init>(Lakk;)V

    move-object v0, v1

    .line 151
    :goto_0
    return-object v0

    .line 100
    :sswitch_1
    new-instance v0, LTR;

    invoke-direct {v0}, LTR;-><init>()V

    goto :goto_0

    .line 104
    :sswitch_2
    new-instance v2, LTS;

    iget-object v0, p0, LTV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 107
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LTV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 105
    invoke-static {v0, v1}, LTV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LTV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->r:Lbsk;

    .line 113
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LTV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->r:Lbsk;

    .line 111
    invoke-static {v1, v3}, LTV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lakk;

    invoke-direct {v2, v0, v1}, LTS;-><init>(Landroid/content/Context;Lakk;)V

    move-object v0, v2

    .line 118
    goto :goto_0

    .line 120
    :sswitch_3
    new-instance v0, LTU;

    iget-object v1, p0, LTV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 123
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LTV;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 121
    invoke-static {v1, v2}, LTV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LTV;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTV;

    iget-object v2, v2, LTV;->d:Lbsk;

    .line 129
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LTV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTV;

    iget-object v3, v3, LTV;->d:Lbsk;

    .line 127
    invoke-static {v2, v3}, LTV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LTS;

    iget-object v3, p0, LTV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTV;

    iget-object v3, v3, LTV;->b:Lbsk;

    .line 135
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LTV;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LTV;

    iget-object v4, v4, LTV;->b:Lbsk;

    .line 133
    invoke-static {v3, v4}, LTV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LTZ;

    iget-object v4, p0, LTV;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Laae;

    iget-object v4, v4, Laae;->e:Lbsk;

    .line 141
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LTV;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Laae;

    iget-object v5, v5, Laae;->e:Lbsk;

    .line 139
    invoke-static {v4, v5}, LTV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laab;

    iget-object v5, p0, LTV;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lyh;

    iget-object v5, v5, Lyh;->k:Lbsk;

    .line 145
    invoke-static {v5}, LTV;->a(LbuE;)LbuE;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LTU;-><init>(Landroid/content/Context;LTS;LTZ;Laab;LbuE;)V

    goto/16 :goto_0

    .line 88
    nop

    :sswitch_data_0
    .sparse-switch
        0x523 -> :sswitch_1
        0x52e -> :sswitch_0
        0x52f -> :sswitch_3
        0x530 -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 168
    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 65
    const-class v0, LTT;

    iget-object v1, p0, LTV;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LTV;->a(Ljava/lang/Class;Lbsk;)V

    .line 66
    const-class v0, LTZ;

    iget-object v1, p0, LTV;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LTV;->a(Ljava/lang/Class;Lbsk;)V

    .line 67
    const-class v0, LTR;

    iget-object v1, p0, LTV;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LTV;->a(Ljava/lang/Class;Lbsk;)V

    .line 68
    const-class v0, LTS;

    iget-object v1, p0, LTV;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LTV;->a(Ljava/lang/Class;Lbsk;)V

    .line 69
    const-class v0, LTU;

    iget-object v1, p0, LTV;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LTV;->a(Ljava/lang/Class;Lbsk;)V

    .line 70
    iget-object v0, p0, LTV;->a:Lbsk;

    iget-object v1, p0, LTV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTV;

    iget-object v1, v1, LTV;->e:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 72
    iget-object v0, p0, LTV;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x52e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 74
    iget-object v0, p0, LTV;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x523

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 76
    iget-object v0, p0, LTV;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x530

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 78
    iget-object v0, p0, LTV;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x52f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 80
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 160
    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method
