.class LLF;
.super Ljava/lang/Object;
.source "SelectionFunctionDoPin.java"

# interfaces
.implements LLw;


# instance fields
.field private final a:LVg;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LtK;LVg;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, LLF;->a:LtK;

    .line 26
    iput-object p2, p0, LLF;->a:LVg;

    .line 27
    return-void
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, LLF;->a:LVg;

    .line 52
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->a(LaFO;)LVf;

    move-result-object v1

    .line 53
    invoke-virtual {p2}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 54
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-virtual {v1, v0}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    goto :goto_0

    .line 56
    :cond_0
    iget-object v0, p0, LLF;->a:LVg;

    invoke-virtual {v1}, LVf;->a()LVd;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->a(LVd;)V

    .line 57
    return-void
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 63
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, LLF;->a:LtK;

    sget-object v1, Lry;->af:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v2

    .line 37
    :cond_1
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 38
    invoke-interface {v0}, LaGu;->g()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, LaGu;->f()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v0}, LaGu;->e()Z

    move-result v4

    if-nez v4, :cond_0

    .line 41
    :cond_2
    invoke-interface {v0}, LaGu;->f()Z

    move-result v0

    if-nez v0, :cond_4

    .line 42
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 44
    goto :goto_1

    :cond_3
    move v2, v1

    .line 45
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method
