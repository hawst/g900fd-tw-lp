.class LamB;
.super Ljava/lang/Object;
.source "RateLimitedExecutorImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lamy;


# direct methods
.method constructor <init>(Lamy;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, LamB;->a:Lamy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 61
    iget-object v1, p0, LamB;->a:Lamy;

    monitor-enter v1

    .line 62
    :try_start_0
    iget-object v0, p0, LamB;->a:Lamy;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lamy;->a(Lamy;J)J

    .line 63
    iget-object v0, p0, LamB;->a:Lamy;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lamy;->a(Lamy;Z)Z

    .line 64
    iget-object v0, p0, LamB;->a:Lamy;

    invoke-static {v0}, Lamy;->a(Lamy;)LamD;

    move-result-object v0

    sget-object v2, LamD;->a:LamD;

    invoke-virtual {v0, v2}, LamD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, LamB;->a:Lamy;

    sget-object v2, LamD;->b:LamD;

    invoke-static {v0, v2}, Lamy;->a(Lamy;LamD;)LamD;

    .line 66
    const-string v0, "RateLimitedExecutor_updater"

    const-string v2, "Launching suppliedRunnable, for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LamB;->a:Lamy;

    .line 67
    invoke-virtual {v5}, Lamy;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 66
    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 68
    iget-object v0, p0, LamB;->a:Lamy;

    invoke-static {v0}, Lamy;->a(Lamy;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v2, p0, LamB;->a:Lamy;

    invoke-static {v2}, Lamy;->c(Lamy;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 72
    :goto_0
    monitor-exit v1

    .line 73
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, LamB;->a:Lamy;

    sget-object v2, LamD;->c:LamD;

    invoke-static {v0, v2}, Lamy;->a(Lamy;LamD;)LamD;

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
