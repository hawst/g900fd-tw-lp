.class final LbrQ;
.super Ljava/lang/Object;
.source "GellyInjector.java"

# interfaces
.implements LbrH;
.implements Lbuu;


# static fields
.field private static final a:LbuB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuB",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LbrC;

.field private final a:LbrE;

.field private final a:Lbsi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsi",
            "<",
            "Lbsk",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:Lbsm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsm",
            "<",
            "LbuB",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LbuH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, LbrR;

    invoke-direct {v0}, LbrR;-><init>()V

    sput-object v0, LbrQ;->a:LbuB;

    return-void
.end method

.method constructor <init>(Lbsi;Lbsm;Ljava/util/Map;LbrE;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbsi",
            "<",
            "Lbsk",
            "<*>;>;",
            "Lbsm",
            "<",
            "LbuB",
            "<*>;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LbuH;",
            ">;",
            "LbrE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsi;

    iput-object v0, p0, LbrQ;->a:Lbsi;

    .line 56
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsm;

    iput-object v0, p0, LbrQ;->a:Lbsm;

    .line 57
    invoke-static {p3}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, LbrQ;->a:Ljava/util/Map;

    .line 59
    invoke-static {p4}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbrE;

    iput-object v0, p0, LbrQ;->a:LbrE;

    .line 60
    new-instance v0, LbrC;

    invoke-direct {v0, p1}, LbrC;-><init>(Lbsi;)V

    iput-object v0, p0, LbrQ;->a:LbrC;

    .line 61
    return-void
.end method


# virtual methods
.method public a(LbuP;)LbuB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuB",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, LbrQ;->a:Lbsm;

    invoke-virtual {v0, p1}, Lbsm;->a(LbuP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuB;

    .line 126
    if-nez v0, :cond_0

    .line 127
    sget-object v0, LbrQ;->a:LbuB;

    .line 129
    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/Class;)LbuB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuB",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, LbrQ;->a:Lbsm;

    invoke-virtual {v0, p1}, Lbsm;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuB;

    .line 136
    if-nez v0, :cond_0

    .line 137
    sget-object v0, LbrQ;->a:LbuB;

    .line 139
    :cond_0
    return-object v0
.end method

.method public final a(Lbuv;)LbuE;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, LbrQ;->a:Lbsi;

    invoke-virtual {v0, p1}, Lbsi;->a(Lbuv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    .line 83
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_0

    .line 85
    invoke-virtual {p1}, Lbuv;->a()LbuP;

    move-result-object v0

    invoke-virtual {v0}, LbuP;->a()Ljava/lang/Class;

    move-result-object v0

    .line 86
    iget-object v1, p0, LbrQ;->a:LbrE;

    invoke-virtual {v1, v0}, LbrE;->a(Ljava/lang/Class;)LbuE;

    move-result-object v0

    .line 89
    :cond_0
    if-nez v0, :cond_1

    .line 90
    new-instance v0, Lbwy;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find a provider for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbwy;-><init>(Ljava/lang/String;)V

    .line 91
    new-instance v1, Lbur;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Lbur;-><init>(Ljava/lang/Iterable;)V

    throw v1

    .line 93
    :cond_1
    return-object v0
.end method

.method public a(Lbuv;)Lbup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "Lbup",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 196
    invoke-virtual {p0, p1}, LbrQ;->b(Lbuv;)Lbup;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbuv;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {p1}, Lbsn;->a(Lbuv;)Lbuv;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0, v0}, LbrQ;->a(Lbuv;)LbuE;

    move-result-object v0

    .line 110
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, LbrQ;->a(Lbuv;)LbuE;

    move-result-object v0

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 98
    invoke-static {p1}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-virtual {p0, v0}, LbrQ;->a(Lbuv;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(LbuP;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "Ljava/util/List",
            "<",
            "Lbup",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, LbrQ;->a:LbrC;

    invoke-virtual {v0, p1}, LbrC;->a(LbuP;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lbuv",
            "<*>;",
            "Lbup",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, LbrQ;->a:Lbsi;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, LbrQ;->a(Ljava/lang/Class;)LbuB;

    move-result-object v0

    .line 119
    invoke-interface {v0, p1}, LbuB;->a(Ljava/lang/Object;)V

    .line 120
    return-void
.end method

.method public b(Lbuv;)Lbup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "Lbup",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, LbrQ;->a:Lbsi;

    invoke-virtual {v0, p1}, Lbsi;->a(Lbuv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbup;

    return-object v0
.end method
