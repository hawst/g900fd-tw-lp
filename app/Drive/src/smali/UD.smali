.class public LUD;
.super Ljava/lang/Object;
.source "OpenEntryLookupHelper.java"


# instance fields
.field private final a:LSF;

.field private final a:LaGM;

.field private final a:Laer;

.field private final a:Lagl;

.field private final a:LbsW;


# direct methods
.method constructor <init>(LaGM;Laer;Lagl;LSF;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v0

    iput-object v0, p0, LUD;->a:LbsW;

    .line 87
    iput-object p1, p0, LUD;->a:LaGM;

    .line 88
    iput-object p2, p0, LUD;->a:Laer;

    .line 89
    iput-object p3, p0, LUD;->a:Lagl;

    .line 90
    iput-object p4, p0, LUD;->a:LSF;

    .line 91
    return-void
.end method

.method static synthetic a(LUD;)LSF;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LUD;->a:LSF;

    return-object v0
.end method

.method static synthetic a(LUD;)LaGM;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LUD;->a:LaGM;

    return-object v0
.end method

.method static synthetic a(LUD;)Laer;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LUD;->a:Laer;

    return-object v0
.end method

.method static synthetic a(LUD;)Lagl;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LUD;->a:Lagl;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
            ")",
            "LbsU",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, LUD;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_0

    .line 102
    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    .line 105
    :cond_0
    iget-object v0, p0, LUD;->a:LbsW;

    new-instance v1, LUE;

    invoke-direct {v1, p0, p1}, LUE;-><init>(LUD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
