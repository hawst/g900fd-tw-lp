.class public final Lbej;
.super Ljava/lang/Object;
.source "HttpRequestFactory.java"


# instance fields
.field private final a:Lbek;

.field private final a:Lbeq;


# direct methods
.method constructor <init>(Lbeq;Lbek;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lbej;->a:Lbeq;

    .line 55
    iput-object p2, p0, Lbej;->a:Lbek;

    .line 56
    return-void
.end method


# virtual methods
.method public a(LbdY;)Lbei;
    .locals 2

    .prologue
    .line 122
    const-string v0, "GET"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lbej;->a(Ljava/lang/String;LbdY;LbdZ;)Lbei;

    move-result-object v0

    return-object v0
.end method

.method public a(LbdY;LbdZ;)Lbei;
    .locals 1

    .prologue
    .line 133
    const-string v0, "POST"

    invoke-virtual {p0, v0, p1, p2}, Lbej;->a(Ljava/lang/String;LbdY;LbdZ;)Lbei;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;LbdY;LbdZ;)Lbei;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lbej;->a:Lbeq;

    invoke-virtual {v0}, Lbeq;->a()Lbei;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lbej;->a:Lbek;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lbej;->a:Lbek;

    invoke-interface {v1, v0}, Lbek;->a(Lbei;)V

    .line 95
    :cond_0
    invoke-virtual {v0, p1}, Lbei;->a(Ljava/lang/String;)Lbei;

    .line 96
    if-eqz p2, :cond_1

    .line 97
    invoke-virtual {v0, p2}, Lbei;->a(LbdY;)Lbei;

    .line 99
    :cond_1
    if-eqz p3, :cond_2

    .line 100
    invoke-virtual {v0, p3}, Lbei;->a(LbdZ;)Lbei;

    .line 102
    :cond_2
    return-object v0
.end method

.method public a()Lbek;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lbej;->a:Lbek;

    return-object v0
.end method

.method public a()Lbeq;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lbej;->a:Lbeq;

    return-object v0
.end method

.method public b(LbdY;LbdZ;)Lbei;
    .locals 1

    .prologue
    .line 144
    const-string v0, "PUT"

    invoke-virtual {p0, v0, p1, p2}, Lbej;->a(Ljava/lang/String;LbdY;LbdZ;)Lbei;

    move-result-object v0

    return-object v0
.end method
