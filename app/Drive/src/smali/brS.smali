.class public abstract LbrS;
.super Ljava/lang/Object;
.source "GellyInjectorBuilderBase.java"

# interfaces
.implements Lbru;
.implements Lbrv;


# instance fields
.field private final a:LbrE;

.field private a:LbrN;

.field private a:LbrQ;

.field private final a:Lbsd;

.field private final a:Lbsi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsi",
            "<",
            "Lbsk",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:Lbsm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsm",
            "<",
            "LbuB",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<+",
            "LbuC;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lbse;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LbrV;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbsi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsi",
            "<",
            "LbuH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "LbuC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    new-instance v0, Lbsi;

    invoke-direct {v0}, Lbsi;-><init>()V

    iput-object v0, p0, LbrS;->a:Lbsi;

    .line 233
    new-instance v0, Lbsm;

    invoke-direct {v0}, Lbsm;-><init>()V

    iput-object v0, p0, LbrS;->a:Lbsm;

    .line 236
    new-instance v0, Lbsi;

    invoke-direct {v0}, Lbsi;-><init>()V

    iput-object v0, p0, LbrS;->b:Lbsi;

    .line 238
    new-instance v0, Lbsd;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbsd;-><init>(LbrT;)V

    iput-object v0, p0, LbrS;->a:Lbsd;

    .line 240
    const-class v0, LbrV;

    .line 241
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LbrS;->a:Ljava/util/EnumSet;

    .line 242
    new-instance v0, LbrE;

    invoke-direct {v0}, LbrE;-><init>()V

    iput-object v0, p0, LbrS;->a:LbrE;

    .line 250
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    iput-object v0, p0, LbrS;->a:Ljava/lang/Iterable;

    .line 252
    invoke-static {p0}, Lbrt;->a(Lbrv;)V

    .line 253
    invoke-static {p0}, Lbrt;->a(Lbru;)V

    .line 254
    return-void
.end method

.method private a(Lbuv;)Lbsk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "Lbsk",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 361
    iget-object v0, p0, LbrS;->a:Lbsi;

    invoke-virtual {v0, p1}, Lbsi;->a(Lbuv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsk;

    .line 362
    if-nez v0, :cond_0

    .line 363
    new-instance v0, Lbsk;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lbsk;-><init>(Lbuv;Ljava/lang/Class;)V

    .line 364
    iget-object v1, p0, LbrS;->a:Lbsi;

    invoke-virtual {v1, p1, v0}, Lbsi;->a(Lbuv;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/Class;)Lbsk;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lbsk",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 376
    iget-object v0, p0, LbrS;->a:Lbsi;

    invoke-virtual {v0, p1}, Lbsi;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsk;

    .line 377
    const-string v1, "Cannot find static provider for class: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    return-object v0
.end method

.method private a(Ljava/lang/Class;)LbuH;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LbuH;"
        }
    .end annotation

    .prologue
    .line 492
    iget-object v0, p0, LbrS;->a:LbrN;

    invoke-virtual {v0}, LbrN;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuH;

    .line 493
    const-string v1, "Cannot find scope instance for %s scope annotation."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    return-object v0
.end method

.method static synthetic a(LbrS;)Ljava/util/EnumSet;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LbrS;->a:Ljava/util/EnumSet;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 278
    new-instance v0, LbrU;

    iget-object v2, p0, LbrS;->a:Lbsd;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, LbrU;-><init>(LbuE;LbrT;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    iget-object v0, p0, LbrS;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuC;

    .line 280
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 283
    :cond_0
    new-instance v2, LbrP;

    invoke-direct {v2}, LbrP;-><init>()V

    .line 284
    new-instance v3, LbrL;

    invoke-direct {v3, v2}, LbrL;-><init>(LbrP;)V

    .line 285
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuC;

    .line 286
    invoke-virtual {v3, v0}, LbrL;->a(LbuC;)V

    goto :goto_1

    .line 288
    :cond_1
    invoke-virtual {v2}, LbrP;->a()LbrN;

    move-result-object v0

    iput-object v0, p0, LbrS;->a:LbrN;

    .line 289
    iget-object v0, p0, LbrS;->b:Lbsi;

    iget-object v1, p0, LbrS;->a:LbrN;

    invoke-virtual {v1}, LbrN;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsi;->putAll(Ljava/util/Map;)V

    .line 290
    return-void
.end method

.method static synthetic a(LbrS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbrS;->a()V

    return-void
.end method

.method private a(Lbuv;Lbxw;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<*>;",
            "Lbxw",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 327
    invoke-direct {p0, p1}, LbrS;->a(Lbuv;)Lbsk;

    move-result-object v1

    .line 328
    invoke-virtual {v1, p2}, Lbsk;->a(Lbxw;)V

    .line 330
    if-eqz p3, :cond_0

    .line 331
    iget-object v0, p0, LbrS;->a:Lbsm;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbsm;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuB;

    .line 332
    if-eqz v0, :cond_0

    .line 333
    invoke-virtual {v1, v0, p3}, Lbsk;->a(LbuB;Ljava/lang/Object;)V

    .line 336
    :cond_0
    return-void
.end method

.method private a(LbwB;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbwB",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 458
    iget-object v0, p0, LbrS;->a:LbrQ;

    invoke-virtual {p1}, LbwB;->a()Lbuv;

    move-result-object v1

    invoke-virtual {v0, v1}, LbrQ;->a(Lbuv;)LbuE;

    move-result-object v0

    .line 459
    invoke-virtual {p1, v0}, LbwB;->a(LbuE;)V

    .line 460
    return-void
.end method

.method private a(Lbwx;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbwx",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 464
    iget-object v0, p0, LbrS;->a:LbrQ;

    .line 465
    invoke-virtual {p1}, Lbwx;->a()LbuP;

    move-result-object v1

    .line 464
    invoke-virtual {v0, v1}, LbrQ;->a(LbuP;)LbuB;

    move-result-object v0

    .line 466
    invoke-virtual {p1, v0}, Lbwx;->a(LbuB;)V

    .line 467
    return-void
.end method

.method private b(Lbuv;)Lbsk;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<*>;)",
            "Lbsk",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 370
    iget-object v0, p0, LbrS;->a:Lbsi;

    invoke-virtual {v0, p1}, Lbsi;->a(Lbuv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsk;

    .line 371
    const-string v1, "Cannot find static provider for key: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 293
    invoke-virtual {p0}, LbrS;->a()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, LbrS;->a:Ljava/util/Collection;

    .line 294
    iget-object v0, p0, LbrS;->a:Ljava/util/Collection;

    invoke-virtual {p0, v0}, LbrS;->a(Ljava/util/Collection;)V

    .line 295
    return-void
.end method

.method static synthetic b(LbrS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbrS;->b()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 314
    iget-object v0, p0, LbrS;->a:LbrN;

    invoke-virtual {v0}, LbrN;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 315
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbuv;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LbwI;->a(Ljava/lang/Object;)LbuE;

    move-result-object v3

    .line 316
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 315
    invoke-direct {p0, v1, v3, v0}, LbrS;->a(Lbuv;Lbxw;Ljava/lang/Object;)V

    goto :goto_0

    .line 320
    :cond_0
    iget-object v0, p0, LbrS;->a:LbrN;

    invoke-virtual {v0}, LbrN;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 321
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbuv;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbxw;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, LbrS;->a(Lbuv;Lbxw;Ljava/lang/Object;)V

    goto :goto_1

    .line 323
    :cond_1
    return-void
.end method

.method static synthetic c(LbrS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbrS;->c()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 342
    iget-object v0, p0, LbrS;->b:Lbsi;

    invoke-virtual {v0}, Lbsi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 343
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbuv;

    .line 344
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuH;

    .line 346
    invoke-direct {p0, v1}, LbrS;->b(Lbuv;)Lbsk;

    move-result-object v1

    .line 347
    invoke-virtual {v1, v0}, Lbsk;->a(LbuH;)V

    goto :goto_0

    .line 350
    :cond_0
    iget-object v0, p0, LbrS;->b:Lbsi;

    invoke-virtual {v0}, Lbsi;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 351
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 352
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuH;

    .line 354
    invoke-direct {p0, v1}, LbrS;->a(Ljava/lang/Class;)Lbsk;

    move-result-object v1

    .line 355
    invoke-virtual {v1, v0}, Lbsk;->a(LbuH;)V

    goto :goto_1

    .line 357
    :cond_1
    return-void
.end method

.method static synthetic d(LbrS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbrS;->d()V

    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    .line 385
    invoke-direct {p0}, LbrS;->f()V

    .line 386
    new-instance v0, LbrQ;

    iget-object v1, p0, LbrS;->a:Lbsi;

    iget-object v2, p0, LbrS;->a:Lbsm;

    iget-object v3, p0, LbrS;->a:LbrN;

    .line 387
    invoke-virtual {v3}, LbrN;->d()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, LbrS;->a:LbrE;

    invoke-direct {v0, v1, v2, v3, v4}, LbrQ;-><init>(Lbsi;Lbsm;Ljava/util/Map;LbrE;)V

    iput-object v0, p0, LbrS;->a:LbrQ;

    .line 389
    iget-object v0, p0, LbrS;->a:LbrE;

    iget-object v1, p0, LbrS;->a:LbrQ;

    invoke-virtual {v0, v1}, LbrE;->a(LbrH;)V

    .line 390
    iget-object v0, p0, LbrS;->a:Lbsd;

    iget-object v1, p0, LbrS;->a:LbrQ;

    invoke-virtual {v0, v1}, Lbsd;->a(Lbuu;)V

    .line 391
    return-void
.end method

.method static synthetic e(LbrS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbrS;->e()V

    return-void
.end method

.method private f()V
    .locals 5

    .prologue
    .line 394
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 395
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 397
    iget-object v0, p0, LbrS;->a:Lbsi;

    invoke-virtual {v0}, Lbsi;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 398
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbsk;

    invoke-virtual {v1}, Lbsk;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 399
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 403
    :cond_1
    iget-object v0, p0, LbrS;->a:Lbsi;

    invoke-virtual {v0}, Lbsi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 404
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbsk;

    invoke-virtual {v1}, Lbsk;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 405
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 409
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 410
    iget-object v2, p0, LbrS;->a:Lbsi;

    invoke-virtual {v2, v0}, Lbsi;->b(Ljava/lang/Class;)Ljava/lang/Object;

    goto :goto_2

    .line 413
    :cond_4
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    .line 414
    iget-object v2, p0, LbrS;->a:Lbsi;

    invoke-virtual {v2, v0}, Lbsi;->b(Lbuv;)Ljava/lang/Object;

    goto :goto_3

    .line 416
    :cond_5
    return-void
.end method

.method static synthetic f(LbrS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbrS;->h()V

    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 434
    iget-object v0, p0, LbrS;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbse;

    .line 435
    invoke-virtual {v0}, Lbse;->b()V

    goto :goto_0

    .line 438
    :cond_0
    iget-object v0, p0, LbrS;->a:LbrN;

    invoke-virtual {v0}, LbrN;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 439
    iget-object v2, p0, LbrS;->a:LbrQ;

    invoke-virtual {v2, v1}, LbrQ;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 441
    :cond_1
    return-void
.end method

.method static synthetic g(LbrS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbrS;->g()V

    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, LbrS;->a:LbrN;

    invoke-virtual {v0}, LbrN;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbwB;

    .line 449
    invoke-direct {p0, v0}, LbrS;->a(LbwB;)V

    goto :goto_0

    .line 452
    :cond_0
    iget-object v0, p0, LbrS;->a:LbrN;

    invoke-virtual {v0}, LbrN;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwx;

    .line 453
    invoke-direct {p0, v0}, LbrS;->a(Lbwx;)V

    goto :goto_1

    .line 455
    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized a()Lbuu;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 260
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LbrS;->a:LbrQ;

    if-eqz v1, :cond_0

    .line 261
    iget-object v0, p0, LbrS;->a:LbrQ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    :goto_0
    monitor-exit p0

    return-object v0

    .line 263
    :cond_0
    :try_start_1
    iget-object v1, p0, LbrS;->a:Ljava/util/EnumSet;

    invoke-virtual {v1}, Ljava/util/EnumSet;->size()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    const-string v2, "This builder is no longer usable. Please create a new builder."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 266
    invoke-static {}, LbrV;->values()[LbrV;

    move-result-object v1

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 267
    invoke-virtual {v3, p0}, LbrV;->a(LbrS;)V

    .line 266
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    move v1, v0

    .line 263
    goto :goto_1

    .line 270
    :cond_2
    iget-object v0, p0, LbrS;->a:LbrQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 553
    iget-object v0, p0, LbrS;->a:LbrN;

    invoke-virtual {v0, p1}, LbrN;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 554
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find module for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    return-object v0
.end method

.method protected abstract a()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lbse;",
            ">;"
        }
    .end annotation
.end method

.method protected a(LbuP;LbuB;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;",
            "LbuB",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 504
    iget-object v0, p0, LbrS;->a:LbrQ;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "No one should be registering additional member injectors after the injector has been initialized."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 507
    iget-object v0, p0, LbrS;->a:Lbsm;

    invoke-virtual {v0, p1, p2}, Lbsm;->a(LbuP;Ljava/lang/Object;)V

    .line 508
    return-void

    :cond_0
    move v0, v1

    .line 504
    goto :goto_0
.end method

.method protected a(Lbuv;Lbsk;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;",
            "Lbsk",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 473
    iget-object v0, p0, LbrS;->a:Lbsi;

    invoke-virtual {v0, p1, p2}, Lbsi;->a(Lbuv;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    invoke-virtual {p2}, Lbsk;->a()Ljava/lang/Class;

    move-result-object v0

    .line 475
    if-eqz v0, :cond_0

    .line 476
    iget-object v1, p0, LbrS;->b:Lbsi;

    invoke-direct {p0, v0}, LbrS;->a(Ljava/lang/Class;)LbuH;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lbsi;->a(Lbuv;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Class;Lbsk;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lbsk",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, LbrS;->a:Lbsi;

    invoke-virtual {v0, p1, p2}, Lbsi;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    invoke-virtual {p2}, Lbsk;->a()Ljava/lang/Class;

    move-result-object v0

    .line 486
    if-eqz v0, :cond_0

    .line 487
    iget-object v1, p0, LbrS;->b:Lbsi;

    invoke-direct {p0, v0}, LbrS;->a(Ljava/lang/Class;)LbuH;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lbsi;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    :cond_0
    return-void
.end method

.method protected a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lbse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 298
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbse;

    .line 299
    invoke-virtual {v0}, Lbse;->a()V

    goto :goto_0

    .line 301
    :cond_0
    return-void
.end method
