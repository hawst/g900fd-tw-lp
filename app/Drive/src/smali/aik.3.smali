.class public Laik;
.super Ljava/io/InputStream;
.source "SyncProgressMonitorInputStream.java"


# instance fields
.field private final a:J

.field private final a:LagH;

.field private a:Ljava/io/InputStream;

.field private b:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;LagH;JJ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 26
    iput-object p1, p0, Laik;->a:Ljava/io/InputStream;

    .line 27
    iput-object p2, p0, Laik;->a:LagH;

    .line 28
    iput-wide p3, p0, Laik;->a:J

    .line 29
    iput-wide p5, p0, Laik;->b:J

    .line 30
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 84
    invoke-static {}, LpD;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, LQp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - thread interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LQp;-><init>(Ljava/lang/String;)V

    .line 87
    iget-wide v2, p0, Laik;->b:J

    long-to-int v1, v2

    iput v1, v0, LQp;->bytesTransferred:I

    .line 88
    throw v0

    .line 90
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 77
    iget-wide v0, p0, Laik;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Laik;->b:J

    .line 78
    iget-object v0, p0, Laik;->a:LagH;

    iget-wide v2, p0, Laik;->b:J

    iget-wide v4, p0, Laik;->a:J

    invoke-interface {v0, v2, v3, v4, v5}, LagH;->a(JJ)V

    .line 79
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Laik;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Laik;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Laik;->a:Ljava/io/InputStream;

    .line 74
    :cond_0
    return-void
.end method

.method public read()I
    .locals 4

    .prologue
    .line 34
    invoke-direct {p0}, Laik;->a()V

    .line 35
    iget-object v0, p0, Laik;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 36
    invoke-direct {p0}, Laik;->a()V

    .line 38
    if-ltz v0, :cond_0

    .line 39
    const-wide/16 v2, 0x1

    invoke-direct {p0, v2, v3}, Laik;->a(J)V

    .line 41
    :cond_0
    return v0
.end method

.method public read([B)I
    .locals 4

    .prologue
    .line 46
    invoke-direct {p0}, Laik;->a()V

    .line 47
    iget-object v0, p0, Laik;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 48
    invoke-direct {p0}, Laik;->a()V

    .line 50
    if-lez v0, :cond_0

    .line 51
    int-to-long v2, v0

    invoke-direct {p0, v2, v3}, Laik;->a(J)V

    .line 53
    :cond_0
    return v0
.end method

.method public read([BII)I
    .locals 4

    .prologue
    .line 58
    invoke-direct {p0}, Laik;->a()V

    .line 59
    iget-object v0, p0, Laik;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 60
    invoke-direct {p0}, Laik;->a()V

    .line 62
    if-lez v0, :cond_0

    .line 63
    int-to-long v2, v0

    invoke-direct {p0, v2, v3}, Laik;->a(J)V

    .line 65
    :cond_0
    return v0
.end method
