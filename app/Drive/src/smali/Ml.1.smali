.class public final enum LMl;
.super Ljava/lang/Enum;
.source "CrossAppStateProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LMl;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LMl;

.field private static final synthetic a:[LMl;

.field public static final enum b:LMl;

.field public static final enum c:LMl;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 84
    new-instance v0, LMl;

    const-string v1, "PINNED_STATE"

    const-string v2, "pinnedState"

    invoke-direct {v0, v1, v3, v2}, LMl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LMl;->a:LMl;

    .line 85
    new-instance v0, LMl;

    const-string v1, "ACCOUNT_METADATA"

    const-string v2, "accountMetadata"

    invoke-direct {v0, v1, v4, v2}, LMl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LMl;->b:LMl;

    .line 86
    new-instance v0, LMl;

    const-string v1, "PROVIDER_VERSION"

    const-string v2, "providerVersion"

    invoke-direct {v0, v1, v5, v2}, LMl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LMl;->c:LMl;

    .line 83
    const/4 v0, 0x3

    new-array v0, v0, [LMl;

    sget-object v1, LMl;->a:LMl;

    aput-object v1, v0, v3

    sget-object v1, LMl;->b:LMl;

    aput-object v1, v0, v4

    sget-object v1, LMl;->c:LMl;

    aput-object v1, v0, v5

    sput-object v0, LMl;->a:[LMl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 91
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LMl;->a:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public static synthetic a(LMl;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, LMl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LMl;
    .locals 1

    .prologue
    .line 83
    const-class v0, LMl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LMl;

    return-object v0
.end method

.method public static values()[LMl;
    .locals 1

    .prologue
    .line 83
    sget-object v0, LMl;->a:[LMl;

    invoke-virtual {v0}, [LMl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LMl;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 98
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LMl;->a:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
