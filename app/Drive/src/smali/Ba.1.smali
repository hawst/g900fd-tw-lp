.class public LBa;
.super Landroid/widget/BaseAdapter;
.source "DocListEmptyViewAdapter.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:LAD;

.field private final a:LAZ;

.field private a:LBd;

.field private a:LCl;

.field private a:LCn;

.field private final a:LJy;

.field private final a:Lafw;

.field private final a:Landroid/app/Activity;

.field private final a:Landroid/database/DataSetObserver;

.field private final a:Landroid/view/LayoutInflater;

.field private a:LapE;

.field private a:LapF;

.field private a:Lapv;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Larg;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LJy;Laja;Lafw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LJy;",
            "Laja",
            "<",
            "Larg;",
            ">;",
            "Lafw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 46
    new-instance v0, LBb;

    invoke-direct {v0, p0}, LBb;-><init>(LBa;)V

    iput-object v0, p0, LBa;->a:Landroid/database/DataSetObserver;

    .line 50
    sget-object v0, LBd;->a:LBd;

    iput-object v0, p0, LBa;->a:LBd;

    .line 51
    sget-object v0, LCn;->l:LCn;

    iput-object v0, p0, LBa;->a:LCn;

    .line 59
    sget-object v0, LapE;->c:LapE;

    iput-object v0, p0, LBa;->a:LapE;

    .line 65
    iput-object p1, p0, LBa;->a:Landroid/app/Activity;

    .line 66
    iput-object p2, p0, LBa;->a:LJy;

    .line 67
    iput-object p3, p0, LBa;->a:Lbxw;

    .line 68
    iput-object p4, p0, LBa;->a:Lafw;

    .line 69
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LBa;->a:Landroid/view/LayoutInflater;

    .line 70
    new-instance v0, LAZ;

    iget-object v1, p0, LBa;->a:Landroid/view/LayoutInflater;

    invoke-direct {v0, v1}, LAZ;-><init>(Landroid/view/LayoutInflater;)V

    iput-object v0, p0, LBa;->a:LAZ;

    .line 71
    iget-object v0, p0, LBa;->a:Landroid/database/DataSetObserver;

    invoke-virtual {p2, v0}, LJy;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 72
    return-void
.end method

.method private a()LBd;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    iget-object v0, p0, LBa;->a:LCl;

    invoke-interface {v0}, LCl;->a()LCn;

    move-result-object v0

    sget-object v3, LCn;->a:LCn;

    invoke-virtual {v0, v3}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LBa;->a:LAD;

    .line 193
    invoke-interface {v0}, LAD;->getCount()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 194
    sget-object v0, LBd;->c:LBd;

    .line 238
    :goto_0
    return-object v0

    .line 195
    :cond_0
    iget-object v0, p0, LBa;->a:LAD;

    invoke-interface {v0}, LAD;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LBa;->a:LJy;

    invoke-virtual {v0}, LJy;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    :cond_1
    sget-object v0, LBd;->a:LBd;

    goto :goto_0

    .line 199
    :cond_2
    iget-object v0, p0, LBa;->a:LCl;

    invoke-interface {v0}, LCl;->a()LaeZ;

    move-result-object v3

    .line 200
    iget-object v0, p0, LBa;->a:LCl;

    invoke-interface {v0}, LCl;->a()LCn;

    move-result-object v0

    sget-object v4, LCn;->e:LCn;

    invoke-virtual {v0, v4}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 201
    iget-object v0, p0, LBa;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    sget-object v5, Larj;->b:Larj;

    iget-object v0, p0, LBa;->a:Lbxw;

    .line 202
    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    invoke-interface {v0}, Larg;->a()Larj;

    move-result-object v0

    invoke-virtual {v5, v0}, Larj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 203
    :goto_1
    iget-object v5, p0, LBa;->a:Lapv;

    if-eqz v5, :cond_4

    iget-object v5, p0, LBa;->a:Lapv;

    invoke-interface {v5}, Lapv;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 204
    :goto_2
    iget-object v5, p0, LBa;->a:LapE;

    sget-object v6, LapE;->c:LapE;

    invoke-virtual {v5, v6}, LapE;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 208
    if-eqz v5, :cond_a

    .line 209
    if-eqz v0, :cond_7

    .line 210
    iget-object v0, p0, LBa;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    if-eqz v0, :cond_5

    iget-object v0, p0, LBa;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Ljava/lang/String;

    move-result-object v0

    .line 211
    :goto_3
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 212
    sget-object v0, LBd;->d:LBd;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 202
    goto :goto_1

    :cond_4
    move v1, v2

    .line 203
    goto :goto_2

    .line 210
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 214
    :cond_6
    sget-object v0, LBd;->g:LBd;

    goto :goto_0

    .line 219
    :cond_7
    iget-object v0, p0, LBa;->a:Lafw;

    invoke-interface {v0}, Lafw;->a()Z

    move-result v0

    .line 220
    sget-object v1, LaeZ;->d:LaeZ;

    invoke-virtual {v3, v1}, LaeZ;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 225
    :goto_4
    if-eqz v2, :cond_8

    .line 226
    sget-object v0, LBd;->f:LBd;

    goto/16 :goto_0

    .line 227
    :cond_8
    if-eqz v4, :cond_9

    sget-object v0, LuM;->a:LuM;

    iget-object v1, p0, LBa;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, LuM;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 228
    sget-object v0, LBd;->e:LBd;

    goto/16 :goto_0

    .line 230
    :cond_9
    sget-object v0, LBd;->d:LBd;

    goto/16 :goto_0

    .line 235
    :cond_a
    if-eqz v1, :cond_b

    .line 236
    sget-object v0, LBd;->g:LBd;

    goto/16 :goto_0

    .line 238
    :cond_b
    sget-object v0, LBd;->b:LBd;

    goto/16 :goto_0

    :cond_c
    move v2, v0

    goto :goto_4
.end method

.method private a()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, LBa;->a:LCl;

    if-nez v0, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    invoke-direct {p0}, LBa;->a()LBd;

    move-result-object v0

    .line 183
    iget-object v1, p0, LBa;->a:LCl;

    invoke-interface {v1}, LCl;->a()LCn;

    move-result-object v1

    .line 184
    iget-object v2, p0, LBa;->a:LBd;

    invoke-virtual {v2, v0}, LBd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LBa;->a:LCn;

    invoke-virtual {v1, v2}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 185
    :cond_2
    iput-object v0, p0, LBa;->a:LBd;

    .line 186
    iput-object v1, p0, LBa;->a:LCn;

    .line 187
    invoke-virtual {p0}, LBa;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method static synthetic a(LBa;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, LBa;->a()V

    return-void
.end method


# virtual methods
.method public a(LAD;)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, LBa;->a:LAD;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    iget-object v0, p0, LBa;->a:LAD;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, LBa;->a:LAD;

    iget-object v1, p0, LBa;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, LAD;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 162
    :cond_0
    iput-object p1, p0, LBa;->a:LAD;

    .line 163
    iget-object v0, p0, LBa;->a:LAD;

    iget-object v1, p0, LBa;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, LAD;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 165
    :cond_1
    return-void
.end method

.method public a(LCl;)V
    .locals 1

    .prologue
    .line 168
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    iput-object v0, p0, LBa;->a:LCl;

    .line 169
    return-void
.end method

.method public a(LapE;)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, LBa;->a:LapE;

    invoke-virtual {p1, v0}, LapE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LapE;

    iput-object v0, p0, LBa;->a:LapE;

    .line 174
    invoke-direct {p0}, LBa;->a()V

    .line 176
    :cond_0
    return-void
.end method

.method public a(LapF;)V
    .locals 1

    .prologue
    .line 154
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LapF;

    iput-object v0, p0, LBa;->a:LapF;

    .line 155
    return-void
.end method

.method public a(Lapv;)V
    .locals 1

    .prologue
    .line 150
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv;

    iput-object v0, p0, LBa;->a:Lapv;

    .line 151
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 1

    .prologue
    .line 146
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iput-object v0, p0, LBa;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 147
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 77
    sget-object v0, LBd;->a:LBd;

    iget-object v1, p0, LBa;->a:LBd;

    invoke-virtual {v0, v1}, LBd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 87
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 92
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 96
    sget-object v0, LBc;->a:[I

    iget-object v2, p0, LBa;->a:LBd;

    invoke-virtual {v2}, LBd;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LBa;->a:LBd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 92
    goto :goto_0

    .line 98
    :pswitch_0
    iget-object v0, p0, LBa;->a:Landroid/view/LayoutInflater;

    sget v2, Lxe;->doc_list_syncing:I

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 120
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v2, p0, LBa;->a:LBd;

    sget-object v3, LBd;->c:LBd;

    invoke-virtual {v2, v3}, LBd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 142
    :goto_2
    return-object v0

    .line 101
    :pswitch_1
    iget-object v0, p0, LBa;->a:Landroid/view/LayoutInflater;

    sget v2, Lxe;->doc_list_empty_recent_view:I

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 104
    :pswitch_2
    iget-object v0, p0, LBa;->a:LAZ;

    iget-object v2, p0, LBa;->a:LapF;

    invoke-virtual {v0, p3, v2}, LAZ;->a(Landroid/view/ViewGroup;LapF;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 107
    :pswitch_3
    iget-object v0, p0, LBa;->a:Landroid/view/LayoutInflater;

    iget-object v2, p0, LBa;->a:LCn;

    invoke-static {v0, p3, v2}, LapJ;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LCn;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 110
    :pswitch_4
    sget-object v0, LapJ;->i:LapJ;

    iget-object v2, p0, LBa;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v2, p3}, LapJ;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 113
    :pswitch_5
    sget-object v0, LapJ;->h:LapJ;

    iget-object v2, p0, LBa;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v2, p3}, LapJ;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 126
    :cond_1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 127
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 128
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 130
    :goto_3
    if-ge v1, v3, :cond_2

    .line 131
    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 132
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int/2addr v2, v4

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 140
    :cond_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    goto :goto_2

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
