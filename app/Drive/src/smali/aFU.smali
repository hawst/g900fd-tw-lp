.class public final LaFU;
.super LaGm;
.source "CachedSearchResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEq;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(LaEz;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 34
    invoke-static {}, LaEq;->a()LaEq;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 35
    iput-wide p2, p0, LaFU;->a:J

    .line 36
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaFU;->a:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 41
    sget-object v0, LaEr;->b:LaEr;

    invoke-virtual {v0}, LaEr;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaFU;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    sget-object v0, LaEr;->a:LaEr;

    invoke-virtual {v0}, LaEr;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaFU;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 43
    return-void
.end method
