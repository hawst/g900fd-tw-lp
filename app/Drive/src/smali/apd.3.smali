.class public Lapd;
.super Lanq;
.source "ThumbnailFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanq",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Laon;",
        "Lcom/google/android/apps/docs/utils/RawPixelData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LanN;

.field private final a:LbjF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjF",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LaGx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LalD",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LbjG;Laoo;LanN;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbjG",
            "<",
            "Laon;",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;>;",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;>;",
            "LanN;",
            "Ljava/util/List",
            "<",
            "LalD",
            "<*>;>;",
            "Ljava/util/List",
            "<",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;>;)V"
        }
    .end annotation

    .prologue
    .line 300
    invoke-direct {p0, p1, p2}, Lanq;-><init>(LbjG;Laoo;)V

    .line 301
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lapd;->a:LbmF;

    .line 302
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lapd;->b:LbmF;

    .line 303
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LanN;

    iput-object v0, p0, Lapd;->a:LanN;

    .line 305
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, LbjG;->a(J)LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->a()LbjF;

    move-result-object v0

    iput-object v0, p0, Lapd;->a:LbjF;

    .line 306
    return-void
.end method

.method synthetic constructor <init>(LbjG;Laoo;LanN;Ljava/util/List;Ljava/util/List;Lape;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct/range {p0 .. p5}, Lapd;-><init>(LbjG;Laoo;LanN;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lapd;)LbjF;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lapd;->a:LbjF;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LaGx;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lapd;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGx;

    .line 317
    if-nez v0, :cond_0

    sget-object v0, LaGx;->a:LaGx;

    :cond_0
    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LakD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    invoke-virtual {p0, p1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;

    move-result-object v0

    .line 344
    invoke-virtual {p0, v0}, Lapd;->a(Ljava/lang/Object;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;
    .locals 1

    .prologue
    .line 420
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Laon;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 310
    invoke-super {p0, p1}, Lanq;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 311
    new-instance v1, Laph;

    invoke-direct {v1, p0, p1}, Laph;-><init>(Lapd;Lcom/google/android/apps/docs/utils/FetchSpec;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbsJ;)V

    .line 312
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)LbsU;
    .locals 1

    .prologue
    .line 84
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lapd;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalD;

    .line 414
    invoke-interface {v0}, LalD;->b()V

    goto :goto_0

    .line 416
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lapd;->b:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakH;

    .line 322
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LakH;->a(Ljava/lang/Comparable;)V

    goto :goto_0

    .line 324
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)Z
    .locals 1

    .prologue
    .line 335
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    iget-object v0, p0, Lapd;->a:LanN;

    invoke-virtual {v0, p1}, LanN;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    iget-object v0, p0, Lapd;->a:LanN;

    invoke-virtual {v0, p1}, LanN;->b(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 330
    new-instance v1, Laph;

    invoke-direct {v1, p0, p1}, Laph;-><init>(Lapd;Lcom/google/android/apps/docs/utils/FetchSpec;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbsJ;)V

    .line 331
    return-object v0
.end method

.method protected synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;

    move-result-object v0

    return-object v0
.end method
