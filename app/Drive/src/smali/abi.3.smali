.class public final enum Labi;
.super Ljava/lang/Enum;
.source "AddCollaboratorLoaderDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Labi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Labi;

.field private static final synthetic a:[Labi;

.field public static final enum b:Labi;

.field public static final enum c:Labi;

.field public static final enum d:Labi;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Labi;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, Labi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labi;->a:Labi;

    .line 38
    new-instance v0, Labi;

    const-string v1, "LOADING_STARTED"

    invoke-direct {v0, v1, v3}, Labi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labi;->b:Labi;

    .line 39
    new-instance v0, Labi;

    const-string v1, "ADD_COLLABORATOR_DIALOG_SHOWN"

    invoke-direct {v0, v1, v4}, Labi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labi;->c:Labi;

    .line 40
    new-instance v0, Labi;

    const-string v1, "ADD_COLLABORATOR_DIALOG_DISMISSED"

    invoke-direct {v0, v1, v5}, Labi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labi;->d:Labi;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Labi;

    sget-object v1, Labi;->a:Labi;

    aput-object v1, v0, v2

    sget-object v1, Labi;->b:Labi;

    aput-object v1, v0, v3

    sget-object v1, Labi;->c:Labi;

    aput-object v1, v0, v4

    sget-object v1, Labi;->d:Labi;

    aput-object v1, v0, v5

    sput-object v0, Labi;->a:[Labi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Labi;
    .locals 1

    .prologue
    .line 36
    const-class v0, Labi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Labi;

    return-object v0
.end method

.method public static values()[Labi;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Labi;->a:[Labi;

    invoke-virtual {v0}, [Labi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Labi;

    return-object v0
.end method
