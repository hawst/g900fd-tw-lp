.class public LaIq;
.super Ljava/lang/Object;
.source "StorageFileCreator.java"


# instance fields
.field private final a:LNc;

.field private final a:LPw;

.field private final a:LaGM;

.field private final a:LaIF;

.field private final a:LahB;


# direct methods
.method public constructor <init>(LNc;LahB;LaIF;LaGM;LPw;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNc;

    iput-object v0, p0, LaIq;->a:LNc;

    .line 48
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahB;

    iput-object v0, p0, LaIq;->a:LahB;

    .line 49
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIF;

    iput-object v0, p0, LaIq;->a:LaIF;

    .line 50
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LaIq;->a:LaGM;

    .line 51
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPw;

    iput-object v0, p0, LaIq;->a:LPw;

    .line 52
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFM;Ljava/lang/String;)LaIA;
    .locals 4

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    if-eqz p1, :cond_0

    .line 61
    :try_start_0
    iget-object v0, p0, LaIq;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 64
    :cond_0
    iget-object v1, p0, LaIq;->a:LPw;

    .line 65
    invoke-virtual {p2}, LaFM;->a()LaFO;

    move-result-object v2

    sget-object v3, LaGv;->a:LaGv;

    .line 64
    invoke-virtual {v1, v2, p3, v3, v0}, LPw;->a(LaFO;Ljava/lang/String;LaGv;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 66
    invoke-static {v0}, LaIA;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;

    move-result-object v0

    .line 67
    iget-object v1, p0, LaIq;->a:LaIF;

    invoke-virtual {p2}, LaFM;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, LaIF;->a(JLjava/lang/String;)LaIA;
    :try_end_0
    .catch LPx; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    const-string v1, "StorageFileCreator"

    const-string v2, "failed to create folder"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 70
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, LPx;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private b(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFM;Ljava/lang/String;Ljava/lang/String;)LaIA;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, LaIq;->a:LNc;

    invoke-virtual {v0}, LNc;->a()LNb;

    move-result-object v0

    .line 78
    invoke-virtual {p2}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LNb;->a(LaFO;)LNb;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p3}, LNb;->a(Ljava/lang/String;)LNb;

    move-result-object v0

    .line 80
    invoke-virtual {v0, p1}, LNb;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;

    move-result-object v0

    .line 81
    invoke-virtual {v0, v4}, LNb;->a(Z)LNb;

    move-result-object v0

    .line 82
    invoke-virtual {v0, p4}, LNb;->c(Ljava/lang/String;)LNb;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, LNb;->a()LMZ;

    move-result-object v0

    .line 86
    :try_start_0
    iget-object v1, p0, LaIq;->a:LahB;

    invoke-interface {v1, v0}, LahB;->a(LMZ;)LaGb;

    move-result-object v0

    .line 87
    invoke-interface {v0}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 88
    invoke-static {v0}, LaIA;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v1, p0, LaIq;->a:LaIF;

    invoke-virtual {p2}, LaFM;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, LaIF;->a(JLjava/lang/String;)LaIA;
    :try_end_0
    .catch LNh; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    const-string v1, "StorageFileCreator"

    const-string v2, "failed to create file"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 92
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "Failed to upload new file \'%s\': %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    .line 93
    invoke-virtual {v0}, LNh;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    .line 92
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFM;Ljava/lang/String;Ljava/lang/String;)LaIA;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 103
    .line 105
    :try_start_0
    iget-object v0, p0, LaIq;->a:LaGM;

    invoke-interface {v0, p2}, LaGM;->a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 110
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    iget-object v0, p0, LaIq;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 112
    if-nez v0, :cond_0

    .line 113
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Folder no longer exists when creating file %s with mimeType=\'%s\'"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v4

    aput-object p4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "Error getting root folder entry spec: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 117
    :cond_0
    invoke-interface {v0}, LaGu;->h()Z

    move-result v1

    if-nez v1, :cond_1

    .line 118
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "Cannot create a file in the readonly folder \'%s\'"

    new-array v3, v3, [Ljava/lang/Object;

    .line 119
    invoke-interface {v0}, LaGu;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 118
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 123
    :cond_1
    const-string v0, "vnd.android.document/directory"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    invoke-direct {p0, p1, p2, p3}, LaIq;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFM;Ljava/lang/String;)LaIA;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, LaIq;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFM;Ljava/lang/String;Ljava/lang/String;)LaIA;

    move-result-object v0

    goto :goto_0
.end method
