.class public LbgO;
.super LbgI;
.source "PdfTrailer.java"


# static fields
.field private static a:J


# instance fields
.field private final a:LbgE;

.field private final a:LbgH;

.field private final a:LbgQ;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, LbgO;->a:J

    return-void
.end method

.method public constructor <init>(LbgE;LbgH;LbgQ;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, LbgI;-><init>()V

    .line 33
    iput-object p1, p0, LbgO;->a:LbgE;

    .line 34
    iput-object p2, p0, LbgO;->a:LbgH;

    .line 35
    iput-object p3, p0, LbgO;->a:LbgQ;

    .line 36
    return-void
.end method

.method private static a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-static {}, LbgO;->a()[B

    move-result-object v2

    .line 72
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v1

    .line 73
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 74
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aget-byte v7, v2, v0

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a()[B
    .locals 6

    .prologue
    .line 88
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 92
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, LbgO;->a:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    sput-wide v4, LbgO;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/io/OutputStream;)I
    .locals 7

    .prologue
    .line 47
    invoke-static {}, LbgO;->a()Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-static {}, LbgO;->a()Ljava/lang/String;

    move-result-object v1

    .line 49
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "<</Root %s/Info %s/Size %d/ID [<%s><%s>]>>\n"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LbgO;->a:LbgE;

    .line 50
    invoke-virtual {v6}, LbgE;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, LbgO;->a:LbgH;

    invoke-virtual {v6}, LbgH;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, LbgO;->a:LbgQ;

    invoke-virtual {v6}, LbgQ;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v0, v4, v5

    const/4 v0, 0x4

    aput-object v1, v4, v0

    .line 49
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-static {v0}, LbgO;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 52
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 53
    array-length v0, v0

    return v0
.end method

.method protected b(Ljava/io/OutputStream;)I
    .locals 1

    .prologue
    .line 40
    const-string v0, "trailer\n"

    invoke-static {v0}, LbgO;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 41
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 42
    array-length v0, v0

    return v0
.end method

.method protected c(Ljava/io/OutputStream;)I
    .locals 6

    .prologue
    .line 58
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "startxref\n%d\n%%%%EOF\n"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LbgO;->a:LbgQ;

    invoke-virtual {v4}, LbgQ;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-static {v0}, LbgO;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 60
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 61
    array-length v0, v0

    return v0
.end method
