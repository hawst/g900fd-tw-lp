.class public Lty;
.super Lvr;
.source "DriveShortcutDefinition.java"


# static fields
.field public static final a:Lvr;

.field public static final b:Lvr;

.field public static final c:Lvr;

.field public static final d:Lvr;

.field public static final e:Lvr;

.field public static final f:Lvr;

.field public static final g:Lvr;

.field public static final h:Lvr;

.field public static final i:Lvr;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 16
    new-instance v0, Lty;

    sget-object v1, LCe;->q:LCe;

    sget-object v2, LCe;->q:LCe;

    .line 18
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_my_drive:I

    sget-object v4, Lrx;->a:Lrx;

    const-string v5, "myDrive"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->a:Lvr;

    .line 23
    new-instance v0, Lty;

    sget-object v1, LCe;->o:LCe;

    sget-object v2, LCe;->o:LCe;

    .line 25
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_shared_with_me:I

    sget-object v4, Lrx;->a:Lrx;

    const-string v5, "sharedWithMe"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->b:Lvr;

    .line 30
    new-instance v0, Lty;

    sget-object v1, LCe;->b:LCe;

    sget-object v2, LCe;->b:LCe;

    .line 32
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_starred:I

    sget-object v4, Lrx;->a:Lrx;

    const-string v5, "starred"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->c:Lvr;

    .line 37
    new-instance v0, Lty;

    sget-object v1, LCe;->p:LCe;

    sget-object v2, LCe;->p:LCe;

    .line 39
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_recently_opened:I

    sget-object v4, Lrx;->a:Lrx;

    const-string v5, "recentlyOpened"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->d:Lvr;

    .line 44
    new-instance v0, Lty;

    sget-object v1, LCe;->c:LCe;

    sget-object v2, LCe;->c:LCe;

    .line 46
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_offline:I

    sget-object v4, Lry;->af:Lry;

    .line 48
    invoke-virtual {v4}, Lry;->a()Lrx;

    move-result-object v4

    const-string v5, "pinned"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->e:Lvr;

    .line 51
    new-instance v0, Lty;

    sget-object v1, LCe;->d:LCe;

    sget-object v2, LCe;->d:LCe;

    .line 53
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_upload:I

    sget-object v4, Lrx;->a:Lrx;

    const-string v5, "browseUpload"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->f:Lvr;

    .line 58
    new-instance v0, Lty;

    sget-object v1, LCe;->a:LCe;

    sget v2, Lxi;->navigation_all_items:I

    sget v3, Lxb;->ic_drive_all_items:I

    sget-object v4, Lrx;->a:Lrx;

    const-string v5, "allItems"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->g:Lvr;

    .line 65
    new-instance v0, Lty;

    sget-object v1, LCe;->n:LCe;

    sget-object v2, LCe;->n:LCe;

    .line 67
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_owned_by_me:I

    sget-object v4, Lrx;->a:Lrx;

    const-string v5, "ownedByMe"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->h:Lvr;

    .line 72
    new-instance v0, Lty;

    sget-object v1, LCe;->m:LCe;

    sget-object v2, LCe;->m:LCe;

    .line 74
    invoke-virtual {v2}, LCe;->a()I

    move-result v2

    sget v3, Lxb;->ic_drive_trash:I

    sget-object v4, Lry;->aI:Lry;

    .line 76
    invoke-virtual {v4}, Lry;->a()Lrx;

    move-result-object v4

    const-string v5, "trash"

    invoke-direct/range {v0 .. v5}, Lty;-><init>(LCl;IILrx;Ljava/lang/String;)V

    sput-object v0, Lty;->i:Lvr;

    .line 72
    return-void
.end method

.method private constructor <init>(LCl;IILrx;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct/range {p0 .. p5}, Lvr;-><init>(LCl;IILrx;Ljava/lang/String;)V

    .line 89
    return-void
.end method
