.class Lbgp;
.super Ljava/lang/Object;
.source "CaptureActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:I

.field final synthetic a:Lbgn;


# direct methods
.method constructor <init>(Lbgn;I)V
    .locals 0

    .prologue
    .line 714
    iput-object p1, p0, Lbgp;->a:Lbgn;

    iput p2, p0, Lbgp;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 717
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 735
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 719
    :pswitch_0
    iget-object v0, p0, Lbgp;->a:Lbgn;

    iget v1, p0, Lbgp;->a:I

    invoke-static {v0, v1}, Lbgn;->a(Lbgn;I)V

    goto :goto_0

    .line 723
    :pswitch_1
    iget-object v0, p0, Lbgp;->a:Lbgn;

    iget v1, p0, Lbgp;->a:I

    invoke-static {v0, v1}, Lbgn;->b(Lbgn;I)V

    goto :goto_0

    .line 728
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-gez v0, :cond_1

    .line 729
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 731
    :cond_1
    iget-object v0, p0, Lbgp;->a:Lbgn;

    iget v1, p0, Lbgp;->a:I

    invoke-static {v0, v1}, Lbgn;->c(Lbgn;I)V

    goto :goto_0

    .line 717
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
