.class public LSe;
.super Landroid/content/ContextWrapper;
.source "WebViewFragment.java"

# interfaces
.implements Laju;


# instance fields
.field private final a:Laju;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/ServiceConnection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LSe;->a:Ljava/util/Set;

    .line 55
    check-cast p1, Laju;

    iput-object p1, p0, LSe;->a:Laju;

    .line 56
    return-void
.end method


# virtual methods
.method public a()Lbuu;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, LSe;->a:Laju;

    invoke-interface {v0}, Laju;->a()Lbuu;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, LSe;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ServiceConnection;

    .line 80
    invoke-super {p0, v0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, LSe;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 83
    return-void
.end method

.method public bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    iget-object v1, p0, LSe;->a:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    return v0
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, LSe;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    const-string v0, "ServiceConnectionContextWrapper"

    const-string v1, "Tried to unbind a service connection that has already been unbound: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, LSe;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 75
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method
