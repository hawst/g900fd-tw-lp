.class LWh;
.super Ljava/lang/Object;
.source "CommonPreferencesInstaller.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:LWf;

.field final synthetic a:Landroid/content/Context;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(LWf;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, LWh;->a:LWf;

    iput-object p2, p0, LWh;->a:Landroid/content/Context;

    iput-object p3, p0, LWh;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 270
    iget-object v0, p0, LWh;->a:Landroid/content/Context;

    sget v1, Lxi;->policy_privacy_local_url_format_string:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LWh;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 271
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 272
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 273
    iget-object v0, p0, LWh;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 274
    return-void
.end method
