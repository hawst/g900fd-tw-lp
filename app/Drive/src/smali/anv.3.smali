.class public abstract Lanv;
.super Ljava/lang/Object;
.source "AbstractStoringFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "C:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoo",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Laoo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoo",
            "<TK;+TV;>;"
        }
    .end annotation
.end field

.field private final a:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final a:LbsW;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TC;",
            "Lanv",
            "<TK;TC;TV;>.anz;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Laoo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 296
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lanv;-><init>(Laoo;Ljava/util/concurrent/ExecutorService;)V

    .line 297
    return-void
.end method

.method protected constructor <init>(Laoo;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<TK;+TV;>;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    new-instance v0, Lanw;

    invoke-direct {v0, p0}, Lanw;-><init>(Lanv;)V

    iput-object v0, p0, Lanv;->a:Laoy;

    .line 272
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lanv;->a:Ljava/util/Map;

    .line 284
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoo;

    iput-object v0, p0, Lanv;->a:Laoo;

    .line 285
    invoke-static {p2}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v0

    iput-object v0, p0, Lanv;->a:LbsW;

    .line 286
    return-void
.end method

.method static synthetic a(Lanv;Ljava/lang/Object;)Lanz;
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lanv;->a(Ljava/lang/Object;)Lanz;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/Object;)Lanz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)",
            "Lanv",
            "<TK;TC;TV;>.anz;"
        }
    .end annotation

    .prologue
    .line 383
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lanv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanz;

    .line 384
    if-nez v0, :cond_0

    .line 385
    new-instance v0, Lanz;

    invoke-direct {v0, p0, p1}, Lanz;-><init>(Lanv;Ljava/lang/Object;)V

    .line 386
    iget-object v1, p0, Lanv;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    :cond_0
    monitor-exit p0

    return-object v0

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lanv;)Laoo;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lanv;->a:Laoo;

    return-object v0
.end method

.method static synthetic a(Lanv;)Laoy;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lanv;->a:Laoy;

    return-object v0
.end method

.method static synthetic a(Lanv;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lanv;->b(Ljava/lang/Object;)V

    return-void
.end method

.method private declared-synchronized b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)V"
        }
    .end annotation

    .prologue
    .line 392
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lanv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393
    monitor-exit p0

    return-void

    .line 392
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;Ljava/lang/Object;I)LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;TV;I)",
            "LbmF",
            "<TV;>;"
        }
    .end annotation
.end method

.method public a(Ljava/lang/Object;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 316
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    invoke-virtual {p0, p1}, Lanv;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 318
    invoke-virtual {p0, v0}, Lanv;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 319
    if-nez v1, :cond_0

    .line 320
    iget-object v1, p0, Lanv;->a:Laoy;

    .line 321
    invoke-static {v1}, Laou;->a(Laoy;)Laou;

    move-result-object v1

    .line 323
    new-instance v2, Lanx;

    invoke-direct {v2, p0, v0, p1, v1}, Lanx;-><init>(Lanv;Ljava/lang/Object;Ljava/lang/Object;Laou;)V

    .line 340
    iget-object v0, p0, Lanv;->a:LbsW;

    .line 341
    invoke-interface {v0, v2}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    invoke-static {v0}, Lalv;->a(LbsU;)LbsU;

    move-result-object v0

    .line 342
    invoke-virtual {v1, v0}, Laou;->b(LbsU;)V

    .line 345
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)TV;"
        }
    .end annotation
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 428
    return-void
.end method

.method protected abstract a(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)Z"
        }
    .end annotation
.end method

.method public b(Ljava/lang/Object;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 355
    invoke-virtual {p0, p1}, Lanv;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 357
    invoke-virtual {p0, v0}, Lanv;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 358
    new-instance v1, Lany;

    invoke-direct {v1, p0, v0, p1}, Lany;-><init>(Lanv;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 370
    iget-object v0, p0, Lanv;->a:LbsW;

    .line 371
    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    invoke-static {v0}, Lalv;->a(LbsU;)LbsU;

    move-result-object v0

    .line 374
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract b(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TC;"
        }
    .end annotation
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    .prologue
    .line 304
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    invoke-virtual {p0, p1}, Lanv;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 306
    invoke-virtual {p0, v0}, Lanv;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
