.class public final enum Lbho;
.super Ljava/lang/Enum;
.source "DocumentEditorView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbho;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbho;

.field private static final synthetic a:[Lbho;

.field public static final enum b:Lbho;

.field public static final enum c:Lbho;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 124
    new-instance v0, Lbho;

    const-string v1, "ROTATE"

    invoke-direct {v0, v1, v2}, Lbho;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbho;->a:Lbho;

    .line 125
    new-instance v0, Lbho;

    const-string v1, "DRAG"

    invoke-direct {v0, v1, v3}, Lbho;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbho;->b:Lbho;

    .line 126
    new-instance v0, Lbho;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lbho;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbho;->c:Lbho;

    .line 123
    const/4 v0, 0x3

    new-array v0, v0, [Lbho;

    sget-object v1, Lbho;->a:Lbho;

    aput-object v1, v0, v2

    sget-object v1, Lbho;->b:Lbho;

    aput-object v1, v0, v3

    sget-object v1, Lbho;->c:Lbho;

    aput-object v1, v0, v4

    sput-object v0, Lbho;->a:[Lbho;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbho;
    .locals 1

    .prologue
    .line 123
    const-class v0, Lbho;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbho;

    return-object v0
.end method

.method public static values()[Lbho;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lbho;->a:[Lbho;

    invoke-virtual {v0}, [Lbho;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbho;

    return-object v0
.end method
