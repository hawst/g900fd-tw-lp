.class final enum LKA;
.super Ljava/lang/Enum;
.source "DropToThisFolderListener.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LKA;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LKA;

.field private static final synthetic a:[LKA;

.field public static final enum b:LKA;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, LKA;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v2}, LKA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKA;->a:LKA;

    .line 38
    new-instance v0, LKA;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v3}, LKA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKA;->b:LKA;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [LKA;

    sget-object v1, LKA;->a:LKA;

    aput-object v1, v0, v2

    sget-object v1, LKA;->b:LKA;

    aput-object v1, v0, v3

    sput-object v0, LKA;->a:[LKA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LKA;
    .locals 1

    .prologue
    .line 36
    const-class v0, LKA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LKA;

    return-object v0
.end method

.method public static values()[LKA;
    .locals 1

    .prologue
    .line 36
    sget-object v0, LKA;->a:[LKA;

    invoke-virtual {v0}, [LKA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LKA;

    return-object v0
.end method
