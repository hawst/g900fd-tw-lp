.class LaeW;
.super Landroid/os/AsyncTask;
.source "DatabaseSyncMoreController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Laff;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaFM;

.field final synthetic a:LaeV;

.field final synthetic a:Lafy;

.field final synthetic a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

.field final synthetic a:Z


# direct methods
.method constructor <init>(LaeV;LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;ZLafy;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, LaeW;->a:LaeV;

    iput-object p2, p0, LaeW;->a:LaFM;

    iput-object p3, p0, LaeW;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iput-boolean p4, p0, LaeW;->a:Z

    iput-object p5, p0, LaeW;->a:Lafy;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Laff;
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, LaeW;->a:LaeV;

    invoke-static {v0}, LaeV;->a(LaeV;)Lafz;

    move-result-object v0

    iget-object v1, p0, LaeW;->a:LaFM;

    iget-object v2, p0, LaeW;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lafz;->a(LaFM;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Laff;

    move-result-object v0

    return-object v0
.end method

.method protected a(Laff;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 152
    iget-object v0, p0, LaeW;->a:LaeV;

    invoke-static {v0, p1}, LaeV;->a(LaeV;Laff;)Laff;

    .line 155
    iget-boolean v0, p0, LaeW;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LaeW;->a:LaeV;

    invoke-static {v0}, LaeV;->a(LaeV;)Laff;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaeW;->a:LaeV;

    .line 156
    invoke-static {v0}, LaeV;->a(LaeV;)Laff;

    move-result-object v0

    invoke-interface {v0}, Laff;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LaeW;->a:LaeV;

    invoke-static {v0}, LaeV;->a(LaeV;)Laff;

    move-result-object v0

    invoke-interface {v0}, Laff;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LaeW;->a:LaeV;

    .line 157
    invoke-static {v0}, LaeV;->a(LaeV;)LQr;

    move-result-object v0

    const-string v1, "enableSyncMoreImplicitely"

    invoke-interface {v0, v1, v3}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v3

    .line 158
    :goto_0
    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, LaeW;->a:LaeV;

    invoke-static {v0}, LaeV;->a(LaeV;)LQr;

    move-result-object v0

    const-string v1, "syncMoreMaxFeedsToRetrieveImplicitly_r2"

    invoke-interface {v0, v1, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v7

    .line 162
    new-instance v0, LaeX;

    iget-object v1, p0, LaeW;->a:LaeV;

    iget-object v2, p0, LaeW;->a:LaeV;

    .line 163
    invoke-static {v2}, LaeV;->a(LaeV;)Laja;

    move-result-object v2

    invoke-virtual {v2}, Laja;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iget-object v4, p0, LaeW;->a:LaFM;

    iget-object v5, p0, LaeW;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v6, p0, LaeW;->a:Lafy;

    invoke-direct/range {v0 .. v6}, LaeX;-><init>(LaeV;Landroid/content/Context;ZLaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;)V

    .line 164
    iget-object v1, p0, LaeW;->a:LaeV;

    invoke-static {v1}, LaeV;->a(LaeV;)Laff;

    move-result-object v1

    invoke-interface {v1, v0, v7}, Laff;->a(Lafg;I)V

    .line 167
    :cond_0
    iget-object v0, p0, LaeW;->a:LaeV;

    iget-object v1, p0, LaeW;->a:Lafy;

    invoke-static {v0, v1}, LaeV;->a(LaeV;Lafy;)V

    .line 168
    return-void

    .line 157
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 144
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaeW;->a([Ljava/lang/Void;)Laff;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 144
    check-cast p1, Laff;

    invoke-virtual {p0, p1}, LaeW;->a(Laff;)V

    return-void
.end method
