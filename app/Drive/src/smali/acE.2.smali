.class public LacE;
.super Ljava/lang/Object;
.source "SharingModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 75
    const-class v0, Laby;

    .line 76
    invoke-static {p1, v0}, LbvT;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvT;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, LbvT;->a()LbuT;

    move-result-object v0

    const-class v1, Labt;

    invoke-interface {v0, v1}, LbuT;->a(Ljava/lang/Class;)LbuU;

    .line 79
    return-void
.end method

.method provideAclManager(Lace;)Labe;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 28
    return-object p1
.end method

.method provideAddCollaboratorClient(Lacj;)Labg;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 52
    return-object p1
.end method

.method provideContactManager(LabG;)LabF;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 34
    return-object p1
.end method

.method provideContactSharingConfirmationListener(Lacj;)LQT;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 58
    return-object p1
.end method

.method provideSharingInfo(Landroid/content/Context;)Lacr;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 64
    const-class v0, Lacr;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacr;

    return-object v0
.end method

.method provideSharingInfoManager(Lact;)Lacs;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 40
    return-object p1
.end method

.method provideSharingInfoProvider(Lacj;)Lacz;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 46
    return-object p1
.end method
