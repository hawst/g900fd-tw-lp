.class LKt;
.super Ljava/lang/Object;
.source "DropToThisFolderListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LKs;


# direct methods
.method constructor <init>(LKs;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, LKt;->a:LKs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 46
    sget-object v0, LKA;->a:LKA;

    iget-object v1, p0, LKt;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)LKA;

    move-result-object v1

    invoke-virtual {v0, v1}, LKA;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, LKt;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)LbsU;

    move-result-object v0

    .line 48
    invoke-interface {v0}, LbsU;->isDone()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lalv;->a(Ljava/util/concurrent/Future;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50
    if-eqz v0, :cond_0

    .line 51
    iget-object v1, p0, LKt;->a:LKs;

    sget-object v2, LKA;->b:LKA;

    invoke-static {v1, v2}, LKs;->a(LKs;LKA;)LKA;

    .line 52
    iget-object v1, p0, LKt;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)LKB;

    move-result-object v1

    invoke-virtual {v1, v0}, LKB;->a(Ljava/lang/String;)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    invoke-interface {v0}, LbsU;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    iget-object v0, p0, LKt;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LKt;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
