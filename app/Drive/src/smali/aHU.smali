.class public LaHU;
.super Ljava/lang/Object;
.source "DocumentCursorRowConverter.java"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final a:LaGA;

.field private final a:LaGM;

.field private final a:LaHV;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LaES;->a:LaES;

    .line 25
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LaES;->r:LaES;

    .line 26
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LaES;->e:LaES;

    .line 27
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaES;->h:LaES;

    .line 28
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaES;->E:LaES;

    .line 29
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaES;->t:LaES;

    .line 30
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaES;->u:LaES;

    .line 31
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaEL;->l:LaEL;

    .line 32
    invoke-virtual {v2}, LaEL;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LaHU;->a:[Ljava/lang/String;

    .line 23
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;LaGA;LaGM;Z)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v1, LaHV;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v0}, LaHV;-><init>([Ljava/lang/String;)V

    iput-object v1, p0, LaHU;->a:LaHV;

    .line 52
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGA;

    iput-object v0, p0, LaHU;->a:LaGA;

    .line 53
    iput-object p3, p0, LaHU;->a:LaGM;

    .line 54
    iput-boolean p4, p0, LaHU;->a:Z

    .line 55
    return-void
.end method

.method public static a(LaGA;[Ljava/lang/String;LaGM;Z)LaHY;
    .locals 3

    .prologue
    .line 91
    new-instance v0, LaHU;

    invoke-direct {v0, p1, p0, p2, p3}, LaHU;-><init>([Ljava/lang/String;LaGA;LaGM;Z)V

    .line 93
    new-instance v1, LaHY;

    invoke-direct {v1, p1}, LaHY;-><init>([Ljava/lang/String;)V

    .line 95
    :goto_0
    invoke-interface {p0}, LaGA;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    invoke-virtual {v0}, LaHU;->a()[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LaHY;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    :cond_0
    return-object v1
.end method

.method public static final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, LaHU;->a:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()[Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 67
    iget-object v0, p0, LaHU;->a:LaGM;

    iget-object v1, p0, LaHU;->a:LaGA;

    invoke-interface {v0, v1}, LaGM;->a(LaGA;)Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object v0, p0, LaHU;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()Ljava/lang/Long;

    move-result-object v5

    .line 69
    iget-object v0, p0, LaHU;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()LaGv;

    move-result-object v3

    .line 70
    invoke-virtual {v3}, LaGv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LaHU;->a:Z

    if-nez v0, :cond_0

    move-object v4, v7

    .line 73
    :goto_0
    iget-object v0, p0, LaHU;->a:LaHV;

    iget-object v2, p0, LaHU;->a:LaGA;

    .line 75
    invoke-interface {v2}, LaGA;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, LaHU;->a:LaGA;

    .line 79
    invoke-interface {v6}, LaGA;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v8, p0, LaHU;->a:LaGA;

    .line 81
    invoke-interface {v8}, LaGA;->h()Z

    move-result v8

    invoke-static {v8}, LaHW;->a(Z)LaHW;

    move-result-object v8

    .line 73
    invoke-virtual/range {v0 .. v8}, LaHV;->a(Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 70
    :cond_0
    iget-object v0, p0, LaHU;->a:LaGA;

    .line 71
    invoke-interface {v0}, LaGA;->e()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
