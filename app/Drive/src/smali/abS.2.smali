.class LabS;
.super Ljava/lang/Object;
.source "GlobalSharingApi.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGu;

.field final synthetic a:LabQ;

.field final synthetic a:LabU;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Lqt;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(LabQ;Lqt;Ljava/lang/String;LaGu;Ljava/lang/String;LabU;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, LabS;->a:LabQ;

    iput-object p2, p0, LabS;->a:Lqt;

    iput-object p3, p0, LabS;->a:Ljava/lang/String;

    iput-object p4, p0, LabS;->a:LaGu;

    iput-object p5, p0, LabS;->b:Ljava/lang/String;

    iput-object p6, p0, LabS;->a:LabU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Lcom/google/api/services/drive/model/Permission;

    invoke-direct {v0}, Lcom/google/api/services/drive/model/Permission;-><init>()V

    .line 144
    iget-object v1, p0, LabS;->a:LabQ;

    iget-object v2, p0, LabS;->a:Lqt;

    invoke-static {v1, v0, v2}, LabQ;->a(LabQ;Lcom/google/api/services/drive/model/Permission;Lqt;)V

    .line 145
    iget-object v1, p0, LabS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/model/Permission;->c(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 146
    const-string v1, "user"

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/model/Permission;->b(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 147
    iget-object v1, p0, LabS;->a:LabQ;

    .line 148
    invoke-static {v1}, LabQ;->a(LabQ;)Lcom/google/api/services/drive/Drive;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$Permissions;

    move-result-object v1

    iget-object v2, p0, LabS;->a:LaGu;

    invoke-interface {v2}, LaGu;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/api/services/drive/Drive$Permissions;->a(Ljava/lang/String;Lcom/google/api/services/drive/model/Permission;)Lcom/google/api/services/drive/Drive$Permissions$Insert;

    move-result-object v0

    .line 149
    iget-object v1, p0, LabS;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, LabS;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/Drive$Permissions$Insert;->b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Permissions$Insert;

    .line 152
    :cond_0
    iget-object v1, p0, LabS;->a:LabU;

    invoke-virtual {v1, v0}, LabU;->a(Lcom/google/api/services/drive/DriveRequest;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Permission;

    .line 153
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, LabS;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
