.class abstract LbmX;
.super LbmY;
.source "ImmutableMultiset.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LbmY",
        "<",
        "Lbpj",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbmV;


# direct methods
.method constructor <init>(LbmV;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, LbmX;->a:LbmV;

    invoke-direct {p0}, LbmY;-><init>()V

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, LbmX;->a:LbmV;

    invoke-virtual {v0}, LbmV;->a()Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 358
    instance-of v1, p1, Lbpj;

    if-eqz v1, :cond_0

    .line 359
    check-cast p1, Lbpj;

    .line 360
    invoke-interface {p1}, Lbpj;->a()I

    move-result v1

    if-gtz v1, :cond_1

    .line 366
    :cond_0
    :goto_0
    return v0

    .line 363
    :cond_1
    iget-object v1, p0, LbmX;->a:LbmV;

    invoke-interface {p1}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LbmV;->a(Ljava/lang/Object;)I

    move-result v1

    .line 364
    invoke-interface {p1}, Lbpj;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, LbmX;->a:LbmV;

    invoke-virtual {v0}, LbmV;->hashCode()I

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 376
    invoke-virtual {p0}, LbmX;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    .line 377
    invoke-virtual {p0, v0}, LbmX;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p0}, LbmX;->size()I

    move-result v0

    .line 388
    array-length v1, p1

    if-ge v1, v0, :cond_1

    .line 389
    invoke-static {p1, v0}, Lbpv;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 396
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 397
    invoke-virtual {p0}, LbmX;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 398
    add-int/lit8 v2, v1, 0x1

    aput-object v0, p1, v1

    move v1, v2

    .line 399
    goto :goto_1

    .line 390
    :cond_1
    array-length v1, p1

    if-le v1, v0, :cond_0

    .line 391
    const/4 v1, 0x0

    aput-object v1, p1, v0

    goto :goto_0

    .line 400
    :cond_2
    return-object p1
.end method
