.class LLH;
.super Ljava/lang/Object;
.source "SelectionFunctionDoRemove.java"

# interfaces
.implements LLw;


# instance fields
.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method public constructor <init>(Laja;LtK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "LtK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, LLH;->a:Laja;

    .line 31
    iput-object p2, p0, LLH;->a:LtK;

    .line 32
    return-void
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    return-void
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, LLH;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 56
    invoke-static {v0, p4}, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a(Landroid/content/Context;LbmF;)Landroid/content/Intent;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 58
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 59
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 37
    iget-object v0, p0, LLH;->a:LtK;

    sget-object v1, Lry;->V:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :goto_0
    return v3

    .line 42
    :cond_0
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 43
    invoke-interface {v0}, LaGu;->e()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_2
    and-int/2addr v0, v1

    move v1, v0

    .line 44
    goto :goto_1

    :cond_1
    move v0, v3

    .line 43
    goto :goto_2

    .line 45
    :cond_2
    invoke-virtual {p1}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    :goto_3
    move v3, v2

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_3
.end method
