.class public Lbgg;
.super LH;
.source "BaseActivity.java"


# instance fields
.field private a:I

.field private a:Lbgj;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, LH;-><init>()V

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lbgg;->a:I

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lbgg;->b:I

    .line 67
    return-void
.end method

.method static synthetic a(Lbgg;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lbgg;->a:I

    return v0
.end method

.method static synthetic a(Lbgg;I)I
    .locals 0

    .prologue
    .line 16
    iput p1, p0, Lbgg;->a:I

    return p1
.end method

.method static synthetic b(Lbgg;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lbgg;->b:I

    return v0
.end method

.method static synthetic b(Lbgg;I)I
    .locals 0

    .prologue
    .line 16
    iput p1, p0, Lbgg;->b:I

    return p1
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lbgg;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lbgg;->a:I

    goto :goto_0
.end method

.method public a(Lbgi;)V
    .locals 4

    .prologue
    .line 105
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 106
    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_title_cancel_scan:I

    .line 107
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_msg_cancel_scan:I

    .line 108
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_ok_button_text:I

    new-instance v3, Lbgh;

    invoke-direct {v3, p0, p1}, Lbgh;-><init>(Lbgg;Lbgi;)V

    .line 109
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_cancel_button_text:I

    const/4 v3, 0x0

    .line 118
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 119
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 120
    return-void
.end method

.method public varargs a([Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;)V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lbgj;

    invoke-direct {v0, p0, p0, p1}, Lbgj;-><init>(Lbgg;Landroid/content/Context;[Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;)V

    iput-object v0, p0, Lbgg;->a:Lbgj;

    .line 41
    iget-object v0, p0, Lbgg;->a:Lbgj;

    invoke-virtual {v0}, Lbgj;->enable()V

    .line 42
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lbgg;->a:Lbgj;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lbgg;->a:Lbgj;

    invoke-virtual {v0}, Lbgj;->disable()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lbgg;->a:Lbgj;

    .line 50
    :cond_0
    return-void
.end method
