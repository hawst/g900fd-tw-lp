.class LacC;
.super Ljava/lang/Object;
.source "SharingListAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LacD;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LacB;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, LacC;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 65
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    return-object p1
.end method


# virtual methods
.method public a(LacD;LacD;)I
    .locals 2

    .prologue
    .line 38
    if-ne p1, p2, :cond_1

    .line 39
    const/4 v0, 0x0

    .line 61
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    if-nez p1, :cond_2

    .line 42
    const/4 v0, 0x1

    goto :goto_0

    .line 44
    :cond_2
    if-nez p2, :cond_3

    .line 45
    const/4 v0, -0x1

    goto :goto_0

    .line 47
    :cond_3
    invoke-virtual {p1}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqt;

    move-result-object v0

    .line 48
    invoke-virtual {p2}, LacD;->a()Laci;

    move-result-object v1

    invoke-virtual {v1}, Laci;->a()Lqt;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lqt;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    .line 50
    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p1}, LacD;->a()LabD;

    move-result-object v0

    invoke-interface {v0}, LabD;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LacC;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p2}, LacD;->a()LabD;

    move-result-object v1

    invoke-interface {v1}, LabD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LacC;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .line 56
    if-nez v0, :cond_0

    .line 59
    invoke-virtual {p1}, LacD;->a()LabD;

    move-result-object v0

    invoke-interface {v0}, LabD;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LacC;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-virtual {p2}, LacD;->a()LabD;

    move-result-object v1

    invoke-interface {v1}, LabD;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LacC;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 35
    check-cast p1, LacD;

    check-cast p2, LacD;

    invoke-virtual {p0, p1, p2}, LacC;->a(LacD;LacD;)I

    move-result v0

    return v0
.end method
