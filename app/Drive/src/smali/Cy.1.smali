.class public LCy;
.super Ljava/lang/Object;
.source "GroupTitleViewHolder.java"


# instance fields
.field private a:LIB;

.field final a:Landroid/view/View;

.field final a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

.field final b:Landroid/view/View;

.field final b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

.field final c:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, LCy;->a:LIB;

    .line 26
    iput-object p1, p0, LCy;->a:Landroid/view/View;

    .line 27
    sget v0, Lxc;->group_title_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LCy;->b:Landroid/view/View;

    .line 28
    sget v0, Lxc;->group_title:I

    .line 29
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LCy;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 30
    sget v0, Lxc;->group_order:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LCy;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 31
    sget v0, Lxc;->doclist_sticky_header_shadow:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LCy;->c:Landroid/view/View;

    .line 32
    return-void
.end method

.method public static a(Landroid/view/View;)LCy;
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    instance-of v1, v0, LCy;

    if-eqz v1, :cond_0

    .line 87
    check-cast v0, LCy;

    .line 91
    :goto_0
    return-object v0

    .line 89
    :cond_0
    new-instance v0, LCy;

    invoke-direct {v0, p0}, LCy;-><init>(Landroid/view/View;)V

    .line 90
    invoke-virtual {p0, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, LCy;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, LCy;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setVisibility(I)V

    .line 48
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LCy;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, LCy;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setText(I)V

    .line 42
    :cond_0
    return-void
.end method

.method public a(LIB;)V
    .locals 1

    .prologue
    .line 100
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIB;

    iput-object v0, p0, LCy;->a:LIB;

    .line 101
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, LCy;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setTextAndTypefaceNoLayout(Ljava/lang/String;Landroid/graphics/Typeface;)V

    .line 36
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, LCy;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 75
    if-eqz p1, :cond_1

    .line 76
    iget-object v0, p0, LCy;->c:Landroid/view/View;

    sget v1, Lxb;->action_bar_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, LCy;->c:Landroid/view/View;

    sget v1, LwZ;->m_doclist_background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, LCy;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, LCy;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setVisibility(I)V

    .line 54
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LCy;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 66
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, LCy;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setVisibility(I)V

    .line 58
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, LCy;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setVisibility(I)V

    .line 62
    return-void
.end method
