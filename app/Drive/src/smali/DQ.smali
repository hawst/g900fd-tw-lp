.class public final LDQ;
.super Ljava/lang/Object;
.source "SyncViewState.java"


# instance fields
.field private final a:Landroid/widget/ProgressBar;

.field private final a:Landroid/widget/TextView;

.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/widget/TextView;Landroid/widget/ProgressBar;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LDQ;->a:Landroid/widget/TextView;

    .line 269
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LDQ;->a:Landroid/widget/ProgressBar;

    .line 270
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LDQ;->a:Ljava/lang/String;

    .line 271
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/view/View;)LDQ;
    .locals 2

    .prologue
    .line 279
    new-instance v0, LDS;

    invoke-direct {v0, p0}, LDS;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 280
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 281
    if-eqz v0, :cond_0

    .line 282
    sget v1, Lxc;->sync_progress_updater:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDQ;

    .line 284
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/widget/TextView;Landroid/widget/ProgressBar;Ljava/lang/String;LahR;)V
    .locals 2

    .prologue
    .line 290
    new-instance v0, LDQ;

    invoke-direct {v0, p1, p2, p3}, LDQ;-><init>(Landroid/widget/TextView;Landroid/widget/ProgressBar;Ljava/lang/String;)V

    .line 291
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    new-instance v1, LDS;

    invoke-direct {v1, p0}, LDS;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 293
    sget v1, Lxc;->sync_progress_updater:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 294
    if-eqz p4, :cond_0

    .line 295
    invoke-virtual {v0, p4}, LDQ;->a(LahR;)V

    .line 297
    :cond_0
    return-void
.end method


# virtual methods
.method public a(LahR;)V
    .locals 3

    .prologue
    .line 274
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    iget-object v0, p0, LDQ;->a:Landroid/widget/TextView;

    iget-object v1, p0, LDQ;->a:Landroid/widget/ProgressBar;

    iget-object v2, p0, LDQ;->a:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2}, LamS;->a(Landroid/widget/TextView;Landroid/widget/ProgressBar;LahR;Ljava/lang/String;)V

    .line 276
    return-void
.end method
