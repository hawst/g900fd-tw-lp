.class LxP;
.super Ljava/lang/Object;
.source "CatalogPromoInfo.java"


# instance fields
.field private final a:Lath;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lath;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LxP;->a:Ljava/lang/String;

    .line 19
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LxP;->b:Ljava/lang/String;

    .line 20
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath;

    iput-object v0, p0, LxP;->a:Lath;

    .line 21
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lath;)LxP;
    .locals 1

    .prologue
    .line 31
    new-instance v0, LxP;

    invoke-direct {v0, p0, p1, p2}, LxP;-><init>(Ljava/lang/String;Ljava/lang/String;Lath;)V

    return-object v0
.end method


# virtual methods
.method public a()Lath;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LxP;->a:Lath;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LxP;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LxP;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 48
    instance-of v1, p1, LxP;

    if-nez v1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 52
    :cond_1
    check-cast p1, LxP;

    .line 53
    iget-object v1, p0, LxP;->a:Ljava/lang/String;

    iget-object v2, p1, LxP;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LxP;->a:Lath;

    iget-object v2, p1, LxP;->a:Lath;

    .line 54
    invoke-virtual {v1, v2}, Lath;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LxP;->b:Ljava/lang/String;

    iget-object v2, p1, LxP;->b:Ljava/lang/String;

    .line 55
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LxP;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LxP;->a:Lath;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LxP;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 65
    const-string v0, "CatalogPromoInfo[%s, %s, %s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LxP;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LxP;->a:Lath;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LxP;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
