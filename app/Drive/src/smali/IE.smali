.class public LIE;
.super Ljava/lang/Object;
.source "QuotaRange.java"


# direct methods
.method static a(J)LID;
    .locals 4

    .prologue
    const-wide/32 v2, 0x100000

    .line 52
    const-wide/32 v0, 0x40000000

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    .line 53
    sget-object v0, LID;->a:LID;

    .line 71
    :goto_0
    return-object v0

    .line 56
    :cond_0
    const-wide/32 v0, 0x6400000

    cmp-long v0, p0, v0

    if-ltz v0, :cond_1

    .line 57
    sget-object v0, LID;->c:LID;

    goto :goto_0

    .line 60
    :cond_1
    const-wide/32 v0, 0xa00000

    cmp-long v0, p0, v0

    if-ltz v0, :cond_2

    .line 61
    sget-object v0, LID;->b:LID;

    goto :goto_0

    .line 64
    :cond_2
    cmp-long v0, p0, v2

    if-ltz v0, :cond_3

    .line 65
    sget-object v0, LID;->d:LID;

    goto :goto_0

    .line 68
    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_4

    cmp-long v0, p0, v2

    if-gez v0, :cond_4

    .line 69
    sget-object v0, LID;->e:LID;

    goto :goto_0

    .line 71
    :cond_4
    sget-object v0, LID;->f:LID;

    goto :goto_0
.end method
