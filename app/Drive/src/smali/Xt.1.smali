.class public final enum LXt;
.super Ljava/lang/Enum;
.source "PunchFormFactor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LXt;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LXt;

.field private static final synthetic a:[LXt;

.field public static final enum b:LXt;


# instance fields
.field private final a:Z

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, LXt;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v2, v3, v2}, LXt;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LXt;->a:LXt;

    .line 10
    new-instance v0, LXt;

    const-string v1, "TABLET"

    invoke-direct {v0, v1, v3, v2, v3}, LXt;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LXt;->b:LXt;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [LXt;

    sget-object v1, LXt;->a:LXt;

    aput-object v1, v0, v2

    sget-object v1, LXt;->b:LXt;

    aput-object v1, v0, v3

    sput-object v0, LXt;->a:[LXt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput-boolean p3, p0, LXt;->a:Z

    .line 18
    iput-boolean p4, p0, LXt;->b:Z

    .line 19
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LXt;
    .locals 1

    .prologue
    .line 8
    const-class v0, LXt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LXt;

    return-object v0
.end method

.method public static values()[LXt;
    .locals 1

    .prologue
    .line 8
    sget-object v0, LXt;->a:[LXt;

    invoke-virtual {v0}, [LXt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LXt;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, LXt;->a:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, LXt;->b:Z

    return v0
.end method
