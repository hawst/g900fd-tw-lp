.class public final Lajw;
.super Ljava/lang/Object;
.source "Lazy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lajw;-><init>(Lbxw;)V

    .line 26
    return-void
.end method

.method private constructor <init>(Lbxw;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxw;

    iput-object v0, p0, Lajw;->a:Lbxw;

    .line 30
    return-void
.end method

.method public static a(Lbxw;)Lajw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbxw",
            "<TT;>;)",
            "Lajw",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lajw;

    invoke-direct {v0, p0}, Lajw;-><init>(Lbxw;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lajw;->a:Ljava/lang/Object;

    .line 61
    if-nez v0, :cond_1

    .line 62
    monitor-enter p0

    .line 63
    :try_start_0
    iget-object v0, p0, Lajw;->a:Ljava/lang/Object;

    .line 64
    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lajw;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "provider delegate %s returns null"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lajw;->a:Lbxw;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LbiT;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 67
    iput-object v0, p0, Lajw;->a:Ljava/lang/Object;

    .line 69
    :cond_0
    monitor-exit p0

    .line 71
    :cond_1
    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
