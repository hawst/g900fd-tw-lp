.class public final LJQ;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJL;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJN;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lara;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJK;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJM;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJJ;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LqV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 44
    iput-object p1, p0, LJQ;->a:LbrA;

    .line 45
    const-class v0, LJL;

    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJQ;->a:Lbsk;

    .line 48
    const-class v0, LJN;

    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJQ;->b:Lbsk;

    .line 51
    const-class v0, Lara;

    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJQ;->c:Lbsk;

    .line 54
    const-class v0, LJK;

    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJQ;->d:Lbsk;

    .line 57
    const-class v0, LJM;

    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJQ;->e:Lbsk;

    .line 60
    const-class v0, LJJ;

    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJQ;->f:Lbsk;

    .line 63
    const-class v0, LqV;

    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJQ;->g:Lbsk;

    .line 66
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 191
    packed-switch p1, :pswitch_data_0

    .line 247
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :pswitch_1
    new-instance v0, LJL;

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 196
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LJQ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LqD;

    iget-object v2, v2, LqD;->c:Lbsk;

    .line 194
    invoke-static {v1, v2}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LqK;

    iget-object v2, p0, LJQ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:La;

    iget-object v2, v2, La;->a:Lbsk;

    .line 202
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LJQ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:La;

    iget-object v3, v3, La;->a:Lbsk;

    .line 200
    invoke-static {v2, v3}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    iget-object v3, p0, LJQ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 208
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LJQ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 206
    invoke-static {v3, v4}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LQr;

    iget-object v4, p0, LJQ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lwc;

    iget-object v4, v4, Lwc;->g:Lbsk;

    .line 214
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LJQ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lwc;

    iget-object v5, v5, Lwc;->g:Lbsk;

    .line 212
    invoke-static {v4, v5}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lwm;

    iget-object v5, p0, LJQ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LJQ;

    iget-object v5, v5, LJQ;->g:Lbsk;

    .line 220
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LJQ;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LJQ;

    iget-object v6, v6, LJQ;->g:Lbsk;

    .line 218
    invoke-static {v5, v6}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LqV;

    iget-object v6, p0, LJQ;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LarZ;

    iget-object v6, v6, LarZ;->s:Lbsk;

    .line 226
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LJQ;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LarZ;

    iget-object v7, v7, LarZ;->s:Lbsk;

    .line 224
    invoke-static {v6, v7}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Latd;

    iget-object v7, p0, LJQ;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LCw;

    iget-object v7, v7, LCw;->aa:Lbsk;

    .line 232
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LJQ;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LCw;

    iget-object v8, v8, LCw;->aa:Lbsk;

    .line 230
    invoke-static {v7, v8}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LsC;

    invoke-direct/range {v0 .. v7}, LJL;-><init>(LqK;Landroid/app/Activity;LQr;Lwm;LqV;Latd;LsC;)V

    .line 237
    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJQ;

    .line 238
    invoke-virtual {v1, v0}, LJQ;->a(LJL;)V

    .line 245
    :goto_0
    return-object v0

    .line 241
    :pswitch_2
    new-instance v0, LJN;

    invoke-direct {v0}, LJN;-><init>()V

    .line 243
    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJQ;

    .line 244
    invoke-virtual {v1, v0}, LJQ;->a(LJN;)V

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x45a
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 274
    sparse-switch p2, :sswitch_data_0

    .line 321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :sswitch_0
    check-cast p1, LJP;

    .line 278
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 281
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 278
    invoke-virtual {p1, v0}, LJP;->provideOnSearchListener(Landroid/content/Context;)Lara;

    move-result-object v0

    .line 314
    :goto_0
    return-object v0

    .line 285
    :sswitch_1
    check-cast p1, LJP;

    .line 287
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJQ;

    iget-object v0, v0, LJQ;->a:Lbsk;

    .line 290
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJL;

    .line 287
    invoke-virtual {p1, v0}, LJP;->provideDocListExtraActionsHelper(LJL;)LJK;

    move-result-object v0

    goto :goto_0

    .line 294
    :sswitch_2
    check-cast p1, LJP;

    .line 296
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJQ;

    iget-object v0, v0, LJQ;->b:Lbsk;

    .line 299
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJN;

    .line 296
    invoke-virtual {p1, v0}, LJP;->provideDocListMenu(LJN;)LJM;

    move-result-object v0

    goto :goto_0

    .line 303
    :sswitch_3
    check-cast p1, LJP;

    .line 305
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 308
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 305
    invoke-virtual {p1, v0}, LJP;->provideDocListActionsListener(Landroid/content/Context;)LJJ;

    move-result-object v0

    goto :goto_0

    .line 312
    :sswitch_4
    check-cast p1, LJP;

    .line 314
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 317
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 314
    invoke-virtual {p1, v0}, LJP;->provideAccountActivity(Landroid/content/Context;)LqV;

    move-result-object v0

    goto :goto_0

    .line 274
    nop

    :sswitch_data_0
    .sparse-switch
        0x2b1 -> :sswitch_1
        0x45b -> :sswitch_4
        0x45c -> :sswitch_0
        0x45e -> :sswitch_3
        0x45f -> :sswitch_2
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 156
    const-class v0, LJN;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x72

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LJQ;->a(LbuP;LbuB;)V

    .line 159
    const-class v0, LJL;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x71

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LJQ;->a(LbuP;LbuB;)V

    .line 162
    const-class v0, LJL;

    iget-object v1, p0, LJQ;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LJQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 163
    const-class v0, LJN;

    iget-object v1, p0, LJQ;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LJQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 164
    const-class v0, Lara;

    iget-object v1, p0, LJQ;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LJQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 165
    const-class v0, LJK;

    iget-object v1, p0, LJQ;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LJQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 166
    const-class v0, LJM;

    iget-object v1, p0, LJQ;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LJQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 167
    const-class v0, LJJ;

    iget-object v1, p0, LJQ;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LJQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 168
    const-class v0, LqV;

    iget-object v1, p0, LJQ;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LJQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 169
    iget-object v0, p0, LJQ;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x45a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 171
    iget-object v0, p0, LJQ;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x45d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 173
    iget-object v0, p0, LJQ;->c:Lbsk;

    const-class v1, LJP;

    const/16 v2, 0x45c

    invoke-virtual {p0, v1, v2}, LJQ;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 175
    iget-object v0, p0, LJQ;->d:Lbsk;

    const-class v1, LJP;

    const/16 v2, 0x2b1

    invoke-virtual {p0, v1, v2}, LJQ;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 177
    iget-object v0, p0, LJQ;->e:Lbsk;

    const-class v1, LJP;

    const/16 v2, 0x45f

    invoke-virtual {p0, v1, v2}, LJQ;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 179
    iget-object v0, p0, LJQ;->f:Lbsk;

    const-class v1, LJP;

    const/16 v2, 0x45e

    invoke-virtual {p0, v1, v2}, LJQ;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 181
    iget-object v0, p0, LJQ;->g:Lbsk;

    const-class v1, LJP;

    const/16 v2, 0x45b

    invoke-virtual {p0, v1, v2}, LJQ;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 183
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 254
    packed-switch p1, :pswitch_data_0

    .line 268
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :pswitch_0
    check-cast p2, LJN;

    .line 258
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJQ;

    .line 259
    invoke-virtual {v0, p2}, LJQ;->a(LJN;)V

    .line 270
    :goto_0
    return-void

    .line 262
    :pswitch_1
    check-cast p2, LJL;

    .line 264
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJQ;

    .line 265
    invoke-virtual {v0, p2}, LJQ;->a(LJL;)V

    goto :goto_0

    .line 254
    :pswitch_data_0
    .packed-switch 0x71
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(LJL;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSr;

    iget-object v0, v0, LSr;->c:Lbsk;

    .line 149
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSv;

    iput-object v0, p1, LJL;->a:LSv;

    .line 151
    return-void
.end method

.method public a(LJN;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 75
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 73
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LJN;->a:LtK;

    .line 79
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 82
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 80
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LJN;->a:Landroid/content/Context;

    .line 86
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJQ;

    iget-object v0, v0, LJQ;->d:Lbsk;

    .line 89
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJQ;

    iget-object v1, v1, LJQ;->d:Lbsk;

    .line 87
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJK;

    iput-object v0, p1, LJN;->a:LJK;

    .line 93
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJQ;

    iget-object v0, v0, LJQ;->f:Lbsk;

    .line 96
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJQ;

    iget-object v1, v1, LJQ;->f:Lbsk;

    .line 94
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJJ;

    iput-object v0, p1, LJN;->a:LJJ;

    .line 100
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->aa:Lbsk;

    .line 103
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->aa:Lbsk;

    .line 101
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsC;

    iput-object v0, p1, LJN;->a:LsC;

    .line 107
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 110
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 108
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, LJN;->a:LQr;

    .line 114
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFJ;

    iget-object v0, v0, LFJ;->a:Lbsk;

    .line 117
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LFJ;

    iget-object v1, v1, LFJ;->a:Lbsk;

    .line 115
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFF;

    iput-object v0, p1, LJN;->a:LFF;

    .line 121
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->j:Lbsk;

    .line 124
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->j:Lbsk;

    .line 122
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p1, LJN;->a:LaFO;

    .line 128
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJT;

    iget-object v0, v0, LJT;->c:Lbsk;

    .line 131
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJT;

    iget-object v1, v1, LJT;->c:Lbsk;

    .line 129
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJR;

    iput-object v0, p1, LJN;->a:LJR;

    .line 135
    iget-object v0, p0, LJQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->y:Lbsk;

    .line 138
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->y:Lbsk;

    .line 136
    invoke-static {v0, v1}, LJQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCU;

    iput-object v0, p1, LJN;->a:LCU;

    .line 142
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 187
    return-void
.end method
