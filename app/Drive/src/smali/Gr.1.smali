.class public LGr;
.super Ljava/lang/Object;
.source "FileOpenerIntentCreatorImpl.java"

# interfaces
.implements LGk;


# static fields
.field public static final a:LGr;


# instance fields
.field private final a:Landroid/content/Intent;

.field private final a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 197
    new-instance v0, LGr;

    invoke-direct {v0}, LGr;-><init>()V

    sput-object v0, LGr;->a:LGr;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object v1, p0, LGr;->a:Landroid/content/Intent;

    .line 212
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LGr;->a:Ljava/util/List;

    .line 213
    iput-object v1, p0, LGr;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 214
    return-void
.end method

.method private constructor <init>(Landroid/content/Intent;Ljava/util/List;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Lcom/google/android/apps/docs/app/DocumentOpenMethod;",
            ")V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    new-instance v1, Landroid/content/Intent;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v1, p0, LGr;->a:Landroid/content/Intent;

    .line 206
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LGr;->a:Ljava/util/List;

    .line 207
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    iput-object v0, p0, LGr;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 208
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Intent;Ljava/util/List;Lcom/google/android/apps/docs/app/DocumentOpenMethod;LGl;)V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0, p1, p2, p3}, LGr;-><init>(Landroid/content/Intent;Ljava/util/List;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;
    .locals 3

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;

    iget-object v1, p0, LGr;->a:Landroid/content/Intent;

    iget-object v2, p0, LGr;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;-><init>(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    return-object v0
.end method

.method public a(I)Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, LGr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 219
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LGr;->a:Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 220
    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;

    iget-object v2, p0, LGr;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;-><init>(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, LGr;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 236
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "QueryResultImpl[intent=%s, %d targets]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LGr;->a:Landroid/content/Intent;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LGr;->a:Ljava/util/List;

    .line 237
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 236
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
