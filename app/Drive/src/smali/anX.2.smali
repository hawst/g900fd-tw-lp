.class final enum LanX;
.super Ljava/lang/Enum;
.source "ChainedImageDownloadFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LanX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LanX;

.field private static final synthetic a:[LanX;

.field public static final enum b:LanX;


# instance fields
.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, LanX;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v2, v3}, LanX;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LanX;->a:LanX;

    .line 96
    new-instance v0, LanX;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3, v2}, LanX;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LanX;->b:LanX;

    .line 94
    const/4 v0, 0x2

    new-array v0, v0, [LanX;

    sget-object v1, LanX;->a:LanX;

    aput-object v1, v0, v2

    sget-object v1, LanX;->b:LanX;

    aput-object v1, v0, v3

    sput-object v0, LanX;->a:[LanX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 101
    iput-boolean p3, p0, LanX;->a:Z

    .line 102
    return-void
.end method

.method static synthetic a(LanX;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, LanX;->a:Z

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LanX;
    .locals 1

    .prologue
    .line 94
    const-class v0, LanX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LanX;

    return-object v0
.end method

.method public static values()[LanX;
    .locals 1

    .prologue
    .line 94
    sget-object v0, LanX;->a:[LanX;

    invoke-virtual {v0}, [LanX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LanX;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, LanX;->a:Z

    return v0
.end method
