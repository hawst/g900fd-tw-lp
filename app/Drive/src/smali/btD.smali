.class public final LbtD;
.super Ljava/lang/Object;
.source "TypeAdapters.java"


# static fields
.field public static final a:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lbtq;

.field public static final b:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lbtq;

.field public static final c:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lbtq;

.field public static final d:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lbtq;

.field public static final e:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lbtq;

.field public static final f:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lbtq;

.field public static final g:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lbtq;

.field public static final h:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Lbtq;

.field public static final i:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lbtq;

.field public static final j:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Lbtq;

.field public static final k:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lbtq;

.field public static final l:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lbtq;

.field public static final m:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lbtq;

.field public static final n:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Lbtq;

.field public static final o:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lbtq;

.field public static final p:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:Lbtq;

.field public static final q:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lbtq;

.field public static final r:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final r:Lbtq;

.field public static final s:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lbtq;

.field public static final t:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final t:Lbtq;

.field public static final u:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final x:Lbtp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtp",
            "<",
            "Lbth;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    new-instance v0, LbtE;

    invoke-direct {v0}, LbtE;-><init>()V

    sput-object v0, LbtD;->a:Lbtp;

    .line 82
    const-class v0, Ljava/lang/Class;

    sget-object v1, LbtD;->a:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->a:Lbtq;

    .line 84
    new-instance v0, LbtP;

    invoke-direct {v0}, LbtP;-><init>()V

    sput-object v0, LbtD;->b:Lbtp;

    .line 141
    const-class v0, Ljava/util/BitSet;

    sget-object v1, LbtD;->b:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->b:Lbtq;

    .line 143
    new-instance v0, LbtZ;

    invoke-direct {v0}, LbtZ;-><init>()V

    sput-object v0, LbtD;->c:Lbtp;

    .line 169
    new-instance v0, Lbud;

    invoke-direct {v0}, Lbud;-><init>()V

    sput-object v0, LbtD;->d:Lbtp;

    .line 183
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, LbtD;->c:Lbtp;

    invoke-static {v0, v1, v2}, LbtD;->a(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->c:Lbtq;

    .line 186
    new-instance v0, Lbue;

    invoke-direct {v0}, Lbue;-><init>()V

    sput-object v0, LbtD;->e:Lbtp;

    .line 206
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, LbtD;->e:Lbtp;

    invoke-static {v0, v1, v2}, LbtD;->a(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->d:Lbtq;

    .line 209
    new-instance v0, Lbuf;

    invoke-direct {v0}, Lbuf;-><init>()V

    sput-object v0, LbtD;->f:Lbtp;

    .line 228
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, LbtD;->f:Lbtp;

    invoke-static {v0, v1, v2}, LbtD;->a(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->e:Lbtq;

    .line 231
    new-instance v0, Lbug;

    invoke-direct {v0}, Lbug;-><init>()V

    sput-object v0, LbtD;->g:Lbtp;

    .line 250
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, LbtD;->g:Lbtp;

    invoke-static {v0, v1, v2}, LbtD;->a(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->f:Lbtq;

    .line 253
    new-instance v0, Lbuh;

    invoke-direct {v0}, Lbuh;-><init>()V

    sput-object v0, LbtD;->h:Lbtp;

    .line 272
    new-instance v0, Lbui;

    invoke-direct {v0}, Lbui;-><init>()V

    sput-object v0, LbtD;->i:Lbtp;

    .line 287
    new-instance v0, LbtF;

    invoke-direct {v0}, LbtF;-><init>()V

    sput-object v0, LbtD;->j:Lbtp;

    .line 302
    new-instance v0, LbtG;

    invoke-direct {v0}, LbtG;-><init>()V

    sput-object v0, LbtD;->k:Lbtp;

    .line 322
    const-class v0, Ljava/lang/Number;

    sget-object v1, LbtD;->k:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->g:Lbtq;

    .line 324
    new-instance v0, LbtH;

    invoke-direct {v0}, LbtH;-><init>()V

    sput-object v0, LbtD;->l:Lbtp;

    .line 343
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, LbtD;->l:Lbtp;

    invoke-static {v0, v1, v2}, LbtD;->a(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->h:Lbtq;

    .line 346
    new-instance v0, LbtI;

    invoke-direct {v0}, LbtI;-><init>()V

    sput-object v0, LbtD;->m:Lbtp;

    .line 366
    new-instance v0, LbtJ;

    invoke-direct {v0}, LbtJ;-><init>()V

    sput-object v0, LbtD;->n:Lbtp;

    .line 384
    new-instance v0, LbtK;

    invoke-direct {v0}, LbtK;-><init>()V

    sput-object v0, LbtD;->o:Lbtp;

    .line 402
    const-class v0, Ljava/lang/String;

    sget-object v1, LbtD;->m:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->i:Lbtq;

    .line 404
    new-instance v0, LbtL;

    invoke-direct {v0}, LbtL;-><init>()V

    sput-object v0, LbtD;->p:Lbtp;

    .line 419
    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, LbtD;->p:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->j:Lbtq;

    .line 422
    new-instance v0, LbtM;

    invoke-direct {v0}, LbtM;-><init>()V

    sput-object v0, LbtD;->q:Lbtp;

    .line 437
    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, LbtD;->q:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->k:Lbtq;

    .line 440
    new-instance v0, LbtN;

    invoke-direct {v0}, LbtN;-><init>()V

    sput-object v0, LbtD;->r:Lbtp;

    .line 456
    const-class v0, Ljava/net/URL;

    sget-object v1, LbtD;->r:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->l:Lbtq;

    .line 458
    new-instance v0, LbtO;

    invoke-direct {v0}, LbtO;-><init>()V

    sput-object v0, LbtD;->s:Lbtp;

    .line 478
    const-class v0, Ljava/net/URI;

    sget-object v1, LbtD;->s:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->m:Lbtq;

    .line 480
    new-instance v0, LbtQ;

    invoke-direct {v0}, LbtQ;-><init>()V

    sput-object v0, LbtD;->t:Lbtp;

    .line 496
    const-class v0, Ljava/net/InetAddress;

    sget-object v1, LbtD;->t:Lbtp;

    invoke-static {v0, v1}, LbtD;->b(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->n:Lbtq;

    .line 499
    new-instance v0, LbtR;

    invoke-direct {v0}, LbtR;-><init>()V

    sput-object v0, LbtD;->u:Lbtp;

    .line 514
    const-class v0, Ljava/util/UUID;

    sget-object v1, LbtD;->u:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->o:Lbtq;

    .line 516
    new-instance v0, LbtS;

    invoke-direct {v0}, LbtS;-><init>()V

    sput-object v0, LbtD;->p:Lbtq;

    .line 537
    new-instance v0, LbtT;

    invoke-direct {v0}, LbtT;-><init>()V

    sput-object v0, LbtD;->v:Lbtp;

    .line 602
    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, LbtD;->v:Lbtp;

    invoke-static {v0, v1, v2}, LbtD;->b(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->q:Lbtq;

    .line 605
    new-instance v0, LbtU;

    invoke-direct {v0}, LbtU;-><init>()V

    sput-object v0, LbtD;->w:Lbtp;

    .line 640
    const-class v0, Ljava/util/Locale;

    sget-object v1, LbtD;->w:Lbtp;

    invoke-static {v0, v1}, LbtD;->a(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->r:Lbtq;

    .line 642
    new-instance v0, LbtV;

    invoke-direct {v0}, LbtV;-><init>()V

    sput-object v0, LbtD;->x:Lbtp;

    .line 714
    const-class v0, Lbth;

    sget-object v1, LbtD;->x:Lbtp;

    invoke-static {v0, v1}, LbtD;->b(Ljava/lang/Class;Lbtp;)Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->s:Lbtq;

    .line 749
    invoke-static {}, LbtD;->a()Lbtq;

    move-result-object v0

    sput-object v0, LbtD;->t:Lbtq;

    return-void
.end method

.method public static a()Lbtq;
    .locals 1

    .prologue
    .line 752
    new-instance v0, LbtW;

    invoke-direct {v0}, LbtW;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Lbtp;)Lbtq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lbtp",
            "<TTT;>;)",
            "Lbtq;"
        }
    .end annotation

    .prologue
    .line 779
    new-instance v0, LbtX;

    invoke-direct {v0, p0, p1}, LbtX;-><init>(Ljava/lang/Class;Lbtp;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lbtp",
            "<-TTT;>;)",
            "Lbtq;"
        }
    .end annotation

    .prologue
    .line 792
    new-instance v0, LbtY;

    invoke-direct {v0, p0, p1, p2}, LbtY;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lbtp;)Lbtq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lbtp",
            "<TTT;>;)",
            "Lbtq;"
        }
    .end annotation

    .prologue
    .line 822
    new-instance v0, Lbub;

    invoke-direct {v0, p0, p1}, Lbub;-><init>(Ljava/lang/Class;Lbtp;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)Lbtq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<+TTT;>;",
            "Lbtp",
            "<-TTT;>;)",
            "Lbtq;"
        }
    .end annotation

    .prologue
    .line 807
    new-instance v0, Lbua;

    invoke-direct {v0, p0, p1, p2}, Lbua;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lbtp;)V

    return-object v0
.end method
