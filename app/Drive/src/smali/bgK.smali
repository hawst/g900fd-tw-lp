.class public LbgK;
.super LbgN;
.source "PdfPageContent.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private final a:LbgF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, LbgK;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>(LbgF;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, LbgN;-><init>()V

    .line 27
    iput-object p1, p0, LbgK;->a:LbgF;

    .line 28
    return-void
.end method


# virtual methods
.method protected a(Ljava/io/OutputStream;)I
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 32
    iget-object v0, p0, LbgK;->a:LbgF;

    invoke-virtual {v0}, LbgF;->a()[F

    move-result-object v0

    .line 33
    iget-object v1, p0, LbgK;->a:LbgF;

    invoke-virtual {v1}, LbgF;->a()F

    move-result v1

    aget v2, v0, v8

    sub-float/2addr v1, v2

    aput v1, v0, v8

    .line 34
    iget-object v1, p0, LbgK;->a:LbgF;

    invoke-virtual {v1}, LbgF;->b()F

    move-result v1

    aget v2, v0, v9

    sub-float/2addr v1, v2

    aput v1, v0, v9

    .line 35
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "q %.2f %.2f %.2f %.2f %.2f %.2f cm /%s Do Q\n\n"

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    aget v4, v0, v6

    .line 36
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    aget v4, v0, v7

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v7

    aget v4, v0, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x3

    const/4 v5, 0x3

    aget v5, v0, v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    aget v4, v0, v8

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v8

    aget v0, v0, v9

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v3, v9

    const/4 v0, 0x6

    iget-object v4, p0, LbgK;->a:LbgF;

    invoke-virtual {v4}, LbgF;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 35
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 37
    sget-object v1, LbgK;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Writing page content: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    invoke-static {v0}, LbgK;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 39
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "<</Length %d>>"

    new-array v3, v7, [Ljava/lang/Object;

    array-length v4, v0

    .line 40
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 39
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 41
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 42
    array-length v1, v1

    .line 43
    invoke-virtual {p0, p1, v0}, LbgK;->a(Ljava/io/OutputStream;[B)I

    move-result v0

    add-int/2addr v0, v1

    .line 44
    return v0
.end method
