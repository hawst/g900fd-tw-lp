.class Lbux;
.super Ljava/lang/Object;
.source "Key.java"

# interfaces
.implements Lbuy;


# instance fields
.field final a:Ljava/lang/annotation/Annotation;


# direct methods
.method constructor <init>(Ljava/lang/annotation/Annotation;)V
    .locals 1

    .prologue
    .line 436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 437
    const-string v0, "annotation"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    iput-object v0, p0, Lbux;->a:Ljava/lang/annotation/Annotation;

    .line 438
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453
    iget-object v0, p0, Lbux;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lbux;->a:Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 457
    instance-of v0, p1, Lbux;

    if-nez v0, :cond_0

    .line 458
    const/4 v0, 0x0

    .line 462
    :goto_0
    return v0

    .line 461
    :cond_0
    check-cast p1, Lbux;

    .line 462
    iget-object v0, p0, Lbux;->a:Ljava/lang/annotation/Annotation;

    iget-object v1, p1, Lbux;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0, v1}, Ljava/lang/annotation/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lbux;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lbux;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
