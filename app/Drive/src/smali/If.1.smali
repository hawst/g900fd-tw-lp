.class public abstract LIf;
.super Ljava/lang/Object;
.source "EntriesGrouper.java"


# instance fields
.field private final a:LIg;

.field protected final a:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;LIg;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LIf;->a:Ljava/lang/String;

    .line 28
    iput-object p2, p0, LIf;->a:LIg;

    .line 29
    return-void
.end method


# virtual methods
.method public abstract a(LCv;)LIL;
.end method

.method public a()LaFr;
    .locals 1

    .prologue
    .line 44
    sget-object v0, LaEz;->a:LaFr;

    return-object v0
.end method

.method public a(LCv;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 53
    invoke-interface {p1}, LCv;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(LCv;)LIy;
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LIf;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LIf;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LIf;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LIf;->a:LIg;

    invoke-virtual {v1}, LIg;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
