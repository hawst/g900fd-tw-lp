.class public LNC;
.super LaGN;
.source "AudioPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LaGo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/ResourceSpec;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, LNC;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    iput-object p2, p0, LNC;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)LaGo;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, LNC;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LaGM;

    iget-object v1, p0, LNC;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v0, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0, p1}, LNC;->a(LaGM;)LaGo;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGo;)V
    .locals 1

    .prologue
    .line 179
    if-eqz p1, :cond_0

    .line 180
    iget-object v0, p0, LNC;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a()LaqY;

    move-result-object v0

    invoke-interface {v0, p1}, LaqY;->a(LaGu;)V

    .line 181
    iget-object v0, p0, LNC;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuX;

    .line 182
    invoke-virtual {v0, p1}, LuX;->a(LaGu;)V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, LNC;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->finish()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 171
    check-cast p1, LaGo;

    invoke-virtual {p0, p1}, LNC;->a(LaGo;)V

    return-void
.end method
