.class LaUN;
.super Ljava/lang/Object;

# interfaces
.implements LaUh;


# instance fields
.field private final a:I

.field private a:LaVa;

.field private a:[B


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LaUN;->a:I

    invoke-virtual {p0}, LaUN;->a()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget v0, p0, LaUN;->a:I

    new-array v0, v0, [B

    iput-object v0, p0, LaUN;->a:[B

    iget-object v0, p0, LaUN;->a:[B

    invoke-static {v0}, LaVa;->a([B)LaVa;

    move-result-object v0

    iput-object v0, p0, LaUN;->a:LaVa;

    return-void
.end method

.method public a(IJ)V
    .locals 2

    iget-object v0, p0, LaUN;->a:LaVa;

    invoke-virtual {v0, p1, p2, p3}, LaVa;->a(IJ)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, LaUN;->a:LaVa;

    invoke-virtual {v0, p1, p2}, LaVa;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a()[B
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LaUN;->a:LaVa;

    invoke-virtual {v0}, LaVa;->a()I

    move-result v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, LaUN;->a:[B

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LaUN;->a:[B

    array-length v1, v1

    sub-int v0, v1, v0

    new-array v0, v0, [B

    iget-object v1, p0, LaUN;->a:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method
