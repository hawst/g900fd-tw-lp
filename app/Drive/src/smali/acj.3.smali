.class public Lacj;
.super Ljava/lang/Object;
.source "SharingHelper.java"

# interfaces
.implements LQT;
.implements Labg;
.implements Lacz;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field a:LM;

.field a:LaGM;

.field a:LaGR;

.field a:LaKR;

.field a:LabQ;

.field private a:Lacp;

.field a:Lacq;

.field a:Lacs;

.field a:LajO;

.field public a:Lakx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakx",
            "<*>;"
        }
    .end annotation
.end field

.field a:Lald;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Laco;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/Executor;

.field a:LtK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 62
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lacj;->a:Ljava/util/Set;

    .line 149
    invoke-static {}, Lakx;->a()Lakx;

    move-result-object v0

    iput-object v0, p0, Lacj;->a:Lakx;

    .line 153
    iput-object p1, p0, Lacj;->a:Landroid/content/Context;

    .line 154
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 155
    new-instance v1, LalI;

    invoke-direct {v1, v0}, LalI;-><init>(Landroid/os/Handler;)V

    iput-object v1, p0, Lacj;->a:Ljava/util/concurrent/Executor;

    .line 156
    return-void
.end method

.method static synthetic a(Lacj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lacj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lacj;Lacr;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lacj;->a(Lacr;)V

    return-void
.end method

.method private a(Lacr;)V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lacj;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laco;

    .line 276
    invoke-interface {v0, p1}, Laco;->a(Lacr;)V

    goto :goto_0

    .line 278
    :cond_0
    return-void
.end method

.method private a(Lakx;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lakx",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lacj;->a:Lakx;

    invoke-virtual {v0}, Lakx;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    const-string v0, "SharingHelper"

    const-string v1, "Background task %s cancelled before completion"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lacj;->a:Lakx;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 284
    :cond_0
    iget-object v0, p0, Lacj;->a:Lakx;

    invoke-virtual {v0}, Lakx;->a()V

    .line 285
    iput-object p1, p0, Lacj;->a:Lakx;

    .line 286
    iget-object v0, p0, Lacj;->a:Lakx;

    iget-object v1, p0, Lacj;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1}, Lakx;->a(Ljava/util/concurrent/Executor;)V

    .line 287
    return-void
.end method


# virtual methods
.method public a()Lacr;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lacj;->a:Lacq;

    invoke-virtual {v0}, Lacq;->a()Lacr;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 312
    iget-object v0, p0, Lacj;->a:Landroid/content/Context;

    sget v1, Lxi;->sharing_list_updated:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 313
    invoke-virtual {p0}, Lacj;->b()V

    .line 314
    return-void
.end method

.method public a(Laco;)V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lacj;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 304
    return-void
.end method

.method public a(Lacp;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lacj;->a:Lacp;

    .line 308
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lacj;->a:Lacq;

    invoke-virtual {v0}, Lacq;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-static {v0, p1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lacj;->a:Lacq;

    invoke-virtual {v0, p1}, Lacq;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 192
    iget-object v0, p0, Lacj;->a:Lacq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacq;->a(Lacr;)V

    .line 194
    :cond_0
    invoke-virtual {p0}, Lacj;->c()V

    .line 195
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LabD;)V
    .locals 6

    .prologue
    .line 180
    iget-object v0, p0, Lacj;->a:Lacq;

    invoke-virtual {v0}, Lacq;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 181
    iget-object v0, p0, Lacj;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 183
    new-instance v0, Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;

    invoke-direct {v0}, Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;-><init>()V

    .line 184
    iget-object v2, p0, Lacj;->a:LajO;

    iget-object v3, p0, Lacj;->a:LaGM;

    iget-object v4, p0, Lacj;->a:Landroid/content/Context;

    .line 185
    invoke-interface {p2}, LabD;->a()Ljava/lang/String;

    move-result-object v5

    .line 184
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LajO;LaGM;Landroid/content/Context;Ljava/lang/String;)V

    .line 186
    iget-object v1, p0, Lacj;->a:LM;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;->a(LM;Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lqo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lacj;->a:LtK;

    sget-object v1, Lry;->K:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lacj;->a:LaGR;

    new-instance v1, Lacn;

    invoke-direct {v1, p0, p1, p2}, Lacn;-><init>(Lacj;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LaGR;->a(LaGN;)V

    .line 350
    :goto_0
    return-void

    .line 348
    :cond_0
    invoke-virtual {p0}, Lacj;->b()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lacj;->a:Lakx;

    invoke-virtual {v0}, Lakx;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 160
    iget-object v1, p0, Lacj;->a:Lacq;

    invoke-virtual {v1}, Lacq;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LbiT;->a(Z)V

    .line 161
    iget-object v1, p0, Lacj;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    iget-object v1, p0, Lacj;->a:Lald;

    iget-object v2, p0, Lacj;->a:Landroid/content/Context;

    sget v3, Lxi;->sharing_offline:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v4}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    :goto_0
    return v0

    .line 165
    :cond_0
    iget-object v1, p0, Lacj;->a:LaGM;

    invoke-interface {v1, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v1

    invoke-interface {v1}, LaGu;->h()Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    iget-object v1, p0, Lacj;->a:Lald;

    iget-object v2, p0, Lacj;->a:Landroid/content/Context;

    sget v3, Lxi;->sharing_cannot_change:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v4}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168
    :cond_1
    iget-object v1, p0, Lacj;->a:Lacq;

    invoke-virtual {v1}, Lacq;->a()Lacr;

    move-result-object v1

    if-nez v1, :cond_2

    .line 169
    iget-object v1, p0, Lacj;->a:Lald;

    iget-object v2, p0, Lacj;->a:Landroid/content/Context;

    sget v3, Lxi;->sharing_info_loading:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v4}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 172
    :cond_2
    invoke-static {}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b()Ljava/lang/String;

    move-result-object v0

    .line 173
    new-instance v1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;-><init>()V

    iget-object v2, p0, Lacj;->a:LM;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 174
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 198
    const-string v0, "SharingHelper"

    const-string v1, "saveAcl()"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, p0, Lacj;->a:Lacq;

    invoke-virtual {v0}, Lacq;->a()Lacr;

    move-result-object v0

    .line 200
    iget-object v1, p0, Lacj;->a:Lacq;

    invoke-virtual {v1, v3}, Lacq;->a(Lacr;)V

    .line 201
    iget-object v1, p0, Lacj;->a:Lacs;

    invoke-interface {v1, v0}, Lacs;->a(Lacr;)LbsU;

    move-result-object v0

    .line 202
    new-instance v1, Lack;

    invoke-direct {v1, p0}, Lack;-><init>(Lacj;)V

    .line 218
    new-instance v2, Lakx;

    invoke-direct {v2, v0, v1}, Lakx;-><init>(LbsU;LbsJ;)V

    .line 219
    invoke-direct {p0, v2}, Lacj;->a(Lakx;)V

    .line 220
    invoke-direct {p0, v3}, Lacj;->a(Lacr;)V

    .line 221
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 226
    const-string v0, "SharingHelper"

    const-string v2, "refreshAcl()"

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v0, p0, Lacj;->a:Lacq;

    invoke-virtual {v0}, Lacq;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 228
    iget-object v0, p0, Lacj;->a:Lacq;

    invoke-virtual {v0}, Lacq;->a()Lacr;

    move-result-object v3

    .line 229
    if-nez v2, :cond_0

    move-object v0, v1

    .line 231
    :goto_0
    if-nez v0, :cond_1

    .line 232
    invoke-direct {p0, v1}, Lacj;->a(Lacr;)V

    .line 272
    :goto_1
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lacj;->a:LaGM;

    .line 230
    invoke-interface {v0, v2}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    goto :goto_0

    .line 237
    :cond_1
    iget-object v1, p0, Lacj;->a:Lacs;

    .line 238
    invoke-interface {v1, v0}, Lacs;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;

    move-result-object v0

    .line 239
    new-instance v1, Lacl;

    invoke-direct {v1, p0, v2}, Lacl;-><init>(Lacj;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 264
    new-instance v2, Lakx;

    invoke-direct {v2, v0, v1}, Lakx;-><init>(LbsU;LbsJ;)V

    .line 265
    invoke-direct {p0, v2}, Lacj;->a(Lakx;)V

    .line 266
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lacm;

    invoke-direct {v1, p0, v3}, Lacm;-><init>(Lacj;Lacr;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public d()V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lacj;->a:Lacp;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lacj;->a:Lacp;

    invoke-interface {v0}, Lacp;->t()V

    .line 359
    :cond_0
    return-void
.end method
