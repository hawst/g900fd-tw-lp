.class LaIt;
.super Ljava/lang/Object;
.source "StorageFileReadWrite.java"

# interfaces
.implements Landroid/os/ParcelFileDescriptor$OnCloseListener;


# instance fields
.field final synthetic a:LaIs;

.field final synthetic a:Ladj;


# direct methods
.method constructor <init>(LaIs;Ladj;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, LaIt;->a:LaIs;

    iput-object p2, p0, LaIt;->a:Ladj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClose(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    :try_start_0
    const-string v0, "StorageFileReadWrite"

    const-string v1, "Document file updated successfully"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v0, p0, LaIt;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()V

    .line 60
    :cond_0
    const-string v0, "StorageFileReadWrite"

    const-string v1, "Document file is closed"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    iget-object v0, p0, LaIt;->a:Ladj;

    invoke-interface {v0}, Ladj;->close()V

    .line 64
    return-void

    .line 62
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaIt;->a:Ladj;

    invoke-interface {v1}, Ladj;->close()V

    throw v0
.end method
