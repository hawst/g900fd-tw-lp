.class LblI;
.super Ljava/lang/Object;
.source "AbstractMapBasedMultiset.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lbpj",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LblH;

.field final synthetic a:Ljava/util/Iterator;

.field a:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TE;",
            "Lbmc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LblH;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, LblI;->a:LblH;

    iput-object p2, p0, LblI;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lbpj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbpj",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, LblI;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 97
    iput-object v0, p0, LblI;->a:Ljava/util/Map$Entry;

    .line 98
    new-instance v1, LblJ;

    invoke-direct {v1, p0, v0}, LblJ;-><init>(LblI;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, LblI;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, LblI;->a()Lbpj;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, LblI;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbnr;->a(Z)V

    .line 119
    iget-object v2, p0, LblI;->a:LblH;

    iget-object v0, p0, LblI;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    invoke-virtual {v0, v1}, Lbmc;->c(I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v0, v1}, LblH;->a(LblH;J)J

    .line 120
    iget-object v0, p0, LblI;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, LblI;->a:Ljava/util/Map$Entry;

    .line 122
    return-void

    :cond_0
    move v0, v1

    .line 118
    goto :goto_0
.end method
