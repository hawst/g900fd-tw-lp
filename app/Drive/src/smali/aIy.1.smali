.class public LaIy;
.super Ljava/lang/Object;
.source "SyncEngine.java"


# instance fields
.field private final a:LQr;

.field private final a:Lafz;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaIC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lafz;Laja;LQr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lafz;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LQr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaIy;->a:Ljava/util/Set;

    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafz;

    iput-object v0, p0, LaIy;->a:Lafz;

    .line 46
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    iput-object v0, p0, LaIy;->a:LbuE;

    .line 47
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p0, LaIy;->a:LQr;

    .line 48
    return-void
.end method

.method private static a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v1, "loading"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 54
    return-object v0
.end method

.method private static a(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    const-string v1, "info"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-object v0
.end method

.method static synthetic a(LaIy;)LbuE;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LaIy;->a:LbuE;

    return-object v0
.end method

.method static synthetic a(LaIy;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LaIy;->a:Ljava/util/Set;

    return-object v0
.end method

.method private static b()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    return-object v0
.end method


# virtual methods
.method public a(LaIC;LaFM;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;Landroid/net/Uri;)Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 90
    monitor-enter p0

    .line 92
    :try_start_0
    iget-object v0, p0, LaIy;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-static {}, LaIy;->a()Landroid/os/Bundle;

    move-result-object v0

    monitor-exit p0

    .line 133
    :goto_0
    return-object v0

    .line 96
    :cond_0
    iget-object v0, p0, LaIy;->a:Lafz;

    invoke-interface {v0, p2, p3}, Lafz;->a(LaFM;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Laff;

    move-result-object v3

    .line 99
    if-eqz v3, :cond_1

    invoke-interface {v3}, Laff;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    :cond_1
    invoke-static {}, LaIy;->b()Landroid/os/Bundle;

    move-result-object v0

    monitor-exit p0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 106
    :cond_2
    :try_start_1
    iget-object v0, p0, LaIy;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    invoke-interface {v3}, Laff;->a()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 111
    const-string v0, "SyncEngine"

    const-string v4, "Requesting sync for: %s, notification_uri=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v2

    aput-object p4, v5, v1

    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 113
    invoke-interface {v3}, Laff;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    const-string v0, "SyncEngine"

    const-string v1, "Not doing sync, because we already have some data"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, LaIy;->a:LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    sget v1, Lxi;->launch_drive:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaIy;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 109
    goto :goto_1

    .line 118
    :cond_4
    iget-object v0, p0, LaIy;->a:LQr;

    const-string v2, "syncMoreMaxFeedsToRetrieveImplicitly_r2"

    invoke-interface {v0, v2, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 122
    new-instance v1, LaIz;

    invoke-direct {v1, p0, p4, p1}, LaIz;-><init>(LaIy;Landroid/net/Uri;LaIC;)V

    invoke-interface {v3, v1, v0}, Laff;->a(Lafg;I)V

    .line 133
    invoke-static {}, LaIy;->a()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method
