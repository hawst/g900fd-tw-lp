.class Lbkm;
.super Ljava/lang/Object;
.source "LocalCache.java"

# interfaces
.implements LbkF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbkF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:Lbjs;

.field volatile a:LbkF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final a:Lbtd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtd",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3423
    invoke-static {}, LbjQ;->a()LbkF;

    move-result-object v0

    invoke-direct {p0, v0}, Lbkm;-><init>(LbkF;)V

    .line 3424
    return-void
.end method

.method public constructor <init>(LbkF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbkF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3419
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    iput-object v0, p0, Lbkm;->a:Lbtd;

    .line 3420
    new-instance v0, Lbjs;

    invoke-direct {v0}, Lbjs;-><init>()V

    iput-object v0, p0, Lbkm;->a:Lbjs;

    .line 3427
    iput-object p1, p0, Lbkm;->a:LbkF;

    .line 3428
    return-void
.end method

.method private a(Ljava/lang/Throwable;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 3460
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    .line 3461
    invoke-static {v0, p1}, Lbkm;->a(Lbtd;Ljava/lang/Throwable;)Z

    .line 3462
    return-object v0
.end method

.method private static a(Lbtd;Ljava/lang/Throwable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbtd",
            "<*>;",
            "Ljava/lang/Throwable;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 3452
    :try_start_0
    invoke-virtual {p0, p1}, Lbtd;->a(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3455
    :goto_0
    return v0

    .line 3453
    :catch_0
    move-exception v0

    .line 3455
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 3439
    iget-object v0, p0, Lbkm;->a:LbkF;

    invoke-interface {v0}, LbkF;->a()I

    move-result v0

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 3499
    iget-object v0, p0, Lbkm;->a:Lbjs;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lbjs;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LbkF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3511
    iget-object v0, p0, Lbkm;->a:LbkF;

    return-object v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lbkr;)LbkF;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "Lbkr",
            "<TK;TV;>;)",
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3520
    return-object p0
.end method

.method public a()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3515
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/Object;LbjM;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LbjM",
            "<-TK;TV;>;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 3479
    iget-object v0, p0, Lbkm;->a:Lbjs;

    invoke-virtual {v0}, Lbjs;->a()Lbjs;

    .line 3480
    iget-object v0, p0, Lbkm;->a:LbkF;

    invoke-interface {v0}, LbkF;->get()Ljava/lang/Object;

    move-result-object v0

    .line 3482
    if-nez v0, :cond_2

    .line 3483
    :try_start_0
    invoke-virtual {p2, p1}, LbjM;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3484
    invoke-virtual {p0, v0}, Lbkm;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lbkm;->a:Lbtd;

    .line 3494
    :cond_0
    :goto_0
    return-object v0

    .line 3484
    :cond_1
    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 3486
    :cond_2
    invoke-virtual {p2, p1, v0}, LbjM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 3488
    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 3490
    :catch_0
    move-exception v0

    .line 3491
    instance-of v1, v0, Ljava/lang/InterruptedException;

    if-eqz v1, :cond_3

    .line 3492
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 3494
    :cond_3
    invoke-virtual {p0, v0}, Lbkm;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lbkm;->a:Lbtd;

    goto :goto_0

    :cond_4
    invoke-direct {p0, v0}, Lbkm;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 3503
    iget-object v0, p0, Lbkm;->a:Lbtd;

    invoke-static {v0}, Lbtf;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 3466
    if-eqz p1, :cond_0

    .line 3469
    invoke-virtual {p0, p1}, Lbkm;->a(Ljava/lang/Object;)Z

    .line 3476
    :goto_0
    return-void

    .line 3472
    :cond_0
    invoke-static {}, LbjQ;->a()LbkF;

    move-result-object v0

    iput-object v0, p0, Lbkm;->a:LbkF;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 3431
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 3443
    iget-object v0, p0, Lbkm;->a:Lbtd;

    invoke-virtual {v0, p1}, Lbtd;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 3447
    iget-object v0, p0, Lbkm;->a:Lbtd;

    invoke-static {v0, p1}, Lbkm;->a(Lbtd;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 3435
    iget-object v0, p0, Lbkm;->a:LbkF;

    invoke-interface {v0}, LbkF;->b()Z

    move-result v0

    return v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 3507
    iget-object v0, p0, Lbkm;->a:LbkF;

    invoke-interface {v0}, LbkF;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
