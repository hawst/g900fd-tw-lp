.class public final enum LIm;
.super Ljava/lang/Enum;
.source "FastScroller.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LIm;

.field private static final synthetic a:[LIm;

.field public static final enum b:LIm;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, LIm;

    const-string v1, "ALWAYS_VISIBLE"

    invoke-direct {v0, v1, v2}, LIm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIm;->a:LIm;

    .line 62
    new-instance v0, LIm;

    const-string v1, "VISIBLE_WHEN_ACTIVE"

    invoke-direct {v0, v1, v3}, LIm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIm;->b:LIm;

    .line 60
    const/4 v0, 0x2

    new-array v0, v0, [LIm;

    sget-object v1, LIm;->a:LIm;

    aput-object v1, v0, v2

    sget-object v1, LIm;->b:LIm;

    aput-object v1, v0, v3

    sput-object v0, LIm;->a:[LIm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LIm;
    .locals 1

    .prologue
    .line 60
    const-class v0, LIm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIm;

    return-object v0
.end method

.method public static values()[LIm;
    .locals 1

    .prologue
    .line 60
    sget-object v0, LIm;->a:[LIm;

    invoke-virtual {v0}, [LIm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIm;

    return-object v0
.end method
