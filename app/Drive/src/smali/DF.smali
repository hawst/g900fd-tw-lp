.class public LDF;
.super Ljava/lang/Object;
.source "SortedMergeAdapterVisibleEntryLookup.java"

# interfaces
.implements LHp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LDC;",
        ">",
        "Ljava/lang/Object;",
        "LHp;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:LHp;

.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "LDE",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LHp;Lbjv;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LHp;",
            "Lbjv",
            "<",
            "LDE",
            "<TT;>;>;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHp;

    iput-object v0, p0, LDF;->a:LHp;

    .line 23
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    iput-object v0, p0, LDF;->a:Lbjv;

    .line 24
    if-ltz p3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "adapterIndex=%s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 25
    iput p3, p0, LDF;->a:I

    .line 26
    return-void

    :cond_0
    move v0, v2

    .line 24
    goto :goto_0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, LDF;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    .line 31
    iget v1, p0, LDF;->a:I

    invoke-virtual {v0, v1, p1}, LDE;->b(II)I

    move-result v0

    .line 32
    iget-object v1, p0, LDF;->a:LHp;

    invoke-interface {v1, v0}, LHp;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
