.class public final LHu;
.super Landroid/widget/BaseAdapter;
.source "DocGridAdapter.java"

# interfaces
.implements LAD;
.implements LKe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/BaseAdapter;",
        "LAD;",
        "LKe",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field private a:LCl;

.field private final a:LHI;

.field private final a:LII;

.field private a:LIK;

.field private final a:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "LBC;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/LayoutInflater;

.field private final a:Landroid/widget/ListView;

.field private final a:Lapd;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "LIA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "Landroid/widget/SectionIndexer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    sget v0, Lxc;->doc_list_row_group_entry_tag:I

    sput v0, LHu;->a:I

    .line 161
    sget v0, Lxe;->doc_grid_row:I

    sput v0, LHu;->b:I

    .line 162
    sget v0, Lxe;->doc_grid_title:I

    sput v0, LHu;->c:I

    .line 163
    sget v0, Lxe;->doc_grid_empty_title:I

    sput v0, LHu;->d:I

    return-void
.end method

.method private constructor <init>(Lbxw;LQX;Lapd;Landroid/widget/ListView;ILJR;LHp;LzN;LII;LsC;LHy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LQX;",
            "Lapd;",
            "Landroid/widget/ListView;",
            "I",
            "LJR;",
            "LHp;",
            "LzN;",
            "LII;",
            "LsC;",
            "LHy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 168
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LHu;->a:Ljava/util/Set;

    .line 173
    const/4 v0, -0x1

    iput v0, p0, LHu;->f:I

    .line 195
    if-lez p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 196
    invoke-interface {p1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LHu;->a:Landroid/content/Context;

    .line 198
    iget-object v0, p0, LHu;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LHu;->a:Landroid/view/LayoutInflater;

    .line 199
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapd;

    iput-object v0, p0, LHu;->a:Lapd;

    .line 200
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, LHu;->a:Landroid/widget/ListView;

    .line 201
    iget-object v0, p0, LHu;->a:Landroid/content/Context;

    invoke-static {v0, p5, p10}, LHu;->a(Landroid/content/Context;ILsC;)I

    move-result v0

    iput v0, p0, LHu;->e:I

    .line 203
    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LII;

    iput-object v0, p0, LHu;->a:LII;

    .line 204
    invoke-virtual {p2}, LQX;->a()LIK;

    move-result-object v0

    iput-object v0, p0, LHu;->a:LIK;

    .line 206
    new-instance v0, LHv;

    invoke-direct {v0, p0}, LHv;-><init>(LHu;)V

    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LHu;->b:Laku;

    .line 212
    new-instance v0, LHw;

    invoke-direct {v0, p0}, LHw;-><init>(LHu;)V

    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LHu;->a:Laku;

    .line 218
    new-instance v0, LHx;

    invoke-direct {v0, p0}, LHx;-><init>(LHu;)V

    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LHu;->c:Laku;

    .line 226
    invoke-virtual {p6, p0}, LJR;->a(LKe;)V

    .line 228
    new-instance v0, LHJ;

    iget-object v1, p0, LHu;->a:Laku;

    invoke-direct {v0, p7, v1}, LHJ;-><init>(LHp;Lbjv;)V

    .line 230
    new-instance v1, LHP;

    iget-object v2, p0, LHu;->a:Laku;

    invoke-direct {v1, p8, v2}, LHP;-><init>(LzN;Lbjv;)V

    .line 232
    invoke-interface {p11, v0, v1}, LHy;->a(LHp;LzN;)LHI;

    move-result-object v0

    iput-object v0, p0, LHu;->a:LHI;

    .line 233
    return-void

    .line 195
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lbxw;LQX;Lapd;Landroid/widget/ListView;ILJR;LHp;LzN;LII;LsC;LHy;LHv;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct/range {p0 .. p11}, LHu;-><init>(Lbxw;LQX;Lapd;Landroid/widget/ListView;ILJR;LHp;LzN;LII;LsC;LHy;)V

    return-void
.end method

.method static synthetic a(LHu;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, LHu;->e:I

    return v0
.end method

.method private static a(Landroid/content/Context;ILsC;)I
    .locals 4

    .prologue
    .line 237
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 238
    invoke-interface {p2, v0}, LsC;->a(Landroid/content/res/Resources;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v1

    .line 239
    sget v2, Lxa;->doc_grid_item_spacing:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 240
    sget v3, Lxa;->doc_grid_card_border_size:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 241
    invoke-virtual {v1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v1

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 242
    const/4 v1, 0x2

    div-int v0, p1, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 243
    return v0
.end method

.method private a(I)LHA;
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    .line 270
    invoke-virtual {v0}, LBC;->a()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 272
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 274
    :cond_0
    invoke-virtual {v0, p1}, LBC;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 275
    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    invoke-virtual {v0}, LBF;->a()LIB;

    move-result-object v0

    invoke-interface {v0}, LIB;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    sget-object v0, LHA;->a:LHA;

    .line 283
    :goto_0
    return-object v0

    .line 278
    :cond_1
    sget-object v0, LHA;->d:LHA;

    goto :goto_0

    .line 280
    :cond_2
    invoke-direct {p0, p1}, LHu;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 281
    sget-object v0, LHA;->c:LHA;

    goto :goto_0

    .line 283
    :cond_3
    sget-object v0, LHA;->b:LHA;

    goto :goto_0
.end method

.method static synthetic a(LHu;)LHI;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LHu;->a:LHI;

    return-object v0
.end method

.method static synthetic a(LHu;)LII;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LHu;->a:LII;

    return-object v0
.end method

.method static synthetic a(LHu;)LIK;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LHu;->a:LIK;

    return-object v0
.end method

.method static synthetic a(LHu;)Laku;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LHu;->b:Laku;

    return-object v0
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 311
    if-nez p2, :cond_0

    iget-object v0, p0, LHu;->a:Landroid/view/LayoutInflater;

    sget v1, LHu;->d:I

    .line 312
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 313
    :cond_0
    sget v0, Lxc;->empty_group_title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 314
    return-object p2
.end method

.method static synthetic a(LHu;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, LHu;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;
    .locals 3

    .prologue
    .line 373
    instance-of v0, p1, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    move-object v0, p1

    .line 375
    :goto_0
    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, LHu;->a:Landroid/view/LayoutInflater;

    sget v1, LHu;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    .line 378
    :cond_0
    return-object v0

    .line 373
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 288
    sget-object v0, LCe;->o:LCe;

    iget-object v1, p0, LHu;->a:LCl;

    invoke-virtual {v0, v1}, LCe;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LHu;->a:LIK;

    sget-object v1, LIK;->h:LIK;

    .line 289
    invoke-virtual {v0, v1}, LIK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const/4 v0, 0x0

    .line 296
    :goto_0
    return v0

    .line 294
    :cond_0
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    invoke-virtual {v0}, LBF;->a()I

    move-result v0

    .line 295
    iget-object v1, p0, LHu;->a:LHI;

    invoke-interface {v1, v0}, LHI;->a(I)LaGv;

    move-result-object v0

    .line 296
    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 318
    if-nez p2, :cond_0

    iget-object v0, p0, LHu;->a:Landroid/view/LayoutInflater;

    sget v1, LHu;->c:I

    const/4 v2, 0x0

    .line 319
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 320
    :cond_0
    invoke-static {p2}, LCy;->a(Landroid/view/View;)LCy;

    move-result-object v1

    .line 321
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v2

    .line 322
    invoke-virtual {v2}, LBF;->a()LIB;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3}, LIB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LCy;->a(Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, LHu;->b:Laku;

    .line 324
    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIA;

    invoke-virtual {v2}, LBF;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LIA;->a(I)LIB;

    move-result-object v0

    .line 323
    invoke-virtual {v1, v0}, LCy;->a(LIB;)V

    .line 325
    return-object p2
.end method

.method static synthetic b(LHu;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, LHu;->c(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 329
    invoke-direct {p0, p2, p3}, LHu;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    move-result-object v5

    .line 330
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    .line 331
    invoke-virtual {v0}, LBF;->a()I

    move-result v7

    .line 332
    invoke-virtual {v0}, LBF;->b()I

    move-result v8

    move v3, v6

    .line 334
    :goto_0
    iget v0, p0, LHu;->e:I

    if-ge v3, v0, :cond_4

    .line 335
    invoke-virtual {v5}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildCount()I

    move-result v0

    if-ge v3, v0, :cond_2

    invoke-virtual {v5, v3}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 337
    :goto_1
    if-ge v3, v8, :cond_3

    .line 338
    iget-object v0, p0, LHu;->a:LHI;

    add-int v1, v7, v3

    move v2, p1

    invoke-interface/range {v0 .. v5}, LHI;->a(IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 346
    :goto_2
    if-eq v0, v4, :cond_1

    .line 347
    if-eqz v4, :cond_0

    .line 348
    const-string v1, "DocGridAdapter"

    const-string v2, "Throwing away view %s - this could result in jankiness later"

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v4, v9, v6

    invoke-static {v1, v2, v9}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 351
    invoke-virtual {v5, v3}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->removeViewAt(I)V

    .line 353
    :cond_0
    invoke-virtual {v5, v0, v3}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->addView(Landroid/view/View;I)V

    .line 334
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 335
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 342
    :cond_3
    iget-object v0, p0, LHu;->a:LHI;

    move v1, v7

    move v2, p1

    invoke-interface/range {v0 .. v5}, LHI;->a(IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 343
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 359
    :cond_4
    invoke-virtual {v5}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildCount()I

    move-result v0

    iget v1, p0, LHu;->e:I

    if-le v0, v1, :cond_5

    .line 361
    const-string v0, "DocGridAdapter"

    const-string v1, "row %d has more children that it needs: %d > %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v5}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    const/4 v3, 0x2

    iget v4, p0, LHu;->e:I

    .line 362
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 361
    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 363
    invoke-virtual {v5}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_3
    iget v1, p0, LHu;->e:I

    if-lt v0, v1, :cond_5

    .line 364
    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 363
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 368
    :cond_5
    return-object v5
.end method

.method static synthetic c(LHu;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, LHu;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, LHu;->b:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 431
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 432
    iget-object v0, p0, LHu;->c:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 433
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 574
    invoke-virtual {p0, p1}, LHu;->d(I)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;)LIB;
    .locals 1

    .prologue
    .line 468
    sget v0, LHu;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIB;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, LHu;->a:LHI;

    invoke-interface {v0}, LHI;->b()V

    .line 493
    return-void
.end method

.method public a(LCs;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 519
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    if-ltz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 522
    :goto_1
    iget-object v0, p0, LHu;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge p2, v0, :cond_4

    .line 523
    iget-object v0, p0, LHu;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p0, v0}, LHu;->d(I)I

    move-result v3

    .line 524
    if-gez v3, :cond_2

    .line 522
    :cond_0
    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 520
    goto :goto_0

    .line 528
    :cond_2
    iget-object v0, p0, LHu;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 530
    instance-of v2, v2, LCy;

    if-eqz v2, :cond_3

    .line 531
    invoke-interface {p1, v0, v3}, LCs;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 532
    :cond_3
    instance-of v2, v0, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    if-eqz v2, :cond_0

    .line 533
    check-cast v0, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    move v2, v1

    .line 535
    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 536
    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 538
    add-int v5, v3, v2

    invoke-interface {p1, v4, v5}, LCs;->a(Landroid/view/View;I)V

    .line 535
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 542
    :cond_4
    return-void
.end method

.method public a(LQX;)V
    .locals 1

    .prologue
    .line 413
    invoke-direct {p0}, LHu;->c()V

    .line 414
    iget-object v0, p0, LHu;->a:Lapd;

    invoke-virtual {v0}, Lapd;->a()V

    .line 415
    invoke-virtual {p1}, LQX;->a()LCl;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    iput-object v0, p0, LHu;->a:LCl;

    .line 416
    iget-object v0, p0, LHu;->a:LHI;

    invoke-interface {v0, p1}, LHI;->a(LQX;)V

    .line 417
    invoke-virtual {p0}, LHu;->notifyDataSetChanged()V

    .line 418
    return-void
.end method

.method public a(LaFX;)V
    .locals 1

    .prologue
    .line 422
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    invoke-direct {p0}, LHu;->c()V

    .line 424
    iget-object v0, p0, LHu;->a:LHI;

    invoke-interface {v0, p1}, LHI;->a(LaFX;)V

    .line 425
    invoke-virtual {p0}, LHu;->notifyDataSetChanged()V

    .line 426
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 503
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 504
    instance-of v1, v0, LCy;

    if-eqz v1, :cond_0

    .line 505
    check-cast v0, LCy;

    invoke-virtual {v0}, LCy;->c()V

    .line 507
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0}, LBC;->a()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 497
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 498
    if-eqz v0, :cond_0

    instance-of v0, v0, LCy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 579
    invoke-virtual {p0, p1}, LHu;->e(I)I

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, LHu;->a:LHI;

    invoke-interface {v0}, LHI;->a()V

    .line 488
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 511
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 512
    instance-of v1, v0, LCy;

    if-eqz v1, :cond_0

    .line 513
    check-cast v0, LCy;

    invoke-virtual {v0}, LCy;->d()V

    .line 515
    :cond_0
    return-void
.end method

.method public c(I)I
    .locals 1

    .prologue
    .line 584
    invoke-virtual {p0, p1}, LHu;->e(I)I

    move-result v0

    return v0
.end method

.method public c_()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 558
    move v1, v2

    :goto_0
    iget-object v0, p0, LHu;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 559
    iget-object v0, p0, LHu;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 560
    instance-of v3, v0, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    if-eqz v3, :cond_0

    .line 561
    check-cast v0, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;

    move v3, v2

    .line 563
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 564
    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/view/DocGridRowLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 566
    iget-object v5, p0, LHu;->a:LHI;

    invoke-interface {v5, v4}, LHI;->a(Landroid/view/View;)V

    .line 563
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 558
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 570
    :cond_1
    return-void
.end method

.method public d(I)I
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    .line 546
    invoke-virtual {v0}, LBC;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 547
    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    invoke-virtual {v0}, LBF;->a()I

    move-result v0

    .line 549
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public e(I)I
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0}, LBC;->a()I

    move-result v0

    .line 249
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 259
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 264
    invoke-direct {p0, p1}, LHu;->a(I)LHA;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, LHA;->a()I

    move-result v0

    return v0
.end method

.method public getPositionForSection(I)I
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, LHu;->c:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v1

    .line 443
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, v1}, LBC;->a(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    .line 449
    invoke-virtual {v0}, LBF;->a()I

    move-result v1

    .line 450
    iget-object v0, p0, LHu;->c:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0, v1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, LHu;->c:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 301
    invoke-direct {p0, p1}, LHu;->a(I)LHA;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, LHA;->a(LHu;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 302
    iget-object v0, p0, LHu;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v2

    .line 303
    iget-object v0, p0, LHu;->b:Laku;

    .line 304
    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIA;

    invoke-virtual {v2}, LBF;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LIA;->a(I)LIB;

    move-result-object v0

    .line 305
    sget v2, LHu;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 306
    return-object v1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 383
    invoke-static {}, LHA;->values()[LHA;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, LHu;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    .line 461
    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_0

    .line 463
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 478
    iget v0, p0, LHu;->f:I

    if-eq v0, p2, :cond_0

    .line 479
    iput p2, p0, LHu;->f:I

    .line 480
    iget-object v0, p0, LHu;->a:Lapd;

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v1, p2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lapd;->a(J)V

    .line 482
    :cond_0
    iget-object v0, p0, LHu;->a:LHI;

    invoke-interface {v0, p1, p2, p3, p4}, LHI;->onScroll(Landroid/widget/AbsListView;III)V

    .line 483
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 473
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, LHu;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 394
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, LHu;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 399
    return-void
.end method
