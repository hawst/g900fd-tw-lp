.class abstract Lbkj;
.super Ljava/lang/Object;
.source "LocalCache.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:I

.field a:LbkS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjQ",
            "<TK;TV;>.bkS;"
        }
    .end annotation
.end field

.field a:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field a:Lbks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbks",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lbkr",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field b:I

.field final synthetic b:LbjQ;

.field b:LbkS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjQ",
            "<TK;TV;>.bkS;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbjQ;)V
    .locals 1

    .prologue
    .line 4194
    iput-object p1, p0, Lbkj;->b:LbjQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4195
    iget-object v0, p1, LbjQ;->a:[Lbks;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbkj;->a:I

    .line 4196
    const/4 v0, -0x1

    iput v0, p0, Lbkj;->b:I

    .line 4197
    invoke-virtual {p0}, Lbkj;->a()V

    .line 4198
    return-void
.end method


# virtual methods
.method a()LbkS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbjQ",
            "<TK;TV;>.bkS;"
        }
    .end annotation

    .prologue
    .line 4279
    iget-object v0, p0, Lbkj;->a:LbkS;

    if-nez v0, :cond_0

    .line 4280
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 4282
    :cond_0
    iget-object v0, p0, Lbkj;->a:LbkS;

    iput-object v0, p0, Lbkj;->b:LbkS;

    .line 4283
    invoke-virtual {p0}, Lbkj;->a()V

    .line 4284
    iget-object v0, p0, Lbkj;->b:LbkS;

    return-object v0
.end method

.method final a()V
    .locals 3

    .prologue
    .line 4203
    const/4 v0, 0x0

    iput-object v0, p0, Lbkj;->a:LbkS;

    .line 4205
    invoke-virtual {p0}, Lbkj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4223
    :cond_0
    :goto_0
    return-void

    .line 4209
    :cond_1
    invoke-virtual {p0}, Lbkj;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4213
    :cond_2
    iget v0, p0, Lbkj;->a:I

    if-ltz v0, :cond_0

    .line 4214
    iget-object v0, p0, Lbkj;->b:LbjQ;

    iget-object v0, v0, LbjQ;->a:[Lbks;

    iget v1, p0, Lbkj;->a:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lbkj;->a:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lbkj;->a:Lbks;

    .line 4215
    iget-object v0, p0, Lbkj;->a:Lbks;

    iget v0, v0, Lbks;->a:I

    if-eqz v0, :cond_2

    .line 4216
    iget-object v0, p0, Lbkj;->a:Lbks;

    iget-object v0, v0, Lbks;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lbkj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 4217
    iget-object v0, p0, Lbkj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbkj;->b:I

    .line 4218
    invoke-virtual {p0}, Lbkj;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 4229
    iget-object v0, p0, Lbkj;->a:Lbkr;

    if-eqz v0, :cond_1

    .line 4230
    iget-object v0, p0, Lbkj;->a:Lbkr;

    invoke-interface {v0}, Lbkr;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, Lbkj;->a:Lbkr;

    :goto_0
    iget-object v0, p0, Lbkj;->a:Lbkr;

    if-eqz v0, :cond_1

    .line 4231
    iget-object v0, p0, Lbkj;->a:Lbkr;

    invoke-virtual {p0, v0}, Lbkj;->a(Lbkr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4232
    const/4 v0, 0x1

    .line 4236
    :goto_1
    return v0

    .line 4230
    :cond_0
    iget-object v0, p0, Lbkj;->a:Lbkr;

    invoke-interface {v0}, Lbkr;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, Lbkj;->a:Lbkr;

    goto :goto_0

    .line 4236
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lbkr;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 4259
    :try_start_0
    iget-object v0, p0, Lbkj;->b:LbjQ;

    iget-object v0, v0, LbjQ;->a:LbjA;

    invoke-virtual {v0}, LbjA;->a()J

    move-result-wide v0

    .line 4260
    invoke-interface {p1}, Lbkr;->a()Ljava/lang/Object;

    move-result-object v2

    .line 4261
    iget-object v3, p0, Lbkj;->b:LbjQ;

    invoke-virtual {v3, p1, v0, v1}, LbjQ;->a(Lbkr;J)Ljava/lang/Object;

    move-result-object v0

    .line 4262
    if-eqz v0, :cond_0

    .line 4263
    new-instance v1, LbkS;

    iget-object v3, p0, Lbkj;->b:LbjQ;

    invoke-direct {v1, v3, v2, v0}, LbkS;-><init>(LbjQ;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lbkj;->a:LbkS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4264
    const/4 v0, 0x1

    .line 4270
    iget-object v1, p0, Lbkj;->a:Lbks;

    invoke-virtual {v1}, Lbks;->l()V

    :goto_0
    return v0

    .line 4267
    :cond_0
    const/4 v0, 0x0

    .line 4270
    iget-object v1, p0, Lbkj;->a:Lbks;

    invoke-virtual {v1}, Lbks;->l()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbkj;->a:Lbks;

    invoke-virtual {v1}, Lbks;->l()V

    throw v0
.end method

.method b()Z
    .locals 3

    .prologue
    .line 4243
    :cond_0
    iget v0, p0, Lbkj;->b:I

    if-ltz v0, :cond_2

    .line 4244
    iget-object v0, p0, Lbkj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lbkj;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lbkj;->b:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkr;

    iput-object v0, p0, Lbkj;->a:Lbkr;

    if-eqz v0, :cond_0

    .line 4245
    iget-object v0, p0, Lbkj;->a:Lbkr;

    invoke-virtual {p0, v0}, Lbkj;->a(Lbkr;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbkj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4246
    :cond_1
    const/4 v0, 0x1

    .line 4250
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 4275
    iget-object v0, p0, Lbkj;->a:LbkS;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 4288
    iget-object v0, p0, Lbkj;->b:LbkS;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 4289
    iget-object v0, p0, Lbkj;->b:LbjQ;

    iget-object v1, p0, Lbkj;->b:LbkS;

    invoke-virtual {v1}, LbkS;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LbjQ;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4290
    const/4 v0, 0x0

    iput-object v0, p0, Lbkj;->b:LbkS;

    .line 4291
    return-void

    .line 4288
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
