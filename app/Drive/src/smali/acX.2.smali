.class LacX;
.super Ljava/lang/Object;
.source "ContentExposer.java"

# interfaces
.implements LadC;


# instance fields
.field private final a:LaGo;

.field private final a:LaGp;

.field private final a:Ladi;

.field private a:Ladj;

.field private final a:LtK;


# direct methods
.method private constructor <init>(LaGp;LaGo;Ladi;LtK;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGp;

    iput-object v0, p0, LacX;->a:LaGp;

    .line 49
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    iput-object v0, p0, LacX;->a:Ladi;

    .line 50
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LacX;->a:LtK;

    .line 52
    if-nez p2, :cond_0

    .line 53
    invoke-virtual {p1}, LaGp;->a()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {p1}, LaGp;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 52
    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 55
    iput-object p2, p0, LacX;->a:LaGo;

    .line 56
    return-void

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(LaGp;LaGo;Ladi;LtK;LacW;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, LacX;-><init>(LaGp;LaGo;Ladi;LtK;)V

    return-void
.end method

.method private a()Ljava/io/File;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, LacX;->a:LaGp;

    invoke-virtual {v0}, LaGp;->a()Ljava/io/File;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, LacX;->a()Ljava/io/File;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public a(LbmY;)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LadI;",
            ">;)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, LacX;->a:Ladj;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 74
    iget-object v0, p0, LacX;->a:LtK;

    sget-object v1, Lry;->aP:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, LadI;->b:LadI;

    .line 75
    invoke-virtual {p1, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LadI;->c:LadI;

    invoke-virtual {p1, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    :cond_0
    new-instance v0, LadB;

    const-string v1, "Writing is not supported"

    invoke-direct {v0, v1}, LadB;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 79
    :cond_2
    invoke-static {p1}, LadI;->a(LbmY;)I

    move-result v1

    .line 80
    if-nez v1, :cond_3

    .line 81
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Unsupported permission"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_3
    iget-object v0, p0, LacX;->a:LaGo;

    if-nez v0, :cond_4

    .line 85
    invoke-direct {p0}, LacX;->a()Ljava/io/File;

    move-result-object v0

    .line 86
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 96
    :goto_1
    return-object v0

    .line 89
    :cond_4
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    iget-object v2, p0, LacX;->a:LaGo;

    invoke-interface {v2}, LaGo;->a()LaGv;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v0

    .line 91
    :try_start_0
    iget-object v2, p0, LacX;->a:Ladi;

    iget-object v3, p0, LacX;->a:LaGp;

    sget-object v4, Ladk;->a:Ladk;

    iget-object v5, p0, LacX;->a:LaGo;

    invoke-interface {v2, v3, v0, v4, v5}, Ladi;->a(LaGp;LacY;Lamr;LaGo;)LbsU;

    move-result-object v0

    .line 92
    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    iput-object v0, p0, LacX;->a:Ladj;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    iget-object v0, p0, LacX;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    .line 93
    :catch_0
    move-exception v0

    .line 94
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Error opening file"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LacX;->a:LaGp;

    invoke-virtual {v0}, LaGp;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, LacX;->a:LaGo;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, LacX;->a:LaGo;

    invoke-interface {v0}, LaGo;->c()Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v1, p0, LacX;->a:LaGo;

    invoke-interface {v1}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-virtual {v1}, LaGv;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".pdf"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    :cond_0
    :goto_0
    return-object v0

    .line 122
    :cond_1
    invoke-direct {p0}, LacX;->a()Ljava/io/File;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, LacX;->a:Ladj;

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "ContentExposer"

    const-string v1, "Close the DocumentFile"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p0, LacX;->a:Ladj;

    invoke-interface {v0}, Ladj;->close()V

    .line 69
    :cond_0
    return-void
.end method
