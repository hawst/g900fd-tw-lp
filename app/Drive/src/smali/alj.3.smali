.class Lalj;
.super Ljava/lang/Object;
.source "FeedbackReport.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lali;


# direct methods
.method constructor <init>(Lali;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lalj;->a:Lali;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 122
    :try_start_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 127
    :try_start_1
    iget-object v2, p0, Lalj;->a:Lali;

    invoke-static {v2}, Lali;->a(Lali;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 131
    :try_start_2
    invoke-virtual {v0}, Landroid/view/View;->isDrawingCacheEnabled()Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v2

    .line 132
    if-nez v2, :cond_0

    .line 133
    const/4 v5, 0x1

    :try_start_3
    invoke-virtual {v0, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    move v1, v3

    .line 138
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 141
    if-eqz v3, :cond_1

    .line 142
    const/high16 v5, 0x400000

    invoke-static {v3, v5}, Lali;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 145
    if-eqz v3, :cond_1

    .line 146
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 155
    :cond_1
    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    .line 156
    const/4 v1, 0x0

    :try_start_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 160
    :cond_2
    :goto_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p2, v0, v4, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 165
    iget-object v0, p0, Lalj;->a:Lali;

    invoke-static {v0}, Lali;->a(Lali;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 167
    :goto_1
    return-void

    .line 149
    :catch_0
    move-exception v2

    move v2, v3

    .line 155
    :goto_2
    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    .line 156
    const/4 v1, 0x0

    :try_start_5
    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 161
    :catch_1
    move-exception v0

    .line 162
    :try_start_6
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 165
    iget-object v0, p0, Lalj;->a:Lali;

    invoke-static {v0}, Lali;->a(Lali;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_1

    .line 151
    :catch_2
    move-exception v2

    .line 155
    :goto_3
    if-eqz v1, :cond_2

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    .line 156
    const/4 v1, 0x0

    :try_start_7
    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lalj;->a:Lali;

    invoke-static {v1}, Lali;->a(Lali;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    throw v0

    .line 155
    :catchall_1
    move-exception v2

    move-object v6, v2

    move v2, v1

    move-object v1, v0

    move-object v0, v6

    :goto_4
    if-eqz v2, :cond_3

    if-nez v3, :cond_3

    if-eqz v1, :cond_3

    .line 156
    const/4 v2, 0x0

    :try_start_8
    invoke-virtual {v1, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    :cond_3
    throw v0
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 155
    :catchall_2
    move-exception v2

    move-object v6, v2

    move v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    :catchall_3
    move-exception v3

    move-object v6, v3

    move v3, v2

    move v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    .line 151
    :catch_3
    move-exception v3

    move v3, v2

    goto :goto_3

    .line 149
    :catch_4
    move-exception v3

    goto :goto_2
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 171
    return-void
.end method
