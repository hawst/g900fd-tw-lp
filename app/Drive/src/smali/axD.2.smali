.class LaxD;
.super Laxy;
.source "PdfLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Laxz;


# direct methods
.method constructor <init>(Laxz;)V
    .locals 1

    .prologue
    .line 442
    iput-object p1, p0, LaxD;->a:Laxz;

    .line 443
    sget-object v0, LaxR;->h:LaxR;

    invoke-direct {p0, p1, v0}, Laxy;-><init>(Laxz;LaxR;)V

    .line 444
    return-void
.end method


# virtual methods
.method protected a(LavE;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 449
    :try_start_0
    invoke-interface {p1}, LavE;->a()V

    .line 450
    iget-object v0, p0, LaxD;->a:Laxz;

    iget-object v0, v0, Laxz;->a:LaxE;

    invoke-static {v0}, LaxE;->a(LaxE;)V

    .line 451
    iget-object v0, p0, LaxD;->a:Laxz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laxz;->a(Laxz;LauW;)LauW;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    :goto_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 452
    :catch_0
    move-exception v0

    .line 453
    const-string v1, "PdfLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on Destroy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0, p1}, LaxD;->a(LavE;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 461
    return-void
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 441
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, LaxD;->a(LaxH;Ljava/lang/Boolean;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 464
    const-string v0, "CloseTask"

    return-object v0
.end method
