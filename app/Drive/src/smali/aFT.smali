.class public final LaFT;
.super LaGm;
.source "CachedSearch.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEs;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final a:Ljava/lang/String;

.field private a:Z

.field private final b:J


# direct methods
.method public constructor <init>(LaEz;JLjava/lang/String;J)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    invoke-static {}, LaEs;->a()LaEs;

    move-result-object v0

    sget-object v3, LaEG;->f:LaEG;

    .line 31
    invoke-virtual {v3}, LaEG;->a()Landroid/net/Uri;

    move-result-object v3

    .line 30
    invoke-direct {p0, p1, v0, v3}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 22
    iput-boolean v2, p0, LaFT;->a:Z

    .line 32
    iput-wide p2, p0, LaFT;->a:J

    .line 33
    cmp-long v0, p2, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "not persisted: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LbiT;->a(ZLjava/lang/Object;)V

    .line 34
    const-string v0, "null query"

    invoke-static {p4, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaFT;->a:Ljava/lang/String;

    .line 35
    cmp-long v0, p5, v6

    if-ltz v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid timestamp: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LbiT;->a(ZLjava/lang/Object;)V

    .line 36
    iput-wide p5, p0, LaFT;->b:J

    .line 37
    return-void

    :cond_0
    move v0, v2

    .line 33
    goto :goto_0

    :cond_1
    move v1, v2

    .line 35
    goto :goto_1
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, LaFT;->b:J

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LaFT;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, LaFT;->a:Z

    .line 49
    return-void
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 41
    sget-object v0, LaEt;->a:LaEt;

    invoke-virtual {v0}, LaEt;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaFT;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 42
    sget-object v0, LaEt;->b:LaEt;

    invoke-virtual {v0}, LaEt;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaFT;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    sget-object v0, LaEt;->c:LaEt;

    invoke-virtual {v0}, LaEt;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaFT;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 44
    sget-object v0, LaEt;->d:LaEt;

    invoke-virtual {v0}, LaEt;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    iget-boolean v0, p0, LaFT;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 45
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, LaFT;->a:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 65
    const-string v0, "CachedSearch[accountSqlId=%s, query=%s, timestamp=%s, completed=%s, sqlId=%s]"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 66
    invoke-virtual {p0}, LaFT;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaFT;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, LaFT;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, LaFT;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, LaFT;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 65
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
