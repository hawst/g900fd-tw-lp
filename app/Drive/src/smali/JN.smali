.class LJN;
.super Ljava/lang/Object;
.source "DocListMenuImpl.java"

# interfaces
.implements LJM;


# instance fields
.field a:LCU;

.field a:LFF;

.field a:LJJ;

.field a:LJK;

.field a:LJR;

.field a:LQa;

.field a:LQr;

.field a:LWz;

.field a:LaFO;

.field a:Landroid/content/Context;

.field a:LsC;

.field a:LtK;

.field a:Lve;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V
    .locals 5

    .prologue
    .line 93
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 95
    if-nez v0, :cond_0

    .line 96
    const-string v0, "DocListMenuImpl"

    const-string v1, "Menu layout does not contain requested item id: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 142
    :goto_0
    return-void

    .line 100
    :cond_0
    if-eqz p3, :cond_1

    .line 101
    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 104
    :cond_1
    new-instance v1, LJO;

    invoke-direct {v1, p0, p2, p4}, LJO;-><init>(LJN;ILJJ;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/Menu;LaqY;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 146
    iget-object v0, p0, LJN;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v1, p0, LJN;->a:LsC;

    .line 147
    invoke-interface {v1}, LsC;->b()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 149
    iget-object v0, p0, LJN;->a:Landroid/content/Context;

    invoke-static {v0}, LUs;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LJN;->a:LtK;

    sget-object v1, Lry;->am:Lry;

    .line 150
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    :cond_0
    sget v0, Lxc;->menu_refresh_icon:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 157
    :goto_0
    sget v0, Lxc;->menu_create_new_doc:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 160
    iget-object v0, p0, LJN;->a:LsC;

    invoke-interface {v0}, LsC;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 161
    sget v0, Lxc;->menu_create_new_doc:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 167
    :goto_1
    iget-object v0, p0, LJN;->a:LsC;

    invoke-interface {v0}, LsC;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 170
    sget v0, Lxc;->menu_filter_by:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 176
    :goto_2
    sget v0, Lxc;->menu_sortings:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 179
    sget v0, Lxc;->menu_settings:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 182
    iget-object v0, p0, LJN;->a:LtK;

    sget-object v1, Lry;->L:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 183
    sget v0, Lxc;->menu_list_mode:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 184
    sget v0, Lxc;->menu_grid_mode:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 192
    :goto_3
    iget-object v0, p0, LJN;->a:LQr;

    const-string v1, "enableMenuHelpMinApi"

    const/16 v2, 0x8

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 194
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v0, :cond_5

    .line 195
    sget v0, Lxc;->menu_help:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 201
    :goto_4
    sget v0, Lxc;->menu_send_feedback:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 203
    sget v0, Lxc;->menu_search:I

    .line 204
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, LJN;->a:LJJ;

    .line 203
    invoke-interface {p2, v0, v1}, LaqY;->a(Landroid/view/MenuItem;Lara;)V

    .line 206
    iget-object v0, p0, LJN;->a:LsC;

    invoke-interface {v0}, LsC;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LJN;->a:LtK;

    sget-object v1, Lry;->Z:Lry;

    .line 207
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LJN;->a:LQa;

    if-eqz v0, :cond_6

    .line 209
    sget v0, Lxc;->menu_open_with_picker:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 215
    :goto_5
    iget-object v0, p0, LJN;->a:LCU;

    invoke-virtual {v0}, LCU;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 216
    iget-object v0, p0, LJN;->a:LJR;

    invoke-virtual {v0}, LJR;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 217
    sget v0, Lxc;->menu_selection_clear:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 218
    sget v0, Lxc;->menu_selection_start:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 223
    :goto_6
    sget v0, Lxc;->menu_selection_all:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 230
    :goto_7
    iget-object v0, p0, LJN;->a:Lve;

    if-eqz v0, :cond_9

    iget-object v0, p0, LJN;->a:LWz;

    if-eqz v0, :cond_9

    iget-object v0, p0, LJN;->a:LWz;

    .line 232
    invoke-interface {v0}, LWz;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 233
    sget v0, Lxc;->menu_add_new_office_doc:I

    iget-object v1, p0, LJN;->a:Landroid/content/Context;

    iget-object v2, p0, LJN;->a:Lve;

    .line 234
    invoke-interface {v2}, Lve;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LJN;->a:LJJ;

    .line 233
    invoke-direct {p0, p1, v0, v1, v2}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 239
    :goto_8
    return-void

    .line 154
    :cond_1
    sget v0, Lxc;->menu_refresh_icon:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_0

    .line 164
    :cond_2
    sget v0, Lxc;->menu_create_new_doc:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    goto/16 :goto_1

    .line 172
    :cond_3
    sget v0, Lxc;->menu_filter_by:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    goto/16 :goto_2

    .line 186
    :cond_4
    sget v0, Lxc;->menu_list_mode:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 187
    sget v0, Lxc;->menu_grid_mode:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    goto/16 :goto_3

    .line 197
    :cond_5
    sget v0, Lxc;->menu_help:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    goto/16 :goto_4

    .line 211
    :cond_6
    sget v0, Lxc;->menu_open_with_picker:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_5

    .line 220
    :cond_7
    sget v0, Lxc;->menu_selection_start:I

    iget-object v1, p0, LJN;->a:LJJ;

    invoke-direct {p0, p1, v0, v3, v1}, LJN;->a(Landroid/view/Menu;ILjava/lang/String;LJJ;)V

    .line 221
    sget v0, Lxc;->menu_selection_clear:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_6

    .line 225
    :cond_8
    sget v0, Lxc;->menu_selection_start:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 226
    sget v0, Lxc;->menu_selection_all:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 227
    sget v0, Lxc;->menu_selection_clear:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_7

    .line 237
    :cond_9
    sget v0, Lxc;->menu_add_new_office_doc:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_8
.end method
