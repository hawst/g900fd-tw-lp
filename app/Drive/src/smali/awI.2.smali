.class public LawI;
.super Landroid/graphics/drawable/Drawable;
.source "HighlightOverlay.java"


# static fields
.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, LawI;->d:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;)V
    .locals 3

    .prologue
    .line 40
    sget-object v0, LawI;->d:Ljava/util/List;

    sget-object v1, LawI;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->flatten()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LawI;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;I)V
    .locals 3

    .prologue
    .line 45
    sget-object v0, LawI;->d:Ljava/util/List;

    invoke-virtual {p1, p2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->get(I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1, p2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->flattenExcludingMatch(I)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LawI;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/viewer/pdflib/Selection;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p1, Lcom/google/android/apps/viewer/pdflib/Selection;->rects:Ljava/util/List;

    sget-object v1, LawI;->d:Ljava/util/List;

    sget-object v2, LawI;->d:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2}, LawI;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 36
    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 28
    iput-object p1, p0, LawI;->a:Ljava/util/List;

    .line 29
    iput-object p2, p0, LawI;->b:Ljava/util/List;

    .line 30
    iput-object p3, p0, LawI;->c:Ljava/util/List;

    .line 31
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, LawI;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 51
    sget-object v2, LawJ;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, LawI;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 54
    sget-object v2, LawJ;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 56
    :cond_1
    iget-object v0, p0, LawI;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 57
    sget-object v2, LawJ;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 59
    :cond_2
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public setAlpha(I)V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setAlpha"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setColorFilter"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
