.class final enum LavQ;
.super Ljava/lang/Enum;
.source "Analytics.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LavQ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LavQ;

.field private static final synthetic a:[LavQ;

.field public static final enum b:LavQ;

.field public static final enum c:LavQ;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, LavQ;

    const-string v1, "ACTION"

    invoke-direct {v0, v1, v2}, LavQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LavQ;->a:LavQ;

    .line 34
    new-instance v0, LavQ;

    const-string v1, "FILE_SOURCE"

    invoke-direct {v0, v1, v3}, LavQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LavQ;->b:LavQ;

    .line 35
    new-instance v0, LavQ;

    const-string v1, "PDF_INFO"

    invoke-direct {v0, v1, v4}, LavQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LavQ;->c:LavQ;

    .line 32
    const/4 v0, 0x3

    new-array v0, v0, [LavQ;

    sget-object v1, LavQ;->a:LavQ;

    aput-object v1, v0, v2

    sget-object v1, LavQ;->b:LavQ;

    aput-object v1, v0, v3

    sget-object v1, LavQ;->c:LavQ;

    aput-object v1, v0, v4

    sput-object v0, LavQ;->a:[LavQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LavQ;
    .locals 1

    .prologue
    .line 32
    const-class v0, LavQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LavQ;

    return-object v0
.end method

.method public static values()[LavQ;
    .locals 1

    .prologue
    .line 32
    sget-object v0, LavQ;->a:[LavQ;

    invoke-virtual {v0}, [LavQ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LavQ;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LavQ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
