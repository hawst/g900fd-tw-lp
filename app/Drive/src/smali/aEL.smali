.class public final enum LaEL;
.super Ljava/lang/Enum;
.source "DocumentTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEL;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEL;

.field private static final synthetic a:[LaEL;

.field public static final enum b:LaEL;

.field public static final enum c:LaEL;

.field public static final enum d:LaEL;

.field public static final enum e:LaEL;

.field public static final enum f:LaEL;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum g:LaEL;

.field public static final enum h:LaEL;

.field public static final enum i:LaEL;

.field public static final enum j:LaEL;

.field public static final enum k:LaEL;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum l:LaEL;

.field public static final enum m:LaEL;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/16 v9, 0xe

    const/4 v8, 0x0

    .line 45
    new-instance v0, LaEL;

    const-string v1, "DO_SYNC"

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "doSync"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 46
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->a:LaEL;

    .line 48
    new-instance v0, LaEL;

    const-string v1, "ENTRY_ID"

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "entryId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 49
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    .line 50
    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 49
    invoke-virtual {v2, v9, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->b:LaEL;

    .line 52
    new-instance v0, LaEL;

    const-string v1, "CONTENT_ID"

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "contentId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 54
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v4

    sget-object v5, LaFH;->b:LaFH;

    .line 53
    invoke-virtual {v3, v4, v10, v5}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x32

    .line 55
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x32

    new-instance v4, LaFG;

    const-string v5, "contentId"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 57
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    sget-object v6, LaFH;->b:LaFH;

    .line 56
    invoke-virtual {v4, v5, v10, v6}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v4

    .line 58
    invoke-virtual {v4}, LaFG;->a()LaFG;

    move-result-object v4

    .line 56
    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->c:LaEL;

    .line 65
    new-instance v0, LaEL;

    const-string v1, "PDF_CONTENT_ID"

    const/4 v2, 0x3

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x1e

    new-instance v5, LaFG;

    const-string v6, "pdfContentId"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 67
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    sget-object v7, LaFH;->b:LaFH;

    .line 66
    invoke-virtual {v5, v6, v10, v7}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->d:LaEL;

    .line 69
    new-instance v0, LaEL;

    const-string v1, "HTML_URI"

    const/4 v2, 0x4

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "htmlUri"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 70
    invoke-virtual {v3, v9, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->e:LaEL;

    .line 75
    new-instance v0, LaEL;

    const-string v1, "_LEGACY_APP_ID"

    const/4 v2, 0x5

    .line 76
    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "appId"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 78
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(LaFy;)LaFG;

    move-result-object v4

    .line 77
    invoke-virtual {v3, v9, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x36

    .line 79
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->f:LaEL;

    .line 81
    new-instance v0, LaEL;

    const-string v1, "PIN_RETRY_COUNT"

    const/4 v2, 0x6

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x18

    new-instance v5, LaFG;

    const-string v6, "pinRetryCount"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 82
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x2b

    .line 83
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->g:LaEL;

    .line 85
    new-instance v0, LaEL;

    const-string v1, "MD5_CHECKSUM"

    const/4 v2, 0x7

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x19

    new-instance v5, LaFG;

    const-string v6, "md5Checksum"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 86
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->h:LaEL;

    .line 88
    new-instance v0, LaEL;

    const-string v1, "CONTENT_FRESH"

    const/16 v2, 0x8

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x19

    new-instance v5, LaFG;

    const-string v6, "contentFresh"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 89
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x3e

    .line 90
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->i:LaEL;

    .line 92
    new-instance v0, LaEL;

    const-string v1, "PDF_CONTENT_FRESH"

    const/16 v2, 0x9

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x1e

    new-instance v5, LaFG;

    const-string v6, "pdfContentFresh"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 93
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x3e

    .line 94
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->j:LaEL;

    .line 99
    new-instance v0, LaEL;

    const-string v1, "__LEGACY_COLUMN_IS_DIRTY"

    const/16 v2, 0xa

    .line 100
    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x29

    new-instance v5, LaFG;

    const-string v6, "isDirty"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 101
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x31

    .line 102
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->k:LaEL;

    .line 104
    new-instance v0, LaEL;

    const-string v1, "SIZE"

    const/16 v2, 0xb

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x51

    new-instance v5, LaFG;

    const-string v6, "size"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 105
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->l:LaEL;

    .line 106
    new-instance v0, LaEL;

    const-string v1, "QUOTA_BYTES_USED"

    const/16 v2, 0xc

    invoke-static {}, LaEK;->b()LaEK;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x55

    new-instance v5, LaFG;

    const-string v6, "quotaBytesUsed"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 107
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEL;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEL;->m:LaEL;

    .line 44
    const/16 v0, 0xd

    new-array v0, v0, [LaEL;

    sget-object v1, LaEL;->a:LaEL;

    aput-object v1, v0, v8

    sget-object v1, LaEL;->b:LaEL;

    aput-object v1, v0, v11

    sget-object v1, LaEL;->c:LaEL;

    aput-object v1, v0, v12

    const/4 v1, 0x3

    sget-object v2, LaEL;->d:LaEL;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaEL;->e:LaEL;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaEL;->f:LaEL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaEL;->g:LaEL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaEL;->h:LaEL;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaEL;->i:LaEL;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaEL;->j:LaEL;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaEL;->k:LaEL;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaEL;->l:LaEL;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LaEL;->m:LaEL;

    aput-object v2, v0, v1

    sput-object v0, LaEL;->a:[LaEL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEL;->a:LaFr;

    .line 113
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEL;
    .locals 1

    .prologue
    .line 44
    const-class v0, LaEL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEL;

    return-object v0
.end method

.method public static values()[LaEL;
    .locals 1

    .prologue
    .line 44
    sget-object v0, LaEL;->a:[LaEL;

    invoke-virtual {v0}, [LaEL;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEL;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, LaEL;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, LaEL;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
