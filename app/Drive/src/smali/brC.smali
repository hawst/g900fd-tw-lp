.class LbrC;
.super Ljava/lang/Object;
.source "BindingsByTypeCache.java"


# instance fields
.field private final a:Lbsi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsi",
            "<",
            "Lbsk",
            "<*>;>;"
        }
    .end annotation
.end field

.field private volatile a:Lbsm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsm",
            "<",
            "Ljava/util/List",
            "<",
            "Lbup",
            "<*>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbsi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbsi",
            "<",
            "Lbsk",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsi;

    iput-object v0, p0, LbrC;->a:Lbsi;

    .line 24
    return-void
.end method

.method private a()Lbsm;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbsm",
            "<",
            "Ljava/util/List",
            "<",
            "Lbup",
            "<*>;>;>;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v2, Lbsm;

    invoke-direct {v2}, Lbsm;-><init>()V

    .line 48
    iget-object v0, p0, LbrC;->a:Lbsi;

    invoke-virtual {v0}, Lbsi;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 49
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsk;

    invoke-static {v2, v1, v0}, LbrC;->a(Lbsm;Ljava/lang/Class;Lbsk;)V

    goto :goto_0

    .line 52
    :cond_0
    iget-object v0, p0, LbrC;->a:Lbsi;

    invoke-virtual {v0}, Lbsi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 53
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbuv;

    invoke-virtual {v1}, Lbuv;->a()LbuP;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsk;

    invoke-static {v2, v1, v0}, LbrC;->a(Lbsm;LbuP;Lbsk;)V

    goto :goto_1

    .line 56
    :cond_1
    return-object v2
.end method

.method private a()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, LbrC;->a:Lbsm;

    if-nez v0, :cond_1

    .line 37
    monitor-enter p0

    .line 38
    :try_start_0
    iget-object v0, p0, LbrC;->a:Lbsm;

    if-nez v0, :cond_0

    .line 39
    invoke-direct {p0}, LbrC;->a()Lbsm;

    move-result-object v0

    iput-object v0, p0, LbrC;->a:Lbsm;

    .line 41
    :cond_0
    monitor-exit p0

    .line 43
    :cond_1
    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Lbsm;LbuP;Lbsk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbsm",
            "<",
            "Ljava/util/List",
            "<",
            "Lbup",
            "<*>;>;>;",
            "LbuP",
            "<*>;",
            "Lbsk",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lbsm;->a(LbuP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 72
    if-nez v0, :cond_0

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 74
    invoke-virtual {p0, p1, v0}, Lbsm;->a(LbuP;Ljava/lang/Object;)V

    .line 76
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method

.method private static a(Lbsm;Ljava/lang/Class;Lbsk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbsm",
            "<",
            "Ljava/util/List",
            "<",
            "Lbup",
            "<*>;>;>;",
            "Ljava/lang/Class",
            "<*>;",
            "Lbsk",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lbsm;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 62
    if-nez v0, :cond_0

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 64
    invoke-virtual {p0, p1, v0}, Lbsm;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 66
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method


# virtual methods
.method public a(LbuP;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "Ljava/util/List",
            "<",
            "Lbup",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, LbrC;->a()V

    .line 28
    iget-object v0, p0, LbrC;->a:Lbsm;

    invoke-virtual {v0, p1}, Lbsm;->a(LbuP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 30
    if-eqz v0, :cond_0

    .line 32
    :goto_0
    return-object v0

    .line 31
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
