.class public LHn;
.super Ljava/lang/Object;
.source "ThirdPartyDocumentOpenerImpl.java"

# interfaces
.implements LHm;


# instance fields
.field private final a:LGZ;

.field private final a:LaHL;

.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

.field private final a:LqK;

.field private final a:LtK;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;LaHL;LGZ;LtK;LqK;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, LHn;->a:Landroid/content/Context;

    .line 89
    iput-object p2, p0, LHn;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    .line 90
    iput-object p3, p0, LHn;->a:LaHL;

    .line 91
    iput-object p4, p0, LHn;->a:LGZ;

    .line 92
    iput-object p5, p0, LHn;->a:LtK;

    .line 93
    iput-object p6, p0, LHn;->a:LqK;

    .line 94
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;
    .locals 8

    .prologue
    .line 99
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    iget-object v1, p0, LHn;->a:Landroid/content/Context;

    iget-object v2, p0, LHn;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v3, p0, LHn;->a:LaHL;

    iget-object v4, p0, LHn;->a:LGZ;

    iget-object v5, p0, LHn;->a:LtK;

    iget-object v7, p0, LHn;->a:LqK;

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;-><init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;LaHL;LGZ;LtK;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;LqK;)V

    return-object v0
.end method
