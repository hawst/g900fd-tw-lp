.class public LawZ;
.super Laww;
.source "PdfViewer.java"

# interfaces
.implements Laue;
.implements Lawu;


# static fields
.field private static a:I


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private final a:LavU;

.field private a:LawP;

.field private final a:Lawf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawf",
            "<",
            "LawH;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LaxH;

.field public final a:LaxV;

.field public final a:Laxk;

.field private a:Laxl;

.field private a:Laxn;

.field private a:Laxp;

.field private a:Laxs;

.field private a:Laxw;

.field private a:Laxz;

.field private a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

.field private final b:I

.field private b:Lawf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawf",
            "<",
            "LawH;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private final c:Lawf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private final d:Lawf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawf",
            "<",
            "Laxo;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lawf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawf",
            "<",
            "Lcom/google/android/apps/viewer/pdflib/Selection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput v0, LawZ;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 123
    invoke-direct {p0, v5}, Laww;-><init>(Z)V

    .line 81
    new-instance v0, LavU;

    invoke-direct {v0}, LavU;-><init>()V

    iput-object v0, p0, LawZ;->a:LavU;

    .line 90
    new-instance v0, Laxk;

    invoke-direct {v0, p0}, Laxk;-><init>(LawZ;)V

    iput-object v0, p0, LawZ;->a:Laxk;

    .line 102
    const/4 v0, 0x4

    iput v0, p0, LawZ;->c:I

    .line 415
    new-instance v0, Laxb;

    invoke-direct {v0, p0}, Laxb;-><init>(LawZ;)V

    iput-object v0, p0, LawZ;->a:Lawf;

    .line 608
    new-instance v0, Laxe;

    invoke-direct {v0, p0}, Laxe;-><init>(LawZ;)V

    iput-object v0, p0, LawZ;->c:Lawf;

    .line 626
    new-instance v0, Laxf;

    invoke-direct {v0, p0}, Laxf;-><init>(LawZ;)V

    iput-object v0, p0, LawZ;->d:Lawf;

    .line 640
    new-instance v0, Laxg;

    invoke-direct {v0, p0}, Laxg;-><init>(LawZ;)V

    iput-object v0, p0, LawZ;->e:Lawf;

    .line 654
    new-instance v0, Laxh;

    invoke-direct {v0, p0}, Laxh;-><init>(LawZ;)V

    iput-object v0, p0, LawZ;->a:LaxV;

    .line 690
    new-instance v0, Laxi;

    invoke-direct {v0, p0}, Laxi;-><init>(LawZ;)V

    iput-object v0, p0, LawZ;->a:LaxH;

    .line 124
    sget v0, LawZ;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LawZ;->a:I

    iput v0, p0, LawZ;->b:I

    .line 125
    sget-boolean v0, LavX;->k:Z

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "PdfViewer"

    const-string v1, "Create instance#%d (%d total) "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LawZ;->b:I

    .line 127
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    sget v3, LawZ;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 126
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_0
    return-void
.end method

.method static synthetic a(LawZ;)I
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, LawZ;->b()I

    move-result v0

    return v0
.end method

.method static synthetic a(LawZ;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic a(LawZ;)LawP;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:LawP;

    return-object v0
.end method

.method static synthetic a(LawZ;)Lawh;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Lawh;

    return-object v0
.end method

.method private a()Laxl;
    .locals 3

    .prologue
    .line 394
    new-instance v0, Laxl;

    const/4 v1, 0x0

    invoke-direct {p0}, LawZ;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v0, v1, v2}, Laxl;-><init>(II)V

    return-object v0
.end method

.method static synthetic a(LawZ;)Laxl;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, LawZ;->a()Laxl;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LawZ;Laxl;)Laxl;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, LawZ;->a:Laxl;

    return-object p1
.end method

.method static synthetic a(LawZ;)Laxn;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Laxn;

    return-object v0
.end method

.method static synthetic a(LawZ;)Laxw;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Laxw;

    return-object v0
.end method

.method static synthetic a(LawZ;)Laxz;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Laxz;

    return-object v0
.end method

.method static synthetic a(LawZ;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    return-object v0
.end method

.method private a(I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 383
    invoke-direct {p0}, LawZ;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "No page @%d (max is %d)."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 384
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-direct {p0}, LawZ;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 383
    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 385
    iget-object v0, p0, LawZ;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    return-object v0

    :cond_0
    move v0, v2

    .line 383
    goto :goto_0
.end method

.method private a(ILcom/google/android/apps/viewer/client/Dimensions;)Lcom/google/android/apps/viewer/viewer/pdf/PageView;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 351
    invoke-direct {p0}, LawZ;->b()I

    move-result v0

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t skip create page @%d (max is %d)."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 352
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-direct {p0}, LawZ;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 351
    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 358
    new-instance v4, Laxa;

    invoke-direct {v4, p0, p1}, Laxa;-><init>(LawZ;I)V

    .line 373
    new-instance v0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    invoke-virtual {p0}, LawZ;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v5, p0, LawZ;->a:LavU;

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;-><init>(Landroid/content/Context;ILcom/google/android/apps/viewer/client/Dimensions;LawO;LavU;)V

    .line 374
    iget-object v1, p0, LawZ;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 375
    iget-object v1, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->requestLayout()V

    .line 377
    iget-object v1, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    iget-object v2, p0, LawZ;->a:Laxk;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->setLongClickListenerForDescendant(Landroid/view/View;Landroid/view/View$OnLongClickListener;)V

    .line 378
    iget-object v1, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    iget-object v2, p0, LawZ;->a:Laxk;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->setClickListenerForDescendant(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 379
    return-object v0

    :cond_0
    move v0, v2

    .line 351
    goto :goto_0
.end method

.method static synthetic a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, LawZ;->a(I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LawZ;ILcom/google/android/apps/viewer/client/Dimensions;)Lcom/google/android/apps/viewer/viewer/pdf/PageView;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, LawZ;->a(ILcom/google/android/apps/viewer/client/Dimensions;)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LawZ;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, LawZ;->b(I)V

    return-void
.end method

.method static synthetic a(LawZ;Laxo;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, LawZ;->a(Laxo;)V

    return-void
.end method

.method private a(Laxo;)V
    .locals 4

    .prologue
    .line 398
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Laxo;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    iget v0, p1, Laxo;->a:I

    invoke-direct {p0}, LawZ;->b()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 402
    iget v0, p1, Laxo;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LawZ;->c(I)V

    goto :goto_0

    .line 405
    :cond_2
    iget v0, p1, Laxo;->a:I

    invoke-direct {p0, v0}, LawZ;->a(I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    .line 406
    iget-object v1, p1, Laxo;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    iget v2, p1, Laxo;->b:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->getFirstRect(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 407
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->getLeft()I

    move-result v3

    add-int/2addr v2, v3

    .line 408
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->getTop()I

    move-result v3

    add-int/2addr v1, v3

    .line 409
    iget-object v3, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(II)V

    .line 411
    invoke-virtual {p1}, Laxo;->a()LawI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Laxz;)V
    .locals 4

    .prologue
    .line 185
    iput-object p1, p0, LawZ;->a:Laxz;

    .line 187
    new-instance v0, Laxn;

    invoke-direct {v0, p1}, Laxn;-><init>(Laxz;)V

    iput-object v0, p0, LawZ;->a:Laxn;

    .line 188
    iget-object v0, p0, LawZ;->a:Laxn;

    invoke-virtual {v0}, Laxn;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->c:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, LawZ;->a:Laxn;

    invoke-virtual {v0}, Laxn;->b()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->d:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    new-instance v0, Laxw;

    invoke-direct {v0, p1}, Laxw;-><init>(Laxz;)V

    iput-object v0, p0, LawZ;->a:Laxw;

    .line 192
    iget-object v0, p0, LawZ;->a:Laxw;

    invoke-virtual {v0}, Laxw;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->e:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    new-instance v0, Laxs;

    iget-object v1, p0, LawZ;->a:Laxw;

    iget-object v2, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    iget-object v3, p0, LawZ;->a:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Laxw;Lcom/google/android/apps/viewer/viewer/image/ZoomView;Landroid/widget/LinearLayout;)V

    iput-object v0, p0, LawZ;->a:Laxs;

    .line 195
    new-instance v0, Laxp;

    invoke-virtual {p0}, LawZ;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, LawZ;->a:Laxw;

    invoke-direct {v0, v1, v2}, Laxp;-><init>(Landroid/app/Activity;Laxw;)V

    iput-object v0, p0, LawZ;->a:Laxp;

    .line 196
    return-void
.end method

.method private b()I
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, LawZ;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    return v0
.end method

.method static synthetic b(LawZ;)Lawh;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Lawh;

    return-object v0
.end method

.method static synthetic b(LawZ;)Laxl;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LawZ;->a:Laxl;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 318
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x2

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 319
    invoke-direct {p0, v0}, LawZ;->c(I)V

    .line 320
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 221
    .line 222
    if-eqz p1, :cond_0

    .line 223
    const-string v0, "plr"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 224
    iget v1, p0, LawZ;->c:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LawZ;->c:I

    .line 226
    :cond_0
    return-void
.end method

.method static synthetic b(LawZ;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, LawZ;->d(I)V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 199
    iget-object v0, p0, LawZ;->a:Laxp;

    invoke-virtual {v0}, Laxp;->a()V

    .line 200
    iput-object v2, p0, LawZ;->a:Laxp;

    .line 202
    iget-object v0, p0, LawZ;->a:Laxs;

    invoke-virtual {v0}, Laxs;->a()V

    .line 203
    iput-object v2, p0, LawZ;->a:Laxs;

    .line 205
    iget-object v0, p0, LawZ;->a:Laxw;

    invoke-virtual {v0}, Laxw;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->e:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 206
    iput-object v2, p0, LawZ;->a:Laxw;

    .line 208
    iget-object v0, p0, LawZ;->a:Laxn;

    invoke-virtual {v0}, Laxn;->b()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->d:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 209
    iget-object v0, p0, LawZ;->a:Laxn;

    invoke-virtual {v0}, Laxn;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->c:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 210
    iput-object v2, p0, LawZ;->a:Laxn;

    .line 212
    iget-object v0, p0, LawZ;->a:Laxz;

    invoke-virtual {v0}, Laxz;->c()V

    .line 213
    iput-object v2, p0, LawZ;->a:Laxz;

    .line 214
    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 330
    iget-object v0, p0, LawZ;->a:Laxz;

    if-nez v0, :cond_1

    .line 331
    const-string v0, "PdfViewer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR Can\'t layout pages as no pdfLoader "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LawZ;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    const/4 v0, 0x0

    .line 335
    iget-object v1, p0, LawZ;->a:Laxz;

    invoke-virtual {v1}, Laxz;->a()I

    move-result v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 336
    :goto_1
    iget v2, p0, LawZ;->d:I

    if-ge v2, v1, :cond_2

    .line 337
    iget-object v0, p0, LawZ;->a:Laxz;

    iget v2, p0, LawZ;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LawZ;->d:I

    invoke-virtual {v0, v2}, Laxz;->b(I)V

    .line 338
    const/4 v0, 0x1

    goto :goto_1

    .line 341
    :cond_2
    if-eqz v0, :cond_0

    .line 342
    const-string v0, "PdfViewer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pushed the boundaries of known pages to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LawZ;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic c(LawZ;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, LawZ;->e(I)V

    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 347
    iget-object v1, p0, LawZ;->a:LawP;

    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, p1}, LawP;->a(Landroid/view/ViewGroup;I)V

    .line 348
    return-void
.end method

.method private e(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 827
    invoke-virtual {p0}, LawZ;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 828
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 829
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 830
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 310
    const/4 v0, 0x0

    iget-object v1, p0, LawZ;->a:Laxz;

    invoke-virtual {v1}, Laxz;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "PdfViewer"

    return-object v0
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 293
    iget-object v0, p0, LawZ;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, LawZ;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, LawZ;->a:Landroid/widget/LinearLayout;

    .line 295
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LawZ;->a:Landroid/widget/LinearLayout;

    .line 297
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, LawZ;->a:Landroid/widget/LinearLayout;

    .line 298
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v3

    .line 294
    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 300
    :cond_0
    return-void
.end method

.method protected a(LauW;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 179
    invoke-virtual {p0}, LawZ;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LawZ;->a:LavU;

    iget-object v2, p0, LawZ;->a:LaxH;

    invoke-static {v0, p1, v1, v2}, Laxz;->a(Landroid/content/Context;LauW;LavU;LaxH;)Laxz;

    move-result-object v0

    invoke-direct {p0, v0}, LawZ;->a(Laxz;)V

    .line 181
    invoke-direct {p0, p2}, LawZ;->b(Landroid/os/Bundle;)V

    .line 182
    return-void
.end method

.method public a(Lawf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lawf",
            "<",
            "LawH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 304
    iput-object p1, p0, LawZ;->b:Lawf;

    .line 305
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, LawZ;->a:Laxz;

    invoke-virtual {v0, p1}, Laxz;->a(Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method protected finalize()V
    .locals 5

    .prologue
    .line 269
    sget-boolean v0, LavX;->k:Z

    if-eqz v0, :cond_0

    .line 270
    const-string v0, "PdfViewer"

    const-string v1, "Good job, finalize instance#%d (%d total) "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LawZ;->b:I

    .line 271
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget v4, LawZ;->a:I

    add-int/lit8 v4, v4, -0x1

    sput v4, LawZ;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 270
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0, p1}, Laww;->onActivityCreated(Landroid/os/Bundle;)V

    .line 171
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->a:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, LawZ;->b:Lawf;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->b:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0, p1}, Laww;->onAttach(Landroid/app/Activity;)V

    .line 139
    new-instance v0, LawP;

    invoke-direct {v0, p1}, LawP;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, LawZ;->a:LawP;

    .line 140
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 144
    invoke-super {p0, p1, p2, p3}, Laww;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 145
    sget v0, Lauk;->file_viewer_pdf:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    iput-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    .line 146
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->setStraightenVerticalScroll(Z)V

    .line 150
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {p0}, LawZ;->getId()I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->setId(I)V

    .line 151
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    sget v1, Lauj;->pdf_viewer:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LawZ;->a:Landroid/widget/LinearLayout;

    .line 153
    new-instance v0, Laxl;

    invoke-direct {v0}, Laxl;-><init>()V

    iput-object v0, p0, LawZ;->a:Laxl;

    .line 155
    iget v0, p0, LawZ;->d:I

    .line 156
    const/4 v1, 0x0

    iput v1, p0, LawZ;->d:I

    .line 158
    iget-object v1, p0, LawZ;->a:Laxz;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 159
    const-string v1, "PdfViewer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restore current layout to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-direct {p0, v0}, LawZ;->c(I)V

    .line 163
    :cond_0
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->setVisibility(I)V

    .line 165
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    return-object v0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 250
    invoke-super {p0}, Laww;->onDestroy()V

    .line 251
    iget-object v0, p0, LawZ;->a:Laxz;

    if-eqz v0, :cond_0

    .line 252
    invoke-direct {p0}, LawZ;->c()V

    .line 255
    :cond_0
    sget-boolean v0, LavX;->k:Z

    if-eqz v0, :cond_1

    .line 256
    const-string v0, "PdfViewer"

    const-string v1, "Destroy instance#%d (%d total) "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LawZ;->b:I

    .line 257
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget v4, LawZ;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 256
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 230
    invoke-super {p0}, Laww;->onDestroyView()V

    .line 232
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->a:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, LawZ;->b:Lawf;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, LawZ;->b:Lawf;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 236
    :cond_0
    invoke-direct {p0}, LawZ;->a()Laxl;

    move-result-object v0

    invoke-virtual {v0}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 237
    invoke-direct {p0, v0}, LawZ;->a(I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->c()V

    goto :goto_0

    .line 239
    :cond_1
    iput-object v2, p0, LawZ;->a:Laxl;

    .line 240
    iput-object v2, p0, LawZ;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    .line 241
    iput-object v2, p0, LawZ;->a:Landroid/widget/LinearLayout;

    .line 242
    iget-object v0, p0, LawZ;->a:Laxz;

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, LawZ;->a:Laxz;

    invoke-virtual {v0}, Laxz;->b()V

    .line 245
    :cond_2
    const-string v0, "PdfViewer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pdf Viewer: detach from activity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LawZ;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 263
    invoke-super {p0}, Laww;->onDetach()V

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, LawZ;->a:LawP;

    .line 265
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 277
    const-string v0, "plr"

    iget v1, p0, LawZ;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 278
    return-void
.end method
