.class public LVm;
.super Ljava/lang/Object;
.source "DelayedRemoveEntriesOperation.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static final a:LbiG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiG",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LVB;

.field private final a:LVg;

.field private final a:LaGM;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, LVn;

    invoke-direct {v0}, LVn;-><init>()V

    sput-object v0, LVm;->a:LbiG;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LaGM;LVg;LVB;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    iput-object v0, p0, LVm;->a:Ljava/util/concurrent/Executor;

    .line 93
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LVm;->a:Ljava/util/Set;

    .line 101
    iput-object p1, p0, LVm;->a:Landroid/content/Context;

    .line 102
    iput-object p2, p0, LVm;->a:LaGM;

    .line 103
    iput-object p3, p0, LVm;->a:LVg;

    .line 104
    iput-object p4, p0, LVm;->a:LVB;

    .line 105
    return-void
.end method

.method static synthetic a(LVm;)LVg;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LVm;->a:LVg;

    return-object v0
.end method

.method static synthetic a(LVm;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LVm;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic a(LVm;LbmF;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, LVm;->c(LbmF;)V

    return-void
.end method

.method private a(LaFO;)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, LVm;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 147
    iget-object v1, p0, LVm;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(LaFM;)V

    .line 148
    return-void
.end method

.method private b(LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 123
    monitor-enter p0

    .line 124
    :try_start_0
    iget-object v0, p0, LVm;->a:Ljava/util/Set;

    sget-object v1, LVm;->a:LbiG;

    invoke-static {p1, v1}, LblV;->a(Ljava/util/Collection;LbiG;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 125
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    invoke-direct {p0, p1}, LVm;->d(LbmF;)V

    .line 128
    return-void

    .line 125
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private c(LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 131
    monitor-enter p0

    .line 132
    :try_start_0
    iget-object v0, p0, LVm;->a:Ljava/util/Set;

    sget-object v1, LVm;->a:LbiG;

    invoke-static {p1, v1}, LblV;->a(Ljava/util/Collection;LbiG;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 133
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    invoke-direct {p0, p1}, LVm;->d(LbmF;)V

    .line 136
    return-void

    .line 133
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private d(LbmF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p1}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 141
    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-direct {p0, v0}, LVm;->a(LaFO;)V

    .line 143
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LVm;->a:Ljava/util/Set;

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LbmF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-static {}, LamV;->a()V

    .line 114
    invoke-direct {p0, p1}, LVm;->b(LbmF;)V

    .line 116
    iget-object v0, p0, LVm;->a:Landroid/content/Context;

    sget v1, Lxi;->selection_undo_message_remove:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    new-instance v1, LVp;

    invoke-direct {v1, p0, p1}, LVp;-><init>(LVm;LbmF;)V

    .line 119
    iget-object v2, p0, LVm;->a:LVB;

    invoke-virtual {v2, v0, v1}, LVB;->a(Ljava/lang/String;LVL;)V

    .line 120
    return-void
.end method
