.class public LDm;
.super LDi;
.source "SelectionViewState.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final a:LCB;

.field private final a:LKs;

.field private a:Landroid/animation/AnimatorSet;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;IILCD;LKs;)V
    .locals 2

    .prologue
    .line 401
    invoke-direct {p0, p1, p2, p3}, LDi;-><init>(Landroid/view/View;II)V

    .line 395
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LDm;->a:Landroid/animation/AnimatorSet;

    .line 396
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LDm;->a:Ljava/util/List;

    .line 402
    invoke-virtual {p4}, LCD;->a()LCB;

    move-result-object v0

    iput-object v0, p0, LDm;->a:LCB;

    .line 403
    iput-object p5, p0, LDm;->a:LKs;

    .line 407
    iget-object v0, p0, LDm;->a:LCB;

    iget-object v1, p0, LDm;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LCB;->a(Landroid/view/View;)V

    .line 408
    return-void
.end method

.method private g()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 419
    iget-object v0, p0, LDm;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 420
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LDm;->a:Landroid/animation/AnimatorSet;

    .line 421
    return-void
.end method


# virtual methods
.method public a(LCu;Lcom/google/android/apps/docs/doclist/SelectionItem;I)V
    .locals 1

    .prologue
    .line 452
    invoke-super {p0, p1, p2, p3}, LDi;->a(LCu;Lcom/google/android/apps/docs/doclist/SelectionItem;I)V

    .line 453
    iget-object v0, p0, LDm;->a:LCB;

    invoke-virtual {v0, p2}, LCB;->a(Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 454
    return-void
.end method

.method public a(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, LDm;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 458
    if-eqz p1, :cond_0

    .line 459
    iget-object v0, p0, LDm;->a:LCB;

    iget-object v1, p0, LDm;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LCB;->a(Landroid/view/View;)V

    .line 460
    iget-object v0, p0, LDm;->a:LCB;

    invoke-virtual {v0, p1}, LCB;->a(Z)V

    .line 464
    :goto_0
    return-void

    .line 462
    :cond_0
    iget-object v0, p0, LDm;->a:LKs;

    iget-object v1, p0, LDm;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LKs;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public d()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 413
    invoke-direct {p0}, LDm;->g()V

    .line 414
    iget-object v0, p0, LDm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 415
    return-void
.end method

.method public e()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 431
    iget-object v0, p0, LDm;->a:Landroid/animation/AnimatorSet;

    iget-object v1, p0, LDm;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 432
    iget-object v0, p0, LDm;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 433
    return-void
.end method
