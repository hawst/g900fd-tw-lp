.class public LaIu;
.super Ljava/lang/Object;
.source "StorageFileReader.java"


# instance fields
.field private final a:LaIr;

.field private final a:Ladi;

.field private final a:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ladi;LaIr;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    iput-object v0, p0, LaIu;->a:Ljava/util/concurrent/Executor;

    .line 42
    iput-object p1, p0, LaIu;->a:Ladi;

    .line 43
    iput-object p2, p0, LaIu;->a:LaIr;

    .line 44
    return-void
.end method

.method private a(LaGo;Landroid/os/ParcelFileDescriptor;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 51
    sget-object v0, LaIs;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 52
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v0

    .line 53
    iget-object v1, p0, LaIu;->a:LaIr;

    invoke-virtual {v1, p1, v0}, LaIr;->a(LaGo;LacY;)LaGo;

    move-result-object v1

    .line 55
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v2

    .line 58
    :try_start_0
    iget-object v3, p0, LaIu;->a:Ladi;

    sget-object v4, Ladk;->a:Ladk;

    invoke-interface {v3, v1, v0, v4}, Ladi;->a(LaGo;LacY;Lamr;)LbsU;

    move-result-object v0

    .line 59
    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    .line 60
    invoke-virtual {v2, v0}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 62
    invoke-interface {v0}, Ladj;->a()Ljava/io/File;

    move-result-object v0

    .line 63
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 64
    invoke-virtual {v2, v1}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 65
    invoke-static {v0, v1}, Lbrq;->a(Ljava/io/File;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    invoke-static {v2, v5}, Lbrl;->a(Ljava/io/Closeable;Z)V

    .line 75
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    :try_start_1
    const-string v1, "StorageFileReader"

    const-string v3, "interrupted"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 69
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    invoke-static {v2, v6}, Lbrl;->a(Ljava/io/Closeable;Z)V

    goto :goto_0

    .line 70
    :catch_1
    move-exception v0

    .line 71
    :try_start_2
    const-string v1, "StorageFileReader"

    const-string v3, "interrupted"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 73
    invoke-static {v2, v6}, Lbrl;->a(Ljava/io/Closeable;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v6}, Lbrl;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method static synthetic a(LaIu;LaGo;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, LaIu;->a(LaGo;Landroid/os/ParcelFileDescriptor;)V

    return-void
.end method


# virtual methods
.method public a(LaGo;)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 81
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 83
    iget-object v1, p0, LaIu;->a:Ljava/util/concurrent/Executor;

    new-instance v2, LaIv;

    invoke-direct {v2, p0, p1, v0}, LaIv;-><init>(LaIu;LaGo;[Landroid/os/ParcelFileDescriptor;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 99
    const/4 v1, 0x0

    aget-object v0, v0, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    new-instance v1, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failure making pipe: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
