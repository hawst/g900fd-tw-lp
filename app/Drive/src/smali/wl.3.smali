.class public Lwl;
.super Ljava/lang/Object;
.source "NavigationModelModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method provideCriterionFactory(LvM;)LvL;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 22
    return-object p1
.end method

.method provideEntryInformationImpl(LvW;)LvU;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 28
    return-object p1
.end method

.method provideEntryListSource(Lwb;)LvY;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 46
    return-object p1
.end method

.method provideEntryListSourceSetter(Lwb;)Lwa;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 52
    return-object p1
.end method

.method provideListViewState(Lwh;)Lwg;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 40
    return-object p1
.end method

.method provideNavigationPath(Lwp;)Lwm;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 34
    return-object p1
.end method
