.class public LqK;
.super Ljava/lang/Object;
.source "Tracker.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private a:I

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqP;

.field private final a:Z


# direct methods
.method public constructor <init>(Laja;Ljava/lang/String;LtK;LQr;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime LqN;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljava/lang/String;",
            "LtK;",
            "LQr;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput v3, p0, LqK;->a:I

    .line 176
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LqK;->a:Ljava/util/Map;

    .line 184
    sget-object v0, Lry;->ai:Lry;

    invoke-interface {p3, v0}, LtK;->a(LtJ;)Z

    move-result v1

    .line 185
    invoke-virtual {p1}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 186
    const-string v2, "enableTrackerPreconditions"

    .line 187
    invoke-interface {p4, v2, v3}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, LqK;->a:Z

    .line 189
    if-nez v1, :cond_0

    if-nez v0, :cond_1

    .line 190
    :cond_0
    new-instance v0, LqO;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LqO;-><init>(LqL;)V

    iput-object v0, p0, LqK;->a:LqP;

    .line 194
    :goto_0
    return-void

    .line 192
    :cond_1
    new-instance v1, LqM;

    invoke-direct {v1, v0, p2, p4}, LqM;-><init>(Landroid/content/Context;Ljava/lang/String;LQr;)V

    iput-object v1, p0, LqK;->a:LqP;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget v0, p0, LqK;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LqK;->a:I

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, LqK;->a:LqP;

    invoke-interface {v0}, LqP;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    :cond_0
    monitor-exit p0

    return-void

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 319
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 320
    invoke-virtual {p0, v0, v1}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 321
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 243
    iget-boolean v0, p0, LqK;->a:Z

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, LqK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 246
    :cond_0
    iget-object v0, p0, LqK;->a:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    return-void

    .line 244
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, LqK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 261
    iget-boolean v1, p0, LqK;->a:Z

    if-eqz v1, :cond_2

    .line 262
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 266
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 267
    const-string v2, "timeSpan"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0, v2, p2, p3, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 268
    const-string v2, "timeSpan"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v2, p2, p3, v0}, LqK;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 269
    :goto_1
    return-void

    .line 262
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 263
    :cond_2
    if-nez v0, :cond_0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 331
    if-eqz p2, :cond_0

    .line 332
    const-string v0, "referrer"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 333
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 334
    iget-object v1, p0, LqK;->a:LqP;

    const-string v2, "referredFrom"

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v0, v3}, LqP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 335
    iget-object v1, p0, LqK;->a:LqP;

    invoke-interface {v1, v0}, LqP;->b(Ljava/lang/String;)V

    .line 338
    :cond_0
    iget-object v0, p0, LqK;->a:LqP;

    invoke-interface {v0, p1}, LqP;->a(Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-virtual {p0, p1, p2, v0, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 286
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 295
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, LqK;->a:LqP;

    invoke-interface {v0, p1, p2, p3, p4}, LqP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 303
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, LqK;->a:LqP;

    invoke-interface {v0, p1, p2}, LqP;->a(Ljava/lang/String;Z)V

    .line 346
    return-void
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LqK;->a:Z

    if-eqz v0, :cond_0

    .line 224
    iget v0, p0, LqK;->a:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 226
    :cond_0
    iget v0, p0, LqK;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LqK;->a:I

    .line 227
    iget v0, p0, LqK;->a:I

    if-nez v0, :cond_1

    .line 228
    iget-object v0, p0, LqK;->a:LqP;

    invoke-interface {v0}, LqP;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    :cond_1
    monitor-exit p0

    return-void

    .line 224
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, LqK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 276
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 277
    return-void

    .line 276
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, LqK;->a:LqP;

    invoke-interface {v0, p1, p2, p3, p4}, LqP;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 311
    return-void
.end method
