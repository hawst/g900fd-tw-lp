.class public final LbwB;
.super Ljava/lang/Object;
.source "ProviderLookup.java"

# interfaces
.implements Lbwu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lbwu;"
    }
.end annotation


# instance fields
.field private a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lbuv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lbuv",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, "source"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LbwB;->a:Ljava/lang/Object;

    .line 47
    const-string v0, "key"

    invoke-static {p2, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    iput-object v0, p0, LbwB;->a:Lbuv;

    .line 48
    return-void
.end method

.method static synthetic a(LbwB;)LbuE;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LbwB;->a:LbuE;

    return-object v0
.end method

.method static synthetic a(LbwB;)Lbuv;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LbwB;->a:Lbuv;

    return-object v0
.end method


# virtual methods
.method public a()LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, LbwC;

    invoke-direct {v0, p0}, LbwC;-><init>(LbwB;)V

    return-object v0
.end method

.method public a()Lbuv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, LbwB;->a:Lbuv;

    return-object v0
.end method

.method public a(LbuE;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuE",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, LbwB;->a:LbuE;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "delegate already initialized"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 69
    const-string v0, "delegate"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    iput-object v0, p0, LbwB;->a:LbuE;

    .line 70
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LbwB;->a:Ljava/lang/Object;

    return-object v0
.end method
