.class public LAH;
.super Ljava/lang/Object;
.source "DocListAdapter.java"


# instance fields
.field private final a:LBf;

.field private final a:LCU;

.field private final a:LCr;

.field private final a:LDU;

.field private final a:LDk;

.field private final a:LDn;

.field private final a:LJR;

.field private final a:LabF;

.field private final a:LamF;

.field private final a:Lapd;

.field private final a:Lapn;

.field private final a:Laqw;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method public constructor <init>(LtK;LaKM;Laja;LBf;LCr;Lapd;LDU;LJR;LCU;LDn;LDk;Lapn;Laqw;LabF;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LtK;",
            "LaKM;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LBf;",
            "LCr;",
            "Lapd;",
            "LDU;",
            "LJR;",
            "LCU;",
            "LDn;",
            "LDk;",
            "Lapn;",
            "Laqw;",
            "LabF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p6, p0, LAH;->a:Lapd;

    .line 110
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    iput-object v2, p0, LAH;->a:LtK;

    .line 111
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbxw;

    iput-object v2, p0, LAH;->a:Lbxw;

    .line 112
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LBf;

    iput-object v2, p0, LAH;->a:LBf;

    .line 113
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LDU;

    iput-object v2, p0, LAH;->a:LDU;

    .line 114
    invoke-static {p8}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LJR;

    iput-object v2, p0, LAH;->a:LJR;

    .line 115
    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LCU;

    iput-object v2, p0, LAH;->a:LCU;

    .line 116
    invoke-static/range {p10 .. p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LDn;

    iput-object v2, p0, LAH;->a:LDn;

    .line 117
    move-object/from16 v0, p11

    iput-object v0, p0, LAH;->a:LDk;

    .line 118
    move-object/from16 v0, p12

    iput-object v0, p0, LAH;->a:Lapn;

    .line 119
    move-object/from16 v0, p13

    iput-object v0, p0, LAH;->a:Laqw;

    .line 120
    move-object/from16 v0, p14

    iput-object v0, p0, LAH;->a:LabF;

    .line 122
    if-nez p5, :cond_0

    .line 123
    new-instance v2, LAI;

    invoke-direct {v2, p0}, LAI;-><init>(LAH;)V

    iput-object v2, p0, LAH;->a:LCr;

    .line 133
    :goto_0
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 134
    invoke-interface {p2}, LaKM;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 135
    new-instance v4, LamF;

    invoke-virtual {p3}, Laja;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v4, v2, v3}, LamF;-><init>(Landroid/content/Context;Landroid/text/format/Time;)V

    iput-object v4, p0, LAH;->a:LamF;

    .line 136
    return-void

    .line 130
    :cond_0
    iput-object p5, p0, LAH;->a:LCr;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/support/v4/app/Fragment;LapF;LCt;Landroid/widget/ListView;LQX;LHp;LzN;Z)LAE;
    .locals 25

    .prologue
    .line 147
    move-object/from16 v0, p0

    iget-object v1, v0, LAH;->a:Lbxw;

    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 149
    new-instance v1, LAE;

    move-object/from16 v0, p0

    iget-object v4, v0, LAH;->a:LtK;

    move-object/from16 v0, p0

    iget-object v6, v0, LAH;->a:LamF;

    move-object/from16 v0, p0

    iget-object v10, v0, LAH;->a:LBf;

    move-object/from16 v0, p0

    iget-object v11, v0, LAH;->a:LCr;

    move-object/from16 v0, p0

    iget-object v12, v0, LAH;->a:Lapd;

    move-object/from16 v0, p0

    iget-object v13, v0, LAH;->a:LDU;

    move-object/from16 v0, p0

    iget-object v14, v0, LAH;->a:LJR;

    move-object/from16 v0, p0

    iget-object v15, v0, LAH;->a:LCU;

    move-object/from16 v0, p0

    iget-object v0, v0, LAH;->a:LDn;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LAH;->a:LDk;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LAH;->a:Lapn;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v3, v0, LAH;->a:Laqw;

    .line 169
    invoke-interface {v3}, Laqw;->a()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, LAH;->a:LabF;

    move-object/from16 v22, v0

    const/16 v24, 0x0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v18, p7

    move-object/from16 v19, p6

    move/from16 v23, p8

    invoke-direct/range {v1 .. v24}, LAE;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LtK;LapF;LamF;LCt;Landroid/widget/ListView;LQX;LBf;LCr;Lapd;LDU;LJR;LCU;LDn;LDk;LzN;LHp;Lapn;ILabF;ZLAF;)V

    return-object v1
.end method
