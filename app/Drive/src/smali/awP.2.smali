.class LawP;
.super Ljava/lang/Object;
.source "PageIndicator.java"


# instance fields
.field private a:F

.field private a:I

.field private final a:Landroid/animation/ValueAnimator;

.field private final a:Landroid/app/Activity;

.field private final a:Landroid/os/Handler;

.field private a:Landroid/widget/TextView;

.field private final a:Lawl;

.field private a:Laxl;

.field private final a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, LawQ;

    invoke-direct {v0, p0}, LawQ;-><init>(LawP;)V

    iput-object v0, p0, LawP;->a:Ljava/lang/Runnable;

    .line 51
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Null activity"

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 52
    iput-object p1, p0, LawP;->a:Landroid/app/Activity;

    .line 53
    new-instance v0, Lawl;

    invoke-direct {v0, p1}, Lawl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LawP;->a:Lawl;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LawP;->a:Landroid/os/Handler;

    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LawP;->a:Landroid/animation/ValueAnimator;

    .line 56
    iget-object v0, p0, LawP;->a:Landroid/animation/ValueAnimator;

    new-instance v1, LawR;

    invoke-direct {v1, p0}, LawR;-><init>(LawP;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 66
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 55
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic a(LawP;)Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, LawP;->a:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic a(LawP;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, LawP;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method private a(F)Ljava/lang/String;
    .locals 5

    .prologue
    .line 147
    iget-object v0, p0, LawP;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 148
    sget v1, Laum;->desc_zoom:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Laxl;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 123
    iget-object v0, p0, LawP;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 124
    invoke-virtual {p1}, Laxl;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 130
    sget v1, Laum;->label_page_range:I

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p1, Laxl;->a:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p1, Laxl;->b:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, LawP;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 126
    :pswitch_0
    const-string v0, ""

    goto :goto_0

    .line 128
    :pswitch_1
    sget v1, Laum;->label_page_single:I

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p1, Laxl;->a:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, LawP;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a()V
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, LawP;->a:Landroid/os/Handler;

    iget-object v1, p0, LawP;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 153
    iget-object v0, p0, LawP;->a:Landroid/os/Handler;

    iget-object v1, p0, LawP;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    return-void
.end method

.method static synthetic a(LawP;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, LawP;->c()V

    return-void
.end method

.method private b(Laxl;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 135
    iget-object v0, p0, LawP;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 136
    invoke-virtual {p1}, Laxl;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 142
    sget v1, Laum;->desc_page_range:I

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p1, Laxl;->a:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p1, Laxl;->b:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, LawP;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 138
    :pswitch_0
    const-string v0, ""

    goto :goto_0

    .line 140
    :pswitch_1
    sget v1, Laum;->desc_page_single:I

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p1, Laxl;->a:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, LawP;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, LawP;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, LawP;->a:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 159
    invoke-direct {p0}, LawP;->a()V

    .line 160
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, LawP;->a:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)V
    .locals 2

    .prologue
    .line 69
    iput p2, p0, LawP;->a:I

    .line 70
    iget-object v0, p0, LawP;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, LawP;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lauk;->page_indicator:I

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 72
    sget v0, Lauj;->pdf_page_num:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LawP;->a:Landroid/widget/TextView;

    .line 74
    :cond_0
    invoke-direct {p0}, LawP;->c()V

    .line 75
    return-void
.end method

.method public a(Laxl;)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, LawP;->a:Laxl;

    invoke-virtual {p1, v0}, Laxl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-direct {p0, p1}, LawP;->b(Laxl;)Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, LawP;->a:Landroid/widget/TextView;

    invoke-direct {p0, p1}, LawP;->a(Laxl;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v1, p0, LawP;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 87
    iput-object p1, p0, LawP;->a:Laxl;

    .line 88
    iget-object v1, p0, LawP;->a:Landroid/app/Activity;

    iget-object v2, p0, LawP;->a:Landroid/widget/TextView;

    invoke-static {v1, v2, v0}, LavM;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 90
    :cond_0
    invoke-direct {p0}, LawP;->b()V

    .line 91
    return-void
.end method

.method public a(Laxl;F)V
    .locals 3

    .prologue
    .line 101
    invoke-direct {p0, p1}, LawP;->b(Laxl;)Ljava/lang/String;

    move-result-object v0

    .line 102
    iget-object v1, p0, LawP;->a:Landroid/widget/TextView;

    invoke-direct {p0, p1}, LawP;->a(Laxl;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v1, p0, LawP;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v1, p0, LawP;->a:Landroid/app/Activity;

    invoke-static {v1}, LavM;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    iget-object v2, p0, LawP;->a:Laxl;

    invoke-virtual {p1, v2}, Laxl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 107
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    :cond_0
    iget v0, p0, LawP;->a:F

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_1

    .line 110
    invoke-direct {p0, p2}, LawP;->a(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, LawP;->a:Landroid/app/Activity;

    iget-object v2, p0, LawP;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, LavM;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 117
    :cond_2
    iput-object p1, p0, LawP;->a:Laxl;

    .line 118
    iput p2, p0, LawP;->a:F

    .line 119
    invoke-direct {p0}, LawP;->b()V

    .line 120
    return-void
.end method
