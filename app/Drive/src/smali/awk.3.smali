.class public final Lawk;
.super Ljava/lang/Object;
.source "RectUtils.java"


# direct methods
.method private static a(F)I
    .locals 2

    .prologue
    .line 54
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/graphics/Rect;)I
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method private static a(Landroid/graphics/Rect;Landroid/graphics/Rect;)I
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    sub-int/2addr v0, v1

    .line 49
    invoke-virtual {p0}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int/2addr v1, v2

    .line 50
    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(Ljava/util/List;Landroid/graphics/Rect;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Landroid/graphics/Rect;",
            ")I"
        }
    .end annotation

    .prologue
    .line 33
    const v2, 0x7fffffff

    .line 34
    const/4 v1, -0x1

    .line 35
    const/4 v0, 0x0

    .line 36
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 37
    invoke-static {v0, p1}, Lawk;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v0

    .line 38
    if-ge v0, v3, :cond_0

    move v2, v1

    move v3, v0

    .line 42
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 43
    goto :goto_0

    .line 44
    :cond_1
    return v2
.end method

.method public static a(Landroid/graphics/Rect;F)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 14
    iget v0, p0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0}, Lawk;->a(F)I

    move-result v0

    iget v1, p0, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Lawk;->a(F)I

    move-result v1

    iget v2, p0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    invoke-static {v2}, Lawk;->b(F)I

    move-result v2

    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    mul-float/2addr v3, p1

    .line 15
    invoke-static {v3}, Lawk;->b(F)I

    move-result v3

    .line 14
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 16
    return-object p0
.end method

.method private static b(F)I
    .locals 2

    .prologue
    .line 58
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method
