.class public LeZ;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompat.java"


# static fields
.field static final a:Lfi;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 591
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 592
    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 593
    new-instance v0, Lfh;

    invoke-direct {v0}, Lfh;-><init>()V

    sput-object v0, LeZ;->a:Lfi;

    .line 603
    :goto_0
    return-void

    .line 594
    :cond_0
    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 595
    new-instance v0, Lff;

    invoke-direct {v0}, Lff;-><init>()V

    sput-object v0, LeZ;->a:Lfi;

    goto :goto_0

    .line 596
    :cond_1
    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 597
    new-instance v0, Lfg;

    invoke-direct {v0}, Lfg;-><init>()V

    sput-object v0, LeZ;->a:Lfi;

    goto :goto_0

    .line 598
    :cond_2
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 599
    new-instance v0, Lfd;

    invoke-direct {v0}, Lfd;-><init>()V

    sput-object v0, LeZ;->a:Lfi;

    goto :goto_0

    .line 601
    :cond_3
    new-instance v0, Lfb;

    invoke-direct {v0}, Lfb;-><init>()V

    sput-object v0, LeZ;->a:Lfi;

    goto :goto_0
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    .line 30
    return-void
.end method


# virtual methods
.method public a(F)LeZ;
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 636
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1}, Lfi;->a(Landroid/view/View;F)V

    .line 638
    :cond_0
    return-object p0
.end method

.method public a(J)LeZ;
    .locals 3

    .prologue
    .line 618
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 619
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1, p2}, Lfi;->a(Landroid/view/View;J)V

    .line 621
    :cond_0
    return-object p0
.end method

.method public a(Landroid/view/animation/Interpolator;)LeZ;
    .locals 2

    .prologue
    .line 758
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 759
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1}, Lfi;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 761
    :cond_0
    return-object p0
.end method

.method public a(Lfn;)LeZ;
    .locals 2

    .prologue
    .line 1190
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1191
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1}, Lfi;->a(Landroid/view/View;Lfn;)V

    .line 1193
    :cond_0
    return-object p0
.end method

.method public a(Lfp;)LeZ;
    .locals 2

    .prologue
    .line 1209
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1210
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1}, Lfi;->a(Landroid/view/View;Lfp;)V

    .line 1212
    :cond_0
    return-object p0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 992
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 993
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0}, Lfi;->a(Landroid/view/View;)V

    .line 995
    :cond_0
    return-void
.end method

.method public b(F)LeZ;
    .locals 2

    .prologue
    .line 669
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 670
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1}, Lfi;->b(Landroid/view/View;F)V

    .line 672
    :cond_0
    return-object p0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1110
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1111
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0}, Lfi;->b(Landroid/view/View;)V

    .line 1113
    :cond_0
    return-void
.end method

.method public c(F)LeZ;
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 687
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1}, Lfi;->c(Landroid/view/View;F)V

    .line 689
    :cond_0
    return-object p0
.end method

.method public d(F)LeZ;
    .locals 2

    .prologue
    .line 964
    iget-object v0, p0, LeZ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 965
    sget-object v1, LeZ;->a:Lfi;

    invoke-interface {v1, v0, p1}, Lfi;->d(Landroid/view/View;F)V

    .line 967
    :cond_0
    return-object p0
.end method
