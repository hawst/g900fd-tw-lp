.class public final Lalm;
.super Ljava/lang/Object;
.source "FetchSpec.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 10

    .prologue
    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 108
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 109
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 114
    new-instance v8, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v8, v0, v1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    .line 115
    new-instance v1, Lcom/google/android/apps/docs/utils/FetchSpec;

    const/4 v9, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/docs/utils/FetchSpec;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;JJLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lalm;)V

    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 1

    .prologue
    .line 121
    new-array v0, p1, [Lcom/google/android/apps/docs/utils/FetchSpec;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lalm;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lalm;->a(I)[Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    return-object v0
.end method
