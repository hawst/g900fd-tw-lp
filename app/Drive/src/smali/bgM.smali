.class public LbgM;
.super Ljava/lang/Object;
.source "PdfPaperSize.java"


# static fields
.field public static final a:LbgM;

.field public static final b:LbgM;

.field public static final c:LbgM;

.field public static final d:LbgM;

.field public static final e:LbgM;

.field public static final f:LbgM;

.field public static final g:LbgM;

.field public static final h:LbgM;


# instance fields
.field private final a:F

.field private final b:F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const v6, 0x44528000    # 842.0f

    const/high16 v5, 0x44460000    # 792.0f

    const/high16 v4, 0x44310000    # 708.0f

    const/high16 v3, 0x44190000    # 612.0f

    const v2, 0x4414c000    # 595.0f

    .line 10
    new-instance v0, LbgM;

    invoke-direct {v0, v3, v5}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->a:LbgM;

    .line 11
    new-instance v0, LbgM;

    const/high16 v1, 0x447c0000    # 1008.0f

    invoke-direct {v0, v3, v1}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->b:LbgM;

    .line 12
    new-instance v0, LbgM;

    const/high16 v1, 0x44990000    # 1224.0f

    invoke-direct {v0, v5, v1}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->c:LbgM;

    .line 13
    new-instance v0, LbgM;

    const v1, 0x4494e000    # 1191.0f

    invoke-direct {v0, v6, v1}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->d:LbgM;

    .line 14
    new-instance v0, LbgM;

    invoke-direct {v0, v2, v6}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->e:LbgM;

    .line 15
    new-instance v0, LbgM;

    const/high16 v1, 0x43d20000    # 420.0f

    invoke-direct {v0, v1, v2}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->f:LbgM;

    .line 16
    new-instance v0, LbgM;

    const/high16 v1, 0x447a0000    # 1000.0f

    invoke-direct {v0, v4, v1}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->g:LbgM;

    .line 17
    new-instance v0, LbgM;

    const/high16 v1, 0x43f90000    # 498.0f

    invoke-direct {v0, v1, v4}, LbgM;-><init>(FF)V

    sput-object v0, LbgM;->h:LbgM;

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, LbgM;->a:F

    .line 31
    iput p2, p0, LbgM;->b:F

    .line 32
    return-void
.end method

.method public static a(Ljava/lang/String;)LbgM;
    .locals 5

    .prologue
    .line 62
    const-class v0, LbgM;

    .line 63
    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 64
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 65
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 67
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbgM;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_1
    return-object v0

    .line 70
    :catch_0
    move-exception v0

    .line 64
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 77
    :cond_1
    sget-object v0, LbgM;->a:LbgM;

    goto :goto_1

    .line 68
    :catch_1
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 40
    iget v0, p0, LbgM;->a:F

    return v0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 49
    iget v0, p0, LbgM;->b:F

    return v0
.end method
