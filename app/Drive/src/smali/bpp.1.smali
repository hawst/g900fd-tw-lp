.class abstract Lbpp;
.super Lbqa;
.source "Multisets.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbqa",
        "<",
        "Lbpj",
        "<TE;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 965
    invoke-direct {p0}, Lbqa;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lbpi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbpi",
            "<TE;>;"
        }
    .end annotation
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 1005
    invoke-virtual {p0}, Lbpp;->a()Lbpi;

    move-result-object v0

    invoke-interface {v0}, Lbpi;->clear()V

    .line 1006
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 970
    instance-of v1, p1, Lbpj;

    if-eqz v1, :cond_0

    .line 975
    check-cast p1, Lbpj;

    .line 976
    invoke-interface {p1}, Lbpj;->a()I

    move-result v1

    if-gtz v1, :cond_1

    .line 983
    :cond_0
    :goto_0
    return v0

    .line 979
    :cond_1
    invoke-virtual {p0}, Lbpp;->a()Lbpi;

    move-result-object v1

    invoke-interface {p1}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lbpi;->a(Ljava/lang/Object;)I

    move-result v1

    .line 980
    invoke-interface {p1}, Lbpj;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 989
    instance-of v1, p1, Lbpj;

    if-eqz v1, :cond_0

    .line 990
    check-cast p1, Lbpj;

    .line 991
    invoke-interface {p1}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v1

    .line 992
    invoke-interface {p1}, Lbpj;->a()I

    move-result v2

    .line 993
    if-eqz v2, :cond_0

    .line 996
    invoke-virtual {p0}, Lbpp;->a()Lbpi;

    move-result-object v3

    .line 997
    invoke-interface {v3, v1, v2, v0}, Lbpi;->a(Ljava/lang/Object;II)Z

    move-result v0

    .line 1000
    :cond_0
    return v0
.end method
