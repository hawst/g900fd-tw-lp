.class public final enum LabO;
.super Ljava/lang/Enum;
.source "ContactSharingOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LabO;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LabO;

.field private static final synthetic a:[LabO;

.field public static final enum b:LabO;

.field public static final enum c:LabO;

.field public static final enum d:LabO;


# instance fields
.field private final a:I

.field private final a:Lqt;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 30
    new-instance v0, LabO;

    const-string v1, "WRITER"

    sget v2, Lxi;->contact_sharing_writer:I

    sget-object v3, Lqt;->b:Lqt;

    invoke-direct {v0, v1, v4, v2, v3}, LabO;-><init>(Ljava/lang/String;IILqt;)V

    sput-object v0, LabO;->a:LabO;

    .line 31
    new-instance v0, LabO;

    const-string v1, "COMMENTER"

    sget v2, Lxi;->contact_sharing_commenter:I

    sget-object v3, Lqt;->c:Lqt;

    invoke-direct {v0, v1, v5, v2, v3}, LabO;-><init>(Ljava/lang/String;IILqt;)V

    sput-object v0, LabO;->b:LabO;

    .line 32
    new-instance v0, LabO;

    const-string v1, "READER"

    sget v2, Lxi;->contact_sharing_reader:I

    sget-object v3, Lqt;->d:Lqt;

    invoke-direct {v0, v1, v6, v2, v3}, LabO;-><init>(Ljava/lang/String;IILqt;)V

    sput-object v0, LabO;->c:LabO;

    .line 35
    new-instance v0, LabO;

    const-string v1, "NO_ACCESS"

    sget v2, Lxi;->contact_sharing_no_access:I

    sget-object v3, Lqt;->f:Lqt;

    invoke-direct {v0, v1, v7, v2, v3}, LabO;-><init>(Ljava/lang/String;IILqt;)V

    sput-object v0, LabO;->d:LabO;

    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [LabO;

    sget-object v1, LabO;->a:LabO;

    aput-object v1, v0, v4

    sget-object v1, LabO;->b:LabO;

    aput-object v1, v0, v5

    sget-object v1, LabO;->c:LabO;

    aput-object v1, v0, v6

    sget-object v1, LabO;->d:LabO;

    aput-object v1, v0, v7

    sput-object v0, LabO;->a:[LabO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILqt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lqt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, LabO;->a:I

    .line 44
    iput-object p4, p0, LabO;->a:Lqt;

    .line 45
    return-void
.end method

.method public static a(Lqt;)LabO;
    .locals 5

    .prologue
    .line 56
    invoke-static {}, LabO;->values()[LabO;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 57
    invoke-virtual {v0}, LabO;->a()Lqt;

    move-result-object v4

    invoke-virtual {v4, p0}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 61
    :goto_1
    return-object v0

    .line 56
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 61
    :cond_1
    sget-object v0, LabO;->d:LabO;

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LajO;LaGM;)LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
            "LajO;",
            "LaGM;",
            ")",
            "LbmF",
            "<",
            "LabO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 81
    invoke-interface {p1, v0}, LajO;->a(LaFO;)LajN;

    move-result-object v0

    .line 83
    invoke-interface {p2, p0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;

    move-result-object v1

    .line 84
    if-nez v1, :cond_0

    .line 85
    const-string v0, "ContactSharingOption"

    const-string v1, "Failed to load the Entry: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 86
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 91
    :goto_0
    invoke-static {v0}, LabO;->a(Ljava/util/Set;)LbmF;

    move-result-object v0

    return-object v0

    .line 88
    :cond_0
    invoke-virtual {v0, v1}, LajN;->a(LaGu;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Set;)LbmF;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lqt;",
            ">;)",
            "LbmF",
            "<",
            "LabO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 106
    invoke-static {}, LabO;->values()[LabO;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 107
    invoke-virtual {v4}, LabO;->a()Lqt;

    move-result-object v5

    .line 108
    invoke-virtual {v5}, Lqt;->a()Ljava/util/Set;

    move-result-object v6

    .line 114
    invoke-interface {v6}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 115
    invoke-virtual {v1, v4}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 106
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_1
    invoke-interface {p0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 117
    invoke-virtual {v1, v4}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_1

    .line 120
    :cond_2
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LabO;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 126
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabO;

    .line 127
    invoke-virtual {v0}, LabO;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :cond_0
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)LabO;
    .locals 1

    .prologue
    .line 27
    const-class v0, LabO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LabO;

    return-object v0
.end method

.method public static values()[LabO;
    .locals 1

    .prologue
    .line 27
    sget-object v0, LabO;->a:[LabO;

    invoke-virtual {v0}, [LabO;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LabO;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, LabO;->a:I

    return v0
.end method

.method public a()Lqt;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LabO;->a:Lqt;

    return-object v0
.end method
