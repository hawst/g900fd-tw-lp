.class public final LaFJ;
.super Ljava/lang/Object;
.source "SqlWhereClause.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/drive/database/common/SqlWhereClause;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 4

    .prologue
    .line 252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 254
    new-instance v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/util/Collection;LaFJ;)V

    return-object v2
.end method

.method public a(I)[Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 1

    .prologue
    .line 259
    new-array v0, p1, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0, p1}, LaFJ;->a(Landroid/os/Parcel;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0, p1}, LaFJ;->a(I)[Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method
