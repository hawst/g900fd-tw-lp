.class public LajS;
.super Ljava/lang/Object;
.source "Accounts.java"


# direct methods
.method public static a(LSF;LaFO;)Landroid/accounts/Account;
    .locals 5

    .prologue
    .line 297
    invoke-interface {p0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 298
    invoke-virtual {p1, v0}, LaFO;->a(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 302
    :goto_1
    return-object v0

    .line 297
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 302
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Landroid/view/View;LabI;LabF;ILandroid/content/Context;Z)Landroid/view/View;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 222
    if-nez p1, :cond_0

    .line 223
    const-string v1, "layout_inflater"

    .line 224
    invoke-virtual {p5, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 226
    invoke-virtual {v1, p4, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 229
    :cond_0
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LakQ;->e(Landroid/content/res/Resources;)Z

    move-result v9

    .line 231
    invoke-virtual {p1, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 233
    new-instance v1, LabE;

    invoke-static {p0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v5

    move-object v6, v4

    move-wide v7, v2

    invoke-direct/range {v1 .. v8}, LabE;-><init>(JLjava/lang/String;Ljava/util/List;Landroid/net/Uri;J)V

    .line 235
    move/from16 v0, p6

    invoke-static {p1, v1, p2, v0, v9}, LajS;->b(Landroid/view/View;LabD;LabI;ZZ)V

    .line 237
    new-instance v1, LajT;

    move-object v2, p3

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move/from16 v6, p6

    move v7, v9

    invoke-direct/range {v1 .. v7}, LajT;-><init>(LabF;Ljava/lang/String;Landroid/view/View;LabI;ZZ)V

    .line 251
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 253
    return-object p1
.end method

.method public static a(LSF;LaFM;)V
    .locals 1

    .prologue
    .line 309
    invoke-static {}, Lcom/google/android/gms/drive/database/DocListProvider;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LajS;->a(LSF;LaFM;Ljava/lang/String;)V

    .line 310
    return-void
.end method

.method public static a(LSF;LaFM;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 317
    .line 318
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-static {p0, v0}, LajS;->a(LSF;LaFO;)Landroid/accounts/Account;

    move-result-object v0

    .line 319
    if-nez v0, :cond_1

    .line 321
    const-string v0, "Accounts"

    const-string v1, "Sync requested for non-existent account: %s"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-interface {p0, v1}, LSF;->b(LaFO;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 331
    const-string v1, "Accounts"

    const-string v2, "calling cancelSync for %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 332
    invoke-static {}, Lcom/google/android/gms/drive/database/DocListProvider;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 335
    :cond_2
    invoke-static {v0, p2}, Landroid/content/ContentResolver;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 336
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 337
    const-string v2, "force"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 338
    const-string v2, "expedited"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 339
    const-string v2, "Accounts"

    const-string v3, "Requesting sync for %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 340
    invoke-static {v0, p2, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method static synthetic a(Landroid/view/View;LabD;LabI;ZZ)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1, p2, p3, p4}, LajS;->b(Landroid/view/View;LabD;LabI;ZZ)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 275
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "@google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 284
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/view/View;LabD;LabI;ZZ)V
    .locals 1

    .prologue
    .line 260
    if-eqz p3, :cond_0

    .line 261
    new-instance v0, LajX;

    invoke-direct {v0, p1, p2, p0}, LajX;-><init>(LabD;LabI;Landroid/view/View;)V

    .line 268
    :goto_0
    invoke-virtual {v0, p4}, LajU;->a(Z)V

    .line 269
    return-void

    .line 264
    :cond_0
    new-instance v0, LajY;

    invoke-direct {v0, p1, p2, p0}, LajY;-><init>(LabD;LabI;Landroid/view/View;)V

    goto :goto_0
.end method
