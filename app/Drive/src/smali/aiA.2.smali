.class LaiA;
.super Ljava/lang/Object;
.source "ActiveOrDefaultContextProvider.java"

# interfaces
.implements LbuE;


# annotations
.annotation runtime LbuO;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbuE",
        "<",
        "Landroid/content/Context;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LaiU;

.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(LaiU;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, LaiA;->a:LaiU;

    .line 25
    iput-object p2, p0, LaiA;->a:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, LaiA;->a:LaiU;

    invoke-interface {v0}, LaiU;->a()Landroid/content/Context;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    .line 35
    :goto_0
    return-object v0

    .line 34
    :cond_0
    const-string v0, "ActiveOrDefaultContextProvider"

    const-string v1, "Missing scope.enter somewhere. Returning default context"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iget-object v0, p0, LaiA;->a:Landroid/content/Context;

    goto :goto_0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, LaiA;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
