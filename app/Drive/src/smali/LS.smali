.class public LLS;
.super LKS;
.source "SelectionFunctionManager.java"


# instance fields
.field private final a:LCU;

.field private final a:LJR;

.field private a:LLW;

.field private final a:LaGR;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LKP;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(LJR;LCU;LaGR;LKU;)V
    .locals 18

    .prologue
    .line 135
    invoke-direct/range {p0 .. p0}, LKS;-><init>()V

    .line 118
    new-instance v2, LLT;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LLT;-><init>(LLS;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LLS;->a:Ljava/lang/Runnable;

    .line 136
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LLS;->a:LJR;

    .line 137
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LLS;->a:LCU;

    .line 138
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, LLS;->a:LaGR;

    .line 141
    new-instance v2, LLX;

    move-object/from16 v0, p4

    iget-object v4, v0, LKU;->a:LLM;

    sget v5, Lxb;->select_share:I

    sget v6, Lxi;->selection_menu_share:I

    sget v7, Lxi;->selection_menu_share_content_desc:I

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, LLX;-><init>(LLS;LLw;III)V

    .line 148
    new-instance v3, LLX;

    move-object/from16 v0, p4

    iget-object v5, v0, LKU;->a:LLK;

    sget v6, Lxb;->select_rename:I

    sget v7, Lxi;->selection_menu_rename:I

    sget v8, Lxi;->selection_menu_rename_content_desc:I

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, LLX;-><init>(LLS;LLw;III)V

    .line 155
    new-instance v4, LLX;

    move-object/from16 v0, p4

    iget-object v6, v0, LKU;->a:LLP;

    sget v7, Lxb;->select_unstar:I

    sget v8, Lxi;->selection_menu_unstar:I

    sget v9, Lxi;->selection_menu_unstar_content_desc:I

    move-object/from16 v0, p4

    iget-object v10, v0, LKU;->a:LLN;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v10}, LLX;-><init>(LLS;LLw;IIILjava/lang/Object;)V

    .line 163
    new-instance v5, LLX;

    move-object/from16 v0, p4

    iget-object v7, v0, LKU;->a:LLN;

    sget v8, Lxb;->select_star:I

    sget v9, Lxi;->selection_menu_star:I

    sget v10, Lxi;->selection_menu_star_content_desc:I

    move-object/from16 v6, p0

    invoke-direct/range {v5 .. v10}, LLX;-><init>(LLS;LLw;III)V

    .line 170
    new-instance v16, LKR;

    const/4 v6, 0x2

    new-array v6, v6, [LKP;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    const/4 v5, 0x1

    aput-object v4, v6, v5

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, LKR;-><init>([LKP;)V

    .line 173
    new-instance v4, LLX;

    move-object/from16 v0, p4

    iget-object v6, v0, LKU;->a:LLH;

    sget v7, Lxb;->select_remove:I

    sget v8, Lxi;->selection_menu_remove:I

    sget v9, Lxi;->selection_menu_remove_content_desc:I

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, LLX;-><init>(LLS;LLw;III)V

    .line 180
    new-instance v5, LLX;

    move-object/from16 v0, p4

    iget-object v7, v0, LKU;->a:LLx;

    sget v8, Lxb;->select_download:I

    sget v9, Lxi;->selection_menu_download:I

    sget v10, Lxi;->selection_menu_download_content_desc:I

    move-object/from16 v6, p0

    invoke-direct/range {v5 .. v10}, LLX;-><init>(LLS;LLw;III)V

    .line 187
    new-instance v6, LLX;

    move-object/from16 v0, p4

    iget-object v8, v0, LKU;->a:LLO;

    sget v9, Lxb;->select_pin:I

    sget v10, Lxi;->selection_menu_unpin:I

    sget v11, Lxi;->selection_menu_unpin_content_desc:I

    move-object/from16 v7, p0

    invoke-direct/range {v6 .. v11}, LLX;-><init>(LLS;LLw;III)V

    .line 194
    new-instance v7, LLX;

    move-object/from16 v0, p4

    iget-object v9, v0, LKU;->a:LLF;

    sget v10, Lxb;->select_pin:I

    sget v11, Lxi;->selection_menu_pin:I

    sget v12, Lxi;->selection_menu_pin_content_desc:I

    move-object/from16 v8, p0

    invoke-direct/range {v7 .. v12}, LLX;-><init>(LLS;LLw;III)V

    .line 201
    new-instance v8, LLX;

    move-object/from16 v0, p4

    iget-object v10, v0, LKU;->a:LLz;

    sget v11, Lxb;->select_move_to:I

    sget v12, Lxi;->selection_menu_move_to:I

    sget v13, Lxi;->selection_menu_move_to_content_desc:I

    move-object/from16 v9, p0

    invoke-direct/range {v8 .. v13}, LLX;-><init>(LLS;LLw;III)V

    .line 208
    new-instance v9, LLX;

    move-object/from16 v0, p4

    iget-object v11, v0, LKU;->a:LLB;

    sget v12, Lxb;->select_move_to_current_folder:I

    sget v13, Lxi;->selection_menu_move_to_current_folder:I

    sget v14, Lxi;->selection_menu_move_to_current_folder_content_desc:I

    move-object/from16 v10, p0

    invoke-direct/range {v9 .. v14}, LLX;-><init>(LLS;LLw;III)V

    .line 215
    new-instance v15, LKR;

    const/4 v9, 0x2

    new-array v9, v9, [LKP;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    const/4 v7, 0x1

    aput-object v6, v9, v7

    invoke-direct {v15, v9}, LKR;-><init>([LKP;)V

    .line 218
    new-instance v9, LLX;

    move-object/from16 v0, p4

    iget-object v11, v0, LKU;->a:LLG;

    sget v12, Lxb;->select_print:I

    sget v13, Lxi;->selection_menu_print:I

    sget v14, Lxi;->selection_menu_print_content_desc:I

    move-object/from16 v10, p0

    invoke-direct/range {v9 .. v14}, LLX;-><init>(LLS;LLw;III)V

    move-object v10, v2

    move-object v11, v15

    move-object v12, v4

    move-object v13, v8

    move-object v14, v5

    move-object v15, v3

    move-object/from16 v17, v9

    .line 228
    invoke-static/range {v10 .. v17}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LLS;->a:LbmF;

    .line 238
    return-void
.end method

.method static synthetic a(LLS;)LJR;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LLS;->a:LJR;

    return-object v0
.end method

.method static synthetic a(LLS;LLW;)LLW;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, LLS;->a:LLW;

    return-object p1
.end method

.method static synthetic a(LLS;)LbmF;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LLS;->a:LbmF;

    return-object v0
.end method

.method static synthetic a(LLS;LaGM;LbmF;)LbmF;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, LLS;->a(LaGM;LbmF;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method private a(LaGM;LbmF;)LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)",
            "LbmF",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 377
    invoke-virtual {p2}, LbmF;->a()Lbqv;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 378
    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 379
    if-eqz v0, :cond_0

    .line 380
    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_0

    .line 384
    :cond_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LLS;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LLS;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(LLS;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, LLS;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private a(LbmF;Landroid/view/ViewGroup;Landroid/view/View$OnTouchListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LKP;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View$OnTouchListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 312
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    invoke-virtual {p0}, LLS;->a()I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 314
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    .line 317
    invoke-interface {v0, p2, p3}, LKP;->a(Landroid/view/ViewGroup;Landroid/view/View$OnTouchListener;)Landroid/view/View;

    goto :goto_1

    .line 313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 319
    :cond_1
    return-void
.end method

.method private a(LbmF;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LKP;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;",
            "Landroid/view/View$OnTouchListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 327
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v0, p0, LLS;->a:LJR;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    invoke-virtual {p1}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    sget v0, Lxc;->selection_actions_container:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 335
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v3, "layout_inflater"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 337
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 338
    sget v4, Lxe;->selection_floating_handle_pop_menu:I

    invoke-virtual {v1, v4, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 339
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v3, v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 340
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 342
    new-instance v3, Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    .line 344
    new-instance v0, LKT;

    invoke-direct {v0, v3, p1}, LKT;-><init>(Landroid/widget/ListPopupWindow;LbmF;)V

    .line 346
    invoke-virtual {v3, v0}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 347
    invoke-virtual {v3, v2}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 349
    sget v0, Lxc;->selection_popup_anchor:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 350
    invoke-virtual {v3, v0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 352
    new-instance v0, LLU;

    invoke-direct {v0, p0, p2, v3}, LLU;-><init>(LLS;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    invoke-virtual {v1, p3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 365
    :cond_0
    return-void

    .line 339
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;)V
    .locals 8

    .prologue
    .line 266
    sget v0, Lxc;->selection_actions_container:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 268
    invoke-virtual {p0}, LLS;->a()I

    move-result v2

    .line 269
    const/4 v1, 0x0

    .line 271
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 272
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v5

    .line 274
    iget-object v3, p0, LLS;->a:LbmF;

    invoke-virtual {v3}, LbmF;->a()Lbqv;

    move-result-object v6

    move v3, v2

    move v2, v1

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LKP;

    .line 275
    invoke-interface {v1}, LKP;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 279
    if-lez v3, :cond_1

    .line 280
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    add-int/lit8 v3, v3, -0x1

    move v1, v2

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v1

    .line 295
    goto :goto_0

    .line 283
    :cond_1
    invoke-virtual {v5, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 284
    if-nez v2, :cond_4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 285
    const/4 v2, 0x1

    .line 286
    if-gtz v3, :cond_2

    .line 288
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LKP;

    .line 289
    invoke-virtual {v5, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move v1, v2

    move v2, v3

    .line 290
    goto :goto_1

    .line 291
    :cond_2
    add-int/lit8 v3, v3, -0x1

    move v1, v2

    move v2, v3

    goto :goto_1

    .line 298
    :cond_3
    invoke-static {v4}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v1

    .line 297
    invoke-direct {p0, v1, v0, p2}, LLS;->a(LbmF;Landroid/view/ViewGroup;Landroid/view/View$OnTouchListener;)V

    .line 299
    invoke-virtual {v5}, LbmH;->a()LbmF;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, LLS;->a(LbmF;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;)V

    .line 300
    return-void

    :cond_4
    move v1, v2

    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    return v0
.end method

.method public a(LLw;)V
    .locals 2

    .prologue
    .line 257
    new-instance v0, LLV;

    iget-object v1, p0, LLS;->a:LCU;

    .line 258
    invoke-virtual {v1}, LCU;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, LLV;-><init>(LLS;LLw;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 259
    iget-object v1, p0, LLS;->a:LaGR;

    invoke-virtual {v1, v0}, LaGR;->a(LaGN;)V

    .line 260
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, LLS;->a:LLW;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, LLS;->a:LLW;

    invoke-virtual {v0}, LLW;->b()V

    .line 252
    :cond_0
    new-instance v0, LLW;

    invoke-direct {v0, p0, p1, p2, p3}, LLW;-><init>(LLS;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    iput-object v0, p0, LLS;->a:LLW;

    .line 253
    iget-object v0, p0, LLS;->a:LaGR;

    iget-object v1, p0, LLS;->a:LLW;

    invoke-virtual {v0, v1}, LaGR;->a(LaGN;)V

    .line 254
    return-void
.end method
