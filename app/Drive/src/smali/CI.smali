.class public LCI;
.super Ljava/lang/Object;
.source "MenuItemsState.java"


# static fields
.field private static final a:LCI;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LCK;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    new-instance v0, LCI;

    .line 42
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2}, LCI;-><init>(Ljava/util/Set;ZZ)V

    sput-object v0, LCI;->a:LCI;

    .line 41
    return-void
.end method

.method private constructor <init>(Ljava/util/Set;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LCK;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    iput-object p1, p0, LCI;->a:Ljava/util/Set;

    .line 304
    iput-boolean p2, p0, LCI;->a:Z

    .line 305
    iput-boolean p3, p0, LCI;->b:Z

    .line 306
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Set;ZZLCJ;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, LCI;-><init>(Ljava/util/Set;ZZ)V

    return-void
.end method

.method static synthetic a()LCI;
    .locals 1

    .prologue
    .line 40
    sget-object v0, LCI;->a:LCI;

    return-object v0
.end method

.method static a(Ljava/util/Set;ZZZZZZZZZZZZZZZZZ)LCI;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LCK;",
            ">;ZZZZZZZZZZZZZZZZZ)",
            "LCI;"
        }
    .end annotation

    .prologue
    .line 261
    if-nez p3, :cond_0

    if-nez p5, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->a(Z)V

    .line 262
    new-instance v3, LCL;

    const/4 v1, 0x0

    invoke-direct {v3, v1}, LCL;-><init>(LCJ;)V

    .line 263
    sget-object v2, LCK;->a:LCK;

    if-nez p12, :cond_1

    if-eqz p1, :cond_5

    :cond_1
    if-eqz p15, :cond_5

    if-nez p7, :cond_5

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 265
    sget-object v1, LCK;->c:LCK;

    invoke-virtual {v3, v1, p7}, LCL;->a(LCK;Z)LCL;

    .line 266
    sget-object v2, LCK;->b:LCK;

    if-eqz p7, :cond_6

    if-eqz p1, :cond_6

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 267
    sget-object v2, LCK;->d:LCK;

    if-eqz p2, :cond_7

    if-nez p7, :cond_7

    if-nez p12, :cond_7

    const/4 v1, 0x1

    :goto_3
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 268
    sget-object v2, LCK;->e:LCK;

    if-eqz p3, :cond_8

    if-nez p1, :cond_2

    if-eqz p4, :cond_8

    :cond_2
    if-nez p7, :cond_8

    const/4 v1, 0x1

    :goto_4
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 272
    sget-object v2, LCK;->f:LCK;

    if-eqz p3, :cond_9

    if-nez p12, :cond_9

    if-nez p1, :cond_3

    if-eqz p4, :cond_9

    :cond_3
    if-nez p7, :cond_9

    const/4 v1, 0x1

    :goto_5
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 274
    sget-object v2, LCK;->j:LCK;

    if-eqz p8, :cond_a

    if-nez p7, :cond_a

    if-nez p7, :cond_a

    if-nez p12, :cond_a

    const/4 v1, 0x1

    :goto_6
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 276
    sget-object v2, LCK;->k:LCK;

    if-eqz p2, :cond_b

    if-nez p7, :cond_b

    const/4 v1, 0x1

    :goto_7
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 278
    if-eqz p17, :cond_c

    if-eqz p5, :cond_c

    if-nez p7, :cond_c

    const/4 v1, 0x1

    .line 279
    :goto_8
    sget-object v4, LCK;->l:LCK;

    if-eqz v1, :cond_d

    if-nez p6, :cond_d

    const/4 v2, 0x1

    :goto_9
    invoke-virtual {v3, v4, v2}, LCL;->a(LCK;Z)LCL;

    .line 280
    sget-object v2, LCK;->m:LCK;

    if-eqz v1, :cond_e

    if-eqz p6, :cond_e

    const/4 v1, 0x1

    :goto_a
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 283
    sget-object v2, LCK;->g:LCK;

    if-eqz p13, :cond_f

    if-eqz p1, :cond_f

    if-nez p7, :cond_f

    const/4 v1, 0x1

    :goto_b
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 285
    sget-object v2, LCK;->h:LCK;

    if-eqz p14, :cond_10

    if-nez p7, :cond_10

    const/4 v1, 0x1

    :goto_c
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 286
    sget-object v2, LCK;->n:LCK;

    if-eqz p9, :cond_11

    if-nez p7, :cond_11

    const/4 v1, 0x1

    :goto_d
    invoke-virtual {v3, v2, v1}, LCL;->a(LCK;Z)LCL;

    .line 287
    sget-object v1, LCK;->i:LCK;

    move/from16 v0, p11

    invoke-virtual {v3, v1, v0}, LCL;->a(LCK;Z)LCL;

    .line 288
    sget-object v1, LCK;->o:LCK;

    move/from16 v0, p16

    invoke-virtual {v3, v1, v0}, LCL;->a(LCK;Z)LCL;

    .line 290
    invoke-virtual {v3, p6}, LCL;->a(Z)LCL;

    .line 291
    invoke-virtual {v3, p10}, LCL;->b(Z)LCL;

    .line 292
    invoke-virtual {v3, p0}, LCL;->a(Ljava/util/Set;)LCL;

    .line 294
    invoke-virtual {v3}, LCL;->a()LCI;

    move-result-object v1

    return-object v1

    .line 261
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 263
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 266
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 267
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 268
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 272
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 274
    :cond_a
    const/4 v1, 0x0

    goto :goto_6

    .line 276
    :cond_b
    const/4 v1, 0x0

    goto :goto_7

    .line 278
    :cond_c
    const/4 v1, 0x0

    goto :goto_8

    .line 279
    :cond_d
    const/4 v2, 0x0

    goto :goto_9

    .line 280
    :cond_e
    const/4 v1, 0x0

    goto :goto_a

    .line 283
    :cond_f
    const/4 v1, 0x0

    goto :goto_b

    .line 285
    :cond_10
    const/4 v1, 0x0

    goto :goto_c

    .line 286
    :cond_11
    const/4 v1, 0x0

    goto :goto_d
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 323
    iget-object v0, p0, LCI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCK;

    .line 324
    invoke-virtual {v0}, LCK;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    :cond_0
    return-object v1
.end method

.method public a(LCK;)Z
    .locals 1

    .prologue
    .line 317
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    iget-object v0, p0, LCI;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 331
    if-ne p0, p1, :cond_1

    .line 338
    :cond_0
    :goto_0
    return v0

    .line 333
    :cond_1
    instance-of v2, p1, LCI;

    if-nez v2, :cond_2

    move v0, v1

    .line 334
    goto :goto_0

    .line 336
    :cond_2
    check-cast p1, LCI;

    .line 337
    iget-boolean v2, p0, LCI;->a:Z

    iget-boolean v3, p1, LCI;->a:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LCI;->b:Z

    iget-boolean v3, p1, LCI;->b:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LCI;->a:Ljava/util/Set;

    iget-object v3, p1, LCI;->a:Ljava/util/Set;

    .line 338
    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 344
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LCI;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LCI;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LCI;->a:Ljava/util/Set;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 349
    const-string v0, "MenuItemsState[enabledMenuItems=%s, isPinned=%s, isCollection=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LCI;->a:Ljava/util/Set;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, LCI;->a:Z

    .line 350
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, LCI;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 349
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
