.class public Lsj;
.super LaGz;
.source "DocListActivity.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocListActivity;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocListActivity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 1297
    iput-object p1, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iput-object p3, p0, Lsj;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0, p2}, LaGz;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method


# virtual methods
.method protected a(LaGu;)V
    .locals 4

    .prologue
    .line 1305
    invoke-interface {p1}, LaGu;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1308
    iget-object v0, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    invoke-static {v0}, Lwr;->a(Ljava/util/List;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1309
    iget-object v0, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    iget-object v1, p0, Lsj;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v2, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v2, v2, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    iget-object v3, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v3, v3, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvO;

    invoke-static {v1, v2, v3}, Lwr;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFO;LvO;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lwm;->a(Ljava/util/List;)V

    .line 1315
    :goto_0
    iget-object v0, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    iget-object v1, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()LbmF;

    move-result-object v1

    invoke-interface {v0, v1}, LAP;->a(LbmF;)V

    .line 1320
    :goto_1
    iget-object v0, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocListActivity;->a(Lcom/google/android/apps/docs/app/DocListActivity;)LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->b()V

    .line 1321
    return-void

    .line 1312
    :cond_0
    iget-object v0, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    iget-object v1, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()LbmF;

    move-result-object v1

    iget-object v2, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v2, v2, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvO;

    iget-object v3, p0, Lsj;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 1313
    invoke-interface {v2, v3}, LvO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v2

    .line 1312
    invoke-static {v1, v2}, Lwr;->a(Ljava/util/List;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LbmF;

    move-result-object v1

    invoke-interface {v0, v1}, Lwm;->a(Ljava/util/List;)V

    goto :goto_0

    .line 1317
    :cond_1
    iget-object v0, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v1, p0, Lsj;->a:Lcom/google/android/apps/docs/app/DocListActivity;

    iget-object v2, p0, Lsj;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lala;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/DocListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
