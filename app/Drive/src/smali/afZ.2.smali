.class LafZ;
.super Ljava/lang/Object;
.source "ContentSyncOverallStatusNotifier.java"


# instance fields
.field private a:I

.field private a:J

.field private final a:LahS;

.field private final a:Lbpi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpi",
            "<",
            "LafT;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LahS;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput v0, p0, LafZ;->a:I

    .line 110
    iput v0, p0, LafZ;->b:I

    .line 111
    iput v0, p0, LafZ;->c:I

    .line 112
    const-class v0, LafT;

    .line 113
    invoke-static {v0}, Lbmk;->a(Ljava/lang/Class;)Lbmk;

    move-result-object v0

    iput-object v0, p0, LafZ;->a:Lbpi;

    .line 114
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LafZ;->a:J

    .line 117
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahS;

    iput-object v0, p0, LafZ;->a:LahS;

    .line 118
    return-void
.end method


# virtual methods
.method public a()Lahl;
    .locals 8

    .prologue
    .line 138
    new-instance v0, Lahl;

    iget v1, p0, LafZ;->a:I

    iget v2, p0, LafZ;->b:I

    iget v3, p0, LafZ;->c:I

    const/4 v4, 0x0

    iget-object v5, p0, LafZ;->a:Lbpi;

    invoke-static {v5}, LbmV;->a(Ljava/lang/Iterable;)LbmV;

    move-result-object v5

    iget-wide v6, p0, LafZ;->a:J

    invoke-direct/range {v0 .. v7}, Lahl;-><init>(IIIILbmV;J)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 143
    iput v0, p0, LafZ;->a:I

    .line 144
    iput v0, p0, LafZ;->b:I

    .line 145
    iput v0, p0, LafZ;->c:I

    .line 146
    iget-object v0, p0, LafZ;->a:Lbpi;

    invoke-interface {v0}, Lbpi;->clear()V

    .line 147
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LafZ;->a:J

    .line 148
    return-void
.end method

.method public a(LahF;)V
    .locals 4

    .prologue
    .line 121
    invoke-interface {p1}, LahF;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 122
    iget v0, p0, LafZ;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LafZ;->b:I

    .line 132
    :goto_0
    invoke-interface {p1}, LahF;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LafZ;->a:LahS;

    invoke-interface {p1}, LahF;->a()LahS;

    move-result-object v1

    invoke-virtual {v0, v1}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    :cond_0
    iget-wide v0, p0, LafZ;->a:J

    invoke-interface {p1}, LahF;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LafZ;->a:J

    .line 135
    :cond_1
    return-void

    .line 123
    :cond_2
    invoke-interface {p1}, LahF;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 124
    iget v0, p0, LafZ;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LafZ;->c:I

    goto :goto_0

    .line 125
    :cond_3
    invoke-interface {p1}, LahF;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 126
    invoke-interface {p1}, LahF;->a()LahR;

    move-result-object v0

    .line 127
    iget-object v1, p0, LafZ;->a:Lbpi;

    invoke-virtual {v0}, LahR;->a()LafT;

    move-result-object v0

    invoke-interface {v1, v0}, Lbpi;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    :cond_4
    iget v0, p0, LafZ;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LafZ;->a:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 152
    const-string v0, "TaskCounter[type=%s, active=%d, completed=%d, failed=%d, waiting=%d,bytesProcessed=%d"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LafZ;->a:LahS;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LafZ;->a:I

    .line 156
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, LafZ;->b:I

    .line 157
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, LafZ;->c:I

    .line 158
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, LafZ;->a:Lbpi;

    .line 159
    invoke-interface {v3}, Lbpi;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v4, p0, LafZ;->a:J

    .line 160
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 153
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
