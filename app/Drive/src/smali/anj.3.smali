.class public Lanj;
.super Ljava/lang/Object;
.source "UiThreadHandler.java"


# static fields
.field private static final a:Landroid/os/Handler;

.field private static final a:Ljava/lang/Thread;

.field private static final a:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lanj;->a:Landroid/os/Handler;

    .line 22
    new-instance v0, LalI;

    sget-object v1, Lanj;->a:Landroid/os/Handler;

    invoke-direct {v0, v1}, LalI;-><init>(Landroid/os/Handler;)V

    sput-object v0, Lanj;->a:Ljava/util/concurrent/Executor;

    .line 24
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lanj;->a:Ljava/lang/Thread;

    return-void
.end method

.method public static a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lanj;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v1

    .line 57
    invoke-static {}, Lanj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LbsY;->a()LbsW;

    move-result-object v0

    .line 58
    :goto_0
    new-instance v2, Lank;

    invoke-direct {v2, v1, p0, p1}, Lank;-><init>(Lbtd;Ljava/util/concurrent/Callable;Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 68
    invoke-static {v1, p1}, Lalv;->a(Ljava/util/concurrent/Future;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 57
    :cond_0
    sget-object v0, Lanj;->a:Ljava/util/concurrent/Executor;

    goto :goto_0
.end method

.method public static a()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lanj;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static a()V
    .locals 5

    .prologue
    .line 44
    invoke-static {}, Lanj;->a()Z

    move-result v0

    const-string v1, "Not on UI thread. Current thread=%s, UI thread=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 45
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lanj;->a:Ljava/lang/Thread;

    aput-object v4, v2, v3

    .line 44
    invoke-static {v0, v1, v2}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 46
    return-void
.end method

.method public static a(LbsU;LbsJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<TT;>;",
            "LbsJ",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-static {p0, p1, v0}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 50
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 40
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lanj;->a:Ljava/lang/Thread;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
