.class public LTQ;
.super Ljava/lang/Object;
.source "UserAgentStringGeneratorProvider.java"

# interfaces
.implements Lbxw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbxw",
        "<",
        "LTO;",
        ">;"
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;
    .annotation runtime Lajg;
    .end annotation
.end field

.field a:LbiP;
    .annotation runtime LQJ;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LTO;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "Cakemix.Debug"

    invoke-direct {p0, v0}, LTQ;-><init>(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, LTQ;->a:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public a()LTO;
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, LTQ;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, LTP;

    iget-object v1, p0, LTQ;->a:Ljava/lang/String;

    iget-object v2, p0, LTQ;->a:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, LTP;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 44
    :goto_0
    return-object v0

    .line 42
    :cond_0
    iget-object v0, p0, LTQ;->a:LbiP;

    .line 44
    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    goto :goto_0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, LTQ;->a()LTO;

    move-result-object v0

    return-object v0
.end method
