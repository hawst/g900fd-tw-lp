.class public LNE;
.super Landroid/os/AsyncTask;
.source "AudioPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, LNE;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;LNC;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0, p1}, LNE;-><init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, LNE;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    iget-object v1, p0, LNE;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v1}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 128
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LNE;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LNE;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, LNE;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)LNB;

    move-result-object v0

    invoke-virtual {v0}, LNB;->prepareAsync()V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, LNE;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->finish()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 118
    check-cast p1, [Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {p0, p1}, LNE;->a([Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 118
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LNE;->a(Ljava/lang/Boolean;)V

    return-void
.end method
