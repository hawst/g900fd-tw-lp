.class public LDK;
.super Ljava/lang/Object;
.source "SqlWhereClauseHelper.java"


# direct methods
.method public static a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 4

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LaER;->a()LaER;

    move-result-object v2

    invoke-virtual {v2}, LaER;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in (select "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LaEx;->a:LaEx;

    .line 56
    invoke-virtual {v2}, LaEx;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 57
    invoke-static {}, LaEw;->a()LaEw;

    move-result-object v2

    invoke-virtual {v2}, LaEw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " where "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LaEx;->b:LaEx;

    .line 58
    invoke-virtual {v2}, LaEx;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LaFM;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LaES;->E:LaES;

    .line 45
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-virtual {p0}, LaFM;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-object v0
.end method

.method public static a(LaFr;Laay;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 10

    .prologue
    .line 67
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->c:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 70
    invoke-virtual {p1}, Laay;->a()LbmF;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, LbmF;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    invoke-virtual {v1}, LbmF;->a()Lbqv;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    const-string v3, "\\|"

    const-string v4, "||"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    const-string v3, "%"

    const-string v4, "|%"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    const-string v3, "_"

    const-string v4, "|_"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    sget-object v3, LaFL;->a:LaFL;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " like ? escape \"|\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "%"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v7, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    invoke-virtual {v3, v1, v4}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    move-object v1, v0

    .line 78
    goto :goto_0

    :cond_0
    move-object v1, v0

    .line 80
    :cond_1
    return-object v1
.end method

.method public static a(Laay;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    invoke-virtual {p0}, Laay;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    sget-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->d:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 109
    :goto_0
    return-object v0

    .line 92
    :cond_0
    sget-object v0, LaES;->a:LaES;

    .line 93
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-static {v0, p0}, LDK;->a(LaFr;Laay;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v3

    .line 95
    invoke-virtual {p0}, Laay;->a()LbmF;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    invoke-virtual {p0}, Laay;->a()J

    move-result-wide v4

    .line 98
    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    .line 99
    cmp-long v0, v4, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    sget-object v4, LaFL;->b:LaFL;

    new-array v5, v1, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v6, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LaES;->p:LaES;

    invoke-virtual {v8}, LaES;->a()LaFr;

    move-result-object v8

    invoke-virtual {v8}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " in (SELECT "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, LaEr;->b:LaEr;

    .line 102
    invoke-virtual {v8}, LaEr;->a()LaFr;

    move-result-object v8

    invoke-virtual {v8}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " FROM "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 103
    invoke-static {}, LaEq;->a()LaEq;

    move-result-object v8

    invoke-virtual {v8}, LaEq;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " WHERE "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, LaEr;->a:LaEr;

    .line 104
    invoke-virtual {v8}, LaEr;->a()LaFr;

    move-result-object v8

    invoke-virtual {v8}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=?)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v2

    .line 101
    invoke-virtual {v4, v3, v5}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 108
    :goto_2
    sget-object v3, LaFL;->a:LaFL;

    new-array v1, v1, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    sget-object v4, LaES;->x:LaES;

    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v3, v0, v1}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 99
    goto/16 :goto_1

    :cond_2
    move-object v0, v3

    goto :goto_2
.end method

.method public static a(LbmY;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;)",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    invoke-virtual {p0}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 139
    const-class v0, LaGv;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    sget-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->c:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 155
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 138
    goto :goto_0

    .line 143
    :cond_1
    sget-object v0, LaGv;->k:LaGv;

    invoke-virtual {p0, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 144
    if-eqz v3, :cond_2

    .line 147
    invoke-static {p0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p0

    .line 149
    :cond_2
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 150
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGv;

    .line 151
    invoke-virtual {v0}, LaGv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 153
    :cond_3
    sget-object v0, LaES;->t:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 154
    if-nez v3, :cond_4

    :goto_3
    invoke-static {v1, v0, v4}, LDK;->a(ZLjava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    .line 155
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move v1, v2

    .line 154
    goto :goto_3
.end method

.method public static a(LbmY;LbmY;Z)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;"
        }
    .end annotation

    .prologue
    .line 160
    sget-object v0, LaFL;->b:LaFL;

    invoke-static {p0}, LDK;->a(LbmY;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v3, 0x0

    .line 161
    invoke-static {p1, p2}, LDK;->a(LbmY;Z)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    .line 160
    invoke-virtual {v0, v1, v2}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbmY;Z)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 118
    const/4 v0, 0x0

    .line 120
    invoke-virtual {p0}, LbmY;->a()Lbqv;

    move-result-object v4

    move-object v3, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 121
    invoke-static {v0}, Lwj;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 123
    if-nez v3, :cond_1

    :goto_2
    move-object v3, v0

    .line 128
    goto :goto_1

    :cond_0
    move v0, v2

    .line 117
    goto :goto_0

    .line 126
    :cond_1
    sget-object v5, LaFL;->b:LaFL;

    new-array v6, v1, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    aput-object v0, v6, v2

    invoke-virtual {v5, v3, v6}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    goto :goto_2

    .line 130
    :cond_2
    if-eqz p1, :cond_3

    .line 131
    sget-object v0, LaFL;->b:LaFL;

    new-array v1, v1, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    sget-object v4, Lwk;->a:Lwk;

    invoke-virtual {v4}, Lwk;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v3

    .line 134
    :cond_3
    return-object v3
.end method

.method public static a(LaFr;Ljava/util/Collection;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFr;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 165
    const/4 v0, 0x1

    invoke-virtual {p0}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, LDK;->a(ZLjava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(ZLjava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 171
    if-eqz p0, :cond_0

    const-string v0, "IN"

    .line 172
    :goto_0
    const-string v1, ""

    invoke-static {v1}, LbiI;->a(Ljava/lang/String;)LbiI;

    move-result-object v1

    invoke-virtual {v1, p2}, LbiI;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid symbol \' in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LbiT;->a(ZLjava/lang/Object;)V

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 177
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 178
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 171
    :cond_0
    const-string v0, "NOT IN"

    goto :goto_0

    .line 172
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 180
    :cond_2
    const-string v0, ","

    invoke-static {v0}, LbiI;->a(Ljava/lang/String;)LbiI;

    move-result-object v0

    invoke-virtual {v0, v2}, LbiI;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string v0, "))"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
