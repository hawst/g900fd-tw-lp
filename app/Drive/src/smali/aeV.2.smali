.class public LaeV;
.super Ljava/lang/Object;
.source "DatabaseSyncMoreController.java"

# interfaces
.implements Lafw;


# instance fields
.field private a:I

.field private final a:LQr;

.field private a:Laff;

.field private final a:Lafz;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Laff;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;

.field private a:Z


# direct methods
.method public constructor <init>(LtK;LQr;Lafz;Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LtK;",
            "LQr;",
            "Lafz;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput v0, p0, LaeV;->a:I

    .line 81
    iput-object p1, p0, LaeV;->a:LtK;

    .line 82
    iput-object p2, p0, LaeV;->a:LQr;

    .line 83
    iput-object p3, p0, LaeV;->a:Lafz;

    .line 84
    iput-object p4, p0, LaeV;->a:Laja;

    .line 85
    return-void
.end method

.method static synthetic a(LaeV;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, LaeV;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaeV;->a:I

    return v0
.end method

.method static synthetic a(LaeV;)LQr;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LaeV;->a:LQr;

    return-object v0
.end method

.method static synthetic a(LaeV;)Laff;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LaeV;->a:Laff;

    return-object v0
.end method

.method static synthetic a(LaeV;Laff;)Laff;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, LaeV;->a:Laff;

    return-object p1
.end method

.method static synthetic a(LaeV;)Lafz;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LaeV;->a:Lafz;

    return-object v0
.end method

.method static synthetic a(LaeV;)Laja;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LaeV;->a:Laja;

    return-object v0
.end method

.method private a(LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 107
    iget-object v0, p0, LaeV;->a:LtK;

    sget-object v1, Lry;->aF:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-static {}, LamV;->a()V

    .line 112
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    if-eqz p4, :cond_2

    .line 117
    iget-object v0, p0, LaeV;->a:Laff;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaeV;->a:Laff;

    invoke-interface {v0}, Laff;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LaeV;->a:Laff;

    .line 118
    invoke-interface {v0}, Laff;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    iget-object v0, p0, LaeV;->a:LQr;

    const-string v1, "syncMoreMaxFeedsToRetrieve_r2"

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v7

    .line 122
    new-instance v0, LaeX;

    iget-object v1, p0, LaeV;->a:Laja;

    .line 123
    invoke-virtual {v1}, Laja;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LaeX;-><init>(LaeV;Landroid/content/Context;ZLaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;)V

    .line 124
    iget-object v1, p0, LaeV;->a:Laff;

    invoke-interface {v1, v0, v7}, Laff;->a(Lafg;I)V

    .line 126
    :cond_1
    invoke-direct {p0, p3}, LaeV;->a(Lafy;)V

    goto :goto_0

    .line 129
    :cond_2
    iget-object v0, p0, LaeV;->a:Laff;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaeV;->a:Laff;

    invoke-interface {v0}, Laff;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 130
    :cond_3
    invoke-direct {p0, p1, p2, p3, v3}, LaeV;->b(LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V

    goto :goto_0

    .line 132
    :cond_4
    invoke-direct {p0, p3}, LaeV;->a(Lafy;)V

    goto :goto_0
.end method

.method static synthetic a(LaeV;LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, LaeV;->b(LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V

    return-void
.end method

.method static synthetic a(LaeV;Lafy;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, LaeV;->a(Lafy;)V

    return-void
.end method

.method private a(Lafy;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, LaeV;->a:Laff;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaeV;->a:Laff;

    invoke-interface {v0}, Laff;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    :cond_0
    sget-object v0, Lafx;->a:Lafx;

    invoke-interface {p1, v0}, Lafy;->a(Lafx;)V

    .line 181
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, LaeV;->a:Laff;

    invoke-interface {v0}, Laff;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    sget-object v0, Lafx;->c:Lafx;

    invoke-interface {p1, v0}, Lafy;->a(Lafx;)V

    goto :goto_0

    .line 179
    :cond_2
    sget-object v0, Lafx;->b:Lafx;

    invoke-interface {p1, v0}, Lafy;->a(Lafx;)V

    goto :goto_0
.end method

.method static synthetic a(LaeV;Z)Z
    .locals 0

    .prologue
    .line 26
    iput-boolean p1, p0, LaeV;->a:Z

    return p1
.end method

.method static synthetic b(LaeV;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, LaeV;->a:I

    return v0
.end method

.method private b(LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V
    .locals 6

    .prologue
    .line 144
    new-instance v0, LaeW;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LaeW;-><init>(LaeV;LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;ZLafy;)V

    iput-object v0, p0, LaeV;->a:Landroid/os/AsyncTask;

    .line 170
    iget-object v0, p0, LaeV;->a:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 171
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 185
    iget-object v0, p0, LaeV;->a:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, LaeV;->a:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 187
    iput-object v2, p0, LaeV;->a:Landroid/os/AsyncTask;

    .line 189
    :cond_0
    iget-object v0, p0, LaeV;->a:Laff;

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, LaeV;->a:Laff;

    invoke-interface {v0}, Laff;->a()V

    .line 191
    iput-object v2, p0, LaeV;->a:Laff;

    .line 193
    :cond_1
    return-void
.end method

.method public a(LaFM;LQX;Lafy;)V
    .locals 2

    .prologue
    .line 90
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-virtual {p2}, LQX;->a()LaFX;

    move-result-object v0

    invoke-virtual {v0}, LaFX;->a()Z

    move-result v0

    iput-boolean v0, p0, LaeV;->a:Z

    .line 92
    invoke-virtual {p2}, LQX;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, p3, v1}, LaeV;->a(LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V

    .line 93
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, LaeV;->a:Z

    return v0
.end method

.method public b(LaFM;LQX;Lafy;)V
    .locals 2

    .prologue
    .line 98
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-virtual {p2}, LQX;->a()LaFX;

    move-result-object v0

    invoke-virtual {v0}, LaFX;->a()Z

    move-result v0

    iput-boolean v0, p0, LaeV;->a:Z

    .line 100
    iget-boolean v0, p0, LaeV;->a:Z

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p2}, LQX;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p3, v1}, LaeV;->a(LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V

    .line 103
    :cond_0
    return-void
.end method
