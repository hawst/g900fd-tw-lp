.class LIR;
.super Ljava/lang/Object;
.source "TimeRangeSectionIndexerFactory.java"


# instance fields
.field private final a:LaKM;

.field private final a:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/content/Context;LaKM;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LIR;->a:Landroid/content/res/Resources;

    .line 31
    iput-object p2, p0, LIR;->a:LaKM;

    .line 32
    return-void
.end method

.method static a(Ljava/util/Calendar;Landroid/content/res/Resources;Z)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Landroid/content/res/Resources;",
            "Z)",
            "Ljava/util/List",
            "<",
            "LIO;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 44
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v3

    .line 46
    if-eqz p2, :cond_0

    .line 48
    sget v0, Lxi;->fast_scroll_title_grouper_collections:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v8, v1}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    .line 51
    :cond_0
    sget v0, Lxi;->fast_scroll_time_grouper_today:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v6, v1}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    .line 52
    invoke-virtual {p0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 53
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 54
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 55
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 56
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 59
    sget v1, Lxi;->fast_scroll_time_grouper_yesterday:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v6, v0}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    .line 61
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 62
    invoke-virtual {v1, v9, v7}, Ljava/util/Calendar;->add(II)V

    .line 65
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Calendar;

    .line 66
    const/4 v4, 0x7

    invoke-virtual {v2}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 67
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v4

    if-gez v4, :cond_1

    .line 68
    sget v4, Lxi;->fast_scroll_time_grouper_this_week:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6, v1}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    move-object v1, v2

    .line 74
    :cond_1
    sget v4, Lxi;->fast_scroll_time_grouper_last_week:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6, v1}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    .line 77
    const/4 v1, 0x4

    invoke-virtual {v2, v1, v7}, Ljava/util/Calendar;->add(II)V

    .line 80
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 81
    invoke-virtual {v1, v9, v8}, Ljava/util/Calendar;->set(II)V

    .line 82
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v4

    if-gez v4, :cond_2

    .line 83
    sget v4, Lxi;->fast_scroll_time_grouper_this_month:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6, v2}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    move-object v2, v1

    .line 89
    :cond_2
    sget v4, Lxi;->fast_scroll_time_grouper_last_month:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6, v2}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    .line 92
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v7}, Ljava/util/Calendar;->add(II)V

    .line 95
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 96
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v8}, Ljava/util/Calendar;->set(II)V

    .line 97
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v2

    if-gez v2, :cond_3

    .line 98
    sget v2, Lxi;->fast_scroll_time_grouper_this_year:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v6, v1}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    move-object v1, v0

    .line 104
    :cond_3
    sget v2, Lxi;->fast_scroll_time_grouper_last_year:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v6, v1}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    .line 107
    invoke-virtual {v0, v8, v7}, Ljava/util/Calendar;->add(II)V

    .line 110
    sget v1, Lxi;->fast_scroll_time_grouper_earlier:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v6, v0}, LIR;->a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V

    .line 112
    invoke-virtual {v3}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method private static a(LbmH;Ljava/lang/String;ZLjava/util/Calendar;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmH",
            "<",
            "LIO;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Calendar;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 36
    if-nez p2, :cond_0

    move v2, v3

    .line 37
    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    neg-long v0, v0

    .line 38
    :goto_1
    new-instance v5, LIO;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v6, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-static {v6}, LIL;->a([Ljava/lang/Object;)LIL;

    move-result-object v0

    invoke-direct {v5, p1, v0}, LIO;-><init>(Ljava/lang/String;LIL;)V

    invoke-virtual {p0, v5}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 39
    return-void

    :cond_0
    move v2, v4

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_1
.end method


# virtual methods
.method public a(LDC;Z)Landroid/widget/SectionIndexer;
    .locals 4

    .prologue
    .line 116
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 117
    iget-object v1, p0, LIR;->a:LaKM;

    invoke-interface {v1}, LaKM;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 118
    iget-object v1, p0, LIR;->a:Landroid/content/res/Resources;

    invoke-static {v0, v1, p2}, LIR;->a(Ljava/util/Calendar;Landroid/content/res/Resources;Z)Ljava/util/List;

    move-result-object v0

    .line 119
    new-instance v1, LIN;

    const/4 v2, 0x0

    new-array v2, v2, [LIO;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIw;

    invoke-direct {v1, p1, v0}, LIN;-><init>(LDC;[LIw;)V

    return-object v1
.end method
