.class public abstract LarD;
.super Ljava/lang/Object;
.source "AbstractCascadingAnimatorFactory.java"

# interfaces
.implements LarF;


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 17
    const-wide/16 v0, 0x64

    const-wide/16 v2, 0x14

    invoke-direct {p0, v0, v1, v2, v3}, LarD;-><init>(JJ)V

    .line 18
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-wide p1, p0, LarD;->a:J

    .line 22
    iput-wide p3, p0, LarD;->b:J

    .line 23
    return-void
.end method


# virtual methods
.method public final a(J)LarE;
    .locals 3

    .prologue
    .line 37
    iget-wide v0, p0, LarD;->a:J

    invoke-virtual {p0, v0, v1, p1, p2}, LarD;->a(JJ)LarE;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(JJ)LarE;
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 27
    invoke-static {}, LakQ;->b()Z

    move-result v0

    return v0
.end method
