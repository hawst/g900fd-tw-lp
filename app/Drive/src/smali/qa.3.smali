.class public final Lqa;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LpY;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LpW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 39
    iput-object p1, p0, Lqa;->a:LbrA;

    .line 40
    const-class v0, LpY;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lqa;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lqa;->a:Lbsk;

    .line 43
    const-class v0, LpW;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, Lqa;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lqa;->b:Lbsk;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 67
    packed-switch p1, :pswitch_data_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :pswitch_0
    new-instance v1, LpY;

    iget-object v0, p0, Lqa;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 72
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lqa;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 70
    invoke-static {v0, v2}, Lqa;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, LpY;-><init>(Laja;)V

    .line 77
    return-object v1

    .line 67
    :pswitch_data_0
    .packed-switch 0x36
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 94
    packed-switch p2, :pswitch_data_0

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :pswitch_0
    check-cast p1, LpZ;

    .line 98
    iget-object v0, p0, Lqa;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lqa;

    iget-object v0, v0, Lqa;->a:Lbsk;

    .line 101
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpY;

    .line 98
    invoke-virtual {p1, v0}, LpZ;->provideAccountFlagStore(LpY;)LpW;

    move-result-object v0

    return-object v0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x37
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 53
    const-class v0, LpY;

    iget-object v1, p0, Lqa;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lqa;->a(Ljava/lang/Class;Lbsk;)V

    .line 54
    const-class v0, LpW;

    iget-object v1, p0, Lqa;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lqa;->a(Ljava/lang/Class;Lbsk;)V

    .line 55
    iget-object v0, p0, Lqa;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x36

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 57
    iget-object v0, p0, Lqa;->b:Lbsk;

    const-class v1, LpZ;

    const/16 v2, 0x37

    invoke-virtual {p0, v1, v2}, Lqa;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 59
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 86
    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method
