.class public LaER;
.super LaEF;
.source "EntryTable.java"


# static fields
.field private static final a:LaER;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, LaER;

    invoke-direct {v0}, LaER;-><init>()V

    sput-object v0, LaER;->a:LaER;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, LaEF;-><init>()V

    .line 57
    return-void
.end method

.method public static a()LaER;
    .locals 1

    .prologue
    .line 52
    sget-object v0, LaER;->a:LaER;

    return-object v0
.end method

.method public static final a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LaES;->y:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 294
    sget-object v0, LaES;->t:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 324
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LaES;->a:LaES;

    .line 326
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LaES;->t:LaES;

    .line 327
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LaES;->v:LaES;

    .line 328
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaES;->g:LaES;

    .line 329
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaES;->y:LaES;

    .line 330
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaES;->l:LaES;

    .line 331
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaES;->x:LaES;

    .line 332
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaES;->e:LaES;

    .line 333
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaES;->h:LaES;

    .line 334
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaES;->p:LaES;

    .line 335
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaES;->q:LaES;

    .line 336
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LaES;->r:LaES;

    .line 337
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 338
    invoke-static {}, LaER;->a()LaER;

    move-result-object v2

    invoke-virtual {v2}, LaER;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method static synthetic b()LaER;
    .locals 1

    .prologue
    .line 25
    sget-object v0, LaER;->a:LaER;

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaES;->t:LaES;

    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<>\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {v1}, LaGv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 309
    sget-object v0, LaES;->p:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 310
    sget-object v1, LaES;->q:LaES;

    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 311
    const/4 v0, 0x0

    .line 313
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, "Entry"

    return-object v0
.end method

.method protected a()[LaES;
    .locals 1

    .prologue
    .line 61
    invoke-static {}, LaES;->values()[LaES;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a()[Lbjv;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, LaER;->a()[LaES;

    move-result-object v0

    return-object v0
.end method
