.class public Lasy;
.super Ljava/lang/Object;
.source "PromotionEnabledLogic.java"


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:LQr;

.field a:Lasz;

.field private a:Z

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 38
    const-string v0, "at"

    const-string v1, "be"

    const-string v2, "bg"

    const-string v3, "cy"

    const-string v4, "cz"

    const-string v5, "dk"

    const/16 v6, 0x2f

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "ee"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "fi"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "fr"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "de"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "gr"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "hu"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "is"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "ie"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "il"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const-string v8, "it"

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "lv"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    const-string v8, "li"

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string v8, "lt"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    const-string v8, "lu"

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string v8, "mt"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    const-string v8, "nl"

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string v8, "no"

    aput-object v8, v6, v7

    const/16 v7, 0x11

    const-string v8, "pl"

    aput-object v8, v6, v7

    const/16 v7, 0x12

    const-string v8, "pt"

    aput-object v8, v6, v7

    const/16 v7, 0x13

    const-string v8, "ro"

    aput-object v8, v6, v7

    const/16 v7, 0x14

    const-string v8, "ru"

    aput-object v8, v6, v7

    const/16 v7, 0x15

    const-string v8, "sk"

    aput-object v8, v6, v7

    const/16 v7, 0x16

    const-string v8, "si"

    aput-object v8, v6, v7

    const/16 v7, 0x17

    const-string v8, "es"

    aput-object v8, v6, v7

    const/16 v7, 0x18

    const-string v8, "se"

    aput-object v8, v6, v7

    const/16 v7, 0x19

    const-string v8, "ch"

    aput-object v8, v6, v7

    const/16 v7, 0x1a

    const-string v8, "tr"

    aput-object v8, v6, v7

    const/16 v7, 0x1b

    const-string v8, "ua"

    aput-object v8, v6, v7

    const/16 v7, 0x1c

    const-string v8, "gb"

    aput-object v8, v6, v7

    const/16 v7, 0x1d

    const-string v8, "ar"

    aput-object v8, v6, v7

    const/16 v7, 0x1e

    const-string v8, "br"

    aput-object v8, v6, v7

    const/16 v7, 0x1f

    const-string v8, "ca"

    aput-object v8, v6, v7

    const/16 v7, 0x20

    const-string v8, "cl"

    aput-object v8, v6, v7

    const/16 v7, 0x21

    const-string v8, "mx"

    aput-object v8, v6, v7

    const/16 v7, 0x22

    const-string v8, "us"

    aput-object v8, v6, v7

    const/16 v7, 0x23

    const-string v8, "au"

    aput-object v8, v6, v7

    const/16 v7, 0x24

    const-string v8, "nz"

    aput-object v8, v6, v7

    const/16 v7, 0x25

    const-string v8, "in"

    aput-object v8, v6, v7

    const/16 v7, 0x26

    const-string v8, "id"

    aput-object v8, v6, v7

    const/16 v7, 0x27

    const-string v8, "hk"

    aput-object v8, v6, v7

    const/16 v7, 0x28

    const-string v8, "jp"

    aput-object v8, v6, v7

    const/16 v7, 0x29

    const-string v8, "kr"

    aput-object v8, v6, v7

    const/16 v7, 0x2a

    const-string v8, "my"

    aput-object v8, v6, v7

    const/16 v7, 0x2b

    const-string v8, "sg"

    aput-object v8, v6, v7

    const/16 v7, 0x2c

    const-string v8, "tw"

    aput-object v8, v6, v7

    const/16 v7, 0x2d

    const-string v8, "th"

    aput-object v8, v6, v7

    const/16 v7, 0x2e

    const-string v8, "vn"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, Lasy;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lasy;->a:Z

    .line 148
    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 95
    iget-boolean v0, p0, Lasy;->a:Z

    if-eqz v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 98
    :cond_0
    iput-boolean v1, p0, Lasy;->a:Z

    .line 99
    iget-object v0, p0, Lasy;->b:Ljava/util/Set;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 100
    iget-object v0, p0, Lasy;->c:Ljava/util/Set;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, LbiT;->b(Z)V

    .line 101
    iget-object v0, p0, Lasy;->a:LQr;

    const-string v3, "oemCountryRestriction"

    const-string v4, "Off"

    invoke-interface {v0, v3, v4}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    iget-object v3, p0, Lasy;->a:LQr;

    const-string v4, "oemCountryBlacklist"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 103
    iget-object v4, p0, Lasy;->a:LQr;

    const-string v5, "oemCountryWhitelistOverride"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 105
    const-string v5, "Whitelist"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 106
    invoke-static {v4}, Lbju;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    sget-object v0, Lasy;->a:Ljava/util/Set;

    iput-object v0, p0, Lasy;->c:Ljava/util/Set;

    .line 130
    :cond_1
    const-string v0, "OfferAvailabilityService"

    const-string v3, "Country final whitelist: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lasy;->c:Ljava/util/Set;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 142
    :goto_3
    iget-object v0, p0, Lasy;->b:Ljava/util/Set;

    if-nez v0, :cond_8

    move v0, v1

    :goto_4
    iget-object v3, p0, Lasy;->c:Ljava/util/Set;

    if-nez v3, :cond_9

    :goto_5
    xor-int/2addr v0, v1

    const-string v1, "Exactly one of blacklist or whitelist must be set."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 99
    goto :goto_1

    :cond_3
    move v0, v2

    .line 100
    goto :goto_2

    .line 109
    :cond_4
    const-string v0, "OfferAvailabilityService"

    const-string v3, "Country whitelist override: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-static {v0, v3, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 110
    sget-object v0, Lasy;->a:Ljava/util/Set;

    invoke-static {v0}, LbpU;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lasy;->c:Ljava/util/Set;

    .line 111
    const-string v0, ","

    invoke-static {v0}, Lbjk;->a(Ljava/lang/String;)Lbjk;

    move-result-object v0

    invoke-virtual {v0}, Lbjk;->b()Lbjk;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbjk;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 112
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v4, v8, :cond_5

    .line 113
    const-string v4, "OfferAvailabilityService"

    const-string v5, "Country override invalid item (%s)"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_6

    .line 115
    :cond_5
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 116
    invoke-virtual {v0, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 117
    packed-switch v4, :pswitch_data_0

    .line 125
    :pswitch_0
    const-string v5, "OfferAvailabilityService"

    const-string v6, "Country override invalid operand (%s) in (%s)"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    aput-object v4, v7, v2

    aput-object v0, v7, v1

    invoke-static {v5, v6, v7}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_6

    .line 119
    :pswitch_1
    iget-object v0, p0, Lasy;->c:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_6

    .line 122
    :pswitch_2
    iget-object v0, p0, Lasy;->c:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 133
    :cond_6
    const-string v4, "Blacklist"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {v3}, Lbju;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 134
    const-string v0, ","

    invoke-static {v0}, Lbjk;->a(Ljava/lang/String;)Lbjk;

    move-result-object v0

    invoke-virtual {v0}, Lbjk;->b()Lbjk;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbjk;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LbpU;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lasy;->b:Ljava/util/Set;

    .line 139
    :goto_7
    const-string v0, "OfferAvailabilityService"

    const-string v3, "Country final blacklist: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lasy;->b:Ljava/util/Set;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_3

    .line 136
    :cond_7
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, Lasy;->b:Ljava/util/Set;

    goto :goto_7

    :cond_8
    move v0, v2

    .line 142
    goto/16 :goto_4

    :cond_9
    move v1, v2

    goto/16 :goto_5

    .line 117
    :pswitch_data_0
    .packed-switch 0x2b
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152
    iget-object v2, p0, Lasy;->a:Lasz;

    invoke-virtual {v2}, Lasz;->a()Ljava/lang/String;

    move-result-object v2

    .line 153
    const-string v3, "OfferAvailabilityService"

    const-string v4, "Detected device country : %s"

    new-array v5, v0, [Ljava/lang/Object;

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 155
    invoke-direct {p0}, Lasy;->a()V

    .line 156
    iget-object v3, p0, Lasy;->b:Ljava/util/Set;

    if-eqz v3, :cond_1

    .line 157
    iget-object v3, p0, Lasy;->b:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 159
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 157
    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lasy;->c:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
