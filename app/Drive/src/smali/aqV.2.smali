.class public abstract LaqV;
.super Ljava/lang/Object;
.source "AbstractActionBarHelper.java"

# interfaces
.implements LaqY;


# instance fields
.field private final a:I

.field private a:LaFO;

.field protected final a:Landroid/app/Activity;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;LaFO;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, LaqV;->a:Landroid/app/Activity;

    .line 23
    iput-object p2, p0, LaqV;->a:LaFO;

    .line 24
    iput p3, p0, LaqV;->a:I

    .line 25
    return-void
.end method


# virtual methods
.method protected final a()LaFO;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, LaqV;->a:LaFO;

    return-object v0
.end method

.method public a(Landroid/widget/Button;LaFO;)V
    .locals 0

    .prologue
    .line 32
    iput-object p2, p0, LaqV;->a:LaFO;

    .line 33
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 41
    if-nez p1, :cond_0

    .line 42
    iget-object v0, p0, LaqV;->a:Landroid/app/Activity;

    iget v1, p0, LaqV;->a:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 44
    :cond_0
    if-nez p2, :cond_1

    .line 45
    const-string p2, ""

    .line 47
    :cond_1
    invoke-virtual {p0, p1, p2}, LaqV;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method protected abstract b(Ljava/lang/String;Ljava/lang/String;)V
.end method
