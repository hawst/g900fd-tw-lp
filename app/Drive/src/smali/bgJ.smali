.class public LbgJ;
.super LbgG;
.source "PdfPage.java"


# instance fields
.field private final a:LbgF;

.field private final a:LbgK;

.field private final a:LbgL;

.field private final a:LbgM;


# direct methods
.method public constructor <init>(LbgL;LbgF;LbgK;LbgM;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, LbgG;-><init>()V

    .line 36
    iput-object p1, p0, LbgJ;->a:LbgL;

    .line 37
    iput-object p2, p0, LbgJ;->a:LbgF;

    .line 38
    iput-object p3, p0, LbgJ;->a:LbgK;

    .line 39
    iput-object p4, p0, LbgJ;->a:LbgM;

    .line 40
    return-void
.end method


# virtual methods
.method protected a(Ljava/io/OutputStream;)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 44
    new-instance v0, Ljava/lang/StringBuffer;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "<</Type/Page/Parent %s/Contents %s/Resources<</XObject<</%s %s>>"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LbgJ;->a:LbgL;

    .line 45
    invoke-virtual {v4}, LbgL;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, LbgJ;->a:LbgK;

    invoke-virtual {v4}, LbgK;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, LbgJ;->a:LbgF;

    invoke-virtual {v4}, LbgF;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    const/4 v4, 0x3

    iget-object v5, p0, LbgJ;->a:LbgF;

    invoke-virtual {v5}, LbgF;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 44
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 46
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]>>/MediaBox[0 0 %.0f %.0f]>>\n"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, LbgJ;->a:LbgM;

    .line 47
    invoke-virtual {v4}, LbgM;->a()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, LbgJ;->a:LbgM;

    invoke-virtual {v4}, LbgM;->b()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v7

    .line 46
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbgJ;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 49
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 50
    array-length v0, v0

    return v0
.end method
