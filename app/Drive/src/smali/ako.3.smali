.class Lako;
.super Landroid/content/BroadcastReceiver;
.source "BatteryStatus.java"


# instance fields
.field final synthetic a:Lakl;


# direct methods
.method private constructor <init>(Lakl;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lako;->a:Lakl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lakl;Lakm;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lako;-><init>(Lakl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 32
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 34
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    iget-object v0, p0, Lako;->a:Lakl;

    invoke-static {v0, p1, v3}, Lakl;->a(Lakl;Landroid/content/Context;Z)V

    .line 41
    :goto_0
    return-void

    .line 36
    :cond_0
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 37
    iget-object v0, p0, Lako;->a:Lakl;

    invoke-static {v0, p1, v4}, Lakl;->a(Lakl;Landroid/content/Context;Z)V

    goto :goto_0

    .line 39
    :cond_1
    const-string v1, "BatteryStatus"

    const-string v2, "Unexpected broadcast received: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
