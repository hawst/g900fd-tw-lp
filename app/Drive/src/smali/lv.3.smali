.class public final Llv;
.super Ljava/lang/Object;
.source "MediaRouter.java"


# instance fields
.field private a:I

.field private a:Landroid/os/Bundle;

.field private a:Landroid/view/Display;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private a:LkN;

.field private final a:Llu;

.field private a:Z

.field private b:I

.field private final b:Ljava/lang/String;

.field private b:Z

.field private c:I

.field private c:Ljava/lang/String;

.field private d:I

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Llu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llv;->a:Ljava/util/ArrayList;

    .line 710
    const/4 v0, -0x1

    iput v0, p0, Llv;->f:I

    .line 764
    iput-object p1, p0, Llv;->a:Llu;

    .line 765
    iput-object p2, p0, Llv;->a:Ljava/lang/String;

    .line 766
    iput-object p3, p0, Llv;->b:Ljava/lang/String;

    .line 767
    return-void
.end method

.method static synthetic a(Llv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Llv;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Llv;)LkN;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Llv;->a:LkN;

    return-object v0
.end method

.method static synthetic a(Llv;)Z
    .locals 1

    .prologue
    .line 695
    iget-boolean v0, p0, Llv;->a:Z

    return v0
.end method

.method static synthetic b(Llv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Llv;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1031
    iget v0, p0, Llv;->a:I

    return v0
.end method

.method a(LkN;)I
    .locals 3

    .prologue
    .line 1178
    const/4 v0, 0x0

    .line 1179
    iget-object v1, p0, Llv;->a:LkN;

    if-eq v1, p1, :cond_b

    .line 1180
    iput-object p1, p0, Llv;->a:LkN;

    .line 1181
    if-eqz p1, :cond_b

    .line 1182
    iget-object v1, p0, Llv;->c:Ljava/lang/String;

    invoke-virtual {p1}, LkN;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Llj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1183
    invoke-virtual {p1}, LkN;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llv;->c:Ljava/lang/String;

    .line 1184
    const/4 v0, 0x1

    .line 1186
    :cond_0
    iget-object v1, p0, Llv;->d:Ljava/lang/String;

    invoke-virtual {p1}, LkN;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Llj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1187
    invoke-virtual {p1}, LkN;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Llv;->d:Ljava/lang/String;

    .line 1188
    or-int/lit8 v0, v0, 0x1

    .line 1190
    :cond_1
    iget-boolean v1, p0, Llv;->a:Z

    invoke-virtual {p1}, LkN;->a()Z

    move-result v2

    if-eq v1, v2, :cond_2

    .line 1191
    invoke-virtual {p1}, LkN;->a()Z

    move-result v1

    iput-boolean v1, p0, Llv;->a:Z

    .line 1192
    or-int/lit8 v0, v0, 0x1

    .line 1194
    :cond_2
    iget-boolean v1, p0, Llv;->b:Z

    invoke-virtual {p1}, LkN;->b()Z

    move-result v2

    if-eq v1, v2, :cond_3

    .line 1195
    invoke-virtual {p1}, LkN;->b()Z

    move-result v1

    iput-boolean v1, p0, Llv;->b:Z

    .line 1196
    or-int/lit8 v0, v0, 0x1

    .line 1198
    :cond_3
    iget-object v1, p0, Llv;->a:Ljava/util/ArrayList;

    invoke-virtual {p1}, LkN;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1199
    iget-object v1, p0, Llv;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1200
    iget-object v1, p0, Llv;->a:Ljava/util/ArrayList;

    invoke-virtual {p1}, LkN;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1201
    or-int/lit8 v0, v0, 0x1

    .line 1203
    :cond_4
    iget v1, p0, Llv;->a:I

    invoke-virtual {p1}, LkN;->a()I

    move-result v2

    if-eq v1, v2, :cond_5

    .line 1204
    invoke-virtual {p1}, LkN;->a()I

    move-result v1

    iput v1, p0, Llv;->a:I

    .line 1205
    or-int/lit8 v0, v0, 0x1

    .line 1207
    :cond_5
    iget v1, p0, Llv;->b:I

    invoke-virtual {p1}, LkN;->b()I

    move-result v2

    if-eq v1, v2, :cond_6

    .line 1208
    invoke-virtual {p1}, LkN;->b()I

    move-result v1

    iput v1, p0, Llv;->b:I

    .line 1209
    or-int/lit8 v0, v0, 0x1

    .line 1211
    :cond_6
    iget v1, p0, Llv;->c:I

    invoke-virtual {p1}, LkN;->e()I

    move-result v2

    if-eq v1, v2, :cond_7

    .line 1212
    invoke-virtual {p1}, LkN;->e()I

    move-result v1

    iput v1, p0, Llv;->c:I

    .line 1213
    or-int/lit8 v0, v0, 0x3

    .line 1215
    :cond_7
    iget v1, p0, Llv;->d:I

    invoke-virtual {p1}, LkN;->c()I

    move-result v2

    if-eq v1, v2, :cond_8

    .line 1216
    invoke-virtual {p1}, LkN;->c()I

    move-result v1

    iput v1, p0, Llv;->d:I

    .line 1217
    or-int/lit8 v0, v0, 0x3

    .line 1219
    :cond_8
    iget v1, p0, Llv;->e:I

    invoke-virtual {p1}, LkN;->d()I

    move-result v2

    if-eq v1, v2, :cond_9

    .line 1220
    invoke-virtual {p1}, LkN;->d()I

    move-result v1

    iput v1, p0, Llv;->e:I

    .line 1221
    or-int/lit8 v0, v0, 0x3

    .line 1223
    :cond_9
    iget v1, p0, Llv;->f:I

    invoke-virtual {p1}, LkN;->f()I

    move-result v2

    if-eq v1, v2, :cond_a

    .line 1224
    invoke-virtual {p1}, LkN;->f()I

    move-result v1

    iput v1, p0, Llv;->f:I

    .line 1225
    const/4 v1, 0x0

    iput-object v1, p0, Llv;->a:Landroid/view/Display;

    .line 1226
    or-int/lit8 v0, v0, 0x5

    .line 1228
    :cond_a
    iget-object v1, p0, Llv;->a:Landroid/os/Bundle;

    invoke-virtual {p1}, LkN;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, Llj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1229
    invoke-virtual {p1}, LkN;->a()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Llv;->a:Landroid/os/Bundle;

    .line 1230
    or-int/lit8 v0, v0, 0x1

    .line 1234
    :cond_b
    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Llv;->c:Ljava/lang/String;

    return-object v0
.end method

.method a()LkR;
    .locals 1

    .prologue
    .line 1242
    iget-object v0, p0, Llv;->a:Llu;

    invoke-virtual {v0}, Llu;->a()LkR;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 1155
    invoke-static {}, Llj;->a()V

    .line 1156
    sget-object v0, Llj;->a:Llo;

    invoke-virtual {v0, p0}, Llo;->a(Llv;)V

    .line 1157
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 1084
    invoke-static {}, Llj;->a()V

    .line 1085
    sget-object v0, Llj;->a:Llo;

    iget v1, p0, Llv;->e:I

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, p0, v1}, Llo;->a(Llv;I)V

    .line 1086
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 825
    iget-boolean v0, p0, Llv;->a:Z

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 913
    if-nez p1, :cond_0

    .line 914
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 916
    :cond_0
    invoke-static {}, Llj;->a()V

    .line 918
    iget-object v0, p0, Llv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 919
    :goto_0
    if-ge v2, v3, :cond_2

    .line 920
    iget-object v0, p0, Llv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 921
    const/4 v0, 0x1

    .line 924
    :goto_1
    return v0

    .line 919
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 924
    goto :goto_1
.end method

.method public a(Llg;)Z
    .locals 2

    .prologue
    .line 887
    if-nez p1, :cond_0

    .line 888
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 890
    :cond_0
    invoke-static {}, Llj;->a()V

    .line 891
    iget-object v0, p0, Llv;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Llg;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1040
    iget v0, p0, Llv;->b:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Llv;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1098
    invoke-static {}, Llj;->a()V

    .line 1099
    if-eqz p1, :cond_0

    .line 1100
    sget-object v0, Llj;->a:Llo;

    invoke-virtual {v0, p0, p1}, Llo;->b(Llv;I)V

    .line 1102
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 835
    iget-boolean v0, p0, Llv;->b:Z

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1051
    iget v0, p0, Llv;->c:I

    return v0
.end method

.method c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1238
    iget-object v0, p0, Llv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 846
    invoke-static {}, Llj;->a()V

    .line 847
    sget-object v0, Llj;->a:Llo;

    invoke-virtual {v0}, Llo;->b()Llv;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1061
    iget v0, p0, Llv;->d:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 858
    invoke-static {}, Llj;->a()V

    .line 859
    sget-object v0, Llj;->a:Llo;

    invoke-virtual {v0}, Llo;->a()Llv;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1071
    iget v0, p0, Llv;->e:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MediaRouter.RouteInfo{ uniqueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llv;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llv;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llv;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Llv;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connecting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Llv;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llv;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llv;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeHandling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llv;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llv;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llv;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentationDisplayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llv;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llv;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", providerPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llv;->a:Llu;

    invoke-virtual {v1}, Llu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
