.class public abstract LajU;
.super Ljava/lang/Object;
.source "Accounts.java"


# instance fields
.field protected final a:J

.field private final a:LabI;

.field protected a:Landroid/view/View;

.field protected a:Landroid/widget/ImageView;

.field protected a:Landroid/widget/TextView;

.field protected final a:Ljava/lang/String;

.field protected b:Landroid/view/View;

.field protected b:Landroid/widget/TextView;

.field protected final b:Ljava/lang/String;

.field protected final c:Landroid/view/View;


# direct methods
.method public constructor <init>(LabD;LabI;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-interface {p1}, LabD;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LajU;->a:Ljava/lang/String;

    .line 66
    invoke-interface {p1}, LabD;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LajU;->b:Ljava/lang/String;

    .line 67
    invoke-interface {p1}, LabD;->b()J

    move-result-wide v0

    iput-wide v0, p0, LajU;->a:J

    .line 68
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabI;

    iput-object v0, p0, LajU;->a:LabI;

    .line 69
    invoke-direct {p0, p3}, LajU;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LajU;->c:Landroid/view/View;

    .line 70
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/view/View;
    .locals 4

    .prologue
    .line 73
    sget v0, Lxc;->account_display_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LajU;->a:Landroid/widget/TextView;

    .line 74
    sget v0, Lxc;->account_email:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LajU;->b:Landroid/widget/TextView;

    .line 75
    sget v0, Lxc;->account_top_padding:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LajU;->b:Landroid/view/View;

    .line 76
    sget v0, Lxc;->account_middle_padding:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LajU;->a:Landroid/view/View;

    .line 77
    sget v0, Lxc;->account_badge:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LajU;->a:Landroid/widget/ImageView;

    .line 79
    iget-object v0, p0, LajU;->a:LabI;

    iget-object v1, p0, LajU;->a:Landroid/widget/ImageView;

    iget-wide v2, p0, LajU;->a:J

    invoke-virtual {v0, v1, v2, v3}, LabI;->a(Landroid/widget/ImageView;J)V

    .line 81
    return-object p1
.end method


# virtual methods
.method public abstract a(Z)V
.end method
