.class public LbeY;
.super LbeO;
.source "GsonFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LbeO;-><init>()V

    .line 59
    return-void
.end method


# virtual methods
.method public a(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)LbeP;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1, p2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-virtual {p0, v0}, LbeY;->a(Ljava/io/Writer;)LbeP;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/Writer;)LbeP;
    .locals 2

    .prologue
    .line 96
    new-instance v0, LbeZ;

    new-instance v1, Lbum;

    invoke-direct {v1, p1}, Lbum;-><init>(Ljava/io/Writer;)V

    invoke-direct {v0, p0, v1}, LbeZ;-><init>(LbeY;Lbum;)V

    return-object v0
.end method

.method public a(Ljava/io/InputStream;)LbeS;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/io/InputStreamReader;

    sget-object v1, Lbfp;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-virtual {p0, v0}, LbeY;->a(Ljava/io/Reader;)LbeS;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;Ljava/nio/charset/Charset;)LbeS;
    .locals 1

    .prologue
    .line 73
    if-nez p2, :cond_0

    .line 74
    invoke-virtual {p0, p1}, LbeY;->a(Ljava/io/InputStream;)LbeS;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-virtual {p0, v0}, LbeY;->a(Ljava/io/Reader;)LbeS;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/io/Reader;)LbeS;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lbfa;

    new-instance v1, Lbuj;

    invoke-direct {v1, p1}, Lbuj;-><init>(Ljava/io/Reader;)V

    invoke-direct {v0, p0, v1}, Lbfa;-><init>(LbeY;Lbuj;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)LbeS;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LbeY;->a(Ljava/io/Reader;)LbeS;

    move-result-object v0

    return-object v0
.end method
