.class final LboP;
.super LboM;
.source "MapMakerInternalMap.java"

# interfaces
.implements Lbox;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LboM",
        "<TK;TV;>;",
        "Lbox",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field b:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field e:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILbox;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1538
    invoke-direct {p0, p1, p2, p3, p4}, LboM;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILbox;)V

    .line 1543
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LboP;->a:J

    .line 1555
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboP;->b:Lbox;

    .line 1568
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboP;->c:Lbox;

    .line 1583
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboP;->d:Lbox;

    .line 1596
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboP;->e:Lbox;

    .line 1539
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1547
    iget-wide v0, p0, LboP;->a:J

    return-wide v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1552
    iput-wide p1, p0, LboP;->a:J

    .line 1553
    return-void
.end method

.method public a(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1565
    iput-object p1, p0, LboP;->b:Lbox;

    .line 1566
    return-void
.end method

.method public b()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1560
    iget-object v0, p0, LboP;->b:Lbox;

    return-object v0
.end method

.method public b(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1578
    iput-object p1, p0, LboP;->c:Lbox;

    .line 1579
    return-void
.end method

.method public c()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1573
    iget-object v0, p0, LboP;->c:Lbox;

    return-object v0
.end method

.method public c(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1593
    iput-object p1, p0, LboP;->d:Lbox;

    .line 1594
    return-void
.end method

.method public d()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1588
    iget-object v0, p0, LboP;->d:Lbox;

    return-object v0
.end method

.method public d(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1606
    iput-object p1, p0, LboP;->e:Lbox;

    .line 1607
    return-void
.end method

.method public e()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1601
    iget-object v0, p0, LboP;->e:Lbox;

    return-object v0
.end method
