.class public Lhm;
.super LcS;
.source "SlidingPaneLayout.java"


# instance fields
.field private final a:Landroid/graphics/Rect;

.field final synthetic a:Landroid/support/v4/widget/SlidingPaneLayout;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
    .locals 1

    .prologue
    .line 1538
    iput-object p1, p0, Lhm;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-direct {p0}, LcS;-><init>()V

    .line 1539
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhm;->a:Landroid/graphics/Rect;

    return-void
.end method

.method private a(LfB;LfB;)V
    .locals 1

    .prologue
    .line 1597
    iget-object v0, p0, Lhm;->a:Landroid/graphics/Rect;

    .line 1599
    invoke-virtual {p2, v0}, LfB;->a(Landroid/graphics/Rect;)V

    .line 1600
    invoke-virtual {p1, v0}, LfB;->b(Landroid/graphics/Rect;)V

    .line 1602
    invoke-virtual {p2, v0}, LfB;->c(Landroid/graphics/Rect;)V

    .line 1603
    invoke-virtual {p1, v0}, LfB;->d(Landroid/graphics/Rect;)V

    .line 1605
    invoke-virtual {p2}, LfB;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->c(Z)V

    .line 1606
    invoke-virtual {p2}, LfB;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LfB;->a(Ljava/lang/CharSequence;)V

    .line 1607
    invoke-virtual {p2}, LfB;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LfB;->b(Ljava/lang/CharSequence;)V

    .line 1608
    invoke-virtual {p2}, LfB;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LfB;->c(Ljava/lang/CharSequence;)V

    .line 1610
    invoke-virtual {p2}, LfB;->j()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->h(Z)V

    .line 1611
    invoke-virtual {p2}, LfB;->h()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->f(Z)V

    .line 1612
    invoke-virtual {p2}, LfB;->c()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->a(Z)V

    .line 1613
    invoke-virtual {p2}, LfB;->d()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->b(Z)V

    .line 1614
    invoke-virtual {p2}, LfB;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->d(Z)V

    .line 1615
    invoke-virtual {p2}, LfB;->g()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->e(Z)V

    .line 1616
    invoke-virtual {p2}, LfB;->i()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->g(Z)V

    .line 1618
    invoke-virtual {p2}, LfB;->a()I

    move-result v0

    invoke-virtual {p1, v0}, LfB;->a(I)V

    .line 1620
    invoke-virtual {p2}, LfB;->b()I

    move-result v0

    invoke-virtual {p1, v0}, LfB;->b(I)V

    .line 1621
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;LfB;)V
    .locals 4

    .prologue
    .line 1543
    invoke-static {p2}, LfB;->a(LfB;)LfB;

    move-result-object v0

    .line 1544
    invoke-super {p0, p1, v0}, LcS;->a(Landroid/view/View;LfB;)V

    .line 1545
    invoke-direct {p0, p2, v0}, Lhm;->a(LfB;LfB;)V

    .line 1546
    invoke-virtual {v0}, LfB;->a()V

    .line 1548
    const-class v0, Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LfB;->b(Ljava/lang/CharSequence;)V

    .line 1549
    invoke-virtual {p2, p1}, LfB;->a(Landroid/view/View;)V

    .line 1551
    invoke-static {p1}, Lec;->a(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    .line 1552
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1553
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, LfB;->c(Landroid/view/View;)V

    .line 1558
    :cond_0
    iget-object v0, p0, Lhm;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildCount()I

    move-result v1

    .line 1559
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 1560
    iget-object v2, p0, Lhm;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1561
    invoke-virtual {p0, v2}, Lhm;->a(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    .line 1563
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lec;->a(Landroid/view/View;I)V

    .line 1565
    invoke-virtual {p2, v2}, LfB;->b(Landroid/view/View;)V

    .line 1559
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1568
    :cond_2
    return-void
.end method

.method public a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1587
    iget-object v0, p0, Lhm;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SlidingPaneLayout;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1580
    invoke-virtual {p0, p2}, Lhm;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1581
    invoke-super {p0, p1, p2, p3}, LcS;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 1583
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1572
    invoke-super {p0, p1, p2}, LcS;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1574
    const-class v0, Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1575
    return-void
.end method
