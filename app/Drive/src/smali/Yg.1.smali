.class public LYg;
.super LdN;
.source "PunchStateSvgViewer.java"


# instance fields
.field final synthetic a:LYc;


# direct methods
.method public constructor <init>(LYc;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, LYg;->a:LYc;

    invoke-direct {p0}, LdN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, LYg;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 191
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LYg;->a:LYc;

    invoke-static {v1}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 192
    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v1

    iget-object v2, p0, LYg;->a:LYc;

    invoke-static {v2, v0}, LYc;->a(LYc;I)I

    move-result v2

    aget-object v1, v1, v2

    if-ne p1, v1, :cond_0

    .line 196
    :goto_1
    return v0

    .line 191
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_1
    const/4 v0, -0x2

    goto :goto_1
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v4, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 136
    iget-object v0, p0, LYg;->a:LYc;

    iget-object v0, v0, LYc;->a:Ljava/lang/String;

    const-string v1, "in instantiateItem viewPagerPosition=%s"

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 137
    iget-object v0, p0, LYg;->a:LYc;

    invoke-static {v0, p2}, LYc;->a(LYc;I)I

    move-result v8

    .line 140
    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v0

    aget-object v0, v0, v8

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, LYg;->a:LYc;

    iget-object v1, v0, LYc;->a:LXD;

    iget-object v0, p0, LYg;->a:LYc;

    iget-object v2, v0, LYc;->a:LTO;

    iget-object v0, p0, LYg;->a:LYc;

    iget-object v3, v0, LYc;->a:LYO;

    iget-object v0, p0, LYg;->a:LYc;

    iget-object v4, v0, LYc;->a:LXR;

    iget-object v0, p0, LYg;->a:LYc;

    iget-object v5, v0, LYc;->a:LqK;

    iget-object v0, p0, LYg;->a:LYc;

    iget-object v6, v0, LYc;->a:Laja;

    iget-object v7, p0, LYg;->a:LYc;

    move-object v0, p1

    .line 142
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Landroid/view/ViewGroup;LXD;LTO;LYO;LXR;LqK;Lbxw;LXQ;)Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v0

    .line 150
    iget-object v1, p0, LYg;->a:LYc;

    invoke-static {v1}, LYc;->a(LYc;)LYr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(LYr;)V

    .line 152
    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v1

    aput-object v0, v1, v8

    .line 153
    iget-object v1, p0, LYg;->a:LYc;

    iget-object v1, v1, LYc;->a:Ljava/lang/String;

    const-string v2, "has created=%s"

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v0, v3, v9

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 169
    :goto_0
    packed-switch v8, :pswitch_data_0

    .line 180
    :goto_1
    return-object v0

    .line 155
    :cond_0
    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v0

    aget-object v1, v0, v8

    .line 156
    iget-object v0, p0, LYg;->a:LYc;

    iget-object v0, v0, LYc;->a:Ljava/lang/String;

    const-string v2, "in instantiateItem reusing=%s"

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v1, v3, v9

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 159
    invoke-virtual {v1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 160
    if-eqz v0, :cond_1

    .line 161
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 164
    :cond_1
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, v1

    goto :goto_0

    .line 171
    :pswitch_0
    iget-object v1, p0, LYg;->a:LYc;

    iget-object v2, p0, LYg;->a:LYc;

    iget-object v2, v2, LYc;->a:LXD;

    invoke-virtual {v2}, LXD;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v9, v2}, LYc;->a(LYc;ILjava/lang/String;)V

    goto :goto_1

    .line 174
    :pswitch_1
    iget-object v1, p0, LYg;->a:LYc;

    iget-object v2, p0, LYg;->a:LYc;

    iget-object v2, v2, LYc;->a:LXD;

    invoke-virtual {v2}, LXD;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v10, v2}, LYc;->a(LYc;ILjava/lang/String;)V

    goto :goto_1

    .line 177
    :pswitch_2
    iget-object v1, p0, LYg;->a:LYc;

    const/4 v2, 0x2

    iget-object v3, p0, LYg;->a:LYc;

    iget-object v3, v3, LYc;->a:LXD;

    invoke-virtual {v3}, LXD;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LYc;->a(LYc;ILjava/lang/String;)V

    goto :goto_1

    .line 169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 5

    .prologue
    .line 125
    instance-of v0, p3, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, LbiT;->b(Z)V

    .line 127
    check-cast p3, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 128
    iget-object v0, p0, LYg;->a:LYc;

    iget-object v0, v0, LYc;->a:Ljava/lang/String;

    const-string v1, "in destroyItem viewPagerPosition=%s for: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 129
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    .line 128
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 130
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 131
    invoke-virtual {p3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c()V

    .line 132
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 120
    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
