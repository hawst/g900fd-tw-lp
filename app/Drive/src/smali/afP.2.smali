.class public LafP;
.super Ljava/lang/Object;
.source "CompoundSyncableFactory.java"

# interfaces
.implements Lahn;


# static fields
.field private static final a:LafR;


# instance fields
.field private final a:LaGM;

.field private final a:LafN;

.field private final a:Lagk;

.field private final a:Lagv;

.field private final a:Lahf;

.field a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LtB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, LafR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LafR;-><init>(LafQ;)V

    sput-object v0, LafP;->a:LafR;

    return-void
.end method

.method public constructor <init>(LaGM;Lagk;LafN;Lagv;Lahf;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, LafP;->a:LaGM;

    .line 65
    iput-object p2, p0, LafP;->a:Lagk;

    .line 66
    iput-object p3, p0, LafP;->a:LafN;

    .line 67
    iput-object p4, p0, LafP;->a:Lagv;

    .line 68
    iput-object p5, p0, LafP;->a:Lahf;

    .line 69
    return-void
.end method

.method private a(LahS;LaGo;)Lahn;
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, LafP;->a(LahS;LaGo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "CompoundSyncableFactory"

    const-string v1, "Deferring to document sync, using Deferred SyncType."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 88
    iget-object v0, p0, LafP;->a:Lagk;

    .line 111
    :goto_0
    return-object v0

    .line 90
    :cond_0
    iget-object v0, p0, LafP;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    iget-object v0, p0, LafP;->a:LafN;

    goto :goto_0

    .line 93
    :cond_1
    sget-object v0, LaGv;->i:LaGv;

    invoke-interface {p2}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    invoke-static {p2}, LUN;->a(LaGo;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    iget-object v0, p0, LafP;->a:LafN;

    goto :goto_0

    .line 98
    :cond_2
    iget-object v0, p0, LafP;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtB;

    invoke-interface {v0}, LtB;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 100
    iget-object v0, p0, LafP;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtB;

    invoke-interface {v0}, LtB;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p0, LafP;->a:LafN;

    goto :goto_0

    .line 103
    :cond_3
    sget-object v0, LafP;->a:LafR;

    goto :goto_0

    .line 108
    :cond_4
    iget-object v0, p0, LafP;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtB;

    invoke-interface {p2}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-interface {v0, v1}, LtB;->a(LaGv;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 109
    iget-object v0, p0, LafP;->a:Lagv;

    goto :goto_0

    .line 111
    :cond_5
    iget-object v0, p0, LafP;->a:LafN;

    goto :goto_0
.end method

.method private a(LahS;LaGo;)Z
    .locals 1

    .prologue
    .line 131
    sget-object v0, LahS;->b:LahS;

    if-ne p1, v0, :cond_0

    .line 132
    iget-object v0, p0, LafP;->a:Lahf;

    invoke-interface {v0, p2}, Lahf;->b(LaGo;)Z

    move-result v0

    .line 134
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LafP;->a:Lahf;

    invoke-interface {v0, p2}, Lahf;->a(LaGo;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(LahJ;LagH;)Lahm;
    .locals 2

    .prologue
    .line 73
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, LafP;->a:LaGM;

    invoke-interface {p1}, LahJ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 77
    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, LafP;->a:LafN;

    invoke-virtual {v0, p1, p2}, LafN;->a(LahJ;LagH;)Lahm;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 80
    :cond_0
    invoke-interface {p1}, LahJ;->a()LahS;

    move-result-object v1

    invoke-direct {p0, v1, v0}, LafP;->a(LahS;LaGo;)Lahn;

    move-result-object v0

    .line 81
    invoke-interface {v0, p1, p2}, Lahn;->a(LahJ;LagH;)Lahm;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LahJ;)Z
    .locals 2

    .prologue
    .line 119
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, LafP;->a:LaGM;

    invoke-interface {p1}, LahJ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 122
    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, LafP;->a:LafN;

    invoke-virtual {v0, p1}, LafN;->a(LahJ;)Z

    move-result v0

    .line 126
    :goto_0
    return v0

    .line 125
    :cond_0
    invoke-interface {p1}, LahJ;->a()LahS;

    move-result-object v1

    invoke-direct {p0, v1, v0}, LafP;->a(LahS;LaGo;)Lahn;

    move-result-object v0

    .line 126
    invoke-interface {v0, p1}, Lahn;->a(LahJ;)Z

    move-result v0

    goto :goto_0
.end method
