.class public final LaeA;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laes;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lael;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laev;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "Laeu;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "Laeu;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 41
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 42
    iput-object p1, p0, LaeA;->a:LbrA;

    .line 43
    const-class v0, Laes;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaeA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeA;->a:Lbsk;

    .line 46
    const-class v0, Lael;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaeA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeA;->b:Lbsk;

    .line 49
    const-class v0, Laev;

    invoke-static {v0, v3}, LaeA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeA;->c:Lbsk;

    .line 52
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Laeu;

    aput-object v2, v1, v4

    .line 53
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 52
    invoke-static {v0, v3}, LaeA;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeA;->d:Lbsk;

    .line 55
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Laeu;

    aput-object v2, v1, v4

    .line 56
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 55
    invoke-static {v0, v3}, LaeA;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeA;->e:Lbsk;

    .line 58
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 116
    packed-switch p1, :pswitch_data_0

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :pswitch_0
    new-instance v3, Laes;

    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->d:Lbsk;

    .line 121
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laek;

    iget-object v1, v1, Laek;->d:Lbsk;

    .line 119
    invoke-static {v0, v1}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbwT;

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaeA;

    iget-object v1, v1, LaeA;->c:Lbsk;

    .line 127
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaeA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaeA;

    iget-object v2, v2, LaeA;->c:Lbsk;

    .line 125
    invoke-static {v1, v2}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laev;

    iget-object v2, p0, LaeA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->k:Lbsk;

    .line 133
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LaeA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LTk;

    iget-object v4, v4, LTk;->k:Lbsk;

    .line 131
    invoke-static {v2, v4}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LTd;

    invoke-direct {v3, v0, v1, v2}, Laes;-><init>(LbwT;Laev;LTd;)V

    move-object v0, v3

    .line 166
    :goto_0
    return-object v0

    .line 140
    :pswitch_1
    new-instance v3, Lael;

    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->c:Lbsk;

    .line 143
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laek;

    iget-object v1, v1, Laek;->c:Lbsk;

    .line 141
    invoke-static {v0, v1}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbwT;

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaeA;

    iget-object v1, v1, LaeA;->c:Lbsk;

    .line 149
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaeA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaeA;

    iget-object v2, v2, LaeA;->c:Lbsk;

    .line 147
    invoke-static {v1, v2}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laev;

    iget-object v2, p0, LaeA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->k:Lbsk;

    .line 155
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LaeA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LTk;

    iget-object v4, v4, LTk;->k:Lbsk;

    .line 153
    invoke-static {v2, v4}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LTd;

    invoke-direct {v3, v0, v1, v2}, Lael;-><init>(LbwT;Laev;LTd;)V

    move-object v0, v3

    .line 160
    goto :goto_0

    .line 162
    :pswitch_2
    new-instance v0, Laev;

    invoke-direct {v0}, Laev;-><init>()V

    .line 164
    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaeA;

    .line 165
    invoke-virtual {v1, v0}, LaeA;->a(Laev;)V

    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1af
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 189
    packed-switch p2, :pswitch_data_0

    .line 217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :pswitch_0
    check-cast p1, Laeq;

    .line 193
    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaeA;

    iget-object v0, v0, LaeA;->e:Lbsk;

    .line 196
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 193
    invoke-virtual {p1, v0}, Laeq;->getLazy1(Laja;)Lajw;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    .line 200
    :pswitch_1
    check-cast p1, Laeq;

    .line 202
    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v2, v0, Laek;->e:Lbsk;

    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 209
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 213
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 202
    invoke-virtual {p1, v2, v0, v1}, Laeq;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x1ad
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    const-class v0, Laev;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x2b

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LaeA;->a(LbuP;LbuB;)V

    .line 93
    const-class v0, Laes;

    iget-object v1, p0, LaeA;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeA;->a(Ljava/lang/Class;Lbsk;)V

    .line 94
    const-class v0, Lael;

    iget-object v1, p0, LaeA;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeA;->a(Ljava/lang/Class;Lbsk;)V

    .line 95
    const-class v0, Laev;

    iget-object v1, p0, LaeA;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeA;->a(Ljava/lang/Class;Lbsk;)V

    .line 96
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Laeu;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaeA;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeA;->a(Lbuv;Lbsk;)V

    .line 97
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Laeu;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaeA;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeA;->a(Lbuv;Lbsk;)V

    .line 98
    iget-object v0, p0, LaeA;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1af

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 100
    iget-object v0, p0, LaeA;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1b1

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 102
    iget-object v0, p0, LaeA;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1b0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 104
    iget-object v0, p0, LaeA;->d:Lbsk;

    const-class v1, Laeq;

    const/16 v2, 0x1ad

    invoke-virtual {p0, v1, v2}, LaeA;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 106
    iget-object v0, p0, LaeA;->e:Lbsk;

    const-class v1, Laeq;

    const/16 v2, 0x1ae

    invoke-virtual {p0, v1, v2}, LaeA;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 108
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 175
    packed-switch p1, :pswitch_data_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :pswitch_0
    check-cast p2, Laev;

    .line 179
    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaeA;

    .line 180
    invoke-virtual {v0, p2}, LaeA;->a(Laev;)V

    .line 185
    return-void

    .line 175
    :pswitch_data_0
    .packed-switch 0x2b
        :pswitch_0
    .end packed-switch
.end method

.method public a(Laev;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 67
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 65
    invoke-static {v0, v1}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Laev;->a:LQr;

    .line 71
    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->i:Lbsk;

    .line 74
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->i:Lbsk;

    .line 72
    invoke-static {v0, v1}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    iput-object v0, p1, Laev;->a:LTO;

    .line 78
    iget-object v0, p0, LaeA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 81
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaeA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 79
    invoke-static {v0, v1}, LaeA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Laev;->a:Laja;

    .line 85
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method
