.class public LafA;
.super Ljava/lang/Object;
.source "SyncMoreFactoryImpl.java"

# interfaces
.implements Lafz;


# instance fields
.field private final a:LSF;

.field private final a:LaGF;

.field private final a:LaGM;

.field private final a:LafB;

.field private final a:LagE;

.field private final a:LagS;

.field private final a:Lagl;


# direct methods
.method public constructor <init>(LafB;LagS;Lagl;LagE;LaGF;LSF;LaGM;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, LafA;->a:LafB;

    .line 47
    iput-object p2, p0, LafA;->a:LagS;

    .line 48
    iput-object p3, p0, LafA;->a:Lagl;

    .line 49
    iput-object p4, p0, LafA;->a:LagE;

    .line 50
    iput-object p5, p0, LafA;->a:LaGF;

    .line 51
    iput-object p6, p0, LafA;->a:LSF;

    .line 52
    iput-object p7, p0, LafA;->a:LaGM;

    .line 53
    return-void
.end method


# virtual methods
.method public a(LaFM;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Laff;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-interface {p2}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-object v0

    .line 66
    :cond_1
    iget-object v1, p0, LafA;->a:LaGM;

    .line 67
    invoke-interface {p2, v1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LaGM;)LaeZ;

    move-result-object v1

    .line 68
    iget-object v2, p0, LafA;->a:LafB;

    invoke-virtual {v2, p1, v1}, LafB;->a(LaFM;LaeZ;)Lafe;

    move-result-object v1

    .line 70
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 71
    iget-object v3, v1, Lafe;->a:Ljava/lang/Object;

    if-eqz v3, :cond_2

    .line 72
    iget-object v3, v1, Lafe;->a:Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_2
    iget-object v3, v1, Lafe;->b:Ljava/lang/Object;

    if-eqz v3, :cond_3

    .line 75
    iget-object v1, v1, Lafe;->b:Ljava/lang/Object;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    new-instance v5, Landroid/content/SyncResult;

    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    .line 89
    iget-object v0, p0, LafA;->a:LagE;

    iget-object v1, p0, LafA;->a:Lagl;

    const/4 v3, 0x0

    .line 90
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 89
    invoke-interface {v0, v1, v5, p1, v3}, LagE;->a(Lagl;Landroid/content/SyncResult;LaFM;Ljava/lang/Boolean;)Lagy;

    move-result-object v3

    .line 91
    new-instance v0, Lafh;

    iget-object v4, p0, LafA;->a:LagS;

    iget-object v6, p0, LafA;->a:LSF;

    iget-object v7, p0, LafA;->a:LaGF;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lafh;-><init>(LaFM;Ljava/util/List;Lagy;LagS;Landroid/content/SyncResult;LSF;LaGF;)V

    goto :goto_0
.end method
