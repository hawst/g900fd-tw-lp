.class public abstract LaDi;
.super LaDh;

# interfaces
.implements LaDs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "LaDc;",
        "A::",
        "LaCR;",
        ">",
        "LaDh",
        "<TR;>;",
        "LaDs",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private final a:LaCT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaCT",
            "<TA;>;"
        }
    .end annotation
.end field

.field private a:LaDq;


# direct methods
.method protected constructor <init>(LaCT;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaCT",
            "<TA;>;)V"
        }
    .end annotation

    invoke-direct {p0}, LaDh;-><init>()V

    invoke-static {p1}, LaSc;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCT;

    iput-object v0, p0, LaDi;->a:LaCT;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, LaDi;->a(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a()LaCT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LaCT",
            "<TA;>;"
        }
    .end annotation

    iget-object v0, p0, LaDi;->a:LaCT;

    return-object v0
.end method

.method protected abstract a(LaCR;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation
.end method

.method public a(LaDq;)V
    .locals 0

    iput-object p1, p0, LaDi;->a:LaDq;

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, LaSc;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, LaDi;->a(Lcom/google/android/gms/common/api/Status;)LaDc;

    move-result-object v0

    invoke-virtual {p0, v0}, LaDi;->a(LaDc;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, LaDh;->b()V

    iget-object v0, p0, LaDi;->a:LaDq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaDi;->a:LaDq;

    invoke-interface {v0, p0}, LaDq;->a(LaDs;)V

    const/4 v0, 0x0

    iput-object v0, p0, LaDi;->a:LaDq;

    :cond_0
    return-void
.end method

.method public final b(LaCR;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    new-instance v0, LaDj;

    invoke-interface {p1}, LaCR;->a()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, LaDj;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, LaDi;->a(LaDj;)V

    :try_start_0
    invoke-virtual {p0, p1}, LaDi;->a(LaCR;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, LaDi;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, LaDi;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method
