.class public LvW;
.super Ljava/lang/Object;
.source "EntryInformationImpl.java"

# interfaces
.implements LaEC;
.implements LvU;
.implements Lwn;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LaEP;

.field private final a:LaGR;

.field private a:LaGu;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LvV;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LvL;

.field private a:LvX;

.field private final a:Lwm;


# direct methods
.method public constructor <init>(LaGR;LvL;Lwm;LaEP;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LvW;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 100
    iput-object p1, p0, LvW;->a:LaGR;

    .line 101
    iput-object p2, p0, LvW;->a:LvL;

    .line 102
    iput-object p3, p0, LvW;->a:Lwm;

    .line 103
    iput-object p4, p0, LvW;->a:LaEP;

    .line 104
    invoke-interface {p3, p0}, Lwm;->a(Lwn;)V

    .line 105
    return-void
.end method

.method static synthetic a(LvW;)LvL;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LvW;->a:LvL;

    return-object v0
.end method

.method private b(LaGu;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 226
    iget-object v1, p0, LvW;->a:LaGu;

    if-eqz v1, :cond_1

    iget-object v1, p0, LvW;->a:LaGu;

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 227
    :goto_0
    if-eqz p1, :cond_0

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 228
    :cond_0
    iput-object p1, p0, LvW;->a:LaGu;

    .line 232
    invoke-static {v1, v0}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    invoke-virtual {p0}, LvW;->a()V

    .line 237
    :goto_1
    return-void

    :cond_1
    move-object v1, v0

    .line 226
    goto :goto_0

    .line 235
    :cond_2
    invoke-direct {p0}, LvW;->e()V

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, LvW;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvV;

    .line 174
    invoke-interface {v0}, LvV;->o()V

    goto :goto_0

    .line 176
    :cond_0
    return-void
.end method


# virtual methods
.method public a()LaGu;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, LvW;->a:LaGu;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 180
    invoke-static {}, LamV;->a()V

    .line 181
    iget-object v0, p0, LvW;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvV;

    .line 182
    invoke-interface {v0}, LvV;->r()V

    goto :goto_0

    .line 184
    :cond_0
    return-void
.end method

.method public a(LaGu;)V
    .locals 2

    .prologue
    .line 217
    invoke-static {}, LamV;->a()V

    .line 218
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, LvW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_0

    iget-object v0, p0, LvW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-direct {p0, p1}, LvW;->b(LaGu;)V

    .line 223
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 157
    if-nez p1, :cond_0

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    const-string v0, "entryInformation_entrySpec"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 162
    invoke-virtual {p0, v0}, LvW;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    invoke-static {}, LamV;->a()V

    .line 127
    iget-object v0, p0, LvW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 129
    invoke-static {v0, p1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v1, p0, LvW;->a:LvX;

    if-eqz v1, :cond_1

    .line 134
    iget-object v1, p0, LvW;->a:LvX;

    invoke-virtual {v1}, LvX;->b()V

    .line 135
    iput-object v2, p0, LvW;->a:LvX;

    .line 138
    :cond_1
    iput-object p1, p0, LvW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 140
    if-eqz v0, :cond_2

    .line 141
    iget-object v1, p0, LvW;->a:LaEP;

    invoke-virtual {v1, v0, p0}, LaEP;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)Z

    .line 144
    :cond_2
    if-eqz p1, :cond_3

    .line 149
    iget-object v0, p0, LvW;->a:LaEP;

    invoke-virtual {v0, p1, p0}, LaEP;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)V

    goto :goto_0

    .line 151
    :cond_3
    invoke-direct {p0, v2}, LvW;->b(LaGu;)V

    goto :goto_0
.end method

.method public a(LvV;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, LvW;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 188
    invoke-static {}, LamV;->a()V

    .line 190
    iget-object v0, p0, LvW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, LvW;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 197
    new-instance v1, LvX;

    iget-object v2, p0, LvW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {v1, p0, v0, v2}, LvX;-><init>(LvW;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    iput-object v1, p0, LvW;->a:LvX;

    .line 198
    iget-object v0, p0, LvW;->a:LaGR;

    iget-object v1, p0, LvW;->a:LvX;

    invoke-virtual {v0, v1}, LaGR;->b(LaGN;)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 167
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    invoke-virtual {p0}, LvW;->a()LaGu;

    move-result-object v0

    .line 169
    const-string v1, "entryInformation_entrySpec"

    if-eqz v0, :cond_0

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 170
    return-void

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LvV;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, LvW;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 115
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 203
    invoke-static {}, LamV;->a()V

    .line 205
    iget-object v0, p0, LvW;->a:LvX;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, LvW;->a:LvX;

    invoke-virtual {v0}, LvX;->b()V

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, LvW;->a:LvX;

    .line 209
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method
