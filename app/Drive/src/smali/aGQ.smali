.class final LaGQ;
.super Ljava/lang/Object;
.source "ModelLoaderAsyncTaskActivityHandler.java"


# instance fields
.field private a:Landroid/app/Dialog;

.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaGN",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaGQ;->a:Ljava/util/Set;

    .line 58
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LaGQ;->a:Ljava/lang/ref/WeakReference;

    .line 59
    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "Will not show progress indicator as activity is finishing"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :goto_0
    return-void

    .line 106
    :cond_0
    instance-of v0, p1, Lrm;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lrm;

    .line 107
    invoke-virtual {v0}, Lrm;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "Will not show progress indicator as activity is not transaction safe"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 112
    :cond_1
    :try_start_0
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "Show progress indicator"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-static {p1}, LaGI;->a(Landroid/content/Context;)LaGI;

    move-result-object v0

    iput-object v0, p0, LaGQ;->a:Landroid/app/Dialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    const-string v1, "ModelLoaderAsyncTaskActivityHandler"

    const-string v2, "Failed to show progress indicator"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public a(LaGN;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGN",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 62
    iget-object v0, p0, LaGQ;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 63
    invoke-static {v0}, LbiT;->b(Z)V

    .line 65
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "Stop progress indicator for task: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 66
    iget-object v0, p0, LaGQ;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, LaGQ;->a:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaGQ;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "Dismiss progress indicator"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 70
    :try_start_0
    iget-object v0, p0, LaGQ;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LaGQ;->a:Landroid/app/Dialog;

    .line 82
    :cond_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 75
    const-string v1, "ModelLoaderAsyncTaskActivityHandler"

    const-string v2, "dialog.dismiss() dismiss caused: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 78
    :cond_1
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "Progress indicator is already gone"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public b(LaGN;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGN",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, LaGQ;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, LaGQ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 87
    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-direct {p0, v0}, LaGQ;->a(Landroid/app/Activity;)V

    .line 94
    :goto_1
    iget-object v0, p0, LaGQ;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :cond_1
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "Progress indicator already shown"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
