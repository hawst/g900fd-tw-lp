.class public LWG;
.super Ljava/lang/Object;
.source "LegacyPrintActivity.java"

# interfaces
.implements LGH;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()LGI;
    .locals 5

    .prologue
    .line 438
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)LGI;

    move-result-object v0

    if-nez v0, :cond_0

    .line 439
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LUT;

    iget-object v1, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    iget-object v2, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    .line 440
    invoke-static {v2}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LUT;->a(Landroid/content/Context;Landroid/net/Uri;)LVa;

    move-result-object v0

    .line 441
    iget-object v1, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    new-instance v2, LGG;

    iget-object v3, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v3}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, LGG;-><init>(Ljava/lang/String;Ljava/lang/String;LVa;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;LGI;)LGI;

    .line 443
    :cond_0
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)LGI;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->finish()V

    .line 434
    return-void
.end method

.method public a(LDL;)V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)LpD;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)LpD;

    move-result-object v0

    invoke-virtual {v0}, LpD;->a()V

    .line 464
    :cond_0
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    new-instance v1, LWH;

    invoke-direct {v1, p0, p1}, LWH;-><init>(LWG;LDL;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;LpD;)LpD;

    .line 476
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)LpD;

    move-result-object v0

    invoke-virtual {v0}, LpD;->start()V

    .line 477
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->startActivity(Landroid/content/Intent;)V

    .line 429
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 448
    const-string v0, "PrintDialogActivity"

    const-string v1, "UrlLoaderHelper.loadUrlInWebView Loading URL: %s in webview."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 449
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 450
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 454
    const-string v0, "PrintDialogActivity"

    const-string v1, "Error occurred: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 455
    iget-object v0, p0, LWG;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V

    .line 456
    return-void
.end method
