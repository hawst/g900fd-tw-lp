.class LZq;
.super Ljava/lang/Object;
.source "PunchWebViewFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic a:LZn;

.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(LZn;IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, LZq;->a:LZn;

    iput p2, p0, LZq;->a:I

    iput p3, p0, LZq;->b:I

    iput-object p4, p0, LZq;->a:Ljava/lang/String;

    iput-object p5, p0, LZq;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 340
    iget-object v2, p0, LZq;->a:LZn;

    iget-object v2, v2, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v2

    .line 341
    if-nez v2, :cond_1

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    iget v2, p0, LZq;->a:I

    invoke-static {v2}, LZl;->a(I)LZl;

    move-result-object v2

    .line 347
    const-string v3, "PunchWebViewFragment"

    const-string v4, "Processing onSlideLoadStateChange(slideIndex=%s, loadStateCode=%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, LZq;->b:I

    .line 348
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget v6, p0, LZq;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    .line 347
    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 350
    sget-object v3, LZl;->c:LZl;

    if-eq v2, v3, :cond_0

    sget-object v3, LZl;->a:LZl;

    if-eq v2, v3, :cond_0

    .line 359
    new-instance v3, LXF;

    iget-object v4, p0, LZq;->a:Ljava/lang/String;

    iget-object v5, p0, LZq;->b:Ljava/lang/String;

    sget-object v6, LZl;->d:LZl;

    if-ne v2, v6, :cond_2

    :goto_1
    invoke-direct {v3, v4, v5, v0}, LXF;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 361
    iget-object v0, p0, LZq;->a:LZn;

    iget-object v0, v0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    iget v2, p0, LZq;->b:I

    invoke-virtual {v0, v2, v3}, LXD;->a(ILXF;)V

    .line 363
    iget-object v0, p0, LZq;->a:LZn;

    iget-object v0, v0, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget-object v2, p0, LZq;->a:LZn;

    iget-object v2, v2, LZn;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v2}, LXD;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 359
    goto :goto_1

    .line 363
    :cond_3
    const/16 v1, 0x8

    goto :goto_2
.end method
