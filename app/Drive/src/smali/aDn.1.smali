.class LaDn;
.super Ljava/lang/Object;

# interfaces
.implements LaCX;


# instance fields
.field final synthetic a:LaDl;


# direct methods
.method constructor <init>(LaDl;)V
    .locals 0

    iput-object p1, p0, LaDn;->a:LaDl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0, p1}, LaDl;->a(LaDl;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch p1, :pswitch_data_0

    :goto_0
    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-void

    :pswitch_0
    :try_start_1
    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-virtual {v0}, LaDl;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDn;->a:LaDl;

    invoke-static {v1}, LaDl;->a(LaDl;)Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :pswitch_1
    :try_start_2
    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :cond_0
    :try_start_3
    iget-object v0, p0, LaDn;->a:LaDl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LaDl;->a(LaDl;I)I

    iget-object v0, p0, LaDn;->a:LaDl;

    iget-object v0, v0, LaDl;->a:Landroid/os/Handler;

    iget-object v1, p0, LaDn;->a:LaDl;

    iget-object v1, v1, LaDl;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, LaDn;->a:LaDl;

    invoke-static {v2}, LaDl;->a(LaDl;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, LaDn;->a:LaDl;

    invoke-static {v0}, LaDl;->a(LaDl;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDn;->a:LaDl;

    invoke-static {v1}, LaDl;->a(LaDl;)Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
