.class public final LEr;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LEq;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LEt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 39
    iput-object p1, p0, LEr;->a:LbrA;

    .line 40
    const-class v0, LEq;

    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LEr;->a:Lbsk;

    .line 43
    const-class v0, LEt;

    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LEr;->b:Lbsk;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 139
    sparse-switch p1, :sswitch_data_0

    .line 161
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :sswitch_0
    new-instance v0, LEq;

    invoke-direct {v0}, LEq;-><init>()V

    .line 159
    :goto_0
    return-object v0

    .line 145
    :sswitch_1
    new-instance v2, LEt;

    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 148
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 146
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTV;

    iget-object v1, v1, LTV;->a:Lbsk;

    .line 154
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LEr;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTV;

    iget-object v3, v3, LTV;->a:Lbsk;

    .line 152
    invoke-static {v1, v3}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTT;

    invoke-direct {v2, v0, v1}, LEt;-><init>(LtK;LTT;)V

    move-object v0, v2

    .line 159
    goto :goto_0

    .line 139
    nop

    :sswitch_data_0
    .sparse-switch
        0x1e -> :sswitch_0
        0x26 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 188
    .line 190
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 119
    const-class v0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LEr;->a(LbuP;LbuB;)V

    .line 122
    const-class v0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LEr;->a(LbuP;LbuB;)V

    .line 125
    const-class v0, LEq;

    iget-object v1, p0, LEr;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LEr;->a(Ljava/lang/Class;Lbsk;)V

    .line 126
    const-class v0, LEt;

    iget-object v1, p0, LEr;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LEr;->a(Ljava/lang/Class;Lbsk;)V

    .line 127
    iget-object v0, p0, LEr;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 129
    iget-object v0, p0, LEr;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x26

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 131
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 168
    packed-switch p1, :pswitch_data_0

    .line 182
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

    .line 172
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LEr;

    .line 173
    invoke-virtual {v0, p2}, LEr;->a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;)V

    .line 184
    :goto_0
    return-void

    .line 176
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;

    .line 178
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LEr;

    .line 179
    invoke-virtual {v0, p2}, LEr;->a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;)V

    goto :goto_0

    .line 168
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 92
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 93
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->j:Lbsk;

    .line 96
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->j:Lbsk;

    .line 94
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a:LaFO;

    .line 100
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lbrx;

    iget-object v0, v0, Lbrx;->b:Lbsk;

    .line 103
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lbrx;

    iget-object v1, v1, Lbrx;->b:Lbsk;

    .line 101
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a:Ljava/util/List;

    .line 107
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTV;

    iget-object v0, v0, LTV;->a:Lbsk;

    .line 110
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTV;

    iget-object v1, v1, LTV;->a:Lbsk;

    .line 108
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTT;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a:LTT;

    .line 114
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->g:Lbsk;

    .line 55
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->g:Lbsk;

    .line 53
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwm;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Lwm;

    .line 59
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LVq;

    iget-object v0, v0, LVq;->h:Lbsk;

    .line 62
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVq;

    iget-object v1, v1, LVq;->h:Lbsk;

    .line 60
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVB;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LVB;

    .line 66
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->j:Lbsk;

    .line 69
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->j:Lbsk;

    .line 67
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LaFO;

    .line 73
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 76
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 74
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LaGR;

    .line 80
    iget-object v0, p0, LEr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->n:Lbsk;

    .line 83
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LEr;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->n:Lbsk;

    .line 81
    invoke-static {v0, v1}, LEr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Laja;

    .line 87
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 135
    return-void
.end method
