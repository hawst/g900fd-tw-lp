.class public LOJ;
.super Ljava/lang/Object;
.source "PhotosPreferencesInstaller.java"

# interfaces
.implements LWx;


# instance fields
.field a:LSF;

.field a:LVN;

.field private a:LaFO;

.field a:Landroid/app/Activity;

.field private a:Landroid/preference/PreferenceScreen;

.field a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LSu;",
            ">;"
        }
    .end annotation
.end field

.field a:LqK;

.field a:LtK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    return-void
.end method

.method static synthetic a(LOJ;)LaFO;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LOJ;->a:LaFO;

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, LOJ;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 243
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    return-object v0
.end method

.method static synthetic a(LOJ;)Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LOJ;->a:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method private a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V
    .locals 3

    .prologue
    .line 251
    invoke-virtual {p1, p2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    move-result v0

    .line 252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not present in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 253
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 150
    sget v0, LOW;->photos_preferences:I

    return v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method public a(LEE;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public a(Landroid/preference/PreferenceScreen;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 159
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, LOJ;->a:Landroid/preference/PreferenceScreen;

    .line 160
    iget-object v0, p0, LOJ;->a:LSF;

    invoke-interface {v0}, LSF;->a()LaFO;

    move-result-object v0

    iput-object v0, p0, LOJ;->a:LaFO;

    .line 162
    const-string v0, "google_photos"

    .line 163
    invoke-direct {p0, v0}, LOJ;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 165
    iget-object v1, p0, LOJ;->a:LbiP;

    invoke-virtual {v1}, LbiP;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LOJ;->a:LaFO;

    if-eqz v1, :cond_1

    iget-object v1, p0, LOJ;->a:LtK;

    sget-object v4, LOE;->c:Lrg;

    iget-object v5, p0, LOJ;->a:LaFO;

    .line 166
    invoke-interface {v1, v4, v5}, LtK;->a(Lrg;LaFO;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 167
    :goto_0
    const-string v4, "auto_backup"

    invoke-direct {p0, v4}, LOJ;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 168
    if-eqz v1, :cond_2

    .line 169
    new-instance v1, LOK;

    invoke-direct {v1, p0}, LOK;-><init>(LOJ;)V

    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 206
    :goto_1
    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 207
    const-string v1, "auto_add"

    invoke-direct {p0, v1}, LOJ;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 208
    iget-object v4, p0, LOJ;->a:LaFO;

    if-eqz v4, :cond_3

    iget-object v4, p0, LOJ;->a:LtK;

    sget-object v6, LOE;->a:Lrg;

    iget-object v7, p0, LOJ;->a:LaFO;

    .line 209
    invoke-interface {v4, v6, v7}, LtK;->a(Lrg;LaFO;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v2

    .line 210
    :goto_2
    if-eqz v4, :cond_4

    .line 211
    sget v4, LOV;->prefs_auto_add_summary_template:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v6, p0, LOJ;->a:LaFO;

    .line 212
    invoke-virtual {v6}, LaFO;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    .line 211
    invoke-virtual {v5, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 213
    new-instance v4, LOM;

    invoke-direct {v4, p0, v1, v2}, LOM;-><init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 232
    new-instance v4, LON;

    invoke-direct {v4, p0, v1, v2}, LON;-><init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v4, v1}, LON;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 236
    :goto_3
    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 237
    invoke-direct {p0, p1, v0}, LOJ;->a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    .line 239
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 166
    goto :goto_0

    .line 203
    :cond_2
    invoke-direct {p0, v0, v4}, LOJ;->a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    goto :goto_1

    :cond_3
    move v4, v3

    .line 209
    goto :goto_2

    .line 234
    :cond_4
    invoke-direct {p0, v0, v1}, LOJ;->a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;)V

    goto :goto_3
.end method

.method public b()V
    .locals 0

    .prologue
    .line 261
    return-void
.end method
