.class public LagL;
.super Ljava/lang/Thread;
.source "GdataResultsPageProducerImpl.java"

# interfaces
.implements LagX;


# instance fields
.field private final a:I

.field private final a:LZS;

.field private final a:LaFO;

.field private final a:Laem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laem",
            "<+",
            "Lbxj;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LagY;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "LaKa;",
            ">;"
        }
    .end annotation
.end field

.field private volatile a:Z

.field private final b:I


# direct methods
.method public constructor <init>(LagY;Ljava/lang/String;LaFO;Ljava/util/concurrent/BlockingQueue;LZS;Laem;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LagY;",
            "Ljava/lang/String;",
            "LaFO;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "LaKa;",
            ">;",
            "LZS;",
            "Laem",
            "<+",
            "Lbxj;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, LagL;->a:Z

    .line 70
    iput-object p1, p0, LagL;->a:LagY;

    .line 71
    iput-object p4, p0, LagL;->a:Ljava/util/concurrent/BlockingQueue;

    .line 72
    iput-object p6, p0, LagL;->a:Laem;

    .line 73
    iput-object p2, p0, LagL;->a:Ljava/lang/String;

    .line 74
    iput-object p3, p0, LagL;->a:LaFO;

    .line 75
    iput p7, p0, LagL;->a:I

    .line 76
    iput-object p5, p0, LagL;->a:LZS;

    .line 77
    iput p8, p0, LagL;->b:I

    .line 78
    const-class v0, LagL;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LagL;->setName(Ljava/lang/String;)V

    .line 79
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 130
    invoke-virtual {p0}, LagL;->start()V

    .line 131
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, LagL;->isAlive()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 135
    invoke-virtual {p0}, LagL;->join()V

    .line 136
    return-void
.end method

.method public interrupt()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, LagL;->a:Z

    .line 146
    invoke-super {p0}, Ljava/lang/Thread;->interrupt()V

    .line 147
    return-void
.end method

.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 83
    iget-object v4, p0, LagL;->a:Ljava/lang/String;

    move v7, v13

    .line 87
    :goto_0
    if-eqz v4, :cond_0

    :try_start_0
    invoke-virtual {p0}, LagL;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LagL;->a:I

    if-ge v7, v0, :cond_0

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 89
    iget-object v2, p0, LagL;->a:LZS;

    invoke-interface {v2}, LZS;->b()V

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 92
    const-string v5, "ResultsPageProducer"

    const-string v6, "Time to wait for token %s %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    const/4 v9, 0x1

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v8, v9

    invoke-static {v5, v6, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 93
    iget-object v0, p0, LagL;->a:Laem;

    iget-object v1, p0, LagL;->a:LaFO;

    const/4 v5, 0x0

    invoke-interface {v0, v4, v1, v5}, Laem;->a(Ljava/lang/String;LaFO;Ljava/lang/String;)Lbxj;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 95
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 96
    const-string v0, "ResultsPageProducer"

    const-string v5, "Time to get parser for feed %s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v4, v6, v10

    const/4 v10, 0x1

    sub-long v2, v8, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v10

    invoke-static {v0, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 98
    invoke-interface {v1}, Lbxj;->b()Lbxc;

    move-result-object v2

    .line 99
    iget-object v0, p0, LagL;->a:LagY;

    invoke-interface {v0, v2}, LagY;->a(Lbxc;)Ljava/lang/String;

    move-result-object v6

    .line 100
    iget-object v8, p0, LagL;->a:Ljava/util/concurrent/BlockingQueue;

    new-instance v0, LagK;

    const/4 v3, 0x0

    iget v5, p0, LagL;->b:I

    invoke-direct/range {v0 .. v6}, LagK;-><init>(Lbxj;Lbxc;Ljava/lang/Exception;Ljava/lang/String;ILjava/lang/String;)V

    const-wide v2, 0x7fffffffffffffffL

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v8, v0, v2, v3, v5}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 104
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move-object v4, v6

    .line 105
    goto :goto_0

    .line 107
    :cond_0
    :try_start_2
    iget-object v0, p0, LagL;->a:Ljava/util/concurrent/BlockingQueue;

    new-instance v5, LagK;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v10, p0, LagL;->b:I

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, LagK;-><init>(Lbxj;Lbxc;Ljava/lang/Exception;Ljava/lang/String;ILjava/lang/String;)V

    const-wide v2, 0x7fffffffffffffffL

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v5, v2, v3, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    .line 109
    const-string v0, "ResultsPageProducer"

    const-string v1, "%s feed finished, id: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, LagL;->a:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x1

    iget v5, p0, LagL;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 126
    :cond_1
    :goto_1
    return-void

    .line 110
    :catch_0
    move-exception v3

    move-object v1, v12

    .line 111
    :goto_2
    const-string v0, "ResultsPageProducer"

    const-string v2, "Error fetching %s"

    new-array v5, v14, [Ljava/lang/Object;

    aput-object v4, v5, v13

    invoke-static {v0, v2, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 112
    if-eqz v1, :cond_2

    .line 113
    invoke-interface {v1}, Lbxj;->a()V

    .line 117
    :cond_2
    iget-boolean v0, p0, LagL;->a:Z

    if-nez v0, :cond_1

    .line 119
    :try_start_3
    iget-object v7, p0, LagL;->a:Ljava/util/concurrent/BlockingQueue;

    new-instance v0, LagK;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget v5, p0, LagL;->b:I

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LagK;-><init>(Lbxj;Lbxc;Ljava/lang/Exception;Ljava/lang/String;ILjava/lang/String;)V

    const-wide v2, 0x7fffffffffffffffL

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, v0, v2, v3, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 121
    :catch_1
    move-exception v0

    .line 122
    const-string v0, "ResultsPageProducer"

    const-string v1, "Producer cannot be terminated gracefully %s"

    new-array v2, v14, [Ljava/lang/Object;

    iget-object v3, p0, LagL;->a:Ljava/lang/String;

    aput-object v3, v2, v13

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 110
    :catch_2
    move-exception v3

    goto :goto_2
.end method
