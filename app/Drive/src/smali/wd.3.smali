.class public final Lwd;
.super Ljava/lang/Object;
.source "KindAndMimeTypeFilterCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 115
    new-instance v4, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;

    .line 116
    invoke-static {v2}, LbmY;->a([Ljava/lang/Object;)LbmY;

    move-result-object v2

    if-ne v3, v1, :cond_0

    :goto_0
    invoke-direct {v4, v0, v2, v1}, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;-><init>(Ljava/util/Set;Ljava/util/Set;Z)V

    return-object v4

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;
    .locals 1

    .prologue
    .line 122
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lwd;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lwd;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;

    move-result-object v0

    return-object v0
.end method
