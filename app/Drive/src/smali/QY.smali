.class public LQY;
.super Lbw;
.source "DocListCursorBundleLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbw",
        "<",
        "LQX;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LAX;

.field private final a:LCo;

.field private a:LIK;

.field private final a:LIh;

.field private final a:LQV;

.field private final a:LaFO;

.field private final a:LaGM;

.field private final a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

.field private final a:LzO;


# direct methods
.method private constructor <init>(Landroid/content/Context;LQV;LaGM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LIK;LaFO;LzO;LCo;LIh;LAX;)V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lbw;-><init>(Landroid/content/Context;)V

    .line 111
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQV;

    iput-object v0, p0, LQY;->a:LQV;

    .line 112
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LQY;->a:LaGM;

    .line 113
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iput-object v0, p0, LQY;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 114
    iput-object p5, p0, LQY;->a:LIK;

    .line 115
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, LQY;->a:LaFO;

    .line 116
    iput-object p7, p0, LQY;->a:LzO;

    .line 117
    invoke-static {p8}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCo;

    iput-object v0, p0, LQY;->a:LCo;

    .line 118
    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIh;

    iput-object v0, p0, LQY;->a:LIh;

    .line 119
    invoke-static {p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAX;

    iput-object v0, p0, LQY;->a:LAX;

    .line 120
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LQV;LaGM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LIK;LaFO;LzO;LCo;LIh;LAX;LQZ;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct/range {p0 .. p10}, LQY;-><init>(Landroid/content/Context;LQV;LaGM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LIK;LaFO;LzO;LCo;LIh;LAX;)V

    return-void
.end method

.method public static a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LCo;)LCl;
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    .line 202
    if-eqz p0, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LCl;

    move-result-object v0

    .line 205
    :cond_0
    if-nez v0, :cond_1

    .line 206
    sget-object v0, LCn;->l:LCn;

    invoke-interface {p1, v0}, LCo;->a(LCn;)LCl;

    move-result-object v0

    .line 208
    :cond_1
    return-object v0
.end method

.method private a(Ljava/util/Set;)LzO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LzO;",
            ">;)",
            "LzO;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 124
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzO;

    .line 142
    :cond_0
    :goto_0
    return-object v0

    .line 128
    :cond_1
    iget-object v0, p0, LQY;->a:LzO;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, LQY;->a:LzO;

    goto :goto_0

    .line 132
    :cond_2
    iget-object v0, p0, LQY;->a:LQV;

    iget-object v1, p0, LQY;->a:LaFO;

    .line 133
    invoke-virtual {v0, v1}, LQV;->a(LaFO;)LzO;

    move-result-object v0

    .line 135
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    const-string v0, "DocListCursorBundleLoader"

    const-string v1, "Cannot determine the arrangement mode, arbitrary picking one"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzO;

    goto :goto_0
.end method


# virtual methods
.method public a()LQX;
    .locals 11

    .prologue
    .line 147
    const-string v0, "DocListCursorBundleLoader"

    const-string v1, "loadInBackground: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LQY;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 148
    iget-object v0, p0, LQY;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v2

    .line 150
    iget-object v0, p0, LQY;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v1, p0, LQY;->a:LCo;

    invoke-static {v0, v1}, LQY;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LCo;)LCl;

    move-result-object v0

    .line 152
    iget-object v1, p0, LQY;->a:LQV;

    iget-object v3, p0, LQY;->a:LaFO;

    .line 153
    invoke-virtual {v1, v3, v0}, LQV;->a(LaFO;LCl;)LbmY;

    move-result-object v10

    .line 155
    invoke-direct {p0, v10}, LQY;->a(Ljava/util/Set;)LzO;

    move-result-object v7

    .line 157
    iget-object v1, p0, LQY;->a:LIK;

    if-nez v1, :cond_0

    .line 158
    iget-object v1, p0, LQY;->a:LQV;

    iget-object v3, p0, LQY;->a:LaFO;

    iget-object v4, p0, LQY;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 159
    invoke-virtual {v4}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v4

    .line 158
    invoke-virtual {v1, v3, v4}, LQV;->a(LaFO;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LIK;

    move-result-object v1

    iput-object v1, p0, LQY;->a:LIK;

    .line 161
    :cond_0
    invoke-interface {v0}, LCl;->a()Ljava/util/EnumSet;

    move-result-object v0

    .line 165
    iget-object v1, p0, LQY;->a:LIK;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    iput-object v0, p0, LQY;->a:LIK;

    .line 169
    :cond_1
    iget-object v0, p0, LQY;->a:LaGM;

    iget-object v1, p0, LQY;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v4

    .line 170
    iget-object v0, p0, LQY;->a:LIh;

    iget-object v1, p0, LQY;->a:LIK;

    invoke-interface {v0, v1}, LIh;->a(LIK;)LIf;

    move-result-object v3

    .line 172
    invoke-static {v3}, LAC;->a(LIf;)LbmY;

    move-result-object v0

    .line 174
    new-instance v8, Lcom/google/android/apps/docs/doclist/DocListQuery;

    iget-object v1, p0, LQY;->a:LIK;

    invoke-direct {v8, v2, v1, v0}, Lcom/google/android/apps/docs/doclist/DocListQuery;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LbmY;)V

    .line 176
    :try_start_0
    iget-object v0, p0, LQY;->a:LAX;

    invoke-interface {v0, v8}, LAX;->a(Lcom/google/android/apps/docs/doclist/DocListQuery;)LaFX;

    move-result-object v1

    .line 178
    iget-object v0, p0, LQY;->a:LaGM;

    .line 179
    invoke-interface {v2, v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 181
    new-instance v0, LQX;

    iget-object v5, p0, LQY;->a:LIK;

    iget-object v6, p0, LQY;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v9, p0, LQY;->a:LCo;

    invoke-direct/range {v0 .. v10}, LQX;-><init>(LaFX;Lcom/google/android/gms/drive/database/data/EntrySpec;LIf;LaFM;LIK;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LzO;Lcom/google/android/apps/docs/doclist/DocListQuery;LCo;LbmY;)V
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    return-object v0

    .line 192
    :catch_0
    move-exception v2

    .line 193
    new-instance v1, LQX;

    iget-object v5, p0, LQY;->a:LIK;

    iget-object v6, p0, LQY;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v9, p0, LQY;->a:LCo;

    invoke-direct/range {v1 .. v10}, LQX;-><init>(LaGT;LIf;LaFM;LIK;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LzO;Lcom/google/android/apps/docs/doclist/DocListQuery;LCo;LbmY;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, LQY;->a()LQX;

    move-result-object v0

    return-object v0
.end method

.method public a(LQX;)V
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p1}, LQX;->a()LaFX;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {v0}, LaFX;->a()V

    .line 222
    :cond_0
    invoke-virtual {p1}, LQX;->b()V

    .line 223
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, LQX;

    invoke-virtual {p0, p1}, LQY;->a(LQX;)V

    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 213
    invoke-virtual {p0}, LQY;->e()V

    .line 214
    return-void
.end method
