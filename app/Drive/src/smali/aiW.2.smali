.class public LaiW;
.super Ljava/lang/Object;
.source "ContextScopeObject.java"


# instance fields
.field private final a:LajA;

.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 15
    new-instance v0, LajA;

    invoke-direct {v0}, LajA;-><init>()V

    invoke-direct {p0, p1, v0}, LaiW;-><init>(Landroid/content/Context;LajA;)V

    .line 16
    return-void
.end method

.method constructor <init>(Landroid/content/Context;LajA;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LaiW;->a:Landroid/content/Context;

    .line 20
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajA;

    iput-object v0, p0, LaiW;->a:LajA;

    .line 21
    return-void
.end method


# virtual methods
.method a()LajA;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LaiW;->a:LajA;

    return-object v0
.end method

.method a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, LaiW;->a:Landroid/content/Context;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 33
    const-string v0, "ContextScopedObject[context=%s, scopeObjectMap=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaiW;->a:Landroid/content/Context;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaiW;->a:LajA;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
