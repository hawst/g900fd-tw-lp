.class public Ladp;
.super Ljava/lang/Object;
.source "DocumentFileManagerImpl.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ladw;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ladw;

.field final synthetic a:Lamr;

.field final synthetic a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

.field final synthetic b:Ladw;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;Ladw;Lamr;)V
    .locals 0

    .prologue
    .line 1172
    iput-object p1, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iput-object p2, p0, Ladp;->a:Ladw;

    iput-object p3, p0, Ladp;->b:Ladw;

    iput-object p4, p0, Ladp;->a:Lamr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LadY;Ljava/io/File;Lamr;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1176
    iget-object v0, p0, Ladp;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()Ljavax/crypto/SecretKey;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1177
    iget-object v0, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lalo;->a(LadY;Ljava/io/File;)V

    .line 1182
    :goto_0
    return-void

    .line 1179
    :cond_0
    iget-object v0, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v0

    iget-object v1, p0, Ladp;->a:Ladw;

    .line 1180
    invoke-virtual {v1}, Ladw;->a()Ljavax/crypto/SecretKey;

    move-result-object v3

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p3

    .line 1179
    invoke-interface/range {v0 .. v5}, Lalo;->a(LadY;Ljava/io/File;Ljava/security/Key;Ljava/lang/String;Lamr;)V

    goto :goto_0
.end method

.method private b(LadY;Ljava/io/File;Lamr;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1187
    invoke-virtual {p1}, LadY;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1188
    invoke-virtual {p1}, LadY;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadY;

    .line 1189
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, LadY;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1190
    invoke-virtual {v0}, LadY;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1193
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1194
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to create directory "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1197
    :cond_0
    invoke-direct {p0, v0, v2, p3, p4}, Ladp;->b(LadY;Ljava/io/File;Lamr;Ljava/lang/String;)V

    goto :goto_0

    .line 1202
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Ladp;->a(LadY;Ljava/io/File;Lamr;Ljava/lang/String;)V

    .line 1204
    :cond_2
    return-void
.end method


# virtual methods
.method public a()Ladw;
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1209
    .line 1214
    :try_start_0
    iget-object v0, p0, Ladp;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()LadY;

    move-result-object v0

    .line 1215
    iget-object v1, p0, Ladp;->b:Ladw;

    invoke-virtual {v1}, Ladw;->a()Ljava/io/File;

    move-result-object v9

    .line 1218
    :cond_0
    iget-object v1, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v1

    invoke-interface {v1, v0}, Lalo;->a(LadY;)J

    move-result-wide v10

    .line 1220
    iget-object v1, p0, Ladp;->a:Ladw;

    invoke-virtual {v1}, Ladw;->a()J

    move-result-wide v2

    .line 1222
    iget-object v1, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 1223
    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Landroid/content/Context;

    move-result-object v1

    sget v4, Lxi;->decrypting_progress_message:I

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1224
    invoke-static {v2, v3}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1222
    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1225
    invoke-virtual {v0}, LadY;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1229
    iget-object v1, p0, Ladp;->a:Lamr;

    invoke-direct {p0, v0, v9, v1, v6}, Ladp;->b(LadY;Ljava/io/File;Lamr;Ljava/lang/String;)V

    .line 1235
    :goto_0
    invoke-virtual {v0}, LadY;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1238
    iget-object v1, p0, Ladp;->a:Lamr;

    move-wide v4, v2

    invoke-interface/range {v1 .. v6}, Lamr;->a(JJLjava/lang/String;)V

    .line 1240
    :cond_1
    iget-object v1, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v1

    invoke-interface {v1, v0}, Lalo;->a(LadY;)J

    move-result-wide v2

    .line 1241
    cmp-long v1, v2, v10

    if-nez v1, :cond_0

    .line 1243
    iget-object v0, p0, Ladp;->b:Ladw;

    invoke-virtual {v0}, Ladw;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1245
    :try_start_1
    iget-object v0, p0, Ladp;->b:Ladw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 1248
    :try_start_2
    iget-object v1, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Ladp;->a:Ladw;

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1251
    return-object v0

    .line 1233
    :cond_2
    :try_start_3
    iget-object v1, p0, Ladp;->a:Lamr;

    invoke-direct {p0, v0, v9, v1, v6}, Ladp;->a(LadY;Ljava/io/File;Lamr;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1247
    :catchall_0
    move-exception v0

    move v1, v7

    .line 1248
    :goto_1
    :try_start_4
    iget-object v2, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v3, p0, Ladp;->a:Ladw;

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1250
    if-nez v1, :cond_3

    .line 1251
    iget-object v1, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Ladp;->b:Ladw;

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    :cond_3
    throw v0

    .line 1250
    :catchall_1
    move-exception v0

    .line 1251
    throw v0

    .line 1250
    :catchall_2
    move-exception v0

    if-nez v1, :cond_4

    .line 1251
    iget-object v1, p0, Ladp;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Ladp;->b:Ladw;

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    :cond_4
    throw v0

    .line 1247
    :catchall_3
    move-exception v0

    move v1, v8

    goto :goto_1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1172
    invoke-virtual {p0}, Ladp;->a()Ladw;

    move-result-object v0

    return-object v0
.end method
