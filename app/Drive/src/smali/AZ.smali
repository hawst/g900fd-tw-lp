.class LAZ;
.super Ljava/lang/Object;
.source "DocListEmptyHasMoreViewFactory.java"


# instance fields
.field private final a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, LAZ;->a:Landroid/view/LayoutInflater;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;LapF;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 23
    iget-object v0, p0, LAZ;->a:Landroid/view/LayoutInflater;

    sget v1, Lxe;->doc_list_empty_view:I

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 25
    sget v0, Lxc;->empty_list_message:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 26
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 28
    sget v0, Lxc;->empty_list_message_details:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 29
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 31
    if-eqz p2, :cond_0

    sget-object v2, LapG;->c:LapG;

    invoke-interface {p2}, LapF;->a()LapG;

    move-result-object v3

    invoke-virtual {v2, v3}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    sget v2, Lxi;->error_fetch_more_retry_in_file_picker:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 37
    :goto_0
    return-object v1

    .line 34
    :cond_0
    sget v2, Lxi;->error_fetch_more_retry:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
