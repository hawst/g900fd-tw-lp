.class public LAR;
.super Ljava/lang/Object;
.source "DocListControllerImpl.java"

# interfaces
.implements LAP;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LAQ;

.field private final a:LAW;

.field private final a:LRO;

.field private final a:LSF;

.field private final a:LUi;

.field private final a:LaFO;

.field private final a:LaGR;

.field private final a:Lahh;

.field private final a:Lamw;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/lang/Runnable;

.field private final a:LqK;

.field private final a:LsC;

.field private final a:LtK;

.field private final a:LvL;

.field private final a:LvO;

.field private final a:LvU;

.field private final a:Lwa;

.field private final a:Lwm;


# direct methods
.method public constructor <init>(LSF;LaFO;LvU;Lwm;Lwa;LAQ;LRO;LvL;Landroid/content/Context;LqK;LtK;Lamx;LQr;LvO;LUi;Lahh;LaGR;LAW;LsC;)V
    .locals 21

    .prologue
    .line 153
    .line 165
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 153
    invoke-direct/range {v0 .. v20}, LAR;-><init>(LSF;LaFO;LvU;Lwm;Lwa;LAQ;LRO;LvL;Landroid/content/Context;LqK;LtK;Lamx;Ljava/util/concurrent/Executor;LQr;LvO;LUi;Lahh;LaGR;LAW;LsC;)V

    .line 173
    return-void
.end method

.method constructor <init>(LSF;LaFO;LvU;Lwm;Lwa;LAQ;LRO;LvL;Landroid/content/Context;LqK;LtK;Lamx;Ljava/util/concurrent/Executor;LQr;LvO;LUi;Lahh;LaGR;LAW;LsC;)V
    .locals 5

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v2, LAS;

    invoke-direct {v2, p0}, LAS;-><init>(LAR;)V

    iput-object v2, p0, LAR;->a:Ljava/lang/Runnable;

    .line 197
    iput-object p1, p0, LAR;->a:LSF;

    .line 198
    iput-object p2, p0, LAR;->a:LaFO;

    .line 199
    iput-object p3, p0, LAR;->a:LvU;

    .line 200
    iput-object p4, p0, LAR;->a:Lwm;

    .line 201
    iput-object p5, p0, LAR;->a:Lwa;

    .line 202
    iput-object p6, p0, LAR;->a:LAQ;

    .line 203
    iput-object p7, p0, LAR;->a:LRO;

    .line 204
    iput-object p8, p0, LAR;->a:LvL;

    .line 205
    iput-object p9, p0, LAR;->a:Landroid/content/Context;

    .line 206
    iput-object p10, p0, LAR;->a:LqK;

    .line 207
    move-object/from16 v0, p11

    iput-object v0, p0, LAR;->a:LtK;

    .line 208
    move-object/from16 v0, p15

    iput-object v0, p0, LAR;->a:LvO;

    .line 209
    move-object/from16 v0, p16

    iput-object v0, p0, LAR;->a:LUi;

    .line 210
    move-object/from16 v0, p17

    iput-object v0, p0, LAR;->a:Lahh;

    .line 211
    move-object/from16 v0, p18

    iput-object v0, p0, LAR;->a:LaGR;

    .line 212
    move-object/from16 v0, p19

    iput-object v0, p0, LAR;->a:LAW;

    .line 213
    const-string v2, "docListRequeryRateMs"

    const/16 v3, 0x3e8

    .line 214
    move-object/from16 v0, p14

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v2

    .line 215
    iget-object v3, p0, LAR;->a:Ljava/lang/Runnable;

    const-string v4, "DocListController.requery()"

    move-object/from16 v0, p12

    move-object/from16 v1, p13

    invoke-interface {v0, v3, v2, v1, v4}, Lamx;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lamw;

    move-result-object v2

    iput-object v2, p0, LAR;->a:Lamw;

    .line 217
    move-object/from16 v0, p20

    iput-object v0, p0, LAR;->a:LsC;

    .line 218
    return-void
.end method

.method static synthetic a(LAR;)LAQ;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:LAQ;

    return-object v0
.end method

.method static synthetic a(LAR;)LSF;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:LSF;

    return-object v0
.end method

.method static synthetic a(LAR;)LaFO;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:LaFO;

    return-object v0
.end method

.method static synthetic a(LAR;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LAR;Laay;Ljava/util/List;)LbmF;
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, LAR;->a(Laay;Ljava/util/List;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method private a(Laay;Ljava/util/List;)LbmF;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laay;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609
    iget-object v0, p0, LAR;->a:LvL;

    invoke-interface {v0, p1}, LvL;->a(Laay;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    .line 610
    invoke-static {p2}, Lbnm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 611
    new-instance v2, LvN;

    invoke-direct {v2}, LvN;-><init>()V

    .line 612
    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 613
    instance-of v4, v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    if-nez v4, :cond_0

    .line 614
    invoke-virtual {v2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    goto :goto_0

    .line 618
    :cond_1
    invoke-virtual {v2, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 619
    const/4 v0, 0x0

    .line 620
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    .line 619
    invoke-static {v0, v1}, Lwr;->a(Ljava/util/List;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LbmF;

    move-result-object v0

    .line 621
    return-object v0
.end method

.method static synthetic a(LAR;)LtK;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:LtK;

    return-object v0
.end method

.method static synthetic a(LAR;)LvU;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:LvU;

    return-object v0
.end method

.method static synthetic a(LAR;)Lwa;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:Lwa;

    return-object v0
.end method

.method static synthetic a(LAR;)Lwm;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LAR;->a:Lwm;

    return-object v0
.end method

.method private a(LaFO;)V
    .locals 3

    .prologue
    .line 643
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    .line 644
    iget-object v1, p0, LAR;->a:LvO;

    iget-object v2, p0, LAR;->a:LsC;

    .line 645
    invoke-interface {v2}, LsC;->a()LCl;

    move-result-object v2

    .line 644
    invoke-interface {v1, p1, v2}, LvO;->a(LaFO;LCl;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    .line 646
    new-instance v2, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-direct {v2, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-virtual {v0, v2}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 647
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    invoke-virtual {p0, v0}, LAR;->a(LbmF;)V

    .line 648
    return-void
.end method

.method private a(Landroid/os/Bundle;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 452
    if-nez p1, :cond_0

    .line 456
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 460
    :cond_0
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 461
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 462
    const-string v2, "app_data"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    .line 463
    if-nez p1, :cond_1

    .line 464
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 468
    :cond_1
    :goto_0
    iget-object v2, p0, LAR;->a:LvU;

    invoke-interface {v2, p1}, LvU;->a(Landroid/os/Bundle;)V

    .line 474
    invoke-static {p1}, Lwq;->a(Landroid/os/Bundle;)LbmF;

    move-result-object v3

    .line 478
    if-nez p2, :cond_b

    if-eqz v3, :cond_b

    invoke-virtual {v3}, LbmF;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 479
    invoke-virtual {v3}, LbmF;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 484
    :goto_1
    const-string v0, "collectionEntrySpec"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 486
    if-eqz v2, :cond_5

    .line 487
    if-eqz p2, :cond_4

    :goto_2
    invoke-direct {p0, v2, v1}, LAR;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 509
    :goto_3
    invoke-direct {p0}, LAR;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 510
    iget-object v0, p0, LAR;->a:LSF;

    iget-object v1, p0, LAR;->a:LaFO;

    invoke-interface {v0, v1}, LSF;->c(LaFO;)V

    .line 513
    :cond_2
    if-eqz p2, :cond_a

    invoke-direct {p0}, LAR;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "triggerSync"

    .line 514
    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 515
    invoke-direct {p0}, LAR;->e()V

    .line 527
    :cond_3
    :goto_4
    return-void

    :cond_4
    move-object v1, v3

    .line 487
    goto :goto_2

    .line 488
    :cond_5
    if-eqz p2, :cond_7

    if-eqz v0, :cond_7

    .line 489
    iget-object v1, p0, LAR;->a:LvO;

    invoke-interface {v1, v0}, LvO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 490
    if-eqz v3, :cond_6

    .line 492
    invoke-static {v3, v0}, Lwr;->a(Ljava/util/List;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LbmF;

    move-result-object v0

    invoke-virtual {p0, v0}, LAR;->a(LbmF;)V

    goto :goto_3

    .line 494
    :cond_6
    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-static {v1}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    invoke-virtual {p0, v0}, LAR;->a(LbmF;)V

    goto :goto_3

    .line 496
    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {v3}, LbmF;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 497
    invoke-virtual {p0, v3}, LAR;->a(LbmF;)V

    goto :goto_3

    .line 499
    :cond_8
    const-string v0, "mainFilter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LCl;

    .line 500
    if-eqz v0, :cond_9

    .line 501
    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v2, p0, LAR;->a:LvO;

    iget-object v3, p0, LAR;->a:LaFO;

    .line 502
    invoke-interface {v2, v3, v0}, LvO;->a(LaFO;LCl;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 501
    invoke-static {v1}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    invoke-virtual {p0, v0}, LAR;->a(LbmF;)V

    goto :goto_3

    .line 504
    :cond_9
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v1, p0, LAR;->a:LvO;

    iget-object v2, p0, LAR;->a:LaFO;

    .line 505
    invoke-interface {v1, v2}, LvO;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 504
    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    invoke-virtual {p0, v0}, LAR;->a(LbmF;)V

    goto/16 :goto_3

    .line 518
    :cond_a
    :try_start_0
    invoke-direct {p0}, LAR;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LAR;->a:Lahh;

    instance-of v0, v0, LagO;

    if-eqz v0, :cond_3

    iget-object v0, p0, LAR;->a:Lahh;

    check-cast v0, LagO;

    iget-object v1, p0, LAR;->a:LaFO;

    .line 520
    invoke-virtual {v0, v1}, LagO;->a(LaFO;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 521
    invoke-direct {p0}, LAR;->e()V
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 523
    :catch_0
    move-exception v0

    .line 524
    const-string v1, "DocListController"

    const-string v2, "Exception checking whether account has been synced"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_4

    :cond_b
    move-object v2, v0

    goto/16 :goto_1

    :cond_c
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, LAR;->a:LvU;

    invoke-interface {v0, p1}, LvU;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 370
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 566
    iget-object v0, p0, LAR;->a:LqK;

    const-string v1, "search"

    const-string v2, "searchFullText"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 578
    :goto_0
    const-wide/16 v0, -0x1

    .line 579
    invoke-static {p1, v0, v1}, Laay;->a(Ljava/lang/String;J)Laay;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LAR;->a(Laay;Ljava/util/List;)LbmF;

    move-result-object v0

    .line 584
    iget-object v1, p0, LAR;->a:LRO;

    iget-object v2, p0, LAR;->a:LaFO;

    .line 585
    invoke-interface {v1, v2, p1}, LRO;->a(LaFO;Ljava/lang/String;)LbsU;

    move-result-object v1

    .line 586
    new-instance v2, LAU;

    invoke-direct {v2, p0, v0}, LAU;-><init>(LAR;LbmF;)V

    invoke-static {v1, v2}, LbsK;->a(LbsU;LbsJ;)V

    .line 602
    return-void

    .line 573
    :cond_0
    iget-object v0, p0, LAR;->a:LvO;

    iget-object v1, p0, LAR;->a:LaFO;

    .line 574
    invoke-interface {v0, v1}, LvO;->a(LaFO;)LvN;

    move-result-object v0

    .line 575
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    goto :goto_0
.end method

.method static synthetic a(LAR;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Z
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1}, LAR;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Z
    .locals 3

    .prologue
    .line 725
    invoke-interface {p1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 726
    instance-of v2, v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    if-eqz v2, :cond_0

    .line 727
    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    .line 728
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 729
    const/4 v0, 0x1

    .line 734
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 665
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 677
    :cond_0
    :goto_0
    return v2

    .line 669
    :cond_1
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 670
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LCl;

    move-result-object v3

    .line 672
    iget-object v4, p0, LAR;->a:LsC;

    invoke-interface {v4}, LsC;->a()LCl;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 676
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 677
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private b(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 5

    .prologue
    .line 262
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 263
    invoke-interface {p1}, LaGu;->a()LaFM;

    move-result-object v1

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    .line 266
    iget-object v2, p0, LAR;->a:LaFO;

    invoke-virtual {v1, v2}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 267
    const-string v0, "DocListController"

    const-string v2, "The entry account name %s is not the same as the activity account name %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    iget-object v4, p0, LAR;->a:LaFO;

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    invoke-interface {p1}, LaGu;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    invoke-interface {p1}, LaGu;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 274
    iget-object v1, p0, LAR;->a:LqK;

    if-eqz v1, :cond_2

    .line 275
    iget-object v1, p0, LAR;->a:LqK;

    const-string v2, "doclist"

    const-string v3, "collections"

    const-string v4, "FromDoclist"

    invoke-virtual {v1, v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_2
    iget-object v1, p0, LAR;->a:LUi;

    invoke-interface {v1, v0}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 279
    iget-object v1, p0, LAR;->a:LvO;

    invoke-interface {v1, v0}, LvO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 280
    iget-object v1, p0, LAR;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()LbmF;

    move-result-object v1

    invoke-static {v1, v0}, Lwr;->a(Ljava/util/List;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LbmF;

    move-result-object v0

    invoke-direct {p0, v0}, LAR;->c(LbmF;)V

    goto :goto_0

    .line 282
    :cond_3
    iget-object v0, p0, LAR;->a:LAQ;

    invoke-interface {v0, p1, p2}, LAQ;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, LAR;->a:LvU;

    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 742
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LAR;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 743
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private c(LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 530
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    iget-object v1, p0, LAR;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()LbmF;

    move-result-object v1

    invoke-virtual {v1}, LbmF;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 534
    iget-object v0, p0, LAR;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    .line 536
    invoke-virtual {v0, p1}, LbmF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543
    :goto_1
    return-void

    .line 533
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 540
    :cond_1
    invoke-direct {p0}, LAR;->d()V

    .line 542
    invoke-virtual {p0, p1}, LAR;->a(LbmF;)V

    goto :goto_1
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, LAR;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    invoke-direct {p0, v0}, LAR;->b(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, LAR;->a:LvU;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LvU;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 366
    return-void
.end method

.method private d(LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 681
    iget-object v0, p0, LAR;->a:LaGR;

    new-instance v1, LAV;

    invoke-direct {v1, p0, p1}, LAV;-><init>(LAR;LbmF;)V

    invoke-virtual {v0, v1}, LaGR;->b(LaGN;)V

    .line 722
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 377
    const-string v0, "DocListController"

    const-string v1, "in resync"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    new-instance v0, LAT;

    invoke-direct {v0, p0}, LAT;-><init>(LAR;)V

    .line 393
    iget-object v1, p0, LAR;->a:LaGR;

    invoke-virtual {v1, v0}, LaGR;->b(LaGN;)V

    .line 394
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 549
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 550
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 552
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 553
    const-string v2, "accountName"

    iget-object v3, p0, LAR;->a:LaFO;

    invoke-virtual {v3}, LaFO;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const-string v2, "triggerSync"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 555
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 557
    iget-object v1, p0, LAR;->a:LAQ;

    invoke-interface {v1, v0}, LAQ;->b(Landroid/content/Intent;)V

    .line 558
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, LAR;->a:Lamw;

    invoke-interface {v0}, Lamw;->a()V

    .line 362
    return-void
.end method

.method public a(LCl;)V
    .locals 3

    .prologue
    .line 290
    invoke-interface {p1}, LCl;->a()Ljava/lang/String;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_0

    iget-object v1, p0, LAR;->a:LqK;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, LAR;->a:LqK;

    const-string v2, "doclist"

    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_0
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v1, p0, LAR;->a:LvO;

    iget-object v2, p0, LAR;->a:LaFO;

    .line 296
    invoke-interface {v1, v2, p1}, LvO;->a(LaFO;LCl;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 295
    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    invoke-direct {p0, v0}, LAR;->c(LbmF;)V

    .line 297
    return-void
.end method

.method public a(LaGu;)V
    .locals 1

    .prologue
    .line 356
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-direct {p0, v0}, LAR;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 357
    return-void
.end method

.method public a(LaGu;ILcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 3

    .prologue
    .line 236
    invoke-direct {p0}, LAR;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 237
    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    if-ne p3, v0, :cond_1

    .line 238
    :cond_0
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-direct {p0, v0}, LAR;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 251
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v0, p0, LAR;->a:LAQ;

    invoke-interface {v0, p1, p3}, LAQ;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    goto :goto_0

    .line 243
    :cond_2
    iget-object v0, p0, LAR;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "documentOpeningStarted"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, LAR;->a:LAW;

    invoke-virtual {v0, p1, p3}, LAW;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 246
    iget-object v0, p0, LAR;->a:LAQ;

    invoke-interface {v0, p1, p2}, LAQ;->a(LaGu;I)V

    goto :goto_0

    .line 248
    :cond_3
    invoke-direct {p0, p1, p3}, LAR;->b(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    goto :goto_0
.end method

.method public a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 3

    .prologue
    .line 255
    iget-object v0, p0, LAR;->a:LqK;

    const-string v1, "detailFragment"

    const-string v2, "documentOpeningStarted"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-direct {p0, p1, p2}, LAR;->b(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 259
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, LAR;->a:LAQ;

    invoke-interface {v0, p1}, LAQ;->a(Landroid/content/Intent;)V

    .line 408
    return-void
.end method

.method public a(Landroid/os/Bundle;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 223
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 224
    :goto_0
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1, v0}, LAR;->a(Landroid/os/Bundle;Z)V

    .line 226
    return-void

    .line 223
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 634
    .line 635
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 636
    iget-object v1, p0, LAR;->a:Lwm;

    invoke-interface {v1, p1}, Lwm;->a(Ljava/util/List;)V

    .line 637
    iget-object v1, p0, LAR;->a:Lwa;

    invoke-interface {v1, v0}, Lwa;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V

    .line 638
    invoke-direct {p0, p1}, LAR;->d(LbmF;)V

    .line 639
    iget-object v0, p0, LAR;->a:LAQ;

    invoke-interface {v0}, LAQ;->g()V

    .line 640
    return-void
.end method

.method public a(Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LCl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 301
    new-instance v1, LvN;

    invoke-direct {v1}, LvN;-><init>()V

    .line 302
    iget-object v0, p0, LAR;->a:LvL;

    iget-object v2, p0, LAR;->a:LaFO;

    invoke-interface {v0, v2}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 304
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    .line 305
    iget-object v3, p0, LAR;->a:LvL;

    invoke-interface {v3, v0}, LvL;->a(LCl;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    .line 306
    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    goto :goto_0

    .line 309
    :cond_0
    invoke-virtual {v1}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 310
    iget-object v2, p0, LAR;->a:Lwm;

    invoke-interface {v2}, Lwm;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    :goto_1
    return-void

    .line 314
    :cond_1
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    iget-object v2, p0, LAR;->a:Lwm;

    .line 315
    invoke-interface {v2}, Lwm;->a()LbmF;

    move-result-object v2

    invoke-virtual {v0, v2}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 316
    invoke-virtual {v1}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-virtual {v0, v2}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    .line 319
    invoke-direct {p0, v0}, LAR;->c(LbmF;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, LAR;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    .line 413
    invoke-static {v0}, Lwr;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 414
    iget-object v0, p0, LAR;->a:LvO;

    iget-object v1, p0, LAR;->a:LaFO;

    .line 415
    invoke-interface {v0, v1}, LvO;->a(LaFO;)LvN;

    move-result-object v0

    .line 417
    iget-object v1, p0, LAR;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()LbmF;

    move-result-object v1

    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-static {v1, v0}, Lwr;->a(Ljava/util/List;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LbmF;

    move-result-object v0

    .line 419
    :cond_0
    invoke-direct {p0, p1, v0}, LAR;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 420
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 652
    iget-object v2, p0, LAR;->a:Lwm;

    invoke-interface {v2}, Lwm;->a()LbmF;

    move-result-object v2

    .line 653
    invoke-virtual {v2}, LbmF;->size()I

    move-result v3

    if-gt v3, v1, :cond_1

    .line 654
    invoke-direct {p0, v2}, LAR;->a(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 661
    :goto_0
    return v0

    .line 657
    :cond_0
    iget-object v0, p0, LAR;->a:LaFO;

    invoke-direct {p0, v0}, LAR;->a(LaFO;)V

    move v0, v1

    .line 658
    goto :goto_0

    .line 660
    :cond_1
    invoke-virtual {v2}, LbmF;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v0, v3}, LbmF;->a(II)LbmF;

    move-result-object v0

    invoke-virtual {p0, v0}, LAR;->a(LbmF;)V

    move v0, v1

    .line 661
    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, LAR;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    invoke-direct {p0}, LAR;->f()V

    .line 339
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v0, p0, LAR;->a:LAQ;

    invoke-interface {v0}, LAQ;->s()V

    goto :goto_0
.end method

.method public b(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 343
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 350
    invoke-static {v0}, Lwq;->a(Landroid/os/Bundle;)LbmF;

    move-result-object v0

    .line 351
    invoke-direct {p0, v0}, LAR;->c(LbmF;)V

    goto :goto_0
.end method

.method public b(LbmF;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 324
    iget-object v0, p0, LAR;->a:LqK;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, LAR;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "collections"

    const-string v3, "NotFromDoclist"

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_0
    invoke-direct {p0, p1}, LAR;->c(LbmF;)V

    .line 330
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 398
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    invoke-direct {p0}, LAR;->d()V

    .line 403
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v0, p0, LAR;->a:LAQ;

    invoke-interface {v0}, LAQ;->q()V

    goto :goto_0
.end method

.method public i()V
    .locals 3

    .prologue
    .line 424
    iget-object v0, p0, LAR;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    .line 425
    invoke-static {v0}, Lwr;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 426
    const/4 v1, 0x0

    invoke-virtual {v0}, LbmF;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, LbmF;->a(II)LbmF;

    move-result-object v0

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 427
    invoke-direct {p0, v0}, LAR;->c(LbmF;)V

    .line 429
    :cond_0
    return-void
.end method
