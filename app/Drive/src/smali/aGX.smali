.class public final LaGX;
.super LaGm;
.source "PendingOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaFe;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private final a:J

.field private a:Ljava/lang/String;

.field private final b:J


# direct methods
.method public constructor <init>(LaEz;JLjava/lang/String;J)V
    .locals 8

    .prologue
    .line 38
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-wide v5, p5

    invoke-direct/range {v0 .. v7}, LaGX;-><init>(LaEz;JLjava/lang/String;JI)V

    .line 39
    return-void
.end method

.method public constructor <init>(LaEz;JLjava/lang/String;JI)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    invoke-static {}, LaFe;->a()LaFe;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {p0, p1, v0, v3}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 28
    iput-wide p2, p0, LaGX;->a:J

    .line 29
    cmp-long v0, p2, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "not persisted: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LbiT;->b(ZLjava/lang/Object;)V

    .line 30
    const-string v0, "null payload"

    invoke-static {p4, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaGX;->a:Ljava/lang/String;

    .line 31
    cmp-long v0, p5, v6

    if-ltz v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid timestamp: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LbiT;->a(ZLjava/lang/Object;)V

    .line 32
    iput-wide p5, p0, LaGX;->b:J

    .line 33
    iput p7, p0, LaGX;->a:I

    .line 34
    return-void

    :cond_0
    move v0, v2

    .line 29
    goto :goto_0

    :cond_1
    move v1, v2

    .line 31
    goto :goto_1
.end method

.method public static a(LaFM;LaEz;Landroid/database/Cursor;)LaGX;
    .locals 8

    .prologue
    .line 81
    sget-object v0, LaFf;->a:LaFf;

    .line 82
    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 83
    sget-object v0, LaFf;->b:LaFf;

    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p2}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 84
    sget-object v0, LaFf;->c:LaFf;

    .line 85
    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 86
    sget-object v0, LaFf;->d:LaFf;

    .line 87
    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 88
    new-instance v0, LaGX;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LaGX;-><init>(LaEz;JLjava/lang/String;JI)V

    .line 91
    invoke-static {}, LaFe;->a()LaFe;

    move-result-object v1

    invoke-virtual {v1}, LaFe;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 90
    invoke-virtual {v0, v2, v3}, LaGX;->c(J)V

    .line 92
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, LaGX;->a:I

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, LaGX;->b:J

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LaGX;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput v0, p0, LaGX;->a:I

    .line 102
    return-void
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 43
    sget-object v0, LaFf;->a:LaFf;

    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaGX;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 44
    sget-object v0, LaFf;->b:LaFf;

    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGX;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    sget-object v0, LaFf;->c:LaFf;

    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaGX;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 46
    sget-object v0, LaFf;->d:LaFf;

    invoke-virtual {v0}, LaFf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, LaGX;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 47
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 96
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iput-object p1, p0, LaGX;->a:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, LaGX;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaGX;->a:I

    .line 67
    invoke-virtual {p0}, LaGX;->e()V

    .line 68
    iget v0, p0, LaGX;->a:I

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, LaGX;->a:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 73
    const-string v0, "Operation[accountSqlId=%s, payload=%s, timestamp=%s, sqlId=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 74
    invoke-virtual {p0}, LaGX;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaGX;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, LaGX;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, LaGX;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 73
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
