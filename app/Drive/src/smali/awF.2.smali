.class public final LawF;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ZoomView.java"


# instance fields
.field private a:F

.field final synthetic a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:F

.field private b:Z

.field private c:F

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 708
    iput-object p1, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 719
    iput-boolean v1, p0, LawF;->a:Z

    .line 720
    iput-boolean v1, p0, LawF;->b:Z

    .line 721
    iput-boolean v0, p0, LawF;->c:Z

    .line 722
    iput-boolean v0, p0, LawF;->d:Z

    .line 728
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LawF;->a:Ljava/util/Queue;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V
    .locals 0

    .prologue
    .line 708
    invoke-direct {p0, p1}, LawF;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V

    return-void
.end method

.method private a()F
    .locals 2

    .prologue
    .line 795
    iget v0, p0, LawF;->b:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LawF;->a:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private a(III)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/16 v3, 0x19

    .line 896
    packed-switch p1, :pswitch_data_0

    .line 904
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Axis must be MotionEvent.AXIS_X or _Y "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898
    :pswitch_0
    iget-boolean v2, p0, LawF;->a:Z

    if-eqz v2, :cond_0

    neg-int v2, p2

    if-gt v2, v3, :cond_1

    :cond_0
    iget-boolean v2, p0, LawF;->b:Z

    if-eqz v2, :cond_2

    if-le p2, v3, :cond_2

    :cond_1
    move v0, v1

    .line 901
    :cond_2
    :goto_0
    return v0

    :pswitch_1
    iget-boolean v2, p0, LawF;->c:Z

    if-eqz v2, :cond_3

    neg-int v2, p3

    if-gt v2, v3, :cond_4

    :cond_3
    iget-boolean v2, p0, LawF;->d:Z

    if-eqz v2, :cond_2

    if-le p3, v3, :cond_2

    :cond_4
    move v0, v1

    goto :goto_0

    .line 896
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic a(LawF;Z)Z
    .locals 0

    .prologue
    .line 708
    iput-boolean p1, p0, LawF;->a:Z

    return p1
.end method

.method public static synthetic b(LawF;Z)Z
    .locals 0

    .prologue
    .line 708
    iput-boolean p1, p0, LawF;->b:Z

    return p1
.end method

.method public static synthetic c(LawF;Z)Z
    .locals 0

    .prologue
    .line 708
    iput-boolean p1, p0, LawF;->c:Z

    return p1
.end method

.method public static synthetic d(LawF;Z)Z
    .locals 0

    .prologue
    .line 708
    iput-boolean p1, p0, LawF;->d:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 799
    iput v0, p0, LawF;->a:F

    .line 800
    iput v0, p0, LawF;->b:F

    .line 801
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->d(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    move-result v0

    iput-boolean v0, p0, LawF;->e:Z

    .line 802
    iget-object v0, p0, LawF;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 803
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 850
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    .line 851
    iget-object v1, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v1

    .line 852
    iget-object v2, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;FF)V

    .line 853
    cmpl-float v2, v0, v1

    if-eqz v2, :cond_0

    .line 854
    iput v1, p0, LawF;->c:F

    .line 855
    iget-object v1, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v1, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;F)V

    .line 862
    :goto_0
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V

    .line 863
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    .line 864
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)V

    .line 865
    const-string v0, "ZoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Report Position: Finish Double tap "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Lawh;

    move-result-object v2

    invoke-virtual {v2}, Lawh;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    return v5

    .line 857
    :cond_0
    iget v0, p0, LawF;->c:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 858
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iput v0, p0, LawF;->c:F

    .line 860
    :cond_1
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    iget v1, p0, LawF;->c:F

    invoke-static {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;F)V

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11

    .prologue
    .line 807
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z

    .line 808
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z

    .line 811
    div-float v0, p4, p3

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 812
    const/4 p3, 0x0

    .line 815
    :cond_0
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v1

    .line 816
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v2

    .line 817
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v0

    .line 818
    iget-object v3, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 820
    const-string v0, "ZoomView"

    const-string v3, "Abort fling at (%s %s) with v (%s %s) "

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 821
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x2

    neg-float v2, p3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x3

    neg-float v2, p4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v4, v1

    .line 820
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    const/4 v0, 0x1

    .line 844
    :goto_0
    return v0

    .line 826
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 827
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v6, v3, 0x2

    move v5, v6

    .line 833
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v4, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 834
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v3, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v8, v0, 0x2

    move v7, v8

    .line 840
    :goto_2
    const-string v0, "ZoomView"

    const-string v3, "Start fling at (%s %s) with v (%s %s) min (%s, %s) max (%s, %s)"

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 841
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x2

    neg-float v10, p3

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x3

    neg-float v10, p4

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v4, v9

    const/4 v9, 0x7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v4, v9

    .line 840
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/widget/OverScroller;

    move-result-object v0

    neg-float v3, p3

    float-to-int v3, v3

    neg-float v4, p4

    float-to-int v4, v4

    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    .line 843
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->invalidate()V

    .line 844
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 829
    :cond_2
    const/4 v5, 0x0

    .line 830
    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v6, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v6}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int/2addr v4, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto/16 :goto_1

    .line 836
    :cond_3
    const/4 v7, 0x0

    .line 837
    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v4, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v8

    goto/16 :goto_2
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performLongClick()Z

    .line 882
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 738
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 739
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z

    .line 790
    :cond_0
    :goto_0
    return v3

    .line 745
    :cond_1
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 746
    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 748
    iget-boolean v0, p0, LawF;->e:Z

    if-eqz v0, :cond_6

    .line 750
    iget-object v0, p0, LawF;->a:Ljava/util/Queue;

    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5, p3, p4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v5}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 751
    iget v0, p0, LawF;->a:F

    add-float/2addr v0, p3

    iput v0, p0, LawF;->a:F

    .line 752
    iget v0, p0, LawF;->b:F

    add-float/2addr v0, p4

    iput v0, p0, LawF;->b:F

    .line 755
    :goto_1
    invoke-direct {p0}, LawF;->a()F

    move-result v0

    const/high16 v5, 0x42480000    # 50.0f

    cmpl-float v0, v0, v5

    if-lez v0, :cond_2

    iget-object v0, p0, LawF;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-le v0, v3, :cond_2

    .line 757
    iget-object v0, p0, LawF;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 758
    iget v5, p0, LawF;->b:F

    iget v6, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    iput v5, p0, LawF;->b:F

    .line 759
    iget v5, p0, LawF;->a:F

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v0, v5, v0

    iput v0, p0, LawF;->a:F

    goto :goto_1

    .line 762
    :cond_2
    invoke-direct {p0}, LawF;->a()F

    move-result v0

    const/high16 v5, 0x41a00000    # 20.0f

    cmpl-float v0, v0, v5

    if-lez v0, :cond_4

    iget v0, p0, LawF;->b:F

    iget v5, p0, LawF;->a:F

    div-float/2addr v0, v5

    .line 763
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v5, 0x40000000    # 2.0f

    cmpg-float v0, v0, v5

    if-gez v0, :cond_4

    .line 764
    iput-boolean v2, p0, LawF;->e:Z

    .line 765
    const-string v0, "ZoomView"

    const-string v5, "Scroll correction switch"

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 772
    :goto_2
    iget-object v1, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollBy(II)V

    .line 773
    iget-object v1, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    .line 774
    sget-boolean v1, LavX;->e:Z

    if-eqz v1, :cond_3

    .line 775
    const-string v1, "ZoomView"

    const-string v5, "Scroll by (%s %s)"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    :cond_3
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)V

    .line 781
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 783
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    move v0, v2

    .line 784
    :goto_3
    iget-object v1, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)I

    move-result v1

    iget-object v4, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)I

    move-result v4

    invoke-direct {p0, v0, v1, v4}, LawF;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    const-string v0, "ZoomView"

    const-string v1, "Scroll past edge by (%s %s)"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 768
    goto/16 :goto_2

    :cond_5
    move v0, v3

    .line 783
    goto :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_2
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 872
    iget-object v0, p0, LawF;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 874
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
