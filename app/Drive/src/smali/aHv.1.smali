.class public LaHv;
.super Ljava/lang/Object;
.source "OperationQueueNetworkChangeListener.java"

# interfaces
.implements Laad;


# instance fields
.field private final a:LaGM;

.field private final a:LaHr;

.field private final a:Lald;

.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LaGM;Lald;LaHr;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LaHv;->a:Landroid/content/Context;

    .line 37
    iput-object p2, p0, LaHv;->a:LaGM;

    .line 38
    iput-object p3, p0, LaHv;->a:Lald;

    .line 39
    iput-object p4, p0, LaHv;->a:LaHr;

    .line 40
    return-void
.end method

.method static synthetic a(LaHv;)LaGM;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, LaHv;->a:LaGM;

    return-object v0
.end method

.method static synthetic a(LaHv;)LaHr;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, LaHv;->a:LaHr;

    return-object v0
.end method

.method static synthetic a(LaHv;)Lald;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, LaHv;->a:Lald;

    return-object v0
.end method

.method static synthetic a(LaHv;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, LaHv;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 44
    const-string v0, "connectivity"

    .line 45
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 46
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, LaHw;

    invoke-direct {v0, p0}, LaHw;-><init>(LaHv;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 59
    invoke-virtual {v0, v1}, LaHw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 61
    :cond_0
    return-void
.end method
