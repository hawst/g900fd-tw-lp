.class public LyY;
.super Ljava/lang/Object;
.source "DetailListHeaderController.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LQU;

.field private final a:Landroid/content/Context;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/TextView;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;LQU;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, LyY;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, LyY;->a:LQU;

    .line 44
    return-void
.end method

.method static synthetic a(LyY;)LQU;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LyY;->a:LQU;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 95
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LyY;->a:Landroid/content/Context;

    .line 96
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LwZ;->m_app_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 95
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 97
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->thumbnail_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 99
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->thumbnail_gradient:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 101
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LyY;->a(Z)V

    .line 102
    return-void
.end method

.method private a(Landroid/widget/AbsListView;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 162
    iget-object v0, p0, LyY;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LyY;->a:Landroid/view/View;

    if-nez v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 167
    if-eqz v2, :cond_0

    .line 172
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LyY;->b:Landroid/view/View;

    .line 173
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v3, p0, LyY;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    if-lt v0, v3, :cond_2

    const/4 v0, 0x1

    .line 175
    :goto_1
    if-eqz v0, :cond_3

    .line 176
    invoke-direct {p0}, LyY;->b()V

    .line 179
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 181
    if-gez v2, :cond_4

    .line 182
    neg-int v0, v2

    int-to-double v4, v0

    const-wide v6, 0x3ff051eb851eb852L    # 1.02

    div-double/2addr v4, v6

    double-to-int v0, v4

    .line 184
    :goto_2
    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, LyY;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v4, p0, LyY;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 186
    iget-object v3, p0, LyY;->b:Landroid/view/View;

    sget v4, Lxc;->thumbnail:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 187
    invoke-virtual {v3, v1, v0, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 189
    iget-object v0, p0, LyY;->b:Landroid/view/View;

    sget v1, Lxc;->icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 190
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 173
    goto :goto_1

    .line 192
    :cond_3
    invoke-direct {p0}, LyY;->a()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, LyY;->a:Landroid/content/Context;

    invoke-static {v0}, LakQ;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0, p1}, LyY;->b(Z)V

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-direct {p0, p1}, LyY;->c(Z)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 109
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LyY;->a:Landroid/content/Context;

    .line 110
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 109
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 111
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 112
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->thumbnail_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 113
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->thumbnail_gradient:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LyY;->a(Z)V

    .line 116
    return-void
.end method

.method private b(Z)V
    .locals 6

    .prologue
    .line 131
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->title_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 132
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    iget-object v0, p0, LyY;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->sticky_header_elevation:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 138
    :try_start_0
    const-class v1, Landroid/view/View;

    const-string v2, "setTranslationZ"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 139
    iget-object v2, p0, LyY;->a:Landroid/view/View;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_1
    return-void

    .line 139
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    invoke-direct {p0, p1}, LyY;->c(Z)V

    goto :goto_1
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->title_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 147
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 148
    return-void

    .line 147
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 85
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0
.end method

.method a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 76
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 77
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 47
    const-string v0, "DetailListHeaderController"

    const-string v1, "Sticky header view registered."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 48
    iput-object p1, p0, LyY;->a:Landroid/view/View;

    .line 49
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LyZ;

    invoke-direct {v1, p0}, LyZ;-><init>(LyY;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, LyY;->a:Landroid/view/View;

    sget v1, Lxc;->title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LyY;->a:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, LyY;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 57
    iget-object v0, p0, LyY;->a:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 58
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LyY;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, LyY;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, LyY;->b:Landroid/view/View;

    .line 62
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0, p1}, LyY;->a(Landroid/widget/AbsListView;)V

    .line 154
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0, p1}, LyY;->a(Landroid/widget/AbsListView;)V

    .line 159
    return-void
.end method
