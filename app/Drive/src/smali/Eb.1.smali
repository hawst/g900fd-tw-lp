.class final enum LEb;
.super Ljava/lang/Enum;
.source "ThumbnailFetchScheduler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LEb;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LEb;

.field private static final synthetic a:[LEb;

.field public static final enum b:LEb;


# instance fields
.field private final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    new-instance v0, LEb;

    const-string v1, "CACHED"

    const-wide/16 v2, 0x32

    invoke-direct {v0, v1, v4, v2, v3}, LEb;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LEb;->a:LEb;

    .line 17
    new-instance v0, LEb;

    const-string v1, "UNCACHED"

    const-wide/16 v2, 0x64

    invoke-direct {v0, v1, v5, v2, v3}, LEb;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LEb;->b:LEb;

    .line 15
    const/4 v0, 0x2

    new-array v0, v0, [LEb;

    sget-object v1, LEb;->a:LEb;

    aput-object v1, v0, v4

    sget-object v1, LEb;->b:LEb;

    aput-object v1, v0, v5

    sput-object v0, LEb;->a:[LEb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 23
    iput-wide p3, p0, LEb;->a:J

    .line 24
    return-void

    .line 22
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LEb;
    .locals 1

    .prologue
    .line 15
    const-class v0, LEb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LEb;

    return-object v0
.end method

.method public static values()[LEb;
    .locals 1

    .prologue
    .line 15
    sget-object v0, LEb;->a:[LEb;

    invoke-virtual {v0}, [LEb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LEb;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, LEb;->a:J

    return-wide v0
.end method
