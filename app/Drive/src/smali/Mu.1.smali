.class public final LMu;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LMq;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LMs;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LMt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LMu;->a:LbrA;

    .line 41
    const-class v0, LMq;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LMu;->a:Lbsk;

    .line 44
    const-class v0, LMs;

    invoke-static {v0, v2}, LMu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LMu;->b:Lbsk;

    .line 47
    const-class v0, LMt;

    invoke-static {v0, v2}, LMu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LMu;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 158
    packed-switch p1, :pswitch_data_0

    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :pswitch_0
    new-instance v0, LMq;

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 163
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LMu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->j:Lbsk;

    .line 161
    invoke-static {v1, v2}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGg;

    iget-object v2, p0, LMu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LahE;

    iget-object v2, v2, LahE;->h:Lbsk;

    .line 169
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LMu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LahE;

    iget-object v3, v3, LahE;->h:Lbsk;

    .line 167
    invoke-static {v2, v3}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LahB;

    iget-object v3, p0, LMu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTV;

    iget-object v3, v3, LTV;->a:Lbsk;

    .line 175
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LMu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LTV;

    iget-object v4, v4, LTV;->a:Lbsk;

    .line 173
    invoke-static {v3, v4}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LTT;

    iget-object v4, p0, LMu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LqD;

    iget-object v4, v4, LqD;->c:Lbsk;

    .line 181
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LMu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LqD;

    iget-object v5, v5, LqD;->c:Lbsk;

    .line 179
    invoke-static {v4, v5}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LqK;

    iget-object v5, p0, LMu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LQH;

    iget-object v5, v5, LQH;->d:Lbsk;

    .line 187
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LMu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LQH;

    iget-object v6, v6, LQH;->d:Lbsk;

    .line 185
    invoke-static {v5, v6}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LQr;

    iget-object v6, p0, LMu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 193
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LMu;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LpG;

    iget-object v7, v7, LpG;->m:Lbsk;

    .line 191
    invoke-static {v6, v7}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LtK;

    invoke-direct/range {v0 .. v6}, LMq;-><init>(LaGg;LahB;LTT;LqK;LQr;LtK;)V

    .line 198
    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LMu;

    .line 199
    invoke-virtual {v1, v0}, LMu;->a(LMq;)V

    .line 220
    :goto_0
    return-object v0

    .line 202
    :pswitch_1
    new-instance v1, LMs;

    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 205
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LMu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 203
    invoke-static {v0, v2}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LMs;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 210
    goto :goto_0

    .line 212
    :pswitch_2
    new-instance v1, LMt;

    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMu;

    iget-object v0, v0, LMu;->a:Lbsk;

    .line 215
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LMu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LMu;

    iget-object v2, v2, LMu;->a:Lbsk;

    .line 213
    invoke-static {v0, v2}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMq;

    invoke-direct {v1, v0}, LMt;-><init>(LMq;)V

    move-object v0, v1

    .line 220
    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x2b8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 255
    .line 257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 132
    const-class v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x66

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LMu;->a(LbuP;LbuB;)V

    .line 135
    const-class v0, LMm;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x67

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LMu;->a(LbuP;LbuB;)V

    .line 138
    const-class v0, LMq;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x65

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LMu;->a(LbuP;LbuB;)V

    .line 141
    const-class v0, LMq;

    iget-object v1, p0, LMu;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LMu;->a(Ljava/lang/Class;Lbsk;)V

    .line 142
    const-class v0, LMs;

    iget-object v1, p0, LMu;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LMu;->a(Ljava/lang/Class;Lbsk;)V

    .line 143
    const-class v0, LMt;

    iget-object v1, p0, LMu;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LMu;->a(Ljava/lang/Class;Lbsk;)V

    .line 144
    iget-object v0, p0, LMu;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2b8

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 146
    iget-object v0, p0, LMu;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2b9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 148
    iget-object v0, p0, LMu;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2ba

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 150
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 229
    packed-switch p1, :pswitch_data_0

    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;

    .line 233
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMu;

    .line 234
    invoke-virtual {v0, p2}, LMu;->a(Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;)V

    .line 251
    :goto_0
    return-void

    .line 237
    :pswitch_1
    check-cast p2, LMm;

    .line 239
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMu;

    .line 240
    invoke-virtual {v0, p2}, LMu;->a(LMm;)V

    goto :goto_0

    .line 243
    :pswitch_2
    check-cast p2, LMq;

    .line 245
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMu;

    .line 246
    invoke-virtual {v0, p2}, LMu;->a(LMq;)V

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(LMm;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 84
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 82
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, LMm;->a:LaGM;

    .line 88
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->r:Lbsk;

    .line 91
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->r:Lbsk;

    .line 89
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakk;

    iput-object v0, p1, LMm;->a:Lakk;

    .line 95
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaEV;

    iget-object v0, v0, LaEV;->b:Lbsk;

    .line 98
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaEV;

    iget-object v1, v1, LaEV;->b:Lbsk;

    .line 96
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaEz;

    iput-object v0, p1, LMm;->a:LaEz;

    .line 102
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 105
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 103
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LMm;->a:LtK;

    .line 109
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 112
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 110
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, LMm;->a:LqK;

    .line 116
    return-void
.end method

.method public a(LMq;)V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->d:Lbsk;

    .line 123
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->d:Lbsk;

    .line 121
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagO;

    iput-object v0, p1, LMq;->a:LagO;

    .line 127
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMu;

    iget-object v0, v0, LMu;->a:Lbsk;

    .line 59
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LMu;

    iget-object v1, v1, LMu;->a:Lbsk;

    .line 57
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMq;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;->a:LMq;

    .line 63
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->r:Lbsk;

    .line 66
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->r:Lbsk;

    .line 64
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakk;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;->a:Lakk;

    .line 70
    iget-object v0, p0, LMu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 73
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LMu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 71
    invoke-static {v0, v1}, LMu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;->a:LqK;

    .line 77
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method
