.class public LTp;
.super Ljava/lang/Object;
.source "HttpResponseHelper.java"


# direct methods
.method public static a(Lorg/apache/http/HttpEntity;LTF;)Ljava/io/InputStreamReader;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-static {p0}, LTp;->a(Lorg/apache/http/HttpEntity;)Ljava/nio/charset/Charset;

    move-result-object v0

    .line 85
    :try_start_0
    invoke-interface {p1, p0}, LTF;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 86
    :try_start_1
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 88
    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 93
    :cond_0
    if-nez v3, :cond_2

    .line 94
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to create stream reader. Encoding = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 88
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    .line 89
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0

    .line 97
    :cond_2
    return-object v3

    .line 88
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static a(Lorg/apache/http/Header;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 30
    if-nez p0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-object v0

    .line 34
    :cond_1
    invoke-interface {p0}, Lorg/apache/http/Header;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v2

    .line 35
    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 36
    const-string v5, "charset"

    invoke-interface {v4, v5}, Lorg/apache/http/HeaderElement;->getParameterByName(Ljava/lang/String;)Lorg/apache/http/NameValuePair;

    move-result-object v4

    .line 37
    if-eqz v4, :cond_2

    .line 38
    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 35
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static a(Lorg/apache/http/HttpEntity;LTF;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    const/4 v1, 0x0

    .line 113
    :try_start_0
    invoke-static {p0, p1}, LTp;->a(Lorg/apache/http/HttpEntity;LTF;)Ljava/io/InputStreamReader;

    move-result-object v1

    .line 114
    invoke-static {v1}, Lbrk;->a(Ljava/lang/Readable;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 116
    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    :cond_0
    return-object v0

    .line 116
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 117
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    :cond_1
    throw v0
.end method

.method private static a(Lorg/apache/http/HttpEntity;)Ljava/nio/charset/Charset;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 51
    .line 52
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v1

    invoke-static {v1}, LTp;->a(Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v1

    .line 53
    if-eqz v1, :cond_0

    .line 55
    :try_start_0
    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 63
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 64
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    .line 67
    :cond_1
    return-object v0

    .line 56
    :catch_0
    move-exception v1

    .line 57
    const-string v1, "HttpResponseHelper"

    const-string v2, "Unsupported character set returned: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
