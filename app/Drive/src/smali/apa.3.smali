.class Lapa;
.super Ljava/lang/ThreadLocal;
.source "ReusableThumbnailCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ThreadLocal",
        "<",
        "LbjP",
        "<",
        "Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LQr;

.field final synthetic a:LaoY;


# direct methods
.method constructor <init>(LaoY;LQr;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lapa;->a:LaoY;

    iput-object p2, p0, Lapa;->a:LQr;

    invoke-direct {p0}, Ljava/lang/ThreadLocal;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()LbjP;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbjP",
            "<",
            "Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lapa;->a:LQr;

    const-string v1, "thumbnailBitmapCacheExpirationTimeSeconds"

    const/16 v2, 0x12c

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 67
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 68
    invoke-virtual {v1, v2, v3}, LbjG;->a(J)LbjG;

    move-result-object v1

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 69
    invoke-virtual {v1, v2, v3, v0}, LbjG;->b(JLjava/util/concurrent/TimeUnit;)LbjG;

    move-result-object v0

    new-instance v1, Lapb;

    invoke-direct {v1, p0}, Lapb;-><init>(Lapa;)V

    .line 70
    invoke-virtual {v0, v1}, LbjG;->a(LbjM;)LbjP;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic initialValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lapa;->a()LbjP;

    move-result-object v0

    return-object v0
.end method
