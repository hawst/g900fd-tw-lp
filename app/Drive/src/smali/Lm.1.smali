.class public abstract LLm;
.super Ljava/lang/Object;
.source "SelectionDragEvent.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/DragEvent;)LLf;
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v0

    .line 32
    instance-of v1, v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    if-eqz v1, :cond_0

    .line 33
    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    .line 34
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a()LLf;

    move-result-object v0

    .line 36
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/view/View;LLl;)Z
.end method

.method public final onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 42
    invoke-virtual {p2}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v0

    .line 43
    instance-of v0, v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    if-eqz v0, :cond_3

    .line 44
    invoke-direct {p0, p2}, LLm;->a(Landroid/view/DragEvent;)LLf;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    new-instance v0, LLn;

    invoke-direct {v0, p2}, LLn;-><init>(Landroid/view/DragEvent;)V

    invoke-virtual {p0, p1, v0}, LLm;->a(Landroid/view/View;LLl;)Z

    move-result v0

    .line 65
    :goto_0
    return v0

    .line 51
    :cond_0
    new-instance v1, LLn;

    invoke-direct {v1, p2}, LLn;-><init>(Landroid/view/DragEvent;)V

    invoke-virtual {p0, p1, v1}, LLm;->a(Landroid/view/View;LLl;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    invoke-virtual {v0, p1, p0}, LLf;->a(Landroid/view/View;LLm;)V

    .line 56
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 58
    :cond_2
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 59
    invoke-direct {p0, p2}, LLm;->a(Landroid/view/DragEvent;)LLf;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_3

    .line 62
    invoke-virtual {v0}, LLf;->a()V

    .line 65
    :cond_3
    new-instance v0, LLn;

    invoke-direct {v0, p2}, LLn;-><init>(Landroid/view/DragEvent;)V

    invoke-virtual {p0, p1, v0}, LLm;->a(Landroid/view/View;LLl;)Z

    move-result v0

    goto :goto_0
.end method
