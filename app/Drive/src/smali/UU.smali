.class public LUU;
.super Ljava/lang/Object;
.source "UrlParserImpl.java"

# interfaces
.implements LUT;


# instance fields
.field private final a:LQr;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LVb;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LVb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LQr;)V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    iput-object p1, p0, LUU;->a:LQr;

    .line 201
    invoke-direct {p0}, LUU;->a()V

    .line 202
    return-void
.end method

.method static synthetic a(LUU;)LQr;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LUU;->a:LQr;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 37
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    new-instance v1, LUL;

    sget-object v2, LVc;->f:LVc;

    const-string v3, "(/spreadsheet)?/(m|ccc|lv)"

    const-string v4, "key"

    invoke-direct {v1, v2, v3, v4}, LUL;-><init>(LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LUU;->a:Ljava/util/List;

    .line 44
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    new-instance v1, LUL;

    sget-object v2, LVc;->a:LVc;

    const-string v3, "/document/(m|edit|view)"

    const-string v4, "id"

    invoke-direct {v1, v2, v3, v4}, LUL;-><init>(LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUC;

    sget-object v2, LVc;->a:LVc;

    const-string v3, "/document/d/([^/]*).*"

    invoke-direct {v1, v2, v3, v5}, LUC;-><init>(LVc;Ljava/lang/String;I)V

    .line 52
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUZ;

    sget-object v2, LVc;->a:LVc;

    const-string v3, "/(Doc|View)"

    invoke-direct {v1, p0, v2, v3}, LUZ;-><init>(LUU;LVc;Ljava/lang/String;)V

    .line 59
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUL;

    sget-object v2, LVc;->e:LVc;

    const-string v3, "/(present|presentation)/(view|edit)"

    const-string v4, "id"

    invoke-direct {v1, v2, v3, v4}, LUL;-><init>(LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUC;

    sget-object v2, LVc;->e:LVc;

    const-string v3, "/presentation/d/([^/]*).*"

    invoke-direct {v1, v2, v3, v5}, LUC;-><init>(LVc;Ljava/lang/String;I)V

    .line 89
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUL;

    sget-object v2, LVc;->b:LVc;

    const-string v3, "/drawings/(view|edit)"

    const-string v4, "id"

    invoke-direct {v1, v2, v3, v4}, LUL;-><init>(LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    iget-object v1, p0, LUU;->a:Ljava/util/List;

    .line 99
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    new-instance v1, LUC;

    sget-object v2, LVc;->f:LVc;

    sget-object v3, LUN;->a:Ljava/util/regex/Pattern;

    invoke-direct {v1, v2, v3, v5}, LUC;-><init>(LVc;Ljava/util/regex/Pattern;I)V

    .line 103
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUY;

    sget-object v2, LVc;->d:LVc;

    const-string v3, "/forms/d/([^/]*).*"

    invoke-direct {v1, p0, v2, v3, v5}, LUY;-><init>(LUU;LVc;Ljava/lang/String;I)V

    .line 106
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUL;

    sget-object v2, LVc;->g:LVc;

    const-string v3, "/folderview$"

    const-string v4, "id"

    invoke-direct {v1, v2, v3, v4}, LUL;-><init>(LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUC;

    sget-object v2, LVc;->g:LVc;

    const-string v3, "/folder/d/([^/]*).*"

    invoke-direct {v1, v2, v3, v5}, LUC;-><init>(LVc;Ljava/lang/String;I)V

    .line 132
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUC;

    sget-object v2, LVc;->g:LVc;

    const-string v3, "(?:/u/\\d+)?/folders/(?:.*/)*(.*)"

    invoke-direct {v1, v2, v3, v5}, LUC;-><init>(LVc;Ljava/lang/String;I)V

    .line 137
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUX;

    sget-object v2, LVc;->g:LVc;

    const-string v3, "^folders/([^/]+)"

    invoke-direct {v1, p0, v2, v3, v5}, LUX;-><init>(LUU;LVc;Ljava/lang/String;I)V

    .line 142
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUC;

    sget-object v2, LVc;->c:LVc;

    const-string v3, "/file/d/([^/]*).*"

    invoke-direct {v1, v2, v3, v5}, LUC;-><init>(LVc;Ljava/lang/String;I)V

    .line 156
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUL;

    sget-object v2, LVc;->i:LVc;

    const-string v3, "/(leaf|uc)"

    const-string v4, "id"

    invoke-direct {v1, v2, v3, v4}, LUL;-><init>(LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUL;

    sget-object v2, LVc;->j:LVc;

    const-string v3, "/open"

    const-string v4, "id"

    invoke-direct {v1, v2, v3, v4}, LUL;-><init>(LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUW;

    sget-object v2, LVc;->k:LVc;

    const-string v3, "/viewer"

    const-string v4, "srcid"

    invoke-direct {v1, p0, v2, v3, v4}, LUW;-><init>(LUU;LVc;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUV;

    sget-object v2, LVc;->h:LVc;

    const-string v3, "/(m?|starred|recent|incoming|my-drive|trash)"

    invoke-direct {v1, p0, v2, v3}, LUV;-><init>(LUU;LVc;Ljava/lang/String;)V

    .line 187
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LUU;->b:Ljava/util/List;

    .line 196
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;)LVa;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 206
    const-string v0, "UrlParserImpl"

    const-string v1, "Opening %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p2, v2, v6

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 208
    invoke-static {p2}, LFO;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 211
    if-eqz v2, :cond_3

    .line 213
    invoke-static {p2}, LFO;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LUU;->b:Ljava/util/List;

    .line 217
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVb;

    .line 218
    invoke-virtual {v0, p2, v2}, LVb;->a(Landroid/net/Uri;Ljava/lang/String;)LVa;

    move-result-object v1

    .line 219
    if-eqz v1, :cond_0

    .line 220
    const-string v2, "UrlParserImpl"

    const-string v3, "Got docs ID %s matcher={%s}"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, LVa;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    .line 227
    :goto_1
    return-object v0

    .line 214
    :cond_1
    invoke-static {p2}, LFO;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LUU;->a:Ljava/util/List;

    goto :goto_0

    .line 215
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_3
    const-string v0, "UrlParserImpl"

    const-string v1, "parse Falling back to UrlType.UNDETERMINED"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 227
    new-instance v0, LVa;

    const/4 v1, 0x0

    sget-object v2, LVc;->l:LVc;

    invoke-direct {v0, v1, v2, p2}, LVa;-><init>(Ljava/lang/String;LVc;Landroid/net/Uri;)V

    goto :goto_1
.end method
