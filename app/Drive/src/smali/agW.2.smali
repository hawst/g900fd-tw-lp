.class public LagW;
.super Ljava/lang/Object;
.source "PinnedItemSyncUtil.java"


# instance fields
.field private final a:LaGg;

.field private final a:Ladi;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Lahf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaGg;Ladi;Laja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGg;",
            "Ladi;",
            "Laja",
            "<",
            "Lahf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, LagW;->a:LaGg;

    .line 31
    iput-object p2, p0, LagW;->a:Ladi;

    .line 32
    iput-object p3, p0, LagW;->a:Laja;

    .line 33
    return-void
.end method

.method private a(LaFM;)J
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v0

    .line 103
    iget-object v1, p0, LagW;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, LaFQ;->a()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a(LaGd;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-virtual {p1}, LaGd;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    invoke-virtual {p1}, LaGd;->i()Z

    move-result v2

    if-nez v2, :cond_0

    .line 68
    invoke-virtual {p1}, LaGd;->a()LaFM;

    move-result-object v2

    invoke-direct {p0, v2}, LagW;->a(LaFM;)J

    move-result-wide v2

    .line 69
    invoke-virtual {p1}, LaGd;->d()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public a(LaGo;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, LagW;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    invoke-interface {v0, p1}, Lahf;->a(LaGo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, LagW;->a:Ladi;

    sget-object v2, LacY;->a:LacY;

    invoke-interface {v0, p1, v2}, Ladi;->a(LaGo;LacY;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 39
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, LagW;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 42
    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, LagW;->a(LaGd;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 76
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, LagW;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 79
    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 84
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, LagW;->a(LaGo;)Z

    move-result v0

    goto :goto_0
.end method
