.class public final LaOc;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lcom/google/android/gms/internal/an;)LnA;
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/internal/an;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    new-instance v3, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/internal/an;->a:Ljava/util/List;

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    new-instance v0, LnA;

    new-instance v1, Ljava/util/Date;

    iget-wide v4, p0, Lcom/google/android/gms/internal/an;->a:J

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    iget v2, p0, Lcom/google/android/gms/internal/an;->b:I

    invoke-static {v2}, LaOc;->a(I)Lny;

    move-result-object v2

    iget-boolean v4, p0, Lcom/google/android/gms/internal/an;->a:Z

    iget-object v5, p0, Lcom/google/android/gms/internal/an;->a:Landroid/location/Location;

    invoke-direct/range {v0 .. v5}, LnA;-><init>(Ljava/util/Date;Lny;Ljava/util/Set;ZLandroid/location/Location;)V

    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static a(I)Lny;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lny;->a:Lny;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lny;->c:Lny;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lny;->b:Lny;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/internal/aq;)Lnz;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x6

    new-array v1, v1, [Lnz;

    sget-object v2, Lnz;->a:Lnz;

    aput-object v2, v1, v0

    const/4 v2, 0x1

    sget-object v3, Lnz;->b:Lnz;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lnz;->c:Lnz;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lnz;->d:Lnz;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lnz;->e:Lnz;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lnz;->f:Lnz;

    aput-object v3, v1, v2

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2}, Lnz;->a()I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/internal/aq;->d:I

    if-ne v2, v3, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Lnz;->b()I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/internal/aq;->b:I

    if-ne v2, v3, :cond_0

    aget-object v0, v1, v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lnz;

    iget v1, p0, Lcom/google/android/gms/internal/aq;->d:I

    iget v2, p0, Lcom/google/android/gms/internal/aq;->b:I

    iget-object v3, p0, Lcom/google/android/gms/internal/aq;->a:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Layy;->a(IILjava/lang/String;)Layw;

    move-result-object v1

    invoke-direct {v0, v1}, Lnz;-><init>(Layw;)V

    goto :goto_1
.end method
