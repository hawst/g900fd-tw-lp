.class public final enum LaEh;
.super Ljava/lang/Enum;
.source "AclTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEh;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEh;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic a:[LaEh;

.field public static final enum b:LaEh;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:LaEh;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum d:LaEh;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x2a

    const/16 v7, 0xe

    .line 53
    new-instance v0, LaEh;

    const-string v1, "RESOURCE_ID"

    .line 54
    invoke-static {}, LaEg;->b()LaEg;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "resourceId"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 55
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-virtual {v2, v8}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEh;->a:LaEh;

    .line 57
    new-instance v0, LaEh;

    const-string v1, "ACCOUNT_NAME"

    .line 58
    invoke-static {}, LaEg;->b()LaEg;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "accountName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 59
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-virtual {v2, v8}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEh;->b:LaEh;

    .line 61
    new-instance v0, LaEh;

    const-string v1, "ROLE"

    .line 62
    invoke-static {}, LaEg;->b()LaEg;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "role"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 63
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-virtual {v2, v8}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEh;->c:LaEh;

    .line 65
    new-instance v0, LaEh;

    const-string v1, "SCOPE"

    const/4 v2, 0x3

    .line 66
    invoke-static {}, LaEg;->b()LaEg;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "scope"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 67
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-virtual {v3, v8}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEh;->d:LaEh;

    .line 52
    const/4 v0, 0x4

    new-array v0, v0, [LaEh;

    sget-object v1, LaEh;->a:LaEh;

    aput-object v1, v0, v9

    sget-object v1, LaEh;->b:LaEh;

    aput-object v1, v0, v10

    sget-object v1, LaEh;->c:LaEh;

    aput-object v1, v0, v11

    const/4 v1, 0x3

    sget-object v2, LaEh;->d:LaEh;

    aput-object v2, v0, v1

    sput-object v0, LaEh;->a:[LaEh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 73
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEh;->a:LaFr;

    .line 74
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEh;
    .locals 1

    .prologue
    .line 52
    const-class v0, LaEh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEh;

    return-object v0
.end method

.method public static values()[LaEh;
    .locals 1

    .prologue
    .line 52
    sget-object v0, LaEh;->a:[LaEh;

    invoke-virtual {v0}, [LaEh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEh;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, LaEh;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, LaEh;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
