.class public Lacb;
.super Ljava/lang/Object;
.source "SelectionDialogHelper.java"


# instance fields
.field private a:I

.field private a:Landroid/widget/AdapterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/AdapterView",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lacb;->a:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 55
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    goto :goto_0
.end method

.method public a(Lacd;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lacc;

    invoke-direct {v0, p0, p1}, Lacc;-><init>(Lacb;Lacd;)V

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getCount()I

    move-result v0

    .line 94
    iget v1, p0, Lacb;->a:I

    invoke-static {v1, v0}, LbiT;->b(II)I

    .line 96
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    check-cast v0, Landroid/widget/ListView;

    iget v1, p0, Lacb;->a:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lacb;->a:Landroid/widget/AdapterView;

    iget v1, p0, Lacb;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 46
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 47
    iput p1, p0, Lacb;->a:I

    .line 48
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 81
    if-eqz p1, :cond_0

    .line 82
    const-string v0, "selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lacb;->a:I

    .line 84
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 74
    iget v0, p0, Lacb;->a:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The initial selection state has not been set yet."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 77
    iput-object p1, p0, Lacb;->a:Landroid/widget/AdapterView;

    .line 78
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lacb;->a()I

    move-result v0

    iput v0, p0, Lacb;->a:I

    .line 88
    const-string v0, "selection"

    iget v1, p0, Lacb;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 89
    return-void
.end method
