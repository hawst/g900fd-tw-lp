.class public final LaJW;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaJV;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaJZ;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaJR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LaJW;->a:LbrA;

    .line 41
    const-class v0, LaJV;

    invoke-static {v0, v1}, LaJW;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaJW;->a:Lbsk;

    .line 44
    const-class v0, LaJZ;

    invoke-static {v0, v1}, LaJW;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaJW;->b:Lbsk;

    .line 47
    const-class v0, LaJR;

    invoke-static {v0, v1}, LaJW;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaJW;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :pswitch_0
    new-instance v1, LaJV;

    iget-object v0, p0, LaJW;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaJW;

    iget-object v0, v0, LaJW;->b:Lbsk;

    .line 79
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaJW;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaJW;

    iget-object v2, v2, LaJW;->b:Lbsk;

    .line 77
    invoke-static {v0, v2}, LaJW;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJZ;

    invoke-direct {v1, v0}, LaJV;-><init>(LaJZ;)V

    .line 84
    return-object v1

    .line 74
    :pswitch_data_0
    .packed-switch 0xa1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 101
    packed-switch p2, :pswitch_data_0

    .line 121
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :pswitch_1
    check-cast p1, LaJX;

    .line 105
    iget-object v0, p0, LaJW;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaKj;

    iget-object v0, v0, LaKj;->c:Lbsk;

    .line 108
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKg;

    .line 105
    invoke-virtual {p1, v0}, LaJX;->provideRemoteResourceAccessorFactory(LaKg;)LaJZ;

    move-result-object v0

    .line 114
    :goto_0
    return-object v0

    .line 112
    :pswitch_2
    check-cast p1, LaJX;

    .line 114
    iget-object v0, p0, LaJW;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaKj;

    iget-object v0, v0, LaKj;->b:Lbsk;

    .line 117
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKd;

    .line 114
    invoke-virtual {p1, v0}, LaJX;->provideAccountMetadataEntryFactory(LaKd;)LaJR;

    move-result-object v0

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x9f
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 57
    const-class v0, LaJV;

    iget-object v1, p0, LaJW;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaJW;->a(Ljava/lang/Class;Lbsk;)V

    .line 58
    const-class v0, LaJZ;

    iget-object v1, p0, LaJW;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaJW;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, LaJR;

    iget-object v1, p0, LaJW;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaJW;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    iget-object v0, p0, LaJW;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xa1

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 62
    iget-object v0, p0, LaJW;->b:Lbsk;

    const-class v1, LaJX;

    const/16 v2, 0x9f

    invoke-virtual {p0, v1, v2}, LaJW;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 64
    iget-object v0, p0, LaJW;->c:Lbsk;

    const-class v1, LaJX;

    const/16 v2, 0xa2

    invoke-virtual {p0, v1, v2}, LaJW;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 66
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 93
    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
