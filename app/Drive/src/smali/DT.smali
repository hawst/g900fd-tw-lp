.class public LDT;
.super Ljava/lang/Object;
.source "SyncViewState.java"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/ImageButton;

.field private final a:Landroid/widget/ProgressBar;

.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/view/View;

.field private final b:Landroid/widget/ImageButton;

.field private final c:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LDT;->a:Landroid/content/res/Resources;

    .line 112
    sget v0, Lxc;->cancel_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LDT;->a:Landroid/widget/ImageButton;

    .line 113
    sget v0, Lxc;->resume_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LDT;->b:Landroid/widget/ImageButton;

    .line 114
    sget v0, Lxc;->pause_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LDT;->c:Landroid/widget/ImageButton;

    .line 115
    sget v0, Lxc;->offline_status:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LDT;->a:Landroid/widget/TextView;

    .line 116
    sget v0, Lxc;->progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LDT;->a:Landroid/widget/ProgressBar;

    .line 117
    sget v0, Lxc;->doc_entry_root:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LDT;->a:Landroid/view/View;

    .line 118
    sget v0, Lxc;->sync_state_background:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LDT;->b:Landroid/view/View;

    .line 119
    return-void
.end method

.method static synthetic a(LDT;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->a:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic a(LDT;)Landroid/view/View;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(LDT;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->a:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic a(LDT;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->a:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic a(LDT;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(LDT;)Landroid/view/View;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(LDT;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->b:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic c(LDT;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LDT;->c:Landroid/widget/ImageButton;

    return-object v0
.end method
