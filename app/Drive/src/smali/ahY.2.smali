.class public LahY;
.super Ljava/lang/Object;
.source "DocumentAttachedBinaryFileDownloader.java"

# interfaces
.implements LagG;


# instance fields
.field private final a:LaGM;

.field private final a:Ladi;

.field private final a:LahU;

.field private final a:Laie;

.field private final a:LamL;


# direct methods
.method public constructor <init>(LaGM;Ladi;LamL;Laie;LahU;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, LahY;->a:LaGM;

    .line 55
    iput-object p2, p0, LahY;->a:Ladi;

    .line 56
    iput-object p3, p0, LahY;->a:LamL;

    .line 57
    iput-object p4, p0, LahY;->a:Laie;

    .line 58
    iput-object p5, p0, LahY;->a:LahU;

    .line 59
    return-void
.end method

.method static synthetic a(LahY;)Ladi;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, LahY;->a:Ladi;

    return-object v0
.end method

.method static synthetic a(LahY;)Laie;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, LahY;->a:Laie;

    return-object v0
.end method

.method static synthetic a(LahY;)LamL;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, LahY;->a:LamL;

    return-object v0
.end method

.method private a(LaGo;LacY;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 138
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 142
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, LacY;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144
    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;LacY;LagH;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 64
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :try_start_0
    iget-object v0, p0, LahY;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGo;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 77
    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    const-string v1, "Failed to get document for %s: %s"

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 81
    :cond_1
    new-instance v5, LahZ;

    invoke-direct {v5, p0, v0, p4}, LahZ;-><init>(LahY;LaGo;LacY;)V

    .line 98
    new-instance v6, Laia;

    invoke-direct {v6, p0, v0}, Laia;-><init>(LahY;LaGo;)V

    .line 109
    invoke-direct {p0, v0, p4}, LahY;->a(LaGo;LacY;)Ljava/lang/String;

    move-result-object v4

    .line 111
    iget-object v0, p0, LahY;->a:LahU;

    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    move-object v2, p2

    move-object v3, p3

    move-object v7, p5

    .line 112
    invoke-virtual/range {v0 .. v7}, LahU;->a(LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LahW;LahV;LagH;)Laif;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0}, Laif;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    invoke-interface {v0}, Laif;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 123
    invoke-interface {v0}, Laif;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 128
    :catch_1
    move-exception v0

    .line 129
    const-string v1, "BinaryFileSynchronizer"

    const-string v2, "Error on syncDown: %s"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 125
    :cond_2
    :try_start_2
    iget-object v1, p0, LahY;->a:Laie;

    invoke-interface {v1, v4, v0}, Laie;->a(Ljava/lang/String;Laif;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method
