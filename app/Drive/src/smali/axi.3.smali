.class Laxi;
.super Ljava/lang/Object;
.source "PdfViewer.java"

# interfaces
.implements LaxH;


# instance fields
.field final synthetic a:LawZ;

.field final a:Ljava/lang/String;


# direct methods
.method constructor <init>(LawZ;)V
    .locals 1

    .prologue
    .line 690
    iput-object p1, p0, Laxi;->a:LawZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 692
    const-string v0, "password-dialog"

    iput-object v0, p0, Laxi;->a:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/app/FragmentManager;)LawS;
    .locals 1

    .prologue
    .line 695
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "password-dialog"

    invoke-virtual {p1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, LawS;

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Laxi;->a(Landroid/app/FragmentManager;)LawS;

    move-result-object v0

    .line 741
    if-eqz v0, :cond_0

    .line 742
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 744
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 730
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_0

    .line 731
    invoke-direct {p0}, Laxi;->b()V

    .line 732
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Laxi;->a:LawZ;

    sget v2, Laum;->error_file_format_pdf:I

    .line 733
    invoke-virtual {v1, v2}, LawZ;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 732
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 733
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 734
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 735
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "status"

    const-string v2, "broken"

    invoke-virtual {v0, v1, v2}, LavT;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_0

    .line 721
    invoke-direct {p0}, Laxi;->b()V

    .line 722
    iget-object v0, p0, Laxi;->a:LawZ;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LawZ;->a(LawZ;I)V

    .line 723
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0, p1}, LawZ;->b(LawZ;I)V

    .line 724
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "pages"

    invoke-virtual {v0, v1, p1}, LavT;->b(Ljava/lang/String;I)V

    .line 726
    :cond_0
    return-void
.end method

.method public a(ILandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_0

    .line 781
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0, p1}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setPageBitmap(Landroid/graphics/Bitmap;)V

    .line 783
    :cond_0
    return-void
.end method

.method public a(ILawr;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 773
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_0

    .line 774
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0, p1}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setTileBitmap(Lawr;Landroid/graphics/Bitmap;)V

    .line 776
    :cond_0
    return-void
.end method

.method public a(ILcom/google/android/apps/viewer/client/Dimensions;)V
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_1

    .line 749
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0, p1, p2}, LawZ;->a(LawZ;ILcom/google/android/apps/viewer/client/Dimensions;)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    .line 751
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Lawh;

    move-result-object v0

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->b:LawA;

    if-ne v0, v1, :cond_0

    .line 752
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->setVisibility(I)V

    .line 753
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Lawh;

    move-result-object v0

    sget-object v1, LawA;->c:LawA;

    invoke-virtual {v0, v1}, Lawh;->b(Ljava/lang/Object;)V

    .line 756
    :cond_0
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0}, Laxn;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laxi;->a:LawZ;

    .line 757
    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0}, Laxn;->b()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laxi;->a:LawZ;

    .line 758
    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0}, Laxn;->b()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxo;

    iget v0, v0, Laxo;->a:I

    if-ne v0, p1, :cond_1

    .line 759
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 761
    new-instance v0, Laxj;

    invoke-direct {v0, p0}, Laxj;-><init>(Laxi;)V

    invoke-static {v0}, Lawm;->a(Ljava/lang/Runnable;)V

    .line 769
    :cond_1
    return-void
.end method

.method public a(ILcom/google/android/apps/viewer/pdflib/Selection;)V
    .locals 2

    .prologue
    .line 816
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_1

    .line 817
    if-eqz p2, :cond_0

    .line 818
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laxn;->a(Ljava/lang/String;)V

    .line 820
    :cond_0
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxw;

    move-result-object v0

    invoke-virtual {v0, p2}, Laxw;->a(Lcom/google/android/apps/viewer/pdflib/Selection;)V

    .line 822
    :cond_1
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 787
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_0

    .line 788
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0, p1}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setPageText(Ljava/lang/String;)V

    .line 790
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;)V
    .locals 2

    .prologue
    .line 794
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0}, Laxn;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Laxn;->a(ILcom/google/android/apps/viewer/pdflib/MatchRects;)V

    .line 796
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Laxl;

    move-result-object v0

    invoke-virtual {v0, p2}, Laxl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 797
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0, p2}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    iget-object v1, p0, Laxi;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)Laxn;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Laxn;->a(ILcom/google/android/apps/viewer/pdflib/MatchRects;)LawI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 800
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V
    .locals 2

    .prologue
    .line 805
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0}, Laxn;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 806
    sget-object v0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->NO_MATCHES:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    if-eq p3, v0, :cond_1

    .line 807
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Laxn;->a(ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    .line 812
    :cond_0
    :goto_0
    return-void

    .line 809
    :cond_1
    iget-object v0, p0, Laxi;->a:LawZ;

    sget v1, Laum;->message_no_matches_found:I

    invoke-static {v0, v1}, LawZ;->c(LawZ;I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 701
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LawA;->a:LawA;

    if-eq v0, v1, :cond_1

    .line 702
    iget-object v0, p0, Laxi;->a:LawZ;

    invoke-virtual {v0}, LawZ;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 704
    invoke-direct {p0, v1}, Laxi;->a(Landroid/app/FragmentManager;)LawS;

    move-result-object v0

    .line 705
    if-nez v0, :cond_0

    .line 706
    new-instance v0, LawS;

    invoke-direct {v0}, LawS;-><init>()V

    .line 707
    iget-object v2, p0, Laxi;->a:LawZ;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LawS;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 708
    const-string v2, "password-dialog"

    invoke-virtual {v0, v1, v2}, LawS;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 710
    :cond_0
    if-eqz p1, :cond_2

    .line 711
    invoke-virtual {v0}, LawS;->a()V

    .line 716
    :cond_1
    :goto_0
    return-void

    .line 713
    :cond_2
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "status"

    const-string v2, "password-protected"

    invoke-virtual {v0, v1, v2}, LavT;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
