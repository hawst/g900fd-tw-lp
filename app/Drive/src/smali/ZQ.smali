.class public final LZQ;
.super Ljava/lang/Object;
.source "ExponentialBackoffWaitingRateLimiter.java"

# interfaces
.implements LZS;


# instance fields
.field private final a:D

.field private a:I

.field private final a:J

.field private final a:Ljava/util/Random;

.field private final b:J


# direct methods
.method public constructor <init>(JD)V
    .locals 9

    .prologue
    .line 30
    const-wide v6, 0x7fffffffffffffffL

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v7}, LZQ;-><init>(JDJ)V

    .line 31
    return-void
.end method

.method public constructor <init>(JDJ)V
    .locals 9

    .prologue
    .line 45
    new-instance v8, Ljava/util/Random;

    invoke-direct {v8}, Ljava/util/Random;-><init>()V

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, LZQ;-><init>(JDJLjava/util/Random;)V

    .line 46
    return-void
.end method

.method constructor <init>(JDJLjava/util/Random;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput v2, p0, LZQ;->a:I

    .line 51
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 52
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p3, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 53
    cmp-long v0, p1, p5

    if-gtz v0, :cond_2

    :goto_2
    invoke-static {v1}, LbiT;->a(Z)V

    .line 54
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iput-wide p1, p0, LZQ;->a:J

    .line 56
    iput-wide p3, p0, LZQ;->a:D

    .line 57
    iput-wide p5, p0, LZQ;->b:J

    .line 58
    iput-object p7, p0, LZQ;->a:Ljava/util/Random;

    .line 59
    return-void

    :cond_0
    move v0, v2

    .line 51
    goto :goto_0

    :cond_1
    move v0, v2

    .line 52
    goto :goto_1

    :cond_2
    move v1, v2

    .line 53
    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized a()I
    .locals 1

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    iget v0, p0, LZQ;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a()J
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 76
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LZQ;->a:D

    sub-double/2addr v0, v4

    iget-object v2, p0, LZQ;->a:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    .line 77
    iget-wide v2, p0, LZQ;->a:D

    iget v4, p0, LZQ;->a:I

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    .line 78
    iget-wide v2, p0, LZQ;->a:J

    long-to-double v2, v2

    mul-double/2addr v0, v2

    .line 79
    iget v2, p0, LZQ;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LZQ;->a:I

    .line 80
    iget-wide v2, p0, LZQ;->b:J

    long-to-double v2, v2

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 81
    iget-wide v0, p0, LZQ;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    double-to-long v0, v0

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 2

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LZQ;->a()J

    move-result-wide v0

    .line 93
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 1

    .prologue
    .line 98
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, LZQ;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 111
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ExponentialBackoffRateLimiter[%d tokens, initialMs=%d, factor=%.3f]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LZQ;->a:I

    .line 112
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, LZQ;->a:J

    .line 113
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, LZQ;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    .line 111
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
