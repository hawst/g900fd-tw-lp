.class public Ljo;
.super Ljava/lang/Object;
.source "MenuPopupHelper.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Ljq;


# static fields
.field static final a:I


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/LayoutInflater;

.field private a:Landroid/view/View;

.field private a:Landroid/view/ViewGroup;

.field private a:Landroid/view/ViewTreeObserver;

.field private final a:Ljb;

.field private final a:Ljp;

.field private a:Ljr;

.field private a:LmK;

.field a:Z

.field private final b:I

.field private final b:Z

.field private final c:I

.field private c:Z

.field private d:I

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    sget v0, Lix;->abc_popup_menu_item_layout:I

    sput v0, Ljo;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljb;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 83
    const/4 v4, 0x0

    sget v5, Liq;->popupMenuStyle:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Ljo;-><init>(Landroid/content/Context;Ljb;Landroid/view/View;ZI)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljb;Landroid/view/View;ZI)V
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Ljo;->e:I

    .line 88
    iput-object p1, p0, Ljo;->a:Landroid/content/Context;

    .line 89
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Ljo;->a:Landroid/view/LayoutInflater;

    .line 90
    iput-object p2, p0, Ljo;->a:Ljb;

    .line 91
    new-instance v0, Ljp;

    iget-object v1, p0, Ljo;->a:Ljb;

    invoke-direct {v0, p0, v1}, Ljp;-><init>(Ljo;Ljb;)V

    iput-object v0, p0, Ljo;->a:Ljp;

    .line 92
    iput-boolean p4, p0, Ljo;->b:Z

    .line 93
    iput p5, p0, Ljo;->c:I

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Lit;->abc_config_prefDialogWidth:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ljo;->b:I

    .line 99
    iput-object p3, p0, Ljo;->a:Landroid/view/View;

    .line 102
    invoke-virtual {p2, p0, p1}, Ljb;->a(Ljq;Landroid/content/Context;)V

    .line 103
    return-void
.end method

.method private a()I
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 193
    .line 197
    iget-object v6, p0, Ljo;->a:Ljp;

    .line 198
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 199
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 200
    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v9

    move v5, v0

    move v2, v0

    move-object v4, v3

    move v1, v0

    .line 201
    :goto_0
    if-ge v5, v9, :cond_1

    .line 202
    invoke-interface {v6, v5}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 203
    if-eq v0, v2, :cond_3

    move v2, v0

    move-object v0, v3

    .line 208
    :goto_1
    iget-object v4, p0, Ljo;->a:Landroid/view/ViewGroup;

    if-nez v4, :cond_0

    .line 209
    new-instance v4, Landroid/widget/FrameLayout;

    iget-object v10, p0, Ljo;->a:Landroid/content/Context;

    invoke-direct {v4, v10}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Ljo;->a:Landroid/view/ViewGroup;

    .line 212
    :cond_0
    iget-object v4, p0, Ljo;->a:Landroid/view/ViewGroup;

    invoke-interface {v6, v5, v0, v4}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 213
    invoke-virtual {v4, v7, v8}, Landroid/view/View;->measure(II)V

    .line 215
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 216
    iget v10, p0, Ljo;->b:I

    if-lt v0, v10, :cond_2

    .line 217
    iget v1, p0, Ljo;->b:I

    .line 223
    :cond_1
    return v1

    .line 218
    :cond_2
    if-le v0, v1, :cond_4

    .line 201
    :goto_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_0

    :cond_3
    move-object v0, v4

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method static synthetic a(Ljo;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ljo;->a:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic a(Ljo;)Ljb;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ljo;->a:Ljb;

    return-object v0
.end method

.method static synthetic a(Ljo;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Ljo;->b:Z

    return v0
.end method


# virtual methods
.method public a()LmK;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Ljo;->a:LmK;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Ljo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MenuPopupHelper cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 114
    iput p1, p0, Ljo;->e:I

    .line 115
    return-void
.end method

.method public a(Landroid/content/Context;Ljb;)V
    .locals 0

    .prologue
    .line 242
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Ljo;->a:Landroid/view/View;

    .line 107
    return-void
.end method

.method public a(Ljb;Z)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Ljo;->a:Ljb;

    if-eq p1, v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    invoke-virtual {p0}, Ljo;->b()V

    .line 296
    iget-object v0, p0, Ljo;->a:Ljr;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Ljo;->a:Ljr;

    invoke-interface {v0, p1, p2}, Ljr;->a(Ljb;Z)V

    goto :goto_0
.end method

.method public a(Ljr;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Ljo;->a:Ljr;

    .line 261
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Ljo;->a:Z

    .line 111
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljb;Ljf;)Z
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljw;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 265
    invoke-virtual {p1}, Ljw;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    new-instance v3, Ljo;

    iget-object v0, p0, Ljo;->a:Landroid/content/Context;

    iget-object v4, p0, Ljo;->a:Landroid/view/View;

    invoke-direct {v3, v0, p1, v4}, Ljo;-><init>(Landroid/content/Context;Ljb;Landroid/view/View;)V

    .line 267
    iget-object v0, p0, Ljo;->a:Ljr;

    invoke-virtual {v3, v0}, Ljo;->a(Ljr;)V

    .line 270
    invoke-virtual {p1}, Ljw;->size()I

    move-result v4

    move v0, v2

    .line 271
    :goto_0
    if-ge v0, v4, :cond_3

    .line 272
    invoke-virtual {p1, v0}, Ljw;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 273
    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_1

    move v0, v1

    .line 278
    :goto_1
    invoke-virtual {v3, v0}, Ljo;->a(Z)V

    .line 280
    invoke-virtual {v3}, Ljo;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    iget-object v0, p0, Ljo;->a:Ljr;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Ljo;->a:Ljr;

    invoke-interface {v0, p1}, Ljr;->a(Ljb;)Z

    .line 287
    :cond_0
    :goto_2
    return v1

    .line 271
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 287
    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Ljo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Ljo;->a:LmK;

    invoke-virtual {v0}, LmK;->a()V

    .line 161
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljo;->c:Z

    .line 253
    iget-object v0, p0, Ljo;->a:Ljp;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Ljo;->a:Ljp;

    invoke-virtual {v0}, Ljp;->notifyDataSetChanged()V

    .line 256
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 128
    new-instance v2, LmK;

    iget-object v3, p0, Ljo;->a:Landroid/content/Context;

    const/4 v4, 0x0

    iget v5, p0, Ljo;->c:I

    invoke-direct {v2, v3, v4, v5}, LmK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Ljo;->a:LmK;

    .line 129
    iget-object v2, p0, Ljo;->a:LmK;

    invoke-virtual {v2, p0}, LmK;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 130
    iget-object v2, p0, Ljo;->a:LmK;

    invoke-virtual {v2, p0}, LmK;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 131
    iget-object v2, p0, Ljo;->a:LmK;

    iget-object v3, p0, Ljo;->a:Ljp;

    invoke-virtual {v2, v3}, LmK;->a(Landroid/widget/ListAdapter;)V

    .line 132
    iget-object v2, p0, Ljo;->a:LmK;

    invoke-virtual {v2, v1}, LmK;->a(Z)V

    .line 134
    iget-object v2, p0, Ljo;->a:Landroid/view/View;

    .line 135
    if-eqz v2, :cond_3

    .line 136
    iget-object v3, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    if-nez v3, :cond_0

    move v0, v1

    .line 137
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    iput-object v3, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    .line 138
    if-eqz v0, :cond_1

    iget-object v0, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 139
    :cond_1
    iget-object v0, p0, Ljo;->a:LmK;

    invoke-virtual {v0, v2}, LmK;->a(Landroid/view/View;)V

    .line 140
    iget-object v0, p0, Ljo;->a:LmK;

    iget v2, p0, Ljo;->e:I

    invoke-virtual {v0, v2}, LmK;->b(I)V

    .line 145
    iget-boolean v0, p0, Ljo;->c:Z

    if-nez v0, :cond_2

    .line 146
    invoke-direct {p0}, Ljo;->a()I

    move-result v0

    iput v0, p0, Ljo;->d:I

    .line 147
    iput-boolean v1, p0, Ljo;->c:Z

    .line 150
    :cond_2
    iget-object v0, p0, Ljo;->a:LmK;

    iget v2, p0, Ljo;->d:I

    invoke-virtual {v0, v2}, LmK;->d(I)V

    .line 151
    iget-object v0, p0, Ljo;->a:LmK;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, LmK;->e(I)V

    .line 152
    iget-object v0, p0, Ljo;->a:LmK;

    invoke-virtual {v0}, LmK;->b()V

    .line 153
    iget-object v0, p0, Ljo;->a:LmK;

    invoke-virtual {v0}, LmK;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 154
    :goto_0
    return v1

    :cond_3
    move v1, v0

    .line 142
    goto :goto_0
.end method

.method public b(Ljb;Ljf;)Z
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Ljo;->a:LmK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljo;->a:LmK;

    invoke-virtual {v0}, LmK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 164
    iput-object v1, p0, Ljo;->a:LmK;

    .line 165
    iget-object v0, p0, Ljo;->a:Ljb;

    invoke-virtual {v0}, Ljb;->close()V

    .line 166
    iget-object v0, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljo;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    .line 168
    :cond_0
    iget-object v0, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 169
    iput-object v1, p0, Ljo;->a:Landroid/view/ViewTreeObserver;

    .line 171
    :cond_1
    return-void
.end method

.method public onGlobalLayout()V
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Ljo;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Ljo;->a:Landroid/view/View;

    .line 230
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 231
    :cond_0
    invoke-virtual {p0}, Ljo;->b()V

    .line 237
    :cond_1
    :goto_0
    return-void

    .line 232
    :cond_2
    invoke-virtual {p0}, Ljo;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Ljo;->a:LmK;

    invoke-virtual {v0}, LmK;->b()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Ljo;->a:Ljp;

    .line 180
    invoke-static {v0}, Ljp;->a(Ljp;)Ljb;

    move-result-object v1

    invoke-virtual {v0, p3}, Ljp;->a(I)Ljf;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljb;->a(Landroid/view/MenuItem;I)Z

    .line 181
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 184
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    .line 185
    invoke-virtual {p0}, Ljo;->b()V

    .line 188
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
