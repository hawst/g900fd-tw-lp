.class public LaaL;
.super Ljava/lang/Object;
.source "OcrEvaluator.java"

# interfaces
.implements LaaP;


# instance fields
.field private final a:Lamj;

.field private final a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lamj;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, LaaL;->a:Landroid/app/Activity;

    .line 39
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    iput-object v0, p0, LaaL;->a:Lamj;

    .line 40
    return-void
.end method

.method static synthetic a(LaaL;)Lamj;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LaaL;->a:Lamj;

    return-object v0
.end method

.method static synthetic a(LaaL;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LaaL;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, LaaL;->a:Landroid/app/Activity;

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    .line 74
    sget v1, LpR;->camera_ocr_blur_warning:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 75
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, LpR;->camera_ocr_blur_title:I

    .line 76
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x108008a

    .line 77
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, LpR;->camera_ocr_warning_continue:I

    const/4 v3, 0x0

    .line 78
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, LpR;->camera_ocr_warning_retake:I

    new-instance v3, LaaO;

    invoke-direct {v3, p0}, LaaO;-><init>(LaaL;)V

    .line 79
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 95
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 97
    return-void
.end method

.method static synthetic a(LaaL;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, LaaL;->a()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 44
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v0, p0, LaaL;->a:Landroid/app/Activity;

    .line 45
    invoke-static {v0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 46
    iget-object v0, p0, LaaL;->a:Landroid/app/Activity;

    sget v2, LpR;->camera_ocr_evaluating_message:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 47
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 48
    const/4 v2, -0x2

    iget-object v0, p0, LaaL;->a:Landroid/app/Activity;

    sget v3, LpR;->camera_ocr_evaluating_skip_button:I

    .line 49
    invoke-virtual {v0, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    .line 48
    invoke-virtual {v1, v2, v3, v0}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 51
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 55
    new-instance v2, LaaM;

    invoke-direct {v2, p0, p1, v0, v1}, LaaM;-><init>(LaaL;Landroid/net/Uri;Landroid/os/Handler;Landroid/app/ProgressDialog;)V

    .line 69
    invoke-virtual {v2}, LaaM;->start()V

    .line 70
    return-void
.end method
