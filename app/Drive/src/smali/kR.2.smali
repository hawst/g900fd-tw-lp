.class public abstract LkR;
.super Ljava/lang/Object;
.source "MediaRouteProvider.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private a:LkQ;

.field private a:LkT;

.field private final a:LkU;

.field private final a:LkV;

.field private a:LkX;

.field private a:Z

.field private b:Z


# direct methods
.method constructor <init>(Landroid/content/Context;LkV;)V
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, LkU;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LkU;-><init>(LkR;LkS;)V

    iput-object v0, p0, LkR;->a:LkU;

    .line 83
    if-nez p1, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iput-object p1, p0, LkR;->a:Landroid/content/Context;

    .line 88
    if-nez p2, :cond_1

    .line 89
    new-instance v0, LkV;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, LkV;-><init>(Landroid/content/ComponentName;)V

    iput-object v0, p0, LkR;->a:LkV;

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_1
    iput-object p2, p0, LkR;->a:LkV;

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, LkR;->a:Z

    .line 164
    iget-object v0, p0, LkR;->a:LkQ;

    invoke-virtual {p0, v0}, LkR;->b(LkQ;)V

    .line 165
    return-void
.end method

.method static synthetic a(LkR;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, LkR;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, LkR;->b:Z

    .line 236
    iget-object v0, p0, LkR;->a:LkT;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, LkR;->a:LkT;

    iget-object v1, p0, LkR;->a:LkX;

    invoke-virtual {v0, p0, v1}, LkT;->a(LkR;LkX;)V

    .line 239
    :cond_0
    return-void
.end method

.method static synthetic b(LkR;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, LkR;->a()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, LkR;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, LkR;->a:LkU;

    return-object v0
.end method

.method public final a()LkQ;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, LkR;->a:LkQ;

    return-object v0
.end method

.method public final a()LkV;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, LkR;->a:LkV;

    return-object v0
.end method

.method public a(Ljava/lang/String;)LkW;
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()LkX;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, LkR;->a:LkX;

    return-object v0
.end method

.method public final a(LkQ;)V
    .locals 2

    .prologue
    .line 148
    invoke-static {}, Llj;->a()V

    .line 150
    iget-object v0, p0, LkR;->a:LkQ;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LkR;->a:LkQ;

    if-eqz v0, :cond_1

    iget-object v0, p0, LkR;->a:LkQ;

    invoke-virtual {v0, p1}, LkQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iput-object p1, p0, LkR;->a:LkQ;

    .line 156
    iget-boolean v0, p0, LkR;->a:Z

    if-nez v0, :cond_0

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, LkR;->a:Z

    .line 158
    iget-object v0, p0, LkR;->a:LkU;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LkU;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final a(LkT;)V
    .locals 0

    .prologue
    .line 122
    invoke-static {}, Llj;->a()V

    .line 123
    iput-object p1, p0, LkR;->a:LkT;

    .line 124
    return-void
.end method

.method public final a(LkX;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 222
    invoke-static {}, Llj;->a()V

    .line 224
    iget-object v0, p0, LkR;->a:LkX;

    if-eq v0, p1, :cond_0

    .line 225
    iput-object p1, p0, LkR;->a:LkX;

    .line 226
    iget-boolean v0, p0, LkR;->b:Z

    if-nez v0, :cond_0

    .line 227
    iput-boolean v1, p0, LkR;->b:Z

    .line 228
    iget-object v0, p0, LkR;->a:LkU;

    invoke-virtual {v0, v1}, LkU;->sendEmptyMessage(I)Z

    .line 231
    :cond_0
    return-void
.end method

.method public b(LkQ;)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method
