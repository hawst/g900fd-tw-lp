.class public Lavi;
.super Ljava/lang/Object;
.source "MediaTypes.java"


# instance fields
.field public final a:[Ljava/lang/String;

.field public final b:[Ljava/lang/String;

.field public final c:[Ljava/lang/String;

.field public final d:[Ljava/lang/String;

.field public final e:[Ljava/lang/String;

.field public final f:[Ljava/lang/String;

.field public final g:[Ljava/lang/String;

.field public final h:[Ljava/lang/String;

.field public final i:[Ljava/lang/String;

.field public final j:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lavj;->a:Lavj;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "x-compress"

    aput-object v2, v1, v3

    const-string v2, "zip"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->a:[Ljava/lang/String;

    .line 31
    sget-object v0, Lavj;->e:Lavj;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "gif"

    aput-object v2, v1, v3

    const-string v2, "jpeg"

    aput-object v2, v1, v4

    const-string v2, "png"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->b:[Ljava/lang/String;

    .line 33
    sget-object v0, Lavj;->d:Lavj;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "mpeg"

    aput-object v2, v1, v3

    const-string v2, "webm"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->c:[Ljava/lang/String;

    .line 35
    sget-object v0, Lavj;->b:Lavj;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "mpeg"

    aput-object v2, v1, v3

    const-string v2, "ogg"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->d:[Ljava/lang/String;

    .line 37
    sget-object v0, Lavj;->a:Lavj;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "msword"

    aput-object v2, v1, v3

    const-string v2, "vnd.openxmlformats-officedocument.wordprocessingml.document"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->e:[Ljava/lang/String;

    .line 41
    sget-object v0, Lavj;->a:Lavj;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "vnd.ms-excel"

    aput-object v2, v1, v3

    const-string v2, "vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->f:[Ljava/lang/String;

    .line 45
    sget-object v0, Lavj;->a:Lavj;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "pdf"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->g:[Ljava/lang/String;

    .line 47
    sget-object v0, Lavj;->a:Lavj;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "vnd.google-apps.document"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->h:[Ljava/lang/String;

    .line 49
    sget-object v0, Lavj;->a:Lavj;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "vnd.google-apps.presentation"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->i:[Ljava/lang/String;

    .line 52
    sget-object v0, Lavj;->a:Lavj;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "vnd.google-apps.spreadsheet"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lavi;->a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavi;->j:[Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lavj;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 66
    if-nez p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-object v0

    .line 70
    :cond_1
    invoke-static {}, Lavj;->values()[Lavj;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 71
    invoke-static {v1}, Lavj;->a(Lavj;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 72
    goto :goto_0

    .line 70
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method private static varargs a(Lavj;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    array-length v1, p1

    .line 57
    new-array v2, v1, [Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 59
    aget-object v3, p1, v0

    invoke-virtual {p0, v3}, Lavj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_0
    return-object v2
.end method
