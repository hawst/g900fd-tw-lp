.class public final enum LaGn;
.super Ljava/lang/Enum;
.source "DocInfoByMimeType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaGn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaGn;

.field private static final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LaGn;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LaGn;

.field public static final enum b:LaGn;

.field private static final b:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LaGn;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum c:LaGn;

.field public static final enum d:LaGn;

.field public static final enum e:LaGn;

.field public static final enum f:LaGn;

.field public static final enum g:LaGn;


# instance fields
.field private final a:I

.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .prologue
    .line 16
    new-instance v10, LaGn;

    const-string v11, "ZIP"

    const/4 v12, 0x0

    sget v13, Lxb;->ic_type_zip:I

    sget v14, Lxb;->ic_type_zip_alpha:I

    sget v15, Lxb;->ic_type_file_black:I

    sget v16, Lxb;->ic_type_file_black_big:I

    sget v7, Lxi;->document_type_zip_archive:I

    const-string v0, "application/x-compress"

    const-string v1, "application/x-compressed"

    const-string v2, "application/x-gtar"

    const-string v3, "application/x-gzip"

    const-string v4, "application/x-tar"

    const-string v5, "application/zip"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    .line 18
    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v8

    const/4 v9, 0x0

    move-object v0, v10

    move-object v1, v11

    move v2, v12

    move v3, v13

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v0 .. v9}, LaGn;-><init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V

    sput-object v10, LaGn;->a:LaGn;

    .line 24
    new-instance v10, LaGn;

    const-string v11, "IMAGE"

    const/4 v12, 0x1

    sget v13, Lxb;->ic_type_image:I

    sget v14, Lxb;->ic_type_image_alpha:I

    sget v15, Lxb;->ic_type_image_black:I

    sget v16, Lxb;->ic_type_image_black_big:I

    sget v7, Lxi;->document_type_picture:I

    const-string v0, "image/jpeg"

    const-string v1, "image/tiff"

    const-string v2, "image/png"

    const-string v3, "image/cgm"

    const-string v4, "image/fits"

    const-string v5, "image/g3fax"

    const/16 v6, 0x12

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "image/ief"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-string v9, "image/jp2"

    aput-object v9, v6, v8

    const/4 v8, 0x2

    const-string v9, "image/jpm"

    aput-object v9, v6, v8

    const/4 v8, 0x3

    const-string v9, "image/jpx"

    aput-object v9, v6, v8

    const/4 v8, 0x4

    const-string v9, "image/ktx"

    aput-object v9, v6, v8

    const/4 v8, 0x5

    const-string v9, "image/naplps"

    aput-object v9, v6, v8

    const/4 v8, 0x6

    const-string v9, "image/prs.bitf"

    aput-object v9, v6, v8

    const/4 v8, 0x7

    const-string v9, "image/prs.pti"

    aput-object v9, v6, v8

    const/16 v8, 0x8

    const-string v9, "image/svg+xml"

    aput-object v9, v6, v8

    const/16 v8, 0x9

    const-string v9, "image/tiff-fx"

    aput-object v9, v6, v8

    const/16 v8, 0xa

    const-string v9, "image/vnd.adobe.photoshop"

    aput-object v9, v6, v8

    const/16 v8, 0xb

    const-string v9, "image/vnd.svf"

    aput-object v9, v6, v8

    const/16 v8, 0xc

    const-string v9, "image/vnd.xiff"

    aput-object v9, v6, v8

    const/16 v8, 0xd

    const-string v9, "image/vnd.microsoft.icon"

    aput-object v9, v6, v8

    const/16 v8, 0xe

    const-string v9, "image/x-ms-bmp"

    aput-object v9, v6, v8

    const/16 v8, 0xf

    const-string v9, "image/x-canon-cr2"

    aput-object v9, v6, v8

    const/16 v8, 0x10

    const-string v9, "image/gif"

    aput-object v9, v6, v8

    const/16 v8, 0x11

    const-string v9, "application/vnd.google.panorama360+jpg"

    aput-object v9, v6, v8

    .line 26
    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v8

    const-string v9, "image/"

    move-object v0, v10

    move-object v1, v11

    move v2, v12

    move v3, v13

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v0 .. v9}, LaGn;-><init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V

    sput-object v10, LaGn;->b:LaGn;

    .line 50
    new-instance v10, LaGn;

    const-string v11, "VIDEO"

    const/4 v12, 0x2

    sget v13, Lxb;->ic_type_video:I

    sget v14, Lxb;->ic_type_video_alpha:I

    sget v15, Lxb;->ic_type_video_black:I

    sget v16, Lxb;->ic_type_video_black_big:I

    sget v7, Lxi;->document_type_video:I

    const-string v0, "video/3gpp"

    const-string v1, "video/3gp"

    const-string v2, "video/H261"

    const-string v3, "video/H263"

    const-string v4, "video/H264"

    const-string v5, "video/mp4"

    const/16 v6, 0xd

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "video/mpeg"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-string v9, "video/quicktime"

    aput-object v9, v6, v8

    const/4 v8, 0x2

    const-string v9, "video/raw"

    aput-object v9, v6, v8

    const/4 v8, 0x3

    const-string v9, "video/vnd.motorola.video"

    aput-object v9, v6, v8

    const/4 v8, 0x4

    const-string v9, "video/vnd.motorola.videop"

    aput-object v9, v6, v8

    const/4 v8, 0x5

    const-string v9, "video/webm"

    aput-object v9, v6, v8

    const/4 v8, 0x6

    const-string v9, "video/x-flv"

    aput-object v9, v6, v8

    const/4 v8, 0x7

    const-string v9, "video/x-la-asf"

    aput-object v9, v6, v8

    const/16 v8, 0x8

    const-string v9, "video/x-m4v"

    aput-object v9, v6, v8

    const/16 v8, 0x9

    const-string v9, "video/x-matroska"

    aput-object v9, v6, v8

    const/16 v8, 0xa

    const-string v9, "video/x-ms-asf"

    aput-object v9, v6, v8

    const/16 v8, 0xb

    const-string v9, "video/x-msvideo"

    aput-object v9, v6, v8

    const/16 v8, 0xc

    const-string v9, "video/x-sgi-movie"

    aput-object v9, v6, v8

    .line 52
    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v8

    const-string v9, "video/"

    move-object v0, v10

    move-object v1, v11

    move v2, v12

    move v3, v13

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v0 .. v9}, LaGn;-><init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V

    sput-object v10, LaGn;->c:LaGn;

    .line 71
    new-instance v10, LaGn;

    const-string v11, "MSWORD"

    const/4 v12, 0x3

    sget v13, Lxb;->ic_type_word:I

    sget v14, Lxb;->ic_type_word_alpha:I

    sget v15, Lxb;->ic_type_word_black:I

    sget v16, Lxb;->ic_type_word_black_big:I

    sget v7, Lxi;->document_type_ms_word:I

    const-string v0, "application/msword"

    const-string v1, "application/vnd.ms-word"

    const-string v2, "application/vnd.ms-word.document.macroEnabled.12"

    const-string v3, "application/vnd.ms-word.template.macroEnabled.12"

    const-string v4, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v5, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    .line 73
    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v8

    const/4 v9, 0x0

    move-object v0, v10

    move-object v1, v11

    move v2, v12

    move v3, v13

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v0 .. v9}, LaGn;-><init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V

    sput-object v10, LaGn;->d:LaGn;

    .line 83
    new-instance v10, LaGn;

    const-string v11, "MSEXCEL"

    const/4 v12, 0x4

    sget v13, Lxb;->ic_type_excel:I

    sget v14, Lxb;->ic_type_excel_alpha:I

    sget v15, Lxb;->ic_type_excel_black:I

    sget v16, Lxb;->ic_type_excel_black_big:I

    sget v7, Lxi;->document_type_ms_excel:I

    const-string v0, "application/vnd.ms-excel"

    const-string v1, "application/vnd.ms-excel.addin.macroEnabled.12"

    const-string v2, "application/vnd.ms-excel.sheet.binary.macroEnabled.12"

    const-string v3, "application/vnd.ms-excel.sheet.macroEnabled.12"

    const-string v4, "application/vnd.ms-excel.template.macroEnabled.12"

    const-string v5, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    aput-object v9, v6, v8

    .line 85
    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v8

    const/4 v9, 0x0

    move-object v0, v10

    move-object v1, v11

    move v2, v12

    move v3, v13

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v0 .. v9}, LaGn;-><init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V

    sput-object v10, LaGn;->e:LaGn;

    .line 95
    new-instance v10, LaGn;

    const-string v11, "MSPOWERPOINT"

    const/4 v12, 0x5

    sget v13, Lxb;->ic_type_powerpoint:I

    sget v14, Lxb;->ic_type_powerpoint_alpha:I

    sget v15, Lxb;->ic_type_powerpoint_black:I

    sget v16, Lxb;->ic_type_powerpoint_black_big:I

    sget v7, Lxi;->document_type_ms_powerpoint:I

    const-string v0, "application/vnd.ms-powerpoint"

    const-string v1, "application/vnd.ms-powerpoint.addin.macroEnabled.12"

    const-string v2, "application/vnd.ms-powerpoint.presentation.macroEnabled.12"

    const-string v3, "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"

    const-string v4, "application/vnd.ms-powerpoint.template.macroEnabled.12"

    const-string v5, "application/vnd.openxmlformats-officedocument.presentationml.template"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-string v9, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    aput-object v9, v6, v8

    const/4 v8, 0x2

    const-string v9, "application/vnd.openxmlformats-officedocument.presentationml.slide"

    aput-object v9, v6, v8

    .line 98
    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v8

    const/4 v9, 0x0

    move-object v0, v10

    move-object v1, v11

    move v2, v12

    move v3, v13

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v0 .. v9}, LaGn;-><init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V

    sput-object v10, LaGn;->f:LaGn;

    .line 118
    new-instance v10, LaGn;

    const-string v11, "AUDIO"

    const/4 v12, 0x6

    sget v13, Lxb;->ic_type_audio:I

    sget v14, Lxb;->ic_type_audio_alpha:I

    sget v15, Lxb;->ic_type_file_black:I

    sget v16, Lxb;->ic_type_file_black_big:I

    sget v7, Lxi;->document_type_audio:I

    const-string v0, "audio/annodex"

    const-string v1, "audio/basic"

    const-string v2, "audio/flac"

    const-string v3, "audio/x-flac"

    const-string v4, "audio/mid"

    const-string v5, "audio/mpeg"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "audio/ogg"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-string v9, "audio/x-aiff"

    aput-object v9, v6, v8

    const/4 v8, 0x2

    const-string v9, "audio/x-mpegurl"

    aput-object v9, v6, v8

    const/4 v8, 0x3

    const-string v9, "audio/x-pn-realaudio"

    aput-object v9, v6, v8

    const/4 v8, 0x4

    const-string v9, "audio/wav"

    aput-object v9, v6, v8

    const/4 v8, 0x5

    const-string v9, "audio/x-wav"

    aput-object v9, v6, v8

    .line 120
    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v8

    const-string v9, "audio/"

    move-object v0, v10

    move-object v1, v11

    move v2, v12

    move v3, v13

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v0 .. v9}, LaGn;-><init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V

    sput-object v10, LaGn;->g:LaGn;

    .line 15
    const/4 v0, 0x7

    new-array v0, v0, [LaGn;

    const/4 v1, 0x0

    sget-object v2, LaGn;->a:LaGn;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LaGn;->b:LaGn;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LaGn;->c:LaGn;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LaGn;->d:LaGn;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaGn;->e:LaGn;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaGn;->f:LaGn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaGn;->g:LaGn;

    aput-object v2, v0, v1

    sput-object v0, LaGn;->a:[LaGn;

    .line 195
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v2

    .line 196
    invoke-static {}, LaGn;->values()[LaGn;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 197
    invoke-virtual {v5}, LaGn;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 198
    invoke-virtual {v2, v0, v5}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    goto :goto_1

    .line 196
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 201
    :cond_1
    invoke-virtual {v2}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LaGn;->a:LbmL;

    .line 202
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v1

    .line 203
    invoke-static {}, LaGn;->values()[LaGn;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 204
    iget-object v5, v4, LaGn;->a:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 205
    iget-object v5, v4, LaGn;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 203
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 208
    :cond_3
    invoke-virtual {v1}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LaGn;->b:LbmL;

    .line 209
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIILbmY;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 152
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 153
    iput p3, p0, LaGn;->c:I

    .line 154
    iput p4, p0, LaGn;->d:I

    .line 155
    iput p5, p0, LaGn;->a:I

    .line 156
    iput p6, p0, LaGn;->b:I

    .line 157
    iput p7, p0, LaGn;->e:I

    .line 158
    iput-object p8, p0, LaGn;->a:LbmY;

    .line 159
    if-eqz p9, :cond_0

    const-string v0, "/"

    invoke-virtual {p9, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 160
    iput-object p9, p0, LaGn;->a:Ljava/lang/String;

    .line 161
    return-void

    .line 159
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)LaGn;
    .locals 2

    .prologue
    .line 213
    if-nez p0, :cond_1

    .line 214
    const/4 v0, 0x0

    .line 225
    :cond_0
    :goto_0
    return-object v0

    .line 216
    :cond_1
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 217
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 218
    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 219
    sget-object v1, LaGn;->b:LbmL;

    invoke-virtual {v1, v0}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGn;

    .line 220
    if-nez v0, :cond_0

    .line 225
    :cond_2
    sget-object v0, LaGn;->a:LbmL;

    invoke-virtual {v0, p0}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGn;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LaGn;
    .locals 1

    .prologue
    .line 15
    const-class v0, LaGn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaGn;

    return-object v0
.end method

.method public static values()[LaGn;
    .locals 1

    .prologue
    .line 15
    sget-object v0, LaGn;->a:[LaGn;

    invoke-virtual {v0}, [LaGn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaGn;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, LaGn;->c:I

    return v0
.end method

.method public a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, LaGn;->a:LbmY;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, LaGn;->d:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, LaGn;->b:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, LaGn;->e:I

    return v0
.end method
