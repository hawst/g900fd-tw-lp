.class public LaSu;
.super Ljava/lang/Object;


# static fields
.field public static final a:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Lcom/google/android/gms/drive/DriveId;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LaJz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJz",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LaSA;

.field public static final a:LaSB;

.field public static final a:LaSC;

.field public static final a:LaSD;

.field public static final a:LaSw;

.field public static final a:LaSx;

.field public static final a:LaSy;

.field public static final a:LaSz;

.field public static final b:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Lcom/google/android/gms/common/data/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final r:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final t:LaJt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaJt",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const v7, 0x4c4b40

    const v6, 0x432380

    const v5, 0x3e8fa0

    const v4, 0x419ce0

    sget-object v0, LaSL;->a:LaSL;

    sput-object v0, LaSu;->a:LaJt;

    new-instance v0, LaJP;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->b:LaJt;

    new-instance v0, LaSw;

    invoke-direct {v0, v7}, LaSw;-><init>(I)V

    sput-object v0, LaSu;->a:LaSw;

    new-instance v0, LaJP;

    const-string v1, "description"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->c:LaJt;

    new-instance v0, LaJP;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->d:LaJt;

    new-instance v0, LaJP;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->e:LaJt;

    new-instance v0, LaJK;

    const-string v1, "fileSize"

    invoke-direct {v0, v1, v4}, LaJK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->f:LaJt;

    new-instance v0, LaJF;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v4}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->g:LaJt;

    new-instance v0, LaJP;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->h:LaJt;

    new-instance v0, LaJF;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v4}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->i:LaJt;

    new-instance v0, LaJF;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v4}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->j:LaJt;

    new-instance v0, LaJF;

    const-string v1, "isEditable"

    invoke-direct {v0, v1, v5}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->k:LaJt;

    new-instance v0, LaSx;

    const-string v1, "isPinned"

    invoke-direct {v0, v1, v5}, LaSx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaSx;

    new-instance v0, LaJF;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v4}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->l:LaJt;

    new-instance v0, LaJF;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v4}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->m:LaJt;

    new-instance v0, LaJF;

    const-string v1, "isTrashable"

    invoke-direct {v0, v1, v6}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->n:LaJt;

    new-instance v0, LaJF;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v4}, LaJF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->o:LaJt;

    new-instance v0, LaSy;

    const-string v1, "mimeType"

    invoke-direct {v0, v1, v5}, LaSy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaSy;

    new-instance v0, LaJP;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->p:LaJt;

    new-instance v0, LaJO;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1, v4}, LaJO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaJz;

    new-instance v0, LaSz;

    const-string v1, "parents"

    invoke-direct {v0, v1, v5}, LaSz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaSz;

    new-instance v0, LaSA;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1, v4}, LaSA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaSA;

    new-instance v0, LaSB;

    const-string v1, "starred"

    invoke-direct {v0, v1, v5}, LaSB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaSB;

    new-instance v0, LaSv;

    const-string v1, "thumbnail"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v6}, LaSv;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;I)V

    sput-object v0, LaSu;->q:LaJt;

    new-instance v0, LaSC;

    const-string v1, "title"

    invoke-direct {v0, v1, v5}, LaSC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaSC;

    new-instance v0, LaSD;

    const-string v1, "trashed"

    invoke-direct {v0, v1, v5}, LaSD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->a:LaSD;

    new-instance v0, LaJP;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->r:LaJt;

    new-instance v0, LaJP;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v4}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->s:LaJt;

    new-instance v0, LaJP;

    const-string v1, "uniqueIdentifier"

    invoke-direct {v0, v1, v7}, LaJP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaSu;->t:LaJt;

    return-void
.end method
