.class LajE;
.super Ljava/lang/Object;
.source "WrappedAnnotatedBindingBuilder.java"

# interfaces
.implements LbuQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbuQ",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:LbuQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuQ",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbuv",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbuP;Ljava/util/Set;LbuQ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuP",
            "<TT;>;",
            "Ljava/util/Set",
            "<",
            "Lbuv",
            "<*>;>;",
            "LbuQ",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuP;

    iput-object v0, p0, LajE;->a:LbuP;

    .line 32
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, LajE;->a:Ljava/util/Set;

    .line 33
    invoke-static {p3}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuQ;

    iput-object v0, p0, LajE;->a:LbuQ;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)LbuT;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, LajE;->a:Ljava/util/Set;

    iget-object v1, p0, LajE;->a:LbuP;

    invoke-static {v1, p1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(Ljava/lang/Class;)LbuT;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/annotation/Annotation;)LbuT;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, LajE;->a:Ljava/util/Set;

    iget-object v1, p0, LajE;->a:LbuP;

    invoke-static {v1, p1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(Ljava/lang/annotation/Annotation;)LbuT;

    move-result-object v0

    return-object v0
.end method

.method public a(LbuE;)LbuU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuE",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0, p1}, LajE;->a(Lbxw;)LbuU;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbuv;)LbuU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(Lbuv;)LbuU;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbxw;)LbuU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(Lbxw;)LbuU;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)LbuU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    move-result-object v0

    return-object v0
.end method

.method public a(LbuH;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(LbuH;)V

    .line 61
    return-void
.end method

.method public a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(Ljava/lang/Class;)V

    .line 56
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->a(Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method public b(Ljava/lang/Class;)LbuU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lbxw",
            "<+TT;>;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, LajE;->a:LbuQ;

    invoke-interface {v0, p1}, LbuQ;->b(Ljava/lang/Class;)LbuU;

    move-result-object v0

    return-object v0
.end method
