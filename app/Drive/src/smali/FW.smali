.class public LFW;
.super Ljava/lang/Object;
.source "DocumentOpenerModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method provideDocumentOpener(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)LFR;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "DefaultLocal"
    .end annotation

    .prologue
    .line 25
    return-object p1
.end method

.method provideDownloadFileDocumentOpener(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 32
    return-object p1
.end method

.method provideDriveAppSetProvider(LaHO;)LaHL;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 39
    return-object p1
.end method

.method provideFileOpenerIntentCreator(Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;)Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 45
    return-object p1
.end method
