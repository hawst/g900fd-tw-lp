.class LDV;
.super Ljava/lang/Object;
.source "SyncViewState.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LBZ;

.field private final a:LaGM;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:LvU;


# direct methods
.method constructor <init>(LaGM;LBZ;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LDV;->a:LaGM;

    .line 131
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBZ;

    iput-object v0, p0, LDV;->a:LBZ;

    .line 132
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LDV;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 133
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p0, LDV;->a:LvU;

    .line 134
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, LDV;->a:LaGM;

    iget-object v1, p0, LDV;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v1

    .line 139
    if-nez v1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v2, p0, LDV;->a:LBZ;

    invoke-interface {v1}, LaGo;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v1, v0}, LBZ;->a(LaGo;Z)V

    .line 144
    iget-object v0, p0, LDV;->a:LvU;

    invoke-interface {v0}, LvU;->a()V

    goto :goto_0

    .line 143
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
