.class LKi;
.super LaGN;
.source "ActionCardFunctionManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LbmF",
        "<",
        "LaGu;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LKf;

.field private final a:LLw;

.field private final a:Lcom/google/android/apps/docs/doclist/SelectionItem;


# direct methods
.method constructor <init>(LKf;LLw;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 1

    .prologue
    .line 51
    iput-object p1, p0, LKi;->a:LKf;

    invoke-direct {p0}, LaGN;-><init>()V

    .line 52
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLw;

    iput-object v0, p0, LKi;->a:LLw;

    .line 53
    iput-object p3, p0, LKi;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 54
    return-void
.end method


# virtual methods
.method public a(LaGM;)LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            ")",
            "LbmF",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, LKi;->a:LKf;

    invoke-static {v1}, LKf;->a(LKf;)LbmF;

    move-result-object v1

    invoke-virtual {v1}, LbmF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    iget-object v1, p0, LKi;->a:LLw;

    iget-object v2, p0, LKi;->a:LKf;

    invoke-static {v2}, LKf;->a(LKf;)LbmF;

    move-result-object v2

    iget-object v3, p0, LKi;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-interface {v1, v2, v3}, LLw;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    iget-object v0, p0, LKi;->a:LKf;

    invoke-static {v0}, LKf;->a(LKf;)LbmF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v0

    .line 63
    iget-object v1, p0, LKi;->a:LLw;

    iget-object v2, p0, LKi;->a:LKf;

    invoke-static {v2}, LKf;->a(LKf;)LbmF;

    move-result-object v2

    iget-object v3, p0, LKi;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-interface {v1, v0, v2, v3}, LLw;->a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 64
    iget-object v0, p0, LKi;->a:LKf;

    invoke-static {v0}, LKf;->a(LKf;)LbmF;

    move-result-object v0

    .line 69
    :cond_0
    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, LKi;->a(LaGM;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a(LbmF;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    if-eqz p1, :cond_0

    .line 76
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v0

    .line 77
    iget-object v1, p0, LKi;->a:LLw;

    iget-object v2, p0, LKi;->a:LKf;

    invoke-static {v2}, LKf;->a(LKf;)Ljava/lang/Runnable;

    move-result-object v2

    iget-object v3, p0, LKi;->a:LKf;

    invoke-static {v3}, LKf;->b(LKf;)LbmF;

    move-result-object v3

    invoke-interface {v1, v2, v0, p1, v3}, LLw;->a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V

    .line 79
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, LbmF;

    invoke-virtual {p0, p1}, LKi;->a(LbmF;)V

    return-void
.end method
