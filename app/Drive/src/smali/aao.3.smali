.class public Laao;
.super Ljava/lang/Object;
.source "OnlineSearch.java"

# interfaces
.implements Laau;


# instance fields
.field protected final a:LagS;


# direct methods
.method public constructor <init>(LagS;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Laao;->a:LagS;

    .line 75
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 78
    const-string v0, "folder"

    invoke-static {v0}, Lafi;->a(Ljava/lang/String;)Lafi;

    move-result-object v0

    invoke-static {}, Laal;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 79
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 80
    const-string v1, "title-exact"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 81
    const-string v1, "showfolders"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 82
    const-string v1, "showdeleted"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 83
    invoke-static {v0}, Laal;->a(Landroid/net/Uri$Builder;)V

    .line 84
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(LaFO;Ljava/lang/String;Laag;)Laaf;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0, p1, p2, p3}, Laao;->a(LaFO;Ljava/lang/String;Laag;)Laal;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;Ljava/lang/String;Laag;)Laal;
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Laao;->a:LagS;

    invoke-interface {v0}, LagS;->a()Lagx;

    move-result-object v0

    .line 90
    new-instance v1, Laal;

    invoke-static {p2}, Laao;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, p1, v2, p3, v0}, Laal;-><init>(LaFO;Landroid/net/Uri;Laag;Lagx;)V

    return-object v1
.end method
