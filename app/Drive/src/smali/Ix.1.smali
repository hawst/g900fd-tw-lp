.class public abstract LIx;
.super Ljava/lang/Object;
.source "GenericSectionIndexer.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/widget/SectionIndexer;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:Landroid/util/SparseIntArray;

.field private final a:Z

.field private final a:[LIw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LIw",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([LIw;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LIw",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-boolean p2, p0, LIx;->a:Z

    .line 77
    array-length v0, p1

    iput v0, p0, LIx;->a:I

    .line 78
    iput-object p1, p0, LIx;->a:[LIw;

    .line 82
    new-instance v0, Landroid/util/SparseIntArray;

    iget v1, p0, LIx;->a:I

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, LIx;->a:Landroid/util/SparseIntArray;

    .line 86
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p0}, LIx;->a()Ljava/util/Comparator;

    move-result-object v0

    .line 120
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 121
    iget-boolean v1, p0, LIx;->a:Z

    if-eqz v1, :cond_0

    neg-int v0, v0

    :cond_0
    return v0
.end method

.method protected abstract a(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method protected abstract a()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end method

.method public getPositionForSection(I)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    .line 149
    iget-object v4, p0, LIx;->a:Landroid/util/SparseIntArray;

    .line 151
    invoke-virtual {p0}, LIx;->a()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LIx;->a:[LIw;

    if-nez v1, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v0

    .line 156
    :cond_1
    if-lez p1, :cond_0

    .line 159
    iget v1, p0, LIx;->a:I

    if-lt p1, v1, :cond_2

    .line 160
    iget v1, p0, LIx;->a:I

    add-int/lit8 p1, v1, -0x1

    .line 165
    :cond_2
    invoke-virtual {p0}, LIx;->a()I

    move-result v3

    .line 173
    iget-object v1, p0, LIx;->a:[LIw;

    aget-object v1, v1, p1

    invoke-interface {v1}, LIw;->a()Ljava/lang/Object;

    move-result-object v5

    .line 176
    invoke-virtual {v4, p1, v6}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    if-eq v6, v1, :cond_b

    .line 180
    if-gez v1, :cond_5

    .line 181
    neg-int v2, v1

    .line 190
    :goto_1
    if-lez p1, :cond_3

    .line 192
    add-int/lit8 v1, p1, -0x1

    .line 193
    invoke-virtual {v4, v1, v6}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    .line 194
    if-eq v1, v6, :cond_3

    .line 195
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 201
    :cond_3
    add-int v1, v2, v0

    div-int/lit8 v1, v1, 0x2

    move v7, v2

    move v2, v0

    move v0, v7

    .line 203
    :goto_2
    if-ge v1, v0, :cond_4

    .line 207
    invoke-virtual {p0, v1}, LIx;->a(I)Ljava/lang/Object;

    move-result-object v6

    .line 208
    if-nez v6, :cond_7

    .line 209
    if-nez v1, :cond_6

    .line 250
    :cond_4
    :goto_3
    invoke-virtual {v4, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    move v0, v1

    .line 252
    goto :goto_0

    :cond_5
    move v0, v1

    .line 185
    goto :goto_0

    .line 212
    :cond_6
    add-int/lit8 v1, v1, -0x1

    .line 213
    goto :goto_2

    .line 216
    :cond_7
    invoke-virtual {p0, v6, v5}, LIx;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v6

    .line 217
    if-eqz v6, :cond_a

    .line 229
    if-gez v6, :cond_8

    .line 230
    add-int/lit8 v1, v1, 0x1

    .line 231
    if-lt v1, v3, :cond_9

    move v1, v3

    .line 233
    goto :goto_3

    :cond_8
    move v0, v1

    move v1, v2

    .line 248
    :cond_9
    :goto_4
    add-int v2, v1, v0

    div-int/lit8 v2, v2, 0x2

    move v7, v2

    move v2, v1

    move v1, v7

    .line 249
    goto :goto_2

    .line 240
    :cond_a
    if-eq v2, v1, :cond_4

    move v0, v1

    move v1, v2

    .line 245
    goto :goto_4

    :cond_b
    move v2, v3

    goto :goto_1
.end method

.method public getSectionForPosition(I)I
    .locals 3

    .prologue
    .line 264
    invoke-virtual {p0, p1}, LIx;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 268
    iget v0, p0, LIx;->a:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 271
    iget-object v2, p0, LIx;->a:[LIw;

    aget-object v2, v2, v0

    invoke-interface {v2}, LIw;->a()Ljava/lang/Object;

    move-result-object v2

    .line 272
    invoke-virtual {p0, v2, v1}, LIx;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gtz v2, :cond_0

    .line 276
    :goto_1
    return v0

    .line 268
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 276
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, LIx;->a:[LIw;

    return-object v0
.end method
