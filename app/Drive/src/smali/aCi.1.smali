.class public LaCi;
.super Ljava/lang/Object;


# instance fields
.field private final a:LaCh;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Content ID cannot be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, LaCh;

    invoke-direct {v0, p1}, LaCh;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LaCi;->a:LaCh;

    return-void
.end method


# virtual methods
.method public a()LaCh;
    .locals 1

    iget-object v0, p0, LaCi;->a:LaCh;

    invoke-virtual {v0}, LaCh;->a()V

    iget-object v0, p0, LaCi;->a:LaCh;

    return-object v0
.end method

.method public a(I)LaCi;
    .locals 1

    iget-object v0, p0, LaCi;->a:LaCh;

    invoke-virtual {v0, p1}, LaCh;->a(I)V

    return-object p0
.end method

.method public a(LaCj;)LaCi;
    .locals 1

    iget-object v0, p0, LaCi;->a:LaCh;

    invoke-virtual {v0, p1}, LaCh;->a(LaCj;)V

    return-object p0
.end method

.method public a(Ljava/lang/String;)LaCi;
    .locals 1

    iget-object v0, p0, LaCi;->a:LaCh;

    invoke-virtual {v0, p1}, LaCh;->a(Ljava/lang/String;)V

    return-object p0
.end method

.method public a(Lorg/json/JSONObject;)LaCi;
    .locals 1

    iget-object v0, p0, LaCi;->a:LaCh;

    invoke-virtual {v0, p1}, LaCh;->a(Lorg/json/JSONObject;)V

    return-object p0
.end method
