.class public LRC;
.super Ljava/lang/Object;
.source "NavigationFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

.field final synthetic a:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iput-object p2, p0, LRC;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 388
    iget-object v0, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-virtual {v0}, LRK;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_1

    .line 389
    iget-object v0, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 390
    iget-object v0, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0, p3}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;I)I

    .line 391
    iget-object v0, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V

    .line 393
    new-instance v0, LRD;

    invoke-direct {v0, p0, p3}, LRD;-><init>(LRC;I)V

    .line 403
    iget-object v1, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    iget-object v0, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-virtual {v0}, LRK;->getCount()I

    move-result v0

    sub-int v0, p3, v0

    .line 406
    iget-object v1, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 407
    iget-object v1, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 413
    :goto_1
    iget-object v0, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 414
    iget-object v0, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    iget-object v1, p0, LRC;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0

    .line 409
    :cond_2
    const-string v0, "NavigationFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid item index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
