.class public Laon;
.super Ljava/lang/Object;
.source "EntryCacheSpec.java"


# instance fields
.field private final a:J

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;JLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Laon;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 20
    iput-wide p2, p0, Laon;->a:J

    .line 21
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iput-object v0, p0, Laon;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 22
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/EntrySpec;JLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Laon;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Laon;

    invoke-direct {v0, p0, p1, p2, p3}, Laon;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;JLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)V

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Laon;->a:J

    return-wide v0
.end method

.method public a()LaFO;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Laon;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Laon;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Laon;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 47
    instance-of v1, p1, Laon;

    if-nez v1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    check-cast p1, Laon;

    .line 51
    iget-object v1, p0, Laon;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v2, p1, Laon;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Laon;->a:J

    iget-wide v4, p1, Laon;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Laon;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iget-object v2, p1, Laon;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 53
    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Laon;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Laon;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Laon;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
