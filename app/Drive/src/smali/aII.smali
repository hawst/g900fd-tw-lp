.class public final LaII;
.super LaIC;
.source "ViewGuid.java"


# instance fields
.field private final a:LaGM;

.field private final a:LaIJ;

.field private final a:LaIy;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LvL;


# direct methods
.method constructor <init>(JLaIJ;LaGM;LvL;LaIy;Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LaIJ;",
            "LaGM;",
            "LvL;",
            "LaIy;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, LaIC;-><init>(J)V

    .line 104
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIJ;

    iput-object v0, p0, LaII;->a:LaIJ;

    .line 105
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LaII;->a:LaGM;

    .line 106
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIy;

    iput-object v0, p0, LaII;->a:LaIy;

    .line 107
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    iput-object v0, p0, LaII;->a:LbuE;

    .line 108
    iput-object p5, p0, LaII;->a:LvL;

    .line 109
    return-void
.end method


# virtual methods
.method public a()LaGo;
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Z[Ljava/lang/String;LIK;Landroid/net/Uri;)LaHY;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 122
    iget-object v1, p0, LaII;->a:LaGM;

    invoke-virtual {p0}, LaII;->a()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, LaGM;->a(J)LaFM;

    move-result-object v2

    .line 124
    if-nez v2, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-object v0

    .line 128
    :cond_1
    new-instance v1, LvN;

    invoke-direct {v1}, LvN;-><init>()V

    iget-object v3, p0, LaII;->a:LvL;

    .line 129
    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v4

    invoke-interface {v3, v4}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v1, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v1

    iget-object v3, p0, LaII;->a:LvL;

    iget-object v4, p0, LaII;->a:LaIJ;

    .line 130
    invoke-virtual {v4}, LaIJ;->a()LCl;

    move-result-object v4

    invoke-interface {v3, v4}, LvL;->a(LCl;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v1, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v1

    iget-object v3, p0, LaII;->a:LvL;

    .line 131
    invoke-interface {v3}, LvL;->c()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v1, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v1

    iget-object v3, p0, LaII;->a:LvL;

    .line 132
    invoke-interface {v3}, LvL;->a()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v1, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v1

    .line 133
    invoke-virtual {v1}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v3

    .line 135
    iget-object v1, p0, LaII;->a:LaGM;

    invoke-static {p1, p2, v3, p3, v1}, LaII;->a(Z[Ljava/lang/String;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LaGM;)LaHY;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_0

    .line 140
    iget-object v0, p0, LaII;->a:LaIy;

    invoke-virtual {v0, p0, v2, v3, p4}, LaIy;->a(LaIC;LaFM;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v0

    .line 141
    invoke-virtual {v1, v0}, LaHY;->a(Landroid/os/Bundle;)V

    move-object v0, v1

    .line 142
    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaIq;)LaIA;
    .locals 5

    .prologue
    .line 154
    iget-object v0, p0, LaII;->a:LaGM;

    invoke-virtual {p0}, LaII;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LaGM;->a(J)LaFM;

    move-result-object v0

    .line 155
    if-nez v0, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    .line 161
    :cond_0
    :try_start_0
    iget-object v1, p0, LaII;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 165
    invoke-virtual {p3, v1, v0, p1, p2}, LaIq;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFM;Ljava/lang/String;Ljava/lang/String;)LaIA;

    move-result-object v0

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "Error getting root folder entry spec: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a(Z[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 171
    iget-object v0, p0, LaII;->a:LaGM;

    invoke-virtual {p0}, LaII;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LaGM;->a(J)LaFM;

    move-result-object v0

    .line 172
    if-nez v0, :cond_0

    .line 175
    :goto_0
    return-object v5

    .line 177
    :cond_0
    invoke-static {p0}, LaIF;->a(LaIC;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaII;->a:LaIJ;

    iget-object v0, p0, LaII;->a:LbuE;

    .line 178
    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v2, v0}, LaIJ;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LaGv;->a:LaGv;

    const-string v4, "vnd.android.document/directory"

    iget-object v0, p0, LaII;->a:LaIJ;

    .line 183
    invoke-virtual {v0}, LaIJ;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v0, p0, LaII;->a:LaIJ;

    .line 184
    invoke-virtual {v0}, LaIJ;->a()Z

    move-result v0

    invoke-static {v0}, LaHW;->a(Z)LaHW;

    move-result-object v8

    move-object v0, p2

    move-object v6, v5

    .line 175
    invoke-static/range {v0 .. v8}, LaHV;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, LaII;->a:LaIJ;

    invoke-virtual {v0}, LaIJ;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 189
    invoke-super {p0, p1}, LaIC;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x0

    .line 193
    :goto_0
    return v0

    .line 192
    :cond_0
    check-cast p1, LaII;

    .line 193
    iget-object v0, p0, LaII;->a:LaIJ;

    iget-object v1, p1, LaII;->a:LaIJ;

    invoke-virtual {v0, v1}, LaIJ;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 198
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, LaIC;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaII;->a:LaIJ;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
