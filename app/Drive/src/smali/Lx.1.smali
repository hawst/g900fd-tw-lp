.class LLx;
.super LLQ;
.source "SelectionFunctionDoDownload.java"


# instance fields
.field private final a:LPp;

.field private final a:LaKR;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LPp;LtK;LaKR;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, LLQ;-><init>()V

    .line 29
    iput-object p1, p0, LLx;->a:LPp;

    .line 30
    iput-object p2, p0, LLx;->a:LtK;

    .line 31
    iput-object p3, p0, LLx;->a:LaKR;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 53
    invoke-virtual {p3}, LbmF;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 54
    invoke-static {p3}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 55
    iget-object v1, p0, LLx;->a:LPp;

    invoke-interface {v1, v0}, LPp;->e(LaGu;)V

    .line 57
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 58
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 38
    invoke-static {p1}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 39
    invoke-interface {v0}, LaGu;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 47
    :goto_0
    return v0

    .line 42
    :cond_0
    iget-object v3, p0, LLx;->a:LaKR;

    invoke-interface {v3}, LaKR;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, LaGu;->f()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, LaGu;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LLx;->a:LtK;

    sget-object v3, Lry;->A:Lry;

    .line 43
    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 44
    goto :goto_0

    :cond_1
    move v0, v1

    .line 47
    goto :goto_0
.end method
