.class public LaFK;
.super Ljava/lang/Object;
.source "SqlWhereClause.java"


# instance fields
.field private final a:Ljava/lang/StringBuilder;

.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LaFK;->a:Ljava/util/Collection;

    .line 42
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/util/Collection;LaFJ;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, LaFK;-><init>(Ljava/lang/String;Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFK;
    .locals 2

    .prologue
    .line 48
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()LbmF;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, LaFK;->a(LaFL;Ljava/lang/String;Ljava/util/Collection;)LaFK;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFL;Ljava/lang/String;Ljava/util/Collection;)LaFK;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFL;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LaFK;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, LaFK;->a:Ljava/util/Collection;

    invoke-interface {v0, p3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 69
    iget-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    const-string v2, "("

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    iget-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, LaFL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    iget-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    iget-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    return-object p0
.end method

.method public a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, LaFK;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaFK;->a:Ljava/util/Collection;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method
