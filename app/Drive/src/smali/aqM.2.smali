.class public final enum LaqM;
.super Ljava/lang/Enum;
.source "StickyHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaqM;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaqM;

.field private static final synthetic a:[LaqM;

.field public static final enum b:LaqM;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, LaqM;

    const-string v1, "NOT_SCROLLING"

    invoke-direct {v0, v1, v2}, LaqM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaqM;->a:LaqM;

    .line 49
    new-instance v0, LaqM;

    const-string v1, "SCROLLING"

    invoke-direct {v0, v1, v3}, LaqM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaqM;->b:LaqM;

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [LaqM;

    sget-object v1, LaqM;->a:LaqM;

    aput-object v1, v0, v2

    sget-object v1, LaqM;->b:LaqM;

    aput-object v1, v0, v3

    sput-object v0, LaqM;->a:[LaqM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaqM;
    .locals 1

    .prologue
    .line 32
    const-class v0, LaqM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaqM;

    return-object v0
.end method

.method public static values()[LaqM;
    .locals 1

    .prologue
    .line 32
    sget-object v0, LaqM;->a:[LaqM;

    invoke-virtual {v0}, [LaqM;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaqM;

    return-object v0
.end method
