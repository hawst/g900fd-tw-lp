.class public final enum Lbgx;
.super Ljava/lang/Enum;
.source "EditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbgx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbgx;

.field private static final synthetic a:[Lbgx;

.field public static final enum b:Lbgx;

.field public static final enum c:Lbgx;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v0, Lbgx;

    const-string v1, "EDIT_QUAD"

    invoke-direct {v0, v1, v2}, Lbgx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgx;->a:Lbgx;

    .line 73
    new-instance v0, Lbgx;

    const-string v1, "EDIT_DOCUMENT"

    invoke-direct {v0, v1, v3}, Lbgx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgx;->b:Lbgx;

    .line 74
    new-instance v0, Lbgx;

    const-string v1, "CREATE_PDF"

    invoke-direct {v0, v1, v4}, Lbgx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbgx;->c:Lbgx;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lbgx;

    sget-object v1, Lbgx;->a:Lbgx;

    aput-object v1, v0, v2

    sget-object v1, Lbgx;->b:Lbgx;

    aput-object v1, v0, v3

    sget-object v1, Lbgx;->c:Lbgx;

    aput-object v1, v0, v4

    sput-object v0, Lbgx;->a:[Lbgx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbgx;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lbgx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbgx;

    return-object v0
.end method

.method public static values()[Lbgx;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lbgx;->a:[Lbgx;

    invoke-virtual {v0}, [Lbgx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbgx;

    return-object v0
.end method
