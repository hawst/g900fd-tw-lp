.class public LadZ;
.super Ljava/lang/Object;
.source "TempFileManagerImpl.java"

# interfaces
.implements LadA;


# instance fields
.field private final a:LadJ;

.field private final a:Laeb;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laea;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LadJ;Laeb;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LadZ;->a:Ljava/util/Map;

    .line 105
    iput-object p1, p0, LadZ;->a:LadJ;

    .line 106
    iput-object p2, p0, LadZ;->a:Laeb;

    .line 107
    return-void
.end method

.method static synthetic a(LadZ;)LadJ;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LadZ;->a:LadJ;

    return-object v0
.end method

.method static synthetic a(LadZ;)Laeb;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LadZ;->a:Laeb;

    return-object v0
.end method

.method static synthetic a(LadZ;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LadZ;->a:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)LadC;
    .locals 2

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LadZ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laea;

    .line 112
    if-nez v0, :cond_0

    .line 113
    new-instance v0, Laea;

    invoke-direct {v0, p0, p1}, Laea;-><init>(LadZ;Ljava/lang/String;)V

    .line 115
    iget-object v1, p0, LadZ;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :cond_0
    invoke-virtual {v0}, Laea;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-object v0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Ljava/io/File;)Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 124
    return-object v0
.end method
