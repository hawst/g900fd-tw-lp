.class public Lbhz;
.super Ljava/lang/Object;
.source "QuadEditorView.java"

# interfaces
.implements Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/ui/QuadEditorView;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/ui/QuadEditorView;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lbhz;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideFeedback()V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lbhz;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 124
    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 125
    return-void
.end method
