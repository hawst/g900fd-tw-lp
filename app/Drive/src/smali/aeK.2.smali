.class LaeK;
.super Ljava/lang/Object;
.source "AddChildEntrySerializer.java"

# interfaces
.implements Lbxs;


# instance fields
.field private final a:LaeC;

.field private final a:Lbxr;


# direct methods
.method public constructor <init>(Lbxr;LaeC;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxr;

    iput-object v0, p0, LaeK;->a:Lbxr;

    .line 33
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeC;

    iput-object v0, p0, LaeK;->a:LaeC;

    .line 34
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2

    .prologue
    .line 75
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LaeK;->a:LaeC;

    invoke-virtual {v0}, LaeC;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeK;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    :goto_0
    return-void

    .line 69
    :cond_0
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 70
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 71
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string v0, "application/atom+xml"

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 38
    .line 40
    :try_start_0
    iget-object v0, p0, LaeK;->a:Lbxr;

    invoke-interface {v0}, Lbxr;->a()Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 45
    sget-object v1, Lbxq;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 46
    if-eq p2, v3, :cond_0

    .line 47
    sget-object v1, Lbxq;->e:Ljava/lang/String;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 48
    invoke-direct {p0, v0}, LaeK;->a(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 51
    :cond_0
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 52
    invoke-direct {p0, v0, p2}, LaeK;->a(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 53
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 55
    if-eq p2, v3, :cond_1

    .line 56
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 58
    :cond_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 59
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    new-instance v1, Lbxk;

    const-string v2, "Unable to create XmlSerializer."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method
