.class LYA;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"

# interfaces
.implements LYv;


# instance fields
.field private a:F

.field private a:I

.field private final a:LYx;

.field final synthetic a:LYy;

.field private final a:Landroid/os/Handler;

.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LYB;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method private constructor <init>(LYy;LYx;Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 404
    iput-object p1, p0, LYA;->a:LYy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385
    iput v1, p0, LYA;->a:I

    .line 388
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LYA;->a:Landroid/os/Handler;

    .line 391
    const/4 v0, 0x0

    iput v0, p0, LYA;->a:F

    .line 394
    const/4 v0, -0x1

    iput v0, p0, LYA;->b:I

    .line 396
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LYA;->a:Ljava/util/List;

    .line 402
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LYA;->a:Ljava/util/SortedMap;

    .line 405
    iput-object p2, p0, LYA;->a:LYx;

    move v0, v1

    .line 407
    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    .line 408
    new-instance v2, Landroid/webkit/WebView;

    invoke-direct {v2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 409
    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    .line 410
    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 411
    iget-object v3, p0, LYA;->a:Ljava/util/List;

    new-instance v4, LYB;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v2, v0, v5}, LYB;-><init>(LYA;Landroid/webkit/WebView;ILYz;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(LYy;LYx;Landroid/content/Context;LYz;)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, LYA;-><init>(LYy;LYx;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(LYA;)F
    .locals 1

    .prologue
    .line 106
    iget v0, p0, LYA;->a:F

    return v0
.end method

.method private a()I
    .locals 2

    .prologue
    .line 466
    iget v0, p0, LYA;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LYA;->b:I

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xc8

    goto :goto_0
.end method

.method static synthetic a(LYA;)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, LYA;->b()I

    move-result v0

    return v0
.end method

.method static synthetic a(LYA;)LYx;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, LYA;->a:LYx;

    return-object v0
.end method

.method static synthetic a(LYA;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, LYA;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(LYA;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, LYA;->a:Ljava/util/SortedMap;

    return-object v0
.end method

.method private b()I
    .locals 1

    .prologue
    .line 470
    invoke-direct {p0}, LYA;->a()I

    move-result v0

    return v0
.end method

.method static synthetic b(LYA;)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, LYA;->c()I

    move-result v0

    return v0
.end method

.method private c()I
    .locals 4

    .prologue
    .line 474
    invoke-direct {p0}, LYA;->a()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LYA;->a:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 475
    return v0
.end method

.method static synthetic c(LYA;)I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, LYA;->a:I

    return v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, LYA;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYB;

    .line 456
    invoke-static {v0}, LYB;->c(LYB;)V

    goto :goto_0

    .line 458
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 417
    iget-object v0, p0, LYA;->a:LYy;

    invoke-static {v0}, LYy;->a(LYy;)LXz;

    move-result-object v0

    invoke-interface {v0}, LXz;->c()I

    move-result v0

    invoke-static {p1, v0}, LbiT;->a(II)I

    .line 419
    iget-object v0, p0, LYA;->a:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 420
    const-string v1, "PunchThumbnailGeneratorFactoryImpl"

    const-string v2, "Getting thumbnail: slideIndex=%s bitmap=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 421
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, LYA;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYB;

    .line 432
    invoke-static {v0}, LYB;->b(LYB;)V

    goto :goto_0

    .line 434
    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 438
    iput p1, p0, LYA;->a:F

    .line 439
    invoke-direct {p0}, LYA;->c()V

    .line 440
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 426
    iput p1, p0, LYA;->a:I

    .line 427
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, LYA;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    .line 463
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 450
    iput p1, p0, LYA;->b:I

    .line 451
    invoke-direct {p0}, LYA;->c()V

    .line 452
    return-void
.end method
