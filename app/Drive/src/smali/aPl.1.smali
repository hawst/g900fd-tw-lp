.class public LaPl;
.super LaRw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaRw",
        "<",
        "LaPq;",
        ">;"
    }
.end annotation


# instance fields
.field final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LaCF;LaCG;I)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, LaRw;-><init>(Landroid/content/Context;LaCF;LaCG;[Ljava/lang/String;)V

    iput p4, p0, LaPl;->a:I

    return-void
.end method


# virtual methods
.method public a()LaPq;
    .locals 1

    invoke-super {p0}, LaRw;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LaPq;

    return-object v0
.end method

.method protected a(Landroid/os/IBinder;)LaPq;
    .locals 1

    invoke-static {p1}, LaPr;->a(Landroid/os/IBinder;)LaPq;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, LaPl;->a(Landroid/os/IBinder;)LaPq;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.ads.service.START"

    return-object v0
.end method

.method protected a(LaRS;LaRA;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget v1, p0, LaPl;->a:I

    invoke-virtual {p0}, LaPl;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, LaRS;->g(LaRP;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.ads.internal.request.IAdRequestService"

    return-object v0
.end method
