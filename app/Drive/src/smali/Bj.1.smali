.class public LBj;
.super Landroid/widget/BaseAdapter;
.source "DocListGroupingAdapter.java"

# interfaces
.implements LAD;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final a:LDD;

.field private final a:LII;

.field private a:LIK;

.field private final a:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "LIA;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/view/LayoutInflater;

.field private final a:Landroid/widget/ListView;

.field private final a:Z

.field private final b:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "LBC;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "Landroid/widget/SectionIndexer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget v0, Lxe;->doc_grid_empty_title:I

    sput v0, LBj;->a:I

    .line 31
    sget v0, Lxc;->doc_list_row_group_entry_tag:I

    sput v0, LBj;->b:I

    .line 32
    sget v0, Lxc;->doc_list_row_view_type:I

    sput v0, LBj;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/widget/ListView;LBo;LII;ILIK;LHp;LzN;Z)V
    .locals 3

    .prologue
    .line 98
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 99
    iput-object p1, p0, LBj;->a:Landroid/widget/ListView;

    .line 100
    invoke-virtual {p1}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LBj;->a:Landroid/view/LayoutInflater;

    .line 101
    iput p4, p0, LBj;->d:I

    .line 102
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    iput-object v0, p0, LBj;->a:LIK;

    .line 103
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LII;

    iput-object v0, p0, LBj;->a:LII;

    .line 104
    iput-boolean p8, p0, LBj;->a:Z

    .line 106
    new-instance v0, LBk;

    invoke-direct {v0, p0}, LBk;-><init>(LBj;)V

    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LBj;->a:Laku;

    .line 112
    new-instance v0, LBl;

    invoke-direct {v0, p0}, LBl;-><init>(LBj;)V

    .line 113
    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LBj;->b:Laku;

    .line 120
    new-instance v0, LBm;

    invoke-direct {v0, p0}, LBm;-><init>(LBj;)V

    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LBj;->c:Laku;

    .line 128
    new-instance v0, LCz;

    iget-object v1, p0, LBj;->b:Laku;

    invoke-direct {v0, p6, v1}, LCz;-><init>(LHp;Lbjv;)V

    .line 130
    new-instance v1, LHP;

    iget-object v2, p0, LBj;->b:Laku;

    invoke-direct {v1, p7, v2}, LHP;-><init>(LzN;Lbjv;)V

    .line 133
    invoke-interface {p2, v0, v1}, LBo;->a(LHp;LzN;)LDD;

    move-result-object v0

    iput-object v0, p0, LBj;->a:LDD;

    .line 134
    return-void
.end method

.method private a(I)LBp;
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    .line 176
    invoke-virtual {v0}, LBC;->a()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 177
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 179
    :cond_0
    invoke-virtual {v0, p1}, LBC;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 180
    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    invoke-virtual {v0}, LBF;->a()LIB;

    move-result-object v0

    invoke-interface {v0}, LIB;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    sget-object v0, LBp;->a:LBp;

    .line 186
    :goto_0
    return-object v0

    .line 183
    :cond_1
    sget-object v0, LBp;->b:LBp;

    goto :goto_0

    .line 186
    :cond_2
    sget-object v0, LBp;->c:LBp;

    goto :goto_0
.end method

.method static synthetic a(LBj;)LDD;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LBj;->a:LDD;

    return-object v0
.end method

.method static synthetic a(LBj;)LII;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LBj;->a:LII;

    return-object v0
.end method

.method static synthetic a(LBj;)LIK;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LBj;->a:LIK;

    return-object v0
.end method

.method static synthetic a(LBj;)Laku;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LBj;->a:Laku;

    return-object v0
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 212
    if-nez p2, :cond_0

    iget-object v0, p0, LBj;->a:Landroid/view/LayoutInflater;

    sget v1, LBj;->a:I

    .line 213
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 214
    :cond_0
    sget v0, Lxc;->empty_group_title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 215
    return-object p2
.end method

.method static synthetic a(LBj;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, LBj;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 219
    if-nez p2, :cond_0

    iget-object v0, p0, LBj;->a:Landroid/view/LayoutInflater;

    iget v1, p0, LBj;->d:I

    const/4 v2, 0x0

    .line 220
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 221
    :cond_0
    invoke-static {p2}, LCy;->a(Landroid/view/View;)LCy;

    move-result-object v1

    .line 222
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v2

    .line 223
    invoke-virtual {v2}, LBF;->a()LIB;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3}, LIB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LCy;->a(Ljava/lang/String;)V

    .line 225
    iget-boolean v0, p0, LBj;->a:Z

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, LBj;->a:LIK;

    invoke-virtual {v0}, LIK;->b()I

    move-result v0

    invoke-virtual {v1, v0}, LCy;->a(I)V

    .line 227
    invoke-virtual {v1}, LCy;->a()V

    .line 232
    :goto_0
    iget-object v0, p0, LBj;->a:Laku;

    .line 233
    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIA;

    invoke-virtual {v2}, LBF;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LIA;->a(I)LIB;

    move-result-object v0

    .line 234
    invoke-virtual {v1, v0}, LCy;->a(LIB;)V

    .line 235
    return-object p2

    .line 229
    :cond_1
    invoke-virtual {v1}, LCy;->b()V

    goto :goto_0
.end method

.method static synthetic b(LBj;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, LBj;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private b(LaFX;)V
    .locals 0

    .prologue
    .line 266
    if-eqz p1, :cond_0

    .line 268
    invoke-virtual {p0}, LBj;->notifyDataSetChanged()V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-virtual {p0}, LBj;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method private c(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 239
    invoke-direct {p0, p1}, LBj;->e(I)I

    move-result v0

    .line 240
    iget-object v1, p0, LBj;->a:LDD;

    invoke-interface {v1, v0, p2, p3}, LDD;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(LBj;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, LBj;->c(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, LBj;->a:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 277
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 278
    iget-object v0, p0, LBj;->c:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 279
    return-void
.end method

.method private d(I)I
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    .line 365
    invoke-virtual {v0}, LBC;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 366
    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    invoke-virtual {v0}, LBF;->a()I

    move-result v0

    .line 368
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private e(I)I
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    .line 386
    invoke-virtual {v0}, LBF;->a()I

    move-result v0

    .line 387
    return v0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 373
    invoke-direct {p0, p1}, LBj;->d(I)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;)LIB;
    .locals 1

    .prologue
    .line 303
    sget v0, LBj;->b:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIB;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, LBj;->a:LDD;

    invoke-interface {v0}, LDD;->c()V

    .line 320
    return-void
.end method

.method public a(LCs;I)V
    .locals 3

    .prologue
    .line 351
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    if-ltz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 354
    :goto_1
    iget-object v0, p0, LBj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge p2, v0, :cond_2

    .line 355
    iget-object v0, p0, LBj;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 356
    sget v0, LBj;->c:I

    invoke-virtual {v1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBp;

    .line 357
    sget-object v2, LBp;->c:LBp;

    invoke-virtual {v2, v0}, LBp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, LBj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    add-int/2addr v0, p2

    invoke-interface {p1, v1, v0}, LCs;->a(Landroid/view/View;I)V

    .line 354
    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 361
    :cond_2
    return-void
.end method

.method public a(LQX;)V
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p1}, LQX;->a()LIK;

    move-result-object v0

    iput-object v0, p0, LBj;->a:LIK;

    .line 253
    invoke-direct {p0}, LBj;->c()V

    .line 254
    iget-object v0, p0, LBj;->a:LDD;

    invoke-interface {v0, p1}, LDD;->a(LQX;)V

    .line 255
    invoke-virtual {p1}, LQX;->a()LaFX;

    move-result-object v0

    invoke-direct {p0, v0}, LBj;->b(LaFX;)V

    .line 256
    return-void
.end method

.method public a(LaFX;)V
    .locals 1

    .prologue
    .line 260
    invoke-direct {p0}, LBj;->c()V

    .line 261
    iget-object v0, p0, LBj;->a:LDD;

    invoke-interface {v0, p1}, LDD;->a(LaFX;)V

    .line 262
    invoke-direct {p0, p1}, LBj;->b(LaFX;)V

    .line 263
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 335
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 336
    instance-of v1, v0, LCy;

    if-eqz v1, :cond_0

    .line 337
    check-cast v0, LCy;

    invoke-virtual {v0}, LCy;->c()V

    .line 339
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, LBj;->a:LDD;

    invoke-interface {v0}, LDD;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 329
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    instance-of v0, v0, LCy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)I

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, LBj;->a:LDD;

    invoke-interface {v0}, LDD;->b()V

    .line 325
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 343
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 344
    instance-of v1, v0, LCy;

    if-eqz v1, :cond_0

    .line 345
    check-cast v0, LCy;

    invoke-virtual {v0}, LCy;->d()V

    .line 347
    :cond_0
    return-void
.end method

.method public c(I)I
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0}, LBC;->a()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0, p1}, LBj;->a(I)LBp;

    move-result-object v0

    .line 144
    sget-object v1, LBn;->a:[I

    invoke-virtual {v0}, LBp;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 153
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 146
    :pswitch_0
    iget-object v0, p0, LBj;->b:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)LBF;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, LBF;->a()LIB;

    move-result-object v0

    goto :goto_0

    .line 149
    :pswitch_1
    invoke-direct {p0, p1}, LBj;->e(I)I

    move-result v0

    .line 150
    iget-object v1, p0, LBj;->a:LDD;

    invoke-interface {v1, v0}, LDD;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 151
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 159
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    .line 164
    invoke-direct {p0, p1}, LBj;->a(I)LBp;

    move-result-object v1

    .line 165
    invoke-virtual {v1}, LBp;->a()I

    move-result v0

    .line 166
    sget-object v2, LBp;->c:LBp;

    invoke-virtual {v1, v2}, LBp;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    invoke-direct {p0, p1}, LBj;->e(I)I

    move-result v1

    .line 168
    iget-object v2, p0, LBj;->a:LDD;

    invoke-interface {v2, v1}, LDD;->getItemViewType(I)I

    move-result v1

    .line 169
    add-int/2addr v0, v1

    .line 171
    :cond_0
    return v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, LBj;->c:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, LBj;->c:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, LBj;->c:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 192
    invoke-direct {p0, p1}, LBj;->a(I)LBp;

    move-result-object v1

    .line 193
    invoke-virtual {v1, p0, p1, p2, p3}, LBp;->a(LBj;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 194
    invoke-direct {p0, p1}, LBj;->e(I)I

    move-result v3

    .line 195
    iget-object v0, p0, LBj;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIA;

    invoke-virtual {v0, v3}, LIA;->a(I)LIB;

    move-result-object v0

    .line 196
    sget v3, LBj;->b:I

    invoke-virtual {v2, v3, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 197
    sget v0, LBj;->c:I

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 198
    return-object v2
.end method

.method public getViewTypeCount()I
    .locals 2

    .prologue
    .line 245
    invoke-static {}, LBp;->values()[LBp;

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 246
    iget-object v1, p0, LBj;->a:LDD;

    invoke-interface {v1}, LDD;->getViewTypeCount()I

    move-result v1

    .line 247
    add-int/2addr v0, v1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, LBj;->a:LDD;

    invoke-interface {v0, p1, p2, p3, p4}, LDD;->onScroll(Landroid/widget/AbsListView;III)V

    .line 315
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, LBj;->a:LDD;

    invoke-interface {v0, p1, p2}, LDD;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 309
    return-void
.end method
