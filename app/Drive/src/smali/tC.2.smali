.class public LtC;
.super Ljava/lang/Object;
.source "EditorsOfflineSwitchProvider.java"

# interfaces
.implements Lbxw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbxw",
        "<",
        "LtB;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LQr;

.field private final a:LaGg;

.field private final a:LbiP;
    .annotation runtime LQP;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LtB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGg;LQr;LbiP;)V
    .locals 0
    .param p3    # LbiP;
        .annotation runtime LQP;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGg;",
            "LQr;",
            "LbiP",
            "<",
            "LtB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, LtC;->a:LaGg;

    .line 79
    iput-object p2, p0, LtC;->a:LQr;

    .line 80
    iput-object p3, p0, LtC;->a:LbiP;

    .line 81
    return-void
.end method


# virtual methods
.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, LtC;->a()LtB;

    move-result-object v0

    return-object v0
.end method

.method public a()LtB;
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, LtC;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LtC;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtB;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LtD;

    iget-object v1, p0, LtC;->a:LaGg;

    iget-object v2, p0, LtC;->a:LQr;

    invoke-direct {v0, v1, v2}, LtD;-><init>(LaGg;LQr;)V

    goto :goto_0
.end method
