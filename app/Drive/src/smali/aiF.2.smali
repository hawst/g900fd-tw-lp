.class public LaiF;
.super Landroid/app/Fragment;
.source "ActivitySessionFragmentImpl.java"

# interfaces
.implements LaiD;


# instance fields
.field private a:LaiE;

.field private final a:LajA;

.field private a:Ljava/util/UUID;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 17
    new-instance v0, LajA;

    invoke-direct {v0}, LajA;-><init>()V

    iput-object v0, p0, LaiF;->a:LajA;

    return-void
.end method


# virtual methods
.method public a()LajA;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, LaiF;->a:LajA;

    return-object v0
.end method

.method public a()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LaiF;->a:Ljava/util/UUID;

    return-object v0
.end method

.method public a(LaiE;)V
    .locals 1

    .prologue
    .line 27
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiE;

    iput-object v0, p0, LaiF;->a:LaiE;

    .line 28
    return-void
.end method

.method public a(Ljava/util/UUID;)V
    .locals 1

    .prologue
    .line 45
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/UUID;

    iput-object v0, p0, LaiF;->a:Ljava/util/UUID;

    .line 46
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LaiF;->a:LaiE;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, LaiF;->a:LaiE;

    invoke-interface {v0, p0}, LaiE;->a(LaiD;)V

    .line 35
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 36
    return-void
.end method
