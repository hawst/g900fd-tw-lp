.class public final LbjD;
.super Ljava/lang/Object;
.source "AbstractCache.java"

# interfaces
.implements LbjE;


# instance fields
.field private final a:LbkT;

.field private final b:LbkT;

.field private final c:LbkT;

.field private final d:LbkT;

.field private final e:LbkT;

.field private final f:LbkT;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    invoke-static {}, LbkU;->a()LbkT;

    move-result-object v0

    iput-object v0, p0, LbjD;->a:LbkT;

    .line 201
    invoke-static {}, LbkU;->a()LbkT;

    move-result-object v0

    iput-object v0, p0, LbjD;->b:LbkT;

    .line 202
    invoke-static {}, LbkU;->a()LbkT;

    move-result-object v0

    iput-object v0, p0, LbjD;->c:LbkT;

    .line 203
    invoke-static {}, LbkU;->a()LbkT;

    move-result-object v0

    iput-object v0, p0, LbjD;->d:LbkT;

    .line 204
    invoke-static {}, LbkU;->a()LbkT;

    move-result-object v0

    iput-object v0, p0, LbjD;->e:LbkT;

    .line 205
    invoke-static {}, LbkU;->a()LbkT;

    move-result-object v0

    iput-object v0, p0, LbjD;->f:LbkT;

    .line 210
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, LbjD;->f:LbkT;

    invoke-interface {v0}, LbkT;->a()V

    .line 240
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 217
    iget-object v0, p0, LbjD;->a:LbkT;

    int-to-long v2, p1

    invoke-interface {v0, v2, v3}, LbkT;->a(J)V

    .line 218
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, LbjD;->c:LbkT;

    invoke-interface {v0}, LbkT;->a()V

    .line 230
    iget-object v0, p0, LbjD;->e:LbkT;

    invoke-interface {v0, p1, p2}, LbkT;->a(J)V

    .line 231
    return-void
.end method

.method public b(I)V
    .locals 4

    .prologue
    .line 225
    iget-object v0, p0, LbjD;->b:LbkT;

    int-to-long v2, p1

    invoke-interface {v0, v2, v3}, LbkT;->a(J)V

    .line 226
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, LbjD;->d:LbkT;

    invoke-interface {v0}, LbkT;->a()V

    .line 235
    iget-object v0, p0, LbjD;->e:LbkT;

    invoke-interface {v0, p1, p2}, LbkT;->a(J)V

    .line 236
    return-void
.end method
