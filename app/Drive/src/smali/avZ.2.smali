.class public LavZ;
.super Ljava/lang/Object;
.source "IOUtils.java"


# direct methods
.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20
    const/16 v0, 0x2000

    new-array v2, v0, [B

    move v0, v1

    .line 23
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_0

    .line 24
    add-int/2addr v0, v3

    .line 25
    invoke-virtual {p1, v2, v1, v3}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 28
    const-string v1, "IOUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "copied "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " b."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    return v0
.end method

.method public static a([B)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 75
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 76
    const/4 v0, 0x0

    aget-object v0, v1, v0

    .line 77
    const/4 v2, 0x1

    aget-object v1, v1, v2

    .line 78
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v3, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v3, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-static {v2, v3}, LavZ;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-object v0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    const-string v1, "IOUtils"

    const-string v2, "i/o error while creating pipe"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 83
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 33
    if-eqz p0, :cond_0

    .line 35
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lawa;

    invoke-direct {v1, p0, p1}, Lawa;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 66
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 67
    return-void
.end method

.method public static b(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    .locals 3

    .prologue
    .line 45
    :try_start_0
    invoke-static {p0, p1}, LavZ;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v0

    .line 46
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    invoke-static {p0}, LavZ;->a(Ljava/io/Closeable;)V

    .line 53
    invoke-static {p1}, LavZ;->a(Ljava/io/Closeable;)V

    :goto_0
    return v0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    :try_start_1
    const-string v1, "IOUtils"

    const-string v2, "i/o error while copying streams"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    const/4 v0, -0x1

    .line 52
    invoke-static {p0}, LavZ;->a(Ljava/io/Closeable;)V

    .line 53
    invoke-static {p1}, LavZ;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    invoke-static {p0}, LavZ;->a(Ljava/io/Closeable;)V

    .line 53
    invoke-static {p1}, LavZ;->a(Ljava/io/Closeable;)V

    throw v0
.end method
