.class public Laha;
.super Ljava/lang/Object;
.source "SingleDocSynchronizerImpl.java"

# interfaces
.implements LagZ;


# instance fields
.field private final a:LaGM;

.field private final a:Ladi;

.field private final a:Laer;

.field private final a:Lagl;

.field private final a:LahT;

.field private final a:LamL;


# direct methods
.method public constructor <init>(Laer;Lagl;Ladi;LamL;LahT;LaGM;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Laha;->a:Laer;

    .line 50
    iput-object p2, p0, Laha;->a:Lagl;

    .line 51
    iput-object p3, p0, Laha;->a:Ladi;

    .line 52
    iput-object p4, p0, Laha;->a:LamL;

    .line 53
    iput-object p5, p0, Laha;->a:LahT;

    .line 54
    iput-object p6, p0, Laha;->a:LaGM;

    .line 55
    return-void
.end method


# virtual methods
.method public a(LaGo;)V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lahb;

    invoke-direct {v0, p0, p1}, Lahb;-><init>(Laha;LaGo;)V

    .line 64
    invoke-virtual {v0}, Lahb;->start()V

    .line 65
    return-void
.end method

.method a(LaGo;Z)V
    .locals 3

    .prologue
    .line 95
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    invoke-interface {p1}, LaGo;->a()LaFM;

    move-result-object v1

    invoke-interface {p1}, LaGo;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Laha;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LaFM;Ljava/lang/String;Z)V

    .line 98
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 3

    .prologue
    .line 151
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Laha;->a:LaGM;

    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 154
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v0, v1, v2}, Laha;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LaFM;Ljava/lang/String;Z)V

    .line 155
    return-void
.end method

.method a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LaFM;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 103
    if-nez p1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    if-eqz p4, :cond_2

    .line 110
    :try_start_0
    iget-object v0, p0, Laha;->a:Laer;

    invoke-interface {v0, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;
    :try_end_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 125
    :goto_1
    if-eqz v0, :cond_0

    .line 127
    :try_start_1
    iget-object v1, p0, Laha;->a:Lagl;

    const/4 v2, 0x0

    invoke-interface {v1, p2, v0, v2}, Lagl;->a(LaFM;LaJT;Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const-string v1, "SingleDocSynchronizerImpl"

    const-string v2, "Failed to update entry: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 112
    :cond_2
    :try_start_2
    iget-object v0, p0, Laha;->a:Laer;

    invoke-interface {v0, p1, p3}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)LaeF;
    :try_end_2
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_1
    .catch LbwO; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    goto :goto_1

    .line 114
    :catch_1
    move-exception v0

    .line 115
    const-string v1, "SingleDocSynchronizerImpl"

    const-string v2, "Failed to get entry: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 117
    :catch_2
    move-exception v0

    .line 118
    const-string v1, "SingleDocSynchronizerImpl"

    const-string v2, "Failed to get entry: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 120
    :catch_3
    move-exception v0

    .line 121
    const-string v1, "SingleDocSynchronizerImpl"

    const-string v2, "Failed to get entry: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public b(LaGo;)V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lahd;

    invoke-direct {v0, p0, p1}, Lahd;-><init>(Laha;LaGo;)V

    .line 90
    invoke-virtual {v0}, Lahd;->start()V

    .line 91
    return-void
.end method

.method public c(LaGo;)V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lahc;

    invoke-direct {v0, p0, p1}, Lahc;-><init>(Laha;LaGo;)V

    .line 79
    invoke-virtual {v0}, Lahc;->start()V

    .line 80
    return-void
.end method

.method public d(LaGo;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Laha;->a(LaGo;Z)V

    .line 70
    return-void
.end method

.method public e(LaGo;)V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Laha;->a(LaGo;Z)V

    .line 147
    return-void
.end method

.method f(LaGo;)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Laha;->a:Ladi;

    sget-object v1, LacY;->a:LacY;

    invoke-interface {v0, p1, v1}, Ladi;->a(LaGo;LacY;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laha;->a:LamL;

    .line 139
    invoke-interface {v0}, LamL;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Laha;->a:LahT;

    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LahT;->e(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 142
    :cond_0
    return-void
.end method
