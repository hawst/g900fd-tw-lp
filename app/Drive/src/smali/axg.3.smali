.class Laxg;
.super Ljava/lang/Object;
.source "PdfViewer.java"

# interfaces
.implements Lawf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lawf",
        "<",
        "Lcom/google/android/apps/viewer/pdflib/Selection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LawZ;


# direct methods
.method constructor <init>(LawZ;)V
    .locals 0

    .prologue
    .line 640
    iput-object p1, p0, Laxg;->a:LawZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/viewer/pdflib/Selection;Lcom/google/android/apps/viewer/pdflib/Selection;)V
    .locals 2

    .prologue
    .line 643
    if-eqz p1, :cond_0

    iget-object v0, p0, Laxg;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Laxl;

    move-result-object v0

    iget v1, p1, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    invoke-virtual {v0, v1}, Laxl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Laxg;->a:LawZ;

    iget v1, p1, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    invoke-static {v0, v1}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 646
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Laxg;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Laxl;

    move-result-object v0

    iget v1, p2, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    invoke-virtual {v0, v1}, Laxl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 647
    iget-object v0, p0, Laxg;->a:LawZ;

    iget v1, p2, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    invoke-static {v0, v1}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    new-instance v1, LawI;

    invoke-direct {v1, p2}, LawI;-><init>(Lcom/google/android/apps/viewer/pdflib/Selection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 649
    :cond_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 640
    check-cast p1, Lcom/google/android/apps/viewer/pdflib/Selection;

    check-cast p2, Lcom/google/android/apps/viewer/pdflib/Selection;

    invoke-virtual {p0, p1, p2}, Laxg;->a(Lcom/google/android/apps/viewer/pdflib/Selection;Lcom/google/android/apps/viewer/pdflib/Selection;)V

    return-void
.end method
