.class public final enum Laat;
.super Ljava/lang/Enum;
.source "SearchHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laat;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laat;

.field private static final synthetic a:[Laat;

.field public static final enum b:Laat;

.field public static final enum c:Laat;

.field public static final enum d:Laat;

.field public static final enum e:Laat;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Laat;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, Laat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laat;->a:Laat;

    .line 14
    new-instance v0, Laat;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Laat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laat;->b:Laat;

    .line 15
    new-instance v0, Laat;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, Laat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laat;->c:Laat;

    .line 16
    new-instance v0, Laat;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v5}, Laat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laat;->d:Laat;

    .line 17
    new-instance v0, Laat;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v6}, Laat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laat;->e:Laat;

    .line 12
    const/4 v0, 0x5

    new-array v0, v0, [Laat;

    sget-object v1, Laat;->a:Laat;

    aput-object v1, v0, v2

    sget-object v1, Laat;->b:Laat;

    aput-object v1, v0, v3

    sget-object v1, Laat;->c:Laat;

    aput-object v1, v0, v4

    sget-object v1, Laat;->d:Laat;

    aput-object v1, v0, v5

    sget-object v1, Laat;->e:Laat;

    aput-object v1, v0, v6

    sput-object v0, Laat;->a:[Laat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laat;
    .locals 1

    .prologue
    .line 12
    const-class v0, Laat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laat;

    return-object v0
.end method

.method public static values()[Laat;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Laat;->a:[Laat;

    invoke-virtual {v0}, [Laat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laat;

    return-object v0
.end method
