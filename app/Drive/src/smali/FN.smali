.class public LFN;
.super Lbuo;
.source "ContextScopedProviderPackageModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lbuo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public get1(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime Lbwm;
            value = "DefaultRemote"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "DefaultRemote"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LFR;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LFR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public get2(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime Lbwm;
            value = "DefaultLocal"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "DefaultLocal"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LFR;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LFR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public getLazy1(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime Lbwm;
            value = "DefaultRemote"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "DefaultRemote"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LFR;",
            ">;)",
            "Lajw",
            "<",
            "LFR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method

.method public getLazy2(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime Lbwm;
            value = "DefaultLocal"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "DefaultLocal"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LFR;",
            ">;)",
            "Lajw",
            "<",
            "LFR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method
