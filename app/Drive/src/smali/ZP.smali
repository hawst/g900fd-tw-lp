.class public LZP;
.super Ljava/lang/Object;
.source "DelayWaitingRateLimiter.java"

# interfaces
.implements LZS;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LaKM;

.field private final a:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(LaKM;)V
    .locals 4
    .param p1    # LaKM;
        .annotation runtime Lbxv;
            a = "UptimeClock"
        .end annotation
    .end param

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LZP;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 35
    iput-object p1, p0, LZP;->a:LaKM;

    .line 36
    return-void
.end method


# virtual methods
.method a()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 79
    iget-object v0, p0, LZP;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    iget-object v4, p0, LZP;->a:LaKM;

    invoke-interface {v4}, LaKM;->a()J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 81
    const-wide/16 v4, 0x7530

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 82
    iget-object v0, p0, LZP;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    move-wide v0, v2

    .line 85
    :cond_0
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(J)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 49
    cmp-long v0, p1, v8

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 50
    cmp-long v0, p1, v8

    if-nez v0, :cond_2

    .line 70
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 49
    goto :goto_0

    .line 53
    :cond_2
    const-wide/16 v4, 0x7530

    invoke-static {v4, v5, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 54
    iget-object v0, p0, LZP;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 55
    cmp-long v0, v4, v8

    if-ltz v0, :cond_4

    :goto_2
    invoke-static {v1}, LbiT;->b(Z)V

    .line 68
    :cond_3
    iget-object v0, p0, LZP;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    .line 69
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    iget-object v2, p0, LZP;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v0, v1, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_4
    move v1, v2

    .line 55
    goto :goto_2
.end method

.method public b()V
    .locals 4

    .prologue
    .line 91
    :goto_0
    invoke-virtual {p0}, LZP;->a()J

    move-result-wide v0

    .line 92
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 98
    return-void

    .line 96
    :cond_0
    const-wide/16 v2, 0x5

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 106
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s[%d msec wait time remaining]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "DelayRateLimiter"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, LZP;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
