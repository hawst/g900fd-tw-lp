.class abstract enum LbqU;
.super Ljava/lang/Enum;
.source "Hashing.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbqU;",
        ">;",
        "Lbjv",
        "<",
        "Ljava/util/zip/Checksum;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbqU;

.field private static final synthetic a:[LbqU;

.field public static final enum b:LbqU;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 223
    new-instance v0, LbqV;

    const-string v1, "CRC_32"

    invoke-direct {v0, v1, v2, v4}, LbqV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LbqU;->a:LbqU;

    .line 230
    new-instance v0, LbqW;

    const-string v1, "ADLER_32"

    invoke-direct {v0, v1, v3, v4}, LbqW;-><init>(Ljava/lang/String;II)V

    sput-object v0, LbqU;->b:LbqU;

    .line 222
    const/4 v0, 0x2

    new-array v0, v0, [LbqU;

    sget-object v1, LbqU;->a:LbqU;

    aput-object v1, v0, v2

    sget-object v1, LbqU;->b:LbqU;

    aput-object v1, v0, v3

    sput-object v0, LbqU;->a:[LbqU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 241
    iput p3, p0, LbqU;->a:I

    .line 242
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILbqT;)V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0, p1, p2, p3}, LbqU;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method static synthetic a(LbqU;)I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, LbqU;->a:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LbqU;
    .locals 1

    .prologue
    .line 222
    const-class v0, LbqU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbqU;

    return-object v0
.end method

.method public static values()[LbqU;
    .locals 1

    .prologue
    .line 222
    sget-object v0, LbqU;->a:[LbqU;

    invoke-virtual {v0}, [LbqU;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbqU;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 222
    invoke-virtual {p0}, LbqU;->a()Ljava/util/zip/Checksum;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()Ljava/util/zip/Checksum;
.end method
