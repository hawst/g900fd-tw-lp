.class public LarG;
.super Ljava/lang/Object;
.source "CascadingFadeAnimator.java"

# interfaces
.implements LarE;


# instance fields
.field private a:F

.field private a:I

.field private final a:J

.field private a:Landroid/animation/AnimatorSet$Builder;

.field private final a:Landroid/animation/AnimatorSet;

.field private final a:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(JJ)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LarG;->a:Landroid/animation/AnimatorSet;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, LarG;->a:I

    .line 26
    iput-wide p3, p0, LarG;->a:J

    .line 27
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    iput-object v0, p0, LarG;->a:Landroid/animation/ObjectAnimator;

    .line 28
    iget-object v0, p0, LarG;->a:Landroid/animation/ObjectAnimator;

    const-string v1, "alpha"

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, LarG;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 30
    invoke-virtual {p0}, LarG;->b()V

    .line 31
    return-void
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, LarG;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LarG;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 66
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 45
    iget-object v0, p0, LarG;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 46
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 47
    iget v1, p0, LarG;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LarG;->a:I

    int-to-long v2, v1

    iget-wide v4, p0, LarG;->a:J

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 49
    iget v1, p0, LarG;->a:F

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 51
    iget-object v1, p0, LarG;->a:Landroid/animation/AnimatorSet$Builder;

    if-nez v1, :cond_0

    .line 52
    iget-object v1, p0, LarG;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iput-object v0, p0, LarG;->a:Landroid/animation/AnimatorSet$Builder;

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v1, p0, LarG;->a:Landroid/animation/AnimatorSet$Builder;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 34
    const/4 v0, 0x0

    iput v0, p0, LarG;->a:F

    .line 35
    iget-object v0, p0, LarG;->a:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 36
    return-void

    .line 35
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
