.class public Ladf;
.super Ljava/lang/Object;
.source "DocumentContentSpecCrypter.java"


# instance fields
.field private final a:LQr;

.field private final a:LaGM;

.field private final a:LaKM;

.field private final a:Ladg;


# direct methods
.method public constructor <init>(Ladg;LaGM;LaKM;LQr;)V
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladg;

    iput-object v0, p0, Ladf;->a:Ladg;

    .line 142
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, Ladf;->a:LaGM;

    .line 143
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKM;

    iput-object v0, p0, Ladf;->a:LaKM;

    .line 144
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p0, Ladf;->a:LQr;

    .line 145
    return-void
.end method

.method private static a(LQr;)J
    .locals 2

    .prologue
    .line 211
    const-string v0, "exposedContentValidityMilliseconds"

    const v1, 0xdbba0

    invoke-interface {p0, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method static a([B)[B
    .locals 1

    .prologue
    .line 152
    invoke-static {}, LbqS;->a()LbqQ;

    move-result-object v0

    invoke-interface {v0, p0}, LbqQ;->a([B)LbqL;

    move-result-object v0

    invoke-virtual {v0}, LbqL;->a()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)LaGp;
    .locals 4

    .prologue
    .line 237
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    invoke-virtual {p0, p1}, Ladf;->a(Ljava/lang/String;)Ladd;

    move-result-object v0

    .line 239
    iget-object v1, p0, Ladf;->a:LaKM;

    invoke-interface {v1}, LaKM;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ladd;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 240
    new-instance v0, Lade;

    invoke-direct {v0}, Lade;-><init>()V

    throw v0

    .line 243
    :cond_0
    iget-object v1, p0, Ladf;->a:LaGM;

    .line 244
    invoke-virtual {v0}, Ladd;->a()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, LaGM;->a(J)LaGp;

    move-result-object v0

    .line 245
    if-nez v0, :cond_1

    .line 246
    new-instance v0, Ljava/security/GeneralSecurityException;

    invoke-direct {v0}, Ljava/security/GeneralSecurityException;-><init>()V

    throw v0

    .line 249
    :cond_1
    return-object v0
.end method

.method a(Ljava/lang/String;)Ladd;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    .line 189
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 190
    array-length v1, v0

    if-eq v1, v6, :cond_0

    .line 191
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "Invalid string"

    invoke-direct {v0, v1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1, v3}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v1

    .line 195
    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, v3}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v0

    .line 196
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 197
    iget-object v3, p0, Ladf;->a:Ladg;

    invoke-virtual {v3}, Ladg;->a()Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 198
    invoke-interface {v3}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AES"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, LbiT;->a(Z)V

    .line 199
    const-string v4, "AES/CBC/PKCS5Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v4

    .line 200
    invoke-virtual {v4, v6, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 201
    invoke-virtual {v4, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 203
    invoke-static {v0}, Ladf;->a([B)[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 204
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "Invalid string"

    invoke-direct {v0, v1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_1
    invoke-static {v0}, Ladd;->a([B)Ladd;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGp;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, Ladf;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v0

    iget-object v2, p0, Ladf;->a:LQr;

    invoke-static {v2}, Ladf;->a(LQr;)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 223
    invoke-virtual {p1, v0, v1}, LaGp;->e(J)V

    .line 225
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v2

    invoke-static {v2, v3, v0, v1}, Ladd;->a(JJ)Ladd;

    move-result-object v0

    .line 226
    invoke-virtual {p0, v0}, Ladf;->a(Ladd;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(Ladd;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 161
    invoke-virtual {p1}, Ladd;->a()[B

    move-result-object v0

    .line 162
    invoke-static {v0}, Ladf;->a([B)[B

    move-result-object v0

    .line 163
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v1, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 165
    iget-object v2, p0, Ladf;->a:Ladg;

    invoke-virtual {v2}, Ladg;->a()Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 166
    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AES"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, LbiT;->a(Z)V

    .line 167
    const-string v3, "AES/CBC/PKCS5Padding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 168
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 169
    invoke-virtual {p1}, Ladd;->a()[B

    move-result-object v1

    invoke-virtual {v3, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 173
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 174
    invoke-static {v1, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
