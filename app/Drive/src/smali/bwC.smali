.class LbwC;
.super Ljava/lang/Object;
.source "ProviderLookup.java"

# interfaces
.implements LbwD;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbwD",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbwB;


# direct methods
.method constructor <init>(LbwB;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, LbwC;->a:LbwB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, LbwC;->a:LbwB;

    invoke-static {v0}, LbwB;->a(LbwB;)LbuE;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This Provider cannot be used until the Injector has been created."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 94
    iget-object v0, p0, LbwC;->a:LbwB;

    invoke-static {v0}, LbwB;->a(LbwB;)LbuE;

    move-result-object v0

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Provider<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LbwC;->a:LbwB;

    invoke-static {v1}, LbwB;->a(LbwB;)Lbuv;

    move-result-object v1

    invoke-virtual {v1}, Lbuv;->a()LbuP;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
