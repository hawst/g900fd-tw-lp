.class abstract LoX;
.super Ljava/lang/Object;
.source "AbstractClassLoaderExtender.java"

# interfaces
.implements Lpe;


# instance fields
.field private final a:LoY;

.field private final a:LoZ;

.field protected final a:Lpw;


# direct methods
.method protected constructor <init>(LoZ;LoY;Lpw;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, LoX;->a:LoZ;

    .line 25
    iput-object p2, p0, LoX;->a:LoY;

    .line 26
    iput-object p3, p0, LoX;->a:Lpw;

    .line 27
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 16
    invoke-static {}, LoX;->b()I

    move-result v0

    return v0
.end method

.method private static b()I
    .locals 1

    .prologue
    .line 60
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method


# virtual methods
.method a()V
    .locals 5

    .prologue
    .line 44
    iget-object v0, p0, LoX;->a:LoZ;

    invoke-virtual {v0}, LoZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LoX;->a:LoY;

    invoke-virtual {v0}, LoY;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    :cond_0
    new-instance v0, Lpm;

    const-string v1, "Unsupported SDK version. Current SDK is %d, min SDK is %d, max SDK is %d."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 47
    invoke-static {}, LoX;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LoX;->a:LoZ;

    invoke-virtual {v4}, LoZ;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LoX;->a:LoY;

    invoke-virtual {v4}, LoY;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 45
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lpm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, LoX;->a:Lpw;

    const-string v1, "Archive list is empty. Did you forget to build a multidex apk?"

    invoke-virtual {v0, v1}, Lpw;->a(Ljava/lang/String;)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-virtual {p0}, LoX;->a()V

    .line 39
    invoke-virtual {p0, p1, p2}, LoX;->b(Ljava/util/List;Ljava/io/File;)V

    goto :goto_0
.end method

.method protected abstract b(Ljava/util/List;Ljava/io/File;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation
.end method
