.class public final Lbrx;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/util/List",
            "<",
            "LAt;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/util/List",
            "<",
            "LatE;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/util/Set",
            "<",
            "Laby;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/util/Collection",
            "<",
            "LbuE",
            "<",
            "Laby;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 43
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 44
    iput-object p1, p0, Lbrx;->a:LbrA;

    .line 45
    const-class v0, Ljava/lang/Class;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, Landroid/app/Activity;

    .line 46
    invoke-static {v2}, LbwL;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, Lajo;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 45
    invoke-static {v0, v5}, Lbrx;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lbrx;->a:Lbsk;

    .line 48
    const-class v0, Ljava/util/List;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, LAt;

    aput-object v2, v1, v6

    .line 49
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 48
    invoke-static {v0, v5}, Lbrx;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lbrx;->b:Lbsk;

    .line 51
    const-class v0, Ljava/lang/Integer;

    new-instance v1, Lbwl;

    const-string v2, "punchThumbnailCacheSize"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 52
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 51
    invoke-static {v0, v5}, Lbrx;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lbrx;->c:Lbsk;

    .line 54
    const-class v0, Ljava/util/List;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, LatE;

    aput-object v2, v1, v6

    .line 55
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LatG;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 54
    invoke-static {v0, v5}, Lbrx;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lbrx;->d:Lbsk;

    .line 57
    const-class v0, Ljava/lang/String;

    sget-object v1, LqD;->a:Ljava/lang/Class;

    .line 58
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 57
    invoke-static {v0, v5}, Lbrx;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lbrx;->e:Lbsk;

    .line 60
    const-class v0, Ljava/util/Set;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, Laby;

    aput-object v2, v1, v6

    .line 61
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 60
    invoke-static {v0, v5}, Lbrx;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lbrx;->f:Lbsk;

    .line 63
    const-class v0, Ljava/util/Collection;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, LbuE;

    new-array v3, v3, [Ljava/lang/reflect/Type;

    const-class v4, Laby;

    aput-object v4, v3, v6

    .line 64
    invoke-static {v2, v3}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 63
    invoke-static {v0, v5}, Lbrx;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lbrx;->g:Lbsk;

    .line 66
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 91
    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 108
    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 73
    const-class v0, Ljava/lang/Class;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, Landroid/app/Activity;

    invoke-static {v2}, LbwL;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, Lajo;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lbrx;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbrx;->a(Lbuv;Lbsk;)V

    .line 74
    const-class v0, Ljava/util/List;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, LAt;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lbrx;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbrx;->a(Lbuv;Lbsk;)V

    .line 75
    const-class v0, Ljava/lang/Integer;

    new-instance v1, Lbwl;

    const-string v2, "punchThumbnailCacheSize"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lbrx;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbrx;->a(Lbuv;Lbsk;)V

    .line 76
    const-class v0, Ljava/util/List;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, LatE;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LatG;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lbrx;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbrx;->a(Lbuv;Lbsk;)V

    .line 77
    const-class v0, Ljava/lang/String;

    sget-object v1, LqD;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lbrx;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbrx;->a(Lbuv;Lbsk;)V

    .line 78
    const-class v0, Ljava/util/Set;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, Laby;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lbrx;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbrx;->a(Lbuv;Lbsk;)V

    .line 79
    const-class v0, Ljava/util/Collection;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, LbuE;

    new-array v3, v3, [Ljava/lang/reflect/Type;

    const-class v4, Laby;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lbrx;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbrx;->a(Lbuv;Lbsk;)V

    .line 80
    iget-object v0, p0, Lbrx;->a:Lbsk;

    iget-object v1, p0, Lbrx;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->af:Lbsk;

    invoke-static {v1}, Lbrx;->a(Lbxw;)LbuE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 83
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 100
    .line 102
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method
