.class LabT;
.super Ljava/lang/Object;
.source "GlobalSharingApi.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LabQ;

.field final synthetic a:LabU;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Lqt;

.field final synthetic a:Z


# direct methods
.method constructor <init>(LabQ;LabU;Ljava/lang/String;ZLqt;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, LabT;->a:LabQ;

    iput-object p2, p0, LabT;->a:LabU;

    iput-object p3, p0, LabT;->a:Ljava/lang/String;

    iput-boolean p4, p0, LabT;->a:Z

    iput-object p5, p0, LabT;->a:Lqt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 177
    iget-object v0, p0, LabT;->a:LabQ;

    iget-object v3, p0, LabT;->a:LabU;

    iget-object v4, p0, LabT;->a:Ljava/lang/String;

    invoke-static {v0, v3, v4}, LabQ;->a(LabQ;LabU;Ljava/lang/String;)[Lcom/google/api/services/drive/model/Permission;

    move-result-object v0

    .line 178
    aget-object v3, v0, v2

    .line 179
    iget-boolean v4, p0, LabT;->a:Z

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    .line 181
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 198
    :goto_0
    return-object v0

    .line 182
    :cond_0
    if-eqz v3, :cond_1

    .line 183
    iget-object v4, p0, LabT;->a:LabU;

    iget-object v5, p0, LabT;->a:LabQ;

    .line 184
    invoke-static {v5}, LabQ;->a(LabQ;)Lcom/google/api/services/drive/Drive;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$Permissions;

    move-result-object v5

    iget-object v6, p0, LabT;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/api/services/drive/model/Permission;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v6, v3}, Lcom/google/api/services/drive/Drive$Permissions;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Permissions$Delete;

    move-result-object v3

    .line 183
    invoke-virtual {v4, v3}, LabU;->a(Lcom/google/api/services/drive/DriveRequest;)Ljava/lang/Object;

    .line 188
    :cond_1
    invoke-static {}, LabQ;->a()LbmY;

    move-result-object v3

    iget-object v4, p0, LabT;->a:Lqt;

    invoke-virtual {v3, v4}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 189
    aget-object v0, v0, v1

    .line 190
    iget-object v3, p0, LabT;->a:LabQ;

    iget-object v4, p0, LabT;->a:Lqt;

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/Permission;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, LabQ;->a(LabQ;Lqt;Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    move-result-object v0

    .line 192
    iget-object v3, p0, LabT;->a:LabU;

    iget-object v4, p0, LabT;->a:LabQ;

    .line 193
    invoke-static {v4}, LabQ;->a(LabQ;)Lcom/google/api/services/drive/Drive;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$Permissions;

    move-result-object v4

    iget-object v5, p0, LabT;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Lcom/google/api/services/drive/Drive$Permissions;->a(Ljava/lang/String;Lcom/google/api/services/drive/model/Permission;)Lcom/google/api/services/drive/Drive$Permissions$Insert;

    move-result-object v0

    invoke-virtual {v3, v0}, LabU;->a(Lcom/google/api/services/drive/DriveRequest;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Permission;

    .line 194
    if-eqz v0, :cond_2

    move v0, v1

    .line 196
    :goto_1
    iget-object v1, p0, LabT;->a:LabQ;

    invoke-static {v1}, LabQ;->a(LabQ;)LqK;

    move-result-object v1

    const-string v2, "sharing"

    const-string v3, "changeLinkSharingPermissions"

    iget-object v4, p0, LabT;->a:Lqt;

    .line 197
    invoke-virtual {v4}, Lqt;->name()Ljava/lang/String;

    move-result-object v4

    .line 196
    invoke-virtual {v1, v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 194
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, LabT;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
