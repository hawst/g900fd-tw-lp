.class public LaGB;
.super LaGm;
.source "EntryInCollection.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEw;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(LaEz;JJ)V
    .locals 2

    .prologue
    .line 28
    invoke-static {}, LaEw;->a()LaEw;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 29
    iput-wide p2, p0, LaGB;->a:J

    .line 30
    iput-wide p4, p0, LaGB;->b:J

    .line 31
    return-void
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaGB;
    .locals 6

    .prologue
    .line 60
    sget-object v0, LaEx;->a:LaEx;

    .line 61
    invoke-virtual {v0}, LaEx;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 62
    sget-object v0, LaEx;->b:LaEx;

    .line 63
    invoke-virtual {v0}, LaEx;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 64
    new-instance v0, LaGB;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LaGB;-><init>(LaEz;JJ)V

    .line 66
    invoke-static {}, LaEw;->a()LaEw;

    move-result-object v1

    invoke-virtual {v1}, LaEw;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 65
    invoke-virtual {v0, v2, v3}, LaGB;->c(J)V

    .line 67
    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, LaGB;->a:J

    return-wide v0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 52
    sget-object v0, LaEx;->a:LaEx;

    invoke-virtual {v0}, LaEx;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGB;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 53
    sget-object v0, LaEx;->b:LaEx;

    invoke-virtual {v0}, LaEx;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGB;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 54
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, LaGB;->b:J

    return-wide v0
.end method
