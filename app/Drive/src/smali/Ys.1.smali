.class public abstract LYs;
.super Ljava/lang/Object;
.source "PunchSwipeableSvgViewerBase.java"

# interfaces
.implements LXQ;


# instance fields
.field a:LTO;

.field a:LXD;

.field protected a:LXR;

.field protected a:LYO;

.field private final a:LYu;

.field a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LXR;",
            ">;"
        }
    .end annotation
.end field

.field protected a:Landroid/support/v4/view/ViewPager;

.field protected a:Landroid/view/ViewGroup;

.field protected a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

.field final a:Ljava/lang/String;

.field protected a:LqK;

.field protected a:Z

.field protected b:Z

.field c:Z


# direct methods
.method constructor <init>(Ljava/lang/String;LYu;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-boolean v1, p0, LYs;->a:Z

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, LYs;->b:Z

    .line 137
    iput-boolean v1, p0, LYs;->c:Z

    .line 141
    iput-object p1, p0, LYs;->a:Ljava/lang/String;

    .line 142
    iput-object p2, p0, LYs;->a:LYu;

    .line 143
    return-void
.end method


# virtual methods
.method a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)LXB;
    .locals 5

    .prologue
    .line 162
    iget-object v0, p0, LYs;->a:Ljava/lang/String;

    const-string v1, "in getSlideContentReceiver requestedSlideIndex=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 163
    iget-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 167
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2, p1, p3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(ILXP;)LXB;

    move-result-object v0

    goto :goto_0
.end method

.method public a()LYu;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, LYs;->a:LYu;

    return-object v0
.end method

.method abstract a()LdN;
.end method

.method abstract a()LeT;
.end method

.method public abstract a()V
.end method

.method protected final a(IZ)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()LdN;

    move-result-object v0

    invoke-virtual {v0}, LdN;->a()V

    .line 130
    iget-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 131
    return-void
.end method

.method public a(Landroid/view/ViewGroup;LYO;LXR;LqK;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, LYs;->a:Ljava/lang/String;

    const-string v1, "SKIPPING Creation of svgView as viewPager not null"

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :goto_0
    return-void

    .line 179
    :cond_0
    iput-object p2, p0, LYs;->a:LYO;

    .line 180
    iput-object p3, p0, LYs;->a:LXR;

    .line 181
    iput-object p4, p0, LYs;->a:LqK;

    .line 182
    iput-object p1, p0, LYs;->a:Landroid/view/ViewGroup;

    .line 184
    iget-boolean v0, p0, LYs;->c:Z

    if-nez v0, :cond_1

    .line 185
    iget-object v0, p0, LYs;->a:Ljava/lang/String;

    const-string v1, "SKIPPING Creation of svgView as not active"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {p0}, LYs;->c()V

    goto :goto_0
.end method

.method abstract a(Z)V
.end method

.method abstract b()V
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 150
    iput-boolean p1, p0, LYs;->c:Z

    .line 151
    iget-boolean v0, p0, LYs;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 152
    invoke-virtual {p0}, LYs;->c()V

    .line 154
    :cond_0
    return-void
.end method

.method protected c()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 193
    new-instance v0, LYt;

    iget-object v1, p0, LYs;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LYt;-><init>(LYs;Landroid/content/Context;)V

    iput-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    .line 268
    iget-object v0, p0, LYs;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    iget-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, LYs;->a()LeT;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LeT;)V

    .line 273
    iget-object v0, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 274
    invoke-virtual {p0}, LYs;->a()LdN;

    move-result-object v0

    .line 275
    iget-object v1, p0, LYs;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LdN;)V

    .line 276
    return-void
.end method
