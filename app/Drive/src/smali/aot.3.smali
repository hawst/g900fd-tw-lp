.class public Laot;
.super Ljava/lang/Object;
.source "FetchingRetryRateLimiterProvider.java"

# interfaces
.implements Lbxw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbxw",
        "<",
        "LZS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LQr;


# direct methods
.method public constructor <init>(LQr;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Laot;->a:LQr;

    .line 26
    return-void
.end method


# virtual methods
.method public a()LZS;
    .locals 8

    .prologue
    .line 30
    new-instance v1, LZQ;

    iget-object v0, p0, Laot;->a:LQr;

    const-string v2, "fetchingFailureMinWaitMs"

    const/16 v3, 0xc8

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    iget-object v0, p0, Laot;->a:LQr;

    const-string v4, "fetchingFailureWaitGrowthFactor"

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    .line 32
    invoke-interface {v0, v4, v6, v7}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v4

    iget-object v0, p0, Laot;->a:LQr;

    const-string v6, "fetchingFailureMaxWaitMs"

    const/16 v7, 0x2710

    .line 34
    invoke-interface {v0, v6, v7}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v6, v0

    invoke-direct/range {v1 .. v7}, LZQ;-><init>(JDJ)V

    return-object v1
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Laot;->a()LZS;

    move-result-object v0

    return-object v0
.end method
