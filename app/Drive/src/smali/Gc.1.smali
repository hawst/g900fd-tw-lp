.class public final LGc;
.super Ljava/lang/Object;
.source "DownloadFileDocumentOpenerImpl.java"

# interfaces
.implements LGb;


# instance fields
.field private final a:LGa;

.field private final a:LGj;


# direct methods
.method private constructor <init>(LGa;LGj;)V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGa;

    iput-object v0, p0, LGc;->a:LGa;

    .line 108
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGj;

    iput-object v0, p0, LGc;->a:LGj;

    .line 109
    return-void
.end method

.method synthetic constructor <init>(LGa;LGj;LFZ;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, LGc;-><init>(LGa;LGj;)V

    return-void
.end method


# virtual methods
.method public a(LaGo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, LGc;->a:LGj;

    invoke-virtual {v0}, LGj;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LFS;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 115
    iget-object v0, p0, LGc;->a:LGa;

    .line 116
    invoke-virtual {v0, p1, p2}, LGa;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LFS;)Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "exportFormat"

    iget-object v2, p0, LGc;->a:LGj;

    .line 118
    invoke-virtual {v2}, LGj;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 119
    const-string v1, "DownloadFileDocumentOpener"

    const-string v2, "export url is %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 120
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
