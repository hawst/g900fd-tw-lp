.class public LhW;
.super Ljava/lang/Object;
.source "ActionBarActivityDelegateBase.java"

# interfaces
.implements Lmv;


# instance fields
.field final synthetic a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

.field private a:Lmv;


# direct methods
.method public constructor <init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;Lmv;)V
    .locals 0

    .prologue
    .line 1359
    iput-object p1, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1360
    iput-object p2, p0, LhW;->a:Lmv;

    .line 1361
    return-void
.end method


# virtual methods
.method public a(Lmu;)V
    .locals 2

    .prologue
    .line 1376
    iget-object v0, p0, LhW;->a:Lmv;

    invoke-interface {v0, p1}, Lmv;->a(Lmu;)V

    .line 1377
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_3

    .line 1378
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v1, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1379
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1386
    :cond_0
    :goto_0
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    .line 1387
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->removeAllViews()V

    .line 1389
    :cond_1
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    if-eqz v0, :cond_2

    .line 1391
    :try_start_0
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    iget-object v1, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v1, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    invoke-virtual {v0, v1}, LhN;->b(Lmu;)V
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1396
    :cond_2
    :goto_1
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    .line 1397
    return-void

    .line 1380
    :cond_3
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_0

    .line 1381
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setVisibility(I)V

    .line 1382
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383
    iget-object v0, p0, LhW;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lec;->b(Landroid/view/View;)V

    goto :goto_0

    .line 1392
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public a(Lmu;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1364
    iget-object v0, p0, LhW;->a:Lmv;

    invoke-interface {v0, p1, p2}, Lmv;->a(Lmu;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Lmu;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1372
    iget-object v0, p0, LhW;->a:Lmv;

    invoke-interface {v0, p1, p2}, Lmv;->a(Lmu;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b(Lmu;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1368
    iget-object v0, p0, LhW;->a:Lmv;

    invoke-interface {v0, p1, p2}, Lmv;->b(Lmu;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
