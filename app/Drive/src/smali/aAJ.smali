.class public LaAJ;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, LaDP;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a:[Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-static {p1, v1, v2, p2, v3}, LaDP;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a:I

    invoke-static {p1, v1, v2}, LaDP;->a(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->b:I

    invoke-static {p1, v1, v2}, LaDP;->a(Landroid/os/Parcel;II)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a:[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    invoke-static {p1, v1, v2, p2, v3}, LaDP;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->c:I

    invoke-static {p1, v1, v2}, LaDP;->a(Landroid/os/Parcel;II)V

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->d:I

    invoke-static {p1, v1, v2}, LaDP;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LaDP;->a(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;
    .locals 9

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-static {p1}, LaDN;->b(Landroid/os/Parcel;)I

    move-result v7

    move v5, v6

    move v3, v6

    move-object v2, v4

    move v1, v6

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v7, :cond_0

    invoke-static {p1}, LaDN;->a(Landroid/os/Parcel;)I

    move-result v0

    invoke-static {v0}, LaDN;->a(I)I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    invoke-static {p1, v0}, LaDN;->a(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    sget-object v2, Lcom/google/android/gms/appdatasearch/CorpusId;->CREATOR:LaAq;

    invoke-static {p1, v0, v2}, LaDN;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/CorpusId;

    move-object v2, v0

    goto :goto_0

    :sswitch_1
    invoke-static {p1, v0}, LaDN;->b(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :sswitch_2
    invoke-static {p1, v0}, LaDN;->b(Landroid/os/Parcel;I)I

    move-result v3

    goto :goto_0

    :sswitch_3
    sget-object v4, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->CREATOR:LaAy;

    invoke-static {p1, v0, v4}, LaDN;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    move-object v4, v0

    goto :goto_0

    :sswitch_4
    invoke-static {p1, v0}, LaDN;->b(Landroid/os/Parcel;I)I

    move-result v5

    goto :goto_0

    :sswitch_5
    invoke-static {p1, v0}, LaDN;->b(Landroid/os/Parcel;I)I

    move-result v6

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v7, :cond_1

    new-instance v0, LaDO;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Overread allowed size end="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LaDO;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;-><init>(I[Lcom/google/android/gms/appdatasearch/CorpusId;I[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;II)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(I)[Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, LaAJ;->a(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, LaAJ;->a(I)[Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    move-result-object v0

    return-object v0
.end method
