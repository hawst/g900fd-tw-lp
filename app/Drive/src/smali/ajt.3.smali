.class public Lajt;
.super Ljava/lang/Object;
.source "GuiceUtils.java"


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lajt;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 28
    const/4 v0, 0x0

    .line 30
    :try_start_0
    const-string v2, "android.test.mock.MockContext"

    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 34
    :goto_0
    sput-object v0, Lajt;->a:Ljava/lang/Class;

    .line 35
    return-void

    .line 31
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 114
    const-string v0, "provided Context is null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, p0

    .line 117
    :goto_0
    invoke-static {v1}, Lajt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    :goto_1
    return-object v1

    .line 120
    :cond_0
    instance-of v0, v1, Landroid/content/ContextWrapper;

    if-nez v0, :cond_1

    .line 121
    invoke-static {v1}, Lajt;->c(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 124
    check-cast v0, Landroid/content/ContextWrapper;

    .line 125
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    .line 129
    if-eqz p0, :cond_2

    move-object v1, p0

    .line 134
    goto :goto_0

    .line 132
    :cond_2
    invoke-static {v1}, Lajt;->c(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)Lbuu;
    .locals 5

    .prologue
    .line 47
    const-string v0, "context is null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 50
    :goto_0
    invoke-static {v0}, Lajt;->b(Landroid/content/Context;)Lbuu;

    move-result-object v1

    .line 51
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 66
    :goto_1
    return-object v0

    .line 55
    :cond_0
    invoke-static {v0}, Lajt;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 56
    if-nez v1, :cond_1

    .line 63
    invoke-static {v0}, Lajt;->c(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v2

    .line 64
    invoke-static {v2}, Lajt;->b(Landroid/content/Context;)Lbuu;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 66
    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 61
    goto :goto_0

    .line 69
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Neither "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", its base Context "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", nor the application Context "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " implement InjectorProvider nor registered to InjectorRegistry"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 138
    instance-of v0, p0, Landroid/app/Application;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/app/Activity;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/app/Service;

    if-nez v0, :cond_0

    sget-object v0, Lajt;->a:Ljava/lang/Class;

    if-eqz v0, :cond_1

    sget-object v0, Lajt;->a:Ljava/lang/Class;

    .line 141
    invoke-virtual {v0, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Landroid/content/Context;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 75
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 84
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    .line 79
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move-object v0, v1

    .line 81
    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Lbuu;
    .locals 1

    .prologue
    .line 89
    instance-of v0, p0, Laju;

    if-eqz v0, :cond_0

    .line 90
    check-cast p0, Laju;

    invoke-interface {p0}, Laju;->a()Lbuu;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    .line 92
    :cond_0
    sget-object v0, Lajv;->a:Lajv;

    invoke-virtual {v0, p0}, Lajv;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;)Landroid/content/Context;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 145
    instance-of v0, p0, Landroid/app/Application;

    if-eqz v0, :cond_0

    .line 154
    :goto_0
    return-object p0

    .line 149
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 150
    if-eqz v3, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "%s getApplicationContext() returns null"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p0, v5, v2

    invoke-static {v0, v4, v5}, Lbso;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 152
    instance-of v0, v3, Landroid/app/Application;

    const-string v4, "%s getApplicationContext() does not return an instance of Application"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, Lbso;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    move-object p0, v3

    .line 154
    goto :goto_0

    :cond_1
    move v0, v2

    .line 150
    goto :goto_1
.end method
