.class final enum Lama;
.super Ljava/lang/Enum;
.source "MediaStoreUtilities.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lama;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lama;

.field private static final synthetic a:[Lama;

.field public static final enum b:Lama;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25
    new-instance v0, Lama;

    const-string v1, "PHOTO"

    const-string v2, "photo"

    const-string v3, "_id"

    invoke-direct {v0, v1, v4, v2, v3}, Lama;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lama;->a:Lama;

    .line 26
    new-instance v0, Lama;

    const-string v1, "VIDEO"

    const-string v2, "video"

    const-string v3, "_id"

    invoke-direct {v0, v1, v5, v2, v3}, Lama;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lama;->b:Lama;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lama;

    sget-object v1, Lama;->a:Lama;

    aput-object v1, v0, v4

    sget-object v1, Lama;->b:Lama;

    aput-object v1, v0, v5

    sput-object v0, Lama;->a:[Lama;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lama;->a:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lama;->b:Ljava/lang/String;

    .line 34
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lama;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lama;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lama;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lama;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lama;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lama;

    return-object v0
.end method

.method public static values()[Lama;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lama;->a:[Lama;

    invoke-virtual {v0}, [Lama;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lama;

    return-object v0
.end method
