.class LWl;
.super LEC;
.source "CommonPreferencesInstaller.java"


# instance fields
.field final synthetic a:LWa;


# direct methods
.method constructor <init>(LWa;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, LWl;->a:LWa;

    invoke-direct {p0}, LEC;-><init>()V

    return-void
.end method

.method static synthetic a(LWl;Z)V
    .locals 0

    .prologue
    .line 371
    invoke-direct {p0, p1}, LWl;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, LWl;->a:LWa;

    invoke-static {v0}, LWa;->c(LWa;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, LWl;->a:LWa;

    invoke-static {v0}, LWa;->c(LWa;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 415
    if-nez p1, :cond_0

    .line 416
    iget-object v0, p0, LWl;->a:LWa;

    iget-object v0, v0, LWa;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahK;

    invoke-interface {v0}, LahK;->b()V

    .line 419
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 374
    invoke-static {p1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    sget v1, Lxi;->pin_sync_broadband_warning_title:I

    .line 375
    invoke-virtual {v0, v1}, LEU;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->pin_sync_broadband_warning_message:I

    .line 376
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, LWn;

    invoke-direct {v2, p0}, LWn;-><init>(LWl;)V

    .line 377
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->pin_sync_broadband_warning_learn_more:I

    new-instance v2, LWm;

    invoke-direct {v2, p0, p1}, LWm;-><init>(LWl;Landroid/content/Context;)V

    .line 384
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    .line 398
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 400
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LWl;->a(Z)V

    .line 406
    return-void
.end method
