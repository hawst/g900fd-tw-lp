.class public final LaHb;
.super Ljava/lang/Object;
.source "ResourceSpec.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 4

    .prologue
    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 20
    new-instance v2, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/gms/drive/database/data/ResourceSpec;-><init>(LaFO;Ljava/lang/String;LaHb;)V

    return-object v2
.end method

.method public a(I)[Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 25
    new-array v0, p1, [Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1}, LaHb;->a(Landroid/os/Parcel;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1}, LaHb;->a(I)[Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method
