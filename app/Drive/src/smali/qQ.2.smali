.class public abstract LqQ;
.super Ljava/lang/Object;
.source "AbstractAccountSpecificFeature.java"

# interfaces
.implements Lrg;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final a:Lrx;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lrx;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LqQ;->a:Ljava/lang/String;

    .line 19
    iput-object p2, p0, LqQ;->a:Lrx;

    .line 20
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, LqQ;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a()Lrx;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LqQ;->a:Lrx;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 35
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Feature[%s, %s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LqQ;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LqQ;->a:Lrx;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
