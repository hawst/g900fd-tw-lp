.class public LTg;
.super LTl;
.source "AuthenticatedHttpContentSource.java"


# instance fields
.field private final a:LTh;


# direct methods
.method public constructor <init>(LTh;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, LTl;-><init>(LTF;)V

    .line 31
    iput-object p1, p0, LTg;->a:LTh;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;LaFO;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 48
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :try_start_0
    iget-object v0, p0, LTg;->a:LTh;

    .line 52
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x0

    .line 51
    invoke-static {v0, p2, v1, v2, v3}, LTs;->a(LTh;LaFO;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 53
    invoke-virtual {p0, p1, v0}, LTg;->a(Landroid/net/Uri;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 54
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, LTr;

    const-string v2, "Authenticator malfunctioning."

    invoke-direct {v1, v2, v0}, LTr;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 57
    :catch_1
    move-exception v0

    .line 58
    new-instance v1, LTt;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid redirect URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LTt;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
