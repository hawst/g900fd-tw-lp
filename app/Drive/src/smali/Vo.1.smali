.class LVo;
.super Ljava/lang/Object;
.source "DelayedRemoveEntriesOperation.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LVm;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LVm;LbmF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    iput-object p1, p0, LVo;->a:LVm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmF;

    iput-object v0, p0, LVo;->a:LbmF;

    .line 49
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, LVo;->a:LbmF;

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 54
    iget-object v0, p0, LVo;->a:LbmF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 55
    iget-object v1, p0, LVo;->a:LVm;

    .line 56
    invoke-static {v1}, LVm;->a(LVm;)LVg;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {v1, v0}, LVg;->a(LaFO;)LVf;

    move-result-object v2

    .line 57
    iget-object v0, p0, LVo;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 58
    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v1

    .line 59
    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 60
    :goto_1
    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v2, v0, v1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    goto :goto_0

    .line 59
    :cond_0
    invoke-interface {v1}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_1

    .line 63
    :cond_1
    iget-object v0, p0, LVo;->a:LVm;

    invoke-static {v0}, LVm;->a(LVm;)LVg;

    move-result-object v0

    invoke-virtual {v2}, LVf;->a()LVd;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->b(LVd;)V

    .line 64
    iget-object v0, p0, LVo;->a:LVm;

    iget-object v1, p0, LVo;->a:LbmF;

    invoke-static {v0, v1}, LVm;->a(LVm;LbmF;)V

    .line 66
    :cond_2
    return-void
.end method
