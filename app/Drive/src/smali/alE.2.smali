.class public LalE;
.super Ljava/lang/Object;
.source "GenericQueuedExecutorService.java"

# interfaces
.implements LalD;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/lang/Runnable;",
        ":",
        "Ljava/util/concurrent/Future",
        "<*>;>",
        "Ljava/lang/Object;",
        "LalD",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LbiG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiG",
            "<TT;TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/ExecutorService;Lalf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lalf",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/BlockingQueue;

    iput-object v0, p0, LalE;->a:Ljava/util/concurrent/BlockingQueue;

    .line 63
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, LalE;->a:Ljava/util/concurrent/ExecutorService;

    .line 64
    new-instance v0, LalF;

    invoke-direct {v0, p0, p2}, LalF;-><init>(LalE;Lalf;)V

    iput-object v0, p0, LalE;->a:LbiG;

    .line 71
    return-void
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;Lalf;)LalE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Runnable;",
            ":",
            "LakM",
            "<*>;:",
            "Ljava/util/concurrent/Future",
            "<*>;>(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lalf",
            "<TT;>;)",
            "LalE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, LalE;

    invoke-direct {v0, p0, p1}, LalE;-><init>(Ljava/util/concurrent/ExecutorService;Lalf;)V

    return-object v0
.end method

.method static synthetic a(LalE;)Ljava/util/concurrent/BlockingQueue;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, LalE;->a:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, LalE;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 98
    return-void
.end method

.method public a(Ljava/lang/Runnable;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 75
    iget-object v0, p0, LalE;->a:LbiG;

    invoke-interface {v0, p1}, LbiG;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 76
    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, LalE;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, LalG;

    invoke-direct {v2, p0}, LalG;-><init>(LalE;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move v0, v1

    .line 91
    :goto_0
    return v0

    .line 90
    :cond_0
    check-cast v0, Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 91
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 125
    invoke-static {}, LbnG;->a()Ljava/util/LinkedList;

    move-result-object v0

    .line 126
    iget-object v1, p0, LalE;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;)I

    .line 127
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 128
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    .line 130
    :cond_0
    return-void
.end method
