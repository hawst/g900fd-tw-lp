.class public abstract enum LFf;
.super Ljava/lang/Enum;
.source "InternalReleaseDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LFf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LFf;

.field private static final synthetic a:[LFf;

.field public static final enum b:LFf;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, LFg;

    const-string v1, "NO_ACCEPTANCE_REQUIRED"

    invoke-direct {v0, v1, v2}, LFg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LFf;->a:LFf;

    .line 71
    new-instance v0, LFh;

    const-string v1, "ACCEPTANCE_REQUIRED_ONCE"

    invoke-direct {v0, v1, v3}, LFh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LFf;->b:LFf;

    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [LFf;

    sget-object v1, LFf;->a:LFf;

    aput-object v1, v0, v2

    sget-object v1, LFf;->b:LFf;

    aput-object v1, v0, v3

    sput-object v0, LFf;->a:[LFf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILFe;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, LFf;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LFf;
    .locals 1

    .prologue
    .line 57
    const-class v0, LFf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LFf;

    return-object v0
.end method

.method public static values()[LFf;
    .locals 1

    .prologue
    .line 57
    sget-object v0, LFf;->a:[LFf;

    invoke-virtual {v0}, [LFf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LFf;

    return-object v0
.end method


# virtual methods
.method public abstract a(LM;Landroid/content/Context;)V
.end method

.method public abstract a(LM;Landroid/content/Context;)Z
.end method
