.class public LOt;
.super Ljava/lang/Object;
.source "IncomingTabsDocListHeader.java"

# interfaces
.implements LBt;


# instance fields
.field private final a:LAP;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method public constructor <init>(Laja;LAP;LtK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "LAP;",
            "LtK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, LOt;->a:Laja;

    .line 86
    iput-object p2, p0, LOt;->a:LAP;

    .line 87
    iput-object p3, p0, LOt;->a:LtK;

    .line 88
    return-void
.end method

.method static synthetic a(LOt;)LAP;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LOt;->a:LAP;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 93
    invoke-static {}, LamV;->a()V

    .line 95
    iget-object v0, p0, LOt;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 96
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 98
    sget v3, LOU;->incoming_view_tabs:I

    invoke-virtual {v1, v3, v9, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 100
    const v1, 0x1020012

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TabHost;

    .line 101
    invoke-virtual {v1}, Landroid/widget/TabHost;->setup()V

    .line 103
    invoke-static {}, LOw;->values()[LOw;

    move-result-object v4

    array-length v5, v4

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 104
    iget-object v7, v6, LOw;->a:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v7

    .line 105
    iget v8, v6, LOw;->a:I

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 106
    iget v6, v6, LOw;->b:I

    invoke-virtual {v7, v6}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 107
    invoke-virtual {v1, v7}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 110
    :cond_0
    new-instance v0, LOu;

    invoke-direct {v0, p0}, LOu;-><init>(LOt;)V

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 129
    new-instance v0, LOv;

    invoke-direct {v0, p0, v1}, LOv;-><init>(LOt;Landroid/widget/TabHost;)V

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 139
    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 140
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 141
    return-object v3
.end method

.method public a(Landroid/view/View;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 147
    invoke-static {}, LamV;->a()V

    .line 149
    iget-object v0, p0, LOt;->a:LtK;

    sget-object v1, LOE;->a:Lrg;

    .line 150
    invoke-interface {p2}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LaFO;

    move-result-object v2

    .line 149
    invoke-interface {v0, v1, v2}, LtK;->a(Lrg;LaFO;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 178
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LbmF;

    move-result-object v0

    .line 156
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 157
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 161
    :cond_1
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    invoke-static {v0}, LOw;->a(LCl;)LOw;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_3

    .line 163
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 164
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    .line 165
    if-eqz v0, :cond_2

    .line 166
    iget-object v2, v1, LOw;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 170
    :cond_2
    iget-object v0, p0, LOt;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 171
    iget v1, v1, LOw;->a:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 172
    const v2, 0x1020002

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 173
    invoke-static {v0, v2, v1}, LUs;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 176
    :cond_3
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
