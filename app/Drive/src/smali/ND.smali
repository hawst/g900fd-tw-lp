.class public LND;
.super Ljava/lang/Object;
.source "AudioPlayerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;LNC;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, LND;-><init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->setResult(I)V

    .line 101
    iget-object v0, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->finish()V

    .line 102
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 109
    const-string v0, "AudioPlayerActivity"

    const-string v1, "Error during audio playback %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 110
    iget-object v0, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0, v5}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;Z)Z

    .line 111
    return v5
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    .line 91
    const-string v0, "AudioPlayerActivity"

    const-string v1, "AudioPlayer is prepared."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Lcom/google/android/apps/docs/drive/media/AudioController;

    move-result-object v0

    iget-object v1, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v1}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)LNB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/drive/media/AudioController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    .line 93
    iget-object v0, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Lcom/google/android/apps/docs/drive/media/AudioController;

    move-result-object v0

    iget-object v1, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    sget v2, LNJ;->top_frame:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/drive/media/AudioController;->setAnchorView(Landroid/view/View;)V

    .line 94
    iget-object v0, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)LNB;

    move-result-object v0

    invoke-virtual {v0}, LNB;->start()V

    .line 95
    iget-object v0, p0, LND;->a:Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Lcom/google/android/apps/docs/drive/media/AudioController;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/drive/media/AudioController;->setShowing(Z)V

    .line 96
    return-void
.end method
