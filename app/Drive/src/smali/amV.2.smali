.class public LamV;
.super Ljava/lang/Object;
.source "Threads.java"


# direct methods
.method public static a()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 28
    if-nez v0, :cond_0

    .line 32
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-static {v0}, LamV;->a(Landroid/os/Looper;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    goto :goto_0
.end method

.method private static a(Landroid/os/Looper;)Z
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 38
    if-nez v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-static {v0}, LamV;->a(Landroid/os/Looper;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
