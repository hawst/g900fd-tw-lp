.class public LaCn;
.super Ljava/lang/Object;

# interfaces
.implements LaCe;


# instance fields
.field private final a:LaCs;

.field private final a:LaRf;

.field private final a:Ljava/lang/Object;


# direct methods
.method static synthetic a(LaCn;)LaCs;
    .locals 1

    iget-object v0, p0, LaCn;->a:LaCs;

    return-object v0
.end method

.method static synthetic a(LaCn;)LaRf;
    .locals 1

    iget-object v0, p0, LaCn;->a:LaRf;

    return-object v0
.end method

.method static synthetic a(LaCn;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LaCn;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a(LaCV;LaCh;Z)LaCZ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaCV;",
            "LaCh;",
            "Z)",
            "LaCZ",
            "<",
            "LaCr;",
            ">;"
        }
    .end annotation

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v6}, LaCn;->a(LaCV;LaCh;ZJLorg/json/JSONObject;)LaCZ;

    move-result-object v0

    return-object v0
.end method

.method public a(LaCV;LaCh;ZJLorg/json/JSONObject;)LaCZ;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaCV;",
            "LaCh;",
            "ZJ",
            "Lorg/json/JSONObject;",
            ")",
            "LaCZ",
            "<",
            "LaCr;",
            ">;"
        }
    .end annotation

    new-instance v1, LaCo;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, LaCo;-><init>(LaCn;LaCV;LaCh;ZJLorg/json/JSONObject;)V

    invoke-interface {p1, v1}, LaCV;->b(LaDi;)LaDi;

    move-result-object v0

    return-object v0
.end method

.method public a(LaCV;Lorg/json/JSONObject;)LaCZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaCV;",
            "Lorg/json/JSONObject;",
            ")",
            "LaCZ",
            "<",
            "LaCr;",
            ">;"
        }
    .end annotation

    new-instance v0, LaCp;

    invoke-direct {v0, p0, p1, p2}, LaCp;-><init>(LaCn;LaCV;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, LaCV;->b(LaDi;)LaDi;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, LaCn;->a:LaRf;

    invoke-virtual {v0, p3}, LaRf;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b(LaCV;Lorg/json/JSONObject;)LaCZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaCV;",
            "Lorg/json/JSONObject;",
            ")",
            "LaCZ",
            "<",
            "LaCr;",
            ">;"
        }
    .end annotation

    new-instance v0, LaCq;

    invoke-direct {v0, p0, p1, p2}, LaCq;-><init>(LaCn;LaCV;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, LaCV;->b(LaDi;)LaDi;

    move-result-object v0

    return-object v0
.end method
