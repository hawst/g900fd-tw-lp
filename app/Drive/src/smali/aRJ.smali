.class final LaRJ;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field final synthetic a:LaRI;

.field private final a:LaRK;

.field private a:Landroid/content/ComponentName;

.field private a:Landroid/os/IBinder;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LaRw",
            "<*>.aRB;>;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>(LaRI;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, LaRJ;->a:LaRI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, LaRJ;->a:Ljava/lang/String;

    new-instance v0, LaRK;

    invoke-direct {v0, p0}, LaRK;-><init>(LaRJ;)V

    iput-object v0, p0, LaRJ;->a:LaRK;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaRJ;->a:Ljava/util/HashSet;

    const/4 v0, 0x0

    iput v0, p0, LaRJ;->a:I

    return-void
.end method

.method static synthetic a(LaRJ;I)I
    .locals 0

    iput p1, p0, LaRJ;->a:I

    return p1
.end method

.method static synthetic a(LaRJ;Landroid/content/ComponentName;)Landroid/content/ComponentName;
    .locals 0

    iput-object p1, p0, LaRJ;->a:Landroid/content/ComponentName;

    return-object p1
.end method

.method static synthetic a(LaRJ;Landroid/os/IBinder;)Landroid/os/IBinder;
    .locals 0

    iput-object p1, p0, LaRJ;->a:Landroid/os/IBinder;

    return-object p1
.end method

.method static synthetic a(LaRJ;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, LaRJ;->a:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LaRJ;->a:I

    return v0
.end method

.method public a()LaRK;
    .locals 1

    iget-object v0, p0, LaRJ;->a:LaRK;

    return-object v0
.end method

.method public a()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, LaRJ;->a:Landroid/content/ComponentName;

    return-object v0
.end method

.method public a()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, LaRJ;->a:Landroid/os/IBinder;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaRJ;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(LaRB;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaRw",
            "<*>.aRB;)V"
        }
    .end annotation

    iget-object v0, p0, LaRJ;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, LaRJ;->a:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, LaRJ;->a:Z

    return v0
.end method

.method public a(LaRB;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaRw",
            "<*>.aRB;)Z"
        }
    .end annotation

    iget-object v0, p0, LaRJ;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(LaRB;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaRw",
            "<*>.aRB;)V"
        }
    .end annotation

    iget-object v0, p0, LaRJ;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, LaRJ;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    return v0
.end method
