.class LbhM;
.super Lcom/google/bionics/scanner/unveil/util/HoneycombAsyncTask;
.source "CameraProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/bionics/scanner/unveil/util/HoneycombAsyncTask",
        "<",
        "Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/res/Resources;

.field final synthetic a:LbhL;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Ljava/util/Map;


# direct methods
.method constructor <init>(LbhL;Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, LbhM;->a:LbhL;

    iput-object p2, p0, LbhM;->a:Ljava/lang/String;

    iput-object p3, p0, LbhM;->a:Ljava/util/Map;

    iput-object p4, p0, LbhM;->a:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/util/HoneycombAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;
    .locals 5

    .prologue
    .line 97
    invoke-static {}, LbhL;->a()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, LbhM;->a:LbhL;

    iget-object v2, p0, LbhM;->a:Ljava/lang/String;

    iget-object v3, p0, LbhM;->a:Ljava/util/Map;

    iget-object v4, p0, LbhM;->a:Landroid/content/res/Resources;

    invoke-static {v0, v2, v3, v4}, LbhL;->a(LbhL;Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, LbhM;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V
    .locals 1

    .prologue
    .line 104
    if-eqz p1, :cond_0

    .line 105
    iget-object v0, p0, LbhM;->a:LbhL;

    invoke-static {v0}, LbhL;->a(LbhL;)LbhO;

    move-result-object v0

    invoke-interface {v0, p1}, LbhO;->onCameraAcquired(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, LbhM;->a:LbhL;

    invoke-static {v0}, LbhL;->a(LbhL;)LbhO;

    move-result-object v0

    invoke-interface {v0}, LbhO;->onCameraAcquisitionError()V

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 94
    check-cast p1, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-virtual {p0, p1}, LbhM;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V

    return-void
.end method
