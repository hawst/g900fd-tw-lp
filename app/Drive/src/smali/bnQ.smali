.class abstract enum LbnQ;
.super Ljava/lang/Enum;
.source "MapMaker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbnQ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbnQ;

.field private static final synthetic a:[LbnQ;

.field public static final enum b:LbnQ;

.field public static final enum c:LbnQ;

.field public static final enum d:LbnQ;

.field public static final enum e:LbnQ;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 691
    new-instance v0, LbnR;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, LbnR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbnQ;->a:LbnQ;

    .line 705
    new-instance v0, LbnS;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, LbnS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbnQ;->b:LbnQ;

    .line 717
    new-instance v0, LbnT;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, LbnT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbnQ;->c:LbnQ;

    .line 729
    new-instance v0, LbnU;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, LbnU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbnQ;->d:LbnQ;

    .line 741
    new-instance v0, LbnV;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, LbnV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbnQ;->e:LbnQ;

    .line 686
    const/4 v0, 0x5

    new-array v0, v0, [LbnQ;

    sget-object v1, LbnQ;->a:LbnQ;

    aput-object v1, v0, v2

    sget-object v1, LbnQ;->b:LbnQ;

    aput-object v1, v0, v3

    sget-object v1, LbnQ;->c:LbnQ;

    aput-object v1, v0, v4

    sget-object v1, LbnQ;->d:LbnQ;

    aput-object v1, v0, v5

    sget-object v1, LbnQ;->e:LbnQ;

    aput-object v1, v0, v6

    sput-object v0, LbnQ;->a:[LbnQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 686
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbnO;)V
    .locals 0

    .prologue
    .line 686
    invoke-direct {p0, p1, p2}, LbnQ;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbnQ;
    .locals 1

    .prologue
    .line 686
    const-class v0, LbnQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbnQ;

    return-object v0
.end method

.method public static values()[LbnQ;
    .locals 1

    .prologue
    .line 686
    sget-object v0, LbnQ;->a:[LbnQ;

    invoke-virtual {v0}, [LbnQ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbnQ;

    return-object v0
.end method
