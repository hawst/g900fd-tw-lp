.class public LQD;
.super Ljava/lang/Object;
.source "ClientFlagSynchronizerImpl.java"

# interfaces
.implements LQB;


# instance fields
.field private final a:LQE;

.field private final a:LQr;

.field private final a:LQt;

.field private final a:LQy;

.field private final a:LTh;


# direct methods
.method public constructor <init>(LTh;LQr;LQt;LQy;LQE;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, LQD;->a:LTh;

    .line 54
    iput-object p2, p0, LQD;->a:LQr;

    .line 55
    iput-object p3, p0, LQD;->a:LQt;

    .line 56
    iput-object p4, p0, LQD;->a:LQy;

    .line 57
    iput-object p5, p0, LQD;->a:LQE;

    .line 58
    return-void
.end method

.method private a(LaFO;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 4

    .prologue
    .line 140
    :try_start_0
    iget-object v0, p0, LQD;->a:LTh;

    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v0, p1, v1, v2, v3}, LTs;->a(LTh;LaFO;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_4
    .catch LTt; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v0

    return-object v0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    new-instance v1, LQC;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client protocol error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 145
    :catch_1
    move-exception v0

    .line 146
    new-instance v1, LQC;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IO Exception opening: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 147
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 148
    :catch_2
    move-exception v0

    .line 149
    new-instance v1, LQC;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Authentication problem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 151
    :catch_3
    move-exception v0

    .line 152
    new-instance v1, LQC;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Authentication problem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 154
    :catch_4
    move-exception v0

    .line 155
    new-instance v1, LQC;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 156
    :catch_5
    move-exception v0

    .line 157
    new-instance v1, LQC;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Redirection error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 130
    :try_start_0
    iget-object v0, p0, LQD;->a:LQt;

    iget-object v1, p0, LQD;->a:LQr;

    invoke-interface {v0, v1}, LQt;->a(LQr;)V

    .line 131
    iget-object v0, p0, LQD;->a:LQr;

    invoke-interface {v0}, LQr;->a()V
    :try_end_0
    .catch LQu; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    new-instance v1, LQC;

    const-string v2, "Error saving client flags: "

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a(LaFO;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 66
    iget-object v0, p0, LQD;->a:LQt;

    invoke-interface {v0}, LQt;->a()Z

    move-result v0

    const-string v2, "Client flags not loaded on startup"

    invoke-static {v0, v2}, LbiT;->b(ZLjava/lang/Object;)V

    .line 68
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v0

    .line 69
    iget-object v2, p0, LQD;->a:LQE;

    invoke-interface {v2, v0}, LQE;->a(Lrx;)Ljava/lang/String;

    move-result-object v2

    .line 70
    const-string v3, "ClientFlagSynchronizerImpl"

    const-string v4, "Found ClientMode=%s, server=%s, path=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    aput-object v2, v5, v7

    iget-object v6, v0, Lrx;->c:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 76
    :try_start_0
    invoke-direct {p0, p1, v2}, LQD;->a(LaFO;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 77
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 78
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_2

    .line 79
    const-string v3, "ClientFlagSynchronizerImpl"

    const-string v5, "Unable to load resource: %s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 80
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    aput-object v2, v6, v4

    .line 79
    invoke-static {v3, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch LQz; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v1

    .line 95
    :goto_0
    if-eqz v2, :cond_0

    .line 97
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 103
    :cond_0
    :goto_1
    iget-object v2, p0, LQD;->a:LTh;

    invoke-interface {v2}, LTh;->b()V

    .line 110
    if-eqz p2, :cond_5

    .line 117
    :goto_2
    if-eqz p2, :cond_1

    .line 119
    :try_start_2
    iget-object v0, p0, LQD;->a:LQy;

    iget-object v1, p0, LQD;->a:LQr;

    const/4 v2, 0x0

    invoke-interface {v0, p2, v1, v2}, LQy;->a(Ljava/lang/String;LQr;Z)V
    :try_end_2
    .catch LQz; {:try_start_2 .. :try_end_2} :catch_3

    .line 124
    :cond_1
    invoke-direct {p0}, LQD;->a()V

    .line 125
    return-void

    .line 82
    :cond_2
    :try_start_3
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 83
    if-eqz v2, :cond_4

    .line 84
    iget-object v3, p0, LQD;->a:LTh;

    invoke-interface {v3, v2}, LTh;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch LQz; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 85
    :try_start_4
    iget-object v3, p0, LQD;->a:LQy;

    iget-object v4, p0, LQD;->a:LQr;

    const/4 v5, 0x1

    invoke-interface {v3, v2, v4, v5}, LQy;->a(Ljava/io/InputStream;LQr;Z)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch LQz; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 91
    :goto_3
    :try_start_5
    new-instance v2, LQC;

    const-string v3, "Error downloading client flags file: "

    invoke-direct {v2, v3, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 95
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_3

    .line 97
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 103
    :cond_3
    :goto_5
    iget-object v1, p0, LQD;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    throw v0

    .line 87
    :cond_4
    :try_start_7
    new-instance v0, Ljava/io/IOException;

    const-string v2, "The entity is null."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch LQz; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 90
    :catch_1
    move-exception v0

    goto :goto_3

    .line 92
    :catch_2
    move-exception v0

    .line 93
    :goto_6
    :try_start_8
    new-instance v2, LQC;

    const-string v3, "Error parsing client flags file: "

    invoke-direct {v2, v3, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 112
    :cond_5
    iget-object v2, v0, Lrx;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 113
    iget-object p2, v0, Lrx;->c:Ljava/lang/String;

    goto :goto_2

    :cond_6
    move-object p2, v1

    .line 115
    goto :goto_2

    .line 120
    :catch_3
    move-exception v0

    .line 121
    new-instance v1, LQC;

    const-string v2, "Error parsing local client flags file: "

    invoke-direct {v1, v2, v0}, LQC;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 98
    :catch_4
    move-exception v2

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_5

    .line 95
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 92
    :catch_6
    move-exception v0

    move-object v1, v2

    goto :goto_6
.end method
