.class public LajZ;
.super Ljava/lang/Object;
.source "ActivityUpdater.java"


# instance fields
.field private final a:Lakc;

.field private final a:Lake;

.field private final a:Lamw;

.field private a:Landroid/database/ContentObserver;

.field private final a:Landroid/os/Handler;

.field private final a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lakc;Lake;)V
    .locals 5

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LajZ;->a:Landroid/os/Handler;

    .line 86
    iput-object p1, p0, LajZ;->a:Lakc;

    .line 87
    iput-object p2, p0, LajZ;->a:Lake;

    .line 88
    new-instance v0, Laka;

    invoke-direct {v0, p0}, Laka;-><init>(LajZ;)V

    iput-object v0, p0, LajZ;->a:Ljava/lang/Runnable;

    .line 98
    sget-object v0, Lamy;->a:Lamx;

    iget-object v1, p0, LajZ;->a:Ljava/lang/Runnable;

    const/16 v2, 0x7d0

    new-instance v3, LalI;

    iget-object v4, p0, LajZ;->a:Landroid/os/Handler;

    invoke-direct {v3, v4}, LalI;-><init>(Landroid/os/Handler;)V

    const-string v4, "ActivityUpdater"

    invoke-interface {v0, v1, v2, v3, v4}, Lamx;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lamw;

    move-result-object v0

    iput-object v0, p0, LajZ;->a:Lamw;

    .line 100
    return-void
.end method

.method static synthetic a(LajZ;)Lakc;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LajZ;->a:Lakc;

    return-object v0
.end method

.method static synthetic a(LajZ;)Lamw;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LajZ;->a:Lamw;

    return-object v0
.end method

.method static synthetic a(LajZ;)Landroid/database/ContentObserver;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LajZ;->a:Landroid/database/ContentObserver;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, LajZ;->a:Lamw;

    invoke-interface {v0}, Lamw;->b()V

    .line 149
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, LajZ;->a:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "ActivityUpdater"

    const-string v1, "clearing onAccountUpdatedObserver"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, LajZ;->a:Lake;

    iget-object v1, p0, LajZ;->a:Landroid/database/ContentObserver;

    invoke-interface {v0, p1, v1}, Lake;->b(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, LajZ;->a:Landroid/database/ContentObserver;

    .line 111
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, LajZ;->a:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 119
    const-string v0, "ActivityUpdater"

    const-string v1, "registering onAccountUpdatedObserver"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    new-instance v0, Lakb;

    iget-object v1, p0, LajZ;->a:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lakb;-><init>(LajZ;Landroid/os/Handler;)V

    iput-object v0, p0, LajZ;->a:Landroid/database/ContentObserver;

    .line 131
    iget-object v0, p0, LajZ;->a:Lake;

    iget-object v1, p0, LajZ;->a:Landroid/database/ContentObserver;

    invoke-interface {v0, p1, v1}, Lake;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 133
    if-eqz p2, :cond_0

    .line 134
    iget-object v0, p0, LajZ;->a:Lamw;

    invoke-interface {v0}, Lamw;->a()V

    .line 137
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 141
    const-string v0, "ActivityUpdater[rateLimiter=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LajZ;->a:Lamw;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
