.class public LaiO;
.super Lbuo;
.source "AndroidModule.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lbuo;-><init>()V

    .line 37
    iput-object p1, p0, LaiO;->a:Landroid/content/Context;

    .line 38
    return-void
.end method

.method static synthetic a(LaiO;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LaiO;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 61
    new-instance v0, LaiV;

    invoke-direct {v0}, LaiV;-><init>()V

    .line 62
    const-class v1, LaiX;

    invoke-virtual {p0, v1, v0}, LaiO;->a(Ljava/lang/Class;LbuH;)V

    .line 63
    const-class v1, LaiV;

    invoke-virtual {p0, v1}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v1

    invoke-interface {v1, v0}, LbuQ;->a(Ljava/lang/Object;)V

    .line 64
    new-instance v1, LaiB;

    invoke-direct {v1, v0}, LaiB;-><init>(LaiU;)V

    .line 65
    const-class v2, LaiC;

    invoke-virtual {p0, v2, v1}, LaiO;->a(Ljava/lang/Class;LbuH;)V

    .line 66
    new-instance v1, LaiH;

    iget-object v2, p0, LaiO;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, LaiH;-><init>(Landroid/content/Context;LaiU;)V

    .line 68
    const-class v0, LaiL;

    invoke-virtual {p0, v0, v1}, LaiO;->a(Ljava/lang/Class;LbuH;)V

    .line 69
    const-class v0, LaiH;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Object;)V

    .line 75
    const-class v0, LaiZ;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    .line 76
    const-class v0, Landroid/content/Context;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LaiA;

    invoke-interface {v0, v1}, LbuQ;->b(Ljava/lang/Class;)LbuU;

    .line 77
    const-class v0, LaiW;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LaiS;

    invoke-interface {v0, v1}, LbuQ;->b(Ljava/lang/Class;)LbuU;

    .line 79
    iget-object v0, p0, LaiO;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, LaiO;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Application;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaiO;->a:Landroid/content/Context;

    .line 82
    :goto_0
    const-class v1, Landroid/app/Application;

    invoke-virtual {p0, v1}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v1

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, LbwI;->a(Ljava/lang/Object;)LbuE;

    move-result-object v0

    invoke-interface {v1, v0}, LbuQ;->a(LbuE;)LbuU;

    .line 88
    :goto_1
    const-class v0, Landroid/content/Context;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, Lajg;

    .line 89
    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuT;

    move-result-object v0

    iget-object v1, p0, LaiO;->a:Landroid/content/Context;

    invoke-static {v1}, LbwI;->a(Ljava/lang/Object;)LbuE;

    move-result-object v1

    invoke-interface {v0, v1}, LbuT;->a(LbuE;)LbuU;

    .line 91
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    new-instance v1, LaiP;

    invoke-direct {v1, p0}, LaiP;-><init>(LaiO;)V

    invoke-interface {v0, v1}, LbuQ;->a(LbuE;)LbuU;

    .line 99
    const-class v0, Landroid/content/res/AssetManager;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    new-instance v1, LaiQ;

    invoke-direct {v1, p0}, LaiQ;-><init>(LaiO;)V

    invoke-interface {v0, v1}, LbuQ;->a(LbuE;)LbuU;

    .line 106
    invoke-virtual {p0}, LaiO;->a()Lcom/google/inject/Binder;

    move-result-object v0

    const-class v1, LH;

    invoke-static {v0, v1}, LaiT;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)V

    .line 107
    invoke-virtual {p0}, LaiO;->a()Lcom/google/inject/Binder;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LaiT;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)V

    .line 108
    const-class v0, LM;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, Lajn;

    invoke-interface {v0, v1}, LbuQ;->b(Ljava/lang/Class;)LbuU;

    .line 109
    const-class v0, Landroid/app/Activity;

    invoke-static {v0}, Lbsp;->a(Ljava/lang/Class;)LbuP;

    move-result-object v0

    invoke-virtual {p0, v0}, LaiO;->a(LbuP;)LbuQ;

    move-result-object v0

    const-class v1, Laix;

    .line 110
    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuT;

    move-result-object v0

    const-class v1, Laiy;

    .line 111
    invoke-interface {v0, v1}, LbuT;->b(Ljava/lang/Class;)LbuU;

    .line 112
    const-class v0, Landroid/content/pm/PackageManager;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    new-instance v1, LaiR;

    invoke-direct {v1, p0}, LaiR;-><init>(LaiO;)V

    invoke-interface {v0, v1}, LbuQ;->a(LbuE;)LbuU;

    .line 118
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, LaiO;->a:Landroid/content/Context;

    .line 81
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    goto/16 :goto_0

    .line 86
    :cond_1
    const-class v0, Landroid/app/Application;

    invoke-virtual {p0, v0}, LaiO;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LbwI;->a(Ljava/lang/Object;)LbuE;

    move-result-object v1

    invoke-interface {v0, v1}, LbuQ;->a(LbuE;)LbuU;

    goto/16 :goto_1
.end method
