.class abstract enum Lbjb;
.super Ljava/lang/Enum;
.source "Predicates.java"

# interfaces
.implements LbiU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbjb;",
        ">;",
        "LbiU",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbjb;

.field private static final synthetic a:[Lbjb;

.field public static final enum b:Lbjb;

.field public static final enum c:Lbjb;

.field public static final enum d:Lbjb;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 268
    new-instance v0, Lbjc;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1, v2}, Lbjc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->a:Lbjb;

    .line 273
    new-instance v0, Lbjd;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1, v3}, Lbjd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->b:Lbjb;

    .line 278
    new-instance v0, Lbje;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1, v4}, Lbje;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->c:Lbjb;

    .line 283
    new-instance v0, Lbjf;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1, v5}, Lbjf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbjb;->d:Lbjb;

    .line 267
    const/4 v0, 0x4

    new-array v0, v0, [Lbjb;

    sget-object v1, Lbjb;->a:Lbjb;

    aput-object v1, v0, v2

    sget-object v1, Lbjb;->b:Lbjb;

    aput-object v1, v0, v3

    sget-object v1, Lbjb;->c:Lbjb;

    aput-object v1, v0, v4

    sget-object v1, Lbjb;->d:Lbjb;

    aput-object v1, v0, v5

    sput-object v0, Lbjb;->a:[Lbjb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbiW;)V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Lbjb;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbjb;
    .locals 1

    .prologue
    .line 267
    const-class v0, Lbjb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbjb;

    return-object v0
.end method

.method public static values()[Lbjb;
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lbjb;->a:[Lbjb;

    invoke-virtual {v0}, [Lbjb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbjb;

    return-object v0
.end method


# virtual methods
.method a()LbiU;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LbiU",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 292
    return-object p0
.end method
