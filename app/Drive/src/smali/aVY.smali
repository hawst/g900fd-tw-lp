.class public LaVY;
.super LaLa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaLa",
        "<",
        "LaVX;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:LaLq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaLq",
            "<",
            "LaVX;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/ViewGroup;

.field private final a:Lcom/google/android/gms/maps/GoogleMapOptions;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 0

    invoke-direct {p0}, LaLa;-><init>()V

    iput-object p1, p0, LaVY;->a:Landroid/view/ViewGroup;

    iput-object p2, p0, LaVY;->a:Landroid/content/Context;

    iput-object p3, p0, LaVY;->a:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method protected a(LaLq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaLq",
            "<",
            "LaVX;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LaVY;->a:LaLq;

    invoke-virtual {p0}, LaVY;->h()V

    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, LaVY;->a:LaLq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaVY;->a()LaKZ;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaVY;->a:Landroid/content/Context;

    invoke-static {v0}, LaXQ;->a(Landroid/content/Context;)LaWQ;

    move-result-object v0

    iget-object v1, p0, LaVY;->a:Landroid/content/Context;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    iget-object v2, p0, LaVY;->a:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v0, v1, v2}, LaWQ;->a(LaLm;Lcom/google/android/gms/maps/GoogleMapOptions;)LaWu;

    move-result-object v0

    iget-object v1, p0, LaVY;->a:LaLq;

    new-instance v2, LaVX;

    iget-object v3, p0, LaVY;->a:Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, LaVX;-><init>(Landroid/view/ViewGroup;LaWu;)V

    invoke-interface {v1, v2}, LaLq;->a(LaKZ;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaCH; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, LaYd;

    invoke-direct {v1, v0}, LaYd;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
