.class public LNU;
.super Ljava/lang/Object;
.source "VideoController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private a:J

.field final synthetic a:Lcom/google/android/apps/docs/drive/media/VideoController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/drive/media/VideoController;)V
    .locals 2

    .prologue
    .line 225
    iput-object p1, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LNU;->a:J

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4

    .prologue
    .line 236
    if-nez p3, :cond_0

    .line 243
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Lcom/google/android/apps/docs/drive/media/VideoController;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->getDuration()I

    move-result v0

    int-to-long v0, v0

    .line 241
    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, LNU;->a:J

    .line 242
    iget-object v0, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    iget-wide v2, p0, LNU;->a:J

    long-to-int v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Lcom/google/android/apps/docs/drive/media/VideoController;I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->f()V

    .line 230
    iget-object v0, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    iget-object v1, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-static {v1}, Lcom/google/android/apps/docs/drive/media/VideoController;->b(Lcom/google/android/apps/docs/drive/media/VideoController;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoController;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 231
    iget-object v0, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    iget-object v1, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-static {v1}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Lcom/google/android/apps/docs/drive/media/VideoController;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoController;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 232
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    .prologue
    .line 247
    iget-object v0, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Lcom/google/android/apps/docs/drive/media/VideoController;)Landroid/widget/VideoView;

    move-result-object v0

    iget-wide v2, p0, LNU;->a:J

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 248
    iget-object v0, p0, LNU;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->e()V

    .line 249
    return-void
.end method
