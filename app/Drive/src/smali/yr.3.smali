.class public Lyr;
.super Ljava/lang/Object;
.source "CsiSyncerImpl.java"

# interfaces
.implements Lyq;


# instance fields
.field private final a:I

.field private final a:LQr;

.field private final a:LZS;

.field private final a:Lamm;

.field private final a:Ljava/util/concurrent/Executor;

.field private final a:Lorg/apache/http/client/HttpClient;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LTO;LtK;LQr;Lamm;)V
    .locals 7
    .param p4    # Lamm;
        .annotation runtime Lyw;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p2, p0, Lyr;->a:LtK;

    .line 57
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p0, Lyr;->a:LQr;

    .line 58
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamm;

    iput-object v0, p0, Lyr;->a:Lamm;

    .line 62
    invoke-interface {p1}, LTO;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lyr;->a:Lorg/apache/http/client/HttpClient;

    .line 64
    iget-object v0, p0, Lyr;->a:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-static {v0, v6}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 66
    new-instance v0, LZQ;

    const-wide/16 v2, 0xc8

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    invoke-direct {v0, v2, v3, v4, v5}, LZQ;-><init>(JD)V

    iput-object v0, p0, Lyr;->a:LZS;

    .line 68
    const-string v0, "csiOfflineRequestRetryAttempts"

    const/16 v1, 0xa

    invoke-interface {p3, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lyr;->a:I

    .line 70
    const-string v0, "CsiSyncerImpl"

    invoke-static {v6, v0}, Lalg;->a(ILjava/lang/String;)LbsW;

    move-result-object v0

    iput-object v0, p0, Lyr;->a:Ljava/util/concurrent/Executor;

    .line 71
    return-void
.end method

.method static synthetic a(Lyr;)LQr;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lyr;->a:LQr;

    return-object v0
.end method

.method static synthetic a(Lyr;)Lamm;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lyr;->a:Lamm;

    return-object v0
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 146
    const/16 v0, 0xc8

    if-ge p1, v0, :cond_0

    const/16 v0, 0x12c

    if-ge p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 107
    iget-object v0, p0, Lyr;->a:LZS;

    invoke-interface {v0}, LZS;->c()V

    move v0, v1

    .line 108
    :goto_0
    iget v3, p0, Lyr;->a:I

    if-gt v0, v3, :cond_1

    .line 110
    if-lez v0, :cond_0

    .line 112
    :try_start_0
    iget-object v3, p0, Lyr;->a:LZS;

    invoke-interface {v3}, LZS;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 118
    :cond_0
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v3

    sget-object v4, Lrx;->d:Lrx;

    invoke-virtual {v3, v4}, Lrx;->a(Lrx;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 121
    const-string v0, "CsiSyncerImpl"

    const-string v3, "Sending CSI data disabled\nCSI request URL: %s"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v1, v2

    .line 142
    :cond_1
    :goto_1
    return v1

    .line 126
    :cond_2
    :try_start_1
    iget-object v3, p0, Lyr;->a:Lorg/apache/http/client/HttpClient;

    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 129
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 130
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 133
    :cond_3
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    invoke-direct {p0, v4}, Lyr;->a(I)Z

    move-result v4

    if-eqz v4, :cond_4

    move v1, v2

    .line 134
    goto :goto_1

    .line 136
    :cond_4
    const-string v4, "CsiSyncerImpl"

    const-string v5, "Error sending CSI report [%s], status = %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    .line 137
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    .line 136
    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 108
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :catch_0
    move-exception v3

    .line 139
    const-string v4, "CsiSyncerImpl"

    const-string v5, "Error sending csi report [%s]"

    new-array v6, v2, [Ljava/lang/Object;

    aput-object p1, v6, v1

    invoke-static {v4, v3, v5, v6}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 113
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lyr;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lyr;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lyr;->a:LtK;

    sget-object v1, Lry;->D:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    const-string v0, "CsiSyncerImpl"

    const-string v1, "CSI report sync is disabled, skipping"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 98
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lyr;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lys;

    invoke-direct {v1, p0}, Lys;-><init>(Lyr;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
