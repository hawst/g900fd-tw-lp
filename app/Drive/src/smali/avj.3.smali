.class public final enum Lavj;
.super Ljava/lang/Enum;
.source "MediaTypes.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lavj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lavj;

.field private static final synthetic a:[Lavj;

.field public static final enum b:Lavj;

.field public static final enum c:Lavj;

.field public static final enum d:Lavj;

.field public static final enum e:Lavj;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lavj;

    const-string v1, "APPLICATION"

    const-string v2, "application"

    invoke-direct {v0, v1, v3, v2}, Lavj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lavj;->a:Lavj;

    .line 13
    new-instance v0, Lavj;

    const-string v1, "AUDIO"

    const-string v2, "audio"

    invoke-direct {v0, v1, v4, v2}, Lavj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lavj;->b:Lavj;

    .line 14
    new-instance v0, Lavj;

    const-string v1, "TEXT"

    const-string v2, "text"

    invoke-direct {v0, v1, v5, v2}, Lavj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lavj;->c:Lavj;

    .line 15
    new-instance v0, Lavj;

    const-string v1, "VIDEO"

    const-string v2, "video"

    invoke-direct {v0, v1, v6, v2}, Lavj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lavj;->d:Lavj;

    .line 16
    new-instance v0, Lavj;

    const-string v1, "IMAGE"

    const-string v2, "image"

    invoke-direct {v0, v1, v7, v2}, Lavj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lavj;->e:Lavj;

    .line 11
    const/4 v0, 0x5

    new-array v0, v0, [Lavj;

    sget-object v1, Lavj;->a:Lavj;

    aput-object v1, v0, v3

    sget-object v1, Lavj;->b:Lavj;

    aput-object v1, v0, v4

    sget-object v1, Lavj;->c:Lavj;

    aput-object v1, v0, v5

    sget-object v1, Lavj;->d:Lavj;

    aput-object v1, v0, v6

    sget-object v1, Lavj;->e:Lavj;

    aput-object v1, v0, v7

    sput-object v0, Lavj;->a:[Lavj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput-object p3, p0, Lavj;->a:Ljava/lang/String;

    .line 22
    return-void
.end method

.method static synthetic a(Lavj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lavj;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lavj;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lavj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lavj;

    return-object v0
.end method

.method public static values()[Lavj;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lavj;->a:[Lavj;

    invoke-virtual {v0}, [Lavj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lavj;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 25
    const-string v0, "%s/%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lavj;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
