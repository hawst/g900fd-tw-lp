.class public enum Lqq;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lqq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lqq;

.field private static final synthetic a:[Lqq;

.field public static final enum b:Lqq;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 78
    new-instance v0, Lqq;

    const-string v1, "COMMENTER"

    const-string v2, "commenter"

    invoke-direct {v0, v1, v3, v2}, Lqq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lqq;->a:Lqq;

    .line 79
    new-instance v0, Lqr;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, Lqr;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lqq;->b:Lqq;

    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [Lqq;

    sget-object v1, Lqq;->a:Lqq;

    aput-object v1, v0, v3

    sget-object v1, Lqq;->b:Lqq;

    aput-object v1, v0, v4

    sput-object v0, Lqq;->a:[Lqq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 88
    iput-object p3, p0, Lqq;->a:Ljava/lang/String;

    .line 89
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Lqp;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Lqq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lqq;
    .locals 6

    .prologue
    .line 104
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-static {}, Lqq;->values()[Lqq;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 106
    iget-object v5, v0, Lqq;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 110
    :goto_1
    return-object v0

    .line 105
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 110
    :cond_1
    sget-object v0, Lqq;->b:Lqq;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lqq;
    .locals 1

    .prologue
    .line 77
    const-class v0, Lqq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lqq;

    return-object v0
.end method

.method public static values()[Lqq;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lqq;->a:[Lqq;

    invoke-virtual {v0}, [Lqq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lqq;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lqq;->a:Ljava/lang/String;

    return-object v0
.end method
