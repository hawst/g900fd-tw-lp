.class LDx;
.super Ljava/lang/Object;
.source "SelectionViewStateImpl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LDi;

.field final synthetic a:LDt;

.field private final a:Lcom/google/android/apps/docs/doclist/SelectionItem;

.field private final a:Z


# direct methods
.method constructor <init>(LDt;Lcom/google/android/apps/docs/doclist/SelectionItem;LDi;Z)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, LDx;->a:LDt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p2, p0, LDx;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 47
    iput-boolean p4, p0, LDx;->a:Z

    .line 48
    iput-object p3, p0, LDx;->a:LDi;

    .line 49
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, LDx;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LJR;

    move-result-object v0

    iget-object v1, p0, LDx;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    iget-boolean v2, p0, LDx;->a:Z

    invoke-virtual {v0, v1, v2}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 54
    iget-boolean v0, p0, LDx;->a:Z

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, LDx;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LKZ;

    move-result-object v0

    iget-object v1, p0, LDx;->a:LDi;

    iget-object v1, v1, LDi;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LKZ;->b(Landroid/view/View;)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, LDx;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LKZ;

    move-result-object v0

    iget-object v1, p0, LDx;->a:LDi;

    iget-object v1, v1, LDi;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LKZ;->a(Landroid/view/View;)V

    goto :goto_0
.end method
