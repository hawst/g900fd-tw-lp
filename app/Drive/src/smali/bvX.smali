.class public abstract LbvX;
.super Ljava/lang/Object;
.source "OptionalBinder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LbvY;)V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, LbvX;-><init>()V

    return-void
.end method

.method static a(LbuP;)LbuP;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuP",
            "<",
            "LbiP",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 183
    const-class v0, LbiP;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    .line 184
    invoke-virtual {p0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 183
    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    return-object v0
.end method

.method static a(Lbuv;)Lbuv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "Lbuv",
            "<",
            "LbuE",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p0}, Lbuv;->a()LbuP;

    move-result-object v0

    invoke-virtual {v0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, LbwL;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 204
    invoke-virtual {p0, v0}, Lbuv;->b(Ljava/lang/reflect/Type;)Lbuv;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/inject/Binder;LbuP;)LbvX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/inject/Binder;",
            "LbuP",
            "<TT;>;)",
            "LbvX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 170
    invoke-static {p1}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    invoke-static {p0, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/inject/Binder;Lbuv;)LbvX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/inject/Binder;",
            "Lbuv",
            "<TT;>;)",
            "LbvX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 174
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, LbvX;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lbwe;

    aput-object v2, v0, v1

    invoke-interface {p0, v0}, Lcom/google/inject/Binder;->a([Ljava/lang/Class;)Lcom/google/inject/Binder;

    move-result-object v0

    .line 175
    new-instance v1, Lbwe;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, v2}, Lbwe;-><init>(Lcom/google/inject/Binder;Lbuv;LbvY;)V

    .line 176
    invoke-interface {v0, v1}, Lcom/google/inject/Binder;->a(LbuC;)V

    .line 177
    return-object v1
.end method

.method public static a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/inject/Binder;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbvX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 166
    invoke-static {p1}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {p0, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    move-result-object v0

    return-object v0
.end method

.method static b(LbuP;)LbuP;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuP",
            "<",
            "LbiP",
            "<",
            "Lbxw",
            "<TT;>;>;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 190
    const-class v0, LbiP;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, Lbxw;

    new-array v3, v3, [Ljava/lang/reflect/Type;

    .line 192
    invoke-virtual {p0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v2

    aput-object v2, v1, v5

    .line 191
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 190
    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    return-object v0
.end method

.method static c(LbuP;)LbuP;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuP",
            "<",
            "LbiP",
            "<",
            "LbuE",
            "<TT;>;>;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 197
    const-class v0, LbiP;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const-class v2, LbuE;

    new-array v3, v3, [Ljava/lang/reflect/Type;

    .line 198
    invoke-virtual {p0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v2

    aput-object v2, v1, v5

    .line 197
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a()LbuT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuT",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract b()LbuT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuT",
            "<TT;>;"
        }
    .end annotation
.end method
