.class public LWU;
.super Ljava/lang/Object;
.source "DisplayManagerHelperDummyImpl.java"

# interfaces
.implements LWS;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LWT;)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public a(LWT;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, LWU;->a:Landroid/content/Context;

    .line 21
    return-void
.end method

.method public a(Ljava/lang/String;)[Landroid/view/Display;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/Display;

    iget-object v0, p0, LWU;->a:Landroid/content/Context;

    const-string v2, "window"

    .line 42
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    aput-object v0, v1, v3

    move-object v0, v1

    .line 44
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v3, [Landroid/view/Display;

    goto :goto_0
.end method
