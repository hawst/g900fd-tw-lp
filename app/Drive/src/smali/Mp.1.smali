.class public final enum LMp;
.super Ljava/lang/Enum;
.source "CrossAppStateRow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LMp;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LMp;

.field private static final synthetic a:[LMp;

.field public static final enum b:LMp;

.field public static final enum c:LMp;

.field public static final enum d:LMp;

.field public static final enum e:LMp;

.field public static final enum f:LMp;

.field public static final enum g:LMp;

.field public static final enum h:LMp;

.field public static final enum i:LMp;

.field public static final enum j:LMp;

.field public static final enum k:LMp;

.field public static final enum l:LMp;


# instance fields
.field private final a:LaFr;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    new-instance v0, LMp;

    const-string v1, "ID"

    const-string v2, "_id"

    invoke-direct {v0, v1, v4, v2}, LMp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LMp;->a:LMp;

    .line 64
    new-instance v0, LMp;

    const-string v1, "RESOURCE_ID"

    sget-object v2, LaES;->p:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->b:LMp;

    .line 65
    new-instance v0, LMp;

    const-string v1, "ACCOUNT_HOLDER_NAME"

    sget-object v2, LaEf;->a:LaEf;

    invoke-virtual {v2}, LaEf;->a()LaFr;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->c:LMp;

    .line 66
    new-instance v0, LMp;

    const-string v1, "KIND"

    sget-object v2, LaES;->t:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->d:LMp;

    .line 67
    new-instance v0, LMp;

    const-string v1, "DEFAULT_EXPORT_MIME_TYPE"

    sget-object v2, LaES;->r:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->e:LMp;

    .line 68
    new-instance v0, LMp;

    const-string v1, "HTML_URI"

    const/4 v2, 0x5

    sget-object v3, LaEL;->e:LaEL;

    invoke-virtual {v3}, LaEL;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->f:LMp;

    .line 69
    new-instance v0, LMp;

    const-string v1, "PINNED"

    const/4 v2, 0x6

    sget-object v3, LaES;->y:LaES;

    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->g:LMp;

    .line 70
    new-instance v0, LMp;

    const-string v1, "LAST_PINNED_STATE_CHANGE_TIME"

    const/4 v2, 0x7

    sget-object v3, LaES;->z:LaES;

    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->h:LMp;

    .line 71
    new-instance v0, LMp;

    const-string v1, "LAST_OFFLINE_CONTENT_UPDATE_TIME"

    const/16 v2, 0x8

    sget-object v3, LaES;->A:LaES;

    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->i:LMp;

    .line 72
    new-instance v0, LMp;

    const-string v1, "CONTENT_TYPE"

    const/16 v2, 0x9

    sget-object v3, LaEJ;->b:LaEJ;

    invoke-virtual {v3}, LaEJ;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->j:LMp;

    .line 73
    new-instance v0, LMp;

    const-string v1, "OWNED_FILE_PATH"

    const/16 v2, 0xa

    sget-object v3, LaEJ;->g:LaEJ;

    invoke-virtual {v3}, LaEJ;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->k:LMp;

    .line 74
    new-instance v0, LMp;

    const-string v1, "SERVER_SIDE_LAST_MODIFIED_TIME"

    const/16 v2, 0xb

    sget-object v3, LaEJ;->m:LaEJ;

    invoke-virtual {v3}, LaEJ;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMp;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LMp;->l:LMp;

    .line 62
    const/16 v0, 0xc

    new-array v0, v0, [LMp;

    sget-object v1, LMp;->a:LMp;

    aput-object v1, v0, v4

    sget-object v1, LMp;->b:LMp;

    aput-object v1, v0, v5

    sget-object v1, LMp;->c:LMp;

    aput-object v1, v0, v6

    sget-object v1, LMp;->d:LMp;

    aput-object v1, v0, v7

    sget-object v1, LMp;->e:LMp;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LMp;->f:LMp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LMp;->g:LMp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LMp;->h:LMp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LMp;->i:LMp;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LMp;->j:LMp;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LMp;->k:LMp;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LMp;->l:LMp;

    aput-object v2, v0, v1

    sput-object v0, LMp;->a:[LMp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    invoke-virtual {p3}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LMp;->a:Ljava/lang/String;

    .line 86
    iput-object p3, p0, LMp;->a:LaFr;

    .line 87
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    iput-object p3, p0, LMp;->a:Ljava/lang/String;

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, LMp;->a:LaFr;

    .line 82
    return-void
.end method

.method private a()LaFr;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, LMp;->a:LaFr;

    return-object v0
.end method

.method static synthetic a(LMp;)LaFr;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, LMp;->a()LaFr;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LMp;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LMp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, LMp;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LMp;
    .locals 1

    .prologue
    .line 62
    const-class v0, LMp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LMp;

    return-object v0
.end method

.method public static values()[LMp;
    .locals 1

    .prologue
    .line 62
    sget-object v0, LMp;->a:[LMp;

    invoke-virtual {v0}, [LMp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LMp;

    return-object v0
.end method
