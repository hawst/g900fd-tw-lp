.class public final LaIA;
.super LaIC;
.source "EntryGuid.java"


# instance fields
.field private final a:LaGM;

.field private final a:LaIy;

.field private final a:Landroid/webkit/MimeTypeMap;

.field private final a:Ljava/lang/String;

.field private final a:LvL;


# direct methods
.method constructor <init>(JLjava/lang/String;LaGM;LvL;LaIy;)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, LaIC;-><init>(J)V

    .line 52
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    iput-object v0, p0, LaIA;->a:Landroid/webkit/MimeTypeMap;

    .line 57
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 58
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaIA;->a:Ljava/lang/String;

    .line 59
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LaIA;->a:LaGM;

    .line 60
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvL;

    iput-object v0, p0, LaIA;->a:LvL;

    .line 61
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIy;

    iput-object v0, p0, LaIA;->a:LaIy;

    .line 62
    invoke-static {p3}, LaIA;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 63
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    instance-of v0, p0, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 76
    if-eqz v0, :cond_0

    .line 77
    check-cast p0, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v0

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 80
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "esp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    .line 103
    invoke-static {p0}, LaIA;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 106
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 128
    const-string v0, "doc="

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "esp="

    .line 129
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 130
    :goto_0
    return v0

    .line 129
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    invoke-static {p0}, LaIA;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;

    move-result-object v0

    .line 115
    const-string v1, "doc="

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    const-string v1, "doc="

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()LaGo;
    .locals 2

    .prologue
    .line 239
    invoke-virtual {p0}, LaIA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 240
    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LaIA;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Z[Ljava/lang/String;LIK;Landroid/net/Uri;)LaHY;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 167
    iget-object v1, p0, LaIA;->a:LaGM;

    invoke-virtual {p0, v1}, LaIA;->a(LaGM;)LaFM;

    move-result-object v2

    .line 168
    if-nez v2, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-object v0

    .line 171
    :cond_1
    iget-object v1, p0, LaIA;->a:Ljava/lang/String;

    invoke-virtual {p0, v2, v1}, LaIA;->a(LaFM;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 173
    iget-object v3, p0, LaIA;->a:LvL;

    invoke-interface {v3, v1}, LvL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    .line 174
    new-instance v3, LvN;

    invoke-direct {v3}, LvN;-><init>()V

    iget-object v4, p0, LaIA;->a:LvL;

    .line 175
    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v5

    invoke-interface {v4, v5}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v4

    invoke-virtual {v3, v4}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v3

    .line 176
    invoke-virtual {v3, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v1

    iget-object v3, p0, LaIA;->a:LvL;

    .line 177
    invoke-interface {v3}, LvL;->a()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v1, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v1

    .line 178
    invoke-virtual {v1}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v3

    .line 180
    iget-object v1, p0, LaIA;->a:LaGM;

    invoke-static {p1, p2, v3, p3, v1}, LaIA;->a(Z[Ljava/lang/String;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LaGM;)LaHY;

    move-result-object v1

    .line 182
    if-eqz v1, :cond_0

    .line 186
    iget-object v0, p0, LaIA;->a:LaIy;

    invoke-virtual {v0, p0, v2, v3, p4}, LaIy;->a(LaIC;LaFM;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v0

    .line 187
    invoke-virtual {v1, v0}, LaHY;->a(Landroid/os/Bundle;)V

    move-object v0, v1

    .line 188
    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaIq;)LaIA;
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, LaIA;->a:LaGM;

    invoke-virtual {p0, v0}, LaIA;->a(LaGM;)LaFM;

    move-result-object v0

    .line 251
    if-nez v0, :cond_0

    .line 252
    const/4 v0, 0x0

    .line 255
    :goto_0
    return-object v0

    .line 254
    :cond_0
    iget-object v1, p0, LaIA;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LaIA;->a(LaFM;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 255
    invoke-virtual {p3, v1, v0, p1, p2}, LaIq;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFM;Ljava/lang/String;Ljava/lang/String;)LaIA;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Z[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 194
    invoke-virtual {p0}, LaIA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 195
    if-nez v1, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-object v7

    .line 200
    :cond_1
    iget-object v0, p0, LaIA;->a:LaGM;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_2

    .line 204
    invoke-interface {v0}, LaGo;->a()Ljava/lang/Long;

    move-result-object v5

    .line 210
    :goto_1
    if-eqz v0, :cond_0

    .line 214
    invoke-interface {v0}, LaGu;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    invoke-interface {v0}, LaGu;->a()LaGv;

    move-result-object v3

    .line 219
    sget-object v1, LaIs;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGu;)Ljava/lang/String;

    move-result-object v4

    .line 221
    invoke-interface {v0}, LaGu;->c()Ljava/lang/String;

    move-result-object v2

    .line 223
    invoke-interface {v0}, LaGu;->b()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    .line 227
    invoke-static {p0}, LaIF;->a(LaIC;)Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 234
    invoke-interface {v0}, LaGu;->h()Z

    move-result v0

    invoke-static {v0}, LaHW;->a(Z)LaHW;

    move-result-object v8

    move-object v0, p2

    .line 225
    invoke-static/range {v0 .. v8}, LaHV;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, LaIA;->a:LaGM;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v0

    move-object v5, v7

    goto :goto_1
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 6

    .prologue
    .line 152
    iget-object v0, p0, LaIA;->a:LaGM;

    invoke-virtual {p0, v0}, LaIA;->a(LaGM;)LaFM;

    move-result-object v0

    .line 153
    if-nez v0, :cond_0

    .line 154
    const-string v0, "EntryGuid"

    const-string v1, "Account not found: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, LaIA;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 155
    const/4 v0, 0x0

    .line 157
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LaIA;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LaIA;->a(LaFM;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method a(LaFM;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 6

    .prologue
    .line 134
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    .line 135
    const-string v0, "doc="

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    const-string v0, "doc="

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 137
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 138
    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;J)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    .line 144
    :goto_1
    return-object v0

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_1
    const-string v0, "esp="

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    const-string v0, "esp="

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 141
    iget-object v2, p0, LaIA;->a:LaGM;

    invoke-interface {v2, v1, v0}, LaGM;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    goto :goto_1

    .line 143
    :cond_2
    const-string v0, "EntryGuid"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Malformed docId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, LaIA;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 260
    invoke-super {p0, p1}, LaIC;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    const/4 v0, 0x0

    .line 264
    :goto_0
    return v0

    .line 263
    :cond_0
    check-cast p1, LaIA;

    .line 264
    iget-object v0, p0, LaIA;->a:Ljava/lang/String;

    iget-object v1, p1, LaIA;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 269
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, LaIC;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaIA;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 274
    const-string v0, "EntryGuid[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, LaIC;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaIA;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
