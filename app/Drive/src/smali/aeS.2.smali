.class public LaeS;
.super Lbxu;
.source "XmlDocsListGDataSerializer.java"


# instance fields
.field private final a:LaeF;

.field private final a:Lbxr;


# direct methods
.method constructor <init>(Lbxr;LaeF;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lbxu;-><init>(Lbxr;Lbxb;)V

    .line 39
    iput-object p1, p0, LaeS;->a:Lbxr;

    .line 40
    iput-object p2, p0, LaeS;->a:LaeF;

    .line 41
    return-void
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 117
    :cond_0
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 118
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 119
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 147
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 151
    :cond_0
    sget-object v0, Lbxq;->w:Ljava/lang/String;

    invoke-interface {p0, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 152
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    sget-object v0, Lbxq;->x:Ljava/lang/String;

    invoke-interface {p0, v3, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 155
    :cond_1
    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 156
    sget-object v0, Lbxq;->y:Ljava/lang/String;

    invoke-interface {p0, v3, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 158
    :cond_2
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 159
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p0, v3, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_1

    .line 161
    :cond_3
    sget-object v0, Lbxq;->w:Ljava/lang/String;

    invoke-interface {p0, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    const-string v0, "authorizedApp"

    .line 125
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 126
    const-string v2, "http://schemas.google.com/docs/2007"

    const-string v3, "authorizedApp"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 130
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 131
    const-string v0, "http://schemas.google.com/docs/2007"

    const-string v2, "authorizedApp"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method

.method private static b(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2

    .prologue
    .line 108
    const-string v0, "http://schemas.google.com/g/2005"

    sget-object v1, Lbxq;->F:Ljava/lang/String;

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 109
    const-string v0, "http://schemas.google.com/g/2005"

    sget-object v1, Lbxq;->F:Ljava/lang/String;

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 110
    return-void
.end method

.method private b(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 4

    .prologue
    .line 77
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 78
    iget-object v0, p0, LaeS;->a:LaeF;

    invoke-virtual {v0}, LaeF;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 80
    :cond_0
    iget-object v0, p0, LaeS;->a:LaeF;

    invoke-virtual {v0}, LaeF;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeS;->b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, LaeS;->a:LaeF;

    invoke-virtual {v0}, LaeF;->u()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaeS;->a:LaeF;

    invoke-virtual {v1}, LaeF;->v()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    .line 81
    invoke-static {p1, v0, v1, v2}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 84
    const-string v1, "label"

    iget-object v0, p0, LaeS;->a:LaeF;

    .line 85
    invoke-virtual {v0}, LaeF;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "starred"

    .line 84
    :goto_0
    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 86
    const-string v2, "label"

    iget-object v0, p0, LaeS;->a:LaeF;

    .line 87
    invoke-virtual {v0}, LaeF;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "hidden"

    .line 86
    :goto_1
    invoke-static {v2, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 89
    const-string v2, "http://schemas.google.com/g/2005/labels#starred"

    const-string v3, "http://schemas.google.com/g/2005/labels"

    invoke-static {p1, v2, v3, v1}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 91
    const-string v1, "http://schemas.google.com/g/2005/labels#hidden"

    const-string v2, "http://schemas.google.com/g/2005/labels"

    invoke-static {p1, v1, v2, v0}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 94
    iget-object v0, p0, LaeS;->a:LaeF;

    invoke-virtual {v0}, LaeF;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    const-string v0, "label"

    const-string v1, "trashed"

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 96
    const-string v1, "http://schemas.google.com/g/2005/labels#trashed"

    const-string v2, "http://schemas.google.com/g/2005/labels"

    invoke-static {p1, v1, v2, v0}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 98
    invoke-static {p1}, LaeS;->b(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 101
    :cond_1
    iget-object v0, p0, LaeS;->a:LaeF;

    invoke-virtual {v0}, LaeF;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeS;->c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, LaeS;->a:LaeF;

    invoke-virtual {v0}, LaeF;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeS;->d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, LaeS;->a:LaeF;

    invoke-virtual {v0}, LaeF;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, v0}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V

    .line 104
    invoke-virtual {p0, p1, p2}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 105
    return-void

    .line 85
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 87
    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method private static b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    :goto_0
    return-void

    .line 168
    :cond_0
    sget-object v0, Lbxq;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 169
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 170
    sget-object v0, Lbxq;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private c(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2

    .prologue
    .line 137
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "gd"

    const-string v1, "http://schemas.google.com/g/2005"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0, p1}, LaeS;->a(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 140
    return-void
.end method

.method private static c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 175
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    :goto_0
    return-void

    .line 178
    :cond_0
    sget-object v0, Lbxq;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 179
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 180
    sget-object v0, Lbxq;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 185
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    :goto_0
    return-void

    .line 188
    :cond_0
    const-string v0, "http://schemas.google.com/g/2005"

    const-string v1, "lastViewed"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 189
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 190
    const-string v0, "http://schemas.google.com/g/2005"

    const-string v1, "lastViewed"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/OutputStream;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 52
    .line 54
    :try_start_0
    iget-object v0, p0, LaeS;->a:Lbxr;

    invoke-interface {v0}, Lbxr;->a()Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 59
    sget-object v1, Lbxq;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 60
    if-eq p2, v3, :cond_0

    .line 61
    sget-object v1, Lbxq;->e:Ljava/lang/String;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 62
    invoke-direct {p0, v0}, LaeS;->c(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 65
    :cond_0
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 66
    invoke-direct {p0, v0, p2}, LaeS;->b(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 67
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 69
    if-eq p2, v3, :cond_1

    .line 70
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 72
    :cond_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 73
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Lbxk;

    const-string v2, "Unable to create XmlSerializer."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
