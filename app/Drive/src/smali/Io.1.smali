.class public LIo;
.super LIk;
.source "FastScrollerCustom.java"


# instance fields
.field private a:I

.field private a:LIp;

.field private a:Landroid/graphics/Paint;

.field private a:Landroid/graphics/RectF;

.field private a:Landroid/graphics/drawable/NinePatchDrawable;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;LIm;LIl;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct/range {p0 .. p5}, LIk;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;LIm;LIl;)V

    .line 53
    invoke-direct {p0, p1}, LIo;->a(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, LIo;->a:Landroid/graphics/Paint;

    .line 75
    sget v1, Lxb;->menu_submenu_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, LIo;->a:Landroid/graphics/drawable/NinePatchDrawable;

    .line 76
    iget-object v0, p0, LIo;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 77
    iget-object v0, p0, LIo;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 80
    iget-object v0, p0, LIo;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v0, p0, LIo;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    iget-object v0, p0, LIo;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 83
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LIo;->a:Landroid/graphics/RectF;

    .line 84
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, LIk;->a()V

    .line 60
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 130
    invoke-virtual {p0}, LIo;->a()I

    move-result v0

    .line 131
    invoke-super {p0, p1}, LIk;->a(I)V

    .line 132
    if-ne p1, v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    if-eq p1, v2, :cond_2

    if-ne v0, v2, :cond_0

    .line 137
    :cond_2
    iget-object v0, p0, LIo;->a:LIp;

    if-eqz v0, :cond_0

    .line 138
    iget-object v1, p0, LIo;->a:LIp;

    if-ne p1, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, LIp;->a(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public a(IIIILandroid/content/res/Resources;)V
    .locals 6

    .prologue
    .line 98
    invoke-virtual {p0, p1, p2, p3, p4}, LIo;->a(IIII)V

    .line 99
    iget-object v0, p0, LIo;->a:Landroid/graphics/RectF;

    .line 100
    iget v1, p0, LIo;->a:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 101
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, LIo;->a:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 102
    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 103
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 104
    iget-object v2, p0, LIo;->a:Landroid/graphics/Paint;

    const-string v3, "W"

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 105
    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {p5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, LIo;->b:I

    .line 106
    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, p0, LIo;->b:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 107
    iget-object v1, p0, LIo;->a:Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, LIo;->a:Landroid/graphics/drawable/NinePatchDrawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 110
    :cond_0
    return-void
.end method

.method public a(ILandroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 150
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 151
    const/4 v1, 0x1

    int-to-float v2, p1

    .line 152
    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LIo;->a:I

    .line 153
    return-void
.end method

.method public a(LIp;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, LIo;->a:LIp;

    .line 161
    return-void
.end method

.method a(Landroid/graphics/Canvas;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 88
    iget-object v0, p0, LIo;->a:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 89
    iget-object v0, p0, LIo;->a:Landroid/graphics/Paint;

    .line 90
    iget-object v1, p0, LIo;->a:Landroid/graphics/RectF;

    .line 91
    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    .line 92
    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v3

    .line 93
    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v5

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, p2, v4, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 95
    return-void
.end method

.method public a(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1, p2, p3, p4}, LIk;->a(Landroid/widget/AbsListView;III)V

    .line 116
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, LIk;->a()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1}, LIk;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0}, LIk;->b()V

    .line 66
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, LIo;->a:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 157
    return-void
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 125
    invoke-super {p0, p1}, LIk;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 164
    const-string v0, ""

    iput-object v0, p0, LIo;->a:Ljava/lang/String;

    .line 165
    invoke-super {p0}, LIk;->c()V

    .line 166
    return-void
.end method
