.class final LbpQ;
.super Lbnk;
.source "RegularImmutableSortedSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbnk",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final transient a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbmF;Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<TE;>;",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p2}, Lbnk;-><init>(Ljava/util/Comparator;)V

    .line 54
    iput-object p1, p0, LbpQ;->a:LbmF;

    .line 55
    invoke-virtual {p1}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 56
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {p0}, LbpQ;->a()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, p1, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    return v0
.end method


# virtual methods
.method a(Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 267
    if-nez p1, :cond_0

    .line 277
    :goto_0
    return v1

    .line 272
    :cond_0
    :try_start_0
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {p0}, LbpQ;->a()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lbqn;->a:Lbqn;

    sget-object v4, Lbqj;->c:Lbqj;

    invoke-static {v0, p1, v2, v3, v4}, Lbqh;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lbqn;Lbqj;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 277
    if-ltz v0, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 274
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method a(Ljava/lang/Object;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)I"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v1, p0, LbpQ;->a:LbmF;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, LbpQ;->comparator()Ljava/util/Comparator;

    move-result-object v3

    if-eqz p2, :cond_0

    sget-object v0, Lbqn;->d:Lbqn;

    :goto_0
    sget-object v4, Lbqj;->b:Lbqj;

    invoke-static {v1, v2, v3, v0, v4}, Lbqh;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lbqn;Lbqj;)I

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lbqn;->c:Lbqn;

    goto :goto_0
.end method

.method a(II)Lbnk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 255
    if-nez p1, :cond_0

    invoke-virtual {p0}, LbpQ;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 261
    :goto_0
    return-object p0

    .line 257
    :cond_0
    if-ge p1, p2, :cond_1

    .line 258
    new-instance v0, LbpQ;

    iget-object v1, p0, LbpQ;->a:LbmF;

    invoke-virtual {v1, p1, p2}, LbmF;->a(II)LbmF;

    move-result-object v1

    iget-object v2, p0, LbpQ;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, LbpQ;-><init>(LbmF;Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0

    .line 261
    :cond_1
    iget-object v0, p0, LbpQ;->a:Ljava/util/Comparator;

    invoke-static {v0}, LbpQ;->a(Ljava/util/Comparator;)Lbnk;

    move-result-object p0

    goto :goto_0
.end method

.method a(Ljava/lang/Object;Z)Lbnk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 222
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, LbpQ;->a(Ljava/lang/Object;Z)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LbpQ;->a(II)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 233
    invoke-virtual {p0, p1, p2}, LbpQ;->b(Ljava/lang/Object;Z)Lbnk;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lbnk;->a(Ljava/lang/Object;Z)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public a()Lbqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method a()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, LbpQ;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Z

    move-result v0

    return v0
.end method

.method b(Ljava/lang/Object;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)I"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v1, p0, LbpQ;->a:LbmF;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, LbpQ;->comparator()Ljava/util/Comparator;

    move-result-object v3

    if-eqz p2, :cond_0

    sget-object v0, Lbqn;->c:Lbqn;

    :goto_0
    sget-object v4, Lbqj;->b:Lbqj;

    invoke-static {v1, v2, v3, v0, v4}, Lbqh;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lbqn;Lbqj;)I

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lbqn;->d:Lbqn;

    goto :goto_0
.end method

.method b()LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 282
    new-instance v0, Lbnf;

    iget-object v1, p0, LbpQ;->a:LbmF;

    invoke-direct {v0, p0, v1}, Lbnf;-><init>(Lbnk;LbmF;)V

    return-object v0
.end method

.method b(Ljava/lang/Object;Z)Lbnk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 238
    invoke-virtual {p0, p1, p2}, LbpQ;->b(Ljava/lang/Object;Z)I

    move-result v0

    invoke-virtual {p0}, LbpQ;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LbpQ;->a(II)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public b()Lbqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a_()LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 80
    if-eqz p1, :cond_0

    :try_start_0
    invoke-direct {p0, p1}, LbpQ;->b(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 81
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    invoke-virtual {p0}, LbpQ;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0, p1}, Lbqg;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-gt v0, v1, :cond_1

    .line 93
    :cond_0
    invoke-super {p0, p1}, Lbnk;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 129
    :goto_0
    return v0

    .line 100
    :cond_1
    invoke-virtual {p0}, LbpQ;->a()Lbqv;

    move-result-object v3

    .line 101
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 102
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 106
    :cond_2
    :goto_1
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 108
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v5, v0}, LbpQ;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    .line 110
    if-nez v5, :cond_4

    .line 112
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 114
    goto :goto_0

    .line 117
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_1

    .line 119
    :cond_4
    if-lez v5, :cond_2

    move v0, v2

    .line 120
    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    move v0, v2

    .line 124
    goto :goto_0

    .line 125
    :catch_1
    move-exception v0

    move v0, v2

    .line 126
    goto :goto_0

    :cond_5
    move v0, v2

    .line 129
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 153
    if-ne p1, p0, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 156
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-nez v2, :cond_2

    move v0, v1

    .line 157
    goto :goto_0

    .line 160
    :cond_2
    check-cast p1, Ljava/util/Set;

    .line 161
    invoke-virtual {p0}, LbpQ;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 162
    goto :goto_0

    .line 165
    :cond_3
    iget-object v2, p0, LbpQ;->a:Ljava/util/Comparator;

    invoke-static {v2, p1}, Lbqg;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 166
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 168
    :try_start_0
    invoke-virtual {p0}, LbpQ;->a()Lbqv;

    move-result-object v3

    .line 169
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 170
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 171
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 172
    if-eqz v5, :cond_5

    invoke-virtual {p0, v4, v5}, LbpQ;->a(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_4

    :cond_5
    move v0, v1

    .line 173
    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    move v0, v1

    .line 178
    goto :goto_0

    .line 179
    :catch_1
    move-exception v0

    move v0, v1

    .line 180
    goto :goto_0

    .line 183
    :cond_6
    invoke-virtual {p0, p1}, LbpQ;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public first()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, LbpQ;->a:LbmF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, LbpQ;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {p0}, LbpQ;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {v0}, LbmF;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, LbpQ;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
