.class final LbvW;
.super Ljava/lang/Object;
.source "Multibinder.java"

# interfaces
.implements LbwD;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbwD",
        "<",
        "Ljava/util/Collection",
        "<",
        "LbuE",
        "<TT;>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbvV;


# direct methods
.method constructor <init>(LbvV;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, LbvW;->a:LbvV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()Lbuv;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, LbvW;->a:LbvV;

    invoke-static {v0}, LbvV;->a(LbvV;)Lbuv;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0}, LbvW;->a()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LbuE",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 436
    iget-object v1, p0, LbvW;->a:LbvV;

    invoke-static {v1}, LbvV;->a(LbvV;)Z

    move-result v1

    const-string v2, "Multibinder is not initialized"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LbvT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 437
    iget-object v1, p0, LbvW;->a:LbvV;

    invoke-static {v1}, LbvV;->a(LbvV;)LbmF;

    move-result-object v1

    invoke-virtual {v1}, LbmF;->size()I

    move-result v2

    .line 439
    new-array v3, v2, [LbuE;

    move v1, v0

    .line 440
    :goto_0
    if-ge v1, v2, :cond_0

    .line 441
    iget-object v0, p0, LbvW;->a:LbvV;

    invoke-static {v0}, LbvV;->a(LbvV;)LbmF;

    move-result-object v0

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbup;

    invoke-interface {v0}, Lbup;->a()LbuE;

    move-result-object v0

    aput-object v0, v3, v1

    .line 440
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 443
    :cond_0
    invoke-static {v3}, LbmF;->a([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 464
    instance-of v0, p1, LbvW;

    if-eqz v0, :cond_0

    check-cast p1, LbvW;

    .line 466
    invoke-virtual {p1}, LbvW;->a()Lbuv;

    move-result-object v0

    invoke-virtual {p0}, LbvW;->a()Lbuv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbuv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 470
    invoke-virtual {p0}, LbvW;->a()Lbuv;

    move-result-object v0

    invoke-virtual {v0}, Lbuv;->hashCode()I

    move-result v0

    return v0
.end method
