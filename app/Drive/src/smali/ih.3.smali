.class public Lih;
.super Landroid/app/Dialog;
.source "MediaRouteControllerDialog.java"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/Button;

.field private a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/SeekBar;

.field private final a:Lil;

.field private final a:Llj;

.field private final a:Llv;

.field private a:Z

.field private b:Landroid/graphics/drawable/Drawable;

.field private b:Z

.field private c:Landroid/graphics/drawable/Drawable;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lih;-><init>(Landroid/content/Context;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 74
    invoke-static {p1, v1}, Lio;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 60
    iput-boolean v1, p0, Lih;->b:Z

    .line 75
    invoke-virtual {p0}, Lih;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 77
    invoke-static {v0}, Llj;->a(Landroid/content/Context;)Llj;

    move-result-object v0

    iput-object v0, p0, Lih;->a:Llj;

    .line 78
    new-instance v0, Lil;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lil;-><init>(Lih;Lii;)V

    iput-object v0, p0, Lih;->a:Lil;

    .line 79
    iget-object v0, p0, Lih;->a:Llj;

    invoke-virtual {v0}, Llj;->b()Llv;

    move-result-object v0

    iput-object v0, p0, Lih;->a:Llv;

    .line 80
    return-void
.end method

.method private a()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lih;->a:Llv;

    invoke-virtual {v0}, Llv;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lih;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lih;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lmo;->mediaRouteConnectingDrawable:I

    invoke-static {v0, v1}, Lio;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lih;->a:Landroid/graphics/drawable/Drawable;

    .line 270
    :cond_0
    iget-object v0, p0, Lih;->a:Landroid/graphics/drawable/Drawable;

    .line 276
    :goto_0
    return-object v0

    .line 272
    :cond_1
    iget-object v0, p0, Lih;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 273
    invoke-virtual {p0}, Lih;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lmo;->mediaRouteOnDrawable:I

    invoke-static {v0, v1}, Lio;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lih;->b:Landroid/graphics/drawable/Drawable;

    .line 276
    :cond_2
    iget-object v0, p0, Lih;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method static synthetic a(Lih;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lih;->a:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic a(Lih;)Llj;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lih;->a:Llj;

    return-object v0
.end method

.method static synthetic a(Lih;)Llv;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lih;->a:Llv;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 281
    iget-boolean v0, p0, Lih;->c:Z

    if-nez v0, :cond_0

    .line 282
    invoke-direct {p0}, Lih;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    iget-object v0, p0, Lih;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lih;->a:Landroid/widget/SeekBar;

    iget-object v1, p0, Lih;->a:Llv;

    invoke-virtual {v1}, Llv;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 285
    iget-object v0, p0, Lih;->a:Landroid/widget/SeekBar;

    iget-object v1, p0, Lih;->a:Llv;

    invoke-virtual {v1}, Llv;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lih;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lih;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lih;->a()V

    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 242
    iget-object v2, p0, Lih;->a:Llv;

    invoke-virtual {v2}, Llv;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lih;->a:Llv;

    invoke-virtual {v2}, Llv;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 243
    :cond_0
    invoke-virtual {p0}, Lih;->dismiss()V

    move v0, v1

    .line 261
    :cond_1
    :goto_0
    return v0

    .line 247
    :cond_2
    iget-object v2, p0, Lih;->a:Llv;

    invoke-virtual {v2}, Llv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lih;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    invoke-direct {p0}, Lih;->a()V

    .line 250
    invoke-direct {p0}, Lih;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 251
    iget-object v3, p0, Lih;->c:Landroid/graphics/drawable/Drawable;

    if-eq v2, v3, :cond_1

    .line 252
    iput-object v2, p0, Lih;->c:Landroid/graphics/drawable/Drawable;

    .line 258
    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 259
    invoke-virtual {p0}, Lih;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(Lih;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lih;->c:Z

    return v0
.end method

.method static synthetic a(Lih;Z)Z
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lih;->c:Z

    return p1
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 293
    iget-boolean v1, p0, Lih;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lih;->a:Llv;

    invoke-virtual {v1}, Llv;->c()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lih;)Z
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lih;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    .line 208
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 210
    iget-object v0, p0, Lih;->a:Llj;

    sget-object v1, Llg;->a:Llg;

    iget-object v2, p0, Lih;->a:Lil;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Llj;->a(Llg;Lll;I)V

    .line 212
    invoke-direct {p0}, Lih;->a()Z

    .line 213
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Lih;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 140
    sget v0, Lmq;->mr_media_route_controller_dialog:I

    invoke-virtual {p0, v0}, Lih;->setContentView(I)V

    .line 142
    sget v0, Lmp;->media_route_volume_layout:I

    invoke-virtual {p0, v0}, Lih;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lih;->a:Landroid/widget/LinearLayout;

    .line 143
    sget v0, Lmp;->media_route_volume_slider:I

    invoke-virtual {p0, v0}, Lih;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lih;->a:Landroid/widget/SeekBar;

    .line 144
    iget-object v0, p0, Lih;->a:Landroid/widget/SeekBar;

    new-instance v1, Lii;

    invoke-direct {v1, p0}, Lii;-><init>(Lih;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 180
    sget v0, Lmp;->media_route_disconnect_button:I

    invoke-virtual {p0, v0}, Lih;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lih;->a:Landroid/widget/Button;

    .line 181
    iget-object v0, p0, Lih;->a:Landroid/widget/Button;

    new-instance v1, Lik;

    invoke-direct {v1, p0}, Lik;-><init>(Lih;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lih;->a:Z

    .line 192
    invoke-direct {p0}, Lih;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0, p1}, Lih;->a(Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lih;->a:Landroid/view/View;

    .line 194
    sget v0, Lmp;->media_route_control_frame:I

    invoke-virtual {p0, v0}, Lih;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 196
    iget-object v1, p0, Lih;->a:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lih;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 198
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lih;->a:Llj;

    iget-object v1, p0, Lih;->a:Lil;

    invoke-virtual {v0, v1}, Llj;->a(Lll;)V

    .line 219
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 220
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/16 v3, 0x19

    const/4 v1, 0x1

    .line 224
    if-eq p1, v3, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_2

    .line 226
    :cond_0
    iget-object v2, p0, Lih;->a:Llv;

    if-ne p1, v3, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v2, v0}, Llv;->b(I)V

    .line 229
    :goto_1
    return v1

    :cond_1
    move v0, v1

    .line 226
    goto :goto_0

    .line 229
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 234
    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    .line 236
    :cond_0
    const/4 v0, 0x1

    .line 238
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
