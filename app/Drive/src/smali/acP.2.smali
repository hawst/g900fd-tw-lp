.class public LacP;
.super Ljava/lang/Object;
.source "DocumentScanShortcutHelper.java"


# instance fields
.field private final a:LaGM;

.field private final a:LaIc;

.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LaGM;LaIc;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, LacP;->a:Landroid/content/Context;

    .line 33
    iput-object p2, p0, LacP;->a:LaGM;

    .line 34
    iput-object p3, p0, LacP;->a:LaIc;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 44
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v0, p0, LacP;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v1

    .line 46
    if-nez v1, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    .line 49
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 50
    iget-object v2, p0, LacP;->a:Landroid/content/Context;

    sget v3, LatL;->ic_scandoc_widget:I

    invoke-static {v2, v3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    .line 52
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 53
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-interface {v1}, LaFV;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    iget-object v1, p0, LacP;->a:LaIc;

    invoke-interface {v1, p1}, LaIc;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v1

    .line 56
    const-string v2, "android.intent.extra.shortcut.INTENT"

    iget-object v3, p0, LacP;->a:Landroid/content/Context;

    .line 58
    invoke-static {v3, v1}, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method
