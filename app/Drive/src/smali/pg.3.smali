.class Lpg;
.super Ljava/lang/Object;
.source "CompositeClassLoaderExtender.java"

# interfaces
.implements Lpe;


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<+",
            "Lpe;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lpw;


# direct methods
.method public constructor <init>(Ljava/util/Collection;Lpw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lpe;",
            ">;",
            "Lpw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lpg;->a:Ljava/util/Collection;

    .line 22
    iput-object p2, p0, Lpg;->a:Lpw;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/io/File;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 28
    iget-object v0, p0, Lpg;->a:Lpw;

    const-string v1, "Attempting to patch the classloader using the following patchers %s"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lpg;->a:Ljava/util/Collection;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpw;->a(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lpg;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpe;

    .line 33
    :try_start_0
    iget-object v1, p0, Lpg;->a:Lpw;

    const-string v3, "Trying %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lpw;->a(Ljava/lang/String;)V

    .line 34
    invoke-interface {v0, p1, p2}, Lpe;->a(Ljava/util/List;Ljava/io/File;)V

    .line 35
    iget-object v1, p0, Lpg;->a:Lpw;

    const-string v3, "Patching was successful."

    invoke-virtual {v1, v3}, Lpw;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lpm; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    return-void

    .line 37
    :catch_0
    move-exception v1

    .line 38
    iget-object v3, p0, Lpg;->a:Lpw;

    const-string v4, "%s failed."

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Lpw;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 42
    :cond_0
    new-instance v0, Lpm;

    const-string v1, "Unable to patch the class path. Check logcat for details."

    invoke-direct {v0, v1}, Lpm;-><init>(Ljava/lang/String;)V

    throw v0
.end method
