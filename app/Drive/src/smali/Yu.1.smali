.class public final enum LYu;
.super Ljava/lang/Enum;
.source "PunchSwipeableSvgViewerBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LYu;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LYu;

.field private static final synthetic a:[LYu;

.field public static final enum b:LYu;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 106
    new-instance v0, LYu;

    const-string v1, "STATE_VIEWER"

    sget v2, LpN;->editor_state_view_state:I

    invoke-direct {v0, v1, v3, v2}, LYu;-><init>(Ljava/lang/String;II)V

    sput-object v0, LYu;->a:LYu;

    .line 111
    new-instance v0, LYu;

    const-string v1, "COMPLETE_VIEWER"

    sget v2, LpN;->editor_state_view_full_slide:I

    invoke-direct {v0, v1, v4, v2}, LYu;-><init>(Ljava/lang/String;II)V

    sput-object v0, LYu;->b:LYu;

    .line 102
    const/4 v0, 0x2

    new-array v0, v0, [LYu;

    sget-object v1, LYu;->a:LYu;

    aput-object v1, v0, v3

    sget-object v1, LYu;->b:LYu;

    aput-object v1, v0, v4

    sput-object v0, LYu;->a:[LYu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput p3, p0, LYu;->a:I

    .line 117
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LYu;
    .locals 1

    .prologue
    .line 102
    const-class v0, LYu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LYu;

    return-object v0
.end method

.method public static values()[LYu;
    .locals 1

    .prologue
    .line 102
    sget-object v0, LYu;->a:[LYu;

    invoke-virtual {v0}, [LYu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LYu;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, LYu;->a:I

    return v0
.end method
