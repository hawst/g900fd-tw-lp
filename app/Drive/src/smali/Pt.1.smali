.class public final LPt;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPq;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPw;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPu;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LPt;->a:LbrA;

    .line 42
    const-class v0, LPq;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LPt;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPt;->a:Lbsk;

    .line 45
    const-class v0, LPw;

    invoke-static {v0, v2}, LPt;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPt;->b:Lbsk;

    .line 48
    const-class v0, LPu;

    invoke-static {v0, v2}, LPt;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPt;->c:Lbsk;

    .line 51
    const-class v0, LPp;

    invoke-static {v0, v2}, LPt;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPt;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 21

    .prologue
    .line 128
    packed-switch p1, :pswitch_data_0

    .line 298
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown binding ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 130
    :pswitch_1
    new-instance v1, LPq;

    move-object/from16 v0, p0

    iget-object v2, v0, LPt;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 133
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LPt;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->t:Lbsk;

    .line 131
    invoke-static {v2, v3}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laja;

    move-object/from16 v0, p0

    iget-object v3, v0, LPt;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 139
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LPt;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 137
    invoke-static {v3, v4}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGM;

    move-object/from16 v0, p0

    iget-object v4, v0, LPt;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->M:Lbsk;

    .line 145
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LPt;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LCw;

    iget-object v5, v5, LCw;->M:Lbsk;

    .line 143
    invoke-static {v4, v5}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LBZ;

    move-object/from16 v0, p0

    iget-object v5, v0, LPt;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 151
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LPt;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 149
    invoke-static {v5, v6}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LtK;

    move-object/from16 v0, p0

    iget-object v6, v0, LPt;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LqD;

    iget-object v6, v6, LqD;->c:Lbsk;

    .line 157
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LPt;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LqD;

    iget-object v7, v7, LqD;->c:Lbsk;

    .line 155
    invoke-static {v6, v7}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LqK;

    move-object/from16 v0, p0

    iget-object v7, v0, LPt;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LUh;

    iget-object v7, v7, LUh;->f:Lbsk;

    .line 163
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LPt;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LUh;

    iget-object v8, v8, LUh;->f:Lbsk;

    .line 161
    invoke-static {v7, v8}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LUi;

    move-object/from16 v0, p0

    iget-object v8, v0, LPt;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LabP;

    iget-object v8, v8, LabP;->e:Lbsk;

    .line 169
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LPt;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LabP;

    iget-object v9, v9, LabP;->e:Lbsk;

    .line 167
    invoke-static {v8, v9}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lacj;

    move-object/from16 v0, p0

    iget-object v9, v0, LPt;->a:LbrA;

    iget-object v9, v9, LbrA;->a:Lwc;

    iget-object v9, v9, Lwc;->j:Lbsk;

    .line 175
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LPt;->a:LbrA;

    iget-object v10, v10, LbrA;->a:Lwc;

    iget-object v10, v10, Lwc;->j:Lbsk;

    .line 173
    invoke-static {v9, v10}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LvU;

    move-object/from16 v0, p0

    iget-object v10, v0, LPt;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LalC;

    iget-object v10, v10, LalC;->y:Lbsk;

    .line 181
    invoke-virtual {v10}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LPt;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LalC;

    iget-object v11, v11, LalC;->y:Lbsk;

    .line 179
    invoke-static {v10, v11}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lald;

    move-object/from16 v0, p0

    iget-object v11, v0, LPt;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LtQ;

    iget-object v11, v11, LtQ;->w:Lbsk;

    .line 187
    invoke-virtual {v11}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LPt;->a:LbrA;

    iget-object v12, v12, LbrA;->a:LtQ;

    iget-object v12, v12, LtQ;->w:Lbsk;

    .line 185
    invoke-static {v11, v12}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LsP;

    move-object/from16 v0, p0

    iget-object v12, v0, LPt;->a:LbrA;

    iget-object v12, v12, LbrA;->a:LpG;

    iget-object v12, v12, LpG;->v:Lbsk;

    .line 193
    invoke-virtual {v12}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, LPt;->a:LbrA;

    iget-object v13, v13, LbrA;->a:LpG;

    iget-object v13, v13, LpG;->v:Lbsk;

    .line 191
    invoke-static {v12, v13}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LtB;

    move-object/from16 v0, p0

    iget-object v13, v0, LPt;->a:LbrA;

    iget-object v13, v13, LbrA;->a:LaEV;

    iget-object v13, v13, LaEV;->e:Lbsk;

    .line 199
    invoke-virtual {v13}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LPt;->a:LbrA;

    iget-object v14, v14, LbrA;->a:LaEV;

    iget-object v14, v14, LaEV;->e:Lbsk;

    .line 197
    invoke-static {v13, v14}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LPt;->a:LbrA;

    iget-object v14, v14, LbrA;->a:LabP;

    iget-object v14, v14, LabP;->k:Lbsk;

    .line 205
    invoke-virtual {v14}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, LPt;->a:LbrA;

    iget-object v15, v15, LbrA;->a:LabP;

    iget-object v15, v15, LabP;->k:Lbsk;

    .line 203
    invoke-static {v14, v15}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LabQ;

    move-object/from16 v0, p0

    iget-object v15, v0, LPt;->a:LbrA;

    iget-object v15, v15, LbrA;->a:LabP;

    iget-object v15, v15, LabP;->h:Lbsk;

    .line 211
    invoke-virtual {v15}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, LbrA;->a:LabP;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, LabP;->h:Lbsk;

    move-object/from16 v16, v0

    .line 209
    invoke-static/range {v15 .. v16}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lach;

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, LbrA;->a:Lwc;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lwc;->l:Lbsk;

    move-object/from16 v16, v0

    .line 217
    invoke-virtual/range {v16 .. v16}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LbrA;->a:Lwc;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lwc;->l:Lbsk;

    move-object/from16 v17, v0

    .line 215
    invoke-static/range {v16 .. v17}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, LvY;

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LbrA;->a:LalC;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LalC;->L:Lbsk;

    move-object/from16 v17, v0

    .line 223
    invoke-virtual/range {v17 .. v17}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, LbrA;->a:LalC;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, LalC;->L:Lbsk;

    move-object/from16 v18, v0

    .line 221
    invoke-static/range {v17 .. v18}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, LaKR;

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, LbrA;->a:LpG;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, LpG;->u:Lbsk;

    move-object/from16 v18, v0

    .line 229
    invoke-virtual/range {v18 .. v18}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LbrA;->a:LpG;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LpG;->u:Lbsk;

    move-object/from16 v19, v0

    .line 227
    invoke-static/range {v18 .. v19}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, LWL;

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LbrA;->a:LbiH;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LbiH;->bg:Lbsk;

    move-object/from16 v19, v0

    .line 235
    invoke-virtual/range {v19 .. v19}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, LPt;->a:LbrA;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LbrA;->a:LbiH;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LbiH;->bg:Lbsk;

    move-object/from16 v20, v0

    .line 233
    invoke-static/range {v19 .. v20}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, LbiP;

    invoke-direct/range {v1 .. v19}, LPq;-><init>(Laja;LaGM;LBZ;LtK;LqK;LUi;Lacj;LvU;Lald;LsP;LtB;Ljava/lang/String;LabQ;Lach;LvY;LaKR;LWL;LbiP;)V

    .line 296
    :goto_0
    return-object v1

    .line 242
    :pswitch_2
    new-instance v5, LPw;

    move-object/from16 v0, p0

    iget-object v1, v0, LPt;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->J:Lbsk;

    .line 245
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LPt;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->J:Lbsk;

    .line 243
    invoke-static {v1, v2}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lagl;

    move-object/from16 v0, p0

    iget-object v2, v0, LPt;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->e:Lbsk;

    .line 251
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LPt;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laek;

    iget-object v3, v3, Laek;->e:Lbsk;

    .line 249
    invoke-static {v2, v3}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laeu;

    move-object/from16 v0, p0

    iget-object v3, v0, LPt;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 257
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LPt;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 255
    invoke-static {v3, v4}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGM;

    move-object/from16 v0, p0

    iget-object v4, v0, LPt;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lafc;

    iget-object v4, v4, Lafc;->d:Lbsk;

    .line 263
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, LPt;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lafc;

    iget-object v6, v6, Lafc;->d:Lbsk;

    .line 261
    invoke-static {v4, v6}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LafB;

    invoke-direct {v5, v1, v2, v3, v4}, LPw;-><init>(Lagl;Laeu;LaGM;LafB;)V

    move-object v1, v5

    .line 268
    goto :goto_0

    .line 270
    :pswitch_3
    new-instance v5, LPu;

    move-object/from16 v0, p0

    iget-object v1, v0, LPt;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->t:Lbsk;

    .line 273
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LPt;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->t:Lbsk;

    .line 271
    invoke-static {v1, v2}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LCM;

    move-object/from16 v0, p0

    iget-object v2, v0, LPt;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LPt;

    iget-object v2, v2, LPt;->d:Lbsk;

    .line 279
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LPt;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LPt;

    iget-object v3, v3, LPt;->d:Lbsk;

    .line 277
    invoke-static {v2, v3}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LPp;

    move-object/from16 v0, p0

    iget-object v3, v0, LPt;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->t:Lbsk;

    .line 285
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LPt;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->t:Lbsk;

    .line 283
    invoke-static {v3, v4}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laja;

    move-object/from16 v0, p0

    iget-object v4, v0, LPt;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lwc;

    iget-object v4, v4, Lwc;->l:Lbsk;

    .line 291
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, LPt;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lwc;

    iget-object v6, v6, Lwc;->l:Lbsk;

    .line 289
    invoke-static {v4, v6}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LvY;

    invoke-direct {v5, v1, v2, v3, v4}, LPu;-><init>(LCM;LPp;Laja;LvY;)V

    move-object v1, v5

    .line 296
    goto/16 :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x2a4
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 319
    packed-switch p2, :pswitch_data_0

    .line 330
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 321
    :pswitch_0
    check-cast p1, LPs;

    .line 323
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->a:Lbsk;

    .line 326
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPq;

    .line 323
    invoke-virtual {p1, v0}, LPs;->provideEntryActionHelper(LPq;)LPp;

    move-result-object v0

    return-object v0

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 105
    const-class v0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x5d

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LPt;->a(LbuP;LbuB;)V

    .line 108
    const-class v0, LPq;

    iget-object v1, p0, LPt;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LPt;->a(Ljava/lang/Class;Lbsk;)V

    .line 109
    const-class v0, LPw;

    iget-object v1, p0, LPt;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LPt;->a(Ljava/lang/Class;Lbsk;)V

    .line 110
    const-class v0, LPu;

    iget-object v1, p0, LPt;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LPt;->a(Ljava/lang/Class;Lbsk;)V

    .line 111
    const-class v0, LPp;

    iget-object v1, p0, LPt;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LPt;->a(Ljava/lang/Class;Lbsk;)V

    .line 112
    iget-object v0, p0, LPt;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2a5

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 114
    iget-object v0, p0, LPt;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2a8

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 116
    iget-object v0, p0, LPt;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2a4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 118
    iget-object v0, p0, LPt;->d:Lbsk;

    const-class v1, LPs;

    const/16 v2, 0x62

    invoke-virtual {p0, v1, v2}, LPt;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 120
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 305
    packed-switch p1, :pswitch_data_0

    .line 313
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    .line 309
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    .line 310
    invoke-virtual {v0, p2}, LPt;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V

    .line 315
    return-void

    .line 305
    :pswitch_data_0
    .packed-switch 0x5d
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->e:Lbsk;

    .line 63
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPt;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->e:Lbsk;

    .line 61
    invoke-static {v0, v1}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    iput-object v0, p1, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lacj;

    .line 67
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->j:Lbsk;

    .line 70
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPt;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->j:Lbsk;

    .line 68
    invoke-static {v0, v1}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p1, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LvU;

    .line 74
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->c:Lbsk;

    .line 77
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPt;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPt;

    iget-object v1, v1, LPt;->c:Lbsk;

    .line 75
    invoke-static {v0, v1}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPu;

    iput-object v0, p1, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPu;

    .line 81
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->g:Lbsk;

    .line 84
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPt;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->g:Lbsk;

    .line 82
    invoke-static {v0, v1}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacF;

    iput-object v0, p1, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LacF;

    .line 88
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->L:Lbsk;

    .line 91
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPo;

    iput-object v0, p1, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPo;

    .line 93
    iget-object v0, p0, LPt;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 96
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPt;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 94
    invoke-static {v0, v1}, LPt;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LtK;

    .line 100
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method
