.class public LaeB;
.super Lbxb;
.source "AclEntry.java"


# instance fields
.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lqv;

.field private a:Lqx;

.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lbxb;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, LaeB;->a:Ljava/lang/String;

    .line 32
    sget-object v0, Lqv;->f:Lqv;

    iput-object v0, p0, LaeB;->a:Lqv;

    .line 33
    sget-object v0, Lqx;->e:Lqx;

    iput-object v0, p0, LaeB;->a:Lqx;

    .line 34
    const-class v0, Lqq;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LaeB;->a:Ljava/util/Set;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, LaeB;->a:Z

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lbxb;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, LaeB;->a:Ljava/lang/String;

    .line 32
    sget-object v0, Lqv;->f:Lqv;

    iput-object v0, p0, LaeB;->a:Lqv;

    .line 33
    sget-object v0, Lqx;->e:Lqx;

    iput-object v0, p0, LaeB;->a:Lqx;

    .line 34
    const-class v0, Lqq;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LaeB;->a:Ljava/util/Set;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, LaeB;->a:Z

    .line 41
    invoke-static {p2, p1}, Lqz;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaeB;->n(Ljava/lang/String;)V

    .line 42
    const-string v0, "http://schemas.google.com/acl/2007#accessRule"

    invoke-virtual {p0, v0}, LaeB;->p(Ljava/lang/String;)V

    .line 43
    const-string v0, "http://schemas.google.com/g/2005#kind"

    invoke-virtual {p0, v0}, LaeB;->q(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lqo;)LaeB;
    .locals 3

    .prologue
    .line 151
    new-instance v1, LaeB;

    invoke-virtual {p1}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, LaeB;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 152
    invoke-virtual {p1}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LaeB;->a(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p1}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lqz;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    invoke-virtual {v1, v0}, LaeB;->v(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p1}, Lqo;->a()Lqv;

    move-result-object v0

    sget-object v2, Lqv;->e:Lqv;

    if-eq v0, v2, :cond_0

    .line 156
    invoke-virtual {p1}, Lqo;->a()Lqv;

    move-result-object v0

    invoke-virtual {v0}, Lqv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LaeB;->b(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p1}, Lqo;->a()Lqx;

    move-result-object v0

    invoke-virtual {v0}, Lqx;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LaeB;->c(Ljava/lang/String;)V

    .line 159
    :cond_0
    invoke-virtual {p1}, Lqo;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqq;

    .line 160
    invoke-virtual {v1, v0}, LaeB;->a(Lqq;)V

    goto :goto_0

    .line 162
    :cond_1
    return-object v1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LaeB;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, LaeB;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lqo;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lqs;

    invoke-direct {v0}, Lqs;-><init>()V

    .line 140
    invoke-virtual {v0, p1}, Lqs;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lqs;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Ljava/lang/String;

    .line 141
    invoke-virtual {v0, v1}, Lqs;->a(Ljava/lang/String;)Lqs;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Lqx;

    .line 142
    invoke-virtual {v0, v1}, Lqs;->a(Lqx;)Lqs;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Lqv;

    .line 143
    invoke-virtual {v0, v1}, Lqs;->a(Lqv;)Lqs;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Ljava/util/Set;

    .line 144
    invoke-virtual {v0, v1}, Lqs;->a(Ljava/util/Set;)Lqs;

    move-result-object v0

    iget-boolean v1, p0, LaeB;->a:Z

    .line 145
    invoke-virtual {v0, v1}, Lqs;->a(Z)Lqs;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lqs;->a()Lqo;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqv;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, LaeB;->a:Lqv;

    return-object v0
.end method

.method public a()Lqx;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, LaeB;->a:Lqx;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lbxb;->a()V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, LaeB;->a:Ljava/lang/String;

    .line 118
    sget-object v0, Lqv;->f:Lqv;

    iput-object v0, p0, LaeB;->a:Lqv;

    .line 119
    sget-object v0, Lqx;->e:Lqx;

    iput-object v0, p0, LaeB;->a:Lqx;

    .line 120
    iget-object v0, p0, LaeB;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, LaeB;->a:Z

    .line 122
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, LaeB;->a:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public a(Lqq;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, LaeB;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, LaeB;->a:Z

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 81
    :try_start_0
    invoke-static {p1}, Lqv;->a(Ljava/lang/String;)Lqv;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 86
    :goto_0
    sget-object v1, Lqv;->d:Lqv;

    if-ne v0, v1, :cond_1

    .line 87
    iput-boolean v4, p0, LaeB;->a:Z

    .line 91
    :cond_0
    :goto_1
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    const-string v0, "AclEntry"

    const-string v1, "Unknown role: %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 84
    sget-object v0, Lqv;->f:Lqv;

    goto :goto_0

    .line 88
    :cond_1
    sget-object v1, Lqv;->f:Lqv;

    if-eq v0, v1, :cond_0

    .line 89
    iput-object v0, p0, LaeB;->a:Lqv;

    goto :goto_1
.end method

.method public c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 103
    :try_start_0
    invoke-static {p1}, Lqx;->a(Ljava/lang/String;)Lqx;

    move-result-object v0

    iput-object v0, p0, LaeB;->a:Lqx;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    const-string v0, "AclEntry"

    const-string v1, "Unknown scope: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 106
    sget-object v0, Lqx;->e:Lqx;

    iput-object v0, p0, LaeB;->a:Lqx;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\rACL Entry: "

    .line 127
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Lqv;

    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    .line 129
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Ljava/util/Set;

    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    .line 131
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Lqx;

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaeB;->a:Ljava/lang/String;

    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LaeB;->a:Z

    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
