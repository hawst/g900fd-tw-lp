.class public final LaPk;
.super LaPi;

# interfaces
.implements LaCF;
.implements LaCG;


# instance fields
.field private final a:LaPh;

.field private final a:LaPl;

.field private final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)V
    .locals 2

    invoke-direct {p0, p2, p3}, LaPi;-><init>(Lcom/google/android/gms/internal/dx;LaPh;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaPk;->a:Ljava/lang/Object;

    iput-object p3, p0, LaPk;->a:LaPh;

    new-instance v0, LaPl;

    iget-object v1, p2, Lcom/google/android/gms/internal/dx;->a:Lcom/google/android/gms/internal/fa;

    iget v1, v1, Lcom/google/android/gms/internal/fa;->c:I

    invoke-direct {v0, p1, p0, p0, v1}, LaPl;-><init>(Landroid/content/Context;LaCF;LaCG;I)V

    iput-object v0, p0, LaPk;->a:LaPl;

    iget-object v0, p0, LaPk;->a:LaPl;

    invoke-virtual {v0}, LaPl;->a()V

    return-void
.end method


# virtual methods
.method public a()LaPq;
    .locals 2

    iget-object v1, p0, LaPk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaPk;->a:LaPl;

    invoke-virtual {v0}, LaPl;->a()LaPq;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(LaCC;)V
    .locals 3

    iget-object v0, p0, LaPk;->a:LaPh;

    new-instance v1, Lcom/google/android/gms/internal/dz;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/dz;-><init>(I)V

    invoke-interface {v0, v1}, LaPh;->a(Lcom/google/android/gms/internal/dz;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    invoke-virtual {p0}, LaPk;->d()V

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v1, p0, LaPk;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaPk;->a:LaPl;

    invoke-virtual {v0}, LaPl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaPk;->a:LaPl;

    invoke-virtual {v0}, LaPl;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LaPk;->a:LaPl;

    invoke-virtual {v0}, LaPl;->b()V

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public p_()V
    .locals 1

    const-string v0, "Disconnected from remote ad request service."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    return-void
.end method
