.class public LDa;
.super Ljava/lang/Object;
.source "SelectionMoveHelper.java"


# instance fields
.field private final a:LJR;


# direct methods
.method public constructor <init>(LJR;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, LDa;->a:LJR;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, LDa;->a:LJR;

    .line 30
    invoke-virtual {v0}, LJR;->a()LbmF;

    move-result-object v0

    new-instance v1, LDb;

    invoke-direct {v1, p0}, LDb;-><init>(LDa;)V

    .line 29
    invoke-static {v0, v1}, LblV;->a(Ljava/util/Collection;LbiG;)Ljava/util/Collection;

    move-result-object v0

    .line 37
    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 38
    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 37
    invoke-static {p1, v0, v1}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(Landroid/content/Context;Lbmv;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 39
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 40
    iget-object v0, p0, LDa;->a:LJR;

    invoke-virtual {v0}, LJR;->a()V

    .line 41
    return-void
.end method
