.class LJA;
.super Ljava/lang/Object;
.source "HelpCardAdapter.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:LJy;

.field final synthetic a:Landroid/view/View;


# direct methods
.method constructor <init>(LJy;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, LJA;->a:LJy;

    iput-object p2, p0, LJA;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, LJA;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 124
    iget-object v0, p0, LJA;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 125
    iget-object v1, p0, LJA;->a:Landroid/view/View;

    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v4, v2, v3

    aput v0, v2, v4

    invoke-static {v1, v2}, Lxm;->b(Landroid/view/View;[I)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    iget-object v1, p0, LJA;->a:LJy;

    .line 126
    invoke-static {v1}, LJy;->a(LJy;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/Context;)Lxr;

    move-result-object v0

    const/16 v1, 0x1f4

    .line 127
    invoke-virtual {v0, v1}, Lxr;->a(I)Lxr;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lxr;->a()Landroid/animation/Animator;

    .line 129
    return v3
.end method
