.class LbkB;
.super Ljava/lang/Object;
.source "LocalCache.java"

# interfaces
.implements Lbkr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lbkr",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field volatile a:LbkF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field final e:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1097
    invoke-static {}, LbjQ;->a()LbkF;

    move-result-object v0

    iput-object v0, p0, LbkB;->a:LbkF;

    .line 1032
    iput-object p1, p0, LbkB;->a:Ljava/lang/Object;

    .line 1033
    iput p2, p0, LbkB;->a:I

    .line 1034
    iput-object p3, p0, LbkB;->e:Lbkr;

    .line 1035
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1108
    iget v0, p0, LbkB;->a:I

    return v0
.end method

.method public a()J
    .locals 1

    .prologue
    .line 1044
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()LbkF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1100
    iget-object v0, p0, LbkB;->a:LbkF;

    return-object v0
.end method

.method public a()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1112
    iget-object v0, p0, LbkB;->e:Lbkr;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1038
    iget-object v0, p0, LbkB;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1048
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LbkF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbkF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1104
    iput-object p1, p0, LbkB;->a:LbkF;

    .line 1105
    return-void
.end method

.method public a(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1056
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()J
    .locals 1

    .prologue
    .line 1070
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1052
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1074
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1064
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1060
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1082
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1078
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1090
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1086
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
