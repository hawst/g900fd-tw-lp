.class public LAa;
.super Landroid/widget/BaseAdapter;
.source "CompoundDocEntriesAdapterImpl.java"

# interfaces
.implements LzZ;


# instance fields
.field private a:LAD;

.field private final a:LBI;

.field private final a:LBa;

.field private final a:LJy;

.field private a:Laqs;


# direct methods
.method public constructor <init>(LJy;LBI;LBa;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 38
    iput-object p1, p0, LAa;->a:LJy;

    .line 39
    iput-object p2, p0, LAa;->a:LBI;

    .line 40
    iput-object p3, p0, LAa;->a:LBa;

    .line 41
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 59
    new-instance v0, LAb;

    invoke-direct {v0, p0}, LAb;-><init>(LAa;)V

    .line 70
    iget-object v1, p0, LAa;->a:Laqs;

    invoke-virtual {v1, v0}, Laqs;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 71
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0}, LJy;->a()V

    .line 72
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0}, LJy;->getCount()I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, LAa;->a:LAD;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, LAa;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 229
    :cond_0
    const/4 p1, -0x1

    .line 240
    :cond_1
    :goto_0
    return p1

    .line 232
    :cond_2
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0}, LJy;->getCount()I

    move-result v0

    .line 234
    if-lt p1, v0, :cond_1

    .line 237
    iget-object v1, p0, LAa;->a:LAD;

    sub-int v2, p1, v0

    .line 238
    invoke-interface {v1, v2}, LAD;->a(I)I

    move-result v1

    add-int p1, v0, v1

    goto :goto_0
.end method

.method public a(Landroid/view/View;)LIB;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->a(Landroid/view/View;)LIB;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, LAa;->a:LAD;

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0}, LAD;->a()V

    goto :goto_0
.end method

.method public a(LAD;LapF;)V
    .locals 4

    .prologue
    .line 48
    iput-object p1, p0, LAa;->a:LAD;

    .line 49
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0, p2}, LJy;->a(LapF;)V

    .line 50
    iget-object v0, p0, LAa;->a:LBa;

    invoke-virtual {v0, p2}, LBa;->a(LapF;)V

    .line 51
    new-instance v0, Laqs;

    iget-object v1, p0, LAa;->a:LJy;

    iget-object v2, p0, LAa;->a:LBI;

    iget-object v3, p0, LAa;->a:LBa;

    invoke-static {v1, p1, v2, v3}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v1

    invoke-direct {v0, v1}, Laqs;-><init>(LbmF;)V

    iput-object v0, p0, LAa;->a:Laqs;

    .line 55
    invoke-direct {p0}, LAa;->c()V

    .line 56
    return-void
.end method

.method public a(LCs;I)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0}, LJy;->getCount()I

    move-result v0

    .line 183
    iget-object v1, p0, LAa;->a:LAD;

    add-int/2addr v0, p2

    invoke-interface {v1, p1, v0}, LAD;->a(LCs;I)V

    .line 184
    return-void
.end method

.method public a(LQX;)V
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p1}, LQX;->a()LCl;

    move-result-object v0

    .line 129
    iget-object v1, p0, LAa;->a:LJy;

    invoke-virtual {v1, v0}, LJy;->a(LCl;)V

    .line 130
    iget-object v0, p0, LAa;->a:LAD;

    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->a(LQX;)V

    goto :goto_0
.end method

.method public a(LaFX;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, LAa;->a:LAD;

    if-nez v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->a(LaFX;)V

    .line 142
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0}, LJy;->a()V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->a(Landroid/view/View;)V

    .line 163
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, LAa;->a:LAD;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0}, LAD;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0}, LAD;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, LAa;->a:LBI;

    invoke-virtual {v0}, LBI;->getCount()I

    move-result v0

    return v0
.end method

.method public b(I)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 245
    iget-object v1, p0, LAa;->a:LAD;

    if-eqz v1, :cond_0

    if-gez p1, :cond_2

    :cond_0
    move p1, v0

    .line 262
    :cond_1
    :goto_0
    return p1

    .line 249
    :cond_2
    iget-object v1, p0, LAa;->a:LJy;

    invoke-virtual {v1}, LJy;->getCount()I

    move-result v1

    .line 251
    if-lt p1, v1, :cond_1

    .line 254
    iget-object v2, p0, LAa;->a:LAD;

    sub-int v3, p1, v1

    .line 255
    invoke-interface {v2, v3}, LAD;->b(I)I

    move-result v2

    .line 256
    if-ltz v2, :cond_3

    .line 257
    add-int p1, v1, v2

    goto :goto_0

    :cond_3
    move p1, v0

    .line 259
    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, LAa;->a:LAD;

    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0}, LAD;->b()V

    goto :goto_0
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->b(Landroid/view/View;)V

    .line 168
    return-void
.end method

.method public c(I)I
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->c(I)I

    move-result v0

    .line 278
    invoke-virtual {p0}, LAa;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, LAa;->a:Laqs;

    invoke-virtual {v0}, Laqs;->getCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, LAa;->a:Laqs;

    invoke-virtual {v0, p1}, Laqs;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, LAa;->a:Laqs;

    invoke-virtual {v0, p1}, Laqs;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, LAa;->a:Laqs;

    invoke-virtual {v0, p1}, Laqs;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public getPositionForSection(I)I
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0}, LJy;->getCount()I

    move-result v0

    .line 113
    iget-object v1, p0, LAa;->a:LAD;

    invoke-interface {v1, p1}, LAD;->getPositionForSection(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, LAa;->a:LJy;

    invoke-virtual {v0}, LJy;->getCount()I

    move-result v0

    .line 119
    sub-int v0, p1, v0

    .line 120
    if-gez v0, :cond_0

    .line 121
    const/4 v0, 0x0

    .line 123
    :cond_0
    iget-object v1, p0, LAa;->a:LAD;

    invoke-interface {v1, v0}, LAD;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0}, LAD;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, LAa;->a:Laqs;

    invoke-virtual {v0, p1, p2, p3}, Laqs;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, LAa;->a:Laqs;

    invoke-virtual {v0}, Laqs;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1}, LAD;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 210
    iget-object v1, p0, LAa;->a:LJy;

    invoke-virtual {v1}, LJy;->getCount()I

    move-result v2

    .line 213
    if-lt p2, v2, :cond_2

    .line 214
    sub-int v1, p2, v2

    .line 216
    :goto_0
    if-gez v1, :cond_1

    .line 219
    :goto_1
    if-ge p2, v2, :cond_0

    .line 220
    sub-int v1, v2, p2

    sub-int/2addr p3, v1

    .line 222
    :cond_0
    iget-object v1, p0, LAa;->a:LAD;

    sub-int v2, p4, v2

    invoke-interface {v1, p1, v0, p3, v2}, LAD;->onScroll(Landroid/widget/AbsListView;III)V

    .line 224
    return-void

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, LAa;->a:LAD;

    invoke-interface {v0, p1, p2}, LAD;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 205
    return-void
.end method
