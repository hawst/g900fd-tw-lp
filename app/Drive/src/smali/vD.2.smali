.class LvD;
.super Landroid/os/AsyncTask;
.source "DetailActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGu;

.field final synthetic a:LvC;


# direct methods
.method constructor <init>(LvC;LaGu;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, LvD;->a:LvC;

    iput-object p2, p0, LvD;->a:LaGu;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 212
    :try_start_0
    iget-object v0, p0, LvD;->a:LaGu;

    if-nez v0, :cond_0

    .line 213
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    .line 215
    :cond_0
    iget-object v0, p0, LvD;->a:LvC;

    iget-object v0, v0, LvC;->a:LaGM;

    iget-object v2, p0, LvD;->a:LaGu;

    invoke-interface {v2}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_1

    invoke-interface {v0}, LaGu;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LvD;->a:LvC;

    iget-object v0, v0, LvC;->a:LVm;

    .line 217
    invoke-virtual {v0}, LVm;->a()LbmY;

    move-result-object v0

    iget-object v2, p0, LvD;->a:LaGu;

    invoke-interface {v2}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v2}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 216
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 219
    :catch_0
    move-exception v0

    .line 220
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 226
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LvD;->a:LvC;

    invoke-virtual {v0}, LvC;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, LvD;->a:LvC;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LvC;->a(LvC;Z)Z

    .line 228
    iget-object v0, p0, LvD;->a:LvC;

    invoke-virtual {v0}, LvC;->m()V

    .line 230
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 208
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LvD;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 208
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LvD;->a(Ljava/lang/Boolean;)V

    return-void
.end method
