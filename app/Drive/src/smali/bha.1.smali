.class public Lbha;
.super Ljava/lang/Object;
.source "PdfCreator.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private final a:LbgX;

.field private final a:LbgZ;

.field private a:Lbhc;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-class v1, Lbha;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbha;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>(Lbhf;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Lbhf;->a()LbgX;

    move-result-object v0

    iput-object v0, p0, Lbha;->a:LbgX;

    .line 50
    invoke-virtual {p1}, Lbhf;->a()LbgZ;

    move-result-object v0

    iput-object v0, p0, Lbha;->a:LbgZ;

    .line 51
    iput-object p2, p0, Lbha;->a:Ljava/lang/String;

    .line 52
    return-void
.end method

.method private a(LbgM;Lbhc;LbgY;)LbgM;
    .locals 4

    .prologue
    .line 116
    .line 117
    sget-object v0, Lbhb;->a:[I

    invoke-virtual {p2}, Lbhc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 139
    sget-object v0, Lbhc;->a:Lbhc;

    iput-object v0, p0, Lbha;->a:Lbhc;

    .line 141
    :cond_0
    :goto_0
    return-object p1

    .line 121
    :pswitch_0
    new-instance v0, LbgM;

    invoke-virtual {p1}, LbgM;->b()F

    move-result v1

    invoke-virtual {p1}, LbgM;->a()F

    move-result v2

    invoke-direct {v0, v1, v2}, LbgM;-><init>(FF)V

    .line 122
    sget-object v1, Lbhc;->b:Lbhc;

    iput-object v1, p0, Lbha;->a:Lbhc;

    move-object p1, v0

    .line 123
    goto :goto_0

    .line 129
    :pswitch_1
    invoke-virtual {p3}, LbgY;->b()F

    move-result v0

    .line 130
    invoke-static {v0}, Lbha;->a(F)Z

    move-result v1

    .line 131
    if-eqz v1, :cond_1

    iget-object v2, p0, Lbha;->a:Lbhc;

    sget-object v3, Lbhc;->b:Lbhc;

    if-eq v2, v3, :cond_2

    :cond_1
    if-nez v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 133
    :cond_2
    new-instance v0, LbgM;

    invoke-virtual {p1}, LbgM;->b()F

    move-result v1

    invoke-virtual {p1}, LbgM;->a()F

    move-result v2

    invoke-direct {v0, v1, v2}, LbgM;-><init>(FF)V

    .line 134
    sget-object v1, Lbhc;->b:Lbhc;

    iput-object v1, p0, Lbha;->a:Lbhc;

    move-object p1, v0

    goto :goto_0

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(F)Z
    .locals 2

    .prologue
    .line 151
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, p0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3dcccccd    # 0.1f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LbgM;Lbhc;Ljava/lang/String;)Ljava/io/File;
    .locals 12

    .prologue
    .line 64
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 65
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    sget-object v0, Lbha;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Writing PDF file to: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    :try_start_0
    iget-object v0, p0, Lbha;->a:Ljava/lang/String;

    invoke-static {v3, v0}, LbgP;->a(Ljava/io/File;Ljava/lang/String;)LbgP;

    move-result-object v4

    .line 69
    invoke-virtual {v4}, LbgP;->a()V

    .line 70
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbha;->a:LbgX;

    invoke-virtual {v0}, LbgX;->a()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 71
    iget-object v0, p0, Lbha;->a:LbgX;

    invoke-virtual {v0, v2}, LbgX;->a(I)LbgY;

    move-result-object v5

    .line 72
    invoke-direct {p0, p1, p2, v5}, Lbha;->a(LbgM;Lbhc;LbgY;)LbgM;

    move-result-object v6

    .line 73
    invoke-virtual {v4, v6}, LbgP;->a(LbgM;)V

    .line 75
    invoke-virtual {v5}, LbgY;->c()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 76
    new-instance v7, LbgF;

    invoke-virtual {v5}, LbgY;->c()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {v5}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v0

    sget-object v8, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_BW:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    if-ne v0, v8, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v7, v1, v0}, LbgF;-><init>(Ljava/lang/String;Z)V

    .line 85
    sget-object v0, Lbha;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "PDF rotation: %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, LbgY;->a()I

    move-result v10

    neg-int v10, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v0, v1, v8}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    invoke-virtual {v5}, LbgY;->a()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v7, v0}, LbgF;->a(F)V

    .line 89
    invoke-virtual {v7}, LbgF;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v7}, LbgF;->b()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 90
    invoke-virtual {v5}, LbgY;->a()I

    move-result v1

    int-to-float v1, v1

    .line 91
    invoke-virtual {v6}, LbgM;->a()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v6}, LbgM;->b()F

    move-result v9

    float-to-int v9, v9

    .line 89
    invoke-static {v0, v1, v8, v9}, Lbhy;->a(FFII)Landroid/graphics/Rect;

    move-result-object v8

    .line 92
    sget-object v0, Lbha;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "PDF page size: %f %f"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v6}, LbgM;->a()F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v6}, LbgM;->b()F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v0, v1, v9}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    sget-object v0, Lbha;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "PDF best fit: %d %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v0, v1, v9}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v7, v0, v1}, LbgF;->b(FF)V

    .line 96
    invoke-virtual {v5}, LbgY;->a()I

    move-result v0

    rem-int/lit16 v0, v0, 0xb4

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_2

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    move v1, v0

    .line 97
    :goto_2
    invoke-virtual {v5}, LbgY;->a()I

    move-result v0

    rem-int/lit16 v0, v0, 0xb4

    const/16 v5, 0x5a

    if-ne v0, v5, :cond_3

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    .line 98
    :goto_3
    invoke-virtual {v6}, LbgM;->a()F

    move-result v5

    sub-float v1, v5, v1

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    .line 99
    invoke-virtual {v6}, LbgM;->b()F

    move-result v5

    sub-float v0, v5, v0

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v0, v5

    .line 100
    sget-object v5, Lbha;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "PDF page position: %f %f"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v8}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    invoke-virtual {v7, v1, v0}, LbgF;->a(FF)V

    .line 103
    invoke-virtual {v4, v7}, LbgP;->a(LbgF;)V

    .line 70
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 79
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Page "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not contain a rectified image!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    sget-object v1, Lbha;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "IO exception encountered!"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    :goto_4
    return-object v3

    .line 96
    :cond_2
    :try_start_1
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    move v1, v0

    goto :goto_2

    .line 97
    :cond_3
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    goto :goto_3

    .line 105
    :cond_4
    invoke-virtual {v4}, LbgP;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method
