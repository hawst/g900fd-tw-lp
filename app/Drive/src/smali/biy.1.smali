.class Lbiy;
.super Lbih;
.source "CharMatcher.java"


# instance fields
.field final o:Lbih;


# direct methods
.method constructor <init>(Lbih;)V
    .locals 2

    .prologue
    .line 665
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".negate()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lbiy;-><init>(Ljava/lang/String;Lbih;)V

    .line 666
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lbih;)V
    .locals 0

    .prologue
    .line 660
    invoke-direct {p0, p1}, Lbih;-><init>(Ljava/lang/String;)V

    .line 661
    iput-object p2, p0, Lbiy;->o:Lbih;

    .line 662
    return-void
.end method


# virtual methods
.method public a()Lbih;
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lbiy;->o:Lbih;

    return-object v0
.end method

.method a(Ljava/lang/String;)Lbih;
    .locals 2

    .prologue
    .line 704
    new-instance v0, Lbiy;

    iget-object v1, p0, Lbiy;->o:Lbih;

    invoke-direct {v0, p1, v1}, Lbiy;-><init>(Ljava/lang/String;Lbih;)V

    return-object v0
.end method

.method public a(C)Z
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lbiy;->o:Lbih;

    invoke-virtual {v0, p1}, Lbih;->a(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 656
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, Lbih;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method
