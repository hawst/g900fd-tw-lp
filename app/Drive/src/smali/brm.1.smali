.class public final Lbrm;
.super Ljava/lang/Object;
.source "Closer.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final b:Lbrp;


# instance fields
.field final a:Lbrp;

.field private a:Ljava/lang/Throwable;

.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/io/Closeable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lbro;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbro;->a:Lbro;

    :goto_0
    sput-object v0, Lbrm;->b:Lbrp;

    return-void

    :cond_0
    sget-object v0, Lbrn;->a:Lbrn;

    goto :goto_0
.end method

.method constructor <init>(Lbrp;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbrm;->a:Ljava/util/LinkedList;

    .line 112
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrp;

    iput-object v0, p0, Lbrm;->a:Lbrp;

    .line 113
    return-void
.end method

.method public static a()Lbrm;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lbrm;

    sget-object v1, Lbrm;->b:Lbrp;

    invoke-direct {v0, v1}, Lbrm;-><init>(Lbrp;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/Closeable;)Ljava/io/Closeable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/io/Closeable;",
            ">(TC;)TC;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lbrm;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 124
    return-object p1
.end method

.method public a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 141
    iput-object p1, p0, Lbrm;->a:Ljava/lang/Throwable;

    .line 142
    const-class v0, Ljava/io/IOException;

    invoke-static {p1, v0}, Lbjz;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 143
    invoke-static {p1}, Lbjz;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public close()V
    .locals 4

    .prologue
    .line 201
    iget-object v1, p0, Lbrm;->a:Ljava/lang/Throwable;

    .line 204
    :goto_0
    iget-object v0, p0, Lbrm;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    iget-object v0, p0, Lbrm;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 207
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_1
    move-object v1, v0

    .line 215
    goto :goto_0

    .line 208
    :catch_0
    move-exception v2

    .line 209
    if-nez v1, :cond_0

    move-object v0, v2

    .line 210
    goto :goto_1

    .line 212
    :cond_0
    iget-object v3, p0, Lbrm;->a:Lbrp;

    invoke-interface {v3, v0, v1, v2}, Lbrp;->a(Ljava/io/Closeable;Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1

    .line 217
    :cond_1
    iget-object v0, p0, Lbrm;->a:Ljava/lang/Throwable;

    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 218
    const-class v0, Ljava/io/IOException;

    invoke-static {v1, v0}, Lbjz;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 219
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 221
    :cond_2
    return-void
.end method
