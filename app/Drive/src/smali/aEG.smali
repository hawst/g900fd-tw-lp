.class public final enum LaEG;
.super Ljava/lang/Enum;
.source "DocListProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEG;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEG;

.field private static final synthetic a:[LaEG;

.field public static final enum b:LaEG;

.field public static final enum c:LaEG;

.field public static final enum d:LaEG;

.field public static final enum e:LaEG;

.field public static final enum f:LaEG;

.field public static final enum g:LaEG;

.field public static final enum h:LaEG;

.field public static final enum i:LaEG;

.field public static final enum j:LaEG;

.field public static final enum k:LaEG;

.field public static final enum l:LaEG;

.field public static final enum m:LaEG;

.field public static final enum n:LaEG;


# instance fields
.field private volatile a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 42
    new-instance v0, LaEG;

    const-string v1, "DOCUMENTS"

    const-string v2, "documents"

    invoke-direct {v0, v1, v5, v2}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->a:LaEG;

    .line 43
    new-instance v0, LaEG;

    const-string v1, "ACCOUNTS"

    const-string v2, "accounts"

    invoke-direct {v0, v1, v6, v2}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->b:LaEG;

    .line 44
    new-instance v0, LaEG;

    const-string v1, "DOCCONTENTS"

    const-string v2, "doc-contents"

    invoke-direct {v0, v1, v7, v2}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->c:LaEG;

    .line 45
    new-instance v0, LaEG;

    const-string v1, "APPCACHE"

    const-string v2, "appcache"

    invoke-direct {v0, v1, v8, v2}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->d:LaEG;

    .line 46
    new-instance v0, LaEG;

    const-string v1, "ACL"

    const-string v2, "acl"

    invoke-direct {v0, v1, v9, v2}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->e:LaEG;

    .line 47
    new-instance v0, LaEG;

    const-string v1, "CACHED_SEARCH"

    const/4 v2, 0x5

    const-string v3, "cachedSearch"

    invoke-direct {v0, v1, v2, v3}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->f:LaEG;

    .line 48
    new-instance v0, LaEG;

    const-string v1, "PARTIAL_FEED"

    const/4 v2, 0x6

    const-string v3, "partialFeed"

    invoke-direct {v0, v1, v2, v3}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->g:LaEG;

    .line 49
    new-instance v0, LaEG;

    const-string v1, "SYNC_STATUS"

    const/4 v2, 0x7

    const-string v3, "syncStatus"

    invoke-direct {v0, v1, v2, v3}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->h:LaEG;

    .line 50
    new-instance v0, LaEG;

    const-string v1, "MANIFEST"

    const/16 v2, 0x8

    const-string v3, "manifest"

    invoke-direct {v0, v1, v2, v3}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->i:LaEG;

    .line 51
    new-instance v0, LaEG;

    const-string v1, "APP_METADATA"

    const/16 v2, 0x9

    const-string v3, "appMetadata"

    invoke-direct {v0, v1, v2, v3}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaEG;->j:LaEG;

    .line 52
    new-instance v0, LaEG;

    const-string v1, "FILES"

    const/16 v2, 0xa

    const-string v3, ""

    const-string v4, "files"

    invoke-direct {v0, v1, v2, v3, v4}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaEG;->k:LaEG;

    .line 53
    new-instance v0, LaEG;

    const-string v1, "EXPOSED_CONTENT_URI"

    const/16 v2, 0xb

    const-string v3, "exposed_content"

    sget-object v4, LaEG;->k:LaEG;

    iget-object v4, v4, LaEG;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaEG;->l:LaEG;

    .line 54
    new-instance v0, LaEG;

    const-string v1, "STORAGE"

    const/16 v2, 0xc

    const-string v3, ""

    const-string v4, "storage"

    invoke-direct {v0, v1, v2, v3, v4}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaEG;->m:LaEG;

    .line 55
    new-instance v0, LaEG;

    const-string v1, "STORAGE_LEGACY"

    const/16 v2, 0xd

    const-string v3, ""

    const-string v4, "storage.legacy"

    invoke-direct {v0, v1, v2, v3, v4}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LaEG;->n:LaEG;

    .line 41
    const/16 v0, 0xe

    new-array v0, v0, [LaEG;

    sget-object v1, LaEG;->a:LaEG;

    aput-object v1, v0, v5

    sget-object v1, LaEG;->b:LaEG;

    aput-object v1, v0, v6

    sget-object v1, LaEG;->c:LaEG;

    aput-object v1, v0, v7

    sget-object v1, LaEG;->d:LaEG;

    aput-object v1, v0, v8

    sget-object v1, LaEG;->e:LaEG;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LaEG;->f:LaEG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaEG;->g:LaEG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaEG;->h:LaEG;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaEG;->i:LaEG;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaEG;->j:LaEG;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaEG;->k:LaEG;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaEG;->l:LaEG;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LaEG;->m:LaEG;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LaEG;->n:LaEG;

    aput-object v2, v0, v1

    sput-object v0, LaEG;->a:[LaEG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    const-string v0, ""

    invoke-direct {p0, p1, p2, p3, v0}, LaEG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 66
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaEG;->b:Ljava/lang/String;

    .line 67
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaEG;->a:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public static synthetic a(LaEG;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, LaEG;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LaEG;
    .locals 1

    .prologue
    .line 41
    const-class v0, LaEG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEG;

    return-object v0
.end method

.method public static values()[LaEG;
    .locals 1

    .prologue
    .line 41
    sget-object v0, LaEG;->a:[LaEG;

    invoke-virtual {v0}, [LaEG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEG;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, LaEG;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "ContentUri not initialized"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 72
    iget-object v0, p0, LaEG;->a:Landroid/net/Uri;

    return-object v0

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 77
    iget-object v1, p0, LaEG;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaEG;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "content"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LaEG;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LaEG;->a:Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
