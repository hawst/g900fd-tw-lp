.class public final LbsK;
.super Ljava/lang/Object;
.source "Futures.java"


# static fields
.field private static final a:Lbpw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpw",
            "<",
            "Ljava/lang/reflect/Constructor",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final a:LbsF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsF",
            "<",
            "LbsU",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 913
    new-instance v0, LbsM;

    invoke-direct {v0}, LbsM;-><init>()V

    sput-object v0, LbsK;->a:LbsF;

    .line 1363
    invoke-static {}, Lbpw;->d()Lbpw;

    move-result-object v0

    new-instance v1, LbsO;

    invoke-direct {v1}, LbsO;-><init>()V

    invoke-virtual {v0, v1}, Lbpw;->a(LbiG;)Lbpw;

    move-result-object v0

    invoke-virtual {v0}, Lbpw;->a()Lbpw;

    move-result-object v0

    sput-object v0, LbsK;->a:Lbpw;

    return-void
.end method

.method public static a(LbsU;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<+",
            "LbsU",
            "<+TV;>;>;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 907
    sget-object v0, LbsK;->a:LbsF;

    invoke-static {p0, v0}, LbsK;->a(LbsU;LbsF;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbsU;LbiG;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<TI;>;",
            "LbiG",
            "<-TI;+TO;>;)",
            "LbsU",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 656
    invoke-static {}, LbsY;->a()LbsW;

    move-result-object v0

    invoke-static {p0, p1, v0}, LbsK;->a(LbsU;LbiG;Ljava/util/concurrent/Executor;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbsU;LbiG;Ljava/util/concurrent/Executor;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<TI;>;",
            "LbiG",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LbsU",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 699
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    new-instance v0, LbsL;

    invoke-direct {v0, p1}, LbsL;-><init>(LbiG;)V

    .line 706
    invoke-static {p0, v0, p2}, LbsK;->a(LbsU;LbsF;Ljava/util/concurrent/Executor;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbsU;LbsF;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<TI;>;",
            "LbsF",
            "<-TI;+TO;>;)",
            "LbsU",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 550
    invoke-static {}, LbsY;->a()LbsW;

    move-result-object v0

    invoke-static {p0, p1, v0}, LbsK;->a(LbsU;LbsF;Ljava/util/concurrent/Executor;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbsU;LbsF;Ljava/util/concurrent/Executor;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<TI;>;",
            "LbsF",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LbsU",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 595
    new-instance v0, LbsP;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, LbsP;-><init>(LbsF;LbsU;LbsL;)V

    .line 596
    invoke-interface {p0, v0, p2}, LbsU;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 597
    return-object v0
.end method

.method public static a(Ljava/lang/Object;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(TV;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 229
    new-instance v0, LbsT;

    invoke-direct {v0, p0}, LbsT;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 255
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    new-instance v0, LbsR;

    invoke-direct {v0, p0}, LbsR;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static a(LbsU;LbsJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<TV;>;",
            "LbsJ",
            "<-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1054
    invoke-static {}, LbsY;->a()LbsW;

    move-result-object v0

    invoke-static {p0, p1, v0}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 1055
    return-void
.end method

.method public static a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<TV;>;",
            "LbsJ",
            "<-TV;>;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1096
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    new-instance v0, LbsN;

    invoke-direct {v0, p0, p1}, LbsN;-><init>(LbsU;LbsJ;)V

    .line 1118
    invoke-interface {p0, v0, p2}, LbsU;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1119
    return-void
.end method
