.class public LaEz;
.super LaFm;
.source "DocListDatabase.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field public static final a:LaFr;

.field static final a:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LaFv;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/String;

.field public static final b:LaFr;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:LQr;

.field private final a:LaEM;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LaIm;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            "LaGe;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile a:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x70

    .line 94
    invoke-static {v5}, LaFt;->a(I)LaFt;

    move-result-object v0

    const/16 v1, 0x62

    new-instance v2, LaFG;

    const-string v3, "lastModifiedAnywhereTime"

    sget-object v4, LaFI;->a:LaFI;

    invoke-direct {v2, v3, v4}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    invoke-virtual {v0, v1, v2}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, LaFt;->a()LaFr;

    move-result-object v0

    sput-object v0, LaEz;->a:LaFr;

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MAX("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaES;->e:LaES;

    .line 98
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", IFNULL("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaES;->m:LaES;

    .line 99
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", 0))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LaEz;->a:Ljava/lang/String;

    .line 105
    invoke-static {v5}, LaFt;->a(I)LaFt;

    move-result-object v0

    const/16 v1, 0x68

    new-instance v2, LaFG;

    const-string v3, "lastModifiedOrCreatedTime"

    sget-object v4, LaFI;->a:LaFI;

    invoke-direct {v2, v3, v4}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    invoke-virtual {v0, v1, v2}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, LaFt;->a()LaFr;

    move-result-object v0

    sput-object v0, LaEz;->b:LaFr;

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "COALESCE("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaES;->h:LaES;

    .line 109
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaES;->d:LaES;

    .line 110
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LaEz;->b:Ljava/lang/String;

    .line 113
    const/16 v0, 0x5f

    .line 115
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, LaEn;

    invoke-direct {v1}, LaEn;-><init>()V

    .line 114
    invoke-static {v0, v1}, Lbng;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Lbng;

    move-result-object v0

    sput-object v0, LaEz;->a:Lbng;

    .line 113
    return-void
.end method

.method public constructor <init>(Laja;Ljava/lang/String;LqK;LQr;LtK;Laja;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime LaEy;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljava/lang/String;",
            "LqK;",
            "LQr;",
            "LtK;",
            "Laja",
            "<",
            "LaIm;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 283
    new-instance v3, LaEB;

    .line 284
    invoke-virtual {p1}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v3, v0, p2, p3}, LaEB;-><init>(Landroid/content/Context;Ljava/lang/String;LqK;)V

    .line 283
    invoke-direct {p0, p4, p5, v3}, LaFm;-><init>(LQr;LtK;LaFu;)V

    .line 122
    new-instance v0, LaEM;

    invoke-direct {v0}, LaEM;-><init>()V

    iput-object v0, p0, LaEz;->a:LaEM;

    .line 125
    new-instance v0, LaEA;

    invoke-direct {v0, p0}, LaEA;-><init>(LaEz;)V

    iput-object v0, p0, LaEz;->a:Ljava/lang/ThreadLocal;

    .line 285
    invoke-virtual {p1}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 286
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 287
    const-string v0, "DocListDatabase"

    const-string v3, "openHelper points on new database: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 288
    iput-object p4, p0, LaEz;->a:LQr;

    .line 289
    iput-object p6, p0, LaEz;->a:Lbxw;

    .line 290
    return-void

    :cond_0
    move v0, v2

    .line 286
    goto :goto_0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, LaEz;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, LaEz;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 9

    .prologue
    .line 305
    iget-object v0, p0, LaEz;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 306
    if-nez v0, :cond_1

    .line 307
    monitor-enter p0

    .line 308
    :try_start_0
    iget-object v0, p0, LaEz;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 309
    if-nez v0, :cond_0

    .line 310
    sget-object v0, LaES;->F:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 313
    invoke-static {}, LaER;->a()LaER;

    move-result-object v1

    invoke-virtual {v1}, LaER;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " DESC"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v0, p0

    .line 312
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 318
    const-wide/16 v0, 0x0

    .line 320
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 321
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    move-wide v2, v0

    .line 324
    :goto_0
    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 326
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 327
    iput-object v0, p0, LaEz;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 329
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331
    :cond_1
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    return-wide v0

    .line 324
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    .line 329
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-wide v2, v0

    goto :goto_0
.end method

.method a()LaEM;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, LaEz;->a:LaEM;

    return-object v0
.end method

.method public a()LaIm;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, LaEz;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIm;

    return-object v0
.end method

.method public a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, LaEz;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    .line 297
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 298
    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    return-object v0

    .line 297
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 8

    .prologue
    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 365
    iget-object v0, p0, LaEz;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    .line 366
    invoke-static {}, LaED;->values()[LaED;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 367
    invoke-virtual {v5}, LaED;->a()LaEF;

    move-result-object v6

    invoke-virtual {v6}, LaEF;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 368
    invoke-virtual {v5}, LaED;->a()LaEF;

    move-result-object v6

    invoke-virtual {v6}, LaEF;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    const/16 v6, 0xa

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 370
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "select * from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, LaED;->a()LaEF;

    move-result-object v5

    invoke-virtual {v5}, LaEF;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 371
    invoke-static {v5, v2}, Landroid/database/DatabaseUtils;->dumpCursor(Landroid/database/Cursor;Ljava/lang/StringBuilder;)V

    .line 366
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 375
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGe;)V
    .locals 2

    .prologue
    .line 355
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    iget-object v0, p0, LaEz;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 341
    iget-object v0, p0, LaEz;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 342
    iget-object v1, p0, LaEz;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    .line 343
    if-eqz p1, :cond_1

    .line 344
    if-eqz v0, :cond_1

    .line 345
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 346
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGe;

    .line 347
    invoke-virtual {v0}, LaGe;->a()LaGd;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 349
    :cond_0
    iget-object v0, p0, LaEz;->a:LaEM;

    invoke-virtual {v0, v1}, LaEM;->a(Ljava/util/List;)V

    .line 352
    :cond_1
    return-void
.end method
