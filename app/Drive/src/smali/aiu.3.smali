.class final Laiu;
.super Ljava/lang/Object;
.source "SyncAlgorithms.java"

# interfaces
.implements Laiq;


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<+",
            "Laiq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Laiq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Laiu;->a:Ljava/util/Collection;

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Collection;Lait;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Laiu;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public a(Lagx;Landroid/content/SyncResult;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Laiu;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiq;

    .line 31
    invoke-interface {v0, p1, p2}, Laiq;->a(Lagx;Landroid/content/SyncResult;)V

    goto :goto_0

    .line 33
    :cond_0
    return-void
.end method

.method public a(Landroid/content/SyncResult;Z)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Laiu;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiq;

    .line 38
    invoke-interface {v0, p1, p2}, Laiq;->a(Landroid/content/SyncResult;Z)V

    goto :goto_0

    .line 40
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    const-string v0, "CompositeSyncAlgorithm[delegates=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Laiu;->a:Ljava/util/Collection;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
