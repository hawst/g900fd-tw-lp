.class public LatP;
.super Ljava/lang/Object;
.source "WidgetAccountChangeListener.java"

# interfaces
.implements LZU;


# instance fields
.field a:LatR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "LaFO;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LaFO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    const-string v0, "WidgetAccountChangeListener"

    const-string v1, "Accounts changed"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    new-instance v1, LatQ;

    invoke-direct {v1, p1}, LatQ;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 31
    new-instance v0, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;

    invoke-direct {v0, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    invoke-virtual {v2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    .line 33
    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 34
    invoke-virtual {v1, v5}, LatQ;->a(I)LaFO;

    move-result-object v6

    .line 35
    iget-object v7, p0, LatP;->a:LatR;

    invoke-virtual {v7, v2, v5, p1, v6}, LatR;->a(Landroid/appwidget/AppWidgetManager;ILandroid/content/Context;LaFO;)V

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_0
    return-void
.end method
