.class abstract enum LHX;
.super Ljava/lang/Enum;
.source "DateGrouper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LHX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LHX;

.field private static final synthetic a:[LHX;

.field public static final enum b:LHX;

.field public static final enum c:LHX;

.field public static final enum d:LHX;

.field public static final enum e:LHX;

.field public static final enum f:LHX;


# instance fields
.field private a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 41
    new-instance v0, LHY;

    const-string v1, "CREATION_TIME"

    sget-object v2, LaES;->d:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, LHY;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LHX;->a:LHX;

    .line 47
    new-instance v0, LHZ;

    const-string v1, "LAST_MODIFIED"

    sget-object v2, LaEz;->a:LaFr;

    invoke-direct {v0, v1, v5, v2}, LHZ;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LHX;->b:LHX;

    .line 53
    new-instance v0, LIa;

    const-string v1, "LAST_OPENED"

    sget-object v2, LaES;->h:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LIa;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LHX;->c:LHX;

    .line 59
    new-instance v0, LIb;

    const-string v1, "LAST_OPENED_BY_ME_OR_CREATED"

    sget-object v2, LaEz;->b:LaFr;

    invoke-direct {v0, v1, v7, v2}, LIb;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LHX;->d:LHX;

    .line 65
    new-instance v0, LIc;

    const-string v1, "MODIFIED_BY_ME"

    sget-object v2, LaES;->m:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LIc;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LHX;->e:LHX;

    .line 71
    new-instance v0, LId;

    const-string v1, "SHARED_WITH_ME"

    const/4 v2, 0x5

    sget-object v3, LaES;->i:LaES;

    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LId;-><init>(Ljava/lang/String;ILaFr;)V

    sput-object v0, LHX;->f:LHX;

    .line 40
    const/4 v0, 0x6

    new-array v0, v0, [LHX;

    sget-object v1, LHX;->a:LHX;

    aput-object v1, v0, v4

    sget-object v1, LHX;->b:LHX;

    aput-object v1, v0, v5

    sget-object v1, LHX;->c:LHX;

    aput-object v1, v0, v6

    sget-object v1, LHX;->d:LHX;

    aput-object v1, v0, v7

    sget-object v1, LHX;->e:LHX;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LHX;->f:LHX;

    aput-object v2, v0, v1

    sput-object v0, LHX;->a:[LHX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 81
    iput-object p3, p0, LHX;->a:LaFr;

    .line 82
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILaFr;LHW;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, LHX;-><init>(Ljava/lang/String;ILaFr;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LHX;
    .locals 1

    .prologue
    .line 40
    const-class v0, LHX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LHX;

    return-object v0
.end method

.method public static values()[LHX;
    .locals 1

    .prologue
    .line 40
    sget-object v0, LHX;->a:[LHX;

    invoke-virtual {v0}, [LHX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LHX;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LHX;->a:LaFr;

    return-object v0
.end method

.method public abstract a(LCv;)Ljava/lang/Long;
.end method
