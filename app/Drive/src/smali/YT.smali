.class public LYT;
.super Ljava/lang/Object;
.source "PunchWebViewActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, LYT;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 184
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 185
    sget v1, LpN;->menu_full_screen:I

    if-ne v0, v1, :cond_0

    .line 186
    iget-object v0, p0, LYT;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    .line 197
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 187
    :cond_0
    sget v1, LpN;->menu_help:I

    if-ne v0, v1, :cond_1

    .line 188
    iget-object v0, p0, LYT;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    goto :goto_0

    .line 189
    :cond_1
    sget v1, LpN;->open_detail_panel:I

    if-ne v0, v1, :cond_2

    .line 190
    iget-object v0, p0, LYT;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    goto :goto_0

    .line 191
    :cond_2
    sget v1, LpN;->menu_edit_icon:I

    if-ne v0, v1, :cond_3

    .line 192
    iget-object v0, p0, LYT;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    goto :goto_0

    .line 194
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
