.class public final Lyu;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lyr;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lyq;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lamm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lyw;

    sput-object v0, Lyu;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, Lyu;->a:LbrA;

    .line 42
    const-class v0, Lyr;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lyu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyu;->a:Lbsk;

    .line 45
    const-class v0, Lyq;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, Lyu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyu;->b:Lbsk;

    .line 48
    const-class v0, Lamm;

    sget-object v1, Lyu;->a:Ljava/lang/Class;

    .line 49
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    const-class v1, Lbxz;

    .line 48
    invoke-static {v0, v1}, Lyu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lyu;->c:Lbsk;

    .line 51
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 75
    packed-switch p1, :pswitch_data_0

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :pswitch_0
    new-instance v4, Lyr;

    iget-object v0, p0, Lyu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->i:Lbsk;

    .line 80
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lyu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->i:Lbsk;

    .line 78
    invoke-static {v0, v1}, Lyu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    iget-object v1, p0, Lyu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 86
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lyu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 84
    invoke-static {v1, v2}, Lyu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    iget-object v2, p0, Lyu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 92
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lyu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 90
    invoke-static {v2, v3}, Lyu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, Lyu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lyu;

    iget-object v3, v3, Lyu;->c:Lbsk;

    .line 98
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, Lyu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lyu;

    iget-object v5, v5, Lyu;->c:Lbsk;

    .line 96
    invoke-static {v3, v5}, Lyu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lamm;

    invoke-direct {v4, v0, v1, v2, v3}, Lyr;-><init>(LTO;LtK;LQr;Lamm;)V

    .line 103
    return-object v4

    .line 75
    :pswitch_data_0
    .packed-switch 0x460
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 120
    sparse-switch p2, :sswitch_data_0

    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :sswitch_0
    check-cast p1, Lyp;

    .line 124
    iget-object v0, p0, Lyu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyu;

    iget-object v0, v0, Lyu;->a:Lbsk;

    .line 127
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyr;

    .line 124
    invoke-virtual {p1, v0}, Lyp;->provideCsiSyncer(Lyr;)Lyq;

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    .line 131
    :sswitch_1
    check-cast p1, Lyp;

    .line 133
    iget-object v0, p0, Lyu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 136
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iget-object v1, p0, Lyu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->b:Lbsk;

    .line 140
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    .line 133
    invoke-virtual {p1, v0, v1}, Lyp;->provideCsiRequestQueue(LQr;Landroid/app/Application;)Lamm;

    move-result-object v0

    goto :goto_0

    .line 120
    :sswitch_data_0
    .sparse-switch
        0x18f -> :sswitch_1
        0x236 -> :sswitch_0
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 58
    const-class v0, Lyr;

    iget-object v1, p0, Lyu;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyu;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, Lyq;

    iget-object v1, p0, Lyu;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyu;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    const-class v0, Lamm;

    sget-object v1, Lyu;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Lyu;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lyu;->a(Lbuv;Lbsk;)V

    .line 61
    iget-object v0, p0, Lyu;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x460

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 63
    iget-object v0, p0, Lyu;->b:Lbsk;

    const-class v1, Lyp;

    const/16 v2, 0x236

    invoke-virtual {p0, v1, v2}, Lyu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 65
    iget-object v0, p0, Lyu;->c:Lbsk;

    const-class v1, Lyp;

    const/16 v2, 0x18f

    invoke-virtual {p0, v1, v2}, Lyu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 112
    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
