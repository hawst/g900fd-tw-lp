.class LrZ;
.super Landroid/os/AsyncTask;
.source "CreateNewDocActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LPx;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic a:LrX;


# direct methods
.method constructor <init>(LrX;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, LrZ;->a:LrX;

    iput-object p2, p0, LrZ;->a:Ljava/lang/String;

    iput-object p3, p0, LrZ;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 188
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 189
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 190
    return-object v0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, LrZ;->a:LrX;

    invoke-virtual {v0, p1}, LrX;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LrZ;->a:LrX;

    iget-object v0, v0, LrX;->a:LaKR;

    .line 195
    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LrZ;->a:LrX;

    .line 196
    invoke-virtual {v0}, LrX;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, LrZ;->a:LrX;

    invoke-virtual {v0}, LrX;->f()V

    .line 203
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, LrZ;->a:LrX;

    iget-object v1, p0, LrZ;->a:LrX;

    invoke-static {v1}, LrX;->a(LrX;)LNl;

    move-result-object v1

    invoke-virtual {v1}, LNl;->d()I

    move-result v1

    invoke-virtual {v0, v1}, LrX;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 200
    iget-object v1, p0, LrZ;->a:LrX;

    iget-object v1, v1, LrX;->a:Lald;

    invoke-interface {v1, v0, p1}, Lald;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 201
    iget-object v0, p0, LrZ;->a:LrX;

    invoke-virtual {v0}, LrX;->finish()V

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, LrZ;->a:LrX;

    invoke-virtual {v0}, LrX;->f()Z

    move-result v1

    .line 174
    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, LrZ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 178
    :goto_0
    if-eqz v1, :cond_1

    .line 179
    iget-object v1, p0, LrZ;->a:LrX;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, LrX;->setResult(ILandroid/content/Intent;)V

    .line 184
    :goto_1
    iget-object v0, p0, LrZ;->a:LrX;

    invoke-virtual {v0}, LrX;->finish()V

    .line 185
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, LrZ;->a:LrX;

    const/4 v2, 0x1

    .line 175
    invoke-static {v0, p1, v2}, Lala;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_1
    iget-object v1, p0, LrZ;->a:LrX;

    invoke-virtual {v1, v0}, LrX;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 5

    .prologue
    .line 144
    :try_start_0
    iget-object v0, p0, LrZ;->a:LrX;

    iget-object v0, v0, LrX;->a:LPw;

    iget-object v1, p0, LrZ;->a:LrX;

    iget-object v1, v1, LrX;->a:LaFO;

    iget-object v2, p0, LrZ;->a:Ljava/lang/String;

    iget-object v3, p0, LrZ;->a:LrX;

    iget-object v3, v3, LrX;->a:LaGv;

    iget-object v4, p0, LrZ;->a:LrX;

    .line 145
    invoke-virtual {v4}, LrX;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v4

    .line 144
    invoke-virtual {v0, v1, v2, v3, v4}, LPw;->a(LaFO;Ljava/lang/String;LaGv;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    :try_end_0
    .catch LPx; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    const-string v1, "CreateNewDocActivityBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to create new entry: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LrZ;->a:LrX;

    iget-object v3, v3, LrX;->a:LaGv;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 148
    iput-object v0, p0, LrZ;->a:LPx;

    .line 149
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, LrZ;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    .line 156
    if-eqz v0, :cond_0

    iget-object v1, p0, LrZ;->a:LrX;

    invoke-virtual {v1}, LrX;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 159
    :cond_0
    iget-object v0, p0, LrZ;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 161
    invoke-virtual {p0}, LrZ;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, LrZ;->a:LPx;

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, LrZ;->a:LPx;

    invoke-direct {p0, v0}, LrZ;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 168
    :cond_2
    invoke-direct {p0, p1}, LrZ;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LrZ;->a([Ljava/lang/Void;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 138
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, p1}, LrZ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method
