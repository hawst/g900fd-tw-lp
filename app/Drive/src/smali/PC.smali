.class public LPC;
.super Ljava/lang/Object;
.source "ActivityApiRequester.java"


# instance fields
.field private final a:LQr;

.field private final a:LTd;

.field private final a:Lcom/google/api/services/appsactivity/Appsactivity;


# direct methods
.method public constructor <init>(LQr;LTd;)V
    .locals 4

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LPC;->a:LQr;

    .line 37
    iput-object p2, p0, LPC;->a:LTd;

    .line 39
    invoke-static {}, Lbdi;->a()Lbeq;

    move-result-object v0

    .line 40
    new-instance v1, LbeY;

    invoke-direct {v1}, LbeY;-><init>()V

    .line 41
    new-instance v2, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;-><init>(Lbeq;LbeO;Lbek;)V

    invoke-virtual {v2}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->build()Lcom/google/api/services/appsactivity/Appsactivity;

    move-result-object v0

    iput-object v0, p0, LPC;->a:Lcom/google/api/services/appsactivity/Appsactivity;

    .line 42
    return-void
.end method


# virtual methods
.method public a(LaGu;Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;
    .locals 5

    .prologue
    .line 46
    invoke-interface {p1}, LaGu;->i()Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-interface {p1}, LaGu;->a()LaFM;

    move-result-object v1

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    .line 48
    iget-object v2, p0, LPC;->a:LQr;

    const-string v3, "recentActivityResultsPerPage"

    const/4 v4, 0x4

    invoke-interface {v2, v3, v4}, LQr;->a(Ljava/lang/String;I)I

    move-result v2

    .line 51
    iget-object v3, p0, LPC;->a:LTd;

    sget-object v4, LTi;->a:Ljava/lang/String;

    invoke-interface {v3, v1, v4}, LTd;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 53
    iget-object v4, p0, LPC;->a:Lcom/google/api/services/appsactivity/Appsactivity;

    invoke-virtual {v4}, Lcom/google/api/services/appsactivity/Appsactivity;->activities()Lcom/google/api/services/appsactivity/Appsactivity$Activities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/appsactivity/Appsactivity$Activities;->list()Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v4

    .line 54
    invoke-virtual {v4, v3}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setOauthToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setPageSize(Ljava/lang/Integer;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v2

    const-string v3, "drive.google.com"

    invoke-virtual {v2, v3}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setSource(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v2

    .line 55
    invoke-virtual {v2, p2}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    .line 57
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v2

    sget-object v3, LaGv;->a:LaGv;

    if-ne v2, v3, :cond_0

    .line 58
    invoke-virtual {v4, v0}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setDriveAncestorId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    .line 65
    :goto_0
    :try_start_0
    invoke-virtual {v4}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->execute()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;
    :try_end_0
    .catch Lbdz; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_1
    return-object v0

    .line 60
    :cond_0
    invoke-virtual {v4, v0}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setDriveFileId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 68
    iget-object v0, p0, LPC;->a:LTd;

    sget-object v2, LTi;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LTd;->b(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-virtual {v4, v0}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setOauthToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    .line 70
    invoke-virtual {v4}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->execute()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    goto :goto_1
.end method
