.class public LuX;
.super Ljava/lang/Object;
.source "ProjectorSharingMenuManager.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LKU;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Runnable;

.field private final a:Lvd;


# direct methods
.method public constructor <init>(LKU;)V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, LuY;

    invoke-direct {v0, p0}, LuY;-><init>(LuX;)V

    iput-object v0, p0, LuX;->a:Ljava/lang/Runnable;

    .line 91
    new-instance v0, Lvd;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lvd;-><init>(LuX;LuY;)V

    iput-object v0, p0, LuX;->a:Lvd;

    .line 96
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, LuX;->a:LbmF;

    .line 101
    iput-object p1, p0, LuX;->a:LKU;

    .line 102
    return-void
.end method

.method static synthetic a(LuX;)LKU;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LuX;->a:LKU;

    return-object v0
.end method

.method static synthetic a(LuX;)LbmF;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LuX;->a:LbmF;

    return-object v0
.end method

.method static synthetic a(LuX;)Lcom/google/android/apps/docs/doclist/selection/ItemKey;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LuX;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    return-object v0
.end method

.method static synthetic a(LuX;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, LuX;->a:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public a(LaGu;)V
    .locals 4

    .prologue
    .line 121
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-static {p1}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, LuX;->a:LbmF;

    .line 123
    new-instance v0, Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {p1}, LaGu;->j()Z

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    iput-object v0, p0, LuX;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 124
    return-void
.end method

.method public a(Landroid/view/MenuItem;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v2

    .line 106
    invoke-interface {v2}, Landroid/view/SubMenu;->clear()V

    .line 107
    iget-object v0, p0, LuX;->a:LbmF;

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_0
    return-void

    .line 111
    :cond_1
    invoke-static {}, LuZ;->values()[LuZ;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 112
    iget-object v6, p0, LuX;->a:LKU;

    invoke-virtual {v5, v6}, LuZ;->a(LKU;)LLw;

    move-result-object v6

    iget-object v7, p0, LuX;->a:LbmF;

    const/4 v8, 0x0

    .line 113
    invoke-interface {v6, v7, v8}, LLw;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 114
    iget v6, v5, LuZ;->a:I

    iget v7, v5, LuZ;->a:I

    iget v8, v5, LuZ;->b:I

    invoke-interface {v2, v1, v6, v7, v8}, Landroid/view/SubMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    iget v5, v5, LuZ;->c:I

    .line 115
    invoke-interface {v6, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-object v6, p0, LuX;->a:Lvd;

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 111
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
