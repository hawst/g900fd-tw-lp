.class public final Laal;
.super Ljava/lang/Object;
.source "OnlineSearch.java"

# interfaces
.implements Laaf;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final a:LaFO;

.field private final a:LpD;

.field private final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "https://docs.google.com/feeds/default/private/full"

    .line 33
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Laal;->a:Landroid/net/Uri;

    .line 32
    return-void
.end method

.method constructor <init>(LaFO;Landroid/net/Uri;Laag;Lagx;)V
    .locals 7

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const-string v0, "null accountId"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, Laal;->a:LaFO;

    .line 114
    const-string v0, "null feed uri"

    invoke-static {p2, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Laal;->b:Landroid/net/Uri;

    .line 115
    const-string v0, "null searchListener"

    invoke-static {p3, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const-string v0, "null driver"

    invoke-static {p4, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    new-instance v0, Laam;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Background online search: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Laam;-><init>(Laal;Ljava/lang/String;Laag;Lagx;Landroid/net/Uri;LaFO;)V

    iput-object v0, p0, Laal;->a:LpD;

    .line 157
    iget-object v0, p0, Laal;->a:LpD;

    invoke-virtual {v0}, LpD;->start()V

    .line 158
    return-void
.end method

.method static synthetic a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Laal;->b()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laal;)LpD;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Laal;->a:LpD;

    return-object v0
.end method

.method private a(Laag;)V
    .locals 2

    .prologue
    .line 161
    const-string v0, "OnlineSearch"

    const-string v1, "Search canceled"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    invoke-interface {p1}, Laag;->b()V

    .line 163
    return-void
.end method

.method static synthetic a(Laal;Laag;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Laal;->a(Laag;)V

    return-void
.end method

.method static synthetic a(Landroid/net/Uri$Builder;)V
    .locals 0

    .prologue
    .line 28
    invoke-static {p0}, Laal;->b(Landroid/net/Uri$Builder;)V

    return-void
.end method

.method private static b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Laal;->a:Landroid/net/Uri;

    return-object v0
.end method

.method private static b(Landroid/net/Uri$Builder;)V
    .locals 2

    .prologue
    .line 95
    const-string v0, "showroot"

    const-string v1, "true"

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 96
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Laal;->a:LpD;

    invoke-virtual {v0}, LpD;->a()V

    .line 175
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 187
    const-string v1, "%sOnlineSearch[account=%s, query=%s]: %s"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p0, Laal;->a:LpD;

    .line 188
    invoke-virtual {v0}, LpD;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Canceled "

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Laal;->a:LaFO;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-object v3, p0, Laal;->b:Landroid/net/Uri;

    aput-object v3, v2, v0

    .line 187
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 188
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
