.class final enum LyH;
.super Ljava/lang/Enum;
.source "ActivityCard.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LyH;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LyH;

.field private static final synthetic a:[LyH;

.field public static final enum b:LyH;

.field public static final enum c:LyH;

.field public static final enum d:LyH;

.field public static final enum e:LyH;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, LyH;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v2}, LyH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LyH;->a:LyH;

    new-instance v0, LyH;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v3}, LyH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LyH;->b:LyH;

    new-instance v0, LyH;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LyH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LyH;->c:LyH;

    new-instance v0, LyH;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v5}, LyH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LyH;->d:LyH;

    new-instance v0, LyH;

    const-string v1, "FINALPAGE"

    invoke-direct {v0, v1, v6}, LyH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LyH;->e:LyH;

    .line 63
    const/4 v0, 0x5

    new-array v0, v0, [LyH;

    sget-object v1, LyH;->a:LyH;

    aput-object v1, v0, v2

    sget-object v1, LyH;->b:LyH;

    aput-object v1, v0, v3

    sget-object v1, LyH;->c:LyH;

    aput-object v1, v0, v4

    sget-object v1, LyH;->d:LyH;

    aput-object v1, v0, v5

    sget-object v1, LyH;->e:LyH;

    aput-object v1, v0, v6

    sput-object v0, LyH;->a:[LyH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LyH;
    .locals 1

    .prologue
    .line 63
    const-class v0, LyH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LyH;

    return-object v0
.end method

.method public static values()[LyH;
    .locals 1

    .prologue
    .line 63
    sget-object v0, LyH;->a:[LyH;

    invoke-virtual {v0}, [LyH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LyH;

    return-object v0
.end method
