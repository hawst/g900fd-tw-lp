.class public abstract LanC;
.super Ljava/lang/Object;
.source "AbstractTransformingFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoo",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Laoo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoo",
            "<TK;+TE;>;"
        }
    .end annotation
.end field

.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<-TK;>;"
        }
    .end annotation
.end field

.field private final a:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Laop;Laoo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<-TK;>;",
            "Laoo",
            "<TK;+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, LanD;

    invoke-direct {v0, p0}, LanD;-><init>(LanC;)V

    iput-object v0, p0, LanC;->a:Laoy;

    .line 32
    new-instance v0, LanE;

    invoke-direct {v0, p0}, LanE;-><init>(LanC;)V

    iput-object v0, p0, LanC;->b:Laoy;

    .line 52
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoo;

    iput-object v0, p0, LanC;->a:Laoo;

    .line 53
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laop;

    iput-object v0, p0, LanC;->a:Laop;

    .line 54
    return-void
.end method

.method private final a(Ljava/lang/Object;LbsU;Laou;Laou;)LbsU;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LbsU",
            "<+TE;>;",
            "Laou",
            "<TE;>;",
            "Laou",
            "<TV;>;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0, p1}, LanC;->a(Ljava/lang/Object;)Laop;

    move-result-object v2

    .line 87
    new-instance v0, LanG;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LanG;-><init>(LanC;Laop;Ljava/lang/Object;Laou;Laou;)V

    .line 105
    invoke-static {p2, v0}, LbsK;->a(LbsU;LbsF;)LbsU;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/Object;)Laop;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Laop",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, LanC;->a:Laop;

    return-object v0
.end method

.method public a(Ljava/lang/Object;)LbsU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, LanC;->a:Laoy;

    .line 59
    invoke-static {v0}, Laou;->a(Laoy;)Laou;

    move-result-object v0

    .line 60
    iget-object v1, p0, LanC;->b:Laoy;

    .line 61
    invoke-static {v1}, Laou;->a(Laoy;)Laou;

    move-result-object v1

    .line 63
    iget-object v2, p0, LanC;->a:Laoo;

    invoke-interface {v2, p1}, Laoo;->a(Ljava/lang/Object;)LbsU;

    move-result-object v2

    .line 64
    invoke-virtual {v0, v2}, Laou;->a(LbsU;)V

    .line 67
    invoke-direct {p0, p1, v2, v0, v1}, LanC;->a(Ljava/lang/Object;LbsU;Laou;Laou;)LbsU;

    move-result-object v2

    .line 69
    new-instance v3, LanF;

    invoke-direct {v3, p0, v1}, LanF;-><init>(LanC;Laou;)V

    invoke-static {v2, v3}, Lalv;->a(LbsU;LalB;)LbsU;

    move-result-object v2

    .line 78
    invoke-virtual {v0, v2}, Laou;->b(LbsU;)V

    .line 79
    invoke-virtual {v1, v2}, Laou;->b(LbsU;)V

    .line 80
    return-object v2
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TE;)TV;"
        }
    .end annotation
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 116
    return-void
.end method

.method protected b(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 119
    return-void
.end method
