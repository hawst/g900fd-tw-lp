.class public Lwb;
.super Ljava/lang/Object;
.source "EntryListSourceSetterImpl.java"

# interfaces
.implements Lwa;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

.field private final a:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LvZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lwb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lwb;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 33
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lwb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvZ;

    .line 106
    invoke-interface {v0, p1}, LvZ;->a(Z)V

    goto :goto_0

    .line 108
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lwb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvZ;

    .line 97
    invoke-interface {v0}, LvZ;->a()V

    goto :goto_0

    .line 99
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 1

    .prologue
    .line 52
    invoke-static {}, LamV;->a()V

    .line 54
    iget-object v0, p0, Lwb;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lwb;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;
    .locals 1

    .prologue
    .line 62
    invoke-static {}, LamV;->a()V

    .line 63
    iget-object v0, p0, Lwb;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lwb;->c()V

    .line 85
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, LamV;->a()V

    .line 75
    iget-object v0, p0, Lwb;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-static {v0, p1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    iput-object p1, p0, Lwb;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lwb;->a(Z)V

    goto :goto_0
.end method

.method public a(LvZ;)V
    .locals 1

    .prologue
    .line 44
    invoke-static {}, LamV;->a()V

    .line 46
    iget-object v0, p0, Lwb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lwb;->a(Z)V

    .line 90
    return-void
.end method

.method public b(LvZ;)V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, LamV;->a()V

    .line 39
    iget-object v0, p0, Lwb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method
