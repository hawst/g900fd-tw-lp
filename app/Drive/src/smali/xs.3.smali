.class public Lxs;
.super Ljava/lang/Object;
.source "Interpolators.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "InlinedApi"
    }
.end annotation


# static fields
.field private static final a:[F

.field private static final b:[F

.field private static final c:[F

.field private static final d:[F

.field private static final e:[F

.field private static final f:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x18

    .line 15
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lxs;->a:[F

    .line 20
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lxs;->b:[F

    .line 25
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lxs;->c:[F

    .line 30
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lxs;->d:[F

    .line 35
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    sput-object v0, Lxs;->e:[F

    .line 40
    new-array v0, v1, [F

    fill-array-data v0, :array_5

    sput-object v0, Lxs;->f:[F

    return-void

    .line 15
    nop

    :array_0
    .array-data 4
        0x0
        0x3a1ccccd
        0x3b200000
        0x3bb7999a
        0x3c266667
        0x3c848000
        0x3cc26667
        0x3d333334    # 0.043750003f
        0x3d910000
        0x3dd80000    # 0.10546875f
        0x3e17e666
        0x3e4ccccd    # 0.2f
        0x3e85a666
        0x3eaa0000    # 0.33203125f
        0x3ed3c000
        0x3f01999a
        0x3f0e9400
        0x3f1c5333
        0x3f2adc00
        0x3f3a3333
        0x3f4a5d9a
        0x3f5b6000
        0x3f6d3f33
        0x3f800000    # 1.0f
    .end array-data

    .line 20
    :array_1
    .array-data 4
        0x0
        0x3b3c0000
        0x3c380000
        0x3cca8000
        0x3d300000    # 0.04296875f
        0x3d866000
        0x3dbd0000
        0x3e200000    # 0.15625f
        0x3e6d8000
        0x3ea20000    # 0.31640625f
        0x3ed04000
        0x3f000000    # 0.5f
        0x3f17e000
        0x3f2f0000
        0x3f44a000
        0x3f580000    # 0.84375f
        0x3f609c00
        0x3f686000
        0x3f6f3400
        0x3f750000
        0x3f79ac00
        0x3f7d2000
        0x3f7f4400
        0x3f800000    # 1.0f
    .end array-data

    .line 25
    :array_2
    .array-data 4
        0x0
        0x3d1be667
        0x3d9e0000
        0x3defd99a
        0x3e21999a
        0x3e4be000
        0x3e76999a
        0x3ea66666    # 0.325f
        0x3ed18000
        0x3efc0000    # 0.4921875f
        0x3f12a666
        0x3f266666    # 0.65f
        0x3f38f333
        0x3f4a0000    # 0.7890625f
        0x3f594000
        0x3f666666    # 0.9f
        0x3f6c1800
        0x3f712666
        0x3f758800
        0x3f793333
        0x3f7c1e66
        0x3f7e4000
        0x3f7f8e66
        0x3f800000    # 1.0f
    .end array-data

    .line 30
    :array_3
    .array-data 4
        0x0
        0x3b3c0000
        0x3c380000
        0x3cca8000
        0x3d300000    # 0.04296875f
        0x3d866000
        0x3dbd0000
        0x3e200000    # 0.15625f
        0x3e6d8000
        0x3ea20000    # 0.31640625f
        0x3ed04000
        0x3f000000    # 0.5f
        0x3f17e000
        0x3f2f0000
        0x3f44a000
        0x3f580000    # 0.84375f
        0x3f609c00
        0x3f686000
        0x3f6f3400
        0x3f750000
        0x3f79ac00
        0x3f7d2000
        0x3f7f4400
        0x3f800000    # 1.0f
    .end array-data

    .line 35
    :array_4
    .array-data 4
        0x0
        0x3d12999a
        0x3d8c0000    # 0.068359375f
        0x3dc8b334
        0x3e000000    # 0.125f
        0x3e194000
        0x3e306667
        0x3e59999a    # 0.2125f
        0x3e7e0000
        0x3e900000    # 0.28125f
        0x3eb33334    # 0.35000002f
        0x3ec7cccd
        0x3ee00000    # 0.4375f
        0x3efd0000
        0x3f06d666
        0x3f100000    # 0.5625f
        0x3f1a1000
        0x3f25199a
        0x3f313000
        0x3f3e6666
        0x3f4cd000    # 0.8000488f
        0x3f5c8000
        0x3f6d899a
        0x3f800000    # 1.0f
    .end array-data

    .line 40
    :array_5
    .array-data 4
        0x0
        0x3b3c0000
        0x3c380000
        0x3cca8000
        0x3d300000    # 0.04296875f
        0x3d866000
        0x3dbd0000
        0x3e200000    # 0.15625f
        0x3e6d8000
        0x3ea20000    # 0.31640625f
        0x3f000000    # 0.5f
        0x3f17e000
        0x3f2f0000
        0x3f44a000
        0x3f4ea400
        0x3f580000    # 0.84375f
        0x3f609c00
        0x3f686000
        0x3f6f3400
        0x3f750000
        0x3f79ac00
        0x3f7d2000
        0x3f7f4400
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 4

    .prologue
    .line 50
    invoke-static {}, Lxs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "linear_out_slow_in"

    invoke-static {p0, v0}, Lxs;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/animation/Interpolator;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lxu;

    sget-object v1, Lxs;->a:[F

    sget-object v2, Lxs;->b:[F

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lxu;-><init>([F[FLxt;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/animation/Interpolator;
    .locals 3

    .prologue
    .line 80
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "interpolator"

    const-string v2, "android"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 81
    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 84
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 4

    .prologue
    .line 61
    invoke-static {}, Lxs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string v0, "fast_out_slow_in"

    invoke-static {p0, v0}, Lxs;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/animation/Interpolator;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lxu;

    sget-object v1, Lxs;->e:[F

    sget-object v2, Lxs;->f:[F

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lxu;-><init>([F[FLxt;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 4

    .prologue
    .line 72
    invoke-static {}, Lxs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "fast_out_linear_in"

    invoke-static {p0, v0}, Lxs;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/view/animation/Interpolator;

    move-result-object v0

    .line 75
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lxu;

    sget-object v1, Lxs;->c:[F

    sget-object v2, Lxs;->d:[F

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lxu;-><init>([F[FLxt;)V

    goto :goto_0
.end method
