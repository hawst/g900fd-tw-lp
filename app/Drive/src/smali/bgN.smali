.class public abstract LbgN;
.super LbgG;
.source "PdfStream.java"


# static fields
.field private static final a:[B

.field private static final b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "stream\n"

    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LbgN;->a:[B

    .line 14
    const-string v0, "\nendstream\n"

    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LbgN;->b:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, LbgG;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method protected a(Ljava/io/OutputStream;Ljava/io/InputStream;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 51
    sget-object v0, LbgN;->a:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 52
    const/high16 v0, 0x10000

    new-array v2, v0, [B

    move v0, v1

    .line 55
    :goto_0
    const/4 v3, -0x1

    invoke-virtual {p2, v2}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 56
    invoke-virtual {p1, v2, v1, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 57
    add-int/2addr v0, v4

    goto :goto_0

    .line 59
    :cond_0
    sget-object v1, LbgN;->b:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 60
    sget-object v1, LbgN;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    sget-object v1, LbgN;->b:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected a(Ljava/io/OutputStream;[B)I
    .locals 2

    .prologue
    .line 34
    sget-object v0, LbgN;->a:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 35
    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write([B)V

    .line 36
    sget-object v0, LbgN;->b:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 37
    sget-object v0, LbgN;->a:[B

    array-length v0, v0

    array-length v1, p2

    add-int/2addr v0, v1

    sget-object v1, LbgN;->b:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method
