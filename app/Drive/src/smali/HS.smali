.class public LHS;
.super Ljava/lang/Object;
.source "SortedMergeGridCellAdapter.java"

# interfaces
.implements LHI;


# instance fields
.field private final a:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "LDE",
            "<",
            "LHI;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LHI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LzN;LHp;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LzN;",
            "LHp;",
            "Ljava/util/List",
            "<",
            "LHy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LHS;->a:Ljava/util/List;

    .line 54
    new-instance v0, LHT;

    invoke-direct {v0, p0}, LHT;-><init>(LHS;)V

    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LHS;->a:Laku;

    .line 61
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 62
    new-instance v2, LDG;

    iget-object v0, p0, LHS;->a:Laku;

    invoke-direct {v2, p1, v0, v1}, LDG;-><init>(LzN;Lbjv;I)V

    .line 64
    new-instance v3, LDF;

    iget-object v0, p0, LHS;->a:Laku;

    invoke-direct {v3, p2, v0, v1}, LDF;-><init>(LHp;Lbjv;I)V

    .line 67
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHy;

    .line 69
    invoke-interface {v0, v3, v2}, LHy;->a(LHp;LzN;)LHI;

    move-result-object v0

    .line 70
    iget-object v2, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method static synthetic a(LHS;)Ljava/util/List;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(I)LIL;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, LHS;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LHI;

    .line 89
    iget-object v1, p0, LHS;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 90
    invoke-interface {v0, v1}, LHI;->a(I)LIL;

    move-result-object v0

    return-object v0
.end method

.method public a(I)LIy;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, LHS;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LHI;

    .line 77
    iget-object v1, p0, LHS;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 78
    invoke-interface {v0, v1}, LHI;->a(I)LIy;

    move-result-object v0

    return-object v0
.end method

.method public a(I)LaGv;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, LHS;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LHI;

    .line 104
    iget-object v1, p0, LHS;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 105
    invoke-interface {v0, v1}, LHI;->a(I)LaGv;

    move-result-object v0

    return-object v0
.end method

.method public a(IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 96
    iget-object v0, p0, LHS;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LHI;

    .line 97
    iget-object v1, p0, LHS;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 98
    invoke-interface/range {v0 .. v5}, LHI;->a(IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHI;

    .line 156
    invoke-interface {v0}, LHI;->a()V

    goto :goto_0

    .line 158
    :cond_0
    return-void
.end method

.method public a(LQX;)V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, LHS;->a:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 111
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHI;

    .line 112
    invoke-interface {v0, p1}, LHI;->a(LQX;)V

    goto :goto_0

    .line 114
    :cond_0
    return-void
.end method

.method public a(LaFX;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, LHS;->a:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 119
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHI;

    .line 120
    invoke-interface {v0, p1}, LHI;->a(LaFX;)V

    goto :goto_0

    .line 122
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHI;

    .line 149
    invoke-interface {v0, p1}, LHI;->a(Landroid/view/View;)V

    goto :goto_0

    .line 151
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHI;

    .line 127
    invoke-interface {v0}, LHI;->b()V

    goto :goto_0

    .line 129
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, LHS;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0}, LDE;->a()I

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHI;

    .line 135
    invoke-interface {v0, p1, p2, p3, p4}, LHI;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0

    .line 137
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, LHS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHI;

    .line 142
    invoke-interface {v0, p1, p2}, LHI;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 144
    :cond_0
    return-void
.end method
