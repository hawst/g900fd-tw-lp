.class public LaHF;
.super LaHq;
.source "StarredOp.java"


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Z)V
    .locals 1

    .prologue
    .line 34
    const-string v0, "starred"

    invoke-direct {p0, p1, p2, v0}, LaHq;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 35
    iput-boolean p3, p0, LaHF;->a:Z

    .line 36
    return-void
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHF;
    .locals 2

    .prologue
    .line 41
    new-instance v0, LaHF;

    const-string v1, "starValue"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {v0, p0, p1, v1}, LaHF;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Z)V

    return-object v0
.end method


# virtual methods
.method public a(LaGe;)LaHo;
    .locals 4

    .prologue
    .line 52
    new-instance v0, LaHF;

    iget-object v1, p0, LaHF;->a:LaGg;

    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-virtual {p1}, LaGe;->c()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LaHF;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Z)V

    .line 53
    iget-boolean v1, p0, LaHF;->a:Z

    invoke-virtual {p1, v1}, LaGe;->a(Z)LaGe;

    .line 54
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, LaHq;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 60
    const-string v1, "operationName"

    const-string v2, "starred"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    const-string v1, "starValue"

    iget-boolean v2, p0, LaHF;->a:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 63
    return-object v0
.end method

.method public a(LaJT;)V
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, LaHF;->b(LaJT;)V

    .line 47
    iget-boolean v0, p0, LaHF;->a:Z

    invoke-interface {p1, v0}, LaJT;->a(Z)V

    .line 48
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 68
    instance-of v1, p1, LaHF;

    if-nez v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    check-cast p1, LaHF;

    .line 72
    invoke-virtual {p0, p1}, LaHF;->a(LaHp;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LaHF;->a:Z

    iget-boolean v2, p1, LaHF;->a:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, LaHF;->b()I

    move-result v1

    iget-boolean v0, p0, LaHF;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 82
    const-string v0, "StarredOp[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, LaHF;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaHF;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
