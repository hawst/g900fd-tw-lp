.class public final LTi;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 17
    const-string v0, "oauth2:%s %s %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "https://www.googleapis.com/auth/drive"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "https://www.googleapis.com/auth/activity"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "https://www.googleapis.com/auth/drive.metadata.readonly"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "https://www.googleapis.com/auth/drive.readonly"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LTi;->a:Ljava/lang/String;

    return-void
.end method
