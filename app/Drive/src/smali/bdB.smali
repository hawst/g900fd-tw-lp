.class public final enum LbdB;
.super Ljava/lang/Enum;
.source "MediaHttpDownloader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbdB;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbdB;

.field private static final synthetic a:[LbdB;

.field public static final enum b:LbdB;

.field public static final enum c:LbdB;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, LbdB;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LbdB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdB;->a:LbdB;

    .line 68
    new-instance v0, LbdB;

    const-string v1, "MEDIA_IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LbdB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdB;->b:LbdB;

    .line 71
    new-instance v0, LbdB;

    const-string v1, "MEDIA_COMPLETE"

    invoke-direct {v0, v1, v4}, LbdB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdB;->c:LbdB;

    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [LbdB;

    sget-object v1, LbdB;->a:LbdB;

    aput-object v1, v0, v2

    sget-object v1, LbdB;->b:LbdB;

    aput-object v1, v0, v3

    sget-object v1, LbdB;->c:LbdB;

    aput-object v1, v0, v4

    sput-object v0, LbdB;->a:[LbdB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbdB;
    .locals 1

    .prologue
    .line 63
    const-class v0, LbdB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbdB;

    return-object v0
.end method

.method public static values()[LbdB;
    .locals 1

    .prologue
    .line 63
    sget-object v0, LbdB;->a:[LbdB;

    invoke-virtual {v0}, [LbdB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbdB;

    return-object v0
.end method
