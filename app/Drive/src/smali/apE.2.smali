.class public final enum LapE;
.super Ljava/lang/Enum;
.source "DocListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LapE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LapE;

.field private static final synthetic a:[LapE;

.field public static final enum b:LapE;

.field public static final enum c:LapE;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    new-instance v0, LapE;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, LapE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapE;->a:LapE;

    .line 117
    new-instance v0, LapE;

    const-string v1, "SYNCING"

    invoke-direct {v0, v1, v3}, LapE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapE;->b:LapE;

    .line 119
    new-instance v0, LapE;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4}, LapE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapE;->c:LapE;

    .line 110
    const/4 v0, 0x3

    new-array v0, v0, [LapE;

    sget-object v1, LapE;->a:LapE;

    aput-object v1, v0, v2

    sget-object v1, LapE;->b:LapE;

    aput-object v1, v0, v3

    sget-object v1, LapE;->c:LapE;

    aput-object v1, v0, v4

    sput-object v0, LapE;->a:[LapE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(ZZ)LapE;
    .locals 1

    .prologue
    .line 122
    if-eqz p0, :cond_0

    .line 123
    sget-object v0, LapE;->b:LapE;

    .line 127
    :goto_0
    return-object v0

    .line 124
    :cond_0
    if-eqz p1, :cond_1

    .line 125
    sget-object v0, LapE;->a:LapE;

    goto :goto_0

    .line 127
    :cond_1
    sget-object v0, LapE;->c:LapE;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LapE;
    .locals 1

    .prologue
    .line 110
    const-class v0, LapE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LapE;

    return-object v0
.end method

.method public static values()[LapE;
    .locals 1

    .prologue
    .line 110
    sget-object v0, LapE;->a:[LapE;

    invoke-virtual {v0}, [LapE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LapE;

    return-object v0
.end method
