.class LbkK;
.super Ljava/lang/ref/WeakReference;
.source "LocalCache.java"

# interfaces
.implements LbkF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TV;>;",
        "LbkF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1573
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1574
    iput-object p3, p0, LbkK;->a:Lbkr;

    .line 1575
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1578
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lbkr;)LbkF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "Lbkr",
            "<TK;TV;>;)",
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1588
    new-instance v0, LbkK;

    invoke-direct {v0, p1, p2, p3}, LbkK;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lbkr;)V

    return-object v0
.end method

.method public a()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1582
    iget-object v0, p0, LbkK;->a:Lbkr;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1600
    invoke-virtual {p0}, LbkK;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1585
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1592
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1596
    const/4 v0, 0x1

    return v0
.end method
