.class public abstract LaHq;
.super LaHp;
.source "OperationOnDocEntry.java"


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, LaHp;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected abstract a(LaJT;)V
.end method

.method public a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 32
    invoke-virtual {p2}, LaHD;->a()Laeo;

    move-result-object v1

    invoke-interface {v1, p1}, Laeo;->a(LaHy;)Laen;

    move-result-object v1

    .line 33
    invoke-interface {v1, p3}, Laen;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaJT;

    move-result-object v3

    .line 34
    if-nez v3, :cond_0

    .line 53
    :goto_0
    return v0

    .line 40
    :cond_0
    invoke-virtual {p0, v3}, LaHq;->a(LaJT;)V

    .line 41
    invoke-interface {v3}, LaJT;->o()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 42
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "https://docs.google.com/feeds/default/private/full/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    invoke-interface {v3}, LaJT;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, LaJT;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 42
    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 44
    invoke-interface {v3, v4}, LaJT;->n(Ljava/lang/String;)V

    .line 48
    :cond_1
    iget-object v4, p0, LaHq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v4, v4, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v1, v3, v4}, Laen;->a(LaJT;LaFO;)LaJT;

    move-result-object v1

    .line 49
    if-eqz v1, :cond_3

    move v1, v2

    .line 50
    :goto_1
    if-nez v1, :cond_2

    .line 51
    const-string v3, "OperationOnDocEntry"

    const-string v4, "Error applying %s on %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v0

    invoke-virtual {p0}, LaHq;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {v3, v4, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_2
    move v0, v1

    .line 53
    goto :goto_0

    :cond_3
    move v1, v0

    .line 49
    goto :goto_1
.end method

.method protected final b(LaJT;)V
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, LaHq;->a()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, LaGG;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-interface {p1, v0}, LaJT;->m(Ljava/lang/String;)V

    .line 68
    return-void
.end method
