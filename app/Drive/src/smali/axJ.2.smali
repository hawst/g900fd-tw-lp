.class LaxJ;
.super Laxy;
.source "PdfPageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Lcom/google/android/apps/viewer/client/Dimensions;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaxI;


# direct methods
.method constructor <init>(LaxI;)V
    .locals 2

    .prologue
    .line 189
    iput-object p1, p0, LaxJ;->a:LaxI;

    .line 190
    invoke-static {p1}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    sget-object v1, LaxR;->b:LaxR;

    invoke-direct {p0, v0, v1}, Laxy;-><init>(Laxz;LaxR;)V

    .line 191
    return-void
.end method


# virtual methods
.method protected a(LavE;)Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, LaxJ;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    invoke-interface {p1, v0}, LavE;->a(I)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0, p1}, LaxJ;->a(LavE;)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;Lcom/google/android/apps/viewer/client/Dimensions;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, LaxJ;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    invoke-interface {p1, v0, p2}, LaxH;->a(ILcom/google/android/apps/viewer/client/Dimensions;)V

    .line 200
    iget-object v0, p0, LaxJ;->a:LaxI;

    const/4 v1, 0x0

    iput-object v1, v0, LaxI;->a:LaxJ;

    .line 201
    return-void
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 188
    check-cast p2, Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-virtual {p0, p1, p2}, LaxJ;->a(LaxH;Lcom/google/android/apps/viewer/client/Dimensions;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 204
    const-string v0, "GetDimensionsTask(page=%d)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaxJ;->a:LaxI;

    invoke-static {v3}, LaxI;->a(LaxI;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
