.class public abstract LrX;
.super Lrm;
.source "CreateNewDocActivityBase.java"

# interfaces
.implements LFa;


# instance fields
.field private a:LNl;

.field a:LPw;

.field public a:LaFO;

.field public a:LaGM;

.field public a:LaGv;

.field a:LaKR;

.field a:Lald;

.field protected a:Ljava/lang/String;

.field protected final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lrm;-><init>()V

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LrX;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(LrX;)LNl;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LrX;->a:LNl;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, LrX;->b:Landroid/os/Handler;

    new-instance v1, LrY;

    invoke-direct {v1, p0}, LrY;-><init>(LrX;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 62
    return-void
.end method


# virtual methods
.method public abstract a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
.end method

.method public abstract a(Ljava/lang/Exception;)Z
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    invoke-virtual {p0, p1}, LrX;->c(Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 135
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 137
    new-instance v1, LrZ;

    invoke-direct {v1, p0, p1, v0}, LrZ;-><init>(LrX;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 206
    new-array v2, v5, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 208
    iget-object v2, p0, LrX;->a:LNl;

    invoke-virtual {v2}, LNl;->c()I

    move-result v2

    invoke-virtual {p0, v2}, LrX;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 209
    new-instance v3, Landroid/app/ProgressDialog;

    .line 210
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 211
    invoke-virtual {v3, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 212
    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 213
    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 214
    invoke-virtual {v3, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 215
    new-instance v2, Lsa;

    invoke-direct {v2, p0, v0, v1}, Lsa;-><init>(LrX;Ljava/util/concurrent/atomic/AtomicReference;Landroid/os/AsyncTask;)V

    invoke-virtual {v3, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 224
    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 225
    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 226
    return-void
.end method

.method public abstract f()V
.end method

.method public abstract f()Z
.end method

.method public j()V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, LrX;->a:LNl;

    .line 103
    invoke-virtual {v0}, LNl;->b()I

    move-result v0

    invoke-virtual {p0, v0}, LrX;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LrX;->a:LNl;

    .line 104
    invoke-virtual {v1}, LNl;->a()I

    move-result v1

    invoke-virtual {p0, v1}, LrX;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    move-result-object v0

    .line 111
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->e(Z)V

    .line 112
    invoke-virtual {p0}, LrX;->a()LM;

    move-result-object v1

    const-string v2, "editTitleDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, LrX;->l()V

    .line 124
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, LrX;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 69
    const-string v0, "kindOfDocumentToCreate"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LaGv;

    iput-object v0, p0, LrX;->a:LaGv;

    .line 70
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, LrX;->a:LaFO;

    .line 71
    iget-object v0, p0, LrX;->a:LaGv;

    invoke-static {v0}, LNl;->a(LaGv;)LNl;

    move-result-object v0

    iput-object v0, p0, LrX;->a:LNl;

    .line 72
    iget-object v0, p0, LrX;->a:LNl;

    invoke-virtual {v0}, LNl;->a()I

    move-result v0

    invoke-virtual {p0, v0}, LrX;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LrX;->a:Ljava/lang/String;

    .line 73
    return-void
.end method
