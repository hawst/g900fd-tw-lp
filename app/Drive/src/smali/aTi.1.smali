.class public interface abstract LaTi;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(LaTf;JZ)LaRM;
.end method

.method public abstract a(LaTf;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/internal/lv;)LaRM;
.end method

.method public abstract a(LaTf;Ljava/lang/String;)LaRM;
.end method

.method public abstract a(LaTf;Ljava/lang/String;I)LaRM;
.end method

.method public abstract a(LaTf;Ljava/lang/String;II)LaRM;
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;I)LaRM;
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;II)LaRM;
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIIZ)LaRM;
.end method

.method public abstract a(LaTf;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
.end method

.method public abstract a(Landroid/net/Uri;)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
.end method

.method public abstract a(LaTf;JZ)V
.end method

.method public abstract a(LaTf;Landroid/os/Bundle;)V
.end method

.method public abstract a(LaTf;Lcom/google/android/gms/internal/lf;Ljava/util/List;Lcom/google/android/gms/internal/lj;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Lcom/google/android/gms/internal/lf;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/lj;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;II)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ)V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/internal/hs;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaTf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/hs;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V
.end method

.method public abstract a(LaTf;Ljava/lang/String;Z[Ljava/lang/String;)V
.end method

.method public abstract a(LaTf;ZZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(LaTf;ZZLjava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract b(LaTf;Landroid/os/Bundle;)V
.end method

.method public abstract b(LaTf;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(LaTf;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract b(LaTf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract c(LaTf;Ljava/lang/String;Ljava/lang/String;)V
.end method
