.class LbmW;
.super Lbqv;
.source "ImmutableMultiset.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbqv",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic a:LbmV;

.field a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field final synthetic a:Ljava/util/Iterator;


# direct methods
.method constructor <init>(LbmV;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, LbmW;->a:LbmV;

    iput-object p2, p0, LbmW;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Lbqv;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 241
    iget v0, p0, LbmW;->a:I

    if-gtz v0, :cond_0

    iget-object v0, p0, LbmW;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 245
    iget v0, p0, LbmW;->a:I

    if-gtz v0, :cond_0

    .line 246
    iget-object v0, p0, LbmW;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 247
    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, LbmW;->a:Ljava/lang/Object;

    .line 248
    invoke-interface {v0}, Lbpj;->a()I

    move-result v0

    iput v0, p0, LbmW;->a:I

    .line 250
    :cond_0
    iget v0, p0, LbmW;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LbmW;->a:I

    .line 251
    iget-object v0, p0, LbmW;->a:Ljava/lang/Object;

    return-object v0
.end method
