.class final LOF;
.super LqQ;
.source "PhotosFeature.java"


# instance fields
.field protected final a:LVR;

.field protected final a:Lrg;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lrx;LVR;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, LqQ;-><init>(Ljava/lang/String;Lrx;)V

    .line 37
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVR;

    iput-object v0, p0, LOF;->a:LVR;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, LOF;->a:Lrg;

    .line 39
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lrx;LVR;Lrg;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, LqQ;-><init>(Ljava/lang/String;Lrx;)V

    .line 44
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVR;

    iput-object v0, p0, LOF;->a:LVR;

    .line 48
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrg;

    iput-object v0, p0, LOF;->a:Lrg;

    .line 49
    return-void
.end method


# virtual methods
.method public final a(LtK;LQr;LpW;LaFO;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, LOF;->a:Lrg;

    if-eqz v1, :cond_0

    iget-object v1, p0, LOF;->a:Lrg;

    invoke-interface {p1, v1, p4}, LtK;->a(Lrg;LaFO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    :goto_0
    return v0

    .line 58
    :cond_0
    new-instance v1, LOx;

    invoke-direct {v1, p3}, LOx;-><init>(LpW;)V

    .line 61
    :try_start_0
    iget-object v2, p0, LOF;->a:LVR;

    invoke-virtual {v1, p4, v2}, LOx;->a(LaFO;LVR;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v1

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Feature flag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, LOF;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error loading whitelist status"

    invoke-static {v2, v1, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
