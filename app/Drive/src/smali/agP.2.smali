.class LagP;
.super Ljava/lang/Thread;
.source "LegacySyncManager.java"


# instance fields
.field final synthetic a:LagO;

.field final synthetic a:Landroid/accounts/Account;

.field final synthetic a:Landroid/content/SyncResult;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(LagO;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, LagP;->a:LagO;

    iput-object p3, p0, LagP;->a:Landroid/accounts/Account;

    iput-object p4, p0, LagP;->a:Ljava/lang/String;

    iput-object p5, p0, LagP;->a:Landroid/content/SyncResult;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 414
    :try_start_0
    iget-object v1, p0, LagP;->a:LagO;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 415
    :try_start_1
    const-string v0, "SyncManager"

    const-string v2, "Sync started: %s %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LagP;->a:Landroid/accounts/Account;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LagP;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 416
    iget-object v0, p0, LagP;->a:LagO;

    invoke-static {v0}, LagO;->a(LagO;)Lamh;

    move-result-object v2

    iget-object v0, p0, LagP;->a:LagO;

    invoke-static {v0}, LagO;->a(LagO;)Laja;

    move-result-object v0

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string v3, "SyncManager"

    new-instance v4, LagQ;

    invoke-direct {v4, p0}, LagQ;-><init>(LagP;)V

    invoke-interface {v2, v0, v3, v4}, Lamh;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 422
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 424
    iget-object v0, p0, LagP;->a:LagO;

    invoke-static {v0}, LagO;->a(LagO;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    iget-object v1, p0, LagP;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    const-string v0, "SyncManager"

    const-string v1, "Sync completed. %s"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LagP;->a:Landroid/content/SyncResult;

    invoke-virtual {v3}, Landroid/content/SyncResult;->toDebugString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 428
    return-void

    .line 422
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 424
    :catchall_1
    move-exception v0

    iget-object v1, p0, LagP;->a:LagO;

    invoke-static {v1}, LagO;->a(LagO;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    iget-object v2, p0, LagP;->a:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncManager-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LagP;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
