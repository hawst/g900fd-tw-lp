.class public LXf;
.super LXn;
.source "InterceptingPunchWebViewTouchListener.java"


# instance fields
.field private final a:LSx;

.field private final a:LXh;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LXR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LXz;LYO;Lbxw;LqK;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LXz;",
            "LYO;",
            "Lbxw",
            "<",
            "LXR;",
            ">;",
            "LqK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p5}, LXn;-><init>(Landroid/content/Context;LXz;LYO;LqK;)V

    .line 45
    new-instance v0, LXh;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LXh;-><init>(LXg;)V

    iput-object v0, p0, LXf;->a:LXh;

    .line 53
    new-instance v0, LSx;

    iget-object v1, p0, LXf;->a:LXh;

    invoke-direct {v0, p1, v1}, LSx;-><init>(Landroid/content/Context;LSA;)V

    iput-object v0, p0, LXf;->a:LSx;

    .line 54
    iput-object p4, p0, LXf;->a:Lbxw;

    .line 55
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, LXf;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXR;

    .line 76
    if-gez p1, :cond_0

    .line 77
    iget-object v1, p0, LXf;->a:LqK;

    const-string v2, "punch"

    const-string v3, "webViewPunchSlideSwipeNext"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-interface {v0}, LXR;->l_()V

    .line 85
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v1, p0, LXf;->a:LqK;

    const-string v2, "punch"

    const-string v3, "webViewPunchSlideSwipePrevious"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-interface {v0}, LXR;->m_()V

    goto :goto_0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-super {p0, p1, p2}, LXn;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 62
    iget-object v0, p0, LXf;->a:LXh;

    iput-object p1, v0, LXh;->a:Landroid/view/View;

    .line 64
    :try_start_0
    iget-object v0, p0, LXf;->a:LSx;

    invoke-virtual {v0, p2}, LSx;->a(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    iget-object v0, p0, LXf;->a:LXh;

    iput-object v2, v0, LXh;->a:Landroid/view/View;

    .line 69
    const/4 v0, 0x1

    return v0

    .line 66
    :catchall_0
    move-exception v0

    iget-object v1, p0, LXf;->a:LXh;

    iput-object v2, v1, LXh;->a:Landroid/view/View;

    throw v0
.end method
