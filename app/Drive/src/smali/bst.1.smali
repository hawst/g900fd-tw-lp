.class public final Lbst;
.super Ljava/lang/Object;
.source "IntMath.java"


# static fields
.field static final a:[B

.field static final a:[I

.field static final b:[I

.field static c:[I

.field private static final d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 162
    const/16 v0, 0x21

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lbst;->a:[B

    .line 166
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lbst;->a:[I

    .line 171
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lbst;->b:[I

    .line 490
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lbst;->d:[I

    .line 530
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lbst;->c:[I

    return-void

    .line 162
    :array_0
    .array-data 1
        0x9t
        0x9t
        0x9t
        0x8t
        0x8t
        0x8t
        0x7t
        0x7t
        0x7t
        0x6t
        0x6t
        0x6t
        0x6t
        0x5t
        0x5t
        0x5t
        0x4t
        0x4t
        0x4t
        0x3t
        0x3t
        0x3t
        0x3t
        0x2t
        0x2t
        0x2t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 166
    nop

    :array_1
    .array-data 4
        0x1
        0xa
        0x64
        0x3e8
        0x2710
        0x186a0
        0xf4240
        0x989680
        0x5f5e100
        0x3b9aca00
    .end array-data

    .line 171
    :array_2
    .array-data 4
        0x3
        0x1f
        0x13c
        0xc5a
        0x7b86
        0x4d343
        0x3040a5
        0x1e28678
        0x12d940b6
        0x7fffffff
    .end array-data

    .line 490
    :array_3
    .array-data 4
        0x1
        0x1
        0x2
        0x6
        0x18
        0x78
        0x2d0
        0x13b0
        0x9d80
        0x58980
        0x375f00
        0x2611500
        0x1c8cfc00
    .end array-data

    .line 530
    :array_4
    .array-data 4
        0x7fffffff
        0x7fffffff
        0x10000
        0x929
        0x1dd
        0xc1
        0x6e
        0x4b
        0x3a
        0x31
        0x2b
        0x27
        0x25
        0x23
        0x22
        0x22
        0x21
    .end array-data
.end method

.method public static a(II)I
    .locals 4

    .prologue
    const/16 v3, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 185
    const-string v2, "exponent"

    invoke-static {v2, p1}, Lbsv;->a(Ljava/lang/String;I)I

    .line 186
    packed-switch p0, :pswitch_data_0

    move v3, v0

    move v2, p0

    .line 205
    :goto_0
    packed-switch p1, :pswitch_data_1

    .line 211
    and-int/lit8 v1, p1, 0x1

    if-nez v1, :cond_5

    move v1, v0

    :goto_1
    mul-int/2addr v3, v1

    .line 212
    mul-int/2addr v2, v2

    .line 204
    shr-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 188
    :pswitch_0
    if-nez p1, :cond_1

    .line 209
    :cond_0
    :goto_2
    :pswitch_1
    return v0

    :cond_1
    move v0, v1

    .line 188
    goto :goto_2

    .line 192
    :pswitch_2
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    goto :goto_2

    .line 194
    :pswitch_3
    if-ge p1, v3, :cond_2

    shl-int v1, v0, p1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 196
    :pswitch_4
    if-ge p1, v3, :cond_4

    .line 197
    and-int/lit8 v1, p1, 0x1

    if-nez v1, :cond_3

    shl-int/2addr v0, p1

    goto :goto_2

    :cond_3
    shl-int/2addr v0, p1

    neg-int v0, v0

    goto :goto_2

    :cond_4
    move v0, v1

    .line 199
    goto :goto_2

    :pswitch_5
    move v0, v3

    .line 207
    goto :goto_2

    .line 209
    :pswitch_6
    mul-int v0, v2, v3

    goto :goto_2

    :cond_5
    move v1, v2

    .line 211
    goto :goto_1

    .line 186
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 205
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
