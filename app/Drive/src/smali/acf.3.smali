.class Lacf;
.super Ljava/lang/Object;
.source "ServerAclManager.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/Set",
        "<",
        "Lqo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lace;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/ResourceSpec;


# direct methods
.method constructor <init>(Lace;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lacf;->a:Lace;

    iput-object p2, p0, Lacf;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lqo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 58
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 93
    :cond_0
    :goto_0
    return-object v0

    .line 63
    :cond_1
    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 64
    iget-object v0, p0, Lacf;->a:Lace;

    invoke-static {v0}, Lace;->a(Lace;)Lbxw;

    move-result-object v0

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lael;

    iget-object v2, p0, Lacf;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 65
    invoke-static {v2}, Lqz;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lacf;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v4, v4, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    const/4 v5, 0x0

    .line 64
    invoke-virtual {v0, v2, v4, v5}, Lael;->a(Ljava/lang/String;LaFO;Ljava/lang/String;)LaeI;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 66
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lbxk; {:try_start_1 .. :try_end_1} :catch_6
    .catch LbwO; {:try_start_1 .. :try_end_1} :catch_5
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 92
    if-eqz v2, :cond_0

    .line 93
    invoke-virtual {v2}, LaeI;->a()V

    goto :goto_0

    .line 70
    :cond_2
    :try_start_2
    invoke-virtual {v2}, LaeI;->b()Lbxc;

    .line 72
    :goto_1
    invoke-virtual {v2}, LaeI;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 73
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_6
    .catch LbwO; {:try_start_2 .. :try_end_2} :catch_5
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 92
    if-eqz v2, :cond_0

    .line 93
    invoke-virtual {v2}, LaeI;->a()V

    goto :goto_0

    .line 76
    :cond_3
    :try_start_3
    invoke-virtual {v2, v3}, LaeI;->a(Lbxb;)LaeB;

    move-result-object v3

    .line 77
    iget-object v0, p0, Lacf;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {v3, v0}, LaeB;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lqo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    const-string v0, "ServerAclLoader"

    const-string v4, "Got ACL entry %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lbxk; {:try_start_3 .. :try_end_3} :catch_6
    .catch LbwO; {:try_start_3 .. :try_end_3} :catch_5
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 81
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 82
    :goto_2
    :try_start_4
    new-instance v2, Labf;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_4

    .line 93
    invoke-virtual {v2}, LaeI;->a()V

    :cond_4
    throw v0

    .line 92
    :cond_5
    if-eqz v2, :cond_6

    .line 93
    invoke-virtual {v2}, LaeI;->a()V

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    .line 83
    :catch_1
    move-exception v0

    move-object v2, v3

    .line 84
    :goto_4
    :try_start_5
    new-instance v1, Labf;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parse Exception in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lbxk;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 92
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 85
    :catch_2
    move-exception v0

    move-object v2, v3

    .line 86
    :goto_5
    new-instance v1, Labf;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authentication Exception in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 87
    invoke-virtual {v0}, LbwO;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 88
    :catch_3
    move-exception v0

    move-object v2, v3

    .line 89
    :goto_6
    new-instance v1, Labf;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authenticator Exception in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 90
    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 92
    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_3

    .line 88
    :catch_4
    move-exception v0

    goto :goto_6

    .line 85
    :catch_5
    move-exception v0

    goto :goto_5

    .line 83
    :catch_6
    move-exception v0

    goto :goto_4

    .line 81
    :catch_7
    move-exception v0

    move-object v1, v3

    goto/16 :goto_2
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lacf;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
