.class public LbgB;
.super Ljava/lang/Object;
.source "Util.java"


# direct methods
.method public static a(II)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 90
    .line 91
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 98
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 99
    add-int/lit8 v0, p0, 0x2d

    div-int/lit8 v0, v0, 0x5a

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit16 p1, v0, 0x168

    .line 101
    :cond_1
    return p1

    .line 94
    :cond_2
    sub-int v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 95
    rsub-int v2, v1, 0x168

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 96
    const/16 v2, 0x32

    if-ge v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 71
    packed-switch v1, :pswitch_data_0

    .line 77
    :goto_0
    :pswitch_0
    return v0

    .line 73
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 74
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 75
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_pdf_paper_size_default:I

    .line 112
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    .line 112
    :cond_0
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_pdf_paper_size_default_non_us:I

    .line 113
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/widget/EditText;Landroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 126
    new-instance v0, LbgC;

    invoke-direct {v0, p1}, LbgC;-><init>(Landroid/app/Dialog;)V

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 148
    return-void
.end method
