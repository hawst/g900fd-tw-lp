.class public final enum LIK;
.super Ljava/lang/Enum;
.source "SortKind.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LIK;

.field private static final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LIK;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LIK;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LIK;

.field public static final enum b:LIK;

.field public static final enum c:LIK;

.field public static final enum d:LIK;

.field public static final enum e:LIK;

.field public static final enum f:LIK;

.field public static final enum g:LIK;

.field public static final enum h:LIK;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, LIK;

    const-string v1, "CREATION_TIME"

    const-string v3, "creationTime"

    sget v4, Lxi;->menu_sort_creation_time:I

    sget v5, Lxi;->doclist_sortby_creation_time:I

    sget v6, Lxi;->doclist_date_created_label:I

    invoke-direct/range {v0 .. v6}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v0, LIK;->a:LIK;

    .line 21
    new-instance v3, LIK;

    const-string v4, "FOLDERS_THEN_TITLE"

    const-string v6, "folderThenTitle"

    sget v7, Lxi;->menu_sort_title:I

    sget v8, Lxi;->doclist_sortby_modified_title:I

    sget v9, Lxi;->doclist_date_modified_label:I

    move v5, v10

    invoke-direct/range {v3 .. v9}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LIK;->b:LIK;

    .line 27
    new-instance v3, LIK;

    const-string v4, "LAST_MODIFIED"

    const-string v6, "lastModified"

    sget v7, Lxi;->menu_sort_last_modified:I

    sget v8, Lxi;->doclist_sortby_modified_title:I

    sget v9, Lxi;->doclist_date_modified_label:I

    move v5, v11

    invoke-direct/range {v3 .. v9}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LIK;->c:LIK;

    .line 29
    new-instance v3, LIK;

    const-string v4, "OPENED_BY_ME_DATE"

    const-string v6, "openedByMeDate"

    sget v7, Lxi;->menu_sort_recently_opened:I

    sget v8, Lxi;->doclist_sortby_opened_title:I

    sget v9, Lxi;->doclist_date_opened_label:I

    move v5, v12

    invoke-direct/range {v3 .. v9}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LIK;->d:LIK;

    .line 31
    new-instance v3, LIK;

    const-string v4, "OPENED_BY_ME_OR_CREATED_DATE"

    const-string v6, "openedByMeOrCreateDate"

    sget v7, Lxi;->menu_sort_recently_opened:I

    sget v8, Lxi;->doclist_sortby_opened_title:I

    sget v9, Lxi;->doclist_date_opened_label:I

    move v5, v13

    invoke-direct/range {v3 .. v9}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LIK;->e:LIK;

    .line 34
    new-instance v3, LIK;

    const-string v4, "EDITED_BY_ME_DATE"

    const/4 v5, 0x5

    const-string v6, "editedByMeDate"

    sget v7, Lxi;->menu_sort_recently_edited:I

    sget v8, Lxi;->doclist_sortby_edited_title:I

    sget v9, Lxi;->doclist_date_edited_label:I

    invoke-direct/range {v3 .. v9}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LIK;->f:LIK;

    .line 36
    new-instance v3, LIK;

    const-string v4, "QUOTA_USED"

    const/4 v5, 0x6

    const-string v6, "quotaUsed"

    sget v7, Lxi;->menu_sort_quota_used:I

    sget v8, Lxi;->doclist_sortby_quota_used:I

    sget v9, Lxi;->doclist_quota_used_label:I

    invoke-direct/range {v3 .. v9}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LIK;->g:LIK;

    .line 38
    new-instance v3, LIK;

    const-string v4, "SHARED_WITH_ME_DATE"

    const/4 v5, 0x7

    const-string v6, "sharedWithMeDate"

    sget v7, Lxi;->doclist_sortby_shared_title:I

    sget v8, Lxi;->doclist_sortby_shared_title:I

    sget v9, Lxi;->doclist_date_shared_label:I

    invoke-direct/range {v3 .. v9}, LIK;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LIK;->h:LIK;

    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [LIK;

    sget-object v1, LIK;->a:LIK;

    aput-object v1, v0, v2

    sget-object v1, LIK;->b:LIK;

    aput-object v1, v0, v10

    sget-object v1, LIK;->c:LIK;

    aput-object v1, v0, v11

    sget-object v1, LIK;->d:LIK;

    aput-object v1, v0, v12

    sget-object v1, LIK;->e:LIK;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v3, LIK;->f:LIK;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, LIK;->g:LIK;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v3, LIK;->h:LIK;

    aput-object v3, v0, v1

    sput-object v0, LIK;->a:[LIK;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LIK;->a:Ljava/util/Map;

    .line 44
    const-class v0, LIK;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    .line 45
    sget-object v3, LIK;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 81
    :cond_0
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    .line 82
    invoke-static {}, LIK;->values()[LIK;

    move-result-object v1

    array-length v3, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v1, v2

    .line 83
    invoke-virtual {v4}, LIK;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 84
    invoke-virtual {v4}, LIK;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 82
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 87
    :cond_2
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LIK;->a:LbmL;

    .line 88
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-object p3, p0, LIK;->a:Ljava/lang/String;

    .line 57
    iput p4, p0, LIK;->a:I

    .line 58
    iput p5, p0, LIK;->b:I

    .line 59
    iput p6, p0, LIK;->c:I

    .line 60
    return-void
.end method

.method public static a(Ljava/lang/String;)LIK;
    .locals 1

    .prologue
    .line 91
    sget-object v0, LIK;->a:LbmL;

    invoke-virtual {v0, p0}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LIK;
    .locals 1

    .prologue
    .line 18
    const-class v0, LIK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIK;

    return-object v0
.end method

.method public static values()[LIK;
    .locals 1

    .prologue
    .line 18
    sget-object v0, LIK;->a:[LIK;

    invoke-virtual {v0}, [LIK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIK;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, LIK;->a:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, LIK;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, LIK;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, LIK;->c:I

    return v0
.end method
