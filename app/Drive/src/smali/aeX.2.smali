.class LaeX;
.super Ljava/lang/Object;
.source "DatabaseSyncMoreController.java"

# interfaces
.implements Lafg;


# instance fields
.field private final a:I

.field private final a:LaFM;

.field final synthetic a:LaeV;

.field private final a:Lafy;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field private final a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

.field private final a:Z


# direct methods
.method constructor <init>(LaeV;Landroid/content/Context;ZLaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;)V
    .locals 1

    .prologue
    .line 41
    iput-object p1, p0, LaeX;->a:LaeV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LaeX;->a:Landroid/os/Handler;

    .line 38
    iget-object v0, p0, LaeX;->a:LaeV;

    invoke-static {v0}, LaeV;->a(LaeV;)I

    move-result v0

    iput v0, p0, LaeX;->a:I

    .line 42
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LaeX;->a:Landroid/content/Context;

    .line 43
    iput-boolean p3, p0, LaeX;->a:Z

    .line 44
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFM;

    iput-object v0, p0, LaeX;->a:LaFM;

    .line 45
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iput-object v0, p0, LaeX;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 46
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafy;

    iput-object v0, p0, LaeX;->a:Lafy;

    .line 47
    return-void
.end method

.method static synthetic a(LaeX;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, LaeX;->a:I

    return v0
.end method

.method static synthetic a(LaeX;)LaFM;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LaeX;->a:LaFM;

    return-object v0
.end method

.method static synthetic a(LaeX;)Lafy;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LaeX;->a:Lafy;

    return-object v0
.end method

.method static synthetic a(LaeX;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LaeX;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LaeX;)Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LaeX;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    return-object v0
.end method

.method static synthetic a(LaeX;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, LaeX;->a:Z

    return v0
.end method


# virtual methods
.method public a(ZZ)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, LaeX;->a:Landroid/os/Handler;

    new-instance v1, LaeY;

    invoke-direct {v1, p0, p1, p2}, LaeY;-><init>(LaeX;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 65
    return-void
.end method
