.class public LaKd;
.super Ljava/lang/Object;
.source "ApiaryAccountMetadataEntry.java"

# interfaces
.implements LaJR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method


# virtual methods
.method public a(Lcom/google/api/services/drive/model/About;Lcom/google/api/services/drive/model/AppList;)LaJQ;
    .locals 2

    .prologue
    .line 80
    new-instance v0, LaKb;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, LaKb;-><init>(Lcom/google/api/services/drive/model/About;Lcom/google/api/services/drive/model/AppList;LaKc;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)LaJQ;
    .locals 4

    .prologue
    .line 58
    if-nez p1, :cond_0

    .line 59
    new-instance v0, Lbxk;

    const-string v1, "Capability string may not be null"

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 64
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 67
    new-instance v3, LbeY;

    invoke-direct {v3}, LbeY;-><init>()V

    .line 68
    const-class v0, Lcom/google/api/services/drive/model/About;

    invoke-virtual {v3, v1, v0}, LbeO;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About;

    .line 69
    const-class v1, Lcom/google/api/services/drive/model/AppList;

    invoke-virtual {v3, v2, v1}, LbeO;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/drive/model/AppList;

    .line 70
    new-instance v2, LaKb;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, LaKb;-><init>(Lcom/google/api/services/drive/model/About;Lcom/google/api/services/drive/model/AppList;LaKc;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v2

    .line 71
    :catch_0
    move-exception v0

    .line 72
    new-instance v1, Lbxk;

    const-string v2, "Error parsing capability string with Apiary parser"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 73
    :catch_1
    move-exception v0

    .line 74
    new-instance v1, Lbxk;

    const-string v2, "Error deserializing JSON array"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
