.class public Layg;
.super Ljava/lang/Object;
.source "SwipeySwitcher.java"


# instance fields
.field private a:Layc;

.field final synthetic a:Lcom/google/android/common/SwipeySwitcher;

.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Layd;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Layd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/common/SwipeySwitcher;Layc;)V
    .locals 1

    .prologue
    .line 113
    iput-object p1, p0, Layg;->a:Lcom/google/android/common/SwipeySwitcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Layg;->a:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Layg;->b:Ljava/util/ArrayList;

    .line 114
    iput-object p2, p0, Layg;->a:Layc;

    .line 115
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Layd;
    .locals 5

    .prologue
    .line 118
    iget-object v0, p0, Layg;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Layg;->a:Ljava/util/ArrayList;

    new-instance v1, Layh;

    iget-object v2, p0, Layg;->a:Layc;

    invoke-interface {v2}, Layc;->a()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Layg;->a:Ljava/util/ArrayList;

    iget-object v4, p0, Layg;->b:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4}, Layh;-><init>(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_0
    iget-object v0, p0, Layg;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Layg;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layd;

    .line 122
    iget-object v1, p0, Layg;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 123
    check-cast v1, Layh;

    invoke-static {v1, p1}, Layh;->a(Layh;Landroid/content/Intent;)Landroid/content/Intent;

    .line 124
    return-object v0
.end method

.method public a(Layd;)Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Layg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Layg;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
