.class LAq;
.super Ljava/lang/Object;
.source "CreateBarControllerImpl.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:LAp;


# direct methods
.method constructor <init>(LAp;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, LAq;->a:LAp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, LAq;->a:LAp;

    invoke-static {v0}, LAp;->a(LAp;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 90
    :goto_0
    return-void

    .line 76
    :pswitch_0
    iget-object v0, p0, LAq;->a:LAp;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LAp;->a(LAp;I)I

    .line 77
    iget-object v0, p0, LAq;->a:LAp;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LAp;->a(LAp;F)F

    goto :goto_0

    .line 82
    :pswitch_1
    iget-object v0, p0, LAq;->a:LAp;

    invoke-static {v0, v2}, LAp;->a(LAp;I)I

    .line 83
    iget-object v0, p0, LAq;->a:LAp;

    iget-object v1, p0, LAq;->a:LAp;

    invoke-static {v1}, LAp;->a(LAp;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LAp;->a(LAp;F)F

    .line 84
    iget-object v0, p0, LAq;->a:LAp;

    invoke-static {v0, v2}, LAp;->a(LAp;Z)V

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method
