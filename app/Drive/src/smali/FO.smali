.class public LFO;
.super Ljava/lang/Object;
.source "DasherHelper.java"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static final d:Ljava/util/regex/Pattern;

.field private static final e:Ljava/util/regex/Pattern;

.field private static final f:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "(?:docs[0-9]*|drive)\\.google\\.com"

    .line 29
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LFO;->a:Ljava/util/regex/Pattern;

    .line 36
    const-string v0, "drive\\.google\\.com"

    .line 37
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LFO;->b:Ljava/util/regex/Pattern;

    .line 44
    const-string v0, "spreadsheets[0-9]*\\.google\\.com"

    .line 45
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LFO;->c:Ljava/util/regex/Pattern;

    .line 52
    const-string v0, ".*\\.google(\\.co(m?))?(\\.\\w{2})?"

    .line 53
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LFO;->d:Ljava/util/regex/Pattern;

    .line 59
    const-string v0, "^((/(corp|prod|scary))?/drive)(/.*)"

    .line 60
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LFO;->e:Ljava/util/regex/Pattern;

    .line 61
    const-string v0, "^(/a/([a-zA-Z0-9.-]+))(/.*)"

    .line 62
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LFO;->f:Ljava/util/regex/Pattern;

    .line 61
    return-void
.end method

.method private static a(Landroid/net/Uri;)LbiQ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LbiQ",
            "<",
            "LFP;",
            "Ljava/util/regex/Matcher;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-static {}, LFP;->values()[LFP;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 83
    iget-object v5, v4, LFP;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 84
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 85
    new-instance v0, LbiQ;

    invoke-direct {v0, v4, v5}, LbiQ;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 89
    :goto_1
    return-object v0

    .line 82
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 120
    const-string v0, "DasherHelper"

    const-string v1, "in getPathSuffixWithoutDomainInstanceIdentifier for uri=%s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p0, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 121
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 122
    if-nez v0, :cond_1

    .line 123
    const-string v0, "DasherHelper"

    const-string v1, "no path found"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 124
    const/4 v0, 0x0

    .line 137
    :cond_0
    :goto_0
    return-object v0

    .line 127
    :cond_1
    invoke-static {p0}, LFO;->a(Landroid/net/Uri;)LbiQ;

    move-result-object v1

    .line 128
    if-eqz v1, :cond_0

    .line 129
    iget-object v0, v1, LbiQ;->a:Ljava/lang/Object;

    check-cast v0, LFP;

    .line 130
    iget-object v1, v1, LbiQ;->b:Ljava/lang/Object;

    check-cast v1, Ljava/util/regex/Matcher;

    .line 131
    iget v2, v0, LFP;->b:I

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 132
    const-string v2, "DasherHelper"

    const-string v3, "Found through %s Non Dasher path [%s] for uri=%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v1, v4, v6

    const/4 v0, 0x2

    aput-object p0, v4, v0

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    .line 134
    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 96
    const-string v0, "DasherHelper"

    const-string v1, "in reformatPath uri=%s newPath=%s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p0, v2, v4

    aput-object p1, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 97
    invoke-static {p0}, LFO;->a(Landroid/net/Uri;)LbiQ;

    move-result-object v1

    .line 98
    if-eqz v1, :cond_1

    .line 99
    iget-object v0, v1, LbiQ;->a:Ljava/lang/Object;

    check-cast v0, LFP;

    .line 100
    iget-object v1, v1, LbiQ;->b:Ljava/lang/Object;

    check-cast v1, Ljava/util/regex/Matcher;

    .line 101
    iget v2, v0, LFP;->a:I

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 103
    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :cond_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 109
    const-string v1, "DasherHelper"

    const-string v2, "Detected a %s and reformatted path as=%s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    aput-object p1, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 113
    :cond_1
    return-object p1
.end method

.method static synthetic a()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 20
    sget-object v0, LFO;->e:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    .line 146
    const-string v0, "DasherHelper"

    const-string v1, "isHostGoogleDocsOrDrive uri=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 147
    sget-object v0, LFO;->a:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, LFO;->a(Ljava/util/regex/Pattern;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/regex/Pattern;Landroid/net/Uri;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 151
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 152
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 158
    :goto_0
    return v0

    .line 156
    :cond_0
    invoke-virtual {p0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    .line 157
    const-string v3, "DasherHelper"

    const-string v4, "isHostMatched returns %b for hostUri=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x1

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 158
    goto :goto_0
.end method

.method static synthetic b()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 20
    sget-object v0, LFO;->f:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static b(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 180
    :goto_0
    return v0

    :cond_0
    sget-object v0, LFO;->c:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    .line 187
    const-string v0, "DasherHelper"

    const-string v1, "isHostGoogle uri=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 188
    sget-object v0, LFO;->d:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, LFO;->a(Ljava/util/regex/Pattern;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
