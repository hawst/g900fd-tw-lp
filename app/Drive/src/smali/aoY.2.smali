.class public LaoY;
.super Ljava/lang/Object;
.source "ReusableThumbnailCache.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static final a:LakG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakG",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LbjP",
            "<",
            "Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, LaoZ;

    invoke-direct {v0}, LaoZ;-><init>()V

    sput-object v0, LaoY;->a:LakG;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LQr;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LwZ;->thumbnail_no_thumbnail_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LaoY;->a:I

    .line 53
    new-instance v0, Lapa;

    invoke-direct {v0, p0, p2}, Lapa;-><init>(LaoY;LQr;)V

    iput-object v0, p0, LaoY;->a:Ljava/lang/ThreadLocal;

    .line 73
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 103
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 104
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 105
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 106
    iget v3, p0, LaoY;->a:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 107
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 108
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 109
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, p1, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;Landroid/graphics/Bitmap;LamE;)LakD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "Landroid/graphics/Bitmap;",
            "LamE;",
            ")",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v1

    .line 78
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 79
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    if-ne v0, v2, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 80
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap$Config;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    :cond_0
    :try_start_0
    iget-object v0, p0, LaoY;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbjP;

    invoke-interface {v0, v1}, LbjP;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    invoke-direct {p0, p2, v0}, LaoY;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    move-object p2, v0

    .line 90
    :cond_1
    invoke-static {p2, p3}, Lcom/google/android/apps/docs/utils/RawPixelData;->a(Landroid/graphics/Bitmap;LamE;)Lcom/google/android/apps/docs/utils/RawPixelData;

    move-result-object v0

    .line 91
    sget-object v1, LaoY;->a:LakG;

    invoke-static {v0, v1}, LakD;->a(Ljava/lang/Object;LakG;)LakD;

    move-result-object v0

    return-object v0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 96
    :try_start_0
    iget-object v0, p0, LaoY;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbjP;

    invoke-interface {v0, p1}, LbjP;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
