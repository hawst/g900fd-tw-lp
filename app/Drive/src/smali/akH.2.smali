.class public LakH;
.super Ljava/util/AbstractQueue;
.source "ClosestDistancePriorityBlockingQueue.java"

# interfaces
.implements Lalf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Ljava/lang/Comparable",
        "<TK;>;E::",
        "LakM",
        "<TK;>;>",
        "Ljava/util/AbstractQueue",
        "<TE;>;",
        "Lalf",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private a:J

.field private final a:LakJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakJ",
            "<TK;>;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<TK;",
            "Ljava/util/Queue",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/locks/Condition;

.field private final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private b:I


# direct methods
.method private constructor <init>(Ljava/lang/Comparable;ILakJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LakJ",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 210
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 168
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 169
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v1

    iput-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/Condition;

    .line 170
    invoke-static {}, LboS;->a()Ljava/util/TreeMap;

    move-result-object v1

    iput-object v1, p0, LakH;->a:Ljava/util/SortedMap;

    .line 178
    iput v0, p0, LakH;->b:I

    .line 179
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LakH;->a:J

    .line 211
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 212
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    iput-object v0, p0, LakH;->a:Ljava/lang/Comparable;

    .line 213
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakJ;

    iput-object v0, p0, LakH;->a:LakJ;

    .line 214
    invoke-interface {p3, p1}, LakJ;->a(Ljava/lang/Object;)Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, LakH;->a:Ljava/util/Comparator;

    .line 215
    iput p2, p0, LakH;->a:I

    .line 216
    return-void
.end method

.method static synthetic a(LakH;)I
    .locals 2

    .prologue
    .line 39
    iget v0, p0, LakH;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LakH;->b:I

    return v0
.end method

.method static synthetic a(LakH;)J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, LakH;->a:J

    return-wide v0
.end method

.method public static a(JI)LakH;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LakM",
            "<",
            "Ljava/lang/Long;",
            ">;>(JI)",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 191
    new-instance v0, LakH;

    .line 192
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, LakK;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, LakK;-><init>(LakI;)V

    invoke-direct {v0, v1, p2, v2}, LakH;-><init>(Ljava/lang/Comparable;ILakJ;)V

    return-object v0
.end method

.method private a(Ljava/lang/Comparable;)LakM;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TE;"
        }
    .end annotation

    .prologue
    .line 466
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 468
    :try_start_0
    iget-object v0, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 469
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LakM;

    .line 470
    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    :cond_0
    iget v0, p0, LakH;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LakH;->b:I

    .line 474
    iget-wide v2, p0, LakH;->a:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, LakH;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    .line 327
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 330
    :try_start_0
    iget-object v0, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    .line 331
    iget-object v1, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    .line 332
    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 333
    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 334
    invoke-interface {v1}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    .line 335
    iget-object v2, p0, LakH;->a:Ljava/util/Comparator;

    invoke-interface {v2, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-gez v2, :cond_0

    .line 344
    :goto_0
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 335
    goto :goto_0

    .line 336
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 337
    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    .line 338
    :cond_2
    :try_start_2
    invoke-interface {v1}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 339
    invoke-interface {v1}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 344
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    .line 341
    :cond_3
    const/4 v0, 0x0

    .line 344
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method static synthetic a(LakH;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LakH;->a:Ljava/util/SortedMap;

    return-object v0
.end method

.method static synthetic a(LakH;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method static synthetic b(LakH;)J
    .locals 4

    .prologue
    .line 39
    iget-wide v0, p0, LakH;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, LakH;->a:J

    return-wide v0
.end method

.method private b(LakM;)LakM;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 441
    iget v0, p0, LakH;->b:I

    iget v1, p0, LakH;->a:I

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 442
    const/4 v0, 0x0

    .line 443
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 445
    :try_start_0
    iget v1, p0, LakH;->b:I

    iget v2, p0, LakH;->a:I

    if-ne v1, v2, :cond_2

    .line 446
    invoke-interface {p1}, LakM;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 447
    iget-object v1, p0, LakH;->a:Ljava/lang/Comparable;

    invoke-direct {p0, v1}, LakH;->b(Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v1

    .line 448
    iget-object v2, p0, LakH;->a:Ljava/util/Comparator;

    invoke-interface {v2, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    .line 449
    invoke-direct {p0, v1}, LakH;->a(Ljava/lang/Comparable;)LakM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p1

    .line 455
    :cond_0
    :goto_1
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 457
    return-object p1

    .line 441
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 455
    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_2
    move-object p1, v0

    goto :goto_1
.end method

.method private b(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    .line 350
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 353
    :try_start_0
    iget-object v0, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    const/4 v0, 0x0

    .line 361
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return-object v0

    .line 356
    :cond_0
    :try_start_1
    iget-object v0, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 357
    iget-object v1, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    .line 358
    iget-object v2, p0, LakH;->a:Ljava/util/Comparator;

    invoke-interface {v2, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-lez v2, :cond_1

    .line 361
    :goto_1
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 358
    goto :goto_1

    .line 361
    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public a()LakM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 272
    :try_start_0
    iget-object v0, p0, LakH;->a:Ljava/lang/Comparable;

    invoke-direct {p0, v0}, LakH;->a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 273
    if-nez v0, :cond_0

    .line 274
    const/4 v0, 0x0

    .line 280
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return-object v0

    .line 276
    :cond_0
    :try_start_1
    iget-object v1, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 277
    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)LakM;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 398
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 399
    iget-object v2, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 401
    :goto_0
    :try_start_0
    iget v2, p0, LakH;->b:I

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 402
    iget-object v2, p0, LakH;->a:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v0

    goto :goto_0

    .line 404
    :cond_0
    invoke-virtual {p0}, LakH;->b()LakM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 406
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public a(LakM;)LakM;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 246
    :try_start_0
    invoke-direct {p0, p1}, LakH;->b(LakM;)LakM;

    move-result-object v2

    .line 247
    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    invoke-interface {p1}, LakM;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 250
    iget-object v1, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 251
    iget-object v1, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 256
    :goto_0
    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 257
    iget v0, p0, LakH;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LakH;->b:I

    .line 258
    iget-wide v0, p0, LakH;->a:J

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    iput-wide v0, p0, LakH;->a:J

    .line 259
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_0
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v2

    .line 253
    :cond_1
    :try_start_1
    invoke-static {}, LbnG;->a()Ljava/util/LinkedList;

    move-result-object v1

    .line 254
    iget-object v3, p0, LakH;->a:Ljava/util/SortedMap;

    invoke-interface {v3, v0, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 263
    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    check-cast p1, LakM;

    invoke-virtual {p0, p1}, LakH;->a(LakM;)LakM;

    move-result-object v0

    return-object v0
.end method

.method public a(LakM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 412
    invoke-virtual {p0, p1}, LakH;->a(LakM;)Z

    .line 413
    return-void
.end method

.method public a(Ljava/lang/Comparable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 223
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 226
    :try_start_0
    iput-object p1, p0, LakH;->a:Ljava/lang/Comparable;

    .line 227
    iget-object v0, p0, LakH;->a:LakJ;

    iget-object v1, p0, LakH;->a:Ljava/lang/Comparable;

    invoke-interface {v0, v1}, LakJ;->a(Ljava/lang/Object;)Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, LakH;->a:Ljava/util/Comparator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 231
    return-void

    .line 229
    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public a(LakM;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 235
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    invoke-virtual {p0, p1}, LakH;->a(LakM;)LakM;

    move-result-object v0

    .line 237
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LakM;JLjava/util/concurrent/TimeUnit;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 392
    invoke-virtual {p0, p1}, LakH;->a(LakM;)Z

    move-result v0

    return v0
.end method

.method public b()LakM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 289
    :try_start_0
    iget-object v0, p0, LakH;->a:Ljava/lang/Comparable;

    invoke-direct {p0, v0}, LakH;->a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 290
    if-nez v0, :cond_0

    .line 291
    const/4 v0, 0x0

    .line 297
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_0
    return-object v0

    .line 293
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, LakH;->a(Ljava/lang/Comparable;)LakM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 297
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public c()LakM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 423
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 425
    :goto_0
    :try_start_0
    iget v0, p0, LakH;->b:I

    if-nez v0, :cond_0

    .line 426
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 430
    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 428
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LakH;->b()LakM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 430
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0
.end method

.method public drainTo(Ljava/util/Collection;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<-TE;>;)I"
        }
    .end annotation

    .prologue
    .line 367
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, LakH;->drainTo(Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public drainTo(Ljava/util/Collection;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<-TE;>;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 372
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 374
    if-ne p0, p1, :cond_1

    .line 375
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    .line 373
    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 379
    :try_start_0
    iget v0, p0, LakH;->b:I

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 380
    :goto_1
    if-ge v1, v2, :cond_2

    .line 381
    invoke-virtual {p0}, LakH;->b()LakM;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakM;

    .line 382
    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 386
    :cond_2
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 307
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 309
    :try_start_0
    new-instance v0, LakN;

    invoke-direct {v0, p0}, LakN;-><init>(LakH;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 39
    check-cast p1, LakM;

    invoke-virtual {p0, p1}, LakH;->a(LakM;)Z

    move-result v0

    return v0
.end method

.method public synthetic offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .locals 2

    .prologue
    .line 39
    check-cast p1, LakM;

    invoke-virtual {p0, p1, p2, p3, p4}, LakH;->a(LakM;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LakH;->a()LakM;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LakH;->b()LakM;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0, p1, p2, p3}, LakH;->a(JLjava/util/concurrent/TimeUnit;)LakM;

    move-result-object v0

    return-object v0
.end method

.method public synthetic put(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p1, LakM;

    invoke-virtual {p0, p1}, LakH;->a(LakM;)V

    return-void
.end method

.method public remainingCapacity()I
    .locals 2

    .prologue
    const v0, 0x7fffffff

    .line 417
    iget v1, p0, LakH;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LakH;->a:I

    invoke-virtual {p0}, LakH;->size()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 319
    :try_start_0
    iget v0, p0, LakH;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LakH;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public synthetic take()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LakH;->c()LakM;

    move-result-object v0

    return-object v0
.end method
