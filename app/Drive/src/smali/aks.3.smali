.class public Laks;
.super Ljava/lang/Object;
.source "BitmapUtilitiesImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/utils/BitmapUtilities;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    return-void
.end method

.method private static a(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 60
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 62
    :try_start_0
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 63
    if-nez v0, :cond_0

    .line 64
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Failed to decode image input stream."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;I)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 45
    invoke-static {p1}, Laks;->a(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 46
    invoke-static {v0}, Lakq;->a(Landroid/graphics/Bitmap;)Lakq;

    move-result-object v1

    .line 47
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 48
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 49
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 50
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    if-le v0, v2, :cond_1

    .line 51
    :cond_0
    invoke-virtual {v1, p2, p3}, Lakq;->a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;I)Lakq;

    .line 55
    :goto_0
    invoke-virtual {v1}, Lakq;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 53
    :cond_1
    invoke-virtual {v1, p2, p3}, Lakq;->b(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;I)Lakq;

    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 22
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 23
    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 25
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 26
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 27
    new-instance v2, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    return-object v2
.end method
