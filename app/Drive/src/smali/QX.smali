.class public final LQX;
.super Ljava/lang/Object;
.source "DocListCursor.java"


# instance fields
.field private final a:LCo;

.field private final a:LIK;

.field private final a:LIf;

.field private final a:LaFM;

.field private a:LaFX;

.field private final a:LaGT;

.field private final a:LaHc;

.field private final a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

.field private final a:Lcom/google/android/apps/docs/doclist/DocListQuery;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LzO;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LzO;


# direct methods
.method private constructor <init>(LaFX;Lcom/google/android/gms/drive/database/data/EntrySpec;LIf;LaFM;LIK;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LzO;Lcom/google/android/apps/docs/doclist/DocListQuery;LCo;LaGT;LbmY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFX;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LIf;",
            "LaFM;",
            "LIK;",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            "LzO;",
            "Lcom/google/android/apps/docs/doclist/DocListQuery;",
            "LCo;",
            "LaGT;",
            "LbmY",
            "<",
            "LzO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-eqz p10, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eq v1, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LbiT;->b(Z)V

    .line 113
    iput-object p1, p0, LQX;->a:LaFX;

    .line 114
    iput-object p3, p0, LQX;->a:LIf;

    .line 115
    iput-object p4, p0, LQX;->a:LaFM;

    .line 116
    iput-object p5, p0, LQX;->a:LIK;

    .line 117
    iput-object p6, p0, LQX;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 118
    iput-object p7, p0, LQX;->a:LzO;

    .line 119
    iput-object p8, p0, LQX;->a:Lcom/google/android/apps/docs/doclist/DocListQuery;

    .line 120
    iput-object p9, p0, LQX;->a:LCo;

    .line 121
    iput-object p10, p0, LQX;->a:LaGT;

    .line 122
    iput-object p11, p0, LQX;->a:Ljava/util/Set;

    .line 123
    if-eqz p1, :cond_4

    .line 124
    const-class v0, LaGA;

    invoke-virtual {p1, v0}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v0

    check-cast v0, LaGA;

    .line 125
    if-eqz v0, :cond_3

    instance-of v1, v0, LaHc;

    if-eqz v1, :cond_3

    .line 126
    invoke-static {v0}, LaHc;->a(LaGA;)LaHc;

    move-result-object v0

    iput-object v0, p0, LQX;->a:LaHc;

    .line 133
    :goto_3
    iput-object p2, p0, LQX;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 134
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 128
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LQX;->a:LaHc;

    goto :goto_3

    .line 131
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, LQX;->a:LaHc;

    goto :goto_3
.end method

.method public constructor <init>(LaFX;Lcom/google/android/gms/drive/database/data/EntrySpec;LIf;LaFM;LIK;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LzO;Lcom/google/android/apps/docs/doclist/DocListQuery;LCo;LbmY;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFX;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LIf;",
            "LaFM;",
            "LIK;",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            "LzO;",
            "Lcom/google/android/apps/docs/doclist/DocListQuery;",
            "LCo;",
            "LbmY",
            "<",
            "LzO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, LQX;-><init>(LaFX;Lcom/google/android/gms/drive/database/data/EntrySpec;LIf;LaFM;LIK;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LzO;Lcom/google/android/apps/docs/doclist/DocListQuery;LCo;LaGT;LbmY;)V

    .line 87
    return-void
.end method

.method public constructor <init>(LaGT;LIf;LaFM;LIK;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LzO;Lcom/google/android/apps/docs/doclist/DocListQuery;LCo;LbmY;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGT;",
            "LIf;",
            "LaFM;",
            "LIK;",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            "LzO;",
            "Lcom/google/android/apps/docs/doclist/DocListQuery;",
            "LCo;",
            "LbmY",
            "<",
            "LzO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object v10, p1

    move-object/from16 v11, p9

    invoke-direct/range {v0 .. v11}, LQX;-><init>(LaFX;Lcom/google/android/gms/drive/database/data/EntrySpec;LIf;LaFM;LIK;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LzO;Lcom/google/android/apps/docs/doclist/DocListQuery;LCo;LaGT;LbmY;)V

    .line 97
    return-void
.end method


# virtual methods
.method public a()LCl;
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, LQX;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v1, p0, LQX;->a:LCo;

    invoke-static {v0, v1}, LQY;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LCo;)LCl;

    move-result-object v0

    return-object v0
.end method

.method public a()LIK;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, LQX;->a:LIK;

    return-object v0
.end method

.method public a()LIf;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, LQX;->a:LIf;

    return-object v0
.end method

.method public a()LaFM;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, LQX;->a:LaFM;

    return-object v0
.end method

.method public a()LaFX;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, LQX;->a:LaFX;

    return-object v0
.end method

.method public a()LaGT;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, LQX;->a:LaGT;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, LQX;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/doclist/DocListQuery;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, LQX;->a:Lcom/google/android/apps/docs/doclist/DocListQuery;

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, LQX;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()LzO;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, LQX;->a:LzO;

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, LQX;->a:LaHc;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, LQX;->a:LaFX;

    const-class v1, LaGA;

    iget-object v2, p0, LQX;->a:LaHc;

    invoke-static {v2}, LaHc;->a(LaGA;)LaHc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaFX;->b(Ljava/lang/Class;LaFW;)LaFX;

    move-result-object v0

    iput-object v0, p0, LQX;->a:LaFX;

    .line 184
    :cond_0
    return-void
.end method

.method public a(LzO;)Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, LQX;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQX;->a:LzO;

    invoke-virtual {v0, p1}, LzO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, LQX;->a:LaHc;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 188
    iget-object v0, p0, LQX;->a:LaHc;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, LQX;->a:LaHc;

    invoke-virtual {v0}, LaHc;->a()V

    .line 191
    :cond_0
    return-void

    .line 187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
