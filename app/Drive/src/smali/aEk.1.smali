.class public final enum LaEk;
.super Ljava/lang/Enum;
.source "AppCacheTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEk;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEk;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic a:[LaEk;

.field public static final enum b:LaEk;

.field public static final enum c:LaEk;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum d:LaEk;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum e:LaEk;

.field public static final enum f:LaEk;

.field public static final enum g:LaEk;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/16 v6, 0xe

    const/16 v9, 0x3f

    const/4 v8, 0x0

    .line 43
    new-instance v0, LaEk;

    const-string v1, "LEGACY_APP_NAME"

    .line 44
    invoke-static {}, LaEj;->b()LaEj;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "appName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 45
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 46
    invoke-virtual {v2, v9}, LaFt;->b(I)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "appName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 47
    invoke-virtual {v2, v9, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEk;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEk;->a:LaEk;

    .line 49
    new-instance v0, LaEk;

    const-string v1, "APP_VERSION"

    invoke-static {}, LaEj;->b()LaEj;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "appVersion"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 50
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEk;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEk;->b:LaEk;

    .line 52
    new-instance v0, LaEk;

    const-string v1, "LEGACY_MANIFEST_ETAG"

    .line 53
    invoke-static {}, LaEj;->b()LaEj;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "manifestETag"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 54
    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEk;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEk;->c:LaEk;

    .line 56
    new-instance v0, LaEk;

    const-string v1, "LEGACY_EXPIRY_DATE"

    const/4 v2, 0x3

    .line 57
    invoke-static {}, LaEj;->b()LaEj;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x17

    new-instance v5, LaFG;

    const-string v6, "expiryDate"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 58
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 59
    invoke-virtual {v3, v9}, LaFt;->b(I)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "expiryDate"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 60
    invoke-virtual {v3, v9, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEk;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEk;->d:LaEk;

    .line 65
    new-instance v0, LaEk;

    const-string v1, "APP_FLAGS"

    const/4 v2, 0x4

    invoke-static {}, LaEj;->b()LaEj;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x36

    new-instance v5, LaFG;

    const-string v6, "appFlags"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 66
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEk;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEk;->e:LaEk;

    .line 71
    new-instance v0, LaEk;

    const-string v1, "ADDITIONAL_DATA"

    const/4 v2, 0x5

    invoke-static {}, LaEj;->b()LaEj;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x4a

    new-instance v5, LaFG;

    const-string v6, "additionalData"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 72
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEk;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEk;->f:LaEk;

    .line 77
    new-instance v0, LaEk;

    const-string v1, "MINIMUM_APP_VERSION"

    const/4 v2, 0x6

    invoke-static {}, LaEj;->b()LaEj;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x58

    new-instance v5, LaFG;

    const-string v6, "minimumAppVersion"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 79
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 78
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEk;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEk;->g:LaEk;

    .line 42
    const/4 v0, 0x7

    new-array v0, v0, [LaEk;

    sget-object v1, LaEk;->a:LaEk;

    aput-object v1, v0, v8

    sget-object v1, LaEk;->b:LaEk;

    aput-object v1, v0, v10

    sget-object v1, LaEk;->c:LaEk;

    aput-object v1, v0, v11

    const/4 v1, 0x3

    sget-object v2, LaEk;->d:LaEk;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaEk;->e:LaEk;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaEk;->f:LaEk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaEk;->g:LaEk;

    aput-object v2, v0, v1

    sput-object v0, LaEk;->a:[LaEk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEk;->a:LaFr;

    .line 86
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEk;
    .locals 1

    .prologue
    .line 42
    const-class v0, LaEk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEk;

    return-object v0
.end method

.method public static values()[LaEk;
    .locals 1

    .prologue
    .line 42
    sget-object v0, LaEk;->a:[LaEk;

    invoke-virtual {v0}, [LaEk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEk;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LaEk;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, LaEk;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
