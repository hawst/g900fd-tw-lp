.class final Laxl;
.super Ljava/lang/Object;
.source "Range.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Laxl;-><init>(II)V

    .line 20
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Laxl;->a:I

    .line 15
    iput p2, p0, Laxl;->b:I

    .line 16
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Laxl;->b:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Laxl;->a:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(ILaxl;)Laxl;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Laxl;

    iget v1, p0, Laxl;->a:I

    sub-int/2addr v1, p1

    iget v2, p2, Laxl;->a:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Laxl;->b:I

    add-int/2addr v2, p1

    iget v3, p2, Laxl;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-direct {v0, v1, v2}, Laxl;-><init>(II)V

    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 45
    iget v0, p0, Laxl;->b:I

    iget v1, p0, Laxl;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Laxl;->a:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Laxl;->b:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Laxl;)Z
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p1}, Laxl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Laxl;->a:I

    iget v1, p1, Laxl;->a:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Laxl;->b:I

    iget v1, p1, Laxl;->b:I

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Laxl;)[Laxl;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 86
    invoke-virtual {p1, p0}, Laxl;->a(Laxl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    new-array v0, v4, [Laxl;

    .line 98
    :goto_0
    return-object v0

    .line 89
    :cond_0
    iget v0, p1, Laxl;->a:I

    iget v2, p0, Laxl;->a:I

    if-gt v0, v2, :cond_1

    move-object v0, v1

    .line 90
    :goto_1
    iget v2, p1, Laxl;->b:I

    iget v3, p0, Laxl;->b:I

    if-lt v2, v3, :cond_2

    move-object v2, v1

    .line 91
    :goto_2
    if-eqz v0, :cond_4

    .line 92
    if-eqz v2, :cond_3

    .line 93
    const/4 v1, 0x2

    new-array v1, v1, [Laxl;

    aput-object v0, v1, v4

    aput-object v2, v1, v5

    move-object v0, v1

    goto :goto_0

    .line 89
    :cond_1
    new-instance v0, Laxl;

    iget v2, p0, Laxl;->a:I

    iget v3, p1, Laxl;->a:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {v0, v2, v3}, Laxl;-><init>(II)V

    goto :goto_1

    .line 90
    :cond_2
    new-instance v1, Laxl;

    iget v2, p1, Laxl;->b:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Laxl;->b:I

    invoke-direct {v1, v2, v3}, Laxl;-><init>(II)V

    move-object v2, v1

    goto :goto_2

    .line 95
    :cond_3
    new-array v1, v5, [Laxl;

    aput-object v0, v1, v4

    move-object v0, v1

    goto :goto_0

    .line 98
    :cond_4
    new-array v0, v5, [Laxl;

    aput-object v2, v0, v4

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24
    if-ne p0, p1, :cond_1

    .line 31
    :cond_0
    :goto_0
    return v0

    .line 27
    :cond_1
    instance-of v2, p1, Laxl;

    if-nez v2, :cond_2

    move v0, v1

    .line 28
    goto :goto_0

    .line 30
    :cond_2
    check-cast p1, Laxl;

    .line 31
    iget v2, p0, Laxl;->a:I

    iget v3, p1, Laxl;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Laxl;->b:I

    iget v3, p1, Laxl;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 36
    iget v0, p0, Laxl;->a:I

    mul-int/lit16 v0, v0, 0x3df

    iget v1, p0, Laxl;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Laxm;

    invoke-direct {v0, p0}, Laxm;-><init>(Laxl;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 41
    const-string v0, "Range [%d, %d]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Laxl;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Laxl;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
