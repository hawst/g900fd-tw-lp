.class public final LbmH;
.super Lbmx;
.source "ImmutableList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbmx",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private a:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 652
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LbmH;-><init>(I)V

    .line 653
    return-void
.end method

.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 656
    invoke-direct {p0}, Lbmx;-><init>()V

    .line 657
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, LbmH;->a:[Ljava/lang/Object;

    .line 658
    const/4 v0, 0x0

    iput v0, p0, LbmH;->a:I

    .line 659
    return-void
.end method


# virtual methods
.method public a()LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 745
    iget v0, p0, LbmH;->a:I

    packed-switch v0, :pswitch_data_0

    .line 754
    iget v0, p0, LbmH;->a:I

    iget-object v1, p0, LbmH;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 756
    new-instance v0, LbpA;

    iget-object v1, p0, LbmH;->a:[Ljava/lang/Object;

    invoke-direct {v0, v1}, LbpA;-><init>([Ljava/lang/Object;)V

    .line 758
    :goto_0
    return-object v0

    .line 747
    :pswitch_0
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    goto :goto_0

    .line 751
    :pswitch_1
    iget-object v0, p0, LbmH;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 752
    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    goto :goto_0

    .line 758
    :cond_0
    new-instance v0, LbpA;

    iget-object v1, p0, LbmH;->a:[Ljava/lang/Object;

    iget v2, p0, LbmH;->a:I

    invoke-static {v1, v2}, Lbpv;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, LbpA;-><init>([Ljava/lang/Object;)V

    goto :goto_0

    .line 745
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(I)LbmH;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LbmH",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 666
    iget-object v0, p0, LbmH;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 667
    iget-object v0, p0, LbmH;->a:[Ljava/lang/Object;

    iget-object v1, p0, LbmH;->a:[Ljava/lang/Object;

    array-length v1, v1

    invoke-static {v1, p1}, LbmH;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lbpv;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LbmH;->a:[Ljava/lang/Object;

    .line 670
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Iterable;)LbmH;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LbmH",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 698
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 699
    check-cast v0, Ljava/util/Collection;

    .line 700
    iget v1, p0, LbmH;->a:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LbmH;->a(I)LbmH;

    .line 702
    :cond_0
    invoke-super {p0, p1}, Lbmx;->a(Ljava/lang/Iterable;)Lbmx;

    .line 703
    return-object p0
.end method

.method public a(Ljava/lang/Object;)LbmH;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LbmH",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 682
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 683
    iget v0, p0, LbmH;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, LbmH;->a(I)LbmH;

    .line 684
    iget-object v0, p0, LbmH;->a:[Ljava/lang/Object;

    iget v1, p0, LbmH;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LbmH;->a:I

    aput-object p1, v0, v1

    .line 685
    return-object p0
.end method

.method public a(Ljava/util/Iterator;)LbmH;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LbmH",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 735
    invoke-super {p0, p1}, Lbmx;->a(Ljava/util/Iterator;)Lbmx;

    .line 736
    return-object p0
.end method

.method public varargs a([Ljava/lang/Object;)LbmH;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "LbmH",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 716
    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 717
    aget-object v2, p1, v0

    invoke-static {v2, v0}, Lbpv;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 716
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 719
    :cond_0
    iget v0, p0, LbmH;->a:I

    array-length v2, p1

    add-int/2addr v0, v2

    invoke-virtual {p0, v0}, LbmH;->a(I)LbmH;

    .line 720
    iget-object v0, p0, LbmH;->a:[Ljava/lang/Object;

    iget v2, p0, LbmH;->a:I

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 721
    iget v0, p0, LbmH;->a:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, LbmH;->a:I

    .line 722
    return-object p0
.end method

.method public bridge synthetic a(Ljava/lang/Iterable;)Lbmx;
    .locals 1

    .prologue
    .line 643
    invoke-virtual {p0, p1}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Lbmx;
    .locals 1

    .prologue
    .line 643
    invoke-virtual {p0, p1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/util/Iterator;)Lbmx;
    .locals 1

    .prologue
    .line 643
    invoke-virtual {p0, p1}, LbmH;->a(Ljava/util/Iterator;)LbmH;

    move-result-object v0

    return-object v0
.end method
