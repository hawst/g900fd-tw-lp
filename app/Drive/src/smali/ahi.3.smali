.class public Lahi;
.super Ljava/lang/Object;
.source "SyncModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 152
    const-class v0, Lafw;

    const-class v1, LQR;

    .line 153
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 152
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 154
    const-class v0, Lahh;

    const-class v1, LQR;

    .line 155
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 154
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 156
    const-class v0, Lahj;

    const-class v1, LQJ;

    .line 157
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 156
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 158
    const-class v0, LagR;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 159
    const-class v0, LafF;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 160
    const-class v0, LacS;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 161
    return-void
.end method

.method provideContentSyncServiceController(Lage;)Lagd;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 48
    return-object p1
.end method

.method provideDocEntrySynchronizer(Lagm;)Lagl;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 99
    return-object p1
.end method

.method provideExecutor()Ljava/util/concurrent/Executor;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 118
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    return-object v0
.end method

.method provideFeedProcessorDriverFactory(LagT;)LagS;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 87
    return-object p1
.end method

.method provideFeedProcessorFactory(LagF;)LagE;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 93
    return-object p1
.end method

.method provideFeedSelector(Lafb;)Lafa;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 80
    return-object p1
.end method

.method provideHelper(LagA;)LagD;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 105
    return-object p1
.end method

.method provideResultsPageProducerFactory(LagM;)Lagz;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 112
    return-object p1
.end method

.method provideSingleDocSynchronizer(Laha;)LagZ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 55
    return-object p1
.end method

.method provideSyncAuthority(Lahg;)Lahf;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 62
    return-object p1
.end method

.method provideSyncManager(LbiP;LagO;)Lahh;
    .locals 1
    .param p1    # LbiP;
        .annotation runtime LQR;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "Lahh;",
            ">;",
            "LagO;",
            ")",
            "Lahh;"
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p1, p2}, LbiP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahh;

    return-object v0
.end method

.method provideSyncMoreController(LbiP;LaeV;)Lafw;
    .locals 1
    .param p1    # LbiP;
        .annotation runtime LQR;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "Lafw;",
            ">;",
            "LaeV;",
            ")",
            "Lafw;"
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p1, p2}, LbiP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    return-object v0
.end method

.method provideSyncMoreFactory(LafA;)Lafz;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 74
    return-object p1
.end method

.method provideSyncScheduler(Lahk;)Lahj;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p1}, Lahk;->a()Lahj;

    move-result-object v0

    return-object v0
.end method

.method provideSyncableFactory(LafP;)Lahn;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 125
    return-object p1
.end method

.method provideVideoUrlFetcher(Laht;)Lahr;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 131
    return-object p1
.end method
