.class public final enum Larj;
.super Ljava/lang/Enum;
.source "ActionBarModeSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Larj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Larj;

.field private static final synthetic a:[Larj;

.field public static final enum b:Larj;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Larj;

    const-string v1, "ACTION_BAR"

    invoke-direct {v0, v1, v2}, Larj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Larj;->a:Larj;

    .line 20
    new-instance v0, Larj;

    const-string v1, "SEARCH_BAR"

    invoke-direct {v0, v1, v3}, Larj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Larj;->b:Larj;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Larj;

    sget-object v1, Larj;->a:Larj;

    aput-object v1, v0, v2

    sget-object v1, Larj;->b:Larj;

    aput-object v1, v0, v3

    sput-object v0, Larj;->a:[Larj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Larj;
    .locals 1

    .prologue
    .line 18
    const-class v0, Larj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Larj;

    return-object v0
.end method

.method public static values()[Larj;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Larj;->a:[Larj;

    invoke-virtual {v0}, [Larj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Larj;

    return-object v0
.end method
