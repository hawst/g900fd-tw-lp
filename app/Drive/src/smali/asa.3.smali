.class public Lasa;
.super Ljava/lang/Object;
.source "HighlightFeaturesConfig.java"


# instance fields
.field private final a:I

.field private final a:LbpT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpT",
            "<",
            "Ljava/lang/Integer;",
            "Lrg;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z


# direct methods
.method private constructor <init>(ILbpT;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LbpT",
            "<",
            "Ljava/lang/Integer;",
            "+",
            "Lrg;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "currentVersion %s > 0: "

    new-array v1, v1, [Ljava/lang/Object;

    .line 35
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    .line 34
    invoke-static {v0, v3, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iput p1, p0, Lasa;->a:I

    .line 37
    invoke-static {p2}, Lbnb;->a(Lbph;)Lbnb;

    move-result-object v0

    iput-object v0, p0, Lasa;->a:LbpT;

    .line 38
    iput-boolean p3, p0, Lasa;->a:Z

    .line 39
    return-void

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0
.end method

.method public static a(ILbpT;)Lasa;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LbpT",
            "<",
            "Ljava/lang/Integer;",
            "+",
            "Lrg;",
            ">;)",
            "Lasa;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lasa;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lasa;-><init>(ILbpT;Z)V

    return-object v0
.end method

.method public static b(ILbpT;)Lasa;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LbpT",
            "<",
            "Ljava/lang/Integer;",
            "+",
            "Lrg;",
            ">;)",
            "Lasa;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lasa;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lasa;-><init>(ILbpT;Z)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lasa;->a:I

    return v0
.end method

.method public a(I)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Set",
            "<",
            "Lrg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lasa;->a:LbpT;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, LbpT;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lasa;->a:Z

    return v0
.end method
