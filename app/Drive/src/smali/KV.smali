.class public LKV;
.super Ljava/lang/Object;
.source "MoveDetector.java"


# instance fields
.field private a:D

.field private a:F

.field private a:LKX;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LKW;",
            ">;"
        }
    .end annotation
.end field

.field private b:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 29
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LKV;->a:Ljava/util/Set;

    .line 38
    sget-object v0, LKX;->a:LKX;

    iput-object v0, p0, LKV;->a:LKX;

    .line 42
    const-wide/high16 v0, 0x4099000000000000L    # 1600.0

    iput-wide v0, p0, LKV;->a:D

    return-void
.end method

.method private static a(FFFF)D
    .locals 4

    .prologue
    .line 110
    sub-float v0, p0, p2

    float-to-double v0, v0

    .line 111
    sub-float v2, p1, p3

    float-to-double v2, v2

    .line 112
    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, LKV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x0

    .line 106
    :goto_0
    return v0

    .line 102
    :cond_0
    iget-object v0, p0, LKV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKW;

    .line 103
    invoke-interface {v0, p1}, LKW;->a(Landroid/view/View;)V

    goto :goto_1

    .line 106
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(LKW;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LKV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 55
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    .line 56
    if-eq v2, v1, :cond_0

    .line 58
    sget-object v1, LKX;->a:LKX;

    iput-object v1, p0, LKV;->a:LKX;

    .line 94
    :goto_0
    return v0

    .line 62
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 64
    :pswitch_0
    sget-object v2, LKX;->b:LKX;

    iput-object v2, p0, LKV;->a:LKX;

    .line 65
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iput v2, p0, LKV;->a:F

    .line 66
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LKV;->b:F

    .line 67
    const-string v0, "MoveDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_DOWN at X="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LKV;->a:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LKV;->b:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 68
    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v1, p0, LKV;->a:LKX;

    sget-object v2, LKX;->b:LKX;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, LKV;->a:LKX;

    sget-object v2, LKX;->c:LKX;

    if-eq v1, v2, :cond_1

    .line 72
    const-string v1, "MoveDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_MOVE in wrong state, state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LKV;->a:LKX;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 75
    :cond_1
    sget-object v1, LKX;->c:LKX;

    iput-object v1, p0, LKV;->a:LKX;

    .line 76
    iget v1, p0, LKV;->a:F

    iget v2, p0, LKV;->b:F

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-static {v1, v2, v3, v4}, LKV;->a(FFFF)D

    move-result-wide v2

    .line 77
    const-string v1, "MoveDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_MOVE moveDistance="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const-wide/high16 v4, 0x4099000000000000L    # 1600.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_2

    .line 79
    const-string v0, "MoveDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_MOVE moved beyond threshold - moveDistance="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-direct {p0, p1}, LKV;->a(Landroid/view/View;)Z

    move-result v0

    goto/16 :goto_0

    .line 82
    :cond_2
    const-string v1, "MoveDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_MOVE still under threshold - moveDistance="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 86
    :pswitch_2
    sget-object v1, LKX;->a:LKX;

    iput-object v1, p0, LKV;->a:LKX;

    goto/16 :goto_0

    .line 90
    :pswitch_3
    sget-object v1, LKX;->a:LKX;

    iput-object v1, p0, LKV;->a:LKX;

    goto/16 :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
