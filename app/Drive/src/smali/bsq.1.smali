.class public final Lbsq;
.super Ljava/lang/Object;
.source "DoubleMath.java"


# static fields
.field private static final a:D

.field static final a:[D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 210
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lbsq;->a:D

    .line 300
    const/16 v0, 0xb

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lbsq;->a:[D

    return-void

    :array_0
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x42b3077775800000L    # 2.0922789888E13
        0x474956ad0aae33a4L    # 2.631308369336935E35
        0x4c9ee69a78d72cb6L    # 1.2413915592536073E61
        0x526fe478ee34844aL    # 1.2688693218588417E89
        0x589c619094edabffL    # 7.156945704626381E118
        0x5f13638dd7bd6347L    # 9.916779348709496E149
        0x65c7cac197cfe503L    # 1.974506857221074E182
        0x6cb1e5dfc140e1e5L    # 3.856204823625804E215
        0x73c8ce85fadb707eL    # 5.5502938327393044E249
        0x7b095d5f3d928edeL    # 4.7147236359920616E284
    .end array-data
.end method

.method public static a(DLjava/math/RoundingMode;)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 223
    const-wide/16 v4, 0x0

    cmpl-double v0, p0, v4

    if-lez v0, :cond_0

    invoke-static {p0, p1}, Lbss;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "x must be positive and finite"

    invoke-static {v0, v3}, LbiT;->a(ZLjava/lang/Object;)V

    .line 224
    invoke-static {p0, p1}, Lbss;->a(D)I

    move-result v3

    .line 225
    invoke-static {p0, p1}, Lbss;->b(D)Z

    move-result v0

    if-nez v0, :cond_1

    .line 226
    const-wide/high16 v0, 0x4330000000000000L    # 4.503599627370496E15

    mul-double/2addr v0, p0

    invoke-static {v0, v1, p2}, Lbsq;->a(DLjava/math/RoundingMode;)I

    move-result v0

    add-int/lit8 v0, v0, -0x34

    .line 258
    :goto_1
    return v0

    :cond_0
    move v0, v2

    .line 223
    goto :goto_0

    .line 231
    :cond_1
    sget-object v0, Lbsr;->a:[I

    invoke-virtual {p2}, Ljava/math/RoundingMode;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 256
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 233
    :pswitch_0
    invoke-static {p0, p1}, Lbsq;->a(D)Z

    move-result v0

    invoke-static {v0}, Lbsv;->a(Z)V

    :pswitch_1
    move v1, v2

    .line 258
    :cond_2
    :goto_2
    if-eqz v1, :cond_7

    add-int/lit8 v0, v3, 0x1

    goto :goto_1

    .line 239
    :pswitch_2
    invoke-static {p0, p1}, Lbsq;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v2

    goto :goto_2

    .line 242
    :pswitch_3
    if-gez v3, :cond_3

    move v0, v1

    :goto_3
    invoke-static {p0, p1}, Lbsq;->a(D)Z

    move-result v4

    if-nez v4, :cond_4

    :goto_4
    and-int/2addr v1, v0

    .line 243
    goto :goto_2

    :cond_3
    move v0, v2

    .line 242
    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4

    .line 245
    :pswitch_4
    if-ltz v3, :cond_5

    move v0, v1

    :goto_5
    invoke-static {p0, p1}, Lbsq;->a(D)Z

    move-result v4

    if-nez v4, :cond_6

    :goto_6
    and-int/2addr v1, v0

    .line 246
    goto :goto_2

    :cond_5
    move v0, v2

    .line 245
    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6

    .line 250
    :pswitch_5
    invoke-static {p0, p1}, Lbss;->a(D)D

    move-result-wide v4

    .line 253
    mul-double/2addr v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    cmpl-double v0, v4, v6

    if-gtz v0, :cond_2

    move v1, v2

    goto :goto_2

    :cond_7
    move v0, v3

    .line 258
    goto :goto_1

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static a(D)Z
    .locals 2

    .prologue
    .line 188
    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_0

    invoke-static {p0, p1}, Lbss;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lbss;->a(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Lbsu;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
