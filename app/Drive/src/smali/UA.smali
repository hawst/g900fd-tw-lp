.class public LUA;
.super LUP;
.source "DocumentViewerUrlOpenerActivityIntentFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    invoke-direct {p0, v0}, LUP;-><init>(Ljava/lang/Class;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;LaFO;LaGu;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 34
    const-string v0, "userstoinvite"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38
    if-eqz p4, :cond_2

    sget-object v0, LaGn;->b:LaGn;

    invoke-interface {p4}, LaGu;->a()LaGn;

    move-result-object v2

    invoke-virtual {v0, v2}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    .line 39
    invoke-interface {p4}, LaGu;->j()Z

    move-result v0

    if-nez v0, :cond_2

    .line 40
    :cond_0
    invoke-interface {p4}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 41
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-static {p1, p4}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/content/Context;LaGu;)Landroid/content/Intent;

    move-result-object v0

    .line 43
    if-eqz v1, :cond_1

    .line 44
    const-string v2, "usersToInvite"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-super/range {p0 .. p5}, LUP;->a(Landroid/content/Context;Landroid/net/Uri;LaFO;LaGu;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
