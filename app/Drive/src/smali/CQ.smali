.class LCQ;
.super Ljava/lang/Object;
.source "SearchSuggestionRestrictionHelperImpl.java"

# interfaces
.implements LCP;


# instance fields
.field private final a:Lrm;

.field private final a:LsC;

.field private final a:LvL;


# direct methods
.method constructor <init>(Lrm;LsC;LvL;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, LCQ;->a:Lrm;

    .line 24
    iput-object p2, p0, LCQ;->a:LsC;

    .line 25
    iput-object p3, p0, LCQ;->a:LvL;

    .line 26
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, LCQ;->a:Lrm;

    invoke-virtual {v0}, Lrm;->a()LaFO;

    move-result-object v0

    .line 31
    new-instance v1, LvN;

    invoke-direct {v1}, LvN;-><init>()V

    .line 32
    iget-object v2, p0, LCQ;->a:LsC;

    invoke-interface {v2}, LsC;->a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v2

    .line 33
    iget-object v3, p0, LCQ;->a:LvL;

    invoke-interface {v3, v0}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 34
    iget-object v0, p0, LCQ;->a:LvL;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a(LvL;LvN;)LvN;

    .line 38
    iget-object v0, p0, LCQ;->a:Lrm;

    invoke-virtual {v1}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Landroid/content/Context;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 39
    return-void
.end method
