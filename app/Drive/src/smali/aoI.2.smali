.class LaoI;
.super Ljava/lang/Object;
.source "PreviewImageFetcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGo;

.field final synthetic a:LaoH;

.field final synthetic a:Laou;

.field final synthetic a:Lcom/google/android/apps/docs/utils/FetchSpec;


# direct methods
.method constructor <init>(LaoH;LaGo;Lcom/google/android/apps/docs/utils/FetchSpec;Laou;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, LaoI;->a:LaoH;

    iput-object p2, p0, LaoI;->a:LaGo;

    iput-object p3, p0, LaoI;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    iput-object p4, p0, LaoI;->a:Laou;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()LakD;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, LaoI;->a:LaGo;

    invoke-interface {v0}, LaGo;->a()LaGv;

    move-result-object v0

    .line 144
    iget-object v1, p0, LaoI;->a:LaGo;

    invoke-interface {v1}, LaGo;->a()LaGn;

    move-result-object v1

    .line 145
    sget-object v2, LaGn;->b:LaGn;

    invoke-virtual {v2, v1}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v0, p0, LaoI;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    .line 147
    iget-object v1, p0, LaoI;->a:LaoH;

    iget-object v2, p0, LaoI;->a:LaGo;

    invoke-static {v1, v2, v0}, LaoH;->a(LaoH;LaGo;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)LakD;

    move-result-object v0

    .line 148
    iget-object v1, p0, LaoI;->a:Laou;

    invoke-virtual {v1, v0}, Laou;->a(Ljava/lang/Object;)V

    .line 149
    return-object v0

    .line 151
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Creating local preview is unsupported for document kind: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, LaoI;->a()LakD;

    move-result-object v0

    return-object v0
.end method
