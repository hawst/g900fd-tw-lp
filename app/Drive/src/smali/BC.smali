.class public LBC;
.super Ljava/lang/Object;
.source "DocListRowSectionIndexer.java"


# instance fields
.field private final a:I

.field private final a:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LIB;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private final b:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LBE;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private c:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LCx;I)V
    .locals 3

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    invoke-interface {p1}, LCx;->a()I

    move-result v0

    iput v0, p0, LBC;->a:I

    .line 123
    iput p2, p0, LBC;->c:I

    .line 124
    invoke-direct {p0, p1}, LBC;->a(LCx;)Lbng;

    move-result-object v0

    iput-object v0, p0, LBC;->a:Lbng;

    .line 125
    invoke-direct {p0}, LBC;->a()Lbng;

    move-result-object v0

    iput-object v0, p0, LBC;->b:Lbng;

    .line 126
    iget-object v0, p0, LBC;->b:Lbng;

    invoke-virtual {v0}, Lbng;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x0

    iput v0, p0, LBC;->b:I

    .line 133
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, LBC;->b:Lbng;

    invoke-virtual {v0}, Lbng;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 130
    iget-object v0, p0, LBC;->b:Lbng;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbng;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBE;

    .line 131
    iget v0, v0, LBE;->b:I

    invoke-direct {p0, v0}, LBC;->b(I)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LBC;->b:I

    goto :goto_0
.end method

.method private a()Lbng;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LBE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    const/4 v1, 0x0

    .line 167
    invoke-static {}, Lbng;->a()Lbni;

    move-result-object v6

    .line 169
    invoke-static {}, Lbng;->a()Lbni;

    move-result-object v7

    .line 171
    iget-object v0, p0, LBC;->a:Lbng;

    invoke-virtual {v0}, Lbng;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, LBC;->a:Lbng;

    invoke-virtual {v0}, Lbng;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 173
    iget-object v0, p0, LBC;->a:Lbng;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbng;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIB;

    .line 175
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Lbni;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbni;

    .line 177
    iget-object v3, p0, LBC;->a:Lbng;

    add-int/lit8 v4, v2, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbng;->b(Ljava/lang/Object;)Lbng;

    move-result-object v3

    invoke-virtual {v3}, Lbng;->a()LbmY;

    move-result-object v3

    invoke-virtual {v3}, LbmY;->a()Lbqv;

    move-result-object v8

    move-object v3, v0

    move v4, v2

    move v2, v1

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 178
    add-int/lit8 v9, v1, 0x1

    .line 179
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 180
    sub-int v10, v5, v4

    .line 181
    invoke-direct {p0, v10}, LBC;->b(I)I

    move-result v1

    add-int/2addr v1, v9

    .line 182
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIB;

    .line 184
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v4, v10}, LBE;->a(LIB;II)LBE;

    move-result-object v3

    .line 183
    invoke-virtual {v6, v2, v3}, Lbni;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbni;

    .line 188
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Lbni;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbni;

    move v2, v1

    move-object v3, v0

    move v4, v5

    .line 189
    goto :goto_0

    .line 190
    :cond_0
    iget v0, p0, LBC;->a:I

    sub-int/2addr v0, v4

    .line 192
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v4, v0}, LBE;->a(LIB;II)LBE;

    move-result-object v3

    .line 191
    invoke-virtual {v6, v2, v3}, Lbni;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbni;

    .line 194
    add-int/lit8 v1, v1, 0x1

    .line 195
    invoke-direct {p0, v0}, LBC;->b(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 198
    :cond_1
    invoke-virtual {v7}, Lbni;->a()Lbng;

    move-result-object v0

    iput-object v0, p0, LBC;->c:Lbng;

    .line 199
    invoke-virtual {v6}, Lbni;->a()Lbng;

    move-result-object v0

    return-object v0
.end method

.method private a(LCx;)Lbng;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCx;",
            ")",
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LIB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    invoke-static {}, Lbng;->a()Lbni;

    move-result-object v1

    .line 145
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, LCx;->a()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 150
    invoke-interface {p1, v0}, LCx;->a(I)LIB;

    move-result-object v2

    .line 151
    invoke-interface {v2}, LIB;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Lbni;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbni;

    .line 145
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {v1}, Lbni;->a()Lbng;

    move-result-object v0

    return-object v0
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 206
    iget v0, p0, LBC;->c:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, LBC;->c:I

    div-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 249
    iget v0, p0, LBC;->b:I

    return v0
.end method

.method public a(I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 237
    iget-object v0, p0, LBC;->c:Lbng;

    invoke-virtual {v0}, Lbng;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 238
    iget-object v0, p0, LBC;->c:Lbng;

    invoke-virtual {v0}, Lbng;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 240
    iget-object v0, p0, LBC;->c:Lbng;

    add-int/lit8 v1, p1, 0x1

    .line 241
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbng;->a(Ljava/lang/Object;)Lbng;

    move-result-object v1

    .line 242
    invoke-interface {v1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 243
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 244
    sub-int v1, p1, v2

    iget v2, p0, LBC;->c:I

    div-int/2addr v1, v2

    .line 245
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    move v0, v2

    .line 237
    goto :goto_0

    :cond_1
    move v1, v2

    .line 238
    goto :goto_1
.end method

.method public a(I)LBF;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 216
    iget v0, p0, LBC;->b:I

    invoke-static {p1, v0}, LbiT;->a(II)I

    .line 217
    iget-object v0, p0, LBC;->b:Lbng;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbng;->a(Ljava/lang/Object;)Lbng;

    move-result-object v1

    .line 218
    invoke-interface {v1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 219
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBE;

    .line 220
    if-ne v2, p1, :cond_0

    .line 221
    new-instance v1, LBF;

    iget-object v2, v0, LBE;->a:LIB;

    iget v0, v0, LBE;->a:I

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3, v5}, LBF;-><init>(LIB;IILBD;)V

    move-object v0, v1

    .line 227
    :goto_0
    return-object v0

    .line 223
    :cond_0
    sub-int v1, p1, v2

    add-int/lit8 v1, v1, -0x1

    .line 224
    iget v2, p0, LBC;->c:I

    mul-int/2addr v2, v1

    .line 225
    iget v1, v0, LBE;->b:I

    sub-int/2addr v1, v2

    .line 226
    iget v3, p0, LBC;->c:I

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 227
    new-instance v1, LBF;

    iget-object v4, v0, LBE;->a:LIB;

    iget v0, v0, LBE;->a:I

    add-int/2addr v0, v2

    invoke-direct {v1, v4, v0, v3, v5}, LBF;-><init>(LIB;IILBD;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, LBC;->b:Lbng;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbng;->a(Ljava/lang/Object;)Lbng;

    move-result-object v0

    .line 212
    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
