.class public Lail;
.super Ljava/io/OutputStream;
.source "SyncProgressMonitorOutputStream.java"


# instance fields
.field private final a:J

.field private final a:LagH;

.field private a:Ljava/io/OutputStream;

.field private b:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;LagH;J)V
    .locals 9

    .prologue
    .line 27
    const-wide/16 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v1 .. v7}, Lail;-><init>(Ljava/io/OutputStream;LagH;JJ)V

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;LagH;JJ)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 32
    iput-object p1, p0, Lail;->a:Ljava/io/OutputStream;

    .line 33
    iput-object p2, p0, Lail;->a:LagH;

    .line 34
    iput-wide p3, p0, Lail;->a:J

    .line 35
    iput-wide p5, p0, Lail;->b:J

    .line 36
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 78
    invoke-static {}, LpD;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, LQp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - thread interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LQp;-><init>(Ljava/lang/String;)V

    .line 81
    iget-wide v2, p0, Lail;->b:J

    long-to-int v1, v2

    iput v1, v0, LQp;->bytesTransferred:I

    .line 82
    throw v0

    .line 84
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 73
    iget-wide v0, p0, Lail;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lail;->b:J

    .line 74
    iget-object v0, p0, Lail;->a:LagH;

    iget-wide v2, p0, Lail;->b:J

    iget-wide v4, p0, Lail;->a:J

    invoke-interface {v0, v2, v3, v4, v5}, LagH;->a(JJ)V

    .line 75
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lail;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lail;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lail;->a:Ljava/io/OutputStream;

    .line 70
    :cond_0
    return-void
.end method

.method public write(I)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Lail;->a()V

    .line 41
    iget-object v0, p0, Lail;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 45
    const-wide/16 v0, 0x1

    invoke-direct {p0, v0, v1}, Lail;->a(J)V

    .line 46
    invoke-direct {p0}, Lail;->a()V

    .line 47
    return-void
.end method

.method public write([B)V
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lail;->write([BII)V

    .line 52
    return-void
.end method

.method public write([BII)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lail;->a()V

    .line 57
    iget-object v0, p0, Lail;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 60
    int-to-long v0, p3

    invoke-direct {p0, v0, v1}, Lail;->a(J)V

    .line 61
    invoke-direct {p0}, Lail;->a()V

    .line 62
    return-void
.end method
