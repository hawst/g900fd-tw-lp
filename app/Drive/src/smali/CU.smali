.class public LCU;
.super Ljava/lang/Object;
.source "SelectionManager.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LDa;

.field private final a:LJR;

.field private a:LLo;

.field private final a:LaGM;

.field private final a:Landroid/content/Context;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LvY;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/view/DocListView;

.field private final a:Z

.field private b:Z


# direct methods
.method public constructor <init>(LaGM;LtK;Landroid/content/Context;LJR;LDa;Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "LtK;",
            "Landroid/content/Context;",
            "LJR;",
            "LDa;",
            "Laja",
            "<",
            "LvY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, LCU;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, LCU;->b:Z

    .line 49
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iput-object p1, p0, LCU;->a:LaGM;

    .line 51
    sget-object v0, Lry;->U:Lry;

    .line 52
    invoke-interface {p2, v0}, LtK;->a(LtJ;)Z

    move-result v0

    iput-boolean v0, p0, LCU;->a:Z

    .line 53
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LCU;->a:Landroid/content/Context;

    .line 54
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJR;

    iput-object v0, p0, LCU;->a:LJR;

    .line 55
    iput-object p5, p0, LCU;->a:LDa;

    .line 56
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxw;

    iput-object v0, p0, LCU;->a:Lbxw;

    .line 57
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/doclist/SelectionItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, LCU;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvY;

    invoke-interface {v0}, LvY;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 88
    if-nez v0, :cond_0

    move-object v0, v1

    .line 95
    :goto_0
    return-object v0

    .line 91
    :cond_0
    iget-object v2, p0, LCU;->a:LaGM;

    invoke-interface {v0, v2}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 92
    if-nez v2, :cond_1

    move-object v0, v1

    .line 93
    goto :goto_0

    .line 95
    :cond_1
    new-instance v0, Lcom/google/android/apps/docs/doclist/SelectionItem;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 115
    invoke-virtual {p0}, LCU;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v0

    .line 116
    if-nez v0, :cond_0

    .line 118
    const-string v0, "SelectionManager"

    const-string v1, "Attempted to move to this folder but this folder does not exist."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v1, p0, LCU;->a:LDa;

    iget-object v2, p0, LCU;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LDa;->a(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    goto :goto_0
.end method

.method public a(LLo;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, LCU;->a:LLo;

    .line 68
    return-void
.end method

.method public a(LaGA;)V
    .locals 5

    .prologue
    .line 147
    if-eqz p1, :cond_0

    invoke-interface {p1}, LaGA;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-virtual {p0}, LCU;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v0

    .line 154
    :try_start_0
    iget-object v1, p0, LCU;->a:LJR;

    invoke-virtual {v1}, LJR;->b()V

    .line 155
    :goto_1
    invoke-interface {p1}, LaGA;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 156
    new-instance v1, Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 157
    invoke-interface {p1}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 158
    invoke-interface {p1}, LaGA;->a()LaGv;

    move-result-object v3

    sget-object v4, LaGv;->a:LaGv;

    invoke-virtual {v3, v4}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 160
    iget-object v2, p0, LCU;->a:LJR;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 161
    invoke-interface {p1}, LaGA;->k()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 164
    :catchall_0
    move-exception v0

    iget-object v1, p0, LCU;->a:LJR;

    invoke-virtual {v1}, LJR;->c()V

    throw v0

    :cond_2
    iget-object v0, p0, LCU;->a:LJR;

    invoke-virtual {v0}, LJR;->c()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 139
    iput-boolean p1, p0, LCU;->b:Z

    .line 140
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, LCU;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LCU;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method
