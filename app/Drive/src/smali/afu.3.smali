.class public final enum Lafu;
.super Ljava/lang/Enum;
.source "SingleFeedFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lafu;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lafu;

.field private static final synthetic a:[Lafu;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 174
    new-instance v0, Lafu;

    const-string v1, "GOOGLE_PHOTOS"

    const-string v2, "GOOGLE_PHOTOS"

    invoke-direct {v0, v1, v3, v2}, Lafu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lafu;->a:Lafu;

    .line 173
    const/4 v0, 0x1

    new-array v0, v0, [Lafu;

    sget-object v1, Lafu;->a:Lafu;

    aput-object v1, v0, v3

    sput-object v0, Lafu;->a:[Lafu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 179
    iput-object p3, p0, Lafu;->a:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lafu;
    .locals 1

    .prologue
    .line 173
    const-class v0, Lafu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lafu;

    return-object v0
.end method

.method public static values()[Lafu;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lafu;->a:[Lafu;

    invoke-virtual {v0}, [Lafu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lafu;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lafu;->a:Ljava/lang/String;

    return-object v0
.end method
