.class public Ltd;
.super Ljava/lang/Object;
.source "DocumentPreviewActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 498
    iget-object v0, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    sget v2, Lxc;->open_file:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 499
    iget-object v0, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    sget v3, Lxc;->open_detail_panel:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 501
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v4, "action_bar_title"

    const-string v5, "id"

    const-string v6, "android"

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 502
    iget-object v4, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 503
    iget-object v0, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    const v5, 0x102002c

    invoke-virtual {v0, v5}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 505
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 507
    :goto_0
    iget-object v5, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-static {v5}, LUs;->b(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 508
    if-eqz v2, :cond_0

    .line 509
    new-instance v5, Lti;

    iget-object v6, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {v5, v6, v1}, Lti;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 510
    new-instance v5, Lth;

    iget-object v6, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {v5, v6, v1}, Lth;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 513
    :cond_0
    if-eqz v3, :cond_1

    .line 514
    new-instance v2, Lti;

    iget-object v5, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {v2, v5, v1}, Lti;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 515
    new-instance v2, Lth;

    iget-object v5, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {v2, v5, v1}, Lth;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 518
    :cond_1
    if-eqz v4, :cond_2

    .line 519
    new-instance v2, Lti;

    iget-object v3, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {v2, v3, v1}, Lti;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 522
    :cond_2
    if-eqz v0, :cond_3

    .line 523
    new-instance v2, Lth;

    iget-object v3, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {v2, v3, v1}, Lth;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 530
    :cond_3
    iget-object v0, p0, Ltd;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    .line 531
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 530
    invoke-static {v0, p0}, LUv;->a(Landroid/view/ViewTreeObserver;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 532
    return-void

    :cond_4
    move-object v0, v1

    .line 505
    goto :goto_0
.end method
