.class public LbhJ;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 565
    iget-object v1, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    monitor-enter v1

    .line 566
    :try_start_0
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;I)I

    .line 568
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V

    .line 570
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;

    .line 571
    invoke-interface {v0, p1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;->onFocus(Z)V

    goto :goto_0

    .line 575
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 573
    :cond_0
    :try_start_1
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 574
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;I)I

    .line 575
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 577
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0, v3}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;Z)Z

    .line 578
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V

    .line 586
    :cond_1
    :goto_1
    return-void

    .line 579
    :cond_2
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 580
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0, v3}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;Z)Z

    .line 581
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, v4, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->takePicture(Lcom/google/bionics/scanner/unveil/camera/CameraManager$ShutterCallback;Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureCallback;)V

    goto :goto_1

    .line 582
    :cond_3
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 583
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    iget-object v1, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->setFlashMode(Ljava/lang/String;)V

    .line 584
    iget-object v0, p0, LbhJ;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method
