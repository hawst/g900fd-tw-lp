.class public final enum LEN;
.super Ljava/lang/Enum;
.source "DialogUtility.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LEN;",
        ">;",
        "Landroid/content/DialogInterface$OnKeyListener;"
    }
.end annotation


# static fields
.field public static final enum a:LEN;

.field private static final synthetic a:[LEN;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    new-instance v0, LEN;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LEN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LEN;->a:LEN;

    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [LEN;

    sget-object v1, LEN;->a:LEN;

    aput-object v1, v0, v2

    sput-object v0, LEN;->a:[LEN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LEN;
    .locals 1

    .prologue
    .line 39
    const-class v0, LEN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LEN;

    return-object v0
.end method

.method public static values()[LEN;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LEN;->a:[LEN;

    invoke-virtual {v0}, [LEN;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LEN;

    return-object v0
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x54

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
