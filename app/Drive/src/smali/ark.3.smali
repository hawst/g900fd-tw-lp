.class public Lark;
.super Ljava/lang/Object;
.source "ActionBarModeSwitcherImpl.java"

# interfaces
.implements Larg;


# static fields
.field private static final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/app/Activity;

.field private final a:Lapn;

.field private a:Larc;

.field private final a:Lare;

.field private final a:Laro;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lari;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lwm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    sget v0, Lxc;->menu_grid_mode:I

    .line 37
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget v1, Lxc;->menu_list_mode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, Lark;->a:LbmY;

    .line 36
    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lapn;Lwm;Lare;Laro;)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lark;->a:Ljava/util/Set;

    .line 106
    iput-object p1, p0, Lark;->a:Landroid/app/Activity;

    .line 107
    iput-object p2, p0, Lark;->a:Lapn;

    .line 108
    iput-object p3, p0, Lark;->a:Lwm;

    .line 109
    iput-object p4, p0, Lark;->a:Lare;

    .line 110
    iput-object p5, p0, Lark;->a:Laro;

    .line 111
    sget-object v0, Larj;->a:Larj;

    invoke-virtual {p4, v0}, Lare;->a(Larj;)Larc;

    move-result-object v0

    iput-object v0, p0, Lark;->a:Larc;

    .line 112
    new-instance v0, Larl;

    invoke-direct {v0, p0}, Larl;-><init>(Lark;)V

    invoke-interface {p3, v0}, Lwm;->a(Lwn;)V

    .line 123
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;Lapn;Lwm;Lare;Laro;Larl;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lark;-><init>(Landroid/app/Activity;Lapn;Lwm;Lare;Laro;)V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, Lark;->a:Landroid/app/Activity;

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lark;->a:Lwm;

    invoke-static {v0}, Lwr;->a(Lwm;)Ljava/lang/String;

    move-result-object v0

    .line 232
    sget-object v1, Larn;->a:[I

    iget-object v2, p0, Lark;->a:Larc;

    invoke-virtual {v2}, Larc;->a()Larj;

    move-result-object v2

    invoke-virtual {v2}, Larj;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 234
    :pswitch_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    sget-object v0, Larj;->b:Larj;

    invoke-virtual {p0, v0}, Lark;->a(Larj;)V

    goto :goto_0

    .line 239
    :pswitch_1
    if-nez v0, :cond_0

    .line 240
    sget-object v0, Larj;->a:Larj;

    invoke-virtual {p0, v0}, Lark;->a(Larj;)V

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/view/Menu;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207
    sget-object v0, Lark;->a:LbmY;

    invoke-interface {p2, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    sget v0, Lxc;->menu_grid_mode:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 209
    sget v1, Lxc;->menu_list_mode:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 210
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 211
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 213
    iget-object v2, p0, Lark;->a:Lapn;

    new-instance v3, Larm;

    invoke-direct {v3, p0, v0, v1}, Larm;-><init>(Lark;Landroid/view/MenuItem;Landroid/view/MenuItem;)V

    invoke-virtual {v2, v3}, Lapn;->d(Lapu;)V

    .line 222
    :cond_0
    return-void
.end method

.method private a(Larj;Z)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lark;->a:Larc;

    invoke-virtual {v0}, Larc;->a()Larj;

    move-result-object v0

    invoke-virtual {v0, p1}, Larj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    iget-object v0, p0, Lark;->a:Larc;

    .line 133
    iget-object v1, p0, Lark;->a:Lare;

    invoke-virtual {v1, p1}, Lare;->a(Larj;)Larc;

    move-result-object v1

    iput-object v1, p0, Lark;->a:Larc;

    .line 134
    iget-object v1, p0, Lark;->a:Landroid/app/Activity;

    invoke-static {v1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lark;->a:Larc;

    invoke-virtual {v1, v0, p2}, Larc;->a(Larc;Z)V

    .line 137
    :cond_0
    iget-object v0, p0, Lark;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 141
    iget-object v0, p0, Lark;->a:Ljava/util/Set;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lari;

    .line 145
    invoke-interface {v0, p1}, Lari;->a(Larj;)V

    goto :goto_0

    .line 148
    :cond_1
    return-void
.end method

.method static synthetic a(Lark;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lark;->a()V

    return-void
.end method


# virtual methods
.method public a()Larj;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lark;->a:Larc;

    invoke-virtual {v0}, Larc;->a()Larj;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 258
    const-string v0, "com.google.android.apps.docs.honeycomb.ActionBarModeSwitcherImpl.MODE"

    iget-object v1, p0, Lark;->a:Larc;

    invoke-virtual {v1}, Larc;->a()Larj;

    move-result-object v1

    invoke-virtual {v1}, Larj;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lark;->a:Larc;

    invoke-virtual {v0, p1}, Larc;->a(Landroid/os/Bundle;)V

    .line 260
    return-void
.end method

.method public a(Landroid/view/Menu;ZLbmY;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Z",
            "LbmY",
            "<",
            "Ljava/lang/Integer;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lark;->a:Laro;

    invoke-virtual {v0}, Laro;->a()V

    .line 174
    if-eqz p2, :cond_1

    iget-object v0, p0, Lark;->a:Larc;

    .line 175
    invoke-virtual {v0}, Larc;->a()LbmY;

    move-result-object v0

    .line 176
    :goto_0
    invoke-static {v0, p3}, LbpU;->c(Ljava/util/Set;Ljava/util/Set;)Lbqb;

    move-result-object v0

    .line 178
    :goto_1
    if-ge p4, p5, :cond_3

    .line 179
    invoke-interface {p1, p4}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 180
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 181
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 182
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 183
    iget-object v2, p0, Lark;->a:Larc;

    invoke-virtual {v2, v1}, Larc;->b(Landroid/view/MenuItem;)V

    .line 184
    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 185
    if-eqz v1, :cond_0

    .line 186
    iget-object v2, p0, Lark;->a:Laro;

    invoke-virtual {v2, v1}, Laro;->a(Landroid/graphics/drawable/Drawable;)V

    .line 178
    :cond_0
    :goto_2
    add-int/lit8 p4, p4, 0x1

    goto :goto_1

    .line 175
    :cond_1
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    goto :goto_0

    .line 189
    :cond_2
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 195
    :cond_3
    iget-object v1, p0, Lark;->a:Landroid/app/Activity;

    invoke-static {v1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 196
    sget v1, Lxc;->menu_search:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 197
    if-eqz v1, :cond_4

    .line 198
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 202
    :cond_4
    invoke-direct {p0, p1, v0}, Lark;->a(Landroid/view/Menu;Ljava/util/Set;)V

    .line 203
    iget-object v0, p0, Lark;->a:Larc;

    iget-object v1, p0, Lark;->a:Laro;

    invoke-virtual {v0, v1}, Larc;->a(Laro;)V

    .line 204
    return-void
.end method

.method public a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lark;->a:Larc;

    invoke-virtual {v0, p1}, Larc;->a(Landroid/view/MenuItem;)V

    .line 227
    return-void
.end method

.method public a(Lari;)V
    .locals 1

    .prologue
    .line 152
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lark;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lark;->a:Larc;

    invoke-virtual {v0}, Larc;->a()Larj;

    move-result-object v0

    invoke-interface {p1, v0}, Lari;->a(Larj;)V

    .line 155
    return-void
.end method

.method public a(Larj;)V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lark;->a(Larj;Z)V

    .line 128
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lark;->a:Larc;

    invoke-virtual {v0}, Larc;->a()Z

    move-result v0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 264
    const-string v0, "com.google.android.apps.docs.honeycomb.ActionBarModeSwitcherImpl.MODE"

    sget-object v1, Larj;->a:Larj;

    invoke-virtual {v1}, Larj;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Larj;->valueOf(Ljava/lang/String;)Larj;

    move-result-object v0

    .line 265
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lark;->a(Larj;Z)V

    .line 266
    iget-object v0, p0, Lark;->a:Larc;

    invoke-virtual {v0, p1}, Larc;->b(Landroid/os/Bundle;)V

    .line 267
    return-void
.end method

.method public b(Lari;)V
    .locals 1

    .prologue
    .line 159
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lark;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 161
    return-void
.end method
