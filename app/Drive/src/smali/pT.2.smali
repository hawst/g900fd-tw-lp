.class public abstract LpT;
.super Ljava/lang/Object;
.source "RequiredSdkVersionProvider.java"

# interfaces
.implements Lbxw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lbxw",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(ILbxw;Lbxw;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:TT;B:TT;>(I",
            "Lbxw",
            "<TA;>;",
            "Lbxw",
            "<TB;>;",
            "Ljava/lang/Class",
            "<TA;>;",
            "Ljava/lang/Class",
            "<TB;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p1, :cond_0

    .line 33
    iput-object p2, p0, LpT;->a:Lbxw;

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    iput-object p3, p0, LpT;->a:Lbxw;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, LpT;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
