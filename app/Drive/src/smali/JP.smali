.class public LJP;
.super Ljava/lang/Object;
.source "DocListMenuModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method provideAccountActivity(Landroid/content/Context;)LqV;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 29
    const-class v0, LqV;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqV;

    return-object v0
.end method

.method provideDocListActionsListener(Landroid/content/Context;)LJJ;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 35
    const-class v0, LJJ;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJJ;

    return-object v0
.end method

.method provideDocListExtraActionsHelper(LJL;)LJK;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 47
    return-object p1
.end method

.method provideDocListMenu(LJN;)LJM;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 23
    return-object p1
.end method

.method provideOnSearchListener(Landroid/content/Context;)Lara;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 41
    const-class v0, Lara;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lara;

    return-object v0
.end method
