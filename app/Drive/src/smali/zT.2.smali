.class public LzT;
.super LalP;
.source "BaseSearchSuggestionProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LalP",
        "<",
        "LzV;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:[Ljava/lang/String;


# instance fields
.field private final a:I

.field private final a:Landroid/content/UriMatcher;

.field private a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_intent_data_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    sput-object v0, LzT;->a:[Ljava/lang/String;

    .line 72
    new-instance v0, LzU;

    invoke-direct {v0}, LzU;-><init>()V

    invoke-static {v0}, Lajw;->a(Lbxw;)Lajw;

    move-result-object v0

    sput-object v0, LzT;->a:Lajw;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, LalP;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput v0, p0, LzT;->a:I

    .line 113
    invoke-direct {p0, p1}, LzT;->a(Ljava/lang/String;)Landroid/content/UriMatcher;

    move-result-object v0

    iput-object v0, p0, LzT;->a:Landroid/content/UriMatcher;

    .line 114
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LzV;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, LzT;-><init>(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0, p2}, LzT;->a(Ljava/lang/Object;)V

    .line 129
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LaFO;
    .locals 2

    .prologue
    .line 293
    if-nez p0, :cond_1

    .line 294
    const/4 v0, 0x0

    .line 304
    :cond_0
    :goto_0
    return-object v0

    .line 299
    :cond_1
    const-string v0, "intent_extra_data_key"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 300
    if-nez v0, :cond_0

    .line 302
    invoke-static {p0}, LzT;->a(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    goto :goto_0
.end method

.method private a()LaGM;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, LzT;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzV;

    iget-object v0, v0, LzV;->a:LaGM;

    return-object v0
.end method

.method private a()LaKM;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, LzT;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzV;

    iget-object v0, v0, LzV;->a:LaKM;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/content/UriMatcher;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 137
    const-string v1, "search_suggest_query"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 138
    const-string v1, "search_suggest_query/*"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 139
    return-object v0
.end method

.method private a(LaGA;)Landroid/database/Cursor;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v1, 0x0

    .line 223
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v0, LzT;->a:[Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 225
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 226
    invoke-direct {p0}, LzT;->a()LaKM;

    move-result-object v3

    invoke-interface {v3}, LaKM;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 227
    new-instance v3, LamF;

    invoke-virtual {p0}, LzT;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v0}, LamF;-><init>(Landroid/content/Context;Landroid/text/format/Time;)V

    move v0, v1

    .line 230
    :cond_0
    :goto_0
    const/16 v4, 0xa

    if-ge v0, v4, :cond_1

    invoke-interface {p1}, LaGA;->k()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 231
    add-int/lit8 v0, v0, 0x1

    .line 232
    invoke-interface {p1}, LaGA;->a()LaGv;

    move-result-object v4

    .line 233
    invoke-interface {p1}, LaGA;->e()Ljava/lang/String;

    move-result-object v5

    .line 234
    invoke-interface {p1}, LaGA;->a()Ljava/lang/String;

    move-result-object v6

    .line 235
    invoke-interface {p1}, LaGA;->c()Z

    move-result v7

    .line 236
    invoke-interface {p1}, LaGA;->a()LaGw;

    .line 237
    invoke-interface {p1}, LaGA;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v8

    .line 238
    if-eqz v8, :cond_0

    .line 242
    invoke-interface {p1}, LaGA;->c()J

    move-result-wide v10

    .line 243
    invoke-virtual {v3, v10, v11}, LamF;->a(J)Ljava/lang/String;

    move-result-object v9

    .line 244
    invoke-virtual {p0}, LzT;->getContext()Landroid/content/Context;

    move-result-object v10

    sget v11, Lxi;->doclist_date_modified_label:I

    new-array v12, v13, [Ljava/lang/Object;

    aput-object v9, v12, v1

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 248
    const/16 v10, 0x8

    new-array v10, v10, [Ljava/lang/Object;

    .line 250
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v1

    const-string v11, "android.intent.action.VIEW"

    aput-object v11, v10, v13

    const/4 v11, 0x2

    .line 254
    invoke-static {v4, v5, v7}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v10, v11

    const/4 v4, 0x3

    aput-object v6, v10, v4

    const/4 v4, 0x4

    aput-object v9, v10, v4

    const/4 v4, 0x5

    .line 260
    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, v4

    const/4 v4, 0x6

    iget-object v5, v8, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 262
    invoke-virtual {v5}, LaFO;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, v4

    const/4 v4, 0x7

    const-string v5, "_-1"

    aput-object v5, v10, v4

    .line 248
    invoke-virtual {v2, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 267
    :cond_1
    return-object v2
.end method

.method public static a(LaFO;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 284
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 285
    const-string v1, "accountName"

    invoke-virtual {p0}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    return-object v0
.end method

.method private static a(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    .line 309
    if-eqz p0, :cond_0

    .line 310
    const-string v0, "app_data"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 312
    :cond_0
    if-nez v0, :cond_1

    .line 313
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 315
    :cond_1
    return-object v0
.end method

.method private a()LvL;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, LzT;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzV;

    iget-object v0, v0, LzV;->a:LvL;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic a(Lbuu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, LzT;->a(Lbuu;)LzV;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbuu;)LzV;
    .locals 2

    .prologue
    .line 118
    new-instance v0, LzV;

    invoke-direct {v0}, LzV;-><init>()V

    .line 119
    invoke-interface {p1, v0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 121
    iget-object v1, v0, LzV;->a:LaGM;

    invoke-interface {v1}, LaGM;->e()V

    .line 122
    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, LzT;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 220
    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 278
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, LzT;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, LzT;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 210
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :pswitch_0
    aget-object v0, p4, v6

    .line 168
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 169
    new-instance v2, LvN;

    invoke-direct {v2}, LvN;-><init>()V

    .line 170
    invoke-direct {p0}, LzT;->a()LvL;

    move-result-object v3

    .line 172
    const-wide/16 v4, -0x1

    .line 173
    invoke-static {v0, v4, v5}, Laay;->a(Ljava/lang/String;J)Laay;

    move-result-object v0

    invoke-interface {v3, v0}, LvL;->a(Laay;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    .line 174
    invoke-virtual {v2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 176
    iget-object v0, p0, LzT;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, LzT;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 179
    invoke-virtual {v2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    goto :goto_0

    .line 186
    :cond_0
    invoke-virtual {p0}, LzT;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 187
    invoke-static {v0}, LajS;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 188
    array-length v4, v0

    if-nez v4, :cond_2

    move-object v0, v1

    .line 204
    :cond_1
    :goto_1
    return-object v0

    .line 191
    :cond_2
    aget-object v0, v0, v6

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 192
    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    invoke-interface {v3, v0}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    .line 191
    invoke-virtual {v2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 197
    :cond_3
    :try_start_0
    invoke-direct {p0}, LzT;->a()LaGM;

    move-result-object v3

    .line 198
    invoke-virtual {v2}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v2

    sget-object v4, LIK;->c:LIK;

    sget-object v0, LzT;->a:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 197
    invoke-interface {v3, v2, v4, v0}, LaGM;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;)LaGA;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 199
    :try_start_1
    invoke-direct {p0, v2}, LzT;->a(LaGA;)Landroid/database/Cursor;
    :try_end_1
    .catch LaGT; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 203
    if-eqz v2, :cond_1

    .line 204
    invoke-interface {v2}, LaGA;->a()V

    goto :goto_1

    .line 200
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 203
    :goto_2
    if-eqz v0, :cond_4

    .line 204
    invoke-interface {v0}, LaGA;->a()V

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 203
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_5

    .line 204
    invoke-interface {v1}, LaGA;->a()V

    :cond_5
    throw v0

    .line 208
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 200
    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_2

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
