.class LLa;
.super Ljava/lang/Object;
.source "SelectionCardAnimator.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:LKZ;

.field final synthetic a:Landroid/view/View;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

.field final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(LKZ;Landroid/view/View;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, LLa;->a:LKZ;

    iput-object p2, p0, LLa;->a:Landroid/view/View;

    iput-object p3, p0, LLa;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    iput-object p4, p0, LLa;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 70
    iget-object v0, p0, LLa;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 72
    iget-object v0, p0, LLa;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getWidth()I

    move-result v0

    .line 73
    iget-object v1, p0, LLa;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getHeight()I

    move-result v1

    .line 75
    iget-object v2, p0, LLa;->a:LKZ;

    invoke-static {v2}, LKZ;->a(LKZ;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 76
    iget-object v2, p0, LLa;->a:LKZ;

    invoke-static {v2}, LKZ;->a(LKZ;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 77
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 78
    iget-object v0, p0, LLa;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object v0, p0, LLa;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v0, p0, LLa;->a:LKZ;

    iget-object v1, p0, LLa;->a:Landroid/view/View;

    iget-object v2, p0, LLa;->b:Landroid/view/View;

    iget-object v3, p0, LLa;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getHeight()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LKZ;->a(LKZ;Landroid/view/View;Landroid/view/View;I)V

    .line 83
    iget-object v0, p0, LLa;->b:Landroid/view/View;

    iget-object v1, p0, LLa;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getX()F

    move-result v1

    iget-object v2, p0, LLa;->a:LKZ;

    invoke-static {v2}, LKZ;->a(LKZ;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 84
    iget-object v0, p0, LLa;->b:Landroid/view/View;

    iget-object v1, p0, LLa;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getY()F

    move-result v1

    iget-object v2, p0, LLa;->a:LKZ;

    invoke-static {v2}, LKZ;->a(LKZ;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    .line 86
    iget-object v0, p0, LLa;->b:Landroid/view/View;

    sget v1, Lxc;->shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 88
    new-array v1, v6, [F

    fill-array-data v1, :array_0

    .line 89
    invoke-static {v0, v1}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    const/16 v1, 0x64

    .line 90
    invoke-virtual {v0, v1}, Lxr;->a(I)Lxr;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 88
    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    iget-object v1, p0, LLa;->b:Landroid/view/View;

    const/4 v2, 0x1

    new-array v2, v2, [F

    iget-object v3, p0, LLa;->b:Landroid/view/View;

    .line 93
    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v3

    const/high16 v4, 0x43960000    # 300.0f

    sub-float/2addr v3, v4

    aput v3, v2, v5

    invoke-static {v1, v2}, Lxm;->b(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v1

    .line 92
    invoke-static {v1}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    iget-object v2, p0, LLa;->b:Landroid/view/View;

    new-array v3, v6, [F

    fill-array-data v3, :array_1

    .line 94
    invoke-static {v2, v3}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 95
    invoke-virtual {v1, v2}, Lxr;->a(Landroid/view/animation/Interpolator;)Lxr;

    move-result-object v1

    const/16 v2, 0x12c

    .line 96
    invoke-virtual {v1, v2}, Lxr;->a(I)Lxr;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Lxr;->b(Lxr;)Lxr;

    move-result-object v0

    new-instance v1, LLb;

    invoke-direct {v1, p0}, LLb;-><init>(LLa;)V

    .line 97
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 105
    iget-object v1, p0, LLa;->a:LKZ;

    invoke-static {v1}, LKZ;->a(LKZ;)LKp;

    move-result-object v1

    iget-object v2, p0, LLa;->b:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, LKp;->a(Landroid/view/View;Landroid/animation/Animator;)V

    .line 106
    return v5

    .line 88
    nop

    :array_0
    .array-data 4
        0x0
        0x3e4ccccd    # 0.2f
    .end array-data

    .line 92
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
