.class public LauI;
.super Landroid/os/AsyncTask;
.source "CastNotificationService.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "LaCh;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LaCh;

.field final synthetic a:Lcom/google/android/apps/viewer/cast/CastNotificationService;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/viewer/cast/CastNotificationService;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/viewer/cast/CastNotificationService;LauH;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0, p1}, LauI;-><init>(Lcom/google/android/apps/viewer/cast/CastNotificationService;)V

    return-void
.end method


# virtual methods
.method protected varargs a([LaCh;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 222
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, LauI;->a:LaCh;

    .line 224
    iget-object v0, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    iput-object v2, v0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/graphics/Bitmap;

    .line 227
    const-string v0, "CastNotificationService"

    const-string v1, "Downloading low res image."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :try_start_0
    iget-object v0, p0, LauI;->a:LaCh;

    invoke-virtual {v0}, LaCh;->a()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "loResUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 237
    :try_start_1
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 238
    :try_start_2
    iget-object v0, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 250
    :goto_0
    return-object v2

    .line 231
    :catch_0
    move-exception v0

    .line 232
    const-string v0, "CastNotificationService"

    const-string v1, "Error in getting the low res url from JSON Object"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 239
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 240
    :goto_1
    const-string v3, "CastNotificationService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setIcon(): Failed to load the image with url: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", using the default one"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 243
    iget-object v0, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    iput-object v2, v0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 244
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 245
    :goto_2
    const-string v3, "CastNotificationService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setIcon(): Failed to load the image with url: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", using the default one"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 248
    iget-object v0, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    iput-object v2, v0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 244
    :catch_3
    move-exception v0

    goto :goto_2

    .line 239
    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 255
    iget-object v0, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    iget-object v0, v0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 256
    const-string v0, "CastNotificationService"

    const-string v1, "default"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v0, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    iget-object v1, p0, LauI;->a:LaCh;

    const/4 v2, 0x0

    iget-object v3, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    invoke-static {v3}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(Lcom/google/android/apps/viewer/cast/CastNotificationService;)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(Lcom/google/android/apps/viewer/cast/CastNotificationService;LaCh;Landroid/graphics/Bitmap;Z)Landroid/widget/RemoteViews;

    .line 262
    :goto_0
    return-void

    .line 259
    :cond_0
    const-string v0, "CastNotificationService"

    const-string v1, "custom low res"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    iget-object v0, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    iget-object v1, p0, LauI;->a:LaCh;

    iget-object v2, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    iget-object v2, v2, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/graphics/Bitmap;

    iget-object v3, p0, LauI;->a:Lcom/google/android/apps/viewer/cast/CastNotificationService;

    invoke-static {v3}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(Lcom/google/android/apps/viewer/cast/CastNotificationService;)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(Lcom/google/android/apps/viewer/cast/CastNotificationService;LaCh;Landroid/graphics/Bitmap;Z)Landroid/widget/RemoteViews;

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 216
    check-cast p1, [LaCh;

    invoke-virtual {p0, p1}, LauI;->a([LaCh;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 216
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, LauI;->a(Ljava/lang/Void;)V

    return-void
.end method
