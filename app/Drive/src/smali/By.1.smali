.class public LBy;
.super LBe;
.source "DocListNormalEntryViewHolder.java"

# interfaces
.implements LCO;
.implements LEg;


# instance fields
.field protected final a:LCF;

.field final a:LDT;

.field final a:LDi;

.field final a:LEd;

.field private final a:LLp;

.field private final b:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LEd;",
            ">;"
        }
    .end annotation
.end field

.field protected final f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lapd;LDk;)V
    .locals 4

    .prologue
    .line 34
    invoke-direct {p0, p1}, LBe;-><init>(Landroid/view/View;)V

    .line 38
    iget-object v0, p0, LBy;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LBy;->c:Landroid/view/View;

    .line 39
    :goto_0
    iput-object v0, p0, LBy;->f:Landroid/view/View;

    .line 40
    iget-object v0, p0, LBy;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->m_doclist_thumbnail_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 42
    new-instance v2, LEd;

    const/4 v3, 0x0

    sget v0, Lxc;->doc_icon:I

    .line 43
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocThumbnailView;

    invoke-direct {v2, p2, v3, v0, v1}, LEd;-><init>(Lapd;Lapc;Lcom/google/android/apps/docs/view/DocThumbnailView;I)V

    iput-object v2, p0, LBy;->a:LEd;

    .line 44
    iget-object v0, p0, LBy;->a:LEd;

    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, LBy;->b:LbmF;

    .line 45
    new-instance v0, LDT;

    invoke-direct {v0, p1}, LDT;-><init>(Landroid/view/View;)V

    iput-object v0, p0, LBy;->a:LDT;

    .line 46
    sget v0, Lxc;->select_button_background:I

    sget v1, Lxc;->unselect_button_background:I

    invoke-virtual {p3, p1, v0, v1}, LDk;->a(Landroid/view/View;II)LDi;

    move-result-object v0

    iput-object v0, p0, LBy;->a:LDi;

    .line 48
    new-instance v0, LCF;

    invoke-direct {v0, p1}, LCF;-><init>(Landroid/view/View;)V

    iput-object v0, p0, LBy;->a:LCF;

    .line 49
    new-instance v0, LLp;

    iget-object v1, p0, LBy;->d:Landroid/view/View;

    iget-object v2, p0, LBy;->f:Landroid/view/View;

    .line 50
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-direct {v0, v1, v2}, LLp;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, LBy;->a:LLp;

    .line 52
    iget-object v0, p0, LBy;->a:LEd;

    invoke-virtual {v0}, LEd;->a()Lcom/google/android/apps/docs/view/FixedSizeImageView;

    move-result-object v0

    new-instance v1, LBz;

    invoke-direct {v1, p0}, LBz;-><init>(LBy;)V

    invoke-static {v0, v1}, Lec;->a(Landroid/view/View;LcS;)V

    .line 64
    return-void

    .line 38
    :cond_0
    sget v0, Lxc;->main_body:I

    .line 39
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "LEd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, LBy;->b:LbmF;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, LBy;->a:LCF;

    invoke-virtual {v0}, LCF;->e()V

    .line 68
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, LBy;->a:LLp;

    invoke-virtual {v0, p1}, LLp;->a(Z)V

    .line 82
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LBy;->a:LCF;

    invoke-virtual {v0}, LCF;->f()V

    .line 72
    return-void
.end method
