.class LaxF;
.super Laxy;
.source "PdfLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Lcom/google/android/apps/viewer/pdflib/MatchRects;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field final synthetic a:Laxz;

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Laxz;Ljava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 402
    iput-object p1, p0, LaxF;->a:Laxz;

    .line 403
    sget-object v0, LaxR;->f:LaxR;

    invoke-direct {p0, p1, v0}, Laxy;-><init>(Laxz;LaxR;)V

    .line 404
    iput-object p2, p0, LaxF;->a:Ljava/lang/String;

    .line 405
    iput p3, p0, LaxF;->a:I

    .line 406
    iput-boolean p4, p0, LaxF;->a:Z

    .line 407
    return-void
.end method

.method static synthetic a(LaxF;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, LaxF;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(LavE;)Lcom/google/android/apps/viewer/pdflib/MatchRects;
    .locals 4

    .prologue
    .line 411
    invoke-interface {p1}, LavE;->b()I

    move-result v0

    if-gtz v0, :cond_0

    .line 412
    const/4 v0, 0x0

    .line 422
    :goto_0
    return-object v0

    .line 414
    :cond_0
    iget v0, p0, LaxF;->a:I

    invoke-interface {p1}, LavE;->b()I

    move-result v1

    iget-boolean v2, p0, LaxF;->a:Z

    invoke-static {v0, v1, v2}, LavV;->a(IIZ)LavV;

    move-result-object v0

    invoke-virtual {v0}, LavV;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 415
    iget-object v1, p0, LaxF;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LavE;->a(ILjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/MatchRects;

    move-result-object v1

    .line 416
    invoke-virtual {v1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 417
    iput v0, p0, LaxF;->b:I

    .line 418
    iget-boolean v0, p0, LaxF;->a:Z

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    iput v0, p0, LaxF;->c:I

    move-object v0, v1

    .line 419
    goto :goto_0

    .line 418
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 422
    :cond_3
    sget-object v0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->NO_MATCHES:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    goto :goto_0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0, p1}, LaxF;->a(LavE;)Lcom/google/android/apps/viewer/pdflib/MatchRects;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;Lcom/google/android/apps/viewer/pdflib/MatchRects;)V
    .locals 3

    .prologue
    .line 427
    iget-object v0, p0, LaxF;->a:Ljava/lang/String;

    iget v1, p0, LaxF;->b:I

    iget v2, p0, LaxF;->c:I

    invoke-interface {p1, v0, v1, p2, v2}, LaxH;->a(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    .line 428
    iget-object v0, p0, LaxF;->a:Laxz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laxz;->a(Laxz;LaxF;)LaxF;

    .line 429
    return-void
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 395
    check-cast p2, Lcom/google/android/apps/viewer/pdflib/MatchRects;

    invoke-virtual {p0, p1, p2}, LaxF;->a(LaxH;Lcom/google/android/apps/viewer/pdflib/MatchRects;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 432
    const-string v0, "FindNextMatchTask(query=\"%s\", startPage=%d, backwards=%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaxF;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LaxF;->a:I

    .line 433
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, LaxF;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 432
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
