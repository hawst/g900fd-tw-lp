.class LLr;
.super Ljava/lang/Object;
.source "SelectionDropTargetUpdater.java"

# interfaces
.implements LarN;


# instance fields
.field final synthetic a:LLp;

.field a:Z


# direct methods
.method private constructor <init>(LLp;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, LLr;->a:LLp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LLp;LLq;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, LLr;-><init>(LLp;)V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, LLr;->a:Z

    .line 22
    return-void
.end method

.method public a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 38
    :goto_0
    return v0

    .line 30
    :cond_0
    iget-object v0, p0, LLr;->a:LLp;

    invoke-static {v0}, LLp;->a(LLp;)LalK;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, LalK;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLs;

    .line 31
    if-eqz v0, :cond_2

    .line 32
    iget-boolean v1, p0, LLr;->a:Z

    invoke-interface {v0, p1, v1}, LLs;->a(Landroid/view/View;Z)V

    .line 38
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 33
    :cond_2
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 34
    iget-object v0, p0, LLr;->a:LLp;

    invoke-static {v0}, LLp;->a(LLp;)LMe;

    move-result-object v0

    iget-boolean v1, p0, LLr;->a:Z

    invoke-virtual {v0, p1, v1}, LMe;->a(Landroid/view/View;Z)V

    goto :goto_1

    .line 35
    :cond_3
    instance-of v0, p1, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 36
    iget-object v0, p0, LLr;->a:LLp;

    invoke-static {v0}, LLp;->a(LLp;)LMa;

    move-result-object v0

    iget-boolean v1, p0, LLr;->a:Z

    invoke-virtual {v0, p1, v1}, LMa;->a(Landroid/view/View;Z)V

    goto :goto_1
.end method
