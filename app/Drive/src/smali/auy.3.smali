.class final Lauy;
.super Landroid/print/PrintDocumentAdapter;
.source "SystemPrintDialog.java"


# instance fields
.field private final a:I

.field final synthetic a:Lauw;

.field private final a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lauw;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lauy;->a:Lauw;

    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 75
    iput-object p2, p0, Lauy;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    .line 76
    iput-object p3, p0, Lauy;->a:Ljava/lang/String;

    .line 77
    iput p4, p0, Lauy;->a:I

    .line 78
    return-void
.end method

.method synthetic constructor <init>(Lauw;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Ljava/lang/String;ILaux;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Lauy;-><init>(Lauw;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lauy;)Lcom/google/android/apps/viewer/client/AuthenticatedUri;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lauy;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    return-object v0
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lauy;->a:Lauw;

    invoke-static {v0}, Lauw;->a(Lauw;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 93
    return-void
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 83
    new-instance v0, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v1, p0, Lauy;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0, v2}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v0

    iget v1, p0, Lauy;->a:I

    .line 85
    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    .line 86
    invoke-virtual {p4, v0, v2}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 87
    return-void
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 2

    .prologue
    .line 98
    new-instance v0, Lauz;

    invoke-direct {v0, p0, p3, p2, p4}, Lauz;-><init>(Lauy;Landroid/os/CancellationSignal;Landroid/os/ParcelFileDescriptor;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 125
    invoke-virtual {v0, v1}, Lauz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 126
    return-void
.end method
