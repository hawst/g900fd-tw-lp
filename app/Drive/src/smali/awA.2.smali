.class public final enum LawA;
.super Ljava/lang/Enum;
.source "Viewer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LawA;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LawA;

.field private static final synthetic a:[LawA;

.field public static final enum b:LawA;

.field public static final enum c:LawA;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, LawA;

    const-string v1, "NO_VIEW"

    invoke-direct {v0, v1, v2}, LawA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LawA;->a:LawA;

    .line 74
    new-instance v0, LawA;

    const-string v1, "VIEW_CREATED"

    invoke-direct {v0, v1, v3}, LawA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LawA;->b:LawA;

    .line 83
    new-instance v0, LawA;

    const-string v1, "VIEW_READY"

    invoke-direct {v0, v1, v4}, LawA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LawA;->c:LawA;

    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [LawA;

    sget-object v1, LawA;->a:LawA;

    aput-object v1, v0, v2

    sget-object v1, LawA;->b:LawA;

    aput-object v1, v0, v3

    sget-object v1, LawA;->c:LawA;

    aput-object v1, v0, v4

    sput-object v0, LawA;->a:[LawA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LawA;
    .locals 1

    .prologue
    .line 60
    const-class v0, LawA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LawA;

    return-object v0
.end method

.method public static values()[LawA;
    .locals 1

    .prologue
    .line 60
    sget-object v0, LawA;->a:[LawA;

    invoke-virtual {v0}, [LawA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LawA;

    return-object v0
.end method
