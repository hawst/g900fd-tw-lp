.class public LXG;
.super Ljava/lang/Object;
.source "PunchMultiScreenModeFragment.java"

# interfaces
.implements LWT;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public b(I)V
    .locals 5

    .prologue
    .line 182
    iget-object v0, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->c:Ljava/lang/String;

    const-string v1, "Display #%s removed."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 183
    iget-object v0, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V

    .line 184
    return-void
.end method

.method public c(I)V
    .locals 5

    .prologue
    .line 170
    iget-object v0, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->c:Ljava/lang/String;

    const-string v1, "Display #%s changed."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 171
    iget-object v0, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)LXI;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)LXI;

    move-result-object v0

    invoke-virtual {v0}, LXI;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 172
    iget-object v0, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->C()V

    .line 173
    iget-object v0, p0, LXG;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V

    .line 175
    :cond_0
    return-void
.end method
