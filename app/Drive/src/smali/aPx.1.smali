.class public final LaPx;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field public final a:LaNu;

.field private a:LaPB;

.field private a:Lcom/google/android/gms/internal/fc;

.field private final a:Ljava/lang/Object;

.field private a:Ljava/lang/String;

.field public final b:LaNu;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaPx;->a:Ljava/lang/Object;

    const/4 v0, -0x2

    iput v0, p0, LaPx;->a:I

    new-instance v0, LaPy;

    invoke-direct {v0, p0}, LaPy;-><init>(LaPx;)V

    iput-object v0, p0, LaPx;->a:LaNu;

    new-instance v0, LaPz;

    invoke-direct {v0, p0}, LaPz;-><init>(LaPx;)V

    iput-object v0, p0, LaPx;->b:LaNu;

    iput-object p1, p0, LaPx;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(LaPx;I)I
    .locals 0

    iput p1, p0, LaPx;->a:I

    return p1
.end method

.method static synthetic a(LaPx;LaPB;)LaPB;
    .locals 0

    iput-object p1, p0, LaPx;->a:LaPB;

    return-object p1
.end method

.method static synthetic a(LaPx;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LaPx;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(LaPx;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaPx;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v1, p0, LaPx;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, LaPx;->a:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()LaPB;
    .locals 3

    iget-object v1, p0, LaPx;->a:Ljava/lang/Object;

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-object v0, p0, LaPx;->a:LaPB;

    if-nez v0, :cond_0

    iget v0, p0, LaPx;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, -0x2

    if-ne v0, v2, :cond_0

    :try_start_1
    iget-object v0, p0, LaPx;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "Ad request service was interrupted."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    monitor-exit v1

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, LaPx;->a:LaPB;

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/internal/fc;)V
    .locals 2

    iget-object v1, p0, LaPx;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LaPx;->a:Lcom/google/android/gms/internal/fc;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
