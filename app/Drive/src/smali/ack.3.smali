.class Lack;
.super Ljava/lang/Object;
.source "SharingHelper.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Lacr;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lacj;


# direct methods
.method constructor <init>(Lacj;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lack;->a:Lacj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lacr;)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lack;->a:Lacj;

    invoke-virtual {v0}, Lacj;->c()V

    .line 216
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 202
    check-cast p1, Lacr;

    invoke-virtual {p0, p1}, Lack;->a(Lacr;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 205
    instance-of v0, p1, LQn;

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lack;->a:Lacj;

    invoke-static {v0, v6}, Lacj;->a(Lacj;Lacr;)V

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 208
    const-string v0, "SharingHelper"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    const-string v2, "Update error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 209
    iget-object v0, p0, Lack;->a:Lacj;

    invoke-static {v0, v6}, Lacj;->a(Lacj;Lacr;)V

    goto :goto_0
.end method
