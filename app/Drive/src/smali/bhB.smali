.class public final enum LbhB;
.super Ljava/lang/Enum;
.source "QuadEditorView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbhB;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbhB;

.field private static final synthetic a:[LbhB;

.field public static final enum b:LbhB;

.field public static final enum c:LbhB;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 111
    new-instance v0, LbhB;

    const-string v1, "MOVE_CORNER"

    invoke-direct {v0, v1, v2}, LbhB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbhB;->a:LbhB;

    .line 112
    new-instance v0, LbhB;

    const-string v1, "MOVE_LINE"

    invoke-direct {v0, v1, v3}, LbhB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbhB;->b:LbhB;

    .line 113
    new-instance v0, LbhB;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LbhB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbhB;->c:LbhB;

    .line 110
    const/4 v0, 0x3

    new-array v0, v0, [LbhB;

    sget-object v1, LbhB;->a:LbhB;

    aput-object v1, v0, v2

    sget-object v1, LbhB;->b:LbhB;

    aput-object v1, v0, v3

    sget-object v1, LbhB;->c:LbhB;

    aput-object v1, v0, v4

    sput-object v0, LbhB;->a:[LbhB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbhB;
    .locals 1

    .prologue
    .line 110
    const-class v0, LbhB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbhB;

    return-object v0
.end method

.method public static values()[LbhB;
    .locals 1

    .prologue
    .line 110
    sget-object v0, LbhB;->a:[LbhB;

    invoke-virtual {v0}, [LbhB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbhB;

    return-object v0
.end method
