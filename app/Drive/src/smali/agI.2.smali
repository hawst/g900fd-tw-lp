.class public abstract LagI;
.super Ljava/lang/Object;
.source "ForwardingFeedProcessor.java"

# interfaces
.implements Lagy;


# instance fields
.field private final a:Lagy;


# direct methods
.method protected constructor <init>(Lagy;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagy;

    iput-object v0, p0, LagI;->a:Lagy;

    .line 26
    return-void
.end method


# virtual methods
.method protected final a()Lagy;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, LagI;->a:Lagy;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LagI;->a:Lagy;

    invoke-interface {v0}, Lagy;->a()V

    .line 41
    return-void
.end method

.method public a(LaeD;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LagI;->a:Lagy;

    invoke-interface {v0, p1}, Lagy;->a(LaeD;)V

    .line 31
    return-void
.end method

.method public a(LaeD;LaJT;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LagI;->a:Lagy;

    invoke-interface {v0, p1, p2}, Lagy;->a(LaeD;LaJT;)V

    .line 51
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LagI;->a:Lagy;

    invoke-interface {v0, p1}, Lagy;->a(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public b(LaeD;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LagI;->a:Lagy;

    invoke-interface {v0, p1}, Lagy;->b(LaeD;)V

    .line 36
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    const-string v0, "ForwardingFeedProcessor[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LagI;->a:Lagy;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
