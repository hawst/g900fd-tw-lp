.class public LaKi;
.super Ljava/lang/Object;
.source "DriveApiFactory.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LTd;


# direct methods
.method public constructor <init>(LTd;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, LaKi;->a:LTd;

    .line 33
    return-void
.end method


# virtual methods
.method public a(LaFO;Ljava/util/List;)Lcom/google/api/services/drive/Drive;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/api/services/drive/Drive;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {}, LamV;->b()V

    .line 39
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "oauth2: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-static {v1}, LbiI;->a(Ljava/lang/String;)LbiI;

    move-result-object v1

    invoke-virtual {v1, p2}, LbiI;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    iget-object v1, p0, LaKi;->a:LTd;

    invoke-interface {v1, p1, v0}, LTd;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/google/api/services/drive/Drive$Builder;

    invoke-static {}, Lbdi;->a()Lbeq;

    move-result-object v2

    new-instance v3, LbeY;

    invoke-direct {v3}, LbeY;-><init>()V

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/api/services/drive/Drive$Builder;-><init>(Lbeq;LbeO;Lbek;)V

    new-instance v2, LaKf;

    invoke-direct {v2, v0}, LaKf;-><init>(Ljava/lang/String;)V

    .line 43
    invoke-virtual {v1, v2}, Lcom/google/api/services/drive/Drive$Builder;->a(LbdM;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive$Builder;->a()Lcom/google/api/services/drive/Drive;

    move-result-object v0

    return-object v0

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
