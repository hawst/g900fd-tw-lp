.class public LAW;
.super Ljava/lang/Object;
.source "DocListControllerImpl.java"


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LTT;

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 757
    new-instance v0, Lbna;

    invoke-direct {v0}, Lbna;-><init>()V

    sget-object v1, LaGv;->b:LaGv;

    .line 758
    invoke-virtual {v0, v1}, Lbna;->a(Ljava/lang/Object;)Lbna;

    move-result-object v0

    sget-object v1, LaGv;->i:LaGv;

    .line 759
    invoke-virtual {v0, v1}, Lbna;->a(Ljava/lang/Object;)Lbna;

    move-result-object v0

    .line 760
    invoke-virtual {v0}, Lbna;->a()LbmY;

    move-result-object v0

    sput-object v0, LAW;->a:Ljava/util/Set;

    .line 757
    return-void
.end method

.method public constructor <init>(LtK;LTT;)V
    .locals 0

    .prologue
    .line 767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 768
    iput-object p1, p0, LAW;->a:LtK;

    .line 769
    iput-object p2, p0, LAW;->a:LTT;

    .line 770
    return-void
.end method

.method private a(LaGu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 782
    invoke-interface {p1}, LaGu;->a()LaGn;

    move-result-object v1

    .line 783
    sget-object v2, LaGn;->b:LaGn;

    invoke-virtual {v2, v1}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 788
    :cond_0
    :goto_0
    return v0

    .line 787
    :cond_1
    sget-object v1, LAW;->a:Ljava/util/Set;

    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LAW;->a:LTT;

    .line 788
    invoke-interface {v1, p1}, LTT;->c(LaGu;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 773
    instance-of v1, p1, LaGo;

    if-nez v1, :cond_1

    .line 778
    :cond_0
    :goto_0
    return v0

    .line 776
    :cond_1
    iget-object v1, p0, LAW;->a:LtK;

    sget-object v2, Lry;->z:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    .line 777
    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, LAW;->a(LaGu;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 778
    invoke-virtual {v1, p2}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
