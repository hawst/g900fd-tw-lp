.class public LaUb;
.super LaRw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaRw",
        "<",
        "LaUl;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;LaCX;LaCY;)V
    .locals 6

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, LaRw;-><init>(Landroid/content/Context;Landroid/os/Looper;LaCX;LaCY;[Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/google/android/gms/internal/gw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaSc;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/gms/internal/gw;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaUb;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)LaUl;
    .locals 1

    invoke-static {p1}, LaUm;->a(Landroid/os/IBinder;)LaUl;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, LaUb;->a(Landroid/os/IBinder;)LaUl;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.photos.autobackup.service.START"

    return-object v0
.end method

.method public a(LaDk;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "Lbav;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p0}, LaUb;->e()V

    new-instance v1, LaUd;

    invoke-direct {v1, p0, p1, v3}, LaUd;-><init>(LaUb;LaDk;LaUc;)V

    :try_start_0
    invoke-virtual {p0}, LaUb;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LaUl;

    invoke-interface {v0, v1}, LaUl;->a(LaUi;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, LaUd;->a(IZLjava/lang/String;)V

    goto :goto_0
.end method

.method protected a(LaRS;LaRA;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x4da6e8

    invoke-virtual {p0}, LaUb;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, LaRS;->n(LaRP;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    return-object v0
.end method

.method public b(LaDk;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "Lbaw;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p0}, LaUb;->e()V

    new-instance v1, LaUf;

    invoke-direct {v1, p0, p1, v2}, LaUf;-><init>(LaUb;LaDk;LaUc;)V

    :try_start_0
    invoke-virtual {p0}, LaUb;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LaUl;

    invoke-interface {v0, v1}, LaUl;->b(LaUi;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2}, LaUf;->a(ILandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public c(LaDk;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "Lbaw;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p0}, LaUb;->e()V

    new-instance v1, LaUf;

    invoke-direct {v1, p0, p1, v3}, LaUf;-><init>(LaUb;LaDk;LaUc;)V

    :try_start_0
    invoke-virtual {p0}, LaUb;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LaUl;

    iget-object v2, p0, LaUb;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LaUl;->a(LaUi;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v3}, LaUf;->a(ILandroid/app/PendingIntent;)V

    goto :goto_0
.end method
