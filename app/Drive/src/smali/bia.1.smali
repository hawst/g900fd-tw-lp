.class public final Lbia;
.super Ljava/lang/Thread;
.source "PictureTracker.java"


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/util/PictureTracker;)V
    .locals 1

    .prologue
    .line 106
    iput-object p1, p0, Lbia;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    .line 107
    const-string v0, "Picture Reaper"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 108
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lbia;->setPriority(I)V

    .line 109
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbia;->setDaemon(Z)V

    .line 110
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-static {}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Reaper started"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    :cond_0
    :goto_0
    iget-object v0, p0, Lbia;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    iget-boolean v0, v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbia;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    iget-object v0, v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 124
    :cond_1
    :try_start_0
    iget-object v0, p0, Lbia;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    iget-object v0, v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->remove()Ljava/lang/ref/Reference;

    move-result-object v0

    check-cast v0, Lbib;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0}, Lbib;->a()V

    .line 130
    invoke-virtual {v0}, Lbib;->clear()V

    .line 131
    iget-object v1, p0, Lbia;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    iget-object v1, v1, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    :cond_2
    invoke-static {}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Reaper stopped"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    return-void

    .line 125
    :catch_0
    move-exception v0

    goto :goto_0
.end method
