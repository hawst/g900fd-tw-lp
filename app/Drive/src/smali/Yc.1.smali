.class public LYc;
.super LYs;
.source "PunchStateSvgViewer.java"


# static fields
.field private static a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;


# instance fields
.field private a:LXA;

.field private final a:LYr;

.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final a:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    sput-object v0, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 201
    const-string v0, "PunchStateSvgViewer"

    sget-object v1, LYu;->a:LYu;

    invoke-direct {p0, v0, v1}, LYs;-><init>(Ljava/lang/String;LYu;)V

    .line 45
    new-instance v0, LYd;

    invoke-direct {v0, p0}, LYd;-><init>(LYc;)V

    iput-object v0, p0, LYc;->a:LYr;

    .line 89
    invoke-static {}, LYc;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LYc;->a:Ljava/util/ArrayList;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, LYc;->a:[Z

    .line 202
    return-void

    .line 96
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, LYc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static synthetic a(LYc;I)I
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, LYc;->a(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(LYc;)LYr;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LYc;->a:LYr;

    return-object v0
.end method

.method private static a()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 386
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, LbnG;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LYc;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LYc;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 240
    sget-object v0, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 241
    sget-object v0, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, LYc;->a:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 244
    :cond_0
    return-void
.end method

.method static synthetic a(LYc;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LYc;->f()V

    return-void
.end method

.method static synthetic a(LYc;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, LYc;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;
    .locals 1

    .prologue
    .line 31
    sget-object v0, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    return-object v0
.end method

.method static synthetic a([Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;
    .locals 0

    .prologue
    .line 31
    sput-object p0, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    return-object p0
.end method

.method static synthetic a(LYc;)[Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LYc;->a:[Z

    return-object v0
.end method

.method static synthetic b(LYc;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LYc;->h()V

    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 390
    iget-object v2, p0, LYc;->a:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, LYc;->a:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_0

    iget-object v2, p0, LYc;->a:[Z

    const/4 v3, 0x2

    aget-boolean v2, v2, v3

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method static synthetic c(LYc;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LYc;->g()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, LYc;->a:LXA;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 216
    new-instance v0, LYe;

    invoke-direct {v0, p0}, LYe;-><init>(LYc;)V

    iput-object v0, p0, LYc;->a:LXA;

    .line 232
    iget-object v0, p0, LYc;->a:LXD;

    iget-object v1, p0, LYc;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->a(LXA;)V

    .line 233
    return-void

    .line 215
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, LYc;->a:LXA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 248
    iget-object v0, p0, LYc;->a:LXD;

    iget-object v1, p0, LYc;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->b(LXA;)V

    .line 249
    const/4 v0, 0x0

    iput-object v0, p0, LYc;->a:LXA;

    .line 250
    return-void

    .line 247
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 368
    iget-object v2, p0, LYc;->a:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_0

    iget-object v2, p0, LYc;->a:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, LYc;->a:[Z

    const/4 v3, 0x2

    aget-boolean v2, v2, v3

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, LYc;->a:Z

    .line 369
    return-void

    :cond_0
    move v0, v1

    .line 368
    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, LYc;->a:Ljava/lang/String;

    const-string v1, "in showAllSvgs"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    invoke-static {}, LYc;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LYc;->a:Ljava/util/ArrayList;

    .line 378
    iget-object v0, p0, LYc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()LdN;

    move-result-object v0

    invoke-virtual {v0}, LdN;->a()V

    .line 379
    return-void
.end method

.method private h()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 400
    invoke-direct {p0}, LYc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v0, p0, LYc;->a:Ljava/lang/String;

    const-string v2, "in hideBlankSvgs"

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    iget-object v0, p0, LYc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 408
    invoke-static {}, LYc;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LYc;->a:Ljava/util/ArrayList;

    .line 413
    iget-object v2, p0, LYc;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_3

    .line 414
    sget-object v3, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 415
    iget-object v3, p0, LYc;->a:Ljava/lang/String;

    const-string v4, "Hidden SVG at position %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 416
    iget-object v3, p0, LYc;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 413
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 420
    :cond_3
    iget-object v2, p0, LYc;->a:Ljava/lang/String;

    const-string v3, "SVG list after hiding blanks: %s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, LYc;->a:Ljava/util/ArrayList;

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 421
    iget-object v2, p0, LYc;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_5

    .line 423
    iget-object v0, p0, LYc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()LdN;

    move-result-object v0

    invoke-virtual {v0}, LdN;->a()V

    goto :goto_0

    .line 427
    :cond_4
    add-int/lit8 v1, v1, 0x1

    :cond_5
    iget-object v2, p0, LYc;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 428
    iget-object v2, p0, LYc;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eq v2, v3, :cond_4

    .line 430
    iget-object v0, p0, LYc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()LdN;

    move-result-object v0

    invoke-virtual {v0}, LdN;->a()V

    goto/16 :goto_0
.end method


# virtual methods
.method a()LdN;
    .locals 1

    .prologue
    .line 272
    new-instance v0, LYg;

    invoke-direct {v0, p0}, LYg;-><init>(LYc;)V

    return-object v0
.end method

.method protected a()LeT;
    .locals 1

    .prologue
    .line 289
    new-instance v0, LYf;

    invoke-direct {v0, p0}, LYf;-><init>(LYc;)V

    return-object v0
.end method

.method a()V
    .locals 0

    .prologue
    .line 442
    return-void
.end method

.method public a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)V
    .locals 0

    .prologue
    .line 260
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, LYc;->a:LXR;

    invoke-interface {v0}, LXR;->c()V

    .line 268
    return-void
.end method

.method a(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 465
    if-eqz p1, :cond_1

    .line 466
    iput-boolean v0, p0, LYc;->b:Z

    .line 467
    sget-object v1, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 468
    if-eqz v3, :cond_0

    .line 469
    invoke-virtual {v3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->e()V

    .line 467
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475
    :cond_1
    iget-boolean v1, p0, LYc;->b:Z

    if-nez v1, :cond_3

    .line 476
    sget-object v1, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 477
    if-eqz v3, :cond_2

    .line 478
    invoke-virtual {v3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->f()V

    .line 476
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 482
    :cond_3
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 446
    invoke-virtual {p0, v0}, LYc;->b(Z)V

    .line 447
    iget-object v1, p0, LYc;->a:Landroid/support/v4/view/ViewPager;

    if-nez v1, :cond_1

    .line 461
    :cond_0
    return-void

    .line 451
    :cond_1
    iput-object v5, p0, LYc;->a:Landroid/support/v4/view/ViewPager;

    .line 452
    sget-object v2, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 453
    if-eqz v4, :cond_2

    .line 454
    invoke-virtual {v4}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a()V

    .line 452
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 458
    :cond_3
    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 459
    sget-object v1, LYc;->a:[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    aput-object v5, v1, v0

    .line 458
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0, p1}, LYs;->b(Z)V

    .line 207
    iget-boolean v0, p0, LYc;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LYc;->a:LXA;

    if-nez v0, :cond_1

    .line 208
    invoke-direct {p0}, LYc;->d()V

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-boolean v0, p0, LYc;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LYc;->a:LXA;

    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0}, LYc;->e()V

    goto :goto_0
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 490
    invoke-super {p0}, LYs;->c()V

    .line 491
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LYc;->a(IZ)V

    .line 492
    return-void
.end method
