.class LacO;
.super Landroid/widget/Filter;
.source "SingleTokenContactFilterAdapter.java"


# instance fields
.field final synthetic a:LacL;


# direct methods
.method private constructor <init>(LacL;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, LacO;->a:LacL;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LacL;LacM;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, LacO;-><init>(LacL;)V

    return-void
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 130
    check-cast p1, Labs;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    iget-object v1, p0, LacO;->a:LacL;

    invoke-static {v1}, LacL;->a(LacL;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 135
    iget-object v1, p0, LacO;->a:LacL;

    invoke-static {v1}, LacL;->a(LacL;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {p1}, Labs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 150
    :goto_0
    return-object v0

    .line 142
    :cond_0
    invoke-virtual {p1}, Labs;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 144
    if-nez v1, :cond_1

    .line 145
    invoke-virtual {p1}, Labs;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string v1, " <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Labs;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_1
    invoke-virtual {p1}, Labs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 56
    new-instance v2, Landroid/widget/Filter$FilterResults;

    invoke-direct {v2}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 59
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v3, p0, LacO;->a:LacL;

    invoke-static {v3}, LacL;->a(LacL;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v0, v3, :cond_0

    iget-object v0, p0, LacO;->a:LacL;

    .line 60
    invoke-static {v0}, LacL;->a(LacL;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LacO;->a:LacL;

    invoke-static {v3}, LacL;->a(LacL;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {p1, v1, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 62
    :goto_0
    if-eqz v0, :cond_2

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 64
    iput v1, v2, Landroid/widget/Filter$FilterResults;->count:I

    .line 111
    :goto_1
    return-object v2

    :cond_1
    move v0, v1

    .line 60
    goto :goto_0

    .line 67
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LacO;->a:LacL;

    invoke-static {v1}, LacL;->a(LacL;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 73
    new-instance v0, LacN;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, LacN;-><init>(Ljava/lang/String;Labs;)V

    .line 74
    iget-object v3, p0, LacO;->a:LacL;

    invoke-static {v3}, LacL;->a(LacL;)[LacN;

    move-result-object v3

    invoke-static {v3, v0}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 83
    :goto_2
    if-lez v0, :cond_3

    iget-object v3, p0, LacO;->a:LacL;

    .line 84
    invoke-static {v3}, LacL;->a(LacL;)[LacN;

    move-result-object v3

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    iget-object v3, v3, LacN;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 85
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 91
    :cond_3
    if-ltz v0, :cond_5

    .line 95
    :goto_3
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 97
    :goto_4
    iget-object v4, p0, LacO;->a:LacL;

    .line 96
    invoke-static {v4}, LacL;->a(LacL;)[LacN;

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_4

    iget-object v4, p0, LacO;->a:LacL;

    .line 97
    invoke-static {v4}, LacL;->a(LacL;)[LacN;

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v4, v4, LacN;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 98
    iget-object v4, p0, LacO;->a:LacL;

    invoke-static {v4}, LacL;->a(LacL;)[LacN;

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v4, v4, LacN;->a:Labs;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    const/4 v5, 0x4

    if-lt v4, v5, :cond_6

    .line 104
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 107
    iput-object v0, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 108
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    goto/16 :goto_1

    .line 91
    :cond_5
    add-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    goto :goto_3

    .line 97
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, LacO;->a:LacL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LacL;->setNotifyOnChange(Z)V

    .line 117
    iget-object v0, p0, LacO;->a:LacL;

    invoke-virtual {v0}, LacL;->clear()V

    .line 119
    if-nez p2, :cond_0

    .line 120
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 122
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labs;

    .line 123
    iget-object v2, p0, LacO;->a:LacL;

    invoke-virtual {v2, v0}, LacL;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 120
    :cond_0
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    goto :goto_0

    .line 125
    :cond_1
    iget-object v0, p0, LacO;->a:LacL;

    invoke-virtual {v0}, LacL;->notifyDataSetChanged()V

    .line 126
    return-void
.end method
