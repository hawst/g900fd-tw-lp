.class final LaDl;
.super Ljava/lang/Object;

# interfaces
.implements LaCV;


# instance fields
.field private a:I

.field private a:J

.field private a:LaCC;

.field final a:LaCX;

.field private final a:LaDq;

.field private final a:LaRF;

.field private final a:LaRH;

.field private final a:Landroid/os/Bundle;

.field final a:Landroid/os/Handler;

.field private final a:Landroid/os/Looper;

.field private final a:Landroid/support/v4/app/Fragment;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaCT",
            "<*>;",
            "LaCR;",
            ">;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LaDs",
            "<*>;>;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaDs",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/locks/Condition;

.field private final a:Ljava/util/concurrent/locks/Lock;

.field private a:Z

.field private b:I

.field private b:Z

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;Ljava/util/Map;Landroid/support/v4/app/Fragment;Ljava/util/Set;Ljava/util/Set;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "Lcom/google/android/gms/internal/gw;",
            "Ljava/util/Map",
            "<",
            "LaCM",
            "<*>;",
            "LaCN;",
            ">;",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/util/Set",
            "<",
            "LaCX;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LaCY;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Condition;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LaDl;->a:Ljava/util/Queue;

    const/4 v0, 0x4

    iput v0, p0, LaDl;->b:I

    const/4 v0, 0x0

    iput v0, p0, LaDl;->c:I

    const/4 v0, 0x0

    iput-boolean v0, p0, LaDl;->a:Z

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, LaDl;->a:J

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LaDl;->a:Landroid/os/Bundle;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaDl;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaDl;->a:Ljava/util/Set;

    new-instance v0, LaDm;

    invoke-direct {v0, p0}, LaDm;-><init>(LaDl;)V

    iput-object v0, p0, LaDl;->a:LaDq;

    new-instance v0, LaDn;

    invoke-direct {v0, p0}, LaDn;-><init>(LaDl;)V

    iput-object v0, p0, LaDl;->a:LaCX;

    new-instance v0, LaDo;

    invoke-direct {v0, p0}, LaDo;-><init>(LaDl;)V

    iput-object v0, p0, LaDl;->a:LaRH;

    new-instance v0, LaRF;

    iget-object v1, p0, LaDl;->a:LaRH;

    invoke-direct {v0, p1, p2, v1}, LaRF;-><init>(Landroid/content/Context;Landroid/os/Looper;LaRH;)V

    iput-object v0, p0, LaDl;->a:LaRF;

    iput-object p5, p0, LaDl;->a:Landroid/support/v4/app/Fragment;

    iput-object p2, p0, LaDl;->a:Landroid/os/Looper;

    new-instance v0, LaDr;

    invoke-direct {v0, p0, p2}, LaDr;-><init>(LaDl;Landroid/os/Looper;)V

    iput-object v0, p0, LaDl;->a:Landroid/os/Handler;

    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCX;

    iget-object v2, p0, LaDl;->a:LaRF;

    invoke-virtual {v2, v0}, LaRF;->a(LaCX;)V

    goto :goto_0

    :cond_0
    invoke-interface/range {p7 .. p7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCY;

    iget-object v2, p0, LaDl;->a:LaRF;

    invoke-virtual {v2, v0}, LaRF;->a(LaCG;)V

    goto :goto_1

    :cond_1
    invoke-interface {p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, LaCM;

    invoke-virtual {v2}, LaCM;->a()LaCS;

    move-result-object v0

    invoke-interface {p4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v8, p0, LaDl;->a:Ljava/util/Map;

    invoke-virtual {v2}, LaCM;->a()LaCT;

    move-result-object v9

    iget-object v5, p0, LaDl;->a:LaCX;

    new-instance v6, LaDp;

    invoke-direct {v6, p0, v0}, LaDp;-><init>(LaDl;LaCS;)V

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v6}, LaDl;->a(LaCS;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;LaCX;LaCY;)LaCR;

    move-result-object v0

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    invoke-virtual {p3}, Lcom/google/android/gms/internal/gw;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LaDl;->a:Ljava/util/List;

    return-void
.end method

.method static synthetic a(LaDl;)I
    .locals 1

    iget v0, p0, LaDl;->b:I

    return v0
.end method

.method static synthetic a(LaDl;I)I
    .locals 0

    iput p1, p0, LaDl;->c:I

    return p1
.end method

.method static synthetic a(LaDl;)J
    .locals 2

    iget-wide v0, p0, LaDl;->a:J

    return-wide v0
.end method

.method static synthetic a(LaDl;)LaCC;
    .locals 1

    iget-object v0, p0, LaDl;->a:LaCC;

    return-object v0
.end method

.method static synthetic a(LaDl;LaCC;)LaCC;
    .locals 0

    iput-object p1, p0, LaDl;->a:LaCC;

    return-object p1
.end method

.method private static a(LaCS;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;LaCX;LaCY;)LaCR;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "LaCR;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LaCS",
            "<TC;TO;>;",
            "Ljava/lang/Object;",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "Lcom/google/android/gms/internal/gw;",
            "LaCX;",
            "LaCY;",
            ")TC;"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, LaCS;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;Ljava/lang/Object;LaCX;LaCY;)LaCR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaDl;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, LaDl;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic a(LaDl;)Ljava/util/concurrent/locks/Lock;
    .locals 1

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method private a(I)V
    .locals 5

    const/4 v1, 0x3

    const/4 v3, 0x1

    const/4 v4, -0x1

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, LaDl;->b:I

    if-eq v0, v1, :cond_a

    if-ne p1, v4, :cond_4

    invoke-virtual {p0}, LaDl;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaDl;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaDs;

    invoke-interface {v0}, LaDs;->a()I

    move-result v2

    if-eq v2, v3, :cond_0

    invoke-interface {v0}, LaDs;->a()V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LaDl;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    :cond_2
    iget-object v0, p0, LaDl;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaDs;

    invoke-interface {v0}, LaDs;->a()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, LaDl;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, LaDl;->a:LaCC;

    if-nez v0, :cond_4

    iget-object v0, p0, LaDl;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, LaDl;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_2
    return-void

    :cond_4
    :try_start_2
    invoke-virtual {p0}, LaDl;->b()Z

    move-result v0

    invoke-virtual {p0}, LaDl;->a()Z

    move-result v1

    const/4 v2, 0x3

    iput v2, p0, LaDl;->b:I

    if-eqz v0, :cond_6

    if-ne p1, v4, :cond_5

    const/4 v0, 0x0

    iput-object v0, p0, LaDl;->a:LaCC;

    :cond_5
    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, LaDl;->b:Z

    iget-object v0, p0, LaDl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCR;

    invoke-interface {v0}, LaCR;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, LaCR;->b()V

    goto :goto_3

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, LaDl;->b:Z

    const/4 v0, 0x4

    iput v0, p0, LaDl;->b:I

    if-eqz v1, :cond_a

    if-eq p1, v4, :cond_9

    iget-object v0, p0, LaDl;->a:LaRF;

    invoke-virtual {v0, p1}, LaRF;->a(I)V

    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, LaDl;->b:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_a
    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2
.end method

.method static synthetic a(LaDl;)V
    .locals 0

    invoke-direct {p0}, LaDl;->d()V

    return-void
.end method

.method static synthetic a(LaDl;I)V
    .locals 0

    invoke-direct {p0, p1}, LaDl;->a(I)V

    return-void
.end method

.method private a(LaDs;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LaCR;",
            ">(",
            "LaDs",
            "<TA;>;)V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, LaDl;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, LaDl;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    const-string v3, "GoogleApiClient is not connected yet."

    invoke-static {v2, v3}, LaSc;->a(ZLjava/lang/Object;)V

    invoke-interface {p1}, LaDs;->a()LaCT;

    move-result-object v2

    if-eqz v2, :cond_2

    :goto_1
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, LaSc;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, LaDl;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LaDl;->a:LaDq;

    invoke-interface {p1, v0}, LaDs;->a(LaDq;)V

    invoke-direct {p0}, LaDl;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, LaDs;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_2
    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    :try_start_1
    invoke-interface {p1}, LaDs;->a()LaCT;

    move-result-object v0

    invoke-virtual {p0, v0}, LaDl;->a(LaCT;)LaCR;

    move-result-object v0

    invoke-interface {p1, v0}, LaDs;->b(LaCR;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic a(LaDl;)Z
    .locals 1

    invoke-direct {p0}, LaDl;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic b(LaDl;)I
    .locals 1

    iget v0, p0, LaDl;->a:I

    return v0
.end method

.method static synthetic b(LaDl;I)I
    .locals 0

    iput p1, p0, LaDl;->a:I

    return p1
.end method

.method static synthetic b(LaDl;)Z
    .locals 1

    iget-boolean v0, p0, LaDl;->b:Z

    return v0
.end method

.method private c()Z
    .locals 2

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, LaDl;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private d()V
    .locals 4

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, LaDl;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LaDl;->d:I

    iget v0, p0, LaDl;->d:I

    if-nez v0, :cond_1

    iget-object v0, p0, LaDl;->a:LaCC;

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, LaDl;->a:Z

    const/4 v0, 0x3

    invoke-direct {p0, v0}, LaDl;->a(I)V

    invoke-direct {p0}, LaDl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LaDl;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LaDl;->c:I

    :cond_0
    invoke-direct {p0}, LaDl;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LaDl;->a:Landroid/os/Handler;

    iget-object v1, p0, LaDl;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, LaDl;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LaDl;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, LaDl;->a:LaRF;

    iget-object v1, p0, LaDl;->a:LaCC;

    invoke-virtual {v0, v1}, LaRF;->a(LaCC;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_3
    const/4 v0, 0x2

    :try_start_2
    iput v0, p0, LaDl;->b:I

    invoke-direct {p0}, LaDl;->f()V

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, LaDl;->e()V

    iget-boolean v0, p0, LaDl;->a:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    iput-boolean v0, p0, LaDl;->a:Z

    const/4 v0, -0x1

    invoke-direct {p0, v0}, LaDl;->a(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, LaDl;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, LaDl;->a:LaRF;

    invoke-virtual {v1, v0}, LaRF;->a(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, LaDl;->a:Landroid/os/Bundle;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private e()V
    .locals 3

    invoke-virtual {p0}, LaDl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LaDl;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, LaSc;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :goto_1
    :try_start_0
    iget-object v0, p0, LaDl;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v0, p0, LaDl;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaDs;

    invoke-direct {p0, v0}, LaDl;->a(LaDs;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "GoogleApiClientImpl"

    const-string v2, "Service died while flushing queue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, LaDl;->c:I

    iget-object v0, p0, LaDl;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public a(LaCT;)LaCR;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "LaCR;",
            ">(",
            "LaCT",
            "<TC;>;)TC;"
        }
    .end annotation

    iget-object v0, p0, LaDl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCR;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, LaSc;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public a(LaDi;)LaDi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LaCR;",
            "T:",
            "LaDi",
            "<+",
            "LaDc;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, LaDl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LaDl;->b(LaDi;)LaDi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :cond_0
    :try_start_1
    iget-object v0, p0, LaDl;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LaDl;->a:Z

    invoke-virtual {p0}, LaDl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaDl;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LaDl;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, LaDl;->a:LaCC;

    const/4 v0, 0x1

    iput v0, p0, LaDl;->b:I

    iget-object v0, p0, LaDl;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, LaDl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, LaDl;->d:I

    iget-object v0, p0, LaDl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCR;

    invoke-interface {v0}, LaCR;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public a(LaCX;)V
    .locals 1

    iget-object v0, p0, LaDl;->a:LaRF;

    invoke-virtual {v0, p1}, LaRF;->a(LaCX;)V

    return-void
.end method

.method public a(LaCY;)V
    .locals 1

    iget-object v0, p0, LaDl;->a:LaRF;

    invoke-virtual {v0, p1}, LaRF;->a(LaCG;)V

    return-void
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, LaDl;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public b(LaDi;)LaDi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LaCR;",
            "T:",
            "LaDi",
            "<+",
            "LaDc;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, LaDl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LaDl;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, LaSc;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, LaDl;->e()V

    :try_start_0
    invoke-direct {p0, p1}, LaDl;->a(LaDs;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v1}, LaDl;->a(I)V

    goto :goto_1
.end method

.method public b()V
    .locals 1

    invoke-direct {p0}, LaDl;->f()V

    const/4 v0, -0x1

    invoke-direct {p0, v0}, LaDl;->a(I)V

    return-void
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v1, p0, LaDl;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaDl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public c()V
    .locals 0

    invoke-virtual {p0}, LaDl;->b()V

    invoke-virtual {p0}, LaDl;->a()V

    return-void
.end method
