.class LafO;
.super LagI;
.source "ChangeFeedProcessor.java"


# instance fields
.field private final a:LaFM;

.field private final a:LaGg;


# direct methods
.method constructor <init>(LaGg;LaFM;Lagy;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p3}, LagI;-><init>(Lagy;)V

    .line 33
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFM;

    iput-object v0, p0, LafO;->a:LaFM;

    .line 34
    iput-object p1, p0, LafO;->a:LaGg;

    .line 35
    return-void
.end method


# virtual methods
.method public b(LaeD;)V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1}, LagI;->b(LaeD;)V

    .line 43
    invoke-virtual {p1}, LaeD;->a()I

    move-result v0

    .line 44
    iget-object v1, p0, LafO;->a:LaGg;

    invoke-interface {v1}, LaGg;->a()V

    .line 46
    :try_start_0
    iget-object v1, p0, LafO;->a:LaGg;

    iget-object v2, p0, LafO;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-interface {v1, v2}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, LaFQ;->a()I

    move-result v2

    if-le v0, v2, :cond_0

    .line 48
    invoke-virtual {v1, v0}, LaFQ;->a(I)V

    .line 49
    invoke-virtual {v1}, LaFQ;->e()V

    .line 51
    :cond_0
    iget-object v0, p0, LafO;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    iget-object v0, p0, LafO;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 55
    return-void

    .line 53
    :catchall_0
    move-exception v0

    iget-object v1, p0, LafO;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChangeFeedProcessor["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LafO;->a:LaFM;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
