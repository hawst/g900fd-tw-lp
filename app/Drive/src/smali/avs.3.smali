.class public Lavs;
.super Ljava/lang/Object;
.source "SecurityUpdates.java"


# static fields
.field private static volatile a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lavs;->a:Z

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    sget-boolean v0, Lavs;->a:Z

    if-eqz v0, :cond_0

    .line 37
    :goto_0
    return-void

    .line 25
    :cond_0
    new-instance v0, Lavt;

    invoke-direct {v0}, Lavt;-><init>()V

    invoke-static {p0, v0}, Lbbz;->a(Landroid/content/Context;LbbB;)V

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 16
    sput-boolean p0, Lavs;->a:Z

    return p0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 40
    sget-boolean v0, Lavs;->a:Z

    if-eqz v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 45
    const-string v0, "SecurityUpdates"

    const-string v1, "Security updates are being installed on the UI thread"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_1
    :try_start_0
    invoke-static {p0}, Lbbz;->a(Landroid/content/Context;)V
    :try_end_0
    .catch LaCI; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaCH; {:try_start_0 .. :try_end_0} :catch_1

    .line 54
    :goto_1
    const/4 v0, 0x1

    sput-boolean v0, Lavs;->a:Z

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    const-string v1, "SecurityUpdates"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to install security updates: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LaCI;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 51
    :catch_1
    move-exception v0

    .line 52
    const-string v1, "SecurityUpdates"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to install security updates: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LaCH;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
