.class public Lakv;
.super Ljava/lang/Object;
.source "CancelableFutureCallback.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbsJ",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:LbsJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsJ",
            "<TV;>;"
        }
    .end annotation
.end field

.field private volatile a:Z

.field private volatile b:Z


# direct methods
.method private constructor <init>(LbsJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsJ",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v0, p0, Lakv;->a:Z

    .line 17
    iput-boolean v0, p0, Lakv;->b:Z

    .line 20
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbsJ;

    iput-object v0, p0, Lakv;->a:LbsJ;

    .line 21
    return-void
.end method

.method public static a()Lakv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lakv",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lakw;

    invoke-direct {v0}, Lakw;-><init>()V

    invoke-static {v0}, Lakv;->a(LbsJ;)Lakv;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbsJ;)Lakv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LbsJ",
            "<TV;>;)",
            "Lakv",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lakv;

    invoke-direct {v0, p0}, Lakv;-><init>(LbsJ;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakv;->a:Z

    .line 49
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakv;->b:Z

    .line 62
    iget-boolean v0, p0, Lakv;->a:Z

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lakv;->a:LbsJ;

    invoke-interface {v0, p1}, LbsJ;->a(Ljava/lang/Object;)V

    .line 65
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakv;->b:Z

    .line 54
    iget-boolean v0, p0, Lakv;->a:Z

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lakv;->a:LbsJ;

    invoke-interface {v0, p1}, LbsJ;->a(Ljava/lang/Throwable;)V

    .line 57
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lakv;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lakv;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
