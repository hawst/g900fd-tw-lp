.class public Lapy;
.super Landroid/database/DataSetObserver;
.source "DocListView.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/view/DocListView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/view/DocListView;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lapy;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lapy;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v0, v0, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBa;

    invoke-virtual {v0}, LBa;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 437
    iget-object v0, p0, Lapy;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;)Lcom/google/android/apps/docs/view/StickyHeaderView;

    move-result-object v0

    iget-object v1, p0, Lapy;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v1}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/StickyHeaderView;->a(Landroid/widget/AbsListView;)V

    .line 441
    :goto_0
    return-void

    .line 439
    :cond_0
    iget-object v0, p0, Lapy;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;)Lcom/google/android/apps/docs/view/StickyHeaderView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/StickyHeaderView;->setVisibility(I)V

    goto :goto_0
.end method
