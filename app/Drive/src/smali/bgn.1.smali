.class public Lbgn;
.super Ljava/lang/Object;
.source "CaptureActivity.java"


# instance fields
.field private a:I

.field private a:Lbgm;

.field final synthetic a:Lcom/google/bionics/scanner/CaptureActivity;

.field private a:Z


# direct methods
.method private constructor <init>(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 1

    .prologue
    .line 673
    iput-object p1, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 688
    const/4 v0, 0x0

    iput v0, p0, Lbgn;->a:I

    .line 692
    sget-object v0, Lbgm;->a:Lbgm;

    iput-object v0, p0, Lbgn;->a:Lbgm;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/bionics/scanner/CaptureActivity;Lbgk;)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lbgn;-><init>(Lcom/google/bionics/scanner/CaptureActivity;)V

    return-void
.end method

.method private a(Lbgm;)V
    .locals 5

    .prologue
    .line 695
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Enter state %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lbgm;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 696
    iput-object p1, p0, Lbgn;->a:Lbgm;

    .line 697
    return-void
.end method

.method static synthetic a(Lbgn;I)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lbgn;->e(I)V

    return-void
.end method

.method public static synthetic a(Lbgn;Z)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lbgn;->a(Z)V

    return-void
.end method

.method private a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 3

    .prologue
    .line 1106
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "takePictureNow"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1107
    sget-object v0, Lbgm;->i:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 1108
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->b(Lcom/google/bionics/scanner/CaptureActivity;)V

    .line 1109
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->g(Lcom/google/bionics/scanner/CaptureActivity;)V

    .line 1110
    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1068
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleFocusEvent(%b)"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1069
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1100
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignore focus event in state %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1103
    :goto_0
    return-void

    .line 1071
    :pswitch_0
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Touch-to-focus complete"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->time(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1072
    invoke-direct {p0}, Lbgn;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1073
    sget-object v0, Lbgm;->b:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    goto :goto_0

    .line 1075
    :cond_0
    sget-object v0, Lbgm;->f:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    goto :goto_0

    .line 1080
    :pswitch_1
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Query focus complete"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->time(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1081
    invoke-direct {p0}, Lbgn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1082
    sget-object v0, Lbgm;->b:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    goto :goto_0

    .line 1084
    :cond_1
    sget-object v0, Lbgm;->f:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    goto :goto_0

    .line 1089
    :pswitch_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbgn;->a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V

    goto :goto_0

    .line 1096
    :pswitch_3
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Unexpected focus transition from state %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1069
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 760
    iget v0, p0, Lbgn;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)V
    .locals 5

    .prologue
    .line 745
    iget v0, p0, Lbgn;->a:I

    or-int/2addr v0, p1

    iput v0, p0, Lbgn;->a:I

    .line 746
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "buttonsDown <- %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbgn;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 747
    return-void
.end method

.method static synthetic b(Lbgn;I)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lbgn;->f(I)V

    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 764
    iget v0, p0, Lbgn;->a:I

    and-int/lit8 v0, v0, 0xf

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)V
    .locals 5

    .prologue
    .line 750
    iget v0, p0, Lbgn;->a:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lbgn;->a:I

    .line 751
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "buttonsDown <- %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbgn;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 752
    return-void
.end method

.method static synthetic c(Lbgn;I)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lbgn;->d(I)V

    return-void
.end method

.method private d(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 898
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleTouchOutEvent(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 899
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 915
    sparse-switch p1, :sswitch_data_0

    .line 926
    :goto_0
    return-void

    .line 902
    :pswitch_0
    sparse-switch p1, :sswitch_data_1

    goto :goto_0

    .line 904
    :sswitch_0
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v4}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    .line 905
    sget-object v0, Lbgm;->f:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    goto :goto_0

    .line 909
    :sswitch_1
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->c(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 917
    :sswitch_2
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v4}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    goto :goto_0

    .line 921
    :sswitch_3
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->c(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 899
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch

    .line 915
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch

    .line 902
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method

.method private e(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 929
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleKeyDownEvent(%d)"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 930
    invoke-direct {p0, p1}, Lbgn;->b(I)V

    .line 931
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 999
    :goto_0
    return-void

    .line 933
    :pswitch_0
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 935
    :sswitch_0
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v5}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    .line 940
    :sswitch_1
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Touch down"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->resetTime(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 943
    sget-object v0, Lbgm;->e:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 944
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "keydown -> focus key requesting focus"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 945
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->d(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 949
    :sswitch_2
    sget-object v0, Lbgm;->e:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 950
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "keydown -> focus key requesting focus"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->d(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 956
    :sswitch_3
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->c(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 964
    :pswitch_1
    sparse-switch p1, :sswitch_data_1

    .line 983
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignore key down because in state %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 966
    :sswitch_4
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->c(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 971
    :sswitch_5
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v5}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    goto :goto_0

    .line 974
    :sswitch_6
    iget-object v0, p0, Lbgn;->a:Lbgm;

    sget-object v1, Lbgm;->e:Lbgm;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lbgn;->a:Lbgm;

    sget-object v1, Lbgm;->d:Lbgm;

    if-ne v0, v1, :cond_1

    .line 975
    :cond_0
    sget-object v0, Lbgm;->g:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 976
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Will take a picture after focus completes."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 978
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbgn;->a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V

    goto/16 :goto_0

    .line 996
    :pswitch_2
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignore key down from state %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 931
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 933
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
        0x20 -> :sswitch_3
    .end sparse-switch

    .line 964
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_5
        0x4 -> :sswitch_6
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method private f(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1002
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v3, "handleKeyUpEvent(%d)"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1003
    invoke-direct {p0, p1}, Lbgn;->c(I)V

    .line 1004
    and-int/lit8 v0, p1, 0xf

    if-eqz v0, :cond_1

    move v0, v1

    .line 1005
    :goto_0
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lbgn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1006
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignoring event because other buttons are still down."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1042
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 1004
    goto :goto_0

    .line 1009
    :cond_2
    sget-object v0, Lbgl;->a:[I

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1039
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v3, "Ignore key up from state %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v4}, Lbgm;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1011
    :pswitch_0
    invoke-direct {p0}, Lbgn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1012
    sget-object v0, Lbgm;->b:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 1021
    :cond_3
    :pswitch_1
    invoke-direct {p0}, Lbgn;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1022
    const/4 v0, 0x4

    if-eq p1, v0, :cond_4

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 1023
    :cond_4
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Dpad center or camera pressed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->resetTime(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1024
    invoke-direct {p0}, Lbgn;->j()V

    goto :goto_1

    .line 1009
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 755
    iput v4, p0, Lbgn;->a:I

    .line 756
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "buttonsDown <- %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lbgn;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 757
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 868
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleShutterButtonClickEvent"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 869
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 886
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v4}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    .line 887
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignoring press on shutter button in state %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 890
    :goto_0
    return-void

    .line 877
    :pswitch_0
    invoke-direct {p0}, Lbgn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 878
    invoke-direct {p0}, Lbgn;->j()V

    goto :goto_0

    .line 881
    :cond_0
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v4}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    goto :goto_0

    .line 869
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1045
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleTakePictureEvent"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1047
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1061
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignore take picture request from state %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1065
    :goto_0
    return-void

    .line 1049
    :pswitch_0
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->e(Lcom/google/bionics/scanner/CaptureActivity;)V

    .line 1050
    sget-object v0, Lbgm;->g:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 1051
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "take picture event requesting focus"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1052
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->f(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 1057
    :pswitch_1
    sget-object v0, Lbgm;->g:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    goto :goto_0

    .line 1047
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v0}, Lbgm;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 741
    sget-object v0, Lbgm;->a:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 742
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 857
    sparse-switch p1, :sswitch_data_0

    .line 865
    :goto_0
    return-void

    .line 859
    :sswitch_0
    invoke-virtual {p0}, Lbgn;->g()V

    goto :goto_0

    .line 862
    :sswitch_1
    invoke-direct {p0}, Lbgn;->i()V

    goto :goto_0

    .line 857
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 707
    new-instance v0, Lbgo;

    invoke-direct {v0, p0, p2}, Lbgo;-><init>(Lbgn;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 714
    new-instance v0, Lbgp;

    invoke-direct {v0, p0, p2}, Lbgp;-><init>(Lbgn;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 738
    return-void
.end method

.method public a(Lcom/google/bionics/scanner/unveil/util/Picture;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1113
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handlePictureTakenEvent"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1114
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    .line 1115
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v1

    const-string v2, "Taken picture size: %dx%d"

    new-array v3, v7, [Ljava/lang/Object;

    iget v4, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1117
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "torch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1120
    sget-object v1, Lbgl;->a:[I

    iget-object v2, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v2}, Lbgm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1150
    :goto_0
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignore picture taken event in state %s"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1153
    :goto_1
    return-void

    .line 1123
    :pswitch_0
    if-eqz v0, :cond_0

    .line 1126
    iget-object v1, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v1}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)V

    .line 1128
    :cond_0
    iput-boolean v0, p0, Lbgn;->a:Z

    .line 1130
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v5}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    .line 1131
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Picture taken"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->time(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1132
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/ui/Viewport;

    move-result-object v0

    iget-object v1, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->getNaturalOrientation(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v7, :cond_1

    .line 1134
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getOrientation()I

    move-result v0

    add-int/lit8 v0, v0, -0x5a

    invoke-virtual {p1, v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->setOrientation(I)V

    .line 1137
    :cond_1
    sget-object v0, Lbgm;->h:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 1138
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->forceReleaseCamera()V

    .line 1139
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, p1}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Lcom/google/bionics/scanner/unveil/util/Picture;)V

    goto :goto_1

    .line 1147
    :pswitch_1
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Unexpected picture taken transition from state %s"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1120
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 768
    sget-object v0, Lbgm;->j:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 769
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 776
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleStartEvent"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 777
    iget-object v0, p0, Lbgn;->a:Lbgm;

    sget-object v1, Lbgm;->j:Lbgm;

    if-ne v0, v1, :cond_0

    .line 786
    :goto_0
    return-void

    .line 780
    :cond_0
    iget-object v0, p0, Lbgn;->a:Lbgm;

    sget-object v1, Lbgm;->a:Lbgm;

    if-eq v0, v1, :cond_1

    .line 781
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Unexpected start event in state %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 784
    :cond_1
    sget-object v0, Lbgm;->b:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 785
    invoke-direct {p0}, Lbgn;->h()V

    goto :goto_0
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 789
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleResumeEvent"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 790
    iget-object v0, p0, Lbgn;->a:Lbgm;

    sget-object v1, Lbgm;->j:Lbgm;

    if-ne v0, v1, :cond_1

    .line 814
    :cond_0
    :goto_0
    return-void

    .line 793
    :cond_1
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 795
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Unexpected resume while in state %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 801
    :pswitch_0
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->acquireCamera()V

    .line 802
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->requestLayout()V

    .line 805
    sget-object v0, Lbgm;->b:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 808
    invoke-direct {p0}, Lbgn;->h()V

    .line 809
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0, v4}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;Z)V

    .line 811
    iget-boolean v0, p0, Lbgn;->a:Z

    if-eqz v0, :cond_0

    .line 812
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)V

    goto :goto_0

    .line 793
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 817
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handlePauseEvent"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 818
    iget-object v0, p0, Lbgn;->a:Lbgm;

    sget-object v1, Lbgm;->j:Lbgm;

    if-ne v0, v1, :cond_0

    .line 843
    :goto_0
    return-void

    .line 821
    :cond_0
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 840
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Ignoring pause event in state %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v3}, Lbgm;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 831
    :pswitch_0
    sget-object v0, Lbgm;->c:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 832
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->b(Lcom/google/bionics/scanner/CaptureActivity;)V

    .line 836
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->forceReleaseCamera()V

    goto :goto_0

    .line 821
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public f()V
    .locals 3

    .prologue
    .line 846
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleStopEvent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 847
    sget-object v0, Lbgl;->a:[I

    iget-object v1, p0, Lbgn;->a:Lbgm;

    invoke-virtual {v1}, Lbgm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    .line 851
    sget-object v0, Lbgm;->a:Lbgm;

    invoke-direct {p0, v0}, Lbgn;->a(Lbgm;)V

    .line 854
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 893
    invoke-static {}, Lcom/google/bionics/scanner/CaptureActivity;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "handleFlashButtonClickEvent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 894
    iget-object v0, p0, Lbgn;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)V

    .line 895
    return-void
.end method
