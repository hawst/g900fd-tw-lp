.class public final LOX;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 38
    iput-object p1, p0, LOX;->a:LbrA;

    .line 39
    const-class v0, LPb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LOX;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOX;->a:Lbsk;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :pswitch_0
    new-instance v0, LPb;

    invoke-direct {v0}, LPb;-><init>()V

    .line 78
    iget-object v1, p0, LOX;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LOX;

    .line 79
    invoke-virtual {v1, v0}, LOX;->a(LPb;)V

    .line 80
    return-object v0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x1b3
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 103
    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 60
    const-class v0, LPb;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x2c

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LOX;->a(LbuP;LbuB;)V

    .line 63
    const-class v0, LPb;

    iget-object v1, p0, LOX;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LOX;->a(Ljava/lang/Class;Lbsk;)V

    .line 64
    iget-object v0, p0, LOX;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1b3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 66
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 89
    packed-switch p1, :pswitch_data_0

    .line 97
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :pswitch_0
    check-cast p2, LPb;

    .line 93
    iget-object v0, p0, LOX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LOX;

    .line 94
    invoke-virtual {v0, p2}, LOX;->a(LPb;)V

    .line 99
    return-void

    .line 89
    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_0
    .end packed-switch
.end method

.method public a(LPb;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, LOX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 51
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOX;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 49
    invoke-static {v0, v1}, LOX;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LPb;->a:Landroid/content/Context;

    .line 55
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
