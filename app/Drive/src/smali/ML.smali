.class public final enum LML;
.super Ljava/lang/Enum;
.source "SwipableDocListLayout.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LML;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LML;

.field private static final synthetic a:[LML;

.field public static final enum b:LML;

.field public static final enum c:LML;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, LML;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LML;-><init>(Ljava/lang/String;I)V

    sput-object v0, LML;->a:LML;

    .line 36
    new-instance v0, LML;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, LML;-><init>(Ljava/lang/String;I)V

    sput-object v0, LML;->b:LML;

    .line 37
    new-instance v0, LML;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LML;-><init>(Ljava/lang/String;I)V

    sput-object v0, LML;->c:LML;

    .line 34
    const/4 v0, 0x3

    new-array v0, v0, [LML;

    sget-object v1, LML;->a:LML;

    aput-object v1, v0, v2

    sget-object v1, LML;->b:LML;

    aput-object v1, v0, v3

    sget-object v1, LML;->c:LML;

    aput-object v1, v0, v4

    sput-object v0, LML;->a:[LML;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static synthetic a(F)LML;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, LML;->b(F)LML;

    move-result-object v0

    return-object v0
.end method

.method private static b(F)LML;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    cmpl-float v0, p0, v1

    if-lez v0, :cond_0

    .line 41
    sget-object v0, LML;->a:LML;

    .line 45
    :goto_0
    return-object v0

    .line 42
    :cond_0
    cmpg-float v0, p0, v1

    if-gez v0, :cond_1

    .line 43
    sget-object v0, LML;->b:LML;

    goto :goto_0

    .line 45
    :cond_1
    sget-object v0, LML;->c:LML;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LML;
    .locals 1

    .prologue
    .line 34
    const-class v0, LML;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LML;

    return-object v0
.end method

.method public static values()[LML;
    .locals 1

    .prologue
    .line 34
    sget-object v0, LML;->a:[LML;

    invoke-virtual {v0}, [LML;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LML;

    return-object v0
.end method
