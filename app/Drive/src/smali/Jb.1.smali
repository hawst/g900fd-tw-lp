.class public final enum LJb;
.super Ljava/lang/Enum;
.source "BaseHelpCard.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LJb;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LJb;

.field private static final synthetic a:[LJb;

.field public static final enum b:LJb;

.field public static final enum c:LJb;

.field public static final enum d:LJb;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    new-instance v0, LJb;

    const-string v1, "GOT_IT"

    sget v2, Lxi;->help_card_button_label_got_it:I

    sget v3, Lxb;->ic_check:I

    invoke-direct {v0, v1, v4, v2, v3}, LJb;-><init>(Ljava/lang/String;III)V

    sput-object v0, LJb;->a:LJb;

    new-instance v0, LJb;

    const-string v1, "NOT_NOW"

    sget v2, Lxi;->help_card_button_label_not_now:I

    invoke-direct {v0, v1, v5, v2, v4}, LJb;-><init>(Ljava/lang/String;III)V

    sput-object v0, LJb;->b:LJb;

    .line 64
    new-instance v0, LJb;

    const-string v1, "NO_THANKS"

    sget v2, Lxi;->help_card_button_label_no_thanks:I

    invoke-direct {v0, v1, v6, v2, v4}, LJb;-><init>(Ljava/lang/String;III)V

    sput-object v0, LJb;->c:LJb;

    .line 65
    new-instance v0, LJb;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v7, v4, v4}, LJb;-><init>(Ljava/lang/String;III)V

    sput-object v0, LJb;->d:LJb;

    .line 62
    const/4 v0, 0x4

    new-array v0, v0, [LJb;

    sget-object v1, LJb;->a:LJb;

    aput-object v1, v0, v4

    sget-object v1, LJb;->b:LJb;

    aput-object v1, v0, v5

    sget-object v1, LJb;->c:LJb;

    aput-object v1, v0, v6

    sget-object v1, LJb;->d:LJb;

    aput-object v1, v0, v7

    sput-object v0, LJb;->a:[LJb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 71
    iput p3, p0, LJb;->a:I

    .line 72
    iput p4, p0, LJb;->b:I

    .line 73
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LJb;
    .locals 1

    .prologue
    .line 62
    const-class v0, LJb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LJb;

    return-object v0
.end method

.method public static values()[LJb;
    .locals 1

    .prologue
    .line 62
    sget-object v0, LJb;->a:[LJb;

    invoke-virtual {v0}, [LJb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LJb;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, LJb;->a:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, LJb;->b:I

    return v0
.end method
