.class public abstract LajI;
.super Ljava/lang/Object;
.source "AbstractRunnableListenableFuture.java"

# interfaces
.implements LbsU;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbsU",
        "<TV;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final a:Lbtd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtd",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    iput-object v0, p0, LajI;->a:Lbtd;

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, LajI;->a:Lbtd;

    invoke-virtual {v0, p1, p2}, Lbtd;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 42
    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LajI;->a:Lbtd;

    invoke-virtual {v0, p1}, Lbtd;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, LajI;->a:Lbtd;

    invoke-virtual {v0}, Lbtd;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, LajI;->a:Lbtd;

    invoke-virtual {v0, p1, p2, p3}, Lbtd;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LajI;->a:Lbtd;

    invoke-virtual {v0}, Lbtd;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, LajI;->a:Lbtd;

    invoke-virtual {v0}, Lbtd;->isDone()Z

    move-result v0

    return v0
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 32
    :try_start_0
    invoke-virtual {p0}, LajI;->a()Ljava/lang/Object;

    move-result-object v0

    .line 33
    iget-object v1, p0, LajI;->a:Lbtd;

    invoke-virtual {v1, v0}, Lbtd;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    iget-object v1, p0, LajI;->a:Lbtd;

    invoke-virtual {v1, v0}, Lbtd;->a(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
