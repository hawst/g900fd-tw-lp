.class LyD;
.super Landroid/database/DataSetObserver;
.source "ActivityCard.java"


# instance fields
.field final synthetic a:LyB;


# direct methods
.method constructor <init>(LyB;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, LyD;->a:LyB;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    .line 101
    const-string v0, "ActivityCard"

    const-string v1, "Received change event from delegate"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 102
    iget-object v0, p0, LyD;->a:LyB;

    invoke-virtual {v0}, LyB;->notifyDataSetChanged()V

    .line 103
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, LyD;->a:LyB;

    invoke-virtual {v0}, LyB;->notifyDataSetInvalidated()V

    .line 108
    return-void
.end method
