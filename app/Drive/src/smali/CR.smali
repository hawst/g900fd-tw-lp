.class public LCR;
.super Ljava/lang/Object;
.source "SelectionIconVisibilityUpdater.java"

# interfaces
.implements LDq;


# instance fields
.field private final a:I

.field private final a:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, LCR;->a:I

    .line 20
    iput-boolean p2, p0, LCR;->a:Z

    .line 21
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;LDr;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    iget-boolean v0, p0, LCR;->a:Z

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    .line 42
    :goto_0
    return-void

    .line 28
    :cond_0
    iget v0, p0, LCR;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 29
    sget-object v1, LCS;->a:[I

    invoke-virtual {p2}, LDr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 39
    const-string v0, "SelectionIconVisibilityUpdater"

    const-string v1, "Unhandled ViewState: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 32
    :pswitch_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 36
    :pswitch_1
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
