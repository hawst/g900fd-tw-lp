.class public final enum LaEm;
.super Ljava/lang/Enum;
.source "AppMetadataTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEm;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEm;

.field private static final synthetic a:[LaEm;

.field public static final enum b:LaEm;

.field public static final enum c:LaEm;

.field public static final enum d:LaEm;

.field public static final enum e:LaEm;

.field public static final enum f:LaEm;

.field public static final enum g:LaEm;

.field public static final enum h:LaEm;

.field public static final enum i:LaEm;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0x5a

    .line 47
    new-instance v0, LaEm;

    const-string v1, "APP_NAME"

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "appName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 48
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->a:LaEm;

    .line 54
    new-instance v0, LaEm;

    const-string v1, "ACCOUNT"

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "account"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 56
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 57
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/String;

    sget-object v5, LaEm;->a:LaEm;

    .line 58
    invoke-virtual {v5}, LaEm;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    .line 55
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->b:LaEm;

    .line 66
    new-instance v0, LaEm;

    const-string v1, "MANIFEST_ID"

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "manifestId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 68
    invoke-static {}, LaFa;->a()LaFa;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 69
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 67
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x5f

    .line 70
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x5f

    new-instance v4, LaFG;

    const-string v5, "manifestId"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 72
    invoke-static {}, LaFa;->a()LaFa;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(LaFy;)LaFG;

    move-result-object v4

    .line 71
    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->c:LaEm;

    .line 81
    new-instance v0, LaEm;

    const-string v1, "DB_DIRECTORY_PATH"

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "dbFilePath"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 82
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->d:LaEm;

    .line 90
    new-instance v0, LaEm;

    const-string v1, "DOCOS_USER_DISPLAY_NAME"

    const/4 v2, 0x4

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x61

    new-instance v5, LaFG;

    const-string v6, "docosUserDisplayName"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 91
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->e:LaEm;

    .line 97
    new-instance v0, LaEm;

    const-string v1, "DOCOS_USER_IMAGE_URI"

    const/4 v2, 0x5

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x61

    new-instance v5, LaFG;

    const-string v6, "docosUserImageUri"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 98
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->f:LaEm;

    .line 105
    new-instance v0, LaEm;

    const-string v1, "WEB_FONTS_SYNC_VERSION"

    const/4 v2, 0x6

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x64

    new-instance v5, LaFG;

    const-string v6, "webFontsSyncVersion"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 106
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->g:LaEm;

    .line 113
    new-instance v0, LaEm;

    const-string v1, "LAST_WEB_FONTS_SYNC_TIME"

    const/4 v2, 0x7

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x64

    new-instance v5, LaFG;

    const-string v6, "lastWebFontsSyncTime"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 114
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->h:LaEm;

    .line 122
    new-instance v0, LaEm;

    const-string v1, "LAST_WEB_FONTS_SYNC_WITH_DELETION_TIME"

    const/16 v2, 0x8

    invoke-static {}, LaEl;->b()LaEl;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x65

    new-instance v5, LaFG;

    const-string v6, "lastWebFontsSyncWithDeletionTime"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 123
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEm;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEm;->i:LaEm;

    .line 40
    const/16 v0, 0x9

    new-array v0, v0, [LaEm;

    sget-object v1, LaEm;->a:LaEm;

    aput-object v1, v0, v8

    sget-object v1, LaEm;->b:LaEm;

    aput-object v1, v0, v9

    sget-object v1, LaEm;->c:LaEm;

    aput-object v1, v0, v10

    sget-object v1, LaEm;->d:LaEm;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, LaEm;->e:LaEm;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaEm;->f:LaEm;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaEm;->g:LaEm;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaEm;->h:LaEm;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaEm;->i:LaEm;

    aput-object v2, v0, v1

    sput-object v0, LaEm;->a:[LaEm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 129
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEm;->a:LaFr;

    .line 130
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEm;
    .locals 1

    .prologue
    .line 40
    const-class v0, LaEm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEm;

    return-object v0
.end method

.method public static values()[LaEm;
    .locals 1

    .prologue
    .line 40
    sget-object v0, LaEm;->a:[LaEm;

    invoke-virtual {v0}, [LaEm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEm;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, LaEm;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, LaEm;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
