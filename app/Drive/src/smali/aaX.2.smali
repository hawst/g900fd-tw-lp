.class public LaaX;
.super Landroid/os/AsyncTask;
.source "UploadSharedItemActivityDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

.field final synthetic a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)V
    .locals 0

    .prologue
    .line 856
    iput-object p1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iput-boolean p2, p0, LaaX;->a:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 882
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-boolean v2, p0, LaaX;->a:Z

    iget-object v3, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;ZZ)LbmF;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LbmF;)LbmF;

    .line 886
    :try_start_0
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMZ;

    .line 887
    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LMZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 888
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 906
    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LbmF;)V

    :goto_0
    return-object v0

    .line 892
    :cond_1
    :try_start_1
    invoke-virtual {p0}, LaaX;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 893
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 906
    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LbmF;)V

    goto :goto_0

    .line 896
    :cond_2
    :try_start_2
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LaFO;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 897
    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 899
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaA;

    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;

    move-result-object v1

    invoke-interface {v0, v1}, LaaA;->a(Ljava/util/List;)LbmF;

    move-result-object v0

    .line 900
    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LbmF;)V

    .line 902
    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/util/List;)V

    .line 903
    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 906
    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LbmF;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LbmF;)V

    throw v0
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 912
    if-nez p1, :cond_0

    .line 913
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 915
    :cond_0
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LaFO;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;ILaFO;)V

    .line 916
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v1, "Upload canceled. %d items were scheduled for upload"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 917
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 918
    return-void
.end method

.method protected b(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 922
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 932
    :goto_0
    return-void

    .line 926
    :cond_0
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 927
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 928
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 930
    :cond_1
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LaFO;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;ILaFO;)V

    .line 931
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 856
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaaX;->a([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 856
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, LaaX;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 856
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, LaaX;->b(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 861
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 878
    :goto_0
    return-void

    .line 865
    :cond_0
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 866
    iget-object v1, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxg;->upload_spinner_message:I

    new-array v4, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 867
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 868
    iget-object v0, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    .line 869
    invoke-static {v0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 871
    iget-object v6, p0, LaaX;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    const-string v1, ""

    new-instance v5, LaaY;

    invoke-direct {v5, p0}, LaaY;-><init>(LaaX;)V

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_0
.end method
