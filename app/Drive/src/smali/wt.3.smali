.class public final Lwt;
.super Ljava/lang/Object;
.source "SimpleCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-static {v0}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a(Ljava/lang/String;)Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    move-result-object v0

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;
    .locals 1

    .prologue
    .line 152
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lwt;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lwt;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    move-result-object v0

    return-object v0
.end method
