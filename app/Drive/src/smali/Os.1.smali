.class public final LOs;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOD;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOx;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOy;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOC;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOr;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOt;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOp;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOQ;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOG;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 47
    iput-object p1, p0, LOs;->a:LbrA;

    .line 48
    const-class v0, LOD;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->a:Lbsk;

    .line 51
    const-class v0, LOx;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->b:Lbsk;

    .line 54
    const-class v0, LOy;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->c:Lbsk;

    .line 57
    const-class v0, LOC;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->d:Lbsk;

    .line 60
    const-class v0, LOr;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->e:Lbsk;

    .line 63
    const-class v0, LOt;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->f:Lbsk;

    .line 66
    const-class v0, LOp;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->g:Lbsk;

    .line 69
    const-class v0, LOQ;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->h:Lbsk;

    .line 72
    const-class v0, LOG;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->i:Lbsk;

    .line 75
    const-class v0, LOJ;

    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LOs;->j:Lbsk;

    .line 78
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 202
    sparse-switch p1, :sswitch_data_0

    .line 404
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :sswitch_0
    new-instance v2, LOD;

    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->I:Lbsk;

    .line 207
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->I:Lbsk;

    .line 205
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzQ;

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 213
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 211
    invoke-static {v1, v3}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    invoke-direct {v2, v0, v1}, LOD;-><init>(LzQ;LtK;)V

    move-object v0, v2

    .line 402
    :goto_0
    return-object v0

    .line 220
    :sswitch_1
    new-instance v1, LOx;

    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lqa;

    iget-object v0, v0, Lqa;->b:Lbsk;

    .line 223
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lqa;

    iget-object v2, v2, Lqa;->b:Lbsk;

    .line 221
    invoke-static {v0, v2}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpW;

    invoke-direct {v1, v0}, LOx;-><init>(LpW;)V

    move-object v0, v1

    .line 228
    goto :goto_0

    .line 230
    :sswitch_2
    new-instance v0, LOy;

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->ac:Lbsk;

    .line 233
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LbiH;

    iget-object v2, v2, LbiH;->ac:Lbsk;

    .line 231
    invoke-static {v1, v2}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LbiP;

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 239
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lc;

    iget-object v3, v3, Lc;->b:Lbsk;

    .line 237
    invoke-static {v2, v3}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->j:Lbsk;

    .line 245
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LOs;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->j:Lbsk;

    .line 243
    invoke-static {v3, v4}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaFO;

    iget-object v4, p0, LOs;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 251
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LOs;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 249
    invoke-static {v4, v5}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LtK;

    iget-object v5, p0, LOs;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LOs;

    iget-object v5, v5, LOs;->d:Lbsk;

    .line 257
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LOs;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LOs;

    iget-object v6, v6, LOs;->d:Lbsk;

    .line 255
    invoke-static {v5, v6}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LOC;

    invoke-direct/range {v0 .. v5}, LOy;-><init>(LbiP;Landroid/content/Context;LaFO;LtK;LOC;)V

    goto/16 :goto_0

    .line 264
    :sswitch_3
    new-instance v3, LOC;

    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->ac:Lbsk;

    .line 267
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->ac:Lbsk;

    .line 265
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJu;

    iget-object v1, v1, LJu;->b:Lbsk;

    .line 273
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LJu;

    iget-object v2, v2, LJu;->b:Lbsk;

    .line 271
    invoke-static {v1, v2}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LJc;

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->Z:Lbsk;

    .line 279
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LOs;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->Z:Lbsk;

    .line 277
    invoke-static {v2, v4}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laja;

    invoke-direct {v3, v0, v1, v2}, LOC;-><init>(LbiP;LJc;Laja;)V

    move-object v0, v3

    .line 284
    goto/16 :goto_0

    .line 286
    :sswitch_4
    new-instance v0, LOr;

    invoke-direct {v0}, LOr;-><init>()V

    .line 288
    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LOs;

    .line 289
    invoke-virtual {v1, v0}, LOs;->a(LOr;)V

    goto/16 :goto_0

    .line 292
    :sswitch_5
    new-instance v3, LOt;

    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->Z:Lbsk;

    .line 295
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->Z:Lbsk;

    .line 293
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->K:Lbsk;

    .line 301
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->K:Lbsk;

    .line 299
    invoke-static {v1, v2}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LAP;

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 307
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LOs;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 305
    invoke-static {v2, v4}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    invoke-direct {v3, v0, v1, v2}, LOt;-><init>(Laja;LAP;LtK;)V

    move-object v0, v3

    .line 312
    goto/16 :goto_0

    .line 314
    :sswitch_6
    new-instance v2, LOp;

    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaKj;

    iget-object v0, v0, LaKj;->a:Lbsk;

    .line 317
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaKj;

    iget-object v1, v1, LaKj;->a:Lbsk;

    .line 315
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKi;

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 323
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 321
    invoke-static {v1, v3}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    invoke-direct {v2, v0, v1}, LOp;-><init>(LaKi;LtK;)V

    move-object v0, v2

    .line 328
    goto/16 :goto_0

    .line 330
    :sswitch_7
    new-instance v0, LOQ;

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVM;

    iget-object v1, v1, LVM;->c:Lbsk;

    .line 333
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LVM;

    iget-object v2, v2, LVM;->c:Lbsk;

    .line 331
    invoke-static {v1, v2}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LVQ;

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LOs;

    iget-object v2, v2, LOs;->b:Lbsk;

    .line 339
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LOs;

    iget-object v3, v3, LOs;->b:Lbsk;

    .line 337
    invoke-static {v2, v3}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LOx;

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->K:Lbsk;

    .line 345
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LOs;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->K:Lbsk;

    .line 343
    invoke-static {v3, v4}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laja;

    iget-object v4, p0, LOs;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->am:Lbsk;

    .line 351
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LOs;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->am:Lbsk;

    .line 349
    invoke-static {v4, v5}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laja;

    iget-object v5, p0, LOs;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->O:Lbsk;

    .line 357
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LOs;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lajo;

    iget-object v6, v6, Lajo;->O:Lbsk;

    .line 355
    invoke-static {v5, v6}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Laja;

    iget-object v6, p0, LOs;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LOs;

    iget-object v6, v6, LOs;->a:Lbsk;

    .line 363
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LOs;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LOs;

    iget-object v7, v7, LOs;->a:Lbsk;

    .line 361
    invoke-static {v6, v7}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LOD;

    invoke-direct/range {v0 .. v6}, LOQ;-><init>(LVQ;LOx;Laja;Laja;Laja;LOD;)V

    goto/16 :goto_0

    .line 370
    :sswitch_8
    new-instance v4, LOG;

    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->ac:Lbsk;

    .line 373
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->ac:Lbsk;

    .line 371
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->v:Lbsk;

    .line 379
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LarZ;

    iget-object v2, v2, LarZ;->v:Lbsk;

    .line 377
    invoke-static {v1, v2}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lasa;

    iget-object v2, p0, LOs;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 385
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 383
    invoke-static {v2, v3}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    iget-object v3, p0, LOs;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LarZ;

    iget-object v3, v3, LarZ;->x:Lbsk;

    .line 391
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LOs;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LarZ;

    iget-object v5, v5, LarZ;->x:Lbsk;

    .line 389
    invoke-static {v3, v5}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lati;

    invoke-direct {v4, v0, v1, v2, v3}, LOG;-><init>(LbiP;Lasa;LtK;Lati;)V

    move-object v0, v4

    .line 396
    goto/16 :goto_0

    .line 398
    :sswitch_9
    new-instance v0, LOJ;

    invoke-direct {v0}, LOJ;-><init>()V

    .line 400
    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LOs;

    .line 401
    invoke-virtual {v1, v0}, LOs;->a(LOJ;)V

    goto/16 :goto_0

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x1c1 -> :sswitch_4
        0x1ce -> :sswitch_7
        0x1d0 -> :sswitch_6
        0x3a9 -> :sswitch_8
        0x3cf -> :sswitch_5
        0x44c -> :sswitch_2
        0x548 -> :sswitch_9
        0x54b -> :sswitch_0
        0x54c -> :sswitch_1
        0x54d -> :sswitch_3
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 437
    .line 439
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 155
    const-class v0, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xad

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LOs;->a(LbuP;LbuB;)V

    .line 158
    const-class v0, LOr;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xae

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LOs;->a(LbuP;LbuB;)V

    .line 161
    const-class v0, LOJ;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xaf

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LOs;->a(LbuP;LbuB;)V

    .line 164
    const-class v0, LOD;

    iget-object v1, p0, LOs;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 165
    const-class v0, LOx;

    iget-object v1, p0, LOs;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 166
    const-class v0, LOy;

    iget-object v1, p0, LOs;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 167
    const-class v0, LOC;

    iget-object v1, p0, LOs;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 168
    const-class v0, LOr;

    iget-object v1, p0, LOs;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 169
    const-class v0, LOt;

    iget-object v1, p0, LOs;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 170
    const-class v0, LOp;

    iget-object v1, p0, LOs;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 171
    const-class v0, LOQ;

    iget-object v1, p0, LOs;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 172
    const-class v0, LOG;

    iget-object v1, p0, LOs;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 173
    const-class v0, LOJ;

    iget-object v1, p0, LOs;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LOs;->a(Ljava/lang/Class;Lbsk;)V

    .line 174
    iget-object v0, p0, LOs;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x54b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 176
    iget-object v0, p0, LOs;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x54c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 178
    iget-object v0, p0, LOs;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x44c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 180
    iget-object v0, p0, LOs;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x54d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 182
    iget-object v0, p0, LOs;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1c1

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 184
    iget-object v0, p0, LOs;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x3cf

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 186
    iget-object v0, p0, LOs;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1d0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 188
    iget-object v0, p0, LOs;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1ce

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 190
    iget-object v0, p0, LOs;->i:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x3a9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 192
    iget-object v0, p0, LOs;->j:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x548

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 194
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 411
    packed-switch p1, :pswitch_data_0

    .line 431
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 413
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;

    .line 415
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LOs;

    .line 416
    invoke-virtual {v0, p2}, LOs;->a(Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;)V

    .line 433
    :goto_0
    return-void

    .line 419
    :pswitch_1
    check-cast p2, LOr;

    .line 421
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LOs;

    .line 422
    invoke-virtual {v0, p2}, LOs;->a(LOr;)V

    goto :goto_0

    .line 425
    :pswitch_2
    check-cast p2, LOJ;

    .line 427
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LOs;

    .line 428
    invoke-virtual {v0, p2}, LOs;->a(LOJ;)V

    goto :goto_0

    .line 411
    :pswitch_data_0
    .packed-switch 0xad
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(LOJ;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:La;

    iget-object v0, v0, La;->a:Lbsk;

    .line 111
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->a:Lbsk;

    .line 109
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p1, LOJ;->a:Landroid/app/Activity;

    .line 115
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 118
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 116
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, LOJ;->a:LSF;

    .line 122
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 125
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 123
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, LOJ;->a:LqK;

    .line 129
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LVM;

    iget-object v0, v0, LVM;->b:Lbsk;

    .line 132
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVM;

    iget-object v1, v1, LVM;->b:Lbsk;

    .line 130
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVN;

    iput-object v0, p1, LOJ;->a:LVN;

    .line 136
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 139
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 137
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LOJ;->a:LtK;

    .line 143
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->ac:Lbsk;

    .line 146
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->ac:Lbsk;

    .line 144
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iput-object v0, p1, LOJ;->a:LbiP;

    .line 150
    return-void
.end method

.method public a(LOr;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->z:Lbsk;

    .line 100
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->z:Lbsk;

    .line 98
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LOr;->a:Laja;

    .line 104
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 85
    invoke-virtual {v0, p1}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;)V

    .line 86
    iget-object v0, p0, LOs;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 89
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LOs;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 87
    invoke-static {v0, v1}, LOs;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a:LQr;

    .line 93
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method
