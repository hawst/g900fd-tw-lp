.class public LOD;
.super Ljava/lang/Object;
.source "PhotosArrangementModeHelper.java"

# interfaces
.implements LzQ;


# instance fields
.field private final a:LtK;

.field private final a:LzQ;


# direct methods
.method public constructor <init>(LzQ;LtK;)V
    .locals 0
    .param p1    # LzQ;
        .annotation runtime LQN;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, LOD;->a:LzQ;

    .line 30
    iput-object p2, p0, LOD;->a:LtK;

    .line 31
    return-void
.end method


# virtual methods
.method public a(LCl;LaFO;)LbmY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCl;",
            "LaFO;",
            ")",
            "LbmY",
            "<",
            "LzO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    sget-object v0, LCe;->r:LCe;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    sget-object v0, LzO;->d:LzO;

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    .line 45
    :goto_0
    return-object v0

    .line 38
    :cond_0
    sget-object v0, LCe;->o:LCe;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 39
    iget-object v0, p0, LOD;->a:LtK;

    sget-object v1, Lry;->P:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    .line 40
    if-nez v0, :cond_1

    iget-object v1, p0, LOD;->a:LtK;

    sget-object v2, LOE;->a:Lrg;

    invoke-interface {v1, v2, p2}, LtK;->a(Lrg;LaFO;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 41
    :cond_1
    if-eqz v0, :cond_2

    sget-object v0, LzO;->c:LzO;

    :goto_1
    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, LzO;->a:LzO;

    goto :goto_1

    .line 45
    :cond_3
    iget-object v0, p0, LOD;->a:LzQ;

    invoke-interface {v0, p1, p2}, LzQ;->a(LCl;LaFO;)LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public a()LzO;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LOD;->a:LzQ;

    invoke-interface {v0}, LzQ;->a()LzO;

    move-result-object v0

    return-object v0
.end method

.method public a(LpU;LzO;)LzO;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LOD;->a:LzQ;

    invoke-interface {v0, p1, p2}, LzQ;->a(LpU;LzO;)LzO;

    move-result-object v0

    return-object v0
.end method
