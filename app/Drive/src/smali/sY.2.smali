.class public LsY;
.super Ljava/lang/Object;
.source "DocumentOpenerActivityDelegate.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

.field final synthetic a:Lcom/google/android/apps/docs/utils/ParcelableTask;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Lcom/google/android/apps/docs/utils/ParcelableTask;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, LsY;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iput-object p2, p0, LsY;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    iput-object p3, p0, LsY;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 561
    :try_start_0
    iget-object v0, p0, LsY;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v1, p0, LsY;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Lcom/google/android/apps/docs/utils/ParcelableTask;)Lcom/google/android/apps/docs/utils/ParcelableTask;

    .line 562
    iget-object v0, p0, LsY;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v1, p0, LsY;->a:Landroid/content/Intent;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Landroid/content/Intent;)V

    .line 563
    iget-object v0, p0, LsY;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v1, p0, LsY;->a:Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->startActivityForResult(Landroid/content/Intent;I)V

    .line 565
    iget-object v0, p0, LsY;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Z)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :goto_0
    return-void

    .line 566
    :catch_0
    move-exception v0

    .line 567
    iget-object v0, p0, LsY;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V

    .line 568
    iget-object v0, p0, LsY;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    sget-object v1, LFV;->c:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LFV;)V

    goto :goto_0
.end method
