.class LLV;
.super LaGN;
.source "SelectionFunctionManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LbmF",
        "<",
        "LaGu;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LLS;

.field private final a:LLw;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/apps/docs/doclist/SelectionItem;


# direct methods
.method constructor <init>(LLS;LLw;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 1

    .prologue
    .line 43
    iput-object p1, p0, LLV;->a:LLS;

    invoke-direct {p0}, LaGN;-><init>()V

    .line 44
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLw;

    iput-object v0, p0, LLV;->a:LLw;

    .line 45
    invoke-static {p1}, LLS;->a(LLS;)LJR;

    move-result-object v0

    invoke-virtual {v0}, LJR;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LLV;->a:LbmF;

    .line 46
    iput-object p3, p0, LLV;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 47
    return-void
.end method


# virtual methods
.method public a(LaGM;)LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            ")",
            "LbmF",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 53
    iget-object v1, p0, LLV;->a:LLS;

    iget-object v2, p0, LLV;->a:LbmF;

    invoke-static {v1, p1, v2}, LLS;->a(LLS;LaGM;LbmF;)LbmF;

    move-result-object v1

    .line 54
    invoke-virtual {v1}, LbmF;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    iget-object v2, p0, LLV;->a:LLw;

    iget-object v3, p0, LLV;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-interface {v2, v1, v3}, LLw;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v0

    .line 57
    iget-object v2, p0, LLV;->a:LLw;

    iget-object v3, p0, LLV;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-interface {v2, v0, v1, v3}, LLw;->a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    move-object v0, v1

    .line 63
    :cond_0
    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, LLV;->a(LaGM;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a(LbmF;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    if-eqz p1, :cond_0

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v0

    .line 71
    iget-object v1, p0, LLV;->a:LLw;

    iget-object v2, p0, LLV;->a:LLS;

    invoke-static {v2}, LLS;->a(LLS;)Ljava/lang/Runnable;

    move-result-object v2

    iget-object v3, p0, LLV;->a:LbmF;

    invoke-interface {v1, v2, v0, p1, v3}, LLw;->a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V

    .line 73
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, LbmF;

    invoke-virtual {p0, p1}, LLV;->a(LbmF;)V

    return-void
.end method
