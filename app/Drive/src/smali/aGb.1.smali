.class public LaGb;
.super LaGd;
.source "DatabaseDocument.java"

# interfaces
.implements LaGo;


# direct methods
.method protected constructor <init>(LaGc;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, LaGd;-><init>(LaGe;)V

    .line 26
    return-void
.end method

.method public static a(LaEz;LaFM;Landroid/database/Cursor;)LaGb;
    .locals 2

    .prologue
    .line 102
    new-instance v0, LaGb;

    invoke-static {p0, p1, p2}, LaGc;->a(LaEz;LaFM;Landroid/database/Cursor;)LaGc;

    move-result-object v1

    invoke-direct {v0, v1}, LaGb;-><init>(LaGc;)V

    return-object v0
.end method

.method public static a(LaGc;)LaGb;
    .locals 2

    .prologue
    .line 37
    new-instance v0, LaGb;

    invoke-virtual {p0}, LaGc;->b()LaGc;

    move-result-object v1

    invoke-direct {v0, v1}, LaGb;-><init>(LaGc;)V

    return-object v0
.end method

.method private b()LaGc;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, LaGb;->a:LaGe;

    check-cast v0, LaGc;

    return-object v0
.end method


# virtual methods
.method public a(LacY;)J
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, LaGb;->b()LaGc;

    move-result-object v0

    invoke-virtual {v0, p1}, LaGc;->a(LacY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LaGc;
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, LaGb;->b()LaGc;

    move-result-object v0

    invoke-virtual {v0}, LaGc;->b()LaGc;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a()LaGe;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, LaGb;->a()LaGc;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGp;)LacY;
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, LaGb;->b()LaGc;

    move-result-object v0

    invoke-virtual {v0, p1}, LaGc;->a(LaGp;)LacY;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, LaGb;->b()LaGc;

    move-result-object v0

    invoke-virtual {v0}, LaGc;->a()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, LaGb;->b()LaGc;

    move-result-object v0

    invoke-virtual {v0}, LaGc;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, LaGb;->b()LaGc;

    move-result-object v0

    invoke-virtual {v0}, LaGc;->b()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, LaGb;->b()LaGc;

    move-result-object v0

    invoke-virtual {v0}, LaGc;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 107
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Document(super=%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-super {p0}, LaGd;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
