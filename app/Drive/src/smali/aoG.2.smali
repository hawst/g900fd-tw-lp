.class public LaoG;
.super Ljava/lang/Object;
.source "NetworkThreadPoolExecutor.java"

# interfaces
.implements Laop;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laop",
        "<",
        "LakM",
        "<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LakH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "LamU",
            "<",
            "Ljava/lang/Long;",
            "*>;>;"
        }
    .end annotation
.end field

.field private final a:LaoX;


# direct methods
.method constructor <init>(LQr;)V
    .locals 4

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "fetchingPoolSizeNetwork"

    const/4 v1, 0x4

    invoke-interface {p1, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 42
    const-string v1, "fetchingQueueSizeNetwork"

    const/16 v2, 0x32

    invoke-interface {p1, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v1

    .line 45
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v1}, LakH;->a(JI)LakH;

    move-result-object v1

    iput-object v1, p0, LaoG;->a:LakH;

    .line 47
    const-string v1, "NetworkThreadPool"

    .line 48
    invoke-static {v0, v1}, Lalg;->a(ILjava/lang/String;)LbsW;

    move-result-object v0

    .line 50
    iget-object v1, p0, LaoG;->a:LakH;

    .line 51
    invoke-static {v0, v1}, LalE;->a(Ljava/util/concurrent/ExecutorService;Lalf;)LalE;

    move-result-object v0

    .line 53
    new-instance v1, LaoX;

    invoke-direct {v1, v0}, LaoX;-><init>(LalD;)V

    iput-object v1, p0, LaoG;->a:LaoX;

    .line 54
    return-void
.end method


# virtual methods
.method public a()LakH;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, LaoG;->a:LakH;

    return-object v0
.end method

.method public a(LakM;Ljava/util/concurrent/Callable;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LakM",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, LaoG;->a:LaoX;

    invoke-virtual {v0, p1, p2}, LaoX;->a(LakM;Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)LbsU;
    .locals 1

    .prologue
    .line 29
    check-cast p1, LakM;

    invoke-virtual {p0, p1, p2}, LaoG;->a(LakM;Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    return-object v0
.end method
