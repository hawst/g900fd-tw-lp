.class public abstract LaGm;
.super Ljava/lang/Object;
.source "DatabaseRow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LaFy;",
        "D:",
        "LaFm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:J

.field private a:LaFB;

.field protected final a:LaFm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private final a:LaFy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final a:Landroid/net/Uri;

.field private b:J


# direct methods
.method protected constructor <init>(LaFm;LaFy;Landroid/net/Uri;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;TT;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaGm;->a:J

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaGm;->b:J

    .line 33
    iput-object p1, p0, LaGm;->a:LaFm;

    .line 34
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFy;

    iput-object v0, p0, LaGm;->a:LaFy;

    .line 35
    iput-object p3, p0, LaGm;->a:Landroid/net/Uri;

    .line 36
    iget-wide v0, p0, LaGm;->a:J

    invoke-virtual {p2, p1, v0, v1}, LaFy;->a(LaFx;J)LaFB;

    move-result-object v0

    iput-object v0, p0, LaGm;->a:LaFB;

    .line 37
    return-void
.end method


# virtual methods
.method final a()Landroid/content/ContentValues;
    .locals 7

    .prologue
    .line 55
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 57
    :try_start_0
    invoke-virtual {p0, v1}, LaGm;->a(Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    return-object v1

    .line 58
    :catch_0
    move-exception v2

    .line 59
    const-string v0, ""

    .line 61
    :try_start_1
    invoke-virtual {p0}, LaGm;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 67
    :goto_0
    const-string v3, "DatabaseRow"

    const-string v4, "Error in fillContentValues() on %s; partial result: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 69
    throw v2

    .line 62
    :catch_1
    move-exception v0

    .line 63
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[additional RuntimeException thrown by toString(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Landroid/content/ContentValues;)V
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, LaGm;->a:J

    return-wide v0
.end method

.method protected final c(J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 44
    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LaGm;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LaGm;->a:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 45
    iget-object v0, p0, LaGm;->a:LaFy;

    iget-object v1, p0, LaGm;->a:LaFm;

    invoke-virtual {v0, v1, p1, p2}, LaFy;->a(LaFx;J)LaFB;

    move-result-object v0

    iput-object v0, p0, LaGm;->a:LaFB;

    .line 46
    iget-object v0, p0, LaGm;->a:LaFB;

    invoke-interface {v0}, LaFB;->a()J

    move-result-wide v0

    iput-wide v0, p0, LaGm;->b:J

    .line 47
    iput-wide p1, p0, LaGm;->a:J

    .line 48
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 7

    .prologue
    .line 79
    iget-object v0, p0, LaGm;->a:LaFB;

    iget-wide v2, p0, LaGm;->b:J

    invoke-interface {v0, v2, v3}, LaFB;->a(J)V

    .line 81
    :try_start_0
    iget-object v1, p0, LaGm;->a:LaFm;

    .line 82
    invoke-virtual {p0}, LaGm;->c()J

    move-result-wide v2

    iget-object v4, p0, LaGm;->a:LaFy;

    invoke-virtual {p0}, LaGm;->a()Landroid/content/ContentValues;

    move-result-object v5

    iget-object v6, p0, LaGm;->a:Landroid/net/Uri;

    invoke-virtual/range {v1 .. v6}, LaFm;->a(JLaFy;Landroid/content/ContentValues;Landroid/net/Uri;)J

    move-result-wide v0

    .line 83
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 84
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error saving "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaGm;->a:LaFB;

    invoke-interface {v1}, LaFB;->a()V

    throw v0

    .line 86
    :cond_0
    :try_start_1
    iget-object v2, p0, LaGm;->a:LaFB;

    invoke-interface {v2}, LaFB;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    iget-object v2, p0, LaGm;->a:LaFB;

    invoke-interface {v2}, LaFB;->a()V

    .line 93
    invoke-virtual {p0, v0, v1}, LaGm;->c(J)V

    .line 94
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 116
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 5

    .prologue
    .line 100
    invoke-virtual {p0}, LaGm;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 101
    iget-object v0, p0, LaGm;->a:LaFm;

    invoke-virtual {p0}, LaGm;->c()J

    move-result-wide v2

    iget-object v1, p0, LaGm;->a:LaFy;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, LaFm;->a(JLaFy;Landroid/net/Uri;)I

    .line 102
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, LaGm;->c(J)V

    .line 103
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 4

    .prologue
    .line 131
    iget-wide v0, p0, LaGm;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 126
    const-string v0, "DatabaseRow[%s, sqlId=%s, values=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaGm;->a:LaFy;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaGm;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 127
    invoke-virtual {p0}, LaGm;->a()Landroid/content/ContentValues;

    move-result-object v3

    aput-object v3, v1, v2

    .line 126
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
