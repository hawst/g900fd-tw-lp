.class public Laht;
.super Ljava/lang/Object;
.source "VideoUrlFetcherImpl.java"

# interfaces
.implements Lahr;


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final a:Lbpw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpw",
            "<",
            "Lahv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LTh;

.field private final a:LaKR;

.field private final a:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-string v0, "https://docs.google.com/get_video_info?mobile=true"

    .line 77
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Laht;->a:Landroid/net/Uri;

    .line 84
    new-instance v0, Lahu;

    invoke-direct {v0}, Lahu;-><init>()V

    sput-object v0, Laht;->a:Lbpw;

    return-void
.end method

.method public constructor <init>(LaKR;LTh;Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Laht;->a:LaKR;

    .line 102
    iput-object p2, p0, Laht;->a:LTh;

    .line 103
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Laht;->a:Landroid/graphics/Point;

    .line 104
    const-string v0, "window"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 105
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 106
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-ge v1, v2, :cond_0

    .line 107
    iget-object v1, p0, Laht;->a:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Point;->set(II)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v1, p0, Laht;->a:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method private a(Z)Landroid/graphics/Point;
    .locals 6

    .prologue
    .line 125
    const-wide v0, 0x3fd3333333333333L    # 0.3

    .line 126
    if-nez p1, :cond_0

    iget-object v2, p0, Laht;->a:LaKR;

    invoke-interface {v2}, LaKR;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 127
    :cond_0
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    .line 129
    :cond_1
    iget-object v2, p0, Laht;->a:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-int v2, v2

    .line 130
    iget-object v3, p0, Laht;->a:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-double v4, v3

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 131
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v1
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 142
    :try_start_0
    sget-object v0, Laht;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "docid"

    .line 143
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 144
    iget-object v1, p0, Laht;->a:LTh;

    iget-object v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    const/4 v3, 0x5

    const/4 v4, 0x1

    invoke-static {v1, v2, v0, v3, v4}, LTs;->a(LTh;LaFO;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 147
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    .line 149
    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 154
    iget-object v1, p0, Laht;->a:LTh;

    invoke-interface {v1}, LTh;->a()V

    .line 155
    iget-object v1, p0, Laht;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    return-object v0

    .line 151
    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Response without entity."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    :catchall_0
    move-exception v0

    iget-object v1, p0, Laht;->a:LTh;

    invoke-interface {v1}, LTh;->a()V

    .line 155
    iget-object v1, p0, Laht;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 208
    new-instance v0, Landroid/net/UrlQuerySanitizer;

    invoke-direct {v0}, Landroid/net/UrlQuerySanitizer;-><init>()V

    .line 209
    invoke-static {}, Landroid/net/UrlQuerySanitizer;->getUrlLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/net/UrlQuerySanitizer;->registerParameter(Ljava/lang/String;Landroid/net/UrlQuerySanitizer$ValueSanitizer;)V

    .line 210
    invoke-virtual {v0, p2}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;)LbmL;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 228
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v2

    .line 229
    const-string v0, "fmt_stream_map"

    invoke-direct {p0, v0, p1}, Laht;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 230
    if-eqz v3, :cond_1

    .line 231
    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    .line 232
    const-string v7, "\\|"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 233
    array-length v8, v7

    if-ne v8, v11, :cond_0

    .line 234
    aget-object v6, v7, v1

    aget-object v7, v7, v10

    invoke-static {v7}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 231
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    :cond_0
    const-string v7, "VideoUrl"

    const-string v8, "Malformed streamPair %s in %s"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v6, v9, v1

    aput-object v3, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 241
    :cond_1
    invoke-virtual {v2}, LbmM;->a()LbmL;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    const-string v0, "video/3gpp"

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    invoke-direct {p0, p1}, Laht;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-direct {p0, p2}, Laht;->a(Z)Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Laht;->a(Ljava/lang/String;Landroid/graphics/Point;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Landroid/graphics/Point;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-virtual {p0, p1}, Laht;->a(Ljava/lang/String;)LbmL;

    move-result-object v2

    .line 178
    invoke-virtual {p0, p1}, Laht;->b(Ljava/lang/String;)LbmL;

    move-result-object v3

    .line 179
    new-instance v4, Lahv;

    iget v0, p2, Landroid/graphics/Point;->x:I

    iget v5, p2, Landroid/graphics/Point;->y:I

    invoke-direct {v4, p0, v0, v5, v1}, Lahv;-><init>(Laht;IILahu;)V

    .line 181
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 182
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 183
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahv;

    .line 184
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 185
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    invoke-static {v0, v4}, Lahv;->a(Lahv;Lahv;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 187
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 193
    :cond_1
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 194
    sget-object v0, Laht;->a:Lbpw;

    invoke-virtual {v0, v6}, Lbpw;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahv;

    .line 199
    :goto_1
    if-nez v0, :cond_3

    .line 200
    new-instance v0, Lahs;

    invoke-direct {v0}, Lahs;-><init>()V

    throw v0

    .line 195
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 196
    sget-object v0, Laht;->a:Lbpw;

    invoke-virtual {v0, v5}, Lbpw;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahv;

    goto :goto_1

    .line 203
    :cond_3
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 204
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method b(Ljava/lang/String;)LbmL;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbmL",
            "<",
            "Lahv;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v1

    .line 260
    const-string v0, "fmt_list"

    invoke-direct {p0, v0, p1}, Laht;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 262
    if-eqz v2, :cond_1

    .line 263
    const-string v0, "^(\\d+)\\/([1-9]\\d*)x([1-9]\\d*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 264
    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    .line 265
    invoke-virtual {v3, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 266
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 268
    :try_start_0
    new-instance v8, Lahv;

    const/4 v9, 0x2

    .line 269
    invoke-virtual {v7, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const/4 v10, 0x3

    invoke-virtual {v7, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    const/4 v11, 0x0

    invoke-direct {v8, p0, v9, v10, v11}, Lahv;-><init>(Laht;IILahu;)V

    .line 270
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v8, v7}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271
    :catch_0
    move-exception v7

    .line 272
    const-string v7, "VideoUrl"

    const-string v8, "Malformed streamPair %s in %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v6, 0x1

    aput-object v2, v9, v6

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 275
    :cond_0
    const-string v7, "VideoUrl"

    const-string v8, "Malformed streamPair %s in %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v6, 0x1

    aput-object v2, v9, v6

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 280
    :cond_1
    invoke-virtual {v1}, LbmM;->a()LbmL;

    move-result-object v0

    return-object v0
.end method
