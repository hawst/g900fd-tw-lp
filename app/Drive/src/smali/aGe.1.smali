.class public abstract LaGe;
.super LaGm;
.source "DatabaseEntryEditor.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaER;",
        "LaEz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field static a:LaKM;

.field public static final a:Landroid/net/Uri;


# instance fields
.field private final a:J

.field private final a:LaFM;

.field private final a:LaGv;

.field private a:LaGw;

.field private a:LaGx;

.field private a:LaGy;

.field private a:Ljava/lang/Long;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/Date;

.field private a:Z

.field private b:Ljava/lang/Long;

.field private final b:Ljava/lang/String;

.field private b:Ljava/util/Date;

.field private b:Z

.field private c:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private c:Ljava/util/Date;

.field private c:Z

.field private d:Ljava/lang/String;

.field private d:Ljava/util/Date;

.field private d:Z

.field private e:Ljava/lang/String;

.field private e:Ljava/util/Date;

.field private e:Z

.field private f:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/lang/String;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, LaKN;->a:LaKN;

    sput-object v0, LaGe;->a:LaKM;

    .line 45
    const/4 v0, 0x0

    sput-object v0, LaGe;->a:Landroid/net/Uri;

    return-void
.end method

.method protected constructor <init>(LaEz;LaFM;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 149
    .line 150
    invoke-static {p3}, LaER;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, LaES;->p:LaES;

    .line 151
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    sget-object v0, LaES;->q:LaES;

    .line 152
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 149
    invoke-direct/range {v0 .. v5}, LaGe;-><init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 153
    invoke-static {}, LaER;->a()LaER;

    move-result-object v0

    invoke-virtual {v0}, LaER;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaGe;->c(J)V

    .line 155
    sget-object v0, LaES;->r:LaES;

    .line 156
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-virtual {p0, v0}, LaGe;->e(Ljava/lang/String;)LaGe;

    .line 157
    sget-object v0, LaES;->s:LaES;

    .line 158
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-virtual {p0, v0}, LaGe;->f(Ljava/lang/String;)LaGe;

    .line 159
    sget-object v0, LaES;->a:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGe;->b(Ljava/lang/String;)V

    .line 160
    new-instance v0, Ljava/util/Date;

    sget-object v1, LaES;->d:LaES;

    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v0}, LaGe;->a(Ljava/util/Date;)LaGe;

    .line 161
    sget-object v0, LaES;->b:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGe;->b(Ljava/lang/String;)LaGe;

    .line 162
    sget-object v0, LaES;->f:LaES;

    .line 163
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-virtual {p0, v0}, LaGe;->c(Ljava/lang/String;)LaGe;

    .line 164
    sget-object v0, LaES;->g:LaES;

    .line 165
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-virtual {p0, v0}, LaGe;->c(Ljava/lang/String;)V

    .line 167
    new-instance v0, Ljava/util/Date;

    sget-object v1, LaES;->e:LaES;

    .line 168
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 167
    invoke-virtual {p0, v0}, LaGe;->b(Ljava/util/Date;)LaGe;

    .line 171
    sget-object v0, LaES;->h:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 173
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v1}, LaGe;->c(Ljava/util/Date;)LaGe;

    .line 176
    :cond_0
    sget-object v0, LaES;->i:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGe;->a(Ljava/lang/Long;)V

    .line 177
    sget-object v0, LaES;->m:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGe;->a(Ljava/lang/Long;)LaGe;

    .line 179
    sget-object v0, LaES;->o:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGe;->g(Ljava/lang/String;)LaGe;

    .line 180
    sget-object v0, LaES;->c:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGe;->d(Ljava/lang/String;)LaGe;

    .line 182
    sget-object v0, LaES;->v:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LaGe;->a(Z)LaGe;

    .line 183
    sget-object v0, LaES;->k:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v0

    invoke-virtual {p0, v0}, LaGe;->a(Z)V

    .line 184
    sget-object v0, LaES;->l:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LaGe;->b(Z)LaGe;

    .line 185
    sget-object v0, LaES;->x:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LaGe;->a(J)V

    .line 186
    sget-object v0, LaES;->H:LaES;

    .line 187
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 186
    invoke-static {v0}, LaGw;->a(I)LaGw;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGe;->a(LaGw;)V

    .line 189
    sget-object v0, LaES;->z:LaES;

    .line 190
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 191
    new-instance v2, LaGY;

    sget-object v3, LaES;->y:LaES;

    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3, p3}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-direct {v2, v3, v4}, LaGY;-><init>(ZLjava/util/Date;)V

    invoke-virtual {p0, v2}, LaGe;->a(LaGY;)LaGe;

    .line 194
    sget-object v0, LaES;->A:LaES;

    .line 195
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 196
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v2}, LaGe;->a(Ljava/util/Date;)V

    .line 198
    sget-object v0, LaES;->B:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LaGe;->b(Z)V

    .line 199
    sget-object v0, LaES;->u:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LaGe;->c(Z)V

    .line 200
    sget-object v0, LaES;->C:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LaGe;->c(Z)LaGe;

    .line 201
    sget-object v0, LaES;->F:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaGe;->c:Ljava/lang/Long;

    .line 202
    sget-object v0, LaES;->G:LaES;

    .line 203
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, LaGx;->a(J)LaGx;

    move-result-object v0

    iput-object v0, p0, LaGe;->a:LaGx;

    .line 204
    return-void
.end method

.method protected constructor <init>(LaEz;LaFM;Ljava/lang/String;LaHa;)V
    .locals 6

    .prologue
    .line 140
    invoke-interface {p4}, LaHa;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LaGe;-><init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 141
    return-void
.end method

.method protected constructor <init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 145
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LaGe;-><init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 146
    return-void
.end method

.method private constructor <init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 123
    invoke-static {}, LaER;->a()LaER;

    move-result-object v0

    sget-object v1, LaGe;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 54
    const-string v0, ""

    iput-object v0, p0, LaGe;->c:Ljava/lang/String;

    .line 55
    iput-object v2, p0, LaGe;->d:Ljava/lang/String;

    .line 56
    iput-object v2, p0, LaGe;->e:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaGe;->a:Ljava/util/Date;

    .line 58
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaGe;->b:Ljava/util/Date;

    .line 59
    iput-object v2, p0, LaGe;->c:Ljava/util/Date;

    .line 60
    iput-object v2, p0, LaGe;->f:Ljava/lang/String;

    .line 61
    iput-object v2, p0, LaGe;->g:Ljava/lang/String;

    .line 62
    iput-object v2, p0, LaGe;->h:Ljava/lang/String;

    .line 63
    iput-boolean v3, p0, LaGe;->a:Z

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGe;->b:Z

    .line 65
    iput-boolean v3, p0, LaGe;->c:Z

    .line 66
    iput-boolean v3, p0, LaGe;->d:Z

    .line 67
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaGe;->d:Ljava/util/Date;

    .line 68
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaGe;->e:Ljava/util/Date;

    .line 69
    iput-boolean v3, p0, LaGe;->e:Z

    .line 71
    sget-object v0, LaGy;->a:LaGy;

    iput-object v0, p0, LaGe;->a:LaGy;

    .line 72
    sget-object v0, LaGw;->a:LaGw;

    iput-object v0, p0, LaGe;->a:LaGw;

    .line 74
    sget-object v0, LaGx;->a:LaGx;

    iput-object v0, p0, LaGe;->a:LaGx;

    .line 83
    iput-object v2, p0, LaGe;->a:Ljava/lang/Long;

    .line 90
    iput-object v2, p0, LaGe;->b:Ljava/lang/Long;

    .line 96
    iput-object v2, p0, LaGe;->i:Ljava/lang/String;

    .line 104
    iput-object v2, p0, LaGe;->j:Ljava/lang/String;

    .line 106
    iput-boolean v3, p0, LaGe;->g:Z

    .line 124
    iput-object p4, p0, LaGe;->a:Ljava/lang/String;

    .line 125
    iput-object p2, p0, LaGe;->a:LaFM;

    .line 126
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaGe;->b:Ljava/lang/String;

    .line 127
    invoke-static {p3}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v0

    iput-object v0, p0, LaGe;->a:LaGv;

    .line 128
    iput-boolean p5, p0, LaGe;->g:Z

    .line 129
    if-eqz p1, :cond_0

    .line 130
    invoke-virtual {p1}, LaEz;->b()J

    move-result-wide v0

    iput-wide v0, p0, LaGe;->a:J

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaGe;->a:J

    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, LaGe;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 467
    invoke-static {}, LaGy;->values()[LaGy;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 468
    invoke-virtual {v3}, LaGy;->a()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 469
    invoke-direct {p0, v3}, LaGe;->a(LaGy;)V

    .line 470
    return-void

    .line 467
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 474
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unaccepted TrashState sql value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(LaGy;)V
    .locals 1

    .prologue
    .line 458
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGy;

    iput-object v0, p0, LaGe;->a:LaGy;

    .line 459
    return-void
.end method

.method private e()Ljava/util/Date;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, LaGe;->e:Ljava/util/Date;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 462
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->a()LaIm;

    move-result-object v0

    .line 463
    invoke-virtual {p0}, LaGe;->a()LaFM;

    move-result-object v1

    invoke-virtual {p0}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LaIm;->a(LaFM;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 464
    return-void
.end method


# virtual methods
.method public a()LaFM;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, LaGe;->a:LaFM;

    return-object v0
.end method

.method public abstract a()LaGd;
.end method

.method protected a()LaGe;
    .locals 1

    .prologue
    .line 750
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGe;

    return-object v0
.end method

.method public a(LaGY;)LaGe;
    .locals 1

    .prologue
    .line 510
    invoke-virtual {p1}, LaGY;->a()Z

    move-result v0

    iput-boolean v0, p0, LaGe;->d:Z

    .line 511
    invoke-virtual {p1}, LaGY;->a()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, LaGe;->d:Ljava/util/Date;

    .line 512
    return-object p0
.end method

.method public a(Ljava/lang/Long;)LaGe;
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, LaGe;->b:Ljava/lang/Long;

    .line 382
    return-object p0
.end method

.method public a(Ljava/lang/String;)LaGe;
    .locals 1

    .prologue
    .line 241
    invoke-static {p1}, LaKY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 242
    iput-object v0, p0, LaGe;->c:Ljava/lang/String;

    .line 243
    return-object p0
.end method

.method public a(Ljava/util/Date;)LaGe;
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, LaGe;->a:Ljava/util/Date;

    .line 306
    return-object p0
.end method

.method public a(Z)LaGe;
    .locals 0

    .prologue
    .line 444
    iput-boolean p1, p0, LaGe;->a:Z

    .line 445
    return-object p0
.end method

.method public a()LaGv;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, LaGe;->a:LaGv;

    return-object v0
.end method

.method public a()LaGw;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, LaGe;->a:LaGw;

    return-object v0
.end method

.method public a()LaGx;
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, LaGe;->a:LaGx;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;
    .locals 4

    .prologue
    .line 208
    invoke-virtual {p0}, LaGe;->c()J

    move-result-wide v0

    .line 209
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 210
    const/4 v0, 0x0

    .line 212
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LaGe;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;J)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 2

    .prologue
    .line 217
    invoke-virtual {p0}, LaGe;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const/4 v0, 0x0

    .line 220
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LaGe;->a:LaFM;

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    iget-object v1, p0, LaGe;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, LaGe;->a:Ljava/util/Date;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 481
    sget-object v0, LaGy;->d:LaGy;

    invoke-direct {p0, v0}, LaGe;->a(LaGy;)V

    .line 482
    invoke-direct {p0}, LaGe;->g()V

    .line 483
    return-void
.end method

.method protected abstract a(LaEz;)V
.end method

.method protected abstract a(LaEz;J)V
.end method

.method public a(LaGw;)V
    .locals 0

    .prologue
    .line 609
    iput-object p1, p0, LaGe;->a:LaGw;

    .line 610
    return-void
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 621
    iget-object v0, p0, LaGe;->c:Ljava/lang/String;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    iget-object v0, p0, LaGe;->d:Ljava/lang/String;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    iget-object v0, p0, LaGe;->i:Ljava/lang/String;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    iget-object v0, p0, LaGe;->e:Ljava/lang/String;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    sget-object v0, LaES;->b:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, LaGe;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    sget-object v0, LaES;->a:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGe;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    sget-object v0, LaES;->o:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGe;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    sget-object v0, LaES;->p:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LaGe;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    sget-object v0, LaES;->q:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, LaGe;->g:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 630
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 629
    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 631
    sget-object v0, LaES;->v:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LaGe;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 632
    sget-object v0, LaES;->k:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LaGe;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 633
    sget-object v0, LaES;->l:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LaGe;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 634
    sget-object v0, LaES;->x:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LaGe;->a:LaGy;

    invoke-virtual {v4}, LaGy;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 635
    sget-object v0, LaES;->y:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LaGe;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 636
    sget-object v0, LaES;->z:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 637
    invoke-virtual {p0}, LaGe;->d()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 636
    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 638
    sget-object v0, LaES;->A:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 639
    invoke-direct {p0}, LaGe;->e()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 638
    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 641
    sget-object v0, LaES;->B:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    .line 642
    invoke-virtual {p0}, LaGe;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 641
    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 643
    sget-object v0, LaES;->f:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LaGe;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    sget-object v0, LaES;->g:LaES;

    .line 646
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LaGe;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    invoke-virtual {p0}, LaGe;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 648
    sget-object v4, LaES;->u:LaES;

    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v0, :cond_6

    move-object v0, v3

    :goto_6
    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 650
    sget-object v0, LaES;->C:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 651
    invoke-virtual {p0}, LaGe;->l()Z

    move-result v4

    if-eqz v4, :cond_8

    :goto_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 650
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 652
    sget-object v0, LaES;->d:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGe;->a()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 653
    sget-object v0, LaES;->e:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 654
    invoke-virtual {p0}, LaGe;->b()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 653
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 656
    invoke-virtual {p0}, LaGe;->c()Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_9

    .line 657
    :goto_8
    sget-object v0, LaES;->h:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 658
    sget-object v0, LaES;->i:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGe;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 659
    sget-object v0, LaES;->m:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGe;->d()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 660
    sget-object v0, LaES;->c:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGe;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    sget-object v0, LaES;->E:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGe;->a()LaFM;

    move-result-object v1

    invoke-virtual {v1}, LaFM;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 662
    sget-object v0, LaES;->t:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGe;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    sget-object v0, LaES;->r:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 664
    invoke-virtual {p0}, LaGe;->g()Ljava/lang/String;

    move-result-object v1

    .line 663
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    sget-object v0, LaES;->s:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 666
    invoke-virtual {p0}, LaGe;->h()Ljava/lang/String;

    move-result-object v1

    .line 665
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    sget-object v0, LaES;->F:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGe;->c:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 668
    sget-object v0, LaES;->G:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGe;->a:LaGx;

    invoke-virtual {v1}, LaGx;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 669
    sget-object v0, LaES;->H:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGe;->a:LaGw;

    .line 670
    invoke-virtual {v1}, LaGw;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 669
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 671
    return-void

    :cond_0
    move v0, v2

    .line 629
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 631
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 632
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 633
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 635
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 642
    goto/16 :goto_5

    .line 649
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_6

    :cond_7
    move v0, v2

    goto :goto_9

    :cond_8
    move v1, v2

    .line 651
    goto/16 :goto_7

    .line 656
    :cond_9
    invoke-virtual {p0}, LaGe;->c()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_8
.end method

.method public a(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, LaGe;->a:Ljava/lang/Long;

    .line 374
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, LaGe;->e:Ljava/util/Date;

    .line 517
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 449
    iput-boolean p1, p0, LaGe;->b:Z

    .line 450
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, LaGe;->a:LaFM;

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-virtual {v0}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGe;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract b()LaGd;
.end method

.method public b(Ljava/lang/String;)LaGe;
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, LaGe;->d:Ljava/lang/String;

    .line 265
    return-object p0
.end method

.method public b(Ljava/util/Date;)LaGe;
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, LaGe;->b:Ljava/util/Date;

    .line 355
    return-object p0
.end method

.method public b(Z)LaGe;
    .locals 0

    .prologue
    .line 453
    iput-boolean p1, p0, LaGe;->c:Z

    .line 454
    return-object p0
.end method

.method public b()Ljava/util/Date;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, LaGe;->b:Ljava/util/Date;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 489
    invoke-virtual {p0}, LaGe;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 490
    sget-object v0, LaGy;->e:LaGy;

    invoke-direct {p0, v0}, LaGe;->a(LaGy;)V

    .line 491
    invoke-direct {p0}, LaGe;->g()V

    .line 492
    return-void

    .line 489
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, LaGe;->c:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 529
    iput-boolean p1, p0, LaGe;->f:Z

    .line 530
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 400
    iget-boolean v0, p0, LaGe;->g:Z

    return v0
.end method

.method public c(Ljava/lang/String;)LaGe;
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, LaGe;->i:Ljava/lang/String;

    .line 274
    return-object p0
.end method

.method public c(Ljava/util/Date;)LaGe;
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, LaGe;->c:Ljava/util/Date;

    .line 365
    return-object p0
.end method

.method public c(Z)LaGe;
    .locals 1

    .prologue
    .line 562
    iput-boolean p1, p0, LaGe;->e:Z

    .line 563
    if-eqz p1, :cond_0

    .line 564
    const-string v0, "unknown_as_place_holder"

    invoke-virtual {p0, v0}, LaGe;->c(Ljava/lang/String;)LaGe;

    .line 565
    const-string v0, "unknown_as_place_holder"

    invoke-virtual {p0, v0}, LaGe;->g(Ljava/lang/String;)LaGe;

    .line 566
    const-string v0, "unknown_as_place_holder"

    invoke-virtual {p0, v0}, LaGe;->d(Ljava/lang/String;)LaGe;

    .line 567
    const-string v0, "unknown_as_place_holder"

    invoke-virtual {p0, v0}, LaGe;->b(Ljava/lang/String;)LaGe;

    .line 569
    :cond_0
    return-object p0
.end method

.method public c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, LaGe;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, LaGe;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/Date;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, LaGe;->c:Ljava/util/Date;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 498
    sget-object v0, LaGy;->c:LaGy;

    invoke-direct {p0, v0}, LaGe;->a(LaGy;)V

    .line 499
    invoke-direct {p0}, LaGe;->g()V

    .line 500
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, LaGe;->j:Ljava/lang/String;

    .line 283
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 546
    iput-boolean p1, p0, LaGe;->h:Z

    .line 547
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, LaGe;->a:Z

    return v0
.end method

.method protected synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, LaGe;->a()LaGe;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)LaGe;
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, LaGe;->e:Ljava/lang/String;

    .line 293
    return-object p0
.end method

.method public d()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, LaGe;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, LaGe;->i:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/util/Date;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, LaGe;->d:Ljava/util/Date;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 506
    sget-object v0, LaGy;->a:LaGy;

    invoke-direct {p0, v0}, LaGe;->a(LaGy;)V

    .line 507
    return-void
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 597
    if-eqz p1, :cond_0

    sget-object v0, LaGx;->b:LaGx;

    :goto_0
    iput-object v0, p0, LaGe;->a:LaGx;

    .line 599
    return-void

    .line 597
    :cond_0
    sget-object v0, LaGx;->c:LaGx;

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 408
    iget-boolean v0, p0, LaGe;->b:Z

    return v0
.end method

.method public e(Ljava/lang/String;)LaGe;
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, LaGe;->f:Ljava/lang/String;

    .line 316
    return-object p0
.end method

.method public e()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, LaGe;->c:Ljava/lang/Long;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, LaGe;->j:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 678
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->b()V

    .line 680
    invoke-virtual {p0}, LaGe;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    iget-wide v2, p0, LaGe;->a:J

    invoke-virtual {v0, v2, v3}, LaEz;->a(J)V

    .line 683
    :cond_0
    iget-object v1, p0, LaGe;->c:Ljava/lang/Long;

    .line 685
    :try_start_0
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaGe;->c:Ljava/lang/Long;

    .line 686
    invoke-super {p0}, LaGm;->e()V

    .line 687
    invoke-virtual {p0}, LaGe;->c()J

    move-result-wide v2

    .line 688
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {p0, v0, v2, v3}, LaGe;->a(LaEz;J)V

    .line 689
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->d()V

    .line 690
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0, p0}, LaEz;->a(LaGe;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 698
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    .line 700
    return-void

    .line 691
    :catch_0
    move-exception v0

    .line 692
    :try_start_1
    iput-object v1, p0, LaGe;->c:Ljava/lang/Long;

    .line 693
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 698
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    throw v1

    .line 694
    :catch_1
    move-exception v0

    .line 695
    :try_start_2
    iput-object v1, p0, LaGe;->c:Ljava/lang/Long;

    .line 696
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 412
    iget-boolean v0, p0, LaGe;->c:Z

    return v0
.end method

.method f(Ljava/lang/String;)LaGe;
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, LaGe;->g:Ljava/lang/String;

    .line 327
    return-object p0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, LaGe;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 711
    invoke-virtual {p0}, LaGe;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 712
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->b()V

    .line 714
    :try_start_0
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->a()LaIm;

    move-result-object v0

    .line 715
    invoke-virtual {p0}, LaGe;->a()LaFM;

    move-result-object v1

    invoke-virtual {p0}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LaIm;->a(LaFM;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 716
    invoke-super {p0}, LaGm;->f()V

    .line 717
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {p0, v0}, LaGe;->a(LaEz;)V

    .line 718
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 720
    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    .line 722
    return-void

    .line 711
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 720
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, LaGe;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    throw v1
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 416
    sget-object v0, LaGy;->a:LaGy;

    iget-object v1, p0, LaGe;->a:LaGy;

    invoke-virtual {v0, v1}, LaGy;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)LaGe;
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, LaGe;->h:Ljava/lang/String;

    .line 391
    return-object p0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, LaGe;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 420
    iget-boolean v0, p0, LaGe;->d:Z

    return v0
.end method

.method public h(Ljava/lang/String;)LaGe;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 573
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    iget-boolean v0, p0, LaGe;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaGe;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 575
    iput-boolean v1, p0, LaGe;->g:Z

    .line 576
    iput-object p1, p0, LaGe;->a:Ljava/lang/String;

    .line 577
    return-object p0

    :cond_1
    move v0, v1

    .line 574
    goto :goto_0
.end method

.method h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, LaGe;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 426
    invoke-virtual {p0}, LaGe;->f()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, LaGv;->f:LaGv;

    const/4 v3, 0x6

    new-array v3, v3, [LaGv;

    sget-object v4, LaGv;->b:LaGv;

    aput-object v4, v3, v1

    sget-object v4, LaGv;->i:LaGv;

    aput-object v4, v3, v0

    const/4 v4, 0x2

    sget-object v5, LaGv;->g:LaGv;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, LaGv;->c:LaGv;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    sget-object v5, LaGv;->d:LaGv;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    sget-object v5, LaGv;->k:LaGv;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    .line 432
    invoke-virtual {p0}, LaGe;->a()LaGv;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LaGe;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, LaGe;->a:LaGv;

    invoke-virtual {v0}, LaGv;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, LaGe;->a:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    .line 337
    :goto_0
    return-object v0

    .line 334
    :cond_0
    iget-object v0, p0, LaGe;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, LaGe;->g:Ljava/lang/String;

    goto :goto_0

    .line 337
    :cond_1
    iget-object v0, p0, LaGe;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 525
    iget-boolean v0, p0, LaGe;->f:Z

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, LaGe;->h:Ljava/lang/String;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, LaGe;->h:Z

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    invoke-virtual {p0}, LaGe;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LaGe;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 542
    const/4 v0, 0x0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, LaGe;->b:Ljava/lang/String;

    return-object v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 558
    iget-boolean v0, p0, LaGe;->e:Z

    return v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 728
    invoke-virtual {p0}, LaGe;->a()LaGv;

    move-result-object v0

    sget-object v1, LaGv;->a:LaGv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 744
    const-string v0, "Entry %s of %s with resource id: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LaGe;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaGe;->a()LaFM;

    move-result-object v3

    .line 745
    invoke-virtual {v3}, LaFM;->a()LaFO;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, LaGe;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 744
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
