.class public Lpf;
.super Ljava/lang/Object;
.source "ClassLoaderPatcher.java"


# instance fields
.field private final a:Ljava/util/regex/Pattern;

.field private final a:Lpi;

.field private final a:Lpw;


# direct methods
.method public constructor <init>(Lpi;Lpw;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lpf;->a:Lpi;

    .line 79
    iput-object p2, p0, Lpf;->a:Lpw;

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lpf;->a:Ljava/util/regex/Pattern;

    .line 81
    return-void
.end method

.method private a(Lph;)Lpi;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lpf;->a:Lpi;

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Lpj;

    iget-object v1, p0, Lpf;->a:Ljava/util/regex/Pattern;

    invoke-direct {v0, p1, v1}, Lpj;-><init>(Lph;Ljava/util/regex/Pattern;)V

    .line 144
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lpf;->a:Lpi;

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lpf;->a(Landroid/content/Context;)Lpb;

    move-result-object v0

    invoke-virtual {v0}, Lpb;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/content/Context;)Lpb;
    .locals 4

    .prologue
    .line 136
    new-instance v0, Lph;

    invoke-direct {v0, p1}, Lph;-><init>(Landroid/content/Context;)V

    .line 137
    new-instance v1, Lpb;

    invoke-direct {p0, v0}, Lpf;->a(Lph;)Lpi;

    move-result-object v2

    iget-object v3, p0, Lpf;->a:Lpw;

    invoke-direct {v1, v0, v2, v3}, Lpb;-><init>(Lph;Lpi;Lpw;)V

    return-object v1
.end method

.method a(Ljava/lang/ClassLoader;)Lpe;
    .locals 7

    .prologue
    .line 124
    new-instance v0, Lps;

    invoke-direct {v0, p1}, Lps;-><init>(Ljava/lang/Object;)V

    .line 125
    new-instance v1, Lpc;

    invoke-direct {v1}, Lpc;-><init>()V

    .line 126
    new-instance v2, Lpg;

    const/4 v3, 0x3

    new-array v3, v3, [LoX;

    const/4 v4, 0x0

    new-instance v5, Lpl;

    iget-object v6, p0, Lpf;->a:Lpw;

    invoke-direct {v5, v0, v1, v6}, Lpl;-><init>(Lps;Lpc;Lpw;)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lpk;

    iget-object v6, p0, Lpf;->a:Lpw;

    invoke-direct {v5, v0, v1, v6}, Lpk;-><init>(Lps;Lpc;Lpw;)V

    aput-object v5, v3, v4

    const/4 v4, 0x2

    new-instance v5, Lpn;

    iget-object v6, p0, Lpf;->a:Lpw;

    invoke-direct {v5, v0, v1, v6}, Lpn;-><init>(Lps;Lpc;Lpw;)V

    aput-object v5, v3, v4

    .line 127
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lpf;->a:Lpw;

    invoke-direct {v2, v0, v1}, Lpg;-><init>(Ljava/util/Collection;Lpw;)V

    return-object v2
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 93
    if-nez v0, :cond_0

    .line 95
    iget-object v0, p0, Lpf;->a:Lpw;

    const-string v1, "Context class loader is null. Must be running in test mode. Skip patching."

    invoke-virtual {v0, v1}, Lpw;->a(Ljava/lang/String;)V

    .line 111
    :goto_0
    return-void

    .line 100
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lpf;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 101
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 102
    iget-object v0, p0, Lpf;->a:Lpw;

    const-string v1, "No supplementary dex files found. Skip patching."

    invoke-virtual {v0, v1}, Lpw;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lpm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    throw v0

    .line 105
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1, v0, v1}, Lpf;->a(Landroid/content/Context;Ljava/lang/ClassLoader;Ljava/util/List;)V
    :try_end_1
    .catch Lpm; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 108
    :catch_1
    move-exception v0

    .line 109
    new-instance v1, Lpm;

    invoke-direct {v1, v0}, Lpm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected a(Landroid/content/Context;Ljava/lang/ClassLoader;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/ClassLoader;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p0, p2}, Lpf;->a(Ljava/lang/ClassLoader;)Lpe;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-interface {v0, p3, v1}, Lpe;->a(Ljava/util/List;Ljava/io/File;)V

    .line 120
    return-void
.end method
