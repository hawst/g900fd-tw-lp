.class public Latx;
.super Landroid/os/AsyncTask;
.source "WelcomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/welcome/Page;

.field final synthetic a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Lcom/google/android/apps/docs/welcome/Page;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    iput-object p2, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/Page;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/Page;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/welcome/Page;->a()LbsU;

    move-result-object v0

    .line 553
    :try_start_0
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 557
    :goto_0
    return-object v0

    .line 554
    :catch_0
    move-exception v0

    .line 555
    const/4 v0, 0x0

    goto :goto_0

    .line 556
    :catch_1
    move-exception v0

    .line 557
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 564
    iget-object v0, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    if-eqz p1, :cond_0

    .line 573
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 574
    iget-object v0, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    iget-object v1, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/Page;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Lcom/google/android/apps/docs/welcome/Page;)V

    goto :goto_0

    .line 576
    :cond_2
    iget-object v0, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Ljava/lang/String;

    move-result-object v0

    .line 577
    iget-object v1, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Ljava/lang/String;

    move-result-object v1

    .line 578
    iget-object v2, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v2}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Ljava/lang/String;

    move-result-object v2

    .line 579
    iget-object v3, p0, Latx;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v3, v0, v2, v1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 547
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Latx;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 547
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Latx;->a(Ljava/lang/Boolean;)V

    return-void
.end method
