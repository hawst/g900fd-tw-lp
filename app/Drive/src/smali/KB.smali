.class public LKB;
.super Ljava/lang/Object;
.source "DropToThisFolderOverlayController.java"


# instance fields
.field private final a:F

.field private a:Landroid/animation/AnimatorSet;

.field private final a:Landroid/app/Activity;

.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, LKB;->a:Landroid/app/Activity;

    .line 47
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 48
    sget v1, Lxa;->action_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LKB;->a:F

    .line 49
    return-void
.end method

.method private a()Landroid/view/WindowManager$LayoutParams;
    .locals 7

    .prologue
    .line 130
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 131
    iget-object v0, p0, LKB;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 133
    invoke-virtual {v0, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 134
    iget v6, v2, Landroid/graphics/Rect;->top:I

    .line 136
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, v3, v2

    const/16 v3, 0x3ea

    const/16 v4, 0x18

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 141
    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 142
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 143
    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 144
    return-object v0
.end method

.method static synthetic a(LKB;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, LKB;->e()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, LKB;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, LKB;->a:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    .line 116
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 117
    sget v1, Lxe;->drop_to_this_folder_action_bar_overlay:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LKB;->a:Landroid/view/View;

    .line 118
    iget-object v0, p0, LKB;->a:Landroid/view/View;

    sget v1, Lxc;->action_bar_popup:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LKB;->c:Landroid/view/View;

    .line 119
    iget-object v0, p0, LKB;->a:Landroid/view/View;

    sget v1, Lxc;->highlight:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LKB;->b:Landroid/view/View;

    .line 121
    iget-object v0, p0, LKB;->a:Landroid/app/Activity;

    const-string v1, "window"

    .line 122
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 124
    invoke-direct {p0}, LKB;->a()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 125
    iget-object v2, p0, LKB;->a:Landroid/view/View;

    invoke-interface {v0, v2, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 150
    iget-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    .line 153
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 156
    iget-object v0, p0, LKB;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 157
    invoke-direct {p0}, LKB;->d()V

    .line 158
    iget-object v0, p0, LKB;->a:Landroid/app/Activity;

    const-string v1, "window"

    .line 159
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 160
    iget-object v1, p0, LKB;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 161
    iput-object v2, p0, LKB;->a:Landroid/view/View;

    .line 162
    iput-object v2, p0, LKB;->c:Landroid/view/View;

    .line 163
    iput-object v2, p0, LKB;->b:Landroid/view/View;

    .line 165
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, LKB;->e()V

    .line 58
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x64

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 64
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-direct {p0}, LKB;->c()V

    .line 66
    iget-object v0, p0, LKB;->a:Landroid/view/View;

    sget v1, Lxc;->folder_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 67
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-direct {p0}, LKB;->d()V

    .line 70
    iget-object v0, p0, LKB;->c:Landroid/view/View;

    iget v1, p0, LKB;->a:F

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    .line 71
    iget-object v0, p0, LKB;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 72
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    .line 73
    iget-object v0, p0, LKB;->c:Landroid/view/View;

    const-string v1, "translationY"

    new-array v2, v4, [F

    iget v3, p0, LKB;->a:F

    neg-float v3, v3

    aput v3, v2, v6

    aput v5, v2, v7

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 74
    iget-object v1, p0, LKB;->b:Landroid/view/View;

    const-string v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 76
    invoke-virtual {v1, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 77
    iget-object v2, p0, LKB;->a:Landroid/animation/AnimatorSet;

    new-array v3, v4, [Landroid/animation/Animator;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 78
    iget-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 79
    return-void

    .line 74
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public b()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x64

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 85
    iget-object v0, p0, LKB;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 86
    invoke-direct {p0}, LKB;->d()V

    .line 87
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    .line 88
    iget-object v0, p0, LKB;->c:Landroid/view/View;

    const-string v1, "translationY"

    new-array v2, v6, [F

    iget v3, p0, LKB;->a:F

    neg-float v3, v3

    aput v3, v2, v5

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 89
    iget-object v1, p0, LKB;->b:Landroid/view/View;

    const-string v2, "alpha"

    new-array v3, v7, [F

    iget-object v4, p0, LKB;->b:Landroid/view/View;

    .line 90
    invoke-virtual {v4}, Landroid/view/View;->getAlpha()F

    move-result v4

    aput v4, v3, v5

    const/4 v4, 0x0

    aput v4, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 92
    invoke-virtual {v1, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 93
    iget-object v2, p0, LKB;->a:Landroid/animation/AnimatorSet;

    new-array v3, v7, [Landroid/animation/Animator;

    aput-object v1, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 94
    iget-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    new-instance v1, LKC;

    invoke-direct {v1, p0}, LKC;-><init>(LKB;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 109
    iget-object v0, p0, LKB;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 111
    :cond_0
    return-void
.end method
