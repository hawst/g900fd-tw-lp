.class public final LPh;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPe;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPg;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LPf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LPh;->a:LbrA;

    .line 41
    const-class v0, LPe;

    invoke-static {v0, v1}, LPh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPh;->a:Lbsk;

    .line 44
    const-class v0, LPg;

    invoke-static {v0, v1}, LPh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPh;->b:Lbsk;

    .line 47
    const-class v0, LPf;

    invoke-static {v0, v1}, LPh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LPh;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 95
    sparse-switch p1, :sswitch_data_0

    .line 141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :sswitch_0
    new-instance v3, LPe;

    iget-object v0, p0, LPh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->k:Lbsk;

    .line 100
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->k:Lbsk;

    .line 98
    invoke-static {v0, v1}, LPh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarT;

    iget-object v1, p0, LPh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->m:Lbsk;

    .line 106
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LPh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LarZ;

    iget-object v2, v2, LarZ;->m:Lbsk;

    .line 104
    invoke-static {v1, v2}, LPh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LasR;

    iget-object v2, p0, LPh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LbiH;

    iget-object v2, v2, LbiH;->Y:Lbsk;

    .line 112
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LPh;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LbiH;

    iget-object v4, v4, LbiH;->Y:Lbsk;

    .line 110
    invoke-static {v2, v4}, LPh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LbiP;

    invoke-direct {v3, v0, v1, v2}, LPe;-><init>(LarT;LasR;LbiP;)V

    move-object v0, v3

    .line 139
    :goto_0
    return-object v0

    .line 119
    :sswitch_1
    new-instance v2, LPg;

    iget-object v0, p0, LPh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->E:Lbsk;

    .line 122
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->E:Lbsk;

    .line 120
    invoke-static {v0, v1}, LPh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasb;

    iget-object v1, p0, LPh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->Y:Lbsk;

    .line 128
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LPh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LbiH;

    iget-object v3, v3, LbiH;->Y:Lbsk;

    .line 126
    invoke-static {v1, v3}, LPh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LbiP;

    invoke-direct {v2, v0, v1}, LPg;-><init>(Lasb;LbiP;)V

    move-object v0, v2

    .line 133
    goto :goto_0

    .line 135
    :sswitch_2
    new-instance v0, LPf;

    invoke-direct {v0}, LPf;-><init>()V

    .line 137
    iget-object v1, p0, LPh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPh;

    .line 138
    invoke-virtual {v1, v0}, LPh;->a(LPf;)V

    goto :goto_0

    .line 95
    nop

    :sswitch_data_0
    .sparse-switch
        0x1f2 -> :sswitch_0
        0x1f9 -> :sswitch_1
        0x205 -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 162
    .line 164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 75
    const-class v0, LPf;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x7a

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LPh;->a(LbuP;LbuB;)V

    .line 78
    const-class v0, LPe;

    iget-object v1, p0, LPh;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LPh;->a(Ljava/lang/Class;Lbsk;)V

    .line 79
    const-class v0, LPg;

    iget-object v1, p0, LPh;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LPh;->a(Ljava/lang/Class;Lbsk;)V

    .line 80
    const-class v0, LPf;

    iget-object v1, p0, LPh;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LPh;->a(Ljava/lang/Class;Lbsk;)V

    .line 81
    iget-object v0, p0, LPh;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1f2

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 83
    iget-object v0, p0, LPh;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1f9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 85
    iget-object v0, p0, LPh;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x205

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 87
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 148
    packed-switch p1, :pswitch_data_0

    .line 156
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :pswitch_0
    check-cast p2, LPf;

    .line 152
    iget-object v0, p0, LPh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPh;

    .line 153
    invoke-virtual {v0, p2}, LPh;->a(LPf;)V

    .line 158
    return-void

    .line 148
    :pswitch_data_0
    .packed-switch 0x7a
        :pswitch_0
    .end packed-switch
.end method

.method public a(LPf;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, LPh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->B:Lbsk;

    .line 59
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->B:Lbsk;

    .line 57
    invoke-static {v0, v1}, LPh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasa;

    iput-object v0, p1, LPf;->a:Lasa;

    .line 63
    iget-object v0, p0, LPh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->Y:Lbsk;

    .line 66
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LPh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->Y:Lbsk;

    .line 64
    invoke-static {v0, v1}, LPh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iput-object v0, p1, LPf;->a:LbiP;

    .line 70
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
