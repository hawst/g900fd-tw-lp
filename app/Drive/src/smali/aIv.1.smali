.class LaIv;
.super Ljava/lang/Object;
.source "StorageFileReader.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaGo;

.field final synthetic a:LaIu;

.field final synthetic a:[Landroid/os/ParcelFileDescriptor;


# direct methods
.method constructor <init>(LaIu;LaGo;[Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, LaIv;->a:LaIu;

    iput-object p2, p0, LaIv;->a:LaGo;

    iput-object p3, p0, LaIv;->a:[Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 87
    :try_start_0
    iget-object v0, p0, LaIv;->a:LaIu;

    iget-object v1, p0, LaIv;->a:LaGo;

    iget-object v2, p0, LaIv;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v0, v1, v2}, LaIu;->a(LaIu;LaGo;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :try_start_1
    iget-object v0, p0, LaIv;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 97
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 94
    const-string v1, "StorageFileReader"

    const-string v2, "Failure closing pipe"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 88
    :catch_1
    move-exception v0

    .line 89
    :try_start_2
    const-string v1, "StorageFileReader"

    const-string v2, "Failure retrieving document content"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 92
    :try_start_3
    iget-object v0, p0, LaIv;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 93
    :catch_2
    move-exception v0

    .line 94
    const-string v1, "StorageFileReader"

    const-string v2, "Failure closing pipe"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    .line 92
    :try_start_4
    iget-object v1, p0, LaIv;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 95
    :goto_1
    throw v0

    .line 93
    :catch_3
    move-exception v1

    .line 94
    const-string v2, "StorageFileReader"

    const-string v3, "Failure closing pipe"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
