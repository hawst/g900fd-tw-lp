.class public final Ladh;
.super Ljava/lang/Object;
.source "DocumentFileCloseTaskImpl.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    .line 26
    new-instance v1, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;-><init>(Lcom/google/android/apps/docs/sync/filemanager/FileSpec;)V

    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;
    .locals 1

    .prologue
    .line 31
    new-array v0, p1, [Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Ladh;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Ladh;->a(I)[Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    move-result-object v0

    return-object v0
.end method
