.class public LPj;
.super Ljava/lang/Object;
.source "DetailDrawerFragment.java"

# interfaces
.implements Lgz;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 141
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LPj;->b(Landroid/view/View;)V

    .line 143
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;Z)Z

    .line 125
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->b(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V

    .line 126
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPo;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPo;

    invoke-interface {v0, p2}, LPo;->b(F)V

    .line 133
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;Z)Z

    .line 111
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V

    .line 112
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->b(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V

    .line 113
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LacF;

    invoke-virtual {v0}, LacF;->a()V

    .line 117
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPo;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, LPj;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPo;

    invoke-interface {v0}, LPo;->k()V

    .line 120
    :cond_0
    return-void
.end method
