.class Lacy;
.super Ljava/lang/Object;
.source "SharingInfoManagerImpl.java"

# interfaces
.implements Lacr;


# instance fields
.field final synthetic a:Lact;

.field private a:Lacx;

.field private final a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lqu;

.field private b:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lqo;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lact;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 2

    .prologue
    .line 73
    iput-object p1, p0, Lacy;->a:Lact;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lacy;->b:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lacy;->b:Ljava/util/List;

    .line 74
    iput-object p2, p0, Lacy;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 75
    iget-object v0, p2, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 76
    invoke-virtual {v0}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-static {v0}, LacH;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacy;->b:Ljava/lang/String;

    .line 77
    new-instance v0, Lacx;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lacx;-><init>(Lacu;)V

    iput-object v0, p0, Lacy;->a:Lacx;

    .line 78
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lacy;->a:Ljava/util/List;

    .line 79
    return-void
.end method

.method private a(Lacx;Lqu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lacy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 84
    iput-object p3, p0, Lacy;->c:Ljava/lang/String;

    .line 85
    iput-object p4, p0, Lacy;->a:Ljava/lang/String;

    .line 86
    iput-object p1, p0, Lacy;->a:Lacx;

    .line 87
    iput-object p2, p0, Lacy;->a:Lqu;

    .line 88
    iget-object v0, p0, Lacy;->a:Lqu;

    sget-object v1, Lqu;->n:Lqu;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lacy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    sget-object v0, Lqu;->a:Lqu;

    iput-object v0, p0, Lacy;->a:Lqu;

    .line 91
    :cond_0
    iget-object v0, p0, Lacy;->a:Lacx;

    iput-object v0, p0, Lacy;->a:Ljava/util/List;

    .line 92
    return-void
.end method

.method static synthetic a(Lacy;Lacx;Lqu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3, p4}, Lacy;->a(Lacx;Lqu;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private c()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 136
    iget-object v0, p0, Lacy;->a:Lacx;

    invoke-virtual {v0}, Lacx;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 137
    invoke-virtual {v0}, LacD;->a()Lqo;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v3

    sget-object v4, Lqx;->a:Lqx;

    if-eq v3, v4, :cond_1

    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v3

    sget-object v4, Lqx;->b:Lqx;

    if-ne v3, v4, :cond_0

    .line 139
    :cond_1
    invoke-virtual {v0}, Lqo;->a()Lqv;

    move-result-object v3

    sget-object v4, Lqv;->a:Lqv;

    if-ne v3, v4, :cond_2

    .line 140
    iget-object v3, p0, Lacy;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-virtual {v3}, LaFO;->b()Ljava/lang/String;

    move-result-object v3

    .line 141
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 150
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 146
    goto :goto_0

    .line 150
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)LacD;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 203
    invoke-virtual {p0}, Lacy;->a()Ljava/util/List;

    move-result-object v0

    .line 205
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 206
    if-nez v0, :cond_1

    move-object v2, v1

    .line 207
    :goto_0
    if-nez v2, :cond_2

    move-object v2, v1

    .line 208
    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    :goto_2
    return-object v0

    .line 206
    :cond_1
    invoke-virtual {v0}, LacD;->a()LabD;

    move-result-object v2

    goto :goto_0

    .line 207
    :cond_2
    invoke-interface {v2}, LabD;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lacy;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lacy;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lacy;->a:Lacx;

    return-object v0
.end method

.method public a()Lqu;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lacy;->a:Lqu;

    return-object v0
.end method

.method public a()V
    .locals 6

    .prologue
    .line 155
    new-instance v1, Lacx;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lacx;-><init>(Lacu;)V

    .line 156
    iget-object v0, p0, Lacy;->a:Lacx;

    invoke-virtual {v0}, Lacx;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 157
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v3

    .line 158
    iget-object v4, p0, Lacy;->a:Lact;

    iget-object v4, v4, Lact;->a:LabF;

    .line 159
    invoke-virtual {v0}, LacD;->a()LabD;

    move-result-object v0

    invoke-interface {v0}, LabD;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Laci;->a()Lqx;

    move-result-object v5

    .line 158
    invoke-interface {v4, v0, v5}, LabF;->a(Ljava/lang/String;Lqx;)LabD;

    move-result-object v0

    .line 160
    new-instance v4, LacD;

    invoke-direct {v4, v0, v3}, LacD;-><init>(LabD;Laci;)V

    invoke-virtual {v1, v4}, Lacx;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    :cond_0
    iput-object v1, p0, Lacy;->a:Lacx;

    .line 163
    iget-object v0, p0, Lacy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 164
    return-void
.end method

.method public a(Lqo;)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lacy;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    return-void
.end method

.method public a(Lqu;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lacy;->a:Lqu;

    .line 194
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lacy;->a:Lacx;

    if-nez v0, :cond_0

    .line 97
    const-string v0, "SharingWorkflowImpl"

    const-string v2, "ACL modification tested while ACL is null."

    invoke-static {v0, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 108
    :goto_0
    return v0

    .line 100
    :cond_0
    iget-object v0, p0, Lacy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v2

    .line 101
    goto :goto_0

    .line 103
    :cond_1
    iget-object v0, p0, Lacy;->a:Lacx;

    invoke-virtual {v0}, Lacx;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 104
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 105
    goto :goto_0

    :cond_3
    move v0, v1

    .line 108
    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lacy;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lacy;->a:Ljava/util/List;

    return-object v0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 113
    iget-object v2, p0, Lacy;->a:Lacx;

    invoke-virtual {v2}, Lacx;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    invoke-direct {p0}, Lacy;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 117
    goto :goto_0

    .line 119
    :cond_2
    sget-object v2, Lacw;->a:[I

    iget-object v3, p0, Lacy;->a:Lqu;

    invoke-virtual {v3}, Lqu;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 122
    goto :goto_0

    .line 125
    :pswitch_1
    iget-object v2, p0, Lacy;->b:Ljava/lang/String;

    iget-object v3, p0, Lacy;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 126
    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lqo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lacy;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 228
    instance-of v1, p1, Lacy;

    if-nez v1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    check-cast p1, Lacy;

    .line 233
    iget-object v1, p0, Lacy;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v2, p1, Lacy;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lacy;->a:Ljava/util/List;

    iget-object v2, p1, Lacy;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lacy;->a:Ljava/lang/String;

    iget-object v2, p1, Lacy;->a:Ljava/lang/String;

    .line 234
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lacy;->a:Lacx;

    iget-object v2, p1, Lacy;->a:Lacx;

    invoke-virtual {v1, v2}, Lacx;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lacy;->a:Lqu;

    iget-object v2, p1, Lacy;->a:Lqu;

    .line 235
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lacy;->c:Ljava/lang/String;

    iget-object v2, p1, Lacy;->c:Ljava/lang/String;

    .line 236
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lacy;->b:Ljava/util/List;

    iget-object v2, p1, Lacy;->b:Ljava/util/List;

    .line 237
    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 242
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lacy;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lacy;->a:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lacy;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lacy;->a:Lacx;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lacy;->a:Lqu;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lacy;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lacy;->b:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
