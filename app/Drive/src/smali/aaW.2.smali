.class public LaaW;
.super LaGN;
.source "UploadSharedItemActivityDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LaFV;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private b:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 773
    iput-object p1, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iput-object p2, p0, LaaW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)LaFV;
    .locals 2

    .prologue
    .line 778
    iget-object v0, p0, LaaW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaaW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 779
    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v0

    .line 780
    :goto_0
    iget-object v1, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LaFO;

    move-result-object v1

    invoke-interface {p1, v1}, LaGM;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    iput-object v1, p0, LaaW;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 781
    return-object v0

    .line 779
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 773
    invoke-virtual {p0, p1}, LaaW;->a(LaGM;)LaFV;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFV;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 786
    .line 788
    if-eqz p1, :cond_0

    iget-object v0, p0, LaaW;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, p0, LaaW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 789
    :cond_0
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v1, "Selected root collection"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    iget-object v0, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaW;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 791
    iget-object v0, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;

    move-result-object v0

    .line 792
    sget v1, Lxb;->ic_drive_my_drive:I

    .line 803
    :goto_0
    iget-object v2, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/widget/Button;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 804
    iget-object v2, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 805
    iget-object v0, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 807
    :cond_1
    return-void

    .line 794
    :cond_2
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v1, "Selected non-root collection %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LaaW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    .line 795
    invoke-interface {p1}, LaFV;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 794
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 796
    iget-object v0, p0, LaaW;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 797
    invoke-interface {p1}, LaFV;->c()Ljava/lang/String;

    move-result-object v0

    .line 799
    invoke-interface {p1}, LaFV;->a()LaGv;

    move-result-object v1

    invoke-interface {p1}, LaFV;->f()Ljava/lang/String;

    move-result-object v2

    .line 800
    invoke-interface {p1}, LaFV;->d()Z

    move-result v3

    .line 798
    invoke-static {v1, v2, v3}, LaGt;->a(LaGv;Ljava/lang/String;Z)I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 773
    check-cast p1, LaFV;

    invoke-virtual {p0, p1}, LaaW;->a(LaFV;)V

    return-void
.end method
