.class public final Lart;
.super LaqV;
.source "HoneycombActionBarHelper.java"


# static fields
.field static final a:Z


# instance fields
.field private final a:LabF;

.field private final a:LabI;

.field private final a:LalO;

.field private a:Landroid/content/ComponentName;

.field private a:Landroid/os/Bundle;

.field private a:Landroid/widget/SearchView;

.field private final a:LarB;

.field private a:Lara;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Larg;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private final a:LtK;

.field private a:[Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 123
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lart;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;LaFO;ILarB;LabF;LalO;Lbxw;LtK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LaFO;",
            "I",
            "LarB;",
            "LabF;",
            "LalO;",
            "Lbxw",
            "<",
            "Larg;",
            ">;",
            "LtK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0, p1, p2, p3}, LaqV;-><init>(Landroid/app/Activity;LaFO;I)V

    .line 132
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/accounts/Account;

    iput-object v0, p0, Lart;->a:[Landroid/accounts/Account;

    .line 149
    iput-object p4, p0, Lart;->a:LarB;

    .line 150
    iput-object p7, p0, Lart;->a:Lbxw;

    .line 151
    invoke-virtual {p0}, Lart;->a()V

    .line 153
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lart;->b(Z)V

    .line 154
    new-instance v0, LabI;

    invoke-direct {v0, p1}, LabI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lart;->a:LabI;

    .line 155
    iput-object p5, p0, Lart;->a:LabF;

    .line 156
    iput-object p6, p0, Lart;->a:LalO;

    .line 157
    iput-object p8, p0, Lart;->a:LtK;

    .line 158
    return-void
.end method

.method private static a([Landroid/accounts/Account;LaFO;)I
    .locals 2

    .prologue
    .line 234
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 235
    aget-object v1, p0, v0

    invoke-virtual {p1, v1}, LaFO;->a(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    :goto_1
    return v0

    .line 234
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic a(Lart;)Landroid/widget/SearchView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    return-object v0
.end method

.method static synthetic a(Lart;)LarB;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lart;->a:LarB;

    return-object v0
.end method

.method private a([Landroid/accounts/Account;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 244
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 245
    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_0
    return-object v1
.end method

.method private a(Landroid/app/ActionBar;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lart;->a:[Landroid/accounts/Account;

    invoke-virtual {p0}, Lart;->a()LaFO;

    move-result-object v1

    invoke-static {v0, v1}, Lart;->a([Landroid/accounts/Account;LaFO;)I

    move-result v0

    .line 253
    if-ltz v0, :cond_0

    .line 254
    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 256
    :cond_0
    return-void
.end method

.method static synthetic a(Lart;Ljava/lang/String;Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lart;->a(Ljava/lang/String;Landroid/view/MenuItem;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/MenuItem;)V
    .locals 4

    .prologue
    .line 443
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 444
    sget-boolean v0, Lart;->a:Z

    if-eqz v0, :cond_0

    .line 445
    invoke-interface {p2}, Landroid/view/MenuItem;->collapseActionView()Z

    .line 450
    :cond_0
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 451
    const-string v0, "shrimponthebarbie"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lart;->a:LtK;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lart;->a:LtK;

    sget-object v2, Lry;->aS:Lry;

    .line 453
    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454
    invoke-static {v1}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 457
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    :goto_0
    return-void

    .line 460
    :catch_0
    move-exception v0

    .line 462
    const-string v2, "DUMP"

    const-string v3, "Send activity cannot be found"

    invoke-static {v2, v3, v0}, LalV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 467
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEARCH"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    const-string v3, "search"

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 472
    iget-object v3, p0, Lart;->a:Landroid/content/ComponentName;

    invoke-virtual {v0, v3}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    .line 473
    if-eqz v0, :cond_4

    .line 474
    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 480
    :goto_1
    if-eqz p1, :cond_2

    .line 481
    const-string v0, "query"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    :cond_2
    iget-object v0, p0, Lart;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 484
    const-string v0, "app_data"

    iget-object v3, p0, Lart;->a:Landroid/os/Bundle;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 487
    :cond_3
    iget-object v0, p0, Lart;->a:Lara;

    if-eqz v0, :cond_5

    .line 488
    iget-object v0, p0, Lart;->a:Lara;

    invoke-interface {v0, v2}, Lara;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 477
    :cond_4
    iget-object v0, p0, Lart;->a:Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_1

    .line 490
    :cond_5
    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    instance-of v0, v0, Lrl;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    check-cast v0, Lrl;

    invoke-interface {v0}, Lrl;->a()Z

    move-result v0

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lart;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lart;->a:Lbxw;

    .line 604
    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    invoke-interface {v0}, Larg;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 605
    :cond_0
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 606
    if-eqz v0, :cond_1

    .line 607
    invoke-direct {p0}, Lart;->a()Z

    move-result v1

    .line 608
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 611
    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 539
    return-void
.end method

.method public a(LaGu;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 543
    invoke-interface {p1}, LaGu;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lart;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v0

    .line 547
    invoke-interface {p1}, LaGu;->f()Ljava/lang/String;

    move-result-object v1

    .line 548
    iget-object v2, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    .line 550
    invoke-static {v0, v1}, LaGt;->a(LaGv;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setLogo(I)V

    .line 551
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    const v1, 0x102002c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 555
    if-eqz v0, :cond_0

    .line 556
    iget-object v1, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 557
    sget v2, Lxa;->action_bar_homebutton_right_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 558
    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 562
    :cond_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "action_bar_title"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 563
    iget-object v1, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_1

    .line 565
    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 568
    :cond_1
    return-void
.end method

.method public a(Landroid/view/MenuItem;Lara;)V
    .locals 4

    .prologue
    .line 308
    if-nez p1, :cond_1

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;

    if-nez v0, :cond_2

    .line 314
    sget v0, Lxb;->ic_menu_search:I

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 317
    :cond_2
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    iput-object p2, p0, Lart;->a:Lara;

    .line 322
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 323
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    .line 324
    instance-of v2, v1, Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 328
    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lart;->a:Landroid/widget/SearchView;

    .line 330
    iget-object v1, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 331
    invoke-static {v1}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v2

    .line 332
    invoke-static {v1}, LakQ;->e(Landroid/content/res/Resources;)Z

    move-result v3

    .line 333
    if-nez v2, :cond_3

    if-nez v3, :cond_3

    .line 334
    sget v2, Lxi;->search_hint_short:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 335
    iget-object v2, p0, Lart;->a:Landroid/widget/SearchView;

    invoke-virtual {v2, v1}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 338
    :cond_3
    iget-object v1, p0, Lart;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lart;->a(Ljava/lang/String;)V

    .line 339
    iget-object v1, p0, Lart;->a:Landroid/widget/SearchView;

    iget-object v2, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 340
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    new-instance v1, Larw;

    invoke-direct {v1, p0, p1}, Larw;-><init>(Lart;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 367
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    new-instance v1, Larx;

    invoke-direct {v1, p0, p1}, Larx;-><init>(Lart;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 381
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    new-instance v1, Lary;

    invoke-direct {v1, p0, p1}, Lary;-><init>(Lart;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    .line 422
    new-instance v0, Larz;

    invoke-direct {v0, p0}, Larz;-><init>(Lart;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public a(Landroid/widget/Button;LaFO;)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lart;->a:LalO;

    invoke-virtual {v0, p2}, LalO;->a(LaFO;)V

    .line 213
    invoke-virtual {p0}, Lart;->a()LaFO;

    move-result-object v0

    invoke-virtual {p2, v0}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    invoke-super {p0, p1, p2}, LaqV;->a(Landroid/widget/Button;LaFO;)V

    .line 217
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_0

    .line 219
    invoke-direct {p0, v0}, Lart;->a(Landroid/app/ActionBar;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/Button;[Landroid/accounts/Account;LaqX;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 261
    .line 262
    invoke-direct {p0, p2}, Lart;->a([Landroid/accounts/Account;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lart;->a:[Landroid/accounts/Account;

    invoke-direct {p0, v1}, Lart;->a([Landroid/accounts/Account;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 264
    iget-object v1, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    .line 265
    if-eqz v8, :cond_0

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {v8, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 267
    invoke-virtual {v8, v7}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 269
    iput-object p2, p0, Lart;->a:[Landroid/accounts/Account;

    .line 270
    new-instance v3, Laru;

    invoke-direct {v3, p0, p2}, Laru;-><init>(Lart;[Landroid/accounts/Account;)V

    .line 283
    new-instance v0, LajV;

    iget-object v1, p0, Lart;->a:Landroid/app/Activity;

    sget v2, Lxe;->account_spinner:I

    iget-object v4, p0, Lart;->a:LabI;

    iget-object v5, p0, Lart;->a:LabF;

    invoke-direct/range {v0 .. v7}, LajV;-><init>(Landroid/content/Context;ILjava/util/List;LabI;LabF;ZZ)V

    .line 290
    new-instance v1, Larv;

    invoke-direct {v1, p0, p2, p3}, Larv;-><init>(Lart;[Landroid/accounts/Account;LaqX;)V

    invoke-virtual {v8, v0, v1}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 299
    invoke-direct {p0, v8}, Lart;->a(Landroid/app/ActionBar;)V

    .line 301
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 528
    iput-object p1, p0, Lart;->a:Ljava/lang/String;

    .line 529
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 531
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 533
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V
    .locals 6

    .prologue
    .line 498
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lart;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lart;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    sget-object v1, Larj;->b:Larj;

    invoke-interface {v0, v1}, Larg;->a(Larj;)V

    .line 524
    :goto_0
    return-void

    .line 508
    :cond_0
    if-nez p5, :cond_1

    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    if-nez v0, :cond_2

    .line 509
    :cond_1
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    const-string v1, "search"

    .line 510
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 511
    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    goto :goto_0

    .line 517
    :cond_2
    if-eqz p1, :cond_3

    .line 518
    iput-object p1, p0, Lart;->a:Ljava/lang/String;

    .line 519
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 521
    :cond_3
    iput-object p4, p0, Lart;->a:Landroid/os/Bundle;

    .line 522
    iput-object p3, p0, Lart;->a:Landroid/content/ComponentName;

    .line 523
    iget-object v0, p0, Lart;->a:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 594
    if-eqz p1, :cond_0

    .line 595
    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 599
    :goto_0
    return-void

    .line 597
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 188
    iget-object v0, p0, Lart;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lart;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    invoke-interface {v0, p1}, Larg;->a(Landroid/view/MenuItem;)V

    .line 191
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x102002c

    if-ne v0, v2, :cond_2

    .line 192
    invoke-direct {p0}, Lart;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lart;->a:LarB;

    invoke-virtual {p0}, Lart;->a()LaFO;

    move-result-object v2

    invoke-interface {v0, v2}, LarB;->c(LaFO;)V

    :goto_0
    move v0, v1

    .line 205
    :goto_1
    return v0

    .line 195
    :cond_1
    iget-object v0, p0, Lart;->a:LarB;

    invoke-virtual {p0}, Lart;->a()LaFO;

    move-result-object v2

    invoke-interface {v0, v2}, LarB;->b(LaFO;)V

    goto :goto_0

    .line 198
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v2, Lxc;->menu_create_new_doc:I

    if-ne v0, v2, :cond_3

    .line 199
    iget-object v0, p0, Lart;->a:LarB;

    invoke-virtual {p0}, Lart;->a()LaFO;

    move-result-object v2

    invoke-interface {v0, v2}, LarB;->a(LaFO;)V

    move v0, v1

    .line 200
    goto :goto_1

    .line 201
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v2, Lxc;->menu_search:I

    if-ne v0, v2, :cond_4

    .line 202
    iget-object v0, p0, Lart;->a:LarB;

    invoke-interface {v0}, LarB;->a()V

    move v0, v1

    .line 203
    goto :goto_1

    .line 205
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 630
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 631
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_1

    .line 227
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 228
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 230
    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 572
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    invoke-static {v0}, LakQ;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 589
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    iget-object v0, p0, Lart;->a:Landroid/app/Activity;

    const v1, 0x102002c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 578
    :goto_1
    if-eqz v0, :cond_0

    .line 579
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 580
    if-eqz p1, :cond_3

    .line 581
    iget-object v1, p0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 582
    sget v2, Lxa;->action_bar_homebutton_left_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 583
    sget v3, Lxa;->action_bar_homebutton_right_margin:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 584
    invoke-virtual {v0, v2, v4, v1, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 577
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 586
    :cond_3
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lart;->a:LabI;

    invoke-virtual {v0}, LabI;->c()V

    .line 616
    return-void
.end method

.method public j_()V
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lart;->a:LabI;

    invoke-virtual {v0}, LabI;->a()V

    .line 626
    return-void
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lart;->a:LabI;

    invoke-virtual {v0}, LabI;->d()V

    .line 621
    return-void
.end method
