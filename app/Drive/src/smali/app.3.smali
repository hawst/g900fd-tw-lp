.class Lapp;
.super Ljava/lang/Object;
.source "DocListLoadingManager.java"

# interfaces
.implements Lao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lao",
        "<",
        "LQX;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lapn;

.field final synthetic a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

.field final synthetic a:Z


# direct methods
.method constructor <init>(Lapn;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 277
    iput-object p1, p0, Lapp;->a:Lapn;

    iput-object p2, p0, Lapp;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iput-boolean p3, p0, Lapp;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)LbI;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "LbI",
            "<",
            "LQX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    invoke-static {}, LamV;->a()V

    .line 284
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LRa;

    move-result-object v0

    iget-object v1, p0, Lapp;->a:Lapn;

    invoke-static {v1}, Lapn;->a(Lapn;)LaFO;

    move-result-object v1

    iget-object v2, p0, Lapp;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v3, p0, Lapp;->a:Lapn;

    .line 285
    invoke-static {v3}, Lapn;->a(Lapn;)LIK;

    move-result-object v3

    iget-object v4, p0, Lapp;->a:Lapn;

    invoke-static {v4}, Lapn;->a(Lapn;)LzO;

    move-result-object v4

    .line 284
    invoke-virtual {v0, v1, v2, v3, v4}, LRa;->a(LaFO;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;LIK;LzO;)LQY;

    move-result-object v0

    .line 286
    iget-object v1, p0, Lapp;->a:Lapn;

    invoke-static {v1, v0}, Lapn;->a(Lapn;LbI;)LbI;

    .line 287
    return-object v0
.end method

.method public a(LbI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbI",
            "<",
            "LQX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 353
    return-void
.end method

.method public a(LbI;LQX;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbI",
            "<",
            "LQX;",
            ">;",
            "LQX;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 293
    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 294
    invoke-static {}, LamV;->a()V

    .line 295
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LbI;

    move-result-object v0

    if-eq p1, v0, :cond_2

    .line 296
    const-string v0, "DocListLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignored stale loaded results from outdated loader: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 297
    invoke-virtual {p2}, LQX;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    .line 296
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 349
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 293
    goto :goto_0

    .line 301
    :cond_2
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LtK;

    move-result-object v0

    sget-object v3, Lry;->aH:Lry;

    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 303
    invoke-virtual {p2}, LQX;->a()LaFX;

    move-result-object v0

    if-nez v0, :cond_4

    .line 304
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LcI;

    move-result-object v0

    iget-object v3, p0, Lapp;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0, v3}, LcI;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    :goto_2
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 311
    const-string v0, "DocListLoader"

    const-string v3, "Queueing preload."

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 312
    iget-object v3, p0, Lapp;->a:Lapn;

    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-static {v3, v0, v1}, Lapn;->a(Lapn;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)V

    .line 320
    :cond_3
    :goto_3
    iget-boolean v0, p0, Lapp;->a:Z

    if-nez v0, :cond_0

    .line 324
    invoke-virtual {p2}, LQX;->a()LaFX;

    move-result-object v0

    if-nez v0, :cond_7

    .line 325
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LaGO;

    move-result-object v1

    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapt;

    move-result-object v0

    invoke-interface {v0}, Lapt;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {p2}, LQX;->a()LaGT;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LaGO;->a(Landroid/app/Activity;LaGT;)V

    goto :goto_1

    .line 306
    :cond_4
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LcI;

    move-result-object v0

    iget-object v3, p0, Lapp;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0, v3, p2}, LcI;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 316
    :cond_5
    iget-boolean v0, p0, Lapp;->a:Z

    if-nez v0, :cond_6

    :goto_4
    invoke-static {v1}, LbiT;->b(Z)V

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4

    .line 329
    :cond_7
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0, p2}, Lapn;->a(Lapn;LQX;)LQX;

    .line 331
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 332
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapt;

    move-result-object v0

    invoke-interface {v0}, Lapt;->a()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 333
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapt;

    move-result-object v0

    invoke-interface {v0, p2}, Lapt;->a(LQX;)V

    .line 345
    :goto_5
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->b(Lapn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapu;

    .line 346
    invoke-interface {v0, p2}, Lapu;->a(LQX;)V

    goto :goto_6

    .line 335
    :cond_8
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0, v2}, Lapn;->a(Lapn;Z)Z

    .line 336
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LIK;

    move-result-object v0

    if-nez v0, :cond_9

    .line 337
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-virtual {p2}, LQX;->a()LIK;

    move-result-object v1

    invoke-static {v0, v1}, Lapn;->a(Lapn;LIK;)LIK;

    .line 339
    :cond_9
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapt;

    move-result-object v0

    invoke-virtual {p2}, LQX;->a()LaFM;

    move-result-object v1

    invoke-interface {v0, v1}, Lapt;->setAccount(LaFM;)V

    .line 340
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapt;

    move-result-object v0

    invoke-virtual {p2}, LQX;->a()LzO;

    move-result-object v1

    invoke-interface {v0, v1}, Lapt;->setArrangementMode(LzO;)V

    .line 341
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapt;

    move-result-object v0

    invoke-interface {v0, p2}, Lapt;->b(LQX;)V

    goto :goto_5

    .line 348
    :cond_a
    iget-object v0, p0, Lapp;->a:Lapn;

    invoke-static {v0}, Lapn;->b(Lapn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_1
.end method

.method public bridge synthetic a(LbI;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 277
    check-cast p2, LQX;

    invoke-virtual {p0, p1, p2}, Lapp;->a(LbI;LQX;)V

    return-void
.end method
