.class public LapP;
.super Ljava/lang/Object;
.source "GifPreviewPage.java"

# interfaces
.implements Laqz;


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private final a:Landroid/view/LayoutInflater;

.field private a:Lcom/google/android/apps/docs/view/GestureImageView;


# direct methods
.method private constructor <init>(LbxA;Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iput-object p2, p0, LapP;->a:Landroid/view/LayoutInflater;

    .line 43
    iput-object p1, p0, LapP;->a:Landroid/graphics/drawable/Drawable;

    .line 44
    return-void
.end method

.method synthetic constructor <init>(LbxA;Landroid/view/LayoutInflater;LapQ;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, LapP;-><init>(LbxA;Landroid/view/LayoutInflater;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, LapP;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, LapP;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()V

    .line 60
    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, LapP;->a:Landroid/view/LayoutInflater;

    sget v1, Lxe;->gif_preview_page:I

    const/4 v2, 0x1

    .line 49
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 51
    sget v1, Lxc;->gif:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/GestureImageView;

    iput-object v0, p0, LapP;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    .line 52
    iget-object v0, p0, LapP;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v1, p0, LapP;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/GestureImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 53
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, LapP;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, LapP;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->b()V

    .line 67
    :cond_0
    return-void
.end method
