.class Lbiz;
.super Lbih;
.source "CharMatcher.java"


# instance fields
.field final o:Lbih;

.field final p:Lbih;


# direct methods
.method constructor <init>(Lbih;Lbih;)V
    .locals 2

    .prologue
    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CharMatcher.or("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lbiz;-><init>(Lbih;Lbih;Ljava/lang/String;)V

    .line 770
    return-void
.end method

.method constructor <init>(Lbih;Lbih;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0, p3}, Lbih;-><init>(Ljava/lang/String;)V

    .line 764
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbih;

    iput-object v0, p0, Lbiz;->o:Lbih;

    .line 765
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbih;

    iput-object v0, p0, Lbiz;->p:Lbih;

    .line 766
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)Lbih;
    .locals 3

    .prologue
    .line 786
    new-instance v0, Lbiz;

    iget-object v1, p0, Lbiz;->o:Lbih;

    iget-object v2, p0, Lbiz;->p:Lbih;

    invoke-direct {v0, v1, v2, p1}, Lbiz;-><init>(Lbih;Lbih;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(C)Z
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lbiz;->o:Lbih;

    invoke-virtual {v0, p1}, Lbih;->a(C)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbiz;->p:Lbih;

    invoke-virtual {v0, p1}, Lbih;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 758
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, Lbih;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method
