.class final LbkA;
.super LbkB;
.source "LocalCache.java"

# interfaces
.implements Lbkr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LbkB",
        "<TK;TV;>;",
        "Lbkr",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field a:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile b:J

.field b:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILbkr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 1213
    invoke-direct {p0, p1, p2, p3}, LbkB;-><init>(Ljava/lang/Object;ILbkr;)V

    .line 1218
    iput-wide v2, p0, LbkA;->a:J

    .line 1230
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkA;->a:Lbkr;

    .line 1243
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkA;->b:Lbkr;

    .line 1258
    iput-wide v2, p0, LbkA;->b:J

    .line 1270
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkA;->c:Lbkr;

    .line 1283
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkA;->d:Lbkr;

    .line 1214
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1222
    iget-wide v0, p0, LbkA;->a:J

    return-wide v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1227
    iput-wide p1, p0, LbkA;->a:J

    .line 1228
    return-void
.end method

.method public a(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1240
    iput-object p1, p0, LbkA;->a:Lbkr;

    .line 1241
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1262
    iget-wide v0, p0, LbkA;->b:J

    return-wide v0
.end method

.method public b()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1235
    iget-object v0, p0, LbkA;->a:Lbkr;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1267
    iput-wide p1, p0, LbkA;->b:J

    .line 1268
    return-void
.end method

.method public b(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1253
    iput-object p1, p0, LbkA;->b:Lbkr;

    .line 1254
    return-void
.end method

.method public c()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1248
    iget-object v0, p0, LbkA;->b:Lbkr;

    return-object v0
.end method

.method public c(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1280
    iput-object p1, p0, LbkA;->c:Lbkr;

    .line 1281
    return-void
.end method

.method public d()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1275
    iget-object v0, p0, LbkA;->c:Lbkr;

    return-object v0
.end method

.method public d(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1293
    iput-object p1, p0, LbkA;->d:Lbkr;

    .line 1294
    return-void
.end method

.method public e()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1288
    iget-object v0, p0, LbkA;->d:Lbkr;

    return-object v0
.end method
