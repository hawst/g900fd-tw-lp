.class public LWK;
.super Ljava/lang/Object;
.source "LegacyPrinter.java"

# interfaces
.implements LWL;


# instance fields
.field private final a:LaKR;

.field private final a:Landroid/content/Context;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Landroid/content/Context;LaKR;LtK;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, LWK;->a:Landroid/content/Context;

    .line 31
    iput-object p2, p0, LWK;->a:LaKR;

    .line 32
    iput-object p3, p0, LWK;->a:LtK;

    .line 33
    return-void
.end method

.method private a(LaGu;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, LWK;->a:LtK;

    sget-object v2, Lry;->ah:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-object v0

    .line 46
    :cond_1
    invoke-interface {p1}, LaGu;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    iget-object v1, p0, LWK;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    invoke-interface {p1}, LaGu;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    iget-object v0, p0, LWK;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Landroid/content/Context;LaGu;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(LaGu;)V
    .locals 3

    .prologue
    .line 65
    invoke-direct {p0, p1}, LWK;->a(LaGu;)Landroid/content/Intent;

    move-result-object v0

    .line 66
    if-nez v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    if-eqz v0, :cond_0

    .line 72
    :try_start_0
    iget-object v1, p0, LWK;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    const-string v1, "LegacyPrinter"

    const-string v2, "Failed to print"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(LaGu;)Z
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, LWK;->a(LaGu;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
