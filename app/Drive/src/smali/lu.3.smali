.class public final Llu;
.super Ljava/lang/Object;
.source "MediaRouter.java"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llv;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LkR;

.field private final a:LkV;

.field private a:LkX;


# direct methods
.method constructor <init>(LkR;)V
    .locals 1

    .prologue
    .line 1262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1255
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llu;->a:Ljava/util/ArrayList;

    .line 1263
    iput-object p1, p0, Llu;->a:LkR;

    .line 1264
    invoke-virtual {p1}, LkR;->a()LkV;

    move-result-object v0

    iput-object v0, p0, Llu;->a:LkV;

    .line 1265
    return-void
.end method

.method static synthetic a(Llu;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 1253
    iget-object v0, p0, Llu;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Llu;)LkR;
    .locals 1

    .prologue
    .line 1253
    iget-object v0, p0, Llu;->a:LkR;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1321
    iget-object v0, p0, Llu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1322
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1323
    iget-object v0, p0, Llu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    invoke-static {v0}, Llv;->a(Llv;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1327
    :goto_1
    return v0

    .line 1322
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1327
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 1286
    iget-object v0, p0, Llu;->a:LkV;

    invoke-virtual {v0}, LkV;->a()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Llu;->a:LkV;

    invoke-virtual {v0}, LkV;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()LkR;
    .locals 1

    .prologue
    .line 1271
    invoke-static {}, Llj;->a()V

    .line 1272
    iget-object v0, p0, Llu;->a:LkR;

    return-object v0
.end method

.method a(LkX;)Z
    .locals 1

    .prologue
    .line 1313
    iget-object v0, p0, Llu;->a:LkX;

    if-eq v0, p1, :cond_0

    .line 1314
    iput-object p1, p0, Llu;->a:LkX;

    .line 1315
    const/4 v0, 0x1

    .line 1317
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MediaRouter.RouteProviderInfo{ packageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Llu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
