.class public LabG;
.super Ljava/lang/Object;
.source "ContactManagerImpl.java"

# interfaces
.implements LabF;


# instance fields
.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LabG;->a:Ljava/util/Locale;

    .line 36
    iput-object p1, p0, LabG;->a:Lbxw;

    .line 37
    return-void
.end method

.method private a(Ljava/lang/String;)LabD;
    .locals 11

    .prologue
    .line 100
    iget-object v1, p0, LabG;->a:Lbxw;

    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 101
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 102
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "contact_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "display_name"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "photo_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "lookup"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data1=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "is_super_primary DESC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 105
    const/4 v1, 0x0

    .line 107
    if-eqz v9, :cond_1

    .line 110
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    const-string v1, "contact_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 112
    const-string v1, "display_name"

    invoke-static {v9, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 113
    const-string v1, "lookup"

    invoke-static {v9, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-static {v2, v3, v1}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 115
    const-string v1, "photo_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 116
    new-instance v1, LabE;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object p1, v5, v10

    .line 117
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct/range {v1 .. v8}, LabE;-><init>(JLjava/lang/String;Ljava/util/List;Landroid/net/Uri;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 124
    :cond_1
    if-nez v1, :cond_2

    .line 125
    new-instance v1, LabE;

    const-wide/16 v2, 0x0

    const-string v4, ""

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    invoke-direct/range {v1 .. v8}, LabE;-><init>(JLjava/lang/String;Ljava/util/List;Landroid/net/Uri;J)V

    .line 128
    :cond_2
    return-object v1

    .line 120
    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method


# virtual methods
.method public a(Ljava/lang/String;Lqx;)LabD;
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    .line 43
    sget-object v1, LabH;->a:[I

    invoke-virtual {p2}, Lqx;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 52
    invoke-direct {p0, p1}, LabG;->a(Ljava/lang/String;)LabD;

    move-result-object v1

    :goto_0
    return-object v1

    .line 45
    :pswitch_0
    new-instance v1, LabE;

    iget-object v4, p0, LabG;->a:Lbxw;

    .line 46
    invoke-interface {v4}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    sget v6, Lxi;->default_scope_contact_name:I

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-wide v7, v2

    invoke-direct/range {v1 .. v8}, LabE;-><init>(JLjava/lang/String;Ljava/util/List;Landroid/net/Uri;J)V

    goto :goto_0

    .line 48
    :pswitch_1
    if-eqz p1, :cond_0

    move-object v4, p1

    .line 50
    :goto_1
    new-instance v1, LabE;

    move-object v6, v5

    move-wide v7, v2

    invoke-direct/range {v1 .. v8}, LabE;-><init>(JLjava/lang/String;Ljava/util/List;Landroid/net/Uri;J)V

    goto :goto_0

    .line 48
    :cond_0
    iget-object v1, p0, LabG;->a:Lbxw;

    .line 49
    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    sget v4, Lxi;->domain_scope_contact_name:I

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LabD;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 64
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v3, v2

    const-string v1, "display_name"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "photo_id"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "lookup"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "data1"

    aput-object v2, v3, v1

    .line 71
    iget-object v1, p0, LabG;->a:Lbxw;

    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 72
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 75
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "data1 NOTNULL"

    const/4 v5, 0x0

    const-string v6, "UPPER(display_name), display_name ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 78
    if-eqz v10, :cond_1

    .line 80
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const-string v1, "contact_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 82
    const-string v1, "display_name"

    invoke-static {v10, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 83
    const-string v1, "lookup"

    invoke-static {v10, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84
    const-string v5, "data1"

    invoke-static {v10, v5}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 85
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LabG;->a:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 86
    invoke-static {v2, v3, v1}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 87
    const-string v1, "photo_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 88
    new-instance v1, LabE;

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object v5, v11, v12

    .line 89
    invoke-static {v11}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct/range {v1 .. v8}, LabE;-><init>(JLjava/lang/String;Ljava/util/List;Landroid/net/Uri;J)V

    .line 90
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 96
    :cond_1
    return-object v9
.end method

.method public a()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LabG;->a:Ljava/util/Locale;

    return-object v0
.end method
