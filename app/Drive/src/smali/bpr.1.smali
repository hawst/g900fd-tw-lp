.class final Lbpr;
.super Ljava/lang/Object;
.source "Multisets.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private final a:Lbpi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpi",
            "<TE;>;"
        }
    .end annotation
.end field

.field private a:Lbpj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpj",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lbpj",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I


# direct methods
.method constructor <init>(Lbpi;Ljava/util/Iterator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbpi",
            "<TE;>;",
            "Ljava/util/Iterator",
            "<",
            "Lbpj",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1027
    iput-object p1, p0, Lbpr;->a:Lbpi;

    .line 1028
    iput-object p2, p0, Lbpr;->a:Ljava/util/Iterator;

    .line 1029
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 1032
    iget v0, p0, Lbpr;->a:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lbpr;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1036
    invoke-virtual {p0}, Lbpr;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1037
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1039
    :cond_0
    iget v0, p0, Lbpr;->a:I

    if-nez v0, :cond_1

    .line 1040
    iget-object v0, p0, Lbpr;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    iput-object v0, p0, Lbpr;->a:Lbpj;

    .line 1041
    iget-object v0, p0, Lbpr;->a:Lbpj;

    invoke-interface {v0}, Lbpj;->a()I

    move-result v0

    iput v0, p0, Lbpr;->a:I

    iput v0, p0, Lbpr;->b:I

    .line 1043
    :cond_1
    iget v0, p0, Lbpr;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbpr;->a:I

    .line 1044
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbpr;->a:Z

    .line 1045
    iget-object v0, p0, Lbpr;->a:Lbpj;

    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 1049
    iget-boolean v0, p0, Lbpr;->a:Z

    invoke-static {v0}, Lbnr;->a(Z)V

    .line 1050
    iget v0, p0, Lbpr;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1051
    iget-object v0, p0, Lbpr;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1055
    :goto_0
    iget v0, p0, Lbpr;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbpr;->b:I

    .line 1056
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbpr;->a:Z

    .line 1057
    return-void

    .line 1053
    :cond_0
    iget-object v0, p0, Lbpr;->a:Lbpi;

    iget-object v1, p0, Lbpr;->a:Lbpj;

    invoke-interface {v1}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lbpi;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
