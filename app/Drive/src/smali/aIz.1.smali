.class LaIz;
.super Ljava/lang/Object;
.source "SyncEngine.java"

# interfaces
.implements Lafg;


# instance fields
.field final synthetic a:LaIC;

.field final synthetic a:LaIy;

.field final synthetic a:Landroid/net/Uri;


# direct methods
.method constructor <init>(LaIy;Landroid/net/Uri;LaIC;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, LaIz;->a:LaIy;

    iput-object p2, p0, LaIz;->a:Landroid/net/Uri;

    iput-object p3, p0, LaIz;->a:LaIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZZ)V
    .locals 6

    .prologue
    .line 125
    iget-object v1, p0, LaIz;->a:LaIy;

    monitor-enter v1

    .line 127
    :try_start_0
    const-string v0, "SyncEngine"

    const-string v2, "Sync finished, notify: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LaIz;->a:Landroid/net/Uri;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 128
    iget-object v0, p0, LaIz;->a:LaIy;

    invoke-static {v0}, LaIy;->a(LaIy;)LbuE;

    move-result-object v0

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, LaIz;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 129
    iget-object v0, p0, LaIz;->a:LaIy;

    invoke-static {v0}, LaIy;->a(LaIy;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, LaIz;->a:LaIC;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 130
    monitor-exit v1

    .line 131
    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
