.class public abstract LbdS;
.super Ljava/lang/Object;
.source "AbstractInputStreamContent.java"

# interfaces
.implements LbdZ;


# instance fields
.field private a:Ljava/lang/String;

.field private a:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, LbdS;->a:Z

    .line 58
    invoke-virtual {p0, p1}, LbdS;->a(Ljava/lang/String;)LbdS;

    .line 59
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LbdS;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, LbdS;->a:Ljava/lang/String;

    .line 97
    return-object p0
.end method

.method public a(Z)LbdS;
    .locals 0

    .prologue
    .line 107
    iput-boolean p1, p0, LbdS;->a:Z

    .line 108
    return-object p0
.end method

.method public abstract a()Ljava/io/InputStream;
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, LbdS;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, LbdS;->a()Ljava/io/InputStream;

    move-result-object v0

    iget-boolean v1, p0, LbdS;->a:Z

    invoke-static {v0, p1, v1}, LbfD;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    .line 73
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 74
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, LbdS;->a:Z

    return v0
.end method
