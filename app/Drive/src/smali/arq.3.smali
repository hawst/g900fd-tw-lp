.class public Larq;
.super Ljava/lang/Object;
.source "ActionBarModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method provideActionBarHelper(Landroid/content/Context;)LaqY;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 29
    const-class v0, LaqY;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqY;

    return-object v0
.end method

.method provideActionBarHelperFactory(LarA;)LaqZ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 23
    return-object p1
.end method

.method provideActionBarIconsAlpha(Laro;)Larh;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 41
    return-object p1
.end method

.method provideActionBarModeSwitcher(Landroid/content/Context;)Larg;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 35
    const-class v0, Larg;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    return-object v0
.end method
