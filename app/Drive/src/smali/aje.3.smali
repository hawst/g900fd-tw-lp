.class Laje;
.super Lbuo;
.source "ContextScopedProviderReflectModule.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LbuC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "LbuC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Lbuo;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laje;->a:Ljava/util/List;

    .line 25
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuC;

    .line 26
    iget-object v2, p0, Laje;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method

.method private a(Lbuv;LbuE;LbuE;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;",
            "LbuE",
            "<",
            "LaiU;",
            ">;",
            "LbuE",
            "<",
            "LaiW;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 49
    const-class v0, Laja;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    .line 50
    invoke-virtual {p1}, Lbuv;->a()LbuP;

    move-result-object v2

    invoke-virtual {v2}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v2

    aput-object v2, v1, v4

    .line 49
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 51
    const-class v1, Lajw;

    new-array v2, v3, [Ljava/lang/reflect/Type;

    .line 52
    invoke-virtual {p1}, Lbuv;->a()LbuP;

    move-result-object v3

    invoke-virtual {v3}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v3

    aput-object v3, v2, v4

    .line 51
    invoke-static {v1, v2}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v2

    .line 53
    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/annotation/Annotation;

    move-result-object v3

    .line 57
    if-eqz v3, :cond_0

    .line 59
    invoke-static {v0, v3}, Lbuv;->a(Ljava/lang/reflect/Type;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v1

    .line 60
    invoke-static {v2, v3}, Lbuv;->a(Ljava/lang/reflect/Type;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 67
    :goto_0
    new-instance v2, Lajb;

    .line 68
    invoke-virtual {p0, p1}, Laje;->a(Lbuv;)LbuE;

    move-result-object v3

    invoke-direct {v2, v3, p2, p3}, Lajb;-><init>(LbuE;LbuE;LbuE;)V

    .line 69
    invoke-virtual {p0, v1}, Laje;->a(Lbuv;)LbuT;

    move-result-object v1

    invoke-interface {v1, v2}, LbuT;->a(LbuE;)LbuU;

    .line 70
    new-instance v1, Lajx;

    invoke-direct {v1, v2}, Lajx;-><init>(Lbxw;)V

    .line 71
    invoke-virtual {p0, v0}, Laje;->a(Lbuv;)LbuT;

    move-result-object v0

    invoke-interface {v0, v1}, LbuT;->a(Lbxw;)LbuU;

    .line 72
    return-void

    .line 63
    :cond_0
    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/Class;

    move-result-object v1

    .line 62
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lbuv;

    move-result-object v1

    .line 64
    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v2, v0}, Lbuv;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    .line 32
    new-instance v1, LajF;

    invoke-virtual {p0}, Laje;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-direct {v1, v0}, LajF;-><init>(Lcom/google/inject/Binder;)V

    .line 33
    iget-object v0, p0, Laje;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuC;

    .line 34
    invoke-virtual {v1, v0}, LajF;->a(LbuC;)V

    goto :goto_0

    .line 37
    :cond_0
    const-class v0, LaiU;

    invoke-virtual {p0, v0}, Laje;->a(Ljava/lang/Class;)LbuE;

    move-result-object v2

    .line 38
    const-class v0, LaiW;

    invoke-virtual {p0, v0}, Laje;->a(Ljava/lang/Class;)LbuE;

    move-result-object v3

    .line 40
    invoke-virtual {v1}, LajF;->a()Ljava/util/Set;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    .line 42
    invoke-direct {p0, v0, v2, v3}, Laje;->a(Lbuv;LbuE;LbuE;)V

    goto :goto_1

    .line 44
    :cond_1
    return-void
.end method
