.class abstract enum LHA;
.super Ljava/lang/Enum;
.source "DocGridAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LHA;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LHA;

.field private static final synthetic a:[LHA;

.field public static final enum b:LHA;

.field public static final enum c:LHA;

.field public static final enum d:LHA;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    new-instance v0, LHB;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v2, v2}, LHB;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHA;->a:LHA;

    .line 122
    new-instance v0, LHC;

    const-string v1, "GRID_ROW"

    invoke-direct {v0, v1, v3, v3}, LHC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHA;->b:LHA;

    .line 129
    new-instance v0, LHD;

    const-string v1, "GRID_FOLDER_ROW"

    invoke-direct {v0, v1, v4, v4}, LHD;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHA;->c:LHA;

    .line 136
    new-instance v0, LHE;

    const-string v1, "EMPTY_SECTION_HEADER"

    invoke-direct {v0, v1, v5, v5}, LHE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHA;->d:LHA;

    .line 114
    const/4 v0, 0x4

    new-array v0, v0, [LHA;

    sget-object v1, LHA;->a:LHA;

    aput-object v1, v0, v2

    sget-object v1, LHA;->b:LHA;

    aput-object v1, v0, v3

    sget-object v1, LHA;->c:LHA;

    aput-object v1, v0, v4

    sget-object v1, LHA;->d:LHA;

    aput-object v1, v0, v5

    sput-object v0, LHA;->a:[LHA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 147
    iput p3, p0, LHA;->a:I

    .line 148
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILHv;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, LHA;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LHA;
    .locals 1

    .prologue
    .line 114
    const-class v0, LHA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LHA;

    return-object v0
.end method

.method public static values()[LHA;
    .locals 1

    .prologue
    .line 114
    sget-object v0, LHA;->a:[LHA;

    invoke-virtual {v0}, [LHA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LHA;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, LHA;->a:I

    return v0
.end method

.method public abstract a(LHu;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method
