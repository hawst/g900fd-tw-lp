.class public Lth;
.super Ljava/lang/Object;
.source "DocumentPreviewActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lth;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V
    .locals 0

    .prologue
    .line 552
    invoke-direct {p0, p1}, Lth;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 555
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 556
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 566
    :goto_0
    return v0

    .line 559
    :sswitch_0
    iget-object v2, p0, Lth;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-static {v2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a(Z)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 566
    goto :goto_0

    .line 556
    nop

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x3e -> :sswitch_0
    .end sparse-switch
.end method
