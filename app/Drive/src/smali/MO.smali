.class public abstract LMO;
.super Ljava/lang/Object;
.source "BaseDocsUploaderImpl.java"

# interfaces
.implements LNi;


# instance fields
.field protected final a:I

.field protected a:J

.field final synthetic a:LMN;

.field a:LMZ;

.field protected final a:LZQ;

.field protected final a:LagH;

.field protected a:Ljava/io/RandomAccessFile;

.field final a:Ljava/util/regex/Pattern;


# direct methods
.method constructor <init>(LMN;LMZ;LagH;)V
    .locals 8

    .prologue
    .line 144
    iput-object p1, p0, LMO;->a:LMN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const-string v0, "(?:bytes? *=? *)?(\\d+)-(-?\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LMO;->a:Ljava/util/regex/Pattern;

    .line 145
    iput-object p2, p0, LMO;->a:LMZ;

    .line 146
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LMO;->a:J

    .line 147
    iput-object p3, p0, LMO;->a:LagH;

    .line 152
    invoke-static {p1}, LMN;->a(LMN;)LQr;

    move-result-object v1

    monitor-enter v1

    .line 153
    :try_start_0
    invoke-static {p1}, LMN;->a(LMN;)LQr;

    move-result-object v0

    const-string v2, "maxUploadChunkRetryCount1"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LMO;->a:I

    .line 155
    const-wide v2, 0x408f400000000000L    # 1000.0

    invoke-static {p1}, LMN;->a(LMN;)LQr;

    move-result-object v0

    const-string v4, "initialChunkBackOff"

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-interface {v0, v4, v6, v7}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-long v2, v2

    .line 157
    invoke-static {p1}, LMN;->a(LMN;)LQr;

    move-result-object v0

    const-string v4, "chunkBackoffGrowthFactor"

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-interface {v0, v4, v6, v7}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v4

    .line 159
    new-instance v0, LZQ;

    invoke-direct {v0, v2, v3, v4, v5}, LZQ;-><init>(JD)V

    iput-object v0, p0, LMO;->a:LZQ;

    .line 161
    monitor-exit v1

    .line 162
    return-void

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/String;)LMP;
    .locals 6

    .prologue
    .line 471
    iget-object v0, p0, LMO;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 472
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_0

    .line 473
    new-instance v0, LNh;

    const-string v1, "Unable to upload file: invalid byte range returned by server."

    invoke-direct {v0, v1}, LNh;-><init>(Ljava/lang/String;)V

    throw v0

    .line 476
    :cond_0
    new-instance v1, LMP;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LMP;-><init>(JJ)V

    return-object v1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 484
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 486
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, v2, v0

    .line 487
    const-string v5, "%"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    and-int/lit16 v4, v4, 0xff

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 490
    :catch_0
    move-exception v0

    .line 491
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "UTF-8 should always be supported"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 493
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 494
    return-object v0
.end method


# virtual methods
.method protected a()LaJT;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, LMO;->a()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 275
    invoke-virtual {p0, v0}, LMO;->a(Lorg/apache/http/client/methods/HttpUriRequest;)LaJT;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaJT;)LaJT;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 168
    iget-object v0, p0, LMO;->a:LZQ;

    invoke-virtual {v0}, LZQ;->c()V

    move-object v1, p1

    .line 169
    :goto_0
    if-nez v1, :cond_2

    .line 170
    invoke-static {}, LamS;->a()V

    .line 174
    :try_start_0
    invoke-virtual {p0}, LMO;->a()LaJT;
    :try_end_0
    .catch LNj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 176
    :try_start_1
    iget-object v0, p0, LMO;->a:LZQ;

    invoke-virtual {v0}, LZQ;->c()V
    :try_end_1
    .catch LNj; {:try_start_1 .. :try_end_1} :catch_2

    move-object v4, v1

    move v1, v2

    .line 184
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 185
    invoke-static {}, LamS;->a()V

    .line 187
    const-wide/16 v6, -0x1

    iput-wide v6, p0, LMO;->a:J

    .line 188
    invoke-virtual {p0}, LMO;->a()V

    .line 190
    invoke-static {}, LamS;->a()V

    .line 192
    :try_start_2
    invoke-virtual {p0}, LMO;->b()LaJT;
    :try_end_2
    .catch LNj; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    move-object v4, v1

    move v1, v2

    .line 200
    goto :goto_1

    .line 177
    :catch_0
    move-exception v0

    move-object v4, v1

    move v1, v3

    .line 178
    :goto_2
    const-string v5, "BaseDocsUploaderImpl"

    const-string v6, "Retrying chunk - failures %d"

    new-array v7, v3, [Ljava/lang/Object;

    iget-object v8, p0, LMO;->a:LZQ;

    invoke-virtual {v8}, LZQ;->a()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v5, v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 179
    iget-object v5, p0, LMO;->a:LZQ;

    invoke-virtual {v5}, LZQ;->a()I

    move-result v5

    iget v6, p0, LMO;->a:I

    if-lt v5, v6, :cond_0

    .line 180
    new-instance v1, LNh;

    const-string v2, "Error uploading after multiple retries."

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 194
    :catch_1
    move-exception v0

    .line 195
    const-string v5, "BaseDocsUploaderImpl"

    const-string v6, "Upload status request failed - failures %d"

    new-array v7, v3, [Ljava/lang/Object;

    iget-object v8, p0, LMO;->a:LZQ;

    .line 196
    invoke-virtual {v8}, LZQ;->a()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    .line 195
    invoke-static {v5, v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 197
    iget-object v5, p0, LMO;->a:LZQ;

    invoke-virtual {v5}, LZQ;->a()I

    move-result v5

    iget v6, p0, LMO;->a:I

    if-lt v5, v6, :cond_0

    .line 198
    new-instance v1, LNh;

    const-string v2, "Error uploading after multiple retries."

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    move-object v1, v4

    .line 202
    goto :goto_0

    .line 203
    :cond_2
    return-object v1

    .line 177
    :catch_2
    move-exception v0

    move-object v4, v1

    move v1, v2

    goto :goto_2
.end method

.method protected a(Lorg/apache/http/client/methods/HttpUriRequest;)LaJT;
    .locals 8

    .prologue
    .line 300
    iget-object v0, p0, LMO;->a:LMZ;

    invoke-virtual {v0}, LMZ;->a()LaFO;

    move-result-object v0

    .line 301
    iget-object v1, p0, LMO;->a:LMN;

    invoke-virtual {v1, v0, p1}, LMN;->a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 303
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 304
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 305
    const-string v3, "BaseDocsUploaderImpl"

    const-string v4, "uploading chunk returns %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 306
    sparse-switch v2, :sswitch_data_0

    .line 332
    new-instance v0, LNh;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to upload item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 334
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to upload "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, LMO;->a:LMZ;

    invoke-virtual {v3}, LMZ;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, LNh;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    :catchall_0
    move-exception v0

    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->a()V

    .line 338
    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->b()V

    throw v0

    .line 310
    :sswitch_0
    :try_start_1
    const-string v1, "BaseDocsUploaderImpl"

    const-string v3, "Server reports upload complete."

    invoke-static {v1, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312
    :try_start_2
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 313
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 314
    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LbwT;

    move-result-object v1

    const-class v3, LaeF;

    invoke-interface {v1, v3, v0}, LbwT;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxj;

    move-result-object v0

    .line 315
    invoke-interface {v0}, Lbxj;->b()Lbxb;

    move-result-object v0

    check-cast v0, LaJT;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 337
    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->a()V

    .line 338
    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->b()V

    :goto_0
    return-object v0

    .line 316
    :catch_0
    move-exception v0

    .line 317
    :try_start_3
    new-instance v1, LNh;

    const-string v3, "Error reading resulting Entry"

    invoke-direct {v1, v3, v0, v2}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1

    .line 318
    :catch_1
    move-exception v0

    .line 319
    new-instance v1, LNh;

    const-string v3, "Error parsing resulting Entry"

    invoke-direct {v1, v3, v0, v2}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1

    .line 323
    :sswitch_1
    invoke-virtual {p0, v0}, LMO;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 324
    const/4 v0, 0x0

    .line 337
    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->a()V

    .line 338
    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->b()V

    goto :goto_0

    .line 328
    :sswitch_2
    :try_start_4
    new-instance v0, LNj;

    const-string v1, "GData Service unavailable."

    invoke-direct {v0, v1}, LNj;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 306
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xc9 -> :sswitch_0
        0x134 -> :sswitch_1
        0x1f7 -> :sswitch_2
    .end sparse-switch
.end method

.method protected a()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 217
    const-string v0, "https://docs.google.com/feeds/upload/create-session/default/private/full"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 219
    iget-object v1, p0, LMO;->a:LMZ;

    invoke-virtual {v1}, LMZ;->b()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 221
    :goto_0
    if-eqz v1, :cond_0

    .line 222
    invoke-interface {v1}, LaFV;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 226
    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lafi;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)Lafi;

    move-result-object v1

    .line 228
    invoke-virtual {v1, v0}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 230
    :cond_0
    return-object v0

    .line 219
    :cond_1
    iget-object v1, p0, LMO;->a:LMN;

    .line 220
    invoke-static {v1}, LMN;->a(LMN;)LaGM;

    move-result-object v1

    iget-object v2, p0, LMO;->a:LMZ;

    invoke-virtual {v2}, LMZ;->b()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v1, v2}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v1

    goto :goto_0
.end method

.method a()Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 12

    .prologue
    .line 371
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    new-instance v1, Ljava/net/URI;

    iget-object v2, p0, LMO;->a:LMZ;

    invoke-virtual {v2}, LMZ;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    const-string v1, "Content-Type"

    iget-object v2, p0, LMO;->a:LMZ;

    invoke-virtual {v2}, LMZ;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v1, "GData-Version"

    const-string v2, "3.0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v1, p0, LMO;->a:LMZ;

    invoke-virtual {v1}, LMZ;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 380
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 381
    const-string v1, "Content-Length"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :goto_0
    return-object v0

    .line 372
    :catch_0
    move-exception v0

    .line 373
    new-instance v1, LNh;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LMO;->a:LMZ;

    invoke-virtual {v3}, LMZ;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 383
    :cond_0
    iget-wide v4, p0, LMO;->a:J

    sub-long/2addr v2, v4

    .line 384
    const-wide/32 v4, 0x40000

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 387
    const-string v1, "Content-Range"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "bytes %d-%d/%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, LMO;->a:J

    .line 388
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p0, LMO;->a:J

    add-long/2addr v6, v8

    const-wide/16 v10, 0x1

    sub-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, LMO;->a:LMZ;

    invoke-virtual {v6}, LMZ;->a()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    .line 387
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v1, p0, LMO;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 395
    new-instance v1, Laik;

    iget-object v3, p0, LMO;->a:LagH;

    iget-object v4, p0, LMO;->a:LMZ;

    .line 396
    invoke-virtual {v4}, LMZ;->a()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    iget-wide v6, p0, LMO;->a:J

    invoke-direct/range {v1 .. v7}, Laik;-><init>(Ljava/io/InputStream;LagH;JJ)V

    .line 397
    iget-wide v2, p0, LMO;->a:J

    add-long/2addr v2, v8

    iput-wide v2, p0, LMO;->a:J

    .line 398
    new-instance v2, Lorg/apache/http/entity/InputStreamEntity;

    invoke-direct {v2, v1, v8, v9}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 399
    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_0

    .line 392
    :catch_1
    move-exception v0

    .line 393
    new-instance v1, LNh;

    const-string v2, "Unable to read input file"

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method a(Ljava/lang/String;Z)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4

    .prologue
    .line 346
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 347
    if-eqz p2, :cond_0

    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :goto_0
    const-string v1, "GData-Version"

    const-string v2, "3.0"

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    if-eqz p2, :cond_1

    .line 354
    const-string v1, "If-Match"

    const-string v2, "*"

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :goto_1
    const-string v1, "Content-Type"

    iget-object v2, p0, LMO;->a:LMZ;

    invoke-virtual {v2}, LMZ;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v1, "X-Upload-Content-Type"

    iget-object v2, p0, LMO;->a:LMZ;

    invoke-virtual {v2}, LMZ;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v1, "X-Upload-Content-Length"

    iget-object v2, p0, LMO;->a:LMZ;

    .line 363
    invoke-virtual {v2}, LMZ;->a()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 362
    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    return-object v0

    .line 347
    :cond_0
    :try_start_1
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 348
    :catch_0
    move-exception v0

    .line 349
    new-instance v1, LNh;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 356
    :cond_1
    iget-object v1, p0, LMO;->a:LMZ;

    invoke-virtual {v1}, LMZ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LMO;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 357
    const-string v2, "Slug"

    invoke-interface {v0, v2, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, LMO;->a:LZQ;

    invoke-virtual {v0}, LZQ;->a()I

    move-result v0

    iget v1, p0, LMO;->a:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "backoff called after too many failures."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 210
    :try_start_0
    iget-object v0, p0, LMO;->a:LZQ;

    invoke-virtual {v0}, LZQ;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :goto_1
    return-void

    .line 207
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    const-string v0, "BaseDocsUploaderImpl"

    const-string v1, "Backoff sleep interrupted."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected abstract a(LMZ;Ljava/lang/String;)V
.end method

.method a(Lorg/apache/http/HttpResponse;)V
    .locals 12

    .prologue
    .line 425
    const-string v0, "Location"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 426
    iget-object v4, p0, LMO;->a:LMZ;

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v4, v3}, LMO;->a(LMZ;Ljava/lang/String;)V

    .line 425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 430
    :cond_0
    const-string v0, "Range"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 431
    new-instance v0, LMT;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resumable upload response missing range header. Response:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LMT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :cond_1
    const-string v0, "Range"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v0, v2, v1

    .line 435
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LMO;->a(Ljava/lang/String;)LMP;

    move-result-object v4

    .line 436
    const-string v0, "BaseDocsUploaderImpl"

    const-string v5, "Resumable upload range = %d-%d (expected %d)"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v4, LMP;->a:J

    .line 437
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, v4, LMP;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-wide v8, p0, LMO;->a:J

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    .line 436
    invoke-static {v0, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 438
    iget-wide v6, v4, LMP;->a:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    .line 441
    new-instance v0, LNh;

    const-string v1, "Unable to upload item: Bytes lost in transmission."

    invoke-direct {v0, v1}, LNh;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_2
    iget-wide v6, p0, LMO;->a:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    iget-wide v6, v4, LMP;->b:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iget-wide v8, p0, LMO;->a:J

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 445
    :goto_2
    if-eqz v0, :cond_3

    .line 446
    const-string v5, "BaseDocsUploaderImpl"

    const-string v6, "Upload server byterange mismatch: we sent %d bytes, server acks %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-wide v10, p0, LMO;->a:J

    .line 447
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-wide v10, v4, LMP;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    .line 446
    invoke-static {v5, v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 449
    :cond_3
    iget-wide v4, v4, LMP;->b:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, LMO;->a:J

    .line 451
    :try_start_0
    iget-object v4, p0, LMO;->a:Ljava/io/RandomAccessFile;

    iget-wide v6, p0, LMO;->a:J

    invoke-virtual {v4, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    if-eqz v0, :cond_5

    .line 457
    iget-object v0, p0, LMO;->a:LagH;

    iget-wide v2, p0, LMO;->a:J

    iget-object v1, p0, LMO;->a:LMZ;

    invoke-virtual {v1}, LMZ;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, LagH;->a(JJ)V

    .line 458
    invoke-virtual {p0}, LMO;->b()V

    .line 459
    new-instance v0, LNj;

    const-string v1, "Unable to upload item: Bytes lost in transmission."

    invoke-direct {v0, v1}, LNj;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 452
    :catch_0
    move-exception v0

    .line 453
    new-instance v1, LNh;

    const-string v2, "Error resending file data"

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 434
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 462
    :cond_6
    return-void
.end method

.method protected a(ZZLaFO;Landroid/net/Uri;Z)V
    .locals 5

    .prologue
    .line 235
    invoke-virtual {p4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "convert"

    .line 236
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ocr"

    .line 237
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p5}, LMO;->a(Ljava/lang/String;Z)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 242
    :try_start_0
    iget-object v1, p0, LMO;->a:LMN;

    invoke-virtual {v1, p3, v0}, LMN;->a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 244
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 245
    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 246
    new-instance v0, LNh;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to upload item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to upload "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LMO;->a:LMZ;

    .line 248
    invoke-virtual {v4}, LMZ;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, LNh;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    :catchall_0
    move-exception v0

    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->a()V

    .line 258
    iget-object v1, p0, LMO;->a:LMN;

    invoke-static {v1}, LMN;->a(LMN;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->b()V

    throw v0

    .line 250
    :cond_0
    :try_start_1
    const-string v1, "Location"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    .line 251
    if-eqz v0, :cond_1

    array-length v1, v0

    const/4 v3, 0x1

    if-eq v1, v3, :cond_2

    .line 252
    :cond_1
    new-instance v0, LNh;

    const-string v1, "Unable to upload item: Server upload URI invalid."

    invoke-direct {v0, v1, v2}, LNh;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 255
    :cond_2
    iget-object v1, p0, LMO;->a:LMZ;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LMO;->a(LMZ;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    iget-object v0, p0, LMO;->a:LMN;

    invoke-static {v0}, LMN;->a(LMN;)LTh;

    move-result-object v0

    invoke-interface {v0}, LTh;->a()V

    .line 258
    iget-object v0, p0, LMO;->a:LMN;

    invoke-static {v0}, LMN;->a(LMN;)LTh;

    move-result-object v0

    invoke-interface {v0}, LTh;->b()V

    .line 260
    return-void
.end method

.method protected b()LaJT;
    .locals 1

    .prologue
    .line 288
    invoke-virtual {p0}, LMO;->b()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 289
    invoke-virtual {p0, v0}, LMO;->a(Lorg/apache/http/client/methods/HttpUriRequest;)LaJT;

    move-result-object v0

    return-object v0
.end method

.method b()Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 6

    .prologue
    .line 408
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    new-instance v1, Ljava/net/URI;

    iget-object v2, p0, LMO;->a:LMZ;

    invoke-virtual {v2}, LMZ;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 413
    const-string v1, "GData-Version"

    const-string v2, "3.0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const-string v1, "Content-Range"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes */"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LMO;->a:LMZ;

    .line 415
    invoke-virtual {v3}, LMZ;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 414
    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-direct {v1, v2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 419
    return-object v0

    .line 409
    :catch_0
    move-exception v0

    .line 410
    new-instance v1, LNh;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LMO;->a:LMZ;

    invoke-virtual {v3}, LMZ;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected abstract b()V
.end method
