.class public final enum LaFb;
.super Ljava/lang/Enum;
.source "ManifestTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFb;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFb;

.field private static final synthetic a:[LaFb;

.field public static final enum b:LaFb;

.field public static final enum c:LaFb;

.field public static final enum d:LaFb;

.field public static final enum e:LaFb;

.field public static final enum f:LaFb;

.field public static final enum g:LaFb;

.field public static final enum h:LaFb;

.field public static final enum i:LaFb;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/16 v11, 0x4d

    const/16 v10, 0x4c

    const/4 v9, 0x0

    const/16 v8, 0x3f

    .line 46
    new-instance v0, LaFb;

    const-string v1, "APP_NAME"

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "appName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 47
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 48
    invoke-virtual {v2, v10}, LaFt;->b(I)LaFt;

    move-result-object v2

    .line 49
    invoke-virtual {v2, v11, v8}, LaFt;->a(II)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->a:LaFb;

    .line 54
    new-instance v0, LaFb;

    const-string v1, "JOBSET_NAME"

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "jobsetName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 55
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    new-array v4, v12, [Ljava/lang/String;

    sget-object v5, LaFb;->a:LaFb;

    .line 56
    invoke-virtual {v5}, LaFb;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    .line 55
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 57
    invoke-virtual {v2, v10}, LaFt;->b(I)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "jobsetName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 58
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v11, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->b:LaFb;

    .line 60
    new-instance v0, LaFb;

    const-string v1, "ETAG"

    const/4 v2, 0x2

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "eTag"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 61
    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 62
    invoke-virtual {v3, v10}, LaFt;->b(I)LaFt;

    move-result-object v3

    .line 63
    invoke-virtual {v3, v11, v8}, LaFt;->a(II)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->c:LaFb;

    .line 68
    new-instance v0, LaFb;

    const-string v1, "APP_CACHE_ID"

    const/4 v2, 0x3

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "appCacheId"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 70
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, LaFH;->c:LaFH;

    .line 69
    invoke-virtual {v4, v5, v6, v7}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v4

    .line 71
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    .line 69
    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 72
    invoke-virtual {v3, v10}, LaFt;->b(I)LaFt;

    move-result-object v3

    .line 73
    invoke-virtual {v3, v11, v8}, LaFt;->a(II)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->d:LaFb;

    .line 78
    new-instance v0, LaFb;

    const-string v1, "REFRESH_TIME"

    const/4 v2, 0x4

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "refreshTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 79
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 80
    invoke-virtual {v3, v10}, LaFt;->b(I)LaFt;

    move-result-object v3

    .line 81
    invoke-virtual {v3, v11, v8}, LaFt;->a(II)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->e:LaFb;

    .line 87
    new-instance v0, LaFb;

    const-string v1, "IS_CACHE_OBSOLETE"

    const/4 v2, 0x5

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "isCacheObsolete"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 88
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 89
    invoke-virtual {v3, v10}, LaFt;->b(I)LaFt;

    move-result-object v3

    .line 90
    invoke-virtual {v3, v11, v8}, LaFt;->a(II)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->f:LaFb;

    .line 96
    new-instance v0, LaFb;

    const-string v1, "ACCOUNT"

    const/4 v2, 0x6

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x4b

    new-instance v5, LaFG;

    const-string v6, "account"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 98
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(LaFy;)LaFG;

    move-result-object v5

    .line 97
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 99
    invoke-virtual {v3, v10}, LaFt;->b(I)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "account"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 101
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(LaFy;)LaFG;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    sget-object v6, LaFb;->a:LaFb;

    .line 102
    invoke-virtual {v6}, LaFb;->a()LaFr;

    move-result-object v6

    invoke-virtual {v6}, LaFr;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    sget-object v6, LaFb;->b:LaFb;

    invoke-virtual {v6}, LaFb;->a()LaFr;

    move-result-object v6

    invoke-virtual {v6}, LaFr;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v12

    invoke-virtual {v4, v5}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v4

    .line 100
    invoke-virtual {v3, v11, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x4f

    .line 103
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    const/16 v4, 0x4f

    new-instance v5, LaFG;

    const-string v6, "account"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 105
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(LaFy;)LaFG;

    move-result-object v5

    .line 104
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->g:LaFb;

    .line 110
    new-instance v0, LaFb;

    const-string v1, "IS_FAST_TRACK"

    const/4 v2, 0x7

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x4f

    new-instance v5, LaFG;

    const-string v6, "isFastTrack"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 111
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    sget-object v7, LaFb;->a:LaFb;

    .line 112
    invoke-virtual {v7}, LaFb;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    sget-object v7, LaFb;->b:LaFb;

    invoke-virtual {v7}, LaFb;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    const/4 v7, 0x2

    sget-object v8, LaFb;->g:LaFb;

    invoke-virtual {v8}, LaFb;->a()LaFr;

    move-result-object v8

    invoke-virtual {v8}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v6}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v5

    .line 111
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x6d

    .line 113
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    const/16 v4, 0x6d

    new-instance v5, LaFG;

    const-string v6, "isFastTrack"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 114
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->h:LaFb;

    .line 119
    new-instance v0, LaFb;

    const-string v1, "LOCALE"

    const/16 v2, 0x8

    invoke-static {}, LaFa;->b()LaFa;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x6d

    new-instance v5, LaFG;

    const-string v6, "locale"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 121
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    sget-object v6, LalT;->a:LaGs;

    .line 122
    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    sget-object v7, LaFb;->a:LaFb;

    .line 123
    invoke-virtual {v7}, LaFb;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    sget-object v7, LaFb;->b:LaFb;

    invoke-virtual {v7}, LaFb;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    const/4 v7, 0x2

    sget-object v8, LaFb;->g:LaFb;

    invoke-virtual {v8}, LaFb;->a()LaFr;

    move-result-object v8

    invoke-virtual {v8}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, LaFb;->h:LaFb;

    .line 124
    invoke-virtual {v8}, LaFb;->a()LaFr;

    move-result-object v8

    invoke-virtual {v8}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 123
    invoke-virtual {v5, v6}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v5

    .line 120
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFb;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFb;->i:LaFb;

    .line 41
    const/16 v0, 0x9

    new-array v0, v0, [LaFb;

    sget-object v1, LaFb;->a:LaFb;

    aput-object v1, v0, v9

    sget-object v1, LaFb;->b:LaFb;

    aput-object v1, v0, v12

    const/4 v1, 0x2

    sget-object v2, LaFb;->c:LaFb;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LaFb;->d:LaFb;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaFb;->e:LaFb;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaFb;->f:LaFb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaFb;->g:LaFb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaFb;->h:LaFb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaFb;->i:LaFb;

    aput-object v2, v0, v1

    sput-object v0, LaFb;->a:[LaFb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 130
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaFb;->a:LaFr;

    .line 131
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFb;
    .locals 1

    .prologue
    .line 41
    const-class v0, LaFb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFb;

    return-object v0
.end method

.method public static values()[LaFb;
    .locals 1

    .prologue
    .line 41
    sget-object v0, LaFb;->a:[LaFb;

    invoke-virtual {v0}, [LaFb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFb;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, LaFb;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, LaFb;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
