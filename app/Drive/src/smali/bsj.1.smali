.class Lbsj;
.super Ljava/lang/Object;
.source "ProvidesMethodProvider.java"

# interfaces
.implements LbuE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbuE",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:Lbse;

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private volatile a:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lbse;Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbse;",
            "Ljava/lang/Class",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbse;

    iput-object v0, p0, Lbsj;->a:Lbse;

    .line 23
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lbsj;->a:Ljava/lang/Class;

    .line 24
    iput p3, p0, Lbsj;->a:I

    .line 25
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lbsj;->a:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 31
    monitor-enter p0

    .line 32
    :try_start_0
    iget-object v0, p0, Lbsj;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lbsj;->a:Lbse;

    iget-object v1, p0, Lbsj;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lbse;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lbsj;->a:Ljava/lang/Object;

    .line 35
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :cond_1
    iget-object v0, p0, Lbsj;->a:Lbse;

    iget-object v1, p0, Lbsj;->a:Ljava/lang/Object;

    iget v2, p0, Lbsj;->a:I

    invoke-virtual {v0, v1, v2}, Lbse;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
