.class public LYi;
.super Ljava/lang/Object;
.source "PunchSvgWebView.java"

# interfaces
.implements LXB;


# instance fields
.field final synthetic a:I

.field final synthetic a:LXP;

.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;I)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    iput-object p2, p0, LYi;->a:LXP;

    iput p3, p0, LYi;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 317
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onSlideContentRequestFailed: slideIndex=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LYi;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 318
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    iget v1, p0, LYi;->a:I

    iget-object v2, p0, LYi;->a:LXP;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;ILXP;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 329
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LYk;

    invoke-direct {v1, p0}, LYk;-><init>(LYi;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 278
    iget-object v0, p0, LYi;->a:LXP;

    sget-object v1, LXP;->b:LXP;

    if-ne v0, v1, :cond_0

    .line 280
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    :cond_0
    if-nez p2, :cond_1

    .line 284
    const-string p2, " "

    .line 287
    :cond_1
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received content: slideIndex=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LYi;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 288
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 313
    :cond_2
    :goto_0
    return-void

    .line 292
    :cond_3
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    iget v1, p0, LYi;->a:I

    iget-object v2, p0, LYi;->a:LXP;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;ILXP;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 293
    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    .line 297
    :cond_4
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 298
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    iget v1, p0, LYi;->a:I

    iget-object v2, p0, LYi;->a:LXP;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;ILXP;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 302
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Slide content was not ready yet. Requesting again."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LYj;

    invoke-direct {v1, p0}, LYj;-><init>(LYi;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 312
    :cond_5
    iget-object v0, p0, LYi;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
