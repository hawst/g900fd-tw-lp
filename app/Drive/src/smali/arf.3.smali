.class Larf;
.super Ljava/lang/Object;
.source "ActionBarModeController.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private final a:I

.field private final a:Landroid/view/View;

.field private final a:Laro;

.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/view/View;Ljava/lang/String;ILaro;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Larf;->a:Landroid/view/View;

    .line 43
    iput-object p2, p0, Larf;->a:Ljava/lang/String;

    .line 44
    iput p3, p0, Larf;->a:I

    .line 45
    iput-object p4, p0, Larf;->a:Laro;

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Ljava/lang/String;ILaro;Lard;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Larf;-><init>(Landroid/view/View;Ljava/lang/String;ILaro;)V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Larf;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 52
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 53
    iget-object v1, p0, Larf;->a:Landroid/view/View;

    iget-object v2, p0, Larf;->a:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/View;->findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V

    .line 56
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 57
    instance-of v2, v0, Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    .line 58
    check-cast v0, Landroid/widget/ImageButton;

    .line 59
    iget v1, p0, Larf;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setColorFilter(I)V

    .line 60
    iget-object v1, p0, Larf;->a:Laro;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Laro;->a(Landroid/graphics/drawable/Drawable;)V

    .line 61
    const/4 v0, 0x0

    .line 65
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
