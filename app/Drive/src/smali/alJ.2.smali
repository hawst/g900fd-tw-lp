.class public LalJ;
.super Ljava/lang/Object;
.source "ImageBlurProcessor.java"


# instance fields
.field a:Lanc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 23
    iget-object v0, p0, LalJ;->a:Lanc;

    invoke-interface {v0, p1}, Lanc;->a(Landroid/graphics/Bitmap;)Lanb;

    move-result-object v8

    .line 37
    invoke-interface {v8, v3, v3}, Lanb;->a(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v3

    :cond_1
    move v2, v5

    move v6, v5

    .line 47
    :goto_1
    if-eqz v6, :cond_5

    move v1, v3

    move v6, v3

    .line 55
    :cond_2
    :goto_2
    neg-int v0, v2

    if-le v1, v0, :cond_4

    move v7, v3

    .line 56
    :goto_3
    const/4 v0, 0x4

    if-ge v7, v0, :cond_3

    .line 59
    packed-switch v7, :pswitch_data_0

    move v0, v3

    move v4, v3

    .line 65
    :goto_4
    invoke-interface {v8, v4, v0}, Lanb;->b(II)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 67
    invoke-interface {v8, v4, v0}, Lanb;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v5

    .line 56
    :goto_5
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v6, v0

    goto :goto_3

    .line 60
    :pswitch_0
    neg-int v0, v2

    move v4, v1

    goto :goto_4

    :pswitch_1
    move v0, v1

    move v4, v2

    .line 61
    goto :goto_4

    .line 62
    :pswitch_2
    neg-int v0, v1

    move v4, v0

    move v0, v2

    goto :goto_4

    .line 63
    :pswitch_3
    neg-int v4, v2

    neg-int v0, v1

    goto :goto_4

    .line 73
    :cond_3
    neg-int v1, v1

    .line 74
    if-ltz v1, :cond_2

    .line 75
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 47
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v3, v5

    .line 80
    goto :goto_0

    :cond_6
    move v0, v6

    goto :goto_5

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
