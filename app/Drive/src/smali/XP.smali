.class public final enum LXP;
.super Ljava/lang/Enum;
.source "PunchPresentationView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LXP;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LXP;

.field private static final synthetic a:[LXP;

.field public static final enum b:LXP;

.field public static final enum c:LXP;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 69
    new-instance v0, LXP;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v2}, LXP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LXP;->a:LXP;

    new-instance v0, LXP;

    const-string v1, "CURRENT"

    invoke-direct {v0, v1, v3}, LXP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LXP;->b:LXP;

    new-instance v0, LXP;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v4}, LXP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LXP;->c:LXP;

    .line 68
    const/4 v0, 0x3

    new-array v0, v0, [LXP;

    sget-object v1, LXP;->a:LXP;

    aput-object v1, v0, v2

    sget-object v1, LXP;->b:LXP;

    aput-object v1, v0, v3

    sget-object v1, LXP;->c:LXP;

    aput-object v1, v0, v4

    sput-object v0, LXP;->a:[LXP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LXP;
    .locals 1

    .prologue
    .line 68
    const-class v0, LXP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LXP;

    return-object v0
.end method

.method public static values()[LXP;
    .locals 1

    .prologue
    .line 68
    sget-object v0, LXP;->a:[LXP;

    invoke-virtual {v0}, [LXP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LXP;

    return-object v0
.end method
