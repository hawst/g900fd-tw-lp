.class LbdG;
.super Ljava/lang/Object;
.source "MediaUploadErrorHandler.java"

# interfaces
.implements Lbeg;
.implements Lber;


# static fields
.field static final a:Ljava/util/logging/Logger;


# instance fields
.field private final a:LbdD;

.field private final a:Lbeg;

.field private final a:Lber;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, LbdG;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LbdG;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(LbdD;Lbei;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbdD;

    iput-object v0, p0, LbdG;->a:LbdD;

    .line 53
    invoke-virtual {p2}, Lbei;->a()Lbeg;

    move-result-object v0

    iput-object v0, p0, LbdG;->a:Lbeg;

    .line 54
    invoke-virtual {p2}, Lbei;->a()Lber;

    move-result-object v0

    iput-object v0, p0, LbdG;->a:Lber;

    .line 56
    invoke-virtual {p2, p0}, Lbei;->a(Lbeg;)Lbei;

    .line 57
    invoke-virtual {p2, p0}, Lbei;->a(Lber;)Lbei;

    .line 58
    return-void
.end method


# virtual methods
.method public a(Lbei;Lbel;Z)Z
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, LbdG;->a:Lber;

    if-eqz v0, :cond_1

    iget-object v0, p0, LbdG;->a:Lber;

    .line 79
    invoke-interface {v0, p1, p2, p3}, Lber;->a(Lbei;Lbel;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 83
    :goto_0
    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Lbel;->a()I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    .line 85
    :try_start_0
    iget-object v0, p0, LbdG;->a:LbdD;

    invoke-virtual {v0}, LbdD;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :cond_0
    :goto_1
    return v1

    .line 79
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v0

    .line 87
    sget-object v2, LbdG;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "exception thrown while calling server callback"

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Lbei;Z)Z
    .locals 5

    .prologue
    .line 61
    iget-object v0, p0, LbdG;->a:Lbeg;

    if-eqz v0, :cond_1

    iget-object v0, p0, LbdG;->a:Lbeg;

    .line 62
    invoke-interface {v0, p1, p2}, Lbeg;->a(Lbei;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 66
    :goto_0
    if-eqz v1, :cond_0

    .line 68
    :try_start_0
    iget-object v0, p0, LbdG;->a:LbdD;

    invoke-virtual {v0}, LbdD;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    :goto_1
    return v1

    .line 62
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    sget-object v2, LbdG;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "exception thrown while calling server callback"

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
