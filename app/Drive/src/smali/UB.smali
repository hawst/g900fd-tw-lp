.class public final LUB;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUM;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUU;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUJ;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUD;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUQ;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 43
    iput-object p1, p0, LUB;->a:LbrA;

    .line 44
    const-class v0, LUM;

    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUB;->a:Lbsk;

    .line 47
    const-class v0, LUU;

    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUB;->b:Lbsk;

    .line 50
    const-class v0, LUJ;

    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUB;->c:Lbsk;

    .line 53
    const-class v0, LUD;

    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUB;->d:Lbsk;

    .line 56
    const-class v0, LUQ;

    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUB;->e:Lbsk;

    .line 59
    const-class v0, LUT;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUB;->f:Lbsk;

    .line 62
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 179
    sparse-switch p1, :sswitch_data_0

    .line 257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :sswitch_0
    new-instance v1, LUM;

    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 184
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LUB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 182
    invoke-static {v0, v2}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LUM;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 255
    :goto_0
    return-object v0

    .line 191
    :sswitch_1
    new-instance v1, LUU;

    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 194
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LUB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 192
    invoke-static {v0, v2}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    invoke-direct {v1, v0}, LUU;-><init>(LQr;)V

    move-object v0, v1

    .line 199
    goto :goto_0

    .line 201
    :sswitch_2
    new-instance v4, LUJ;

    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 204
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 202
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 210
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LUB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 208
    invoke-static {v1, v2}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    iget-object v2, p0, LUB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Ld;

    iget-object v2, v2, Ld;->a:Lbsk;

    .line 216
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LUB;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Ld;

    iget-object v3, v3, Ld;->a:Lbsk;

    .line 214
    invoke-static {v2, v3}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageManager;

    iget-object v3, p0, LUB;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->h:Lbsk;

    .line 222
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LUB;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->h:Lbsk;

    .line 220
    invoke-static {v3, v5}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LUI;

    invoke-direct {v4, v0, v1, v2, v3}, LUJ;-><init>(Landroid/content/Context;LtK;Landroid/content/pm/PackageManager;LUI;)V

    move-object v0, v4

    .line 227
    goto :goto_0

    .line 229
    :sswitch_3
    new-instance v4, LUD;

    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 232
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 230
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laek;

    iget-object v1, v1, Laek;->h:Lbsk;

    .line 238
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LUB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->h:Lbsk;

    .line 236
    invoke-static {v1, v2}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laer;

    iget-object v2, p0, LUB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->J:Lbsk;

    .line 244
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LUB;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LagN;

    iget-object v3, v3, LagN;->J:Lbsk;

    .line 242
    invoke-static {v2, v3}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lagl;

    iget-object v3, p0, LUB;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LSK;

    iget-object v3, v3, LSK;->b:Lbsk;

    .line 250
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LUB;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LSK;

    iget-object v5, v5, LSK;->b:Lbsk;

    .line 248
    invoke-static {v3, v5}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LSF;

    invoke-direct {v4, v0, v1, v2, v3}, LUD;-><init>(LaGM;Laer;Lagl;LSF;)V

    move-object v0, v4

    .line 255
    goto/16 :goto_0

    .line 179
    nop

    :sswitch_data_0
    .sparse-switch
        0x70 -> :sswitch_2
        0x84 -> :sswitch_0
        0x86 -> :sswitch_1
        0x87 -> :sswitch_3
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 278
    sparse-switch p2, :sswitch_data_0

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :sswitch_0
    check-cast p1, LUK;

    .line 282
    invoke-virtual {p1}, LUK;->provideUrlOpenerActivityIntentFactoryMap()LUQ;

    move-result-object v0

    .line 287
    :goto_0
    return-object v0

    .line 285
    :sswitch_1
    check-cast p1, LUK;

    .line 287
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->b:Lbsk;

    .line 290
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUU;

    .line 287
    invoke-virtual {p1, v0}, LUK;->provideUrlParser(LUU;)LUT;

    move-result-object v0

    goto :goto_0

    .line 278
    :sswitch_data_0
    .sparse-switch
        0x85 -> :sswitch_0
        0x8a -> :sswitch_1
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 150
    const-class v0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x17

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LUB;->a(LbuP;LbuB;)V

    .line 153
    const-class v0, LUM;

    iget-object v1, p0, LUB;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LUB;->a(Ljava/lang/Class;Lbsk;)V

    .line 154
    const-class v0, LUU;

    iget-object v1, p0, LUB;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LUB;->a(Ljava/lang/Class;Lbsk;)V

    .line 155
    const-class v0, LUJ;

    iget-object v1, p0, LUB;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LUB;->a(Ljava/lang/Class;Lbsk;)V

    .line 156
    const-class v0, LUD;

    iget-object v1, p0, LUB;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LUB;->a(Ljava/lang/Class;Lbsk;)V

    .line 157
    const-class v0, LUQ;

    iget-object v1, p0, LUB;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LUB;->a(Ljava/lang/Class;Lbsk;)V

    .line 158
    const-class v0, LUT;

    iget-object v1, p0, LUB;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LUB;->a(Ljava/lang/Class;Lbsk;)V

    .line 159
    iget-object v0, p0, LUB;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x84

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 161
    iget-object v0, p0, LUB;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x86

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 163
    iget-object v0, p0, LUB;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x70

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 165
    iget-object v0, p0, LUB;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x87

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 167
    iget-object v0, p0, LUB;->e:Lbsk;

    const-class v1, LUK;

    const/16 v2, 0x85

    invoke-virtual {p0, v1, v2}, LUB;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 169
    iget-object v0, p0, LUB;->f:Lbsk;

    const-class v1, LUK;

    const/16 v2, 0x8a

    invoke-virtual {p0, v1, v2}, LUB;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 171
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 264
    packed-switch p1, :pswitch_data_0

    .line 272
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    .line 268
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    .line 269
    invoke-virtual {v0, p2}, LUB;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V

    .line 274
    return-void

    .line 264
    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 69
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 70
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->e:Lbsk;

    .line 73
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->e:Lbsk;

    .line 71
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUQ;

    .line 77
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSw;

    iget-object v0, v0, LSw;->a:Lbsk;

    .line 80
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSs;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LSs;

    .line 82
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 85
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 83
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaKR;

    .line 89
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->d:Lbsk;

    .line 92
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->d:Lbsk;

    .line 90
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUD;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUD;

    .line 96
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 99
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 97
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/content/Context;

    .line 103
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->a:Lbsk;

    .line 106
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->a:Lbsk;

    .line 104
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUM;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUM;

    .line 110
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 113
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 111
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LSF;

    .line 117
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->f:Lbsk;

    .line 120
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->f:Lbsk;

    .line 118
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTF;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LTF;

    .line 124
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 127
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 125
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Lald;

    .line 131
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->f:Lbsk;

    .line 134
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->f:Lbsk;

    .line 132
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUT;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUT;

    .line 138
    iget-object v0, p0, LUB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->e:Lbsk;

    .line 141
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->e:Lbsk;

    .line 139
    invoke-static {v0, v1}, LUB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lala;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Lala;

    .line 145
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method
