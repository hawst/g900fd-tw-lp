.class LGO;
.super Ljava/lang/Object;
.source "VideoDocumentOpener.java"

# interfaces
.implements LFR;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LGO;->a:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 34
    iget-object v1, p0, LGO;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/content/Intent;

    move-result-object v1

    .line 36
    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cannot be downloaded because it is not known to the server"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    .line 41
    :cond_0
    new-instance v0, LGy;

    iget-object v2, p0, LGO;->a:Landroid/content/Context;

    invoke-interface {p2}, LaGo;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, p1, v3, v1}, LGy;-><init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;)V

    .line 42
    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    return-object v0
.end method
