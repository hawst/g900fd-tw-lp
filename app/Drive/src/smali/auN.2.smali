.class public final LauN;
.super Ljava/lang/Object;
.source "ListFileInfoSource.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/viewer/client/ListFileInfoSource;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/ListFileInfoSource;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/viewer/client/ListFileInfoSource;

    invoke-direct {v0, p1}, Lcom/google/android/apps/viewer/client/ListFileInfoSource;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/viewer/client/ListFileInfoSource;
    .locals 1

    .prologue
    .line 62
    new-array v0, p1, [Lcom/google/android/apps/viewer/client/ListFileInfoSource;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, LauN;->a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/ListFileInfoSource;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, LauN;->a(I)[Lcom/google/android/apps/viewer/client/ListFileInfoSource;

    move-result-object v0

    return-object v0
.end method
