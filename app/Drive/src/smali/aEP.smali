.class public LaEP;
.super Ljava/lang/Object;
.source "EntryChangeNotifier.java"


# instance fields
.field private final a:LaEM;

.field private final a:LaGR;


# direct methods
.method public constructor <init>(LaGR;LaEM;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, LaEP;->a:LaGR;

    .line 33
    iput-object p2, p0, LaEP;->a:LaEM;

    .line 34
    return-void
.end method

.method public constructor <init>(LaGR;LaEz;)V
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p2}, LaEz;->a()LaEM;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LaEP;-><init>(LaGR;LaEM;)V

    .line 27
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)V
    .locals 2

    .prologue
    .line 48
    invoke-static {}, LamV;->a()V

    .line 49
    iget-object v0, p0, LaEP;->a:LaEM;

    invoke-virtual {v0, p1, p2}, LaEM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)V

    .line 50
    iget-object v0, p0, LaEP;->a:LaGR;

    new-instance v1, LaEQ;

    invoke-direct {v1, p0, p1, p2, p1}, LaEQ;-><init>(LaEP;Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v0, v1}, LaGR;->a(LaGN;)V

    .line 62
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)Z
    .locals 1

    .prologue
    .line 70
    invoke-static {}, LamV;->a()V

    .line 71
    iget-object v0, p0, LaEP;->a:LaEM;

    invoke-virtual {v0, p1, p2}, LaEM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)Z

    move-result v0

    return v0
.end method
