.class public LarO;
.super Landroid/os/AsyncTask;
.source "AlphaMaskedImageView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

.field final synthetic b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    iput-object p1, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    iput p2, p0, LarO;->a:I

    iput p3, p0, LarO;->b:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 62
    iget v0, p0, LarO;->a:I

    if-nez v0, :cond_0

    iget v0, p0, LarO;->b:I

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, LarO;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    .line 67
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 68
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 69
    iget-object v1, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, LarO;->b:I

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v3}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 72
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 75
    iget-object v2, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, LarO;->a:I

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 78
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 79
    const/16 v4, 0x14

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    .line 84
    new-instance v5, Landroid/graphics/ColorMatrixColorFilter;

    new-instance v6, Landroid/graphics/ColorMatrix;

    invoke-direct {v6, v4}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-direct {v5, v6}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 86
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 87
    invoke-virtual {v0, v2, v7, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 88
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 79
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 97
    iget-object v0, p0, LarO;->a:Lcom/google/android/apps/docs/warmwelcome/AlphaMaskedImageView;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lxm;->a(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LarO;->a([Ljava/lang/Void;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 58
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, LarO;->a(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
