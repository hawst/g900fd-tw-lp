.class public final Lwo;
.super Ljava/lang/Object;
.source "NavigationPathElement.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;
    .locals 2

    .prologue
    .line 50
    const-class v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 51
    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 52
    const-class v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a(Landroid/os/Parcelable;)V

    .line 53
    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;
    .locals 1

    .prologue
    .line 58
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lwo;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lwo;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    return-object v0
.end method
