.class public abstract Lavq;
.super Landroid/os/AsyncTask;
.source "Fetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/apps/viewer/client/AuthenticatedUri;",
        "Ljava/lang/Void;",
        "LauW;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lavl;


# direct methods
.method public constructor <init>(Lavl;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lavq;->b:Lavl;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 383
    aget-object v0, p1, v5

    .line 384
    iget-object v1, p0, Lavq;->b:Lavl;

    invoke-virtual {v1, v0}, Lavl;->a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;

    move-result-object v1

    .line 385
    iget-object v2, v1, Lavk;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lavq;->a(Ljava/lang/String;)Lavg;

    move-result-object v2

    .line 386
    if-nez v2, :cond_0

    .line 387
    const-string v2, "Fetcher"

    const-string v3, "What file type is that? [%s : %s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v1, v1, Lavk;->a:Ljava/lang/String;

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    const/4 v0, 0x0

    .line 390
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lavk;->a()[B

    move-result-object v1

    invoke-static {v2, v0, v1}, LauW;->a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;[B)LauW;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)Lavg;
    .locals 1

    .prologue
    .line 394
    invoke-static {p1}, Lavg;->a(Ljava/lang/String;)Lavg;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 380
    check-cast p1, [Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-virtual {p0, p1}, Lavq;->a([Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;

    move-result-object v0

    return-object v0
.end method
