.class public abstract LbgG;
.super LbgI;
.source "PdfIndirectObject.java"


# static fields
.field private static final a:[B


# instance fields
.field private final a:I

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "endobj\n"

    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LbgG;->a:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, LbgI;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, LbgG;->a:I

    return-void
.end method


# virtual methods
.method protected b(Ljava/io/OutputStream;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 44
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%d %d obj\n"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LbgG;->b:I

    .line 45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 46
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 47
    array-length v0, v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 39
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%d %d R"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LbgG;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b(I)V
    .locals 0

    .prologue
    .line 30
    iput p1, p0, LbgG;->b:I

    .line 31
    return-void
.end method

.method protected c(Ljava/io/OutputStream;)I
    .locals 1

    .prologue
    .line 52
    sget-object v0, LbgG;->a:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 53
    sget-object v0, LbgG;->a:[B

    array-length v0, v0

    return v0
.end method
