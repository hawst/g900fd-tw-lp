.class public final enum LYQ;
.super Ljava/lang/Enum;
.source "PunchUiModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LYQ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LYQ;

.field private static final synthetic a:[LYQ;

.field public static final enum b:LYQ;

.field public static final enum c:LYQ;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, LYQ;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LYQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYQ;->a:LYQ;

    new-instance v0, LYQ;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v3}, LYQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYQ;->b:LYQ;

    new-instance v0, LYQ;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v4}, LYQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYQ;->c:LYQ;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [LYQ;

    sget-object v1, LYQ;->a:LYQ;

    aput-object v1, v0, v2

    sget-object v1, LYQ;->b:LYQ;

    aput-object v1, v0, v3

    sget-object v1, LYQ;->c:LYQ;

    aput-object v1, v0, v4

    sput-object v0, LYQ;->a:[LYQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LYQ;
    .locals 1

    .prologue
    .line 20
    const-class v0, LYQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LYQ;

    return-object v0
.end method

.method public static values()[LYQ;
    .locals 1

    .prologue
    .line 20
    sget-object v0, LYQ;->a:[LYQ;

    invoke-virtual {v0}, [LYQ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LYQ;

    return-object v0
.end method
