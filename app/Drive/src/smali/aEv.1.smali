.class public final enum LaEv;
.super Ljava/lang/Enum;
.source "CollectionTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEv;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEv;

.field private static final synthetic a:[LaEv;

.field public static final enum b:LaEv;

.field public static final enum c:LaEv;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0xe

    const/4 v6, 0x0

    .line 123
    new-instance v0, LaEv;

    const-string v1, "DAYS_TO_SYNC"

    invoke-static {}, LaEu;->b()LaEu;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "daysToSync"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 124
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LaEv;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEv;->a:LaEv;

    .line 126
    new-instance v0, LaEv;

    const-string v1, "SYNC_NEW_DOCS_BY_DEFAULT"

    invoke-static {}, LaEu;->b()LaEu;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "syncNewDocsByDefault"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 127
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEv;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEv;->b:LaEv;

    .line 129
    new-instance v0, LaEv;

    const-string v1, "ENTRY_ID"

    invoke-static {}, LaEu;->b()LaEu;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "entryId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 130
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    .line 131
    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 130
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEv;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEv;->c:LaEv;

    .line 122
    const/4 v0, 0x3

    new-array v0, v0, [LaEv;

    sget-object v1, LaEv;->a:LaEv;

    aput-object v1, v0, v6

    sget-object v1, LaEv;->b:LaEv;

    aput-object v1, v0, v8

    sget-object v1, LaEv;->c:LaEv;

    aput-object v1, v0, v9

    sput-object v0, LaEv;->a:[LaEv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 137
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEv;->a:LaFr;

    .line 138
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEv;
    .locals 1

    .prologue
    .line 122
    const-class v0, LaEv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEv;

    return-object v0
.end method

.method public static values()[LaEv;
    .locals 1

    .prologue
    .line 122
    sget-object v0, LaEv;->a:[LaEv;

    invoke-virtual {v0}, [LaEv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEv;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, LaEv;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, LaEv;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
