.class public abstract enum Lbqj;
.super Ljava/lang/Enum;
.source "SortedLists.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbqj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbqj;

.field private static final synthetic a:[Lbqj;

.field public static final enum b:Lbqj;

.field public static final enum c:Lbqj;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 150
    new-instance v0, Lbqk;

    const-string v1, "NEXT_LOWER"

    invoke-direct {v0, v1, v2}, Lbqk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqj;->a:Lbqj;

    .line 161
    new-instance v0, Lbql;

    const-string v1, "NEXT_HIGHER"

    invoke-direct {v0, v1, v3}, Lbql;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqj;->b:Lbqj;

    .line 180
    new-instance v0, Lbqm;

    const-string v1, "INVERTED_INSERTION_INDEX"

    invoke-direct {v0, v1, v4}, Lbqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqj;->c:Lbqj;

    .line 145
    const/4 v0, 0x3

    new-array v0, v0, [Lbqj;

    sget-object v1, Lbqj;->a:Lbqj;

    aput-object v1, v0, v2

    sget-object v1, Lbqj;->b:Lbqj;

    aput-object v1, v0, v3

    sget-object v1, Lbqj;->c:Lbqj;

    aput-object v1, v0, v4

    sput-object v0, Lbqj;->a:[Lbqj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbqi;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lbqj;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbqj;
    .locals 1

    .prologue
    .line 145
    const-class v0, Lbqj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbqj;

    return-object v0
.end method

.method public static values()[Lbqj;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lbqj;->a:[Lbqj;

    invoke-virtual {v0}, [Lbqj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbqj;

    return-object v0
.end method


# virtual methods
.method abstract a(I)I
.end method
