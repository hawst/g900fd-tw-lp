.class public LbhL;
.super Ljava/lang/Object;
.source "CameraProvider.java"


# static fields
.field private static a:LbhO;

.field private static a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private static final a:Ljava/lang/Object;

.field private static final a:Ljava/lang/String;

.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/os/Handler;

.field private final a:Ljava/util/concurrent/Executor;

.field private final b:LbhO;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 34
    const-class v0, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    .line 35
    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LbhL;->a:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LbhL;->a:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LbhL;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LbhO;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, LbhL;->a:Ljava/util/concurrent/Executor;

    .line 56
    iput-object p2, p0, LbhL;->b:LbhO;

    .line 57
    return-void
.end method

.method static synthetic a(LbhL;)LbhO;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LbhL;->b:LbhO;

    return-object v0
.end method

.method static synthetic a(LbhL;Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, LbhL;->a(Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 131
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "connectCameraBlocking"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, LbhL;->a:LbhO;

    iget-object v1, p0, LbhL;->b:LbhO;

    if-eq v0, v1, :cond_1

    .line 135
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Already acquired a camera for somebody else!"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    sget-object v0, LbhL;->a:LbhO;

    sget-object v1, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-direct {p0, v0, v1}, LbhL;->a(LbhO;Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V

    .line 144
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, LbhL;->b(Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    move-result-object v0

    sput-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    .line 145
    iget-object v0, p0, LbhL;->b:LbhO;

    sput-object v0, LbhL;->a:LbhO;

    .line 147
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Acquired camera for the first time!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    return-object v0

    .line 138
    :cond_1
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "This object already owns a connected camera!"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    sget-object v0, LbhL;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(LbhL;LbhO;Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, LbhL;->a(LbhO;Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V

    return-void
.end method

.method private a(LbhO;Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 155
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "releaseCameraBlocking"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    if-nez p2, :cond_0

    .line 158
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Asked to release null camera!"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Null camera!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    const-string v0, "%h"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 164
    sget-object v1, LbhL;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 165
    sget-object v1, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Already released this camera!"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    :cond_1
    sget-object v1, LbhL;->a:Ljava/util/Map;

    invoke-interface {v1, v0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    const-string v1, "%h"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 170
    sget-object v2, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Releasing Camera %s owned by CameraManager %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    invoke-interface {p2}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->release()V

    .line 174
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-ne v0, p2, :cond_2

    .line 175
    sput-object v7, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    .line 176
    sput-object v7, LbhL;->a:LbhO;

    .line 180
    :goto_0
    return-void

    .line 178
    :cond_2
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Asked to release non-current camera!"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LbhL;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 194
    :goto_0
    sget-object v1, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Connecting to %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-virtual {v1, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    if-eqz v0, :cond_1

    .line 197
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    .line 198
    array-length v4, v3

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 199
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "open"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 201
    const/4 v5, 0x0

    const/4 v6, 0x3

    :try_start_1
    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, LbhL;->a:Landroid/os/Handler;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    aput-object p3, v6, v7

    invoke-virtual {v0, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    return-object v0

    .line 190
    :catch_0
    move-exception v3

    .line 191
    sget-object v4, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v5, "Error getting camera proxy class for: %s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v1, v6, v2

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 202
    :catch_1
    move-exception v0

    .line 203
    sget-object v5, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "Error opening camera proxy!"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 204
    :catch_2
    move-exception v0

    .line 205
    sget-object v5, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "Error opening camera proxy!"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 206
    :catch_3
    move-exception v0

    .line 207
    sget-object v5, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "Error opening camera proxy!"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 213
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error opening camera proxy!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V
    .locals 5

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "releaseCamera: %h"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    iget-object v0, p0, LbhL;->a:Ljava/util/concurrent/Executor;

    new-instance v1, LbhN;

    invoke-direct {v1, p0, p1}, LbhN;-><init>(LbhL;Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "acquireCamera"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 73
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "CameraProvider.acquireCamera() not called from main thread!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 76
    :cond_0
    :try_start_1
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_2

    .line 77
    sget-object v1, LbhL;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :try_start_2
    sget-object v0, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_1

    iget-object v0, p0, LbhL;->b:LbhO;

    sget-object v2, LbhL;->a:LbhO;

    if-ne v0, v2, :cond_1

    .line 82
    iget-object v0, p0, LbhL;->b:LbhO;

    sget-object v2, LbhL;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0, v2}, LbhO;->onCameraAcquired(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V

    .line 83
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 111
    :goto_0
    monitor-exit p0

    return-void

    .line 85
    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 90
    :cond_2
    :try_start_4
    iget-object v0, p0, LbhL;->a:Landroid/os/Handler;

    if-nez v0, :cond_3

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LbhL;->a:Landroid/os/Handler;

    .line 94
    :cond_3
    new-instance v0, LbhM;

    invoke-direct {v0, p0, p1, p2, p3}, LbhM;-><init>(LbhL;Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)V

    iget-object v1, p0, LbhL;->a:Ljava/util/concurrent/Executor;

    iget-object v2, p0, LbhL;->a:Landroid/os/Handler;

    .line 110
    invoke-virtual {v0, v1, v2}, LbhM;->execute(Ljava/util/concurrent/Executor;Landroid/os/Handler;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 85
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method
