.class LaFD;
.super Ljava/lang/Object;
.source "DatabaseTable.java"


# instance fields
.field private a:J

.field private a:LaFx;


# direct methods
.method public constructor <init>(JLaFx;)V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-wide p1, p0, LaFD;->a:J

    .line 121
    iput-object p3, p0, LaFD;->a:LaFx;

    .line 122
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 126
    instance-of v1, p1, LaFD;

    if-eqz v1, :cond_0

    .line 127
    check-cast p1, LaFD;

    .line 128
    iget-wide v2, p0, LaFD;->a:J

    iget-wide v4, p1, LaFD;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, LaFD;->a:LaFx;

    iget-object v2, p1, LaFD;->a:LaFx;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 130
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 135
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, LaFD;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaFD;->a:LaFx;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
