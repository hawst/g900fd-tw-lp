.class public LaIw;
.super Ljava/lang/Object;
.source "StorageFileWriter.java"


# instance fields
.field private final a:Ladi;


# direct methods
.method public constructor <init>(Ladi;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LaIw;->a:Ladi;

    .line 37
    return-void
.end method

.method private a(LaGo;Landroid/os/ParcelFileDescriptor;Ljava/lang/Long;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 44
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v0

    invoke-virtual {v0}, LaGv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Ljava/io/IOException;

    const-string v1, "cannot write to google document files"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v3

    .line 49
    iget-object v0, p0, LaIw;->a:Ladi;

    invoke-interface {v0, p1}, Ladi;->a(LaGo;)Ladj;

    move-result-object v4

    .line 50
    const-string v0, "StorageFileWriter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Created new content of document: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 54
    invoke-virtual {v3, v0}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 55
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-interface {v4}, Ladj;->a()Ljava/io/File;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 56
    invoke-virtual {v3, v5}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 57
    invoke-static {v0, v5}, Lbrh;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    move-result-wide v6

    .line 58
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v8

    cmp-long v0, v6, v8

    if-nez v0, :cond_3

    :cond_1
    move v0, v2

    .line 64
    :goto_0
    if-nez v0, :cond_4

    :goto_1
    :try_start_1
    invoke-static {v3, v2}, Lbrl;->a(Ljava/io/Closeable;Z)V

    .line 65
    if-eqz v0, :cond_2

    .line 68
    invoke-interface {v4}, Ladj;->a()V

    .line 69
    const-string v0, "StorageFileWriter"

    const-string v1, "File content is copied to the local cache"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    :cond_2
    invoke-interface {v4}, Ladj;->close()V

    .line 76
    return-void

    :cond_3
    move v0, v1

    .line 58
    goto :goto_0

    :cond_4
    move v2, v1

    .line 64
    goto :goto_1

    .line 73
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Ladj;->close()V

    throw v0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    :try_start_2
    const-string v1, "StorageFileWriter"

    const-string v5, "cannot write new file content"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v5, v6}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 61
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63
    :catchall_1
    move-exception v0

    .line 64
    :try_start_3
    invoke-static {v3, v2}, Lbrl;->a(Ljava/io/Closeable;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 73
    invoke-interface {v4}, Ladj;->close()V

    throw v0

    :catchall_2
    move-exception v0

    invoke-interface {v4}, Ladj;->close()V

    throw v0
.end method

.method static synthetic a(LaIw;LaGo;Landroid/os/ParcelFileDescriptor;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, LaIw;->a(LaGo;Landroid/os/ParcelFileDescriptor;Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public a(LaGo;Ljava/lang/Long;)Landroid/os/ParcelFileDescriptor;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 82
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 84
    new-instance v1, LaIx;

    invoke-direct {v1, p0, p1, v0, p2}, LaIx;-><init>(LaIw;LaGo;[Landroid/os/ParcelFileDescriptor;Ljava/lang/Long;)V

    .line 102
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 103
    const/4 v1, 0x1

    aget-object v0, v0, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "failure making pipe"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
