.class public Lbhy;
.super Ljava/lang/Object;
.source "Geometry.java"


# direct methods
.method public static a(F)F
    .locals 3

    .prologue
    const/high16 v2, 0x43b40000    # 360.0f

    .line 98
    rem-float v0, p0, v2

    .line 99
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    add-float/2addr v0, v2

    :cond_0
    return v0
.end method

.method public static a(FFFF)F
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 123
    sub-float v0, p2, p0

    .line 124
    sub-float v1, p3, p1

    .line 125
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    .line 126
    cmpl-float v3, v2, v6

    if-lez v3, :cond_1

    .line 127
    div-float/2addr v0, v2

    .line 128
    div-float/2addr v1, v2

    .line 129
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    mul-double/2addr v2, v4

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 130
    cmpg-float v1, v1, v6

    if-gez v1, :cond_0

    .line 131
    const/high16 v1, 0x43b40000    # 360.0f

    sub-float v0, v1, v0

    .line 133
    :cond_0
    return v0

    .line 135
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Supplied a vector with length 0!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(F)I
    .locals 2

    .prologue
    .line 88
    invoke-static {p0}, Lbhy;->a(F)F

    move-result v0

    const/high16 v1, 0x42340000    # 45.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x42b40000    # 90.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x5a

    invoke-static {v0}, Lbhy;->a(I)I

    move-result v0

    return v0
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 109
    rem-int/lit16 v0, p0, 0x168

    .line 110
    if-gez v0, :cond_0

    add-int/lit16 v0, v0, 0x168

    :cond_0
    return v0
.end method

.method public static a(FFII)Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 24
    mul-float v1, v3, p1

    const/high16 v2, 0x42b40000    # 90.0f

    sub-float/2addr v1, v2

    const/high16 v2, 0x43340000    # 180.0f

    div-float/2addr v1, v2

    const v2, 0x40490e56    # 3.1415f

    mul-float/2addr v1, v2

    invoke-static {v1}, Landroid/util/FloatMath;->sin(F)F

    move-result v1

    add-float/2addr v1, v5

    div-float v2, v1, v3

    .line 27
    int-to-float v1, p2

    int-to-float v3, p3

    div-float/2addr v1, v3

    .line 39
    cmpl-float v3, v1, v5

    if-lez v3, :cond_2

    cmpl-float v3, p0, v5

    if-lez v3, :cond_0

    cmpl-float v3, v1, p0

    if-gtz v3, :cond_1

    :cond_0
    cmpg-float v3, p0, v5

    if-gtz v3, :cond_2

    div-float v3, v5, p0

    cmpl-float v3, v1, v3

    if-lez v3, :cond_2

    .line 45
    :cond_1
    int-to-float v0, p3

    sub-float v1, v5, p0

    mul-float/2addr v1, v2

    add-float/2addr v1, p0

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 46
    int-to-float v0, p3

    div-float v3, v5, p0

    sub-float/2addr v3, v5

    mul-float/2addr v2, v3

    add-float/2addr v2, v5

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 74
    :goto_0
    sub-int v2, p2, v1

    div-int/lit8 v2, v2, 0x2

    .line 75
    sub-int v3, p3, v0

    div-int/lit8 v3, v3, 0x2

    .line 76
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v1, v2

    add-int/2addr v0, v3

    invoke-direct {v4, v2, v3, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4

    .line 47
    :cond_2
    cmpg-float v3, v1, v5

    if-gtz v3, :cond_5

    cmpg-float v3, p0, v5

    if-gtz v3, :cond_3

    cmpg-float v3, v1, p0

    if-lez v3, :cond_4

    :cond_3
    cmpl-float v3, p0, v5

    if-lez v3, :cond_5

    div-float v3, v5, v1

    cmpl-float v3, v3, p0

    if-lez v3, :cond_5

    .line 53
    :cond_4
    int-to-float v0, p2

    sub-float v1, p0, v5

    mul-float/2addr v1, v2

    add-float/2addr v1, v5

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 54
    int-to-float v0, p2

    div-float v3, v5, p0

    div-float v4, v5, p0

    sub-float v4, v5, v4

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    mul-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0

    .line 55
    :cond_5
    cmpl-float v3, p0, v5

    if-lez v3, :cond_8

    cmpl-float v3, v1, v5

    if-lez v3, :cond_6

    cmpg-float v3, v1, p0

    if-lez v3, :cond_7

    :cond_6
    cmpg-float v3, v1, v5

    if-gtz v3, :cond_8

    div-float v3, v5, v1

    cmpg-float v3, v3, p0

    if-gtz v3, :cond_8

    .line 61
    :cond_7
    sub-float v0, v5, v2

    int-to-float v1, p2

    mul-float/2addr v0, v1

    int-to-float v1, p3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 62
    int-to-float v0, v1

    div-float/2addr v0, p0

    float-to-int v0, v0

    goto :goto_0

    .line 63
    :cond_8
    cmpg-float v3, p0, v5

    if-gtz v3, :cond_b

    cmpl-float v3, v1, v5

    if-lez v3, :cond_9

    div-float v3, v5, p0

    cmpg-float v3, v1, v3

    if-lez v3, :cond_a

    :cond_9
    cmpg-float v3, v1, v5

    if-gtz v3, :cond_b

    cmpl-float v1, v1, p0

    if-lez v1, :cond_b

    .line 69
    :cond_a
    sub-float v0, v5, v2

    int-to-float v1, p3

    mul-float/2addr v0, v1

    int-to-float v1, p2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 70
    int-to-float v1, v0

    mul-float/2addr v1, p0

    float-to-int v1, v1

    goto :goto_0

    :cond_b
    move v1, v0

    goto :goto_0
.end method
