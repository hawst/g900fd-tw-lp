.class public final LJW;
.super Ljava/lang/Object;
.source "SelectionModel.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State",
        "<*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    new-instance v0, Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 78
    new-array v0, p1, [Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1}, LJW;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1}, LJW;->a(I)[Lcom/google/android/apps/docs/doclist/selection/SelectionModel$State;

    move-result-object v0

    return-object v0
.end method
