.class public LDi;
.super Ljava/lang/Object;
.source "SelectionViewState.java"


# instance fields
.field private final a:I

.field private final a:LDl;

.field private a:LDr;

.field protected final a:Landroid/view/View;

.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/doclist/SelectionItem;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LDl;

.field public final b:Landroid/view/View;

.field private c:LDl;


# direct methods
.method constructor <init>(Landroid/view/View;II)V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228
    sget-object v0, LDr;->a:LDr;

    iput-object v0, p0, LDi;->a:LDr;

    .line 233
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LDi;->a:Ljava/util/Map;

    .line 237
    new-instance v0, LDl;

    sget v2, Lxc;->select_button:I

    sget v3, Lxc;->unselect_button:I

    sget v6, Lxc;->select_button_background:I

    sget v7, Lxc;->unselect_button_background:I

    const/4 v8, -0x1

    sget v9, Lxc;->type_icon:I

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v9}, LDl;-><init>(Landroid/view/View;IIIIIIII)V

    iput-object v0, p0, LDi;->a:LDl;

    .line 246
    new-instance v0, LDl;

    sget v2, Lxc;->select_folder_button:I

    sget v3, Lxc;->unselect_folder_button:I

    sget v4, Lxc;->select_folder_button:I

    sget v5, Lxc;->unselect_folder_button:I

    sget v6, Lxc;->select_folder_button_background:I

    sget v7, Lxc;->unselect_folder_button_background:I

    sget v8, Lxc;->select_folder_padding:I

    sget v9, Lxc;->folder_type_icon:I

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, LDl;-><init>(Landroid/view/View;IIIIIIII)V

    iput-object v0, p0, LDi;->b:LDl;

    .line 255
    iget-object v0, p0, LDi;->a:LDl;

    iput-object v0, p0, LDi;->c:LDl;

    .line 257
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lxc;->more_actions_button:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lxc;->resume_button:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lxc;->pause_button:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lxc;->cancel_button:I

    aput v2, v0, v1

    invoke-static {p1, v0}, LDi;->a(Landroid/view/View;[I)LbmY;

    move-result-object v0

    iput-object v0, p0, LDi;->a:LbmY;

    .line 259
    sget v0, Lxc;->show_preview_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LDi;->b:Landroid/view/View;

    .line 260
    sget v0, Lxc;->doc_entry_root:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LDi;->a:Landroid/view/View;

    .line 262
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 263
    sget v1, Lxa;->selection_folder_title_text_offset:I

    .line 264
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LDi;->a:I

    .line 265
    return-void
.end method

.method private static varargs a(Landroid/view/View;[I)LbmY;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "[I)",
            "LbmY",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 269
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, p1, v0

    .line 270
    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 271
    if-eqz v3, :cond_0

    .line 272
    invoke-virtual {v1, v3}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 269
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275
    :cond_1
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()LDl;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, LDi;->c:LDl;

    return-object v0
.end method

.method public a()LDr;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, LDi;->a:LDr;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/doclist/SelectionItem;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, LDi;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 283
    iget-object v0, p0, LDi;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 284
    iget-object v0, p0, LDi;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 285
    iget-object v2, p0, LDi;->a:Ljava/util/Map;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 287
    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, LDi;->c:LDl;

    iget v1, p0, LDi;->a:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, LDl;->a(I)V

    .line 324
    return-void
.end method

.method public a(LCu;Lcom/google/android/apps/docs/doclist/SelectionItem;I)V
    .locals 2

    .prologue
    .line 357
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/SelectionItem;

    iput-object v0, p0, LDi;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 358
    invoke-virtual {p0}, LDi;->d()V

    .line 359
    iget-object v0, p0, LDi;->b:LDl;

    invoke-static {v0}, LDl;->a(LDl;)V

    .line 360
    iget-object v0, p0, LDi;->a:LDl;

    invoke-static {v0}, LDl;->a(LDl;)V

    .line 361
    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, LDi;->b:LDl;

    iput-object v0, p0, LDi;->c:LDl;

    .line 363
    iget-object v0, p0, LDi;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 368
    :goto_0
    new-instance v0, LDj;

    invoke-direct {v0, p0, p2, p1, p3}, LDj;-><init>(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;LCu;I)V

    .line 375
    iget-object v1, p0, LDi;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 376
    return-void

    .line 365
    :cond_0
    iget-object v0, p0, LDi;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 366
    iget-object v0, p0, LDi;->a:LDl;

    iput-object v0, p0, LDi;->c:LDl;

    goto :goto_0
.end method

.method public a(LDr;)V
    .locals 1

    .prologue
    .line 350
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDr;

    iput-object v0, p0, LDi;->a:LDr;

    .line 351
    return-void
.end method

.method public a(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 330
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, LDi;->c:LDl;

    iget-object v0, v0, LDl;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    iget-object v0, p0, LDi;->c:LDl;

    iget-object v0, v0, LDl;->d:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    return-void
.end method

.method public a(Landroid/view/View$OnLongClickListener;)V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, LDi;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 312
    iget-object v0, p0, LDi;->c:LDl;

    iget-object v0, v0, LDl;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 313
    iget-object v0, p0, LDi;->c:LDl;

    iget-object v0, v0, LDl;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 314
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 384
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 290
    iget-object v0, p0, LDi;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 291
    iget-object v0, p0, LDi;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 292
    iget-object v1, p0, LDi;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 293
    if-eqz v1, :cond_0

    .line 294
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 297
    :cond_1
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 300
    iget-object v0, p0, LDi;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    iget-object v0, p0, LDi;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 303
    :cond_0
    iget-object v0, p0, LDi;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 304
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 305
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 308
    :cond_2
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, LDi;->c:LDl;

    invoke-static {v0}, LDl;->a(LDl;)V

    .line 380
    iget-object v0, p0, LDi;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 381
    return-void
.end method
