.class public LSL;
.super LSX;
.source "ActiveSearch.java"


# instance fields
.field private final a:Landroid/view/View$OnFocusChangeListener;

.field private a:Landroid/view/View;

.field private a:Ljava/lang/Runnable;

.field private a:Ljava/lang/String;

.field private final a:Lwm;

.field private a:Z


# direct methods
.method constructor <init>(Lara;Laja;Lwm;Landroid/app/Activity;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lara;",
            "Laja",
            "<",
            "Larg;",
            ">;",
            "Lwm;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 79
    sget-object v4, Larj;->b:Larj;

    sget v5, LwZ;->m_icon_search_bar:I

    sget v6, Lxb;->search_bar_background:I

    const/4 v0, 0x6

    new-array v7, v0, [Ljava/lang/Integer;

    sget v0, Lxc;->menu_clear_search:I

    .line 85
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v0, 0x1

    sget v1, Lxc;->menu_list_mode:I

    .line 86
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v0, 0x2

    sget v1, Lxc;->menu_grid_mode:I

    .line 87
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v0, 0x3

    sget v1, Lxc;->menu_selection_all:I

    .line 88
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v0, 0x4

    sget v1, Lxc;->menu_selection_clear:I

    .line 89
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v0, 0x5

    sget v1, Lxc;->menu_selection_start:I

    .line 90
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    .line 79
    invoke-direct/range {v0 .. v7}, LSX;-><init>(Lara;Lbxw;Landroid/app/Activity;Larj;II[Ljava/lang/Integer;)V

    .line 61
    iput-boolean v8, p0, LSL;->a:Z

    .line 63
    new-instance v0, LSM;

    invoke-direct {v0, p0}, LSM;-><init>(LSL;)V

    iput-object v0, p0, LSL;->a:Landroid/view/View$OnFocusChangeListener;

    .line 91
    iput-object p3, p0, LSL;->a:Lwm;

    .line 92
    return-void
.end method

.method static synthetic a(LSL;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LSL;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private a()Landroid/widget/AutoCompleteTextView;
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, LSL;->a:Landroid/view/View;

    sget v1, Lxc;->search_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic a(LSL;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LSL;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, LSL;->d()V

    return-void
.end method

.method static synthetic a(LSL;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, LSL;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, LSL;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    const-wide/16 v0, 0x5dc

    invoke-direct {p0, p1, v0, v1}, LSL;->a(Ljava/lang/String;J)V

    .line 261
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 264
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    .line 266
    iget-object v1, p0, LSL;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, LSL;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 270
    :cond_0
    new-instance v1, LSU;

    invoke-direct {v1, p0, p1}, LSU;-><init>(LSL;Ljava/lang/String;)V

    iput-object v1, p0, LSL;->a:Ljava/lang/Runnable;

    .line 281
    iget-object v1, p0, LSL;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 282
    iput-object p1, p0, LSL;->a:Ljava/lang/String;

    .line 283
    return-void
.end method

.method static synthetic a(LSL;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, LSL;->a:Z

    return v0
.end method

.method static synthetic a(LSL;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, LSL;->a:Z

    return p1
.end method

.method static synthetic b(LSL;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LSL;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 254
    sget-object v1, Larj;->b:Larj;

    iget-object v0, p0, LSL;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    invoke-interface {v0}, Larg;->a()Larj;

    move-result-object v0

    invoke-virtual {v1, v0}, Larj;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(LSL;)Z
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, LSL;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic c(LSL;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LSL;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(LSL;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LSL;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, LSL;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 221
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    .line 222
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setDropDownAnchor(I)V

    .line 223
    new-instance v0, Laaw;

    invoke-virtual {p0}, LSL;->a()Landroid/app/Activity;

    move-result-object v2

    new-instance v3, LSS;

    invoke-direct {v3, p0, v1}, LSS;-><init>(LSL;Landroid/widget/AutoCompleteTextView;)V

    invoke-direct {v0, v2, v3}, Laaw;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 245
    return-void
.end method

.method static synthetic e(LSL;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LSL;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0}, LSX;->a()LbmY;

    move-result-object v0

    .line 104
    invoke-virtual {p0}, LSL;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 105
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    .line 114
    :goto_0
    return-object v0

    .line 107
    :cond_0
    new-instance v1, LSN;

    invoke-direct {v1, p0}, LSN;-><init>(LSL;)V

    invoke-static {v0, v1}, LbpU;->a(Ljava/util/Set;LbiU;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, LSL;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 206
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, LSL;->a()Landroid/app/ActionBar;

    move-result-object v0

    .line 97
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 98
    sget v1, Lxe;->drive_search_bar:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 99
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 291
    invoke-super {p0, p1}, LSX;->a(Landroid/os/Bundle;)V

    .line 292
    const-string v0, "com.google.android.apps.docs.honeycomb.actionbarmode.ActiveSearch.SEARCH_TERM"

    invoke-virtual {p0}, LSL;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method public a(Landroid/view/MenuItem;)V
    .locals 2

    .prologue
    .line 119
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lxc;->menu_clear_search:I

    if-eq v0, v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, LSL;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0}, LSL;->b()V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 130
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LSL;->a:Landroid/view/View;

    .line 132
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    .line 133
    iget-object v1, p0, LSL;->a:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 135
    new-instance v1, LSO;

    invoke-direct {v1, p0, v0}, LSO;-><init>(LSL;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 153
    iget-object v1, p0, LSL;->a:Lwm;

    .line 154
    invoke-interface {v1}, Lwm;->a()LbmF;

    move-result-object v1

    invoke-static {v1}, Lwr;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 153
    invoke-static {v1}, Lbju;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 157
    new-instance v2, LSP;

    invoke-direct {v2, p0}, LSP;-><init>(LSL;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    new-instance v2, LSQ;

    invoke-direct {v2, p0}, LSQ;-><init>(LSL;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 188
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v2, LSR;

    invoke-direct {v2, p0}, LSR;-><init>(LSL;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 201
    :cond_0
    const-wide/16 v2, 0x0

    invoke-direct {p0, v1, v2, v3}, LSL;->a(Ljava/lang/String;J)V

    .line 202
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 214
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 216
    invoke-direct {p0}, LSL;->d()V

    .line 217
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 297
    invoke-super {p0, p1}, LSX;->b(Landroid/os/Bundle;)V

    .line 298
    const-string v0, "com.google.android.apps.docs.honeycomb.actionbarmode.ActiveSearch.SEARCH_TERM"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 299
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    .line 300
    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 249
    invoke-direct {p0}, LSL;->a()Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    .line 250
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 251
    return-void
.end method
