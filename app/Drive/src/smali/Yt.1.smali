.class LYt;
.super Landroid/support/v4/view/ViewPager;
.source "PunchSwipeableSvgViewerBase.java"


# instance fields
.field final synthetic a:LYs;


# direct methods
.method constructor <init>(LYs;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, LYt;->a:LYs;

    invoke-direct {p0, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private c()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 208
    iget-object v1, p0, LYt;->a:LYs;

    iget-boolean v1, v1, LYs;->a:Z

    if-nez v1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 212
    :cond_1
    iget-object v1, p0, LYt;->a:LYs;

    iget-object v1, v1, LYs;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    if-eqz v1, :cond_2

    .line 213
    iget-object v1, p0, LYt;->a:LYs;

    iget-object v1, v1, LYs;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->canZoomOut()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 216
    :cond_2
    iget-object v0, p0, LYt;->a:LYs;

    iget-boolean v0, v0, LYs;->a:Z

    goto :goto_0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, LYt;->a:LYs;

    const/4 v1, 0x1

    iput-boolean v1, v0, LYs;->b:Z

    .line 260
    invoke-direct {p0}, LYt;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 196
    invoke-super/range {p0 .. p5}, Landroid/support/v4/view/ViewPager;->onLayout(ZIIII)V

    .line 198
    iget-object v0, p0, LYt;->a:LYs;

    iget-object v0, v0, LYs;->a:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, LYt;->a:LYs;

    iget-object v0, v0, LYs;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->invalidate()V

    .line 203
    iget-object v0, p0, LYt;->a:LYs;

    invoke-virtual {v0, p1}, LYs;->a(Z)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 221
    iget-object v1, p0, LYt;->a:LYs;

    const/4 v2, 0x1

    iput-boolean v2, v1, LYs;->b:Z

    .line 222
    invoke-direct {p0}, LYt;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    :goto_0
    return v0

    .line 250
    :cond_0
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 251
    :catch_0
    move-exception v1

    .line 252
    iget-object v1, p0, LYt;->a:LYs;

    iget-object v1, v1, LYs;->a:Ljava/lang/String;

    const-string v2, "WARNING: Ignoring exception in ViewPager.onTouchEvent() native code."

    invoke-static {v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
