.class public final enum LavJ;
.super Ljava/lang/Enum;
.source "PdfStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LavJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LavJ;

.field private static final synthetic a:[LavJ;

.field public static final enum b:LavJ;

.field public static final enum c:LavJ;

.field public static final enum d:LavJ;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, LavJ;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LavJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LavJ;->a:LavJ;

    .line 6
    new-instance v0, LavJ;

    const-string v1, "REQUIRES_PASSWORD"

    invoke-direct {v0, v1, v3}, LavJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LavJ;->b:LavJ;

    .line 7
    new-instance v0, LavJ;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, LavJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LavJ;->c:LavJ;

    .line 8
    new-instance v0, LavJ;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, LavJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LavJ;->d:LavJ;

    .line 4
    const/4 v0, 0x4

    new-array v0, v0, [LavJ;

    sget-object v1, LavJ;->a:LavJ;

    aput-object v1, v0, v2

    sget-object v1, LavJ;->b:LavJ;

    aput-object v1, v0, v3

    sget-object v1, LavJ;->c:LavJ;

    aput-object v1, v0, v4

    sget-object v1, LavJ;->d:LavJ;

    aput-object v1, v0, v5

    sput-object v0, LavJ;->a:[LavJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LavJ;
    .locals 1

    .prologue
    .line 4
    const-class v0, LavJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LavJ;

    return-object v0
.end method

.method public static values()[LavJ;
    .locals 1

    .prologue
    .line 4
    sget-object v0, LavJ;->a:[LavJ;

    invoke-virtual {v0}, [LavJ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LavJ;

    return-object v0
.end method
