.class public final LJu;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJn;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJc;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJF;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJp;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJq;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJi;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJr;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJj;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJC;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJg;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJy;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJs;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJf;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lbjv",
            "<",
            "LJw;",
            ">;>;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LJs;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LJs;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lbjv",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 53
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 54
    iput-object p1, p0, LJu;->a:LbrA;

    .line 55
    const-class v0, LJn;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->a:Lbsk;

    .line 58
    const-class v0, LJc;

    invoke-static {v0, v3}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->b:Lbsk;

    .line 61
    const-class v0, LJF;

    invoke-static {v0, v3}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->c:Lbsk;

    .line 64
    const-class v0, LJp;

    invoke-static {v0, v3}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->d:Lbsk;

    .line 67
    const-class v0, LJq;

    invoke-static {v0, v3}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->e:Lbsk;

    .line 70
    const-class v0, LJi;

    invoke-static {v0, v3}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->f:Lbsk;

    .line 73
    const-class v0, LJr;

    invoke-static {v0, v3}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->g:Lbsk;

    .line 76
    const-class v0, LJj;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->h:Lbsk;

    .line 79
    const-class v0, LJC;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->i:Lbsk;

    .line 82
    const-class v0, LJg;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->j:Lbsk;

    .line 85
    const-class v0, LJy;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->k:Lbsk;

    .line 88
    const-class v0, LJs;

    new-instance v1, Lbwl;

    const-string v2, "appFlowHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 89
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 88
    invoke-static {v0, v3}, LJu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->l:Lbsk;

    .line 91
    const-class v0, LJf;

    invoke-static {v0, v3}, LJu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->m:Lbsk;

    .line 94
    const-class v0, Lbjv;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LJw;

    aput-object v2, v1, v4

    .line 95
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "appHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    const-class v1, Lbxz;

    .line 94
    invoke-static {v0, v1}, LJu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->n:Lbsk;

    .line 97
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LJs;

    aput-object v2, v1, v4

    .line 98
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "appFlowHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 97
    invoke-static {v0, v3}, LJu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->o:Lbsk;

    .line 100
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LJs;

    aput-object v2, v1, v4

    .line 101
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "appFlowHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 100
    invoke-static {v0, v3}, LJu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->p:Lbsk;

    .line 103
    const-class v0, Lbjv;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Integer;

    aput-object v2, v1, v4

    .line 104
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "docListActivityVisitCountSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 103
    invoke-static {v0, v3}, LJu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJu;->q:Lbsk;

    .line 106
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 193
    sparse-switch p1, :sswitch_data_0

    .line 375
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :sswitch_0
    new-instance v0, LJn;

    invoke-direct {v0}, LJn;-><init>()V

    .line 197
    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJu;

    .line 198
    invoke-virtual {v1, v0}, LJu;->a(LJn;)V

    .line 373
    :goto_0
    return-object v0

    .line 201
    :sswitch_1
    new-instance v4, LJc;

    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 204
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 202
    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->j:Lbsk;

    .line 210
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->j:Lbsk;

    .line 208
    invoke-static {v1, v2}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaFO;

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LqD;

    iget-object v2, v2, LqD;->c:Lbsk;

    .line 216
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LqD;

    iget-object v3, v3, LqD;->c:Lbsk;

    .line 214
    invoke-static {v2, v3}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LqK;

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->ab:Lbsk;

    .line 222
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LJu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->ab:Lbsk;

    .line 220
    invoke-static {v3, v5}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laja;

    invoke-direct {v4, v0, v1, v2, v3}, LJc;-><init>(Landroid/content/Context;LaFO;LqK;Laja;)V

    move-object v0, v4

    .line 227
    goto :goto_0

    .line 229
    :sswitch_2
    new-instance v2, LJF;

    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->b:Lbsk;

    .line 232
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJu;

    iget-object v1, v1, LJu;->b:Lbsk;

    .line 230
    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJc;

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->d:Lbsk;

    .line 238
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LtQ;

    iget-object v3, v3, LtQ;->d:Lbsk;

    .line 236
    invoke-static {v1, v3}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lub;

    invoke-direct {v2, v0, v1}, LJF;-><init>(LJc;Lub;)V

    move-object v0, v2

    .line 243
    goto/16 :goto_0

    .line 245
    :sswitch_3
    new-instance v0, LJp;

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 248
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 246
    invoke-static {v1, v2}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LJu;

    iget-object v2, v2, LJu;->c:Lbsk;

    .line 254
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LJu;

    iget-object v3, v3, LJu;->c:Lbsk;

    .line 252
    invoke-static {v2, v3}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LJF;

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LJu;

    iget-object v3, v3, LJu;->g:Lbsk;

    .line 260
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LJu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LJu;

    iget-object v4, v4, LJu;->g:Lbsk;

    .line 258
    invoke-static {v3, v4}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LJr;

    iget-object v4, p0, LJu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 266
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LJu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 264
    invoke-static {v4, v5}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LtK;

    iget-object v5, p0, LJu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LJu;

    iget-object v5, v5, LJu;->e:Lbsk;

    .line 272
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LJu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LJu;

    iget-object v6, v6, LJu;->e:Lbsk;

    .line 270
    invoke-static {v5, v6}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LJq;

    invoke-direct/range {v0 .. v5}, LJp;-><init>(Laja;LJF;LJr;LtK;LJq;)V

    goto/16 :goto_0

    .line 279
    :sswitch_4
    new-instance v0, LJq;

    invoke-direct {v0}, LJq;-><init>()V

    .line 281
    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJu;

    .line 282
    invoke-virtual {v1, v0}, LJu;->a(LJq;)V

    goto/16 :goto_0

    .line 285
    :sswitch_5
    new-instance v0, LJi;

    invoke-direct {v0}, LJi;-><init>()V

    goto/16 :goto_0

    .line 289
    :sswitch_6
    new-instance v1, LJr;

    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->b:Lbsk;

    .line 292
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LJu;

    iget-object v2, v2, LJu;->b:Lbsk;

    .line 290
    invoke-static {v0, v2}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJc;

    invoke-direct {v1, v0}, LJr;-><init>(LJc;)V

    move-object v0, v1

    .line 297
    goto/16 :goto_0

    .line 299
    :sswitch_7
    new-instance v2, LJj;

    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->m:Lbsk;

    .line 302
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJu;

    iget-object v1, v1, LJu;->m:Lbsk;

    .line 300
    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJf;

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->ay:Lbsk;

    .line 308
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LbiH;

    iget-object v3, v3, LbiH;->ay:Lbsk;

    .line 306
    invoke-static {v1, v3}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LbiP;

    invoke-direct {v2, v0, v1}, LJj;-><init>(LJf;LbiP;)V

    move-object v0, v2

    .line 313
    goto/16 :goto_0

    .line 315
    :sswitch_8
    new-instance v0, LJC;

    invoke-direct {v0}, LJC;-><init>()V

    goto/16 :goto_0

    .line 319
    :sswitch_9
    new-instance v0, LJg;

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 322
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 320
    invoke-static {v1, v2}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 328
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 326
    invoke-static {v2, v3}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    iget-object v3, p0, LJu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->S:Lbsk;

    .line 334
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LJu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->S:Lbsk;

    .line 332
    invoke-static {v3, v4}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaKM;

    iget-object v4, p0, LJu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LJu;

    iget-object v4, v4, LJu;->q:Lbsk;

    .line 340
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LJu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LJu;

    iget-object v5, v5, LJu;->q:Lbsk;

    .line 338
    invoke-static {v4, v5}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbjv;

    iget-object v5, p0, LJu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LbiH;

    iget-object v5, v5, LbiH;->U:Lbsk;

    .line 346
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LJu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LbiH;

    iget-object v6, v6, LbiH;->U:Lbsk;

    .line 344
    invoke-static {v5, v6}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LbiP;

    invoke-direct/range {v0 .. v5}, LJg;-><init>(Laja;LtK;LaKM;Lbjv;LbiP;)V

    goto/16 :goto_0

    .line 353
    :sswitch_a
    new-instance v3, LJy;

    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 356
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 354
    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJu;

    iget-object v1, v1, LJu;->i:Lbsk;

    .line 362
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LJu;

    iget-object v2, v2, LJu;->i:Lbsk;

    .line 360
    invoke-static {v1, v2}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LJC;

    iget-object v2, p0, LJu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LJu;

    iget-object v2, v2, LJu;->h:Lbsk;

    .line 368
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LJu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LJu;

    iget-object v4, v4, LJu;->h:Lbsk;

    .line 366
    invoke-static {v2, v4}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LJj;

    invoke-direct {v3, v0, v1, v2}, LJy;-><init>(Landroid/content/Context;LJC;LJj;)V

    move-object v0, v3

    .line 373
    goto/16 :goto_0

    .line 193
    :sswitch_data_0
    .sparse-switch
        0xd3 -> :sswitch_a
        0x13a -> :sswitch_0
        0x13b -> :sswitch_1
        0x13e -> :sswitch_2
        0x141 -> :sswitch_9
        0x142 -> :sswitch_3
        0x143 -> :sswitch_6
        0x144 -> :sswitch_4
        0x146 -> :sswitch_5
        0x149 -> :sswitch_7
        0x14b -> :sswitch_8
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 402
    sparse-switch p2, :sswitch_data_0

    .line 466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 404
    :sswitch_0
    check-cast p1, LJo;

    .line 406
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->a:Lbsk;

    .line 409
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJn;

    .line 406
    invoke-virtual {p1, v0}, LJo;->provideFlowHelpCardSupplier(LJn;)LJs;

    move-result-object v0

    .line 459
    :goto_0
    return-object v0

    .line 413
    :sswitch_1
    check-cast p1, LJD;

    .line 415
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->j:Lbsk;

    .line 418
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJg;

    .line 415
    invoke-virtual {p1, v0}, LJD;->provideCentralHelpCardsSupplier(LJg;)LJf;

    move-result-object v0

    goto :goto_0

    .line 422
    :sswitch_2
    check-cast p1, LJo;

    .line 424
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->d:Lbsk;

    .line 427
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJp;

    .line 424
    invoke-virtual {p1, v0}, LJo;->provideSupplierHelpCard(LJp;)Lbjv;

    move-result-object v0

    goto :goto_0

    .line 431
    :sswitch_3
    check-cast p1, LJm;

    .line 433
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->p:Lbsk;

    .line 436
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 433
    invoke-virtual {p1, v0}, LJm;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 440
    :sswitch_4
    check-cast p1, LJm;

    .line 442
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v2, v0, LJu;->l:Lbsk;

    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 449
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 453
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 442
    invoke-virtual {p1, v2, v0, v1}, LJm;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 457
    :sswitch_5
    check-cast p1, LJD;

    .line 459
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    iget-object v0, v0, LJu;->f:Lbsk;

    .line 462
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJi;

    .line 459
    invoke-virtual {p1, v0}, LJD;->provideDocListVisitCountSupplier(LJi;)Lbjv;

    move-result-object v0

    goto :goto_0

    .line 402
    :sswitch_data_0
    .sparse-switch
        0x13d -> :sswitch_0
        0x140 -> :sswitch_1
        0x145 -> :sswitch_2
        0x147 -> :sswitch_3
        0x148 -> :sswitch_4
        0x14c -> :sswitch_5
    .end sparse-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 128
    const-class v0, LJq;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x26

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LJu;->a(LbuP;LbuB;)V

    .line 131
    const-class v0, LJn;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x27

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LJu;->a(LbuP;LbuB;)V

    .line 134
    const-class v0, LJn;

    iget-object v1, p0, LJu;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 135
    const-class v0, LJc;

    iget-object v1, p0, LJu;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 136
    const-class v0, LJF;

    iget-object v1, p0, LJu;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 137
    const-class v0, LJp;

    iget-object v1, p0, LJu;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 138
    const-class v0, LJq;

    iget-object v1, p0, LJu;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 139
    const-class v0, LJi;

    iget-object v1, p0, LJu;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 140
    const-class v0, LJr;

    iget-object v1, p0, LJu;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 141
    const-class v0, LJj;

    iget-object v1, p0, LJu;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 142
    const-class v0, LJC;

    iget-object v1, p0, LJu;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 143
    const-class v0, LJg;

    iget-object v1, p0, LJu;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 144
    const-class v0, LJy;

    iget-object v1, p0, LJu;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 145
    const-class v0, LJs;

    new-instance v1, Lbwl;

    const-string v2, "appFlowHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LJu;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Lbuv;Lbsk;)V

    .line 146
    const-class v0, LJf;

    iget-object v1, p0, LJu;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Ljava/lang/Class;Lbsk;)V

    .line 147
    const-class v0, Lbjv;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LJw;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "appHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LJu;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Lbuv;Lbsk;)V

    .line 148
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LJs;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "appFlowHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LJu;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Lbuv;Lbsk;)V

    .line 149
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LJs;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "appFlowHelpCardSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LJu;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Lbuv;Lbsk;)V

    .line 150
    const-class v0, Lbjv;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Integer;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "docListActivityVisitCountSupplier"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LJu;->q:Lbsk;

    invoke-virtual {p0, v0, v1}, LJu;->a(Lbuv;Lbsk;)V

    .line 151
    iget-object v0, p0, LJu;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x13a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 153
    iget-object v0, p0, LJu;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x13b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 155
    iget-object v0, p0, LJu;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x13e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 157
    iget-object v0, p0, LJu;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x142

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 159
    iget-object v0, p0, LJu;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x144

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 161
    iget-object v0, p0, LJu;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x146

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 163
    iget-object v0, p0, LJu;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x143

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 165
    iget-object v0, p0, LJu;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x149

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 167
    iget-object v0, p0, LJu;->i:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x14b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 169
    iget-object v0, p0, LJu;->j:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x141

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 171
    iget-object v0, p0, LJu;->k:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xd3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 173
    iget-object v0, p0, LJu;->l:Lbsk;

    const-class v1, LJo;

    const/16 v2, 0x13d

    invoke-virtual {p0, v1, v2}, LJu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 175
    iget-object v0, p0, LJu;->m:Lbsk;

    const-class v1, LJD;

    const/16 v2, 0x140

    invoke-virtual {p0, v1, v2}, LJu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 177
    iget-object v0, p0, LJu;->n:Lbsk;

    const-class v1, LJo;

    const/16 v2, 0x145

    invoke-virtual {p0, v1, v2}, LJu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 179
    iget-object v0, p0, LJu;->o:Lbsk;

    const-class v1, LJm;

    const/16 v2, 0x147

    invoke-virtual {p0, v1, v2}, LJu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 181
    iget-object v0, p0, LJu;->p:Lbsk;

    const-class v1, LJm;

    const/16 v2, 0x148

    invoke-virtual {p0, v1, v2}, LJu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 183
    iget-object v0, p0, LJu;->q:Lbsk;

    const-class v1, LJD;

    const/16 v2, 0x14c

    invoke-virtual {p0, v1, v2}, LJu;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 185
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 382
    packed-switch p1, :pswitch_data_0

    .line 396
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :pswitch_0
    check-cast p2, LJq;

    .line 386
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    .line 387
    invoke-virtual {v0, p2}, LJu;->a(LJq;)V

    .line 398
    :goto_0
    return-void

    .line 390
    :pswitch_1
    check-cast p2, LJn;

    .line 392
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJu;

    .line 393
    invoke-virtual {v0, p2}, LJu;->a(LJn;)V

    goto :goto_0

    .line 382
    :pswitch_data_0
    .packed-switch 0x26
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(LJn;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, LJu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->Y:Lbsk;

    .line 119
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->Y:Lbsk;

    .line 117
    invoke-static {v0, v1}, LJu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iput-object v0, p1, LJn;->a:LbiP;

    .line 123
    return-void
.end method

.method public a(LJq;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method
