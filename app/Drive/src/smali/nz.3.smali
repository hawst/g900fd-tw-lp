.class public final Lnz;
.super Ljava/lang/Object;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:Lnz;

.field public static final b:Lnz;

.field public static final c:Lnz;

.field public static final d:Lnz;

.field public static final e:Lnz;

.field public static final f:Lnz;


# instance fields
.field private final a:Layw;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lnz;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const-string v3, "mb"

    invoke-direct {v0, v1, v2, v3}, Lnz;-><init>(IILjava/lang/String;)V

    sput-object v0, Lnz;->a:Lnz;

    new-instance v0, Lnz;

    const/16 v1, 0x140

    const/16 v2, 0x32

    const-string v3, "mb"

    invoke-direct {v0, v1, v2, v3}, Lnz;-><init>(IILjava/lang/String;)V

    sput-object v0, Lnz;->b:Lnz;

    new-instance v0, Lnz;

    const/16 v1, 0x12c

    const/16 v2, 0xfa

    const-string v3, "as"

    invoke-direct {v0, v1, v2, v3}, Lnz;-><init>(IILjava/lang/String;)V

    sput-object v0, Lnz;->c:Lnz;

    new-instance v0, Lnz;

    const/16 v1, 0x1d4

    const/16 v2, 0x3c

    const-string v3, "as"

    invoke-direct {v0, v1, v2, v3}, Lnz;-><init>(IILjava/lang/String;)V

    sput-object v0, Lnz;->d:Lnz;

    new-instance v0, Lnz;

    const/16 v1, 0x2d8

    const/16 v2, 0x5a

    const-string v3, "as"

    invoke-direct {v0, v1, v2, v3}, Lnz;-><init>(IILjava/lang/String;)V

    sput-object v0, Lnz;->e:Lnz;

    new-instance v0, Lnz;

    const/16 v1, 0xa0

    const/16 v2, 0x258

    const-string v3, "as"

    invoke-direct {v0, v1, v2, v3}, Lnz;-><init>(IILjava/lang/String;)V

    sput-object v0, Lnz;->f:Lnz;

    return-void
.end method

.method private constructor <init>(IILjava/lang/String;)V
    .locals 1

    new-instance v0, Layw;

    invoke-direct {v0, p1, p2}, Layw;-><init>(II)V

    invoke-direct {p0, v0}, Lnz;-><init>(Layw;)V

    return-void
.end method

.method public constructor <init>(Layw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnz;->a:Layw;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lnz;->a:Layw;

    invoke-virtual {v0}, Layw;->b()I

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lnz;->a:Layw;

    invoke-virtual {v0}, Layw;->a()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lnz;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lnz;

    iget-object v0, p0, Lnz;->a:Layw;

    iget-object v1, p1, Lnz;->a:Layw;

    invoke-virtual {v0, v1}, Layw;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lnz;->a:Layw;

    invoke-virtual {v0}, Layw;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lnz;->a:Layw;

    invoke-virtual {v0}, Layw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
