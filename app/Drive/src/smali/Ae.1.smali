.class LAe;
.super LpD;
.source "CooperateStateMachineProgressDialog.java"


# instance fields
.field final synthetic a:LAd;

.field final synthetic a:LDL;


# direct methods
.method constructor <init>(LAd;Ljava/lang/String;LDL;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, LAe;->a:LAd;

    iput-object p3, p0, LAe;->a:LDL;

    invoke-direct {p0, p2}, LpD;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 84
    .line 85
    const-string v0, "CooperateStateMachineProgressDialog"

    const-string v2, "Starting machine %s"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, LAe;->a:LDL;

    invoke-interface {v4}, LDL;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 86
    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p0}, LAe;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 87
    const-string v2, "CooperateStateMachineProgressDialog"

    const-string v3, "State: %s"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 88
    iget-object v2, p0, LAe;->a:LDL;

    invoke-interface {v2, v0}, LDL;->a(I)I

    move-result v0

    goto :goto_0

    .line 90
    :cond_0
    const-string v0, "CooperateStateMachineProgressDialog"

    const-string v2, "Finishing machine %s"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, LAe;->a:LDL;

    invoke-interface {v4}, LDL;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 91
    iget-object v0, p0, LAe;->a:LAd;

    invoke-static {v0}, LAd;->a(LAd;)V

    .line 92
    return-void
.end method
