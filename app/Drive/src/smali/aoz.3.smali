.class final enum Laoz;
.super Ljava/lang/Enum;
.source "FutureDependentValueGuard.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laoz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laoz;

.field private static final synthetic a:[Laoz;

.field public static final enum b:Laoz;

.field public static final enum c:Laoz;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Laoz;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v2}, Laoz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laoz;->a:Laoz;

    .line 63
    new-instance v0, Laoz;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v3}, Laoz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laoz;->b:Laoz;

    .line 64
    new-instance v0, Laoz;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, Laoz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laoz;->c:Laoz;

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [Laoz;

    sget-object v1, Laoz;->a:Laoz;

    aput-object v1, v0, v2

    sget-object v1, Laoz;->b:Laoz;

    aput-object v1, v0, v3

    sget-object v1, Laoz;->c:Laoz;

    aput-object v1, v0, v4

    sput-object v0, Laoz;->a:[Laoz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laoz;
    .locals 1

    .prologue
    .line 61
    const-class v0, Laoz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laoz;

    return-object v0
.end method

.method public static values()[Laoz;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Laoz;->a:[Laoz;

    invoke-virtual {v0}, [Laoz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laoz;

    return-object v0
.end method
