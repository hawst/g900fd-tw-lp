.class public LBi;
.super Ljava/lang/Object;
.source "DocListEntrySyncStateImpl.java"

# interfaces
.implements LBf;


# instance fields
.field private a:LBg;

.field private a:LBh;

.field private final a:LQr;

.field private a:LaGo;

.field private final a:LaKR;

.field private a:LaKS;

.field private final a:Ladi;

.field private a:Lagg;

.field private a:LahR;

.field private a:LahS;

.field private final a:Lahf;

.field private final a:Lahy;

.field private final a:Lamn;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Lamn;LaKR;LQr;Ladi;Lahy;Lahf;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, LahS;->c:LahS;

    iput-object v0, p0, LBi;->a:LahS;

    .line 40
    sget-object v0, Lagg;->h:Lagg;

    iput-object v0, p0, LBi;->a:Lagg;

    .line 61
    iput-object p1, p0, LBi;->a:Lamn;

    .line 62
    iput-object p2, p0, LBi;->a:LaKR;

    .line 63
    iput-object p3, p0, LBi;->a:LQr;

    .line 64
    iput-object p4, p0, LBi;->a:Ladi;

    .line 65
    iput-object p5, p0, LBi;->a:Lahy;

    .line 66
    iput-object p6, p0, LBi;->a:Lahf;

    .line 67
    return-void
.end method

.method private a(LCv;)Z
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0, p1}, LBi;->c(LCv;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LBi;->a:LaKS;

    invoke-virtual {v0}, LaKS;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LCv;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    iget-object v0, p0, LBi;->a:LahR;

    if-eqz v0, :cond_1

    iget-object v0, p0, LBi;->a:LahS;

    sget-object v3, LahS;->a:LahS;

    invoke-virtual {v0, v3}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lagg;->a:Lagg;

    sget-object v3, Lagg;->c:Lagg;

    sget-object v4, Lagg;->b:Lagg;

    .line 117
    invoke-static {v0, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iget-object v3, p0, LBi;->a:Lagg;

    .line 118
    invoke-virtual {v0, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LBi;->f:Z

    .line 120
    iget-object v0, p0, LBi;->a:LahR;

    if-eqz v0, :cond_2

    iget-object v0, p0, LBi;->a:LahS;

    sget-object v3, LahS;->a:LahS;

    invoke-virtual {v0, v3}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LBi;->a:Lagg;

    sget-object v3, Lagg;->d:Lagg;

    .line 121
    invoke-virtual {v0, v3}, Lagg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, LBi;->g:Z

    .line 122
    iget-boolean v0, p0, LBi;->c:Z

    if-nez v0, :cond_3

    .line 123
    sget-object v0, LBg;->d:LBg;

    iput-object v0, p0, LBi;->a:LBg;

    .line 141
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 118
    goto :goto_0

    :cond_1
    iget-boolean v0, p0, LBi;->h:Z

    goto :goto_0

    :cond_2
    move v1, v2

    .line 121
    goto :goto_1

    .line 124
    :cond_3
    invoke-direct {p0}, LBi;->g()Z

    move-result v0

    if-nez v0, :cond_4

    .line 125
    sget-object v0, LBg;->a:LBg;

    iput-object v0, p0, LBi;->a:LBg;

    goto :goto_2

    .line 127
    :cond_4
    iget-object v0, p0, LBi;->a:LaGo;

    if-nez v0, :cond_5

    .line 128
    invoke-interface {p1}, LCv;->a()LaGu;

    move-result-object v0

    .line 129
    instance-of v1, v0, LaGo;

    if-eqz v1, :cond_7

    check-cast v0, LaGo;

    :goto_3
    iput-object v0, p0, LBi;->a:LaGo;

    .line 132
    :cond_5
    iget-object v0, p0, LBi;->a:LaGo;

    if-eqz v0, :cond_6

    iget-object v0, p0, LBi;->a:Ladi;

    iget-object v1, p0, LBi;->a:LaGo;

    sget-object v2, LacY;->a:LacY;

    .line 133
    invoke-interface {v0, v1, v2}, Ladi;->d(LaGo;LacY;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 134
    :cond_6
    sget-object v0, LBg;->c:LBg;

    iput-object v0, p0, LBi;->a:LBg;

    goto :goto_2

    .line 129
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 135
    :cond_8
    iget-object v0, p0, LBi;->a:Ladi;

    iget-object v1, p0, LBi;->a:LaGo;

    sget-object v2, LacY;->a:LacY;

    invoke-interface {v0, v1, v2}, Ladi;->b(LaGo;LacY;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 136
    sget-object v0, LBg;->a:LBg;

    iput-object v0, p0, LBi;->a:LBg;

    goto :goto_2

    .line 138
    :cond_9
    sget-object v0, LBg;->b:LBg;

    iput-object v0, p0, LBi;->a:LBg;

    goto :goto_2
.end method

.method private b(LCv;)Z
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, LBi;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LBi;->c(LCv;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LBi;->a:LaKS;

    .line 178
    invoke-virtual {v0}, LaKS;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LCv;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, LBi;->a:LaGo;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LBi;->a:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, LCv;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    :cond_0
    sget-object v0, LBh;->g:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    .line 161
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-direct {p0}, LBi;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 147
    sget-object v0, LBh;->f:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    goto :goto_0

    .line 148
    :cond_2
    iget-boolean v0, p0, LBi;->b:Z

    if-eqz v0, :cond_3

    .line 149
    sget-object v0, LBh;->a:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    goto :goto_0

    .line 150
    :cond_3
    invoke-direct {p0}, LBi;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 151
    sget-object v0, LBh;->d:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    goto :goto_0

    .line 152
    :cond_4
    invoke-direct {p0, p1}, LBi;->a(LCv;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 153
    sget-object v0, LBh;->c:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    goto :goto_0

    .line 154
    :cond_5
    invoke-direct {p0, p1}, LBi;->b(LCv;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 155
    sget-object v0, LBh;->b:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    goto :goto_0

    .line 156
    :cond_6
    invoke-direct {p0}, LBi;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 157
    sget-object v0, LBh;->e:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    goto :goto_0

    .line 159
    :cond_7
    sget-object v0, LBh;->a:LBh;

    iput-object v0, p0, LBi;->a:LBh;

    goto :goto_0
.end method

.method private c(LCv;)Z
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, LBi;->a:Lamn;

    iget-object v1, p0, LBi;->a:LaKS;

    invoke-interface {v0, v1}, Lamn;->a(LaKS;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    invoke-interface {p1}, LCv;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, LBi;->a:LahR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LBi;->a:Lagg;

    sget-object v1, Lagg;->d:Lagg;

    invoke-virtual {v0, v1}, Lagg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, LBi;->a:LahR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LBi;->a:Lagg;

    sget-object v1, Lagg;->a:Lagg;

    invoke-virtual {v0, v1}, Lagg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LBi;->a:Lagg;

    sget-object v1, Lagg;->c:Lagg;

    .line 188
    invoke-virtual {v0, v1}, Lagg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LBi;->a:Lagg;

    sget-object v1, Lagg;->b:Lagg;

    invoke-virtual {v0, v1}, Lagg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LBi;->a:LahR;

    if-nez v0, :cond_2

    iget-boolean v0, p0, LBi;->h:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, LBi;->e:Z

    return v0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 202
    sget-object v0, LBh;->d:LBh;

    sget-object v1, LBh;->a:LBh;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iget-object v1, p0, LBi;->a:LBh;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()LBg;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, LBi;->a:LBg;

    return-object v0
.end method

.method public a()LBh;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, LBi;->a:LBh;

    return-object v0
.end method

.method public a()LahR;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, LBi;->a:LahR;

    return-object v0
.end method

.method public a()LahS;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, LBi;->a:LahS;

    return-object v0
.end method

.method public a(LCv;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 71
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-interface {p1}, LCv;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LBi;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 74
    iget-object v0, p0, LBi;->a:Lahy;

    iget-object v4, p0, LBi;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v4}, Lahy;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahR;

    move-result-object v0

    iput-object v0, p0, LBi;->a:LahR;

    .line 75
    iget-object v0, p0, LBi;->a:LahR;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, LBi;->a:LahR;

    invoke-virtual {v0}, LahR;->a()LahS;

    move-result-object v0

    iput-object v0, p0, LBi;->a:LahS;

    .line 77
    iget-object v0, p0, LBi;->a:LahR;

    invoke-virtual {v0}, LahR;->a()Lagg;

    move-result-object v0

    iput-object v0, p0, LBi;->a:Lagg;

    .line 83
    :goto_0
    invoke-interface {p1}, LCv;->d()Z

    move-result v0

    iput-boolean v0, p0, LBi;->a:Z

    .line 84
    iget-boolean v0, p0, LBi;->a:Z

    if-eqz v0, :cond_4

    .line 85
    invoke-interface {p1}, LCv;->a()LaGu;

    move-result-object v0

    .line 86
    instance-of v4, v0, LaGo;

    if-eqz v4, :cond_2

    check-cast v0, LaGo;

    :goto_1
    iput-object v0, p0, LBi;->a:LaGo;

    .line 87
    invoke-interface {p1}, LCv;->f()Z

    move-result v0

    iput-boolean v0, p0, LBi;->e:Z

    .line 88
    invoke-interface {p1}, LCv;->a()J

    move-result-wide v0

    iget-object v4, p0, LBi;->a:LQr;

    .line 89
    invoke-static {v4}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LQr;)I

    move-result v4

    int-to-long v4, v4

    cmp-long v0, v0, v4

    if-ltz v0, :cond_3

    move v0, v2

    :goto_2
    iput-boolean v0, p0, LBi;->b:Z

    .line 95
    :goto_3
    invoke-interface {p1}, LCv;->b()Z

    move-result v0

    iput-boolean v0, p0, LBi;->c:Z

    .line 96
    invoke-interface {p1}, LCv;->a()Z

    move-result v0

    iput-boolean v0, p0, LBi;->d:Z

    .line 97
    iget-object v0, p0, LBi;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()LaKS;

    move-result-object v0

    iput-object v0, p0, LBi;->a:LaKS;

    .line 105
    invoke-interface {p1}, LCv;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 106
    invoke-interface {p1}, LCv;->e()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, LBi;->b:Z

    if-nez v0, :cond_5

    .line 108
    invoke-interface {p1}, LCv;->f()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, LBi;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LBi;->a:Lamn;

    iget-object v1, p0, LBi;->a:LaKS;

    .line 109
    invoke-interface {v0, v1}, Lamn;->a(LaKS;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    :goto_4
    iput-boolean v2, p0, LBi;->h:Z

    .line 111
    invoke-direct {p0, p1}, LBi;->b(LCv;)V

    .line 112
    invoke-direct {p0, p1}, LBi;->c(LCv;)V

    .line 113
    return-void

    .line 79
    :cond_1
    sget-object v0, LahS;->c:LahS;

    iput-object v0, p0, LBi;->a:LahS;

    .line 80
    sget-object v0, Lagg;->h:Lagg;

    iput-object v0, p0, LBi;->a:Lagg;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 86
    goto :goto_1

    :cond_3
    move v0, v3

    .line 89
    goto :goto_2

    .line 91
    :cond_4
    iput-object v1, p0, LBi;->a:LaGo;

    .line 92
    iput-boolean v3, p0, LBi;->e:Z

    .line 93
    iput-boolean v3, p0, LBi;->b:Z

    goto :goto_3

    :cond_5
    move v2, v3

    .line 109
    goto :goto_4
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 198
    iget-boolean v0, p0, LBi;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, LBi;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LahS;->a:LahS;

    iget-object v1, p0, LBi;->a:LahS;

    invoke-virtual {v0, v1}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, LBi;->g:Z

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, LBi;->a:LaGo;

    if-nez v0, :cond_0

    .line 234
    const/4 v0, 0x0

    .line 237
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LBi;->a:Lahf;

    iget-object v1, p0, LBi;->a:LaGo;

    invoke-interface {v0, v1}, Lahf;->a(LaGo;)Z

    move-result v0

    goto :goto_0
.end method

.method public d()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 242
    iget-object v1, p0, LBi;->a:LaGo;

    if-nez v1, :cond_1

    .line 246
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LBi;->a:LaGo;

    sget-object v2, LacY;->a:LacY;

    invoke-interface {v1, v2}, LaGo;->a(LacY;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, LBi;->d:Z

    return v0
.end method
