.class final LbpX;
.super Lbqb;
.source "Sets.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbqb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbiU;

.field final synthetic a:Ljava/util/Set;

.field final synthetic b:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Set;LbiU;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 760
    iput-object p1, p0, LbpX;->a:Ljava/util/Set;

    iput-object p2, p0, LbpX;->a:LbiU;

    iput-object p3, p0, LbpX;->b:Ljava/util/Set;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbqb;-><init>(LbpV;)V

    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 778
    iget-object v0, p0, LbpX;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LbpX;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 773
    iget-object v0, p0, LbpX;->b:Ljava/util/Set;

    iget-object v1, p0, LbpX;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 763
    iget-object v0, p0, LbpX;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, LbpX;->a:LbiU;

    invoke-static {v0, v1}, Lbnr;->a(Ljava/util/Iterator;LbiU;)Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, LbpX;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lbnr;->a(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method
