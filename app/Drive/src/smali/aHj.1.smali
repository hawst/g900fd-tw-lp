.class public final LaHj;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHx;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHz;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHD;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHv;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHI;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHs;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lagw;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHr;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 46
    iput-object p1, p0, LaHj;->a:LbrA;

    .line 47
    const-class v0, LaHx;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->a:Lbsk;

    .line 50
    const-class v0, LaHz;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->b:Lbsk;

    .line 53
    const-class v0, LaHD;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->c:Lbsk;

    .line 56
    const-class v0, LaHv;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->d:Lbsk;

    .line 59
    const-class v0, LaHI;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->e:Lbsk;

    .line 62
    const-class v0, LaHs;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->f:Lbsk;

    .line 65
    const-class v0, Lagw;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->g:Lbsk;

    .line 68
    const-class v0, LaHr;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->h:Lbsk;

    .line 71
    const-class v0, LaHE;

    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaHj;->i:Lbsk;

    .line 74
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 116
    sparse-switch p1, :sswitch_data_0

    .line 280
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :sswitch_0
    new-instance v1, LaHx;

    iget-object v0, p0, LaHj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 121
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 119
    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    invoke-direct {v1, v0}, LaHx;-><init>(LQr;)V

    move-object v0, v1

    .line 278
    :goto_0
    return-object v0

    .line 128
    :sswitch_1
    new-instance v0, LaHz;

    iget-object v1, p0, LaHj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 131
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 129
    invoke-static {v1, v2}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->j:Lbsk;

    .line 137
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaHj;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->j:Lbsk;

    .line 135
    invoke-static {v2, v3}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGg;

    iget-object v3, p0, LaHj;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaHj;

    iget-object v3, v3, LaHj;->h:Lbsk;

    .line 143
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaHj;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaHj;

    iget-object v4, v4, LaHj;->h:Lbsk;

    .line 141
    invoke-static {v3, v4}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaHr;

    iget-object v4, p0, LaHj;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->y:Lbsk;

    .line 149
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaHj;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->y:Lbsk;

    .line 147
    invoke-static {v4, v5}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lald;

    iget-object v5, p0, LaHj;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->t:Lbsk;

    .line 155
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaHj;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lajo;

    iget-object v6, v6, Lajo;->t:Lbsk;

    .line 153
    invoke-static {v5, v6}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Laja;

    iget-object v6, p0, LaHj;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->L:Lbsk;

    .line 161
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaHj;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LalC;

    iget-object v7, v7, LalC;->L:Lbsk;

    .line 159
    invoke-static {v6, v7}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LaKR;

    invoke-direct/range {v0 .. v6}, LaHz;-><init>(LtK;LaGg;LaHr;Lald;Laja;LaKR;)V

    goto/16 :goto_0

    .line 168
    :sswitch_2
    new-instance v2, LaHD;

    iget-object v0, p0, LaHj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->f:Lbsk;

    .line 171
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaHj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laek;

    iget-object v1, v1, Laek;->f:Lbsk;

    .line 169
    invoke-static {v0, v1}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeo;

    iget-object v1, p0, LaHj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHj;

    iget-object v1, v1, LaHj;->i:Lbsk;

    .line 177
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaHj;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaHj;

    iget-object v3, v3, LaHj;->i:Lbsk;

    .line 175
    invoke-static {v1, v3}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaHE;

    invoke-direct {v2, v0, v1}, LaHD;-><init>(Laeo;LaHE;)V

    move-object v0, v2

    .line 182
    goto/16 :goto_0

    .line 184
    :sswitch_3
    new-instance v4, LaHv;

    iget-object v0, p0, LaHj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 187
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaHj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 185
    invoke-static {v0, v1}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LaHj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 193
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 191
    invoke-static {v1, v2}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->y:Lbsk;

    .line 199
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaHj;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->y:Lbsk;

    .line 197
    invoke-static {v2, v3}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lald;

    iget-object v3, p0, LaHj;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaHj;

    iget-object v3, v3, LaHj;->h:Lbsk;

    .line 205
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LaHj;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaHj;

    iget-object v5, v5, LaHj;->h:Lbsk;

    .line 203
    invoke-static {v3, v5}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaHr;

    invoke-direct {v4, v0, v1, v2, v3}, LaHv;-><init>(Landroid/content/Context;LaGM;Lald;LaHr;)V

    move-object v0, v4

    .line 210
    goto/16 :goto_0

    .line 212
    :sswitch_4
    new-instance v1, LaHI;

    iget-object v0, p0, LaHj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->h:Lbsk;

    .line 215
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->h:Lbsk;

    .line 213
    invoke-static {v0, v2}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laer;

    invoke-direct {v1, v0}, LaHI;-><init>(Laer;)V

    move-object v0, v1

    .line 220
    goto/16 :goto_0

    .line 222
    :sswitch_5
    new-instance v0, LaHs;

    iget-object v1, p0, LaHj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 225
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->j:Lbsk;

    .line 223
    invoke-static {v1, v2}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGg;

    iget-object v2, p0, LaHj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->D:Lbsk;

    .line 231
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaHj;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LagN;

    iget-object v3, v3, LagN;->D:Lbsk;

    .line 229
    invoke-static {v2, v3}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iget-object v3, p0, LaHj;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 237
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaHj;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 235
    invoke-static {v3, v4}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LQr;

    iget-object v4, p0, LaHj;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaHj;

    iget-object v4, v4, LaHj;->a:Lbsk;

    .line 243
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaHj;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaHj;

    iget-object v5, v5, LaHj;->a:Lbsk;

    .line 241
    invoke-static {v4, v5}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LaHx;

    iget-object v5, p0, LaHj;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lc;

    iget-object v5, v5, Lc;->a:Lbsk;

    .line 249
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaHj;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lc;

    iget-object v6, v6, Lc;->a:Lbsk;

    .line 247
    invoke-static {v5, v6}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    iget-object v6, p0, LaHj;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LUw;

    iget-object v6, v6, LUw;->b:Lbsk;

    .line 255
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaHj;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LUw;

    iget-object v7, v7, LUw;->b:Lbsk;

    .line 253
    invoke-static {v6, v7}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LUx;

    iget-object v7, p0, LaHj;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaHj;

    iget-object v7, v7, LaHj;->c:Lbsk;

    .line 261
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LaHj;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LaHj;

    iget-object v8, v8, LaHj;->c:Lbsk;

    .line 259
    invoke-static {v7, v8}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LaHD;

    iget-object v8, p0, LaHj;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LalC;

    iget-object v8, v8, LalC;->L:Lbsk;

    .line 267
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, LaHj;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LalC;

    iget-object v9, v9, LalC;->L:Lbsk;

    .line 265
    invoke-static {v8, v9}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LaKR;

    iget-object v9, p0, LaHj;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LpG;

    iget-object v9, v9, LpG;->m:Lbsk;

    .line 273
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, LaHj;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LpG;

    iget-object v10, v10, LpG;->m:Lbsk;

    .line 271
    invoke-static {v9, v10}, LaHj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LtK;

    invoke-direct/range {v0 .. v9}, LaHs;-><init>(LaGg;Ljava/util/concurrent/Executor;LQr;LaHx;Landroid/content/Context;LUx;LaHD;LaKR;LtK;)V

    goto/16 :goto_0

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0xf -> :sswitch_1
        0x275 -> :sswitch_3
        0x2c5 -> :sswitch_5
        0x2c6 -> :sswitch_0
        0x2c7 -> :sswitch_2
        0x2c9 -> :sswitch_4
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 295
    sparse-switch p2, :sswitch_data_0

    .line 324
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :sswitch_0
    check-cast p1, LaHA;

    .line 299
    iget-object v0, p0, LaHj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHj;

    iget-object v0, v0, LaHj;->f:Lbsk;

    .line 302
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHs;

    .line 299
    invoke-virtual {p1, v0}, LaHA;->provideEntryUpdateListener(LaHs;)Lagw;

    move-result-object v0

    .line 317
    :goto_0
    return-object v0

    .line 306
    :sswitch_1
    check-cast p1, LaHA;

    .line 308
    iget-object v0, p0, LaHj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHj;

    iget-object v0, v0, LaHj;->f:Lbsk;

    .line 311
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHs;

    .line 308
    invoke-virtual {p1, v0}, LaHA;->provide(LaHs;)LaHr;

    move-result-object v0

    goto :goto_0

    .line 315
    :sswitch_2
    check-cast p1, LaHA;

    .line 317
    iget-object v0, p0, LaHj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHj;

    iget-object v0, v0, LaHj;->e:Lbsk;

    .line 320
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHI;

    .line 317
    invoke-virtual {p1, v0}, LaHA;->provideServerRequestIssuer(LaHI;)LaHE;

    move-result-object v0

    goto :goto_0

    .line 295
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x12 -> :sswitch_2
        0x24d -> :sswitch_0
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 81
    const-class v0, LaHx;

    iget-object v1, p0, LaHj;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 82
    const-class v0, LaHz;

    iget-object v1, p0, LaHj;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 83
    const-class v0, LaHD;

    iget-object v1, p0, LaHj;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 84
    const-class v0, LaHv;

    iget-object v1, p0, LaHj;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 85
    const-class v0, LaHI;

    iget-object v1, p0, LaHj;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 86
    const-class v0, LaHs;

    iget-object v1, p0, LaHj;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 87
    const-class v0, Lagw;

    iget-object v1, p0, LaHj;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 88
    const-class v0, LaHr;

    iget-object v1, p0, LaHj;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 89
    const-class v0, LaHE;

    iget-object v1, p0, LaHj;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LaHj;->a(Ljava/lang/Class;Lbsk;)V

    .line 90
    iget-object v0, p0, LaHj;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2c6

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 92
    iget-object v0, p0, LaHj;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xf

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 94
    iget-object v0, p0, LaHj;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2c7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 96
    iget-object v0, p0, LaHj;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x275

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 98
    iget-object v0, p0, LaHj;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2c9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 100
    iget-object v0, p0, LaHj;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2c5

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 102
    iget-object v0, p0, LaHj;->g:Lbsk;

    const-class v1, LaHA;

    const/16 v2, 0x24d

    invoke-virtual {p0, v1, v2}, LaHj;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 104
    iget-object v0, p0, LaHj;->h:Lbsk;

    const-class v1, LaHA;

    const/16 v2, 0x10

    invoke-virtual {p0, v1, v2}, LaHj;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 106
    iget-object v0, p0, LaHj;->i:Lbsk;

    const-class v1, LaHA;

    const/16 v2, 0x12

    invoke-virtual {p0, v1, v2}, LaHj;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 108
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 287
    .line 289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method
