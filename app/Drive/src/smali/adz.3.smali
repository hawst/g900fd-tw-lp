.class public final enum Ladz;
.super Ljava/lang/Enum;
.source "FileContentInstance.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ladz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ladz;

.field private static final synthetic a:[Ladz;

.field public static final enum b:Ladz;

.field public static final enum c:Ladz;

.field public static final enum d:Ladz;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Ladz;

    const-string v1, "LOCKED"

    invoke-direct {v0, v1, v2}, Ladz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ladz;->a:Ladz;

    .line 60
    new-instance v0, Ladz;

    const-string v1, "LOCKED_FOR_CREATION"

    invoke-direct {v0, v1, v3}, Ladz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ladz;->b:Ladz;

    .line 61
    new-instance v0, Ladz;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4}, Ladz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ladz;->c:Ladz;

    .line 62
    new-instance v0, Ladz;

    const-string v1, "DESTROYED"

    invoke-direct {v0, v1, v5}, Ladz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ladz;->d:Ladz;

    .line 58
    const/4 v0, 0x4

    new-array v0, v0, [Ladz;

    sget-object v1, Ladz;->a:Ladz;

    aput-object v1, v0, v2

    sget-object v1, Ladz;->b:Ladz;

    aput-object v1, v0, v3

    sget-object v1, Ladz;->c:Ladz;

    aput-object v1, v0, v4

    sget-object v1, Ladz;->d:Ladz;

    aput-object v1, v0, v5

    sput-object v0, Ladz;->a:[Ladz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ladz;
    .locals 1

    .prologue
    .line 58
    const-class v0, Ladz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ladz;

    return-object v0
.end method

.method public static values()[Ladz;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Ladz;->a:[Ladz;

    invoke-virtual {v0}, [Ladz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ladz;

    return-object v0
.end method
