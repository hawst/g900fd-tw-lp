.class public final enum LaJS;
.super Ljava/lang/Enum;
.source "AccountMetadataEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaJS;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaJS;

.field private static final synthetic a:[LaJS;

.field public static final enum b:LaJS;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, LaJS;

    const-string v1, "LIMITED"

    invoke-direct {v0, v1, v2}, LaJS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaJS;->a:LaJS;

    .line 24
    new-instance v0, LaJS;

    const-string v1, "UNLIMITED"

    invoke-direct {v0, v1, v3}, LaJS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaJS;->b:LaJS;

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [LaJS;

    sget-object v1, LaJS;->a:LaJS;

    aput-object v1, v0, v2

    sget-object v1, LaJS;->b:LaJS;

    aput-object v1, v0, v3

    sput-object v0, LaJS;->a:[LaJS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaJS;
    .locals 1

    .prologue
    .line 22
    const-class v0, LaJS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaJS;

    return-object v0
.end method

.method public static values()[LaJS;
    .locals 1

    .prologue
    .line 22
    sget-object v0, LaJS;->a:[LaJS;

    invoke-virtual {v0}, [LaJS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaJS;

    return-object v0
.end method
