.class public LZV;
.super Ljava/lang/Object;
.source "ActivityAppForEntryInstalledListener.java"

# interfaces
.implements LZX;


# instance fields
.field private final a:LM;

.field private final a:LZY;

.field private final a:LaGM;

.field private final a:Lals;

.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;LZY;Lals;LM;LaGM;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, LZV;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, LZV;->a:LZY;

    .line 37
    iput-object p3, p0, LZV;->a:Lals;

    .line 38
    iput-object p4, p0, LZV;->a:LM;

    .line 39
    iput-object p5, p0, LZV;->a:LaGM;

    .line 40
    return-void
.end method

.method static synthetic a(LZV;)LM;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, LZV;->a:LM;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LZV;->a:LZY;

    invoke-virtual {v0, p0}, LZY;->a(LZX;)V

    .line 66
    return-void
.end method

.method public a(Ljava/util/Set;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LuM;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, LZV;->a:Landroid/content/Context;

    invoke-static {v0, p1}, LZY;->a(Landroid/content/Context;Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public a(LuM;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 2

    .prologue
    .line 45
    if-eqz p2, :cond_0

    iget-object v0, p0, LZV;->a:LaGM;

    invoke-interface {v0, p2}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 46
    :goto_0
    if-nez v0, :cond_1

    .line 47
    iget-object v0, p0, LZV;->a:Landroid/content/Context;

    invoke-static {v0, p1}, LZY;->a(Landroid/content/Context;LuM;)Z

    move-result v0

    .line 56
    :goto_1
    return v0

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    :cond_1
    iget-object v0, p0, LZV;->a:Lals;

    new-instance v1, LZW;

    invoke-direct {v1, p0, p1, p2}, LZW;-><init>(LZV;LuM;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v0, v1}, Lals;->execute(Ljava/lang/Runnable;)V

    .line 56
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, LZV;->a:LZY;

    invoke-virtual {v0, p0}, LZY;->b(LZX;)V

    .line 70
    return-void
.end method
