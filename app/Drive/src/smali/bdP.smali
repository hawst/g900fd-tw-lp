.class public abstract LbdP;
.super LbdJ;
.source "AbstractGoogleJsonClientRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LbdJ",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final jsonContent:Ljava/lang/Object;


# direct methods
.method public constructor <init>(LbdN;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbdN;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 57
    if-nez p4, :cond_0

    move-object v4, v0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LbdJ;-><init>(LbdH;Ljava/lang/String;Ljava/lang/String;LbdZ;Ljava/lang/Class;)V

    .line 61
    iput-object p4, p0, LbdP;->jsonContent:Ljava/lang/Object;

    .line 62
    return-void

    .line 57
    :cond_0
    new-instance v1, LbeL;

    .line 58
    invoke-virtual {p1}, LbdN;->getJsonFactory()LbeO;

    move-result-object v2

    invoke-direct {v1, v2, p4}, LbeL;-><init>(LbeO;Ljava/lang/Object;)V

    .line 59
    invoke-virtual {p1}, LbdN;->getObjectParser()LbeQ;

    move-result-object v2

    invoke-virtual {v2}, LbeQ;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, LbeL;->a(Ljava/lang/String;)LbeL;

    move-result-object v4

    goto :goto_0

    :cond_1
    const-string v0, "data"

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic getAbstractGoogleClient()LbdH;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, LbdP;->getAbstractGoogleClient()LbdN;

    move-result-object v0

    return-object v0
.end method

.method public getAbstractGoogleClient()LbdN;
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, LbdJ;->getAbstractGoogleClient()LbdH;

    move-result-object v0

    check-cast v0, LbdN;

    return-object v0
.end method

.method public getJsonContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, LbdP;->jsonContent:Ljava/lang/Object;

    return-object v0
.end method

.method protected newExceptionOnError(Lbel;)Lbdz;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, LbdP;->getAbstractGoogleClient()LbdN;

    move-result-object v0

    invoke-virtual {v0}, LbdN;->getJsonFactory()LbeO;

    move-result-object v0

    invoke-static {v0, p1}, Lbdz;->a(LbeO;Lbel;)Lbdz;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic newExceptionOnError(Lbel;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1}, LbdP;->newExceptionOnError(Lbel;)Lbdz;

    move-result-object v0

    return-object v0
.end method

.method public final queue(Lbdm;Lbdv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbdm;",
            "Lbdv",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 108
    const-class v0, Lbdy;

    invoke-super {p0, p1, v0, p2}, LbdJ;->queue(Lbdm;Ljava/lang/Class;Lbdl;)V

    .line 109
    return-void
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, LbdP;->set(Ljava/lang/String;Ljava/lang/Object;)LbdP;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "LbdP",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, LbdJ;->set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;

    move-result-object v0

    check-cast v0, LbdP;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, LbdP;->set(Ljava/lang/String;Ljava/lang/Object;)LbdP;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setDisableGZipContent(Z)LbdJ;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1}, LbdP;->setDisableGZipContent(Z)LbdP;

    move-result-object v0

    return-object v0
.end method

.method public setDisableGZipContent(Z)LbdP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LbdP",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 71
    invoke-super {p0, p1}, LbdJ;->setDisableGZipContent(Z)LbdJ;

    move-result-object v0

    check-cast v0, LbdP;

    return-object v0
.end method

.method public bridge synthetic setRequestHeaders(Lbed;)LbdJ;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1}, LbdP;->setRequestHeaders(Lbed;)LbdP;

    move-result-object v0

    return-object v0
.end method

.method public setRequestHeaders(Lbed;)LbdP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbed;",
            ")",
            "LbdP",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-super {p0, p1}, LbdJ;->setRequestHeaders(Lbed;)LbdJ;

    move-result-object v0

    check-cast v0, LbdP;

    return-object v0
.end method
