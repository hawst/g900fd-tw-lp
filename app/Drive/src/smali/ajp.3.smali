.class public Lajp;
.super Landroid/app/Application;
.source "GuiceApplication.java"

# interfaces
.implements Laju;


# instance fields
.field private volatile a:Lbuu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method private b()Lbuu;
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lajp;->b()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 37
    invoke-virtual {p0, v0}, Lajp;->a(Ljava/util/List;)V

    .line 38
    sget-object v1, Lajv;->a:Lajv;

    invoke-virtual {v1, p0, v0}, Lajv;->a(Landroid/content/Context;Ljava/util/List;)Lbuu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lbuu;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lajp;->a:Lbuu;

    if-nez v0, :cond_1

    .line 25
    const-class v1, Lbrw;

    monitor-enter v1

    .line 26
    :try_start_0
    iget-object v0, p0, Lajp;->a:Lbuu;

    if-nez v0, :cond_0

    .line 27
    invoke-direct {p0}, Lajp;->b()Lbuu;

    move-result-object v0

    iput-object v0, p0, Lajp;->a:Lbuu;

    .line 29
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    :cond_1
    iget-object v0, p0, Lajp;->a:Lbuu;

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LbuC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lajp;->a:Lbuu;

    .line 55
    sget-object v0, Lajv;->a:Lajv;

    invoke-virtual {v0, p0}, Lajv;->a(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 60
    invoke-virtual {p0}, Lajp;->d()V

    .line 61
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 62
    return-void
.end method
