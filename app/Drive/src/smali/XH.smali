.class public LXH;
.super Ljava/lang/Object;
.source "PunchMultiScreenModeFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, LXH;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 5

    .prologue
    .line 307
    iget-object v0, p0, LXH;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()LH;

    .line 309
    check-cast p1, Landroid/app/Presentation;

    .line 310
    invoke-virtual {p1}, Landroid/app/Presentation;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    .line 311
    iget-object v1, p0, LXH;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->c:Ljava/lang/String;

    const-string v2, "Presentation on display #%s was dismissed."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 312
    return-void
.end method
