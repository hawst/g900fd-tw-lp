.class public LbhP;
.super Ljava/lang/Object;
.source "FrameProcessor.java"


# instance fields
.field private a:F

.field private a:I

.field private a:J

.field final synthetic a:Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;)V
    .locals 2

    .prologue
    .line 82
    iput-object p1, p0, LbhP;->a:Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput v0, p0, LbhP;->a:I

    .line 84
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LbhP;->a:J

    .line 86
    const/4 v0, 0x0

    iput v0, p0, LbhP;->a:F

    return-void
.end method

.method private a(FFF)F
    .locals 2

    .prologue
    .line 89
    mul-float v0, p3, p1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p3

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    return v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 93
    iget v0, p0, LbhP;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LbhP;->a:I

    .line 95
    iget-wide v0, p0, LbhP;->a:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LbhP;->a:J

    .line 97
    iget-wide v0, p0, LbhP;->a:J

    long-to-float v0, v0

    iget v1, p0, LbhP;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 99
    iget v1, p0, LbhP;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 100
    iput v0, p0, LbhP;->a:F

    .line 107
    :goto_0
    return-void

    .line 104
    :cond_0
    iget v0, p0, LbhP;->a:F

    long-to-float v1, p1

    const v2, 0x3f733333    # 0.95f

    invoke-direct {p0, v0, v1, v2}, LbhP;->a(FFF)F

    move-result v0

    iput v0, p0, LbhP;->a:F

    goto :goto_0
.end method

.method public static synthetic a(LbhP;J)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, LbhP;->a(J)V

    return-void
.end method


# virtual methods
.method public a()F
    .locals 2

    .prologue
    .line 120
    iget v0, p0, LbhP;->a:I

    if-gtz v0, :cond_0

    .line 121
    const/4 v0, 0x0

    .line 123
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, LbhP;->a:J

    long-to-float v0, v0

    iget v1, p0, LbhP;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, LbhP;->a:Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LbhP;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " frames] ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LbhP;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms total] ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LbhP;->a:F

    const/4 v2, 0x2

    .line 113
    invoke-static {v1, v2}, Lcom/google/bionics/scanner/unveil/util/NumberUtils;->format(FI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[DISABLED]"

    goto :goto_0
.end method
