.class LFL;
.super Ljava/lang/Object;
.source "AudioDocumentOpener.java"

# interfaces
.implements LFR;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, LFL;->a:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 33
    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot be downloaded because it is not known to the server"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    .line 38
    :cond_0
    iget-object v1, p0, LFL;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/content/Intent;

    move-result-object v0

    .line 40
    new-instance v1, LGy;

    iget-object v2, p0, LFL;->a:Landroid/content/Context;

    invoke-interface {p2}, LaGo;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, v0}, LGy;-><init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;)V

    .line 41
    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
