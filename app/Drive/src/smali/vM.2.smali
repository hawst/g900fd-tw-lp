.class public LvM;
.super Ljava/lang/Object;
.source "CriterionFactoryImpl.java"

# interfaces
.implements LvL;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1

    .prologue
    .line 54
    const-string v0, "notInTrash"

    invoke-static {v0}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a(Ljava/lang/String;)Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    move-result-object v0

    return-object v0
.end method

.method public a(LCl;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    invoke-direct {v0, p1, v1, v1}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;-><init>(LCl;ZZ)V

    return-object v0
.end method

.method public a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;-><init>(LaFO;)V

    return-object v0
.end method

.method public a(Laay;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;-><init>(Laay;)V

    return-object v0
.end method

.method public a(LbmY;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;)",
            "Lcom/google/android/apps/docs/app/model/navigation/Criterion;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;-><init>(LbmY;)V

    return-object v0
.end method

.method public a(LbmY;Z)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/android/apps/docs/app/model/navigation/Criterion;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;-><init>(LbmY;Z)V

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-object v0
.end method

.method public b()Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1

    .prologue
    .line 80
    const-string v0, "noCollection"

    invoke-static {v0}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a(Ljava/lang/String;)Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    move-result-object v0

    return-object v0
.end method

.method public b(LCl;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;-><init>(LCl;ZZ)V

    return-object v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-object v0
.end method

.method public c()Lcom/google/android/apps/docs/app/model/navigation/Criterion;
    .locals 1

    .prologue
    .line 85
    const-string v0, "noPlaceholder"

    invoke-static {v0}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a(Ljava/lang/String;)Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    move-result-object v0

    return-object v0
.end method
