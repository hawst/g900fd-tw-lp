.class public final LaNe;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private a:Landroid/location/Location;

.field private final a:Landroid/os/Bundle;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/Date;

.field private final a:Ljava/util/HashMap;

.field private final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private b:Ljava/lang/String;

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaNe;->a:Ljava/util/HashSet;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LaNe;->a:Landroid/os/Bundle;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaNe;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaNe;->b:Ljava/util/HashSet;

    iput v1, p0, LaNe;->a:I

    const/4 v0, 0x0

    iput-boolean v0, p0, LaNe;->a:Z

    iput v1, p0, LaNe;->b:I

    return-void
.end method

.method static synthetic a(LaNe;)I
    .locals 1

    iget v0, p0, LaNe;->a:I

    return v0
.end method

.method static synthetic a(LaNe;)Landroid/location/Location;
    .locals 1

    iget-object v0, p0, LaNe;->a:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic a(LaNe;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, LaNe;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic a(LaNe;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaNe;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LaNe;)Ljava/util/Date;
    .locals 1

    iget-object v0, p0, LaNe;->a:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic a(LaNe;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, LaNe;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(LaNe;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, LaNe;->a:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic a(LaNe;)Z
    .locals 1

    iget-boolean v0, p0, LaNe;->a:Z

    return v0
.end method

.method static synthetic b(LaNe;)I
    .locals 1

    iget v0, p0, LaNe;->b:I

    return v0
.end method

.method static synthetic b(LaNe;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaNe;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(LaNe;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, LaNe;->b:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, LaNe;->a:I

    return-void
.end method

.method public a(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LayD;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, LaNe;->a:Landroid/os/Bundle;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, LaNe;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, LaNe;->a:Ljava/util/Date;

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, LaNe;->b:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, LaNe;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method
