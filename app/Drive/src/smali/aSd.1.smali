.class public final LaSd;
.super LaLr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaLr",
        "<",
        "LaRV;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LaSd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaSd;

    invoke-direct {v0}, LaSd;-><init>()V

    sput-object v0, LaSd;->a:LaSd;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.common.ui.SignInButtonCreatorImpl"

    invoke-direct {p0, v0}, LaLr;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;II)Landroid/view/View;
    .locals 1

    sget-object v0, LaSd;->a:LaSd;

    invoke-direct {v0, p0, p1, p2}, LaSd;->b(Landroid/content/Context;II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;II)Landroid/view/View;
    .locals 4

    :try_start_0
    invoke-static {p1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    invoke-virtual {p0, p1}, LaSd;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaRV;

    invoke-interface {v0, v1, p2, p3}, LaRV;->a(LaLm;II)LaLm;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LaLs;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not get button with size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and color "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LaLs;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a(Landroid/os/IBinder;)LaRV;
    .locals 1

    invoke-static {p1}, LaRW;->a(Landroid/os/IBinder;)LaRV;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, LaSd;->a(Landroid/os/IBinder;)LaRV;

    move-result-object v0

    return-object v0
.end method
