.class public Lxr;
.super Ljava/lang/Object;
.source "Animators.java"


# instance fields
.field private a:I

.field private a:Landroid/animation/Animator$AnimatorListener;

.field private a:Landroid/animation/Animator;

.field private a:Landroid/view/animation/Interpolator;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    const/4 v0, -0x1

    iput v0, p0, Lxr;->a:I

    .line 271
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lxr;->a:Ljava/util/List;

    .line 272
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lxr;->b:Ljava/util/List;

    .line 277
    iput-object p1, p0, Lxr;->a:Landroid/animation/Animator;

    .line 278
    return-void
.end method

.method synthetic constructor <init>(Landroid/animation/Animator;Lxn;)V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0, p1}, Lxr;-><init>(Landroid/animation/Animator;)V

    return-void
.end method

.method private a()Landroid/animation/AnimatorSet;
    .locals 2

    .prologue
    .line 415
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 416
    iget-object v1, p0, Lxr;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 417
    iget-object v1, p0, Lxr;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 419
    :cond_0
    iget-object v1, p0, Lxr;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 420
    iget-object v1, p0, Lxr;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playSequentially(Ljava/util/List;)V

    .line 422
    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 381
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lxr;->a(J)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 388
    invoke-virtual {p0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 389
    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 390
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 391
    return-object v0
.end method

.method public a()Lxr;
    .locals 1

    .prologue
    .line 303
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lxr;->a:Landroid/view/animation/Interpolator;

    .line 304
    return-object p0
.end method

.method public a(I)Lxr;
    .locals 0

    .prologue
    .line 293
    iput p1, p0, Lxr;->a:I

    .line 294
    return-object p0
.end method

.method public a(Landroid/animation/Animator$AnimatorListener;)Lxr;
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lxr;->a:Landroid/animation/Animator$AnimatorListener;

    .line 299
    return-object p0
.end method

.method public a(Landroid/animation/Animator;)Lxr;
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lxr;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lxr;->a:Ljava/util/List;

    iget-object v1, p0, Lxr;->a:Landroid/animation/Animator;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lxr;->a:Landroid/animation/Animator;

    .line 354
    :cond_0
    iget-object v0, p0, Lxr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    return-object p0
.end method

.method public a(Landroid/content/Context;)Lxr;
    .locals 1

    .prologue
    .line 312
    invoke-static {p1}, Lxs;->b(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lxr;->a:Landroid/view/animation/Interpolator;

    .line 313
    return-object p0
.end method

.method public a(Landroid/content/res/Resources;)Lxr;
    .locals 1

    .prologue
    .line 281
    const/high16 v0, 0x10e0000

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lxr;->a(I)Lxr;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/animation/Interpolator;)Lxr;
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lxr;->a:Landroid/view/animation/Interpolator;

    .line 336
    return-object p0
.end method

.method public a(Lxr;)Lxr;
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p1}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    return-object v0
.end method

.method public b()Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 396
    iget-object v0, p0, Lxr;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_3

    .line 397
    iget-object v0, p0, Lxr;->a:Landroid/animation/Animator;

    .line 402
    :goto_0
    iget v1, p0, Lxr;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 403
    iget v1, p0, Lxr;->a:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 405
    :cond_0
    iget-object v1, p0, Lxr;->a:Landroid/animation/Animator$AnimatorListener;

    if-eqz v1, :cond_1

    .line 406
    iget-object v1, p0, Lxr;->a:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 408
    :cond_1
    iget-object v1, p0, Lxr;->a:Landroid/view/animation/Interpolator;

    if-eqz v1, :cond_2

    .line 409
    iget-object v1, p0, Lxr;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 411
    :cond_2
    return-object v0

    .line 399
    :cond_3
    invoke-direct {p0}, Lxr;->a()Landroid/animation/AnimatorSet;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/animation/Animator;)Lxr;
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lxr;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lxr;->b:Ljava/util/List;

    iget-object v1, p0, Lxr;->a:Landroid/animation/Animator;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    const/4 v0, 0x0

    iput-object v0, p0, Lxr;->a:Landroid/animation/Animator;

    .line 373
    :cond_0
    iget-object v0, p0, Lxr;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    return-object p0
.end method

.method public b(Landroid/content/Context;)Lxr;
    .locals 1

    .prologue
    .line 321
    invoke-static {p1}, Lxs;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lxr;->a:Landroid/view/animation/Interpolator;

    .line 322
    return-object p0
.end method

.method public b(Landroid/content/res/Resources;)Lxr;
    .locals 1

    .prologue
    .line 285
    const v0, 0x10e0002

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lxr;->a(I)Lxr;

    move-result-object v0

    return-object v0
.end method

.method public b(Lxr;)Lxr;
    .locals 1

    .prologue
    .line 362
    invoke-virtual {p1}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lxr;->b(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/Context;)Lxr;
    .locals 1

    .prologue
    .line 330
    invoke-static {p1}, Lxs;->c(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lxr;->a:Landroid/view/animation/Interpolator;

    .line 331
    return-object p0
.end method

.method public c(Landroid/content/res/Resources;)Lxr;
    .locals 1

    .prologue
    .line 289
    const v0, 0x10e0001

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lxr;->a(I)Lxr;

    move-result-object v0

    return-object v0
.end method
