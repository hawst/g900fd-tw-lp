.class LbkJ;
.super Ljava/lang/ref/WeakReference;
.source "LocalCache.java"

# interfaces
.implements Lbkr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TK;>;",
        "Lbkr",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field volatile a:LbkF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final e:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1302
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1367
    invoke-static {}, LbjQ;->a()LbkF;

    move-result-object v0

    iput-object v0, p0, LbkJ;->a:LbkF;

    .line 1303
    iput p3, p0, LbkJ;->a:I

    .line 1304
    iput-object p4, p0, LbkJ;->e:Lbkr;

    .line 1305
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1378
    iget v0, p0, LbkJ;->a:I

    return v0
.end method

.method public a()J
    .locals 1

    .prologue
    .line 1314
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()LbkF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1370
    iget-object v0, p0, LbkJ;->a:LbkF;

    return-object v0
.end method

.method public a()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1382
    iget-object v0, p0, LbkJ;->e:Lbkr;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1308
    invoke-virtual {p0}, LbkJ;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1318
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LbkF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbkF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1374
    iput-object p1, p0, LbkJ;->a:LbkF;

    .line 1375
    return-void
.end method

.method public a(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1326
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()J
    .locals 1

    .prologue
    .line 1340
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1322
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1344
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1330
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1352
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1348
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1360
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1356
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
