.class public Laox;
.super Ljava/lang/Object;
.source "FutureDependentValueGuard.java"

# interfaces
.implements Laoy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Ljava/io/Closeable;",
        ">",
        "Ljava/lang/Object;",
        "Laoy",
        "<TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/Closeable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 53
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    const-string v1, "FutureDependentValueGuard"

    const-string v2, "IOException thrown while closing Closeable"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Ljava/io/Closeable;

    invoke-virtual {p0, p1}, Laox;->a(Ljava/io/Closeable;)V

    return-void
.end method
