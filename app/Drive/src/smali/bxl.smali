.class public Lbxl;
.super Ljava/lang/Object;
.source "DocsXmlGDataParser.java"

# interfaces
.implements Lbxj;


# instance fields
.field private final a:Ljava/io/InputStream;

.field private a:Ljava/lang/String;

.field private final a:Lorg/xmlpull/v1/XmlPullParser;

.field private a:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lbxl;->a:Ljava/io/InputStream;

    .line 77
    iput-object p2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 79
    const-string v0, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    invoke-interface {p2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A XmlGDataParser needs to be constructed with a namespace aware XmlPullParser"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxl;->a:Z

    .line 86
    iget-object v0, p0, Lbxl;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 88
    :try_start_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_1
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    new-instance v1, Lbxk;

    const-string v2, "Could not create XmlGDataParser"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 748
    invoke-static {p0, p1}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 744
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final c()Lbxc;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 197
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbxl;->a:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 200
    :goto_0
    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 201
    packed-switch v1, :pswitch_data_0

    .line 214
    :cond_0
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_0

    .line 203
    :pswitch_0
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 204
    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v2

    .line 206
    const-string v3, "http://www.w3.org/2005/Atom"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    sget-object v2, Lbxq;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    invoke-direct {p0}, Lbxl;->d()Lbxc;

    move-result-object v0

    .line 217
    :cond_1
    return-object v0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private final d()Lbxc;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 232
    invoke-virtual {p0}, Lbxl;->a()Lbxc;

    move-result-object v0

    .line 234
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    const-string v2, "http://schemas.google.com/g/2005"

    sget-object v3, Lbxq;->k:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxc;->g(Ljava/lang/String;)V

    .line 236
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 237
    :goto_0
    const/4 v2, 0x1

    if-eq v1, v2, :cond_a

    .line 238
    packed-switch v1, :pswitch_data_0

    .line 290
    :cond_0
    :goto_1
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_0

    .line 242
    :pswitch_0
    invoke-virtual {p0, v0}, Lbxl;->a(Lbxc;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 247
    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v2

    .line 250
    const-string v3, "http://a9.com/-/spec/opensearchrss/1.0/"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "http://a9.com/-/spec/opensearch/1.1/"

    .line 251
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 252
    :cond_1
    sget-object v2, Lbxq;->C:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 253
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 254
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-static {v1, v4}, Lbxd;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbxc;->a(I)V

    goto :goto_1

    .line 255
    :cond_2
    sget-object v2, Lbxq;->D:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 256
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 257
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 256
    invoke-static {v1, v4}, Lbxd;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbxc;->b(I)V

    goto :goto_1

    .line 258
    :cond_3
    sget-object v2, Lbxq;->E:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 260
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 259
    invoke-static {v1, v4}, Lbxd;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbxc;->c(I)V

    goto :goto_1

    .line 262
    :cond_4
    const-string v3, "http://www.w3.org/2005/Atom"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 263
    sget-object v2, Lbxq;->q:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 264
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxc;->f(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 265
    :cond_5
    sget-object v2, Lbxq;->p:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 266
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxc;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 267
    :cond_6
    sget-object v2, Lbxq;->A:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 268
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxc;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 269
    :cond_7
    sget-object v2, Lbxq;->w:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 270
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->x:Ljava/lang/String;

    .line 271
    invoke-interface {v1, v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    invoke-static {v1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 273
    invoke-virtual {v0, v1}, Lbxc;->b(Ljava/lang/String;)V

    .line 275
    :cond_8
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->y:Ljava/lang/String;

    .line 276
    invoke-interface {v1, v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 277
    invoke-static {v1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 278
    invoke-virtual {v0, v1}, Lbxc;->c(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 280
    :cond_9
    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    :cond_a
    return-object v0

    .line 285
    :cond_b
    invoke-virtual {p0, v0}, Lbxl;->a(Lbxc;)V

    goto/16 :goto_1

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private d(Lbxb;)V
    .locals 3

    .prologue
    .line 677
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 678
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    .line 680
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sget-object v0, Lbxq;->t:Ljava/lang/String;

    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 681
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 683
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected <author>: Actual element: <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 685
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 688
    :cond_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 689
    :goto_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    .line 690
    packed-switch v0, :pswitch_data_0

    .line 710
    :cond_2
    :goto_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 692
    :pswitch_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 693
    sget-object v1, Lbxq;->v:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 694
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 695
    invoke-virtual {p1, v0}, Lbxb;->o(Ljava/lang/String;)V

    goto :goto_1

    .line 696
    :cond_3
    sget-object v1, Lbxq;->u:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 697
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 698
    invoke-virtual {p1, v0}, Lbxb;->A(Ljava/lang/String;)V

    goto :goto_1

    .line 702
    :pswitch_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 703
    sget-object v1, Lbxq;->t:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 712
    :cond_4
    return-void

    .line 690
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e(Lbxb;)V
    .locals 4

    .prologue
    .line 716
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 717
    sget-object v1, Lbxq;->G:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 718
    new-instance v0, Lbxh;

    invoke-direct {v0}, Lbxh;-><init>()V

    .line 719
    invoke-static {p1, v0}, Lbxi;->a(Lbxb;Lbxh;)V

    .line 720
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->H:Ljava/lang/String;

    invoke-static {v1, v2}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lbxh;->a(I)V

    .line 721
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->I:Ljava/lang/String;

    invoke-static {v1, v2}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxh;->a(Ljava/lang/String;)V

    .line 722
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->J:Ljava/lang/String;

    invoke-static {v1, v2}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxh;->b(Ljava/lang/String;)V

    .line 724
    invoke-virtual {p0}, Lbxl;->c()V

    .line 741
    :goto_0
    return-void

    .line 725
    :cond_0
    sget-object v1, Lbxq;->p:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 726
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxi;->a(Lbxb;Ljava/lang/String;)V

    goto :goto_0

    .line 727
    :cond_1
    sget-object v1, Lbxq;->B:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 728
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v1, Lbxq;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxi;->b(Lbxb;Ljava/lang/String;)V

    goto :goto_0

    .line 729
    :cond_2
    const-string v1, "interrupted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 730
    new-instance v0, Lbxg;

    invoke-direct {v0}, Lbxg;-><init>()V

    .line 731
    invoke-static {p1, v0}, Lbxi;->a(Lbxb;Lbxg;)V

    .line 732
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->I:Ljava/lang/String;

    invoke-static {v1, v2}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxg;->a(Ljava/lang/String;)V

    .line 733
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->K:Ljava/lang/String;

    invoke-static {v1, v2}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lbxg;->c(I)V

    .line 734
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->L:Ljava/lang/String;

    invoke-static {v1, v2}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lbxg;->b(I)V

    .line 735
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->M:Ljava/lang/String;

    invoke-static {v1, v2}, Lbxl;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lbxg;->a(I)V

    .line 737
    invoke-virtual {p0}, Lbxl;->c()V

    goto :goto_0

    .line 739
    :cond_3
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected batch element "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected a()Lbxb;
    .locals 1

    .prologue
    .line 182
    new-instance v0, Lbxb;

    invoke-direct {v0}, Lbxb;-><init>()V

    return-object v0
.end method

.method public a(Lbxb;)Lbxb;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 343
    invoke-virtual {p0}, Lbxl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "you shouldn\'t call this if hasMoreData() is false"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_0
    :try_start_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 354
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 355
    new-instance v1, Lbxk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected event START_TAG: Actual event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lorg/xmlpull/v1/XmlPullParser;->TYPES:[Ljava/lang/String;

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v1

    .line 350
    :catch_0
    move-exception v0

    .line 351
    new-instance v1, Lbxk;

    const-string v2, "Could not parse entry."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 359
    :cond_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 363
    sget-object v1, Lbxq;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lbxq;->a:Ljava/lang/String;

    .line 364
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 365
    new-instance v1, Lbxk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected <entry> or <partial>: Actual element: <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ">"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v1

    .line 369
    :cond_2
    if-nez p1, :cond_3

    .line 370
    invoke-virtual {p0}, Lbxl;->a()Lbxb;

    move-result-object p1

    .line 376
    :goto_0
    :try_start_1
    sget-object v1, Lbxq;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 377
    invoke-virtual {p0, p1}, Lbxl;->c(Lbxb;)V
    :try_end_1
    .catch Lbxk; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3

    .line 400
    :goto_1
    return-object p1

    .line 372
    :cond_3
    invoke-virtual {p1}, Lbxb;->a()V

    goto :goto_0

    .line 379
    :cond_4
    :try_start_2
    invoke-virtual {p0, p1}, Lbxl;->b(Lbxb;)V
    :try_end_2
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 381
    :catch_1
    move-exception v0

    .line 383
    :try_start_3
    invoke-virtual {p0}, Lbxl;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lbxl;->b()V
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2

    .line 389
    :cond_5
    :goto_2
    new-instance v1, Lbxk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not parse <entry>, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 384
    :catch_2
    move-exception v1

    .line 387
    iput-boolean v2, p0, Lbxl;->a:Z

    goto :goto_2

    .line 390
    :catch_3
    move-exception v0

    .line 392
    :try_start_4
    invoke-virtual {p0}, Lbxl;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lbxl;->b()V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_4

    .line 398
    :cond_6
    :goto_3
    new-instance v1, Lbxk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not parse <entry>, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 393
    :catch_4
    move-exception v1

    .line 396
    iput-boolean v2, p0, Lbxl;->a:Z

    goto :goto_3
.end method

.method protected a()Lbxc;
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lbxc;

    invoke-direct {v0}, Lbxc;-><init>()V

    return-object v0
.end method

.method protected final a()Lorg/xmlpull/v1/XmlPullParser;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 753
    iget-object v0, p0, Lbxl;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 755
    :try_start_0
    iget-object v0, p0, Lbxl;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 760
    :cond_0
    :goto_0
    return-void

    .line 756
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected a(Lbxb;)V
    .locals 0

    .prologue
    .line 773
    return-void
.end method

.method protected a(Lbxc;)V
    .locals 0

    .prologue
    .line 318
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxb;)V
    .locals 0

    .prologue
    .line 805
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    iget-boolean v2, p0, Lbxl;->a:Z

    if-eqz v2, :cond_0

    .line 333
    :goto_0
    return v1

    .line 330
    :cond_0
    :try_start_0
    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 331
    if-eq v2, v0, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 332
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected a(Lbxb;)Z
    .locals 1

    .prologue
    .line 787
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lbxc;)Z
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    return v0
.end method

.method public b()Lbxb;
    .locals 3

    .prologue
    .line 412
    const/4 v0, 0x0

    iput-object v0, p0, Lbxl;->a:Ljava/lang/String;

    .line 413
    invoke-virtual {p0}, Lbxl;->a()Lbxb;

    move-result-object v0

    .line 417
    :try_start_0
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 421
    if-eqz v1, :cond_0

    .line 422
    new-instance v0, Lbxk;

    const-string v1, "Attempting to initialize parsing beyond the start of the document."

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 418
    :catch_0
    move-exception v0

    .line 419
    new-instance v1, Lbxk;

    const-string v2, "Could not parse GData entry."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 427
    :cond_0
    :try_start_1
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v1

    .line 433
    :goto_0
    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    .line 434
    packed-switch v1, :pswitch_data_0

    .line 467
    :cond_1
    :try_start_2
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_7

    move-result v1

    goto :goto_0

    .line 428
    :catch_1
    move-exception v0

    .line 429
    new-instance v1, Lbxk;

    const-string v2, "Could not read next event."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 430
    :catch_2
    move-exception v0

    .line 431
    new-instance v1, Lbxk;

    const-string v2, "Could not read next event."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 436
    :pswitch_0
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 437
    sget-object v2, Lbxq;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 439
    :try_start_3
    invoke-virtual {p0, v0}, Lbxl;->b(Lbxb;)V
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 451
    :goto_1
    return-object v0

    .line 441
    :catch_3
    move-exception v0

    .line 442
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <partial> entry."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 444
    :catch_4
    move-exception v0

    .line 445
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <partial> entry."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 448
    :cond_2
    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 450
    :try_start_4
    invoke-virtual {p0, v0}, Lbxl;->c(Lbxb;)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_1

    .line 452
    :catch_5
    move-exception v0

    .line 453
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <entry>."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 455
    :catch_6
    move-exception v0

    .line 456
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <entry>."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 468
    :catch_7
    move-exception v0

    .line 469
    new-instance v1, Lbxk;

    const-string v2, "Could not read next event."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 472
    :cond_3
    new-instance v0, Lbxk;

    const-string v1, "No <entry> found in document."

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lbxc;
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lbxl;->a:Ljava/lang/String;

    .line 104
    :try_start_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    new-instance v0, Lbxk;

    const-string v1, "Attempting to initialize parsing beyond the start of the document."

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    new-instance v1, Lbxk;

    const-string v2, "Could not parse GData feed."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 114
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    .line 120
    :goto_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 121
    packed-switch v0, :pswitch_data_0

    .line 150
    :cond_1
    :try_start_2
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    move-result v0

    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    .line 116
    new-instance v1, Lbxk;

    const-string v2, "Could not read next event."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 117
    :catch_2
    move-exception v0

    .line 118
    new-instance v1, Lbxk;

    const-string v2, "Could not read next event."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 123
    :pswitch_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 124
    sget-object v1, Lbxq;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    :try_start_3
    invoke-direct {p0}, Lbxl;->c()Lbxc;
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v0

    .line 134
    :goto_1
    return-object v0

    .line 127
    :catch_3
    move-exception v0

    .line 128
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <partial> feed start"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 129
    :catch_4
    move-exception v0

    .line 130
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <partial> feed start"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 132
    :cond_2
    sget-object v1, Lbxq;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    :try_start_4
    invoke-direct {p0}, Lbxl;->d()Lbxc;
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    move-result-object v0

    goto :goto_1

    .line 135
    :catch_5
    move-exception v0

    .line 136
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <feed>."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 138
    :catch_6
    move-exception v0

    .line 139
    new-instance v1, Lbxk;

    const-string v2, "Unable to parse <feed>."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 151
    :catch_7
    move-exception v0

    .line 152
    new-instance v1, Lbxk;

    const-string v2, "Could not read next event."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 153
    :catch_8
    move-exception v0

    .line 154
    new-instance v1, Lbxk;

    const-string v2, "Could not read next event."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 157
    :cond_3
    new-instance v0, Lbxk;

    const-string v1, "No <feed> found in document."

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 482
    invoke-virtual {p0}, Lbxl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "you shouldn\'t call this if hasMoreData() is false"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 486
    :cond_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 489
    :goto_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 490
    packed-switch v0, :pswitch_data_0

    .line 497
    :cond_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 492
    :pswitch_0
    sget-object v0, Lbxq;->c:Ljava/lang/String;

    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    :cond_2
    return-void

    .line 490
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected b(Lbxb;)V
    .locals 3

    .prologue
    .line 541
    sget-object v0, Lbxq;->a:Ljava/lang/String;

    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected <partial>: Actual element: <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 544
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    const/4 v1, 0x0

    sget-object v2, Lbxq;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbxl;->a:Ljava/lang/String;

    .line 549
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 551
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 552
    :goto_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 553
    packed-switch v0, :pswitch_data_0

    .line 563
    :cond_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 555
    :pswitch_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 556
    sget-object v1, Lbxq;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    invoke-virtual {p0, p1}, Lbxl;->c(Lbxb;)V

    .line 565
    :cond_2
    return-void

    .line 553
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 511
    const/4 v0, 0x1

    .line 512
    :goto_0
    if-lez v0, :cond_0

    .line 513
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 514
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 516
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    .line 517
    goto :goto_0

    .line 519
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 520
    goto :goto_0

    .line 525
    :cond_0
    return-void

    .line 514
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected c(Lbxb;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 581
    sget-object v0, Lbxq;->c:Ljava/lang/String;

    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 582
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected <entry>: Actual element: <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 584
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 587
    :cond_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    const-string v1, "http://schemas.google.com/g/2005"

    sget-object v2, Lbxq;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->y(Ljava/lang/String;)V

    .line 588
    iget-object v0, p0, Lbxl;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lbxb;->z(Ljava/lang/String;)V

    .line 590
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 592
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 593
    :goto_0
    if-eq v0, v5, :cond_11

    .line 594
    packed-switch v0, :pswitch_data_0

    .line 669
    :cond_1
    :goto_1
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 596
    :pswitch_0
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 597
    sget-object v1, Lbxq;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 672
    :goto_2
    return-void

    .line 603
    :cond_2
    invoke-virtual {p0, p1}, Lbxl;->a(Lbxb;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 607
    const-string v1, "http://schemas.google.com/gdata/batch"

    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 610
    invoke-direct {p0, p1}, Lbxl;->e(Lbxb;)V

    goto :goto_1

    .line 611
    :cond_3
    sget-object v1, Lbxq;->p:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 612
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 613
    :cond_4
    sget-object v1, Lbxq;->q:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 614
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->l(Ljava/lang/String;)V

    goto :goto_1

    .line 615
    :cond_5
    sget-object v1, Lbxq;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 616
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v1, Lbxq;->h:Ljava/lang/String;

    .line 617
    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 618
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->l:Ljava/lang/String;

    .line 619
    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 620
    iget-object v2, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v3, Lbxq;->j:Ljava/lang/String;

    .line 621
    invoke-interface {v2, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 622
    sget-object v3, Lbxq;->f:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 623
    invoke-virtual {p1, v2}, Lbxb;->n(Ljava/lang/String;)V

    goto :goto_1

    .line 624
    :cond_6
    sget-object v3, Lbxq;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    sget-object v3, Lbxq;->o:Ljava/lang/String;

    .line 625
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 626
    invoke-virtual {p1, v2}, Lbxb;->u(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 628
    :cond_7
    invoke-virtual {p0, v0, v1, v2, p1}, Lbxl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxb;)V

    goto/16 :goto_1

    .line 633
    :cond_8
    sget-object v1, Lbxq;->r:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 634
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->x(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 635
    :cond_9
    sget-object v1, Lbxq;->s:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 636
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v1, Lbxq;->l:Ljava/lang/String;

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->s(Ljava/lang/String;)V

    .line 637
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v1, Lbxq;->m:Ljava/lang/String;

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->t(Ljava/lang/String;)V

    .line 638
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->r(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 639
    :cond_a
    sget-object v1, Lbxq;->t:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 640
    invoke-direct {p0, p1}, Lbxl;->d(Lbxb;)V

    goto/16 :goto_1

    .line 641
    :cond_b
    sget-object v1, Lbxq;->w:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 642
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v1, Lbxq;->x:Ljava/lang/String;

    .line 643
    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 644
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_c

    .line 645
    invoke-virtual {p1, v0}, Lbxb;->p(Ljava/lang/String;)V

    .line 647
    :cond_c
    iget-object v1, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    sget-object v2, Lbxq;->y:Ljava/lang/String;

    .line 648
    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 651
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 652
    invoke-virtual {p1, v1}, Lbxb;->q(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 654
    :cond_d
    sget-object v1, Lbxq;->z:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 655
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 656
    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 655
    invoke-virtual {p1, v0}, Lbxb;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 657
    :cond_e
    sget-object v1, Lbxq;->A:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 658
    iget-object v0, p0, Lbxl;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbxb;->m(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 659
    :cond_f
    sget-object v1, Lbxq;->F:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 660
    invoke-virtual {p1, v5}, Lbxb;->i(Z)V

    goto/16 :goto_1

    .line 662
    :cond_10
    invoke-virtual {p0, p1}, Lbxl;->a(Lbxb;)V

    goto/16 :goto_1

    .line 671
    :cond_11
    invoke-virtual {p1}, Lbxb;->b()V

    goto/16 :goto_2

    .line 594
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
