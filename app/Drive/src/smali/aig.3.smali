.class public Laig;
.super Ljava/lang/Object;
.source "PartialDownloadedFileStoreImpl.java"

# interfaces
.implements Laie;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:I

.field private final a:Lalo;

.field private final a:LcI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LcI",
            "<",
            "Ljava/lang/String;",
            "Laif;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LQr;Lalo;)V
    .locals 2

    .prologue
    const/4 v0, 0x3

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    if-eqz p1, :cond_0

    const-string v1, "maxIncompleteDownloads"

    .line 148
    invoke-interface {p1, v1, v0}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Laig;->a:I

    .line 149
    new-instance v0, Laih;

    iget v1, p0, Laig;->a:I

    invoke-direct {v0, p0, v1}, Laih;-><init>(Laig;I)V

    iput-object v0, p0, Laig;->a:LcI;

    .line 158
    iput-object p2, p0, Laig;->a:Lalo;

    .line 159
    return-void
.end method

.method static synthetic a(Ljava/io/Closeable;)V
    .locals 0

    .prologue
    .line 32
    invoke-static {p0}, Laig;->b(Ljava/io/Closeable;)V

    return-void
.end method

.method private static b(Ljava/io/Closeable;)V
    .locals 3

    .prologue
    .line 189
    if-eqz p0, :cond_0

    .line 191
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 193
    const-string v1, "PartialDownloadedFileStoreImpl"

    const-string v2, "IOException thrown while closing Closeable"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public a(Ladj;)Laif;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Laii;

    iget-object v1, p0, Laig;->a:Lalo;

    invoke-direct {v0, p1, v1}, Laii;-><init>(Ladj;Lalo;)V

    return-object v0
.end method

.method public declared-synchronized a(Ljava/lang/String;)Laif;
    .locals 2

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laig;->a:LcI;

    invoke-virtual {v0, p1}, LcI;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 176
    if-eqz v0, :cond_0

    invoke-interface {v0}, Laif;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 177
    invoke-static {v0}, Laig;->b(Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    const/4 v0, 0x0

    .line 180
    :cond_0
    monitor-exit p0

    return-object v0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Laif;)V
    .locals 1

    .prologue
    .line 163
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 165
    :try_start_0
    iget v0, p0, Laig;->a:I

    if-nez v0, :cond_1

    .line 166
    invoke-static {p2}, Laig;->b(Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 168
    :cond_1
    :try_start_1
    iget-object v0, p0, Laig;->a:LcI;

    invoke-virtual {v0, p1, p2}, LcI;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Laig;->b(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
