.class public final LtL;
.super Ljava/lang/Object;
.source "FeatureChecker.java"

# interfaces
.implements LtK;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean p1, p0, LtL;->a:Z

    .line 25
    return-void
.end method


# virtual methods
.method public a(Lrg;LaFO;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, LtL;->a:Z

    return v0
.end method

.method public a(LtJ;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, LtL;->a:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 39
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ConstantResultFeatureChecker[enabled=%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v4, p0, LtL;->a:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
