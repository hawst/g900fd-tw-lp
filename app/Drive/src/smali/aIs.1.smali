.class public LaIs;
.super Ljava/lang/Object;
.source "StorageFileReadWrite.java"


# static fields
.field public static final a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;


# instance fields
.field private final a:LaIr;

.field private final a:Ladi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    sput-object v0, LaIs;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    return-void
.end method

.method public constructor <init>(Ladi;LaIr;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, LaIs;->a:Ladi;

    .line 42
    iput-object p2, p0, LaIs;->a:LaIr;

    .line 43
    return-void
.end method

.method private a(Ladj;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 49
    :try_start_0
    invoke-interface {p1}, Ladj;->a()Ljava/io/File;

    move-result-object v0

    .line 50
    new-instance v1, LaIt;

    invoke-direct {v1, p0, p1}, LaIt;-><init>(LaIs;Ladj;)V

    invoke-static {v0, p2, p3, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;ILandroid/os/Handler;Landroid/os/ParcelFileDescriptor$OnCloseListener;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    const-string v1, "StorageFileReadWrite"

    const-string v2, "Failed to open the file"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 68
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(LaGv;I)V
    .locals 3

    .prologue
    .line 80
    invoke-static {p1}, LaIs;->a(LaGv;)Z

    move-result v0

    if-nez v0, :cond_0

    const/high16 v0, 0x10000000

    if-eq p2, v0, :cond_0

    .line 81
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot open "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for writing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    return-void
.end method

.method public static a(LaGv;)Z
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, LaGv;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaGo;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 88
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LaIs;->a(LaGv;I)V

    .line 92
    sget-object v0, LaIs;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v0

    .line 93
    iget-object v1, p0, LaIs;->a:LaIr;

    invoke-virtual {v1, p1, v0}, LaIr;->a(LaGo;LacY;)LaGo;

    move-result-object v1

    .line 97
    :try_start_0
    iget-object v2, p0, LaIs;->a:Ladi;

    sget-object v3, Ladk;->a:Ladk;

    invoke-interface {v2, v1, v0, v3}, Ladi;->a(LaGo;LacY;Lamr;)LbsU;

    move-result-object v0

    .line 98
    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 106
    invoke-direct {p0, v0, p2, p3}, LaIs;->a(Ladj;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    const-string v1, "StorageFileReadWrite"

    const-string v2, "failed to obtain the file"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 101
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :catch_1
    move-exception v0

    .line 103
    const-string v1, "StorageFileReadWrite"

    const-string v2, "failed to obtain the file"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 104
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public b(LaGo;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 111
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :try_start_0
    iget-object v0, p0, LaIs;->a:Ladi;

    invoke-interface {v0, p1}, Ladi;->a(LaGo;)Ladj;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 120
    invoke-direct {p0, v0, p2, p3}, LaIs;->a(Ladj;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0

    .line 116
    :catch_0
    move-exception v0

    .line 117
    const-string v1, "StorageFileReadWrite"

    const-string v2, "failed to obtain the file"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 118
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
