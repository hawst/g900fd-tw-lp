.class public abstract LmP;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:F

.field private final a:I

.field private final a:Landroid/view/View;

.field private a:Ljava/lang/Runnable;

.field private a:Z

.field private final a:[I

.field private final b:I

.field private b:Ljava/lang/Runnable;

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LmP;->a:[I

    .line 1235
    iput-object p1, p0, LmP;->a:Landroid/view/View;

    .line 1236
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LmP;->a:F

    .line 1237
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    iput v0, p0, LmP;->a:I

    .line 1239
    iget v0, p0, LmP;->a:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LmP;->b:I

    .line 1240
    return-void
.end method

.method static synthetic a(LmP;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, LmP;->a:Landroid/view/View;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1364
    iget-object v0, p0, LmP;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1365
    iget-object v0, p0, LmP;->a:Landroid/view/View;

    iget-object v1, p0, LmP;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1368
    :cond_0
    iget-object v0, p0, LmP;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 1369
    iget-object v0, p0, LmP;->a:Landroid/view/View;

    iget-object v1, p0, LmP;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1371
    :cond_1
    return-void
.end method

.method static synthetic a(LmP;)V
    .locals 0

    .prologue
    .line 1201
    invoke-direct {p0}, LmP;->b()V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1320
    iget-object v2, p0, LmP;->a:Landroid/view/View;

    .line 1321
    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1360
    :cond_0
    :goto_0
    return v0

    .line 1325
    :cond_1
    invoke-static {p1}, LdH;->a(Landroid/view/MotionEvent;)I

    move-result v3

    .line 1326
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1328
    :pswitch_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, LmP;->c:I

    .line 1329
    iput-boolean v0, p0, LmP;->b:Z

    .line 1331
    iget-object v1, p0, LmP;->a:Ljava/lang/Runnable;

    if-nez v1, :cond_2

    .line 1332
    new-instance v1, LmQ;

    invoke-direct {v1, p0, v6}, LmQ;-><init>(LmP;LmL;)V

    iput-object v1, p0, LmP;->a:Ljava/lang/Runnable;

    .line 1334
    :cond_2
    iget-object v1, p0, LmP;->a:Ljava/lang/Runnable;

    iget v3, p0, LmP;->a:I

    int-to-long v4, v3

    invoke-virtual {v2, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1335
    iget-object v1, p0, LmP;->b:Ljava/lang/Runnable;

    if-nez v1, :cond_3

    .line 1336
    new-instance v1, LmR;

    invoke-direct {v1, p0, v6}, LmR;-><init>(LmP;LmL;)V

    iput-object v1, p0, LmP;->b:Ljava/lang/Runnable;

    .line 1338
    :cond_3
    iget-object v1, p0, LmP;->b:Ljava/lang/Runnable;

    iget v3, p0, LmP;->b:I

    int-to-long v4, v3

    invoke-virtual {v2, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1341
    :pswitch_1
    iget v3, p0, LmP;->c:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    .line 1342
    if-ltz v3, :cond_0

    .line 1343
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    .line 1344
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 1345
    iget v5, p0, LmP;->a:F

    invoke-static {v2, v4, v3, v5}, LmP;->a(Landroid/view/View;FFF)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1346
    invoke-direct {p0}, LmP;->a()V

    .line 1349
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    move v0, v1

    .line 1350
    goto :goto_0

    .line 1356
    :pswitch_2
    invoke-direct {p0}, LmP;->a()V

    goto :goto_0

    .line 1326
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/view/View;FFF)Z
    .locals 2

    .prologue
    .line 1434
    neg-float v0, p3

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    neg-float v0, p3

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1444
    iget-object v0, p0, LmP;->a:[I

    .line 1445
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1446
    const/4 v1, 0x0

    aget v1, v0, v1

    neg-int v1, v1

    int-to-float v1, v1

    aget v0, v0, v2

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v1, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1447
    return v2
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 1374
    invoke-direct {p0}, LmP;->a()V

    .line 1376
    iget-object v0, p0, LmP;->a:Landroid/view/View;

    .line 1377
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1396
    :cond_0
    :goto_0
    return-void

    .line 1381
    :cond_1
    invoke-virtual {p0}, LmP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1386
    iget-object v0, p0, LmP;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1389
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1390
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1391
    iget-object v1, p0, LmP;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1392
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1394
    iput-boolean v8, p0, LmP;->a:Z

    .line 1395
    iput-boolean v8, p0, LmP;->b:Z

    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1405
    iget-object v2, p0, LmP;->a:Landroid/view/View;

    .line 1406
    invoke-virtual {p0}, LmP;->a()LmK;

    move-result-object v3

    .line 1407
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LmK;->a()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1430
    :cond_0
    :goto_0
    return v1

    .line 1411
    :cond_1
    invoke-static {v3}, LmK;->a(LmK;)LmO;

    move-result-object v3

    .line 1412
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LmO;->isShown()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1417
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    .line 1418
    invoke-direct {p0, v2, v4}, LmP;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1419
    invoke-direct {p0, v3, v4}, LmP;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1422
    iget v2, p0, LmP;->c:I

    invoke-virtual {v3, v4, v2}, LmO;->a(Landroid/view/MotionEvent;I)Z

    move-result v3

    .line 1423
    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    .line 1426
    invoke-static {p1}, LdH;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1427
    if-eq v2, v0, :cond_2

    const/4 v4, 0x3

    if-eq v2, v4, :cond_2

    move v2, v0

    .line 1430
    :goto_1
    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1427
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1430
    goto :goto_2
.end method

.method private b(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1455
    iget-object v0, p0, LmP;->a:[I

    .line 1456
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1457
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    aget v0, v0, v2

    int-to-float v0, v0

    invoke-virtual {p2, v1, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1458
    return v2
.end method


# virtual methods
.method public abstract a()LmK;
.end method

.method protected a()Z
    .locals 2

    .prologue
    .line 1292
    invoke-virtual {p0}, LmP;->a()LmK;

    move-result-object v0

    .line 1293
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LmK;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1294
    invoke-virtual {v0}, LmK;->b()V

    .line 1296
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected b()Z
    .locals 2

    .prologue
    .line 1306
    invoke-virtual {p0}, LmP;->a()LmK;

    move-result-object v0

    .line 1307
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LmK;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1308
    invoke-virtual {v0}, LmK;->a()V

    .line 1310
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1256
    iget-boolean v10, p0, LmP;->a:Z

    .line 1258
    if-eqz v10, :cond_5

    .line 1259
    iget-boolean v0, p0, LmP;->b:Z

    if-eqz v0, :cond_2

    .line 1263
    invoke-direct {p0, p2}, LmP;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1280
    :goto_0
    iput-boolean v0, p0, LmP;->a:Z

    .line 1281
    if-nez v0, :cond_0

    if-eqz v10, :cond_1

    :cond_0
    move v7, v8

    :cond_1
    return v7

    .line 1265
    :cond_2
    invoke-direct {p0, p2}, LmP;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, LmP;->b()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v8

    goto :goto_0

    :cond_4
    move v0, v7

    goto :goto_0

    .line 1268
    :cond_5
    invoke-direct {p0, p2}, LmP;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, LmP;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    move v9, v8

    .line 1270
    :goto_1
    if-eqz v9, :cond_6

    .line 1272
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1273
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1275
    iget-object v1, p0, LmP;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1276
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_6
    move v0, v9

    goto :goto_0

    :cond_7
    move v9, v7

    .line 1268
    goto :goto_1
.end method
