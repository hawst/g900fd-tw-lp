.class public final enum Lbhc;
.super Ljava/lang/Enum;
.source "PdfCreator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbhc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbhc;

.field private static final synthetic a:[Lbhc;

.field public static final enum b:Lbhc;

.field public static final enum c:Lbhc;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lbhc;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v2}, Lbhc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhc;->a:Lbhc;

    .line 35
    new-instance v0, Lbhc;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v3}, Lbhc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhc;->b:Lbhc;

    .line 36
    new-instance v0, Lbhc;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v4}, Lbhc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhc;->c:Lbhc;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lbhc;

    sget-object v1, Lbhc;->a:Lbhc;

    aput-object v1, v0, v2

    sget-object v1, Lbhc;->b:Lbhc;

    aput-object v1, v0, v3

    sget-object v1, Lbhc;->c:Lbhc;

    aput-object v1, v0, v4

    sput-object v0, Lbhc;->a:[Lbhc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbhc;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lbhc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbhc;

    return-object v0
.end method

.method public static values()[Lbhc;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lbhc;->a:[Lbhc;

    invoke-virtual {v0}, [Lbhc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbhc;

    return-object v0
.end method
