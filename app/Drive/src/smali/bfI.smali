.class public final LbfI;
.super Ljava/lang/Object;
.source "LoggingStreamingContent.java"

# interfaces
.implements LbfR;


# instance fields
.field private final a:I

.field private final a:LbfR;

.field private final a:Ljava/util/logging/Level;

.field private final a:Ljava/util/logging/Logger;


# direct methods
.method public constructor <init>(LbfR;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, LbfI;->a:LbfR;

    .line 56
    iput-object p2, p0, LbfI;->a:Ljava/util/logging/Logger;

    .line 57
    iput-object p3, p0, LbfI;->a:Ljava/util/logging/Level;

    .line 58
    iput p4, p0, LbfI;->a:I

    .line 59
    return-void
.end method


# virtual methods
.method public a(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 62
    new-instance v1, LbfH;

    iget-object v0, p0, LbfI;->a:Ljava/util/logging/Logger;

    iget-object v2, p0, LbfI;->a:Ljava/util/logging/Level;

    iget v3, p0, LbfI;->a:I

    invoke-direct {v1, p1, v0, v2, v3}, LbfH;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    .line 65
    :try_start_0
    iget-object v0, p0, LbfI;->a:LbfR;

    invoke-interface {v0, v1}, LbfR;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    invoke-virtual {v1}, LbfH;->a()LbfF;

    move-result-object v0

    invoke-virtual {v0}, LbfF;->close()V

    .line 70
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 71
    return-void

    .line 68
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LbfH;->a()LbfF;

    move-result-object v1

    invoke-virtual {v1}, LbfF;->close()V

    throw v0
.end method
