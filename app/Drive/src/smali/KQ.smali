.class public abstract LKQ;
.super Ljava/lang/Object;
.source "MenuFunctionViewFactoryBase.java"

# interfaces
.implements LKP;


# instance fields
.field private final a:LLw;

.field private a:Z

.field private b:Z

.field private final c:Z


# direct methods
.method public constructor <init>(LLw;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean v0, p0, LKQ;->a:Z

    .line 15
    iput-boolean v0, p0, LKQ;->b:Z

    .line 22
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLw;

    iput-object v0, p0, LKQ;->a:LLw;

    .line 23
    iput-boolean p2, p0, LKQ;->c:Z

    .line 24
    return-void
.end method


# virtual methods
.method public a()LLw;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LKQ;->a:LLw;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, LKQ;->a:Z

    return v0
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, LKQ;->a:LLw;

    invoke-interface {v0, p1, p2}, LLw;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v0

    iput-boolean v0, p0, LKQ;->b:Z

    .line 30
    iget-boolean v0, p0, LKQ;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LKQ;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LKQ;->a:Z

    .line 31
    iget-boolean v0, p0, LKQ;->a:Z

    return v0

    .line 30
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, LKQ;->b:Z

    return v0
.end method
