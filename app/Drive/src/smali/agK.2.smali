.class LagK;
.super Ljava/lang/Object;
.source "GdataResultsPage.java"

# interfaces
.implements LaKa;


# instance fields
.field private final a:I

.field private final a:Lbxc;

.field private final a:Lbxj;

.field private final a:Ljava/lang/Exception;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbxj;Lbxc;Ljava/lang/Exception;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LagK;->a:Lbxj;

    .line 37
    iput-object p3, p0, LagK;->a:Ljava/lang/Exception;

    .line 38
    iput-object p2, p0, LagK;->a:Lbxc;

    .line 39
    iput p5, p0, LagK;->a:I

    .line 40
    iput-object p4, p0, LagK;->a:Ljava/lang/String;

    .line 41
    iput-object p6, p0, LagK;->b:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, LagK;->a:I

    return v0
.end method

.method public a()Lbxc;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, LagK;->a:Lbxc;

    return-object v0
.end method

.method public a()Lbxj;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, LagK;->a:Lbxj;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LagK;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LaJT;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 55
    :goto_0
    iget-object v0, p0, LagK;->a:Lbxj;

    invoke-interface {v0}, Lbxj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    invoke-static {}, LpD;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 61
    :goto_1
    return-object v0

    .line 59
    :cond_0
    iget-object v0, p0, LagK;->a:Lbxj;

    invoke-interface {v0, v1}, Lbxj;->a(Lbxb;)Lbxb;

    move-result-object v0

    check-cast v0, LaJT;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 61
    goto :goto_1
.end method

.method public a()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, LagK;->a:Lbxj;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, LagK;->a:Lbxj;

    invoke-interface {v0}, Lbxj;->a()V

    .line 99
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LagK;->a:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, LagK;->a:Ljava/lang/Exception;

    throw v0

    .line 49
    :cond_0
    iget-object v0, p0, LagK;->a:Lbxc;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LagK;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LagK;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " status "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LagK;->a:Lbxc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
