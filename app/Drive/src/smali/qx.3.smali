.class public enum Lqx;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lqx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lqx;

.field private static final synthetic a:[Lqx;

.field public static final enum b:Lqx;

.field public static final enum c:Lqx;

.field public static final enum d:Lqx;

.field public static final enum e:Lqx;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 185
    new-instance v0, Lqx;

    const-string v1, "USER"

    const-string v2, "user"

    invoke-direct {v0, v1, v3, v2}, Lqx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lqx;->a:Lqx;

    .line 186
    new-instance v0, Lqx;

    const-string v1, "GROUP"

    const-string v2, "group"

    invoke-direct {v0, v1, v4, v2}, Lqx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lqx;->b:Lqx;

    .line 187
    new-instance v0, Lqx;

    const-string v1, "DOMAIN"

    const-string v2, "domain"

    invoke-direct {v0, v1, v5, v2}, Lqx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lqx;->c:Lqx;

    .line 188
    new-instance v0, Lqx;

    const-string v1, "DEFAULT"

    const-string v2, "default"

    invoke-direct {v0, v1, v6, v2}, Lqx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lqx;->d:Lqx;

    .line 189
    new-instance v0, Lqy;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v7, v2}, Lqy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lqx;->e:Lqx;

    .line 184
    const/4 v0, 0x5

    new-array v0, v0, [Lqx;

    sget-object v1, Lqx;->a:Lqx;

    aput-object v1, v0, v3

    sget-object v1, Lqx;->b:Lqx;

    aput-object v1, v0, v4

    sget-object v1, Lqx;->c:Lqx;

    aput-object v1, v0, v5

    sget-object v1, Lqx;->d:Lqx;

    aput-object v1, v0, v6

    sget-object v1, Lqx;->e:Lqx;

    aput-object v1, v0, v7

    sput-object v0, Lqx;->a:[Lqx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 198
    iput-object p3, p0, Lqx;->a:Ljava/lang/String;

    .line 199
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Lqp;)V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0, p1, p2, p3}, Lqx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lqx;
    .locals 6

    .prologue
    .line 214
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 215
    invoke-static {}, Lqx;->values()[Lqx;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 216
    iget-object v5, v0, Lqx;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 220
    :goto_1
    return-object v0

    .line 215
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 220
    :cond_1
    sget-object v0, Lqx;->e:Lqx;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lqx;
    .locals 1

    .prologue
    .line 184
    const-class v0, Lqx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lqx;

    return-object v0
.end method

.method public static values()[Lqx;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lqx;->a:[Lqx;

    invoke-virtual {v0}, [Lqx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lqx;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lqx;->a:Ljava/lang/String;

    return-object v0
.end method
