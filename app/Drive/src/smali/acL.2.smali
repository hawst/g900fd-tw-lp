.class public LacL;
.super Landroid/widget/ArrayAdapter;
.source "SingleTokenContactFilterAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Labs;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LabI;

.field private a:Landroid/widget/Filter;

.field private final a:Ljava/lang/String;

.field private final a:[LacN;


# direct methods
.method public constructor <init>(Landroid/content/Context;[LabD;LabI;)V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LacL;-><init>(Landroid/content/Context;[LabD;Ljava/lang/String;LabI;)V

    .line 184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[LabD;Ljava/lang/String;LabI;)V
    .locals 14

    .prologue
    .line 202
    sget v2, Lxe;->add_collaborator_list_item:I

    sget v3, Lxc;->sharee_name:I

    invoke-direct {p0, p1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 204
    if-eqz p3, :cond_1

    :goto_0
    move-object/from16 v0, p3

    iput-object v0, p0, LacL;->a:Ljava/lang/String;

    .line 205
    move-object/from16 v0, p4

    iput-object v0, p0, LacL;->a:LabI;

    .line 208
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 209
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 210
    move-object/from16 v0, p2

    array-length v6, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_0

    aget-object v7, p2, v3

    .line 212
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 234
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Labs;

    .line 235
    invoke-virtual {p0, v2}, LacL;->add(Ljava/lang/Object;)V

    goto :goto_2

    .line 204
    :cond_1
    const-string p3, ""

    goto :goto_0

    .line 216
    :cond_2
    invoke-interface {v7}, LabD;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v8, " "

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    .line 217
    invoke-interface {v7}, LabD;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 218
    new-instance v10, Labs;

    .line 219
    invoke-interface {v7}, LabD;->b()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v7}, LabD;->b()J

    move-result-wide v12

    invoke-direct {v10, v11, v2, v12, v13}, Labs;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 220
    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 226
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 228
    new-instance v12, LacN;

    invoke-direct {v12, v2, v10}, LacN;-><init>(Ljava/lang/String;Labs;)V

    .line 229
    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 210
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 239
    :cond_5
    const/4 v2, 0x0

    new-array v2, v2, [LacN;

    invoke-interface {v4, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [LacN;

    iput-object v2, p0, LacL;->a:[LacN;

    .line 240
    iget-object v2, p0, LacL;->a:[LacN;

    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 241
    return-void
.end method

.method static synthetic a(LacL;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LacL;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LacL;)[LacN;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LacL;->a:[LacN;

    return-object v0
.end method


# virtual methods
.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, LacL;->a:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 246
    new-instance v0, LacO;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LacO;-><init>(LacL;LacM;)V

    iput-object v0, p0, LacL;->a:Landroid/widget/Filter;

    .line 248
    :cond_0
    iget-object v0, p0, LacL;->a:Landroid/widget/Filter;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 253
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 255
    sget v0, Lxc;->sharee_name:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 256
    sget v1, Lxc;->sharee_description:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 257
    sget v2, Lxc;->sharee_badge:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 259
    invoke-virtual {p0, p1}, LacL;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Labs;

    .line 260
    invoke-virtual {v3}, Labs;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    invoke-virtual {v3}, Labs;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v0, p0, LacL;->a:LabI;

    invoke-virtual {v3}, Labs;->a()J

    move-result-wide v6

    invoke-virtual {v0, v2, v6, v7}, LabI;->a(Landroid/widget/ImageView;J)V

    .line 264
    return-object v4
.end method
