.class public Luf;
.super Ljava/lang/Object;
.source "KixZipFileExposer.java"

# interfaces
.implements LvB;


# instance fields
.field private final a:Laeb;

.field private final a:Lalo;

.field private final a:Lamn;


# direct methods
.method public constructor <init>(Lalo;Laeb;Lamn;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Luf;->a:Lalo;

    .line 39
    iput-object p2, p0, Luf;->a:Laeb;

    .line 40
    iput-object p3, p0, Luf;->a:Lamn;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Ljava/io/File;LaGo;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    new-instance v0, Laeg;

    iget-object v1, p0, Luf;->a:Lalo;

    const-string v2, "index.html"

    invoke-direct {v0, v1, p1, v2}, Laeg;-><init>(Lalo;Ljava/io/File;Ljava/lang/String;)V

    .line 47
    iget-object v1, p0, Luf;->a:Laeb;

    iget-object v2, p0, Luf;->a:Lamn;

    invoke-static {v1, v2, v0}, LadD;->a(Laeb;Lamn;LadJ;)LadA;

    move-result-object v0

    .line 48
    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(LadA;)Landroid/net/Uri;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "index.html"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method public a(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 55
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-static {p1}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Landroid/net/Uri;)V

    .line 57
    return-void
.end method
