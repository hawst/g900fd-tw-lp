.class public LatD;
.super Ljava/lang/Object;
.source "WelcomeModule.java"

# interfaces
.implements LbuC;


# instance fields
.field private final a:Lasa;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x1

    .line 27
    invoke-static {}, Lbnb;->a()Lbnb;

    move-result-object v1

    invoke-static {v0, v1}, Lasa;->a(ILbpT;)Lasa;

    move-result-object v0

    iput-object v0, p0, LatD;->a:Lasa;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 66
    const-class v0, Lasa;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, LbvX;->a()LbuT;

    move-result-object v0

    iget-object v1, p0, LatD;->a:Lasa;

    invoke-interface {v0, v1}, LbuT;->a(Ljava/lang/Object;)V

    .line 68
    const-class v0, Lasb;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, LbvX;->a()LbuT;

    move-result-object v0

    const-class v1, Lasc;

    invoke-interface {v0, v1}, LbuT;->a(Ljava/lang/Class;)LbuU;

    .line 70
    const-class v0, Lati;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, LbvX;->a()LbuT;

    move-result-object v0

    const-class v1, LarT;

    invoke-interface {v0, v1}, LbuT;->a(Ljava/lang/Class;)LbuU;

    move-result-object v0

    const-class v1, Lbxz;

    invoke-interface {v0, v1}, LbuU;->a(Ljava/lang/Class;)V

    .line 72
    return-void
.end method

.method provideCatalog(Lase;)Latj;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 47
    return-object p1
.end method

.method provideHighlightFeaturesConfig()Lasa;
    .locals 1
    .annotation runtime LQN;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, LatD;->a:Lasa;

    return-object v0
.end method

.method provideHighlightsLauncher(Lasc;)Lasb;
    .locals 0
    .annotation runtime LQN;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 61
    return-object p1
.end method

.method provideRedeemVoucherFlow(Landroid/content/Context;)LasI;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasI;

    return-object v0
.end method

.method provideVoucherService(LasZ;)LasX;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 40
    return-object p1
.end method
