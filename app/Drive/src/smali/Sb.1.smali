.class LSb;
.super Landroid/os/AsyncTask;
.source "StorageDisplayImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LajW;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LRZ;


# direct methods
.method constructor <init>(LRZ;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, LSb;->a:LRZ;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)LajW;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, LSb;->a:LRZ;

    .line 139
    invoke-static {v0}, LRZ;->a(LRZ;)LajO;

    move-result-object v0

    iget-object v1, p0, LSb;->a:LRZ;

    invoke-static {v1}, LRZ;->a(LRZ;)LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LajO;->a(LaFO;)LajN;

    move-result-object v0

    .line 141
    new-instance v1, LajW;

    invoke-direct {v1, v0}, LajW;-><init>(LajN;)V

    return-object v1
.end method

.method protected a(LajW;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 146
    sget-object v0, LaJS;->b:LaJS;

    invoke-virtual {p1}, LajW;->a()LaJS;

    move-result-object v1

    invoke-virtual {v0, v1}, LaJS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, LSb;->a:LRZ;

    invoke-static {v0}, LRZ;->a(LRZ;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->nav_panel_storage_summary_unlimited:I

    new-array v2, v7, [Ljava/lang/Object;

    .line 148
    invoke-virtual {p1}, LajW;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 147
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p0, LSb;->a:LRZ;

    invoke-static {v1}, LRZ;->a(LRZ;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    iget-object v1, p0, LSb;->a:LRZ;

    invoke-static {v1}, LRZ;->b(LRZ;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v1, p0, LSb;->a:LRZ;

    invoke-static {v1}, LRZ;->a(LRZ;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, LSb;->a:LRZ;

    invoke-static {v0}, LRZ;->a(LRZ;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->nav_panel_storage_percent:I

    new-array v2, v7, [Ljava/lang/Object;

    .line 155
    invoke-virtual {p1}, LajW;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    .line 154
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 156
    iget-object v1, p0, LSb;->a:LRZ;

    invoke-static {v1}, LRZ;->a(LRZ;)Landroid/content/Context;

    move-result-object v1

    sget v2, Lxi;->nav_panel_storage_summary:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 157
    invoke-virtual {p1}, LajW;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 158
    invoke-virtual {p1}, LajW;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 156
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 160
    iget-object v2, p0, LSb;->a:LRZ;

    invoke-static {v2}, LRZ;->b(LRZ;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v2, p0, LSb;->a:LRZ;

    invoke-static {v2}, LRZ;->a(LRZ;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v2, p0, LSb;->a:LRZ;

    invoke-static {v2}, LRZ;->a(LRZ;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v2, p0, LSb;->a:LRZ;

    invoke-static {v2}, LRZ;->a(LRZ;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 135
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LSb;->a([Ljava/lang/Void;)LajW;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 135
    check-cast p1, LajW;

    invoke-virtual {p0, p1}, LSb;->a(LajW;)V

    return-void
.end method
