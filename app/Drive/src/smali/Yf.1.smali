.class LYf;
.super Ljava/lang/Object;
.source "PunchStateSvgViewer.java"

# interfaces
.implements LeT;


# instance fields
.field final synthetic a:LYc;


# direct methods
.method constructor <init>(LYc;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, LYf;->a:LYc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 291
    return-void
.end method

.method public a(IFI)V
    .locals 0

    .prologue
    .line 295
    return-void
.end method

.method public b(I)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 299
    iget-object v0, p0, LYf;->a:LYc;

    iget-object v0, v0, LYc;->a:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    if-nez p1, :cond_0

    .line 310
    iget-object v0, p0, LYf;->a:LYc;

    iget-object v0, v0, LYc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v1

    .line 311
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0, v1}, LYc;->a(LYc;I)I

    move-result v0

    .line 312
    iget-object v2, p0, LYf;->a:LYc;

    iget-object v2, v2, LYc;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Paging into viewPagerPosition %s (stateArrayIndex: %s). SVGs currently shown: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LYf;->a:LYc;

    .line 313
    invoke-static {v4}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 312
    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 315
    if-eq v0, v7, :cond_0

    .line 321
    if-nez v0, :cond_5

    .line 323
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v0, v8, :cond_2

    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_3

    .line 324
    :cond_2
    iget-object v0, p0, LYf;->a:LYc;

    iget-object v0, v0, LYc;->a:Ljava/lang/String;

    const-string v2, "Cannot page into position %s. SVG is blank."

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 329
    :cond_3
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)[Z

    move-result-object v0

    aput-boolean v6, v0, v6

    .line 330
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)V

    .line 333
    new-array v0, v9, [Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    aput-object v10, v0, v6

    .line 334
    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v1

    aget-object v1, v1, v6

    aput-object v1, v0, v7

    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v1

    aget-object v1, v1, v7

    aput-object v1, v0, v8

    .line 333
    invoke-static {v0}, LYc;->a([Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 335
    iget-object v0, p0, LYf;->a:LYc;

    iget-object v0, v0, LYc;->a:LXR;

    invoke-interface {v0}, LXR;->d()V

    .line 355
    :cond_4
    :goto_1
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->c(LYc;)V

    .line 357
    iget-object v0, p0, LYf;->a:LYc;

    iget-object v0, v0, LYc;->a:Ljava/lang/String;

    const-string v1, "Resetting ViewPager to middle item (current state)"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    iget-object v0, p0, LYf;->a:LYc;

    invoke-virtual {v0, v7, v6}, LYc;->a(IZ)V

    .line 359
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v1

    aget-object v1, v1, v7

    iput-object v1, v0, LYc;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    goto/16 :goto_0

    .line 336
    :cond_5
    if-ne v0, v8, :cond_4

    .line 338
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v0, v8, :cond_6

    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, LYf;->a:LYc;

    invoke-static {v2}, LYc;->a(LYc;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v8, :cond_7

    .line 339
    :cond_6
    iget-object v0, p0, LYf;->a:LYc;

    iget-object v0, v0, LYc;->a:Ljava/lang/String;

    const-string v2, "Cannot page into position %s. SVG is blank."

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 344
    :cond_7
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)[Z

    move-result-object v0

    aput-boolean v6, v0, v8

    .line 345
    iget-object v0, p0, LYf;->a:LYc;

    invoke-static {v0}, LYc;->a(LYc;)V

    .line 348
    new-array v0, v9, [Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 349
    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v1

    aget-object v1, v1, v7

    aput-object v1, v0, v6

    invoke-static {}, LYc;->a()[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v1

    aget-object v1, v1, v8

    aput-object v1, v0, v7

    aput-object v10, v0, v8

    .line 348
    invoke-static {v0}, LYc;->a([Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)[Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 350
    iget-object v0, p0, LYf;->a:LYc;

    iget-object v0, v0, LYc;->a:LXR;

    invoke-interface {v0}, LXR;->c()V

    goto/16 :goto_1
.end method
