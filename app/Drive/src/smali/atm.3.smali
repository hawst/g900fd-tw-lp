.class Latm;
.super Ljava/lang/Object;
.source "WelcomeController.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    iput-object p1, p0, Latm;->a:Ljava/lang/String;

    .line 256
    iput-object p2, p0, Latm;->b:Ljava/lang/String;

    .line 257
    return-void
.end method

.method public static a(Ljava/lang/String;)Latm;
    .locals 4

    .prologue
    .line 266
    :try_start_0
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    invoke-virtual {v0, p0}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v3

    .line 267
    new-instance v0, Latm;

    const-string v1, "catalogURL"

    .line 268
    invoke-virtual {v3, v1}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "catalogURL"

    invoke-virtual {v3, v1}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->a()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_0
    const-string v1, "currentAnnounce"

    .line 269
    invoke-virtual {v3, v1}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "currentAnnounce"

    invoke-virtual {v3, v1}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->a()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-direct {v0, v2, v1}, Latm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :goto_2
    return-object v0

    .line 268
    :cond_0
    const-string v1, ""

    move-object v2, v1

    goto :goto_0

    .line 269
    :cond_1
    const-string v1, ""
    :try_end_0
    .catch Lbto; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    const-string v1, "WelcomeController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Corrupted CurrentStories json : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 273
    new-instance v0, Latm;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Latm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic a(Latm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Latm;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Latm;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Latm;->a:Ljava/lang/String;

    return-object p1
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Latm;->b:Ljava/lang/String;

    invoke-static {v0}, Lbju;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Latm;)Z
    .locals 1

    .prologue
    .line 246
    invoke-direct {p0}, Latm;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Latm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Latm;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Latm;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Latm;->b:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 279
    new-instance v0, Lbtk;

    invoke-direct {v0}, Lbtk;-><init>()V

    .line 280
    const-string v1, "catalogURL"

    iget-object v2, p0, Latm;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbtk;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v1, "currentAnnounce"

    iget-object v2, p0, Latm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbtk;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-virtual {v0}, Lbtk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CurrentStories: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Latm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
