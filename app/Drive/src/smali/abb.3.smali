.class public Labb;
.super Ljava/lang/Object;
.source "UploadSharedItemActivityDelegate.java"


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1145
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Labb;->a:Landroid/content/Intent;

    .line 1148
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1149
    return-void
.end method


# virtual methods
.method public a()Labb;
    .locals 3

    .prologue
    .line 1238
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "forceFileCopy"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1239
    return-object p0
.end method

.method public a(LaFO;)Labb;
    .locals 3

    .prologue
    .line 1221
    if-eqz p1, :cond_0

    .line 1222
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1224
    :cond_0
    return-object p0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;)Labb;
    .locals 2

    .prologue
    .line 1177
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1178
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1179
    return-object p0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Labb;
    .locals 2

    .prologue
    .line 1228
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1229
    return-object p0
.end method

.method public a(Ljava/lang/String;)Labb;
    .locals 2

    .prologue
    .line 1211
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1212
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Labb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Labb;"
        }
    .end annotation

    .prologue
    .line 1187
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 1189
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1190
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1191
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1192
    return-object p0

    .line 1187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)Labb;
    .locals 2

    .prologue
    .line 1196
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "showConversionOption"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1197
    return-object p0
.end method

.method public a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1243
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Labb;->a:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method public b(Z)Labb;
    .locals 2

    .prologue
    .line 1216
    iget-object v0, p0, Labb;->a:Landroid/content/Intent;

    const-string v1, "evaluateBeforeUploading"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1217
    return-object p0
.end method
