.class public Lain;
.super Ljava/lang/Object;
.source "FullFeedSyncAlgorithm.java"

# interfaces
.implements Laiq;


# static fields
.field static final a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;


# instance fields
.field private final a:I

.field private final a:LaFM;

.field private a:LaGW;

.field private final a:LaGg;

.field private final a:LagE;

.field private final a:Lagl;

.field private final a:Z

.field private final b:I

.field private b:LaGW;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 49
    sget-object v0, LaFL;->a:LaFL;

    sget-object v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v3, 0x0

    sget-object v4, LaES;->y:LaES;

    .line 50
    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    sput-object v0, Lain;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 49
    return-void
.end method

.method public constructor <init>(LaFM;Lagl;LaGg;LagE;IZI)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lain;->c:I

    .line 95
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFM;

    iput-object v0, p0, Lain;->a:LaFM;

    .line 96
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p0, Lain;->a:LaGg;

    .line 97
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagE;

    iput-object v0, p0, Lain;->a:LagE;

    .line 98
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagl;

    iput-object v0, p0, Lain;->a:Lagl;

    .line 99
    iput p5, p0, Lain;->a:I

    .line 100
    iput-boolean p6, p0, Lain;->a:Z

    .line 101
    iput p7, p0, Lain;->b:I

    .line 102
    return-void
.end method

.method private a(Lagx;Ljava/lang/String;LaFO;Lagy;)LaGW;
    .locals 8

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 139
    iget-object v2, p0, Lain;->a:LaGg;

    iget-object v3, p0, Lain;->a:LaFM;

    invoke-interface {v2, v3, p2, v0, v1}, LaGg;->a(LaFM;Ljava/lang/String;J)LaGW;

    move-result-object v2

    .line 142
    invoke-virtual {v2}, LaGW;->a()Ljava/lang/Long;

    move-result-object v3

    .line 143
    if-eqz v3, :cond_0

    .line 144
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 145
    :cond_0
    iget-object v3, p0, Lain;->a:LagE;

    .line 146
    invoke-interface {v3, p4, v0, v1}, LagE;->a(Lagy;J)Lahp;

    move-result-object v0

    .line 148
    new-instance v1, Laio;

    invoke-direct {v1, v2, v0}, Laio;-><init>(LaGW;Lahp;)V

    .line 151
    iget v0, p0, Lain;->a:I

    int-to-long v4, v0

    invoke-virtual {v2}, LaGW;->a()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-int v0, v4

    .line 152
    invoke-virtual {v2}, LaGW;->b()Ljava/lang/String;

    move-result-object v3

    .line 153
    if-lez v0, :cond_1

    if-eqz v3, :cond_1

    .line 154
    invoke-interface {p1, v3, p3, v1, v0}, Lagx;->a(Ljava/lang/String;LaFO;Lagy;I)V

    .line 155
    iget v0, p0, Lain;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lain;->c:I

    .line 157
    :cond_1
    return-object v2
.end method

.method private a(Landroid/content/SyncResult;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 205
    iget-object v1, p0, Lain;->a:LaGg;

    invoke-interface {v1}, LaGg;->a()V

    .line 217
    :try_start_0
    iget-object v1, p0, Lain;->a:LaGg;

    iget-object v2, p0, Lain;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-interface {v1, v2}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v2

    .line 219
    iget-object v1, p0, Lain;->a:Lagl;

    iget-object v3, p0, Lain;->a:LaFM;

    .line 220
    invoke-virtual {v2}, LaFQ;->a()J

    move-result-wide v4

    invoke-interface {v1, v3, v4, v5}, Lagl;->a(LaFM;J)I

    move-result v1

    .line 221
    if-eqz p1, :cond_0

    .line 222
    iget-object v3, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numDeletes:J

    int-to-long v6, v1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numDeletes:J

    .line 225
    :cond_0
    invoke-virtual {p0}, Lain;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 231
    iget-object v1, p0, Lain;->a:LaGW;

    .line 232
    invoke-virtual {v1}, LaGW;->a()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/util/Date;

    iget-object v3, p0, Lain;->a:LaGW;

    invoke-virtual {v3}, LaGW;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 233
    :goto_0
    iget-object v3, p0, Lain;->b:LaGW;

    .line 234
    invoke-virtual {v3}, LaGW;->a()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v0, Ljava/util/Date;

    iget-object v3, p0, Lain;->b:LaGW;

    invoke-virtual {v3}, LaGW;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 235
    :cond_1
    invoke-virtual {v2, v1}, LaFQ;->c(Ljava/util/Date;)V

    .line 236
    invoke-virtual {v2, v0}, LaFQ;->b(Ljava/util/Date;)V

    .line 241
    :goto_1
    invoke-virtual {v2}, LaFQ;->e()V

    .line 242
    iget-object v0, p0, Lain;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    iget-object v0, p0, Lain;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 246
    return-void

    :cond_2
    move-object v1, v0

    .line 232
    goto :goto_0

    .line 238
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v2, v0}, LaFQ;->c(Ljava/util/Date;)V

    .line 239
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, LaFQ;->b(Ljava/util/Date;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 244
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lain;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method


# virtual methods
.method public a(Lagx;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 162
    iget-object v0, p0, Lain;->a:LagE;

    iget-object v1, p0, Lain;->a:Lagl;

    iget-object v2, p0, Lain;->a:LaFM;

    .line 163
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v1, p2, v2, v3}, LagE;->a(Lagl;Landroid/content/SyncResult;LaFM;Ljava/lang/Boolean;)Lagy;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lain;->a:LaFM;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    .line 167
    iget-boolean v2, p0, Lain;->a:Z

    if-eqz v2, :cond_0

    .line 168
    iget-object v2, p0, Lain;->a:LaGg;

    iget-object v3, p0, Lain;->a:LaFM;

    invoke-interface {v2, v3}, LaGg;->a(LaFM;)I

    .line 171
    :cond_0
    const-string v2, "https://docs.google.com/feeds/default/private/full?showdeleted=true&showroot=true"

    invoke-direct {p0, p1, v2, v1, v0}, Lain;->a(Lagx;Ljava/lang/String;LaFO;Lagy;)LaGW;

    move-result-object v2

    iput-object v2, p0, Lain;->a:LaGW;

    .line 172
    const-string v2, "https://docs.google.com/feeds/default/private/full/-/folder?showdeleted=true&showroot=true"

    invoke-direct {p0, p1, v2, v1, v0}, Lain;->a(Lagx;Ljava/lang/String;LaFO;Lagy;)LaGW;

    move-result-object v0

    iput-object v0, p0, Lain;->b:LaGW;

    .line 175
    iget-object v0, p0, Lain;->a:LaGW;

    invoke-virtual {v0}, LaGW;->a()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lain;->b:LaGW;

    .line 176
    invoke-virtual {v0}, LaGW;->a()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lain;->a:Z

    if-eqz v0, :cond_3

    .line 179
    :cond_2
    new-instance v0, Ljava/util/Date;

    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 180
    iget-object v1, p0, Lain;->a:LaGg;

    invoke-interface {v1}, LaGg;->a()V

    .line 182
    :try_start_0
    iget-object v1, p0, Lain;->a:LaGg;

    iget-object v2, p0, Lain;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-interface {v1, v2}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v1

    .line 183
    invoke-virtual {v1, v0}, LaFQ;->c(Ljava/util/Date;)V

    .line 184
    invoke-virtual {v1, v0}, LaFQ;->b(Ljava/util/Date;)V

    .line 185
    iget-object v0, p0, Lain;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LaFQ;->a(J)V

    .line 187
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LaFQ;->a(Z)V

    .line 188
    iget v0, p0, Lain;->b:I

    invoke-virtual {v1, v0}, LaFQ;->a(I)V

    .line 190
    invoke-virtual {v1}, LaFQ;->e()V

    .line 191
    iget-object v0, p0, Lain;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    iget-object v0, p0, Lain;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 197
    :cond_3
    iget v0, p0, Lain;->c:I

    if-nez v0, :cond_4

    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lain;->a(Landroid/content/SyncResult;)V

    .line 200
    :cond_4
    return-void

    .line 193
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lain;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method

.method public a(Landroid/content/SyncResult;Z)V
    .locals 0

    .prologue
    .line 255
    if-eqz p2, :cond_0

    .line 256
    invoke-direct {p0, p1}, Lain;->a(Landroid/content/SyncResult;)V

    .line 258
    :cond_0
    return-void
.end method

.method a()Z
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lain;->a:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
