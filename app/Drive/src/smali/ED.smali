.class public LED;
.super Ljava/lang/Object;
.source "DialogModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method provideEditTitleDialogFragmentListener(Landroid/content/Context;)LFa;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 58
    const-class v0, LFa;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFa;

    return-object v0
.end method

.method provideOnAccountPickedListener(Landroid/content/Context;)LFq;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 33
    const-class v0, LFq;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFq;

    return-object v0
.end method

.method provideOnFilterSelectedListener(Landroid/content/Context;)LFc;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 52
    const-class v0, LFc;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFc;

    return-object v0
.end method

.method provideOnRetryButtonClickedListener(Landroid/content/Context;)LET;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 40
    const-class v0, LET;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LET;

    return-object v0
.end method

.method provideSortSelectionListener(Landroid/content/Context;)LFE;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 46
    const-class v0, LFE;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFE;

    return-object v0
.end method

.method provideStartupDialogListener(Landroid/content/Context;)LFi;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 26
    const-class v0, LFi;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFi;

    return-object v0
.end method
