.class public LaUY;
.super LaLr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaLr",
        "<",
        "LaUV;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LaUY;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.wallet.dynamite.WalletDynamiteCreatorImpl"

    invoke-direct {p0, v0}, LaLr;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;LaLj;Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;LaUS;)LaUP;
    .locals 2

    invoke-static {p0}, LaCJ;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, LaCH;

    invoke-direct {v1, v0}, LaCH;-><init>(I)V

    throw v1

    :cond_0
    :try_start_0
    invoke-static {}, LaUY;->a()LaUY;

    move-result-object v0

    invoke-virtual {v0, p0}, LaUY;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaUV;

    invoke-static {p0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2, p3}, LaUV;->a(LaLm;LaLj;Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;LaUS;)LaUP;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaLs; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a()LaUY;
    .locals 1

    sget-object v0, LaUY;->a:LaUY;

    if-nez v0, :cond_0

    new-instance v0, LaUY;

    invoke-direct {v0}, LaUY;-><init>()V

    sput-object v0, LaUY;->a:LaUY;

    :cond_0
    sget-object v0, LaUY;->a:LaUY;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)LaUV;
    .locals 1

    invoke-static {p1}, LaUW;->a(Landroid/os/IBinder;)LaUV;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, LaUY;->a(Landroid/os/IBinder;)LaUV;

    move-result-object v0

    return-object v0
.end method
