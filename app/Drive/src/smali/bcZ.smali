.class public final enum LbcZ;
.super Ljava/lang/Enum;
.source "LoginData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbcZ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbcZ;

.field private static final synthetic a:[LbcZ;

.field public static final enum b:LbcZ;

.field public static final enum c:LbcZ;

.field public static final enum d:LbcZ;

.field public static final enum e:LbcZ;

.field public static final enum f:LbcZ;

.field public static final enum g:LbcZ;

.field public static final enum h:LbcZ;

.field public static final enum i:LbcZ;

.field public static final enum j:LbcZ;

.field public static final enum k:LbcZ;

.field public static final enum l:LbcZ;

.field public static final enum m:LbcZ;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, LbcZ;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->a:LbcZ;

    .line 31
    new-instance v0, LbcZ;

    const-string v1, "ACCOUNT_DISABLED"

    invoke-direct {v0, v1, v4}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->b:LbcZ;

    .line 32
    new-instance v0, LbcZ;

    const-string v1, "BAD_USERNAME"

    invoke-direct {v0, v1, v5}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->c:LbcZ;

    .line 33
    new-instance v0, LbcZ;

    const-string v1, "BAD_REQUEST"

    invoke-direct {v0, v1, v6}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->d:LbcZ;

    .line 34
    new-instance v0, LbcZ;

    const-string v1, "LOGIN_FAIL"

    invoke-direct {v0, v1, v7}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->e:LbcZ;

    .line 35
    new-instance v0, LbcZ;

    const-string v1, "SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->f:LbcZ;

    .line 36
    new-instance v0, LbcZ;

    const-string v1, "MISSING_APPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->g:LbcZ;

    .line 37
    new-instance v0, LbcZ;

    const-string v1, "NO_GMAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->h:LbcZ;

    .line 38
    new-instance v0, LbcZ;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->i:LbcZ;

    .line 39
    new-instance v0, LbcZ;

    const-string v1, "CAPTCHA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->j:LbcZ;

    .line 40
    new-instance v0, LbcZ;

    const-string v1, "CANCELLED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->k:LbcZ;

    .line 41
    new-instance v0, LbcZ;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->l:LbcZ;

    .line 42
    new-instance v0, LbcZ;

    const-string v1, "OAUTH_MIGRATION_REQUIRED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LbcZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbcZ;->m:LbcZ;

    .line 29
    const/16 v0, 0xd

    new-array v0, v0, [LbcZ;

    sget-object v1, LbcZ;->a:LbcZ;

    aput-object v1, v0, v3

    sget-object v1, LbcZ;->b:LbcZ;

    aput-object v1, v0, v4

    sget-object v1, LbcZ;->c:LbcZ;

    aput-object v1, v0, v5

    sget-object v1, LbcZ;->d:LbcZ;

    aput-object v1, v0, v6

    sget-object v1, LbcZ;->e:LbcZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LbcZ;->f:LbcZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LbcZ;->g:LbcZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LbcZ;->h:LbcZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LbcZ;->i:LbcZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LbcZ;->j:LbcZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LbcZ;->k:LbcZ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LbcZ;->l:LbcZ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LbcZ;->m:LbcZ;

    aput-object v2, v0, v1

    sput-object v0, LbcZ;->a:[LbcZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbcZ;
    .locals 1

    .prologue
    .line 29
    const-class v0, LbcZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbcZ;

    return-object v0
.end method

.method public static final values()[LbcZ;
    .locals 1

    .prologue
    .line 29
    sget-object v0, LbcZ;->a:[LbcZ;

    invoke-virtual {v0}, [LbcZ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbcZ;

    return-object v0
.end method
