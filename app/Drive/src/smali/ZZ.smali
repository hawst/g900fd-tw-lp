.class LZZ;
.super Ljava/lang/Object;
.source "AppInstalledNotifier.java"


# instance fields
.field final a:J

.field final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LuM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/google/android/gms/drive/database/data/EntrySpec;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LuM;",
            ">;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "empty nativeApps"

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 43
    invoke-static {p1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, LZZ;->a:Ljava/util/Set;

    .line 44
    iput-object p2, p0, LZZ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 45
    iput-wide p3, p0, LZZ;->a:J

    .line 46
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
