.class public final enum LZj;
.super Ljava/lang/Enum;
.source "PunchWebViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LZj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LZj;

.field private static final a:LalK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalK",
            "<",
            "LZj;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LZj;

.field public static final enum b:LZj;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 99
    new-instance v0, LZj;

    const-string v1, "PORTRAIT"

    const-string v2, "webViewPunchPortraitDuration"

    invoke-direct {v0, v1, v4, v2, v3}, LZj;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LZj;->a:LZj;

    .line 100
    new-instance v0, LZj;

    const-string v1, "LANDSCAPE"

    const-string v2, "webViewPunchLandscapeDuration"

    invoke-direct {v0, v1, v3, v2, v5}, LZj;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LZj;->b:LZj;

    .line 98
    new-array v0, v5, [LZj;

    sget-object v1, LZj;->a:LZj;

    aput-object v1, v0, v4

    sget-object v1, LZj;->b:LZj;

    aput-object v1, v0, v3

    sput-object v0, LZj;->a:[LZj;

    .line 107
    invoke-static {}, LZj;->a()LalK;

    move-result-object v0

    sput-object v0, LZj;->a:LalK;

    .line 106
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 110
    iput-object p3, p0, LZj;->a:Ljava/lang/String;

    .line 111
    iput p4, p0, LZj;->a:I

    .line 112
    return-void
.end method

.method public static synthetic a(I)LZj;
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, LZj;->b(I)LZj;

    move-result-object v0

    return-object v0
.end method

.method private static a()LalK;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LalK",
            "<",
            "LZj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {}, LalK;->a()LalM;

    move-result-object v1

    .line 125
    invoke-static {}, LZj;->values()[LZj;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 126
    iget v5, v4, LZj;->a:I

    invoke-virtual {v1, v5, v4}, LalM;->a(ILjava/lang/Object;)LalM;

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_0
    invoke-virtual {v1}, LalM;->a()LalK;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, LZj;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(LZj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, LZj;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)LZj;
    .locals 1

    .prologue
    .line 119
    sget-object v0, LZj;->a:LalK;

    invoke-virtual {v0, p0}, LalK;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LZj;
    .locals 1

    .prologue
    .line 98
    const-class v0, LZj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LZj;

    return-object v0
.end method

.method public static values()[LZj;
    .locals 1

    .prologue
    .line 98
    sget-object v0, LZj;->a:[LZj;

    invoke-virtual {v0}, [LZj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LZj;

    return-object v0
.end method
