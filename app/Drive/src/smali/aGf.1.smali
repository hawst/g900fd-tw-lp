.class public final LaGf;
.super Ljava/lang/Object;
.source "DatabaseEntrySpec.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;
    .locals 4

    .prologue
    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 36
    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;J)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(I)[Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;
    .locals 1

    .prologue
    .line 41
    new-array v0, p1, [Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, LaGf;->a(Landroid/os/Parcel;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, LaGf;->a(I)[Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    return-object v0
.end method
