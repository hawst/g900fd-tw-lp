.class public Lawr;
.super Ljava/lang/Object;
.source "TileBoard.java"


# instance fields
.field private final a:I

.field final synthetic a:Lawn;

.field private final b:I


# direct methods
.method private constructor <init>(Lawn;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 253
    iput-object p1, p0, Lawr;->a:Lawn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    if-ltz p2, :cond_0

    invoke-virtual {p1}, Lawn;->a()I

    move-result v0

    if-ge p2, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Index %d incompatible with this board %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 255
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p1, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 256
    invoke-static {p1}, Lawn;->a(Lawn;)I

    move-result v0

    div-int v0, p2, v0

    iput v0, p0, Lawr;->a:I

    .line 257
    invoke-static {p1}, Lawn;->a(Lawn;)I

    move-result v0

    rem-int v0, p2, v0

    iput v0, p0, Lawr;->b:I

    .line 258
    return-void

    :cond_0
    move v0, v2

    .line 254
    goto :goto_0
.end method

.method synthetic constructor <init>(Lawn;ILawo;)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0, p1, p2}, Lawr;-><init>(Lawn;I)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lawr;->a:Lawn;

    invoke-static {v0}, Lawn;->a(Lawn;)I

    move-result v0

    iget v1, p0, Lawr;->a:I

    mul-int/2addr v0, v1

    iget v1, p0, Lawr;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public a()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 284
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lawr;->b:I

    sget-object v2, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v2, v2, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    mul-int/2addr v1, v2

    iget v2, p0, Lawr;->a:I

    sget-object v3, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    mul-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public a()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 292
    invoke-virtual {p0}, Lawr;->a()Landroid/graphics/Point;

    move-result-object v0

    .line 293
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    iget v4, v0, Landroid/graphics/Point;->x:I

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    add-int/2addr v4, v5

    iget v0, v0, Landroid/graphics/Point;->y:I

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    add-int/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method public a()Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 1

    .prologue
    .line 270
    sget-object v0, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    return-object v0
.end method

.method public a(Lawn;)Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lawr;->a:Lawn;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 5

    .prologue
    .line 275
    iget v0, p0, Lawr;->a:I

    iget-object v1, p0, Lawr;->a:Lawn;

    invoke-static {v1}, Lawn;->b(Lawn;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lawr;->b:I

    iget-object v1, p0, Lawr;->a:Lawn;

    invoke-static {v1}, Lawn;->a(Lawn;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 276
    sget-object v0, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    .line 280
    :goto_0
    return-object v0

    .line 278
    :cond_0
    invoke-virtual {p0}, Lawr;->a()Landroid/graphics/Point;

    move-result-object v1

    .line 279
    new-instance v0, Lcom/google/android/apps/viewer/client/Dimensions;

    sget-object v2, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v2, v2, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget-object v3, p0, Lawr;->a:Lawn;

    iget-object v3, v3, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget v4, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    sget-object v3, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    iget-object v4, p0, Lawr;->a:Lawn;

    iget-object v4, v4, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v4, v4, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int v1, v4, v1

    .line 280
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/viewer/client/Dimensions;-><init>(II)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 298
    if-ne p0, p1, :cond_1

    .line 305
    :cond_0
    :goto_0
    return v0

    .line 301
    :cond_1
    instance-of v2, p1, Lawr;

    if-eqz v2, :cond_3

    .line 302
    check-cast p1, Lawr;

    .line 303
    iget-object v2, p0, Lawr;->a:Lawn;

    invoke-virtual {p1, v2}, Lawr;->a(Lawn;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lawr;->a:I

    iget v3, p1, Lawr;->a:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lawr;->b:I

    iget v3, p1, Lawr;->b:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 305
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lawr;->a:Lawn;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lawr;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 315
    const-string v0, "Tile %d @(%d, %d)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lawr;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lawr;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lawr;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
