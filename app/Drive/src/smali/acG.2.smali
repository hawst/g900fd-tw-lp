.class final enum LacG;
.super Ljava/lang/Enum;
.source "SharingRequestFlow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LacG;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LacG;

.field private static final synthetic a:[LacG;

.field public static final enum b:LacG;

.field public static final enum c:LacG;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, LacG;

    const-string v1, "UNINITILAIZED"

    invoke-direct {v0, v1, v2}, LacG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LacG;->a:LacG;

    new-instance v0, LacG;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v3}, LacG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LacG;->b:LacG;

    new-instance v0, LacG;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, LacG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LacG;->c:LacG;

    const/4 v0, 0x3

    new-array v0, v0, [LacG;

    sget-object v1, LacG;->a:LacG;

    aput-object v1, v0, v2

    sget-object v1, LacG;->b:LacG;

    aput-object v1, v0, v3

    sget-object v1, LacG;->c:LacG;

    aput-object v1, v0, v4

    sput-object v0, LacG;->a:[LacG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LacG;
    .locals 1

    .prologue
    .line 19
    const-class v0, LacG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LacG;

    return-object v0
.end method

.method public static values()[LacG;
    .locals 1

    .prologue
    .line 19
    sget-object v0, LacG;->a:[LacG;

    invoke-virtual {v0}, [LacG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LacG;

    return-object v0
.end method
