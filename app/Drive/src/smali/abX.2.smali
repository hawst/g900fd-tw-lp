.class public LabX;
.super Ljava/lang/Object;
.source "ModifySharingActivity.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Lacr;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lacr;)V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Lacr;)Lacr;

    .line 282
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lald;

    invoke-interface {v0}, Lald;->a()V

    .line 283
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget-object v1, Laca;->c:Laca;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Laca;)Laca;

    .line 285
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 287
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 288
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 267
    check-cast p1, Lacr;

    invoke-virtual {p0, p1}, LabX;->a(Lacr;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 270
    instance-of v0, p1, LQn;

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Lxi;->sharing_list_offline:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    .line 276
    :cond_0
    :goto_0
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->finish()V

    .line 277
    return-void

    .line 272
    :cond_1
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 273
    const-string v0, "ModifySharingActivity"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 274
    iget-object v0, p0, LabX;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Lxi;->sharing_error:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    goto :goto_0
.end method
