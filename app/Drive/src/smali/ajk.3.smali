.class abstract Lajk;
.super Ljava/lang/Object;
.source "FragmentActivitySession.java"

# interfaces
.implements LaiN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LaiD;",
        ">",
        "Ljava/lang/Object;",
        "LaiN;"
    }
.end annotation


# instance fields
.field volatile a:LaiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final synthetic a:Lajj;

.field final a:Landroid/app/Activity;

.field a:Ljava/util/UUID;

.field a:Z


# direct methods
.method constructor <init>(Lajj;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 57
    iput-object p1, p0, Lajk;->a:Lajj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lajk;->a:Landroid/app/Activity;

    .line 59
    return-void
.end method

.method private b()LaiD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lajk;->a:LaiD;

    .line 102
    if-nez v0, :cond_2

    .line 103
    monitor-enter p0

    .line 104
    :try_start_0
    iget-object v0, p0, Lajk;->a:LaiD;

    .line 105
    if-nez v0, :cond_1

    .line 106
    iget-object v0, p0, Lajk;->a:Ljava/util/UUID;

    const-string v1, "Session key not initialized, onCreate probably was not called"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lajk;->a:Lajj;

    invoke-static {v0}, Lajj;->b(Lajj;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lajk;->a:Ljava/util/UUID;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiD;

    .line 109
    if-nez v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lajk;->a()LaiD;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lajk;->a:Lajj;

    invoke-interface {v0, v1}, LaiD;->a(LaiE;)V

    .line 112
    iget-object v1, p0, Lajk;->a:Ljava/util/UUID;

    invoke-interface {v0, v1}, LaiD;->a(Ljava/util/UUID;)V

    .line 113
    invoke-direct {p0}, Lajk;->d()V

    .line 114
    iget-object v1, p0, Lajk;->a:Lajj;

    invoke-static {v1}, Lajj;->b(Lajj;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lajk;->a:Ljava/util/UUID;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :cond_0
    iput-object v0, p0, Lajk;->a:LaiD;

    .line 119
    :cond_1
    monitor-exit p0

    .line 122
    :cond_2
    return-object v0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lajk;->a:LaiD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lajk;->a:LaiD;

    invoke-interface {v0}, LaiD;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lajk;->a:Z

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0}, Lajk;->c()V

    .line 129
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()LaiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method a()LajA;
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Lajk;->b()LaiD;

    move-result-object v0

    invoke-interface {v0}, LaiD;->a()LajA;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lajk;->a:Z

    .line 74
    invoke-direct {p0}, Lajk;->d()V

    .line 75
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 63
    if-eqz p1, :cond_0

    const-string v0, "ActivitySessionKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "ActivitySessionKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/UUID;

    iput-object v0, p0, Lajk;->a:Ljava/util/UUID;

    .line 66
    :cond_0
    iget-object v0, p0, Lajk;->a:Ljava/util/UUID;

    if-nez v0, :cond_1

    .line 67
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lajk;->a:Ljava/util/UUID;

    .line 69
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lajk;->a:Lajj;

    invoke-static {v0}, Lajj;->a(Lajj;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lajk;->a:Landroid/app/Activity;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lajk;->a:Ljava/util/UUID;

    const-string v1, "Session key not initialized, onCreate probably was not called"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string v0, "ActivitySessionKey"

    iget-object v1, p0, Lajk;->a:Ljava/util/UUID;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 87
    return-void
.end method

.method protected abstract c()V
.end method
