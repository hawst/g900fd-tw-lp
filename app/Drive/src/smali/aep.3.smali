.class public Laep;
.super Ljava/lang/Object;
.source "ChangedMetadataSyncerImpl.java"

# interfaces
.implements Laen;


# instance fields
.field private final a:LaHy;

.field private final a:Laer;


# direct methods
.method public constructor <init>(LaHy;Laer;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Laep;->a:LaHy;

    .line 31
    iput-object p2, p0, Laep;->a:Laer;

    .line 32
    return-void
.end method


# virtual methods
.method public a(LaJT;LaFO;)LaJT;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 46
    instance-of v0, p1, LaeF;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 47
    check-cast v0, LaeF;

    .line 50
    :try_start_0
    const-string v2, "ChangedMetadataSyncer"

    const-string v3, "Start uploadEntryInfo for: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, LaJT;->k()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 51
    iget-object v2, p0, Laep;->a:Laer;

    invoke-interface {v2, v0, p2}, Laer;->a(Lbxb;LaFO;)Lbxb;

    move-result-object v0

    .line 52
    instance-of v2, v0, LaJT;

    if-nez v2, :cond_0

    .line 53
    const-string v0, "ChangedMetadataSyncer"

    const-string v2, "Returned type not of kind DocEntry"

    invoke-static {v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iget-object v0, p0, Laep;->a:LaHy;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LaHy;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 75
    :goto_0
    return-object v0

    .line 57
    :cond_0
    check-cast v0, LaJT;
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 71
    const-string v1, "ChangedMetadataSyncer"

    const-string v2, "Finished uploadEntryInfo for: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-interface {p1}, LaJT;->k()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    const-string v2, "ChangedMetadataSyncer"

    const-string v3, "Authentication error while uploading: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, LaJT;->k()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 60
    iget-object v2, p0, Laep;->a:LaHy;

    invoke-interface {v2, v7, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 61
    goto :goto_0

    .line 62
    :catch_1
    move-exception v0

    .line 63
    const-string v2, "ChangedMetadataSyncer"

    const-string v3, "Conflict error while uploading: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, LaJT;->k()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 64
    iget-object v2, p0, Laep;->a:LaHy;

    invoke-interface {v2, v7, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 65
    goto :goto_0

    .line 66
    :catch_2
    move-exception v0

    .line 67
    const-string v2, "ChangedMetadataSyncer"

    const-string v3, "Network error while uploading: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, LaJT;->k()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 68
    iget-object v2, p0, Laep;->a:LaHy;

    const/4 v3, 0x3

    invoke-interface {v2, v3, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 69
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 75
    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaJT;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 85
    :try_start_0
    iget-object v0, p0, Laep;->a:Laer;

    invoke-interface {v0, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 97
    :goto_0
    return-object v0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "ChangedMetadataSyncer"

    const-string v2, "Authentication error: %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, LbwO;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 89
    iget-object v1, p0, Laep;->a:LaHy;

    invoke-interface {v1, v5, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 97
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :catch_1
    move-exception v0

    .line 91
    const-string v1, "ChangedMetadataSyncer"

    const-string v2, "Unable to parse document feed: %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lbxk;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 92
    iget-object v1, p0, Laep;->a:LaHy;

    const/4 v2, 0x2

    invoke-interface {v1, v2, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    goto :goto_1

    .line 93
    :catch_2
    move-exception v0

    .line 94
    const-string v1, "ChangedMetadataSyncer"

    const-string v2, "Network error: %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 95
    iget-object v1, p0, Laep;->a:LaHy;

    const/4 v2, 0x3

    invoke-interface {v1, v2, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    goto :goto_1
.end method
