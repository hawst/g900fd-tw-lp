.class public LaIH;
.super LaIC;
.source "SearchGuid.java"


# instance fields
.field private final a:LaGM;

.field private final a:Ljava/lang/String;

.field private final a:LvL;


# direct methods
.method constructor <init>(JLjava/lang/String;LaGM;LvL;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, LaIC;-><init>(J)V

    .line 36
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaIH;->a:Ljava/lang/String;

    .line 37
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LaIH;->a:LaGM;

    .line 38
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvL;

    iput-object v0, p0, LaIH;->a:LvL;

    .line 39
    return-void
.end method


# virtual methods
.method public a()LaGo;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Z[Ljava/lang/String;LIK;Landroid/net/Uri;)LaHY;
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, LaIH;->a:LaGM;

    invoke-virtual {p0}, LaIH;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LaGM;->a(J)LaFM;

    move-result-object v0

    .line 48
    if-nez v0, :cond_0

    .line 49
    const/4 v0, 0x0

    .line 57
    :goto_0
    return-object v0

    .line 52
    :cond_0
    iget-object v1, p0, LaIH;->a:Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-static {v1, v2, v3}, Laay;->a(Ljava/lang/String;J)Laay;

    move-result-object v1

    .line 53
    new-instance v2, LvN;

    invoke-direct {v2}, LvN;-><init>()V

    iget-object v3, p0, LaIH;->a:LvL;

    .line 54
    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-interface {v3, v0}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {v2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    iget-object v2, p0, LaIH;->a:LvL;

    .line 55
    invoke-interface {v2, v1}, LvL;->a(Laay;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 57
    iget-object v1, p0, LaIH;->a:LaGM;

    invoke-static {p1, p2, v0, p3, v1}, LaIC;->a(Z[Ljava/lang/String;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LaGM;)LaHY;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Z[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 83
    invoke-super {p0, p1}, LaIC;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 87
    :goto_0
    return v0

    .line 86
    :cond_0
    check-cast p1, LaIH;

    .line 87
    iget-object v0, p0, LaIH;->a:Ljava/lang/String;

    iget-object v1, p1, LaIH;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, LaIC;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaIH;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
