.class public LCa;
.super Ljava/lang/Object;
.source "DocumentPinActionHelperImpl.java"

# interfaces
.implements LBZ;


# instance fields
.field private final a:LVg;

.field private final a:LamL;

.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(LamL;Landroid/content/Context;LVg;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, LCa;->a:LamL;

    .line 30
    iput-object p2, p0, LCa;->a:Landroid/content/Context;

    .line 31
    iput-object p3, p0, LCa;->a:LVg;

    .line 32
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, LCa;->a:Landroid/content/Context;

    instance-of v0, v0, LH;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, LCa;->a:Landroid/content/Context;

    check-cast v0, LH;

    .line 63
    new-instance v1, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;-><init>()V

    .line 64
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v2, "pin_warning"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method


# virtual methods
.method public a(LaGo;Z)V
    .locals 2

    .prologue
    .line 36
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-interface {p1}, LaGo;->f()Z

    move-result v0

    if-ne p2, v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, LCa;->a:LVg;

    .line 43
    invoke-interface {p1}, LaGo;->a()LaFM;

    move-result-object v1

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->a(LaFO;)LVf;

    move-result-object v0

    .line 44
    if-eqz p2, :cond_2

    .line 45
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    .line 47
    iget-object v1, p0, LCa;->a:LamL;

    invoke-interface {v1}, LamL;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 52
    invoke-direct {p0}, LCa;->a()V

    .line 57
    :cond_1
    :goto_1
    iget-object v1, p0, LCa;->a:LVg;

    invoke-virtual {v0}, LVf;->a()LVd;

    move-result-object v0

    invoke-virtual {v1, v0}, LVg;->a(LVd;)V

    goto :goto_0

    .line 55
    :cond_2
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, LVf;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    goto :goto_1
.end method
