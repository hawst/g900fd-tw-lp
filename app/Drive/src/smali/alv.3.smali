.class public Lalv;
.super Ljava/lang/Object;
.source "FutureUtils.java"


# static fields
.field private static final a:LbsF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsF",
            "<",
            "LbsU",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lalw;

    invoke-direct {v0}, Lalw;-><init>()V

    sput-object v0, Lalv;->a:Ljava/util/concurrent/Executor;

    .line 138
    new-instance v0, Lalx;

    invoke-direct {v0}, Lalx;-><init>()V

    sput-object v0, Lalv;->a:LbsF;

    return-void
.end method

.method public static a(LbsU;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<+",
            "LbsU",
            "<+TV;>;>;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lalv;->a:LbsF;

    invoke-static {p0, v0}, LbsK;->a(LbsU;LbsF;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbsU;LalB;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<+TV;>;",
            "LalB",
            "<+TV;>;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 154
    invoke-static {}, LbsY;->a()LbsW;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lalv;->a(LbsU;LalB;Ljava/util/concurrent/Executor;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbsU;LalB;Ljava/util/concurrent/Executor;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LbsU",
            "<+TV;>;",
            "LalB",
            "<+TV;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 166
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    new-instance v0, Laly;

    invoke-direct {v0, p0, p1, p2}, Laly;-><init>(LbsU;LalB;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Future;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<+TV;>;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 175
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p1

    .line 179
    :goto_0
    return-object p1

    .line 176
    :catch_0
    move-exception v0

    goto :goto_0

    .line 178
    :catch_1
    move-exception v0

    goto :goto_0
.end method
