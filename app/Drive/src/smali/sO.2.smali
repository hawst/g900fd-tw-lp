.class public LsO;
.super LaGN;
.source "DocumentOpenerActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LaGu;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field final synthetic a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/content/Intent;Z)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, LsO;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    iput-object p2, p0, LsO;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object p3, p0, LsO;->a:Landroid/content/Intent;

    iput-boolean p4, p0, LsO;->a:Z

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)LaGu;
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, LsO;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, LsO;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    .line 184
    :cond_0
    iget-object v0, p0, LsO;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    iget-object v1, p0, LsO;->a:Landroid/content/Intent;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_1

    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0, p1}, LsO;->a(LaGM;)LaGu;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGu;)V
    .locals 3

    .prologue
    .line 191
    if-nez p1, :cond_0

    .line 193
    iget-object v0, p0, LsO;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    sget v1, Lxi;->error_document_not_available:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;I)V

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, LsO;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    iget-object v1, p0, LsO;->a:Landroid/content/Intent;

    iget-boolean v2, p0, LsO;->a:Z

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Landroid/content/Intent;LaGu;Z)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 178
    check-cast p1, LaGu;

    invoke-virtual {p0, p1}, LsO;->a(LaGu;)V

    return-void
.end method
