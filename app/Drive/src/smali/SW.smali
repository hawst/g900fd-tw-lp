.class public final LSW;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSL;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 39
    iput-object p1, p0, LSW;->a:LbrA;

    .line 40
    const-class v0, LSL;

    invoke-static {v0, v1}, LSW;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LSW;->a:Lbsk;

    .line 43
    const-class v0, LSV;

    invoke-static {v0, v1}, LSW;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LSW;->b:Lbsk;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 67
    packed-switch p1, :pswitch_data_0

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :pswitch_0
    new-instance v4, LSL;

    iget-object v0, p0, LSW;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJQ;

    iget-object v0, v0, LJQ;->c:Lbsk;

    .line 72
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LSW;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJQ;

    iget-object v1, v1, LJQ;->c:Lbsk;

    .line 70
    invoke-static {v0, v1}, LSW;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lara;

    iget-object v1, p0, LSW;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->n:Lbsk;

    .line 78
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LSW;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->n:Lbsk;

    .line 76
    invoke-static {v1, v2}, LSW;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LSW;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lwc;

    iget-object v2, v2, Lwc;->g:Lbsk;

    .line 84
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LSW;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lwc;

    iget-object v3, v3, Lwc;->g:Lbsk;

    .line 82
    invoke-static {v2, v3}, LSW;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lwm;

    iget-object v3, p0, LSW;->a:LbrA;

    iget-object v3, v3, LbrA;->a:La;

    iget-object v3, v3, La;->a:Lbsk;

    .line 90
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LSW;->a:LbrA;

    iget-object v5, v5, LbrA;->a:La;

    iget-object v5, v5, La;->a:Lbsk;

    .line 88
    invoke-static {v3, v5}, LSW;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-direct {v4, v0, v1, v2, v3}, LSL;-><init>(Lara;Laja;Lwm;Landroid/app/Activity;)V

    move-object v0, v4

    .line 105
    :goto_0
    return-object v0

    .line 97
    :pswitch_1
    new-instance v1, LSV;

    iget-object v0, p0, LSW;->a:LbrA;

    iget-object v0, v0, LbrA;->a:La;

    iget-object v0, v0, La;->a:Lbsk;

    .line 100
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LSW;->a:LbrA;

    iget-object v2, v2, LbrA;->a:La;

    iget-object v2, v2, La;->a:Lbsk;

    .line 98
    invoke-static {v0, v2}, LSW;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v1, v0}, LSV;-><init>(Landroid/app/Activity;)V

    move-object v0, v1

    .line 105
    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 122
    .line 124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 53
    const-class v0, LSL;

    iget-object v1, p0, LSW;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LSW;->a(Ljava/lang/Class;Lbsk;)V

    .line 54
    const-class v0, LSV;

    iget-object v1, p0, LSW;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LSW;->a(Ljava/lang/Class;Lbsk;)V

    .line 55
    iget-object v0, p0, LSW;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x30

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 57
    iget-object v0, p0, LSW;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x31

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 59
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 114
    .line 116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method
