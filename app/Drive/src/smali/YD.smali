.class LYD;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"

# interfaces
.implements Landroid/webkit/WebView$PictureListener;


# instance fields
.field final synthetic a:I

.field final synthetic a:LYA;

.field final synthetic a:LYB;


# direct methods
.method constructor <init>(LYB;LYA;I)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, LYD;->a:LYB;

    iput-object p2, p0, LYD;->a:LYA;

    iput p3, p0, LYD;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewPicture(Landroid/webkit/WebView;Landroid/graphics/Picture;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 157
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "t=%s onNewPicture(): slideIndex=%s imageLoaded=%s publishOnPageFinished=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LYD;->a:I

    .line 159
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, LYD;->a:LYB;

    invoke-static {v3}, LYB;->a(LYB;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, LYD;->a:LYB;

    invoke-static {v3}, LYB;->b(LYB;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x3

    iget-object v4, p0, LYD;->a:LYB;

    invoke-static {v4}, LYB;->c(LYB;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 157
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 161
    iget-object v0, p0, LYD;->a:LYB;

    invoke-static {v0}, LYB;->b(LYB;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LYD;->a:LYB;

    invoke-static {v0}, LYB;->c(LYB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "t=%s aborting onNewPicture() for slideIndex=%s"

    new-array v2, v7, [Ljava/lang/Object;

    iget v3, p0, LYD;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, LYD;->a:LYB;

    invoke-static {v3}, LYB;->a(LYB;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 174
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, LYD;->a:LYB;

    iget-object v0, v0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LYE;

    invoke-direct {v1, p0}, LYE;-><init>(LYD;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
