.class public abstract LwN;
.super Lrm;
.source "CaptureBaseActivity.java"


# instance fields
.field public a:LaFO;

.field public a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lrm;-><init>()V

    .line 185
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 178
    invoke-static {}, Lcom/google/android/apps/docs/capture/DocScannerActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, LpR;->create_new_from_scan:I

    :goto_0
    return v0

    :cond_0
    sget v0, LpR;->create_new_from_camera:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LaFO;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LwN;->a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lcom/google/android/apps/docs/capture/DocScannerActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/apps/docs/capture/DocScannerActivity;

    .line 148
    invoke-static {v0, p0, p1, p2}, LwN;->a(Ljava/lang/Class;Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 150
    :goto_0
    return-object v0

    .line 148
    :cond_0
    const-class v0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;

    .line 150
    invoke-static {v0, p0, p1, p2}, LwN;->a(Ljava/lang/Class;Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LwN;",
            ">;",
            "Landroid/content/Context;",
            "LaFO;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 165
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 166
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 168
    invoke-virtual {v0, p1, p0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 169
    const-string v1, "accountName"

    invoke-virtual {p2}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 171
    return-object v0

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 66
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, LwN;->a:LaFO;

    .line 67
    iget-object v0, p0, LwN;->a:LaFO;

    if-nez v0, :cond_1

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LwN;->setResult(I)V

    .line 69
    invoke-virtual {p0}, LwN;->finish()V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    const-string v0, "collectionEntrySpec"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LwN;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 73
    invoke-virtual {p0, p1, p2}, LwN;->a(Landroid/content/Intent;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LwN;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera.autofocus"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Intent;)Landroid/content/Intent;
.end method

.method public abstract a(Landroid/content/Intent;Landroid/os/Bundle;)Landroid/content/Intent;
.end method

.method public f()V
    .locals 2

    .prologue
    .line 60
    const-string v0, "ScannerBaseActivity"

    const-string v1, "No external storage present, cannot take picture."

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    sget v0, LpR;->camera_ocr_no_sdcard:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 62
    invoke-virtual {p0}, LwN;->finish()V

    .line 63
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 103
    invoke-super {p0, p1, p2, p3}, Lrm;->onActivityResult(IILandroid/content/Intent;)V

    .line 104
    if-ne p1, v1, :cond_0

    .line 105
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 106
    invoke-virtual {p0, p3}, LwN;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, LwN;->startActivity(Landroid/content/Intent;)V

    .line 107
    invoke-virtual {p0}, LwN;->finish()V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    if-eqz p2, :cond_2

    .line 110
    sget v0, LpR;->camera_ocr_error_capture:I

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 112
    :cond_2
    invoke-virtual {p0}, LwN;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, LwN;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LwN;->a(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 45
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Lrm;->onNewIntent(Landroid/content/Intent;)V

    .line 50
    invoke-virtual {p0, p1}, LwN;->setIntent(Landroid/content/Intent;)V

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LwN;->a(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 91
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 92
    return-void
.end method
