.class public final enum LIP;
.super Ljava/lang/Enum;
.source "TimeRange.java"

# interfaces
.implements LIy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIP;",
        ">;",
        "LIy;"
    }
.end annotation


# static fields
.field public static final enum a:LIP;

.field private static final synthetic a:[LIP;

.field public static final enum b:LIP;

.field public static final enum c:LIP;

.field public static final enum d:LIP;

.field public static final enum e:LIP;

.field public static final enum f:LIP;

.field public static final enum g:LIP;


# instance fields
.field private final a:LIr;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    new-instance v0, LIP;

    const-string v1, "TODAY"

    sget v2, Lxi;->time_range_today:I

    invoke-direct {v0, v1, v4, v2}, LIP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIP;->a:LIP;

    .line 17
    new-instance v0, LIP;

    const-string v1, "YESTERDAY"

    sget v2, Lxi;->time_range_yesterday:I

    invoke-direct {v0, v1, v5, v2}, LIP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIP;->b:LIP;

    .line 18
    new-instance v0, LIP;

    const-string v1, "THIS_WEEK"

    sget v2, Lxi;->time_range_this_week:I

    invoke-direct {v0, v1, v6, v2}, LIP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIP;->c:LIP;

    .line 19
    new-instance v0, LIP;

    const-string v1, "THIS_MONTH"

    sget v2, Lxi;->time_range_this_month:I

    invoke-direct {v0, v1, v7, v2}, LIP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIP;->d:LIP;

    .line 20
    new-instance v0, LIP;

    const-string v1, "THIS_YEAR"

    sget v2, Lxi;->time_range_this_year:I

    invoke-direct {v0, v1, v8, v2}, LIP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIP;->e:LIP;

    .line 21
    new-instance v0, LIP;

    const-string v1, "LAST_YEAR"

    const/4 v2, 0x5

    sget v3, Lxi;->time_range_last_year:I

    invoke-direct {v0, v1, v2, v3}, LIP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIP;->f:LIP;

    .line 22
    new-instance v0, LIP;

    const-string v1, "OLDER"

    const/4 v2, 0x6

    sget v3, Lxi;->time_range_older:I

    invoke-direct {v0, v1, v2, v3}, LIP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIP;->g:LIP;

    .line 15
    const/4 v0, 0x7

    new-array v0, v0, [LIP;

    sget-object v1, LIP;->a:LIP;

    aput-object v1, v0, v4

    sget-object v1, LIP;->b:LIP;

    aput-object v1, v0, v5

    sget-object v1, LIP;->c:LIP;

    aput-object v1, v0, v6

    sget-object v1, LIP;->d:LIP;

    aput-object v1, v0, v7

    sget-object v1, LIP;->e:LIP;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LIP;->f:LIP;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LIP;->g:LIP;

    aput-object v2, v0, v1

    sput-object v0, LIP;->a:[LIP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 98
    new-instance v0, LIr;

    invoke-direct {v0, p3}, LIr;-><init>(I)V

    iput-object v0, p0, LIP;->a:LIr;

    .line 99
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LIP;
    .locals 1

    .prologue
    .line 15
    const-class v0, LIP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIP;

    return-object v0
.end method

.method public static values()[LIP;
    .locals 1

    .prologue
    .line 15
    sget-object v0, LIP;->a:[LIP;

    invoke-virtual {v0}, [LIP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIP;

    return-object v0
.end method


# virtual methods
.method public a(ZZ)LIB;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, LIP;->a:LIr;

    invoke-virtual {v0, p1, p2}, LIr;->a(ZZ)LIB;

    move-result-object v0

    return-object v0
.end method
