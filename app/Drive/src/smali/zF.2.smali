.class LzF;
.super Ljava/lang/Object;
.source "SharingCard.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:LzD;


# direct methods
.method private constructor <init>(LzD;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, LzF;->a:LzD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LzD;LzE;)V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0, p1}, LzF;-><init>(LzD;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 271
    iget-object v0, p0, LzF;->a:LzD;

    invoke-static {v0}, LzD;->a(LzD;)LaKR;

    move-result-object v0

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 272
    iget-object v0, p0, LzF;->a:LzD;

    invoke-static {v0}, LzD;->a(LzD;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sharing_offline:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, LzF;->a:LzD;

    invoke-static {v0}, LzD;->a(LzD;)Lacr;

    move-result-object v0

    invoke-interface {v0}, Lacr;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 276
    iget-object v0, p0, LzF;->a:LzD;

    invoke-static {v0}, LzD;->a(LzD;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sharing_cannot_change:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 279
    :cond_2
    iget-object v0, p0, LzF;->a:LzD;

    invoke-static {v0}, LzD;->a(LzD;)Lacj;

    move-result-object v0

    invoke-virtual {v0}, Lacj;->a()Lacr;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lacr;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, LzF;->a:LzD;

    invoke-static {v0}, LzD;->a(LzD;)Lacj;

    move-result-object v0

    iget-object v1, p0, LzF;->a:LzD;

    invoke-static {v1}, LzD;->a(LzD;)LaGu;

    move-result-object v1

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    goto :goto_0
.end method
