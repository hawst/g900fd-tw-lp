.class public final enum LaFl;
.super Ljava/lang/Enum;
.source "UniqueIdTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFl;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFl;

.field private static final synthetic a:[LaFl;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 36
    new-instance v0, LaFl;

    const-string v1, "NULL_HOLDER"

    invoke-static {}, LaFk;->b()LaFk;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    const/16 v3, 0x24

    new-instance v4, LaFG;

    const-string v5, "nullHolder"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 37
    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaFl;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFl;->a:LaFl;

    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [LaFl;

    sget-object v1, LaFl;->a:LaFl;

    aput-object v1, v0, v7

    sput-object v0, LaFl;->a:[LaFl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaFl;->a:LaFr;

    .line 44
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFl;
    .locals 1

    .prologue
    .line 26
    const-class v0, LaFl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFl;

    return-object v0
.end method

.method public static values()[LaFl;
    .locals 1

    .prologue
    .line 26
    sget-object v0, LaFl;->a:[LaFl;

    invoke-virtual {v0}, [LaFl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFl;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LaFl;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, LaFl;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
