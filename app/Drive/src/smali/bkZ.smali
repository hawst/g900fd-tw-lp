.class public abstract enum LbkZ;
.super Ljava/lang/Enum;
.source "RemovalCause.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbkZ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbkZ;

.field private static final synthetic a:[LbkZ;

.field public static final enum b:LbkZ;

.field public static final enum c:LbkZ;

.field public static final enum d:LbkZ;

.field public static final enum e:LbkZ;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lbla;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Lbla;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbkZ;->a:LbkZ;

    .line 54
    new-instance v0, Lblb;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Lblb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbkZ;->b:LbkZ;

    .line 67
    new-instance v0, Lblc;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Lblc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbkZ;->c:LbkZ;

    .line 79
    new-instance v0, Lbld;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Lbld;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbkZ;->d:LbkZ;

    .line 91
    new-instance v0, Lble;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Lble;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbkZ;->e:LbkZ;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [LbkZ;

    sget-object v1, LbkZ;->a:LbkZ;

    aput-object v1, v0, v2

    sget-object v1, LbkZ;->b:LbkZ;

    aput-object v1, v0, v3

    sget-object v1, LbkZ;->c:LbkZ;

    aput-object v1, v0, v4

    sget-object v1, LbkZ;->d:LbkZ;

    aput-object v1, v0, v5

    sget-object v1, LbkZ;->e:LbkZ;

    aput-object v1, v0, v6

    sput-object v0, LbkZ;->a:[LbkZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbla;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, LbkZ;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbkZ;
    .locals 1

    .prologue
    .line 32
    const-class v0, LbkZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbkZ;

    return-object v0
.end method

.method public static values()[LbkZ;
    .locals 1

    .prologue
    .line 32
    sget-object v0, LbkZ;->a:[LbkZ;

    invoke-virtual {v0}, [LbkZ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbkZ;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
