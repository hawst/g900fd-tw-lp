.class public LZO;
.super Lbuo;
.source "ContextScopedProviderPackageModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lbuo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public get1(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime Lbwm;
            value = "wapiFeedProcessor"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "wapiFeedProcessor"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LZS;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public get2(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime Lbwm;
            value = "wapiFeedProcessor"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "wapiFeedProcessor"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LZP;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LZP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public get3(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime LaoD;
        .end annotation
    .end param
    .annotation runtime LaoD;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LZS;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public get4(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime LaoE;
        .end annotation
    .end param
    .annotation runtime LaoE;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LZS;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public getLazy1(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime Lbwm;
            value = "wapiFeedProcessor"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "wapiFeedProcessor"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LZS;",
            ">;)",
            "Lajw",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method

.method public getLazy2(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime Lbwm;
            value = "wapiFeedProcessor"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "wapiFeedProcessor"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LZP;",
            ">;)",
            "Lajw",
            "<",
            "LZP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method

.method public getLazy3(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime LaoD;
        .end annotation
    .end param
    .annotation runtime LaoD;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LZS;",
            ">;)",
            "Lajw",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method

.method public getLazy4(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime LaoE;
        .end annotation
    .end param
    .annotation runtime LaoE;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LZS;",
            ">;)",
            "Lajw",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method
