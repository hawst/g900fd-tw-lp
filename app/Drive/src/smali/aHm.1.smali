.class public LaHm;
.super LaHq;
.source "MaybeFailingOperation.java"


# instance fields
.field private final a:I

.field a:Z


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;IZ)V
    .locals 1

    .prologue
    .line 42
    const-string v0, "opMayFail"

    invoke-direct {p0, p1, p2, v0}, LaHq;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 43
    iput p3, p0, LaHm;->a:I

    .line 44
    iput-boolean p4, p0, LaHm;->a:Z

    .line 45
    return-void
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHm;
    .locals 3

    .prologue
    .line 49
    new-instance v0, LaHm;

    const-string v1, "id"

    .line 50
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "success"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-direct {v0, p0, p1, v1, v2}, LaHm;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;IZ)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, LaHm;->a:I

    return v0
.end method

.method public a(LaGe;)LaHo;
    .locals 5

    .prologue
    .line 68
    new-instance v0, LaHm;

    iget-object v1, p0, LaHm;->a:LaGg;

    .line 69
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    iget v3, p0, LaHm;->a:I

    iget-boolean v4, p0, LaHm;->a:Z

    invoke-direct {v0, v1, v2, v3, v4}, LaHm;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;IZ)V

    .line 70
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 81
    invoke-super {p0}, LaHq;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 82
    const-string v1, "operationName"

    const-string v2, "opMayFail"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 83
    const-string v1, "id"

    iget v2, p0, LaHm;->a:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 84
    const-string v1, "success"

    iget-boolean v2, p0, LaHm;->a:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 86
    return-object v0
.end method

.method public a(LaJT;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, LaHm;->a:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 91
    instance-of v1, p1, LaHm;

    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    check-cast p1, LaHm;

    .line 95
    invoke-virtual {p0, p1}, LaHm;->a(LaHp;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LaHm;->a:I

    invoke-virtual {p1}, LaHm;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, LaHm;->b()I

    move-result v0

    iget v1, p0, LaHm;->a:I

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 105
    const-string v0, "OpMayFail[%d, %b, %s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LaHm;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, LaHm;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, LaHm;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
