.class public abstract Lbhm;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"


# instance fields
.field protected a:Landroid/app/Activity;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lbhm;->a:Landroid/app/Activity;

    .line 45
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lbhm;
    .locals 2

    .prologue
    .line 34
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 35
    new-instance v0, Lbhx;

    invoke-direct {v0, p0}, Lbhx;-><init>(Landroid/app/Activity;)V

    .line 39
    :goto_0
    return-object v0

    .line 36
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 37
    new-instance v0, Lbhu;

    invoke-direct {v0, p0}, Lbhu;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 39
    :cond_1
    new-instance v0, Lbhp;

    invoke-direct {v0, p0}, Lbhp;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 0

    .prologue
    .line 81
    return-object p1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public a(LbgY;)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public abstract a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public abstract a(Z)V
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public abstract b(Z)V
.end method
