.class public final LaEV;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaEH;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaEz;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaEM;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaEP;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, LaEy;

    sput-object v0, LaEV;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 43
    iput-object p1, p0, LaEV;->a:LbrA;

    .line 44
    const-class v0, LaEH;

    invoke-static {v0, v2}, LaEV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaEV;->a:Lbsk;

    .line 47
    const-class v0, LaEz;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaEV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaEV;->b:Lbsk;

    .line 50
    const-class v0, LaEM;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaEV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaEV;->c:Lbsk;

    .line 53
    const-class v0, LaEP;

    invoke-static {v0, v2}, LaEV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaEV;->d:Lbsk;

    .line 56
    const-class v0, Ljava/lang/String;

    sget-object v1, LaEV;->a:Ljava/lang/Class;

    .line 57
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 56
    invoke-static {v0, v2}, LaEV;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaEV;->e:Lbsk;

    .line 59
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 103
    sparse-switch p1, :sswitch_data_0

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :sswitch_0
    new-instance v0, LaEH;

    invoke-direct {v0}, LaEH;-><init>()V

    .line 107
    iget-object v1, p0, LaEV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaEV;

    .line 108
    invoke-virtual {v1, v0}, LaEV;->a(LaEH;)V

    .line 169
    :goto_0
    return-object v0

    .line 111
    :sswitch_1
    new-instance v0, LaEz;

    iget-object v1, p0, LaEV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 114
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaEV;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 112
    invoke-static {v1, v2}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LaEV;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaEV;

    iget-object v2, v2, LaEV;->e:Lbsk;

    .line 120
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaEV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaEV;

    iget-object v3, v3, LaEV;->e:Lbsk;

    .line 118
    invoke-static {v2, v3}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, LaEV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LqD;

    iget-object v3, v3, LqD;->c:Lbsk;

    .line 126
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaEV;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LqD;

    iget-object v4, v4, LqD;->c:Lbsk;

    .line 124
    invoke-static {v3, v4}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LqK;

    iget-object v4, p0, LaEV;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 132
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaEV;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LQH;

    iget-object v5, v5, LQH;->d:Lbsk;

    .line 130
    invoke-static {v4, v5}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LQr;

    iget-object v5, p0, LaEV;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 138
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaEV;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 136
    invoke-static {v5, v6}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LtK;

    iget-object v6, p0, LaEV;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lajo;

    iget-object v6, v6, Lajo;->ak:Lbsk;

    .line 144
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaEV;->a:LbrA;

    iget-object v7, v7, LbrA;->a:Lajo;

    iget-object v7, v7, Lajo;->ak:Lbsk;

    .line 142
    invoke-static {v6, v7}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Laja;

    invoke-direct/range {v0 .. v6}, LaEz;-><init>(Laja;Ljava/lang/String;LqK;LQr;LtK;Laja;)V

    goto/16 :goto_0

    .line 151
    :sswitch_2
    new-instance v0, LaEM;

    invoke-direct {v0}, LaEM;-><init>()V

    goto/16 :goto_0

    .line 155
    :sswitch_3
    new-instance v2, LaEP;

    iget-object v0, p0, LaEV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 158
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaEV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 156
    invoke-static {v0, v1}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iget-object v1, p0, LaEV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaEV;

    iget-object v1, v1, LaEV;->b:Lbsk;

    .line 164
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaEV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaEV;

    iget-object v3, v3, LaEV;->b:Lbsk;

    .line 162
    invoke-static {v1, v3}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaEz;

    invoke-direct {v2, v0, v1}, LaEP;-><init>(LaGR;LaEz;)V

    move-object v0, v2

    .line 169
    goto/16 :goto_0

    .line 103
    nop

    :sswitch_data_0
    .sparse-switch
        0x292 -> :sswitch_1
        0x51a -> :sswitch_3
        0x540 -> :sswitch_0
        0x541 -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 192
    packed-switch p2, :pswitch_data_0

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :pswitch_0
    check-cast p1, LaEE;

    .line 196
    invoke-virtual {p1}, LaEE;->provideDatabaseName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x2a6
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 77
    const-class v0, LaEH;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xa9

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LaEV;->a(LbuP;LbuB;)V

    .line 80
    const-class v0, LaEH;

    iget-object v1, p0, LaEV;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaEV;->a(Ljava/lang/Class;Lbsk;)V

    .line 81
    const-class v0, LaEz;

    iget-object v1, p0, LaEV;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaEV;->a(Ljava/lang/Class;Lbsk;)V

    .line 82
    const-class v0, LaEM;

    iget-object v1, p0, LaEV;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaEV;->a(Ljava/lang/Class;Lbsk;)V

    .line 83
    const-class v0, LaEP;

    iget-object v1, p0, LaEV;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaEV;->a(Ljava/lang/Class;Lbsk;)V

    .line 84
    const-class v0, Ljava/lang/String;

    sget-object v1, LaEV;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaEV;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LaEV;->a(Lbuv;Lbsk;)V

    .line 85
    iget-object v0, p0, LaEV;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x540

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 87
    iget-object v0, p0, LaEV;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x292

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 89
    iget-object v0, p0, LaEV;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x541

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 91
    iget-object v0, p0, LaEV;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x51a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 93
    iget-object v0, p0, LaEV;->e:Lbsk;

    const-class v1, LaEE;

    const/16 v2, 0x2a6

    invoke-virtual {p0, v1, v2}, LaEV;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 95
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 178
    packed-switch p1, :pswitch_data_0

    .line 186
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :pswitch_0
    check-cast p2, LaEH;

    .line 182
    iget-object v0, p0, LaEV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaEV;

    .line 183
    invoke-virtual {v0, p2}, LaEV;->a(LaEH;)V

    .line 188
    return-void

    .line 178
    :pswitch_data_0
    .packed-switch 0xa9
        :pswitch_0
    .end packed-switch
.end method

.method public a(LaEH;)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, LaEV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaEV;

    iget-object v0, v0, LaEV;->b:Lbsk;

    .line 68
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaEV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaEV;

    iget-object v1, v1, LaEV;->b:Lbsk;

    .line 66
    invoke-static {v0, v1}, LaEV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaEz;

    iput-object v0, p1, LaEH;->a:LaEz;

    .line 72
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method
