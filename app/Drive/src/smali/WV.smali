.class public LWV;
.super Ljava/lang/Object;
.source "DisplayManagerHelperImpl.java"

# interfaces
.implements LWS;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field private a:LWT;

.field private final a:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private a:Landroid/hardware/display/DisplayManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, LakQ;->h()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 27
    new-instance v0, LWW;

    invoke-direct {v0, p0}, LWW;-><init>(LWV;)V

    iput-object v0, p0, LWV;->a:Landroid/hardware/display/DisplayManager$DisplayListener;

    .line 43
    return-void
.end method

.method static synthetic a(LWV;)LWT;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, LWV;->a:LWT;

    return-object v0
.end method


# virtual methods
.method public a(LWT;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, LWV;->a:Landroid/hardware/display/DisplayManager;

    iget-object v1, p0, LWV;->a:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, LWV;->a:LWT;

    .line 71
    return-void
.end method

.method public a(LWT;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 60
    iput-object p1, p0, LWV;->a:LWT;

    .line 61
    iget-object v0, p0, LWV;->a:Landroid/hardware/display/DisplayManager;

    iget-object v1, p0, LWV;->a:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v0, v1, p2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 62
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const-string v0, "display"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, LWV;->a:Landroid/hardware/display/DisplayManager;

    .line 52
    return-void
.end method

.method public a(Ljava/lang/String;)[Landroid/view/Display;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, LWV;->a:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0

    return-object v0
.end method
