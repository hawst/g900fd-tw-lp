.class public LDt;
.super Ljava/lang/Object;
.source "SelectionViewStateImpl.java"

# interfaces
.implements LDg;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final a:LCu;

.field private final a:LJR;

.field private final a:LKZ;

.field private final a:LKp;

.field private final b:LDq;


# direct methods
.method constructor <init>(LJR;LCu;LKZ;LKp;LDq;)V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJR;

    iput-object v0, p0, LDt;->a:LJR;

    .line 103
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCu;

    iput-object v0, p0, LDt;->a:LCu;

    .line 104
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKZ;

    iput-object v0, p0, LDt;->a:LKZ;

    .line 105
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKp;

    iput-object v0, p0, LDt;->a:LKp;

    .line 106
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDq;

    iput-object v0, p0, LDt;->b:LDq;

    .line 107
    return-void
.end method

.method static synthetic a(LDt;)LJR;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LDt;->a:LJR;

    return-object v0
.end method

.method static synthetic a(LDt;)LKZ;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LDt;->a:LKZ;

    return-object v0
.end method

.method static synthetic a(LDt;)LKp;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LDt;->a:LKp;

    return-object v0
.end method

.method public static varargs a(LDi;[F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 396
    const-string v0, "titlePaddingWidth"

    invoke-static {p0, v0, p1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(I[Landroid/view/View;)V
    .locals 3

    .prologue
    .line 113
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 114
    if-eqz v2, :cond_0

    .line 115
    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 113
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_1
    return-void
.end method

.method private a(LDi;I)V
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 294
    :cond_0
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(LDi;LDr;)V
    .locals 1

    .prologue
    .line 214
    sget-object v0, LDr;->a:LDr;

    invoke-virtual {p2, v0}, LDr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {p1}, LDi;->b()V

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-virtual {p1}, LDi;->c()V

    goto :goto_0
.end method

.method private a(LDi;LaGv;LDr;)V
    .locals 2

    .prologue
    .line 266
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 271
    :goto_0
    return-void

    .line 269
    :cond_0
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->a:Landroid/widget/ImageView;

    invoke-virtual {p2}, LaGv;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 270
    invoke-direct {p0, p1, p3}, LDt;->b(LDi;LDr;)V

    goto :goto_0
.end method

.method private a(LDi;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 145
    invoke-virtual {p1}, LDi;->d()V

    .line 146
    new-array v0, v4, [F

    aput v2, v0, v3

    invoke-static {p2, v0}, Lxm;->d(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    new-array v1, v4, [F

    aput v2, v1, v3

    .line 147
    invoke-static {p2, v1}, Lxm;->e(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    new-array v1, v4, [F

    aput v2, v1, v3

    .line 148
    invoke-static {p1, v1}, LDt;->a(LDi;[F)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 149
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 146
    invoke-virtual {p1, v0}, LDi;->a(Landroid/animation/Animator;)V

    .line 151
    invoke-virtual {p1}, LDi;->e()V

    .line 152
    return-void
.end method

.method private a(LDi;Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 122
    new-array v0, v1, [F

    aput v2, v0, v3

    invoke-static {p2, v0}, Lxm;->d(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    new-array v1, v1, [F

    aput v2, v1, v3

    .line 123
    invoke-static {p2, v1}, Lxm;->e(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 124
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    new-instance v1, LDu;

    invoke-direct {v1, p0, p3}, LDu;-><init>(LDt;Landroid/view/View;)V

    .line 125
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 122
    invoke-virtual {p1, v0}, LDi;->a(Landroid/animation/Animator;)V

    .line 131
    return-void
.end method

.method private a(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 3

    .prologue
    .line 208
    new-instance v0, LDx;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p2, p1, v1}, LDx;-><init>(LDt;Lcom/google/android/apps/docs/doclist/SelectionItem;LDi;Z)V

    new-instance v1, LDx;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p2, p1, v2}, LDx;-><init>(LDt;Lcom/google/android/apps/docs/doclist/SelectionItem;LDi;Z)V

    invoke-virtual {p1, v0, v1}, LDi;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 211
    return-void
.end method

.method private a(LDi;ZLandroid/view/View;)V
    .locals 12

    .prologue
    .line 156
    .line 159
    const/16 v0, 0x18

    new-array v1, v0, [F

    .line 160
    const-wide/16 v2, 0x0

    .line 161
    const-wide v4, 0x3fe921fb54442d18L    # 0.7853981633974483

    .line 162
    const/4 v0, 0x0

    :goto_0
    const/16 v6, 0x17

    if-ge v0, v6, :cond_0

    .line 163
    const-wide v6, 0x3fe99999a0000000L    # 0.800000011920929

    rsub-int/lit8 v8, v0, 0x18

    int-to-double v8, v8

    const-wide/high16 v10, 0x4038000000000000L    # 24.0

    div-double/2addr v8, v10

    mul-double/2addr v6, v8

    .line 164
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v10, v6

    mul-double/2addr v6, v10

    sub-double v6, v8, v6

    double-to-float v6, v6

    aput v6, v1, v0

    .line 162
    add-int/lit8 v0, v0, 0x1

    add-double/2addr v2, v4

    goto :goto_0

    .line 166
    :cond_0
    const/16 v0, 0x17

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v1, v0

    .line 168
    invoke-virtual {p1}, LDi;->d()V

    .line 171
    invoke-static {p3, v1}, Lxm;->d(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 172
    invoke-static {p3, v1}, Lxm;->e(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 173
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->b(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    new-instance v1, LDv;

    invoke-direct {v1, p0, p3}, LDv;-><init>(LDt;Landroid/view/View;)V

    .line 174
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 170
    invoke-virtual {p1, v0}, LDi;->a(Landroid/animation/Animator;)V

    .line 185
    if-eqz p2, :cond_1

    .line 186
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    .line 187
    invoke-static {p1, v0}, LDt;->a(LDi;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 188
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 186
    invoke-virtual {p1, v0}, LDi;->a(Landroid/animation/Animator;)V

    .line 190
    :cond_1
    invoke-virtual {p1}, LDi;->e()V

    .line 191
    return-void

    .line 186
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private varargs a([Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 194
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 195
    if-eqz v2, :cond_0

    .line 196
    invoke-virtual {v2, v3}, Landroid/view/View;->setScaleX(F)V

    .line 197
    invoke-virtual {v2, v3}, Landroid/view/View;->setScaleY(F)V

    .line 194
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 200
    :cond_1
    return-void
.end method

.method private b(LDi;)V
    .locals 3

    .prologue
    .line 134
    invoke-virtual {p1}, LDi;->d()V

    .line 136
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->a:Landroid/view/View;

    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v1

    iget-object v1, v1, LDl;->e:Landroid/view/View;

    .line 135
    invoke-direct {p0, p1, v0, v1}, LDt;->a(LDi;Landroid/view/View;Landroid/view/View;)V

    .line 138
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->b:Landroid/view/View;

    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v1

    iget-object v1, v1, LDl;->f:Landroid/view/View;

    .line 137
    invoke-direct {p0, p1, v0, v1}, LDt;->a(LDi;Landroid/view/View;Landroid/view/View;)V

    .line 139
    const/4 v0, 0x1

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    invoke-static {p1, v0}, LDt;->a(LDi;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 140
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v1

    iget-object v1, v1, LDl;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 139
    invoke-virtual {p1, v0}, LDi;->a(Landroid/animation/Animator;)V

    .line 141
    invoke-virtual {p1}, LDi;->e()V

    .line 142
    return-void
.end method

.method private b(LDi;LDr;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 274
    sget-object v0, LDw;->a:[I

    invoke-virtual {p2}, LDr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 286
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 276
    :pswitch_0
    invoke-direct {p0, p1, v2}, LDt;->a(LDi;I)V

    .line 288
    :goto_0
    return-void

    .line 280
    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LDt;->a(LDi;I)V

    goto :goto_0

    .line 283
    :pswitch_2
    invoke-direct {p0, p1, v2}, LDt;->a(LDi;I)V

    goto :goto_0

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 1

    .prologue
    .line 302
    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Z

    move-result v0

    invoke-virtual {p1, v0}, LDi;->a(Z)V

    .line 303
    return-void
.end method

.method private c(LDi;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v1

    iget-object v1, v1, LDl;->e:Landroid/view/View;

    aput-object v1, v0, v2

    invoke-static {v2, v0}, LDt;->a(I[Landroid/view/View;)V

    .line 204
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->a:Landroid/view/View;

    invoke-direct {p0, p1, v0}, LDt;->a(LDi;Landroid/view/View;)V

    .line 205
    return-void
.end method

.method private c(LDi;LDr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 340
    sget-object v0, LDw;->a:[I

    invoke-virtual {p2}, LDr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 353
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 343
    :pswitch_0
    invoke-direct {p0, p1}, LDt;->c(LDi;)V

    .line 355
    :goto_0
    :pswitch_1
    return-void

    .line 346
    :pswitch_2
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 347
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->f:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 348
    invoke-direct {p0, p1, v2}, LDt;->a(LDi;I)V

    goto :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private d(LDi;LDr;)V
    .locals 3

    .prologue
    .line 358
    sget-object v0, LDw;->a:[I

    invoke-virtual {p2}, LDr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 367
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 364
    :pswitch_0
    invoke-direct {p0, p1}, LDt;->b(LDi;)V

    .line 369
    :pswitch_1
    return-void

    .line 358
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private e(LDi;LDr;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 372
    sget-object v0, LDw;->a:[I

    invoke-virtual {p2}, LDr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 388
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 375
    :pswitch_0
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 376
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 377
    const/4 v0, 0x1

    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v1

    iget-object v1, v1, LDl;->b:Landroid/view/View;

    invoke-direct {p0, p1, v0, v1}, LDt;->a(LDi;ZLandroid/view/View;)V

    .line 390
    :goto_0
    :pswitch_1
    return-void

    .line 382
    :pswitch_2
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 383
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 384
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->b:Landroid/view/View;

    invoke-direct {p0, p1, v2, v0}, LDt;->a(LDi;ZLandroid/view/View;)V

    .line 385
    invoke-direct {p0, p1, v2}, LDt;->a(LDi;I)V

    goto :goto_0

    .line 372
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(LDi;)V
    .locals 5

    .prologue
    .line 307
    iget-object v0, p1, LDi;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    invoke-virtual {p1}, LDi;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v0

    .line 312
    invoke-virtual {p1}, LDi;->a()LDr;

    move-result-object v1

    .line 313
    iget-object v2, p0, LDt;->a:LJR;

    invoke-static {v2, v0}, LDr;->a(LJR;Lcom/google/android/apps/docs/doclist/SelectionItem;)LDr;

    move-result-object v2

    .line 315
    invoke-virtual {v1, v2}, LDr;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 316
    invoke-direct {p0, p1, v2}, LDt;->a(LDi;LDr;)V

    .line 317
    invoke-direct {p0, p1, v2}, LDt;->b(LDi;LDr;)V

    .line 318
    iget-object v3, p0, LDt;->b:LDq;

    iget-object v4, p1, LDi;->a:Landroid/view/View;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Z

    move-result v0

    invoke-interface {v3, v4, v2, v0}, LDq;->a(Landroid/view/View;LDr;Z)V

    .line 319
    invoke-virtual {p1, v2}, LDi;->a(LDr;)V

    .line 320
    sget-object v0, LDw;->a:[I

    invoke-virtual {v2}, LDr;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 334
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 322
    :pswitch_0
    invoke-direct {p0, p1, v1}, LDt;->d(LDi;LDr;)V

    goto :goto_0

    .line 325
    :pswitch_1
    invoke-direct {p0, p1, v1}, LDt;->e(LDi;LDr;)V

    goto :goto_0

    .line 328
    :pswitch_2
    invoke-direct {p0, p1, v1}, LDt;->c(LDi;LDr;)V

    goto :goto_0

    .line 331
    :pswitch_3
    invoke-virtual {p1}, LDi;->f()V

    goto :goto_0

    .line 320
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;ILaGv;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 228
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    invoke-virtual {p1}, LDi;->a()V

    .line 232
    iget-object v0, p0, LDt;->a:LCu;

    invoke-virtual {p1, v0, p2, p3}, LDi;->a(LCu;Lcom/google/android/apps/docs/doclist/SelectionItem;I)V

    .line 233
    iget-object v0, p0, LDt;->a:LJR;

    invoke-static {v0, p2}, LDr;->a(LJR;Lcom/google/android/apps/docs/doclist/SelectionItem;)LDr;

    move-result-object v0

    .line 234
    invoke-virtual {p1, v0}, LDi;->a(LDr;)V

    .line 236
    new-instance v1, LDy;

    invoke-direct {v1, p0, p2, p1}, LDy;-><init>(LDt;Lcom/google/android/apps/docs/doclist/SelectionItem;LDi;)V

    .line 238
    invoke-virtual {p1, v1}, LDi;->a(Landroid/view/View$OnLongClickListener;)V

    .line 239
    invoke-direct {p0, p1, p2}, LDt;->b(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 240
    invoke-direct {p0, p1, p2}, LDt;->a(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 241
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/view/View;

    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v2

    iget-object v2, v2, LDl;->a:Landroid/view/View;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v3

    iget-object v3, v3, LDl;->b:Landroid/view/View;

    aput-object v3, v1, v2

    invoke-direct {p0, v1}, LDt;->a([Landroid/view/View;)V

    .line 242
    sget-object v1, LDr;->a:LDr;

    invoke-virtual {v0, v1}, LDr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 243
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, LDi;->a(F)V

    .line 248
    :goto_0
    invoke-direct {p0, p1, v0}, LDt;->a(LDi;LDr;)V

    .line 249
    invoke-direct {p0, p1, p4, v0}, LDt;->a(LDi;LaGv;LDr;)V

    .line 250
    iget-object v1, p0, LDt;->b:LDq;

    iget-object v2, p1, LDi;->a:Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Z

    move-result v3

    invoke-interface {v1, v2, v0, v3}, LDq;->a(Landroid/view/View;LDr;Z)V

    .line 251
    sget-object v1, LDw;->a:[I

    invoke-virtual {v0}, LDr;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 263
    :goto_1
    :pswitch_0
    return-void

    .line 245
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v1}, LDi;->a(F)V

    goto :goto_0

    .line 255
    :pswitch_1
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 258
    :pswitch_2
    invoke-virtual {p1}, LDi;->a()LDl;

    move-result-object v0

    iget-object v0, v0, LDl;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 251
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
