.class Lahv;
.super Ljava/lang/Object;
.source "VideoUrlFetcherImpl.java"


# instance fields
.field private final a:I

.field final synthetic a:Laht;

.field private final b:I


# direct methods
.method private constructor <init>(Laht;II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    iput-object p1, p0, Lahv;->a:Laht;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 54
    if-lez p3, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->a(Z)V

    .line 55
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lahv;->a:I

    .line 56
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lahv;->b:I

    .line 57
    return-void

    :cond_0
    move v0, v2

    .line 53
    goto :goto_0

    :cond_1
    move v1, v2

    .line 54
    goto :goto_1
.end method

.method synthetic constructor <init>(Laht;IILahu;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lahv;-><init>(Laht;II)V

    return-void
.end method

.method private a()J
    .locals 4

    .prologue
    .line 60
    iget v0, p0, Lahv;->a:I

    int-to-long v0, v0

    iget v2, p0, Lahv;->b:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method static synthetic a(Lahv;)J
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Lahv;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Lahv;)Z
    .locals 2

    .prologue
    .line 68
    iget v0, p0, Lahv;->a:I

    iget v1, p1, Lahv;->a:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lahv;->b:I

    iget v1, p1, Lahv;->b:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lahv;Lahv;)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lahv;->a(Lahv;)Z

    move-result v0

    return v0
.end method
