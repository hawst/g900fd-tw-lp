.class public Lbvq;
.super Ljava/lang/Object;
.source "MoreTypes.java"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LbuP",
            "<*>;",
            "LbuP",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final a:[Ljava/lang/reflect/Type;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/reflect/Type;

    sput-object v0, Lbvq;->a:[Ljava/lang/reflect/Type;

    .line 53
    new-instance v0, LbmM;

    invoke-direct {v0}, LbmM;-><init>()V

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    .line 55
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Boolean;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    .line 56
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Byte;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    .line 57
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Short;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 58
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Integer;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    .line 59
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Long;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 60
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Float;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    .line 61
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Double;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    .line 62
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Character;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    .line 63
    invoke-static {v1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    const-class v2, Ljava/lang/Void;

    invoke-static {v2}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, Lbvq;->a:Ljava/util/Map;

    .line 53
    return-void
.end method

.method static synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 47
    invoke-static {p0}, Lbvq;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static a([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 305
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 306
    aget-object v1, p0, v0

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 307
    return v0

    .line 305
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 310
    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public static a(LbuP;)LbuP;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuP",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lbvq;->b(Ljava/lang/reflect/Type;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    new-instance v0, Lbvd;

    invoke-direct {v0}, Lbvd;-><init>()V

    invoke-virtual {v0, p0}, Lbvd;->a(LbuP;)Lbvd;

    move-result-object v0

    .line 80
    new-instance v1, Lbur;

    invoke-virtual {v0}, Lbvd;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Lbur;-><init>(Ljava/lang/Iterable;)V

    throw v1

    .line 83
    :cond_0
    invoke-virtual {p0}, LbuP;->a()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lbxw;

    if-ne v1, v2, :cond_2

    .line 84
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 90
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, LbwL;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 89
    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    .line 96
    :cond_1
    :goto_0
    return-object v0

    .line 95
    :cond_2
    sget-object v0, Lbvq;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuP;

    .line 96
    if-nez v0, :cond_1

    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/reflect/Type;)Ljava/lang/Class;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 152
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 154
    check-cast p0, Ljava/lang/Class;

    .line 174
    :goto_0
    return-object p0

    .line 156
    :cond_0
    instance-of v0, p0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 157
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 162
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 163
    instance-of v1, v0, Ljava/lang/Class;

    const-string v2, "Expected a Class, but <%s> is of type %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    const/4 v4, 0x1

    .line 164
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 163
    invoke-static {v1, v2, v3}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 165
    check-cast v0, Ljava/lang/Class;

    move-object p0, v0

    goto :goto_0

    .line 167
    :cond_1
    instance-of v0, p0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_2

    .line 168
    check-cast p0, Ljava/lang/reflect/GenericArrayType;

    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 169
    invoke-static {v0}, Lbvq;->a(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    goto :goto_0

    .line 171
    :cond_2
    instance-of v0, p0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_3

    .line 174
    const-class p0, Ljava/lang/Object;

    goto :goto_0

    .line 177
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a Class, ParameterizedType, or GenericArrayType, but <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> is of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 178
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/reflect/TypeVariable;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/TypeVariable;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 318
    invoke-interface {p0}, Ljava/lang/reflect/TypeVariable;->getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;

    move-result-object v0

    .line 319
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Class;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/reflect/Type;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .locals 4

    .prologue
    .line 125
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_2

    .line 126
    check-cast p0, Ljava/lang/Class;

    .line 127
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lbvs;

    invoke-virtual {p0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lbvq;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-direct {v0, v1}, Lbvs;-><init>(Ljava/lang/reflect/Type;)V

    :goto_0
    check-cast v0, Ljava/lang/reflect/Type;

    move-object p0, v0

    .line 147
    :cond_0
    :goto_1
    return-object p0

    :cond_1
    move-object v0, p0

    .line 127
    goto :goto_0

    .line 129
    :cond_2
    instance-of v0, p0, Lbvr;

    if-nez v0, :cond_0

    .line 132
    instance-of v0, p0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_3

    .line 133
    check-cast p0, Ljava/lang/reflect/ParameterizedType;

    .line 134
    new-instance v0, Lbvt;

    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 135
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lbvt;-><init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    move-object p0, v0

    goto :goto_1

    .line 137
    :cond_3
    instance-of v0, p0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_4

    .line 138
    check-cast p0, Ljava/lang/reflect/GenericArrayType;

    .line 139
    new-instance v0, Lbvs;

    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-direct {v0, v1}, Lbvs;-><init>(Ljava/lang/reflect/Type;)V

    move-object p0, v0

    goto :goto_1

    .line 141
    :cond_4
    instance-of v0, p0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_0

    .line 142
    check-cast p0, Ljava/lang/reflect/WildcardType;

    .line 143
    new-instance v0, Lbvu;

    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbvu;-><init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    move-object p0, v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Type;"
        }
    .end annotation

    .prologue
    .line 254
    if-ne p2, p1, :cond_0

    .line 284
    :goto_0
    return-object p0

    .line 259
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260
    invoke-virtual {p1}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v1

    .line 261
    const/4 v0, 0x0

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    .line 262
    aget-object v3, v1, v0

    if-ne v3, p2, :cond_1

    .line 263
    invoke-virtual {p1}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v1

    aget-object p0, v1, v0

    goto :goto_0

    .line 264
    :cond_1
    aget-object v3, v1, v0

    invoke-virtual {p2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 265
    invoke-virtual {p1}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v2

    aget-object v2, v2, v0

    aget-object v0, v1, v0

    invoke-static {v2, v0, p2}, Lbvq;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object p0

    goto :goto_0

    .line 261
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 271
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-nez v0, :cond_6

    .line 272
    :goto_2
    const-class v0, Ljava/lang/Object;

    if-eq p1, v0, :cond_6

    .line 273
    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 274
    if-ne v0, p2, :cond_4

    .line 275
    invoke-virtual {p1}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object p0

    goto :goto_0

    .line 276
    :cond_4
    invoke-virtual {p2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 277
    invoke-virtual {p1}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1, v0, p2}, Lbvq;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object p0

    goto :goto_0

    :cond_5
    move-object p1, v0

    .line 280
    goto :goto_2

    :cond_6
    move-object p0, p2

    .line 284
    goto :goto_0
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/TypeVariable;)Ljava/lang/reflect/Type;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/TypeVariable;",
            ")",
            "Ljava/lang/reflect/Type;"
        }
    .end annotation

    .prologue
    .line 288
    invoke-static {p2}, Lbvq;->a(Ljava/lang/reflect/TypeVariable;)Ljava/lang/Class;

    move-result-object v1

    .line 291
    if-nez v1, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-object p2

    .line 295
    :cond_1
    invoke-static {p0, p1, v1}, Lbvq;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 296
    instance-of v2, v0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v2, :cond_0

    .line 297
    invoke-virtual {v1}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v1

    invoke-static {v1, p2}, Lbvq;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 298
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object p2, v0, v1

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/reflect/Type;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-static {p0, p1}, Lbvq;->b(Ljava/lang/reflect/Type;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/reflect/Type;)Z
    .locals 1

    .prologue
    .line 47
    invoke-static {p0}, Lbvq;->b(Ljava/lang/reflect/Type;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 186
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 236
    :cond_0
    :goto_0
    return v1

    .line 190
    :cond_1
    instance-of v2, p0, Ljava/lang/Class;

    if-eqz v2, :cond_2

    .line 192
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 194
    :cond_2
    instance-of v2, p0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v2, :cond_4

    .line 195
    instance-of v2, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v2, :cond_0

    .line 200
    check-cast p0, Ljava/lang/reflect/ParameterizedType;

    .line 201
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    .line 202
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-static {v2, v3}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 203
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 204
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 206
    :cond_4
    instance-of v2, p0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v2, :cond_5

    .line 207
    instance-of v0, p1, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_0

    .line 211
    check-cast p0, Ljava/lang/reflect/GenericArrayType;

    .line 212
    check-cast p1, Ljava/lang/reflect/GenericArrayType;

    .line 213
    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v0, v1}, Lbvq;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v1

    goto :goto_0

    .line 215
    :cond_5
    instance-of v2, p0, Ljava/lang/reflect/WildcardType;

    if-eqz v2, :cond_7

    .line 216
    instance-of v2, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v2, :cond_0

    .line 220
    check-cast p0, Ljava/lang/reflect/WildcardType;

    .line 221
    check-cast p1, Ljava/lang/reflect/WildcardType;

    .line 222
    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 223
    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :goto_2
    move v1, v0

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_2

    .line 225
    :cond_7
    instance-of v2, p0, Ljava/lang/reflect/TypeVariable;

    if-eqz v2, :cond_0

    .line 226
    instance-of v2, p1, Ljava/lang/reflect/TypeVariable;

    if-eqz v2, :cond_0

    .line 229
    check-cast p0, Ljava/lang/reflect/TypeVariable;

    .line 230
    check-cast p1, Ljava/lang/reflect/TypeVariable;

    .line 231
    invoke-interface {p0}, Ljava/lang/reflect/TypeVariable;->getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 232
    invoke-interface {p0}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :goto_3
    move v1, v0

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method private static b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 241
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/reflect/Type;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 507
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Primitive types are not allowed in %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v1

    aput-object p0, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 509
    return-void

    :cond_1
    move v0, v1

    .line 507
    goto :goto_0
.end method

.method private static b(Ljava/lang/reflect/Type;)Z
    .locals 1

    .prologue
    .line 105
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    .line 108
    :cond_0
    instance-of v0, p0, Lbvr;

    if-eqz v0, :cond_1

    .line 109
    check-cast p0, Lbvr;

    invoke-interface {p0}, Lbvr;->a()Z

    move-result v0

    goto :goto_0

    .line 111
    :cond_1
    instance-of v0, p0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_2

    .line 112
    const/4 v0, 0x0

    goto :goto_0

    .line 115
    :cond_2
    invoke-static {p0}, Lbvq;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Lbvr;

    invoke-interface {v0}, Lbvr;->a()Z

    move-result v0

    goto :goto_0
.end method
