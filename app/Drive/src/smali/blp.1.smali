.class public abstract Lblp;
.super Lbqv;
.source "AbstractIterator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lbqv",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Lblr;

.field private a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lbqv;-><init>()V

    .line 65
    sget-object v0, Lblr;->b:Lblr;

    iput-object v0, p0, Lblp;->a:Lblr;

    .line 68
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 141
    sget-object v0, Lblr;->d:Lblr;

    iput-object v0, p0, Lblp;->a:Lblr;

    .line 142
    invoke-virtual {p0}, Lblp;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lblp;->a:Ljava/lang/Object;

    .line 143
    iget-object v0, p0, Lblp;->a:Lblr;

    sget-object v1, Lblr;->c:Lblr;

    if-eq v0, v1, :cond_0

    .line 144
    sget-object v0, Lblr;->a:Lblr;

    iput-object v0, p0, Lblp;->a:Lblr;

    .line 145
    const/4 v0, 0x1

    .line 147
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lblr;->c:Lblr;

    iput-object v0, p0, Lblp;->a:Lblr;

    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Lblp;->a:Lblr;

    sget-object v3, Lblr;->d:Lblr;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 130
    sget-object v0, Lblq;->a:[I

    iget-object v3, p0, Lblp;->a:Lblr;

    invoke-virtual {v3}, Lblr;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 137
    invoke-direct {p0}, Lblp;->a()Z

    move-result v2

    :goto_1
    :pswitch_0
    return v2

    :cond_0
    move v0, v2

    .line 129
    goto :goto_0

    :pswitch_1
    move v2, v1

    .line 134
    goto :goto_1

    .line 130
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0}, Lblp;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 154
    :cond_0
    sget-object v0, Lblr;->b:Lblr;

    iput-object v0, p0, Lblp;->a:Lblr;

    .line 155
    iget-object v0, p0, Lblp;->a:Ljava/lang/Object;

    return-object v0
.end method
