.class public final enum LamE;
.super Ljava/lang/Enum;
.source "RawPixelData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LamE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LamE;

.field private static final synthetic a:[LamE;

.field public static final enum b:LamE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, LamE;

    const-string v1, "STATIC"

    invoke-direct {v0, v1, v2}, LamE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LamE;->a:LamE;

    .line 23
    new-instance v0, LamE;

    const-string v1, "ANIMATED_GIF"

    invoke-direct {v0, v1, v3}, LamE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LamE;->b:LamE;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [LamE;

    sget-object v1, LamE;->a:LamE;

    aput-object v1, v0, v2

    sget-object v1, LamE;->b:LamE;

    aput-object v1, v0, v3

    sput-object v0, LamE;->a:[LamE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LamE;
    .locals 1

    .prologue
    .line 17
    const-class v0, LamE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LamE;

    return-object v0
.end method

.method public static values()[LamE;
    .locals 1

    .prologue
    .line 17
    sget-object v0, LamE;->a:[LamE;

    invoke-virtual {v0}, [LamE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LamE;

    return-object v0
.end method
