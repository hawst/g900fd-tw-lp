.class public LbhC;
.super Ljava/lang/Object;
.source "QuadsRenderer.java"

# interfaces
.implements Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;
.implements Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay$OverlayRenderer;


# instance fields
.field private a:F

.field private a:I

.field private final a:Landroid/graphics/Paint;

.field private final a:Landroid/view/View;

.field private a:LbhH;

.field private b:F

.field private b:I

.field private c:F

.field private c:I


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LbhC;->a:F

    .line 36
    iput v1, p0, LbhC;->b:F

    .line 39
    iput v1, p0, LbhC;->c:F

    .line 61
    iput-object p1, p0, LbhC;->a:Landroid/view/View;

    .line 62
    iput p2, p0, LbhC;->a:I

    .line 65
    const/16 v0, 0x5a

    if-ne p2, v0, :cond_1

    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LbhC;->b:F

    .line 75
    :cond_0
    :goto_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LbhC;->a:Landroid/graphics/Paint;

    .line 76
    iget-object v0, p0, LbhC;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 77
    iget-object v0, p0, LbhC;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 78
    iget-object v0, p0, LbhC;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    iget-object v0, p0, LbhC;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 80
    iget-object v0, p0, LbhC;->a:Landroid/graphics/Paint;

    const v1, 0x3c78d8

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v0, p0, LbhC;->a:Landroid/graphics/Paint;

    const/16 v1, 0xb2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 82
    return-void

    .line 67
    :cond_1
    const/16 v0, 0xb4

    if-ne p2, v0, :cond_2

    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LbhC;->b:F

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LbhC;->c:F

    goto :goto_0

    .line 70
    :cond_2
    const/16 v0, 0x10e

    if-ne p2, v0, :cond_0

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LbhC;->c:F

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, LbhC;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 163
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 164
    iget-object v0, p0, LbhC;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, LbhC;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 153
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LbhC;->a:LbhH;

    .line 154
    const/4 v0, 0x0

    iput v0, p0, LbhC;->b:I

    .line 155
    const/4 v0, 0x0

    iput v0, p0, LbhC;->c:I

    .line 156
    invoke-direct {p0}, LbhC;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    monitor-exit p0

    return-void

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onProcessingEnabled(Z)V
    .locals 1

    .prologue
    .line 144
    monitor-enter p0

    if-nez p1, :cond_0

    .line 145
    :try_start_0
    invoke-virtual {p0}, LbhC;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :cond_0
    monitor-exit p0

    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onQuadsChanged(LbhH;II)V
    .locals 1

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LbhC;->a:LbhH;

    .line 137
    iput p2, p0, LbhC;->b:I

    .line 138
    iput p3, p0, LbhC;->c:I

    .line 139
    invoke-direct {p0}, LbhC;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    monitor-exit p0

    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized render(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x0

    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LbhC;->a:LbhH;

    if-eqz v0, :cond_3

    .line 87
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 88
    iget v0, p0, LbhC;->b:F

    iget v1, p0, LbhC;->c:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 91
    iget v0, p0, LbhC;->a:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 92
    iget v0, p0, LbhC;->a:F

    iget v1, p0, LbhC;->a:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 94
    iget-object v0, p0, LbhC;->a:LbhH;

    invoke-interface {v0}, LbhH;->getVertices()[Landroid/graphics/PointF;

    move-result-object v8

    move v7, v6

    .line 95
    :goto_0
    if-ge v7, v9, :cond_2

    .line 96
    add-int/lit8 v0, v7, 0x1

    if-ne v0, v9, :cond_1

    move v0, v6

    .line 97
    :goto_1
    aget-object v2, v8, v7

    .line 98
    aget-object v0, v8, v0

    .line 99
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 100
    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, LbhC;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 95
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 96
    :cond_1
    add-int/lit8 v0, v7, 0x1

    goto :goto_1

    .line 103
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_3
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
