.class Lalz;
.super Ljava/lang/Object;
.source "FutureUtils.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LalB;

.field final synthetic a:Laly;


# direct methods
.method constructor <init>(Laly;LalB;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lalz;->a:Laly;

    iput-object p2, p0, Lalz;->a:LalB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lalz;->a:Laly;

    invoke-static {v0, p1}, Laly;->a(Laly;Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lalz;->a:Laly;

    invoke-virtual {v0}, Laly;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 72
    :cond_0
    :try_start_0
    iget-object v0, p0, Lalz;->a:Laly;

    iget-object v1, p0, Lalz;->a:LalB;

    invoke-interface {v1, p1}, LalB;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v1

    invoke-static {v0, v1}, Laly;->a(Laly;LbsU;)LbsU;

    .line 73
    iget-object v0, p0, Lalz;->a:Laly;

    invoke-virtual {v0}, Laly;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lalz;->a:Laly;

    invoke-static {v0}, Laly;->a(Laly;)LbsU;

    move-result-object v0

    iget-object v1, p0, Lalz;->a:Laly;

    invoke-static {v1}, Laly;->a(Laly;)Z

    move-result v1

    invoke-interface {v0, v1}, LbsU;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    iget-object v1, p0, Lalz;->a:Laly;

    invoke-static {v1, v0}, Laly;->b(Laly;Ljava/lang/Throwable;)Z

    goto :goto_0

    .line 77
    :cond_1
    :try_start_1
    iget-object v0, p0, Lalz;->a:Laly;

    invoke-static {v0}, Laly;->a(Laly;)LbsU;

    move-result-object v0

    new-instance v1, LalA;

    invoke-direct {v1, p0}, LalA;-><init>(Lalz;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbsJ;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
