.class public Ladg;
.super Ljava/lang/Object;
.source "DocumentContentSpecCrypter.java"


# instance fields
.field private final a:Lakz;

.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lakz;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Ladg;->a:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Ladg;->a:Lakz;

    .line 59
    return-void
.end method

.method private a()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Ladg;->a:Landroid/content/Context;

    const-string v1, "DocumentContentSpecCrypter.KeyStore"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/SharedPreferences;)Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 63
    const-string v1, "algorithm"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    const-string v2, "encodedForm"

    invoke-interface {p1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 65
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-object v0

    .line 69
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v2

    .line 70
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, v2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljavax/crypto/SecretKey;Landroid/content/SharedPreferences;)Z
    .locals 4

    .prologue
    .line 78
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 81
    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 83
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AES"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, LbiT;->b(Z)V

    .line 84
    const-string v2, "algorithm"

    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 85
    const-string v2, "encodedForm"

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 86
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    .line 114
    const-class v1, Ladg;

    monitor-enter v1

    .line 115
    :try_start_0
    invoke-direct {p0}, Ladg;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 116
    invoke-direct {p0, v2}, Ladg;->a(Landroid/content/SharedPreferences;)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    monitor-exit v1

    .line 125
    :goto_0
    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Ladg;->a:Lakz;

    invoke-interface {v0}, Lakz;->a()Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 122
    invoke-direct {p0, v0, v2}, Ladg;->a(Ljavax/crypto/SecretKey;Landroid/content/SharedPreferences;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 123
    new-instance v0, LQm;

    invoke-direct {v0}, LQm;-><init>()V

    throw v0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 125
    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
