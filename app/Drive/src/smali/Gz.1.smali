.class public LGz;
.super Ljava/lang/Object;
.source "LocalFileIntentOpener.java"

# interfaces
.implements LDL;


# instance fields
.field private final a:LFT;

.field private final a:LaGo;

.field private final a:LacY;

.field private a:Lamr;

.field private final a:Landroid/os/Bundle;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LFT;LaGo;LacY;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, LGz;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p3, p0, LGz;->a:LaGo;

    .line 48
    iput-object p4, p0, LGz;->a:LacY;

    .line 49
    iput-object p2, p0, LGz;->a:LFT;

    .line 50
    iput-object p5, p0, LGz;->a:Landroid/os/Bundle;

    .line 51
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 6

    .prologue
    .line 55
    iget-object v0, p0, LGz;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    iget-object v1, p0, LGz;->a:LFT;

    iget-object v2, p0, LGz;->a:LaGo;

    iget-object v3, p0, LGz;->a:LacY;

    iget-object v4, p0, LGz;->a:Landroid/os/Bundle;

    iget-object v5, p0, LGz;->a:Lamr;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LFT;LaGo;LacY;Landroid/os/Bundle;Lamr;)V

    .line 56
    const/4 v0, -0x1

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, LGz;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->opening_document:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LGz;->a:LaGo;

    invoke-interface {v3}, LaGo;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lamr;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, LGz;->a:Lamr;

    .line 73
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, LGz;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Ladb;

    move-result-object v0

    iget-object v1, p0, LGz;->a:LaGo;

    iget-object v2, p0, LGz;->a:LacY;

    invoke-interface {v0, v1, v2}, Ladb;->c(LaGo;LacY;)Z

    move-result v0

    return v0
.end method
