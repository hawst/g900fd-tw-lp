.class Ljn;
.super Lji;
.source "MenuItemWrapperJB.java"

# interfaces
.implements Ldh;


# instance fields
.field a:Landroid/view/ActionProvider$VisibilityListener;

.field final synthetic a:Ljm;


# direct methods
.method public constructor <init>(Ljm;Ldf;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Ljn;->a:Ljm;

    .line 39
    invoke-direct {p0, p1, p2}, Lji;-><init>(Ljh;Ldf;)V

    .line 40
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ljn;->a:Landroid/view/ActionProvider$VisibilityListener;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Ljn;->a:Landroid/view/ActionProvider$VisibilityListener;

    invoke-interface {v0, p1}, Landroid/view/ActionProvider$VisibilityListener;->onActionProviderVisibilityChanged(Z)V

    .line 74
    :cond_0
    return-void
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ljn;->a:Ldf;

    invoke-virtual {v0}, Ldf;->b()Z

    move-result v0

    return v0
.end method

.method public onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ljn;->a:Ldf;

    invoke-virtual {v0, p1}, Ldf;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public overridesItemVisibility()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Ljn;->a:Ldf;

    invoke-virtual {v0}, Ldf;->a()Z

    move-result v0

    return v0
.end method

.method public refreshVisibility()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ljn;->a:Ldf;

    invoke-virtual {v0}, Ldf;->a()V

    .line 60
    return-void
.end method

.method public setVisibilityListener(Landroid/view/ActionProvider$VisibilityListener;)V
    .locals 1

    .prologue
    .line 65
    iput-object p1, p0, Ljn;->a:Landroid/view/ActionProvider$VisibilityListener;

    .line 66
    iget-object v0, p0, Ljn;->a:Ldf;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p0}, Ldf;->a(Ldh;)V

    .line 67
    return-void

    .line 66
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
