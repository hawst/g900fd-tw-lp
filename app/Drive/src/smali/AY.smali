.class public LAY;
.super Ljava/lang/Object;
.source "DocListCursorsContainerLoaderImpl.java"

# interfaces
.implements LAX;


# instance fields
.field private final a:LVm;

.field private final a:LaGM;

.field a:LbiP;
    .annotation runtime LQP;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LAX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGM;LVm;LbiP;)V
    .locals 0
    .param p3    # LbiP;
        .annotation runtime LQP;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "LVm;",
            "LbiP",
            "<",
            "LAX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, LAY;->a:LaGM;

    .line 35
    iput-object p2, p0, LAY;->a:LVm;

    .line 36
    iput-object p3, p0, LAY;->a:LbiP;

    .line 37
    return-void
.end method

.method private a(LaGA;LbmY;)LaGA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGA;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)",
            "LaGA;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p2}, LbmY;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, LRg;

    invoke-direct {v0, p1, p2}, LRg;-><init>(LaGA;LbmY;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/docs/doclist/DocListQuery;)LaFX;
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, LAY;->a:LaGM;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/doclist/DocListQuery;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/docs/doclist/DocListQuery;->a()LIK;

    move-result-object v2

    .line 59
    invoke-virtual {p1}, Lcom/google/android/apps/docs/doclist/DocListQuery;->a()[Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-interface {v0, v1, v2, v3}, LaGM;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;)LaGA;

    move-result-object v0

    .line 60
    iget-object v1, p0, LAY;->a:LVm;

    .line 61
    invoke-virtual {v1}, LVm;->a()LbmY;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LAY;->a(LaGA;LbmY;)LaGA;

    move-result-object v0

    .line 60
    invoke-static {v0}, LaHc;->a(LaGA;)LaHc;

    move-result-object v0

    .line 62
    const-class v1, LaGA;

    invoke-static {v1, v0}, LaFX;->a(Ljava/lang/Class;LaFW;)LaFX;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/doclist/DocListQuery;)LaFX;
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1}, LAY;->b(Lcom/google/android/apps/docs/doclist/DocListQuery;)LaFX;

    move-result-object v1

    .line 43
    iget-object v0, p0, LAY;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    :try_start_0
    iget-object v0, p0, LAY;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAX;

    invoke-interface {v0, p1}, LAX;->a(Lcom/google/android/apps/docs/doclist/DocListQuery;)LaFX;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 51
    invoke-virtual {v1, v0}, LaFX;->a(LaFX;)LaFX;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    .line 47
    :catch_0
    move-exception v0

    .line 48
    invoke-virtual {v1}, LaFX;->a()V

    .line 49
    throw v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
