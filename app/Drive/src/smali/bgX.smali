.class public LbgX;
.super Ljava/lang/Object;
.source "Document.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private final transient a:Lbhd;

.field private a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LbgY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, LbgX;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    .line 18
    new-instance v0, Lbhd;

    invoke-direct {v0}, Lbhd;-><init>()V

    iput-object v0, p0, LbgX;->a:Lbhd;

    .line 28
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a()J
    .locals 5

    .prologue
    .line 149
    const-wide/16 v0, 0x0

    .line 150
    iget-object v2, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbgY;

    .line 151
    invoke-virtual {v0}, LbgY;->a()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 152
    goto :goto_0

    .line 153
    :cond_0
    return-wide v2
.end method

.method public a(I)LbgY;
    .locals 1

    .prologue
    .line 126
    if-ltz p1, :cond_0

    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 127
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbgY;

    .line 129
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, LbgX;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    return-object v0
.end method

.method public a(LbgY;)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, LbgX;->a:Lbhd;

    invoke-virtual {p1}, LbgY;->b()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbhd;->a(Ljava/lang/String;)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 53
    sget-object v0, LbgX;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Clearing pages and document"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbgY;

    .line 55
    invoke-virtual {v0}, LbgY;->a()V

    goto :goto_0

    .line 57
    :cond_0
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 58
    iget-object v0, p0, LbgX;->a:Lbhd;

    invoke-virtual {v0}, Lbhd;->a()V

    .line 59
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, LbgX;->a(I)LbgY;

    move-result-object v0

    invoke-virtual {v0}, LbgY;->a()V

    .line 116
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 117
    return-void
.end method

.method public a(ILbgY;)V
    .locals 1

    .prologue
    .line 78
    if-eqz p2, :cond_0

    .line 79
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 81
    :cond_0
    return-void
.end method

.method public a(LbgY;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, LbgX;->a:Lbhd;

    invoke-virtual {p1}, LbgY;->b()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbhd;->a(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, LbgX;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 163
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public b(ILbgY;)V
    .locals 2

    .prologue
    .line 101
    if-eqz p2, :cond_0

    .line 102
    invoke-virtual {p0, p1, p2}, LbgX;->a(ILbgY;)V

    .line 103
    invoke-virtual {p0}, LbgX;->a()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 104
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, LbgX;->a(I)V

    .line 107
    :cond_0
    return-void
.end method

.method public b(LbgY;)V
    .locals 1

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 90
    iget-object v0, p0, LbgX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    return-void
.end method
