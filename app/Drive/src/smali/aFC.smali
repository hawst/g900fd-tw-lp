.class LaFC;
.super Ljava/lang/Object;
.source "DatabaseTable.java"

# interfaces
.implements LaFB;


# instance fields
.field private a:J

.field private final a:LaFx;


# direct methods
.method public constructor <init>(LaFx;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFx;

    iput-object v0, p0, LaFC;->a:LaFx;

    .line 51
    return-void
.end method


# virtual methods
.method public declared-synchronized a()J
    .locals 2

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LaFC;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LaFC;->a:LaFx;

    invoke-interface {v0}, LaFx;->c()V

    .line 76
    return-void
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 55
    .line 56
    iget-object v0, p0, LaFC;->a:LaFx;

    invoke-interface {v0}, LaFx;->b()V

    .line 58
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 59
    :try_start_1
    iget-wide v0, p0, LaFC;->a:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 60
    iget-wide v0, p0, LaFC;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LaFC;->a:J

    .line 64
    monitor-exit p0

    .line 71
    return-void

    .line 62
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Database row has been changed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67
    :catchall_1
    move-exception v0

    .line 68
    iget-object v1, p0, LaFC;->a:LaFx;

    invoke-interface {v1}, LaFx;->c()V

    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LaFC;->a:LaFx;

    invoke-interface {v0}, LaFx;->d()V

    .line 86
    return-void
.end method
