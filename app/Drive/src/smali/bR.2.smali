.class public final enum LbR;
.super Ljava/lang/Enum;
.source "ModernAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbR;

.field private static final synthetic a:[LbR;

.field public static final enum b:LbR;

.field public static final enum c:LbR;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, LbR;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, LbR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbR;->a:LbR;

    .line 97
    new-instance v0, LbR;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, LbR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbR;->b:LbR;

    .line 101
    new-instance v0, LbR;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, LbR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbR;->c:LbR;

    .line 89
    const/4 v0, 0x3

    new-array v0, v0, [LbR;

    sget-object v1, LbR;->a:LbR;

    aput-object v1, v0, v2

    sget-object v1, LbR;->b:LbR;

    aput-object v1, v0, v3

    sget-object v1, LbR;->c:LbR;

    aput-object v1, v0, v4

    sput-object v0, LbR;->a:[LbR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbR;
    .locals 1

    .prologue
    .line 89
    const-class v0, LbR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbR;

    return-object v0
.end method

.method public static values()[LbR;
    .locals 1

    .prologue
    .line 89
    sget-object v0, LbR;->a:[LbR;

    invoke-virtual {v0}, [LbR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbR;

    return-object v0
.end method
