.class public LYp;
.super Landroid/webkit/WebViewClient;
.source "PunchSvgWebView.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LYh;)V
    .locals 0

    .prologue
    .line 368
    invoke-direct {p0, p1}, LYp;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 371
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "in onPageFinished view=%s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 375
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 376
    const-string v0, "about:blank"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "discarding %s"

    new-array v2, v3, [Ljava/lang/Object;

    const-string v3, "about:blank"

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    iget-object v0, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    sget-object v1, LYq;->c:LYq;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LYq;)LYq;

    .line 384
    iget-object v0, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    goto :goto_0
.end method

.method public onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 394
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    .line 395
    iget-object v0, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0, p3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;F)F

    .line 400
    float-to-double v0, p2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    float-to-double v0, p3

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 401
    iget-object v0, p0, LYp;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    .line 403
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 407
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 408
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 410
    :try_start_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 419
    :goto_0
    return v3

    .line 411
    :catch_0
    move-exception v0

    goto :goto_0
.end method
