.class public final Lzc;
.super LyR;
.source "InformationCard.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:LaGv;

.field private final a:LabF;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/View;

.field private a:Ljava/lang/Long;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/Date;

.field private a:Z

.field private b:Ljava/lang/String;

.field private b:Ljava/util/Date;

.field private c:Ljava/lang/String;

.field private c:Ljava/util/Date;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LabF;LajM;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 49
    invoke-direct {p0}, LyR;-><init>()V

    .line 50
    iput-object p1, p0, Lzc;->a:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lzc;->a:LabF;

    .line 53
    const-string v0, "layout_inflater"

    .line 54
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 55
    sget v1, Lxe;->detail_card_information:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lzc;->a:Landroid/view/View;

    .line 56
    invoke-virtual {p3}, LajM;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iput-boolean v3, p0, Lzc;->a:Z

    .line 58
    iget-object v0, p0, Lzc;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 60
    :cond_0
    return-void
.end method

.method private a(IIILjava/util/Date;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lzc;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 160
    if-nez p4, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 162
    if-nez p4, :cond_1

    .line 171
    :goto_1
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lzc;->a:LabF;

    sget-object v1, Lqx;->a:Lqx;

    invoke-interface {v0, p5, v1}, LabF;->a(Ljava/lang/String;Lqx;)LabD;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_2

    invoke-interface {v0}, LabD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 168
    invoke-interface {v0}, LabD;->b()Ljava/lang/String;

    move-result-object p5

    .line 170
    :cond_2
    invoke-direct {p0, p2, p3, p4, p5}, Lzc;->a(IILjava/util/Date;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(IILjava/util/Date;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 174
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 175
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 176
    new-instance v1, LamF;

    iget-object v2, p0, Lzc;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, LamF;-><init>(Landroid/content/Context;Landroid/text/format/Time;)V

    .line 177
    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LamF;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 179
    if-eqz p4, :cond_0

    .line 180
    iget-object v1, p0, Lzc;->a:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p4, v2, v0

    invoke-virtual {v1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 182
    :goto_0
    iget-object v0, p0, Lzc;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 183
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 117
    iget-object v0, p0, Lzc;->a:Landroid/view/View;

    sget v1, Lxc;->size_and_kind:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    iget-object v1, p0, Lzc;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    move v1, v2

    .line 121
    :goto_0
    iget-object v4, p0, Lzc;->a:Landroid/content/Context;

    iget-object v5, p0, Lzc;->a:LaGv;

    iget-object v6, p0, Lzc;->a:Ljava/lang/String;

    .line 122
    invoke-static {v5, v6}, LaGt;->b(LaGv;Ljava/lang/String;)I

    move-result v5

    .line 121
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 123
    if-eqz v1, :cond_1

    .line 124
    iget-object v1, p0, Lzc;->a:Landroid/content/Context;

    const/4 v5, -0x1

    iget-object v6, p0, Lzc;->a:Ljava/lang/Long;

    invoke-static {v1, v5, v6}, Lalr;->a(Landroid/content/Context;ILjava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    .line 125
    iget-object v5, p0, Lzc;->a:Landroid/content/Context;

    sget v6, Lxi;->detail_preview_type_with_size:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v3

    aput-object v1, v7, v2

    .line 126
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :goto_1
    return-void

    :cond_0
    move v1, v3

    .line 119
    goto :goto_0

    .line 128
    :cond_1
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, Lzc;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 134
    :goto_0
    if-eqz v2, :cond_0

    .line 135
    iget-object v0, p0, Lzc;->a:Landroid/view/View;

    sget v3, Lxc;->location:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    iget-object v3, p0, Lzc;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    :cond_0
    iget-object v0, p0, Lzc;->a:Landroid/view/View;

    sget v3, Lxc;->location_row:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 139
    if-eqz v2, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 140
    return-void

    :cond_1
    move v2, v1

    .line 133
    goto :goto_0

    .line 139
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private d()V
    .locals 6

    .prologue
    .line 143
    sget v1, Lxc;->created_row:I

    sget v2, Lxc;->created:I

    sget v3, Lxi;->detail_preview_date_performed_by:I

    iget-object v4, p0, Lzc;->a:Ljava/util/Date;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lzc;->a(IIILjava/util/Date;Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 148
    sget v1, Lxc;->modified_row:I

    sget v2, Lxc;->modified:I

    sget v3, Lxi;->detail_preview_date_performed_by:I

    iget-object v4, p0, Lzc;->b:Ljava/util/Date;

    iget-object v5, p0, Lzc;->c:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lzc;->a(IIILjava/util/Date;Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 153
    sget v1, Lxc;->opened_row:I

    sget v2, Lxc;->opened:I

    sget v3, Lxi;->detail_preview_date_performed_by:I

    iget-object v4, p0, Lzc;->c:Ljava/util/Date;

    iget-object v5, p0, Lzc;->d:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lzc;->a(IIILjava/util/Date;Ljava/lang/String;)V

    .line 155
    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lzc;->a:Landroid/view/View;

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lzc;->b()V

    .line 110
    invoke-direct {p0}, Lzc;->c()V

    .line 111
    invoke-direct {p0}, Lzc;->d()V

    .line 112
    invoke-direct {p0}, Lzc;->e()V

    .line 113
    invoke-direct {p0}, Lzc;->f()V

    .line 114
    return-void
.end method

.method public a(LaGu;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 68
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    .line 69
    invoke-interface {p1}, LaGu;->g()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-interface {p1}, LaGu;->a()Ljava/util/Date;

    move-result-object v5

    .line 73
    invoke-interface {p1}, LaGu;->b()Ljava/util/Date;

    move-result-object v6

    .line 74
    invoke-interface {p1}, LaGu;->e()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-object v3, p3

    move-object v4, p2

    move-object v9, v8

    .line 68
    invoke-virtual/range {v0 .. v9}, Lzc;->a(LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public a(LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 95
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGv;

    iput-object v0, p0, Lzc;->a:LaGv;

    .line 96
    iput-object p2, p0, Lzc;->a:Ljava/lang/String;

    .line 97
    iput-object p3, p0, Lzc;->a:Ljava/lang/Long;

    .line 98
    iput-object p4, p0, Lzc;->b:Ljava/lang/String;

    .line 99
    iput-object p5, p0, Lzc;->a:Ljava/util/Date;

    .line 100
    iput-object p6, p0, Lzc;->b:Ljava/util/Date;

    .line 101
    iput-object p7, p0, Lzc;->c:Ljava/lang/String;

    .line 102
    iput-object p8, p0, Lzc;->c:Ljava/util/Date;

    .line 103
    iput-object p9, p0, Lzc;->d:Ljava/lang/String;

    .line 105
    invoke-virtual {p0}, Lzc;->a()V

    .line 106
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lzc;->a:Z

    return v0
.end method
