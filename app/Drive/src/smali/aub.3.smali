.class Laub;
.super Ljava/lang/Object;
.source "ProjectorChrome.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# instance fields
.field final synthetic a:Laua;


# direct methods
.method constructor <init>(Laua;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Laub;->a:Laua;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 69
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Laub;->a:Laua;

    invoke-static {v0, v4}, Laua;->a(Laua;Z)Z

    .line 77
    :cond_0
    :goto_0
    const-string v0, "ProjectorChrome"

    const-string v1, "Visibility changed to %s [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Laub;->a:Laua;

    invoke-static {v3}, Laua;->a(Laua;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Laub;->a:Laua;

    invoke-static {v0, v5}, Laua;->a(Laua;Z)Z

    .line 73
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Laub;->a:Laua;

    invoke-virtual {v0}, Laua;->a()V

    goto :goto_0
.end method
