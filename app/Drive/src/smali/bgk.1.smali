.class public Lbgk;
.super Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;
.source "CaptureActivity.java"


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/CaptureActivity;

.field private final a:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 1

    .prologue
    .line 392
    iput-object p1, p0, Lbgk;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;-><init>()V

    .line 393
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbgk;->a:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lbgk;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/ui/DebugView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 398
    iget-object v0, p0, Lbgk;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/ui/DebugView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->invalidate()V

    .line 400
    :cond_0
    return-void
.end method

.method public getDebugText()Ljava/util/Vector;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404
    iget-object v0, p0, Lbgk;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 405
    iget-object v0, p0, Lbgk;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Capture state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbgk;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v2}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lbgn;

    move-result-object v2

    invoke-virtual {v2}, Lbgn;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 406
    iget-object v0, p0, Lbgk;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Camera state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbgk;->a:Lcom/google/bionics/scanner/CaptureActivity;

    invoke-static {v2}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getStateName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 407
    iget-object v0, p0, Lbgk;->a:Ljava/util/Vector;

    return-object v0
.end method
