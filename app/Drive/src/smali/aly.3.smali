.class Laly;
.super LbsC;
.source "FutureUtils.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LbsC",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private volatile a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<+TV;>;"
        }
    .end annotation
.end field

.field private volatile a:Z


# direct methods
.method constructor <init>(LbsU;LalB;Ljava/util/concurrent/Executor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsU",
            "<+TV;>;",
            "LalB",
            "<+TV;>;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, LbsC;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Laly;->a:Z

    .line 59
    iput-object p1, p0, Laly;->a:LbsU;

    .line 60
    iget-object v0, p0, Laly;->a:LbsU;

    new-instance v1, Lalz;

    invoke-direct {v1, p0, p2}, Lalz;-><init>(Laly;LalB;)V

    invoke-static {v0, v1, p3}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 97
    return-void
.end method

.method static synthetic a(Laly;)LbsU;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laly;->a:LbsU;

    return-object v0
.end method

.method static synthetic a(Laly;LbsU;)LbsU;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Laly;->a:LbsU;

    return-object p1
.end method

.method static synthetic a(Laly;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Laly;->a:Z

    return v0
.end method

.method static synthetic a(Laly;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Laly;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Laly;Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Laly;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Laly;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Laly;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Laly;Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Laly;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1

    .prologue
    .line 101
    iput-boolean p1, p0, Laly;->a:Z

    .line 102
    invoke-super {p0, p1}, LbsC;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Laly;->a:LbsU;

    invoke-interface {v0, p1}, LbsU;->cancel(Z)Z

    .line 104
    const/4 v0, 0x1

    .line 106
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
