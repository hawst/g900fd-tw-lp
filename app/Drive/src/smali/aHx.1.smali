.class public LaHx;
.super Ljava/lang/Object;
.source "OperationQueueRateLimiterFactory.java"

# interfaces
.implements LZT;


# instance fields
.field private final a:LQr;


# direct methods
.method public constructor <init>(LQr;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, LaHx;->a:LQr;

    .line 29
    return-void
.end method


# virtual methods
.method public a()LZS;
    .locals 8

    .prologue
    .line 33
    new-instance v1, LZQ;

    iget-object v0, p0, LaHx;->a:LQr;

    const-string v2, "multiOperationQueueMinWait"

    const/16 v3, 0x3e8

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    iget-object v0, p0, LaHx;->a:LQr;

    const-string v4, "multiOperationQueueWaitGrowthFactor"

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    .line 35
    invoke-interface {v0, v4, v6, v7}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v4

    iget-object v0, p0, LaHx;->a:LQr;

    const-string v6, "multiOperationQueueMaxWait"

    const v7, 0xea60

    .line 37
    invoke-interface {v0, v6, v7}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v6, v0

    invoke-direct/range {v1 .. v7}, LZQ;-><init>(JDJ)V

    return-object v1
.end method
