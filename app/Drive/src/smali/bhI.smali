.class public LbhI;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Landroid/hardware/Camera$ShutterCallback;


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, LbhI;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShutter()V
    .locals 3

    .prologue
    .line 462
    iget-object v1, p0, LbhI;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    monitor-enter v1

    .line 463
    :try_start_0
    iget-object v0, p0, LbhI;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;I)I

    .line 464
    iget-object v0, p0, LbhI;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$ShutterCallback;

    .line 465
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$ShutterCallback;->onShutter()V

    goto :goto_0

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 467
    :cond_0
    :try_start_1
    iget-object v0, p0, LbhI;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 468
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 469
    return-void
.end method
