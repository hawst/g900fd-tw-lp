.class public abstract LaIC;
.super Ljava/lang/Object;
.source "Guid.java"


# instance fields
.field private final a:J


# direct methods
.method protected constructor <init>(J)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-wide p1, p0, LaIC;->a:J

    .line 32
    return-void
.end method

.method protected static final a(Z[Ljava/lang/String;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LaGM;)LaHY;
    .locals 6

    .prologue
    .line 83
    const/4 v5, 0x0

    move v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LaIC;->a(Z[Ljava/lang/String;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LaGM;Ljava/lang/String;)LaHY;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Z[Ljava/lang/String;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LaGM;Ljava/lang/String;)LaHY;
    .locals 1

    .prologue
    .line 98
    .line 99
    :try_start_0
    invoke-static {}, LaHU;->a()[Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-interface {p4, p2, p3, v0, p5}, LaGM;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;Ljava/lang/String;)LaGA;

    move-result-object v0

    .line 101
    invoke-static {v0, p1, p4, p0}, LaHU;->a(LaGA;[Ljava/lang/String;LaGM;Z)LaHY;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    .line 103
    :catch_0
    move-exception v0

    .line 105
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, LaIC;->a:J

    return-wide v0
.end method

.method public a(LaGM;)LaFM;
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, LaIC;->a()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, LaGM;->a(J)LaFM;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()LaGo;
.end method

.method public abstract a(Z[Ljava/lang/String;LIK;Landroid/net/Uri;)LaHY;
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaIq;)LaIA;
    .locals 2

    .prologue
    .line 121
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "not supported"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract a(Z[Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 130
    if-ne p0, p1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v0

    .line 133
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 134
    goto :goto_0

    .line 136
    :cond_2
    check-cast p1, LaIC;

    .line 137
    iget-wide v2, p0, LaIC;->a:J

    iget-wide v4, p1, LaIC;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 142
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, LaIC;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 147
    const-string v0, "Guid[rootId=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, LaIC;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
