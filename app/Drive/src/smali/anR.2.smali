.class public LanR;
.super Ljava/lang/Object;
.source "ChainedImageDecodeFetcher.java"


# instance fields
.field private final a:LQr;

.field private final a:LanU;

.field private final a:Ltp;


# direct methods
.method public constructor <init>(LQr;LanU;Ltp;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, LanR;->a:LQr;

    .line 49
    iput-object p2, p0, LanR;->a:LanU;

    .line 50
    iput-object p3, p0, LanR;->a:Ltp;

    .line 51
    return-void
.end method


# virtual methods
.method public a()LanP;
    .locals 6

    .prologue
    const/4 v3, 0x3

    .line 54
    iget-object v0, p0, LanR;->a:LQr;

    const-string v1, "projectorImageDecoderQueueSize"

    invoke-interface {v0, v1, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 56
    iget-object v1, p0, LanR;->a:LQr;

    const-string v2, "projectorImageDecoderThreadPoolSize"

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v1

    .line 63
    const-wide/16 v2, 0x0

    .line 64
    invoke-static {v2, v3, v0}, LakH;->a(JI)LakH;

    move-result-object v0

    .line 65
    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v4

    .line 69
    invoke-static {v1}, Lalg;->a(I)LbsW;

    move-result-object v1

    .line 68
    invoke-static {v1, v0}, LalE;->a(Ljava/util/concurrent/ExecutorService;Lalf;)LalE;

    move-result-object v0

    .line 70
    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v3

    .line 72
    new-instance v5, LaoX;

    invoke-direct {v5, v0}, LaoX;-><init>(LalD;)V

    .line 74
    iget-object v0, p0, LanR;->a:LanU;

    .line 75
    invoke-virtual {v0}, LanU;->b()LanS;

    move-result-object v2

    .line 76
    new-instance v1, LaoF;

    iget-object v0, p0, LanR;->a:Ltp;

    invoke-direct {v1, v0, v5, v2}, LaoF;-><init>(Ltp;LaoX;Laoo;)V

    .line 78
    new-instance v0, LanP;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LanP;-><init>(Laoo;LanS;Ljava/util/List;Ljava/util/List;LanQ;)V

    return-object v0
.end method
