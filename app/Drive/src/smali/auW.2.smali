.class public LauW;
.super Ljava/lang/Object;
.source "DisplayData.java"


# static fields
.field private static final a:Ljava/nio/charset/Charset;


# instance fields
.field private final a:Lave;

.field public final a:Lavg;

.field public final a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LauW;->a:Ljava/nio/charset/Charset;

    return-void
.end method

.method private constructor <init>(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lave;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavg;

    iput-object v0, p0, LauW;->a:Lavg;

    .line 73
    invoke-static {p2}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iput-object v0, p0, LauW;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    .line 74
    invoke-static {p3}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lave;

    iput-object v0, p0, LauW;->a:Lave;

    .line 75
    return-void
.end method

.method public static a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;
    .locals 3

    .prologue
    .line 60
    new-instance v0, LauW;

    new-instance v1, Lavc;

    iget-object v2, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {v1, v2}, Lavc;-><init>(Landroid/net/Uri;)V

    invoke-direct {v0, p0, p1, v1}, LauW;-><init>(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lave;)V

    return-object v0
.end method

.method public static a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Landroid/content/ContentResolver;)LauW;
    .locals 3

    .prologue
    .line 56
    new-instance v0, LauW;

    new-instance v1, LauZ;

    iget-object v2, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {v1, p2, v2}, LauZ;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    invoke-direct {v0, p0, p1, v1}, LauW;-><init>(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lave;)V

    return-object v0
.end method

.method public static a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;[B)LauW;
    .locals 2

    .prologue
    .line 64
    new-instance v0, LauW;

    new-instance v1, LauY;

    invoke-direct {v1, p2}, LauY;-><init>([B)V

    invoke-direct {v0, p0, p1, v1}, LauW;-><init>(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lave;)V

    return-object v0
.end method

.method public static b(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;
    .locals 3

    .prologue
    .line 68
    new-instance v0, LauW;

    new-instance v1, Lavd;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lavd;-><init>(LauX;)V

    invoke-direct {v0, p0, p1, v1}, LauW;-><init>(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lave;)V

    return-object v0
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    const-string v1, "t"

    iget-object v2, p0, LauW;->a:Lavg;

    invoke-virtual {v2}, Lavg;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v1, "url"

    iget-object v2, p0, LauW;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 81
    const-string v1, "o"

    iget-object v2, p0, LauW;->a:Lave;

    invoke-interface {v2}, Lave;->a()Lavf;

    move-result-object v2

    invoke-virtual {v2}, Lavf;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    return-object v0
.end method

.method public a()Landroid/os/ParcelFileDescriptor;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, LauW;->a:Lave;

    invoke-interface {v0}, Lave;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 94
    const-string v0, "Display Data [%s] %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LauW;->a:Lavg;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LauW;->a:Lave;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
