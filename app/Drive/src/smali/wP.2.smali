.class public final LwP;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LwO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 38
    iput-object p1, p0, LwP;->a:LbrA;

    .line 39
    const-class v0, LwO;

    const-class v1, LbuO;

    invoke-static {v0, v1}, LwP;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LwP;->a:Lbsk;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 87
    packed-switch p1, :pswitch_data_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :pswitch_0
    new-instance v0, LwO;

    invoke-direct {v0}, LwO;-><init>()V

    .line 91
    return-object v0

    .line 87
    :pswitch_data_0
    .packed-switch 0x53e
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 126
    .line 128
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 67
    const-class v0, Lcom/google/android/apps/docs/capture/DocScannerActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb1

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LwP;->a(LbuP;LbuB;)V

    .line 70
    const-class v0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb3

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LwP;->a(LbuP;LbuB;)V

    .line 73
    const-class v0, LwN;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb2

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LwP;->a(LbuP;LbuB;)V

    .line 76
    const-class v0, LwO;

    iget-object v1, p0, LwP;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LwP;->a(Ljava/lang/Class;Lbsk;)V

    .line 77
    iget-object v0, p0, LwP;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x53e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 79
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 100
    packed-switch p1, :pswitch_data_0

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/capture/DocScannerActivity;

    .line 104
    iget-object v0, p0, LwP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwP;

    .line 105
    invoke-virtual {v0, p2}, LwP;->a(Lcom/google/android/apps/docs/capture/DocScannerActivity;)V

    .line 122
    :goto_0
    return-void

    .line 108
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/capture/OcrCameraActivity;

    .line 110
    iget-object v0, p0, LwP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwP;

    .line 111
    invoke-virtual {v0, p2}, LwP;->a(Lcom/google/android/apps/docs/capture/OcrCameraActivity;)V

    goto :goto_0

    .line 114
    :pswitch_2
    check-cast p2, LwN;

    .line 116
    iget-object v0, p0, LwP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwP;

    .line 117
    invoke-virtual {v0, p2}, LwP;->a(LwN;)V

    goto :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0xb1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/capture/DocScannerActivity;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LwP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwP;

    .line 49
    invoke-virtual {v0, p1}, LwP;->a(LwN;)V

    .line 50
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/capture/OcrCameraActivity;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, LwP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwP;

    .line 55
    invoke-virtual {v0, p1}, LwP;->a(LwN;)V

    .line 56
    return-void
.end method

.method public a(LwN;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LwP;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 61
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 62
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method
