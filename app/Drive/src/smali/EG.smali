.class public final enum LEG;
.super Ljava/lang/Enum;
.source "DialogRegistryFactoryImpl.java"

# interfaces
.implements LEF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LEG;",
        ">;",
        "LEF;"
    }
.end annotation


# static fields
.field public static final enum a:LEG;

.field private static final synthetic a:[LEG;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13
    new-instance v0, LEG;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LEG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LEG;->a:LEG;

    .line 11
    const/4 v0, 0x1

    new-array v0, v0, [LEG;

    sget-object v1, LEG;->a:LEG;

    aput-object v1, v0, v2

    sput-object v0, LEG;->a:[LEG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LEG;
    .locals 1

    .prologue
    .line 11
    const-class v0, LEG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LEG;

    return-object v0
.end method

.method public static values()[LEG;
    .locals 1

    .prologue
    .line 11
    sget-object v0, LEG;->a:[LEG;

    invoke-virtual {v0}, [LEG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LEG;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;I)LEE;
    .locals 1

    .prologue
    .line 17
    new-instance v0, LEH;

    invoke-direct {v0, p1, p2}, LEH;-><init>(Landroid/app/Activity;I)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
