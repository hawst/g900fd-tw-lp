.class LaxM;
.super Laxy;
.source "PdfPageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lawr;

.field final synthetic a:LaxI;

.field final a:Lcom/google/android/apps/viewer/client/Dimensions;


# direct methods
.method constructor <init>(LaxI;Lcom/google/android/apps/viewer/client/Dimensions;Lawr;)V
    .locals 2

    .prologue
    .line 256
    iput-object p1, p0, LaxM;->a:LaxI;

    .line 257
    invoke-static {p1}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    sget-object v1, LaxR;->d:LaxR;

    invoke-direct {p0, v0, v1}, Laxy;-><init>(Laxz;LaxR;)V

    .line 258
    iput-object p2, p0, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    .line 259
    iput-object p3, p0, LaxM;->a:Lawr;

    .line 260
    return-void
.end method


# virtual methods
.method protected a(LavE;)Landroid/graphics/Bitmap;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 264
    iget-object v0, p0, LaxM;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    iget-object v0, v0, Laxz;->a:LavU;

    iget-object v1, p0, LaxM;->a:Lawr;

    invoke-virtual {v1}, Lawr;->a()Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v1

    invoke-virtual {v0, v1}, LavU;->a(Lcom/google/android/apps/viewer/client/Dimensions;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 265
    if-eqz v8, :cond_0

    .line 266
    invoke-static {}, Lawt;->a()Lawt;

    move-result-object v9

    .line 267
    iget-object v0, p0, LaxM;->a:Lawr;

    invoke-virtual {v0}, Lawr;->a()Landroid/graphics/Point;

    move-result-object v10

    .line 270
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 271
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 272
    const/4 v2, 0x1

    aget-object v7, v0, v2

    .line 273
    invoke-static {v8, v1}, Lcom/google/android/apps/viewer/util/ParcelUtils;->a(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V

    .line 275
    iget-object v0, p0, LaxM;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v1

    iget-object v0, p0, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v2, v0, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget-object v0, p0, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v0, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    iget v4, v10, Landroid/graphics/Point;->x:I

    iget v5, v10, Landroid/graphics/Point;->y:I

    iget-object v0, p0, LaxM;->a:Lawr;

    .line 276
    invoke-virtual {v0}, Lawr;->a()Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v6

    move-object v0, p1

    .line 275
    invoke-interface/range {v0 .. v7}, LavE;->a(IIIIILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z

    .line 277
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    const-string v0, "PdfPageLoader"

    const-string v1, "Render tile p.%d/(%s) off=%s page=%s bw=%s [%d ms]"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LaxM;->a:LaxI;

    .line 284
    invoke-static {v3}, LaxI;->a(LaxI;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    iget-object v3, p0, LaxM;->a:Lawr;

    aput-object v3, v2, v12

    const/4 v3, 0x2

    aput-object v10, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-virtual {v9}, Lawt;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 283
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, v8

    .line 286
    :goto_0
    return-object v0

    .line 278
    :catch_0
    move-exception v0

    .line 279
    const-string v1, "PdfPageLoader"

    const-string v2, "i/o error while creating pipe"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 280
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p0, p1}, LaxM;->a(LavE;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 290
    if-eqz p2, :cond_0

    .line 291
    iget-object v0, p0, LaxM;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    iget-object v1, p0, LaxM;->a:Lawr;

    invoke-interface {p1, v0, v1, p2}, LaxH;->a(ILawr;Landroid/graphics/Bitmap;)V

    .line 293
    :cond_0
    iget-object v0, p0, LaxM;->a:LaxI;

    iget-object v0, v0, LaxI;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    return-void
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 252
    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, LaxM;->a(LaxH;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 303
    if-eqz p1, :cond_0

    instance-of v1, p1, LaxM;

    if-eqz v1, :cond_0

    .line 304
    check-cast p1, LaxM;

    .line 305
    iget-object v1, p0, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget-object v2, p1, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/viewer/client/Dimensions;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaxM;->a:Lawr;

    iget-object v2, p1, LaxM;->a:Lawr;

    invoke-virtual {v1, v2}, Lawr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 307
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, LaxM;->a:Lawr;

    invoke-virtual {v0}, Lawr;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 297
    const-string v0, "RenderTileTask(page=%d width=%d height=%d tile=%s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaxM;->a:LaxI;

    .line 298
    invoke-static {v3}, LaxI;->a(LaxI;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LaxM;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LaxM;->a:Lawr;

    aput-object v3, v1, v2

    .line 297
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
