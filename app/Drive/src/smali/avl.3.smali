.class public Lavl;
.super Ljava/lang/Object;
.source "Fetcher.java"


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/viewer/client/AuthenticatedUri;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lavl;->a:Ljava/util/Map;

    .line 74
    iput-object p1, p0, Lavl;->a:Landroid/content/ContentResolver;

    .line 75
    return-void
.end method

.method private a(Landroid/net/Uri;)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 254
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_size"

    aput-object v1, v2, v0

    .line 255
    iget-object v0, p0, Lavl;->a:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 256
    if-eqz v1, :cond_1

    .line 258
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const-string v0, "_size"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 260
    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 261
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 265
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 268
    :goto_0
    return v0

    .line 265
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 268
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 265
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;)LauW;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lavl;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Lavg;->a(Ljava/lang/String;)Lavg;

    move-result-object v0

    .line 197
    if-nez v0, :cond_0

    .line 200
    :goto_0
    invoke-static {p2, p1}, LauW;->a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;

    move-result-object v0

    return-object v0

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;Ljava/lang/String;)Lavk;
    .locals 4

    .prologue
    .line 349
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 355
    if-eqz p2, :cond_0

    .line 356
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OAuth "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    .line 360
    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 361
    iget-object v3, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentLength()I

    move-result v0

    invoke-direct {p0, v3, v2, v0}, Lavl;->a(Landroid/net/Uri;Ljava/io/InputStream;I)Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    .line 362
    new-instance v2, Lavk;

    invoke-direct {v2, v1, v0}, Lavk;-><init>(Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)V

    return-object v2

    .line 350
    :catch_0
    move-exception v0

    .line 351
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Landroid/net/Uri;Ljava/io/InputStream;I)Ljava/io/ByteArrayOutputStream;
    .locals 6

    .prologue
    .line 370
    if-gtz p3, :cond_0

    const/16 p3, 0x400

    .line 371
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, p3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 372
    invoke-static {p2, v0}, LavZ;->b(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 373
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    div-int/lit16 v1, v1, 0x400

    .line 374
    const-string v2, "Fetcher"

    const-string v3, "Finished downloading %s [%s kb]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    return-object v0
.end method

.method private a(ILavo;LauW;)V
    .locals 4

    .prologue
    .line 227
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lavm;

    invoke-direct {v1, p0, p2, p1, p3}, Lavm;-><init>(Lavl;Lavo;ILauW;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 232
    return-void
.end method

.method private a(ILcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;Lavo;)V
    .locals 3

    .prologue
    .line 181
    if-eqz p2, :cond_0

    .line 182
    new-instance v0, Lavp;

    invoke-direct {v0, p0, p1, p3, p4}, Lavp;-><init>(Lavl;ILavg;Lavo;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lavp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 184
    :cond_0
    return-void
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 439
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 440
    const-string v1, "file"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lavl;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lavl;->a:Z

    return v0
.end method

.method private b(Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;)LauW;
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lavl;->a:Landroid/content/ContentResolver;

    iget-object v1, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-static {v0}, Lavg;->a(Ljava/lang/String;)Lavg;

    move-result-object v0

    .line 214
    if-nez v0, :cond_0

    .line 217
    :goto_0
    iget-object v0, p0, Lavl;->a:Landroid/content/ContentResolver;

    invoke-static {p2, p1, v0}, LauW;->a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Landroid/content/ContentResolver;)LauW;

    move-result-object v0

    return-object v0

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;
    .locals 4

    .prologue
    .line 272
    iget-object v0, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    .line 273
    iget-object v1, p0, Lavl;->a:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 274
    invoke-direct {p0, v0}, Lavl;->a(Landroid/net/Uri;)I

    move-result v2

    .line 277
    :try_start_0
    iget-object v3, p0, Lavl;->a:Landroid/content/ContentResolver;

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 278
    iget-object v3, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {p0, v3, v0, v2}, Lavl;->a(Landroid/net/Uri;Ljava/io/InputStream;I)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    .line 279
    new-instance v0, Lavk;

    invoke-direct {v0, v1, v2}, Lavk;-><init>(Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :goto_0
    return-object v0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    const-string v1, "Fetcher"

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 312
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 313
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 314
    if-ltz v1, :cond_0

    .line 315
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 318
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 287
    iget-object v1, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lavl;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 290
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v3, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 292
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 293
    const-wide/32 v6, 0x7fffffff

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 294
    const-string v1, "Fetcher"

    const-string v2, "We probably can\'t open a file that big: %d kb"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v8, 0x400

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 296
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 306
    :goto_0
    return-object v0

    .line 297
    :catch_0
    move-exception v1

    .line 298
    :try_start_2
    const-string v2, "Fetcher"

    const-string v3, "... And on top of it, we can\'t close the resource!"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 304
    :catch_1
    move-exception v1

    .line 305
    const-string v2, "Fetcher"

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 302
    :cond_0
    :try_start_3
    iget-object v1, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    long-to-int v4, v4

    invoke-direct {p0, v1, v3, v4}, Lavl;->a(Landroid/net/Uri;Ljava/io/InputStream;I)Ljava/io/ByteArrayOutputStream;

    move-result-object v3

    .line 303
    new-instance v1, Lavk;

    invoke-direct {v1, v2, v3}, Lavk;-><init>(Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v0, v1

    goto :goto_0
.end method

.method private d(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 322
    iget-object v0, p0, Lavl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    :try_start_0
    iget-object v0, p0, Lavl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lavl;->a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;Ljava/lang/String;)Lavk;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 341
    :goto_0
    return-object v0

    .line 325
    :catch_0
    move-exception v0

    .line 326
    iget-object v0, p0, Lavl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    const-string v0, "Fetcher"

    const-string v2, "Fetch failed using cached token"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->tokens:Lcom/google/android/apps/viewer/client/TokenSource;

    invoke-interface {v0}, Lcom/google/android/apps/viewer/client/TokenSource;->newToken()Ljava/lang/String;

    move-result-object v2

    .line 332
    :try_start_1
    invoke-direct {p0, p1, v2}, Lavl;->a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;Ljava/lang/String;)Lavk;

    move-result-object v0

    .line 333
    iget-object v3, p0, Lavl;->a:Ljava/util/Map;

    invoke-interface {v3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 335
    :catch_1
    move-exception v0

    .line 337
    const-string v2, "Fetcher"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 338
    goto :goto_0

    .line 339
    :catch_2
    move-exception v0

    .line 340
    const-string v2, "Fetcher"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 341
    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;
    .locals 2

    .prologue
    .line 241
    iget-object v0, p1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 243
    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    invoke-direct {p0, p1}, Lavl;->b(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    .line 245
    :cond_0
    const-string v1, "file"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    invoke-direct {p0, p1}, Lavl;->c(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;

    move-result-object v0

    goto :goto_0

    .line 248
    :cond_1
    invoke-direct {p0, p1}, Lavl;->d(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 87
    :try_start_0
    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "title"

    aput-object v1, v2, v0

    .line 90
    iget-object v0, p0, Lavl;->a:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 91
    if-eqz v1, :cond_2

    .line 93
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    const-string v0, "_display_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 95
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 106
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 115
    :goto_0
    return-object v0

    .line 99
    :cond_0
    :try_start_3
    const-string v0, "title"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 100
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 106
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 114
    const-string v1, "Fetcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Caught exception getting file name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_1
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 111
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavl;->a:Z

    .line 435
    return-void
.end method

.method public a(Lavr;LauM;ILavo;)V
    .locals 5

    .prologue
    .line 122
    const-string v0, "Fetcher"

    const-string v1, "Fetch file @ %d in %s res"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lavr;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    sget-object v0, Lavr;->a:Lavr;

    if-ne p1, v0, :cond_3

    .line 126
    iget-object v0, p2, LauM;->c:Ljava/lang/String;

    invoke-static {v0}, Lavg;->a(Ljava/lang/String;)Lavg;

    move-result-object v0

    .line 127
    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can\'t handle files of type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, LauM;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, p3, v0}, Lavo;->a(ILjava/lang/String;)V

    .line 157
    :goto_0
    return-void

    .line 132
    :cond_0
    sget-object v1, Lavn;->a:[I

    invoke-virtual {v0}, Lavg;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 151
    iget-object v1, p2, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-direct {p0, p3, v1, v0, p4}, Lavl;->a(ILcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;Lavo;)V

    goto :goto_0

    .line 135
    :pswitch_0
    sget-object v0, Lavg;->a:Lavg;

    iget-object v1, p2, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-static {v0, v1}, LauW;->b(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;

    move-result-object v0

    invoke-direct {p0, p3, p4, v0}, Lavl;->a(ILavo;LauW;)V

    goto :goto_0

    .line 138
    :pswitch_1
    iget-object v1, p2, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v1, v1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 140
    sget-boolean v2, LavX;->q:Z

    if-nez v2, :cond_1

    const-string v2, "file"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 141
    iget-object v1, p2, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-direct {p0, v1, v0}, Lavl;->a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;)LauW;

    move-result-object v0

    .line 142
    invoke-interface {p4, p3, v0}, Lavo;->a(ILauW;)V

    goto :goto_0

    .line 143
    :cond_1
    sget-boolean v2, LavX;->q:Z

    if-nez v2, :cond_2

    const-string v2, "content"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 144
    iget-object v1, p2, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-direct {p0, v1, v0}, Lavl;->b(Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;)LauW;

    move-result-object v0

    .line 145
    invoke-interface {p4, p3, v0}, Lavo;->a(ILauW;)V

    goto :goto_0

    .line 147
    :cond_2
    iget-object v1, p2, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-direct {p0, p3, v1, v0, p4}, Lavl;->a(ILcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;Lavo;)V

    goto :goto_0

    .line 155
    :cond_3
    iget-object v0, p2, LauM;->b:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    const/4 v1, 0x0

    invoke-direct {p0, p3, v0, v1, p4}, Lavl;->a(ILcom/google/android/apps/viewer/client/AuthenticatedUri;Lavg;Lavo;)V

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
