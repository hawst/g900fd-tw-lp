.class Laxk;
.super Ljava/lang/Object;
.source "PdfViewer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:LawZ;


# direct methods
.method constructor <init>(LawZ;)V
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, Laxk;->a:LawZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 683
    instance-of v0, p1, Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Laxk;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxw;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laxw;->a(Lcom/google/android/apps/viewer/pdflib/Selection;)V

    .line 686
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 672
    instance-of v0, p1, Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    if-eqz v0, :cond_0

    .line 673
    check-cast p1, Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    .line 674
    iget-object v0, p0, Laxk;->a:LawZ;

    .line 675
    invoke-static {v0}, LawZ;->a(LawZ;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Landroid/graphics/Point;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->atPoint(Landroid/graphics/Point;)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    move-result-object v0

    .line 676
    iget-object v1, p0, Laxk;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)Laxz;

    move-result-object v1

    iget v2, p1, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:I

    invoke-virtual {v1, v2, v0, v0}, Laxz;->a(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V

    .line 678
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
