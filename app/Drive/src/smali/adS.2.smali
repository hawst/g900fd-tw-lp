.class public LadS;
.super Ljava/lang/Object;
.source "LocalFileManagerImpl.java"

# interfaces
.implements LadQ;


# instance fields
.field private final a:LaGg;

.field private final a:LaHr;

.field private final a:Ladi;

.field private final a:LagZ;

.field private final a:LahB;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LadU;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z


# direct methods
.method public constructor <init>(Ladi;LaGg;LahB;LagZ;LQr;LaHr;)V
    .locals 2

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LadS;->a:Ljava/util/Map;

    .line 527
    iput-object p1, p0, LadS;->a:Ladi;

    .line 528
    iput-object p2, p0, LadS;->a:LaGg;

    .line 529
    iput-object p3, p0, LadS;->a:LahB;

    .line 530
    iput-object p4, p0, LadS;->a:LagZ;

    .line 531
    const-string v0, "enableReSyncOnClose"

    const/4 v1, 0x1

    .line 532
    invoke-interface {p5, v0, v1}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LadS;->a:Z

    .line 533
    iput-object p6, p0, LadS;->a:LaHr;

    .line 534
    return-void
.end method

.method static synthetic a(LadS;)LaGg;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LadS;->a:LaGg;

    return-object v0
.end method

.method static synthetic a(LadS;)LaHr;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LadS;->a:LaHr;

    return-object v0
.end method

.method static synthetic a(LadS;)LagZ;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LadS;->a:LagZ;

    return-object v0
.end method

.method static synthetic a(LadS;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LadS;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(LadS;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, LadS;->a:Z

    return v0
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LadR;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 539
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    iget-object v0, p0, LadS;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v1

    .line 542
    if-nez v1, :cond_0

    .line 543
    const-string v0, "LocalFileManagerImpl"

    const-string v1, "Trying to open or create a file for a non-existing entry!"

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v6

    .line 608
    :goto_0
    monitor-exit p0

    return-object v0

    .line 547
    :cond_0
    :try_start_1
    sget-object v0, LacY;->a:LacY;

    invoke-interface {v1, v0}, LaGo;->a(LacY;)J

    move-result-wide v8

    .line 548
    iget-object v0, p0, LadS;->a:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadU;

    .line 549
    if-eqz v0, :cond_1

    .line 550
    invoke-static {v0}, LadU;->a(LadU;)I

    .line 551
    const-string v1, "LocalFileManagerImpl"

    const-string v2, "Returning an active document file. docId: %s, isBeingCreated: %s, #references: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 553
    invoke-virtual {v0}, LadU;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, LadU;->a(LadU;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, LadU;->b(LadU;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 551
    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 539
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 557
    :cond_1
    :try_start_2
    invoke-interface {v1}, LaGo;->i()Ljava/lang/String;

    move-result-object v7

    .line 558
    iget-object v0, p0, LadS;->a:Ladi;

    invoke-interface {v0, v1}, Ladi;->a(LaGo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 562
    iget-object v0, p0, LadS;->a:LaGg;

    invoke-interface {v0, v8, v9}, LaGg;->a(J)LaGp;

    move-result-object v0

    .line 563
    if-nez v7, :cond_4

    .line 564
    invoke-virtual {v0}, LaGp;->d()Ljava/lang/String;

    move-result-object v4

    .line 568
    const-string v2, "A local only document doesn\'t have the document ID in its DocumentContent entry."

    invoke-static {v4, v2}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 572
    :goto_1
    :try_start_3
    iget-object v2, p0, LadS;->a:Ladi;

    sget-object v3, LacY;->a:LacY;

    sget-object v5, Ladk;->a:Ladk;

    invoke-interface {v2, v0, v3, v5, v1}, Ladi;->a(LaGp;LacY;Lamr;LaGo;)LbsU;

    move-result-object v0

    .line 573
    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ladj;

    .line 574
    new-instance v0, LadU;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LadU;-><init>(LadS;Ladj;ZLjava/lang/String;LadT;)V

    .line 575
    iget-object v1, p0, LadS;->a:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    const-string v1, "LocalFileManagerImpl"

    const-string v2, "Offline DB file exists for doc %s, returning its document file."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 579
    :catch_0
    move-exception v0

    .line 580
    :try_start_4
    const-string v1, "LocalFileManagerImpl"

    const-string v2, "Interrupted while opening document file for doc %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v6

    .line 581
    goto/16 :goto_0

    .line 582
    :catch_1
    move-exception v0

    .line 583
    const-string v1, "LocalFileManagerImpl"

    const-string v2, "Failed to open document file for doc %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v6

    .line 584
    goto/16 :goto_0

    .line 588
    :cond_2
    if-nez v7, :cond_3

    .line 589
    const-string v0, "LocalFileManagerImpl"

    const-string v2, "Creating a new document file for a new document."

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :goto_2
    invoke-interface {v1}, LaGo;->a()LaGv;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, LaGp;->a(LaGv;Z)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 598
    :try_start_5
    iget-object v0, p0, LadS;->a:Ladi;

    const/4 v3, 0x0

    sget-object v4, LacY;->a:LacY;

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Ladi;->a(LaGo;Ljava/lang/String;Ljava/lang/String;LacY;Ljava/lang/String;)Ladj;

    move-result-object v2

    .line 600
    new-instance v0, LadU;

    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, v7

    invoke-direct/range {v0 .. v5}, LadU;-><init>(LadS;Ladj;ZLjava/lang/String;LadT;)V

    .line 601
    iget-object v1, p0, LadS;->a:Ljava/util/Map;

    invoke-static {v0}, LadU;->a(LadU;)Ladj;

    move-result-object v2

    invoke-interface {v2}, Ladj;->a()LaGp;

    move-result-object v2

    invoke-virtual {v2}, LaGp;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch LQm; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 603
    :catch_2
    move-exception v0

    .line 604
    :try_start_6
    const-string v1, "LocalFileManagerImpl"

    const-string v2, "Failed to create document file for doc %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v6

    .line 605
    goto/16 :goto_0

    .line 592
    :cond_3
    const-string v0, "LocalFileManagerImpl"

    const-string v2, "Offline DB file does not exist for doc %s. Creating a new document file for it"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 606
    :catch_3
    move-exception v0

    .line 607
    const-string v1, "LocalFileManagerImpl"

    const-string v2, "Failed to create document file for doc %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-object v0, v6

    .line 608
    goto/16 :goto_0

    :cond_4
    move-object v4, v7

    goto/16 :goto_1
.end method
