.class Laiz;
.super Ljava/lang/Object;
.source "ActiveContextCastingProvider.java"

# interfaces
.implements LbuE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbuE",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Laiz;->a:Ljava/lang/Class;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, Laiz;->a:Lbxw;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "this instance has not been members-injected"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 34
    iget-object v0, p0, Laiz;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 35
    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 37
    iget-object v3, p0, Laiz;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    const-string v4, "The canonical active context %s is not a %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v2

    iget-object v2, p0, Laiz;->a:Ljava/lang/Class;

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 42
    return-object v0

    :cond_0
    move v0, v2

    .line 32
    goto :goto_0
.end method

.method a(Lbxw;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 26
    iget-object v0, p0, Laiz;->a:Lbxw;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "This method should be injected once"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 27
    iput-object p1, p0, Laiz;->a:Lbxw;

    .line 28
    return-void

    :cond_0
    move v0, v1

    .line 26
    goto :goto_0
.end method
