.class public LaFS;
.super LaGm;
.source "CacheListEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEo;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(LaEz;JJ)V
    .locals 2

    .prologue
    .line 28
    invoke-static {}, LaEo;->a()LaEo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 29
    iput-wide p2, p0, LaFS;->a:J

    .line 30
    iput-wide p4, p0, LaFS;->b:J

    .line 31
    return-void
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaFS;
    .locals 6

    .prologue
    .line 41
    sget-object v0, LaEp;->a:LaEp;

    .line 42
    invoke-virtual {v0}, LaEp;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 43
    sget-object v0, LaEp;->b:LaEp;

    .line 44
    invoke-virtual {v0}, LaEp;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 45
    new-instance v0, LaFS;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LaFS;-><init>(LaEz;JJ)V

    .line 47
    invoke-static {}, LaEo;->a()LaEo;

    move-result-object v1

    invoke-virtual {v1}, LaEo;->d()Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaFS;->c(J)V

    .line 48
    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, LaFS;->b:J

    return-wide v0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 53
    sget-object v0, LaEp;->a:LaEp;

    invoke-virtual {v0}, LaEp;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaFS;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 54
    sget-object v0, LaEp;->b:LaEp;

    invoke-virtual {v0}, LaEp;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaFS;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 55
    return-void
.end method
