.class public final enum LIl;
.super Ljava/lang/Enum;
.source "FastScroller.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIl;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LIl;

.field private static final synthetic a:[LIl;

.field public static final enum b:LIl;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    new-instance v0, LIl;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LIl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIl;->a:LIl;

    .line 53
    new-instance v0, LIl;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, LIl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIl;->b:LIl;

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [LIl;

    sget-object v1, LIl;->a:LIl;

    aput-object v1, v0, v2

    sget-object v1, LIl;->b:LIl;

    aput-object v1, v0, v3

    sput-object v0, LIl;->a:[LIl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LIl;
    .locals 1

    .prologue
    .line 51
    const-class v0, LIl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIl;

    return-object v0
.end method

.method public static values()[LIl;
    .locals 1

    .prologue
    .line 51
    sget-object v0, LIl;->a:[LIl;

    invoke-virtual {v0}, [LIl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIl;

    return-object v0
.end method
