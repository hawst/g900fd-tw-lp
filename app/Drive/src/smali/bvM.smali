.class public final LbvM;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, LbvK;

    sput-object v0, LbvM;->a:Ljava/lang/Class;

    .line 33
    const-class v0, LbvZ;

    sput-object v0, LbvM;->b:Ljava/lang/Class;

    .line 34
    const-class v0, Lbwc;

    sput-object v0, LbvM;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LbvM;->a:LbrA;

    .line 41
    return-void
.end method

.method static synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, LbvM;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, LbvM;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, LbvM;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic e(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, LbvM;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic f(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, LbvM;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 86
    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 115
    .line 117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 72
    const-class v0, LbvV;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x5b

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LbvM;->a(LbuP;LbuB;)V

    .line 75
    const-class v0, Lbwi;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x5c

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LbvM;->a(LbuP;LbuB;)V

    .line 78
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 95
    packed-switch p1, :pswitch_data_0

    .line 109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :pswitch_0
    check-cast p2, LbvV;

    .line 99
    iget-object v0, p0, LbvM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbvM;

    .line 100
    invoke-virtual {v0, p2}, LbvM;->a(LbvV;)V

    .line 111
    :goto_0
    return-void

    .line 103
    :pswitch_1
    check-cast p2, Lbwi;

    .line 105
    iget-object v0, p0, LbvM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbvM;

    .line 106
    invoke-virtual {v0, p2}, LbvM;->a(Lbwi;)V

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x5b
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(LbvV;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, LbvM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lbut;

    iget-object v0, v0, Lbut;->a:Lbsk;

    .line 50
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LbvM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lbut;

    iget-object v1, v1, Lbut;->a:Lbsk;

    .line 48
    invoke-static {v0, v1}, LbvM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuu;

    .line 47
    invoke-virtual {p1, v0}, LbvV;->a(Lbuu;)V

    .line 55
    return-void
.end method

.method public a(Lbwi;)V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, LbvM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lbut;

    iget-object v0, v0, Lbut;->a:Lbsk;

    .line 62
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LbvM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lbut;

    iget-object v1, v1, Lbut;->a:Lbsk;

    .line 60
    invoke-static {v0, v1}, LbvM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuu;

    .line 59
    invoke-virtual {p1, v0}, Lbwi;->a(Lbuu;)V

    .line 67
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method
