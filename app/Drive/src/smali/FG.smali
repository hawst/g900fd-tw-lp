.class LFG;
.super Ljava/lang/Object;
.source "DocListDocumentCreatorImpl.java"

# interfaces
.implements LFF;


# instance fields
.field private final a:LaFO;

.field private final a:LaGR;

.field private final a:Landroid/app/Activity;

.field private final a:LsI;

.field private final a:Lwm;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LaFO;Lwm;LsI;LaGR;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, LFG;->a:Landroid/app/Activity;

    .line 42
    iput-object p2, p0, LFG;->a:LaFO;

    .line 43
    iput-object p3, p0, LFG;->a:Lwm;

    .line 44
    iput-object p4, p0, LFG;->a:LsI;

    .line 45
    iput-object p5, p0, LFG;->a:LaGR;

    .line 46
    return-void
.end method

.method static synthetic a(LFG;)LaFO;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LFG;->a:LaFO;

    return-object v0
.end method

.method static synthetic a(LFG;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LFG;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(LFG;)LsI;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LFG;->a:LsI;

    return-object v0
.end method


# virtual methods
.method public a(LaGM;Ljava/util/List;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;"
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 55
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 56
    invoke-static {p2}, Lwr;->a(Ljava/util/List;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, LFG;->a:LaFO;

    invoke-interface {p1, v0}, LaGM;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 62
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, LFG;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    .line 68
    iget-object v1, p0, LFG;->a:LaGR;

    new-instance v2, LFH;

    invoke-direct {v2, p0, v0}, LFH;-><init>(LFG;Ljava/util/List;)V

    invoke-virtual {v1, v2}, LaGR;->b(LaGN;)V

    .line 81
    return-void
.end method
