.class public Laou;
.super Ljava/lang/Object;
.source "FutureDependentValueGuard.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<TV;>;"
        }
    .end annotation
.end field

.field private a:Laoz;

.field private final a:LbsJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsJ",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Laoy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoy",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Laov;

    invoke-direct {v0, p0}, Laov;-><init>(Laou;)V

    iput-object v0, p0, Laou;->a:LbsJ;

    .line 80
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Laou;->a:Ljava/util/Set;

    .line 82
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Laou;->b:Ljava/util/Set;

    .line 86
    sget-object v0, Laoz;->a:Laoz;

    iput-object v0, p0, Laou;->a:Laoz;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Laou;->a:Z

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Laou;->a:LbsU;

    .line 104
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoy;

    iput-object v0, p0, Laou;->a:Laoy;

    .line 105
    return-void
.end method

.method public static a()Laou;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V::",
            "Ljava/io/Closeable;",
            ">()",
            "Laou",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 96
    new-instance v0, Laou;

    new-instance v1, Laox;

    invoke-direct {v1}, Laox;-><init>()V

    invoke-direct {v0, v1}, Laou;-><init>(Laoy;)V

    return-object v0
.end method

.method public static a(Laoy;)Laou;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Laoy",
            "<TV;>;)",
            "Laou",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Laou;

    invoke-direct {v0, p0}, Laou;-><init>(Laoy;)V

    return-object v0
.end method

.method static synthetic a(Laou;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Laou;->b(Ljava/lang/Object;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 213
    .line 214
    monitor-enter p0

    .line 215
    :try_start_0
    iget-object v0, p0, Laou;->a:Ljava/util/Set;

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    .line 216
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 218
    iget-object v2, p0, Laou;->a:Laoy;

    invoke-interface {v2, v1}, Laoy;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 216
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 220
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 201
    monitor-enter p0

    .line 202
    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Laoz;->a:Laoz;

    iget-object v1, p0, Laou;->a:Laoz;

    invoke-virtual {v0, v1}, Laoz;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 205
    sget-object v0, Laoz;->b:Laoz;

    iput-object v0, p0, Laou;->a:Laoz;

    .line 206
    iget-object v0, p0, Laou;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v0, p0, Laou;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 208
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    invoke-direct {p0}, Laou;->b()V

    .line 210
    return-void

    .line 208
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 192
    monitor-enter p0

    .line 193
    :try_start_0
    sget-object v0, Laoz;->a:Laoz;

    iget-object v1, p0, Laou;->a:Laoz;

    invoke-virtual {v0, v1}, Laoz;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 194
    sget-object v0, Laoz;->c:Laoz;

    iput-object v0, p0, Laou;->a:Laoz;

    .line 195
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    invoke-direct {p0}, Laou;->b()V

    .line 198
    return-void

    .line 195
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(LbsU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsU",
            "<+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Laou;->a:LbsJ;

    invoke-static {p1, v0}, LbsK;->a(LbsU;LbsJ;)V

    .line 162
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const/4 v0, 0x0

    .line 124
    monitor-enter p0

    .line 125
    :try_start_0
    iget-object v1, p0, Laou;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 126
    sget-object v1, Laoz;->a:Laoz;

    iget-object v2, p0, Laou;->a:Laoz;

    invoke-virtual {v1, v2}, Laoz;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p0, Laou;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object p1, v0

    .line 132
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    if-eqz p1, :cond_1

    .line 134
    iget-object v0, p0, Laou;->a:Laoy;

    invoke-interface {v0, p1}, Laoy;->a(Ljava/lang/Object;)V

    .line 136
    :cond_1
    return-void

    .line 132
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p1, v0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    sget-object v0, Laoz;->a:Laoz;

    iget-object v1, p0, Laou;->a:Laoz;

    invoke-virtual {v0, v1}, Laoz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Laou;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Laou;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    :cond_0
    const/4 v0, 0x1

    .line 152
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(LbsU;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsU",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 169
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    monitor-enter p0

    .line 171
    :try_start_0
    iget-boolean v1, p0, Laou;->a:Z

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Laou;->a:Z

    .line 173
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    new-instance v0, Laow;

    invoke-direct {v0, p0}, Laow;-><init>(Laou;)V

    invoke-static {p1, v0}, LbsK;->a(LbsU;LbsJ;)V

    .line 185
    return-void

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
