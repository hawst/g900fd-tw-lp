.class public final LarZ;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field public A:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LasX;",
            ">;"
        }
    .end annotation
.end field

.field public B:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasa;",
            ">;"
        }
    .end annotation
.end field

.field public C:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Latj;",
            ">;"
        }
    .end annotation
.end field

.field public D:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "Lasb;",
            ">;>;"
        }
    .end annotation
.end field

.field public E:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasb;",
            ">;"
        }
    .end annotation
.end field

.field public F:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LasP;",
            ">;>;"
        }
    .end annotation
.end field

.field public G:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "Lasa;",
            ">;>;"
        }
    .end annotation
.end field

.field public H:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LasP;",
            ">;>;"
        }
    .end annotation
.end field

.field public I:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "Lasb;",
            ">;>;"
        }
    .end annotation
.end field

.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lati;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lati;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasb;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasb;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LatC;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LasP;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasa;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasy;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LarX;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lato;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LarT;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasl;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LasR;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LasQ;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lass;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasc;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LasZ;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lase;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Latd;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasz;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasa;",
            ">;"
        }
    .end annotation
.end field

.field public v:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasa;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lasb;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lati;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "Lasa;",
            ">;>;"
        }
    .end annotation
.end field

.field public z:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LasI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 71
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 72
    iput-object p1, p0, LarZ;->a:LbrA;

    .line 73
    const-class v0, Lati;

    new-instance v1, LbvO;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvO;-><init>(Ljava/lang/String;)V

    .line 74
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 73
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->a:Lbsk;

    .line 76
    const-class v0, Lati;

    new-instance v1, LbvP;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvP;-><init>(Ljava/lang/String;)V

    .line 77
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    const-class v1, LbuO;

    .line 76
    invoke-static {v0, v1}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->b:Lbsk;

    .line 79
    const-class v0, Lasb;

    new-instance v1, LbvO;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvO;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 79
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->c:Lbsk;

    .line 82
    const-class v0, Lasb;

    new-instance v1, LbvP;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvP;-><init>(Ljava/lang/String;)V

    .line 83
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 82
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->d:Lbsk;

    .line 85
    const-class v0, LatC;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->e:Lbsk;

    .line 88
    const-class v0, LasP;

    sget-object v1, LQH;->e:Ljava/lang/Class;

    .line 89
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 88
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->f:Lbsk;

    .line 91
    const-class v0, Lasa;

    new-instance v1, LbvO;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvO;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 91
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->g:Lbsk;

    .line 94
    const-class v0, Lasy;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->h:Lbsk;

    .line 97
    const-class v0, LarX;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->i:Lbsk;

    .line 100
    const-class v0, Lato;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->j:Lbsk;

    .line 103
    const-class v0, LarT;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->k:Lbsk;

    .line 106
    const-class v0, Lasl;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->l:Lbsk;

    .line 109
    const-class v0, LasR;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->m:Lbsk;

    .line 112
    const-class v0, LasQ;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->n:Lbsk;

    .line 115
    const-class v0, Lass;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->o:Lbsk;

    .line 118
    const-class v0, Lasc;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->p:Lbsk;

    .line 121
    const-class v0, LasZ;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->q:Lbsk;

    .line 124
    const-class v0, Lase;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->r:Lbsk;

    .line 127
    const-class v0, Latd;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->s:Lbsk;

    .line 130
    const-class v0, Lasz;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->t:Lbsk;

    .line 133
    const-class v0, Lasa;

    new-instance v1, LbvP;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvP;-><init>(Ljava/lang/String;)V

    .line 134
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 133
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->u:Lbsk;

    .line 136
    const-class v0, Lasa;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->v:Lbsk;

    .line 139
    const-class v0, Lasb;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->w:Lbsk;

    .line 142
    const-class v0, Lati;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->x:Lbsk;

    .line 145
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Lasa;

    aput-object v2, v1, v4

    .line 146
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 145
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->y:Lbsk;

    .line 148
    const-class v0, LasI;

    invoke-static {v0, v3}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->z:Lbsk;

    .line 151
    const-class v0, LasX;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->A:Lbsk;

    .line 154
    const-class v0, Lasa;

    sget-object v1, LQH;->j:Ljava/lang/Class;

    .line 155
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 154
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->B:Lbsk;

    .line 157
    const-class v0, Latj;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->C:Lbsk;

    .line 160
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Lasb;

    aput-object v2, v1, v4

    .line 161
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 160
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->D:Lbsk;

    .line 163
    const-class v0, Lasb;

    sget-object v1, LQH;->j:Ljava/lang/Class;

    .line 164
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 163
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->E:Lbsk;

    .line 166
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LasP;

    aput-object v2, v1, v4

    .line 167
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->e:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 166
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->F:Lbsk;

    .line 169
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Lasa;

    aput-object v2, v1, v4

    .line 170
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 169
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->G:Lbsk;

    .line 172
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LasP;

    aput-object v2, v1, v4

    .line 173
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->e:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 172
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->H:Lbsk;

    .line 175
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Lasb;

    aput-object v2, v1, v4

    .line 176
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 175
    invoke-static {v0, v3}, LarZ;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LarZ;->I:Lbsk;

    .line 178
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 525
    sparse-switch p1, :sswitch_data_0

    .line 771
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 527
    :sswitch_0
    new-instance v0, Lasy;

    invoke-direct {v0}, Lasy;-><init>()V

    .line 529
    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    .line 530
    invoke-virtual {v1, v0}, LarZ;->a(Lasy;)V

    .line 769
    :goto_0
    return-object v0

    .line 533
    :sswitch_1
    new-instance v0, LarX;

    invoke-direct {v0}, LarX;-><init>()V

    .line 535
    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    .line 536
    invoke-virtual {v1, v0}, LarZ;->a(LarX;)V

    goto :goto_0

    .line 539
    :sswitch_2
    new-instance v2, Lato;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 542
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 540
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->v:Lbsk;

    .line 548
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LarZ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LarZ;

    iget-object v3, v3, LarZ;->v:Lbsk;

    .line 546
    invoke-static {v1, v3}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lasa;

    invoke-direct {v2, v0, v1}, Lato;-><init>(Landroid/content/Context;Lasa;)V

    move-object v0, v2

    .line 553
    goto :goto_0

    .line 555
    :sswitch_3
    new-instance v0, LarT;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->i:Lbsk;

    .line 558
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->i:Lbsk;

    .line 556
    invoke-static {v1, v2}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx;

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LarZ;

    iget-object v2, v2, LarZ;->m:Lbsk;

    .line 564
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LarZ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LarZ;

    iget-object v3, v3, LarZ;->m:Lbsk;

    .line 562
    invoke-static {v2, v3}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LasR;

    iget-object v3, p0, LarZ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->D:Lbsk;

    .line 570
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LarZ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->D:Lbsk;

    .line 568
    invoke-static {v3, v4}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lakz;

    iget-object v4, p0, LarZ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->t:Lbsk;

    .line 576
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LarZ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->t:Lbsk;

    .line 574
    invoke-static {v4, v5}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laja;

    iget-object v5, p0, LarZ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LarZ;

    iget-object v5, v5, LarZ;->h:Lbsk;

    .line 582
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LarZ;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LarZ;

    iget-object v6, v6, LarZ;->h:Lbsk;

    .line 580
    invoke-static {v5, v6}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lasy;

    invoke-direct/range {v0 .. v5}, LarT;-><init>(Lrx;LasR;Lakz;Laja;Lasy;)V

    .line 587
    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    .line 588
    invoke-virtual {v1, v0}, LarZ;->a(LarT;)V

    goto/16 :goto_0

    .line 591
    :sswitch_4
    new-instance v0, Lasl;

    invoke-direct {v0}, Lasl;-><init>()V

    goto/16 :goto_0

    .line 595
    :sswitch_5
    new-instance v4, LasR;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 598
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 596
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->K:Lbsk;

    .line 604
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->K:Lbsk;

    .line 602
    invoke-static {v1, v2}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lalo;

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->a:Lbsk;

    .line 610
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LarZ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTk;

    iget-object v3, v3, LTk;->a:Lbsk;

    .line 608
    invoke-static {v2, v3}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LTl;

    iget-object v3, p0, LarZ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->p:Lbsk;

    .line 616
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LarZ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LadM;

    iget-object v5, v5, LadM;->p:Lbsk;

    .line 614
    invoke-static {v3, v5}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laeb;

    invoke-direct {v4, v0, v1, v2, v3}, LasR;-><init>(Laja;Lalo;LTl;Laeb;)V

    move-object v0, v4

    .line 621
    goto/16 :goto_0

    .line 623
    :sswitch_6
    new-instance v0, LasQ;

    invoke-direct {v0}, LasQ;-><init>()V

    .line 625
    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    .line 626
    invoke-virtual {v1, v0}, LarZ;->a(LasQ;)V

    goto/16 :goto_0

    .line 629
    :sswitch_7
    new-instance v3, Lass;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->ac:Lbsk;

    .line 632
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->ac:Lbsk;

    .line 630
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->Z:Lbsk;

    .line 638
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->Z:Lbsk;

    .line 636
    invoke-static {v1, v2}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LSK;

    iget-object v2, v2, LSK;->b:Lbsk;

    .line 644
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LarZ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LSK;

    iget-object v4, v4, LSK;->b:Lbsk;

    .line 642
    invoke-static {v2, v4}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LSF;

    invoke-direct {v3, v0, v1, v2}, Lass;-><init>(LbiP;Laja;LSF;)V

    move-object v0, v3

    .line 649
    goto/16 :goto_0

    .line 651
    :sswitch_8
    new-instance v3, Lasc;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->x:Lbsk;

    .line 654
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->x:Lbsk;

    .line 652
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lati;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->v:Lbsk;

    .line 660
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LarZ;

    iget-object v2, v2, LarZ;->v:Lbsk;

    .line 658
    invoke-static {v1, v2}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lasa;

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 666
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LarZ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 664
    invoke-static {v2, v4}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    invoke-direct {v3, v0, v1, v2}, Lasc;-><init>(Lati;Lasa;LtK;)V

    move-object v0, v3

    .line 671
    goto/16 :goto_0

    .line 673
    :sswitch_9
    new-instance v0, LasZ;

    invoke-direct {v0}, LasZ;-><init>()V

    .line 675
    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    .line 676
    invoke-virtual {v1, v0}, LarZ;->a(LasZ;)V

    goto/16 :goto_0

    .line 679
    :sswitch_a
    new-instance v3, Lase;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->m:Lbsk;

    .line 682
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->m:Lbsk;

    .line 680
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasR;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->a:Lbsk;

    .line 688
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->a:Lbsk;

    .line 686
    invoke-static {v1, v2}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTl;

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LwG;

    iget-object v2, v2, LwG;->c:Lbsk;

    .line 694
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LarZ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LwG;

    iget-object v4, v4, LwG;->c:Lbsk;

    .line 692
    invoke-static {v2, v4}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LwH;

    invoke-direct {v3, v0, v1, v2}, Lase;-><init>(LasR;LTl;LwH;)V

    move-object v0, v3

    .line 699
    goto/16 :goto_0

    .line 701
    :sswitch_b
    new-instance v0, Latd;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 704
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 702
    invoke-static {v1, v2}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LarZ;

    iget-object v2, v2, LarZ;->x:Lbsk;

    .line 710
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LarZ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LarZ;

    iget-object v3, v3, LarZ;->x:Lbsk;

    .line 708
    invoke-static {v2, v3}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lati;

    iget-object v3, p0, LarZ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LarZ;

    iget-object v3, v3, LarZ;->C:Lbsk;

    .line 716
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LarZ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LarZ;

    iget-object v4, v4, LarZ;->C:Lbsk;

    .line 714
    invoke-static {v3, v4}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Latj;

    iget-object v4, p0, LarZ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->t:Lbsk;

    .line 722
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LarZ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->t:Lbsk;

    .line 720
    invoke-static {v4, v5}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laja;

    iget-object v5, p0, LarZ;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LQH;

    iget-object v5, v5, LQH;->d:Lbsk;

    .line 728
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LarZ;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LQH;

    iget-object v6, v6, LQH;->d:Lbsk;

    .line 726
    invoke-static {v5, v6}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LQr;

    iget-object v6, p0, LarZ;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LqD;

    iget-object v6, v6, LqD;->c:Lbsk;

    .line 734
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LarZ;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LqD;

    iget-object v7, v7, LqD;->c:Lbsk;

    .line 732
    invoke-static {v6, v7}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LqK;

    iget-object v7, p0, LarZ;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LarZ;

    iget-object v7, v7, LarZ;->w:Lbsk;

    .line 740
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LarZ;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LarZ;

    iget-object v8, v8, LarZ;->w:Lbsk;

    .line 738
    invoke-static {v7, v8}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lasb;

    iget-object v8, p0, LarZ;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LarZ;

    iget-object v8, v8, LarZ;->v:Lbsk;

    .line 746
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, LarZ;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LarZ;

    iget-object v9, v9, LarZ;->v:Lbsk;

    .line 744
    invoke-static {v8, v9}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lasa;

    iget-object v9, p0, LarZ;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LarZ;

    iget-object v9, v9, LarZ;->j:Lbsk;

    .line 752
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, LarZ;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LarZ;

    iget-object v10, v10, LarZ;->j:Lbsk;

    .line 750
    invoke-static {v9, v10}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lato;

    invoke-direct/range {v0 .. v9}, Latd;-><init>(LtK;Lati;Latj;Laja;LQr;LqK;Lasb;Lasa;Lato;)V

    .line 757
    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    .line 758
    invoke-virtual {v1, v0}, LarZ;->a(Latd;)V

    goto/16 :goto_0

    .line 761
    :sswitch_c
    new-instance v1, Lasz;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 764
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LarZ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 762
    invoke-static {v0, v2}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, Lasz;-><init>(Laja;)V

    move-object v0, v1

    .line 769
    goto/16 :goto_0

    .line 525
    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_5
        0x1ec -> :sswitch_c
        0x1ee -> :sswitch_9
        0x1f0 -> :sswitch_0
        0x1f4 -> :sswitch_3
        0x1f5 -> :sswitch_1
        0x1f6 -> :sswitch_2
        0x1fb -> :sswitch_8
        0x1fc -> :sswitch_4
        0x1fe -> :sswitch_a
        0x202 -> :sswitch_6
        0x206 -> :sswitch_7
        0x20e -> :sswitch_b
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 852
    sparse-switch p2, :sswitch_data_0

    .line 973
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 854
    :sswitch_0
    check-cast p1, LarW;

    .line 856
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v2, v0, LarZ;->B:Lbsk;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 863
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 867
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 856
    invoke-virtual {p1, v2, v0, v1}, LarW;->get3(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    .line 958
    :goto_0
    return-object v0

    .line 871
    :sswitch_1
    check-cast p1, LatD;

    .line 873
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 876
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 873
    invoke-virtual {p1, v0}, LatD;->provideRedeemVoucherFlow(Landroid/content/Context;)LasI;

    move-result-object v0

    goto :goto_0

    .line 880
    :sswitch_2
    check-cast p1, LatD;

    .line 882
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->q:Lbsk;

    .line 885
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasZ;

    .line 882
    invoke-virtual {p1, v0}, LatD;->provideVoucherService(LasZ;)LasX;

    move-result-object v0

    goto :goto_0

    .line 889
    :sswitch_3
    check-cast p1, LatD;

    .line 891
    invoke-virtual {p1}, LatD;->provideHighlightFeaturesConfig()Lasa;

    move-result-object v0

    goto :goto_0

    .line 894
    :sswitch_4
    check-cast p1, LatD;

    .line 896
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->r:Lbsk;

    .line 899
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lase;

    .line 896
    invoke-virtual {p1, v0}, LatD;->provideCatalog(Lase;)Latj;

    move-result-object v0

    goto :goto_0

    .line 903
    :sswitch_5
    check-cast p1, LarW;

    .line 905
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->I:Lbsk;

    .line 908
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 905
    invoke-virtual {p1, v0}, LarW;->getLazy2(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 912
    :sswitch_6
    check-cast p1, LatD;

    .line 914
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->p:Lbsk;

    .line 917
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasc;

    .line 914
    invoke-virtual {p1, v0}, LatD;->provideHighlightsLauncher(Lasc;)Lasb;

    move-result-object v0

    goto :goto_0

    .line 921
    :sswitch_7
    check-cast p1, LarW;

    .line 923
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->H:Lbsk;

    .line 926
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 923
    invoke-virtual {p1, v0}, LarW;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 930
    :sswitch_8
    check-cast p1, LarW;

    .line 932
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->y:Lbsk;

    .line 935
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 932
    invoke-virtual {p1, v0}, LarW;->getLazy3(Laja;)Lajw;

    move-result-object v0

    goto/16 :goto_0

    .line 939
    :sswitch_9
    check-cast p1, LarW;

    .line 941
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v2, v0, LarZ;->f:Lbsk;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 948
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 952
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 941
    invoke-virtual {p1, v2, v0, v1}, LarW;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto/16 :goto_0

    .line 956
    :sswitch_a
    check-cast p1, LarW;

    .line 958
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v2, v0, LarZ;->E:Lbsk;

    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 965
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 969
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 958
    invoke-virtual {p1, v2, v0, v1}, LarW;->get2(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto/16 :goto_0

    .line 852
    :sswitch_data_0
    .sparse-switch
        0x1e9 -> :sswitch_0
        0x1ea -> :sswitch_3
        0x1eb -> :sswitch_1
        0x1ed -> :sswitch_2
        0x1fd -> :sswitch_4
        0x1ff -> :sswitch_5
        0x200 -> :sswitch_a
        0x209 -> :sswitch_6
        0x20a -> :sswitch_7
        0x20b -> :sswitch_9
        0x20d -> :sswitch_8
    .end sparse-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 385
    const-class v0, Lasy;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x3d

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 388
    const-class v0, Latd;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x3e

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 391
    const-class v0, LasZ;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x41

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 394
    const-class v0, Lasx;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x42

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 397
    const-class v0, LarX;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x3f

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 400
    const-class v0, LarT;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x40

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 403
    const-class v0, LasQ;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x43

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 406
    const-class v0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x44

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 409
    const-class v0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x45

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 412
    const-class v0, Lcom/google/android/apps/docs/welcome/Page;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x47

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 415
    const-class v0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x48

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 418
    const-class v0, Lati;

    new-instance v1, LbvO;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvO;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 419
    const-class v0, Lati;

    new-instance v1, LbvP;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvP;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 420
    const-class v0, Lasb;

    new-instance v1, LbvO;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvO;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 421
    const-class v0, Lasb;

    new-instance v1, LbvP;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvP;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 422
    const-class v0, LatC;

    iget-object v1, p0, LarZ;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 423
    const-class v0, LasP;

    sget-object v1, LQH;->e:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 424
    const-class v0, Lasa;

    new-instance v1, LbvO;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvO;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 425
    const-class v0, Lasy;

    iget-object v1, p0, LarZ;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 426
    const-class v0, LarX;

    iget-object v1, p0, LarZ;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 427
    const-class v0, Lato;

    iget-object v1, p0, LarZ;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 428
    const-class v0, LarT;

    iget-object v1, p0, LarZ;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 429
    const-class v0, Lasl;

    iget-object v1, p0, LarZ;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 430
    const-class v0, LasR;

    iget-object v1, p0, LarZ;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 431
    const-class v0, LasQ;

    iget-object v1, p0, LarZ;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 432
    const-class v0, Lass;

    iget-object v1, p0, LarZ;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 433
    const-class v0, Lasc;

    iget-object v1, p0, LarZ;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 434
    const-class v0, LasZ;

    iget-object v1, p0, LarZ;->q:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 435
    const-class v0, Lase;

    iget-object v1, p0, LarZ;->r:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 436
    const-class v0, Latd;

    iget-object v1, p0, LarZ;->s:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 437
    const-class v0, Lasz;

    iget-object v1, p0, LarZ;->t:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 438
    const-class v0, Lasa;

    new-instance v1, LbvP;

    const-string v2, ""

    invoke-direct {v1, v2}, LbvP;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->u:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 439
    const-class v0, Lasa;

    iget-object v1, p0, LarZ;->v:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 440
    const-class v0, Lasb;

    iget-object v1, p0, LarZ;->w:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 441
    const-class v0, Lati;

    iget-object v1, p0, LarZ;->x:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 442
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lasa;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->y:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 443
    const-class v0, LasI;

    iget-object v1, p0, LarZ;->z:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 444
    const-class v0, LasX;

    iget-object v1, p0, LarZ;->A:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 445
    const-class v0, Lasa;

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->B:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 446
    const-class v0, Latj;

    iget-object v1, p0, LarZ;->C:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 447
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lasb;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->D:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 448
    const-class v0, Lasb;

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->E:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 449
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LasP;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->e:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->F:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 450
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lasa;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->G:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 451
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LasP;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->e:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->H:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 452
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lasb;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->j:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LarZ;->I:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 453
    iget-object v0, p0, LarZ;->a:Lbsk;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPh;

    iget-object v1, v1, LPh;->a:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 455
    iget-object v0, p0, LarZ;->b:Lbsk;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->k:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 457
    iget-object v0, p0, LarZ;->c:Lbsk;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPh;

    iget-object v1, v1, LPh;->b:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 459
    iget-object v0, p0, LarZ;->d:Lbsk;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->p:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 461
    iget-object v0, p0, LarZ;->e:Lbsk;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LOX;

    iget-object v1, v1, LOX;->a:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 463
    iget-object v0, p0, LarZ;->f:Lbsk;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->n:Lbsk;

    invoke-static {v1}, LarZ;->a(Lbxw;)LbuE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 466
    iget-object v0, p0, LarZ;->g:Lbsk;

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPh;

    iget-object v1, v1, LPh;->c:Lbsk;

    invoke-static {v1}, LarZ;->a(Lbxw;)LbuE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 469
    iget-object v0, p0, LarZ;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1f0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 471
    iget-object v0, p0, LarZ;->i:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1f5

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 473
    iget-object v0, p0, LarZ;->j:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1f6

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 475
    iget-object v0, p0, LarZ;->k:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1f4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 477
    iget-object v0, p0, LarZ;->l:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1fc

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 479
    iget-object v0, p0, LarZ;->m:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x83

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 481
    iget-object v0, p0, LarZ;->n:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x202

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 483
    iget-object v0, p0, LarZ;->o:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x206

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 485
    iget-object v0, p0, LarZ;->p:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1fb

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 487
    iget-object v0, p0, LarZ;->q:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1ee

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 489
    iget-object v0, p0, LarZ;->r:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1fe

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 491
    iget-object v0, p0, LarZ;->s:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x20e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 493
    iget-object v0, p0, LarZ;->t:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1ec

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 495
    iget-object v0, p0, LarZ;->y:Lbsk;

    const-class v1, LarW;

    const/16 v2, 0x1e9

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 497
    iget-object v0, p0, LarZ;->z:Lbsk;

    const-class v1, LatD;

    const/16 v2, 0x1eb

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 499
    iget-object v0, p0, LarZ;->A:Lbsk;

    const-class v1, LatD;

    const/16 v2, 0x1ed

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 501
    iget-object v0, p0, LarZ;->B:Lbsk;

    const-class v1, LatD;

    const/16 v2, 0x1ea

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 503
    iget-object v0, p0, LarZ;->C:Lbsk;

    const-class v1, LatD;

    const/16 v2, 0x1fd

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 505
    iget-object v0, p0, LarZ;->D:Lbsk;

    const-class v1, LarW;

    const/16 v2, 0x1ff

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 507
    iget-object v0, p0, LarZ;->E:Lbsk;

    const-class v1, LatD;

    const/16 v2, 0x209

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 509
    iget-object v0, p0, LarZ;->F:Lbsk;

    const-class v1, LarW;

    const/16 v2, 0x20a

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 511
    iget-object v0, p0, LarZ;->G:Lbsk;

    const-class v1, LarW;

    const/16 v2, 0x20d

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 513
    iget-object v0, p0, LarZ;->H:Lbsk;

    const-class v1, LarW;

    const/16 v2, 0x20b

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 515
    iget-object v0, p0, LarZ;->I:Lbsk;

    const-class v1, LarW;

    const/16 v2, 0x200

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 517
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 778
    packed-switch p1, :pswitch_data_0

    .line 846
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 780
    :pswitch_1
    check-cast p2, Lasy;

    .line 782
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 783
    invoke-virtual {v0, p2}, LarZ;->a(Lasy;)V

    .line 848
    :goto_0
    return-void

    .line 786
    :pswitch_2
    check-cast p2, Latd;

    .line 788
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 789
    invoke-virtual {v0, p2}, LarZ;->a(Latd;)V

    goto :goto_0

    .line 792
    :pswitch_3
    check-cast p2, LasZ;

    .line 794
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 795
    invoke-virtual {v0, p2}, LarZ;->a(LasZ;)V

    goto :goto_0

    .line 798
    :pswitch_4
    check-cast p2, Lasx;

    .line 800
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 801
    invoke-virtual {v0, p2}, LarZ;->a(Lasx;)V

    goto :goto_0

    .line 804
    :pswitch_5
    check-cast p2, LarX;

    .line 806
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 807
    invoke-virtual {v0, p2}, LarZ;->a(LarX;)V

    goto :goto_0

    .line 810
    :pswitch_6
    check-cast p2, LarT;

    .line 812
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 813
    invoke-virtual {v0, p2}, LarZ;->a(LarT;)V

    goto :goto_0

    .line 816
    :pswitch_7
    check-cast p2, LasQ;

    .line 818
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 819
    invoke-virtual {v0, p2}, LarZ;->a(LasQ;)V

    goto :goto_0

    .line 822
    :pswitch_8
    check-cast p2, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    .line 824
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 825
    invoke-virtual {v0, p2}, LarZ;->a(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;)V

    goto :goto_0

    .line 828
    :pswitch_9
    check-cast p2, Lcom/google/android/apps/docs/welcome/WelcomeActivity;

    .line 830
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 831
    invoke-virtual {v0, p2}, LarZ;->a(Lcom/google/android/apps/docs/welcome/WelcomeActivity;)V

    goto :goto_0

    .line 834
    :pswitch_a
    check-cast p2, Lcom/google/android/apps/docs/welcome/Page;

    .line 836
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 837
    invoke-virtual {v0, p2}, LarZ;->a(Lcom/google/android/apps/docs/welcome/Page;)V

    goto :goto_0

    .line 840
    :pswitch_b
    check-cast p2, Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    .line 842
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    .line 843
    invoke-virtual {v0, p2}, LarZ;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    goto :goto_0

    .line 778
    :pswitch_data_0
    .packed-switch 0x3d
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public a(LarT;)V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->f:Lbsk;

    .line 277
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->f:Lbsk;

    .line 275
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasP;

    iput-object v0, p1, LarT;->a:LasP;

    .line 281
    return-void
.end method

.method public a(LarX;)V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lak;

    iget-object v0, v0, Lak;->a:Lbsk;

    .line 266
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lak;

    iget-object v1, v1, Lak;->a:Lbsk;

    .line 264
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM;

    iput-object v0, p1, LarX;->a:LM;

    .line 270
    return-void
.end method

.method public a(LasQ;)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->m:Lbsk;

    .line 288
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->m:Lbsk;

    .line 286
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasR;

    iput-object v0, p1, LasQ;->a:LasR;

    .line 292
    return-void
.end method

.method public a(LasZ;)V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 216
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 214
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LasZ;->a:Landroid/content/Context;

    .line 220
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 223
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 221
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, LasZ;->a:LqK;

    .line 227
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->f:Lbsk;

    .line 230
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->f:Lbsk;

    .line 228
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTF;

    iput-object v0, p1, LasZ;->a:LTF;

    .line 234
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 237
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 235
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, LasZ;->a:LQr;

    .line 241
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->k:Lbsk;

    .line 244
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->k:Lbsk;

    .line 242
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTd;

    iput-object v0, p1, LasZ;->a:LTd;

    .line 248
    return-void
.end method

.method public a(Lasx;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->h:Lbsk;

    .line 255
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->h:Lbsk;

    .line 253
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasy;

    iput-object v0, p1, Lasx;->a:Lasy;

    .line 259
    return-void
.end method

.method public a(Lasy;)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 187
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 185
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lasy;->a:LQr;

    .line 191
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->t:Lbsk;

    .line 194
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->t:Lbsk;

    .line 192
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasz;

    iput-object v0, p1, Lasy;->a:Lasz;

    .line 198
    return-void
.end method

.method public a(Latd;)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->e:Lbsk;

    .line 205
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->e:Lbsk;

    .line 203
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LatC;

    iput-object v0, p1, Latd;->a:LatC;

    .line 209
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/welcome/Page;)V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->l:Lbsk;

    .line 330
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->l:Lbsk;

    .line 328
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasl;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/Page;->a:Lasl;

    .line 334
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->z:Lbsk;

    .line 337
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->z:Lbsk;

    .line 335
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasI;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/Page;->a:LasI;

    .line 341
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 299
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 297
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LqK;

    .line 303
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->i:Lbsk;

    .line 306
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->i:Lbsk;

    .line 304
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarX;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LarX;

    .line 310
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->A:Lbsk;

    .line 313
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->A:Lbsk;

    .line 311
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasX;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LasX;

    .line 317
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/welcome/WelcomeActivity;)V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    .line 322
    invoke-virtual {v0, p1}, LpG;->a(LpH;)V

    .line 323
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 348
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 346
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LqK;

    .line 352
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->o:Lbsk;

    .line 355
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->o:Lbsk;

    .line 353
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lass;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Lass;

    .line 359
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->s:Lbsk;

    .line 362
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->s:Lbsk;

    .line 360
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latd;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Latd;

    .line 366
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->l:Lbsk;

    .line 369
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->l:Lbsk;

    .line 367
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasl;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Lasl;

    .line 373
    iget-object v0, p0, LarZ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->b:Lbsk;

    .line 376
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LarZ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->b:Lbsk;

    .line 374
    invoke-static {v0, v1}, LarZ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqF;

    iput-object v0, p1, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LqF;

    .line 380
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 521
    return-void
.end method
