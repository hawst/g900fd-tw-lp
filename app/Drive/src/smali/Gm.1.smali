.class public abstract enum LGm;
.super Ljava/lang/Enum;
.source "FileOpenerIntentCreatorImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LGm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LGm;

.field private static final synthetic a:[LGm;

.field public static final enum b:LGm;

.field public static final enum c:LGm;

.field public static final enum d:LGm;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    new-instance v0, LGn;

    const-string v1, "URI_WITH_MIME_TYPE"

    invoke-direct {v0, v1, v2}, LGn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGm;->a:LGm;

    .line 95
    new-instance v0, LGo;

    const-string v1, "URI_WITHOUT_MIME_TYPE"

    invoke-direct {v0, v1, v3}, LGo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGm;->b:LGm;

    .line 102
    new-instance v0, LGp;

    const-string v1, "EXTENSION_WITH_MIME_TYPE"

    invoke-direct {v0, v1, v4}, LGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGm;->c:LGm;

    .line 109
    new-instance v0, LGq;

    const-string v1, "EXTENSION_WITHOUT_MIME_TYPE"

    invoke-direct {v0, v1, v5}, LGq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGm;->d:LGm;

    .line 87
    const/4 v0, 0x4

    new-array v0, v0, [LGm;

    sget-object v1, LGm;->a:LGm;

    aput-object v1, v0, v2

    sget-object v1, LGm;->b:LGm;

    aput-object v1, v0, v3

    sget-object v1, LGm;->c:LGm;

    aput-object v1, v0, v4

    sget-object v1, LGm;->d:LGm;

    aput-object v1, v0, v5

    sput-object v0, LGm;->a:[LGm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILGl;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, LGm;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LGm;
    .locals 1

    .prologue
    .line 87
    const-class v0, LGm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LGm;

    return-object v0
.end method

.method public static values()[LGm;
    .locals 1

    .prologue
    .line 87
    sget-object v0, LGm;->a:[LGm;

    invoke-virtual {v0}, [LGm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGm;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;Landroid/net/Uri;Landroid/net/Uri;)LGk;
.end method
