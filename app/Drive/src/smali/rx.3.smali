.class public final enum Lrx;
.super Ljava/lang/Enum;
.source "ClientMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lrx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lrx;

.field private static final synthetic a:[Lrx;

.field public static final enum b:Lrx;

.field public static final enum c:Lrx;

.field public static final enum d:Lrx;

.field public static final e:Lrx;


# instance fields
.field public final a:Ljava/lang/String;

.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v13, 0x1

    .line 16
    new-instance v0, Lrx;

    const-string v1, "RELEASE"

    const-string v6, "_r1.0"

    const-string v8, "client_flags"

    move v4, v2

    move v5, v2

    move-object v7, v3

    invoke-direct/range {v0 .. v8}, Lrx;-><init>(Ljava/lang/String;ILjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lrx;->a:Lrx;

    .line 19
    new-instance v4, Lrx;

    const-string v5, "DOGFOOD"

    const-string v7, "d"

    const-string v10, "_dogfood"

    const-string v12, "dogfood_client_flags"

    move v6, v13

    move v8, v13

    move v9, v13

    move-object v11, v3

    invoke-direct/range {v4 .. v12}, Lrx;-><init>(Ljava/lang/String;ILjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v4, Lrx;->b:Lrx;

    .line 22
    new-instance v4, Lrx;

    const-string v5, "CAKEFOOD"

    const-string v7, "c"

    const-string v10, "_cakefood"

    const-string v12, "cakefood_client_flags"

    move v6, v14

    move v8, v13

    move v9, v13

    move-object v11, v3

    invoke-direct/range {v4 .. v12}, Lrx;-><init>(Ljava/lang/String;ILjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v4, Lrx;->c:Lrx;

    .line 26
    new-instance v3, Lrx;

    const-string v4, "EXPERIMENTAL"

    const-string v6, "x"

    const-string v9, "_cakefood"

    const-string v10, "docs_flags"

    const-string v11, "cakefood_client_flags"

    move v5, v15

    move v7, v13

    move v8, v2

    invoke-direct/range {v3 .. v11}, Lrx;-><init>(Ljava/lang/String;ILjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lrx;->d:Lrx;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lrx;

    sget-object v1, Lrx;->a:Lrx;

    aput-object v1, v0, v2

    sget-object v1, Lrx;->b:Lrx;

    aput-object v1, v0, v13

    sget-object v1, Lrx;->c:Lrx;

    aput-object v1, v0, v14

    sget-object v1, Lrx;->d:Lrx;

    aput-object v1, v0, v15

    sput-object v0, Lrx;->a:[Lrx;

    .line 29
    sget-object v0, Lrx;->a:Lrx;

    sput-object v0, Lrx;->e:Lrx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, Lrx;->a:Ljava/lang/String;

    .line 51
    iput-boolean p4, p0, Lrx;->a:Z

    .line 52
    iput-boolean p5, p0, Lrx;->b:Z

    .line 53
    iput-object p7, p0, Lrx;->c:Ljava/lang/String;

    .line 54
    iput-object p6, p0, Lrx;->d:Ljava/lang/String;

    .line 55
    iput-object p8, p0, Lrx;->b:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public static a(Ljava/lang/String;)Lrx;
    .locals 6

    .prologue
    .line 85
    const/4 v1, 0x0

    .line 87
    invoke-static {}, Lrx;->values()[Lrx;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 88
    iget-object v5, v0, Lrx;->a:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 87
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 90
    :cond_0
    iget-object v5, v0, Lrx;->a:Ljava/lang/String;

    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 95
    :goto_2
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lrx;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lrx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lrx;

    return-object v0
.end method

.method public static values()[Lrx;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lrx;->a:[Lrx;

    invoke-virtual {v0}, [Lrx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lrx;

    return-object v0
.end method


# virtual methods
.method public a(Lrx;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 72
    if-nez p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lrx;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
