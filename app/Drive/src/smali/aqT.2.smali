.class public LaqT;
.super Ljava/lang/Object;
.source "TouchEventTracker.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaqU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 27
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaqT;->a:Ljava/util/Set;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Landroid/view/DragEvent;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, LaqT;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqU;

    .line 37
    invoke-interface {v0, p1}, LaqU;->a(Landroid/view/DragEvent;)V

    goto :goto_0

    .line 39
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, LaqT;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqU;

    .line 31
    invoke-interface {v0, p1}, LaqU;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 33
    :cond_0
    return-void
.end method

.method public a(LaqU;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LaqT;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method
