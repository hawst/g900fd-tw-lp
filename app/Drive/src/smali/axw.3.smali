.class public Laxw;
.super Ljava/lang/Object;
.source "SelectionModel.java"


# instance fields
.field private final a:Lawh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawh",
            "<",
            "Lcom/google/android/apps/viewer/pdflib/Selection;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Laxz;


# direct methods
.method public constructor <init>(Laxz;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-static {v0}, Lawg;->a(Ljava/lang/Object;)Lawh;

    move-result-object v0

    iput-object v0, p0, Laxw;->a:Lawh;

    .line 19
    iput-object p1, p0, Laxw;->a:Laxz;

    .line 20
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Laxw;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/pdflib/Selection;

    .line 28
    if-eqz v0, :cond_0

    iget v0, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a()Lawe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lawe",
            "<",
            "Lcom/google/android/apps/viewer/pdflib/Selection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Laxw;->a:Lawh;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Laxw;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/pdflib/Selection;

    .line 33
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->text:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/viewer/pdflib/Selection;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Laxw;->a:Lawh;

    invoke-virtual {v0, p1}, Lawh;->b(Ljava/lang/Object;)V

    .line 39
    return-void
.end method

.method public a(Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Laxw;->a:Laxz;

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0}, Laxw;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 49
    iget-object v1, p0, Laxw;->a:Laxz;

    invoke-virtual {v1, v0, p1, p2}, Laxz;->a(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V

    .line 51
    :cond_0
    return-void
.end method
