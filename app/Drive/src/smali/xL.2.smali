.class public final LxL;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LxK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 38
    iput-object p1, p0, LxL;->a:LbrA;

    .line 39
    const-class v0, LxK;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LxL;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LxL;->a:Lbsk;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 90
    packed-switch p1, :pswitch_data_0

    .line 98
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :pswitch_0
    new-instance v0, LxK;

    invoke-direct {v0}, LxK;-><init>()V

    .line 94
    iget-object v1, p0, LxL;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LxL;

    .line 95
    invoke-virtual {v1, v0}, LxL;->a(LxK;)V

    .line 96
    return-object v0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x2bb
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 125
    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 73
    const-class v0, LxK;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x68

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LxL;->a(LbuP;LbuB;)V

    .line 76
    const-class v0, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x69

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LxL;->a(LbuP;LbuB;)V

    .line 79
    const-class v0, LxK;

    iget-object v1, p0, LxL;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LxL;->a(Ljava/lang/Class;Lbsk;)V

    .line 80
    iget-object v0, p0, LxL;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2bb

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 82
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 105
    packed-switch p1, :pswitch_data_0

    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :pswitch_0
    check-cast p2, LxK;

    .line 109
    iget-object v0, p0, LxL;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LxL;

    .line 110
    invoke-virtual {v0, p2}, LxL;->a(LxK;)V

    .line 121
    :goto_0
    return-void

    .line 113
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;

    .line 115
    iget-object v0, p0, LxL;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LxL;

    .line 116
    invoke-virtual {v0, p2}, LxL;->a(Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;)V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LxL;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    .line 67
    invoke-virtual {v0, p1}, LpG;->a(LpH;)V

    .line 68
    return-void
.end method

.method public a(LxK;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, LxL;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHX;

    iget-object v0, v0, LaHX;->m:Lbsk;

    .line 51
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LxL;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHX;

    iget-object v1, v1, LaHX;->m:Lbsk;

    .line 49
    invoke-static {v0, v1}, LxL;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIm;

    iput-object v0, p1, LxK;->a:LaIm;

    .line 55
    iget-object v0, p0, LxL;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 58
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LxL;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 56
    invoke-static {v0, v1}, LxL;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LxK;->a:Landroid/content/Context;

    .line 62
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method
