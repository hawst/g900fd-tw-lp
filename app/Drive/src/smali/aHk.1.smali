.class public LaHk;
.super LaHq;
.source "LastViewedOp.java"


# instance fields
.field protected final a:Ljava/util/Date;


# direct methods
.method private constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 71
    const-string v0, "viewed"

    invoke-direct {p0, p1, p2, v0}, LaHq;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 72
    iput-object p3, p0, LaHk;->a:Ljava/util/Date;

    .line 73
    return-void
.end method

.method public static a(LaGg;LaGd;Ljava/util/Date;)LaHk;
    .locals 2

    .prologue
    .line 46
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-virtual {p1}, LaGd;->c()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaGd;->c()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p1}, LaGd;->c()Ljava/util/Date;

    move-result-object p2

    .line 53
    :cond_0
    new-instance v0, LaHk;

    invoke-virtual {p1}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, LaHk;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/util/Date;)V

    return-object v0
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHk;
    .locals 4

    .prologue
    .line 62
    const/4 v0, 0x0

    .line 63
    const-string v1, "lastViewed"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    new-instance v0, Ljava/util/Date;

    const-string v1, "lastViewed"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 66
    :cond_0
    new-instance v1, LaHk;

    invoke-direct {v1, p0, p1, v0}, LaHk;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/util/Date;)V

    return-object v1
.end method


# virtual methods
.method public a(LaGe;)LaHo;
    .locals 4

    .prologue
    .line 85
    new-instance v0, LaHk;

    iget-object v1, p0, LaHk;->a:LaGg;

    .line 86
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-virtual {p1}, LaGe;->c()Ljava/util/Date;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaHk;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/util/Date;)V

    .line 87
    iget-object v1, p0, LaHk;->a:Ljava/util/Date;

    invoke-virtual {p1, v1}, LaGe;->c(Ljava/util/Date;)LaGe;

    .line 88
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 93
    invoke-super {p0}, LaHq;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 94
    const-string v1, "operationName"

    const-string v2, "viewed"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 95
    iget-object v1, p0, LaHk;->a:Ljava/util/Date;

    if-eqz v1, :cond_0

    .line 96
    const-string v1, "lastViewed"

    iget-object v2, p0, LaHk;->a:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 98
    :cond_0
    return-object v0
.end method

.method public a(LaJT;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, LaHk;->a:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaHk;->a:Ljava/util/Date;

    invoke-static {v0}, LaGG;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    invoke-interface {p1, v0}, LaJT;->f(Ljava/lang/String;)V

    .line 79
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 103
    instance-of v1, p1, LaHk;

    if-nez v1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    check-cast p1, LaHk;

    .line 107
    invoke-virtual {p0, p1}, LaHk;->a(LaHp;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaHk;->a:Ljava/util/Date;

    iget-object v2, p1, LaHk;->a:Ljava/util/Date;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0}, LaHk;->b()I

    move-result v1

    iget-object v0, p0, LaHk;->a:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaHk;->a:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x11

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 117
    const-string v0, "LastViewedOp[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaHk;->a:Ljava/util/Date;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaHk;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
