.class public abstract LSX;
.super Larc;
.source "SearchBase.java"


# instance fields
.field protected final a:Lara;

.field protected final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Larg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected varargs constructor <init>(Lara;Lbxw;Landroid/app/Activity;Larj;II[Ljava/lang/Integer;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lara;",
            "Lbxw",
            "<",
            "Larg;",
            ">;",
            "Landroid/app/Activity;",
            "Larj;",
            "II[",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Larc;-><init>(Landroid/app/Activity;Larj;ZII[Ljava/lang/Integer;)V

    .line 33
    iput-object p1, p0, LSX;->a:Lara;

    .line 34
    iput-object p2, p0, LSX;->a:Lbxw;

    .line 35
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 49
    sget v0, Lxc;->back_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 50
    new-instance v1, LSY;

    invoke-direct {v1, p0}, LSY;-><init>(LSX;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/view/View;)V
.end method

.method public a(Larc;Z)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Larc;->a(Larc;Z)V

    .line 41
    invoke-virtual {p0}, LSX;->a()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 44
    invoke-direct {p0, v1}, LSX;->b(Landroid/view/View;)V

    .line 45
    invoke-virtual {p0, v1}, LSX;->a(Landroid/view/View;)V

    .line 46
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
