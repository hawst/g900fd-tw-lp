.class public Laqi;
.super Landroid/widget/ScrollView;
.source "LinearLayoutListView.java"


# instance fields
.field a:I

.field final synthetic a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

.field b:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 222
    iput-object p1, p0, Laqi;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    .line 223
    invoke-direct {p0, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 224
    invoke-virtual {p0}, Laqi;->getScrollX()I

    move-result v0

    iput v0, p0, Laqi;->a:I

    .line 225
    invoke-virtual {p0}, Laqi;->getScrollY()I

    move-result v0

    iput v0, p0, Laqi;->b:I

    .line 226
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 230
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 234
    iget v0, p0, Laqi;->a:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Laqi;->b:I

    if-eq v0, p2, :cond_1

    .line 235
    :cond_0
    iput p1, p0, Laqi;->a:I

    .line 236
    iput p2, p0, Laqi;->b:I

    .line 237
    iget-object v0, p0, Laqi;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(IIII)V

    .line 239
    :cond_1
    return-void
.end method
