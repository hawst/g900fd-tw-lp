.class Lps;
.super Ljava/lang/Object;
.source "Reflector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lps;->a:Ljava/lang/Object;

    .line 25
    return-void
.end method


# virtual methods
.method a(Lpu;)Ljava/lang/reflect/AccessibleObject;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/reflect/AccessibleObject;",
            ":",
            "Ljava/lang/reflect/Member;",
            ">(",
            "Lpu",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 62
    iget-object v0, p0, Lps;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 63
    invoke-interface {p1, v0}, Lpu;->a(Ljava/lang/Class;)Ljava/lang/reflect/AccessibleObject;

    move-result-object v1

    .line 64
    if-nez v1, :cond_0

    .line 62
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {v1}, Ljava/lang/reflect/AccessibleObject;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    invoke-virtual {v1, v4}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 72
    :cond_1
    return-object v1

    .line 75
    :cond_2
    new-instance v0, Lpr;

    const-string v1, "%s not found in %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    iget-object v3, p0, Lps;->a:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lpr;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a(Ljava/lang/String;)Lpp;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lpt;

    invoke-direct {v0, p1}, Lpt;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lps;->a(Lpu;)Ljava/lang/reflect/AccessibleObject;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    .line 36
    new-instance v1, Lpp;

    iget-object v2, p0, Lps;->a:Ljava/lang/Object;

    invoke-direct {v1, v2, v0}, Lpp;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Field;)V

    return-object v1
.end method

.method varargs a(Ljava/lang/String;[Ljava/lang/Class;)Lpq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)",
            "Lpq;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lpv;

    invoke-direct {v0, p1, p2}, Lpv;-><init>(Ljava/lang/String;[Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lps;->a(Lpu;)Ljava/lang/reflect/AccessibleObject;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    .line 49
    new-instance v1, Lpq;

    iget-object v2, p0, Lps;->a:Ljava/lang/Object;

    invoke-direct {v1, v2, v0}, Lpq;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    return-object v1
.end method
