.class public LahI;
.super Ljava/lang/Object;
.source "ScheduledSyncTaskImpl.java"


# instance fields
.field private final a:LaGg;

.field private final a:LaKM;

.field private final a:LaKR;

.field private final a:Lagd;

.field private final a:LahD;

.field private final a:LahO;

.field private final a:LahP;

.field private final a:Lamn;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Lahn;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:LtK;


# direct methods
.method constructor <init>(Lagd;LaKM;LaGg;LahD;LahO;LahP;LaKR;Lamn;Laja;LtK;LqK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lagd;",
            "LaKM;",
            "LaGg;",
            "LahD;",
            "LahO;",
            "LahP;",
            "LaKR;",
            "Lamn;",
            "Laja",
            "<",
            "Lahn;",
            ">;",
            "LtK;",
            "LqK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, LahI;->a:Lagd;

    .line 86
    iput-object p2, p0, LahI;->a:LaKM;

    .line 87
    iput-object p3, p0, LahI;->a:LaGg;

    .line 88
    iput-object p4, p0, LahI;->a:LahD;

    .line 89
    iput-object p5, p0, LahI;->a:LahO;

    .line 90
    iput-object p6, p0, LahI;->a:LahP;

    .line 91
    iput-object p9, p0, LahI;->a:Lbxw;

    .line 92
    iput-object p7, p0, LahI;->a:LaKR;

    .line 93
    iput-object p8, p0, LahI;->a:Lamn;

    .line 94
    iput-object p10, p0, LahI;->a:LtK;

    .line 95
    iput-object p11, p0, LahI;->a:LqK;

    .line 96
    return-void
.end method


# virtual methods
.method public a(LahL;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHd;)LahG;
    .locals 15

    .prologue
    .line 101
    new-instance v0, LahG;

    iget-object v3, p0, LahI;->a:Lagd;

    iget-object v4, p0, LahI;->a:LaKM;

    iget-object v5, p0, LahI;->a:LaGg;

    iget-object v6, p0, LahI;->a:LahD;

    iget-object v7, p0, LahI;->a:LahO;

    iget-object v8, p0, LahI;->a:LahP;

    iget-object v1, p0, LahI;->a:Lbxw;

    .line 110
    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lahn;

    iget-object v11, p0, LahI;->a:LaKR;

    iget-object v12, p0, LahI;->a:Lamn;

    iget-object v13, p0, LahI;->a:LtK;

    iget-object v14, p0, LahI;->a:LqK;

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v9, p1

    invoke-direct/range {v0 .. v14}, LahG;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHd;Lagd;LaKM;LaGg;LahD;LahO;LahP;LahL;Lahn;LaKR;Lamn;LtK;LqK;)V

    return-object v0
.end method
