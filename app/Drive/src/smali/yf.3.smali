.class public Lyf;
.super Ljava/lang/Object;
.source "CrossAppPromoManagerImpl.java"

# interfaces
.implements Lye;


# instance fields
.field private final a:LasR;

.field private final a:LtK;

.field private final a:LxV;

.field private final a:Lyb;

.field private final a:Lyd;


# direct methods
.method public constructor <init>(LasR;LtK;LxV;Lyd;Lyb;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasR;

    iput-object v0, p0, Lyf;->a:LasR;

    .line 38
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, Lyf;->a:LtK;

    .line 39
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxV;

    iput-object v0, p0, Lyf;->a:LxV;

    .line 40
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyd;

    iput-object v0, p0, Lyf;->a:Lyd;

    .line 41
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyb;

    iput-object v0, p0, Lyf;->a:Lyb;

    .line 42
    return-void
.end method

.method private a(Ljava/lang/String;)LasP;
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lyf;->a:LxV;

    invoke-interface {v0, p1}, LxV;->a(Ljava/lang/String;)Ladj;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lyf;->a:Lyb;

    invoke-interface {v1, p1}, Lyb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    invoke-static {v1}, Lath;->a(Ljava/lang/String;)Lath;

    move-result-object v1

    .line 123
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 124
    :cond_0
    const/4 v0, 0x0

    .line 127
    :goto_0
    return-object v0

    .line 126
    :cond_1
    iget-object v2, p0, Lyf;->a:LasR;

    invoke-virtual {v1}, Lath;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LasR;->a(Ladj;Ljava/lang/String;)LasP;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 133
    invoke-static {p1, p2}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lyf;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-virtual {p0, p1}, Lyf;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    iget-object v1, p0, Lyf;->a:Lyb;

    invoke-interface {v1, p1}, Lyb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-static {p1, v1}, LxO;->a(Ljava/lang/String;Ljava/lang/String;)LxO;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, LxO;->c()Ljava/lang/String;

    move-result-object v2

    .line 83
    invoke-direct {p0, v2}, Lyf;->a(Ljava/lang/String;)LasP;

    move-result-object v2

    .line 84
    invoke-virtual {v1}, LxO;->d()Ljava/lang/String;

    move-result-object v1

    .line 85
    iget-object v3, p0, Lyf;->a:Lyd;

    invoke-virtual {v3, v2, v1, p2, v0}, Lyd;->a(LasP;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 89
    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lyf;->a:LtK;

    sget-object v1, Lry;->k:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lyf;->a:Lyb;

    invoke-interface {v0, p1}, Lyb;->b(Ljava/lang/String;)Z

    move-result v0

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-virtual {p0, p1}, Lyf;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lyf;->a:Lyb;

    invoke-interface {v1, p1}, Lyb;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-static {p1, v1}, LxO;->a(Ljava/lang/String;Ljava/lang/String;)LxO;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, LxO;->c()Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-direct {p0, v2}, Lyf;->a(Ljava/lang/String;)LasP;

    move-result-object v2

    .line 106
    invoke-virtual {v1}, LxO;->d()Ljava/lang/String;

    move-result-object v1

    .line 107
    iget-object v3, p0, Lyf;->a:Lyd;

    invoke-virtual {v3, v2, v1, v0, v0}, Lyd;->a(LasP;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 111
    :cond_0
    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lyf;->a:LtK;

    sget-object v1, Lry;->k:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lyf;->a:Lyb;

    invoke-interface {v0, p1}, Lyb;->c(Ljava/lang/String;)Z

    move-result v0

    .line 59
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lyf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lyf;->a:Lyb;

    invoke-interface {v0, p1}, Lyb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-static {p1, v0}, LxO;->a(Ljava/lang/String;Ljava/lang/String;)LxO;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, LxO;->a()Z

    move-result v0

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
