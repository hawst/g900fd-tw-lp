.class public LavB;
.super Ljava/util/AbstractList;
.source "MatchRects.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

.field final synthetic b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;II)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, LavB;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    iput p2, p0, LavB;->a:I

    iput p3, p0, LavB;->b:I

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 78
    iget v0, p0, LavB;->a:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LavB;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    # getter for: Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->access$000(Lcom/google/android/apps/viewer/pdflib/MatchRects;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 79
    :goto_0
    return-object v0

    .line 78
    :cond_0
    iget-object v0, p0, LavB;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    .line 79
    # getter for: Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->access$000(Lcom/google/android/apps/viewer/pdflib/MatchRects;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, LavB;->a:I

    sub-int v1, p1, v1

    iget v2, p0, LavB;->b:I

    add-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, LavB;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, LavB;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    # getter for: Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->access$000(Lcom/google/android/apps/viewer/pdflib/MatchRects;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LavB;->b:I

    iget v2, p0, LavB;->a:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    return v0
.end method
