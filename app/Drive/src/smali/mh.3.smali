.class Lmh;
.super Lmd;
.source "SystemMediaRouteProvider.java"

# interfaces
.implements LlH;


# instance fields
.field private a:LlG;

.field private a:LlJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmm;)V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0, p1, p2}, Lmd;-><init>(Landroid/content/Context;Lmm;)V

    .line 717
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 772
    invoke-static {p0}, LlF;->a(LlH;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lmf;LkP;)V
    .locals 1

    .prologue
    .line 742
    invoke-super {p0, p1, p2}, Lmd;->a(Lmf;LkP;)V

    .line 744
    iget-object v0, p1, Lmf;->a:Ljava/lang/Object;

    invoke-static {v0}, LlK;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 745
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LkP;->a(Z)LkP;

    .line 748
    :cond_0
    invoke-virtual {p0, p1}, Lmh;->a(Lmf;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LkP;->b(Z)LkP;

    .line 752
    :cond_1
    iget-object v0, p1, Lmf;->a:Ljava/lang/Object;

    invoke-static {v0}, LlK;->a(Ljava/lang/Object;)Landroid/view/Display;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_2

    .line 755
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p2, v0}, LkP;->f(I)LkP;

    .line 757
    :cond_2
    return-void
.end method

.method protected a(Lmf;)Z
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lmh;->a:LlJ;

    if-nez v0, :cond_0

    .line 777
    new-instance v0, LlJ;

    invoke-direct {v0}, LlJ;-><init>()V

    iput-object v0, p0, Lmh;->a:LlJ;

    .line 779
    :cond_0
    iget-object v0, p0, Lmh;->a:LlJ;

    iget-object v1, p1, Lmf;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LlJ;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 761
    invoke-super {p0}, Lmd;->b()V

    .line 763
    iget-object v0, p0, Lmh;->a:LlG;

    if-nez v0, :cond_0

    .line 764
    new-instance v0, LlG;

    invoke-virtual {p0}, Lmh;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lmh;->a()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LlG;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lmh;->a:LlG;

    .line 767
    :cond_0
    iget-object v1, p0, Lmh;->a:LlG;

    iget-boolean v0, p0, Lmh;->a:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmh;->a:I

    :goto_0
    invoke-virtual {v1, v0}, LlG;->a(I)V

    .line 768
    return-void

    .line 767
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 721
    invoke-virtual {p0, p1}, Lmh;->a(Ljava/lang/Object;)I

    move-result v0

    .line 722
    if-ltz v0, :cond_0

    .line 723
    iget-object v1, p0, Lmh;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmf;

    .line 724
    invoke-static {p1}, LlK;->a(Ljava/lang/Object;)Landroid/view/Display;

    move-result-object v1

    .line 726
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    .line 728
    :goto_0
    iget-object v2, v0, Lmf;->a:LkN;

    invoke-virtual {v2}, LkN;->f()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 730
    new-instance v2, LkP;

    iget-object v3, v0, Lmf;->a:LkN;

    invoke-direct {v2, v3}, LkP;-><init>(LkN;)V

    invoke-virtual {v2, v1}, LkP;->f(I)LkP;

    move-result-object v1

    invoke-virtual {v1}, LkP;->a()LkN;

    move-result-object v1

    iput-object v1, v0, Lmf;->a:LkN;

    .line 734
    invoke-virtual {p0}, Lmh;->a()V

    .line 737
    :cond_0
    return-void

    .line 726
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method
