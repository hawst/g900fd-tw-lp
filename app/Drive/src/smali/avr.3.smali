.class public final enum Lavr;
.super Ljava/lang/Enum;
.source "Fetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lavr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lavr;

.field private static final synthetic a:[Lavr;

.field public static final enum b:Lavr;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    new-instance v0, Lavr;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2}, Lavr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavr;->a:Lavr;

    new-instance v0, Lavr;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v3}, Lavr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavr;->b:Lavr;

    const/4 v0, 0x2

    new-array v0, v0, [Lavr;

    sget-object v1, Lavr;->a:Lavr;

    aput-object v1, v0, v2

    sget-object v1, Lavr;->b:Lavr;

    aput-object v1, v0, v3

    sput-object v0, Lavr;->a:[Lavr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lavr;
    .locals 1

    .prologue
    .line 58
    const-class v0, Lavr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lavr;

    return-object v0
.end method

.method public static values()[Lavr;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lavr;->a:[Lavr;

    invoke-virtual {v0}, [Lavr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lavr;

    return-object v0
.end method
