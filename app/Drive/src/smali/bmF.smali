.class public abstract LbmF;
.super Lbmv;
.source "ImmutableList.java"

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbmv",
        "<TE;>;",
        "Ljava/util/List",
        "<TE;>;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lbmv;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 218
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-static {p0}, LblV;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LbmF;->a(Ljava/util/Iterator;)LbmF;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lbqd;

    invoke-direct {v0, p0}, Lbqd;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 96
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 105
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 114
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 123
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 141
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 150
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;TE;TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 159
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 243
    instance-of v0, p0, Lbmv;

    if-eqz v0, :cond_1

    .line 246
    check-cast p0, Lbmv;

    invoke-virtual {p0}, Lbmv;->a()LbmF;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, LbmF;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LbmF;->b(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 249
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, LbmF;->b(Ljava/util/Collection;)LbmF;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Iterator;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 259
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    .line 262
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 263
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 264
    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    goto :goto_0

    .line 266
    :cond_1
    new-instance v1, LbmH;

    invoke-direct {v1}, LbmH;-><init>()V

    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    invoke-virtual {v0, p0}, LbmH;->a(Ljava/util/Iterator;)LbmH;

    move-result-object v0

    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 277
    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    .line 283
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    :goto_0
    return-object v0

    .line 279
    :pswitch_0
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    goto :goto_0

    .line 281
    :pswitch_1
    new-instance v0, Lbqd;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Lbqd;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a()LbmH;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LbmH",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 624
    new-instance v0, LbmH;

    invoke-direct {v0}, LbmH;-><init>()V

    return-object v0
.end method

.method private static b(Ljava/util/Collection;)LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 307
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LbmF;->b([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method static b([Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            ")",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 293
    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    .line 302
    invoke-static {p0}, LbmF;->c([Ljava/lang/Object;)LbmF;

    move-result-object v0

    :goto_0
    return-object v0

    .line 295
    :pswitch_0
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    goto :goto_0

    .line 299
    :pswitch_1
    new-instance v0, Lbqd;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Lbqd;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 293
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lbme;->a:Lbme;

    return-object v0
.end method

.method private static varargs c([Ljava/lang/Object;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            ")",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 312
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 313
    aget-object v1, p0, v0

    invoke-static {v1, v0}, Lbpv;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    :cond_0
    new-instance v0, LbpA;

    invoke-direct {v0, p0}, LbpA;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a()LbmF;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 466
    return-object p0
.end method

.method public a(II)LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 364
    invoke-virtual {p0}, LbmF;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, LbiT;->a(III)V

    .line 365
    sub-int v0, p2, p1

    .line 366
    packed-switch v0, :pswitch_data_0

    .line 372
    invoke-virtual {p0, p1, p2}, LbmF;->b(II)LbmF;

    move-result-object v0

    :goto_0
    return-object v0

    .line 368
    :pswitch_0
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    goto :goto_0

    .line 370
    :pswitch_1
    invoke-virtual {p0, p1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    goto :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()Lbqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 324
    invoke-virtual {p0}, LbmF;->a()Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public a()Lbqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqw",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 328
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LbmF;->a(I)Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lbqw;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lbqw",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 332
    new-instance v0, LbmG;

    invoke-virtual {p0}, LbmF;->size()I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, LbmG;-><init>(LbmF;II)V

    return-object v0
.end method

.method public a_()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 478
    new-instance v0, LbmI;

    invoke-direct {v0, p0}, LbmI;-><init>(LbmF;)V

    return-object v0
.end method

.method public final add(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 445
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 423
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method b(II)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 382
    new-instance v0, LbmK;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, LbmK;-><init>(LbmF;II)V

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 351
    invoke-virtual {p0, p1}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 584
    invoke-static {p0, p1}, LbnG;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 589
    invoke-static {p0}, LbnG;->a(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 342
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, LbnG;->a(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, LbmF;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 346
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, LbnG;->b(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, LbmF;->a()Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, LbmF;->a(I)Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 456
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 434
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1, p2}, LbmF;->a(II)LbmF;

    move-result-object v0

    return-object v0
.end method
