.class public abstract LhO;
.super Ljava/lang/Object;
.source "ActionBarActivityDelegate.java"


# instance fields
.field private a:Landroid/view/MenuInflater;

.field private a:LhJ;

.field public final a:LhN;

.field final a:LiC;

.field public a:Z

.field private b:LiC;

.field public b:Z

.field public c:Z

.field public d:Z

.field private e:Z


# direct methods
.method public constructor <init>(LhN;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, LhP;

    invoke-direct {v0, p0}, LhP;-><init>(LhO;)V

    iput-object v0, p0, LhO;->a:LiC;

    .line 112
    iput-object p1, p0, LhO;->a:LhN;

    .line 113
    iget-object v0, p0, LhO;->a:LiC;

    iput-object v0, p0, LhO;->b:LiC;

    .line 114
    return-void
.end method

.method static a(LhN;)LhO;
    .locals 2

    .prologue
    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 51
    new-instance v0, LhZ;

    invoke-direct {v0, p0}, LhZ;-><init>(LhN;)V

    .line 53
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;-><init>(LhN;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public final a()Landroid/content/Context;
    .locals 2

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 251
    invoke-virtual {p0}, LhO;->b()LhJ;

    move-result-object v1

    .line 252
    if-eqz v1, :cond_0

    .line 253
    invoke-virtual {v1}, LhJ;->a()Landroid/content/Context;

    move-result-object v0

    .line 256
    :cond_0
    if-nez v0, :cond_1

    .line 257
    iget-object v0, p0, LhO;->a:LhN;

    .line 259
    :cond_1
    return-object v0
.end method

.method a()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, LhO;->a:Landroid/view/MenuInflater;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, LiO;

    invoke-virtual {p0}, LhO;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LiO;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LhO;->a:Landroid/view/MenuInflater;

    .line 139
    :cond_0
    iget-object v0, p0, LhO;->a:Landroid/view/MenuInflater;

    return-object v0
.end method

.method public abstract a(I)Landroid/view/View;
.end method

.method public abstract a(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
.end method

.method public abstract a()LhJ;
.end method

.method public final a()LiC;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, LhO;->b:LiC;

    return-object v0
.end method

.method final a()Ll;
    .locals 2

    .prologue
    .line 218
    new-instance v0, LhQ;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LhQ;-><init>(LhO;LhP;)V

    return-object v0
.end method

.method public abstract a(Lmv;)Lmu;
.end method

.method public abstract a()V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(ILandroid/view/Menu;)V
.end method

.method public abstract a(Landroid/content/res/Configuration;)V
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, LhO;->a:LhN;

    sget-object v1, LiA;->Theme:[I

    invoke-virtual {v0, v1}, LhN;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 145
    sget v1, LiA;->Theme_windowActionBar:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 147
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    sget v1, LiA;->Theme_windowActionBar:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LhO;->a:Z

    .line 152
    sget v1, LiA;->Theme_windowActionBarOverlay:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LhO;->b:Z

    .line 153
    sget v1, LiA;->Theme_windowActionModeOverlay:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LhO;->c:Z

    .line 154
    sget v1, LiA;->Theme_android_windowIsFloating:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LhO;->d:Z

    .line 155
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 156
    return-void
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public abstract a()Z
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method public abstract a(ILandroid/view/Menu;)Z
.end method

.method public abstract a(ILandroid/view/View;Landroid/view/Menu;)Z
.end method

.method a(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 188
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 192
    iget-object v0, p0, LhO;->a:LhN;

    invoke-virtual {v0, p2}, LhN;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 194
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LhO;->a:LhN;

    invoke-virtual {v0, p1, p2}, LhN;->b(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()LhJ;
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, LhO;->a:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, LhO;->a:LhJ;

    if-nez v0, :cond_0

    .line 123
    invoke-virtual {p0}, LhO;->a()LhJ;

    move-result-object v0

    iput-object v0, p0, LhO;->a:LhJ;

    .line 126
    :cond_0
    iget-object v0, p0, LhO;->a:LhJ;

    return-object v0
.end method

.method public abstract b(Lmv;)Lmu;
.end method

.method public abstract b()V
.end method

.method public abstract b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, LhO;->e:Z

    return v0
.end method

.method public abstract b(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract b(ILandroid/view/Menu;)Z
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method final e()V
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, LhO;->e:Z

    .line 315
    return-void
.end method
