.class public LWa;
.super Ljava/lang/Object;
.source "CommonPreferencesInstaller.java"

# interfaces
.implements LWx;


# instance fields
.field private a:LFr;

.field a:LQr;

.field a:LSF;

.field a:LaGg;

.field a:LaKR;

.field a:LadL;

.field a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LahK;",
            ">;"
        }
    .end annotation
.end field

.field a:Lamn;

.field a:Landroid/app/Activity;

.field private a:Landroid/preference/CheckBoxPreference;

.field private a:Landroid/preference/ListPreference;

.field private a:Landroid/preference/Preference;

.field private a:Landroid/preference/PreferenceScreen;

.field a:LtK;

.field private a:Z

.field private b:LFr;

.field private b:Landroid/preference/CheckBoxPreference;

.field private c:LFr;

.field private c:Landroid/preference/CheckBoxPreference;

.field private d:LFr;

.field private e:LFr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object v1, p0, LWa;->a:Landroid/preference/ListPreference;

    .line 70
    iput-object v1, p0, LWa;->a:Landroid/preference/Preference;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, LWa;->a:Z

    .line 105
    iput-object v1, p0, LWa;->a:Landroid/preference/CheckBoxPreference;

    .line 106
    iput-object v1, p0, LWa;->b:Landroid/preference/CheckBoxPreference;

    .line 107
    iput-object v1, p0, LWa;->c:Landroid/preference/CheckBoxPreference;

    return-void
.end method

.method static synthetic a(LWa;)LFr;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LWa;->e:LFr;

    return-object v0
.end method

.method static synthetic a(LWa;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LWa;->b:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, LWa;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 480
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    return-object v0
.end method

.method static synthetic a(LWa;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, LWa;->g()V

    return-void
.end method

.method static synthetic a(LWa;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, LWa;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 463
    iget-object v0, p0, LWa;->a:Landroid/preference/ListPreference;

    iget-object v1, p0, LWa;->a:Landroid/app/Activity;

    .line 464
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxi;->prefs_cache_size_summary_format_string:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 463
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 465
    return-void
.end method

.method private a(Ljava/lang/String;LFr;)V
    .locals 2

    .prologue
    .line 194
    invoke-direct {p0, p1}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 195
    new-instance v1, LWd;

    invoke-direct {v1, p0, p2}, LWd;-><init>(LWa;LFr;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 202
    return-void
.end method

.method private a(Ljava/lang/String;LFr;LWq;)V
    .locals 2

    .prologue
    .line 217
    invoke-direct {p0, p1}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 218
    new-instance v1, LWe;

    invoke-direct {v1, p0, p3, p2}, LWe;-><init>(LWa;LWq;LFr;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 227
    return-void
.end method

.method static synthetic a(LWa;Landroid/preference/CheckBoxPreference;LWq;)Z
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, LWa;->a(Landroid/preference/CheckBoxPreference;LWq;)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/preference/CheckBoxPreference;LWq;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 206
    sget-object v1, LWq;->b:LWq;

    invoke-virtual {p2, v1}, LWq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    .line 211
    :cond_0
    :goto_0
    return v0

    .line 208
    :cond_1
    sget-object v1, LWq;->c:LWq;

    invoke-virtual {p2, v1}, LWq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 209
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(LWa;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LWa;->a:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private b(LEE;)V
    .locals 1

    .prologue
    .line 257
    new-instance v0, LWf;

    invoke-direct {v0, p0}, LWf;-><init>(LWa;)V

    invoke-interface {p1, v0}, LEE;->a(LEB;)LFr;

    move-result-object v0

    iput-object v0, p0, LWa;->a:LFr;

    .line 289
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x1

    iput-boolean v0, p0, LWa;->a:Z

    .line 469
    invoke-direct {p0, p1}, LWa;->a(Ljava/lang/String;)V

    .line 470
    return-void
.end method

.method static synthetic c(LWa;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LWa;->c:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 129
    :try_start_0
    const-string v0, "shared_preferences.cache_size"

    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, LWa;->a:Landroid/preference/ListPreference;

    .line 130
    invoke-direct {p0}, LWa;->f()V

    .line 132
    const-string v0, "clear_cache"

    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, LWa;->a:Landroid/preference/Preference;

    .line 133
    iget-object v0, p0, LWa;->a:Landroid/preference/ListPreference;

    new-instance v1, LWb;

    invoke-direct {v1, p0}, LWb;-><init>(LWa;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 141
    iget-object v0, p0, LWa;->a:Landroid/preference/Preference;

    new-instance v1, LWc;

    invoke-direct {v1, p0}, LWc;-><init>(LWa;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 150
    const-string v0, "cache_category"

    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 151
    iget-object v1, p0, LWa;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private c(LEE;)V
    .locals 1

    .prologue
    .line 292
    new-instance v0, LWi;

    invoke-direct {v0, p0}, LWi;-><init>(LWa;)V

    invoke-interface {p1, v0}, LEE;->a(LEB;)LFr;

    move-result-object v0

    iput-object v0, p0, LWa;->b:LFr;

    .line 306
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 156
    iget-object v0, p0, LWa;->a:LtK;

    sget-object v1, Lry;->af:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    .line 157
    if-nez v0, :cond_0

    .line 158
    iget-object v0, p0, LWa;->a:Landroid/preference/PreferenceScreen;

    const-string v1, "encryption"

    invoke-direct {p0, v1}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 176
    :goto_0
    return-void

    .line 160
    :cond_0
    const-string v0, "enable_pin_encryption"

    .line 161
    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, LWa;->a:Landroid/preference/CheckBoxPreference;

    .line 162
    iget-object v0, p0, LWa;->a:Lamn;

    invoke-interface {v0}, Lamn;->a()Z

    move-result v0

    .line 163
    if-nez v0, :cond_1

    .line 164
    const-string v0, "encryption"

    .line 165
    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 166
    const-string v1, "streaming_decryption"

    .line 167
    invoke-direct {p0, v1}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 166
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 173
    :goto_1
    const-string v0, "enable_pin_encryption"

    iget-object v1, p0, LWa;->c:LFr;

    sget-object v2, LWq;->a:LWq;

    invoke-direct {p0, v0, v1, v2}, LWa;->a(Ljava/lang/String;LFr;LWq;)V

    goto :goto_0

    .line 169
    :cond_1
    const-string v0, "streaming_decryption"

    .line 170
    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, LWa;->b:Landroid/preference/CheckBoxPreference;

    .line 171
    iget-object v0, p0, LWa;->b:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, LWa;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private d(LEE;)V
    .locals 1

    .prologue
    .line 309
    new-instance v0, LWj;

    invoke-direct {v0, p0}, LWj;-><init>(LWa;)V

    invoke-interface {p1, v0}, LEE;->a(LEB;)LFr;

    move-result-object v0

    iput-object v0, p0, LWa;->c:LFr;

    .line 368
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, LWa;->a:LaKR;

    invoke-interface {v0}, LaKR;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    const-string v0, "docs_preference_screen.data_usage"

    .line 182
    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 183
    iget-object v1, p0, LWa;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 191
    :goto_0
    return-void

    .line 185
    :cond_0
    const-string v0, "shared_preferences.sync_over_wifi_only"

    .line 186
    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, LWa;->c:Landroid/preference/CheckBoxPreference;

    .line 188
    const-string v0, "shared_preferences.sync_over_wifi_only"

    iget-object v1, p0, LWa;->d:LFr;

    sget-object v2, LWq;->c:LWq;

    invoke-direct {p0, v0, v1, v2}, LWa;->a(Ljava/lang/String;LFr;LWq;)V

    goto :goto_0
.end method

.method private e(LEE;)V
    .locals 1

    .prologue
    .line 371
    new-instance v0, LWl;

    invoke-direct {v0, p0}, LWl;-><init>(LWa;)V

    invoke-interface {p1, v0}, LEE;->a(LEB;)LFr;

    move-result-object v0

    iput-object v0, p0, LWa;->d:LFr;

    .line 421
    return-void
.end method

.method private f()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 444
    iget-object v0, p0, LWa;->a:Lamn;

    invoke-interface {v0}, Lamn;->a()Lamo;

    move-result-object v2

    .line 445
    iget-object v0, p0, LWa;->a:Landroid/app/Activity;

    .line 446
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lxi;->prefs_cache_size_choice_format_string:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 448
    invoke-interface {v2}, Lamo;->a()[I

    move-result-object v4

    .line 449
    array-length v0, v4

    new-array v5, v0, [Ljava/lang/CharSequence;

    .line 450
    array-length v0, v4

    new-array v6, v0, [Ljava/lang/CharSequence;

    move v0, v1

    .line 451
    :goto_0
    array-length v7, v4

    if-ge v0, v7, :cond_0

    .line 452
    aget v7, v4, v0

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    .line 453
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aget v8, v4, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 456
    :cond_0
    invoke-interface {v2}, Lamo;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 457
    iget-object v1, p0, LWa;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1, v5}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 458
    iget-object v1, p0, LWa;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1, v6}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 459
    iget-object v1, p0, LWa;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 460
    return-void
.end method

.method private f(LEE;)V
    .locals 1

    .prologue
    .line 424
    new-instance v0, LWo;

    invoke-direct {v0, p0}, LWo;-><init>(LWa;)V

    invoke-interface {p1, v0}, LEE;->a(LEB;)LFr;

    move-result-object v0

    iput-object v0, p0, LWa;->e:LFr;

    .line 441
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, LWa;->a:LadL;

    invoke-interface {v0}, LadL;->c()V

    .line 474
    iget-object v0, p0, LWa;->a:Landroid/preference/Preference;

    sget v1, Lxi;->clear_cache_cleared_message:I

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 475
    iget-object v0, p0, LWa;->a:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 476
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 113
    sget v0, Lxl;->preferences:I

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, LWa;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, LWa;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LWa;->a(Ljava/lang/String;)V

    .line 234
    :cond_0
    return-void
.end method

.method public a(LEE;)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0, p1}, LWa;->b(LEE;)V

    .line 250
    invoke-direct {p0, p1}, LWa;->c(LEE;)V

    .line 251
    invoke-direct {p0, p1}, LWa;->d(LEE;)V

    .line 252
    invoke-direct {p0, p1}, LWa;->e(LEE;)V

    .line 253
    invoke-direct {p0, p1}, LWa;->f(LEE;)V

    .line 254
    return-void
.end method

.method public a(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 118
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, LWa;->a:Landroid/preference/PreferenceScreen;

    .line 120
    invoke-direct {p0}, LWa;->d()V

    .line 121
    invoke-direct {p0}, LWa;->e()V

    .line 122
    invoke-direct {p0}, LWa;->c()V

    .line 123
    const-string v0, "about"

    iget-object v1, p0, LWa;->a:LFr;

    invoke-direct {p0, v0, v1}, LWa;->a(Ljava/lang/String;LFr;)V

    .line 124
    const-string v0, "legal"

    iget-object v1, p0, LWa;->b:LFr;

    invoke-direct {p0, v0, v1}, LWa;->a(Ljava/lang/String;LFr;)V

    .line 125
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, LWa;->a:Z

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, LWa;->a:Lamn;

    invoke-interface {v0}, Lamn;->a()V

    .line 242
    iget-object v0, p0, LWa;->a:LadL;

    invoke-interface {v0}, LadL;->a()V

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, LWa;->a:Z

    .line 245
    :cond_0
    return-void
.end method
