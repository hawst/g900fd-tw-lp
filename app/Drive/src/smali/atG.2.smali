.class public final LatG;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LatP;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LatR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, LatI;

    sput-object v0, LatG;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LatG;->a:LbrA;

    .line 41
    const-class v0, LatP;

    invoke-static {v0, v1}, LatG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LatG;->a:Lbsk;

    .line 44
    const-class v0, LatR;

    invoke-static {v0, v1}, LatG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LatG;->b:Lbsk;

    .line 47
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 126
    sparse-switch p1, :sswitch_data_0

    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :sswitch_0
    new-instance v0, LatP;

    invoke-direct {v0}, LatP;-><init>()V

    .line 130
    iget-object v1, p0, LatG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LatG;

    .line 131
    invoke-virtual {v1, v0}, LatG;->a(LatP;)V

    .line 142
    :goto_0
    return-object v0

    .line 134
    :sswitch_1
    new-instance v1, LatR;

    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lbrx;

    iget-object v0, v0, Lbrx;->d:Lbsk;

    .line 137
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LatG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lbrx;

    iget-object v2, v2, Lbrx;->d:Lbsk;

    .line 135
    invoke-static {v0, v2}, LatG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v1, v0}, LatR;-><init>(Ljava/util/List;)V

    move-object v0, v1

    .line 142
    goto :goto_0

    .line 126
    :sswitch_data_0
    .sparse-switch
        0x6a -> :sswitch_0
        0x53f -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 177
    .line 179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 103
    const-class v0, Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xa6

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LatG;->a(LbuP;LbuB;)V

    .line 106
    const-class v0, LatP;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xa7

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LatG;->a(LbuP;LbuB;)V

    .line 109
    const-class v0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xa8

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LatG;->a(LbuP;LbuB;)V

    .line 112
    const-class v0, LatP;

    iget-object v1, p0, LatG;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LatG;->a(Ljava/lang/Class;Lbsk;)V

    .line 113
    const-class v0, LatR;

    iget-object v1, p0, LatG;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LatG;->a(Ljava/lang/Class;Lbsk;)V

    .line 114
    iget-object v0, p0, LatG;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x6a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 116
    iget-object v0, p0, LatG;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x53f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 118
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 151
    packed-switch p1, :pswitch_data_0

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;

    .line 155
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LatG;

    .line 156
    invoke-virtual {v0, p2}, LatG;->a(Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;)V

    .line 173
    :goto_0
    return-void

    .line 159
    :pswitch_1
    check-cast p2, LatP;

    .line 161
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LatG;

    .line 162
    invoke-virtual {v0, p2}, LatG;->a(LatP;)V

    goto :goto_0

    .line 165
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;

    .line 167
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LatG;

    .line 168
    invoke-virtual {v0, p2}, LatG;->a(Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;)V

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0xa6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(LatP;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LatG;

    iget-object v0, v0, LatG;->b:Lbsk;

    .line 67
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LatG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LatG;

    iget-object v1, v1, LatG;->b:Lbsk;

    .line 65
    invoke-static {v0, v1}, LatG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LatR;

    iput-object v0, p1, LatP;->a:LatR;

    .line 71
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LatG;

    iget-object v0, v0, LatG;->b:Lbsk;

    .line 56
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LatG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LatG;

    iget-object v1, v1, LatG;->b:Lbsk;

    .line 54
    invoke-static {v0, v1}, LatG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LatR;

    iput-object v0, p1, Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;->a:LatR;

    .line 60
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 76
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 77
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LatG;

    iget-object v0, v0, LatG;->b:Lbsk;

    .line 80
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LatG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LatG;

    iget-object v1, v1, LatG;->b:Lbsk;

    .line 78
    invoke-static {v0, v1}, LatG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LatR;

    iput-object v0, p1, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LatR;

    .line 84
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 87
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LatG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 85
    invoke-static {v0, v1}, LatG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LSF;

    .line 91
    iget-object v0, p0, LatG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 94
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LatG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 92
    invoke-static {v0, v1}, LatG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:Lald;

    .line 98
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 122
    return-void
.end method
