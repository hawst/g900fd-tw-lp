.class public LJR;
.super Ljava/lang/Object;
.source "EntrySelectionModel.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/selection/SelectionModel;


# annotations
.annotation runtime LaiC;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/docs/doclist/selection/SelectionModel",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/docs/doclist/selection/SelectionModel",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaFO;LJS;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p2, p1}, LJS;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    .line 37
    if-nez v0, :cond_0

    .line 38
    new-instance v0, LJX;

    invoke-direct {v0}, LJX;-><init>()V

    .line 39
    invoke-virtual {p2, p1, v0}, LJS;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :cond_0
    iput-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    .line 42
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a()I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LKb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)",
            "LKb;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LKb;

    move-result-object v0

    return-object v0
.end method

.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a()V

    .line 52
    return-void
.end method

.method public a(LKe;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKe",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a(LKe;)V

    .line 97
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 112
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a(Z)V

    .line 77
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a()Z

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->b()V

    .line 82
    return-void
.end method

.method public b(LKe;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKe",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->b(LKe;)V

    .line 102
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->b()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, LJR;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->c()V

    .line 87
    return-void
.end method
