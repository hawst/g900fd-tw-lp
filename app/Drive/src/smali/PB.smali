.class public LPB;
.super Ljava/lang/Object;
.source "ActivityApiProvider.java"


# instance fields
.field private final a:LPC;

.field private a:LPy;

.field private final a:LQr;

.field private final a:LaGM;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(LaGM;LQr;LPC;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, LPB;->a:LaGM;

    .line 25
    iput-object p3, p0, LPB;->a:LPC;

    .line 26
    iput-object p2, p0, LPB;->a:LQr;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LPy;
    .locals 4

    .prologue
    .line 30
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iget-object v0, p0, LPB;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, LPB;->a:LPy;

    .line 37
    :goto_0
    return-object v0

    .line 35
    :cond_0
    iput-object p1, p0, LPB;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 36
    new-instance v0, LPy;

    iget-object v1, p0, LPB;->a:LaGM;

    iget-object v2, p0, LPB;->a:LPC;

    iget-object v3, p0, LPB;->a:LQr;

    invoke-direct {v0, v1, p1, v2, v3}, LPy;-><init>(LaGM;Lcom/google/android/gms/drive/database/data/EntrySpec;LPC;LQr;)V

    iput-object v0, p0, LPB;->a:LPy;

    .line 37
    iget-object v0, p0, LPB;->a:LPy;

    goto :goto_0
.end method
