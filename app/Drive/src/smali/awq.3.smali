.class Lawq;
.super Ljava/lang/Object;
.source "TileBoard.java"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method private constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    iput p1, p0, Lawq;->a:I

    .line 358
    iput p2, p0, Lawq;->b:I

    .line 359
    iput p3, p0, Lawq;->c:I

    .line 360
    iput p4, p0, Lawq;->d:I

    .line 361
    return-void
.end method

.method static synthetic a(Lawq;)I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lawq;->b:I

    return v0
.end method

.method public static a(Landroid/graphics/Rect;II)Lawq;
    .locals 6

    .prologue
    .line 335
    iget v0, p0, Landroid/graphics/Rect;->left:I

    sget-object v1, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v1, v1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    div-int v3, v0, v1

    .line 336
    iget v0, p0, Landroid/graphics/Rect;->top:I

    sget-object v1, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v1, v1, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    div-int v1, v0, v1

    .line 337
    iget v0, p0, Landroid/graphics/Rect;->right:I

    sget-object v2, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v2, v2, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    div-int v2, v0, v2

    .line 338
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    sget-object v4, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v4, v4, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    div-int/2addr v0, v4

    .line 340
    if-lez v3, :cond_0

    iget v4, p0, Landroid/graphics/Rect;->left:I

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    rem-int/2addr v4, v5

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    div-int/lit8 v5, v5, 0x2

    if-ge v4, v5, :cond_0

    .line 341
    add-int/lit8 v3, v3, -0x1

    .line 343
    :cond_0
    add-int/lit8 v4, p1, -0x1

    if-ge v2, v4, :cond_1

    iget v4, p0, Landroid/graphics/Rect;->right:I

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    rem-int/2addr v4, v5

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    div-int/lit8 v5, v5, 0x2

    if-le v4, v5, :cond_1

    .line 344
    add-int/lit8 v2, v2, 0x1

    .line 346
    :cond_1
    if-lez v1, :cond_2

    iget v4, p0, Landroid/graphics/Rect;->top:I

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    rem-int/2addr v4, v5

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    div-int/lit8 v5, v5, 0x2

    if-ge v4, v5, :cond_2

    .line 347
    add-int/lit8 v1, v1, -0x1

    .line 349
    :cond_2
    add-int/lit8 v4, p2, -0x1

    if-ge v0, v4, :cond_3

    iget v4, p0, Landroid/graphics/Rect;->bottom:I

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    rem-int/2addr v4, v5

    sget-object v5, Lawn;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    div-int/lit8 v5, v5, 0x2

    if-le v4, v5, :cond_3

    .line 350
    add-int/lit8 v0, v0, 0x1

    .line 353
    :cond_3
    new-instance v4, Lawq;

    invoke-direct {v4, v3, v1, v2, v0}, Lawq;-><init>(IIII)V

    return-object v4
.end method

.method static synthetic b(Lawq;)I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lawq;->a:I

    return v0
.end method

.method static synthetic c(Lawq;)I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lawq;->d:I

    return v0
.end method

.method static synthetic d(Lawq;)I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lawq;->c:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 364
    iget v0, p0, Lawq;->c:I

    iget v1, p0, Lawq;->a:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lawq;->d:I

    iget v2, p0, Lawq;->b:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 369
    if-eqz p1, :cond_0

    instance-of v1, p1, Lawq;

    if-nez v1, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v0

    .line 372
    :cond_1
    check-cast p1, Lawq;

    .line 373
    iget v1, p0, Lawq;->a:I

    iget v2, p1, Lawq;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lawq;->b:I

    iget v2, p1, Lawq;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lawq;->c:I

    iget v2, p1, Lawq;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lawq;->d:I

    iget v2, p1, Lawq;->d:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 381
    iget v0, p0, Lawq;->d:I

    add-int/lit8 v0, v0, 0x1f

    .line 382
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lawq;->a:I

    add-int/2addr v0, v1

    .line 383
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lawq;->c:I

    add-int/2addr v0, v1

    .line 384
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lawq;->b:I

    add-int/2addr v0, v1

    .line 385
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 390
    const-string v0, "Area [%d tiles] (%d %d - %d %d)"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lawq;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lawq;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lawq;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lawq;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lawq;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
