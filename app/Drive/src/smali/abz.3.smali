.class public Labz;
.super Landroid/widget/ArrayAdapter;
.source "ContactFilterAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Labs;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LabC;

.field private final a:LabI;

.field private a:Landroid/widget/Filter;


# direct methods
.method public constructor <init>(Landroid/content/Context;LabC;LabI;)V
    .locals 2

    .prologue
    .line 167
    sget v0, Lxe;->add_collaborator_list_item:I

    sget v1, Lxc;->sharee_name:I

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 168
    iput-object p3, p0, Labz;->a:LabI;

    .line 169
    iput-object p2, p0, Labz;->a:LabC;

    .line 170
    return-void
.end method

.method static synthetic a(Labz;)LabC;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Labz;->a:LabC;

    return-object v0
.end method

.method public static a(Ljava/util/List;Ljava/util/Locale;)LabC;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LabD;",
            ">;",
            "Ljava/util/Locale;",
            ")",
            "LabC;"
        }
    .end annotation

    .prologue
    .line 177
    invoke-static {p0, p1}, LabC;->a(Ljava/util/List;Ljava/util/Locale;)LabC;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0}, Labz;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    sget-object v0, Lbih;->n:Lbih;

    .line 206
    invoke-static {v0}, Lbjk;->a(Lbih;)Lbjk;

    move-result-object v0

    invoke-virtual {v0}, Lbjk;->a()Lbjk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbjk;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 207
    invoke-static {v0}, LbmF;->a(Ljava/lang/Iterable;)LbmF;

    move-result-object v0

    .line 208
    return-object v0
.end method


# virtual methods
.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Labz;->a:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 183
    new-instance v0, LabB;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LabB;-><init>(Labz;LabA;)V

    iput-object v0, p0, Labz;->a:Landroid/widget/Filter;

    .line 185
    :cond_0
    iget-object v0, p0, Labz;->a:Landroid/widget/Filter;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 190
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 191
    sget v0, Lxc;->sharee_name:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 192
    sget v1, Lxc;->sharee_description:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 193
    sget v2, Lxc;->sharee_badge:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 195
    invoke-virtual {p0, p1}, Labz;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Labs;

    .line 196
    invoke-virtual {v3}, Labs;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual {v3}, Labs;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, p0, Labz;->a:LabI;

    invoke-virtual {v3}, Labs;->a()J

    move-result-wide v6

    invoke-virtual {v0, v2, v6, v7}, LabI;->a(Landroid/widget/ImageView;J)V

    .line 200
    return-object v4
.end method
