.class public LavI;
.super LavF;
.source "PdfDocumentService.java"


# instance fields
.field private a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

.field final synthetic a:Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;

    invoke-direct {p0}, LavF;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;LavH;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, LavI;-><init>(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->getStatus()LavJ;

    move-result-object v0

    invoke-virtual {v0}, LavJ;->ordinal()I

    move-result v0

    return v0
.end method

.method public a(I)Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->getPageDimensions(I)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    return-object v0
.end method

.method public a(ILjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/MatchRects;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;

    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;->a(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "crash"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->destroy()V

    .line 106
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;->a(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;Z)Z

    .line 108
    :cond_0
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->searchPageText(ILjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/MatchRects;

    move-result-object v0

    return-object v0
.end method

.method public a(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)Lcom/google/android/apps/viewer/pdflib/Selection;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->selectPageText(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)Lcom/google/android/apps/viewer/pdflib/Selection;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->getPageText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->destroy()V

    .line 67
    return-void
.end method

.method public a(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 52
    sget-boolean v0, LavX;->q:Z

    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->createFromFd(ILjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    move-result-object v0

    iput-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    .line 62
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v0, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 58
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 59
    invoke-static {v0, v1}, LavZ;->b(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 60
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->createFromBytes([BLjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    move-result-object v0

    iput-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    goto :goto_0
.end method

.method public a(IIIIILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z
    .locals 8

    .prologue
    .line 98
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->renderTileFd(IIIIILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z

    move-result v0

    return v0
.end method

.method public a(ILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->renderPageFd(ILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z

    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LavI;->a:Lcom/google/android/apps/viewer/pdflib/PdfDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->numPages()I

    move-result v0

    return v0
.end method
