.class Lbma;
.super Ljava/util/AbstractCollection;
.source "Collections2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:LbiG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiG",
            "<-TF;+TT;>;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TF;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Collection;LbiG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TF;>;",
            "LbiG",
            "<-TF;+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 286
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lbma;->a:Ljava/util/Collection;

    .line 287
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiG;

    iput-object v0, p0, Lbma;->a:LbiG;

    .line 288
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lbma;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 293
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lbma;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 302
    iget-object v0, p0, Lbma;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lbma;->a:LbiG;

    invoke-static {v0, v1}, Lbnr;->a(Ljava/util/Iterator;LbiG;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lbma;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method
