.class public abstract Lbet;
.super Ljava/lang/Object;
.source "LowLevelHttpRequest.java"


# instance fields
.field private a:J

.field private a:LbfR;

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbet;->a:J

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lbet;->a:J

    return-wide v0
.end method

.method public abstract a()Lbeu;
.end method

.method public final a()LbfR;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lbet;->a:LbfR;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lbet;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 75
    iput-wide p1, p0, Lbet;->a:J

    .line 76
    return-void
.end method

.method public final a(LbfR;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lbet;->a:LbfR;

    .line 134
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lbet;->a:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lbet;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lbet;->b:Ljava/lang/String;

    .line 114
    return-void
.end method
