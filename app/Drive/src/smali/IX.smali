.class LIX;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "BaseHelpCard.java"


# instance fields
.field final synthetic a:LIU;

.field final synthetic a:Landroid/content/Context;

.field final synthetic a:Landroid/view/View;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;


# direct methods
.method constructor <init>(LIU;Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;Landroid/view/View;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, LIX;->a:LIU;

    iput-object p2, p0, LIX;->a:Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;

    iput-object p3, p0, LIX;->a:Landroid/view/View;

    iput-object p4, p0, LIX;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method private a(F)V
    .locals 5

    .prologue
    .line 307
    iget-object v0, p0, LIX;->a:Landroid/view/View;

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, LIX;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, LIX;->a:Landroid/view/View;

    .line 308
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, LIX;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, LIX;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 307
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 309
    return-void
.end method

.method private a(FF)Z
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, LIX;->a:LIU;

    invoke-static {v0, p1, p2}, LIU;->a(LIU;FF)V

    .line 299
    sget-object v0, LJd;->b:LJd;

    iget-object v1, p0, LIX;->a:LIU;

    invoke-static {v1}, LIU;->a(LIU;)LJd;

    move-result-object v1

    invoke-virtual {v0, v1}, LJd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, LIX;->a:Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a()V

    .line 301
    const/4 v0, 0x1

    .line 303
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 270
    const-string v0, "BaseHelpCard"

    const-string v1, "onDown: %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 271
    iget-object v0, p0, LIX;->a:LIU;

    invoke-static {v0}, LIU;->a(LIU;)V

    .line 272
    return v4
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 313
    const-string v2, "BaseHelpCard"

    const-string v3, "onFling: %s\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    aput-object p2, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 315
    invoke-direct {p0, p3, p4}, LIX;->a(FF)Z

    move-result v2

    .line 317
    if-eqz v2, :cond_0

    .line 331
    :goto_0
    return v0

    .line 321
    :cond_0
    iget-object v0, p0, LIX;->a:LIU;

    invoke-static {v0, p3, p4}, LIU;->a(LIU;FF)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1

    sget v0, LwV;->translate_right_out:I

    .line 324
    :goto_1
    iget-object v2, p0, LIX;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 325
    new-instance v2, LJa;

    iget-object v3, p0, LIX;->a:LIU;

    iget-object v4, p0, LIX;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v4}, LJa;-><init>(LIU;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 326
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 327
    iget-object v2, p0, LIX;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_2
    move v0, v1

    .line 331
    goto :goto_0

    .line 322
    :cond_1
    sget v0, LwV;->translate_left_out:I

    goto :goto_1

    .line 329
    :cond_2
    iget-object v0, p0, LIX;->a:LIU;

    iget-object v2, p0, LIX;->a:Landroid/view/View;

    invoke-static {v0, v2}, LIU;->a(LIU;Landroid/view/View;)V

    goto :goto_2
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 277
    const-string v2, "BaseHelpCard"

    const-string v3, "onScroll: %s\n%s\ndistanceX: %f distanceY: %f"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    aput-object p2, v4, v1

    const/4 v5, 0x2

    .line 281
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 282
    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    .line 277
    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 284
    invoke-direct {p0, p3, p4}, LIX;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    iget-object v1, p0, LIX;->a:Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a()V

    .line 290
    :goto_0
    return v0

    .line 289
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-direct {p0, v0}, LIX;->a(F)V

    move v0, v1

    .line 290
    goto :goto_0
.end method
