.class public abstract Lanq;
.super Lanv;
.source "AbstractCloseableReferenceCachingFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "C:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lanv",
        "<TK;TC;",
        "LakD",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field private final a:LbjF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjF",
            "<TC;",
            "LakD",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private final a:Lblf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lblf",
            "<TC;",
            "LakD",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(LbjG;Laoo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbjG",
            "<TC;",
            "LakD",
            "<TV;>;>;",
            "Laoo",
            "<TK;",
            "LakD",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p2}, Lanv;-><init>(Laoo;)V

    .line 27
    new-instance v0, Lanr;

    invoke-direct {v0, p0}, Lanr;-><init>(Lanq;)V

    iput-object v0, p0, Lanq;->a:Lblf;

    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbjG;

    iget-object v1, p0, Lanq;->a:Lblf;

    .line 46
    invoke-virtual {v0, v1}, LbjG;->a(Lblf;)LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->a()LbjF;

    move-result-object v0

    iput-object v0, p0, Lanq;->a:LbjF;

    .line 47
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Object;)LakD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)",
            "LakD",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-object v0, p0, Lanq;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakD;

    .line 53
    if-eqz v0, :cond_1

    .line 54
    invoke-static {v0}, LakD;->a(LakD;)LakD;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, LakD;->a()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 57
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Ljava/lang/Object;LakD;I)LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;",
            "LakD",
            "<TV;>;I)",
            "LbmF",
            "<",
            "LakD",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :try_start_0
    iget-object v0, p0, Lanq;->a:LbjF;

    invoke-static {p2}, LakD;->a(LakD;)LakD;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 68
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 71
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    .line 72
    :try_start_1
    invoke-static {p2}, LakD;->a(LakD;)LakD;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    move-object v1, v0

    .line 77
    :try_start_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakD;

    .line 78
    invoke-virtual {v0}, LakD;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 84
    :catchall_1
    move-exception v0

    invoke-virtual {p2}, LakD;->close()V

    throw v0

    .line 79
    :cond_0
    :try_start_3
    throw v1

    .line 82
    :cond_1
    invoke-static {v2}, LbmF;->a(Ljava/util/Collection;)LbmF;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 84
    invoke-virtual {p2}, LakD;->close()V

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;I)LbmF;
    .locals 1

    .prologue
    .line 25
    check-cast p2, LakD;

    invoke-virtual {p0, p1, p2, p3}, Lanq;->a(Ljava/lang/Object;LakD;I)LbmF;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lanq;->a(Ljava/lang/Object;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected final a(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p1}, LakD;->close()V

    .line 96
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, Lanq;->a(LakD;)V

    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)Z"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lanq;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
