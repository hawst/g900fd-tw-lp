.class public LZk;
.super LGJ;
.source "PunchWebViewFragment.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LGH;LaFO;)V
    .locals 10

    .prologue
    .line 538
    iput-object p1, p0, LZk;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    .line 539
    invoke-virtual {p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v1

    iget-object v4, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQr;

    iget-object v5, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Class;

    iget-object v6, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUT;

    .line 540
    invoke-virtual {p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    const-string v2, "webview"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LH;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iget-object v8, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LTd;

    invoke-static {p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->j(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v9

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    .line 539
    invoke-direct/range {v0 .. v9}, LGJ;-><init>(Landroid/content/Context;LGH;LaFO;LQr;Ljava/lang/Class;LUT;Landroid/content/SharedPreferences;LTd;Landroid/os/Handler;)V

    .line 541
    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 546
    iget-object v0, p0, LZk;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 547
    instance-of v1, v0, Lrm;

    if-eqz v1, :cond_0

    .line 548
    check-cast v0, Lrm;

    .line 549
    const-string v1, "PunchWebViewFragment"

    const-string v2, "in onReceivedError errorCode=%s description=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 550
    invoke-super {p0, p1, p2, p3, p4}, LGJ;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 551
    invoke-virtual {v0}, Lrm;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, LZk;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    .line 555
    :cond_0
    return-void
.end method
