.class public LKL;
.super Ljava/lang/Object;
.source "FloatingHandleView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/content/ClipData;

.field final synthetic a:Landroid/view/View$DragShadowBuilder;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

.field final synthetic a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Z)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, LKL;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    iput-object p2, p0, LKL;->a:Landroid/content/ClipData;

    iput-object p3, p0, LKL;->a:Landroid/view/View$DragShadowBuilder;

    iput-boolean p4, p0, LKL;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, LKL;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 389
    iget-object v0, p0, LKL;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 390
    new-instance v1, LKM;

    invoke-direct {v1, p0}, LKM;-><init>(LKL;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 395
    const/4 v0, 0x0

    return v0
.end method
