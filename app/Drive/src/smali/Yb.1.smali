.class public LYb;
.super Landroid/widget/BaseAdapter;
.source "PunchSlideAdapter.java"


# instance fields
.field private final a:LXz;

.field private final a:LYv;

.field private final a:Landroid/view/LayoutInflater;

.field private a:Z


# direct methods
.method public constructor <init>(LXz;LYv;Landroid/view/LayoutInflater;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, LYb;->a:Z

    .line 51
    iput-object p1, p0, LYb;->a:LXz;

    .line 52
    iput-object p2, p0, LYb;->a:LYv;

    .line 53
    iput-object p3, p0, LYb;->a:Landroid/view/LayoutInflater;

    .line 54
    return-void
.end method

.method private a()F
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, LYb;->a:LXz;

    invoke-interface {v0}, LXz;->a()I

    move-result v1

    .line 145
    iget-object v0, p0, LYb;->a:LXz;

    invoke-interface {v0}, LXz;->b()I

    move-result v2

    .line 146
    const/high16 v0, 0x3f800000    # 1.0f

    .line 147
    if-ltz v1, :cond_0

    if-ltz v2, :cond_0

    .line 148
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    .line 150
    :cond_0
    return v0
.end method

.method private a(ILandroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, LYb;->a:LYv;

    invoke-interface {v0, p1}, LYv;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 140
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 141
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 99
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 102
    sget v0, LpN;->slide_index:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    add-int/lit8 v1, p2, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    sget v0, LpN;->speaker_notes_indicator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 107
    iget-object v1, p0, LYb;->a:LXz;

    invoke-interface {v1, p2}, LXz;->a(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v3

    .line 108
    :goto_0
    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 111
    sget v0, LpN;->thumbnail:I

    .line 112
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;

    .line 114
    iget-boolean v1, p0, LYb;->a:Z

    if-nez v1, :cond_0

    .line 115
    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->getWidth()I

    move-result v1

    .line 116
    if-lez v1, :cond_0

    .line 117
    iput-boolean v3, p0, LYb;->a:Z

    .line 118
    iget-object v4, p0, LYb;->a:LYv;

    invoke-interface {v4, v1}, LYv;->b(I)V

    .line 122
    :cond_0
    invoke-direct {p0}, LYb;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->setAspect(F)V

    .line 130
    if-ne p2, p3, :cond_1

    move v2, v3

    .line 131
    :cond_1
    sget v1, LpN;->thumbnail_border:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 132
    if-eqz v2, :cond_4

    sget v2, LpK;->slide_thumbnail_activated:I

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 135
    invoke-direct {p0, p2, v0}, LYb;->a(ILandroid/widget/ImageView;)V

    .line 136
    return-void

    :cond_2
    move v1, v2

    .line 107
    goto :goto_0

    .line 108
    :cond_3
    const/16 v1, 0x8

    goto :goto_1

    .line 132
    :cond_4
    sget v2, LpM;->punch_frame_thumbnail_background:I

    goto :goto_2
.end method


# virtual methods
.method public a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 91
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget v0, LpN;->thumbnail:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 93
    invoke-direct {p0, p2, v0}, LYb;->a(ILandroid/widget/ImageView;)V

    .line 94
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LYb;->a:LXz;

    invoke-interface {v0}, LXz;->c()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 68
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 73
    .line 74
    if-nez p2, :cond_0

    .line 75
    iget-object v0, p0, LYb;->a:Landroid/view/LayoutInflater;

    sget v1, LpP;->punch_slide_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 76
    sget v0, LpN;->slide_index:I

    sget v1, LpN;->slide_index:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 77
    sget v0, LpN;->speaker_notes_indicator:I

    sget v1, LpN;->speaker_notes_indicator:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 78
    sget v0, LpN;->thumbnail:I

    sget v1, LpN;->thumbnail:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 79
    sget v0, LpN;->thumbnail_border:I

    sget v1, LpN;->thumbnail_border:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 82
    :cond_0
    iget-object v0, p0, LYb;->a:LXz;

    invoke-interface {v0}, LXz;->d()I

    move-result v0

    invoke-direct {p0, p2, p1, v0}, LYb;->a(Landroid/view/View;II)V

    .line 84
    return-object p2
.end method
