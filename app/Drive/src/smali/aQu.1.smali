.class public LaQu;
.super Landroid/webkit/WebViewClient;


# instance fields
.field private a:LaNk;

.field private a:LaNv;

.field private a:LaNx;

.field private a:LaOl;

.field private a:LaOo;

.field private a:LaQw;

.field private a:LaVk;

.field protected final a:Lcom/google/android/gms/internal/fc;

.field private final a:Ljava/lang/Object;

.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LaNu;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/fc;Z)V
    .locals 1

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaQu;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaQu;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaQu;->a:Z

    iput-object p1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    iput-boolean p2, p0, LaQu;->b:Z

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 6

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LaQu;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaNu;

    if-eqz v0, :cond_1

    invoke-static {p1}, LaPS;->a(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v3}, LaQc;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received GMSG: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LaQc;->d(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LaQc;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-interface {v0, v1, v2}, LaNu;->a(Lcom/google/android/gms/internal/fc;Ljava/util/Map;)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No GMSG handler found for GMSG: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Landroid/net/Uri;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "https"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v1, p0, LaQu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaQu;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, LaQu;->a:LaVk;

    const/4 v0, 0x0

    iput-object v0, p0, LaQu;->a:LaOl;

    const/4 v0, 0x0

    iput-object v0, p0, LaQu;->a:LaQw;

    const/4 v0, 0x0

    iput-object v0, p0, LaQu;->a:LaNk;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaQu;->a:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, LaQu;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, LaQu;->a:LaNv;

    const/4 v0, 0x0

    iput-object v0, p0, LaQu;->a:LaOo;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LaQw;)V
    .locals 0

    iput-object p1, p0, LaQu;->a:LaQw;

    return-void
.end method

.method public a(LaVk;LaOl;LaNk;LaOo;ZLaNv;)V
    .locals 2

    const-string v0, "/appEvent"

    new-instance v1, LaNj;

    invoke-direct {v1, p3}, LaNj;-><init>(LaNk;)V

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/canOpenURLs"

    sget-object v1, LaNl;->b:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/click"

    sget-object v1, LaNl;->c:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/close"

    sget-object v1, LaNl;->d:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/customClose"

    sget-object v1, LaNl;->e:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/httpTrack"

    sget-object v1, LaNl;->f:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/log"

    sget-object v1, LaNl;->g:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/open"

    new-instance v1, LaNy;

    invoke-direct {v1, p6}, LaNy;-><init>(LaNv;)V

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/touch"

    sget-object v1, LaNl;->h:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    const-string v0, "/video"

    sget-object v1, LaNl;->i:LaNu;

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    iput-object p1, p0, LaQu;->a:LaVk;

    iput-object p2, p0, LaQu;->a:LaOl;

    iput-object p3, p0, LaQu;->a:LaNk;

    iput-object p6, p0, LaQu;->a:LaNv;

    iput-object p4, p0, LaQu;->a:LaOo;

    invoke-virtual {p0, p5}, LaQu;->a(Z)V

    return-void
.end method

.method public a(LaVk;LaOl;LaNk;LaOo;ZLaNv;LaNx;)V
    .locals 2

    invoke-virtual/range {p0 .. p6}, LaQu;->a(LaVk;LaOl;LaNk;LaOo;ZLaNv;)V

    const-string v0, "/setInterstitialProperties"

    new-instance v1, LaNw;

    invoke-direct {v1, p7}, LaNw;-><init>(LaNx;)V

    invoke-virtual {p0, v0, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    iput-object p7, p0, LaQu;->a:LaNx;

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/cj;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()Z

    move-result v1

    new-instance v0, Lcom/google/android/gms/internal/cm;

    if-eqz v1, :cond_0

    iget-object v2, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/aq;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v2, :cond_0

    move-object v2, v3

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    iget-object v4, p0, LaQu;->a:LaOo;

    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/fa;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/cm;-><init>(Lcom/google/android/gms/internal/cj;LaVk;LaOl;LaOo;Lcom/google/android/gms/internal/fa;)V

    invoke-virtual {p0, v0}, LaQu;->a(Lcom/google/android/gms/internal/cm;)V

    return-void

    :cond_0
    iget-object v2, p0, LaQu;->a:LaVk;

    goto :goto_0

    :cond_1
    iget-object v3, p0, LaQu;->a:LaOl;

    goto :goto_1
.end method

.method protected a(Lcom/google/android/gms/internal/cm;)V
    .locals 1

    iget-object v0, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LaOf;->a(Landroid/content/Context;Lcom/google/android/gms/internal/cm;)V

    return-void
.end method

.method public final a(Ljava/lang/String;LaNu;)V
    .locals 1

    iget-object v0, p0, LaQu;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, LaQu;->a:Z

    return-void
.end method

.method public final a(ZI)V
    .locals 8

    iget-object v0, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()Z

    move-result v1

    new-instance v0, Lcom/google/android/gms/internal/cm;

    if-eqz v1, :cond_0

    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/aq;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, LaQu;->a:LaOl;

    iget-object v3, p0, LaQu;->a:LaOo;

    iget-object v4, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    iget-object v5, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v5}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/fa;

    move-result-object v7

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/internal/cm;-><init>(LaVk;LaOl;LaOo;Lcom/google/android/gms/internal/fc;ZILcom/google/android/gms/internal/fa;)V

    invoke-virtual {p0, v0}, LaQu;->a(Lcom/google/android/gms/internal/cm;)V

    return-void

    :cond_0
    iget-object v1, p0, LaQu;->a:LaVk;

    goto :goto_0
.end method

.method public final a(ZILjava/lang/String;)V
    .locals 11

    const/4 v2, 0x0

    iget-object v0, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()Z

    move-result v3

    new-instance v0, Lcom/google/android/gms/internal/cm;

    if-eqz v3, :cond_0

    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/aq;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v1, :cond_0

    move-object v1, v2

    :goto_0
    if-eqz v3, :cond_1

    :goto_1
    iget-object v3, p0, LaQu;->a:LaNk;

    iget-object v4, p0, LaQu;->a:LaOo;

    iget-object v5, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    iget-object v6, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/fa;

    move-result-object v9

    iget-object v10, p0, LaQu;->a:LaNv;

    move v6, p1

    move v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/internal/cm;-><init>(LaVk;LaOl;LaNk;LaOo;Lcom/google/android/gms/internal/fc;ZILjava/lang/String;Lcom/google/android/gms/internal/fa;LaNv;)V

    invoke-virtual {p0, v0}, LaQu;->a(Lcom/google/android/gms/internal/cm;)V

    return-void

    :cond_0
    iget-object v1, p0, LaQu;->a:LaVk;

    goto :goto_0

    :cond_1
    iget-object v2, p0, LaQu;->a:LaOl;

    goto :goto_1
.end method

.method public final a(ZILjava/lang/String;Ljava/lang/String;)V
    .locals 12

    iget-object v0, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()Z

    move-result v2

    new-instance v0, Lcom/google/android/gms/internal/cm;

    if-eqz v2, :cond_0

    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/aq;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, LaQu;->a:LaNk;

    iget-object v4, p0, LaQu;->a:LaOo;

    iget-object v5, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    iget-object v6, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/fa;

    move-result-object v10

    iget-object v11, p0, LaQu;->a:LaNv;

    move v6, p1

    move v7, p2

    move-object v8, p3

    move-object/from16 v9, p4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/internal/cm;-><init>(LaVk;LaOl;LaNk;LaOo;Lcom/google/android/gms/internal/fc;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/fa;LaNv;)V

    invoke-virtual {p0, v0}, LaQu;->a(Lcom/google/android/gms/internal/cm;)V

    return-void

    :cond_0
    iget-object v1, p0, LaQu;->a:LaVk;

    goto :goto_0

    :cond_1
    iget-object v2, p0, LaQu;->a:LaOl;

    goto :goto_1
.end method

.method public a()Z
    .locals 2

    iget-object v1, p0, LaQu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LaQu;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 4

    iget-object v1, p0, LaQu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LaQu;->a:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, LaQu;->b:Z

    iget-object v0, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaOf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, LaQb;->b()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, LaQb;->a:Landroid/os/Handler;

    new-instance v3, LaQv;

    invoke-direct {v3, p0, v0}, LaQv;-><init>(LaQu;LaOf;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    invoke-virtual {v0}, LaOf;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loading resource: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->d(Ljava/lang/String;)V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, LaQu;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaQu;->a:LaQw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaQu;->a:LaQw;

    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-interface {v0, v1}, LaQw;->a(Lcom/google/android/gms/internal/fc;)V

    const/4 v0, 0x0

    iput-object v0, p0, LaQu;->a:LaQw;

    :cond_0
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 8

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdWebView shouldOverrideUrlLoading: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->d(Ljava/lang/String;)V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, LaQu;->a(Landroid/net/Uri;)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-boolean v1, p0, LaQu;->a:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    if-ne p1, v1, :cond_1

    invoke-static {v0}, LaQu;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :cond_1
    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->willNotDraw()Z

    move-result v1

    if-nez v1, :cond_3

    :try_start_0
    iget-object v1, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->a()LaSP;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, LaSP;->b(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LaQu;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/fc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LaSP;->a(Landroid/net/Uri;Landroid/content/Context;)Landroid/net/Uri;
    :try_end_0
    .catch LaSX; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_2
    move-object v2, v0

    :goto_2
    new-instance v0, Lcom/google/android/gms/internal/cj;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/internal/cj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LaQu;->a(Lcom/google/android/gms/internal/cj;)V

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to append parameter to URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LaQc;->e(Ljava/lang/String;)V

    move-object v2, v0

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdWebView unable to handle URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
