.class Lajj;
.super Ljava/lang/Object;
.source "FragmentActivitySession.java"

# interfaces
.implements LaiE;
.implements LaiM;


# instance fields
.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/app/Activity;",
            "Lajk",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "LaiD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbuE;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuE",
            "<",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lajj;->a:Ljava/util/Map;

    .line 205
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lajj;->b:Ljava/util/Map;

    .line 210
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    iput-object v0, p0, Lajj;->a:LbuE;

    .line 211
    return-void
.end method

.method static synthetic a(Lajj;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lajj;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lajj;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lajj;->b:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)LaiN;
    .locals 2

    .prologue
    .line 222
    instance-of v0, p1, LH;

    if-eqz v0, :cond_0

    new-instance v0, Lajm;

    invoke-direct {v0, p0, p1}, Lajm;-><init>(Lajj;Landroid/app/Activity;)V

    .line 225
    :goto_0
    iget-object v1, p0, Lajj;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    return-object v0

    .line 222
    :cond_0
    new-instance v0, Lajl;

    invoke-direct {v0, p0, p1}, Lajl;-><init>(Lajj;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public a()LajA;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lajj;->a:LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 216
    iget-object v1, p0, Lajj;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajk;

    .line 217
    invoke-virtual {v0}, Lajk;->a()LajA;

    move-result-object v0

    return-object v0
.end method

.method public a(LaiD;)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lajj;->b:Ljava/util/Map;

    invoke-interface {p1}, LaiD;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    return-void
.end method
