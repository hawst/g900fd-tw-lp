.class public LajW;
.super Ljava/lang/Object;
.source "Accounts.java"


# instance fields
.field private final a:J

.field private final a:LaJS;

.field private final b:J

.field private final c:J


# direct methods
.method public constructor <init>(LajN;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x64

    const-wide/16 v6, 0x0

    .line 353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    invoke-virtual {p1}, LajN;->a()J

    move-result-wide v0

    iput-wide v0, p0, LajW;->a:J

    .line 355
    invoke-virtual {p1}, LajN;->b()J

    move-result-wide v0

    iput-wide v0, p0, LajW;->b:J

    .line 356
    invoke-virtual {p1}, LajN;->a()LaJS;

    move-result-object v0

    iput-object v0, p0, LajW;->a:LaJS;

    .line 357
    iget-wide v0, p0, LajW;->a:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    move-wide v0, v2

    :goto_0
    iput-wide v0, p0, LajW;->c:J

    .line 358
    iget-wide v0, p0, LajW;->c:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    iget-wide v0, p0, LajW;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, LajW;->a:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_1

    .line 359
    :cond_0
    const-string v0, "Accounts"

    const-string v1, "Wrong quota info. Total: %s. Used: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, LajW;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, LajW;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 361
    :cond_1
    return-void

    .line 357
    :cond_2
    iget-wide v0, p0, LajW;->b:J

    mul-long/2addr v0, v2

    iget-wide v4, p0, LajW;->a:J

    div-long/2addr v0, v4

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 6

    .prologue
    .line 376
    const-wide/16 v0, 0x64

    const-wide/16 v2, 0x0

    iget-wide v4, p0, LajW;->c:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LaJS;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, LajW;->a:LaJS;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 368
    iget-wide v0, p0, LajW;->a:J

    invoke-static {v0, v1}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 372
    iget-wide v0, p0, LajW;->b:J

    invoke-static {v0, v1}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
