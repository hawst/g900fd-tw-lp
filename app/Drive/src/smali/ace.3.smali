.class public Lace;
.super Ljava/lang/Object;
.source "ServerAclManager.java"

# interfaces
.implements Labe;


# instance fields
.field private final a:LbsW;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Lael;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Lael;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v0

    iput-object v0, p0, Lace;->a:LbsW;

    .line 50
    iput-object p1, p0, Lace;->a:Lbxw;

    .line 51
    return-void
.end method

.method static synthetic a(Lace;)Lbxw;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lace;->a:Lbxw;

    return-object v0
.end method


# virtual methods
.method public a(LaFO;Ljava/util/Set;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "Ljava/util/Set",
            "<",
            "Lqo;",
            ">;)",
            "LbsU",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 104
    invoke-static {p2}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lace;->a:LbsW;

    new-instance v2, Lacg;

    invoke-direct {v2, p0, v0, p1}, Lacg;-><init>(Lace;Ljava/util/Set;LaFO;)V

    invoke-interface {v1, v2}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    .line 156
    const-string v1, "ServerAclLoader"

    const-string v2, "Started updating..."

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
            ")",
            "LbsU",
            "<",
            "Ljava/util/Set",
            "<",
            "Lqo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lace;->a:LbsW;

    new-instance v1, Lacf;

    invoke-direct {v1, p0, p1}, Lacf;-><init>(Lace;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    .line 98
    const-string v1, "ServerAclLoader"

    const-string v2, "Started parsing..."

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    return-object v0
.end method
