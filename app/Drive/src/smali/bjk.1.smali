.class public final Lbjk;
.super Ljava/lang/Object;
.source "Splitter.java"


# instance fields
.field private final a:I

.field private final a:Lbih;

.field private final a:Lbjr;

.field private final a:Z


# direct methods
.method private constructor <init>(Lbjr;)V
    .locals 3

    .prologue
    .line 108
    const/4 v0, 0x0

    sget-object v1, Lbih;->m:Lbih;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lbjk;-><init>(Lbjr;ZLbih;I)V

    .line 109
    return-void
.end method

.method private constructor <init>(Lbjr;ZLbih;I)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lbjk;->a:Lbjr;

    .line 113
    iput-boolean p2, p0, Lbjk;->a:Z

    .line 114
    iput-object p3, p0, Lbjk;->a:Lbih;

    .line 115
    iput p4, p0, Lbjk;->a:I

    .line 116
    return-void
.end method

.method static synthetic a(Lbjk;)I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lbjk;->a:I

    return v0
.end method

.method static synthetic a(Lbjk;)Lbih;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lbjk;->a:Lbih;

    return-object v0
.end method

.method public static a(C)Lbjk;
    .locals 1

    .prologue
    .line 127
    invoke-static {p0}, Lbih;->a(C)Lbih;

    move-result-object v0

    invoke-static {v0}, Lbjk;->a(Lbih;)Lbjk;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbih;)Lbjk;
    .locals 2

    .prologue
    .line 141
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    new-instance v0, Lbjk;

    new-instance v1, Lbjl;

    invoke-direct {v1, p0}, Lbjl;-><init>(Lbih;)V

    invoke-direct {v0, v1}, Lbjk;-><init>(Lbjr;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lbjk;
    .locals 2

    .prologue
    .line 169
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The separator may not be the empty string."

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 171
    new-instance v0, Lbjk;

    new-instance v1, Lbjn;

    invoke-direct {v1, p0}, Lbjn;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lbjk;-><init>(Lbjr;)V

    return-object v0

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lbjk;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lbjk;->a(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lbjk;->a:Lbjr;

    invoke-interface {v0, p0, p1}, Lbjr;->a(Lbjk;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbjk;)Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lbjk;->a:Z

    return v0
.end method


# virtual methods
.method public a()Lbjk;
    .locals 5

    .prologue
    .line 314
    new-instance v0, Lbjk;

    iget-object v1, p0, Lbjk;->a:Lbjr;

    const/4 v2, 0x1

    iget-object v3, p0, Lbjk;->a:Lbih;

    iget v4, p0, Lbjk;->a:I

    invoke-direct {v0, v1, v2, v3, v4}, Lbjk;-><init>(Lbjr;ZLbih;I)V

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    new-instance v0, Lbjp;

    invoke-direct {v0, p0, p1}, Lbjp;-><init>(Lbjk;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public b()Lbjk;
    .locals 1

    .prologue
    .line 354
    sget-object v0, Lbih;->n:Lbih;

    invoke-virtual {p0, v0}, Lbjk;->b(Lbih;)Lbjk;

    move-result-object v0

    return-object v0
.end method

.method public b(Lbih;)Lbjk;
    .locals 4

    .prologue
    .line 371
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    new-instance v0, Lbjk;

    iget-object v1, p0, Lbjk;->a:Lbjr;

    iget-boolean v2, p0, Lbjk;->a:Z

    iget v3, p0, Lbjk;->a:I

    invoke-direct {v0, v1, v2, p1, v3}, Lbjk;-><init>(Lbjr;ZLbih;I)V

    return-object v0
.end method
