.class final LaTq;
.super LaRy;

# interfaces
.implements LaZh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaRw",
        "<",
        "LaTi;",
        ">.aRy<",
        "LaDk",
        "<",
        "LaZh;",
        ">;>;",
        "LaZh;"
    }
.end annotation


# instance fields
.field final synthetic a:LaTo;

.field private final a:Landroid/os/ParcelFileDescriptor;

.field private final a:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>(LaTo;LaDk;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "LaZh;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Landroid/os/ParcelFileDescriptor;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, LaTq;->a:LaTo;

    invoke-direct {p0, p1, p2}, LaRy;-><init>(LaRw;Ljava/lang/Object;)V

    iput-object p3, p0, LaTq;->a:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, LaTq;->a:Landroid/os/ParcelFileDescriptor;

    return-void
.end method


# virtual methods
.method public a()Landroid/os/ParcelFileDescriptor;
    .locals 1

    iget-object v0, p0, LaTq;->a:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, LaTq;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, LaTq;->a:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaTq;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, LaSq;->a(Landroid/os/ParcelFileDescriptor;)V

    :cond_0
    return-void
.end method

.method protected a(LaDk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "LaZh;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, LaDk;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, LaDk;

    invoke-virtual {p0, p1}, LaTq;->a(LaDk;)V

    return-void
.end method

.method protected b()V
    .locals 0

    invoke-virtual {p0}, LaTq;->a()V

    return-void
.end method
