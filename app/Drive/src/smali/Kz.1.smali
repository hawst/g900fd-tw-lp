.class LKz;
.super LLm;
.source "DropToThisFolderListener.java"


# instance fields
.field final synthetic a:LKs;


# direct methods
.method private constructor <init>(LKs;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, LKz;->a:LKs;

    invoke-direct {p0}, LLm;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LKs;LKt;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, LKz;-><init>(LKs;)V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)LbsU;

    .line 108
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->b(LKs;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 109
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 110
    sget-object v0, LKA;->a:LKA;

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)LKA;

    move-result-object v1

    invoke-virtual {v0, v1}, LKA;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 113
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->b(LKs;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 117
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 118
    sget-object v0, LKA;->b:LKA;

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)LKA;

    move-result-object v1

    invoke-virtual {v0, v1}, LKA;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->b(LKs;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;LLl;)Z
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)LCU;

    move-result-object v0

    invoke-virtual {v0}, LCU;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    .line 79
    :cond_0
    invoke-interface {p2}, LLl;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 102
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 81
    :pswitch_0
    invoke-direct {p0}, LKz;->a()V

    goto :goto_1

    .line 85
    :pswitch_1
    invoke-direct {p0}, LKz;->b()V

    goto :goto_1

    .line 90
    :pswitch_2
    sget-object v0, LKA;->b:LKA;

    iget-object v1, p0, LKz;->a:LKs;

    invoke-static {v1}, LKs;->a(LKs;)LKA;

    move-result-object v1

    invoke-virtual {v0, v1}, LKA;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    invoke-direct {p0}, LKz;->b()V

    .line 92
    iget-object v0, p0, LKz;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)LCU;

    move-result-object v0

    invoke-virtual {v0}, LCU;->a()V

    goto :goto_1

    .line 79
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
