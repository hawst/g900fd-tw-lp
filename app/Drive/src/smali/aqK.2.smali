.class LaqK;
.super Landroid/webkit/WebView;
.source "ScrollSharingWebView.java"

# interfaces
.implements LaqS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method


# virtual methods
.method public a(I)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    if-gez p1, :cond_2

    move v5, v1

    .line 24
    :goto_0
    if-lez p1, :cond_3

    move v0, v1

    .line 25
    :goto_1
    invoke-virtual {p0}, LaqK;->computeHorizontalScrollOffset()I

    move-result v3

    if-nez v3, :cond_4

    move v3, v1

    .line 26
    :goto_2
    invoke-virtual {p0}, LaqK;->computeHorizontalScrollOffset()I

    move-result v4

    .line 27
    invoke-virtual {p0}, LaqK;->computeHorizontalScrollExtent()I

    move-result v6

    add-int/2addr v4, v6

    invoke-virtual {p0}, LaqK;->computeHorizontalScrollRange()I

    move-result v6

    sub-int/2addr v4, v6

    if-nez v4, :cond_5

    move v4, v1

    .line 29
    :goto_3
    if-eqz v5, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    if-eqz v0, :cond_6

    if-nez v3, :cond_6

    :cond_1
    :goto_4
    return v1

    :cond_2
    move v5, v2

    .line 23
    goto :goto_0

    :cond_3
    move v0, v2

    .line 24
    goto :goto_1

    :cond_4
    move v3, v2

    .line 25
    goto :goto_2

    :cond_5
    move v4, v2

    .line 27
    goto :goto_3

    :cond_6
    move v1, v2

    .line 29
    goto :goto_4
.end method
