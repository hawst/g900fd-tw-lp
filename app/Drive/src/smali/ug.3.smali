.class public Lug;
.super Ljava/lang/Object;
.source "MainProxyLogic.java"


# instance fields
.field private final a:LSF;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field a:LtK;

.field a:Z


# direct methods
.method public constructor <init>(Laja;LSF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LSF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object p1, p0, Lug;->a:Lbxw;

    .line 116
    iput-object p2, p0, Lug;->a:LSF;

    .line 117
    return-void
.end method

.method private a(LaFO;)LaFO;
    .locals 1

    .prologue
    .line 151
    if-nez p1, :cond_2

    .line 152
    iget-object v0, p0, Lug;->a:LSF;

    invoke-interface {v0}, LSF;->a()LaFO;

    move-result-object v0

    .line 154
    :goto_0
    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lug;->a:LSF;

    invoke-static {v0}, LSH;->a(LSF;)Landroid/accounts/Account;

    move-result-object v0

    .line 157
    if-nez v0, :cond_1

    .line 159
    const/4 v0, 0x0

    .line 164
    :cond_0
    :goto_1
    return-object v0

    .line 161
    :cond_1
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method private a(LaFO;Luj;Lui;Luh;)V
    .locals 1

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Lug;->a(LaFO;Luj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-interface {p4, p1, p3}, Luh;->b(LaFO;Lui;)V

    .line 174
    :goto_0
    return-void

    .line 172
    :cond_0
    invoke-interface {p4, p1, p3}, Luh;->a(LaFO;Lui;)V

    goto :goto_0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lug;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 187
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 188
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v0

    .line 190
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lug;->a:Z

    goto :goto_0
.end method

.method private a(LaFO;Luj;)Z
    .locals 1

    .prologue
    .line 177
    sget-object v0, Luj;->c:Luj;

    invoke-virtual {p2, v0}, Luj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lug;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lug;->a:LSF;

    .line 178
    invoke-interface {v0}, LSF;->a()LaFO;

    move-result-object v0

    invoke-virtual {p1, v0}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaFO;Luj;Luh;)V
    .locals 2

    .prologue
    .line 137
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-direct {p0, p1}, Lug;->a(LaFO;)LaFO;

    move-result-object v1

    .line 140
    if-nez v1, :cond_0

    .line 141
    invoke-interface {p3}, Luh;->a()V

    .line 148
    :goto_0
    return-void

    .line 144
    :cond_0
    sget-object v0, Luj;->a:Luj;

    if-ne p2, v0, :cond_1

    sget-object v0, Lui;->c:Lui;

    .line 146
    :goto_1
    invoke-direct {p0, v1, p2, v0, p3}, Lug;->a(LaFO;Luj;Lui;Luh;)V

    goto :goto_0

    .line 144
    :cond_1
    sget-object v0, Lui;->a:Lui;

    goto :goto_1
.end method
