.class public LyV;
.super Ljava/lang/Object;
.source "DetailListFragment.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LyX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 50
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LyV;->a:Ljava/util/Set;

    .line 49
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, LyV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyX;

    .line 62
    invoke-interface {v0, p1}, LyX;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method public static synthetic a(LyV;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, LyV;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, LyV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyX;

    .line 68
    invoke-interface {v0, p1}, LyX;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method

.method public static synthetic b(LyV;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, LyV;->a(Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public a(LyX;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, LyV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method
