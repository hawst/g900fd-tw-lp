.class public LAd;
.super LEz;
.source "CooperateStateMachineProgressDialog.java"

# interfaces
.implements Lamr;


# instance fields
.field private a:J

.field private a:LDL;

.field private final a:Landroid/os/Handler;

.field private a:LpD;

.field private b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 41
    invoke-direct {p0, p1, p2}, LEz;-><init>(Landroid/content/Context;I)V

    .line 26
    iput-wide v0, p0, LAd;->a:J

    .line 27
    iput-wide v0, p0, LAd;->b:J

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LAd;->a:Landroid/os/Handler;

    .line 43
    return-void
.end method

.method static synthetic a(LAd;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, LAd;->c()V

    return-void
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 131
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LAd;->a:LDL;

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, LAd;->a:LpD;

    .line 133
    invoke-virtual {p0}, LAd;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, LAd;->a:Landroid/os/Handler;

    new-instance v1, LAf;

    invoke-direct {v1, p0}, LAf;-><init>(LAd;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_0
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 3

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    const-string v0, "CooperateStateMachineProgressDialog"

    const-string v1, "in startMachine"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, LAd;->a:LDL;

    if-nez v0, :cond_1

    .line 73
    invoke-virtual {p0}, LAd;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 76
    :cond_1
    :try_start_1
    iget-object v0, p0, LAd;->a:LpD;

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, LAd;->a:LDL;

    .line 78
    new-instance v1, LAe;

    const-string v2, "Execute stateMachine for progress dialog"

    invoke-direct {v1, p0, v2, v0}, LAe;-><init>(LAd;Ljava/lang/String;LDL;)V

    iput-object v1, p0, LAd;->a:LpD;

    .line 95
    iget-object v0, p0, LAd;->a:LpD;

    invoke-virtual {v0}, LpD;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(JJLjava/lang/String;)V
    .locals 5

    .prologue
    .line 120
    iget-wide v0, p0, LAd;->a:J

    sub-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LAd;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    const-string v0, "CooperateStateMachineProgressDialog"

    const-string v1, "Progress: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 125
    iput-wide p1, p0, LAd;->a:J

    .line 126
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LAd;->b:J

    .line 127
    invoke-virtual/range {p0 .. p5}, LAd;->b(JJLjava/lang/String;)V

    goto :goto_0
.end method

.method public a(LDL;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    const-string v0, "CooperateStateMachineProgressDialog"

    const-string v3, "in setMachine"

    invoke-static {v0, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v0, p0, LAd;->a:LDL;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 51
    iget-object v0, p0, LAd;->a:LpD;

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 52
    iput-object p1, p0, LAd;->a:LDL;

    .line 53
    invoke-interface {p1, p0}, LDL;->a(Lamr;)V

    .line 54
    return-void

    :cond_0
    move v0, v2

    .line 50
    goto :goto_0

    :cond_1
    move v1, v2

    .line 51
    goto :goto_1
.end method

.method public declared-synchronized b()V
    .locals 2

    .prologue
    .line 110
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LAd;->a:LpD;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, LAd;->a:LpD;

    invoke-virtual {v0}, LpD;->a()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, LAd;->a:LpD;

    .line 114
    :cond_0
    const-string v0, "CooperateStateMachineProgressDialog"

    const-string v1, "Machine stopped."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, LAd;->a:LDL;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    monitor-exit p0

    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 64
    invoke-super {p0}, LEz;->onStart()V

    .line 65
    return-void
.end method

.method protected declared-synchronized onStop()V
    .locals 1

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LEz;->onStop()V

    .line 102
    invoke-virtual {p0}, LAd;->b()V

    .line 106
    invoke-virtual {p0}, LAd;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
