.class public abstract LajH;
.super Landroid/graphics/drawable/Drawable;
.source "AbstractMaskedBitmapDrawable.java"


# instance fields
.field private final a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 22
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    .line 23
    iget-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 24
    invoke-virtual {p0, p1}, LajH;->a(Landroid/graphics/Bitmap;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected abstract a()Landroid/graphics/Matrix;
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 73
    new-instance v2, Landroid/graphics/BitmapShader;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v2, p1, v3, v4}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 76
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 77
    cmpl-float v4, v0, v1

    if-lez v4, :cond_0

    .line 78
    sub-float/2addr v0, v1

    div-float/2addr v0, v7

    invoke-virtual {v3, v0, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 79
    div-float v0, v5, v1

    .line 86
    :goto_0
    invoke-virtual {v3, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 87
    invoke-virtual {p0}, LajH;->a()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 88
    invoke-virtual {v2, v3}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 89
    iget-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 90
    invoke-virtual {p0}, LajH;->invalidateSelf()V

    .line 91
    return-void

    .line 80
    :cond_0
    cmpg-float v4, v0, v1

    if-gez v4, :cond_1

    .line 81
    sub-float/2addr v1, v0

    div-float/2addr v1, v7

    invoke-virtual {v3, v6, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 82
    div-float v0, v5, v0

    goto :goto_0

    .line 84
    :cond_1
    div-float v0, v5, v0

    goto :goto_0
.end method

.method protected abstract a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v0}, LajH;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 52
    return-void
.end method

.method public getAlpha()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 35
    invoke-virtual {p0}, LajH;->invalidateSelf()V

    .line 36
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LajH;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 46
    invoke-virtual {p0}, LajH;->invalidateSelf()V

    .line 47
    return-void
.end method
