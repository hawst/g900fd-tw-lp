.class LUL;
.super LVb;
.source "QueryParameterPatternMatcher.java"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(LVc;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, LVb;-><init>(LVc;Ljava/lang/String;)V

    .line 24
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LUL;->a:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method protected a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LUL;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, LVb;->a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LUL;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
