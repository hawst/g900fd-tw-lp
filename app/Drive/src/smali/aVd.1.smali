.class public abstract LaVd;
.super Ljava/lang/Object;


# instance fields
.field protected volatile b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, LaVd;->b:I

    return-void
.end method

.method public static final a(LaVd;[BII)V
    .locals 3

    :try_start_0
    invoke-static {p1, p2, p3}, LaVa;->a([BII)LaVa;

    move-result-object v0

    invoke-virtual {p0, v0}, LaVd;->a(LaVa;)V

    invoke-virtual {v0}, LaVa;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static final a(LaVd;)[B
    .locals 3

    invoke-virtual {p0}, LaVd;->b()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, LaVd;->a(LaVd;[BII)V

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(LaVa;)V
    .locals 0

    return-void
.end method

.method public b()I
    .locals 1

    invoke-virtual {p0}, LaVd;->a()I

    move-result v0

    iput v0, p0, LaVd;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, LaVe;->a(LaVd;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
