.class public Lxm;
.super Ljava/lang/Object;
.source "Animators.java"


# direct methods
.method public static a(Landroid/animation/Animator;Landroid/view/View;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lxo;

    invoke-direct {v0, p1}, Lxo;-><init>(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 188
    return-object p0
.end method

.method public static a(Landroid/view/View;)Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 195
    invoke-static {p0}, Lxm;->a(Landroid/view/View;)Lxr;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;I)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 202
    invoke-static {p0}, Lxm;->a(Landroid/view/View;)Lxr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lxr;->a(I)Lxr;

    move-result-object v0

    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Landroid/view/View;Landroid/util/Property;[F)Landroid/animation/Animator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;[F)",
            "Landroid/animation/Animator;"
        }
    .end annotation

    .prologue
    .line 255
    invoke-static {p0, p1, p2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-static {v0, p0}, Lxm;->a(Landroid/animation/Animator;Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Landroid/view/View;[F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    invoke-static {p0, v0, p1}, Lxm;->a(Landroid/view/View;Landroid/util/Property;[F)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Landroid/view/View;[I)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lxv;

    invoke-direct {v0, p0}, Lxv;-><init>(Landroid/view/View;)V

    invoke-static {v0, p1}, Lxm;->a(Lxv;[I)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0, p0}, Lxm;->a(Landroid/animation/Animator;Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/Object;[F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 58
    const-string v0, "alpha"

    invoke-static {p0, v0, p1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Lxv;[I)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 246
    const-string v0, "width"

    invoke-static {p0, v0, p1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lxr;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    new-instance v0, Lxr;

    invoke-direct {v0, v1, v1}, Lxr;-><init>(Landroid/animation/Animator;Lxn;)V

    return-object v0
.end method

.method public static a(Landroid/animation/Animator;)Lxr;
    .locals 2

    .prologue
    .line 235
    new-instance v0, Lxr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lxr;-><init>(Landroid/animation/Animator;Lxn;)V

    return-object v0
.end method

.method private static a(Landroid/view/View;)Lxr;
    .locals 3

    .prologue
    .line 206
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    invoke-static {p0, v0}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 207
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->b(Landroid/content/Context;)Lxr;

    move-result-object v0

    new-instance v1, Lxp;

    invoke-direct {v1, p0}, Lxp;-><init>(Landroid/view/View;)V

    .line 208
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/view/View;)Landroid/animation/Animator;
    .locals 3

    .prologue
    .line 220
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    invoke-static {p0, v0}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 221
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    .line 222
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->c(Landroid/content/Context;)Lxr;

    move-result-object v0

    new-instance v1, Lxq;

    invoke-direct {v1, p0}, Lxq;-><init>(Landroid/view/View;)V

    .line 223
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs b(Landroid/view/View;[F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    invoke-static {p0, v0, p1}, Lxm;->a(Landroid/view/View;Landroid/util/Property;[F)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs b(Landroid/view/View;[I)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lxv;

    invoke-direct {v0, p0}, Lxv;-><init>(Landroid/view/View;)V

    invoke-static {v0, p1}, Lxm;->b(Lxv;[I)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0, p0}, Lxm;->a(Landroid/animation/Animator;Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private static varargs b(Lxv;[I)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 250
    const-string v0, "height"

    invoke-static {p0, v0, p1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs c(Landroid/view/View;[F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    invoke-static {p0, v0, p1}, Lxm;->a(Landroid/view/View;Landroid/util/Property;[F)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs d(Landroid/view/View;[F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    invoke-static {p0, v0, p1}, Lxm;->a(Landroid/view/View;Landroid/util/Property;[F)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs e(Landroid/view/View;[F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    invoke-static {p0, v0, p1}, Lxm;->a(Landroid/view/View;Landroid/util/Property;[F)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method
