.class LKH;
.super Ljava/lang/Object;
.source "FloatingHandleView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LKG;


# direct methods
.method constructor <init>(LKG;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, LKH;->a:LKG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LLt;

    move-result-object v0

    invoke-virtual {v0}, LLt;->a()V

    .line 109
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLf;

    .line 111
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKN;

    move-result-object v0

    invoke-virtual {v0}, LKN;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    sget-object v1, LKN;->a:LKN;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;LKN;)LKN;

    .line 113
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKn;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKn;

    move-result-object v0

    invoke-virtual {v0}, LKn;->a()V

    .line 115
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKn;

    move-result-object v0

    iget-object v1, p0, LKH;->a:LKG;

    iget-object v1, v1, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, LKn;->a(Ljava/lang/Runnable;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKn;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, LKH;->a:LKG;

    iget-object v2, v2, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->b(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LKn;->b(ZLjava/lang/Runnable;)V

    goto :goto_0

    .line 122
    :cond_2
    iget-object v0, p0, LKH;->a:LKG;

    iget-object v0, v0, LKG;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->b(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
