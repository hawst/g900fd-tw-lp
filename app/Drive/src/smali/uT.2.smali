.class public LuT;
.super Lajq;
.source "PhoneskyApplicationInstallerActivity.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, LuT;->a:Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;

    invoke-direct {p0}, Lajq;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;LuQ;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0, p1}, LuT;-><init>(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 155
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 156
    return-object v0
.end method

.method protected a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 162
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 169
    iget-object v1, p0, LuT;->a:Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;

    invoke-static {v1}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, LuU;

    invoke-direct {v2, p0, v0}, LuU;-><init>(LuT;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
