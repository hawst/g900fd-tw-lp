.class LOL;
.super Ljava/lang/Object;
.source "PhotosPreferencesInstaller.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Landroid/app/PendingIntent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LOK;

.field final synthetic a:LbsU;


# direct methods
.method constructor <init>(LOK;LbsU;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, LOL;->a:LOK;

    iput-object p2, p0, LOL;->a:LbsU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 184
    :try_start_0
    iget-object v0, p0, LOL;->a:LbsU;

    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :goto_0
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    iget-object v0, p0, LOL;->a:LOK;

    iget-object v0, v0, LOK;->a:LOJ;

    iget-object v0, v0, LOJ;->a:Landroid/app/Activity;

    sget v1, LOV;->auto_backup_open_settings_page_failure_toast:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 180
    check-cast p1, Landroid/app/PendingIntent;

    invoke-virtual {p0, p1}, LOL;->a(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, LOL;->a:LOK;

    iget-object v0, v0, LOK;->a:LOJ;

    iget-object v0, v0, LOJ;->a:Landroid/app/Activity;

    sget v1, LOV;->auto_backup_open_settings_page_failure_toast:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 195
    return-void
.end method
