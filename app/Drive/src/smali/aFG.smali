.class public LaFG;
.super Ljava/lang/Object;
.source "FieldDefinition.java"


# instance fields
.field private a:LaFH;

.field private final a:LaFI;

.field private a:LaFr;

.field private a:LaFy;

.field private a:Ljava/lang/Object;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LaFI;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object v1, p0, LaFG;->a:LaFy;

    .line 91
    iput-object v1, p0, LaFG;->a:LaFr;

    .line 92
    iput-boolean v2, p0, LaFG;->a:Z

    .line 93
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaFG;->a:Ljava/util/Set;

    .line 94
    iput-boolean v2, p0, LaFG;->b:Z

    .line 95
    iput-object v1, p0, LaFG;->a:Ljava/lang/Object;

    .line 96
    sget-object v0, LaFH;->a:LaFH;

    iput-object v0, p0, LaFG;->a:LaFH;

    .line 107
    iput-object p1, p0, LaFG;->a:Ljava/lang/String;

    .line 108
    iput-object p2, p0, LaFG;->a:LaFI;

    .line 109
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 188
    iget-boolean v0, p0, LaFG;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaFG;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 189
    const-string v0, "FieldDefinition"

    const-string v1, "Ignoring isIndexed constraint as field also has uniqueness constraint (on just this field, and therefore SQLite will have to create an index on that. For field: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 193
    iput-boolean v3, p0, LaFG;->a:Z

    .line 195
    :cond_0
    return-void
.end method


# virtual methods
.method public a()LaFE;
    .locals 11

    .prologue
    .line 224
    new-instance v0, LaFE;

    iget-object v1, p0, LaFG;->a:Ljava/lang/String;

    iget-object v2, p0, LaFG;->a:LaFI;

    iget-boolean v3, p0, LaFG;->a:Z

    iget-object v4, p0, LaFG;->a:Ljava/util/Set;

    iget-boolean v5, p0, LaFG;->b:Z

    iget-object v6, p0, LaFG;->a:Ljava/lang/Object;

    iget-object v7, p0, LaFG;->a:LaFy;

    iget-object v8, p0, LaFG;->a:LaFr;

    iget-object v9, p0, LaFG;->a:LaFH;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, LaFE;-><init>(Ljava/lang/String;LaFI;ZLjava/util/Set;ZLjava/lang/Object;LaFy;LaFr;LaFH;LaFF;)V

    return-object v0
.end method

.method public a()LaFG;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, LaFG;->a:Z

    .line 156
    invoke-direct {p0}, LaFG;->a()V

    .line 157
    return-object p0
.end method

.method public a(LaFy;)LaFG;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LaFG;->a(LaFy;LaFr;)LaFG;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFy;LaFr;)LaFG;
    .locals 1

    .prologue
    .line 130
    sget-object v0, LaFH;->a:LaFH;

    invoke-virtual {p0, p1, p2, v0}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFy;LaFr;LaFH;)LaFG;
    .locals 0

    .prologue
    .line 143
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iput-object p1, p0, LaFG;->a:LaFy;

    .line 146
    iput-object p2, p0, LaFG;->a:LaFr;

    .line 147
    iput-object p3, p0, LaFG;->a:LaFH;

    .line 148
    return-object p0
.end method

.method public a(Ljava/lang/Object;)LaFG;
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, LaFG;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "defaultValue already set"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 204
    const-string v0, "null defaultValue"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    iput-object p1, p0, LaFG;->a:Ljava/lang/Object;

    .line 206
    return-object p0

    .line 203
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs a([Ljava/lang/String;)LaFG;
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, LaFG;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 177
    iget-object v0, p0, LaFG;->a:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 178
    iget-object v0, p0, LaFG;->a:Ljava/util/Set;

    iget-object v1, p0, LaFG;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 179
    invoke-direct {p0}, LaFG;->a()V

    .line 180
    return-object p0
.end method

.method public b()LaFG;
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, LaFG;->b:Z

    .line 216
    return-object p0
.end method
