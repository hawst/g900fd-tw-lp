.class public Lgu;
.super LcS;
.source "DrawerLayout.java"


# instance fields
.field private final a:Landroid/graphics/Rect;

.field final synthetic a:Landroid/support/v4/widget/DrawerLayout;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/DrawerLayout;)V
    .locals 1

    .prologue
    .line 1804
    iput-object p1, p0, Lgu;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-direct {p0}, LcS;-><init>()V

    .line 1805
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgu;->a:Landroid/graphics/Rect;

    return-void
.end method

.method private a(LfB;Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 1857
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 1858
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1859
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1860
    invoke-static {v2}, Landroid/support/v4/widget/DrawerLayout;->e(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1861
    invoke-virtual {p1, v2}, LfB;->b(Landroid/view/View;)V

    .line 1858
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1864
    :cond_1
    return-void
.end method

.method private a(LfB;LfB;)V
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lgu;->a:Landroid/graphics/Rect;

    .line 1884
    invoke-virtual {p2, v0}, LfB;->a(Landroid/graphics/Rect;)V

    .line 1885
    invoke-virtual {p1, v0}, LfB;->b(Landroid/graphics/Rect;)V

    .line 1887
    invoke-virtual {p2, v0}, LfB;->c(Landroid/graphics/Rect;)V

    .line 1888
    invoke-virtual {p1, v0}, LfB;->d(Landroid/graphics/Rect;)V

    .line 1890
    invoke-virtual {p2}, LfB;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->c(Z)V

    .line 1891
    invoke-virtual {p2}, LfB;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LfB;->a(Ljava/lang/CharSequence;)V

    .line 1892
    invoke-virtual {p2}, LfB;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LfB;->b(Ljava/lang/CharSequence;)V

    .line 1893
    invoke-virtual {p2}, LfB;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LfB;->c(Ljava/lang/CharSequence;)V

    .line 1895
    invoke-virtual {p2}, LfB;->j()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->h(Z)V

    .line 1896
    invoke-virtual {p2}, LfB;->h()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->f(Z)V

    .line 1897
    invoke-virtual {p2}, LfB;->c()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->a(Z)V

    .line 1898
    invoke-virtual {p2}, LfB;->d()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->b(Z)V

    .line 1899
    invoke-virtual {p2}, LfB;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->d(Z)V

    .line 1900
    invoke-virtual {p2}, LfB;->g()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->e(Z)V

    .line 1901
    invoke-virtual {p2}, LfB;->i()Z

    move-result v0

    invoke-virtual {p1, v0}, LfB;->g(Z)V

    .line 1903
    invoke-virtual {p2}, LfB;->a()I

    move-result v0

    invoke-virtual {p1, v0}, LfB;->a(I)V

    .line 1904
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;LfB;)V
    .locals 3

    .prologue
    .line 1809
    invoke-static {p2}, LfB;->a(LfB;)LfB;

    move-result-object v1

    .line 1810
    invoke-super {p0, p1, v1}, LcS;->a(Landroid/view/View;LfB;)V

    .line 1812
    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LfB;->b(Ljava/lang/CharSequence;)V

    .line 1813
    invoke-virtual {p2, p1}, LfB;->a(Landroid/view/View;)V

    .line 1814
    invoke-static {p1}, Lec;->a(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    .line 1815
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1816
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, LfB;->c(Landroid/view/View;)V

    .line 1818
    :cond_0
    invoke-direct {p0, p2, v1}, Lgu;->a(LfB;LfB;)V

    .line 1820
    invoke-virtual {v1}, LfB;->a()V

    .line 1822
    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0, p2, p1}, Lgu;->a(LfB;Landroid/view/ViewGroup;)V

    .line 1823
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    .prologue
    .line 1839
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 1840
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    .line 1841
    iget-object v1, p0, Lgu;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-static {v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/support/v4/widget/DrawerLayout;)Landroid/view/View;

    move-result-object v1

    .line 1842
    if-eqz v1, :cond_0

    .line 1843
    iget-object v2, p0, Lgu;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/view/View;)I

    move-result v1

    .line 1844
    iget-object v2, p0, Lgu;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1845
    if-eqz v1, :cond_0

    .line 1846
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1850
    :cond_0
    const/4 v0, 0x1

    .line 1853
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, LcS;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1869
    invoke-static {p2}, Landroid/support/v4/widget/DrawerLayout;->e(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1870
    invoke-super {p0, p1, p2, p3}, LcS;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 1872
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1827
    invoke-super {p0, p1, p2}, LcS;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1829
    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1830
    return-void
.end method
