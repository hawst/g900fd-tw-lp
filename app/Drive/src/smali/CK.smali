.class public final enum LCK;
.super Ljava/lang/Enum;
.source "MenuItemsState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LCK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LCK;

.field private static final synthetic a:[LCK;

.field public static final enum b:LCK;

.field public static final enum c:LCK;

.field public static final enum d:LCK;

.field public static final enum e:LCK;

.field public static final enum f:LCK;

.field public static final enum g:LCK;

.field public static final enum h:LCK;

.field public static final enum i:LCK;

.field public static final enum j:LCK;

.field public static final enum k:LCK;

.field public static final enum l:LCK;

.field public static final enum m:LCK;

.field public static final enum n:LCK;

.field public static final enum o:LCK;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 45
    new-instance v0, LCK;

    const-string v1, "DELETE"

    sget v2, Lxc;->menu_delete:I

    invoke-direct {v0, v1, v4, v2}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->a:LCK;

    .line 46
    new-instance v0, LCK;

    const-string v1, "DELETE_FOREVER"

    sget v2, Lxc;->menu_delete_forever:I

    invoke-direct {v0, v1, v5, v2}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->b:LCK;

    .line 47
    new-instance v0, LCK;

    const-string v1, "UNTRASH"

    sget v2, Lxc;->menu_untrash:I

    invoke-direct {v0, v1, v6, v2}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->c:LCK;

    .line 48
    new-instance v0, LCK;

    const-string v1, "SHARING"

    sget v2, Lxc;->menu_sharing:I

    invoke-direct {v0, v1, v7, v2}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->d:LCK;

    .line 49
    new-instance v0, LCK;

    const-string v1, "OPEN_WITH"

    sget v2, Lxc;->menu_open_with:I

    invoke-direct {v0, v1, v8, v2}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->e:LCK;

    .line 50
    new-instance v0, LCK;

    const-string v1, "SEND"

    const/4 v2, 0x5

    sget v3, Lxc;->menu_send:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->f:LCK;

    .line 51
    new-instance v0, LCK;

    const-string v1, "DOWNLOAD"

    const/4 v2, 0x6

    sget v3, Lxc;->menu_download:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->g:LCK;

    .line 52
    new-instance v0, LCK;

    const-string v1, "CREATE_SHORTCUT"

    const/4 v2, 0x7

    sget v3, Lxc;->menu_create_shortcut:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->h:LCK;

    .line 53
    new-instance v0, LCK;

    const-string v1, "PRINT"

    const/16 v2, 0x8

    sget v3, Lxc;->menu_print:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->i:LCK;

    .line 54
    new-instance v0, LCK;

    const-string v1, "SEND_LINK"

    const/16 v2, 0x9

    sget v3, Lxc;->menu_send_link:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->j:LCK;

    .line 55
    new-instance v0, LCK;

    const-string v1, "RENAME"

    const/16 v2, 0xa

    sget v3, Lxc;->menu_rename:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->k:LCK;

    .line 56
    new-instance v0, LCK;

    const-string v1, "PIN"

    const/16 v2, 0xb

    sget v3, Lxc;->menu_pin:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->l:LCK;

    .line 57
    new-instance v0, LCK;

    const-string v1, "UNPIN"

    const/16 v2, 0xc

    sget v3, Lxc;->menu_unpin:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->m:LCK;

    .line 58
    new-instance v0, LCK;

    const-string v1, "MOVE_TO_FOLDER"

    const/16 v2, 0xd

    sget v3, Lxc;->menu_move_to_folder:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->n:LCK;

    .line 59
    new-instance v0, LCK;

    const-string v1, "DUMP_DATABASE"

    const/16 v2, 0xe

    sget v3, Lxc;->menu_dump_database:I

    invoke-direct {v0, v1, v2, v3}, LCK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LCK;->o:LCK;

    .line 44
    const/16 v0, 0xf

    new-array v0, v0, [LCK;

    sget-object v1, LCK;->a:LCK;

    aput-object v1, v0, v4

    sget-object v1, LCK;->b:LCK;

    aput-object v1, v0, v5

    sget-object v1, LCK;->c:LCK;

    aput-object v1, v0, v6

    sget-object v1, LCK;->d:LCK;

    aput-object v1, v0, v7

    sget-object v1, LCK;->e:LCK;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LCK;->f:LCK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LCK;->g:LCK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LCK;->h:LCK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LCK;->i:LCK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LCK;->j:LCK;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LCK;->k:LCK;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LCK;->l:LCK;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LCK;->m:LCK;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LCK;->n:LCK;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LCK;->o:LCK;

    aput-object v2, v0, v1

    sput-object v0, LCK;->a:[LCK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput p3, p0, LCK;->a:I

    .line 65
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LCK;
    .locals 1

    .prologue
    .line 44
    const-class v0, LCK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LCK;

    return-object v0
.end method

.method public static values()[LCK;
    .locals 1

    .prologue
    .line 44
    sget-object v0, LCK;->a:[LCK;

    invoke-virtual {v0}, [LCK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LCK;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, LCK;->a:I

    return v0
.end method
