.class public Lasc;
.super Ljava/lang/Object;
.source "HighlightsLauncherImpl.java"

# interfaces
.implements Lasb;


# instance fields
.field private final a:Lasa;

.field private final a:Lati;

.field private final a:LtK;


# direct methods
.method constructor <init>(Lati;Lasa;LtK;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lasc;->a:Lati;

    .line 28
    iput-object p2, p0, Lasc;->a:Lasa;

    .line 29
    iput-object p3, p0, Lasc;->a:LtK;

    .line 30
    return-void
.end method

.method private a(LasJ;ILaFO;)LasJ;
    .locals 4

    .prologue
    .line 51
    .line 52
    iget-object v0, p0, Lasc;->a:Lasa;

    invoke-virtual {v0}, Lasa;->a()I

    move-result v2

    .line 53
    add-int/lit8 v0, p2, 0x1

    move v1, v0

    move-object v0, p1

    :goto_0
    if-gt v1, v2, :cond_1

    .line 55
    iget-object v3, p0, Lasc;->a:Lasa;

    .line 56
    invoke-virtual {v3, v1}, Lasa;->a(I)Ljava/util/Set;

    move-result-object v3

    .line 57
    if-eqz v3, :cond_0

    .line 58
    invoke-direct {p0, v0, v3, p3}, Lasc;->a(LasJ;Ljava/util/Set;LaFO;)LasJ;

    move-result-object v0

    .line 54
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    :cond_1
    return-object v0
.end method

.method private a(LasJ;Ljava/util/Set;LaFO;)LasJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LasJ;",
            "Ljava/util/Set",
            "<",
            "Lrg;",
            ">;",
            "LaFO;",
            ")",
            "LasJ;"
        }
    .end annotation

    .prologue
    .line 66
    .line 67
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, p1

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrg;

    .line 68
    iget-object v3, p0, Lasc;->a:LtK;

    invoke-interface {v3, v0, p3}, LtK;->a(Lrg;LaFO;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 69
    iget-object v3, p0, Lasc;->a:Lati;

    invoke-interface {v3, v0}, Lati;->a(Lrg;)LasP;

    move-result-object v0

    .line 70
    if-eqz p1, :cond_1

    .line 72
    invoke-virtual {p1}, LasJ;->a()LasO;

    move-result-object v3

    invoke-static {v0, v3}, LasJ;->a(LasP;LasO;)LasJ;

    move-result-object v0

    invoke-virtual {v1, v0}, LasJ;->a(LasJ;)LasJ;

    move-result-object v1

    move-object v0, v1

    :goto_1
    move-object v1, v0

    .line 75
    goto :goto_0

    .line 76
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Context;ILaFO;)Z
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lasc;->a:Lati;

    invoke-interface {v0}, Lati;->c()LasP;

    move-result-object v0

    .line 36
    sget-object v1, LasO;->b:LasO;

    invoke-static {v0, v1}, LasJ;->a(LasP;LasO;)LasJ;

    move-result-object v0

    .line 37
    invoke-direct {p0, v0, p2, p3}, Lasc;->a(LasJ;ILaFO;)LasJ;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, LasJ;->a()I

    move-result v1

    if-lez v1, :cond_0

    .line 40
    invoke-static {p1, v0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Context;LasJ;)Landroid/content/Intent;

    move-result-object v0

    .line 41
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 42
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
