.class LbmI;
.super LbmF;
.source "ImmutableList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LbmF",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final transient a:I

.field private final transient a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbmF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 485
    invoke-direct {p0}, LbmF;-><init>()V

    .line 486
    iput-object p1, p0, LbmI;->a:LbmF;

    .line 487
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    iput v0, p0, LbmI;->a:I

    .line 488
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 491
    iget v0, p0, LbmI;->a:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method static synthetic a(LbmI;I)I
    .locals 1

    .prologue
    .line 481
    invoke-direct {p0, p1}, LbmI;->a(I)I

    move-result v0

    return v0
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 495
    iget v0, p0, LbmI;->a:I

    sub-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public a(II)LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 527
    iget v0, p0, LbmI;->a:I

    invoke-static {p1, p2, v0}, LbiT;->a(III)V

    .line 528
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-direct {p0, p2}, LbmI;->b(I)I

    move-result v1

    invoke-direct {p0, p1}, LbmI;->b(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LbmF;->a(II)LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->a_()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lbqw;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lbqw",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 538
    iget v0, p0, LbmI;->a:I

    invoke-static {p1, v0}, LbiT;->b(II)I

    .line 539
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-direct {p0, p1}, LbmI;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, LbmF;->a(I)Lbqw;

    move-result-object v0

    .line 540
    new-instance v1, LbmJ;

    invoke-direct {v1, p0, v0}, LbmJ;-><init>(LbmI;Lbqw;)V

    return-object v1
.end method

.method a()Z
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Z

    move-result v0

    return v0
.end method

.method public a_()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 500
    iget-object v0, p0, LbmI;->a:LbmF;

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 510
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 532
    iget v0, p0, LbmI;->a:I

    invoke-static {p1, v0}, LbiT;->a(II)I

    .line 533
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-direct {p0, p1}, LbmI;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    .line 516
    if-ltz v0, :cond_0

    invoke-direct {p0, v0}, LbmI;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 481
    invoke-super {p0}, LbmF;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, LbmI;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 522
    if-ltz v0, :cond_0

    invoke-direct {p0, v0}, LbmI;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 481
    invoke-super {p0}, LbmF;->a()Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0, p1}, LbmI;->a(I)Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 568
    iget v0, p0, LbmI;->a:I

    return v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0, p1, p2}, LbmI;->a(II)LbmF;

    move-result-object v0

    return-object v0
.end method
