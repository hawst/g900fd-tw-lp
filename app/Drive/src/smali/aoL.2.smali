.class public abstract LaoL;
.super Ljava/lang/Object;
.source "RetryingFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoo",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:LZS;

.field private final a:Laoo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoo",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final a:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final a:LbsJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsJ",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laoo;ILaoy;LZS;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<TK;TV;>;I",
            "Laoy",
            "<TV;>;",
            "LZS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 36
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoo;

    iput-object v0, p0, LaoL;->a:Laoo;

    .line 37
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoy;

    iput-object v0, p0, LaoL;->a:Laoy;

    .line 38
    iput p2, p0, LaoL;->a:I

    .line 39
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p0, LaoL;->a:LZS;

    .line 40
    new-instance v0, LaoM;

    invoke-direct {v0, p0}, LaoM;-><init>(LaoL;)V

    iput-object v0, p0, LaoL;->a:LbsJ;

    .line 51
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LaoL;)LZS;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, LaoL;->a:LZS;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, LaoL;->a:Laoy;

    invoke-static {v0}, Laou;->a(Laoy;)Laou;

    move-result-object v0

    .line 56
    iget v1, p0, LaoL;->a:I

    invoke-virtual {p0, p1, v0, v1}, LaoL;->a(Ljava/lang/Object;Laou;I)LbsU;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Laou;->b(LbsU;)V

    .line 58
    iget-object v0, p0, LaoL;->a:LbsJ;

    invoke-static {v1, v0}, LbsK;->a(LbsU;LbsJ;)V

    .line 59
    return-object v1
.end method

.method a(Ljava/lang/Object;Laou;I)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Laou",
            "<TV;>;I)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, LaoL;->a:Laoo;

    invoke-interface {v0, p1}, Laoo;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 65
    invoke-virtual {p2, v0}, Laou;->a(LbsU;)V

    .line 67
    new-instance v1, LaoN;

    invoke-direct {v1, p0, p1, p3, p2}, LaoN;-><init>(LaoL;Ljava/lang/Object;ILaou;)V

    invoke-static {v0, v1}, Lalv;->a(LbsU;LalB;)LbsU;

    move-result-object v0

    .line 87
    return-object v0
.end method
