.class public LGe;
.super Ljava/lang/Object;
.source "DownloadFileDocumentOpenerImpl.java"

# interfaces
.implements LDL;


# instance fields
.field private final a:LFT;

.field private final a:LacY;

.field private a:Lamr;

.field private final a:Landroid/os/Bundle;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;LFT;LaGo;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 251
    iput-object p1, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, LGe;->a:Lamr;

    .line 252
    iput-object p4, p0, LGe;->a:Landroid/os/Bundle;

    .line 253
    invoke-interface {p3}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    iput-object v0, p0, LGe;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 254
    invoke-interface {p3}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LGe;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 255
    invoke-interface {p3}, LaGo;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGe;->a:Ljava/lang/String;

    .line 256
    invoke-virtual {p1, p3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(LaGo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGe;->b:Ljava/lang/String;

    .line 257
    iput-object p2, p0, LGe;->a:LFT;

    .line 258
    invoke-static {p4}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v0

    .line 259
    invoke-interface {p3}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v1

    iput-object v1, p0, LGe;->a:LacY;

    .line 260
    invoke-static {p1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Lrm;

    move-result-object v1

    iget v0, v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:I

    invoke-virtual {v1, v0}, Lrm;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGe;->c:Ljava/lang/String;

    .line 261
    return-void
.end method

.method private a()I
    .locals 11

    .prologue
    const/4 v6, -0x1

    const/4 v9, 0x0

    const/4 v7, 0x1

    .line 319
    iget-object v0, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LaGM;

    move-result-object v0

    iget-object v1, p0, LGe;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 320
    if-nez v0, :cond_1

    .line 366
    :cond_0
    :goto_0
    return v6

    .line 323
    :cond_1
    iget-object v1, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Ladb;

    move-result-object v1

    iget-object v2, p0, LGe;->a:LacY;

    invoke-interface {v1, v0, v2}, Ladb;->a(LaGo;LacY;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    .line 325
    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LaGM;

    move-result-object v1

    iget-object v2, p0, LGe;->a:LacY;

    invoke-interface {v1, v0, v2}, LaGM;->a(LaGo;LacY;)LaGp;

    move-result-object v1

    .line 324
    invoke-direct {p0, v0, v1}, LGe;->a(LaGo;LaGp;)Z

    move-result v1

    if-nez v1, :cond_2

    move v6, v7

    .line 326
    goto :goto_0

    .line 329
    :cond_2
    iget-object v1, p0, LGe;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 330
    if-nez v1, :cond_3

    .line 331
    invoke-interface {v0}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 333
    :cond_3
    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    iget-object v0, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LqK;

    move-result-object v0

    invoke-virtual {v0, p0}, LqK;->a(Ljava/lang/Object;)V

    .line 337
    const/4 v2, 0x0

    .line 339
    :try_start_0
    iget-object v0, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LGb;

    move-result-object v0

    iget-object v3, p0, LGe;->a:LFT;

    invoke-interface {v0, v1, v3}, LGb;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LFS;)Ljava/lang/String;
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 351
    :goto_1
    if-eqz v2, :cond_0

    .line 355
    new-instance v8, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v8, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 356
    iget-object v0, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LagG;

    move-result-object v0

    iget-object v3, p0, LGe;->b:Ljava/lang/String;

    iget-object v4, p0, LGe;->a:LacY;

    new-instance v5, LGf;

    iget-object v9, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    .line 357
    invoke-static {v9}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Lrm;

    move-result-object v9

    iget-object v10, p0, LGe;->a:Lamr;

    invoke-direct {v5, p0, v9, v10, v8}, LGf;-><init>(LGe;Landroid/content/Context;Lamr;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 356
    invoke-interface/range {v0 .. v5}, LagG;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;LacY;LagH;)V

    .line 364
    iget-object v0, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LqK;

    move-result-object v0

    const-string v1, "downloadTime"

    invoke-virtual {v0, p0, v1}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v6

    :goto_2
    move v6, v0

    goto :goto_0

    .line 340
    :catch_0
    move-exception v0

    .line 341
    const-string v3, "DownloadFileDocumentOpener"

    const-string v4, "Authentication error: %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, LbwO;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v9

    invoke-static {v3, v4, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 342
    iget-object v3, p0, LGe;->a:LFT;

    sget-object v4, LFV;->f:LFV;

    invoke-interface {v3, v4, v0}, LFT;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 343
    :catch_1
    move-exception v0

    .line 344
    const-string v3, "DownloadFileDocumentOpener"

    const-string v4, "Unable to parse document feed: %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lbxk;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v9

    invoke-static {v3, v4, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 345
    iget-object v3, p0, LGe;->a:LFT;

    sget-object v4, LFV;->h:LFV;

    invoke-interface {v3, v4, v0}, LFT;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 346
    :catch_2
    move-exception v0

    .line 347
    const-string v3, "DownloadFileDocumentOpener"

    const-string v4, "Network error: %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v9

    invoke-static {v3, v4, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 348
    iget-object v3, p0, LGe;->a:LFT;

    sget-object v4, LFV;->g:LFV;

    invoke-interface {v3, v4, v0}, LFT;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    move v0, v7

    .line 366
    goto :goto_2
.end method

.method static synthetic a(LGe;)LFT;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, LGe;->a:LFT;

    return-object v0
.end method

.method private a(LaGo;LaGp;)Z
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 295
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v0

    invoke-virtual {v0}, LaGv;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    :goto_0
    return v4

    .line 309
    :cond_0
    invoke-virtual {p2}, LaGp;->d()Ljava/lang/Long;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 312
    :goto_1
    invoke-interface {p1}, LaGo;->c()Ljava/lang/Long;

    move-result-object v5

    .line 313
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 315
    :cond_1
    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v4, v0

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 310
    goto :goto_1

    :cond_3
    move v0, v4

    .line 315
    goto :goto_2
.end method

.method private b()I
    .locals 4

    .prologue
    .line 370
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const/4 v0, -0x1

    .line 375
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    iget-object v1, p0, LGe;->a:LFT;

    iget-object v2, p0, LGe;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, p0, LGe;->a:Landroid/os/Bundle;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;LFT;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/os/Bundle;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 265
    packed-switch p1, :pswitch_data_0

    .line 273
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 269
    :pswitch_0
    invoke-direct {p0}, LGe;->a()I

    move-result v0

    goto :goto_0

    .line 271
    :pswitch_1
    invoke-direct {p0}, LGe;->b()I

    move-result v0

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 279
    iget-object v0, p0, LGe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LGe;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lamr;)V
    .locals 7

    .prologue
    .line 289
    iput-object p1, p0, LGe;->a:Lamr;

    .line 290
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x64

    iget-object v6, p0, LGe;->c:Ljava/lang/String;

    move-object v1, p1

    invoke-interface/range {v1 .. v6}, Lamr;->a(JJLjava/lang/String;)V

    .line 291
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x1

    return v0
.end method
