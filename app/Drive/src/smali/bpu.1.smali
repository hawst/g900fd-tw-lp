.class final Lbpu;
.super Lbpw;
.source "NullsLastOrdering.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lbpw",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field final a:Lbpw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpw",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lbpw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbpw",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lbpw;-><init>()V

    .line 31
    iput-object p1, p0, Lbpu;->a:Lbpw;

    .line 32
    return-void
.end method


# virtual methods
.method public a()Lbpw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "Lbpw",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lbpu;->a:Lbpw;

    invoke-virtual {v0}, Lbpw;->a()Lbpw;

    move-result-object v0

    invoke-virtual {v0}, Lbpw;->b()Lbpw;

    move-result-object v0

    return-object v0
.end method

.method public b()Lbpw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "Lbpw",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lbpu;->a:Lbpw;

    invoke-virtual {v0}, Lbpw;->b()Lbpw;

    move-result-object v0

    return-object v0
.end method

.method public c()Lbpw;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "Lbpw",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 63
    return-object p0
.end method

.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 36
    if-ne p1, p2, :cond_0

    .line 37
    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    .line 39
    :cond_0
    if-nez p1, :cond_1

    .line 40
    const/4 v0, 0x1

    goto :goto_0

    .line 42
    :cond_1
    if-nez p2, :cond_2

    .line 43
    const/4 v0, -0x1

    goto :goto_0

    .line 45
    :cond_2
    iget-object v0, p0, Lbpu;->a:Lbpw;

    invoke-virtual {v0, p1, p2}, Lbpw;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 68
    if-ne p1, p0, :cond_0

    .line 69
    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    .line 71
    :cond_0
    instance-of v0, p1, Lbpu;

    if-eqz v0, :cond_1

    .line 72
    check-cast p1, Lbpu;

    .line 73
    iget-object v0, p0, Lbpu;->a:Lbpw;

    iget-object v1, p1, Lbpu;->a:Lbpw;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 75
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lbpu;->a:Lbpw;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, -0x36e88db8    # -620324.5f

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbpu;->a:Lbpw;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nullsLast()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
