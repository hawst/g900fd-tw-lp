.class public LahL;
.super Ljava/lang/Object;
.source "SyncTaskQueueImpl.java"

# interfaces
.implements LahK;
.implements Lahy;


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static final a:LbiU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiU",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LahG;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LQr;

.field private final a:LZS;

.field private final a:LaGg;

.field private final a:Lagd;

.field private final a:LahI;

.field private final a:LahO;

.field private final a:Landroid/os/Handler;

.field private final a:LbjF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjF",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LahG;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LahG;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lahz;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LahG;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LahG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, LahM;

    invoke-direct {v0}, LahM;-><init>()V

    sput-object v0, LahL;->a:LbiU;

    return-void
.end method

.method public constructor <init>(LaGg;LahO;LahI;LQr;Lagd;LZS;)V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->c()LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->a()LbjF;

    move-result-object v0

    iput-object v0, p0, LahL;->a:LbjF;

    .line 88
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LahL;->a:Ljava/util/Map;

    .line 90
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LahL;->b:Ljava/util/Map;

    .line 92
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LahL;->c:Ljava/util/Map;

    .line 93
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LahL;->a:Ljava/util/Set;

    .line 97
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, LahL;->a:Landroid/os/Handler;

    .line 107
    iput-object p1, p0, LahL;->a:LaGg;

    .line 108
    iput-object p2, p0, LahL;->a:LahO;

    .line 109
    iput-object p3, p0, LahL;->a:LahI;

    .line 110
    iput-object p4, p0, LahL;->a:LQr;

    .line 111
    iput-object p5, p0, LahL;->a:Lagd;

    .line 112
    iput-object p6, p0, LahL;->a:LZS;

    .line 113
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/Thread;Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 322
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, LahL;->c(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p2}, LahL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p2}, LahL;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 339
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 326
    :cond_1
    :try_start_1
    invoke-direct {p0, p2}, LahL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahG;

    move-result-object v1

    .line 327
    if-eqz v1, :cond_0

    .line 331
    invoke-virtual {v1}, LahG;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 332
    const-string v1, "SyncTaskQueue"

    const-string v2, "Max sync attempt count reached: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 336
    :cond_2
    :try_start_2
    invoke-virtual {v1, p1}, LahG;->a(Ljava/lang/Thread;)V

    .line 338
    iget-object v0, p0, LahL;->a:Ljava/util/Map;

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    .line 339
    goto :goto_0
.end method

.method private declared-synchronized a(LaGb;)LahG;
    .locals 3

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    iget-object v0, p0, LahL;->a:LahO;

    invoke-interface {v0, p1}, LahO;->a(LaGb;)LaHd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 185
    if-nez v0, :cond_0

    .line 186
    const/4 v0, 0x0

    .line 189
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, p0, LahL;->a:LahI;

    invoke-virtual {p1}, LaGb;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-virtual {v1, p0, v2, v0}, LahI;->a(LahL;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHd;)LahG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahG;
    .locals 2

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahG;

    .line 144
    if-nez v0, :cond_0

    .line 145
    iget-object v1, p0, LahL;->a:LaGg;

    invoke-interface {v1, p1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v1

    .line 146
    if-eqz v1, :cond_0

    .line 147
    invoke-direct {p0, v1}, LahL;->a(LaGb;)LahG;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    .line 149
    iget-object v1, p0, LahL;->a:LbjF;

    invoke-interface {v1, p1, v0}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :cond_0
    monitor-exit p0

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 307
    invoke-virtual {p0}, LahL;->a()I

    move-result v0

    .line 309
    iget-object v1, p0, LahL;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahG;
    .locals 2

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LahL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahG;

    move-result-object v0

    .line 168
    if-nez v0, :cond_0

    .line 169
    iget-object v1, p0, LahL;->a:LaGg;

    invoke-interface {v1, p1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    .line 171
    iget-object v0, p0, LahL;->a:LahO;

    invoke-interface {v0, v1}, LahO;->b(LaGb;)LaHd;

    move-result-object v0

    .line 172
    iget-object v1, p0, LahL;->a:LahI;

    invoke-virtual {v1, p0, p1, v0}, LahI;->a(LahL;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHd;)LahG;

    move-result-object v0

    .line 173
    iget-object v1, p0, LahL;->a:LbjF;

    invoke-interface {v1, p1, v0}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_0
    monitor-exit p0

    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(LahS;)V
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, LahL;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 252
    monitor-enter p0

    .line 253
    :try_start_0
    iget-object v0, p0, LahL;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahG;

    .line 254
    if-eqz p1, :cond_1

    invoke-virtual {v0}, LahG;->a()LahS;

    move-result-object v2

    invoke-virtual {p1, v2}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    :cond_1
    invoke-virtual {v0}, LahG;->m()V

    goto :goto_0

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    invoke-virtual {p0}, LahL;->b()V

    .line 261
    return-void
.end method

.method private declared-synchronized c(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, LahL;->a:LQr;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LQr;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/database/Cursor;)LahF;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, LahL;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 137
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, LahL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, LahL;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 130
    invoke-direct {p0, p1}, LahL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahG;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Thread;)LahF;
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, LahL;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 197
    invoke-virtual {p0}, LahL;->c()V

    .line 199
    invoke-virtual {p0}, LahL;->d()V

    .line 201
    const/4 v0, 0x0

    .line 203
    invoke-direct {p0}, LahL;->a()Landroid/database/Cursor;

    move-result-object v1

    .line 205
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 206
    iget-object v2, p0, LahL;->a:LaGg;

    invoke-interface {v2, v1}, LaGg;->a(Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 207
    if-eqz v2, :cond_0

    .line 211
    invoke-direct {p0, p1, v2}, LahL;->a(Ljava/lang/Thread;Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 212
    if-eqz v0, :cond_0

    .line 217
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 220
    return-object v0

    .line 217
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahR;
    .locals 1

    .prologue
    .line 368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahF;

    .line 369
    if-eqz v0, :cond_0

    .line 370
    invoke-interface {v0}, LahF;->a()LahR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 377
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 368
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 119
    iget-object v0, p0, LahL;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 120
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahG;

    invoke-virtual {v0}, LahG;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 122
    :cond_0
    :try_start_1
    iget-object v0, p0, LahL;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(LahF;)V
    .locals 2

    .prologue
    .line 302
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, LahF;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 303
    iget-object v1, p0, LahL;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    monitor-exit p0

    return-void

    .line 302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(LahG;)V
    .locals 2

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LahG;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 265
    iget-object v1, p0, LahL;->b:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    monitor-exit p0

    return-void

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LahS;)V
    .locals 0

    .prologue
    .line 245
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    invoke-direct {p0, p1}, LahL;->b(LahS;)V

    .line 247
    return-void
.end method

.method public a(Lahz;)V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, LahL;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 383
    return-void
.end method

.method a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 288
    monitor-enter p0

    .line 289
    :try_start_0
    iget-object v0, p0, LahL;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahG;

    .line 290
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, LahL;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V

    .line 294
    :cond_0
    return-void

    .line 290
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Lcom/google/android/gms/drive/database/data/EntrySpec;LahR;)V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, LahL;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahz;

    .line 392
    invoke-interface {v0, p1, p2}, Lahz;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LahR;)V

    goto :goto_0

    .line 394
    :cond_0
    return-void
.end method

.method public declared-synchronized a()Z
    .locals 1

    .prologue
    .line 398
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lahz;)Z
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, LahL;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method declared-synchronized a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, LahL;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 162
    invoke-direct {p0, p1}, LahL;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahG;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, LahL;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 228
    monitor-enter p0

    .line 229
    :try_start_0
    iget-object v0, p0, LahL;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    sget-object v1, LahL;->a:LbiU;

    invoke-static {v0, v1}, Lbnm;->a(Ljava/lang/Iterable;LbiU;)Z

    move-result v0

    .line 231
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, LahL;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V

    .line 236
    :cond_0
    return-void

    .line 231
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method declared-synchronized b(LahG;)V
    .locals 4

    .prologue
    .line 273
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LahG;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 274
    iget-object v1, p0, LahL;->c:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    iget-object v1, p0, LahL;->a:LQr;

    const-string v2, "syncDelayTimeMs"

    const/16 v3, 0x4e20

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v1

    .line 277
    iget-object v2, p0, LahL;->a:Landroid/os/Handler;

    new-instance v3, LahN;

    invoke-direct {v3, p0, v0}, LahN;-><init>(LahL;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    int-to-long v0, v1

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    monitor-exit p0

    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 298
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, LahL;->a:LaGg;

    iget-object v1, p0, LahL;->a:LZS;

    invoke-interface {v0, v1}, LaGg;->a(LZS;)V

    .line 318
    return-void
.end method

.method declared-synchronized d()V
    .locals 3

    .prologue
    .line 348
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahL;->a:LahO;

    invoke-interface {v0}, LahO;->a()LbmF;

    move-result-object v0

    .line 350
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHe;

    .line 351
    iget-object v2, p0, LahL;->a:LahO;

    invoke-interface {v2, v0}, LahO;->a(LaHe;)LaGb;

    move-result-object v2

    .line 352
    if-eqz v2, :cond_1

    .line 353
    invoke-interface {v2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-direct {p0, v2}, LahL;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahG;

    move-result-object v2

    .line 354
    if-nez v2, :cond_0

    .line 355
    invoke-virtual {v0}, LaHe;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 357
    :cond_0
    :try_start_1
    invoke-virtual {v2, v0}, LahG;->a(LaHe;)V

    goto :goto_0

    .line 360
    :cond_1
    invoke-virtual {v0}, LaHe;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 363
    :cond_2
    monitor-exit p0

    return-void
.end method
