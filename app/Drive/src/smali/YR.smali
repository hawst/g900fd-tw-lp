.class public final enum LYR;
.super Ljava/lang/Enum;
.source "PunchUiModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LYR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LYR;

.field private static final synthetic a:[LYR;

.field public static final enum b:LYR;

.field public static final enum c:LYR;

.field public static final enum d:LYR;


# instance fields
.field private final a:I

.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 42
    new-instance v0, LYR;

    const-string v1, "NORMAL"

    sget v7, LpP;->punch_web_view_whole:I

    move v3, v2

    move v5, v2

    move v6, v2

    invoke-direct/range {v0 .. v7}, LYR;-><init>(Ljava/lang/String;IZZZZI)V

    sput-object v0, LYR;->a:LYR;

    .line 43
    new-instance v5, LYR;

    const-string v6, "FULL_SCREEN"

    sget v12, LpP;->punch_web_view_whole:I

    move v7, v4

    move v8, v2

    move v9, v2

    move v10, v4

    move v11, v2

    invoke-direct/range {v5 .. v12}, LYR;-><init>(Ljava/lang/String;IZZZZI)V

    sput-object v5, LYR;->b:LYR;

    .line 44
    new-instance v5, LYR;

    const-string v6, "SPLIT_SCREEN"

    sget v12, LpP;->punch_web_view_whole:I

    move v7, v13

    move v8, v4

    move v9, v4

    move v10, v4

    move v11, v2

    invoke-direct/range {v5 .. v12}, LYR;-><init>(Ljava/lang/String;IZZZZI)V

    sput-object v5, LYR;->c:LYR;

    .line 45
    new-instance v5, LYR;

    const-string v6, "PRESENTATION"

    sget v12, LpP;->punch_web_view_whole_presentation_mode:I

    move v7, v14

    move v8, v4

    move v9, v4

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v12}, LYR;-><init>(Ljava/lang/String;IZZZZI)V

    sput-object v5, LYR;->d:LYR;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [LYR;

    sget-object v1, LYR;->a:LYR;

    aput-object v1, v0, v2

    sget-object v1, LYR;->b:LYR;

    aput-object v1, v0, v4

    sget-object v1, LYR;->c:LYR;

    aput-object v1, v0, v13

    sget-object v1, LYR;->d:LYR;

    aput-object v1, v0, v14

    sput-object v0, LYR;->a:[LYR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZI)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput-boolean p3, p0, LYR;->a:Z

    .line 60
    iput-boolean p4, p0, LYR;->b:Z

    .line 61
    iput-boolean p5, p0, LYR;->c:Z

    .line 62
    iput-boolean p6, p0, LYR;->d:Z

    .line 63
    iput p7, p0, LYR;->a:I

    .line 64
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LYR;
    .locals 1

    .prologue
    .line 41
    const-class v0, LYR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LYR;

    return-object v0
.end method

.method public static values()[LYR;
    .locals 1

    .prologue
    .line 41
    sget-object v0, LYR;->a:[LYR;

    invoke-virtual {v0}, [LYR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LYR;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, LYR;->a:I

    return v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, LYR;->b:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, LYR;->c:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, LYR;->d:Z

    return v0
.end method
