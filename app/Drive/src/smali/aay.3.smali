.class public Laay;
.super Ljava/lang/Object;
.source "SearchTerm.java"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final a:J

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "[^\\s\"\']+|\"([^\"]*)\"|\'([^\']*)\'"

    .line 27
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Laay;->a:Ljava/util/regex/Pattern;

    .line 26
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 40
    iput-object p1, p0, Laay;->a:Ljava/lang/String;

    .line 41
    iput-wide p2, p0, Laay;->a:J

    .line 42
    invoke-static {p1}, Laay;->a(Ljava/lang/String;)LbmF;

    move-result-object v0

    iput-object v0, p0, Laay;->a:LbmF;

    .line 43
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()Laay;
    .locals 4

    .prologue
    .line 121
    new-instance v0, Laay;

    const-string v1, ""

    const-wide/16 v2, -0x1

    invoke-direct {v0, v1, v2, v3}, Laay;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;J)Laay;
    .locals 1

    .prologue
    .line 131
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    new-instance v0, Laay;

    invoke-direct {v0, p0, p1, p2}, Laay;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method static a(Ljava/lang/String;)LbmF;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 47
    if-nez p0, :cond_0

    .line 48
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    .line 51
    :cond_0
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v2

    .line 52
    sget-object v0, Laay;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 53
    const/4 v0, 0x0

    .line 54
    :cond_1
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 56
    invoke-virtual {v3, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 58
    invoke-virtual {v3, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 67
    :goto_1
    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 68
    invoke-virtual {v2, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 69
    add-int/lit8 v0, v0, 0x1

    .line 70
    const/16 v1, 0xa

    if-lt v0, v1, :cond_1

    .line 76
    :cond_2
    invoke-virtual {v2}, LbmH;->a()LbmF;

    move-result-object v0

    goto :goto_0

    .line 59
    :cond_3
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 61
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 64
    :cond_4
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 114
    iget-wide v0, p0, Laay;->a:J

    return-wide v0
.end method

.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Laay;->a:LbmF;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Laay;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 81
    instance-of v1, p1, Laay;

    if-nez v1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 84
    :cond_1
    check-cast p1, Laay;

    .line 85
    iget-object v1, p0, Laay;->a:Ljava/lang/String;

    iget-object v2, p1, Laay;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Laay;->a:J

    iget-wide v4, p1, Laay;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 90
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Laay;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Laay;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
