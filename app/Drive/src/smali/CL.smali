.class LCL;
.super Ljava/lang/Object;
.source "MenuItemsState.java"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LCK;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const-class v0, LCK;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LCL;->a:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(LCJ;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, LCL;-><init>()V

    return-void
.end method


# virtual methods
.method a()LCI;
    .locals 5

    .prologue
    .line 118
    iget-object v0, p0, LCL;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LCL;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 119
    new-instance v0, LCI;

    iget-object v1, p0, LCL;->a:Ljava/util/Set;

    iget-object v2, p0, LCL;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, LCL;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, LCI;-><init>(Ljava/util/Set;ZZLCJ;)V

    return-object v0

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(LCK;Z)LCL;
    .locals 1

    .prologue
    .line 78
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    if-eqz p2, :cond_0

    .line 80
    iget-object v0, p0, LCL;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    :goto_0
    return-object p0

    .line 82
    :cond_0
    iget-object v0, p0, LCL;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method a(Ljava/util/Set;)LCL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LCK;",
            ">;)",
            "LCL;"
        }
    .end annotation

    .prologue
    .line 102
    const-class v0, LCK;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 103
    iget-object v0, p0, LCL;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCK;

    .line 104
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 105
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, LCL;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 110
    return-object p0
.end method

.method a(Z)LCL;
    .locals 1

    .prologue
    .line 88
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LCL;->a:Ljava/lang/Boolean;

    .line 89
    return-object p0
.end method

.method b(Z)LCL;
    .locals 1

    .prologue
    .line 93
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LCL;->b:Ljava/lang/Boolean;

    .line 94
    return-object p0
.end method
