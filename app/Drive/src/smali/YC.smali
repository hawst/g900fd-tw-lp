.class LYC;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"


# instance fields
.field final synthetic a:I

.field final synthetic a:LYA;

.field final synthetic a:LYB;


# direct methods
.method constructor <init>(LYB;LYA;I)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, LYC;->a:LYB;

    iput-object p2, p0, LYC;->a:LYA;

    iput p3, p0, LYC;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadImage()V
    .locals 6
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 144
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "t=%s img.onload(): slideIndex=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LYC;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, LYC;->a:LYB;

    invoke-static {v3}, LYB;->a(LYB;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 146
    iget-object v0, p0, LYC;->a:LYB;

    invoke-static {v0}, LYB;->a(LYB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, LYC;->a:LYB;

    invoke-static {v0, v5}, LYB;->a(LYB;Z)Z

    .line 148
    iget-object v0, p0, LYC;->a:LYB;

    invoke-static {v0, v4}, LYB;->b(LYB;Z)Z

    .line 150
    :cond_0
    return-void
.end method
