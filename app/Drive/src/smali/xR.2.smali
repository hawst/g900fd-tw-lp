.class LxR;
.super Ljava/lang/Object;
.source "CrossAppPromoCatalogFetcherImpl.java"

# interfaces
.implements LxQ;


# instance fields
.field private final a:LTl;

.field private final a:LxU;


# direct methods
.method public constructor <init>(LTl;LxU;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTl;

    iput-object v0, p0, LxR;->a:LTl;

    .line 34
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxU;

    iput-object v0, p0, LxR;->a:LxU;

    .line 35
    return-void
.end method

.method private static a(Ljava/lang/String;)Lbtk;
    .locals 4

    .prologue
    .line 44
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    invoke-virtual {v0, p0}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lbth;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    new-instance v1, LxT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Catalog information is not valid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LxT;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)LxS;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 57
    iget-object v0, p0, LxR;->a:LTl;

    invoke-virtual {v0, p1}, LTl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 60
    :try_start_0
    invoke-static {v1}, LxR;->a(Ljava/lang/String;)Lbtk;

    move-result-object v0

    .line 61
    iget-object v2, p0, LxR;->a:LxU;

    .line 62
    invoke-virtual {v2, v0}, LxU;->a(Lbtk;)LxS;
    :try_end_0
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_0
    .catch LxT; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    const-string v2, "CrossAppPromoCatalogFetcher"

    const-string v3, "Catalog file is malformed: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 72
    :goto_1
    sget-object v0, LxS;->a:LxS;

    goto :goto_0

    .line 66
    :catch_1
    move-exception v0

    .line 69
    const-string v1, "CrossAppPromoCatalogFetcher"

    const-string v2, "Catalog information is not valid. Error in parsing catalog."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
