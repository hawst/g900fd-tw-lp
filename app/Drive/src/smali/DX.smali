.class LDX;
.super Ljava/lang/Object;
.source "ThumbnailCallback.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "LakD",
        "<",
        "Lcom/google/android/apps/docs/utils/RawPixelData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/apps/docs/utils/FetchSpec;

.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LEd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LEd;Lcom/google/android/apps/docs/utils/FetchSpec;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 24
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LDX;->a:Ljava/lang/ref/WeakReference;

    .line 25
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/FetchSpec;

    iput-object v0, p0, LDX;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 26
    return-void
.end method


# virtual methods
.method public a(LakD;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .line 45
    :try_start_0
    iget-object v0, p0, LDX;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEd;

    .line 46
    if-eqz v0, :cond_0

    .line 47
    iget-object v1, p0, LDX;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :cond_0
    return-void

    .line 51
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_1

    .line 52
    invoke-virtual {p1}, LakD;->close()V

    :cond_1
    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, LDX;->a(LakD;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 30
    const-string v0, "ThumbnailCallback"

    const-string v1, "Fail to fetch thumbnail for %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LDX;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 31
    iget-object v0, p0, LDX;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEd;

    .line 32
    if-eqz v0, :cond_0

    .line 33
    instance-of v1, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v1, :cond_1

    .line 34
    iget-object v1, p0, LDX;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {v0, v1}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)V

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    iget-object v1, p0, LDX;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {v0, v1, v5}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;Z)V

    goto :goto_0
.end method
