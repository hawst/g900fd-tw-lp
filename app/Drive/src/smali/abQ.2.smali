.class public LabQ;
.super Ljava/lang/Object;
.source "GlobalSharingApi.java"


# static fields
.field private static final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lqt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LTd;

.field private final a:LagZ;

.field private final a:LbsW;

.field private final a:Lcom/google/api/services/drive/Drive;

.field private final a:LqK;

.field private final a:Z

.field private final b:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    sget-object v0, Lqt;->d:Lqt;

    sget-object v1, Lqt;->b:Lqt;

    sget-object v2, Lqt;->c:Lqt;

    .line 62
    invoke-static {v0, v1, v2}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, LabQ;->a:LbmY;

    .line 61
    return-void
.end method

.method public constructor <init>(LTd;LQr;LtK;LagZ;LqK;)V
    .locals 5

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v0

    iput-object v0, p0, LabQ;->a:LbsW;

    .line 117
    iput-object p1, p0, LabQ;->a:LTd;

    .line 118
    iput-object p5, p0, LabQ;->a:LqK;

    .line 119
    invoke-direct {p0, p2}, LabQ;->a(LQr;)LbmY;

    move-result-object v0

    iput-object v0, p0, LabQ;->b:LbmY;

    .line 120
    sget-object v0, Lry;->R:Lry;

    invoke-interface {p3, v0}, LtK;->a(LtJ;)Z

    move-result v0

    iput-boolean v0, p0, LabQ;->a:Z

    .line 122
    iput-object p4, p0, LabQ;->a:LagZ;

    .line 123
    const-string v0, "linkSharingApiTtlMs"

    const/16 v1, 0x1388

    invoke-interface {p2, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 124
    new-instance v1, Lcom/google/api/services/drive/Drive$Builder;

    .line 125
    invoke-static {}, Lbdi;->a()Lbeq;

    move-result-object v2

    new-instance v3, LbeY;

    invoke-direct {v3}, LbeY;-><init>()V

    new-instance v4, LabR;

    invoke-direct {v4, p0, v0}, LabR;-><init>(LabQ;I)V

    invoke-direct {v1, v2, v3, v4}, Lcom/google/api/services/drive/Drive$Builder;-><init>(Lbeq;LbeO;Lbek;)V

    .line 132
    invoke-virtual {v1}, Lcom/google/api/services/drive/Drive$Builder;->a()Lcom/google/api/services/drive/Drive;

    move-result-object v0

    iput-object v0, p0, LabQ;->a:Lcom/google/api/services/drive/Drive;

    .line 133
    return-void
.end method

.method static synthetic a(LabQ;)LTd;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LabQ;->a:LTd;

    return-object v0
.end method

.method static synthetic a(LabQ;)LagZ;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LabQ;->a:LagZ;

    return-object v0
.end method

.method static synthetic a()LbmY;
    .locals 1

    .prologue
    .line 52
    sget-object v0, LabQ;->a:LbmY;

    return-object v0
.end method

.method private a(LQr;)LbmY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LQr;",
            ")",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    const-string v0, "linkSharingPublicDomains"

    const-string v1, "gmail.com, googlemail.com"

    invoke-interface {p1, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbmY;->a([Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method private a(LaGu;Lqt;Z)LbsU;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGu;",
            "Lqt;",
            "Z)",
            "LbsU",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    invoke-interface {p1}, LaGu;->i()Ljava/lang/String;

    move-result-object v3

    .line 171
    new-instance v2, LabU;

    invoke-interface {p1}, LaGu;->a()LaFM;

    move-result-object v0

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-direct {v2, p0, v0}, LabU;-><init>(LabQ;LaFO;)V

    .line 172
    iget-object v6, p0, LabQ;->a:LbsW;

    new-instance v0, LabT;

    move-object v1, p0

    move v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LabT;-><init>(LabQ;LabU;Ljava/lang/String;ZLqt;)V

    invoke-interface {v6, v0}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    .line 201
    new-instance v1, LabV;

    invoke-direct {v1, p0, p1}, LabV;-><init>(LabQ;LaGu;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LabQ;)Lcom/google/api/services/drive/Drive;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LabQ;->a:Lcom/google/api/services/drive/Drive;

    return-object v0
.end method

.method static synthetic a(LabQ;Lqt;Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, LabQ;->a(Lqt;Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    move-result-object v0

    return-object v0
.end method

.method private a(Lqt;Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 211
    new-instance v0, Lcom/google/api/services/drive/model/Permission;

    invoke-direct {v0}, Lcom/google/api/services/drive/model/Permission;-><init>()V

    .line 212
    invoke-direct {p0, v0, p1}, LabQ;->a(Lcom/google/api/services/drive/model/Permission;Lqt;)V

    .line 213
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/model/Permission;->a(Ljava/lang/Boolean;)Lcom/google/api/services/drive/model/Permission;

    .line 219
    if-eqz p2, :cond_1

    iget-boolean v1, p0, LabQ;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LabQ;->b:LbmY;

    invoke-virtual {v1, p2}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 220
    :cond_0
    const-string v1, "GlobalSharingApi"

    const-string v2, "Setting link sharing status to domain %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 221
    const-string v1, "domain"

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/model/Permission;->b(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 222
    invoke-virtual {v0, p2}, Lcom/google/api/services/drive/model/Permission;->c(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 233
    :goto_0
    return-object v0

    .line 224
    :cond_1
    const-string v1, "GlobalSharingApi"

    const-string v2, "Setting link sharing status to anyone"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 225
    const-string v1, "anyone"

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/model/Permission;->b(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 231
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/model/Permission;->c(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    goto :goto_0
.end method

.method private a(Lqt;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lqt;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    new-instance v1, Ljava/util/ArrayList;

    .line 243
    invoke-virtual {p1}, Lqt;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 244
    invoke-virtual {p1}, Lqt;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqq;

    .line 245
    invoke-virtual {v0}, Lqq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 247
    :cond_0
    return-object v1
.end method

.method static synthetic a(LabQ;)LqK;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LabQ;->a:LqK;

    return-object v0
.end method

.method static synthetic a(LabQ;Lcom/google/api/services/drive/model/Permission;Lqt;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, LabQ;->a(Lcom/google/api/services/drive/model/Permission;Lqt;)V

    return-void
.end method

.method private a(Lcom/google/api/services/drive/model/Permission;Lqt;)V
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p2}, Lqt;->a()Lqv;

    move-result-object v0

    invoke-virtual {v0}, Lqv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/api/services/drive/model/Permission;->a(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 238
    invoke-direct {p0, p2}, LabQ;->a(Lqt;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/api/services/drive/model/Permission;->a(Ljava/util/List;)Lcom/google/api/services/drive/model/Permission;

    .line 239
    return-void
.end method

.method static synthetic a(LabQ;LabU;Ljava/lang/String;)[Lcom/google/api/services/drive/model/Permission;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, LabQ;->a(LabU;Ljava/lang/String;)[Lcom/google/api/services/drive/model/Permission;

    move-result-object v0

    return-object v0
.end method

.method private a(LabU;Ljava/lang/String;)[Lcom/google/api/services/drive/model/Permission;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 252
    iget-object v0, p0, LabQ;->a:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive;->a()Lcom/google/api/services/drive/Drive$Permissions;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/api/services/drive/Drive$Permissions;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Permissions$List;

    move-result-object v0

    invoke-virtual {p1, v0}, LabU;->a(Lcom/google/api/services/drive/DriveRequest;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/PermissionList;

    .line 255
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/PermissionList;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Permission;

    .line 256
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/Permission;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lqv;->a(Ljava/lang/String;)Lqv;

    move-result-object v4

    .line 258
    const-string v5, "anyone"

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/Permission;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "domain"

    invoke-virtual {v0}, Lcom/google/api/services/drive/model/Permission;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move-object v2, v0

    .line 261
    :cond_1
    sget-object v5, Lqv;->a:Lqv;

    invoke-virtual {v5, v4}, Lqv;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_1
    move-object v1, v0

    .line 264
    goto :goto_0

    .line 265
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/api/services/drive/model/Permission;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(LaGu;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGu;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    sget-object v0, Lqt;->d:Lqt;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, LabQ;->a(LaGu;Lqt;Z)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGu;Ljava/lang/String;Lqt;Ljava/lang/String;)LbsU;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGu;",
            "Ljava/lang/String;",
            "Lqt;",
            "Ljava/lang/String;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v6, LabU;

    invoke-interface {p1}, LaGu;->a()LaFM;

    move-result-object v0

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-direct {v6, p0, v0}, LabU;-><init>(LabQ;LaFO;)V

    .line 139
    iget-object v7, p0, LabQ;->a:LbsW;

    new-instance v0, LabS;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LabS;-><init>(LabQ;Lqt;Ljava/lang/String;LaGu;Ljava/lang/String;LabU;)V

    invoke-interface {v7, v0}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    .line 156
    new-instance v1, LabV;

    invoke-direct {v1, p0, p1}, LabV;-><init>(LabQ;LaGu;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGu;Lqt;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGu;",
            "Lqt;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LabQ;->a(LaGu;Lqt;Z)LbsU;

    move-result-object v0

    return-object v0
.end method
