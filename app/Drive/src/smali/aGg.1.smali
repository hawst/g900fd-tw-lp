.class public interface abstract LaGg;
.super Ljava/lang/Object;
.source "DatabaseModelLoader.java"

# interfaces
.implements LaEi;
.implements LaGM;
.implements Laaj;
.implements LahO;


# virtual methods
.method public abstract a(LaFM;)I
.end method

.method public abstract a(LaFM;J)I
.end method

.method public abstract a()J
.end method

.method public abstract a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)J
.end method

.method public abstract a(LaFM;J)LaFV;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaFZ;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LaFZ;)LaGB;
.end method

.method public abstract a(LaFM;Ljava/lang/String;J)LaGX;
.end method

.method public abstract a(LaFM;Ljava/lang/String;)LaGa;
.end method

.method public abstract a(LaFM;Ljava/lang/String;Ljava/lang/String;LaHa;)LaGb;
.end method

.method public abstract a(LaGp;)LaGb;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGb;
.end method

.method public abstract a(LaFM;Ljava/lang/String;Ljava/lang/String;)LaGc;
.end method

.method public abstract a(LaFM;Ljava/lang/String;)LaGd;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;
.end method

.method public abstract a(LaGp;)LaGo;
.end method

.method public abstract a(LaGo;)LaGp;
.end method

.method public abstract a(LaGp;)LaGp;
.end method

.method public abstract a(Landroid/database/Cursor;)LaGp;
.end method

.method public abstract a(Ljava/lang/String;)LaGr;
.end method

.method public abstract a()Landroid/database/Cursor;
.end method

.method public abstract a(I)Landroid/database/Cursor;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;
.end method

.method public abstract a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LaGp;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LaFM;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            ")",
            "Ljava/util/List",
            "<",
            "LaGX;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LaGB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()V
.end method

.method public abstract a(LZS;)V
.end method

.method public abstract a(LaGp;)V
.end method

.method public abstract a(LaGp;LaGp;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGY;)V
.end method

.method public abstract a(LZS;)Z
.end method

.method public abstract a(LaFM;)Z
.end method

.method public abstract a(LaGo;LacY;)Z
.end method

.method public abstract a(LaGp;)Z
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGZ;)Z
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public abstract b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;
.end method

.method public abstract b()V
.end method

.method public abstract b(Ljava/lang/String;)Z
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method
