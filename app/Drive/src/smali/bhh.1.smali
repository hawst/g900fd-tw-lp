.class Lbhh;
.super Landroid/os/AsyncTask;
.source "ScanSession.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/bionics/scanner/unveil/util/Picture;",
        "Ljava/lang/Void;",
        "Lbhk;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private a:J

.field private a:LbgY;

.field final synthetic a:Lbhf;

.field private final a:Lbhi;

.field private final a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field private a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

.field private a:Lcom/google/bionics/scanner/unveil/util/Picture;

.field private a:Ljava/io/File;

.field private b:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field private b:Ljava/io/File;


# direct methods
.method public constructor <init>(Lbhf;LbgY;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V
    .locals 1

    .prologue
    .line 84
    iput-object p1, p0, Lbhh;->a:Lbhf;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 85
    iput-object p2, p0, Lbhh;->a:LbgY;

    .line 86
    invoke-virtual {p2}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v0

    iput-object v0, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 87
    iput-object p3, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 88
    iput-object p4, p0, Lbhh;->a:Lbhi;

    .line 89
    return-void
.end method

.method public constructor <init>(Lbhf;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lbhh;->a:Lbhf;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 79
    iput-object p2, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 80
    iput-object p3, p0, Lbhh;->a:Lbhi;

    .line 81
    return-void
.end method


# virtual methods
.method protected varargs a([Lcom/google/bionics/scanner/unveil/util/Picture;)Lbhk;
    .locals 9

    .prologue
    .line 93
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "RectifyAndStoreTask"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 95
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 97
    iget-object v2, p0, Lbhh;->a:LbgY;

    if-nez v2, :cond_0

    .line 98
    iput-wide v0, p0, Lbhh;->a:J

    .line 99
    iget-object v2, p0, Lbhh;->a:Lbhf;

    invoke-static {v2}, Lbhf;->a(Lbhf;)LbgZ;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v3}, Lcom/google/bionics/scanner/unveil/util/Picture;->getJpegData()[B

    move-result-object v3

    iget-object v4, p0, Lbhh;->a:Lbhf;

    .line 100
    invoke-static {v4}, Lbhf;->a(Lbhf;)LbgZ;

    move-result-object v4

    iget-wide v6, p0, Lbhh;->a:J

    invoke-virtual {v4, v6, v7}, LbgZ;->a(J)Ljava/lang/String;

    move-result-object v4

    .line 99
    invoke-virtual {v2, v3, v4}, LbgZ;->a([BLjava/lang/String;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lbhh;->a:Ljava/io/File;

    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 102
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {v4}, Lcom/google/bionics/scanner/unveil/util/Picture;->getOrientation()I

    move-result v4

    iput v4, p0, Lbhh;->a:I

    .line 103
    invoke-static {}, Lbhf;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v4

    const-string v5, "Time it took to save original JPEG: %d ms"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    :goto_0
    iget-object v0, p0, Lbhh;->a:LbgY;

    if-nez v0, :cond_1

    .line 111
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 112
    iget-object v2, p0, Lbhh;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/bionics/scanner/rectifier/QuadDetector;->extractQuadsInFile(Ljava/lang/String;I)Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v2

    iput-object v2, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 115
    invoke-static {}, Lbhf;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v4

    const-string v5, "Time to extract quadrilaterals: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    :goto_1
    invoke-static {}, Lbhf;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Image enhancement to apply: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v4}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 123
    iget-object v2, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    iget-object v3, p0, Lbhh;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    iget-object v5, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->rectifyDownsampledJpeg(Ljava/lang/String;ILcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)Lcom/google/bionics/scanner/rectifier/ImageData;

    move-result-object v2

    .line 125
    invoke-static {}, Lbhf;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v3

    const-string v4, "test result length: %d, size: %dx%d, first byte: %d"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->data:[B

    array-length v7, v7

    .line 126
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->width:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget v7, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->height:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->data:[B

    const/4 v8, 0x0

    aget-byte v7, v7, v8

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v5, v6

    .line 125
    invoke-virtual {v3, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    iget v3, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->width:I

    iget v4, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->height:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 129
    iget-object v4, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->data:[B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 130
    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->createBitmap(Landroid/graphics/Bitmap;I)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v3

    iput-object v3, p0, Lbhh;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    .line 131
    iget-object v3, p0, Lbhh;->a:Lbhf;

    invoke-static {v3}, Lbhf;->a(Lbhf;)LbgZ;

    move-result-object v3

    iget-object v4, p0, Lbhh;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    iget-object v5, p0, Lbhh;->a:Lbhf;

    .line 132
    invoke-static {v5}, Lbhf;->a(Lbhf;)LbgZ;

    move-result-object v5

    iget-wide v6, p0, Lbhh;->a:J

    invoke-virtual {v5, v6, v7}, LbgZ;->c(J)Ljava/lang/String;

    move-result-object v5

    .line 131
    invoke-virtual {v3, v4, v5}, LbgZ;->a(Lcom/google/bionics/scanner/unveil/util/Picture;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    iput-object v3, p0, Lbhh;->b:Ljava/io/File;

    .line 133
    iget-object v2, v2, Lcom/google/bionics/scanner/rectifier/ImageData;->enhancementMethod:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    iput-object v2, p0, Lbhh;->b:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 135
    invoke-static {}, Lbhf;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v4

    const-string v5, "Time to rectify and enhance downsampled JPEG: %d at factor %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x1

    const/4 v1, 0x4

    .line 136
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    .line 135
    invoke-virtual {v4, v5, v6}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    invoke-static {}, Lbhf;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Image enhancement applied: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbhh;->b:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v4}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lbhl; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 145
    sget-object v0, Lbhk;->a:Lbhk;

    :goto_2
    return-object v0

    .line 105
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbhh;->a:LbgY;

    invoke-virtual {v0}, LbgY;->a()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lbhh;->a:Ljava/io/File;

    .line 106
    iget-object v0, p0, Lbhh;->a:LbgY;

    invoke-virtual {v0}, LbgY;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lbhh;->a:J
    :try_end_1
    .catch Lbhl; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    sget-object v0, Lbhk;->b:Lbhk;

    goto :goto_2

    .line 117
    :cond_1
    :try_start_2
    iget-object v0, p0, Lbhh;->a:LbgY;

    invoke-virtual {v0}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v0

    iput-object v0, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;
    :try_end_2
    .catch Lbhl; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 140
    :catch_1
    move-exception v0

    .line 141
    sget-object v0, Lbhk;->c:Lbhk;

    goto :goto_2

    .line 142
    :catch_2
    move-exception v0

    .line 143
    sget-object v0, Lbhk;->d:Lbhk;

    goto :goto_2
.end method

.method protected a(Lbhk;)V
    .locals 4

    .prologue
    .line 150
    sget-object v0, Lbhk;->a:Lbhk;

    if-ne p1, v0, :cond_0

    .line 151
    iget-object v0, p0, Lbhh;->a:LbgY;

    if-nez v0, :cond_2

    .line 153
    new-instance v0, LbgY;

    invoke-direct {v0}, LbgY;-><init>()V

    iget-object v1, p0, Lbhh;->a:Ljava/io/File;

    .line 154
    invoke-virtual {v0, v1}, LbgY;->a(Ljava/io/File;)LbgY;

    move-result-object v0

    iget v1, p0, Lbhh;->a:I

    .line 155
    invoke-virtual {v0, v1}, LbgY;->a(I)LbgY;

    move-result-object v0

    iget-object v1, p0, Lbhh;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    iget-object v2, p0, Lbhh;->b:Ljava/io/File;

    .line 156
    invoke-virtual {v0, v1, v2}, LbgY;->a(Lcom/google/bionics/scanner/unveil/util/Picture;Ljava/io/File;)LbgY;

    move-result-object v0

    iget-wide v2, p0, Lbhh;->a:J

    .line 157
    invoke-virtual {v0, v2, v3}, LbgY;->a(J)LbgY;

    move-result-object v0

    iget-object v1, p0, Lbhh;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 158
    invoke-virtual {v0, v1}, LbgY;->b(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)LbgY;

    move-result-object v0

    iget-object v1, p0, Lbhh;->b:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 159
    invoke-virtual {v0, v1}, LbgY;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)LbgY;

    move-result-object v0

    iput-object v0, p0, Lbhh;->a:LbgY;

    .line 167
    :goto_0
    iget-object v0, p0, Lbhh;->a:Lbhf;

    invoke-static {v0}, Lbhf;->a(Lbhf;)LbgS;

    move-result-object v0

    iget-object v1, p0, Lbhh;->a:LbgY;

    invoke-virtual {v0, v1}, LbgS;->a(LbgY;)Ljava/util/concurrent/FutureTask;

    .line 169
    :cond_0
    iget-object v0, p0, Lbhh;->a:Lbhi;

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lbhh;->a:Lbhi;

    iget-object v1, p0, Lbhh;->a:LbgY;

    invoke-interface {v0, p1, v1}, Lbhi;->a(Lbhk;LbgY;)V

    .line 172
    :cond_1
    return-void

    .line 162
    :cond_2
    iget-object v0, p0, Lbhh;->a:LbgY;

    iget-object v1, p0, Lbhh;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    iget-object v2, p0, Lbhh;->b:Ljava/io/File;

    .line 163
    invoke-virtual {v0, v1, v2}, LbgY;->a(Lcom/google/bionics/scanner/unveil/util/Picture;Ljava/io/File;)LbgY;

    move-result-object v0

    iget-object v1, p0, Lbhh;->b:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 164
    invoke-virtual {v0, v1}, LbgY;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)LbgY;

    .line 165
    iget-object v0, p0, Lbhh;->a:Lbhf;

    invoke-static {v0}, Lbhf;->a(Lbhf;)LbgX;

    move-result-object v0

    iget-object v1, p0, Lbhh;->a:LbgY;

    invoke-virtual {v0, v1}, LbgX;->a(LbgY;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    check-cast p1, [Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {p0, p1}, Lbhh;->a([Lcom/google/bionics/scanner/unveil/util/Picture;)Lbhk;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 65
    check-cast p1, Lbhk;

    invoke-virtual {p0, p1}, Lbhh;->a(Lbhk;)V

    return-void
.end method
