.class public LsS;
.super Ljava/lang/Object;
.source "DocumentOpenerActivityDelegate.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGo;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Ljava/lang/String;LaGo;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iput-object p2, p0, LsS;->a:Ljava/lang/String;

    iput-object p3, p0, LsS;->a:LaGo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 313
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V

    .line 315
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LUi;

    iget-object v1, p0, LsS;->a:LaGo;

    invoke-interface {v1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 316
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LqK;

    move-result-object v0

    const-string v1, "documentOpener"

    const-string v2, "documentOpened"

    iget-object v3, p0, LsS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, LsS;->a:LaGo;

    invoke-interface {v0}, LaGo;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LamL;

    invoke-interface {v0}, LamL;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 322
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    sget-object v1, LFV;->e:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LFV;)V

    goto :goto_0

    .line 324
    :cond_1
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    sget-object v1, LFV;->c:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LFV;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 291
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LsS;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 294
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "Abort open document"

    invoke-static {v0, p1, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 295
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    .line 309
    :goto_0
    return-void

    .line 298
    :cond_0
    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    goto :goto_0

    .line 302
    :cond_1
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LqK;

    move-result-object v0

    const-string v1, "documentOpener"

    const-string v2, "documentOpeningError"

    iget-object v3, p0, LsS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, LsS;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    sget-object v1, LFV;->h:LFV;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
