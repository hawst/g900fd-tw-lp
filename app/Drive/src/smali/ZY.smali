.class public LZY;
.super Ljava/lang/Object;
.source "AppInstalledNotifier.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static final a:J


# instance fields
.field private final a:LQr;

.field private a:LZZ;

.field private final a:LaKM;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LZX;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    .line 50
    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sput-wide v0, LZY;->a:J

    .line 49
    return-void
.end method

.method constructor <init>(Landroid/content/Context;LaKM;LQr;LtK;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LZY;->a:Ljava/util/List;

    .line 65
    iput-object p1, p0, LZY;->a:Landroid/content/Context;

    .line 66
    iput-object p2, p0, LZY;->a:LaKM;

    .line 67
    iput-object p3, p0, LZY;->a:LQr;

    .line 68
    iput-object p4, p0, LZY;->a:LtK;

    .line 69
    return-void
.end method

.method private static a(Ljava/util/Set;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LuM;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 224
    sget-object v0, LuM;->b:LuM;

    sget-object v1, LuM;->c:LuM;

    invoke-static {v0, v1}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    invoke-virtual {v0, p0}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    sget v0, Lxi;->app_installed_toast_docs_and_sheets:I

    .line 231
    :goto_0
    return v0

    .line 226
    :cond_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 227
    invoke-static {p0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuM;

    .line 228
    invoke-virtual {v0}, LuM;->a()I

    move-result v0

    goto :goto_0

    .line 231
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 236
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 240
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LZX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0, p1}, LZY;->a(Ljava/util/List;)Z

    move-result v0

    .line 141
    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, LZY;->a:LZZ;

    .line 144
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 177
    iget-object v0, p0, LZY;->a:LZZ;

    if-nez v0, :cond_0

    move v0, v1

    .line 187
    :goto_0
    return v0

    .line 181
    :cond_0
    iget-object v0, p0, LZY;->a:LZZ;

    iget-object v0, v0, LZZ;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuM;

    .line 182
    iget-object v3, p0, LZY;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, LuM;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 183
    goto :goto_0

    .line 187
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 208
    invoke-static {p0, p1}, LZY;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 209
    const/4 v0, 0x1

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Set;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "LuM;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 214
    invoke-static {p1}, LZY;->a(Ljava/util/Set;)I

    move-result v0

    .line 215
    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 219
    :goto_0
    return v0

    .line 218
    :cond_0
    invoke-static {p0, v0}, LZY;->a(Landroid/content/Context;I)V

    .line 219
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LuM;)Z
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p1}, LuM;->a()I

    move-result v0

    invoke-static {p0, v0}, LZY;->a(Landroid/content/Context;I)V

    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method private a(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LZX;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 150
    invoke-direct {p0}, LZY;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 173
    :goto_0
    return v0

    .line 154
    :cond_0
    invoke-direct {p0}, LZY;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, LZY;->a:Landroid/content/Context;

    iget-object v1, p0, LZY;->a:LZZ;

    iget-object v1, v1, LZZ;->a:Ljava/util/Set;

    invoke-static {v0, v1}, LZY;->a(Landroid/content/Context;Ljava/util/Set;)Z

    move v0, v3

    .line 156
    goto :goto_0

    .line 159
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZX;

    .line 160
    iget-object v1, p0, LZY;->a:LZZ;

    iget-object v1, v1, LZZ;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 161
    iget-object v1, p0, LZY;->a:LZZ;

    iget-object v1, v1, LZZ;->a:Ljava/util/Set;

    .line 162
    invoke-static {v1}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LuM;

    iget-object v5, p0, LZY;->a:LZZ;

    iget-object v5, v5, LZZ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 161
    invoke-interface {v0, v1, v5}, LZX;->a(LuM;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    .line 164
    goto :goto_0

    .line 167
    :cond_3
    iget-object v1, p0, LZY;->a:LZZ;

    iget-object v1, v1, LZZ;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, LZX;->a(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    .line 168
    goto :goto_0

    :cond_4
    move v0, v2

    .line 173
    goto :goto_0
.end method

.method private b()Z
    .locals 6

    .prologue
    .line 191
    iget-object v0, p0, LZY;->a:LQr;

    const-string v1, "phoneskyAppInstalledNotificationExpireTime"

    sget-wide v2, LZY;->a:J

    long-to-int v2, v2

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 194
    iget-object v2, p0, LZY;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    iget-object v4, p0, LZY;->a:LZZ;

    iget-wide v4, v4, LZZ;->a:J

    sub-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, LZY;->a:LtK;

    sget-object v1, Lry;->ac:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized a(LZX;)V
    .locals 1

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LZY;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 120
    :goto_0
    monitor-exit p0

    return-void

    .line 118
    :cond_0
    :try_start_1
    iget-object v0, p0, LZY;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-static {p1}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    invoke-direct {p0, v0}, LZY;->a(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 4

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    sget-object v0, LuM;->a:LuM;

    .line 82
    invoke-virtual {v0}, LuM;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LuM;->a:LuM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :goto_0
    if-nez v0, :cond_4

    .line 93
    :goto_1
    monitor-exit p0

    return-void

    .line 82
    :cond_0
    :try_start_1
    sget-object v0, LuM;->b:LuM;

    .line 83
    invoke-virtual {v0}, LuM;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LuM;->b:LuM;

    goto :goto_0

    :cond_1
    sget-object v0, LuM;->c:LuM;

    .line 84
    invoke-virtual {v0}, LuM;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LuM;->c:LuM;

    goto :goto_0

    :cond_2
    sget-object v0, LuM;->d:LuM;

    .line 85
    invoke-virtual {v0}, LuM;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LuM;->d:LuM;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :cond_4
    new-instance v1, LZZ;

    .line 92
    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    iget-object v2, p0, LZY;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    invoke-direct {v1, v0, p2, v2, v3}, LZZ;-><init>(Ljava/util/Set;Lcom/google/android/gms/drive/database/data/EntrySpec;J)V

    iput-object v1, p0, LZY;->a:LZZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    if-nez p1, :cond_0

    .line 135
    :try_start_0
    iget-object v0, p0, LZY;->a:Ljava/util/List;

    invoke-direct {p0, v0}, LZY;->a(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_0
    monitor-exit p0

    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(LZX;)V
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LZY;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :goto_0
    monitor-exit p0

    return-void

    .line 127
    :cond_0
    :try_start_1
    iget-object v0, p0, LZY;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
