.class public final enum LbeX;
.super Ljava/lang/Enum;
.source "JsonToken.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbeX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbeX;

.field private static final synthetic a:[LbeX;

.field public static final enum b:LbeX;

.field public static final enum c:LbeX;

.field public static final enum d:LbeX;

.field public static final enum e:LbeX;

.field public static final enum f:LbeX;

.field public static final enum g:LbeX;

.field public static final enum h:LbeX;

.field public static final enum i:LbeX;

.field public static final enum j:LbeX;

.field public static final enum k:LbeX;

.field public static final enum l:LbeX;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, LbeX;

    const-string v1, "START_ARRAY"

    invoke-direct {v0, v1, v3}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->a:LbeX;

    .line 29
    new-instance v0, LbeX;

    const-string v1, "END_ARRAY"

    invoke-direct {v0, v1, v4}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->b:LbeX;

    .line 32
    new-instance v0, LbeX;

    const-string v1, "START_OBJECT"

    invoke-direct {v0, v1, v5}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->c:LbeX;

    .line 35
    new-instance v0, LbeX;

    const-string v1, "END_OBJECT"

    invoke-direct {v0, v1, v6}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->d:LbeX;

    .line 38
    new-instance v0, LbeX;

    const-string v1, "FIELD_NAME"

    invoke-direct {v0, v1, v7}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->e:LbeX;

    .line 41
    new-instance v0, LbeX;

    const-string v1, "VALUE_STRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->f:LbeX;

    .line 47
    new-instance v0, LbeX;

    const-string v1, "VALUE_NUMBER_INT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->g:LbeX;

    .line 50
    new-instance v0, LbeX;

    const-string v1, "VALUE_NUMBER_FLOAT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->h:LbeX;

    .line 53
    new-instance v0, LbeX;

    const-string v1, "VALUE_TRUE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->i:LbeX;

    .line 56
    new-instance v0, LbeX;

    const-string v1, "VALUE_FALSE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->j:LbeX;

    .line 59
    new-instance v0, LbeX;

    const-string v1, "VALUE_NULL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->k:LbeX;

    .line 62
    new-instance v0, LbeX;

    const-string v1, "NOT_AVAILABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LbeX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbeX;->l:LbeX;

    .line 23
    const/16 v0, 0xc

    new-array v0, v0, [LbeX;

    sget-object v1, LbeX;->a:LbeX;

    aput-object v1, v0, v3

    sget-object v1, LbeX;->b:LbeX;

    aput-object v1, v0, v4

    sget-object v1, LbeX;->c:LbeX;

    aput-object v1, v0, v5

    sget-object v1, LbeX;->d:LbeX;

    aput-object v1, v0, v6

    sget-object v1, LbeX;->e:LbeX;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LbeX;->f:LbeX;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LbeX;->g:LbeX;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LbeX;->h:LbeX;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LbeX;->i:LbeX;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LbeX;->j:LbeX;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LbeX;->k:LbeX;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LbeX;->l:LbeX;

    aput-object v2, v0, v1

    sput-object v0, LbeX;->a:[LbeX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbeX;
    .locals 1

    .prologue
    .line 23
    const-class v0, LbeX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbeX;

    return-object v0
.end method

.method public static values()[LbeX;
    .locals 1

    .prologue
    .line 23
    sget-object v0, LbeX;->a:[LbeX;

    invoke-virtual {v0}, [LbeX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbeX;

    return-object v0
.end method
