.class public LPG;
.super Lans;
.source "AvatarDownloader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lans",
        "<",
        "Ljava/net/URL;",
        "[B>;"
    }
.end annotation


# instance fields
.field private final a:LTF;

.field private final a:LZS;


# direct methods
.method constructor <init>(LTF;LQr;LaKM;)V
    .locals 3

    .prologue
    .line 35
    new-instance v0, LaoU;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LaoU;-><init>(I)V

    invoke-direct {p0, v0}, Lans;-><init>(Laop;)V

    .line 36
    iput-object p1, p0, LPG;->a:LTF;

    .line 38
    new-instance v0, LZN;

    const-string v1, "recentActivityAvatarRateBucketSize"

    const-string v2, "recentActivityAvatarRateBucketPeriod"

    invoke-direct {v0, p2, p3, v1, v2}, LZN;-><init>(LQr;LaKM;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LPG;->a:LZS;

    .line 41
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Ljava/net/URL;

    invoke-virtual {p0, p1}, LPG;->a(Ljava/net/URL;)[B

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/net/URL;)[B
    .locals 6

    .prologue
    .line 45
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 46
    iget-object v1, p0, LPG;->a:LZS;

    invoke-interface {v1}, LZS;->b()V

    .line 47
    iget-object v1, p0, LPG;->a:LTF;

    invoke-interface {v1, v0}, LTF;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 49
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 50
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v2

    .line 52
    const/16 v3, 0xc8

    if-eq v1, v3, :cond_0

    .line 53
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Downloading URL %s failed: server returned %d %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    .line 54
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x2

    aput-object v2, v4, v1

    .line 53
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :catchall_0
    move-exception v0

    iget-object v1, p0, LPG;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    throw v0

    .line 57
    :cond_0
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 59
    if-nez v0, :cond_1

    .line 60
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Downloading URL %s failed: result was empty."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 66
    iget-object v1, p0, LPG;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    return-object v0
.end method
