.class Laxv;
.super Ljava/lang/Object;
.source "SelectionHandles.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:F

.field final synthetic a:Laxs;

.field private a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

.field private b:F

.field private b:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;


# direct methods
.method private constructor <init>(Laxs;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Laxv;->a:Laxs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laxs;Laxt;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0, p1}, Laxv;-><init>(Laxs;)V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 156
    iget-object v0, p0, Laxv;->a:Laxs;

    invoke-static {v0}, Laxs;->a(Laxs;)Laxw;

    move-result-object v0

    invoke-virtual {v0}, Laxw;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/pdflib/Selection;

    .line 157
    iget-object v1, p0, Laxv;->a:Laxs;

    invoke-static {v1}, Laxs;->a(Laxs;)Landroid/widget/ImageView;

    move-result-object v1

    if-ne p1, v1, :cond_1

    move v1, v2

    .line 159
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_4

    .line 161
    if-eqz v1, :cond_2

    iget-object v3, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->stop:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    :goto_1
    iput-object v3, p0, Laxv;->a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 162
    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->start:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    :goto_2
    iput-object v0, p0, Laxv;->b:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 163
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Laxv;->a:F

    .line 164
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Laxv;->b:F

    .line 175
    :cond_0
    :goto_3
    return v2

    .line 157
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 161
    :cond_2
    iget-object v3, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->start:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    goto :goto_1

    .line 162
    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->stop:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    goto :goto_2

    .line 166
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 168
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v1, p0, Laxv;->a:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Laxv;->a:Laxs;

    invoke-static {v1}, Laxs;->a(Laxs;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 169
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v3, p0, Laxv;->b:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Laxv;->a:Laxs;

    invoke-static {v3}, Laxs;->a(Laxs;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v3

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 170
    iget-object v3, p0, Laxv;->a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    iget v3, v3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->x:I

    add-int/2addr v0, v3

    iget-object v3, p0, Laxv;->a:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    iget v3, v3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->y:I

    add-int/2addr v1, v3

    invoke-static {v0, v1}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->atPoint(II)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    move-result-object v0

    .line 172
    iget-object v1, p0, Laxv;->a:Laxs;

    invoke-static {v1}, Laxs;->a(Laxs;)Laxw;

    move-result-object v1

    iget-object v3, p0, Laxv;->b:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    invoke-virtual {v1, v3, v0}, Laxw;->a(Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V

    goto :goto_3
.end method
