.class public LIi;
.super Ljava/lang/Object;
.source "EntriesGrouperFactoryImpl.java"

# interfaces
.implements LIh;


# instance fields
.field private final a:LIe;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LIs;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LIG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Laja;LIe;Laja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LIs;",
            ">;",
            "LIe;",
            "Laja",
            "<",
            "LIG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, LIi;->a:Lbxw;

    .line 22
    iput-object p2, p0, LIi;->a:LIe;

    .line 23
    iput-object p3, p0, LIi;->b:Lbxw;

    .line 24
    return-void
.end method


# virtual methods
.method public a(LIK;)LIf;
    .locals 3

    .prologue
    .line 28
    sget-object v0, LIj;->a:[I

    invoke-virtual {p1}, LIK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown sortKind"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :pswitch_0
    iget-object v0, p0, LIi;->a:LIe;

    sget-object v1, LHX;->a:LHX;

    sget-object v2, LIg;->b:LIg;

    invoke-virtual {v0, v1, v2}, LIe;->a(LHX;LIg;)LIf;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 33
    :pswitch_1
    iget-object v0, p0, LIi;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIf;

    goto :goto_0

    .line 35
    :pswitch_2
    iget-object v0, p0, LIi;->a:LIe;

    sget-object v1, LHX;->b:LHX;

    sget-object v2, LIg;->b:LIg;

    invoke-virtual {v0, v1, v2}, LIe;->a(LHX;LIg;)LIf;

    move-result-object v0

    goto :goto_0

    .line 38
    :pswitch_3
    iget-object v0, p0, LIi;->a:LIe;

    sget-object v1, LHX;->c:LHX;

    sget-object v2, LIg;->b:LIg;

    invoke-virtual {v0, v1, v2}, LIe;->a(LHX;LIg;)LIf;

    move-result-object v0

    goto :goto_0

    .line 41
    :pswitch_4
    iget-object v0, p0, LIi;->a:LIe;

    sget-object v1, LHX;->e:LHX;

    sget-object v2, LIg;->b:LIg;

    invoke-virtual {v0, v1, v2}, LIe;->a(LHX;LIg;)LIf;

    move-result-object v0

    goto :goto_0

    .line 44
    :pswitch_5
    iget-object v0, p0, LIi;->b:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIf;

    goto :goto_0

    .line 46
    :pswitch_6
    iget-object v0, p0, LIi;->a:LIe;

    sget-object v1, LHX;->f:LHX;

    sget-object v2, LIg;->b:LIg;

    invoke-virtual {v0, v1, v2}, LIe;->a(LHX;LIg;)LIf;

    move-result-object v0

    goto :goto_0

    .line 49
    :pswitch_7
    iget-object v0, p0, LIi;->a:LIe;

    sget-object v1, LHX;->d:LHX;

    sget-object v2, LIg;->b:LIg;

    invoke-virtual {v0, v1, v2}, LIe;->a(LHX;LIg;)LIf;

    move-result-object v0

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
