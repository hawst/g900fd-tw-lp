.class LyM;
.super Ljava/lang/Object;
.source "ActivityEntryAdapter.java"


# instance fields
.field public a:Landroid/widget/ImageView;

.field public a:Landroid/widget/TextView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;)LyM;
    .locals 2

    .prologue
    .line 146
    new-instance v1, LyM;

    invoke-direct {v1}, LyM;-><init>()V

    .line 147
    sget v0, Lxc;->recent_event_avatar:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LyM;->a:Landroid/widget/ImageView;

    .line 148
    sget v0, Lxc;->recent_event_username:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LyM;->a:Landroid/widget/TextView;

    .line 149
    sget v0, Lxc;->recent_event_eventType:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LyM;->b:Landroid/widget/TextView;

    .line 150
    sget v0, Lxc;->recent_event_timestamp:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LyM;->c:Landroid/widget/TextView;

    .line 151
    return-object v1
.end method
