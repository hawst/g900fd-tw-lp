.class public abstract enum LaKN;
.super Ljava/lang/Enum;
.source "Clocks.java"

# interfaces
.implements LaKM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaKN;",
        ">;",
        "LaKM;"
    }
.end annotation


# static fields
.field public static final enum a:LaKN;

.field private static final synthetic a:[LaKN;

.field public static final enum b:LaKN;

.field public static final enum c:LaKN;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, LaKO;

    const-string v1, "WALL"

    invoke-direct {v0, v1, v2}, LaKO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaKN;->a:LaKN;

    .line 31
    new-instance v0, LaKP;

    const-string v1, "UPTIME"

    invoke-direct {v0, v1, v3}, LaKP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaKN;->b:LaKN;

    .line 40
    new-instance v0, LaKQ;

    const-string v1, "REALTIME"

    invoke-direct {v0, v1, v4}, LaKQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaKN;->c:LaKN;

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [LaKN;

    sget-object v1, LaKN;->a:LaKN;

    aput-object v1, v0, v2

    sget-object v1, LaKN;->b:LaKN;

    aput-object v1, v0, v3

    sget-object v1, LaKN;->c:LaKN;

    aput-object v1, v0, v4

    sput-object v0, LaKN;->a:[LaKN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILaKO;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, LaKN;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaKN;
    .locals 1

    .prologue
    .line 16
    const-class v0, LaKN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaKN;

    return-object v0
.end method

.method public static values()[LaKN;
    .locals 1

    .prologue
    .line 16
    sget-object v0, LaKN;->a:[LaKN;

    invoke-virtual {v0}, [LaKN;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaKN;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 49
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Clock[type=%s, time=%d]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, LaKN;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, LaKN;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
