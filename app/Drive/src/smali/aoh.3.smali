.class public Laoh;
.super Lanv;
.source "DocumentFileUriStoringFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanv",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Laon;",
        "LakD",
        "<",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Laon;",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Laoe;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lanv;-><init>(Laoo;)V

    .line 54
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Laoh;->a:Ljava/util/Map;

    .line 59
    return-void
.end method

.method static synthetic a(Laoh;Laon;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Laoh;->a(Laon;)V

    return-void
.end method

.method private declared-synchronized a(Laon;)V
    .locals 1

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laoh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected declared-synchronized a(Laon;)LakD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laon;",
            ")",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, Laoh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Laoh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakD;

    .line 78
    invoke-static {v0}, LakD;->a(LakD;)LakD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 80
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Laon;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized a(Laon;LakD;I)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laon;",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;I)",
            "LbmF",
            "<",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 94
    monitor-enter p0

    if-lez p3, :cond_0

    .line 96
    :try_start_0
    invoke-virtual {p2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Laoi;

    invoke-direct {v1, p0, p1, p2}, Laoi;-><init>(Laoh;Laon;LakD;)V

    .line 95
    invoke-static {v0, v1}, LakD;->a(Ljava/lang/Object;LakG;)LakD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 98
    :try_start_1
    iget-object v0, p0, Laoh;->a:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-static {v1, p3}, LakD;->a(LakD;I)LbmF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 101
    :try_start_2
    invoke-virtual {v1}, LakD;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 107
    :try_start_3
    invoke-virtual {p2}, LakD;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_0
    monitor-exit p0

    return-object v0

    .line 101
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, LakD;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 107
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-virtual {p2}, LakD;->close()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 94
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 104
    :cond_0
    :try_start_6
    invoke-static {}, LbmF;->c()LbmF;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v0

    .line 107
    :try_start_7
    invoke-virtual {p2}, LakD;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;I)LbmF;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Laon;

    check-cast p2, LakD;

    invoke-virtual {p0, p1, p2, p3}, Laoh;->a(Laon;LakD;I)LbmF;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Laon;

    invoke-virtual {p0, p1}, Laoh;->a(Laon;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1}, LakD;->close()V

    .line 114
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, Laoh;->a(LakD;)V

    return-void
.end method

.method protected declared-synchronized a(Laon;)Z
    .locals 1

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laoh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 27
    check-cast p1, Laon;

    invoke-virtual {p0, p1}, Laoh;->a(Laon;)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, Laoh;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;

    move-result-object v0

    return-object v0
.end method
