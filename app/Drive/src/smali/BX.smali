.class public LBX;
.super Ljava/lang/Object;
.source "DocListViewListArrangementModeDelegate.java"


# instance fields
.field private final a:LAH;

.field a:LBU;
    .annotation runtime LQP;
    .end annotation
.end field

.field private final a:LBa;

.field private final a:LII;

.field private final a:LKs;

.field private final a:LaGM;

.field private final a:LarF;

.field private final a:LtK;

.field private final a:LzZ;


# direct methods
.method public constructor <init>(LKs;LAH;LzZ;LaGM;LtK;LII;LBa;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAH;

    iput-object v0, p0, LBX;->a:LAH;

    .line 66
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LBX;->a:LaGM;

    .line 67
    iput-object p3, p0, LBX;->a:LzZ;

    .line 68
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LBX;->a:LtK;

    .line 69
    iput-object p1, p0, LBX;->a:LKs;

    .line 70
    iput-object p7, p0, LBX;->a:LBa;

    .line 72
    new-instance v0, LBY;

    invoke-direct {v0, p0}, LBY;-><init>(LBX;)V

    iput-object v0, p0, LBX;->a:LarF;

    .line 78
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LII;

    iput-object v0, p0, LBX;->a:LII;

    .line 79
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;)LBS;
    .locals 15

    .prologue
    .line 84
    new-instance v0, LBS;

    iget-object v2, p0, LBX;->a:LAH;

    iget-object v3, p0, LBX;->a:LKs;

    iget-object v4, p0, LBX;->a:LaGM;

    iget-object v5, p0, LBX;->a:LtK;

    iget-object v9, p0, LBX;->a:LarF;

    iget-object v10, p0, LBX;->a:LzZ;

    iget-object v11, p0, LBX;->a:LII;

    iget-object v12, p0, LBX;->a:LBU;

    iget-object v13, p0, LBX;->a:LBa;

    const/4 v14, 0x0

    move-object/from16 v1, p2

    move-object/from16 v6, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v0 .. v14}, LBS;-><init>(Landroid/support/v4/app/Fragment;LAH;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;LarF;LzZ;LII;LBU;LBa;LBT;)V

    return-object v0
.end method
