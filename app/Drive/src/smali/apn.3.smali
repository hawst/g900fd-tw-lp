.class public Lapn;
.super Ljava/lang/Object;
.source "DocListLoadingManager.java"

# interfaces
.implements LFE;
.implements LvV;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:LIK;

.field private final a:LQV;

.field private a:LQX;

.field private final a:LRa;

.field private final a:LaFO;

.field private final a:LaGO;

.field private a:Lan;

.field private a:Lapt;

.field private a:LbI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbI",
            "<",
            "LQX;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LcI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LcI",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            "LQX;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;

.field private final a:LvU;

.field private final a:LvY;

.field private a:LvZ;

.field private a:LzO;

.field private a:Z

.field private b:LIK;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lapu;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(LaFO;LvY;LvU;LQV;LaGO;LtK;LRa;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lapn;->a:Ljava/util/List;

    .line 126
    iput-boolean v1, p0, Lapn;->b:Z

    .line 133
    iput-boolean v1, p0, Lapn;->c:Z

    .line 136
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lapn;->b:Ljava/util/List;

    .line 79
    iput-object p1, p0, Lapn;->a:LaFO;

    .line 80
    iput-object p2, p0, Lapn;->a:LvY;

    .line 81
    iput-object p3, p0, Lapn;->a:LvU;

    .line 82
    iput-object p4, p0, Lapn;->a:LQV;

    .line 83
    iput-object p5, p0, Lapn;->a:LaGO;

    .line 84
    iput-object p6, p0, Lapn;->a:LtK;

    .line 85
    iput-object p7, p0, Lapn;->a:LRa;

    .line 86
    new-instance v0, Lapo;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Lapo;-><init>(Lapn;I)V

    iput-object v0, p0, Lapn;->a:LcI;

    .line 101
    return-void
.end method

.method static synthetic a(Lapn;)LIK;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LIK;

    return-object v0
.end method

.method static synthetic a(Lapn;LIK;)LIK;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lapn;->a:LIK;

    return-object p1
.end method

.method static synthetic a(Lapn;)LQV;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LQV;

    return-object v0
.end method

.method static synthetic a(Lapn;LQX;)LQX;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lapn;->a:LQX;

    return-object p1
.end method

.method static synthetic a(Lapn;)LRa;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LRa;

    return-object v0
.end method

.method static synthetic a(Lapn;)LaFO;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LaFO;

    return-object v0
.end method

.method static synthetic a(Lapn;)LaGO;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LaGO;

    return-object v0
.end method

.method static synthetic a(Lapn;)Lapt;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:Lapt;

    return-object v0
.end method

.method static synthetic a(Lapn;)LbI;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LbI;

    return-object v0
.end method

.method static synthetic a(Lapn;LbI;)LbI;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lapn;->a:LbI;

    return-object p1
.end method

.method static synthetic a(Lapn;)LcI;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LcI;

    return-object v0
.end method

.method static synthetic a(Lapn;)Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    return-object v0
.end method

.method static synthetic a(Lapn;)Ljava/util/List;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lapn;)LtK;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LtK;

    return-object v0
.end method

.method static synthetic a(Lapn;)LvY;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LvY;

    return-object v0
.end method

.method static synthetic a(Lapn;)LzO;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->a:LzO;

    return-object v0
.end method

.method private a(LIK;Z)V
    .locals 3

    .prologue
    .line 492
    iget-object v0, p0, Lapn;->a:LIK;

    invoke-virtual {p1, v0}, LIK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 493
    const/4 v0, 0x1

    iput-boolean v0, p0, Lapn;->b:Z

    .line 494
    iput-object p1, p0, Lapn;->a:LIK;

    .line 495
    iget-object v0, p0, Lapn;->a:LcI;

    invoke-virtual {v0}, LcI;->a()V

    .line 496
    invoke-direct {p0}, Lapn;->f()V

    .line 499
    :cond_0
    if-eqz p2, :cond_1

    .line 501
    iget-object v0, p0, Lapn;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 502
    iget-object v1, p0, Lapn;->a:LaFO;

    .line 504
    new-instance v2, Laps;

    invoke-direct {v2, p0, p1, v1, v0}, Laps;-><init>(Lapn;LIK;LaFO;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    .line 513
    invoke-virtual {v2, v0}, Laps;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 515
    :cond_1
    return-void
.end method

.method static synthetic a(Lapn;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lapn;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)V

    return-void
.end method

.method private a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 274
    iget-boolean v0, p0, Lapn;->c:Z

    iget-boolean v1, p0, Lapn;->b:Z

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lapn;->c:Z

    .line 275
    iput-boolean v6, p0, Lapn;->b:Z

    .line 356
    new-instance v1, Lapp;

    invoke-direct {v1, p0, p1, p2}, Lapp;-><init>(Lapn;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)V

    .line 357
    iget-object v0, p0, Lapn;->a:LcI;

    invoke-virtual {v0, p1}, LcI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQX;

    .line 358
    if-eqz v0, :cond_0

    .line 359
    const-string v2, "DocListLoader"

    const-string v3, "A cache hit! %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, LQX;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 360
    invoke-virtual {v0}, LQX;->a()V

    .line 361
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 362
    invoke-virtual {v1, v6, v2}, Lapp;->a(ILandroid/os/Bundle;)LbI;

    move-result-object v2

    .line 361
    invoke-virtual {v1, v2, v0}, Lapp;->a(LbI;LQX;)V

    .line 367
    :goto_0
    return-void

    .line 365
    :cond_0
    iget-object v0, p0, Lapn;->a:Lan;

    const/4 v2, 0x0

    invoke-virtual {v0, v6, v2, v1}, Lan;->a(ILandroid/os/Bundle;Lao;)LbI;

    goto :goto_0
.end method

.method private a(LzO;Z)V
    .locals 2

    .prologue
    .line 410
    invoke-static {}, LamV;->a()V

    .line 411
    iget-object v0, p0, Lapn;->a:LzO;

    invoke-virtual {p1, v0}, LzO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lapn;->b:Z

    .line 413
    iget-object v0, p0, Lapn;->a:LcI;

    invoke-virtual {v0}, LcI;->a()V

    .line 414
    iput-object p1, p0, Lapn;->a:LzO;

    .line 415
    invoke-direct {p0}, Lapn;->f()V

    .line 418
    :cond_0
    if-eqz p2, :cond_1

    .line 419
    new-instance v0, Lapr;

    invoke-direct {v0, p0, p1}, Lapr;-><init>(Lapn;LzO;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 425
    invoke-virtual {v0, v1}, Lapr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 427
    :cond_1
    return-void
.end method

.method static synthetic a(Lapn;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lapn;->c:Z

    return v0
.end method

.method static synthetic a(Lapn;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lapn;->c:Z

    return p1
.end method

.method static synthetic b(Lapn;)Ljava/util/List;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lapn;->b:Ljava/util/List;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lapn;->a:LaFO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lapn;->a:Z

    if-nez v0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v0, p0, Lapn;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lapn;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 379
    iget-object v0, p0, Lapn;->a:Lapt;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    invoke-static {}, LamV;->a()V

    .line 381
    iget-object v0, p0, Lapn;->a:LvU;

    invoke-interface {v0, p0}, LvU;->a(LvV;)V

    .line 382
    iget-object v0, p0, Lapn;->a:LvZ;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lapn;->a:LvY;

    iget-object v1, p0, Lapn;->a:LvZ;

    invoke-interface {v0, v1}, LvY;->a(LvZ;)V

    .line 387
    :cond_0
    iget-object v0, p0, Lapn;->a:LvY;

    invoke-interface {v0}, LvY;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    new-array v1, v2, [Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {p0, v2, v0, v1}, Lapn;->a(ZLcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;[Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V

    .line 389
    invoke-virtual {p0}, Lapn;->o()V

    .line 390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lapn;->a:Z

    .line 391
    invoke-direct {p0}, Lapn;->f()V

    .line 392
    return-void
.end method

.method public a(LIK;)V
    .locals 1

    .prologue
    .line 523
    invoke-static {}, LamV;->a()V

    .line 524
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lapn;->a(LIK;Z)V

    .line 525
    return-void
.end method

.method public a(Lapt;Lan;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 244
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    invoke-static {}, LamV;->a()V

    .line 246
    iget-object v0, p0, Lapn;->a:Lapt;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 247
    iput-boolean v1, p0, Lapn;->b:Z

    .line 248
    iput-object p1, p0, Lapn;->a:Lapt;

    .line 249
    iput-object p2, p0, Lapn;->a:Lan;

    .line 250
    if-eqz p3, :cond_0

    .line 251
    new-instance v0, Lapq;

    invoke-direct {v0, p0}, Lapq;-><init>(Lapn;)V

    iput-object v0, p0, Lapn;->a:LvZ;

    .line 264
    :cond_0
    return-void

    .line 246
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lapu;)V
    .locals 1

    .prologue
    .line 200
    invoke-static {}, LamV;->a()V

    .line 201
    iget-object v0, p0, Lapn;->a:LQX;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lapn;->c:Z

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lapn;->a:LQX;

    invoke-interface {p1, v0}, Lapu;->a(LQX;)V

    .line 206
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lapn;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(LzO;)V
    .locals 1

    .prologue
    .line 434
    invoke-static {}, LamV;->a()V

    .line 435
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lapn;->a(LzO;Z)V

    .line 436
    return-void
.end method

.method public varargs a(ZLcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;[Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V
    .locals 4

    .prologue
    .line 457
    invoke-static {}, LamV;->a()V

    .line 458
    if-nez p1, :cond_0

    iget-object v0, p0, Lapn;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-static {p2, v0}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    :goto_0
    return-void

    .line 461
    :cond_0
    iget-object v0, p0, Lapn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 462
    array-length v1, p3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v2, p3, v0

    .line 463
    iget-object v3, p0, Lapn;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 465
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lapn;->b:Z

    .line 466
    iput-object p2, p0, Lapn;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 470
    iget-object v0, p0, Lapn;->b:LIK;

    iput-object v0, p0, Lapn;->a:LIK;

    .line 471
    invoke-direct {p0}, Lapn;->f()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 375
    iget-boolean v0, p0, Lapn;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lapn;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 395
    invoke-static {}, LamV;->a()V

    .line 396
    const/4 v0, 0x0

    iput-boolean v0, p0, Lapn;->a:Z

    .line 397
    iget-object v0, p0, Lapn;->a:LcI;

    invoke-virtual {v0}, LcI;->a()V

    .line 398
    iget-object v0, p0, Lapn;->a:LvU;

    invoke-interface {v0, p0}, LvU;->b(LvV;)V

    .line 399
    iget-object v0, p0, Lapn;->a:LvZ;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lapn;->a:LvY;

    iget-object v1, p0, Lapn;->a:LvZ;

    invoke-interface {v0, v1}, LvY;->b(LvZ;)V

    .line 402
    :cond_0
    return-void
.end method

.method public b(LIK;)V
    .locals 2

    .prologue
    .line 531
    invoke-static {}, LamV;->a()V

    .line 532
    iput-object p1, p0, Lapn;->b:LIK;

    .line 533
    iget-object v0, p0, Lapn;->b:LIK;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lapn;->a(LIK;Z)V

    .line 534
    return-void
.end method

.method public b(Lapu;)V
    .locals 1

    .prologue
    .line 209
    invoke-static {}, LamV;->a()V

    .line 210
    iget-object v0, p0, Lapn;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    return-void
.end method

.method public b(LzO;)V
    .locals 1

    .prologue
    .line 442
    invoke-static {}, LamV;->a()V

    .line 443
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lapn;->a(LzO;Z)V

    .line 444
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 405
    invoke-static {}, LamV;->a()V

    .line 406
    iget-object v0, p0, Lapn;->a:Lan;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lan;->a(I)V

    .line 407
    return-void
.end method

.method public c(Lapu;)V
    .locals 1

    .prologue
    .line 218
    invoke-static {}, LamV;->a()V

    .line 219
    iget-object v0, p0, Lapn;->a:LQX;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lapn;->a:LQX;

    invoke-interface {p1, v0}, Lapu;->a(LQX;)V

    .line 224
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lapn;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lapn;->a:LcI;

    invoke-virtual {v0}, LcI;->a()V

    .line 448
    invoke-direct {p0}, Lapn;->f()V

    .line 449
    return-void
.end method

.method public d(Lapu;)V
    .locals 1

    .prologue
    .line 233
    invoke-static {}, LamV;->a()V

    .line 234
    iget-boolean v0, p0, Lapn;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lapn;->a:LQX;

    if-nez v0, :cond_1

    .line 235
    :cond_0
    iget-object v0, p0, Lapn;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    :cond_1
    iget-object v0, p0, Lapn;->a:LQX;

    if-eqz v0, :cond_2

    .line 238
    iget-object v0, p0, Lapn;->a:LQX;

    invoke-interface {p1, v0}, Lapu;->a(LQX;)V

    .line 240
    :cond_2
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 541
    invoke-static {}, LamV;->a()V

    .line 542
    iget-object v0, p0, Lapn;->a:LbI;

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lapn;->a:Lan;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lan;->a(I)V

    .line 544
    iput-object v2, p0, Lapn;->a:LbI;

    .line 546
    :cond_0
    iput-object v2, p0, Lapn;->a:Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 547
    iget-object v0, p0, Lapn;->a:Lapt;

    invoke-interface {v0}, Lapt;->a()V

    .line 548
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 483
    invoke-static {}, LamV;->a()V

    .line 484
    iget-object v0, p0, Lapn;->a:Lapt;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lapn;->a:LvU;

    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lapn;->a:LvU;

    .line 486
    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 487
    :goto_0
    iget-object v1, p0, Lapn;->a:Lapt;

    invoke-interface {v1, v0}, Lapt;->setSelectedEntrySpec(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 489
    :cond_0
    return-void

    .line 486
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()V
    .locals 1

    .prologue
    .line 476
    invoke-static {}, LamV;->a()V

    .line 477
    iget-object v0, p0, Lapn;->a:LcI;

    invoke-virtual {v0}, LcI;->a()V

    .line 478
    invoke-direct {p0}, Lapn;->f()V

    .line 479
    return-void
.end method
