.class Laii;
.super Ljava/lang/Object;
.source "PartialDownloadedFileStoreImpl.java"

# interfaces
.implements Laif;


# instance fields
.field private a:J

.field private a:Ladj;

.field private final a:Lalo;

.field private a:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ladj;Lalo;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    iput-object v0, p0, Laii;->a:Ladj;

    .line 52
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalo;

    iput-object v0, p0, Laii;->a:Lalo;

    .line 53
    return-void
.end method

.method static synthetic a(Laii;J)J
    .locals 1

    .prologue
    .line 44
    iput-wide p1, p0, Laii;->a:J

    return-wide p1
.end method

.method private a()Ljava/io/OutputStream;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 107
    iget-object v0, p0, Laii;->a:Ladj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 108
    iget-object v0, p0, Laii;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()Ljava/io/OutputStream;

    move-result-object v2

    .line 109
    new-instance v3, Laij;

    invoke-direct {v3, p0}, Laij;-><init>(Laii;)V

    .line 115
    new-instance v1, Lail;

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, Lail;-><init>(Ljava/io/OutputStream;LagH;JJ)V

    .line 116
    return-object v1

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Laii;->a:J

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Laii;->a:Ladj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 87
    iget-object v0, p0, Laii;->a:Ladj;

    invoke-interface {v0}, Ladj;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Laii;->a:Ladj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 75
    iget-object v0, p0, Laii;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()V

    .line 76
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Laii;->a:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 93
    invoke-direct {p0}, Laii;->a()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Laii;->a:Ljava/io/OutputStream;

    .line 98
    :cond_0
    iget-object v0, p0, Laii;->a:Lalo;

    iget-object v1, p0, Laii;->a:Ljava/io/OutputStream;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    .line 99
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Laii;->a:Ladj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 81
    iget-object v0, p0, Laii;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()Z

    move-result v0

    return v0

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Laii;->a:Ladj;

    if-nez v0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 129
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Laii;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    iget-object v0, p0, Laii;->a:Ladj;

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 62
    :cond_0
    :try_start_0
    iget-object v0, p0, Laii;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Laii;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :cond_1
    iget-object v0, p0, Laii;->a:Ladj;

    invoke-interface {v0}, Ladj;->close()V

    .line 67
    iput-object v2, p0, Laii;->a:Ljava/io/OutputStream;

    .line 68
    iput-object v2, p0, Laii;->a:Ladj;

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    iget-object v1, p0, Laii;->a:Ladj;

    invoke-interface {v1}, Ladj;->close()V

    .line 67
    iput-object v2, p0, Laii;->a:Ljava/io/OutputStream;

    .line 68
    iput-object v2, p0, Laii;->a:Ladj;

    throw v0
.end method
