.class public final LaeQ;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaeJ;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaeN;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaeH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LaeQ;->a:LbrA;

    .line 41
    const-class v0, LaeJ;

    invoke-static {v0, v1}, LaeQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeQ;->a:Lbsk;

    .line 44
    const-class v0, LaeN;

    invoke-static {v0, v1}, LaeQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeQ;->b:Lbsk;

    .line 47
    const-class v0, LaeH;

    invoke-static {v0, v1}, LaeQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaeQ;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 112
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :pswitch_1
    new-instance v2, LaeJ;

    iget-object v0, p0, LaeQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->i:Lbsk;

    .line 79
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaeQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laek;

    iget-object v1, v1, Laek;->i:Lbsk;

    .line 77
    invoke-static {v0, v1}, LaeQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxr;

    iget-object v1, p0, LaeQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 85
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaeQ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 83
    invoke-static {v1, v3}, LaeQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    invoke-direct {v2, v0, v1}, LaeJ;-><init>(Lbxr;LtK;)V

    move-object v0, v2

    .line 110
    :goto_0
    return-object v0

    .line 92
    :pswitch_2
    new-instance v1, LaeN;

    iget-object v0, p0, LaeQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->i:Lbsk;

    .line 95
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaeQ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->i:Lbsk;

    .line 93
    invoke-static {v0, v2}, LaeQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxr;

    invoke-direct {v1, v0}, LaeN;-><init>(Lbxr;)V

    move-object v0, v1

    .line 100
    goto :goto_0

    .line 102
    :pswitch_3
    new-instance v1, LaeH;

    iget-object v0, p0, LaeQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->i:Lbsk;

    .line 105
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaeQ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->i:Lbsk;

    .line 103
    invoke-static {v0, v2}, LaeQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxr;

    invoke-direct {v1, v0}, LaeH;-><init>(Lbxr;)V

    move-object v0, v1

    .line 110
    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 127
    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 57
    const-class v0, LaeJ;

    iget-object v1, p0, LaeQ;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 58
    const-class v0, LaeN;

    iget-object v1, p0, LaeQ;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, LaeH;

    iget-object v1, p0, LaeQ;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaeQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    iget-object v0, p0, LaeQ;->a:Lbsk;

    new-instance v1, LbrD;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 62
    iget-object v0, p0, LaeQ;->b:Lbsk;

    new-instance v1, LbrD;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 64
    iget-object v0, p0, LaeQ;->c:Lbsk;

    new-instance v1, LbrD;

    const/4 v2, 0x4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 66
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 119
    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
