.class public Laoc;
.super Lanv;
.source "DocumentFileStoringFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanv",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Laon;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LakR;


# direct methods
.method constructor <init>(Laom;Lago;LaGM;LakR;LZS;)V
    .locals 1
    .param p4    # LakR;
        .annotation runtime LaoC;
        .end annotation
    .end param
    .param p5    # LZS;
        .annotation runtime LaoD;
        .end annotation
    .end param

    .prologue
    .line 99
    invoke-static {p1, p2, p3, p5}, Laoc;->a(Laom;Lago;LaGM;LZS;)Laok;

    move-result-object v0

    invoke-direct {p0, v0}, Lanv;-><init>(Laoo;)V

    .line 102
    iput-object p4, p0, Laoc;->a:LakR;

    .line 103
    return-void
.end method

.method static synthetic a(Ljava/lang/Exception;)LaoK;
    .locals 1

    .prologue
    .line 38
    invoke-static {p0}, Laoc;->b(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    return-object v0
.end method

.method private static a(Laom;Lago;LaGM;LZS;)Laok;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Laod;

    invoke-direct {v0, p2, p1}, Laod;-><init>(LaGM;Lago;)V

    .line 89
    invoke-virtual {p0, v0, p3}, Laom;->a(Laoo;LZS;)Laok;

    move-result-object v0

    .line 90
    return-object v0
.end method

.method private a(Laon;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 112
    invoke-virtual {p1}, Laon;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 113
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "documentContent_%s_%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    .line 114
    invoke-virtual {p1}, Laon;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    .line 113
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 115
    return-object v0
.end method

.method private static b(Ljava/lang/Exception;)LaoK;
    .locals 2

    .prologue
    .line 153
    new-instance v0, LaoK;

    const-string v1, "Failed to fetch document content."

    invoke-direct {v0, v1, p0}, LaoK;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155
    return-object v0
.end method


# virtual methods
.method protected a(Laon;)LakD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laon;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0, p1}, Laoc;->a(Laon;)Ljava/lang/String;

    move-result-object v0

    .line 127
    iget-object v1, p0, Laoc;->a:LakR;

    invoke-virtual {p1}, Laon;->a()LaFO;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LakR;->a(LaFO;Ljava/lang/String;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Laon;

    move-result-object v0

    return-object v0
.end method

.method protected a(Laon;LakD;I)LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laon;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;I)",
            "LbmF",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 141
    :try_start_0
    invoke-direct {p0, p1}, Laoc;->a(Laon;)Ljava/lang/String;

    move-result-object v1

    .line 142
    iget-object v2, p0, Laoc;->a:LakR;

    .line 143
    invoke-virtual {p2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {p1}, Laon;->a()LaFO;

    move-result-object v3

    .line 142
    invoke-virtual {v2, v0, v3, v1}, LakR;->a(Ljava/io/File;LaFO;Ljava/lang/String;)LakD;

    move-result-object v0

    .line 144
    invoke-static {v0, p3}, LakD;->a(LakD;I)LbmF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 148
    invoke-virtual {p2}, LakD;->close()V

    return-object v0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    :try_start_1
    new-instance v1, LaoK;

    const-string v2, "Failed saving image to cache"

    invoke-direct {v1, v2, v0}, LaoK;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    :catchall_0
    move-exception v0

    invoke-virtual {p2}, LakD;->close()V

    throw v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;I)LbmF;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Laon;

    check-cast p2, LakD;

    invoke-virtual {p0, p1, p2, p3}, Laoc;->a(Laon;LakD;I)LbmF;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Laon;

    invoke-virtual {p0, p1}, Laoc;->a(Laon;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p1}, LakD;->close()V

    .line 161
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, Laoc;->a(LakD;)V

    return-void
.end method

.method protected a(Laon;)Z
    .locals 3

    .prologue
    .line 132
    invoke-direct {p0, p1}, Laoc;->a(Laon;)Ljava/lang/String;

    move-result-object v0

    .line 133
    iget-object v1, p0, Laoc;->a:LakR;

    invoke-virtual {p1}, Laon;->a()LaFO;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LakR;->a(LaFO;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 38
    check-cast p1, Laon;

    invoke-virtual {p0, p1}, Laoc;->a(Laon;)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, Laoc;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Laon;

    move-result-object v0

    return-object v0
.end method
