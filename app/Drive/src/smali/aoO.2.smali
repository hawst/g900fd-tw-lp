.class public LaoO;
.super LanC;
.source "ReusingImageDecompressFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LanC",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;",
        "LakD",
        "<",
        "Lcom/google/android/apps/docs/utils/RawPixelData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LaoY;

.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[B>;"
        }
    .end annotation
.end field

.field private final a:LtK;

.field private final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laoo;Laop;LtK;LaoY;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;",
            "Laop",
            "<-",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ">;",
            "LtK;",
            "LaoY;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p2, p1}, LanC;-><init>(Laop;Laoo;)V

    .line 85
    new-instance v0, LaoP;

    invoke-direct {v0, p0}, LaoP;-><init>(LaoO;)V

    iput-object v0, p0, LaoO;->a:Ljava/lang/ThreadLocal;

    .line 92
    new-instance v0, LaoQ;

    invoke-direct {v0, p0}, LaoQ;-><init>(LaoO;)V

    iput-object v0, p0, LaoO;->b:Ljava/lang/ThreadLocal;

    .line 113
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LaoO;->a:LtK;

    .line 114
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaoY;

    iput-object v0, p0, LaoO;->a:LaoY;

    .line 115
    return-void
.end method

.method private a(Ljava/io/InputStream;LaoS;)Landroid/graphics/Bitmap;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 169
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 172
    const/4 v0, 0x1

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 173
    iget-object v0, p0, LaoO;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 174
    iget-boolean v0, p2, LaoS;->a:Z

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, LaoO;->a:LaoY;

    iget-object v2, p2, LaoS;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-virtual {v0, v2}, LaoY;->a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 178
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 180
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private a(Ljava/io/InputStream;)LaoS;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 213
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 217
    const/4 v0, 0x1

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 218
    iget-object v0, p0, LaoO;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, v2, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 219
    const/4 v0, 0x0

    invoke-static {p1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 220
    new-instance v3, LaoS;

    new-instance v4, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v4, v0, v5}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    const-string v0, "image/gif"

    iget-object v2, v2, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    .line 222
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v3, v4, v0}, LaoS;-><init>(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v3

    .line 222
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private a(Ljava/io/InputStream;)Lbre;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 186
    :try_start_0
    iget-object v0, p0, LaoO;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move-object v2, v0

    move v0, v1

    .line 189
    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    .line 190
    array-length v0, v2

    sub-int/2addr v0, v1

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 191
    if-lez v0, :cond_0

    .line 192
    add-int/2addr v1, v0

    .line 193
    array-length v3, v2

    sub-int/2addr v3, v1

    if-gtz v3, :cond_0

    .line 194
    array-length v3, v2

    add-int/lit16 v3, v3, 0x4000

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lbsx;->a([BII)[B

    move-result-object v2

    goto :goto_0

    .line 198
    :cond_1
    iget-object v0, p0, LaoO;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 199
    new-instance v0, LaoR;

    invoke-direct {v0, v2, v1}, LaoR;-><init>([BI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;)LakD;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v1

    .line 122
    invoke-virtual {p2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 124
    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1, v2}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 126
    invoke-direct {p0, v0}, LaoO;->a(Ljava/io/InputStream;)Lbre;

    move-result-object v1

    .line 128
    sget-object v0, LamE;->a:LamE;

    .line 129
    invoke-virtual {v1}, Lbre;->a()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 131
    :try_start_1
    iget-object v3, p0, LaoO;->a:LtK;

    sget-object v4, Lry;->ag:Lry;

    invoke-interface {v3, v4}, LtK;->a(LtJ;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 132
    invoke-static {v2}, LalH;->a(Ljava/io/InputStream;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    sget-object v0, LamE;->b:LamE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 140
    invoke-virtual {v1}, Lbre;->a()Ljava/io/InputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 142
    :try_start_3
    invoke-direct {p0, v2}, LaoO;->a(Ljava/io/InputStream;)LaoS;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v3

    .line 144
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 147
    invoke-virtual {v1}, Lbre;->a()Ljava/io/InputStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v1

    .line 149
    :try_start_5
    invoke-direct {p0, v1, v3}, LaoO;->a(Ljava/io/InputStream;LaoS;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 150
    iget-object v4, p0, LaoO;->a:LaoY;

    .line 151
    invoke-virtual {v4, p1, v2, v0}, LaoY;->a(Lcom/google/android/apps/docs/utils/FetchSpec;Landroid/graphics/Bitmap;LamE;)LakD;

    move-result-object v0

    .line 153
    iget-boolean v3, v3, LaoS;->a:Z

    if-nez v3, :cond_1

    .line 154
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 158
    :cond_1
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 161
    invoke-virtual {p2}, LakD;->close()V

    return-object v0

    .line 136
    :catchall_0
    move-exception v0

    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 161
    :catchall_1
    move-exception v0

    invoke-virtual {p2}, LakD;->close()V

    throw v0

    .line 144
    :catchall_2
    move-exception v0

    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0

    .line 158
    :catchall_3
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    check-cast p2, LakD;

    invoke-virtual {p0, p1, p2}, LaoO;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected final a(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230
    invoke-virtual {p1}, LakD;->close()V

    .line 231
    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, LaoO;->b(LakD;)V

    return-void
.end method

.method protected final b(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p1}, LakD;->close()V

    .line 236
    return-void
.end method

.method protected synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, LaoO;->a(LakD;)V

    return-void
.end method
