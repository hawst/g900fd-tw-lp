.class public final LaLt;
.super LaLk;


# instance fields
.field private a:Landroid/support/v4/app/Fragment;


# direct methods
.method private constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 0

    invoke-direct {p0}, LaLk;-><init>()V

    iput-object p1, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    return-void
.end method

.method public static a(Landroid/support/v4/app/Fragment;)LaLt;
    .locals 1

    if-eqz p0, :cond_0

    new-instance v0, LaLt;

    invoke-direct {v0, p0}, LaLt;-><init>(Landroid/support/v4/app/Fragment;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->b()I

    move-result v0

    return v0
.end method

.method public a()LaLj;
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-static {v0}, LaLt;->a(Landroid/support/v4/app/Fragment;)LaLt;

    move-result-object v0

    return-object v0
.end method

.method public a()LaLm;
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaLm;)V
    .locals 2

    invoke-static {p1}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->f(Z)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->h()Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->c()I

    move-result v0

    return v0
.end method

.method public b()LaLj;
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-static {v0}, LaLt;->a(Landroid/support/v4/app/Fragment;)LaLt;

    move-result-object v0

    return-object v0
.end method

.method public b()LaLm;
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    return-object v0
.end method

.method public b(LaLm;)V
    .locals 2

    invoke-static {p1}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->b(Landroid/view/View;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->g(Z)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Z

    move-result v0

    return v0
.end method

.method public c()LaLm;
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->e(Z)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    return v0
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->h(Z)V

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->b()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->g()Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->d()Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->c()Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->e()Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, LaLt;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->f()Z

    move-result v0

    return v0
.end method
