.class public LaZD;
.super LaZJ;
.source "OwnersAvatarManager.java"


# static fields
.field private static a:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;LaCV;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LaZJ;-><init>(Landroid/content/Context;LaCV;Z)V

    .line 62
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 87
    sget-object v0, LaZD;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LaZW;->avatar_placeholder:I

    .line 88
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, LaZC;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, LaZD;->a:Landroid/graphics/Bitmap;

    .line 91
    :cond_0
    sget-object v0, LaZD;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic a(LaZD;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;LaZK;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 25
    invoke-super/range {p0 .. p5}, LaZJ;->a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;LaZK;Landroid/graphics/Bitmap;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 83
    invoke-static {p1}, LaZD;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 71
    new-instance v0, LaZE;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LaZE;-><init>(LaZD;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    invoke-virtual {p0}, LaZD;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p2, p3, p4}, LaZD;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 71
    invoke-virtual {p0, v0, v1}, LaZD;->a(LaZK;Landroid/graphics/Bitmap;)V

    .line 73
    return-void
.end method
