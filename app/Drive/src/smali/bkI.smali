.class final LbkI;
.super LbkJ;
.source "LocalCache.java"

# interfaces
.implements Lbkr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LbkJ",
        "<TK;TV;>;",
        "Lbkr",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field a:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile b:J

.field b:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILbkr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 1482
    invoke-direct {p0, p1, p2, p3, p4}, LbkJ;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILbkr;)V

    .line 1487
    iput-wide v2, p0, LbkI;->a:J

    .line 1499
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkI;->a:Lbkr;

    .line 1512
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkI;->b:Lbkr;

    .line 1527
    iput-wide v2, p0, LbkI;->b:J

    .line 1539
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkI;->c:Lbkr;

    .line 1552
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    iput-object v0, p0, LbkI;->d:Lbkr;

    .line 1483
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1491
    iget-wide v0, p0, LbkI;->a:J

    return-wide v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1496
    iput-wide p1, p0, LbkI;->a:J

    .line 1497
    return-void
.end method

.method public a(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1509
    iput-object p1, p0, LbkI;->a:Lbkr;

    .line 1510
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1531
    iget-wide v0, p0, LbkI;->b:J

    return-wide v0
.end method

.method public b()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1504
    iget-object v0, p0, LbkI;->a:Lbkr;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1536
    iput-wide p1, p0, LbkI;->b:J

    .line 1537
    return-void
.end method

.method public b(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1522
    iput-object p1, p0, LbkI;->b:Lbkr;

    .line 1523
    return-void
.end method

.method public c()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1517
    iget-object v0, p0, LbkI;->b:Lbkr;

    return-object v0
.end method

.method public c(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1549
    iput-object p1, p0, LbkI;->c:Lbkr;

    .line 1550
    return-void
.end method

.method public d()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1544
    iget-object v0, p0, LbkI;->c:Lbkr;

    return-object v0
.end method

.method public d(Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1562
    iput-object p1, p0, LbkI;->d:Lbkr;

    .line 1563
    return-void
.end method

.method public e()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1557
    iget-object v0, p0, LbkI;->d:Lbkr;

    return-object v0
.end method
