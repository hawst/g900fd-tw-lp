.class public final enum LDr;
.super Ljava/lang/Enum;
.source "SelectionViewState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LDr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LDr;

.field private static final synthetic a:[LDr;

.field public static final enum b:LDr;

.field public static final enum c:LDr;

.field public static final enum d:LDr;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, LDr;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v2}, LDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LDr;->a:LDr;

    .line 50
    new-instance v0, LDr;

    const-string v1, "SELECTED"

    invoke-direct {v0, v1, v3}, LDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LDr;->b:LDr;

    .line 51
    new-instance v0, LDr;

    const-string v1, "NOT_SELECTED"

    invoke-direct {v0, v1, v4}, LDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LDr;->c:LDr;

    .line 52
    new-instance v0, LDr;

    const-string v1, "NOT_SELECTABLE"

    invoke-direct {v0, v1, v5}, LDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LDr;->d:LDr;

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [LDr;

    sget-object v1, LDr;->a:LDr;

    aput-object v1, v0, v2

    sget-object v1, LDr;->b:LDr;

    aput-object v1, v0, v3

    sget-object v1, LDr;->c:LDr;

    aput-object v1, v0, v4

    sget-object v1, LDr;->d:LDr;

    aput-object v1, v0, v5

    sput-object v0, LDr;->a:[LDr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(LJR;Lcom/google/android/apps/docs/doclist/SelectionItem;)LDr;
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p0, p1}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LKb;

    move-result-object v2

    .line 56
    if-nez v2, :cond_0

    const/4 v0, 0x0

    move v1, v0

    .line 57
    :goto_0
    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 58
    :goto_1
    invoke-virtual {p0}, LJR;->b()Z

    move-result v2

    .line 60
    if-nez v2, :cond_2

    .line 61
    sget-object v0, LDr;->a:LDr;

    .line 67
    :goto_2
    return-object v0

    .line 56
    :cond_0
    invoke-virtual {v2}, LKb;->a()Z

    move-result v0

    move v1, v0

    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v2}, LKb;->b()Z

    move-result v0

    goto :goto_1

    .line 62
    :cond_2
    if-eqz v1, :cond_3

    .line 63
    sget-object v0, LDr;->b:LDr;

    goto :goto_2

    .line 64
    :cond_3
    if-eqz v0, :cond_4

    .line 65
    sget-object v0, LDr;->c:LDr;

    goto :goto_2

    .line 67
    :cond_4
    sget-object v0, LDr;->d:LDr;

    goto :goto_2
.end method

.method public static valueOf(Ljava/lang/String;)LDr;
    .locals 1

    .prologue
    .line 48
    const-class v0, LDr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LDr;

    return-object v0
.end method

.method public static values()[LDr;
    .locals 1

    .prologue
    .line 48
    sget-object v0, LDr;->a:[LDr;

    invoke-virtual {v0}, [LDr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LDr;

    return-object v0
.end method
