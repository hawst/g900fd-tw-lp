.class public LaMI;
.super Ljava/lang/Object;

# interfaces
.implements LaMG;


# instance fields
.field private final a:Lcom/google/android/gms/internal/fc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fa;)V
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/gms/internal/aq;

    invoke-direct {v1}, Lcom/google/android/gms/internal/aq;-><init>()V

    const/4 v4, 0x0

    move-object v0, p1

    move v3, v2

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/fc;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;ZZLaSP;Lcom/google/android/gms/internal/fa;)Lcom/google/android/gms/internal/fc;

    move-result-object v0

    iput-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    invoke-static {v0}, LaPS;->a(Landroid/webkit/WebView;)V

    return-void
.end method

.method public a(LaMH;)V
    .locals 2

    iget-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    new-instance v1, LaMJ;

    invoke-direct {v1, p0, p1}, LaMJ;-><init>(LaMI;LaMH;)V

    invoke-virtual {v0, v1}, LaQu;->a(LaQw;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/fc;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;LaNu;)V
    .locals 1

    iget-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LaQu;->a(Ljava/lang/String;LaNu;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    iget-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/fc;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    invoke-static {v0}, LaPS;->b(Landroid/webkit/WebView;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaMI;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LaQu;->a(Ljava/lang/String;LaNu;)V

    return-void
.end method
