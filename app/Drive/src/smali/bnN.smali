.class public final LbnN;
.super Lbmo;
.source "MapMaker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbmo",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field a:J

.field a:LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field a:LbjA;

.field a:LbnQ;

.field a:LboA;

.field a:Z

.field b:I

.field b:J

.field b:LboA;

.field c:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 139
    invoke-direct {p0}, Lbmo;-><init>()V

    .line 119
    iput v0, p0, LbnN;->a:I

    .line 120
    iput v0, p0, LbnN;->b:I

    .line 121
    iput v0, p0, LbnN;->c:I

    .line 126
    iput-wide v2, p0, LbnN;->a:J

    .line 127
    iput-wide v2, p0, LbnN;->b:J

    .line 139
    return-void
.end method


# virtual methods
.method a()I
    .locals 2

    .prologue
    .line 182
    iget v0, p0, LbnN;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LbnN;->a:I

    goto :goto_0
.end method

.method a()J
    .locals 4

    .prologue
    .line 398
    iget-wide v0, p0, LbnN;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LbnN;->a:J

    goto :goto_0
.end method

.method a()LbiD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, LbnN;->a:LbiD;

    invoke-virtual {p0}, LbnN;->a()LboA;

    move-result-object v1

    invoke-virtual {v1}, LboA;->a()LbiD;

    move-result-object v1

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiD;

    return-object v0
.end method

.method a()LbjA;
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, LbnN;->a:LbjA;

    invoke-static {}, LbjA;->a()LbjA;

    move-result-object v1

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbjA;

    return-object v0
.end method

.method a()LboA;
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, LbnN;->a:LboA;

    sget-object v1, LboA;->a:LboA;

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LboA;

    return-object v0
.end method

.method public a()Ljava/util/concurrent/ConcurrentMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 506
    iget-boolean v0, p0, LbnN;->a:Z

    if-nez v0, :cond_0

    .line 507
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, LbnN;->a()I

    move-result v1

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-virtual {p0}, LbnN;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 509
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LbnN;->a:LbnQ;

    if-nez v0, :cond_1

    new-instance v0, LbnY;

    invoke-direct {v0, p0}, LbnY;-><init>(LbnN;)V

    :goto_1
    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    goto :goto_0

    :cond_1
    new-instance v0, LbnP;

    invoke-direct {v0, p0}, LbnP;-><init>(LbnN;)V

    goto :goto_1
.end method

.method b()I
    .locals 2

    .prologue
    .line 253
    iget v0, p0, LbnN;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LbnN;->b:I

    goto :goto_0
.end method

.method b()J
    .locals 4

    .prologue
    .line 441
    iget-wide v0, p0, LbnN;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LbnN;->b:J

    goto :goto_0
.end method

.method b()LboA;
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, LbnN;->b:LboA;

    sget-object v1, LboA;->a:LboA;

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LboA;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, -0x1

    .line 597
    invoke-static {p0}, LbiL;->a(Ljava/lang/Object;)LbiN;

    move-result-object v0

    .line 598
    iget v1, p0, LbnN;->a:I

    if-eq v1, v3, :cond_0

    .line 599
    const-string v1, "initialCapacity"

    iget v2, p0, LbnN;->a:I

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    .line 601
    :cond_0
    iget v1, p0, LbnN;->b:I

    if-eq v1, v3, :cond_1

    .line 602
    const-string v1, "concurrencyLevel"

    iget v2, p0, LbnN;->b:I

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    .line 604
    :cond_1
    iget v1, p0, LbnN;->c:I

    if-eq v1, v3, :cond_2

    .line 605
    const-string v1, "maximumSize"

    iget v2, p0, LbnN;->c:I

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    .line 607
    :cond_2
    iget-wide v2, p0, LbnN;->a:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 608
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LbnN;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 610
    :cond_3
    iget-wide v2, p0, LbnN;->b:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 611
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LbnN;->b:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 613
    :cond_4
    iget-object v1, p0, LbnN;->a:LboA;

    if-eqz v1, :cond_5

    .line 614
    const-string v1, "keyStrength"

    iget-object v2, p0, LbnN;->a:LboA;

    invoke-virtual {v2}, LboA;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbig;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 616
    :cond_5
    iget-object v1, p0, LbnN;->b:LboA;

    if-eqz v1, :cond_6

    .line 617
    const-string v1, "valueStrength"

    iget-object v2, p0, LbnN;->b:LboA;

    invoke-virtual {v2}, LboA;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbig;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 619
    :cond_6
    iget-object v1, p0, LbnN;->a:LbiD;

    if-eqz v1, :cond_7

    .line 620
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, LbiN;->a(Ljava/lang/Object;)LbiN;

    .line 622
    :cond_7
    iget-object v1, p0, LbnN;->a:LbnW;

    if-eqz v1, :cond_8

    .line 623
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, LbiN;->a(Ljava/lang/Object;)LbiN;

    .line 625
    :cond_8
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
