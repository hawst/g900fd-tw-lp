.class public final LavK;
.super Ljava/lang/Object;
.source "Selection.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/viewer/pdflib/Selection;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/pdflib/Selection;
    .locals 6

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/viewer/pdflib/Selection;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const-class v2, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 54
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    const-class v3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 55
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    const-class v4, Landroid/graphics/Rect;

    .line 56
    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v4

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/viewer/pdflib/Selection;-><init>(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/viewer/pdflib/Selection;
    .locals 1

    .prologue
    .line 62
    new-array v0, p1, [Lcom/google/android/apps/viewer/pdflib/Selection;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0, p1}, LavK;->a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/pdflib/Selection;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0, p1}, LavK;->a(I)[Lcom/google/android/apps/viewer/pdflib/Selection;

    move-result-object v0

    return-object v0
.end method
