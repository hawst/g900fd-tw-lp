.class public LJD;
.super Ljava/lang/Object;
.source "HelpCardModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 37
    const-class v0, LJs;

    const-string v1, "appFlowHelpCardSupplier"

    .line 38
    invoke-static {v1}, Lbwo;->a(Ljava/lang/String;)Lbwm;

    move-result-object v1

    .line 37
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 39
    new-instance v0, LJE;

    invoke-direct {v0, p0}, LJE;-><init>(LJD;)V

    const-string v1, "appHelpCardSupplier"

    .line 40
    invoke-static {v1}, Lbwo;->a(Ljava/lang/String;)Lbwm;

    move-result-object v1

    .line 39
    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 41
    return-void
.end method

.method provideCentralHelpCardsSupplier(LJg;)LJf;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 24
    return-object p1
.end method

.method provideDocListVisitCountSupplier(LJi;)Lbjv;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "docListActivityVisitCountSupplier"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LJi;",
            ")",
            "Lbjv",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    return-object p1
.end method
