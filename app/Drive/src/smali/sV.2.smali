.class public LsV;
.super LaGN;
.source "DocumentOpenerActivityDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LaGo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)LaGo;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p0, p1}, LsV;->a(LaGM;)LaGo;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 426
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LDL;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 427
    :cond_0
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V

    .line 428
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LDL;)LDL;

    .line 429
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    .line 446
    :goto_0
    return-void

    .line 433
    :cond_1
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LDL;

    move-result-object v0

    invoke-interface {v0}, LDL;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 434
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 435
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    .line 436
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a()LM;

    move-result-object v0

    iget-object v1, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LDL;

    move-result-object v1

    .line 435
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a(LM;LDL;LaGo;)V

    .line 445
    :cond_2
    :goto_1
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LDL;)LDL;

    goto :goto_0

    .line 440
    :cond_3
    const/4 v0, 0x0

    .line 442
    :cond_4
    iget-object v1, p0, LsV;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LDL;

    move-result-object v1

    invoke-interface {v1, v0}, LDL;->a(I)I

    move-result v0

    .line 443
    if-gez v0, :cond_4

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 418
    check-cast p1, LaGo;

    invoke-virtual {p0, p1}, LsV;->a(LaGo;)V

    return-void
.end method
