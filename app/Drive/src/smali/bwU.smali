.class public abstract LbwU;
.super Ljava/lang/Object;
.source "GDataServiceClient.java"


# static fields
.field protected static a:Ljava/lang/String;


# instance fields
.field private final a:LbwS;

.field private final a:LbwT;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "2.0"

    sput-object v0, LbwU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LbwS;LbwT;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, LbwU;->a:LbwS;

    .line 36
    iput-object p2, p0, LbwU;->a:LbwT;

    .line 37
    return-void
.end method

.method private a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxb;
    .locals 2

    .prologue
    .line 355
    const/4 v1, 0x0

    .line 357
    :try_start_0
    iget-object v0, p0, LbwU;->a:LbwT;

    invoke-interface {v0, p1, p2}, LbwT;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxj;

    move-result-object v1

    .line 358
    invoke-interface {v1}, Lbxj;->b()Lbxb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 360
    if-eqz v1, :cond_0

    .line 361
    invoke-interface {v1}, Lbxj;->a()V

    :cond_0
    return-object v0

    .line 360
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 361
    invoke-interface {v1}, Lbxj;->a()V

    :cond_1
    throw v0
.end method


# virtual methods
.method protected a()LbwS;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LbwU;->a:LbwS;

    return-object v0
.end method

.method public a(Lbxb;Ljava/lang/String;)Lbxb;
    .locals 6

    .prologue
    .line 253
    invoke-virtual {p1}, Lbxb;->o()Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    new-instance v0, Lbxk;

    const-string v1, "No edit URI -- cannot update."

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :cond_0
    iget-object v0, p0, LbwU;->a:LbwT;

    invoke-interface {v0, p1}, LbwT;->a(Lbxb;)Lbxs;

    move-result-object v5

    .line 260
    :try_start_0
    iget-object v0, p0, LbwU;->a:LbwS;

    invoke-virtual {p1}, Lbxb;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, LbwU;->a()Ljava/lang/String;

    move-result-object v4

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, LbwS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxs;)Ljava/io/InputStream;

    move-result-object v0

    .line 263
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {p0, v2, v0}, LbwU;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxb;
    :try_end_0
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not update entry "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, v0}, LbwU;->a(Ljava/lang/Class;Ljava/lang/String;LbwW;)V

    .line 266
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbxb;
    .locals 3

    .prologue
    .line 219
    :try_start_0
    invoke-virtual {p0}, LbwU;->a()LbwS;

    move-result-object v0

    invoke-virtual {p0}, LbwU;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, p3, p4, v1}, LbwS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 220
    invoke-direct {p0, p1, v0}, LbwU;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxb;
    :try_end_0
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 223
    :goto_0
    return-object v0

    .line 221
    :catch_0
    move-exception v0

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not fetch entry "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LbwU;->b(Ljava/lang/String;LbwW;)V

    .line 223
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lbxb;)Lbxb;
    .locals 4

    .prologue
    .line 178
    iget-object v0, p0, LbwU;->a:LbwT;

    invoke-interface {v0, p3}, LbwT;->a(Lbxb;)Lbxs;

    move-result-object v0

    .line 180
    :try_start_0
    iget-object v1, p0, LbwU;->a:LbwS;

    invoke-virtual {p0}, LbwU;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2, v0}, LbwS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxs;)Ljava/io/InputStream;

    move-result-object v0

    .line 182
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v1, v0}, LbwU;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxb;
    :try_end_0
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 191
    :goto_0
    return-object v0

    .line 183
    :catch_0
    move-exception v0

    .line 185
    :try_start_1
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create entry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v0}, LbwU;->a(Ljava/lang/Class;Ljava/lang/String;LbwW;)V
    :try_end_1
    .catch LbwZ; {:try_start_1 .. :try_end_1} :catch_1

    .line 191
    const/4 v0, 0x0

    goto :goto_0

    .line 187
    :catch_1
    move-exception v1

    .line 189
    throw v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbxj;
    .locals 3

    .prologue
    .line 112
    :try_start_0
    iget-object v0, p0, LbwU;->a:LbwS;

    invoke-virtual {p0}, LbwU;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, p3, p4, v1}, LbwS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 113
    iget-object v1, p0, LbwU;->a:LbwT;

    invoke-interface {v1, p1, v0}, LbwT;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxj;
    :try_end_0
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not fetch feed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LbwU;->a(Ljava/lang/String;LbwW;)V

    .line 116
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Enumeration;)Lbxj;
    .locals 3

    .prologue
    .line 385
    iget-object v0, p0, LbwU;->a:LbwT;

    invoke-interface {v0, p4}, LbwT;->a(Ljava/util/Enumeration;)Lbxs;

    move-result-object v0

    .line 387
    :try_start_0
    iget-object v1, p0, LbwU;->a:LbwS;

    invoke-virtual {p0}, LbwU;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p2, p3, v2, v0}, LbwS;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxs;)Ljava/io/InputStream;

    move-result-object v0

    .line 389
    iget-object v1, p0, LbwU;->a:LbwT;

    invoke-interface {v1, p1, v0}, LbwT;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxj;
    :try_end_0
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 392
    :goto_0
    return-object v0

    .line 390
    :catch_0
    move-exception v0

    .line 391
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not submit batch "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LbwU;->c(Ljava/lang/String;LbwW;)V

    .line 392
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method protected a(Ljava/lang/Class;Ljava/lang/String;LbwW;)V
    .locals 4

    .prologue
    .line 453
    invoke-virtual {p3}, LbwW;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 474
    :pswitch_0
    new-instance v0, LbwW;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, LbwW;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, LbwW;->a()I

    move-result v2

    invoke-virtual {p3}, LbwW;->a()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LbwW;-><init>(Ljava/lang/String;ILjava/io/InputStream;)V

    throw v0

    .line 455
    :pswitch_1
    const/4 v0, 0x0

    .line 456
    if-eqz p1, :cond_0

    .line 457
    invoke-virtual {p3}, LbwW;->a()Ljava/io/InputStream;

    move-result-object v1

    .line 458
    if-eqz v1, :cond_0

    .line 459
    invoke-virtual {p3}, LbwW;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LbwU;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxb;

    move-result-object v0

    .line 462
    :cond_0
    new-instance v1, LbwM;

    invoke-direct {v1, v0}, LbwM;-><init>(Lbxb;)V

    throw v1

    .line 464
    :pswitch_2
    new-instance v0, LbwP;

    invoke-direct {v0, p2, p3}, LbwP;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 466
    :pswitch_3
    new-instance v0, LbwR;

    invoke-direct {v0, p2, p3}, LbwR;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 468
    :pswitch_4
    new-instance v0, LbwO;

    invoke-direct {v0, p2, p3}, LbwO;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 470
    :pswitch_5
    new-instance v0, LbwX;

    invoke-direct {v0, p2, p3}, LbwX;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 472
    :pswitch_6
    new-instance v0, LbwZ;

    invoke-direct {v0, p2, p3}, LbwZ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 453
    nop

    :pswitch_data_0
    .packed-switch 0x190
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected a(Ljava/lang/String;LbwW;)V
    .locals 4

    .prologue
    .line 399
    invoke-virtual {p2}, LbwW;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 409
    new-instance v0, LbwW;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, LbwW;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LbwW;->a()I

    move-result v2

    invoke-virtual {p2}, LbwW;->a()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LbwW;-><init>(Ljava/lang/String;ILjava/io/InputStream;)V

    throw v0

    .line 401
    :sswitch_0
    new-instance v0, LbwR;

    invoke-direct {v0, p1, p2}, LbwR;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 403
    :sswitch_1
    new-instance v0, LbwO;

    invoke-direct {v0, p1, p2}, LbwO;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 405
    :sswitch_2
    new-instance v0, LbwY;

    invoke-direct {v0, p1, p2}, LbwY;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 407
    :sswitch_3
    new-instance v0, Lbxa;

    invoke-direct {v0, p1, p2}, Lbxa;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 399
    :sswitch_data_0
    .sparse-switch
        0x130 -> :sswitch_3
        0x191 -> :sswitch_1
        0x193 -> :sswitch_0
        0x19a -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 343
    :try_start_0
    iget-object v0, p0, LbwU;->a:LbwS;

    invoke-interface {v0, p1, p2, p3}, LbwS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 344
    :catch_0
    move-exception v0

    .line 345
    invoke-virtual {v0}, LbwW;->a()I

    move-result v1

    const/16 v2, 0x194

    if-eq v1, v2, :cond_0

    .line 350
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v0}, LbwU;->a(Ljava/lang/Class;Ljava/lang/String;LbwW;)V

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;LbwW;)V
    .locals 4

    .prologue
    .line 417
    invoke-virtual {p2}, LbwW;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 427
    new-instance v0, LbwW;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, LbwW;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LbwW;->a()I

    move-result v2

    invoke-virtual {p2}, LbwW;->a()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LbwW;-><init>(Ljava/lang/String;ILjava/io/InputStream;)V

    throw v0

    .line 419
    :sswitch_0
    new-instance v0, LbwR;

    invoke-direct {v0, p1, p2}, LbwR;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 421
    :sswitch_1
    new-instance v0, LbwO;

    invoke-direct {v0, p1, p2}, LbwO;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 423
    :sswitch_2
    new-instance v0, LbwZ;

    invoke-direct {v0, p1, p2}, LbwZ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 425
    :sswitch_3
    new-instance v0, Lbxa;

    invoke-direct {v0, p1, p2}, Lbxa;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 417
    :sswitch_data_0
    .sparse-switch
        0x130 -> :sswitch_3
        0x191 -> :sswitch_1
        0x193 -> :sswitch_0
        0x194 -> :sswitch_2
    .end sparse-switch
.end method

.method protected c(Ljava/lang/String;LbwW;)V
    .locals 4

    .prologue
    .line 435
    invoke-virtual {p2}, LbwW;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 443
    :pswitch_0
    new-instance v0, LbwW;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, LbwW;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LbwW;->a()I

    move-result v2

    invoke-virtual {p2}, LbwW;->a()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LbwW;-><init>(Ljava/lang/String;ILjava/io/InputStream;)V

    throw v0

    .line 437
    :pswitch_1
    new-instance v0, LbwR;

    invoke-direct {v0, p1, p2}, LbwR;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 439
    :pswitch_2
    new-instance v0, LbwO;

    invoke-direct {v0, p1, p2}, LbwO;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 441
    :pswitch_3
    new-instance v0, LbwP;

    invoke-direct {v0, p1, p2}, LbwP;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 435
    :pswitch_data_0
    .packed-switch 0x190
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
