.class public final LFQ;
.super Ljava/lang/Object;
.source "DocumentFileCloseAndTrackTask.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;
    .locals 3

    .prologue
    .line 26
    .line 27
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/ParcelableTask;

    .line 28
    new-instance v1, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;-><init>(Lcom/google/android/apps/docs/utils/ParcelableTask;Ljava/lang/Object;)V

    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;
    .locals 1

    .prologue
    .line 33
    new-array v0, p1, [Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, LFQ;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, LFQ;->a(I)[Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;

    move-result-object v0

    return-object v0
.end method
