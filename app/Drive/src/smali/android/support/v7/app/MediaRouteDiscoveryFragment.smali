.class public Landroid/support/v7/app/MediaRouteDiscoveryFragment;
.super Landroid/support/v4/app/Fragment;
.source "MediaRouteDiscoveryFragment.java"


# instance fields
.field private a:Llg;

.field private a:Llj;

.field private a:Lll;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 37
    const-string v0, "selector"

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->c:Ljava/lang/String;

    .line 44
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llj;

    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, Llj;->a(Landroid/content/Context;)Llj;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llj;

    .line 58
    :cond_0
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llg;

    if-nez v0, :cond_1

    .line 101
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    .line 103
    const-string v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Llg;->a(Landroid/os/Bundle;)Llg;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llg;

    .line 105
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llg;

    if-nez v0, :cond_1

    .line 106
    sget-object v0, Llg;->a:Llg;

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llg;

    .line 109
    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x4

    return v0
.end method

.method public a()Lll;
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lin;

    invoke-direct {v0, p0}, Lin;-><init>(Landroid/support/v7/app/MediaRouteDiscoveryFragment;)V

    return-object v0
.end method

.method public j_()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Lll;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llj;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Lll;

    invoke-virtual {v0, v1}, Llj;->a(Lll;)V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Lll;

    .line 157
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->j_()V

    .line 158
    return-void
.end method

.method public k_()V
    .locals 4

    .prologue
    .line 140
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->k_()V

    .line 142
    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->t()V

    .line 143
    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a()V

    .line 144
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a()Lll;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Lll;

    .line 145
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Lll;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llj;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Llg;

    iget-object v2, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Lll;

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Llj;->a(Llg;Lll;I)V

    .line 148
    :cond_0
    return-void
.end method
