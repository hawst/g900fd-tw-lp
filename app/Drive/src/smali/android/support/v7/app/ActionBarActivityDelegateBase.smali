.class public Landroid/support/v7/app/ActionBarActivityDelegateBase;
.super LhO;
.source "ActionBarActivityDelegateBase.java"

# interfaces
.implements Ljc;


# instance fields
.field private a:I

.field private a:Landroid/graphics/Rect;

.field private a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

.field public a:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private a:Landroid/view/View;

.field private a:Landroid/view/ViewGroup;

.field public a:Landroid/widget/PopupWindow;

.field private a:LhV;

.field private a:LhY;

.field private a:Ljava/lang/CharSequence;

.field public a:Ljava/lang/Runnable;

.field private a:Lke;

.field public a:Lmu;

.field private a:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

.field private b:Landroid/graphics/Rect;

.field private b:Landroid/view/ViewGroup;

.field private final b:Ljava/lang/Runnable;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(LhN;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0, p1}, LhO;-><init>(LhN;)V

    .line 112
    new-instance v0, LhR;

    invoke-direct {v0, p0}, LhR;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Ljava/lang/Runnable;

    .line 135
    return-void
.end method

.method private a(I)I
    .locals 8

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1268
    .line 1270
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_b

    .line 1271
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_b

    .line 1272
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1276
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1277
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/graphics/Rect;

    if-nez v1, :cond_0

    .line 1278
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/graphics/Rect;

    .line 1279
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/graphics/Rect;

    .line 1281
    :cond_0
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/graphics/Rect;

    .line 1282
    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/graphics/Rect;

    .line 1283
    invoke-virtual {v1, v2, p1, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1285
    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    invoke-static {v5, v1, v4}, LkM;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 1286
    iget v1, v4, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_4

    move v1, p1

    .line 1287
    :goto_0
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v4, v1, :cond_a

    .line 1289
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1291
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    if-nez v1, :cond_5

    .line 1292
    new-instance v1, Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    .line 1293
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v4}, LhN;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lis;->abc_input_method_navigation_guard:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1295
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move v1, v3

    .line 1311
    :goto_1
    iget-boolean v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c:Z

    if-nez v4, :cond_1

    move p1, v2

    :cond_1
    move v7, v1

    move v1, v3

    move v3, v7

    .line 1325
    :goto_2
    if-eqz v3, :cond_2

    .line 1326
    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    move v0, v1

    .line 1330
    :goto_3
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1331
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    if-eqz v0, :cond_8

    :goto_4
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1334
    :cond_3
    return p1

    :cond_4
    move v1, v2

    .line 1286
    goto :goto_0

    .line 1299
    :cond_5
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1300
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v4, p1, :cond_6

    .line 1301
    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1302
    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_6
    move v1, v3

    goto :goto_1

    .line 1320
    :cond_7
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz v1, :cond_9

    .line 1322
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v1, v2

    goto :goto_2

    .line 1331
    :cond_8
    const/16 v2, 0x8

    goto :goto_4

    :cond_9
    move v3, v2

    move v1, v2

    goto :goto_2

    :cond_a
    move v1, v2

    goto :goto_1

    :cond_b
    move v0, v2

    goto :goto_3
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:I

    return v0
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;I)I
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:I

    return p1
.end method

.method private a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1185
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-gt v1, p1, :cond_2

    .line 1186
    :cond_0
    add-int/lit8 v1, p1, 0x1

    new-array v1, v1, [Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    .line 1187
    if-eqz v0, :cond_1

    .line 1188
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1190
    :cond_1
    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-object v0, v1

    .line 1193
    :cond_2
    aget-object v1, v0, p1

    .line 1194
    if-nez v1, :cond_3

    .line 1195
    new-instance v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    invoke-direct {v1, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;-><init>(I)V

    aput-object v1, v0, p1

    move-object v0, v1

    .line 1197
    :goto_0
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1172
    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    .line 1173
    if-eqz v3, :cond_0

    array-length v0, v3

    :goto_0
    move v2, v1

    .line 1174
    :goto_1
    if-ge v2, v0, :cond_2

    .line 1175
    aget-object v1, v3, v2

    .line 1176
    if-eqz v1, :cond_1

    iget-object v4, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-ne v4, p1, :cond_1

    move-object v0, v1

    .line 1180
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 1173
    goto :goto_0

    .line 1174
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1180
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 1150
    if-nez p3, :cond_1

    .line 1152
    if-nez p2, :cond_0

    .line 1153
    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1154
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    aget-object p2, v0, p1

    .line 1158
    :cond_0
    if-eqz p2, :cond_1

    .line 1160
    iget-object p3, p2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    .line 1165
    :cond_1
    if-eqz p2, :cond_2

    iget-boolean v0, p2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Z

    if-nez v0, :cond_2

    .line 1169
    :goto_0
    return-void

    .line 1168
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    invoke-interface {v0, p1, p3}, LiC;->a(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;)V
    .locals 1

    .prologue
    .line 867
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/ViewGroup;

    iput-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/view/ViewGroup;

    .line 868
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a(Landroid/content/Context;)V

    .line 869
    return-void
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 822
    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 864
    :cond_0
    :goto_0
    return-void

    .line 828
    :cond_1
    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-nez v0, :cond_2

    .line 829
    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    .line 830
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 831
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v4, 0x4

    if-ne v0, v4, :cond_3

    move v0, v1

    .line 833
    :goto_1
    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_4

    move v3, v1

    .line 836
    :goto_2
    if-eqz v0, :cond_2

    if-nez v3, :cond_0

    .line 841
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    .line 842
    if-eqz v0, :cond_5

    iget v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    iget-object v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-interface {v0, v3, v4}, LiC;->b(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 844
    invoke-direct {p0, p1, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 831
    goto :goto_1

    :cond_4
    move v3, v2

    .line 833
    goto :goto_2

    .line 849
    :cond_5
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Z

    if-eqz v0, :cond_7

    .line 854
    :cond_6
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;)V

    .line 858
    :cond_7
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 862
    iput-boolean v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->b:Z

    .line 863
    iput-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Z

    goto :goto_0
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1120
    if-eqz p2, :cond_1

    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v0}, Lke;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1122
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-direct {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Ljb;)V

    .line 1146
    :cond_0
    :goto_0
    return-void

    .line 1126
    :cond_1
    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Z

    if-eqz v0, :cond_2

    .line 1127
    if-eqz p2, :cond_2

    .line 1128
    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    invoke-direct {p0, v0, p1, v2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V

    .line 1132
    :cond_2
    iput-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Z

    .line 1133
    iput-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->b:Z

    .line 1134
    iput-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Z

    .line 1137
    iput-object v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/view/View;

    .line 1141
    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Z

    .line 1143
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-ne v0, p1, :cond_0

    .line 1144
    iput-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    goto :goto_0
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;I)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)V

    return-void
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V

    return-void
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    return-void
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Ljb;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Ljb;)V

    return-void
.end method

.method private a(Ljb;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 872
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v0}, Lke;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Lev;->a(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v0}, Lke;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 876
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    .line 878
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v1}, Lke;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p2, :cond_4

    .line 879
    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 881
    iget-boolean v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i:Z

    if-eqz v1, :cond_2

    iget v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 883
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/ViewGroup;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 884
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 887
    :cond_2
    invoke-direct {p0, v3, v4}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v1

    .line 891
    iget-object v2, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-eqz v2, :cond_3

    iget-boolean v2, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->f:Z

    if-nez v2, :cond_3

    iget-object v2, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-interface {v0, v3, v6, v2}, LiC;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 893
    iget-object v1, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-interface {v0, v5, v1}, LiC;->b(ILandroid/view/Menu;)Z

    .line 894
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v0}, Lke;->e()Z

    .line 913
    :cond_3
    :goto_0
    return-void

    .line 898
    :cond_4
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v0}, Lke;->f()Z

    .line 899
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 900
    invoke-direct {p0, v3, v4}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 901
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v1, v5, v0}, LhN;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_0

    .line 907
    :cond_5
    invoke-direct {p0, v3, v4}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 909
    iput-boolean v4, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Z

    .line 910
    invoke-direct {p0, v0, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    .line 912
    invoke-direct {p0, v0, v6}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 953
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    .line 956
    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_4

    .line 958
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 959
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 960
    sget v0, Liq;->actionBarTheme:I

    invoke-virtual {v3, v0, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 962
    const/4 v0, 0x0

    .line 963
    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    if-eqz v4, :cond_3

    .line 964
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 965
    invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 966
    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 967
    sget v4, Liq;->actionBarWidgetTheme:I

    invoke-virtual {v0, v4, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 974
    :goto_0
    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    if-eqz v4, :cond_2

    .line 975
    if-nez v0, :cond_1

    .line 976
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 977
    invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 979
    :cond_1
    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_2
    move-object v2, v0

    .line 982
    if-eqz v2, :cond_4

    .line 983
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 984
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 988
    :goto_1
    new-instance v1, Ljb;

    invoke-direct {v1, v0}, Ljb;-><init>(Landroid/content/Context;)V

    .line 989
    invoke-virtual {v1, p0}, Ljb;->a(Ljc;)V

    .line 990
    invoke-virtual {p1, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a(Ljb;)V

    .line 992
    return v5

    .line 970
    :cond_3
    sget v4, Liq;->actionBarWidgetTheme:I

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1012
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1102
    :cond_0
    :goto_0
    return v2

    .line 1017
    :cond_1
    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Z

    if-eqz v0, :cond_2

    move v2, v1

    .line 1018
    goto :goto_0

    .line 1021
    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eq v0, p1, :cond_3

    .line 1023
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    invoke-direct {p0, v0, v2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    .line 1026
    :cond_3
    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-eqz v0, :cond_4

    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_a

    :cond_4
    move v0, v1

    .line 1029
    :goto_1
    if-eqz v0, :cond_5

    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v3, :cond_5

    .line 1032
    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v3}, Lke;->setMenuPrepared()V

    .line 1036
    :cond_5
    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-eqz v3, :cond_6

    iget-boolean v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->f:Z

    if-eqz v3, :cond_c

    .line 1037
    :cond_6
    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-nez v3, :cond_7

    .line 1038
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-eqz v3, :cond_0

    .line 1043
    :cond_7
    if-eqz v0, :cond_9

    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v3, :cond_9

    .line 1044
    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhV;

    if-nez v3, :cond_8

    .line 1045
    new-instance v3, LhV;

    invoke-direct {v3, p0, v6}, LhV;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;LhR;)V

    iput-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhV;

    .line 1047
    :cond_8
    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    iget-object v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhV;

    invoke-interface {v3, v4, v5}, Lke;->setMenu(Landroid/view/Menu;Ljr;)V

    .line 1052
    :cond_9
    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v3}, Ljb;->b()V

    .line 1053
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v3

    iget v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    iget-object v5, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-interface {v3, v4, v5}, LiC;->a(ILandroid/view/Menu;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 1055
    invoke-virtual {p1, v6}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a(Ljb;)V

    .line 1057
    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhV;

    invoke-interface {v0, v6, v1}, Lke;->setMenu(Landroid/view/Menu;Ljr;)V

    goto :goto_0

    :cond_a
    move v0, v2

    .line 1026
    goto :goto_1

    .line 1065
    :cond_b
    iput-boolean v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->f:Z

    .line 1070
    :cond_c
    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v3}, Ljb;->b()V

    .line 1074
    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/os/Bundle;

    if-eqz v3, :cond_d

    .line 1075
    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    iget-object v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/os/Bundle;

    invoke-virtual {v3, v4}, Ljb;->b(Landroid/os/Bundle;)V

    .line 1076
    iput-object v6, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/os/Bundle;

    .line 1080
    :cond_d
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v3

    iget-object v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-interface {v3, v2, v6, v4}, LiC;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 1081
    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_e

    .line 1084
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhV;

    invoke-interface {v0, v6, v1}, Lke;->setMenu(Landroid/view/Menu;Ljr;)V

    .line 1086
    :cond_e
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v0}, Ljb;->c()V

    goto/16 :goto_0

    .line 1091
    :cond_f
    if-eqz p2, :cond_10

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    :goto_2
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    .line 1093
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    if-eq v0, v1, :cond_11

    move v0, v1

    :goto_3
    iput-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Z

    .line 1094
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    iget-boolean v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Z

    invoke-virtual {v0, v3}, Ljb;->setQwertyMode(Z)V

    .line 1095
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v0}, Ljb;->c()V

    .line 1098
    iput-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Z

    .line 1099
    iput-boolean v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->b:Z

    .line 1100
    iput-object p1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move v2, v1

    .line 1102
    goto/16 :goto_0

    .line 1091
    :cond_10
    const/4 v0, -0x1

    goto :goto_2

    :cond_11
    move v0, v2

    .line 1093
    goto :goto_3
.end method

.method public static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Z)Z
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i:Z

    return p1
.end method

.method public static synthetic b(Landroid/support/v7/app/ActionBarActivityDelegateBase;I)I
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(I)I

    move-result v0

    return v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1226
    iget v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:I

    shl-int v1, v2, p1

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:I

    .line 1228
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1229
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lec;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1230
    iput-boolean v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i:Z

    .line 1232
    :cond_0
    return-void
.end method

.method private b(Ljb;)V
    .locals 2

    .prologue
    .line 1106
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Z

    if-eqz v0, :cond_0

    .line 1117
    :goto_0
    return-void

    .line 1110
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Z

    .line 1111
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v0}, Lke;->b()V

    .line 1112
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    .line 1113
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1114
    const/16 v1, 0x8

    invoke-interface {v0, v1, p1}, LiC;->a(ILandroid/view/Menu;)V

    .line 1116
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Z

    goto :goto_0
.end method

.method private b(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 996
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-nez v0, :cond_0

    .line 1008
    :goto_0
    return v1

    .line 1000
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhY;

    if-nez v0, :cond_1

    .line 1001
    new-instance v0, LhY;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, LhY;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;LhR;)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhY;

    .line 1004
    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhY;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a(Ljr;)Ljs;

    move-result-object v0

    .line 1006
    check-cast v0, Landroid/view/View;

    iput-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/view/View;

    .line 1008
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/view/View;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private c(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1235
    invoke-direct {p0, p1, v4}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 1237
    iget-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-eqz v1, :cond_1

    .line 1238
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1239
    iget-object v2, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v2, v1}, Ljb;->a(Landroid/os/Bundle;)V

    .line 1240
    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1241
    iput-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/os/Bundle;

    .line 1244
    :cond_0
    iget-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v1}, Ljb;->b()V

    .line 1245
    iget-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v1}, Ljb;->clear()V

    .line 1247
    :cond_1
    iput-boolean v4, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->f:Z

    .line 1248
    iput-boolean v4, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Z

    .line 1251
    const/16 v0, 0x8

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_3

    .line 1253
    invoke-direct {p0, v3, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 1254
    if-eqz v0, :cond_3

    .line 1255
    iput-boolean v3, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Z

    .line 1256
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    .line 1259
    :cond_3
    return-void
.end method

.method private h()V
    .locals 11

    .prologue
    const/4 v10, 0x6

    const/4 v9, 0x5

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 381
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    sget-object v2, LiA;->Theme:[I

    invoke-virtual {v0, v2}, LhN;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 388
    sget v0, LiA;->Theme_windowFixedWidthMajor:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 389
    if-nez v1, :cond_f

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 390
    :goto_0
    sget v2, LiA;->Theme_windowFixedWidthMajor:I

    invoke-virtual {v6, v2, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 392
    :goto_1
    sget v2, LiA;->Theme_windowFixedWidthMinor:I

    invoke-virtual {v6, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 393
    if-nez v1, :cond_d

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 394
    :goto_2
    sget v3, LiA;->Theme_windowFixedWidthMinor:I

    invoke-virtual {v6, v3, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 396
    :goto_3
    sget v3, LiA;->Theme_windowFixedHeightMajor:I

    invoke-virtual {v6, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 397
    if-nez v1, :cond_b

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 398
    :goto_4
    sget v5, LiA;->Theme_windowFixedHeightMajor:I

    invoke-virtual {v6, v5, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 400
    :goto_5
    sget v5, LiA;->Theme_windowFixedHeightMinor:I

    invoke-virtual {v6, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 401
    if-nez v1, :cond_0

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 402
    :cond_0
    sget v5, LiA;->Theme_windowFixedHeightMinor:I

    invoke-virtual {v6, v5, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 405
    :cond_1
    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v5}, LhN;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    .line 406
    iget v5, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v8, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v5, v8, :cond_4

    const/4 v5, 0x1

    .line 410
    :goto_6
    if-eqz v5, :cond_5

    .line 411
    :goto_7
    if-eqz v2, :cond_a

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_a

    .line 412
    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v9, :cond_6

    .line 413
    invoke-virtual {v2, v7}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    .line 419
    :goto_8
    if-eqz v5, :cond_7

    .line 420
    :goto_9
    if-eqz v3, :cond_9

    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_9

    .line 421
    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-ne v0, v9, :cond_8

    .line 422
    invoke-virtual {v3, v7}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 428
    :goto_a
    if-ne v2, v4, :cond_2

    if-eq v0, v4, :cond_3

    .line 429
    :cond_2
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v1}, LhN;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/view/Window;->setLayout(II)V

    .line 432
    :cond_3
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 433
    return-void

    .line 406
    :cond_4
    const/4 v5, 0x0

    goto :goto_6

    :cond_5
    move-object v2, v0

    .line 410
    goto :goto_7

    .line 414
    :cond_6
    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_a

    .line 415
    iget v0, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v8, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v8, v8

    invoke-virtual {v2, v0, v8}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    goto :goto_8

    :cond_7
    move-object v3, v1

    .line 419
    goto :goto_9

    .line 423
    :cond_8
    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_9

    .line 424
    iget v0, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget v1, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    invoke-virtual {v3, v0, v1}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    goto :goto_a

    :cond_9
    move v0, v4

    goto :goto_a

    :cond_a
    move v2, v4

    goto :goto_8

    :cond_b
    move-object v3, v1

    goto/16 :goto_4

    :cond_c
    move-object v3, v1

    goto/16 :goto_5

    :cond_d
    move-object v2, v1

    goto/16 :goto_2

    :cond_e
    move-object v2, v1

    goto/16 :goto_3

    :cond_f
    move-object v0, v1

    goto/16 :goto_0

    :cond_10
    move-object v0, v1

    goto/16 :goto_1
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 706
    sget v0, Liq;->homeAsUpIndicator:I

    return v0
.end method

.method public a(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 471
    .line 474
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    if-nez v0, :cond_2

    .line 476
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    .line 477
    if-eqz v0, :cond_1

    .line 478
    invoke-interface {v0, p1}, LiC;->a(I)Landroid/view/View;

    move-result-object v0

    .line 481
    :goto_0
    if-nez v0, :cond_0

    .line 483
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v2

    .line 484
    invoke-direct {p0, v2, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)V

    .line 485
    iget-boolean v1, v2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Z

    if-eqz v1, :cond_0

    .line 486
    iget-object v0, v2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Landroid/view/View;

    .line 490
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method a(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 2

    .prologue
    .line 749
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 752
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 765
    :cond_1
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 752
    :sswitch_0
    const-string v1, "EditText"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "Spinner"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "CheckBox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "RadioButton"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "CheckedTextView"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 754
    :pswitch_0
    new-instance v0, Landroid/support/v7/internal/widget/TintEditText;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/TintEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 756
    :pswitch_1
    new-instance v0, Landroid/support/v7/internal/widget/TintSpinner;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/TintSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 758
    :pswitch_2
    new-instance v0, Landroid/support/v7/internal/widget/TintCheckBox;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/TintCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 760
    :pswitch_3
    new-instance v0, Landroid/support/v7/internal/widget/TintRadioButton;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/TintRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 762
    :pswitch_4
    new-instance v0, Landroid/support/v7/internal/widget/TintCheckedTextView;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/TintCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 752
    nop

    :sswitch_data_0
    .sparse-switch
        -0x56c015e7 -> :sswitch_4
        -0x1440b607 -> :sswitch_1
        0x2e46a6ed -> :sswitch_3
        0x5f7507c3 -> :sswitch_2
        0x63577677 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a()LhJ;
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f()V

    .line 156
    new-instance v0, LiD;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    iget-boolean v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Z

    invoke-direct {v0, v1, v2}, LiD;-><init>(LhN;Z)V

    .line 157
    iget-boolean v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Z

    invoke-virtual {v0, v1}, LhJ;->e(Z)V

    .line 158
    return-object v0
.end method

.method public a(Lmv;)Lmu;
    .locals 3

    .prologue
    .line 561
    if-nez p1, :cond_0

    .line 562
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ActionMode callback can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()V

    .line 569
    :cond_1
    new-instance v0, LhW;

    invoke-direct {v0, p0, p1}, LhW;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;Lmv;)V

    .line 571
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v1

    .line 572
    if-eqz v1, :cond_2

    .line 573
    invoke-virtual {v1, v0}, LhJ;->a(Lmv;)Lmu;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    .line 574
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    if-eqz v1, :cond_2

    .line 575
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    invoke-virtual {v1, v2}, LhN;->a(Lmu;)V

    .line 579
    :cond_2
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    if-nez v1, :cond_3

    .line 581
    invoke-virtual {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Lmv;)Lmu;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    .line 584
    :cond_3
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LhJ;->f(Z)V

    .line 204
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 225
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f()V

    .line 226
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 227
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 228
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v1}, LhN;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 229
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->e()V

    .line 230
    return-void
.end method

.method public a(ILandroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 511
    invoke-direct {p0, p1, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_0

    .line 514
    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    .line 517
    :cond_0
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    .line 518
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    .line 519
    if-eqz v0, :cond_1

    .line 520
    invoke-virtual {v0, v1}, LhJ;->g(Z)V

    .line 527
    :cond_1
    :goto_0
    return-void

    .line 522
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 525
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0, p1, p2}, LhN;->a(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->e:Z

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {v0, p1}, LhJ;->a(Landroid/content/res/Configuration;)V

    .line 196
    :cond_0
    return-void
.end method

.method a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 139
    invoke-super {p0, p1}, LhO;->a(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/view/ViewGroup;

    .line 143
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-static {v0}, Lar;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    .line 145
    if-nez v0, :cond_1

    .line 146
    iput-boolean v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Z

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {v0, v1}, LhJ;->e(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f()V

    .line 217
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 218
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 219
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 220
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->e()V

    .line 221
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f()V

    .line 235
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 236
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 237
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->e()V

    .line 239
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-interface {v0, p1}, Lke;->setWindowTitle(Ljava/lang/CharSequence;)V

    .line 467
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 463
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    invoke-virtual {v0, p1}, LhJ;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 465
    :cond_1
    iput-object p1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public a(Ljb;)V
    .locals 1

    .prologue
    .line 556
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Ljb;Z)V

    .line 557
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 667
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    if-eqz v1, :cond_1

    .line 668
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    invoke-virtual {v1}, Lmu;->a()V

    .line 678
    :cond_0
    :goto_0
    return v0

    .line 673
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v1

    .line 674
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LhJ;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 678
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 744
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method a(ILandroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 531
    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    .line 532
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v1

    .line 533
    if-eqz v1, :cond_0

    .line 534
    invoke-virtual {v1, v0}, LhJ;->g(Z)V

    .line 538
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0, p1, p2}, LhN;->b(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 503
    if-eqz p1, :cond_0

    .line 504
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, LiC;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    .line 506
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;ILandroid/view/KeyEvent;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1202
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1222
    :cond_0
    :goto_0
    return v0

    .line 1210
    :cond_1
    iget-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Z

    if-nez v1, :cond_2

    invoke-direct {p0, p1, p3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-eqz v1, :cond_3

    .line 1212
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    invoke-virtual {v0, p2, p3, p4}, Ljb;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    .line 1215
    :cond_3
    if-eqz v0, :cond_0

    .line 1217
    and-int/lit8 v1, p4, 0x1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-nez v1, :cond_0

    .line 1218
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    goto :goto_0
.end method

.method public a(Ljb;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 544
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    .line 545
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 546
    invoke-virtual {p1}, Ljb;->a()Ljb;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v1

    .line 547
    if-eqz v1, :cond_0

    .line 548
    iget v1, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    invoke-interface {v0, v1, p2}, LiC;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    .line 551
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Lmv;)Lmu;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 597
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()V

    .line 601
    :cond_0
    new-instance v3, LhW;

    invoke-direct {v3, p0, p1}, LhW;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;Lmv;)V

    .line 602
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()Landroid/content/Context;

    move-result-object v4

    .line 604
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-nez v0, :cond_1

    .line 605
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->d:Z

    if-eqz v0, :cond_5

    .line 606
    new-instance v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-direct {v0, v4}, Landroid/support/v7/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 607
    new-instance v0, Landroid/widget/PopupWindow;

    sget v5, Liq;->actionModePopupWindowStyle:I

    invoke-direct {v0, v4, v7, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    .line 609
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 610
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    const/4 v5, -0x1

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 612
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 613
    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v5}, LhN;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    sget v6, Liq;->actionBarSize:I

    invoke-virtual {v5, v6, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 614
    iget v0, v0, Landroid/util/TypedValue;->data:I

    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v5}, LhN;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    .line 616
    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v5, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setContentHeight(I)V

    .line 617
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    const/4 v5, -0x2

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 618
    new-instance v0, LhU;

    invoke-direct {v0, p0}, LhU;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Ljava/lang/Runnable;

    .line 636
    :cond_1
    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_3

    .line 637
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()V

    .line 638
    new-instance v5, LiL;

    iget-object v6, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    invoke-direct {v5, v4, v6, v3, v0}, LiL;-><init>(Landroid/content/Context;Landroid/support/v7/internal/widget/ActionBarContextView;Lmv;Z)V

    .line 640
    invoke-virtual {v5}, Lmu;->a()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {p1, v5, v0}, Lmv;->a(Lmu;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 641
    invoke-virtual {v5}, Lmu;->b()V

    .line 642
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Lmu;)V

    .line 643
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->setVisibility(I)V

    .line 644
    iput-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    .line 645
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    .line 646
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 648
    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 651
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 652
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lec;->b(Landroid/view/View;)V

    .line 658
    :cond_3
    :goto_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    if-eqz v0, :cond_4

    .line 659
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    invoke-virtual {v0, v1}, LhN;->a(Lmu;)V

    .line 661
    :cond_4
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    return-object v0

    .line 626
    :cond_5
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    sget v5, Liv;->action_mode_bar_stub:I

    invoke-virtual {v0, v5}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 628
    if-eqz v0, :cond_1

    .line 630
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ViewStubCompat;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    .line 631
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 638
    goto :goto_1

    .line 655
    :cond_7
    iput-object v7, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lmu;

    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 208
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    .line 210
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LhJ;->f(Z)V

    .line 212
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 243
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f()V

    .line 244
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 245
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 246
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->e()V

    .line 247
    return-void
.end method

.method b(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 713
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v2, :cond_1

    .line 714
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-virtual {p0, v2, v3, p2, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    move-result v2

    .line 716
    if-eqz v2, :cond_1

    .line 717
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v1, :cond_0

    .line 718
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    iput-boolean v0, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->b:Z

    .line 737
    :cond_0
    :goto_0
    return v0

    .line 728
    :cond_1
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-nez v2, :cond_2

    .line 729
    invoke-direct {p0, v1, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v2

    .line 730
    invoke-direct {p0, v2, p2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    .line 731
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-virtual {p0, v2, v3, p2, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    move-result v3

    .line 732
    iput-boolean v1, v2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Z

    .line 733
    if-nez v3, :cond_0

    :cond_2
    move v0, v1

    .line 737
    goto :goto_0
.end method

.method public b(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 495
    if-eqz p1, :cond_0

    .line 496
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LiC;->a(ILandroid/view/Menu;)Z

    move-result v0

    .line 498
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 589
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()LhJ;

    move-result-object v0

    .line 590
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LhJ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    :goto_0
    return-void

    .line 592
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(I)V

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method final f()V
    .locals 7

    .prologue
    const v6, 0x1020002

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 255
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->e:Z

    if-nez v0, :cond_6

    .line 256
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Z

    if-eqz v0, :cond_8

    .line 262
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 263
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v2, Liq;->actionBarTheme:I

    invoke-virtual {v0, v2, v1, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 266
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_7

    .line 267
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v0, v2, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 273
    :goto_0
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lix;->abc_screen_toolbar:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    .line 276
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    sget v1, Liv;->decor_content_parent:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lke;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    .line 278
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v1

    invoke-interface {v0, v1}, Lke;->setWindowCallback(LiC;)V

    .line 283
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Z

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Lke;->a(I)V

    .line 286
    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Z

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lke;->a(I)V

    .line 289
    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Z

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lke;->a(I)V

    .line 336
    :cond_2
    :goto_1
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    invoke-static {v0}, LkM;->a(Landroid/view/View;)V

    .line 339
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, LhN;->a(Landroid/view/View;)V

    .line 343
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0, v6}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 344
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 345
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    sget v2, Liv;->action_bar_activity_content:I

    invoke-virtual {v1, v2}, LhN;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 346
    invoke-virtual {v1, v6}, Landroid/view/View;->setId(I)V

    .line 350
    instance-of v1, v0, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_3

    .line 351
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 355
    :cond_3
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    if-eqz v0, :cond_4

    .line 356
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Lke;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Lke;->setWindowTitle(Ljava/lang/CharSequence;)V

    .line 357
    iput-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Ljava/lang/CharSequence;

    .line 360
    :cond_4
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h()V

    .line 362
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g()V

    .line 364
    iput-boolean v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->e:Z

    .line 371
    invoke-direct {p0, v4, v4}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(IZ)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 372
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v1

    if-nez v1, :cond_6

    if-eqz v0, :cond_5

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:Ljb;

    if-nez v0, :cond_6

    .line 373
    :cond_5
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(I)V

    .line 376
    :cond_6
    return-void

    .line 269
    :cond_7
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    goto/16 :goto_0

    .line 293
    :cond_8
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c:Z

    if-eqz v0, :cond_9

    .line 294
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lix;->abc_screen_simple_overlay_action_mode:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    .line 301
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_a

    .line 304
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    new-instance v1, LhS;

    invoke-direct {v1, p0}, LhS;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    invoke-static {v0, v1}, Lec;->a(Landroid/view/View;LdM;)V

    goto/16 :goto_1

    .line 297
    :cond_9
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lix;->abc_screen_simple:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    goto :goto_2

    .line 325
    :cond_a
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Landroid/view/ViewGroup;

    check-cast v0, Lkh;

    new-instance v1, LhT;

    invoke-direct {v1, p0}, LhT;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    invoke-interface {v0, v1}, Lkh;->setOnFitSystemWindowsListener(Lki;)V

    goto/16 :goto_1
.end method

.method public g()V
    .locals 0

    .prologue
    .line 378
    return-void
.end method
