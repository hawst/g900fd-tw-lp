.class Laxr;
.super Ljava/lang/Object;
.source "SelectionActionMode.java"

# interfaces
.implements Lmv;


# instance fields
.field final synthetic a:Laxp;


# direct methods
.method private constructor <init>(Laxp;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Laxr;->a:Laxp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laxp;Laxq;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Laxr;-><init>(Laxp;)V

    return-void
.end method


# virtual methods
.method public a(Lmu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, Laxr;->a:Laxp;

    invoke-static {v0}, Laxp;->a(Laxp;)Laua;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laua;->a(Z)V

    .line 131
    iget-object v0, p0, Laxr;->a:Laxp;

    invoke-static {v0, v2}, Laxp;->a(Laxp;Lmu;)Lmu;

    .line 132
    iget-object v0, p0, Laxr;->a:Laxp;

    invoke-static {v0}, Laxp;->a(Laxp;)Laxw;

    move-result-object v0

    invoke-virtual {v0, v2}, Laxw;->a(Lcom/google/android/apps/viewer/pdflib/Selection;)V

    .line 133
    return-void
.end method

.method public a(Lmu;Landroid/view/Menu;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 88
    iget-object v0, p0, Laxr;->a:Laxp;

    invoke-static {v0}, Laxp;->a(Laxp;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {}, Laxp;->a()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 90
    iget-object v1, p0, Laxr;->a:Laxp;

    invoke-static {v1}, Laxp;->a(Laxp;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1040016

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmu;->b(Ljava/lang/CharSequence;)V

    .line 91
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lmu;->a(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {p1, v5}, Lmu;->a(Z)V

    .line 94
    const/4 v1, 0x6

    .line 96
    const v2, 0x102001f

    const v3, 0x104000d

    invoke-interface {p2, v4, v2, v4, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    .line 97
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    const/16 v3, 0x61

    .line 98
    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v2

    .line 99
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 101
    const v2, 0x1020021

    const v3, 0x1040001

    invoke-interface {p2, v4, v2, v4, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    .line 102
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v2, 0x63

    .line 103
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v0

    .line 104
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 105
    return v5
.end method

.method public a(Lmu;Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 115
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 117
    :pswitch_1
    iget-object v0, p0, Laxr;->a:Laxp;

    invoke-static {v0}, Laxp;->a(Laxp;)Laxw;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->PAGE_START:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    sget-object v2, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->PAGE_END:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    invoke-virtual {v0, v1, v2}, Laxw;->a(Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V

    goto :goto_0

    .line 121
    :pswitch_2
    iget-object v0, p0, Laxr;->a:Laxp;

    iget-object v1, p0, Laxr;->a:Laxp;

    invoke-static {v1}, Laxp;->a(Laxp;)Laxw;

    move-result-object v1

    invoke-virtual {v1}, Laxw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laxp;->a(Laxp;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Laxr;->a:Laxp;

    invoke-static {v0}, Laxp;->a(Laxp;)Laxw;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laxw;->a(Lcom/google/android/apps/viewer/pdflib/Selection;)V

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x102001f
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lmu;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method
