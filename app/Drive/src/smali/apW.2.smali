.class public LapW;
.super Ljava/lang/Object;
.source "ImagePreviewPage.java"

# interfaces
.implements Laqz;


# instance fields
.field private final a:LPp;

.field private final a:LaGR;

.field private final a:LaGn;

.field private final a:Landroid/graphics/Bitmap;

.field private final a:Landroid/view/LayoutInflater;

.field private a:Landroid/widget/ImageView;

.field private a:Lcom/google/android/apps/docs/view/GestureImageView;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method private constructor <init>(LPp;LaGn;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/graphics/Bitmap;Landroid/view/LayoutInflater;LaGR;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, LapW;->a:LPp;

    .line 73
    iput-object p5, p0, LapW;->a:Landroid/view/LayoutInflater;

    .line 74
    iput-object p2, p0, LapW;->a:LaGn;

    .line 75
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LapW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 76
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, LapW;->a:Landroid/graphics/Bitmap;

    .line 77
    iput-object p6, p0, LapW;->a:LaGR;

    .line 78
    return-void
.end method

.method synthetic constructor <init>(LPp;LaGn;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/graphics/Bitmap;Landroid/view/LayoutInflater;LaGR;LapX;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p6}, LapW;-><init>(LPp;LaGn;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/graphics/Bitmap;Landroid/view/LayoutInflater;LaGR;)V

    return-void
.end method

.method static synthetic a(LapW;)LPp;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LapW;->a:LPp;

    return-object v0
.end method

.method static synthetic a(LapW;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, LapW;->c()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 109
    sget-object v0, LaGn;->c:LaGn;

    iget-object v1, p0, LapW;->a:LaGn;

    invoke-virtual {v0, v1}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 111
    iget-object v0, p0, LapW;->a:LaGR;

    new-instance v1, LapY;

    iget-object v2, p0, LapW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {v1, p0, v2}, LapY;-><init>(LapW;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v0, v1}, LaGR;->a(LaGN;)V

    .line 117
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, LapW;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, LapW;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()V

    .line 106
    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, LapW;->a:Landroid/view/LayoutInflater;

    sget v1, Lxe;->image_preview_page:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 84
    sget v1, Lxc;->image:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/view/GestureImageView;

    iput-object v1, p0, LapW;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    .line 85
    iget-object v1, p0, LapW;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 86
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, LapW;->a:Landroid/graphics/Bitmap;

    invoke-direct {v2, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 87
    iget-object v1, p0, LapW;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/view/GestureImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 89
    sget v1, Lxc;->video_play_overlay:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LapW;->a:Landroid/widget/ImageView;

    .line 90
    sget-object v0, LaGn;->c:LaGn;

    iget-object v1, p0, LapW;->a:LaGn;

    invoke-virtual {v0, v1}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, LapW;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 92
    iget-object v0, p0, LapW;->a:Landroid/widget/ImageView;

    new-instance v1, LapX;

    invoke-direct {v1, p0}, LapX;-><init>(LapW;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, LapW;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->b()V

    .line 122
    return-void
.end method
