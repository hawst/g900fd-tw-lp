.class public LHL;
.super Ljava/lang/Object;
.source "GridViewBinderFactory.java"


# instance fields
.field private final a:I

.field private final a:LBf;

.field private final a:LCU;

.field private final a:LCt;

.field private final a:LDU;

.field private final a:LDk;

.field private final a:LDn;

.field private final a:LamF;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/support/v4/app/Fragment;

.field private final a:Lapc;

.field private final a:Lapd;

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LBf;LDU;LCU;LamF;LDk;ILtK;Lapd;LaoY;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LCt;LDn;)V
    .locals 3

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-object p1, p0, LHL;->a:Landroid/content/Context;

    .line 141
    iput-object p2, p0, LHL;->a:Landroid/support/v4/app/Fragment;

    .line 142
    iput-object p3, p0, LHL;->a:LBf;

    .line 143
    iput-object p4, p0, LHL;->a:LDU;

    .line 144
    iput-object p5, p0, LHL;->a:LCU;

    .line 145
    iput-object p6, p0, LHL;->a:LamF;

    .line 146
    iput-object p7, p0, LHL;->a:LDk;

    .line 147
    iput p8, p0, LHL;->a:I

    .line 148
    iput-object p9, p0, LHL;->a:LtK;

    .line 149
    iput-object p10, p0, LHL;->a:Lapd;

    .line 150
    iput-object p12, p0, LHL;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 151
    move-object/from16 v0, p13

    iput-object v0, p0, LHL;->a:LCt;

    .line 152
    move-object/from16 v0, p14

    iput-object v0, p0, LHL;->a:LDn;

    .line 153
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 154
    sget v2, Lxb;->ic_no_thumbnail:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 155
    new-instance v2, Lapc;

    invoke-direct {v2, p11, v1}, Lapc;-><init>(LaoY;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, LHL;->a:Lapc;

    .line 157
    return-void
.end method

.method private a(LQX;)Z
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p1}, LQX;->a()LzO;

    move-result-object v0

    sget-object v1, LzO;->d:LzO;

    invoke-virtual {v0, v1}, LzO;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(LQX;)LHK;
    .locals 22

    .prologue
    .line 164
    invoke-direct/range {p0 .. p1}, LHL;->a(LQX;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    new-instance v2, LHO;

    move-object/from16 v0, p0

    iget-object v3, v0, LHL;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, LHL;->a:Landroid/support/v4/app/Fragment;

    move-object/from16 v0, p0

    iget-object v5, v0, LHL;->a:LBf;

    move-object/from16 v0, p0

    iget-object v6, v0, LHL;->a:LDU;

    move-object/from16 v0, p0

    iget-object v7, v0, LHL;->a:LCU;

    move-object/from16 v0, p0

    iget-object v8, v0, LHL;->a:LamF;

    move-object/from16 v0, p0

    iget-object v9, v0, LHL;->a:LDk;

    move-object/from16 v0, p0

    iget v10, v0, LHL;->a:I

    move-object/from16 v0, p0

    iget-object v11, v0, LHL;->a:LtK;

    move-object/from16 v0, p0

    iget-object v13, v0, LHL;->a:Lapd;

    new-instance v14, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-object/from16 v0, p0

    iget-object v12, v0, LHL;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 176
    invoke-virtual {v12}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v15, v0, LHL;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-virtual {v15}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v15

    invoke-direct {v14, v12, v15}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v15, v0, LHL;->a:Lapc;

    move-object/from16 v0, p0

    iget-object v0, v0, LHL;->a:LCt;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v12, v0, LHL;->a:LDn;

    move-object/from16 v0, p0

    iget-object v0, v0, LHL;->a:LCt;

    move-object/from16 v17, v0

    sget-object v18, LDg;->a:LDq;

    .line 179
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, LDn;->a(LCt;LDq;)LDg;

    move-result-object v17

    move-object/from16 v12, p1

    invoke-direct/range {v2 .. v17}, LHO;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LBf;LDU;LCU;LamF;LDk;ILtK;LQX;Lapd;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lapc;LCt;LDg;)V

    .line 196
    :goto_0
    return-object v2

    .line 182
    :cond_0
    new-instance v2, LHR;

    move-object/from16 v0, p0

    iget-object v3, v0, LHL;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, LHL;->a:Landroid/support/v4/app/Fragment;

    move-object/from16 v0, p0

    iget-object v5, v0, LHL;->a:LBf;

    move-object/from16 v0, p0

    iget-object v6, v0, LHL;->a:LDU;

    move-object/from16 v0, p0

    iget-object v7, v0, LHL;->a:LCU;

    move-object/from16 v0, p0

    iget-object v8, v0, LHL;->a:LamF;

    move-object/from16 v0, p0

    iget-object v9, v0, LHL;->a:LDk;

    move-object/from16 v0, p0

    iget v10, v0, LHL;->a:I

    move-object/from16 v0, p0

    iget-object v11, v0, LHL;->a:LtK;

    move-object/from16 v0, p0

    iget-object v13, v0, LHL;->a:Lapd;

    move-object/from16 v0, p0

    iget-object v14, v0, LHL;->a:Lapc;

    move-object/from16 v0, p0

    iget-object v15, v0, LHL;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-object/from16 v0, p0

    iget-object v0, v0, LHL;->a:LCt;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v12, v0, LHL;->a:LDn;

    move-object/from16 v0, p0

    iget-object v0, v0, LHL;->a:LCt;

    move-object/from16 v17, v0

    new-instance v18, LDf;

    sget v19, Lxc;->title:I

    sget v20, Lxc;->doc_icon:I

    const/16 v21, 0x1

    invoke-direct/range {v18 .. v21}, LDf;-><init>(IIZ)V

    .line 196
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, LDn;->a(LCt;LDq;)LDg;

    move-result-object v17

    move-object/from16 v12, p1

    invoke-direct/range {v2 .. v17}, LHR;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LBf;LDU;LCU;LamF;LDk;ILtK;LQX;Lapd;Lapc;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LCt;LDg;)V

    goto :goto_0
.end method
