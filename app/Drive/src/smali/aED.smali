.class public final enum LaED;
.super Ljava/lang/Enum;
.source "DocListDatabase.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaED;",
        ">;",
        "Lbjv",
        "<",
        "LaEF;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaED;

.field private static final synthetic a:[LaED;

.field public static final enum b:LaED;

.field public static final enum c:LaED;

.field public static final enum d:LaED;

.field public static final enum e:LaED;

.field public static final enum f:LaED;

.field public static final enum g:LaED;

.field public static final enum h:LaED;

.field public static final enum i:LaED;

.field public static final enum j:LaED;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum k:LaED;

.field public static final enum l:LaED;

.field public static final enum m:LaED;

.field public static final enum n:LaED;

.field public static final enum o:LaED;

.field public static final enum p:LaED;

.field public static final enum q:LaED;

.field public static final enum r:LaED;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum s:LaED;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum t:LaED;

.field public static final enum u:LaED;

.field public static final enum v:LaED;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final a:LaEF;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 230
    new-instance v0, LaED;

    const-string v1, "ACCOUNT"

    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->a:LaED;

    .line 231
    new-instance v0, LaED;

    const-string v1, "ACCOUNT_METADATA"

    invoke-static {}, LaEc;->a()LaEc;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->b:LaED;

    .line 232
    new-instance v0, LaED;

    const-string v1, "DOCUMENT_CONTENT"

    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->c:LaED;

    .line 233
    new-instance v0, LaED;

    const-string v1, "ENTRY"

    invoke-static {}, LaER;->a()LaER;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->d:LaED;

    .line 234
    new-instance v0, LaED;

    const-string v1, "COLLECTION"

    invoke-static {}, LaEu;->a()LaEu;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->e:LaED;

    .line 235
    new-instance v0, LaED;

    const-string v1, "DOCUMENT"

    const/4 v2, 0x5

    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->f:LaED;

    .line 236
    new-instance v0, LaED;

    const-string v1, "CONTAINS_ID"

    const/4 v2, 0x6

    invoke-static {}, LaEw;->a()LaEw;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->g:LaED;

    .line 237
    new-instance v0, LaED;

    const-string v1, "APP_CACHE"

    const/4 v2, 0x7

    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->h:LaED;

    .line 238
    new-instance v0, LaED;

    const-string v1, "CACHE_LIST"

    const/16 v2, 0x8

    invoke-static {}, LaEo;->a()LaEo;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->i:LaED;

    .line 239
    new-instance v0, LaED;

    const-string v1, "__LEGACY_TABLE_ACL"

    const/16 v2, 0x9

    .line 240
    invoke-static {}, LaEg;->a()LaEg;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->j:LaED;

    .line 241
    new-instance v0, LaED;

    const-string v1, "PENDING_OPERATION"

    const/16 v2, 0xa

    invoke-static {}, LaFe;->a()LaFe;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->k:LaED;

    .line 242
    new-instance v0, LaED;

    const-string v1, "CACHED_SEARCH"

    const/16 v2, 0xb

    invoke-static {}, LaEs;->a()LaEs;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->l:LaED;

    .line 243
    new-instance v0, LaED;

    const-string v1, "CACHED_SEARCH_RESULT"

    const/16 v2, 0xc

    invoke-static {}, LaEq;->a()LaEq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->m:LaED;

    .line 244
    new-instance v0, LaED;

    const-string v1, "PARTIAL_FEED"

    const/16 v2, 0xd

    invoke-static {}, LaFc;->a()LaFc;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->n:LaED;

    .line 245
    new-instance v0, LaED;

    const-string v1, "SYNC_REQUEST"

    const/16 v2, 0xe

    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->o:LaED;

    .line 246
    new-instance v0, LaED;

    const-string v1, "SYNC_REQUEST_JOURNAL_ENTRY"

    const/16 v2, 0xf

    invoke-static {}, LaFg;->a()LaFg;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->p:LaED;

    .line 247
    new-instance v0, LaED;

    const-string v1, "UNIQUE_ID"

    const/16 v2, 0x10

    invoke-static {}, LaFk;->a()LaFk;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->q:LaED;

    .line 248
    new-instance v0, LaED;

    const-string v1, "__LEGACY_TABLE_EXPOSED_CONTENT"

    const/16 v2, 0x11

    .line 249
    invoke-static {}, LaET;->a()LaET;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->r:LaED;

    .line 250
    new-instance v0, LaED;

    const-string v1, "__LEGACY_JOBSET"

    const/16 v2, 0x12

    .line 251
    invoke-static {}, LaEW;->a()LaEW;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->s:LaED;

    .line 252
    new-instance v0, LaED;

    const-string v1, "MANIFEST"

    const/16 v2, 0x13

    invoke-static {}, LaFa;->a()LaFa;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->t:LaED;

    .line 253
    new-instance v0, LaED;

    const-string v1, "APP_METADATA"

    const/16 v2, 0x14

    invoke-static {}, LaEl;->a()LaEl;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->u:LaED;

    .line 254
    new-instance v0, LaED;

    const-string v1, "__LEGACY_LOCAL_FILE_ENTRY"

    const/16 v2, 0x15

    .line 255
    invoke-static {}, LaEY;->a()LaEY;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaED;-><init>(Ljava/lang/String;ILaEF;)V

    sput-object v0, LaED;->v:LaED;

    .line 229
    const/16 v0, 0x16

    new-array v0, v0, [LaED;

    sget-object v1, LaED;->a:LaED;

    aput-object v1, v0, v4

    sget-object v1, LaED;->b:LaED;

    aput-object v1, v0, v5

    sget-object v1, LaED;->c:LaED;

    aput-object v1, v0, v6

    sget-object v1, LaED;->d:LaED;

    aput-object v1, v0, v7

    sget-object v1, LaED;->e:LaED;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LaED;->f:LaED;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaED;->g:LaED;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaED;->h:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaED;->i:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaED;->j:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaED;->k:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaED;->l:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LaED;->m:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LaED;->n:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LaED;->o:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LaED;->p:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LaED;->q:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LaED;->r:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LaED;->s:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LaED;->t:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LaED;->u:LaED;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LaED;->v:LaED;

    aput-object v2, v0, v1

    sput-object v0, LaED;->a:[LaED;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaEF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaEF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 260
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 261
    iput-object p3, p0, LaED;->a:LaEF;

    .line 262
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaED;
    .locals 1

    .prologue
    .line 229
    const-class v0, LaED;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaED;

    return-object v0
.end method

.method public static values()[LaED;
    .locals 1

    .prologue
    .line 229
    sget-object v0, LaED;->a:[LaED;

    invoke-virtual {v0}, [LaED;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaED;

    return-object v0
.end method


# virtual methods
.method public a()LaEF;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, LaED;->a:LaEF;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, LaED;->a()LaEF;

    move-result-object v0

    return-object v0
.end method
