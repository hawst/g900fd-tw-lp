.class public abstract enum Lyk;
.super Ljava/lang/Enum;
.source "CsiErrorHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lyk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lyk;

.field private static final synthetic a:[Lyk;

.field public static final enum b:Lyk;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lyl;

    const-string v1, "THROW"

    invoke-direct {v0, v1, v2}, Lyl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyk;->a:Lyk;

    .line 27
    new-instance v0, Lym;

    const-string v1, "LOG"

    invoke-direct {v0, v1, v3}, Lym;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyk;->b:Lyk;

    .line 11
    const/4 v0, 0x2

    new-array v0, v0, [Lyk;

    sget-object v1, Lyk;->a:Lyk;

    aput-object v1, v0, v2

    sget-object v1, Lyk;->b:Lyk;

    aput-object v1, v0, v3

    sput-object v0, Lyk;->a:[Lyk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILyl;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lyk;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lyk;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lyk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lyk;

    return-object v0
.end method

.method public static values()[Lyk;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lyk;->a:[Lyk;

    invoke-virtual {v0}, [Lyk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lyk;

    return-object v0
.end method
