.class public LaKU;
.super Ljava/lang/Object;
.source "FixedSizeWorkerPool.java"


# instance fields
.field private a:I

.field private final a:LaKV;

.field private final a:LaKW;

.field private final a:Ljava/lang/Runnable;

.field private b:I


# direct methods
.method public constructor <init>(LaKW;Ljava/lang/Runnable;IJ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, LaKV;

    invoke-direct {v0, p0, p3, p4, p5}, LaKV;-><init>(LaKU;IJ)V

    iput-object v0, p0, LaKU;->a:LaKV;

    .line 118
    iget-object v0, p0, LaKU;->a:LaKV;

    invoke-static {v0}, LaKV;->a(LaKV;)V

    .line 120
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKW;

    iput-object v0, p0, LaKU;->a:LaKW;

    .line 121
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, LaKU;->a:Ljava/lang/Runnable;

    .line 123
    iput v1, p0, LaKU;->b:I

    .line 124
    iput v1, p0, LaKU;->a:I

    .line 125
    return-void
.end method

.method static synthetic a(LaKU;)I
    .locals 2

    .prologue
    .line 27
    iget v0, p0, LaKU;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LaKU;->b:I

    return v0
.end method

.method static synthetic a(LaKU;)LaKW;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, LaKU;->a:LaKW;

    return-object v0
.end method

.method static synthetic a(LaKU;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, LaKU;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(LaKU;)I
    .locals 2

    .prologue
    .line 27
    iget v0, p0, LaKU;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LaKU;->b:I

    return v0
.end method

.method static synthetic c(LaKU;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, LaKU;->a:I

    return v0
.end method

.method static synthetic d(LaKU;)I
    .locals 2

    .prologue
    .line 27
    iget v0, p0, LaKU;->a:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LaKU;->a:I

    return v0
.end method

.method static synthetic e(LaKU;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, LaKU;->b:I

    return v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 4

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaKU;->a:LaKV;

    invoke-virtual {v0}, LaKV;->getCorePoolSize()I

    move-result v0

    iget v1, p0, LaKU;->b:I

    sub-int v1, v0, v1

    .line 130
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 131
    iget-object v2, p0, LaKU;->a:LaKV;

    iget-object v3, p0, LaKU;->a:LaKW;

    invoke-interface {v3}, LaKW;->a()Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, LaKV;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_0
    iget v0, p0, LaKU;->b:I

    iput v0, p0, LaKU;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, LaKU;->a:LaKV;

    invoke-virtual {v0}, LaKV;->isShutdown()Z

    move-result v0

    return v0
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, LaKU;->a:LaKV;

    invoke-virtual {v0, p1, p2, p3}, LaKV;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, LaKU;->a:LaKV;

    invoke-virtual {v0}, LaKV;->shutdown()V

    .line 143
    return-void
.end method
