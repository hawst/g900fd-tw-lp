.class Lpp;
.super Lpo;
.source "ReflectedField.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lpo",
        "<",
        "Ljava/lang/reflect/Field;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/reflect/Field;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lpo;-><init>(Ljava/lang/Object;Ljava/lang/reflect/AccessibleObject;)V

    .line 16
    return-void
.end method


# virtual methods
.method a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 20
    :try_start_0
    iget-object v0, p0, Lpp;->a:Ljava/lang/reflect/AccessibleObject;

    check-cast v0, Ljava/lang/reflect/Field;

    iget-object v1, p0, Lpp;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    new-instance v1, Lpm;

    invoke-direct {v1, v0}, Lpm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 28
    :try_start_0
    iget-object v0, p0, Lpp;->a:Ljava/lang/reflect/AccessibleObject;

    check-cast v0, Ljava/lang/reflect/Field;

    iget-object v1, p0, Lpp;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 30
    new-instance v1, Lpm;

    invoke-direct {v1, v0}, Lpm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
