.class LqM;
.super Ljava/lang/Object;
.source "Tracker.java"

# interfaces
.implements LqP;


# static fields
.field private static a:LoS;


# instance fields
.field private final a:LQr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LQr;)V
    .locals 4

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p3, p0, LqM;->a:LQr;

    .line 105
    sget-object v0, LqM;->a:LoS;

    if-nez v0, :cond_0

    .line 106
    invoke-static {p1}, Low;->a(Landroid/content/Context;)Low;

    move-result-object v0

    .line 107
    invoke-virtual {v0, p2}, Low;->a(Ljava/lang/String;)LoS;

    move-result-object v0

    sput-object v0, LqM;->a:LoS;

    .line 108
    iget-object v0, p0, LqM;->a:LQr;

    const-string v1, "analyticsSampleRatePercent"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-interface {v0, v1, v2, v3}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v0

    .line 110
    sget-object v2, LqM;->a:LoS;

    invoke-virtual {v2, v0, v1}, LoS;->a(D)V

    .line 114
    :try_start_0
    sget v0, Lxi;->app_name:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 119
    :goto_0
    invoke-static {}, Laml;->a()Ljava/lang/String;

    move-result-object v1

    .line 120
    sget-object v2, LqM;->a:LoS;

    invoke-virtual {v2, v0}, LoS;->a(Ljava/lang/String;)V

    .line 121
    sget-object v0, LqM;->a:LoS;

    invoke-virtual {v0, v1}, LoS;->b(Ljava/lang/String;)V

    .line 125
    :goto_1
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    const-string v0, "unknown"

    goto :goto_0

    .line 123
    :cond_0
    sget-object v0, LqM;->a:LoS;

    invoke-virtual {v0}, LoS;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, LqM;->a:LQr;

    const-string v1, "googleAnalyticsDispatchPeriodS"

    const/16 v2, 0x3c

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 131
    invoke-static {}, Lod;->a()Lod;

    move-result-object v1

    invoke-virtual {v1, v0}, Lod;->a(I)V

    .line 132
    sget-object v0, LqM;->a:LoS;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LoS;->a(Z)V

    .line 133
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 153
    sget-object v0, LqM;->a:LoS;

    invoke-virtual {v0, p1}, LoS;->c(Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 142
    sget-object v0, LqM;->a:LoS;

    invoke-virtual {v0, p1, p2, p3, p4}, LoS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 143
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 163
    sget-object v0, LqM;->a:LoS;

    invoke-virtual {v0, p1, p2}, LoS;->a(Ljava/lang/String;Z)V

    .line 164
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 137
    invoke-static {}, Lod;->a()Lod;

    move-result-object v0

    invoke-virtual {v0}, Lod;->a()V

    .line 138
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 158
    sget-object v0, LqM;->a:LoS;

    invoke-virtual {v0, p1}, LoS;->d(Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6

    .prologue
    .line 148
    sget-object v0, LqM;->a:LoS;

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LoS;->a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 149
    return-void
.end method
