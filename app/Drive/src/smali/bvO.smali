.class public final LbvO;
.super Ljava/lang/Object;
.source "GellyInjectorStore.java"

# interfaces
.implements LbvZ;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    iput-object p1, p0, LbvO;->a:Ljava/lang/String;

    .line 236
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, LbvO;->a:Ljava/lang/String;

    return-object v0
.end method

.method public annotationType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    const-class v0, LbvZ;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 250
    if-ne p1, p0, :cond_0

    .line 251
    const/4 v0, 0x1

    .line 257
    :goto_0
    return v0

    .line 252
    :cond_0
    instance-of v0, p1, LbvZ;

    if-nez v0, :cond_1

    .line 253
    const/4 v0, 0x0

    goto :goto_0

    .line 255
    :cond_1
    check-cast p1, LbvZ;

    .line 256
    iget-object v0, p0, LbvO;->a:Ljava/lang/String;

    .line 257
    invoke-interface {p1}, LbvZ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbvM;->e(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 265
    const-string v0, "value"

    .line 266
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x7f

    iget-object v1, p0, LbvO;->a:Ljava/lang/String;

    .line 267
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.google.inject.multibindings.OptionalBinder.Actual(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LbvO;->a:Ljava/lang/String;

    .line 275
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
