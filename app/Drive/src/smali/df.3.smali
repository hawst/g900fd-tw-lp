.class public abstract Ldf;
.super Ljava/lang/Object;
.source "ActionProvider.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private a:Ldg;

.field private a:Ldh;


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ldf;->a:Landroid/content/Context;

    return-object v0
.end method

.method public abstract a()Landroid/view/View;
.end method

.method public a(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Ldf;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Ldf;->a:Ldh;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldf;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Ldf;->a:Ldh;

    invoke-virtual {p0}, Ldf;->b()Z

    move-result v1

    invoke-interface {v0, v1}, Ldh;->a(Z)V

    .line 148
    :cond_0
    return-void
.end method

.method public a(Landroid/view/SubMenu;)V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public a(Ldg;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Ldf;->a:Ldg;

    .line 227
    return-void
.end method

.method public a(Ldh;)V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Ldf;->a:Ldh;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 237
    const-string v0, "ActionProvider(support)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " instance while it is still in use somewhere else?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    iput-object p1, p0, Ldf;->a:Ldh;

    .line 242
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Ldf;->a:Ldg;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Ldf;->a:Ldg;

    invoke-interface {v0, p1}, Ldg;->a(Z)V

    .line 220
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method
