.class final LaTw;
.super LaTe;


# instance fields
.field private final a:LaDk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaDk",
            "<",
            "LaZh;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:LaTo;


# direct methods
.method public constructor <init>(LaTo;LaDk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "LaZh;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LaTw;->a:LaTo;

    invoke-direct {p0}, LaTe;-><init>()V

    iput-object p2, p0, LaTw;->a:LaDk;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 5

    invoke-static {}, LaTx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Avatar callback: status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pfd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LaTx;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0, p2}, LaTo;->a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v1, p0, LaTw;->a:LaTo;

    new-instance v2, LaTq;

    iget-object v3, p0, LaTw;->a:LaTo;

    iget-object v4, p0, LaTw;->a:LaDk;

    invoke-direct {v2, v3, v4, v0, p3}, LaTq;-><init>(LaTo;LaDk;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;)V

    invoke-virtual {v1, v2}, LaTo;->b(LaRy;)V

    return-void
.end method
