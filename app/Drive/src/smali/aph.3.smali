.class Laph;
.super Ljava/lang/Object;
.source "ThumbnailFetcher.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbsJ",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lapd;

.field private final a:Lcom/google/android/apps/docs/utils/FetchSpec;


# direct methods
.method public constructor <init>(Lapd;Lcom/google/android/apps/docs/utils/FetchSpec;)V
    .locals 1

    .prologue
    .line 272
    iput-object p1, p0, Laph;->a:Lapd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/FetchSpec;

    iput-object v0, p0, Laph;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 274
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Laph;->a:Lapd;

    invoke-static {v0}, Lapd;->a(Lapd;)LbjF;

    move-result-object v0

    iget-object v1, p0, Laph;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    sget-object v2, LaGx;->b:LaGx;

    invoke-interface {v0, v1, v2}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 286
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 278
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 279
    iget-object v0, p0, Laph;->a:Lapd;

    invoke-static {v0}, Lapd;->a(Lapd;)LbjF;

    move-result-object v0

    iget-object v1, p0, Laph;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    sget-object v2, LaGx;->c:LaGx;

    invoke-interface {v0, v1, v2}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 281
    :cond_0
    return-void
.end method
