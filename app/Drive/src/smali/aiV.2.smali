.class public LaiV;
.super Laji;
.source "ContextScopeImpl.java"

# interfaces
.implements LaiU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laji",
        "<",
        "LaiW;",
        ">;",
        "LaiU;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Laji;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()LajA;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-virtual {p0}, LaiV;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiW;

    .line 30
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "no active context scope object, missing a scope.enter"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 32
    invoke-virtual {v0}, LaiW;->a()LajA;

    move-result-object v0

    return-object v0

    :cond_0
    move v1, v2

    .line 30
    goto :goto_0
.end method

.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, LaiV;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiW;

    .line 20
    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {v0}, LaiW;->a()Landroid/content/Context;

    move-result-object v0

    .line 23
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
