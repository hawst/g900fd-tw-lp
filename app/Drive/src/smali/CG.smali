.class public LCG;
.super Ljava/lang/Object;
.source "ListAdapterVisibleRange.java"

# interfaces
.implements LzN;


# instance fields
.field private final a:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, LCG;->a:Landroid/widget/ListView;

    .line 19
    return-void
.end method


# virtual methods
.method public a()LalS;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    iget-object v0, p0, LCG;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 25
    :cond_0
    invoke-static {v1, v1}, LalS;->b(II)LalS;

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LCG;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iget-object v1, p0, LCG;->a:Landroid/widget/ListView;

    .line 29
    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 28
    invoke-static {v0, v1}, LalS;->a(II)LalS;

    move-result-object v0

    goto :goto_0
.end method

.method public b()LalS;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    iget-object v0, p0, LCG;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 35
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 36
    :cond_0
    invoke-static {v3, v3}, LalS;->b(II)LalS;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 39
    :cond_1
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 40
    iget-object v1, p0, LCG;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v3, v0}, LalX;->a(III)I

    move-result v1

    .line 41
    iget-object v2, p0, LCG;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v3, v0}, LalX;->a(III)I

    move-result v0

    .line 43
    add-int/lit8 v0, v0, 0x1

    invoke-static {v1, v0}, LalS;->a(II)LalS;

    move-result-object v0

    goto :goto_0
.end method
