.class public LAJ;
.super Ljava/lang/Object;
.source "DocListAdapterState.java"


# instance fields
.field private final a:LAO;

.field private final a:LBf;

.field private final a:LCE;

.field private final a:LCl;

.field private final a:LCr;

.field private a:LCv;

.field private final a:LDM;

.field private final a:LDg;

.field private final a:LIK;

.field private final a:LIf;

.field private a:LaGA;

.field private final a:LabF;

.field private final a:LabI;

.field private final a:LamF;

.field private final a:LapF;

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Landroid/content/Context;LaGA;Lcom/google/android/gms/drive/database/data/EntrySpec;LIf;LIK;LtK;LCl;LapF;LamF;LBf;LCr;LDU;LDn;LCt;LabF;ZLzO;)V
    .locals 11

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p2, p0, LAJ;->a:LaGA;

    .line 106
    move-object/from16 v0, p6

    iput-object v0, p0, LAJ;->a:LtK;

    .line 107
    move-object/from16 v0, p7

    iput-object v0, p0, LAJ;->a:LCl;

    .line 108
    move-object/from16 v0, p8

    iput-object v0, p0, LAJ;->a:LapF;

    .line 109
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LIf;

    iput-object v2, p0, LAJ;->a:LIf;

    .line 110
    invoke-static/range {p5 .. p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LIK;

    iput-object v2, p0, LAJ;->a:LIK;

    .line 111
    invoke-static/range {p11 .. p11}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LCr;

    iput-object v2, p0, LAJ;->a:LCr;

    .line 112
    invoke-static/range {p10 .. p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LBf;

    iput-object v2, p0, LAJ;->a:LBf;

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 115
    invoke-interface/range {p7 .. p7}, LCl;->a()LCn;

    move-result-object v7

    .line 116
    move-object/from16 v0, p9

    iput-object v0, p0, LAJ;->a:LamF;

    .line 117
    new-instance v2, LCE;

    iget-object v8, p0, LAJ;->a:LIf;

    move-object v3, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move/from16 v9, p16

    invoke-direct/range {v2 .. v9}, LCE;-><init>(Landroid/content/Context;LIK;LamF;LBf;LCn;LIf;Z)V

    iput-object v2, p0, LAJ;->a:LCE;

    .line 119
    const-string v2, "%s"

    const-string v3, "%s"

    move-object/from16 v0, p12

    move-object/from16 v1, p10

    invoke-virtual {v0, v1, v7, v2, v3}, LDU;->a(LBf;LCn;Ljava/lang/String;Ljava/lang/String;)LDM;

    move-result-object v2

    iput-object v2, p0, LAJ;->a:LDM;

    .line 122
    new-instance v2, LDf;

    sget v3, Lxc;->title:I

    sget v4, Lxc;->doc_icon:I

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, LDf;-><init>(IIZ)V

    move-object/from16 v0, p13

    move-object/from16 v1, p14

    invoke-virtual {v0, v1, v2}, LDn;->a(LCt;LDq;)LDg;

    move-result-object v2

    iput-object v2, p0, LAJ;->a:LDg;

    .line 124
    move-object/from16 v0, p15

    iput-object v0, p0, LAJ;->a:LabF;

    .line 125
    new-instance v2, LabI;

    invoke-direct {v2, p1}, LabI;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LAJ;->a:LabI;

    .line 126
    invoke-static {v10}, LAE;->a(Landroid/content/res/Resources;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v2

    iput-object v2, p0, LAJ;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 127
    sget-object v2, LzO;->c:LzO;

    move-object/from16 v0, p17

    invoke-virtual {v0, v2}, LzO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, LAM;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, LAM;-><init>(LAJ;LAK;)V

    :goto_0
    iput-object v2, p0, LAJ;->a:LAO;

    .line 129
    iput-object p3, p0, LAJ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 130
    invoke-direct {p0, p2}, LAJ;->b(LaGA;)V

    .line 131
    return-void

    .line 127
    :cond_0
    new-instance v2, LAN;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, LAN;-><init>(LAJ;LAK;)V

    goto :goto_0
.end method

.method static synthetic a(LAJ;Landroid/view/View;)LBe;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, LAJ;->a(Landroid/view/View;)LBe;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)LBe;
    .locals 2

    .prologue
    .line 445
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBe;

    .line 446
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 447
    return-object v0

    .line 446
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic a(LAJ;)LBf;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LBf;

    return-object v0
.end method

.method static synthetic a(LAJ;)LCE;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LCE;

    return-object v0
.end method

.method static synthetic a(LAJ;)LCr;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LCr;

    return-object v0
.end method

.method static synthetic a(LAJ;)LCv;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LCv;

    return-object v0
.end method

.method static synthetic a(LAJ;)LDM;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LDM;

    return-object v0
.end method

.method static synthetic a(LAJ;)LDg;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LDg;

    return-object v0
.end method

.method static synthetic a(LAJ;)LIK;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LIK;

    return-object v0
.end method

.method static synthetic a(LAJ;)LIf;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LIf;

    return-object v0
.end method

.method static synthetic a(LAJ;)LabF;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LabF;

    return-object v0
.end method

.method static synthetic a(LAJ;)LabI;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LabI;

    return-object v0
.end method

.method static synthetic a(LAJ;)LamF;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LamF;

    return-object v0
.end method

.method static synthetic a(LAJ;)LapF;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LapF;

    return-object v0
.end method

.method static synthetic a(LAJ;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method static synthetic a(LAJ;)LtK;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LAJ;->a:LtK;

    return-object v0
.end method

.method private b(LaGA;)V
    .locals 1

    .prologue
    .line 154
    iput-object p1, p0, LAJ;->a:LCv;

    .line 155
    iget-object v0, p0, LAJ;->a:LCE;

    invoke-virtual {v0, p1}, LCE;->a(LCv;)V

    .line 156
    return-void
.end method


# virtual methods
.method public a()LIf;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, LAJ;->a:LIf;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, LAJ;->a:LCv;

    invoke-interface {v0}, LCv;->e()Ljava/lang/String;

    move-result-object v0

    .line 475
    invoke-static {v0}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    .line 476
    sget-object v1, LaGn;->b:LaGn;

    invoke-virtual {v1, v0}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, LAJ;->a:LaGA;

    iget-object v1, p0, LAJ;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 478
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a(LaGA;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    .line 481
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGA;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, LAJ;->a:LaGA;

    .line 145
    invoke-direct {p0, p1}, LAJ;->b(LaGA;)V

    .line 146
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, LAJ;->a:LDg;

    if-eqz v0, :cond_0

    .line 491
    invoke-direct {p0, p1}, LAJ;->a(Landroid/view/View;)LBe;

    move-result-object v0

    .line 492
    if-eqz v0, :cond_0

    instance-of v1, v0, LBy;

    if-eqz v1, :cond_0

    .line 493
    check-cast v0, LBy;

    .line 494
    iget-object v0, v0, LBy;->a:LDi;

    .line 495
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    iget-object v1, p0, LAJ;->a:LDg;

    invoke-interface {v1, v0}, LDg;->a(LDi;)V

    .line 499
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;LaGA;)V
    .locals 3

    .prologue
    .line 134
    invoke-direct {p0, p1}, LAJ;->a(Landroid/view/View;)LBe;

    move-result-object v0

    .line 136
    invoke-interface {p2}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 137
    iget-object v2, p0, LAJ;->a:LapF;

    invoke-interface {v2}, LapF;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 138
    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 140
    iget-object v2, p0, LAJ;->a:LAO;

    invoke-interface {v2, v0, p1, p2, v1}, LAO;->a(LBe;Landroid/view/View;LaGA;Z)V

    .line 141
    return-void
.end method
