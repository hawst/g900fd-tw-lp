.class public final Ltq;
.super Ljava/lang/Thread;
.source "DownloadActivity.java"


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Landroid/net/Uri;

.field final synthetic a:Landroid/os/Handler;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 64
    iput-object p2, p0, Ltq;->a:Ljava/lang/String;

    iput-object p3, p0, Ltq;->a:Landroid/content/Context;

    iput-object p4, p0, Ltq;->a:Landroid/net/Uri;

    iput-object p5, p0, Ltq;->a:Landroid/content/Intent;

    iput-object p6, p0, Ltq;->a:Landroid/os/Handler;

    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 67
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 69
    iget-object v1, p0, Ltq;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Laln;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 70
    new-instance v1, Lalp;

    invoke-direct {v1}, Lalp;-><init>()V

    .line 76
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 80
    :cond_0
    iget-object v0, p0, Ltq;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 81
    iget-object v2, p0, Ltq;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 82
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 83
    invoke-interface {v1, v0, v2}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 86
    iget-object v0, p0, Ltq;->a:Landroid/content/Context;

    const-string v1, "download"

    .line 87
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 88
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ltq;->a:Landroid/content/Context;

    sget v3, Lxi;->downloaded_from_drive:I

    .line 89
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p0, Ltq;->a:Landroid/content/Intent;

    .line 91
    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    .line 92
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 93
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x1

    .line 88
    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J

    .line 96
    sget v0, Lxi;->download_from_drive_complete:I

    iget-object v1, p0, Ltq;->a:Landroid/os/Handler;

    iget-object v2, p0, Ltq;->a:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/app/DownloadActivity;->a(ILandroid/os/Handler;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    sget v0, Lxi;->download_from_drive_failed:I

    iget-object v1, p0, Ltq;->a:Landroid/os/Handler;

    iget-object v2, p0, Ltq;->a:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/app/DownloadActivity;->a(ILandroid/os/Handler;Landroid/content/Context;)V

    goto :goto_0
.end method
