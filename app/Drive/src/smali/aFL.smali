.class public final enum LaFL;
.super Ljava/lang/Enum;
.source "SqlWhereClause.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFL;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFL;

.field private static final synthetic a:[LaFL;

.field public static final enum b:LaFL;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 91
    new-instance v0, LaFL;

    const-string v1, "AND"

    invoke-direct {v0, v1, v2}, LaFL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaFL;->a:LaFL;

    .line 92
    new-instance v0, LaFL;

    const-string v1, "OR"

    invoke-direct {v0, v1, v3}, LaFL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaFL;->b:LaFL;

    .line 90
    const/4 v0, 0x2

    new-array v0, v0, [LaFL;

    sget-object v1, LaFL;->a:LaFL;

    aput-object v1, v0, v2

    sget-object v1, LaFL;->b:LaFL;

    aput-object v1, v0, v3

    sput-object v0, LaFL;->a:[LaFL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFL;
    .locals 1

    .prologue
    .line 90
    const-class v0, LaFL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFL;

    return-object v0
.end method

.method public static values()[LaFL;
    .locals 1

    .prologue
    .line 90
    sget-object v0, LaFL;->a:[LaFL;

    invoke-virtual {v0}, [LaFL;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFL;

    return-object v0
.end method


# virtual methods
.method public varargs a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 4

    .prologue
    .line 98
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()LaFK;

    move-result-object v1

    .line 101
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    .line 102
    invoke-virtual {v1, p0, v3}, LaFK;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFK;

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual {v1}, LaFK;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;",
            ">;)",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;"
        }
    .end annotation

    .prologue
    .line 109
    const/4 v1, 0x0

    .line 110
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 111
    if-nez v1, :cond_0

    .line 112
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()LaFK;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 116
    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {v1, p0, v0}, LaFK;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFK;

    move-object v0, v1

    goto :goto_1

    .line 117
    :cond_1
    if-nez v1, :cond_3

    .line 118
    sget-object v0, LaFL;->a:LaFL;

    invoke-virtual {p0, v0}, LaFL;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->d:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 120
    :goto_2
    return-object v0

    .line 118
    :cond_2
    sget-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->c:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    goto :goto_2

    .line 120
    :cond_3
    invoke-virtual {v1}, LaFK;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    goto :goto_2
.end method
