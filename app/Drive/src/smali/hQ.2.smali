.class LhQ;
.super Ljava/lang/Object;
.source "ActionBarActivityDelegate.java"

# interfaces
.implements Ll;


# instance fields
.field final synthetic a:LhO;


# direct methods
.method private constructor <init>(LhO;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, LhQ;->a:LhO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LhO;LhP;)V
    .locals 0

    .prologue
    .line 265
    invoke-direct {p0, p1}, LhQ;-><init>(LhO;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 270
    iget-object v0, p0, LhQ;->a:LhO;

    invoke-virtual {v0}, LhO;->a()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    iget-object v2, p0, LhQ;->a:LhO;

    invoke-virtual {v2}, LhO;->a()I

    move-result v2

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 272
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 273
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 274
    return-object v1
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, LhQ;->a:LhO;

    invoke-virtual {v0}, LhO;->b()LhJ;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {v0, p1}, LhJ;->b(I)V

    .line 297
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, LhQ;->a:LhO;

    invoke-virtual {v0}, LhO;->b()LhJ;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {v0, p1}, LhJ;->a(Landroid/graphics/drawable/Drawable;)V

    .line 287
    invoke-virtual {v0, p2}, LhJ;->b(I)V

    .line 289
    :cond_0
    return-void
.end method
