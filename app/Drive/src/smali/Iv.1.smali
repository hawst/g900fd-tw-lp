.class public final LIv;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIR;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIe;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIi;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LII;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIs;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIF;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIG;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIS;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LIh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 46
    iput-object p1, p0, LIv;->a:LbrA;

    .line 47
    const-class v0, LIR;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->a:Lbsk;

    .line 50
    const-class v0, LIe;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->b:Lbsk;

    .line 53
    const-class v0, LIi;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->c:Lbsk;

    .line 56
    const-class v0, LII;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->d:Lbsk;

    .line 59
    const-class v0, LIs;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->e:Lbsk;

    .line 62
    const-class v0, LIF;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->f:Lbsk;

    .line 65
    const-class v0, LIG;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->g:Lbsk;

    .line 68
    const-class v0, LIS;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->h:Lbsk;

    .line 71
    const-class v0, LIh;

    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LIv;->i:Lbsk;

    .line 74
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 116
    sparse-switch p1, :sswitch_data_0

    .line 216
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :sswitch_0
    new-instance v2, LIR;

    iget-object v0, p0, LIv;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 121
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LIv;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 119
    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LIv;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->S:Lbsk;

    .line 127
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LIv;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->S:Lbsk;

    .line 125
    invoke-static {v1, v3}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaKM;

    invoke-direct {v2, v0, v1}, LIR;-><init>(Landroid/content/Context;LaKM;)V

    move-object v0, v2

    .line 214
    :goto_0
    return-object v0

    .line 134
    :sswitch_1
    new-instance v1, LIe;

    iget-object v0, p0, LIv;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->S:Lbsk;

    .line 137
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LIv;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->S:Lbsk;

    .line 135
    invoke-static {v0, v2}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKM;

    invoke-direct {v1, v0}, LIe;-><init>(LaKM;)V

    move-object v0, v1

    .line 142
    goto :goto_0

    .line 144
    :sswitch_2
    new-instance v3, LIi;

    iget-object v0, p0, LIv;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->w:Lbsk;

    .line 147
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LIv;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->w:Lbsk;

    .line 145
    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LIv;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LIv;

    iget-object v1, v1, LIv;->b:Lbsk;

    .line 153
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LIv;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LIv;

    iget-object v2, v2, LIv;->b:Lbsk;

    .line 151
    invoke-static {v1, v2}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LIe;

    iget-object v2, p0, LIv;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->u:Lbsk;

    .line 159
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LIv;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->u:Lbsk;

    .line 157
    invoke-static {v2, v4}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laja;

    invoke-direct {v3, v0, v1, v2}, LIi;-><init>(Laja;LIe;Laja;)V

    move-object v0, v3

    .line 164
    goto :goto_0

    .line 166
    :sswitch_3
    new-instance v3, LII;

    iget-object v0, p0, LIv;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LIv;

    iget-object v0, v0, LIv;->h:Lbsk;

    .line 169
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LIv;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LIv;

    iget-object v1, v1, LIv;->h:Lbsk;

    .line 167
    invoke-static {v0, v1}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIS;

    iget-object v1, p0, LIv;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LIv;

    iget-object v1, v1, LIv;->a:Lbsk;

    .line 175
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LIv;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LIv;

    iget-object v2, v2, LIv;->a:Lbsk;

    .line 173
    invoke-static {v1, v2}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LIR;

    iget-object v2, p0, LIv;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LIv;

    iget-object v2, v2, LIv;->f:Lbsk;

    .line 181
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LIv;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LIv;

    iget-object v4, v4, LIv;->f:Lbsk;

    .line 179
    invoke-static {v2, v4}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LIF;

    invoke-direct {v3, v0, v1, v2}, LII;-><init>(LIS;LIR;LIF;)V

    move-object v0, v3

    .line 186
    goto/16 :goto_0

    .line 188
    :sswitch_4
    new-instance v0, LIs;

    invoke-direct {v0}, LIs;-><init>()V

    goto/16 :goto_0

    .line 192
    :sswitch_5
    new-instance v1, LIF;

    iget-object v0, p0, LIv;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 195
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LIv;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 193
    invoke-static {v0, v2}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LIF;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 200
    goto/16 :goto_0

    .line 202
    :sswitch_6
    new-instance v0, LIG;

    invoke-direct {v0}, LIG;-><init>()V

    goto/16 :goto_0

    .line 206
    :sswitch_7
    new-instance v1, LIS;

    iget-object v0, p0, LIv;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 209
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LIv;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 207
    invoke-static {v0, v2}, LIv;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LIS;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 214
    goto/16 :goto_0

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0x136 -> :sswitch_3
        0x342 -> :sswitch_6
        0x357 -> :sswitch_4
        0x51e -> :sswitch_0
        0x51f -> :sswitch_1
        0x520 -> :sswitch_2
        0x521 -> :sswitch_7
        0x522 -> :sswitch_5
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 231
    packed-switch p2, :pswitch_data_0

    .line 242
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :pswitch_0
    check-cast p1, LIC;

    .line 235
    iget-object v0, p0, LIv;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LIv;

    iget-object v0, v0, LIv;->c:Lbsk;

    .line 238
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIi;

    .line 235
    invoke-virtual {p1, v0}, LIC;->provideEntriesGrouperFactory(LIi;)LIh;

    move-result-object v0

    return-object v0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x2ac
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 81
    const-class v0, LIR;

    iget-object v1, p0, LIv;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 82
    const-class v0, LIe;

    iget-object v1, p0, LIv;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 83
    const-class v0, LIi;

    iget-object v1, p0, LIv;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 84
    const-class v0, LII;

    iget-object v1, p0, LIv;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 85
    const-class v0, LIs;

    iget-object v1, p0, LIv;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 86
    const-class v0, LIF;

    iget-object v1, p0, LIv;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 87
    const-class v0, LIG;

    iget-object v1, p0, LIv;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 88
    const-class v0, LIS;

    iget-object v1, p0, LIv;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 89
    const-class v0, LIh;

    iget-object v1, p0, LIv;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LIv;->a(Ljava/lang/Class;Lbsk;)V

    .line 90
    iget-object v0, p0, LIv;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x51e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 92
    iget-object v0, p0, LIv;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x51f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 94
    iget-object v0, p0, LIv;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x520

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 96
    iget-object v0, p0, LIv;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x136

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 98
    iget-object v0, p0, LIv;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x357

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 100
    iget-object v0, p0, LIv;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x522

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 102
    iget-object v0, p0, LIv;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x342

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 104
    iget-object v0, p0, LIv;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x521

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 106
    iget-object v0, p0, LIv;->i:Lbsk;

    const-class v1, LIC;

    const/16 v2, 0x2ac

    invoke-virtual {p0, v1, v2}, LIv;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 108
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 223
    .line 225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method
