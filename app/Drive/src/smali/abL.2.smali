.class LabL;
.super Landroid/os/HandlerThread;
.source "ContactPhotoLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:LabI;

.field private final a:Landroid/content/ContentResolver;

.field private a:Landroid/os/Handler;

.field private final a:Ljava/lang/StringBuilder;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LabI;Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, LabL;->a:LabI;

    .line 55
    const-string v0, "ContactPhotoLoader"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LabL;->a:Ljava/lang/StringBuilder;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LabL;->a:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LabL;->b:Ljava/util/ArrayList;

    .line 56
    iput-object p2, p0, LabL;->a:Landroid/content/ContentResolver;

    .line 57
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 81
    const-string v0, "ContactPhotoLoader"

    const-string v1, "Loading photos from DB."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v0, p0, LabL;->a:LabI;

    iget-object v1, p0, LabL;->a:Ljava/util/ArrayList;

    iget-object v2, p0, LabL;->b:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, LabI;->a(LabI;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 84
    iget-object v0, p0, LabL;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 85
    if-nez v1, :cond_1

    .line 125
    :cond_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, LabL;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 90
    iget-object v0, p0, LabL;->a:Ljava/lang/StringBuilder;

    const-string v2, "_id IN("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v7

    .line 91
    :goto_0
    if-ge v0, v1, :cond_3

    .line 92
    if-eqz v0, :cond_2

    .line 93
    iget-object v2, p0, LabL;->a:Ljava/lang/StringBuilder;

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 95
    :cond_2
    iget-object v2, p0, LabL;->a:Ljava/lang/StringBuilder;

    const/16 v3, 0x3f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_3
    iget-object v0, p0, LabL;->a:Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    :try_start_0
    iget-object v0, p0, LabL;->a:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, LabL;->a:LabI;

    invoke-static {v2}, LabI;->a(LabI;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LabL;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LabL;->b:Ljava/util/ArrayList;

    .line 102
    invoke-static {}, LabI;->a()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 101
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 104
    if-eqz v1, :cond_5

    .line 105
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 106
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 107
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 108
    iget-object v3, p0, LabL;->a:LabI;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5, v2}, LabI;->a(LabI;J[B)V

    .line 109
    iget-object v2, p0, LabL;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 110
    const-string v2, "ContactPhotoLoader"

    const-string v3, "Got photo for contact ID %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 114
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_4

    .line 115
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 114
    :cond_5
    if-eqz v1, :cond_6

    .line 115
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 121
    :cond_6
    iget-object v0, p0, LabL;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v7

    .line 122
    :goto_3
    if-ge v1, v2, :cond_0

    .line 123
    iget-object v3, p0, LabL;->a:LabI;

    iget-object v0, p0, LabL;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5, v6}, LabI;->a(LabI;J[B)V

    .line 122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 114
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, LabL;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, LabL;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LabL;->a:Landroid/os/Handler;

    .line 66
    :cond_0
    iget-object v0, p0, LabL;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 67
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, LabL;->b()V

    .line 76
    iget-object v0, p0, LabL;->a:LabI;

    invoke-static {v0}, LabI;->a(LabI;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 77
    const/4 v0, 0x1

    return v0
.end method
