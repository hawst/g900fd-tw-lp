.class public final LqD;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LqE;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LqF;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LqK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, LqN;

    sput-object v0, LqD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LqD;->a:LbrA;

    .line 42
    const-class v0, LqE;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LqD;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LqD;->a:Lbsk;

    .line 45
    const-class v0, LqF;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LqD;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LqD;->b:Lbsk;

    .line 48
    const-class v0, LqK;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LqD;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LqD;->c:Lbsk;

    .line 51
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 75
    sparse-switch p1, :sswitch_data_0

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :sswitch_0
    new-instance v1, LqE;

    iget-object v0, p0, LqD;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->b:Lbsk;

    .line 80
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LqD;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LqD;

    iget-object v2, v2, LqD;->b:Lbsk;

    .line 78
    invoke-static {v0, v2}, LqD;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqF;

    invoke-direct {v1, v0}, LqE;-><init>(LqF;)V

    move-object v0, v1

    .line 129
    :goto_0
    return-object v0

    .line 87
    :sswitch_1
    new-instance v2, LqF;

    iget-object v0, p0, LqD;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->h:Lbsk;

    .line 90
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LqD;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->h:Lbsk;

    .line 88
    invoke-static {v0, v1}, LqD;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTh;

    iget-object v1, p0, LqD;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 96
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LqD;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 94
    invoke-static {v1, v3}, LqD;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    invoke-direct {v2, v0, v1}, LqF;-><init>(LTh;LQr;)V

    move-object v0, v2

    .line 101
    goto :goto_0

    .line 103
    :sswitch_2
    new-instance v4, LqK;

    iget-object v0, p0, LqD;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 106
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LqD;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 104
    invoke-static {v0, v1}, LqD;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LqD;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lbrx;

    iget-object v1, v1, Lbrx;->e:Lbsk;

    .line 112
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LqD;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lbrx;

    iget-object v2, v2, Lbrx;->e:Lbsk;

    .line 110
    invoke-static {v1, v2}, LqD;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, LqD;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 118
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LqD;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 116
    invoke-static {v2, v3}, LqD;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    iget-object v3, p0, LqD;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 124
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LqD;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LQH;

    iget-object v5, v5, LQH;->d:Lbsk;

    .line 122
    invoke-static {v3, v5}, LqD;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LQr;

    invoke-direct {v4, v0, v1, v2, v3}, LqK;-><init>(Laja;Ljava/lang/String;LtK;LQr;)V

    move-object v0, v4

    .line 129
    goto/16 :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        0xe -> :sswitch_2
        0x7b -> :sswitch_1
        0x2bf -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 146
    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 58
    const-class v0, LqE;

    iget-object v1, p0, LqD;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LqD;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, LqF;

    iget-object v1, p0, LqD;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LqD;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    const-class v0, LqK;

    iget-object v1, p0, LqD;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LqD;->a(Ljava/lang/Class;Lbsk;)V

    .line 61
    iget-object v0, p0, LqD;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2bf

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 63
    iget-object v0, p0, LqD;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x7b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 65
    iget-object v0, p0, LqD;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xe

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 138
    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
