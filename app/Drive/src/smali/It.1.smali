.class public final enum LIt;
.super Ljava/lang/Enum;
.source "FoldersThenTitleGrouper.java"

# interfaces
.implements LIy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIt;",
        ">;",
        "LIy;"
    }
.end annotation


# static fields
.field public static final enum a:LIt;

.field private static final synthetic a:[LIt;


# instance fields
.field private final a:LIr;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 29
    new-instance v0, LIt;

    const-string v1, "FILES"

    sget v2, Lxi;->title_grouper_files:I

    invoke-direct {v0, v1, v3, v2}, LIt;-><init>(Ljava/lang/String;II)V

    sput-object v0, LIt;->a:LIt;

    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [LIt;

    sget-object v1, LIt;->a:LIt;

    aput-object v1, v0, v3

    sput-object v0, LIt;->a:[LIt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    new-instance v0, LIr;

    invoke-direct {v0, p3}, LIr;-><init>(I)V

    iput-object v0, p0, LIt;->a:LIr;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LIt;
    .locals 1

    .prologue
    .line 28
    const-class v0, LIt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIt;

    return-object v0
.end method

.method public static values()[LIt;
    .locals 1

    .prologue
    .line 28
    sget-object v0, LIt;->a:[LIt;

    invoke-virtual {v0}, [LIt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIt;

    return-object v0
.end method


# virtual methods
.method public a(ZZ)LIB;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, LIt;->a:LIr;

    invoke-virtual {v0, p1, p2}, LIr;->a(ZZ)LIB;

    move-result-object v0

    return-object v0
.end method
