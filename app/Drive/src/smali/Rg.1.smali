.class public LRg;
.super LaGE;
.source "FilteringEntryCursor.java"


# instance fields
.field private final a:I

.field private final a:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(LaGA;LbmY;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGA;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0, p1}, LaGE;-><init>(LaGA;)V

    .line 17
    const/4 v0, -0x1

    iput v0, p0, LRg;->b:I

    .line 21
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    invoke-interface {p1}, LaGA;->a()I

    move-result v5

    .line 26
    invoke-static {}, Lbng;->a()Lbni;

    move-result-object v6

    move v4, v1

    move v0, v2

    move v3, v1

    .line 28
    :goto_0
    if-ge v4, v5, :cond_2

    .line 29
    invoke-interface {p1, v4}, LaGA;->a(I)Z

    .line 30
    invoke-interface {p1}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v7

    .line 31
    invoke-virtual {p2, v7}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 32
    if-eqz v0, :cond_0

    .line 33
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sub-int v7, v4, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Lbni;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbni;

    move v0, v1

    .line 36
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 28
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 38
    goto :goto_1

    .line 42
    :cond_2
    invoke-virtual {v6}, Lbni;->a()Lbng;

    move-result-object v0

    iput-object v0, p0, LRg;->a:Lbng;

    .line 43
    iput v3, p0, LRg;->a:I

    .line 44
    iget-object v0, p0, LRg;->a:LaGA;

    iget v1, p0, LRg;->b:I

    invoke-interface {v0, v1}, LaGA;->a(I)Z

    .line 45
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 114
    iget v0, p0, LRg;->a:I

    invoke-static {p1, v0}, LbiT;->b(II)I

    .line 115
    iget-object v0, p0, LRg;->a:Lbng;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbng;->a(Ljava/lang/Object;)Lbng;

    move-result-object v1

    .line 116
    invoke-virtual {v1}, Lbng;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 117
    invoke-virtual {v1}, Lbng;->a()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 118
    add-int/2addr v0, p1

    return v0

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, LRg;->a:I

    return v0
.end method

.method public a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 49
    if-gez p1, :cond_0

    .line 50
    const/4 v1, -0x1

    iput v1, p0, LRg;->b:I

    .line 65
    :goto_0
    return v0

    .line 54
    :cond_0
    iget v1, p0, LRg;->a:I

    if-lt p1, v1, :cond_1

    .line 55
    iget v1, p0, LRg;->a:I

    iput v1, p0, LRg;->b:I

    goto :goto_0

    .line 59
    :cond_1
    iget v0, p0, LRg;->b:I

    if-ne v0, p1, :cond_2

    .line 60
    const/4 v0, 0x1

    goto :goto_0

    .line 63
    :cond_2
    iput p1, p0, LRg;->b:I

    .line 64
    invoke-direct {p0, p1}, LRg;->a(I)I

    move-result v0

    .line 65
    iget-object v1, p0, LRg;->a:LaGA;

    invoke-interface {v1, v0}, LaGA;->a(I)Z

    move-result v0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, LRg;->b:I

    return v0
.end method

.method public j()Z
    .locals 2

    .prologue
    .line 90
    iget v0, p0, LRg;->b:I

    iget v1, p0, LRg;->a:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 95
    iget v0, p0, LRg;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, LRg;->a(I)Z

    move-result v0

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LRg;->a(I)Z

    move-result v0

    return v0
.end method
