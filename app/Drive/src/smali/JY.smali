.class LJY;
.super Ljava/lang/Object;
.source "SelectionModelItemImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Landroid/os/Parcelable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LJX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LJX",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LJV",
            "<",
            "LKd;",
            ">;",
            "LKa;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LJV",
            "<",
            "LKc;",
            ">;",
            "LKa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/selection/ItemKey;LJX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;",
            "LJX",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, LJY;->a:Z

    .line 35
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LJY;->a:Ljava/util/HashMap;

    .line 36
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LJY;->b:Ljava/util/HashMap;

    .line 39
    iput-object p2, p0, LJY;->a:LJX;

    .line 40
    iput-object p1, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 41
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, LJY;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0, p0}, LJX;->a(LJY;)V

    .line 138
    :cond_0
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/lang/Object;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<",
            "LJV",
            "<TT;>;",
            "LKa;",
            ">;TT;",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<*>;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 49
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    if-eqz p4, :cond_2

    move v1, v2

    .line 53
    :goto_0
    const/4 v3, 0x0

    .line 54
    new-instance v4, LJV;

    invoke-direct {v4, p2, p3}, LJV;-><init>(Ljava/lang/Object;Lcom/google/android/apps/docs/doclist/selection/ItemKey;)V

    .line 56
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKa;

    .line 57
    if-nez v0, :cond_0

    .line 58
    new-instance v0, LKa;

    const/4 v5, 0x0

    invoke-direct {v0, v5}, LKa;-><init>(LJZ;)V

    .line 59
    invoke-interface {p1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_0
    invoke-virtual {v0, v1}, LKa;->a(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 63
    invoke-interface {p1, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    invoke-direct {p0}, LJY;->a()V

    .line 70
    :goto_1
    return v2

    .line 52
    :cond_2
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method a()LKb;
    .locals 4

    .prologue
    .line 185
    new-instance v0, LKb;

    iget-boolean v1, p0, LJY;->a:Z

    iget-object v2, p0, LJY;->a:Ljava/util/HashMap;

    .line 186
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, LJY;->b:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LKb;-><init>(ZLjava/util/Set;Ljava/util/Set;)V

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    return-object v0
.end method

.method a(LKc;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKc;",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->b()V

    .line 162
    :try_start_0
    iget-object v0, p0, LJY;->b:Ljava/util/HashMap;

    invoke-direct {p0, v0, p1, p2, p3}, LJY;->a(Ljava/util/Map;Ljava/lang/Object;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)Z

    move-result v0

    .line 163
    invoke-direct {p0}, LJY;->a()V

    .line 164
    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_0
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->c()V

    .line 170
    return-void

    .line 168
    :catchall_0
    move-exception v0

    iget-object v1, p0, LJY;->a:LJX;

    invoke-virtual {v1}, LJX;->c()V

    throw v0
.end method

.method a(LKd;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKd;",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;Z)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->b()V

    .line 106
    :try_start_0
    iget-object v0, p0, LJY;->a:Ljava/util/HashMap;

    invoke-direct {p0, v0, p1, p2, p3}, LJY;->a(Ljava/util/Map;Ljava/lang/Object;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)Z

    move-result v0

    .line 108
    sget-object v1, LJZ;->a:[I

    invoke-virtual {p1}, LKd;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 122
    :cond_0
    :goto_0
    invoke-direct {p0}, LJY;->a()V

    .line 123
    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_1
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->c()V

    .line 129
    return-void

    .line 111
    :pswitch_0
    :try_start_1
    iget-object v1, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-interface {v1}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_0

    .line 115
    iget-object v2, p0, LJY;->a:LJX;

    sget-object v3, LKd;->c:LKd;

    iget-object v4, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-virtual {v2, v1, v3, v4, p3}, LJX;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;LKd;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    iget-object v1, p0, LJY;->a:LJX;

    invoke-virtual {v1}, LJX;->c()V

    throw v0

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method a(Z)V
    .locals 4

    .prologue
    .line 74
    iget-boolean v0, p0, LJY;->a:Z

    if-ne p1, v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->b()V

    .line 79
    :try_start_0
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0, p1}, LJX;->b(Z)V

    .line 80
    iput-boolean p1, p0, LJY;->a:Z

    .line 81
    iget-object v0, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_1

    .line 83
    iget-object v1, p0, LJY;->a:LJX;

    sget-object v2, LKd;->b:LKd;

    iget-object v3, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-virtual {v1, v0, v2, v3, p1}, LJX;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;LKd;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 86
    :cond_1
    iget-object v0, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    iget-object v0, p0, LJY;->a:LJX;

    iget-object v1, p0, LJY;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    sget-object v2, LKc;->a:LKc;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p1}, LJX;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;LKc;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 90
    :cond_2
    invoke-direct {p0}, LJY;->a()V

    .line 91
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    iget-object v0, p0, LJY;->a:LJX;

    invoke-virtual {v0}, LJX;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LJY;->a:LJX;

    invoke-virtual {v1}, LJX;->c()V

    throw v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, LJY;->a:Z

    return v0
.end method

.method b()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, LJY;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LJY;->a:Ljava/util/HashMap;

    .line 145
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LJY;->b:Ljava/util/HashMap;

    .line 146
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
