.class public final Lbtk;
.super Lbth;
.source "JsonObject.java"


# instance fields
.field private final a:Lbtu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtu",
            "<",
            "Ljava/lang/String;",
            "Lbth;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lbth;-><init>()V

    .line 33
    new-instance v0, Lbtu;

    invoke-direct {v0}, Lbtu;-><init>()V

    iput-object v0, p0, Lbtk;->a:Lbtu;

    return-void
.end method

.method private a(Ljava/lang/Object;)Lbth;
    .locals 1

    .prologue
    .line 122
    if-nez p1, :cond_0

    sget-object v0, Lbtj;->a:Lbtj;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbtn;

    invoke-direct {v0, p1}, Lbtn;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbtg;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0, p1}, Lbtu;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtg;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lbth;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0, p1}, Lbtu;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbth;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lbtk;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0, p1}, Lbtu;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtk;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Lbth;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0}, Lbtu;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lbth;)V
    .locals 1

    .prologue
    .line 54
    if-nez p2, :cond_0

    .line 55
    sget-object p2, Lbtj;->a:Lbtj;

    .line 57
    :cond_0
    iget-object v0, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0, p1, p2}, Lbtu;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0, p2}, Lbtk;->a(Ljava/lang/Object;)Lbth;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbtk;->a(Ljava/lang/String;Lbth;)V

    .line 102
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Number;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p2}, Lbtk;->a(Ljava/lang/Object;)Lbth;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbtk;->a(Ljava/lang/String;Lbth;)V

    .line 91
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p2}, Lbtk;->a(Ljava/lang/Object;)Lbth;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbtk;->a(Ljava/lang/String;Lbth;)V

    .line 80
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0, p1}, Lbtu;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 187
    if-eq p1, p0, :cond_0

    instance-of v0, p1, Lbtk;

    if-eqz v0, :cond_1

    check-cast p1, Lbtk;

    iget-object v0, p1, Lbtk;->a:Lbtu;

    iget-object v1, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0, v1}, Lbtu;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lbtk;->a:Lbtu;

    invoke-virtual {v0}, Lbtu;->hashCode()I

    move-result v0

    return v0
.end method
