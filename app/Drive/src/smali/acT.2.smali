.class public final enum LacT;
.super Ljava/lang/Enum;
.source "SyncResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LacT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LacT;

.field private static final synthetic a:[LacT;

.field public static final enum b:LacT;

.field public static final enum c:LacT;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, LacT;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LacT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LacT;->a:LacT;

    .line 15
    new-instance v0, LacT;

    const-string v1, "RETRY_DELAYED"

    invoke-direct {v0, v1, v3}, LacT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LacT;->b:LacT;

    .line 18
    new-instance v0, LacT;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v4}, LacT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LacT;->c:LacT;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [LacT;

    sget-object v1, LacT;->a:LacT;

    aput-object v1, v0, v2

    sget-object v1, LacT;->b:LacT;

    aput-object v1, v0, v3

    sget-object v1, LacT;->c:LacT;

    aput-object v1, v0, v4

    sput-object v0, LacT;->a:[LacT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LacT;
    .locals 1

    .prologue
    .line 7
    const-class v0, LacT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LacT;

    return-object v0
.end method

.method public static values()[LacT;
    .locals 1

    .prologue
    .line 7
    sget-object v0, LacT;->a:[LacT;

    invoke-virtual {v0}, [LacT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LacT;

    return-object v0
.end method
