.class public Lbxu;
.super Ljava/lang/Object;
.source "XmlEntryGDataSerializer.java"

# interfaces
.implements Lbxs;


# instance fields
.field private final a:Lbxb;

.field private final a:Lbxr;

.field private final a:Z


# direct methods
.method public constructor <init>(Lbxr;Lbxb;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lbxu;->a:Lbxr;

    .line 41
    iput-object p2, p0, Lbxu;->a:Lbxb;

    .line 42
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbxu;->a:Z

    .line 43
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 182
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 185
    :cond_0
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 186
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 187
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 252
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    sget-object v0, Lbxq;->t:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 256
    sget-object v0, Lbxq;->v:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 257
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 258
    sget-object v0, Lbxq;->v:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 259
    sget-object v0, Lbxq;->u:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 260
    invoke-interface {p0, p2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 261
    sget-object v0, Lbxq;->u:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 262
    sget-object v0, Lbxq;->t:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method protected static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 214
    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 217
    :cond_0
    sget-object v0, Lbxq;->i:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 218
    sget-object v0, Lbxq;->h:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 219
    sget-object v0, Lbxq;->j:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 220
    invoke-static {p3}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lbxq;->l:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 221
    :cond_1
    invoke-static {p4}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lbxq;->k:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 222
    :cond_2
    sget-object v0, Lbxq;->i:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private b(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2

    .prologue
    .line 111
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "gd"

    const-string v1, "http://schemas.google.com/g/2005"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0, p1}, Lbxu;->a(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 114
    return-void
.end method

.method private b(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 128
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 129
    invoke-direct {p0, p1}, Lbxu;->c(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 132
    :cond_0
    if-eq p2, v3, :cond_1

    .line 133
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxu;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 136
    :cond_1
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxu;->b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 138
    if-eq p2, v3, :cond_2

    .line 139
    sget-object v0, Lbxq;->f:Ljava/lang/String;

    iget-object v1, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v1}, Lbxb;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, v4, v4}, Lbxu;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    sget-object v0, Lbxq;->g:Ljava/lang/String;

    iget-object v1, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v1}, Lbxb;->r()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbxq;->o:Ljava/lang/String;

    invoke-static {p1, v0, v1, v2, v4}, Lbxu;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_2
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxu;->c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxu;->d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->t()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v1}, Lbxb;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lbxu;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->u()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v1}, Lbxb;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lbxu;->b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    if-nez p2, :cond_3

    .line 154
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxu;->e(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 158
    :cond_3
    if-eq p2, v3, :cond_4

    .line 159
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbxu;->f(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 163
    :cond_4
    invoke-virtual {p0, p1, p2}, Lbxu;->a(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 164
    return-void
.end method

.method private static b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 193
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 196
    :cond_0
    sget-object v0, Lbxq;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 197
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 198
    sget-object v0, Lbxq;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    :goto_0
    return-void

    .line 273
    :cond_0
    sget-object v0, Lbxq;->w:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 274
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    sget-object v0, Lbxq;->x:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 277
    :cond_1
    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 278
    sget-object v0, Lbxq;->y:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 280
    :cond_2
    sget-object v0, Lbxq;->w:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private c(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    const-string v0, "http://schemas.google.com/g/2005"

    sget-object v1, Lbxq;->k:Ljava/lang/String;

    iget-object v2, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v2}, Lbxb;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 313
    :cond_0
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-static {v0}, Lbxi;->b(Lbxb;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 314
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->B:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 315
    const/4 v0, 0x0

    sget-object v1, Lbxq;->l:Ljava/lang/String;

    iget-object v2, p0, Lbxu;->a:Lbxb;

    invoke-static {v2}, Lbxi;->b(Lbxb;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 317
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->B:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 319
    :cond_1
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-static {v0}, Lbxi;->a(Lbxb;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 320
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 321
    iget-object v0, p0, Lbxu;->a:Lbxb;

    invoke-static {v0}, Lbxi;->a(Lbxb;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 322
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 324
    :cond_2
    return-void
.end method

.method private static c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    :goto_0
    return-void

    .line 231
    :cond_0
    sget-object v0, Lbxq;->r:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 232
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 233
    sget-object v0, Lbxq;->r:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 239
    if-nez p1, :cond_0

    .line 246
    :goto_0
    return-void

    .line 242
    :cond_0
    sget-object v0, Lbxq;->s:Ljava/lang/String;

    invoke-interface {p0, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 243
    sget-object v0, Lbxq;->l:Ljava/lang/String;

    sget-object v1, Lbxq;->n:Ljava/lang/String;

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 244
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 245
    sget-object v0, Lbxq;->s:Ljava/lang/String;

    invoke-interface {p0, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static e(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 287
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    :goto_0
    return-void

    .line 290
    :cond_0
    sget-object v0, Lbxq;->z:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 291
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 292
    sget-object v0, Lbxq;->z:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static f(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 299
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    :goto_0
    return-void

    .line 302
    :cond_0
    sget-object v0, Lbxq;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 303
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 304
    sget-object v0, Lbxq;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string v0, "application/atom+xml"

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;I)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 74
    .line 76
    :try_start_0
    iget-object v0, p0, Lbxu;->a:Lbxr;

    invoke-interface {v0}, Lbxr;->a()Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 82
    sget-object v1, Lbxq;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 83
    if-eq p2, v4, :cond_0

    .line 84
    sget-object v1, Lbxq;->e:Ljava/lang/String;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 85
    invoke-direct {p0, v0}, Lbxu;->b(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 87
    :cond_0
    iget-object v1, p0, Lbxu;->a:Lbxb;

    invoke-virtual {v1}, Lbxb;->z()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-virtual {p0}, Lbxu;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    const-string v2, "http://schemas.google.com/g/2005"

    sget-object v3, Lbxq;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 90
    const/4 v2, 0x0

    sget-object v3, Lbxq;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 93
    :cond_1
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 95
    invoke-direct {p0, v0, p2}, Lbxu;->b(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 97
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Lbxq;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 99
    invoke-virtual {p0}, Lbxu;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 100
    const-string v1, "http://schemas.google.com/g/2005"

    sget-object v2, Lbxq;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 103
    :cond_2
    if-eq p2, v4, :cond_3

    .line 104
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 106
    :cond_3
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 107
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    new-instance v1, Lbxk;

    const-string v2, "Unable to create XmlSerializer."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected a(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method protected a(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lbxu;->a:Z

    return v0
.end method
