.class public LpH;
.super Lajr;
.source "InterfaceProviderActivity.java"

# interfaces
.implements Lue;


# instance fields
.field public a:LqK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lajr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 41
    if-nez p2, :cond_1

    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 60
    :cond_0
    :goto_0
    return-object p0

    .line 50
    :cond_1
    invoke-virtual {p0}, LpH;->a()LM;

    move-result-object v1

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v1, p2}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p0

    .line 51
    if-nez p0, :cond_0

    move-object p0, v0

    .line 52
    goto :goto_0

    :cond_2
    move-object p0, v0

    .line 60
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0, p1}, Lajr;->onCreate(Landroid/os/Bundle;)V

    .line 27
    iget-object v0, p0, LpH;->a:LqK;

    invoke-virtual {v0}, LqK;->a()V

    .line 28
    if-nez p1, :cond_0

    .line 29
    iget-object v0, p0, LpH;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Landroid/app/Activity;)V

    .line 31
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LpH;->a:LqK;

    invoke-virtual {v0}, LqK;->b()V

    .line 36
    invoke-super {p0}, Lajr;->onDestroy()V

    .line 37
    return-void
.end method
