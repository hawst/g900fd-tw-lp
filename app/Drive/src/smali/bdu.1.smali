.class Lbdu;
.super LbdR;
.source "HttpRequestContent.java"


# instance fields
.field private final a:Lbei;


# direct methods
.method constructor <init>(Lbei;)V
    .locals 1

    .prologue
    .line 40
    const-string v0, "application/http"

    invoke-direct {p0, v0}, LbdR;-><init>(Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lbdu;->a:Lbei;

    .line 42
    return-void
.end method


# virtual methods
.method public a(Ljava/io/OutputStream;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 45
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-virtual {p0}, Lbdu;->a()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    .line 47
    iget-object v1, p0, Lbdu;->a:Lbei;

    invoke-virtual {v1}, Lbei;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 48
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lbdu;->a:Lbei;

    invoke-virtual {v1}, Lbei;->a()LbdY;

    move-result-object v1

    invoke-virtual {v1}, LbdY;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 50
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 52
    new-instance v1, Lbed;

    invoke-direct {v1}, Lbed;-><init>()V

    .line 53
    iget-object v2, p0, Lbdu;->a:Lbei;

    invoke-virtual {v2}, Lbei;->a()Lbed;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbed;->a(Lbed;)V

    .line 54
    invoke-virtual {v1, v8}, Lbed;->a(Ljava/lang/String;)Lbed;

    move-result-object v2

    invoke-virtual {v2, v8}, Lbed;->l(Ljava/lang/String;)Lbed;

    move-result-object v2

    .line 55
    invoke-virtual {v2, v8}, Lbed;->c(Ljava/lang/String;)Lbed;

    move-result-object v2

    invoke-virtual {v2, v8}, Lbed;->e(Ljava/lang/String;)Lbed;

    move-result-object v2

    invoke-virtual {v2, v8}, Lbed;->a(Ljava/lang/Long;)Lbed;

    .line 57
    iget-object v2, p0, Lbdu;->a:Lbei;

    invoke-virtual {v2}, Lbei;->a()LbdZ;

    move-result-object v2

    .line 58
    if-eqz v2, :cond_0

    .line 59
    invoke-interface {v2}, LbdZ;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lbed;->e(Ljava/lang/String;)Lbed;

    .line 61
    invoke-interface {v2}, LbdZ;->a()J

    move-result-wide v4

    .line 62
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 63
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Lbed;->a(Ljava/lang/Long;)Lbed;

    .line 66
    :cond_0
    invoke-static {v1, v8, v8, v0}, Lbed;->a(Lbed;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Ljava/io/Writer;)V

    .line 68
    if-eqz v2, :cond_1

    .line 69
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 71
    invoke-interface {v2, p1}, LbdZ;->a(Ljava/io/OutputStream;)V

    .line 73
    :cond_1
    return-void
.end method
