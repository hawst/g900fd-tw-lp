.class public LaxE;
.super Ljava/lang/Object;
.source "PdfLoader.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private final a:Landroid/content/Context;

.field private a:LavE;

.field private a:Ljava/lang/Runnable;

.field private final a:Ljava/util/concurrent/locks/Condition;

.field private final a:Ljava/util/concurrent/locks/Lock;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    .line 226
    iget-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Condition;

    .line 232
    iput-object p1, p0, LaxE;->a:Landroid/content/Context;

    .line 233
    return-void
.end method

.method static synthetic a(LaxE;)LavE;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, LaxE;->a:LavE;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, LaxE;->a:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 275
    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 267
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LaxE;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 269
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 270
    iget-object v1, p0, LaxE;->a:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 271
    return-void
.end method

.method static synthetic a(LaxE;)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, LaxE;->a()V

    return-void
.end method

.method static synthetic a(LaxE;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0, p1}, LaxE;->a(Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, LaxE;->a:Ljava/lang/Runnable;

    .line 238
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, LaxE;->a:LavE;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 242
    const-string v0, "PdfLoader"

    const-string v1, "Service connected %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 245
    :try_start_0
    invoke-static {p2}, LavF;->a(Landroid/os/IBinder;)LavE;

    move-result-object v0

    iput-object v0, p0, LaxE;->a:LavE;

    .line 246
    iget-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    iget-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 250
    iget-object v0, p0, LaxE;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, LaxE;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 253
    :cond_0
    return-void

    .line 248
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 259
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LaxE;->a:LavE;

    .line 260
    const-string v0, "PdfLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service disconnected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    iget-object v0, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 264
    return-void

    .line 262
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaxE;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
