.class public LaFt;
.super Ljava/lang/Object;
.source "DatabaseField.java"


# instance fields
.field private a:I

.field private a:LaFE;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LaFE;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, LaFt;->a:LaFE;

    .line 316
    const/4 v0, 0x0

    iput v0, p0, LaFt;->a:I

    .line 320
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LaFt;->a:Ljava/util/Map;

    .line 321
    iput p1, p0, LaFt;->b:I

    .line 322
    return-void
.end method

.method public static a(I)LaFt;
    .locals 1

    .prologue
    .line 325
    new-instance v0, LaFt;

    invoke-direct {v0, p0}, LaFt;-><init>(I)V

    return-object v0
.end method

.method public static a(LaFy;)LaFt;
    .locals 2

    .prologue
    .line 330
    new-instance v0, LaFt;

    invoke-virtual {p0}, LaFy;->a()I

    move-result v1

    invoke-direct {v0, v1}, LaFt;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 4

    .prologue
    .line 401
    iget-object v0, p0, LaFt;->a:LaFE;

    if-eqz v0, :cond_0

    .line 403
    iget v0, p0, LaFt;->b:I

    iget v1, p0, LaFt;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, LaFt;->b(I)LaFt;

    .line 405
    :cond_0
    new-instance v0, LaFr;

    iget-object v1, p0, LaFt;->a:Ljava/util/Map;

    iget v2, p0, LaFt;->b:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LaFr;-><init>(Ljava/util/Map;ILaFs;)V

    return-object v0
.end method

.method public a(II)LaFt;
    .locals 5

    .prologue
    .line 350
    iget-object v0, p0, LaFt;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFE;

    .line 351
    const-string v1, "No existing FieldDefinition at %s that has been removed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 352
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 351
    invoke-static {v0, v1, v2}, LbiT;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    invoke-virtual {p0, p1, v0}, LaFt;->a(ILaFE;)LaFt;

    move-result-object v0

    return-object v0
.end method

.method public a(ILaFE;)LaFt;
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, LaFt;->a:LaFE;

    if-eqz v0, :cond_0

    .line 362
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add a new field definition until the existing definition is removed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_0
    iget v0, p0, LaFt;->a:I

    if-ge p1, v0, :cond_1

    .line 366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Versions must be 0 or greater and specified in ascending order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369
    :cond_1
    iput-object p2, p0, LaFt;->a:LaFE;

    .line 370
    iput p1, p0, LaFt;->a:I

    .line 371
    return-object p0
.end method

.method public a(ILaFG;)LaFt;
    .locals 1

    .prologue
    .line 338
    invoke-virtual {p2}, LaFG;->a()LaFE;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LaFt;->a(ILaFE;)LaFt;

    move-result-object v0

    return-object v0
.end method

.method public b(I)LaFt;
    .locals 4

    .prologue
    .line 380
    iget-object v0, p0, LaFt;->a:LaFE;

    if-nez v0, :cond_0

    .line 381
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No field definition to remove"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_0
    iget v0, p0, LaFt;->a:I

    if-gt p1, v0, :cond_1

    .line 384
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Versions must be 0 or greater and specified in ascending order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_1
    iget v0, p0, LaFt;->a:I

    :goto_0
    if-ge v0, p1, :cond_2

    .line 388
    iget-object v1, p0, LaFt;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LaFt;->a:LaFE;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 390
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LaFt;->a:LaFE;

    .line 391
    iput p1, p0, LaFt;->a:I

    .line 392
    return-object p0
.end method
