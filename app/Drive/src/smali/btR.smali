.class final LbtR;
.super Lbtp;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbtp",
        "<",
        "Ljava/util/UUID;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 499
    invoke-direct {p0}, Lbtp;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lbuj;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 499
    invoke-virtual {p0, p1}, LbtR;->a(Lbuj;)Ljava/util/UUID;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbuj;)Ljava/util/UUID;
    .locals 2

    .prologue
    .line 502
    invoke-virtual {p1}, Lbuj;->a()Lbul;

    move-result-object v0

    sget-object v1, Lbul;->i:Lbul;

    if-ne v0, v1, :cond_0

    .line 503
    invoke-virtual {p1}, Lbuj;->e()V

    .line 504
    const/4 v0, 0x0

    .line 506
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lbuj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Lbum;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 499
    check-cast p2, Ljava/util/UUID;

    invoke-virtual {p0, p1, p2}, LbtR;->a(Lbum;Ljava/util/UUID;)V

    return-void
.end method

.method public a(Lbum;Ljava/util/UUID;)V
    .locals 1

    .prologue
    .line 510
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lbum;->b(Ljava/lang/String;)Lbum;

    .line 511
    return-void

    .line 510
    :cond_0
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
