.class public final LUh;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUr;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUl;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUi;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LUi;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LUi;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, LUk;

    sput-object v0, LUh;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 43
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 44
    iput-object p1, p0, LUh;->a:LbrA;

    .line 45
    const-class v0, LUr;

    invoke-static {v0, v3}, LUh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUh;->a:Lbsk;

    .line 48
    const-class v0, LUl;

    invoke-static {v0, v3}, LUh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUh;->b:Lbsk;

    .line 51
    const-class v0, LUi;

    sget-object v1, LUh;->a:Ljava/lang/Class;

    .line 52
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 51
    invoke-static {v0, v3}, LUh;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUh;->c:Lbsk;

    .line 54
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LUi;

    aput-object v2, v1, v4

    .line 55
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LUh;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 54
    invoke-static {v0, v3}, LUh;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUh;->d:Lbsk;

    .line 57
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LUi;

    aput-object v2, v1, v4

    .line 58
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LUh;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 57
    invoke-static {v0, v3}, LUh;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUh;->e:Lbsk;

    .line 60
    const-class v0, LUi;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LUh;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LUh;->f:Lbsk;

    .line 63
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 96
    sparse-switch p1, :sswitch_data_0

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :sswitch_0
    new-instance v2, LUr;

    iget-object v0, p0, LUh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->c:Lbsk;

    .line 101
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LUh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUh;

    iget-object v1, v1, LUh;->c:Lbsk;

    .line 99
    invoke-static {v0, v1}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUi;

    iget-object v1, p0, LUh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 107
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LUh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LqD;

    iget-object v3, v3, LqD;->c:Lbsk;

    .line 105
    invoke-static {v1, v3}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LqK;

    invoke-direct {v2, v0, v1}, LUr;-><init>(LUi;LqK;)V

    move-object v0, v2

    .line 152
    :goto_0
    return-object v0

    .line 114
    :sswitch_1
    new-instance v0, LUl;

    iget-object v1, p0, LUh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHj;

    iget-object v1, v1, LaHj;->b:Lbsk;

    .line 117
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LUh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaHj;

    iget-object v2, v2, LaHj;->b:Lbsk;

    .line 115
    invoke-static {v1, v2}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaHz;

    iget-object v2, p0, LUh;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaHj;

    iget-object v2, v2, LaHj;->h:Lbsk;

    .line 123
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LUh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaHj;

    iget-object v3, v3, LaHj;->h:Lbsk;

    .line 121
    invoke-static {v2, v3}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaHr;

    iget-object v3, p0, LUh;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->j:Lbsk;

    .line 129
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LUh;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->j:Lbsk;

    .line 127
    invoke-static {v3, v4}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGg;

    iget-object v4, p0, LUh;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaHj;

    iget-object v4, v4, LaHj;->i:Lbsk;

    .line 135
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LUh;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaHj;

    iget-object v5, v5, LaHj;->i:Lbsk;

    .line 133
    invoke-static {v4, v5}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LaHE;

    iget-object v5, p0, LUh;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LahE;

    iget-object v5, v5, LahE;->h:Lbsk;

    .line 141
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LUh;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LahE;

    iget-object v6, v6, LahE;->h:Lbsk;

    .line 139
    invoke-static {v5, v6}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LahB;

    iget-object v6, p0, LUh;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LagN;

    iget-object v6, v6, LagN;->D:Lbsk;

    .line 147
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LUh;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LagN;

    iget-object v7, v7, LagN;->D:Lbsk;

    .line 145
    invoke-static {v6, v7}, LUh;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v6}, LUl;-><init>(LaHz;LaHr;LaGg;LaHE;LahB;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    .line 96
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_1
        0xd -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 169
    packed-switch p2, :pswitch_data_0

    .line 219
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :pswitch_1
    check-cast p1, LUj;

    .line 173
    iget-object v0, p0, LUh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->ap:Lbsk;

    .line 176
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iget-object v1, p0, LUh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUh;

    iget-object v1, v1, LUh;->b:Lbsk;

    .line 180
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LUl;

    .line 173
    invoke-virtual {p1, v0, v1}, LUj;->provideOperationQueueMetadataChanger(LbiP;LUl;)LUi;

    move-result-object v0

    .line 212
    :goto_0
    return-object v0

    .line 184
    :pswitch_2
    check-cast p1, LUg;

    .line 186
    iget-object v0, p0, LUh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->e:Lbsk;

    .line 189
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 186
    invoke-virtual {p1, v0}, LUg;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 193
    :pswitch_3
    check-cast p1, LUg;

    .line 195
    iget-object v0, p0, LUh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v2, v0, LUh;->c:Lbsk;

    iget-object v0, p0, LUh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 202
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LUh;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 206
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 195
    invoke-virtual {p1, v2, v0, v1}, LUg;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 210
    :pswitch_4
    check-cast p1, LUj;

    .line 212
    iget-object v0, p0, LUh;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->a:Lbsk;

    .line 215
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUr;

    .line 212
    invoke-virtual {p1, v0}, LUj;->provideMetadataChanger(LUr;)LUi;

    move-result-object v0

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    const-class v0, LUr;

    iget-object v1, p0, LUh;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LUh;->a(Ljava/lang/Class;Lbsk;)V

    .line 71
    const-class v0, LUl;

    iget-object v1, p0, LUh;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LUh;->a(Ljava/lang/Class;Lbsk;)V

    .line 72
    const-class v0, LUi;

    sget-object v1, LUh;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LUh;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LUh;->a(Lbuv;Lbsk;)V

    .line 73
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LUi;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LUh;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LUh;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LUh;->a(Lbuv;Lbsk;)V

    .line 74
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LUi;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LUh;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LUh;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LUh;->a(Lbuv;Lbsk;)V

    .line 75
    const-class v0, LUi;

    iget-object v1, p0, LUh;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LUh;->a(Ljava/lang/Class;Lbsk;)V

    .line 76
    iget-object v0, p0, LUh;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xd

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 78
    iget-object v0, p0, LUh;->b:Lbsk;

    new-instance v1, LbrD;

    const/4 v2, 0x7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 80
    iget-object v0, p0, LUh;->c:Lbsk;

    const-class v1, LUj;

    const/4 v2, 0x5

    invoke-virtual {p0, v1, v2}, LUh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 82
    iget-object v0, p0, LUh;->d:Lbsk;

    const-class v1, LUg;

    const/16 v2, 0x8

    invoke-virtual {p0, v1, v2}, LUh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 84
    iget-object v0, p0, LUh;->e:Lbsk;

    const-class v1, LUg;

    const/16 v2, 0x9

    invoke-virtual {p0, v1, v2}, LUh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 86
    iget-object v0, p0, LUh;->f:Lbsk;

    const-class v1, LUj;

    const/16 v2, 0xc

    invoke-virtual {p0, v1, v2}, LUh;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 88
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 161
    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method
