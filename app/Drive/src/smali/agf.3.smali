.class public Lagf;
.super Ljava/lang/Thread;
.source "ContentSyncService.java"


# instance fields
.field private final a:I

.field final synthetic a:Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;)V
    .locals 3

    .prologue
    .line 200
    iput-object p1, p0, Lagf;->a:Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;

    .line 201
    const-string v0, "ContentSyncService-WaitingThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lagf;->a:Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;

    iget-object v0, v0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LQr;

    const-string v1, "contentSyncServiceWaitingThreadsCompleteSeconds"

    const/16 v2, 0x1e

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lagf;->a:I

    .line 202
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 207
    :try_start_0
    iget-object v0, p0, Lagf;->a:Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;

    iget-object v0, v0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LaKU;

    iget v1, p0, Lagf;->a:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LaKU;->a(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Threads wont stop after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lagf;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seconds."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected interruption"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 213
    :cond_0
    return-void
.end method
