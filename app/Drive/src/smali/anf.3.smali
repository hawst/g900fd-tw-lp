.class public Lanf;
.super Landroid/os/Handler;
.source "ToastErrorReporter.java"


# instance fields
.field private a:Landroid/widget/Toast;

.field final synthetic a:Lcom/google/android/apps/docs/utils/ToastErrorReporter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/utils/ToastErrorReporter;)V
    .locals 1

    .prologue
    .line 72
    iput-object p1, p0, Lanf;->a:Lcom/google/android/apps/docs/utils/ToastErrorReporter;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lanf;->a:Landroid/widget/Toast;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, Lanf;->a:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lanf;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 80
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lang;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lang;

    .line 82
    iget-object v1, p0, Lanf;->a:Lcom/google/android/apps/docs/utils/ToastErrorReporter;

    invoke-static {v1}, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a(Lcom/google/android/apps/docs/utils/ToastErrorReporter;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lang;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lanf;->a:Landroid/widget/Toast;

    .line 83
    iget-object v1, p0, Lanf;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Lang;->a()I

    move-result v0

    invoke-virtual {v1, v0, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 84
    iget-object v0, p0, Lanf;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 86
    :cond_1
    return-void
.end method
