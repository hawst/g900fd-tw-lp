.class public LbhV;
.super Ljava/lang/Object;
.source "Viewport.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/bionics/scanner/unveil/ui/Viewport;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LbhU;)V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0}, LbhV;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/unveil/ui/Viewport;
    .locals 2

    .prologue
    .line 388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 389
    new-instance v1, Lcom/google/bionics/scanner/unveil/ui/Viewport;

    invoke-direct {v1, v0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;-><init>(I)V

    .line 390
    const-class v0, Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->setLatestBarcodeQueryCrop(Landroid/graphics/Rect;)V

    .line 391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->setLatestBarcodeQueryRotation(I)V

    .line 392
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->setPreviewSize(Lcom/google/bionics/scanner/unveil/util/Size;)V

    .line 393
    return-object v1
.end method

.method public a(I)[Lcom/google/bionics/scanner/unveil/ui/Viewport;
    .locals 1

    .prologue
    .line 398
    new-array v0, p1, [Lcom/google/bionics/scanner/unveil/ui/Viewport;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0, p1}, LbhV;->a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/unveil/ui/Viewport;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0, p1}, LbhV;->a(I)[Lcom/google/bionics/scanner/unveil/ui/Viewport;

    move-result-object v0

    return-object v0
.end method
