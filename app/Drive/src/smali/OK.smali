.class LOK;
.super Ljava/lang/Object;
.source "PhotosPreferencesInstaller.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:LOJ;


# direct methods
.method constructor <init>(LOJ;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, LOK;->a:LOJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, LOK;->a:LOJ;

    iget-object v0, v0, LOJ;->a:LqK;

    const-string v1, "preferences"

    const-string v2, "settingsPhotoBackupEnableOrDisable"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, LOK;->a:LOJ;

    iget-object v0, v0, LOJ;->a:LbiP;

    .line 177
    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSu;

    iget-object v1, p0, LOK;->a:LOJ;

    invoke-static {v1}, LOJ;->a(LOJ;)LaFO;

    move-result-object v1

    invoke-virtual {v1}, LaFO;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LSu;->c(Ljava/lang/String;)LbsU;

    move-result-object v0

    .line 179
    new-instance v1, LOL;

    invoke-direct {v1, p0, v0}, LOL;-><init>(LOK;LbsU;)V

    .line 198
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    .line 197
    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 199
    const/4 v0, 0x1

    return v0
.end method
