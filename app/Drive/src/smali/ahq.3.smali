.class public Lahq;
.super LagI;
.source "UpdatedDateMonitorProcessorImpl.java"

# interfaces
.implements Lahp;


# instance fields
.field private final a:LaGF;

.field private a:Ljava/util/Date;


# direct methods
.method public constructor <init>(Lagy;JLaGF;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1}, LagI;-><init>(Lagy;)V

    .line 32
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGF;

    iput-object v0, p0, Lahq;->a:LaGF;

    .line 33
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p2, p3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lahq;->a:Ljava/util/Date;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Ljava/util/Date;
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lahq;->a:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lahq;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public a(LaeD;LaJT;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 39
    invoke-interface {p2}, LaJT;->n()Ljava/lang/String;

    move-result-object v1

    .line 40
    if-eqz v1, :cond_0

    .line 42
    :try_start_0
    iget-object v0, p0, Lahq;->a:LaGF;

    invoke-interface {v0, v1}, LaGF;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 47
    iget-object v2, p0, Lahq;->a:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 48
    iput-object v0, p0, Lahq;->a:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, LagI;->a(LaeD;LaJT;)V

    .line 62
    return-void

    .line 49
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lahq;->a:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x6ddd00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 52
    const-string v2, "UpdatedDateMonitorProcessor"

    const-string v3, "Time going backward more than expected. oldest date %s,  current date %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lahq;->a:Ljava/util/Date;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    const-string v2, "UpdatedDateMonitorProcessor"

    const-string v3, "Error parsing date %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v1, v4, v7

    invoke-static {v2, v0, v3, v4}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    if-nez p1, :cond_0

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lahq;->a:Ljava/util/Date;

    .line 70
    :cond_0
    invoke-super {p0, p1}, LagI;->a(Ljava/lang/String;)V

    .line 71
    return-void
.end method
