.class public final enum LYq;
.super Ljava/lang/Enum;
.source "PunchSvgWebView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LYq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LYq;

.field private static final synthetic a:[LYq;

.field public static final enum b:LYq;

.field public static final enum c:LYq;

.field public static final enum d:LYq;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 107
    new-instance v0, LYq;

    const-string v1, "HTML_NOT_REQUESTED"

    invoke-direct {v0, v1, v2}, LYq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYq;->a:LYq;

    .line 111
    new-instance v0, LYq;

    const-string v1, "HTML_REQUESTED"

    invoke-direct {v0, v1, v3}, LYq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYq;->b:LYq;

    .line 115
    new-instance v0, LYq;

    const-string v1, "HTML_LOADED"

    invoke-direct {v0, v1, v4}, LYq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYq;->c:LYq;

    .line 119
    new-instance v0, LYq;

    const-string v1, "SVG_LOADED"

    invoke-direct {v0, v1, v5}, LYq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYq;->d:LYq;

    .line 103
    const/4 v0, 0x4

    new-array v0, v0, [LYq;

    sget-object v1, LYq;->a:LYq;

    aput-object v1, v0, v2

    sget-object v1, LYq;->b:LYq;

    aput-object v1, v0, v3

    sget-object v1, LYq;->c:LYq;

    aput-object v1, v0, v4

    sget-object v1, LYq;->d:LYq;

    aput-object v1, v0, v5

    sput-object v0, LYq;->a:[LYq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LYq;
    .locals 1

    .prologue
    .line 103
    const-class v0, LYq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LYq;

    return-object v0
.end method

.method public static values()[LYq;
    .locals 1

    .prologue
    .line 103
    sget-object v0, LYq;->a:[LYq;

    invoke-virtual {v0}, [LYq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LYq;

    return-object v0
.end method
