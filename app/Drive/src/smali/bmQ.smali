.class final LbmQ;
.super Lbmv;
.source "ImmutableMapValues.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lbmv",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbmL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmL",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lbmv;-><init>()V

    .line 36
    iput-object p1, p0, LbmQ;->a:LbmL;

    .line 37
    return-void
.end method


# virtual methods
.method public a()Lbqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, LbmQ;->a:LbmL;

    invoke-virtual {v0}, LbmL;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v0

    invoke-static {v0}, LboS;->a(Lbqv;)Lbqv;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method b()LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, LbmQ;->a:LbmL;

    invoke-virtual {v0}, LbmL;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()LbmF;

    move-result-object v0

    .line 61
    new-instance v1, LbmR;

    invoke-direct {v1, p0, v0}, LbmR;-><init>(LbmQ;LbmF;)V

    return-object v1
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LbmQ;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, LbmQ;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LbmQ;->a:LbmL;

    invoke-virtual {v0}, LbmL;->size()I

    move-result v0

    return v0
.end method
