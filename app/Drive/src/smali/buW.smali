.class public LbuW;
.super Ljava/lang/Object;
.source "Annotations.java"


# static fields
.field private static final a:LbiG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiG",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LbiK;

.field private static final a:LbjP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjP",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Lbva;

.field private static final b:Lbva;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->b()LbjG;

    move-result-object v0

    new-instance v1, LbuX;

    invoke-direct {v1}, LbuX;-><init>()V

    invoke-virtual {v0, v1}, LbjG;->a(LbjM;)LbjP;

    move-result-object v0

    sput-object v0, LbuW;->a:LbjP;

    .line 154
    const-string v0, ", "

    invoke-static {v0}, LbiI;->a(Ljava/lang/String;)LbiI;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, LbiI;->a(Ljava/lang/String;)LbiK;

    move-result-object v0

    sput-object v0, LbuW;->a:LbiK;

    .line 156
    new-instance v0, LbuZ;

    invoke-direct {v0}, LbuZ;-><init>()V

    sput-object v0, LbuW;->a:LbiG;

    .line 241
    new-instance v0, Lbva;

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, LbuI;

    aput-object v2, v1, v3

    const-class v2, Lbxy;

    aput-object v2, v1, v4

    .line 242
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lbva;-><init>(Ljava/util/Collection;)V

    sput-object v0, LbuW;->a:Lbva;

    .line 294
    new-instance v0, Lbva;

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Lbuq;

    aput-object v2, v1, v3

    const-class v2, Lbxx;

    aput-object v2, v1, v4

    .line 295
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lbva;-><init>(Ljava/util/Collection;)V

    sput-object v0, LbuW;->b:Lbva;

    .line 294
    return-void
.end method

.method static synthetic a(Ljava/lang/Class;Ljava/util/Map;)I
    .locals 1

    .prologue
    .line 54
    invoke-static {p0, p1}, LbuW;->b(Ljava/lang/Class;Ljava/util/Map;)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/Class;)LbmL;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v1

    .line 120
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 121
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getDefaultValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v5, v4}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    invoke-virtual {v1}, LbmM;->a()LbmL;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbvd;[Ljava/lang/annotation/Annotation;)Ljava/lang/Class;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvd;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    const/4 v1, 0x0

    .line 190
    array-length v3, p1

    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, p1, v2

    .line 191
    invoke-interface {v1}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    .line 192
    invoke-static {v1}, LbuW;->d(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 193
    if-eqz v0, :cond_1

    .line 194
    invoke-virtual {p0, v0, v1}, Lbvd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbvd;

    .line 190
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 196
    goto :goto_1

    .line 201
    :cond_2
    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    const-class v0, Lbxv;

    if-ne p0, v0, :cond_0

    .line 323
    const-class p0, Lbwm;

    .line 325
    :cond_0
    return-object p0
.end method

.method static synthetic a(Ljava/lang/Class;Ljava/util/Map;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-static {p0, p1}, LbuW;->b(Ljava/lang/Class;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbvd;Ljava/lang/reflect/Member;[Ljava/lang/annotation/Annotation;)Ljava/lang/annotation/Annotation;
    .locals 6

    .prologue
    .line 278
    const/4 v1, 0x0

    .line 280
    array-length v3, p2

    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, p2, v2

    .line 281
    invoke-interface {v1}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v4

    .line 282
    invoke-static {v4}, LbuW;->e(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 283
    if-eqz v0, :cond_1

    .line 284
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0, p1, v1, v4}, Lbvd;->a(Ljava/lang/reflect/Member;Ljava/lang/Class;Ljava/lang/Class;)Lbvd;

    .line 280
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 286
    goto :goto_1

    .line 291
    :cond_2
    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 88
    .line 89
    invoke-static {p0}, LbuW;->b(Ljava/lang/Class;)Z

    move-result v0

    const-string v1, "%s is not all default methods"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    .line 88
    invoke-static {v0, v1, v2}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 90
    sget-object v0, LbuW;->a:LbjP;

    invoke-interface {v0, p0}, LbjP;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public static a(Ljava/lang/annotation/Annotation;)Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 309
    instance-of v0, p0, Lbxv;

    if-eqz v0, :cond_0

    .line 310
    check-cast p0, Lbxv;

    invoke-interface {p0}, Lbxv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbwo;->a(Ljava/lang/String;)Lbwm;

    move-result-object p0

    .line 312
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/Class;Ljava/util/Map;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 54
    invoke-static {p0, p1, p2}, LbuW;->b(Ljava/lang/Class;Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/Class;Ljava/util/Map;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 145
    .line 146
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 147
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    .line 148
    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 149
    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v5

    mul-int/lit8 v5, v5, 0x7f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v6, v7, v1

    invoke-static {v7}, Ljava/util/Arrays;->deepHashCode([Ljava/lang/Object;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1f

    xor-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    return v2
.end method

.method private static b(Ljava/lang/Class;Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 168
    sget-object v1, LbuW;->a:LbiK;

    sget-object v2, LbuW;->a:LbiG;

    invoke-static {p1, v2}, LboS;->a(Ljava/util/Map;LbiG;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LbiK;->a(Ljava/lang/StringBuilder;Ljava/util/Map;)Ljava/lang/StringBuilder;

    .line 169
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 54
    invoke-static {p0}, LbuW;->c(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Class;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 64
    .line 65
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v4

    array-length v5, v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v1, v4, v2

    .line 66
    const/4 v3, 0x1

    .line 67
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getDefaultValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 71
    :goto_1
    return v0

    .line 65
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_0

    :cond_1
    move v0, v1

    .line 71
    goto :goto_1
.end method

.method private static b(Ljava/lang/Class;Ljava/util/Map;Ljava/lang/Object;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 129
    invoke-virtual {p0, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v0

    .line 132
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v4, v3

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 133
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    .line 134
    new-array v7, v1, [Ljava/lang/Object;

    new-array v8, v0, [Ljava/lang/Object;

    .line 135
    invoke-virtual {v5, p2, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v7, v0

    new-array v5, v1, [Ljava/lang/Object;

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v0

    .line 134
    invoke-static {v7, v5}, Ljava/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 132
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 139
    goto :goto_0
.end method

.method private static c(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-static {p0}, LbuW;->a(Ljava/lang/Class;)LbmL;

    move-result-object v0

    .line 96
    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    new-instance v3, LbuY;

    invoke-direct {v3, p0, v0}, LbuY;-><init>(Ljava/lang/Class;Ljava/util/Map;)V

    .line 95
    invoke-static {v1, v2, v3}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public static c(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 176
    const-class v0, Ljava/lang/annotation/Retention;

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Retention;

    .line 177
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/annotation/Retention;->value()Ljava/lang/annotation/RetentionPolicy;

    move-result-object v0

    sget-object v1, Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 245
    sget-object v0, LbuW;->a:Lbva;

    invoke-virtual {v0, p0}, Lbva;->a(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public static e(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 301
    sget-object v0, LbuW;->b:Lbva;

    invoke-virtual {v0, p0}, Lbva;->a(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method
