.class LaoN;
.super Ljava/lang/Object;
.source "RetryingFetcher.java"

# interfaces
.implements LalB;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LalB",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic a:LaoL;

.field final synthetic a:Laou;

.field final synthetic a:Ljava/lang/Object;


# direct methods
.method constructor <init>(LaoL;Ljava/lang/Object;ILaou;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, LaoN;->a:LaoL;

    iput-object p2, p0, LaoN;->a:Ljava/lang/Object;

    iput p3, p0, LaoN;->a:I

    iput-object p4, p0, LaoN;->a:Laou;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)LbsU;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 70
    const-string v0, "RetryingFetcher"

    const-string v1, "Retry attempt failed for %s, numRetriesLeft=%d - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LaoN;->a:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LaoN;->a:I

    .line 71
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    .line 70
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 73
    instance-of v0, p1, LaoK;

    if-eqz v0, :cond_1

    .line 75
    iget v0, p0, LaoN;->a:I

    if-nez v0, :cond_0

    .line 76
    invoke-static {p1}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 78
    :cond_0
    iget-object v0, p0, LaoN;->a:LaoL;

    invoke-static {v0}, LaoL;->a(LaoL;)LZS;

    move-result-object v0

    invoke-interface {v0}, LZS;->b()V

    .line 79
    iget-object v0, p0, LaoN;->a:LaoL;

    iget-object v1, p0, LaoN;->a:Ljava/lang/Object;

    iget-object v2, p0, LaoN;->a:Laou;

    iget v3, p0, LaoN;->a:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, LaoL;->a(Ljava/lang/Object;Laou;I)LbsU;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_1
    invoke-static {p1}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
