.class public final LEd;
.super Ljava/lang/Object;
.source "ThumbnailHolder.java"


# instance fields
.field private final a:I

.field private a:LEf;

.field private a:LamE;

.field private a:Landroid/animation/Animator;

.field private a:Landroid/graphics/Bitmap;

.field private a:Landroid/graphics/drawable/Drawable;

.field private final a:Lapc;

.field private final a:Lapd;

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;>;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private a:Lcom/google/android/apps/docs/utils/FetchSpec;

.field private final a:Lcom/google/android/apps/docs/view/DocThumbnailView;


# direct methods
.method public constructor <init>(Lapd;Lapc;Lcom/google/android/apps/docs/view/DocThumbnailView;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LEd;-><init>(Lapd;Lapc;Lcom/google/android/apps/docs/view/DocThumbnailView;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Lapd;Lapc;Lcom/google/android/apps/docs/view/DocThumbnailView;I)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    sget-object v0, LEf;->a:LEf;

    iput-object v0, p0, LEd;->a:LEf;

    .line 53
    sget-object v0, LamE;->a:LamE;

    iput-object v0, p0, LEd;->a:LamE;

    .line 55
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LEd;->a:Landroid/animation/Animator;

    .line 75
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapd;

    iput-object v0, p0, LEd;->a:Lapd;

    .line 76
    iput-object p2, p0, LEd;->a:Lapc;

    .line 77
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocThumbnailView;

    iput-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    .line 78
    iput p4, p0, LEd;->a:I

    .line 79
    return-void
.end method

.method static synthetic a(LEd;)Lcom/google/android/apps/docs/view/DocThumbnailView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    return-object v0
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    iget-object v1, p0, LEd;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setThumbnail(Landroid/graphics/drawable/Drawable;)V

    .line 105
    iget-object v0, p0, LEd;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 106
    if-eqz p1, :cond_0

    .line 107
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    .line 109
    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->a()Landroid/util/Property;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    .line 108
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 107
    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    .line 110
    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    .line 111
    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->b(Landroid/content/Context;)Lxr;

    move-result-object v0

    new-instance v1, LEe;

    invoke-direct {v1, p0}, LEe;-><init>(LEd;)V

    .line 112
    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lxr;->a()Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, LEd;->a:Landroid/animation/Animator;

    .line 123
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v1, LapI;->a:LapI;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setState(LapI;Z)V

    goto :goto_0

    .line 109
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/view/FixedSizeImageView;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 228
    iget-object v0, p0, LEd;->a:LbsU;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, LEd;->a:LbsU;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LbsU;->cancel(Z)Z

    .line 230
    iput-object v2, p0, LEd;->a:LbsU;

    .line 232
    :cond_0
    iput-object v2, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 233
    sget-object v0, LEf;->a:LEf;

    iput-object v0, p0, LEd;->a:LEf;

    .line 234
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/utils/FetchSpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LEf;->b:LEf;

    iget-object v1, p0, LEd;->a:LEf;

    .line 127
    invoke-virtual {v0, v1}, LEf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    sget-object v0, LEf;->a:LEf;

    iput-object v0, p0, LEd;->a:LEf;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 151
    :try_start_0
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/utils/FetchSpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LEf;->c:LEf;

    iget-object v1, p0, LEd;->a:LEf;

    .line 152
    invoke-virtual {v0, v1}, LEf;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    :cond_0
    if-eqz p2, :cond_1

    .line 182
    invoke-virtual {p2}, LakD;->close()V

    .line 185
    :cond_1
    :goto_0
    return-void

    .line 155
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    .line 156
    iget-object v1, p0, LEd;->a:Landroid/graphics/Bitmap;

    if-nez v1, :cond_6

    .line 157
    iput-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 159
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v0

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, LEd;->a:Landroid/graphics/Bitmap;

    .line 160
    iget v0, p0, LEd;->a:I

    if-eqz v0, :cond_4

    invoke-static {}, LakQ;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 162
    new-instance v0, LamG;

    iget-object v1, p0, LEd;->a:Landroid/graphics/Bitmap;

    iget v2, p0, LEd;->a:I

    invoke-direct {v0, v1, v2}, LamG;-><init>(Landroid/graphics/Bitmap;I)V

    iput-object v0, p0, LEd;->a:Landroid/graphics/drawable/Drawable;

    .line 170
    :cond_3
    :goto_1
    iget-object v0, p0, LEd;->a:Landroid/graphics/Bitmap;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    invoke-virtual {p2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/RawPixelData;

    .line 172
    iget-object v1, p0, LEd;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/utils/RawPixelData;->b(Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 173
    invoke-static {v1}, LbiT;->b(Z)V

    .line 174
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/RawPixelData;->a()LamE;

    move-result-object v0

    iput-object v0, p0, LEd;->a:LamE;

    .line 175
    invoke-direct {p0, p3}, LEd;->e(Z)V

    .line 177
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v1, LapI;->b:LapI;

    sget-object v2, LamE;->b:LamE;

    iget-object v3, p0, LEd;->a:LamE;

    .line 178
    invoke-virtual {v2, v3}, LamE;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 177
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setState(LapI;Z)V

    .line 179
    sget-object v0, LEf;->c:LEf;

    iput-object v0, p0, LEd;->a:LEf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    if-eqz p2, :cond_1

    .line 182
    invoke-virtual {p2}, LakD;->close()V

    goto :goto_0

    .line 164
    :cond_4
    :try_start_2
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, LEd;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, LEd;->a:Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 181
    :catchall_0
    move-exception v0

    if-eqz p2, :cond_5

    .line 182
    invoke-virtual {p2}, LakD;->close()V

    :cond_5
    throw v0

    .line 166
    :cond_6
    :try_start_3
    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 167
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Dimension change %s -> %s not supported"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LEd;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    .line 168
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;Z)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, LEd;->a:Lapc;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, LEd;->a:Lapc;

    .line 139
    invoke-virtual {v0, p1}, Lapc;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LakD;

    move-result-object v0

    .line 140
    invoke-virtual {p0, p1, v0, p2}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;Z)V

    .line 142
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    if-eqz v0, :cond_0

    sget-object v0, LEf;->a:LEf;

    iget-object v1, p0, LEd;->a:LEf;

    invoke-virtual {v0, v1}, LEf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    if-eqz p1, :cond_1

    iget-object v0, p0, LEd;->a:Lapd;

    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 89
    invoke-virtual {v0, v1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    sget-object v0, LEf;->b:LEf;

    iput-object v0, p0, LEd;->a:LEf;

    .line 95
    iget-object v0, p0, LEd;->a:Lapd;

    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {v0, v1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    iput-object v0, p0, LEd;->a:LbsU;

    .line 97
    iget-object v0, p0, LEd;->a:LbsU;

    new-instance v1, LDX;

    iget-object v2, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-direct {v1, p0, v2}, LDX;-><init>(LEd;Lcom/google/android/apps/docs/utils/FetchSpec;)V

    .line 98
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    .line 97
    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, LEd;->a:LEf;

    sget-object v1, LEf;->c:LEf;

    invoke-virtual {v0, v1}, LEf;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/utils/FetchSpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 199
    :goto_0
    if-eqz v0, :cond_0

    .line 200
    iput-object p1, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 202
    sget-object v2, LaGx;->c:LaGx;

    iget-object v3, p0, LEd;->a:Lapd;

    invoke-virtual {v3, p1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LaGx;

    move-result-object v3

    invoke-virtual {v2, v3}, LaGx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 203
    invoke-virtual {p0, p1, v1}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;Z)V

    .line 221
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 198
    goto :goto_0

    .line 205
    :cond_2
    iget-object v1, p0, LEd;->a:Lapd;

    iget-object v2, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 206
    invoke-virtual {v1, v2}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LakD;

    move-result-object v1

    .line 208
    if-nez v1, :cond_3

    .line 209
    :try_start_0
    sget-object v2, LEf;->a:LEf;

    iput-object v2, p0, LEd;->a:LEf;

    .line 210
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, LEd;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    :goto_2
    if-eqz v1, :cond_0

    .line 216
    invoke-virtual {v1}, LakD;->close()V

    goto :goto_1

    .line 212
    :cond_3
    :try_start_1
    iget-object v2, p0, LEd;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v1, v3}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 215
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 216
    invoke-virtual {v1}, LakD;->close()V

    :cond_4
    throw v0
.end method

.method public b(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 241
    if-eqz p1, :cond_1

    .line 242
    invoke-direct {p0, v0}, LEd;->e(Z)V

    .line 246
    :goto_0
    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v2, LapI;->a:LapI;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setState(LapI;Z)V

    .line 248
    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v2, LapI;->b:LapI;

    if-eqz p1, :cond_0

    sget-object v3, LamE;->b:LamE;

    iget-object v4, p0, LEd;->a:LamE;

    .line 249
    invoke-virtual {v3, v4}, LamE;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    .line 248
    :cond_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setState(LapI;Z)V

    .line 250
    return-void

    .line 244
    :cond_1
    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->a()V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v1, LapI;->b:LapI;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->a(LapI;)Z

    move-result v0

    return v0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v1, LapI;->c:LapI;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setState(LapI;Z)V

    .line 266
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v1, LapI;->c:LapI;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->a(LapI;)Z

    move-result v0

    return v0
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/view/DocThumbnailView;

    sget-object v1, LapI;->d:LapI;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setState(LapI;Z)V

    .line 274
    return-void
.end method
