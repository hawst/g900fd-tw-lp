.class public LauQ;
.super Ljava/lang/Object;
.source "ProjectorClientService.java"

# interfaces
.implements LauO;


# instance fields
.field final synthetic a:I

.field final synthetic a:Lcom/google/android/apps/viewer/client/ProjectorClientService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/client/ProjectorClientService;I)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, LauQ;->a:Lcom/google/android/apps/viewer/client/ProjectorClientService;

    iput p2, p0, LauQ;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LauM;)V
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p1}, LauM;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 95
    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 96
    iget v2, p0, LauQ;->a:I

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 97
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 99
    :try_start_0
    iget-object v0, p0, LauQ;->a:Lcom/google/android/apps/viewer/client/ProjectorClientService;

    invoke-static {v0}, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a(Lcom/google/android/apps/viewer/client/ProjectorClientService;)Landroid/os/Messenger;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Problem contacting projector"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
