.class Larw;
.super Ljava/lang/Object;
.source "HoneycombActionBarHelper.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Landroid/view/MenuItem;

.field final synthetic a:Lart;


# direct methods
.method constructor <init>(Lart;Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Larw;->a:Lart;

    iput-object p2, p0, Larw;->a:Landroid/view/MenuItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 343
    if-nez p2, :cond_0

    .line 362
    :goto_0
    return-void

    .line 350
    :cond_0
    sget-boolean v0, Lart;->a:Z

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Larw;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    .line 355
    :cond_1
    iget-object v0, p0, Larw;->a:Lart;

    iget-object v0, v0, Lart;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onSearchRequested()Z

    .line 361
    iget-object v0, p0, Larw;->a:Lart;

    invoke-static {v0}, Lart;->a(Lart;)Landroid/widget/SearchView;

    move-result-object v0

    iget-object v1, p0, Larw;->a:Lart;

    invoke-static {v1}, Lart;->a(Lart;)Landroid/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method
