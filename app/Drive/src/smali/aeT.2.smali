.class public abstract LaeT;
.super Ljava/lang/Object;
.source "AbstractPreparedSyncMore.java"

# interfaces
.implements Laff;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private a:LpD;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LaeT;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method static synthetic a(LaeT;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, LaeT;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method


# virtual methods
.method protected abstract a(I)Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public a()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LaeT;->a:LpD;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, LaeT;->a:LpD;

    invoke-virtual {v0}, LpD;->a()V

    .line 71
    :cond_0
    return-void
.end method

.method public final a(Lafg;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, LaeT;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    const-string v0, "AbstractPreparedSyncMore"

    const-string v3, "Prepared sync not starting - already in state %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LaeT;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 57
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {p0}, LaeT;->c()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Already finished"

    invoke-static {v0, v3}, LbiT;->b(ZLjava/lang/Object;)V

    .line 37
    if-ltz p2, :cond_2

    :goto_2
    invoke-static {v1}, LbiT;->a(Z)V

    .line 39
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    new-instance v0, LaeU;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Running "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2, p1}, LaeU;-><init>(LaeT;Ljava/lang/String;ILafg;)V

    iput-object v0, p0, LaeT;->a:LpD;

    .line 56
    iget-object v0, p0, LaeT;->a:LpD;

    invoke-virtual {v0}, LpD;->start()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 36
    goto :goto_1

    :cond_2
    move v1, v2

    .line 37
    goto :goto_2
.end method

.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 61
    iget-object v1, p0, LaeT;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
