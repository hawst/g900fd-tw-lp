.class public final LaMt;
.super Ljava/lang/Object;


# instance fields
.field private a:J

.field private final a:LaMv;

.field private a:Lcom/google/android/gms/internal/an;

.field private final a:Ljava/lang/Runnable;

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(LaVl;)V
    .locals 2

    new-instance v0, LaMv;

    sget-object v1, LaQb;->a:Landroid/os/Handler;

    invoke-direct {v0, v1}, LaMv;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, v0}, LaMt;-><init>(LaVl;LaMv;)V

    return-void
.end method

.method constructor <init>(LaVl;LaMv;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, LaMt;->a:Z

    iput-boolean v0, p0, LaMt;->b:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaMt;->a:J

    iput-object p2, p0, LaMt;->a:LaMv;

    new-instance v0, LaMu;

    invoke-direct {v0, p0, p1}, LaMu;-><init>(LaMt;LaVl;)V

    iput-object v0, p0, LaMt;->a:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(LaMt;)Lcom/google/android/gms/internal/an;
    .locals 1

    iget-object v0, p0, LaMt;->a:Lcom/google/android/gms/internal/an;

    return-object v0
.end method

.method static synthetic a(LaMt;Z)Z
    .locals 0

    iput-boolean p1, p0, LaMt;->a:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, LaMt;->a:Z

    iget-object v0, p0, LaMt;->a:LaMv;

    iget-object v1, p0, LaMt;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LaMv;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/an;)V
    .locals 2

    const-wide/32 v0, 0xea60

    invoke-virtual {p0, p1, v0, v1}, LaMt;->a(Lcom/google/android/gms/internal/an;J)V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/an;J)V
    .locals 2

    iget-boolean v0, p0, LaMt;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "An ad refresh is already scheduled."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, LaMt;->a:Lcom/google/android/gms/internal/an;

    const/4 v0, 0x1

    iput-boolean v0, p0, LaMt;->a:Z

    iput-wide p2, p0, LaMt;->a:J

    iget-boolean v0, p0, LaMt;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Scheduling ad refresh "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " milliseconds from now."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    iget-object v0, p0, LaMt;->a:LaMv;

    iget-object v1, p0, LaMt;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p2, p3}, LaMv;->a(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, LaMt;->b:Z

    iget-boolean v0, p0, LaMt;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaMt;->a:LaMv;

    iget-object v1, p0, LaMt;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LaMv;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    const/4 v1, 0x0

    iput-boolean v1, p0, LaMt;->b:Z

    iget-boolean v0, p0, LaMt;->a:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, LaMt;->a:Z

    iget-object v0, p0, LaMt;->a:Lcom/google/android/gms/internal/an;

    iget-wide v2, p0, LaMt;->a:J

    invoke-virtual {p0, v0, v2, v3}, LaMt;->a(Lcom/google/android/gms/internal/an;J)V

    :cond_0
    return-void
.end method
