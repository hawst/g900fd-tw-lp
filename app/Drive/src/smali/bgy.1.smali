.class public Lbgy;
.super Ljava/lang/Object;
.source "EditorActivity.java"


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/EditorActivity;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/EditorActivity;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 496
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 497
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 501
    :cond_1
    const-string v1, "ACTION_UPDATE_ADDED_PAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 502
    iget-object v0, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lbhf;

    move-result-object v0

    invoke-virtual {v0}, Lbhf;->a()LbgX;

    move-result-object v0

    invoke-virtual {v0}, LbgX;->b()I

    move-result v0

    .line 503
    if-ltz v0, :cond_2

    .line 504
    iget-object v1, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v1}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lcom/google/bionics/scanner/ui/DocumentEditorView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->setActivePageIndex(I)V

    .line 505
    iget-object v1, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v1

    iget-object v2, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v2}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lbhf;

    move-result-object v2

    invoke-virtual {v2}, Lbhf;->a()LbgX;

    move-result-object v2

    .line 506
    invoke-virtual {v2, v0}, LbgX;->a(I)LbgY;

    move-result-object v0

    invoke-virtual {v0}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v0

    .line 505
    invoke-virtual {v1, v0}, Lbhm;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    goto :goto_0

    .line 509
    :cond_2
    iget-object v0, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setVisibility(I)V

    .line 510
    iget-object v0, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v0}, Lcom/google/bionics/scanner/EditorActivity;->b(Lcom/google/bionics/scanner/EditorActivity;)Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbhm;->b(Z)V

    goto :goto_0

    .line 513
    :cond_3
    const-string v1, "ACTION_UPDATE_REPLACED_PAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    const-string v0, "com.google.bionics.scanner.extra.DOCUMENT_PAGE_INDEX"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 515
    if-ltz v0, :cond_0

    .line 516
    iget-object v1, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v1}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lcom/google/bionics/scanner/ui/DocumentEditorView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->setActivePageIndex(I)V

    .line 517
    iget-object v1, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v1

    iget-object v2, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v2}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lbhf;

    move-result-object v2

    invoke-virtual {v2}, Lbhf;->a()LbgX;

    move-result-object v2

    .line 518
    invoke-virtual {v2, v0}, LbgX;->a(I)LbgY;

    move-result-object v2

    invoke-virtual {v2}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v2

    .line 517
    invoke-virtual {v1, v2}, Lbhm;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    .line 519
    sget-object v1, Lbgw;->a:[I

    iget-object v2, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v2}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lbgx;

    move-result-object v2

    invoke-virtual {v2}, Lbgx;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 522
    :pswitch_0
    iget-object v1, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    iget-object v2, p0, Lbgy;->a:Lcom/google/bionics/scanner/EditorActivity;

    invoke-static {v2}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;)Lbhf;

    move-result-object v2

    invoke-virtual {v2}, Lbhf;->a()LbgX;

    move-result-object v2

    invoke-virtual {v2, v0}, LbgX;->a(I)LbgY;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/bionics/scanner/EditorActivity;->a(Lcom/google/bionics/scanner/EditorActivity;LbgY;)V

    goto/16 :goto_0

    .line 519
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
