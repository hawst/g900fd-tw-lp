.class public abstract LbdH;
.super Ljava/lang/Object;
.source "AbstractGoogleClient.java"


# static fields
.field static final LOGGER:Ljava/util/logging/Logger;


# instance fields
.field private final applicationName:Ljava/lang/String;

.field private final googleClientRequestInitializer:LbdM;

.field private final objectParser:LbfL;

.field private final requestFactory:Lbej;

.field private final rootUrl:Ljava/lang/String;

.field private final servicePath:Ljava/lang/String;

.field private suppressPatternChecks:Z

.field private suppressRequiredParameterChecks:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, LbdH;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LbdH;->LOGGER:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>(LbdI;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iget-object v0, p1, LbdI;->googleClientRequestInitializer:LbdM;

    iput-object v0, p0, LbdH;->googleClientRequestInitializer:LbdM;

    .line 78
    iget-object v0, p1, LbdI;->rootUrl:Ljava/lang/String;

    invoke-static {v0}, LbdH;->normalizeRootUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LbdH;->rootUrl:Ljava/lang/String;

    .line 79
    iget-object v0, p1, LbdI;->servicePath:Ljava/lang/String;

    invoke-static {v0}, LbdH;->normalizeServicePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LbdH;->servicePath:Ljava/lang/String;

    .line 80
    iget-object v0, p1, LbdI;->applicationName:Ljava/lang/String;

    invoke-static {v0}, LbfT;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, LbdH;->LOGGER:Ljava/util/logging/Logger;

    const-string v1, "Application name is not set. Call Builder#setApplicationName."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 83
    :cond_0
    iget-object v0, p1, LbdI;->applicationName:Ljava/lang/String;

    iput-object v0, p0, LbdH;->applicationName:Ljava/lang/String;

    .line 84
    iget-object v0, p1, LbdI;->httpRequestInitializer:Lbek;

    if-nez v0, :cond_1

    iget-object v0, p1, LbdI;->transport:Lbeq;

    .line 85
    invoke-virtual {v0}, Lbeq;->a()Lbej;

    move-result-object v0

    .line 86
    :goto_0
    iput-object v0, p0, LbdH;->requestFactory:Lbej;

    .line 87
    iget-object v0, p1, LbdI;->objectParser:LbfL;

    iput-object v0, p0, LbdH;->objectParser:LbfL;

    .line 88
    iget-boolean v0, p1, LbdI;->suppressPatternChecks:Z

    iput-boolean v0, p0, LbdH;->suppressPatternChecks:Z

    .line 89
    iget-boolean v0, p1, LbdI;->suppressRequiredParameterChecks:Z

    iput-boolean v0, p0, LbdH;->suppressRequiredParameterChecks:Z

    .line 90
    return-void

    .line 85
    :cond_1
    iget-object v0, p1, LbdI;->transport:Lbeq;

    iget-object v1, p1, LbdI;->httpRequestInitializer:Lbek;

    .line 86
    invoke-virtual {v0, v1}, Lbeq;->a(Lbek;)Lbej;

    move-result-object v0

    goto :goto_0
.end method

.method static normalizeRootUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 252
    const-string v0, "root URL cannot be null."

    invoke-static {p0, v0}, LbfN;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 256
    :cond_0
    return-object p0
.end method

.method static normalizeServicePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 264
    const-string v0, "service path cannot be null"

    invoke-static {p0, v0}, LbfN;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 266
    const-string v0, "/"

    .line 267
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "service path must equal \"/\" if it is of length 1."

    .line 266
    invoke-static {v0, v1}, LbfN;->a(ZLjava/lang/Object;)V

    .line 268
    const-string p0, ""

    .line 277
    :cond_0
    :goto_0
    return-object p0

    .line 269
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 270
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 273
    :cond_2
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final batch()Lbdm;
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LbdH;->batch(Lbek;)Lbdm;

    move-result-object v0

    return-object v0
.end method

.method public final batch(Lbek;)Lbdm;
    .locals 4

    .prologue
    .line 230
    new-instance v0, Lbdm;

    .line 231
    invoke-virtual {p0}, LbdH;->getRequestFactory()Lbej;

    move-result-object v1

    invoke-virtual {v1}, Lbej;->a()Lbeq;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lbdm;-><init>(Lbeq;Lbek;)V

    .line 232
    new-instance v1, LbdY;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LbdH;->getRootUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "batch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LbdY;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbdm;->a(LbdY;)Lbdm;

    .line 233
    return-object v0
.end method

.method public final getApplicationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, LbdH;->applicationName:Ljava/lang/String;

    return-object v0
.end method

.method public final getBaseUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LbdH;->rootUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LbdH;->servicePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getGoogleClientRequestInitializer()LbdM;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, LbdH;->googleClientRequestInitializer:LbdM;

    return-object v0
.end method

.method public getObjectParser()LbfL;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, LbdH;->objectParser:LbfL;

    return-object v0
.end method

.method public final getRequestFactory()Lbej;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, LbdH;->requestFactory:Lbej;

    return-object v0
.end method

.method public final getRootUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, LbdH;->rootUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getServicePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, LbdH;->servicePath:Ljava/lang/String;

    return-object v0
.end method

.method public final getSuppressPatternChecks()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, LbdH;->suppressPatternChecks:Z

    return v0
.end method

.method public final getSuppressRequiredParameterChecks()Z
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, LbdH;->suppressRequiredParameterChecks:Z

    return v0
.end method

.method public initialize(LbdJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbdJ",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-virtual {p0}, LbdH;->getGoogleClientRequestInitializer()LbdM;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p0}, LbdH;->getGoogleClientRequestInitializer()LbdM;

    move-result-object v0

    invoke-interface {v0, p1}, LbdM;->initialize(LbdJ;)V

    .line 189
    :cond_0
    return-void
.end method
