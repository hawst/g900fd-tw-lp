.class final enum LFP;
.super Ljava/lang/Enum;
.source "DasherHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LFP;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LFP;

.field private static final synthetic a:[LFP;

.field public static final enum b:LFP;


# instance fields
.field final a:I

.field final a:Ljava/util/regex/Pattern;

.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 65
    new-instance v0, LFP;

    const-string v1, "TESLA_DASHER"

    invoke-static {}, LFO;->a()Ljava/util/regex/Pattern;

    move-result-object v3

    const/4 v5, 0x4

    invoke-direct/range {v0 .. v5}, LFP;-><init>(Ljava/lang/String;ILjava/util/regex/Pattern;II)V

    sput-object v0, LFP;->a:LFP;

    new-instance v5, LFP;

    const-string v6, "NON_TESLA_DASHER"

    .line 66
    invoke-static {}, LFO;->b()Ljava/util/regex/Pattern;

    move-result-object v8

    const/4 v10, 0x3

    move v7, v4

    move v9, v4

    invoke-direct/range {v5 .. v10}, LFP;-><init>(Ljava/lang/String;ILjava/util/regex/Pattern;II)V

    sput-object v5, LFP;->b:LFP;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [LFP;

    sget-object v1, LFP;->a:LFP;

    aput-object v1, v0, v2

    sget-object v1, LFP;->b:LFP;

    aput-object v1, v0, v4

    sput-object v0, LFP;->a:[LFP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/util/regex/Pattern;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/regex/Pattern;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 73
    iput-object p3, p0, LFP;->a:Ljava/util/regex/Pattern;

    .line 74
    iput p4, p0, LFP;->a:I

    .line 75
    iput p5, p0, LFP;->b:I

    .line 76
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LFP;
    .locals 1

    .prologue
    .line 64
    const-class v0, LFP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LFP;

    return-object v0
.end method

.method public static values()[LFP;
    .locals 1

    .prologue
    .line 64
    sget-object v0, LFP;->a:[LFP;

    invoke-virtual {v0}, [LFP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LFP;

    return-object v0
.end method
