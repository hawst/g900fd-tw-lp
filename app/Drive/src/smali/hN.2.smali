.class public LhN;
.super LH;
.source "ActionBarActivity.java"

# interfaces
.implements Lbs;
.implements Lm;


# instance fields
.field private a:LhO;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, LH;-><init>()V

    return-void
.end method

.method private a()LhO;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, LhN;->a:LhO;

    if-nez v0, :cond_0

    .line 557
    invoke-static {p0}, LhO;->a(LhN;)LhO;

    move-result-object v0

    iput-object v0, p0, LhN;->a:LhO;

    .line 559
    :cond_0
    iget-object v0, p0, LhN;->a:LhO;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 466
    invoke-static {p0}, Lar;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a()LhJ;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->b()LhJ;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ll;
    .locals 1

    .prologue
    .line 504
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->a()Ll;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmv;)Lmu;
    .locals 1

    .prologue
    .line 228
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1}, LhO;->a(Lmv;)Lmu;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/view/Menu;)V
    .locals 0

    .prologue
    .line 288
    invoke-super {p0, p1, p2}, LH;->onPanelClosed(ILandroid/view/Menu;)V

    .line 289
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 499
    invoke-static {p0, p1}, Lar;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 500
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 264
    invoke-super {p0, p1}, LH;->setContentView(Landroid/view/View;)V

    .line 265
    return-void
.end method

.method public a(Lbr;)V
    .locals 0

    .prologue
    .line 385
    invoke-virtual {p1, p0}, Lbr;->a(Landroid/app/Activity;)Lbr;

    .line 386
    return-void
.end method

.method public a(Lmu;)V
    .locals 0

    .prologue
    .line 216
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 430
    invoke-virtual {p0}, LhN;->a()Landroid/content/Intent;

    move-result-object v0

    .line 432
    if-eqz v0, :cond_1

    .line 433
    invoke-virtual {p0, v0}, LhN;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 434
    invoke-static {p0}, Lbr;->a(Landroid/content/Context;)Lbr;

    move-result-object v0

    .line 435
    invoke-virtual {p0, v0}, LhN;->a(Lbr;)V

    .line 436
    invoke-virtual {p0, v0}, LhN;->b(Lbr;)V

    .line 437
    invoke-virtual {v0}, Lbr;->a()V

    .line 440
    :try_start_0
    invoke-static {p0}, Lr;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    :goto_0
    const/4 v0, 0x1

    .line 453
    :goto_1
    return v0

    .line 441
    :catch_0
    move-exception v0

    .line 444
    invoke-virtual {p0}, LhN;->finish()V

    goto :goto_0

    .line 449
    :cond_0
    invoke-virtual {p0, v0}, LhN;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 453
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 276
    invoke-super {p0, p1, p2}, LH;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 280
    invoke-super {p0, p1, p2, p3}, LH;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 483
    invoke-static {p0, p1}, Lar;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method protected a(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 256
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    return-void
.end method

.method public b(Lbr;)V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public b(Lmu;)V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method public b(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 292
    invoke-super {p0, p1, p2}, LH;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method b(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 284
    invoke-super {p0, p1, p2}, LH;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->c()V

    .line 200
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 542
    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->a()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->c()V

    .line 207
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 297
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    invoke-super {p0}, LH;->onBackPressed()V

    .line 300
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0, p1}, LH;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 129
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1}, LhO;->a(Landroid/content/res/Configuration;)V

    .line 130
    return-void
.end method

.method public final onContentChanged()V
    .locals 1

    .prologue
    .line 534
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->d()V

    .line 535
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0, p1}, LH;->onCreate(Landroid/os/Bundle;)V

    .line 123
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1}, LhO;->a(Landroid/os/Bundle;)V

    .line 124
    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->b(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1}, LhO;->a(I)Landroid/view/View;

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, LH;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 547
    invoke-super {p0, p1, p2, p3}, LH;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_0

    .line 552
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, LhO;->a(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, LH;->onDestroy()V

    .line 170
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->e()V

    .line 171
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 524
    invoke-super {p0, p1, p2}, LH;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    const/4 v0, 0x1

    .line 527
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 518
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 155
    invoke-super {p0, p1, p2}, LH;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    .line 159
    :cond_0
    invoke-virtual {p0}, LhN;->a()LhJ;

    move-result-object v0

    .line 160
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LhJ;->a()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {p0}, LhN;->a()Z

    move-result v0

    goto :goto_0

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 248
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->a(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->a(ILandroid/view/Menu;)V

    .line 244
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, LH;->onPostResume()V

    .line 141
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->b()V

    .line 142
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 238
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LhO;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, LH;->onStop()V

    .line 135
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0}, LhO;->a()V

    .line 136
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1, p2}, LH;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 176
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1}, LhO;->a(Ljava/lang/CharSequence;)V

    .line 177
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1}, LhO;->a(I)V

    .line 103
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1}, LhO;->a(Landroid/view/View;)V

    .line 108
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, LhN;->a()LhO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LhO;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    return-void
.end method
