.class LaIx;
.super Landroid/os/AsyncTask;
.source "StorageFileWriter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGo;

.field final synthetic a:LaIw;

.field final synthetic a:Ljava/lang/Long;

.field final synthetic a:[Landroid/os/ParcelFileDescriptor;


# direct methods
.method constructor <init>(LaIw;LaGo;[Landroid/os/ParcelFileDescriptor;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, LaIx;->a:LaIw;

    iput-object p2, p0, LaIx;->a:LaGo;

    iput-object p3, p0, LaIx;->a:[Landroid/os/ParcelFileDescriptor;

    iput-object p4, p0, LaIx;->a:Ljava/lang/Long;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 88
    :try_start_0
    iget-object v0, p0, LaIx;->a:LaIw;

    iget-object v1, p0, LaIx;->a:LaGo;

    iget-object v2, p0, LaIx;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v3, p0, LaIx;->a:Ljava/lang/Long;

    invoke-static {v0, v1, v2, v3}, LaIw;->a(LaIw;LaGo;Landroid/os/ParcelFileDescriptor;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :try_start_1
    iget-object v0, p0, LaIx;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 98
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string v1, "StorageFileWriter"

    const-string v2, "Failure closing pipe"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 89
    :catch_1
    move-exception v0

    .line 90
    :try_start_2
    const-string v1, "StorageFileWriter"

    const-string v2, "Failure writing new document content"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    :try_start_3
    iget-object v0, p0, LaIx;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 94
    :catch_2
    move-exception v0

    .line 95
    const-string v1, "StorageFileWriter"

    const-string v2, "Failure closing pipe"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    .line 93
    :try_start_4
    iget-object v1, p0, LaIx;->a:[Landroid/os/ParcelFileDescriptor;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 96
    :goto_1
    throw v0

    .line 94
    :catch_3
    move-exception v1

    .line 95
    const-string v2, "StorageFileWriter"

    const-string v3, "Failure closing pipe"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaIx;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
