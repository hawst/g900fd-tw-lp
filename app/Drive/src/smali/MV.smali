.class public LMV;
.super LMN;
.source "DocsUploaderImpl.java"


# instance fields
.field private final a:LaGM;

.field private final a:Laer;

.field private final a:LahK;

.field private final a:LqK;


# direct methods
.method public constructor <init>(LTh;LQr;LbwT;LaGM;Laer;LqK;LahK;)V
    .locals 0
    .param p3    # LbwT;
        .annotation runtime Lbxv;
            a = "DocFeed"
        .end annotation
    .end param

    .prologue
    .line 244
    invoke-direct {p0, p1, p2, p3, p4}, LMN;-><init>(LTh;LQr;LbwT;LaGM;)V

    .line 245
    iput-object p4, p0, LMV;->a:LaGM;

    .line 246
    iput-object p5, p0, LMV;->a:Laer;

    .line 247
    iput-object p6, p0, LMV;->a:LqK;

    .line 248
    iput-object p7, p0, LMV;->a:LahK;

    .line 249
    return-void
.end method

.method static synthetic a(LMV;)LaGM;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LMV;->a:LaGM;

    return-object v0
.end method

.method static synthetic a(LMV;)Laer;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LMV;->a:Laer;

    return-object v0
.end method

.method static synthetic a(LMV;)LahK;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LMV;->a:LahK;

    return-object v0
.end method

.method static synthetic a(LMV;)LqK;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LMV;->a:LqK;

    return-object v0
.end method


# virtual methods
.method public a(LMZ;LagH;)LaJT;
    .locals 3

    .prologue
    .line 263
    .line 264
    invoke-virtual {p1}, LMZ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ItemToUpload must have entrySpec to be uploaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 263
    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 266
    new-instance v0, LMW;

    iget-object v1, p0, LMV;->a:LahK;

    invoke-direct {v0, p0, p1, p2, v1}, LMW;-><init>(LMV;LMZ;LagH;LahK;)V

    iput-object v0, p0, LMV;->a:LNi;

    .line 267
    iget-object v0, p0, LMV;->a:LNi;

    invoke-interface {v0}, LNi;->c()LaJT;

    move-result-object v0

    return-object v0

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
