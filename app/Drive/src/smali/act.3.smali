.class public Lact;
.super Ljava/lang/Object;
.source "SharingInfoManagerImpl.java"

# interfaces
.implements Lacs;


# instance fields
.field a:LaKR;

.field a:LabF;

.field a:Labe;

.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method static synthetic a(Lact;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/util/Set;)Lacr;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lact;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/util/Set;)Lacr;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/util/Set;)Lacr;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
            "Ljava/util/Set",
            "<",
            "Lqo;",
            ">;)",
            "Lacr;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 272
    new-instance v4, Lacx;

    invoke-direct {v4, v1}, Lacx;-><init>(Lacu;)V

    .line 273
    sget-object v3, Lqu;->n:Lqu;

    .line 276
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqo;

    .line 277
    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v6

    sget-object v7, Lqx;->b:Lqx;

    if-eq v6, v7, :cond_0

    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v6

    sget-object v7, Lqx;->a:Lqx;

    if-eq v6, v7, :cond_0

    .line 278
    invoke-virtual {v0}, Lqo;->a()Lqu;

    move-result-object v3

    .line 279
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v2

    .line 280
    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v6

    sget-object v7, Lqx;->c:Lqx;

    if-ne v6, v7, :cond_2

    .line 281
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    move-object v2, v3

    :goto_1
    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 292
    goto :goto_0

    .line 284
    :cond_0
    iget-object v6, p0, Lact;->a:LabF;

    .line 285
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v8

    .line 284
    invoke-interface {v6, v7, v8}, LabF;->a(Ljava/lang/String;Lqx;)LabD;

    move-result-object v6

    .line 286
    new-instance v7, LacD;

    new-instance v8, Laci;

    invoke-direct {v8, v0}, Laci;-><init>(Lqo;)V

    invoke-direct {v7, v6, v8}, LacD;-><init>(LabD;Laci;)V

    invoke-virtual {v4, v7}, Lacx;->add(Ljava/lang/Object;)Z

    .line 287
    invoke-virtual {v0}, Lqo;->a()Lqv;

    move-result-object v6

    sget-object v7, Lqv;->a:Lqv;

    if-ne v6, v7, :cond_2

    if-nez v1, :cond_2

    .line 289
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LacH;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    .line 293
    :cond_1
    new-instance v0, LRQ;

    invoke-direct {v0}, LRQ;-><init>()V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 294
    new-instance v0, Lacy;

    invoke-direct {v0, p0, p1}, Lacy;-><init>(Lact;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 295
    invoke-static {v0, v4, v3, v2, v1}, Lacy;->a(Lacy;Lacx;Lqu;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return-object v0

    :cond_2
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lacr;
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lacy;

    invoke-direct {v0, p0, p1}, Lacy;-><init>(Lact;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    return-object v0
.end method

.method public a(Lacr;)LbsU;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lacr;",
            ")",
            "LbsU",
            "<",
            "Lacr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323
    iget-object v0, p0, Lact;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    new-instance v0, LQn;

    invoke-direct {v0}, LQn;-><init>()V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    .line 355
    :goto_0
    return-object v0

    .line 326
    :cond_0
    invoke-interface {p1}, Lacr;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 327
    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 329
    :cond_1
    invoke-interface {p1}, Lacr;->a()Ljava/util/List;

    move-result-object v0

    .line 330
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 331
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 332
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 333
    invoke-virtual {v0}, LacD;->a()Lqo;

    move-result-object v4

    invoke-virtual {v4}, Lqo;->a()Lqt;

    move-result-object v4

    sget-object v5, Lqt;->f:Lqt;

    invoke-virtual {v4, v5}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 334
    invoke-virtual {v0}, LacD;->a()Lqo;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    :cond_3
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v4

    invoke-virtual {v4}, Laci;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 337
    invoke-virtual {v0}, LacD;->a()Lqo;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 341
    :cond_4
    invoke-interface {p1}, Lacr;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 342
    invoke-interface {p1}, Lacr;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 344
    invoke-interface {p1}, Lacr;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 346
    iget-object v3, p0, Lact;->a:Labe;

    iget-object v4, v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 347
    invoke-interface {v3, v4, v2}, Labe;->a(LaFO;Ljava/util/Set;)LbsU;

    move-result-object v2

    .line 348
    new-instance v3, Lacv;

    invoke-direct {v3, p0, v0, v1}, Lacv;-><init>(Lact;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/util/Set;)V

    .line 355
    invoke-static {v2, v3}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
            ")",
            "LbsU",
            "<",
            "Lacr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lact;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    new-instance v0, LQn;

    invoke-direct {v0}, LQn;-><init>()V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    .line 318
    :goto_0
    return-object v0

    .line 310
    :cond_0
    iget-object v0, p0, Lact;->a:Labe;

    invoke-interface {v0, p1}, Labe;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;

    move-result-object v0

    .line 311
    new-instance v1, Lacu;

    invoke-direct {v1, p0, p1}, Lacu;-><init>(Lact;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 318
    invoke-static {v0, v1}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
