.class public LPq;
.super Ljava/lang/Object;
.source "EntryActionHelperImpl.java"

# interfaces
.implements LPp;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LBZ;

.field private final a:LUi;

.field private final a:LWL;

.field private final a:LaGM;

.field private final a:LaKR;

.field private final a:LabQ;

.field private final a:Lach;

.field private final a:Lacj;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lald;

.field private final a:Ljava/lang/String;

.field private final a:LqK;

.field private final a:LsP;

.field private final a:LtB;

.field private final a:LtK;

.field private final a:LvU;

.field private final a:LvY;

.field private final a:Lvq;

.field private a:Z


# direct methods
.method public constructor <init>(Laja;LaGM;LBZ;LtK;LqK;LUi;Lacj;LvU;Lald;LsP;LtB;Ljava/lang/String;LabQ;Lach;LvY;LaKR;LWL;LbiP;)V
    .locals 2
    .param p12    # Ljava/lang/String;
        .annotation runtime LaEy;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaGM;",
            "LBZ;",
            "LtK;",
            "LqK;",
            "LUi;",
            "Lacj;",
            "LvU;",
            "Lald;",
            "LsP;",
            "LtB;",
            "Ljava/lang/String;",
            "LabQ;",
            "Lach;",
            "LvY;",
            "LaKR;",
            "LWL;",
            "LbiP",
            "<",
            "Lvq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v1, 0x0

    iput-boolean v1, p0, LPq;->a:Z

    .line 120
    iput-object p1, p0, LPq;->a:Laja;

    .line 121
    iput-object p2, p0, LPq;->a:LaGM;

    .line 122
    iput-object p3, p0, LPq;->a:LBZ;

    .line 123
    iput-object p4, p0, LPq;->a:LtK;

    .line 124
    iput-object p5, p0, LPq;->a:LqK;

    .line 125
    iput-object p6, p0, LPq;->a:LUi;

    .line 126
    iput-object p7, p0, LPq;->a:Lacj;

    .line 127
    iput-object p8, p0, LPq;->a:LvU;

    .line 128
    iput-object p10, p0, LPq;->a:LsP;

    .line 129
    iput-object p9, p0, LPq;->a:Lald;

    .line 130
    iput-object p11, p0, LPq;->a:LtB;

    .line 131
    iput-object p12, p0, LPq;->a:Ljava/lang/String;

    .line 132
    iput-object p13, p0, LPq;->a:LabQ;

    .line 133
    move-object/from16 v0, p15

    iput-object v0, p0, LPq;->a:LvY;

    .line 134
    move-object/from16 v0, p14

    iput-object v0, p0, LPq;->a:Lach;

    .line 135
    move-object/from16 v0, p16

    iput-object v0, p0, LPq;->a:LaKR;

    .line 136
    move-object/from16 v0, p17

    iput-object v0, p0, LPq;->a:LWL;

    .line 137
    invoke-virtual/range {p18 .. p18}, LbiP;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvq;

    iput-object v1, p0, LPq;->a:Lvq;

    .line 138
    return-void
.end method

.method private a()LH;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, LPq;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 142
    instance-of v1, v0, LH;

    invoke-static {v1}, LbiT;->a(Z)V

    .line 143
    check-cast v0, LH;

    return-object v0
.end method

.method static synthetic a(LPq;)Lacj;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, LPq;->a:Lacj;

    return-object v0
.end method

.method public static a(LaGu;Lach;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 347
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    invoke-virtual {p1, p0}, Lach;->a(LaGu;)Ljava/lang/String;

    move-result-object v1

    .line 351
    if-nez v1, :cond_0

    .line 352
    const-string v0, "EntryActionHelper"

    const-string v1, "Can\'t send link for: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 353
    const/4 v0, 0x0

    .line 360
    :goto_0
    return-object v0

    .line 356
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 357
    const-string v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    const-string v2, "android.intent.extra.SUBJECT"

    invoke-interface {p0}, LaGu;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(LPq;)LvU;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, LPq;->a:LvU;

    return-object v0
.end method

.method private a(LH;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 334
    :try_start_0
    sget v0, Lxi;->menu_send_link:I

    .line 335
    invoke-virtual {p1, v0}, LH;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 336
    invoke-virtual {p1, v0}, LH;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v0

    .line 338
    const-string v1, "EntryActionHelper"

    const-string v2, "Failed to send link"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 339
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LPq;->a(Z)V

    goto :goto_0
.end method

.method private a(LaGu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 323
    iget-object v1, p0, LPq;->a:Lacj;

    invoke-virtual {v1}, Lacj;->a()Lacr;

    move-result-object v1

    .line 324
    if-nez v1, :cond_1

    .line 329
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Lacr;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lacr;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaGD;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 4

    .prologue
    .line 192
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v0

    .line 194
    iget-object v1, p0, LPq;->a:LqK;

    const-string v2, "doclist"

    const-string v3, "showDeleteEvent"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-static {p1, p2}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a(LaGD;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;

    move-result-object v1

    .line 197
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v2, "RemoveDialogFragment"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 198
    return-void
.end method

.method public a(LaGu;)V
    .locals 4

    .prologue
    .line 148
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v0

    .line 150
    iget-object v1, p0, LPq;->a:LqK;

    const-string v2, "doclist"

    const-string v3, "showRenameEvent"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-static {p1}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(LaGu;)Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    move-result-object v1

    .line 152
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v2, "RenameDialogFragment"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method public a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 2

    .prologue
    .line 268
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v0

    check-cast v0, Lrm;

    .line 270
    iget-boolean v1, p0, LPq;->a:Z

    if-nez v1, :cond_0

    .line 271
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LPq;->a(Z)V

    .line 273
    iget-object v1, p0, LPq;->a:LsP;

    invoke-virtual {v1, p1, p2}, LsP;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Landroid/content/Intent;

    move-result-object v1

    .line 274
    invoke-virtual {v0, v1}, Lrm;->startActivity(Landroid/content/Intent;)V

    .line 276
    :cond_0
    return-void
.end method

.method public a(LaGu;Z)V
    .locals 1

    .prologue
    .line 365
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    instance-of v0, p1, LaGo;

    if-nez v0, :cond_0

    .line 373
    :goto_0
    return-void

    .line 369
    :cond_0
    check-cast p1, LaGo;

    .line 371
    iget-object v0, p0, LPq;->a:LBZ;

    invoke-interface {v0, p1, p2}, LBZ;->a(LaGo;Z)V

    .line 372
    iget-object v0, p0, LPq;->a:LvU;

    invoke-interface {v0}, LvU;->a()V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;LaGu;)V
    .locals 1

    .prologue
    .line 405
    instance-of v0, p1, Lcom/google/android/apps/docs/app/DocListActivity;

    if-eqz v0, :cond_0

    .line 406
    check-cast p1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {p1, p2}, Lcom/google/android/apps/docs/app/DocListActivity;->a(LaGu;)V

    .line 408
    :cond_0
    return-void
.end method

.method public a(LbmY;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v1

    .line 160
    iget-object v0, p0, LPq;->a:LtK;

    sget-object v2, Lry;->U:Lry;

    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-static {v1, p1}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(Landroid/content/Context;LbmY;)Landroid/content/Intent;

    move-result-object v0

    .line 165
    :goto_0
    invoke-virtual {v1, v0}, LH;->startActivity(Landroid/content/Intent;)V

    .line 166
    return-void

    .line 163
    :cond_0
    invoke-static {p1}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 170
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v0, p0, LPq;->a:Lacj;

    invoke-virtual {v0, p1}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    .line 172
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 262
    iput-boolean p1, p0, LPq;->a:Z

    .line 263
    return-void
.end method

.method public b(LaGu;)V
    .locals 3

    .prologue
    .line 176
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, LPq;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "printEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, LPq;->a:LWL;

    invoke-interface {v0, p1}, LWL;->a(LaGu;)V

    .line 179
    return-void
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 202
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v0

    .line 205
    invoke-static {p1}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;

    move-result-object v1

    .line 206
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v2, "DeleteForeverDialogFragment"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method public c(LaGu;)V
    .locals 1

    .prologue
    .line 221
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->b:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {p0, p1, v0}, LPq;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 223
    return-void
.end method

.method public c(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 211
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    iget-object v0, p0, LPq;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    .line 214
    iget-object v1, p0, LPq;->a:LUi;

    iget-object v0, p0, LPq;->a:Laja;

    .line 215
    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v2, p0, LPq;->a:Lald;

    invoke-static {v0, v2}, LUo;->a(Landroid/content/Context;Lald;)LaHy;

    move-result-object v0

    .line 214
    invoke-interface {v1, p1, v0}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 217
    :cond_0
    return-void
.end method

.method public d(LaGu;)V
    .locals 1

    .prologue
    .line 227
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->d:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {p0, p1, v0}, LPq;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 229
    return-void
.end method

.method public d(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 249
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v0, p0, LPq;->a:Lvq;

    const-string v1, "Shortcut creator not available"

    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v0

    .line 253
    iget-object v1, p0, LPq;->a:Lvq;

    invoke-interface {v1, v0, p1}, Lvq;->b(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v1

    .line 254
    if-eqz v1, :cond_0

    .line 255
    invoke-virtual {v0, v1}, LH;->sendBroadcast(Landroid/content/Intent;)V

    .line 256
    sget v1, Lxi;->shortcut_created:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 258
    :cond_0
    return-void
.end method

.method public e(LaGu;)V
    .locals 4

    .prologue
    .line 233
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    instance-of v0, p1, LaFV;

    if-eqz v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 240
    :cond_0
    invoke-static {p1}, Lala;->a(LaGu;)Ljava/lang/String;

    move-result-object v0

    .line 241
    iget-object v1, p0, LPq;->a:LqK;

    const-string v2, "doclist"

    const-string v3, "downloadEvent"

    invoke-virtual {v1, v2, v3, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->e:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {p0, p1, v0}, LPq;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    goto :goto_0
.end method

.method public e(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 377
    iget-object v0, p0, LPq;->a:LtB;

    invoke-interface {v0}, LtB;->a()Ltz;

    move-result-object v1

    .line 378
    if-nez v1, :cond_0

    .line 401
    :goto_0
    return-void

    .line 381
    :cond_0
    const/4 v0, 0x0

    .line 382
    iget-object v2, p0, LPq;->a:LaGM;

    invoke-interface {v2, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v2

    .line 383
    if-eqz v2, :cond_1

    .line 384
    iget-object v3, p0, LPq;->a:LaGM;

    sget-object v4, LacY;->a:LacY;

    .line 385
    invoke-interface {v3, v2, v4}, LaGM;->a(LaGo;LacY;)LaGp;

    move-result-object v2

    .line 387
    if-eqz v2, :cond_1

    .line 388
    invoke-virtual {v2}, LaGp;->f()Ljava/lang/String;

    move-result-object v2

    .line 389
    if-eqz v2, :cond_1

    .line 390
    invoke-static {v2}, LtA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 395
    :cond_1
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v2

    iget-object v3, p0, LPq;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LH;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 396
    if-nez v0, :cond_2

    .line 397
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    aput-object v2, v3, v5

    invoke-interface {v1, v0, v3}, Ltz;->a(LH;[Ljava/lang/String;)V

    goto :goto_0

    .line 399
    :cond_2
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object v2, v4, v5

    aput-object v0, v4, v6

    invoke-interface {v1, v3, v4}, Ltz;->a(LH;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f(LaGu;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 280
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    invoke-direct {p0}, LPq;->a()LH;

    move-result-object v0

    .line 282
    iget-boolean v1, p0, LPq;->a:Z

    if-eqz v1, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-object v1, p0, LPq;->a:LqK;

    const-string v2, "doclist"

    const-string v3, "sendLinkEvent"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v1, p0, LPq;->a:Lach;

    invoke-static {p1, v1}, LPq;->a(LaGu;Lach;)Landroid/content/Intent;

    move-result-object v1

    .line 288
    if-eqz v1, :cond_0

    .line 292
    const-string v2, "EntryActionHelper"

    const-string v3, "Sending link: %s"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 293
    invoke-virtual {p0, v7}, LPq;->a(Z)V

    .line 294
    iget-object v2, p0, LPq;->a:LtK;

    sget-object v3, Lry;->I:Lry;

    invoke-interface {v2, v3}, LtK;->a(LtJ;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LPq;->a:LaKR;

    .line 295
    invoke-interface {v2}, LaKR;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1}, LPq;->a(LaGu;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 296
    iget-object v2, p0, LPq;->a:LabQ;

    invoke-virtual {v2, p1}, LabQ;->a(LaGu;)LbsU;

    move-result-object v2

    .line 297
    new-instance v3, LPr;

    invoke-direct {v3, p0, v0}, LPr;-><init>(LPq;LH;)V

    invoke-static {v2, v3}, Lanj;->a(LbsU;LbsJ;)V

    .line 319
    :cond_2
    invoke-direct {p0, v0, v1}, LPq;->a(LH;Landroid/content/Intent;)V

    goto :goto_0
.end method
