.class public LatX;
.super Ljava/lang/Object;
.source "PdfViewerActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/viewer/PdfViewerActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/PdfViewerActivity;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 179
    iget-object v0, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-static {v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lcom/google/android/apps/viewer/PdfViewerActivity;)LawZ;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-static {v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lcom/google/android/apps/viewer/PdfViewerActivity;)LawZ;

    move-result-object v0

    invoke-virtual {v0}, LawZ;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v5

    .line 184
    :cond_1
    iget-object v0, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    new-instance v1, LatY;

    invoke-direct {v1, p0}, LatY;-><init>(LatX;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lmv;)Lmu;

    move-result-object v1

    .line 209
    iget-object v0, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    sget v2, Lauj;->action_mode_close_button:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/viewer/PdfViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_2

    .line 211
    iget-object v2, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Laum;->abc_action_mode_done:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 214
    :cond_2
    iget-object v0, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    .line 215
    invoke-virtual {v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lauk;->search:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/widget/FindInFileView;

    .line 216
    iget-object v2, p0, LatX;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-static {v2}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lcom/google/android/apps/viewer/PdfViewerActivity;)LawZ;

    move-result-object v2

    iget-object v2, v2, LawZ;->a:LaxV;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/viewer/widget/FindInFileView;->setFindInFileListener(LaxV;)V

    .line 218
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 220
    invoke-virtual {v0, v2}, Lcom/google/android/apps/viewer/widget/FindInFileView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    invoke-virtual {v1, v0}, Lmu;->a(Landroid/view/View;)V

    .line 222
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "find"

    invoke-virtual {v0, v1}, LavT;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
