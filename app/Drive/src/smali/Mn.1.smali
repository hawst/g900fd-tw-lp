.class public LMn;
.super Ljava/lang/Object;
.source "CrossAppStateRow.java"


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field final a:LaFO;

.field final a:Ljava/lang/Boolean;

.field final a:Ljava/lang/Long;

.field final a:Ljava/lang/String;

.field final b:Ljava/lang/Long;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/Long;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    invoke-static {}, LMp;->values()[LMp;

    move-result-object v1

    .line 34
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, LMn;->a:[Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    :goto_0
    sget-object v2, LMn;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 36
    sget-object v2, LMn;->a:[Ljava/lang/String;

    aget-object v3, v1, v0

    invoke-static {v3}, LMp;->a(LMp;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method

.method constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    sget-object v0, LMp;->b:LMp;

    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LMn;->a:Ljava/lang/String;

    .line 121
    sget-object v0, LMp;->c:LMp;

    .line 122
    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, LMn;->a:LaFO;

    .line 124
    sget-object v0, LMp;->d:LMp;

    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LMn;->b:Ljava/lang/String;

    .line 125
    sget-object v0, LMp;->e:LMp;

    .line 126
    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LMn;->c:Ljava/lang/String;

    .line 127
    sget-object v0, LMp;->f:LMp;

    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LMn;->d:Ljava/lang/String;

    .line 129
    sget-object v0, LMp;->h:LMp;

    .line 130
    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LMn;->a:Ljava/lang/Long;

    .line 131
    sget-object v0, LMp;->i:LMp;

    .line 132
    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LMn;->b:Ljava/lang/Long;

    .line 134
    sget-object v0, LMp;->g:LMp;

    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LMn;->a:Ljava/lang/Boolean;

    .line 136
    sget-object v0, LMp;->j:LMp;

    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LMn;->e:Ljava/lang/String;

    .line 137
    sget-object v0, LMp;->k:LMp;

    invoke-static {v0}, LMp;->a(LMp;)LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LMn;->f:Ljava/lang/String;

    .line 139
    sget-object v0, LMp;->l:LMp;

    invoke-static {v0, p1}, LMn;->a(LMp;Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LMn;->c:Ljava/lang/Long;

    .line 142
    invoke-direct {p0}, LMn;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Row does not contain valid data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    return-void
.end method

.method private static a(LMp;Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 148
    invoke-static {p0}, LMp;->a(LMp;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 149
    if-gez v0, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 152
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 204
    const/4 v0, 0x1

    .line 206
    iget-object v2, p0, LMn;->a:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 207
    const-string v0, "CrossAppStateRow"

    const-string v2, "Cursor contains null resourceId column."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 211
    :cond_0
    iget-object v2, p0, LMn;->a:LaFO;

    if-nez v2, :cond_1

    .line 212
    const-string v0, "CrossAppStateRow"

    const-string v2, "Cursor contains null accountId column."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 216
    :cond_1
    iget-object v2, p0, LMn;->a:Ljava/lang/Long;

    if-nez v2, :cond_2

    .line 217
    const-string v0, "CrossAppStateRow"

    const-string v2, "Cursor contains null lastPinnedChangeTimeMs column."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 221
    :cond_2
    iget-object v2, p0, LMn;->b:Ljava/lang/Long;

    if-nez v2, :cond_3

    .line 222
    const-string v0, "CrossAppStateRow"

    const-string v2, "Cursor contains null lastOfflineContentUpdateTimeMs column."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 226
    :cond_3
    iget-object v2, p0, LMn;->a:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    .line 227
    const-string v0, "CrossAppStateRow"

    const-string v2, "Cursor contains null isPinned column."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 231
    :goto_0
    return v1

    :cond_4
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method a()J
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, LMn;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method a()LaFO;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, LMn;->a:LaFO;

    return-object v0
.end method

.method a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, LMn;->c:Ljava/lang/Long;

    return-object v0
.end method

.method a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, LMn;->a:Ljava/lang/String;

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, LMn;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method b()J
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, LMn;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, LMn;->b:Ljava/lang/String;

    return-object v0
.end method

.method c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, LMn;->c:Ljava/lang/String;

    return-object v0
.end method

.method d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, LMn;->d:Ljava/lang/String;

    return-object v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, LMn;->e:Ljava/lang/String;

    return-object v0
.end method

.method f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, LMn;->f:Ljava/lang/String;

    return-object v0
.end method
