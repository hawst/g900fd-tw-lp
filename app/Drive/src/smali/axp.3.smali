.class public Laxp;
.super Ljava/lang/Object;
.source "SelectionActionMode.java"


# static fields
.field private static final a:[I


# instance fields
.field private final a:Landroid/app/Activity;

.field private final a:Laua;

.field private final a:Laxw;

.field private final a:Ljava/lang/Object;

.field private a:Lmu;

.field private final a:Lmv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Laxp;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x101037e
        0x1010312
    .end array-data
.end method

.method public constructor <init>(Landroid/app/Activity;Laxw;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Laxp;->a:Landroid/app/Activity;

    .line 45
    check-cast p1, LatU;

    invoke-interface {p1}, LatU;->a()Laua;

    move-result-object v0

    iput-object v0, p0, Laxp;->a:Laua;

    .line 46
    iput-object p2, p0, Laxp;->a:Laxw;

    .line 47
    new-instance v0, Laxr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laxr;-><init>(Laxp;Laxq;)V

    iput-object v0, p0, Laxp;->a:Lmv;

    .line 49
    invoke-virtual {p2}, Laxw;->a()Lawe;

    move-result-object v0

    new-instance v1, Laxq;

    invoke-direct {v1, p0}, Laxq;-><init>(Laxp;)V

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laxp;->a:Ljava/lang/Object;

    .line 59
    return-void
.end method

.method static synthetic a(Laxp;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laxp;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Laxp;)Laua;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laxp;->a:Laua;

    return-object v0
.end method

.method static synthetic a(Laxp;)Laxw;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laxp;->a:Laxw;

    return-object v0
.end method

.method static synthetic a(Laxp;Lmu;)Lmu;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Laxp;->a:Lmu;

    return-object p1
.end method

.method static synthetic a(Laxp;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Laxp;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Laxp;->a:Landroid/app/Activity;

    const-string v1, "clipboard"

    .line 138
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 139
    iget-object v1, p0, Laxp;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 140
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 141
    return-void
.end method

.method static synthetic a()[I
    .locals 1

    .prologue
    .line 26
    sget-object v0, Laxp;->a:[I

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Laxp;->a:Laxw;

    invoke-virtual {v0}, Laxw;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, Laxp;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Laxp;->a:Lmu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Laxp;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Laxp;->a:Laua;

    iget-object v1, p0, Laxp;->a:Lmv;

    invoke-virtual {v0, v1}, Laua;->a(Lmv;)Lmu;

    move-result-object v0

    iput-object v0, p0, Laxp;->a:Lmu;

    .line 74
    :cond_0
    invoke-virtual {p0}, Laxp;->a()Z

    move-result v0

    const-string v1, "Calling start() should start the actionMode"

    invoke-static {v0, v1}, LauV;->a(ZLjava/lang/String;)V

    .line 75
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Laxp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Laxp;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()V

    .line 82
    :cond_0
    invoke-virtual {p0}, Laxp;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling finish should finish the actionMode"

    invoke-static {v0, v1}, LauV;->a(ZLjava/lang/String;)V

    .line 83
    return-void

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
