.class LAV;
.super LaGN;
.source "DocListControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Ljava/util/List",
        "<",
        "LvJ;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LAR;

.field final synthetic a:LbmF;


# direct methods
.method constructor <init>(LAR;LbmF;)V
    .locals 0

    .prologue
    .line 681
    iput-object p1, p0, LAV;->a:LAR;

    iput-object p2, p0, LAV;->a:LbmF;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 681
    invoke-virtual {p0, p1}, LAV;->a(LaGM;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGM;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            ")",
            "Ljava/util/List",
            "<",
            "LvJ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 685
    iget-object v0, p0, LAV;->a:LAR;

    .line 686
    invoke-static {v0}, LAR;->a(LAR;)LtK;

    move-result-object v0

    sget-object v1, Lry;->h:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LAV;->a:LbmF;

    .line 687
    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    .line 688
    :goto_0
    iget-object v1, p0, LAV;->a:LbmF;

    .line 689
    invoke-static {v1}, LbnG;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v7, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 690
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 691
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 692
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LCl;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, LAV;->a:LAR;

    .line 693
    invoke-static {v3}, LAR;->a(LAR;)LtK;

    move-result-object v3

    sget-object v4, Lry;->h:Lry;

    invoke-interface {v3, v4}, LtK;->a(LtJ;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 713
    :cond_1
    :goto_1
    invoke-static {v1}, LbnG;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 714
    return-object v0

    .line 687
    :cond_2
    iget-object v0, p0, LAV;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 698
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v3

    iget-object v4, p0, LAV;->a:LAR;

    .line 699
    invoke-static {v4}, LAR;->a(LAR;)Landroid/content/Context;

    move-result-object v4

    .line 698
    invoke-interface {v3, p1, v4}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LaGM;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 700
    iget-object v4, p0, LAV;->a:LbmF;

    iget-object v5, p0, LAV;->a:LbmF;

    .line 701
    invoke-virtual {v5}, LbmF;->size()I

    move-result v5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4, v7, v5}, LbmF;->a(II)LbmF;

    move-result-object v4

    .line 702
    new-instance v5, LvJ;

    invoke-direct {v5, v3, v4}, LvJ;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 704
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v3, p0, LAV;->a:LAR;

    .line 705
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-static {v3, v0}, LAR;->a(LAR;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 681
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, LAV;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LvJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 719
    iget-object v0, p0, LAV;->a:LAR;

    invoke-static {v0}, LAR;->a(LAR;)LAQ;

    move-result-object v0

    invoke-interface {v0, p1}, LAQ;->a(Ljava/util/List;)V

    .line 720
    return-void
.end method
