.class public LawS;
.super Landroid/app/DialogFragment;
.source "PasswordDialog.java"


# instance fields
.field private a:I

.field private a:Z

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private a(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 110
    const v0, 0x10000006

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 113
    invoke-virtual {p0}, LawS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Laum;->button_open:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 114
    const/4 v1, 0x6

    invoke-virtual {p1, v0, v1}, Landroid/widget/EditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 117
    new-instance v0, LawX;

    invoke-direct {v0, p0, p1}, LawX;-><init>(LawS;Landroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 128
    new-instance v0, LawY;

    invoke-direct {v0, p0, p1}, LawY;-><init>(LawS;Landroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 138
    return-void
.end method

.method private a(Landroid/widget/EditText;Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 233
    if-nez p2, :cond_0

    .line 234
    invoke-virtual {p0}, LawS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Laui;->textfield_default_mtrl_alpha:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 239
    :goto_0
    return-void

    .line 236
    :cond_0
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, LawS;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 237
    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(LawS;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, LawS;->c()V

    return-void
.end method

.method static synthetic a(LawS;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, LawS;->b(Landroid/widget/EditText;)V

    return-void
.end method

.method static synthetic a(LawS;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, LawS;->a:Z

    return v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 159
    invoke-virtual {p0}, LawS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "titleDivider"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 160
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_0

    .line 162
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 182
    :cond_0
    return-void
.end method

.method private b(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, LawS;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, LawZ;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LawZ;->a(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 215
    const/4 v0, 0x0

    iput-boolean v0, p0, LawS;->a:Z

    .line 216
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lauj;->label:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 217
    sget v1, Laum;->label_password_first:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 218
    iget v1, p0, LawS;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 220
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lauj;->password:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 221
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v1, v2, :cond_0

    .line 222
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LawS;->a(Landroid/widget/EditText;Z)V

    .line 227
    :goto_0
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lauj;->password_alert:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 228
    return-void

    .line 224
    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, LawS;->b:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, LawS;->a:Z

    .line 196
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lauj;->password:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 197
    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 199
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v1, v2, :cond_0

    .line 200
    invoke-direct {p0, v0, v3}, LawS;->a(Landroid/widget/EditText;Z)V

    .line 202
    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, LawS;->c:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 204
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lauj;->label:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 205
    sget v1, Laum;->label_password_incorrect:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 206
    iget v1, p0, LawS;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 208
    invoke-virtual {p0}, LawS;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    sget v2, Laum;->desc_password_incorrect_message:I

    invoke-static {v0, v1, v2}, LavM;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 211
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lauj;->password_alert:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 212
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, LawS;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 187
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 52
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, LawS;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {p0}, LawS;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 54
    sget v2, Lauk;->dialog_password:I

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 55
    sget v2, Laum;->title_dialog_password:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Laui;->lock:I

    .line 56
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 57
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Laum;->button_open:I

    .line 58
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Laum;->button_exit:I

    .line 59
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 60
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 62
    sget v0, Lauj;->password:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 63
    invoke-direct {p0, v0}, LawS;->a(Landroid/widget/EditText;)V

    .line 66
    new-instance v1, LawT;

    invoke-direct {v1, p0, v2, v0}, LawT;-><init>(LawS;Landroid/app/AlertDialog;Landroid/widget/EditText;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 105
    return-object v2
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 142
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 143
    invoke-virtual {p0}, LawS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Laug;->text_default:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LawS;->a:I

    .line 144
    invoke-virtual {p0}, LawS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Laug;->text_error:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LawS;->c:I

    .line 145
    invoke-virtual {p0}, LawS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Laug;->text_underline:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LawS;->b:I

    .line 147
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lauj;->password:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 148
    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, LawS;->b:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 150
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 151
    invoke-direct {p0}, LawS;->b()V

    .line 154
    :cond_0
    invoke-virtual {p0}, LawS;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 155
    return-void
.end method
