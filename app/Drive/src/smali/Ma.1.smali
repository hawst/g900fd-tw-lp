.class public LMa;
.super Ljava/lang/Object;
.source "SelectionImageViewColorUpdater.java"

# interfaces
.implements LLs;


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, LMa;->a:I

    .line 17
    return-void
.end method

.method private a(Landroid/widget/ImageView;Z)V
    .locals 2

    .prologue
    .line 25
    sget v0, Lxc;->view_tag_key_selection_original_resource:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 26
    if-eqz p2, :cond_0

    .line 27
    sget v0, Lxc;->view_tag_key_selection_original_resource:I

    invoke-virtual {p1}, Landroid/widget/ImageView;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 28
    iget v0, p0, LMa;->a:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 33
    :goto_0
    return-void

    .line 30
    :cond_0
    check-cast v0, Landroid/graphics/ColorFilter;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 31
    sget v0, Lxc;->view_tag_key_selection_original_resource:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Landroid/widget/ImageView;

    invoke-direct {p0, p1, p2}, LMa;->a(Landroid/widget/ImageView;Z)V

    .line 22
    return-void
.end method
