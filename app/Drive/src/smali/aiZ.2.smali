.class public LaiZ;
.super Ljava/lang/Object;
.source "ContextScopedInjectorFactory.java"


# annotations
.annotation runtime LbuO;
.end annotation


# instance fields
.field private final a:LaiU;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LaiY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lbxw;LaiU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LaiY;",
            ">;",
            "LaiU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LaiZ;->a:Lbxw;

    .line 28
    iput-object p2, p0, LaiZ;->a:LaiU;

    .line 29
    return-void
.end method

.method static a(Landroid/content/Context;Lbuu;)LaiY;
    .locals 1

    .prologue
    .line 66
    const-class v0, LaiZ;

    invoke-interface {p1, v0}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiZ;

    .line 67
    invoke-virtual {v0, p0}, LaiZ;->a(Landroid/content/Context;)LaiY;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)LaiY;
    .locals 1

    .prologue
    .line 51
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 52
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    .line 62
    :goto_0
    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    .line 61
    invoke-static {p0, v0}, LaiZ;->a(Landroid/content/Context;Lbuu;)LaiY;

    move-result-object v0

    return-object v0

    .line 53
    :cond_0
    instance-of v0, p0, Landroid/app/Service;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 54
    check-cast v0, Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->getApplication()Landroid/app/Application;

    move-result-object v0

    goto :goto_0

    .line 55
    :cond_1
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 56
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/content/Context;)LaiY;
    .locals 4

    .prologue
    .line 32
    new-instance v2, LaiW;

    invoke-direct {v2, p1}, LaiW;-><init>(Landroid/content/Context;)V

    .line 33
    iget-object v0, p0, LaiZ;->a:LaiU;

    invoke-interface {v0}, LaiU;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiW;

    .line 34
    iget-object v1, p0, LaiZ;->a:LaiU;

    invoke-interface {v1, v2}, LaiU;->a(Ljava/lang/Object;)V

    .line 36
    :try_start_0
    iget-object v1, p0, LaiZ;->a:Lbxw;

    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    iget-object v3, p0, LaiZ;->a:LaiU;

    invoke-interface {v3, v2}, LaiU;->b(Ljava/lang/Object;)V

    .line 39
    if-eqz v0, :cond_0

    .line 40
    iget-object v2, p0, LaiZ;->a:LaiU;

    invoke-interface {v2, v0}, LaiU;->a(Ljava/lang/Object;)V

    :cond_0
    return-object v1

    .line 38
    :catchall_0
    move-exception v1

    iget-object v3, p0, LaiZ;->a:LaiU;

    invoke-interface {v3, v2}, LaiU;->b(Ljava/lang/Object;)V

    .line 39
    if-eqz v0, :cond_1

    .line 40
    iget-object v2, p0, LaiZ;->a:LaiU;

    invoke-interface {v2, v0}, LaiU;->a(Ljava/lang/Object;)V

    :cond_1
    throw v1
.end method
