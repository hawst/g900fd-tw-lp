.class public LahG;
.super LafH;
.source "ScheduledSyncTaskImpl.java"

# interfaces
.implements LahF;


# static fields
.field static final a:J


# instance fields
.field private final a:LaGg;

.field private final a:LaHd;

.field private final a:LaKM;

.field private final a:LaKR;

.field private final a:Lagd;

.field private final a:LahD;

.field private final a:LahL;

.field private final a:LahO;

.field private final a:LahP;

.field private a:LahR;

.field private a:Lahm;

.field private final a:Lahn;

.field private final a:Lamn;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:Ljava/lang/Thread;

.field private final a:LqK;

.field private final a:LtK;

.field private a:Z

.field private b:J

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 52
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 53
    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sput-wide v0, LahG;->a:J

    .line 52
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHd;Lagd;LaKM;LaGg;LahD;LahO;LahP;LahL;Lahn;LaKR;Lamn;LtK;LqK;)V
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, LafH;-><init>()V

    .line 137
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LahG;->b:J

    .line 158
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 159
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHd;

    iput-object v0, p0, LahG;->a:LaHd;

    .line 160
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagd;

    iput-object v0, p0, LahG;->a:Lagd;

    .line 161
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKM;

    iput-object v0, p0, LahG;->a:LaKM;

    .line 162
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahO;

    iput-object v0, p0, LahG;->a:LahO;

    .line 163
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p0, LahG;->a:LaGg;

    .line 164
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahD;

    iput-object v0, p0, LahG;->a:LahD;

    .line 165
    invoke-static {p8}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahP;

    iput-object v0, p0, LahG;->a:LahP;

    .line 166
    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahL;

    iput-object v0, p0, LahG;->a:LahL;

    .line 167
    invoke-static {p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahn;

    iput-object v0, p0, LahG;->a:Lahn;

    .line 168
    invoke-static {p11}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p0, LahG;->a:LaKR;

    .line 169
    invoke-static {p12}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamn;

    iput-object v0, p0, LahG;->a:Lamn;

    .line 170
    invoke-static/range {p14 .. p14}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p0, LahG;->a:LqK;

    .line 171
    iput-object p13, p0, LahG;->a:LtK;

    .line 172
    invoke-virtual {p0}, LahG;->a()LahS;

    move-result-object v0

    invoke-static {v0}, LahR;->a(LahS;)LahR;

    move-result-object v0

    iput-object v0, p0, LahG;->a:LahR;

    .line 173
    iget-object v0, p0, LahG;->a:LahR;

    invoke-virtual {p2}, LaHd;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LahR;->a(Z)V

    .line 174
    return-void
.end method

.method private a()Lahm;
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 382
    iget-object v0, p0, LahG;->a:LahL;

    invoke-virtual {v0}, LahL;->c()V

    .line 383
    monitor-enter p0

    .line 384
    :try_start_0
    invoke-direct {p0}, LahG;->s()V

    .line 386
    const/4 v0, 0x0

    .line 388
    iget-object v1, p0, LahG;->a:LaGg;

    iget-object v2, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v2}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v1

    .line 389
    if-eqz v1, :cond_0

    .line 390
    iget-object v0, p0, LahG;->a:Lahn;

    invoke-interface {v0, p0, p0}, Lahn;->a(LahJ;LagH;)Lahm;

    move-result-object v0

    .line 392
    :cond_0
    monitor-exit p0

    return-object v0

    .line 393
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 257
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 259
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0, p1, p2}, LaHd;->d(J)V

    .line 260
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    monitor-exit p0

    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ZZLaGp;)V
    .locals 4

    .prologue
    .line 503
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 505
    :try_start_1
    invoke-direct {p0, p1}, LahG;->c(Z)V

    .line 507
    invoke-direct {p0}, LahG;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 508
    if-eqz p2, :cond_1

    .line 509
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->g()V

    .line 513
    :goto_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {p3}, LaGp;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaHd;->a(J)V

    .line 516
    :cond_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->b()V

    .line 517
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V

    .line 518
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 520
    :try_start_2
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 523
    iget-object v0, p0, LahG;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 524
    monitor-exit p0

    return-void

    .line 511
    :cond_1
    :try_start_3
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->h()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 520
    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 503
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()J
    .locals 2

    .prologue
    .line 248
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LahG;->c(Z)V

    .line 326
    invoke-direct {p0}, LahG;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->g()V

    .line 329
    :cond_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V

    .line 330
    iget-object v0, p0, LahG;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    monitor-exit p0

    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Z)V
    .locals 4

    .prologue
    .line 527
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaHd;

    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, LahG;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, LaHd;->a(Ljava/util/Date;)V

    .line 529
    if-eqz p1, :cond_0

    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530
    :cond_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0, p1}, LaHd;->c(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    :cond_1
    monitor-exit p0

    return-void

    .line 527
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()Z
    .locals 1

    .prologue
    .line 374
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    invoke-virtual {p0}, LahG;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private k()Z
    .locals 1

    .prologue
    .line 488
    iget-boolean v0, p0, LahG;->b:Z

    return v0
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 587
    invoke-direct {p0}, LahG;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LahG;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized m()Z
    .locals 1

    .prologue
    .line 636
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 638
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 636
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized r()V
    .locals 3

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->a()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 211
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 213
    :try_start_1
    iget-object v0, p0, LahG;->a:LaGg;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_1

    .line 215
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(LaGo;)LaGp;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    .line 217
    iget-object v1, p0, LahG;->a:LaHd;

    invoke-virtual {v1}, LaHd;->g()V

    .line 218
    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    .line 219
    iget-object v2, p0, LahG;->a:LaHd;

    invoke-virtual {v2, v0, v1}, LaHd;->a(J)V

    .line 220
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->b()V

    .line 222
    :cond_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V

    .line 224
    :cond_1
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    :try_start_2
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 228
    monitor-exit p0

    return-void

    .line 226
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 210
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized s()V
    .locals 4

    .prologue
    .line 433
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 434
    if-nez v0, :cond_1

    .line 457
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 438
    :cond_1
    :try_start_1
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(LaGo;)LaGp;

    move-result-object v0

    .line 440
    if-eqz v0, :cond_0

    .line 444
    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    .line 446
    iget-object v2, p0, LahG;->a:LaHd;

    invoke-virtual {v2}, LaHd;->b()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 452
    iget-object v2, p0, LahG;->a:LaHd;

    invoke-virtual {v2}, LaHd;->h()V

    .line 453
    iget-object v2, p0, LahG;->a:LaHd;

    invoke-virtual {v2, v0, v1}, LaHd;->a(J)V

    .line 454
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->b()V

    .line 455
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()J
    .locals 2

    .prologue
    .line 630
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 632
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->d()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 630
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()LMZ;
    .locals 3

    .prologue
    .line 368
    iget-object v0, p0, LahG;->a:LahL;

    invoke-virtual {v0}, LahL;->c()V

    .line 369
    iget-object v0, p0, LahG;->a:LaGg;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 370
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LahG;->a:LahP;

    iget-object v2, p0, LahG;->a:LaHd;

    invoke-virtual {v1, v0, v2}, LahP;->a(LaGo;LaHd;)LMZ;

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized a()LahR;
    .locals 1

    .prologue
    .line 706
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LahR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()LahS;
    .locals 4

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 272
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    sget-object v0, LahS;->a:LahS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, LahS;->b:LahS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 721
    invoke-static {}, LahR;->a()LahR;

    move-result-object v0

    invoke-virtual {p0, v0}, LahG;->a(LahR;)V

    .line 722
    return-void
.end method

.method public a(JJ)V
    .locals 7

    .prologue
    .line 726
    iget-object v0, p0, LahG;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v2

    .line 728
    cmp-long v0, p1, p3

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 729
    :goto_0
    if-nez v0, :cond_0

    iget-wide v0, p0, LahG;->b:J

    sub-long v0, v2, v0

    sget-wide v4, LahG;->a:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 730
    :cond_0
    iput-wide v2, p0, LahG;->b:J

    .line 731
    invoke-direct {p0, p1, p2}, LahG;->a(J)V

    .line 732
    invoke-static {p1, p2, p3, p4}, LahR;->a(JJ)LahR;

    move-result-object v0

    invoke-virtual {p0, v0}, LahG;->a(LahR;)V

    .line 735
    :cond_1
    invoke-virtual {p0}, LahG;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 736
    invoke-virtual {p0}, LahG;->p()V

    .line 739
    :cond_2
    return-void

    .line 728
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaHe;)V
    .locals 4

    .prologue
    .line 643
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 645
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 647
    :try_start_0
    iget-object v0, p0, LahG;->a:LahO;

    invoke-interface {v0, p1}, LahO;->a(LaHe;)LaGb;

    move-result-object v0

    .line 648
    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {p1}, LaHe;->a()LaHf;

    move-result-object v1

    .line 650
    sget-object v2, LahH;->a:[I

    invoke-virtual {v1}, LaHf;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 667
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected sync direction:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 673
    :catchall_0
    move-exception v0

    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    .line 652
    :pswitch_0
    :try_start_1
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(LaGo;)LaGp;

    move-result-object v0

    .line 653
    if-eqz v0, :cond_0

    .line 658
    invoke-virtual {p1}, LaHe;->a()Z

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, LahG;->a(ZZLaGp;)V

    .line 670
    :cond_0
    :goto_0
    invoke-virtual {p1}, LaHe;->f()V

    .line 671
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 673
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 675
    return-void

    .line 662
    :pswitch_1
    :try_start_2
    invoke-interface {v0}, LaGo;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    invoke-virtual {p1}, LaHe;->a()Z

    move-result v0

    invoke-direct {p0, v0}, LahG;->b(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 650
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(LafT;)V
    .locals 1

    .prologue
    .line 768
    invoke-static {p1}, LahR;->b(LafT;)LahR;

    move-result-object v0

    invoke-virtual {p0, v0}, LahG;->a(LahR;)V

    .line 769
    return-void
.end method

.method protected declared-synchronized a(LahR;)V
    .locals 3

    .prologue
    .line 710
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LahR;

    invoke-static {v0, p1}, LahR;->a(LahR;LahR;)LahR;

    move-result-object v0

    iput-object v0, p0, LahG;->a:LahR;

    .line 711
    iget-object v0, p0, LahG;->a:LahL;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v2, p0, LahG;->a:LahR;

    invoke-virtual {v0, v1, v2}, LahL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LahR;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 712
    monitor-exit p0

    return-void

    .line 710
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 6

    .prologue
    .line 749
    const-string v0, "SyncTask"

    const-string v1, "Error syncing: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 751
    sget-object v0, LafT;->o:LafT;

    .line 752
    instance-of v1, p1, Ljava/io/IOException;

    if-eqz v1, :cond_0

    .line 753
    sget-object v0, LafT;->i:LafT;

    .line 756
    :cond_0
    invoke-static {v0}, LahR;->b(LafT;)LahR;

    move-result-object v0

    invoke-virtual {p0, v0}, LahG;->a(LahR;)V

    .line 758
    invoke-virtual {p0}, LahG;->a()LahR;

    move-result-object v0

    .line 759
    invoke-virtual {v0}, LahR;->a()J

    move-result-wide v0

    .line 761
    iget-object v2, p0, LahG;->a:LqK;

    const-string v3, "pinning"

    const-string v4, "content_sync_error"

    const/4 v5, 0x0

    .line 763
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 761
    invoke-virtual {v2, v3, v4, v5, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 764
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 565
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 567
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0, p1}, LaHd;->a(Ljava/lang/String;)V

    .line 568
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 569
    monitor-exit p0

    return-void

    .line 565
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 466
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:Ljava/lang/Thread;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 468
    invoke-virtual {p0}, LahG;->a()LahS;

    move-result-object v0

    invoke-static {v0}, LahR;->a(LahS;)LahR;

    move-result-object v0

    iput-object v0, p0, LahG;->a:LahR;

    .line 469
    iget-object v0, p0, LahG;->a:LahR;

    invoke-virtual {p0}, LahG;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, LahR;->a(Z)V

    .line 471
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    iput-object v0, p0, LahG;->a:Ljava/lang/Thread;

    .line 472
    invoke-direct {p0}, LahG;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LahG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 473
    :cond_0
    invoke-virtual {p0}, LahG;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    :cond_1
    monitor-exit p0

    return-void

    .line 466
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 573
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 575
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0, p1}, LaHd;->c(Z)V

    .line 576
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 577
    monitor-exit p0

    return-void

    .line 573
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Z
    .locals 1

    .prologue
    .line 552
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LahG;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 9

    .prologue
    .line 773
    invoke-virtual {p0}, LahG;->n()V

    .line 774
    invoke-static {}, LahR;->b()LahR;

    move-result-object v0

    invoke-virtual {p0, v0}, LahG;->a(LahR;)V

    .line 776
    invoke-virtual {p0}, LahG;->a()LahR;

    move-result-object v0

    .line 777
    invoke-virtual {v0}, LahR;->b()J

    move-result-wide v2

    .line 778
    invoke-virtual {v0}, LahR;->a()J

    move-result-wide v0

    .line 779
    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 780
    const-string v4, "SyncTask"

    const-string v5, "onSyncComplete: loadedSize(%d) != expectedSize(%d)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 781
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v7

    .line 780
    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 784
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 785
    iget-object v2, p0, LahG;->a:LqK;

    const-string v3, "pinning"

    const-string v4, "content_sync_succeed"

    const/4 v5, 0x0

    .line 786
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 785
    invoke-virtual {v2, v3, v4, v5, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 788
    :cond_1
    return-void
.end method

.method public b()Z
    .locals 4

    .prologue
    .line 253
    invoke-direct {p0}, LahG;->b()J

    move-result-wide v0

    iget-object v2, p0, LahG;->a:LahL;

    invoke-virtual {v2}, LahL;->a()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 792
    invoke-static {}, LahR;->c()LahR;

    move-result-object v0

    invoke-virtual {p0, v0}, LahG;->a(LahR;)V

    .line 793
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, LahG;->a:LahL;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, LahL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized d()V
    .locals 1

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 180
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->d()V

    .line 181
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    monitor-exit p0

    return-void

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Z
    .locals 1

    .prologue
    .line 581
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 583
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 2

    .prologue
    .line 278
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 280
    iget-object v0, p0, LahG;->a:LaHd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaHd;->b(Z)V

    .line 281
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V

    .line 283
    invoke-virtual {p0}, LahG;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    monitor-exit p0

    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Z
    .locals 2

    .prologue
    .line 592
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 594
    iget-object v0, p0, LahG;->a:LtK;

    sget-object v1, Lry;->X:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    .line 597
    invoke-direct {p0}, LahG;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LahG;->a:LaHd;

    invoke-virtual {v1}, LaHd;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LahG;->a:LaHd;

    .line 598
    invoke-virtual {v1}, LaHd;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, LahG;->a:Lahm;

    if-eqz v0, :cond_1

    iget-object v0, p0, LahG;->a:Lahm;

    .line 599
    invoke-interface {v0}, Lahm;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 592
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 2

    .prologue
    .line 294
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 296
    iget-object v0, p0, LahG;->a:LaHd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaHd;->b(Z)V

    .line 298
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->g()V

    .line 303
    :goto_0
    iget-object v0, p0, LahG;->a:LaHd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaHd;->c(Z)V

    .line 304
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V

    .line 306
    iget-object v0, p0, LahG;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    monitor-exit p0

    return-void

    .line 301
    :cond_0
    :try_start_1
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 604
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1}, LaGg;->d()V

    .line 606
    iget-object v1, p0, LahG;->a:LtK;

    sget-object v2, Lry;->Y:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 612
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 611
    :cond_1
    :try_start_1
    invoke-direct {p0}, LahG;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LahG;->a:LaHd;

    invoke-virtual {v1}, LaHd;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LahG;->a:Lahm;

    if-eqz v1, :cond_2

    iget-object v1, p0, LahG;->a:Lahm;

    .line 612
    invoke-interface {v1}, Lahm;->b()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 313
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LahG;->b(Z)V

    .line 314
    return-void
.end method

.method public declared-synchronized g()Z
    .locals 1

    .prologue
    .line 617
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 618
    iget-object v0, p0, LahG;->a:Lahn;

    invoke-interface {v0, p0}, Lahn;->a(LahJ;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 617
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()V
    .locals 2

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 337
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 338
    sget-object v0, LahS;->a:LahS;

    invoke-virtual {p0}, LahG;->a()LahS;

    move-result-object v1

    invoke-virtual {v0, v1}, LahS;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    :try_start_1
    invoke-virtual {p0}, LahG;->n()V

    .line 341
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 342
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    :cond_0
    :try_start_2
    invoke-virtual {p0}, LahG;->p()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 349
    :cond_1
    monitor-exit p0

    return-void

    .line 345
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {p0}, LahG;->p()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 335
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()Z
    .locals 1

    .prologue
    .line 623
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 625
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 623
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 398
    iget-object v0, p0, LahG;->a:LahD;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0}, LahG;->a()LahS;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LahD;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LahS;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403
    iget-object v0, p0, LahG;->a:LahD;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, LahD;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 407
    iget-object v0, p0, LahG;->a:LahD;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0}, LahG;->a()LahS;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LahD;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;LahS;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 408
    iput-boolean v3, p0, LahG;->b:Z

    .line 409
    invoke-virtual {p0}, LahG;->b()V

    .line 430
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    invoke-direct {p0}, LahG;->a()Lahm;

    move-result-object v0

    iput-object v0, p0, LahG;->a:Lahm;

    .line 415
    iget-object v0, p0, LahG;->a:Lahm;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, LahG;->a:Lahm;

    invoke-interface {v0}, Lahm;->c()Z

    move-result v0

    .line 421
    if-eqz v0, :cond_2

    .line 422
    iget-object v0, p0, LahG;->a:LahL;

    invoke-virtual {v0, p0}, LahL;->b(LahG;)V

    .line 425
    const/4 v0, 0x1

    iput-boolean v0, p0, LahG;->b:Z

    goto :goto_0

    .line 428
    :cond_2
    iput-boolean v3, p0, LahG;->b:Z

    goto :goto_0
.end method

.method public i()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    iget-object v0, p0, LahG;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()LaKS;

    move-result-object v3

    .line 233
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->d()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 235
    :goto_0
    invoke-virtual {v3}, LaKS;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v0, :cond_1

    iget-object v0, p0, LahG;->a:Lamn;

    .line 236
    invoke-interface {v0, v3}, Lamn;->a(LaKS;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    :cond_0
    invoke-virtual {p0}, LahG;->o()V

    .line 238
    invoke-virtual {v3}, LaKS;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LafT;->k:LafT;

    .line 240
    :goto_1
    invoke-static {v0}, LahR;->a(LafT;)LahR;

    move-result-object v0

    invoke-virtual {p0, v0}, LahG;->a(LahR;)V

    move v1, v2

    .line 244
    :cond_1
    return v1

    :cond_2
    move v0, v2

    .line 233
    goto :goto_0

    .line 238
    :cond_3
    sget-object v0, LafT;->j:LafT;

    goto :goto_1
.end method

.method public j()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 493
    monitor-enter p0

    .line 494
    :try_start_0
    iget-object v1, p0, LahG;->a:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 495
    const/4 v0, 0x0

    iput-object v0, p0, LahG;->a:Ljava/lang/Thread;

    .line 496
    const/4 v0, 0x0

    iput-boolean v0, p0, LahG;->a:Z

    .line 497
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    iget-object v0, p0, LahG;->a:LahL;

    invoke-virtual {v0, p0}, LahL;->a(LahF;)V

    .line 499
    return-void

    .line 497
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized k()V
    .locals 3

    .prologue
    .line 679
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 681
    invoke-virtual {p0}, LahG;->p()V

    .line 682
    iget-object v0, p0, LahG;->a:LaGg;

    iget-object v1, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 683
    if-eqz v0, :cond_0

    .line 684
    invoke-virtual {v0}, LaGb;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 685
    invoke-virtual {v0}, LaGb;->a()LaGc;

    move-result-object v0

    invoke-virtual {v0}, LaGc;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 702
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 687
    :cond_1
    :try_start_1
    sget-object v1, LahS;->b:LahS;

    invoke-virtual {p0}, LahG;->a()LahS;

    move-result-object v2

    invoke-virtual {v1, v2}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 688
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1}, LaGg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 690
    :try_start_2
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(LaGo;)LaGp;

    move-result-object v0

    .line 691
    if-eqz v0, :cond_2

    .line 692
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(LaGp;)V

    .line 694
    :cond_2
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->f()V

    .line 695
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 697
    :try_start_3
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 679
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 697
    :catchall_1
    move-exception v0

    :try_start_4
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public declared-synchronized l()V
    .locals 1

    .prologue
    .line 536
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->c()V

    .line 537
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    monitor-exit p0

    return-void

    .line 536
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized m()V
    .locals 1

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->a()V

    .line 289
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    monitor-exit p0

    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized n()V
    .locals 4

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 187
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 189
    :try_start_1
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->b()J

    move-result-wide v0

    .line 190
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 191
    iget-object v2, p0, LahG;->a:LaGg;

    .line 192
    invoke-interface {v2, v0, v1}, LaGg;->a(J)LaGp;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EntrySpec: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LahG;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSyncRequest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LahG;->a:LaHd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 191
    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGp;

    .line 194
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaGp;->a(Z)V

    .line 195
    invoke-virtual {v0}, LaGp;->e()V

    .line 196
    iget-object v0, p0, LahG;->a:LaHd;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, LaHd;->a(J)V

    .line 198
    :cond_0
    iget-object v0, p0, LahG;->a:LaHd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaHd;->a(Z)V

    .line 199
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->c()V

    .line 200
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V

    .line 202
    invoke-direct {p0}, LahG;->r()V

    .line 203
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :try_start_2
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207
    monitor-exit p0

    return-void

    .line 205
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, LahG;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 185
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, LahG;->a:LahL;

    invoke-virtual {v0, p0}, LahL;->a(LahG;)V

    .line 480
    return-void
.end method

.method public declared-synchronized p()V
    .locals 1

    .prologue
    .line 542
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 544
    iget-object v0, p0, LahG;->a:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 545
    const/4 v0, 0x1

    iput-boolean v0, p0, LahG;->a:Z

    .line 546
    iget-object v0, p0, LahG;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :cond_0
    monitor-exit p0

    return-void

    .line 542
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized q()V
    .locals 2

    .prologue
    .line 557
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LahG;->a:LaGg;

    invoke-interface {v0}, LaGg;->d()V

    .line 559
    iget-object v0, p0, LahG;->a:LaHd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaHd;->a(Ljava/lang/String;)V

    .line 560
    iget-object v0, p0, LahG;->a:LaHd;

    invoke-virtual {v0}, LaHd;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 561
    monitor-exit p0

    return-void

    .line 557
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
