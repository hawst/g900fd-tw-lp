.class abstract enum LbjY;
.super Ljava/lang/Enum;
.source "LocalCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbjY;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbjY;

.field static final a:[LbjY;

.field public static final enum b:LbjY;

.field private static final synthetic b:[LbjY;

.field public static final enum c:LbjY;

.field public static final enum d:LbjY;

.field public static final enum e:LbjY;

.field public static final enum f:LbjY;

.field public static final enum g:LbjY;

.field public static final enum h:LbjY;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 441
    new-instance v0, LbjZ;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, LbjZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->a:LbjY;

    .line 449
    new-instance v0, Lbka;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1, v4}, Lbka;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->b:LbjY;

    .line 465
    new-instance v0, Lbkb;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1, v5}, Lbkb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->c:LbjY;

    .line 481
    new-instance v0, Lbkc;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1, v6}, Lbkc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->d:LbjY;

    .line 499
    new-instance v0, Lbkd;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, Lbkd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->e:LbjY;

    .line 507
    new-instance v0, Lbke;

    const-string v1, "WEAK_ACCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbke;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->f:LbjY;

    .line 523
    new-instance v0, Lbkf;

    const-string v1, "WEAK_WRITE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbkf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->g:LbjY;

    .line 539
    new-instance v0, Lbkg;

    const-string v1, "WEAK_ACCESS_WRITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbkg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbjY;->h:LbjY;

    .line 440
    const/16 v0, 0x8

    new-array v0, v0, [LbjY;

    sget-object v1, LbjY;->a:LbjY;

    aput-object v1, v0, v3

    sget-object v1, LbjY;->b:LbjY;

    aput-object v1, v0, v4

    sget-object v1, LbjY;->c:LbjY;

    aput-object v1, v0, v5

    sget-object v1, LbjY;->d:LbjY;

    aput-object v1, v0, v6

    sget-object v1, LbjY;->e:LbjY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LbjY;->f:LbjY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LbjY;->g:LbjY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LbjY;->h:LbjY;

    aput-object v2, v0, v1

    sput-object v0, LbjY;->b:[LbjY;

    .line 567
    const/16 v0, 0x8

    new-array v0, v0, [LbjY;

    sget-object v1, LbjY;->a:LbjY;

    aput-object v1, v0, v3

    sget-object v1, LbjY;->b:LbjY;

    aput-object v1, v0, v4

    sget-object v1, LbjY;->c:LbjY;

    aput-object v1, v0, v5

    sget-object v1, LbjY;->d:LbjY;

    aput-object v1, v0, v6

    sget-object v1, LbjY;->e:LbjY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LbjY;->f:LbjY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LbjY;->g:LbjY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LbjY;->h:LbjY;

    aput-object v2, v0, v1

    sput-object v0, LbjY;->a:[LbjY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbjR;)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, LbjY;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lbkv;ZZ)LbjY;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 572
    sget-object v1, Lbkv;->c:Lbkv;

    if-ne p0, v1, :cond_1

    const/4 v1, 0x4

    move v2, v1

    :goto_0
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_1
    or-int/2addr v1, v2

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 574
    sget-object v1, LbjY;->a:[LbjY;

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v2, v0

    .line 572
    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LbjY;
    .locals 1

    .prologue
    .line 440
    const-class v0, LbjY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbjY;

    return-object v0
.end method

.method public static values()[LbjY;
    .locals 1

    .prologue
    .line 440
    sget-object v0, LbjY;->b:[LbjY;

    invoke-virtual {v0}, [LbjY;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbjY;

    return-object v0
.end method


# virtual methods
.method a(Lbks;Lbkr;Lbkr;)Lbkr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbks",
            "<TK;TV;>;",
            "Lbkr",
            "<TK;TV;>;",
            "Lbkr",
            "<TK;TV;>;)",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 597
    invoke-interface {p2}, Lbkr;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lbkr;->a()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, LbjY;->a(Lbks;Ljava/lang/Object;ILbkr;)Lbkr;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lbks;Ljava/lang/Object;ILbkr;)Lbkr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbks",
            "<TK;TV;>;TK;I",
            "Lbkr",
            "<TK;TV;>;)",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method a(Lbkr;Lbkr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbkr",
            "<TK;TV;>;",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 604
    invoke-interface {p1}, Lbkr;->a()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lbkr;->a(J)V

    .line 606
    invoke-interface {p1}, Lbkr;->c()Lbkr;

    move-result-object v0

    invoke-static {v0, p2}, LbjQ;->a(Lbkr;Lbkr;)V

    .line 607
    invoke-interface {p1}, Lbkr;->b()Lbkr;

    move-result-object v0

    invoke-static {p2, v0}, LbjQ;->a(Lbkr;Lbkr;)V

    .line 609
    invoke-static {p1}, LbjQ;->b(Lbkr;)V

    .line 610
    return-void
.end method

.method b(Lbkr;Lbkr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbkr",
            "<TK;TV;>;",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 616
    invoke-interface {p1}, Lbkr;->b()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lbkr;->b(J)V

    .line 618
    invoke-interface {p1}, Lbkr;->e()Lbkr;

    move-result-object v0

    invoke-static {v0, p2}, LbjQ;->b(Lbkr;Lbkr;)V

    .line 619
    invoke-interface {p1}, Lbkr;->d()Lbkr;

    move-result-object v0

    invoke-static {p2, v0}, LbjQ;->b(Lbkr;Lbkr;)V

    .line 621
    invoke-static {p1}, LbjQ;->c(Lbkr;)V

    .line 622
    return-void
.end method
