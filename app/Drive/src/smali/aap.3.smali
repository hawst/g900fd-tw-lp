.class public Laap;
.super Ljava/lang/Object;
.source "OnlineSearch.java"

# interfaces
.implements Laau;


# instance fields
.field protected final a:LagS;


# direct methods
.method public constructor <init>(LagS;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Laap;->a:LagS;

    .line 47
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 51
    const-string v0, "null query"

    invoke-static {p0, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {}, Laal;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 53
    const-string v1, "q"

    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 54
    const-string v1, "showdeleted"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 55
    invoke-static {v0}, Laal;->a(Landroid/net/Uri$Builder;)V

    .line 56
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(LaFO;Ljava/lang/String;Laag;)Laaf;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1, p2, p3}, Laap;->a(LaFO;Ljava/lang/String;Laag;)Laal;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;Ljava/lang/String;Laag;)Laal;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Laap;->a:LagS;

    invoke-interface {v0}, LagS;->a()Lagx;

    move-result-object v0

    .line 62
    new-instance v1, Laal;

    invoke-static {p2}, Laap;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, p1, v2, p3, v0}, Laal;-><init>(LaFO;Landroid/net/Uri;Laag;Lagx;)V

    return-object v1
.end method
