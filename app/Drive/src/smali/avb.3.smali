.class public Lavb;
.super Ljava/lang/Object;
.source "DisplayData.java"


# instance fields
.field private final a:Lava;

.field private final a:Lavf;

.field private final a:Lavg;

.field private final a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;Lava;)V
    .locals 2

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p2, p0, Lavb;->a:Lava;

    .line 186
    const-class v0, LauW;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 187
    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iput-object v0, p0, Lavb;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    .line 188
    invoke-static {}, Lavf;->values()[Lavf;

    move-result-object v0

    const-string v1, "o"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lavb;->a:Lavf;

    .line 189
    invoke-static {p1}, Lavb;->a(Landroid/os/Bundle;)Lavg;

    move-result-object v0

    iput-object v0, p0, Lavb;->a:Lavg;

    .line 190
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lavg;
    .locals 1

    .prologue
    .line 193
    const-string v0, "t"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lavg;->valueOf(Ljava/lang/String;)Lavg;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()LauW;
    .locals 3

    .prologue
    .line 197
    sget-object v0, LauX;->a:[I

    iget-object v1, p0, Lavb;->a:Lavf;

    invoke-virtual {v1}, Lavf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No case for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lavb;->a:Lavf;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :pswitch_0
    iget-object v0, p0, Lavb;->a:Lavg;

    iget-object v1, p0, Lavb;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v2, p0, Lavb;->a:Lava;

    invoke-interface {v2}, Lava;->a()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0, v1, v2}, LauW;->a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Landroid/content/ContentResolver;)LauW;

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    .line 201
    :pswitch_1
    iget-object v0, p0, Lavb;->a:Lavg;

    iget-object v1, p0, Lavb;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-static {v0, v1}, LauW;->a(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;

    move-result-object v0

    goto :goto_0

    .line 203
    :pswitch_2
    iget-object v0, p0, Lavb;->a:Lava;

    iget-object v1, p0, Lavb;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-interface {v0, v1}, Lava;->a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)V

    .line 204
    const/4 v0, 0x0

    goto :goto_0

    .line 206
    :pswitch_3
    iget-object v0, p0, Lavb;->a:Lavg;

    iget-object v1, p0, Lavb;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-static {v0, v1}, LauW;->b(Lavg;Lcom/google/android/apps/viewer/client/AuthenticatedUri;)LauW;

    move-result-object v0

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
