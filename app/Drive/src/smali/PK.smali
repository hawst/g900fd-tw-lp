.class public abstract LPK;
.super Ljava/lang/Object;
.source "EventBinder.java"


# instance fields
.field protected final a:LPD;

.field protected final a:LaGu;


# direct methods
.method public constructor <init>(LPD;LaGu;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, LPK;->a:LPD;

    .line 23
    iput-object p2, p0, LPK;->a:LaGu;

    .line 24
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)Ljava/lang/Object;
.end method

.method public abstract a(Landroid/content/res/Resources;)Ljava/lang/String;
.end method

.method protected a(Landroid/content/res/Resources;IIII)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 54
    iget-object v0, p0, LPK;->a:LPD;

    invoke-virtual {v0}, LPD;->b()I

    move-result v0

    .line 55
    iget-object v1, p0, LPK;->a:LPD;

    invoke-virtual {v1}, LPD;->a()I

    move-result v1

    .line 57
    invoke-virtual {p0}, LPK;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    iget-object v1, p0, LPK;->a:LaGu;

    invoke-interface {v1}, LaGu;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {p1, p5, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 61
    :cond_0
    invoke-virtual {p1, p4, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 64
    :cond_1
    if-ne v0, v3, :cond_2

    .line 65
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-virtual {p1, p2, v1, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 67
    :cond_2
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-virtual {p1, p3, v1, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 37
    invoke-static {p1}, LPW;->a(Landroid/view/View;)LPW;

    move-result-object v1

    .line 39
    iget-object v0, v1, LPW;->a:Landroid/widget/TextView;

    iget-object v2, v1, LPW;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    and-int/lit8 v2, v2, -0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 40
    invoke-virtual {p0}, LPK;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    .line 41
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, LPK;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    invoke-virtual {v1}, LPW;->a()V

    .line 46
    :goto_1
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {v1}, LPW;->b()V

    goto :goto_1
.end method

.method protected a()Z
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, LPK;->a:LPD;

    invoke-virtual {v0}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getTarget()Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Target;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LPK;->a:LaGu;

    invoke-interface {v1}, LaGu;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()Z
.end method
