.class abstract Lbpn;
.super Lbqa;
.source "Multisets.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbqa",
        "<TE;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 915
    invoke-direct {p0}, Lbqa;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lbpi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbpi",
            "<TE;>;"
        }
    .end annotation
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 920
    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v0

    invoke-interface {v0}, Lbpi;->clear()V

    .line 921
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v0

    invoke-interface {v0, p1}, Lbpi;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 930
    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v0

    invoke-interface {v0, p1}, Lbpi;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 935
    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v0

    invoke-interface {v0}, Lbpi;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 940
    new-instance v0, Lbpo;

    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v1

    invoke-interface {v1}, Lbpi;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbpo;-><init>(Lbpn;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 951
    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v0

    invoke-interface {v0, p1}, Lbpi;->a(Ljava/lang/Object;)I

    move-result v0

    .line 952
    if-lez v0, :cond_0

    .line 953
    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lbpi;->b(Ljava/lang/Object;I)I

    .line 954
    const/4 v0, 0x1

    .line 956
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 961
    invoke-virtual {p0}, Lbpn;->a()Lbpi;

    move-result-object v0

    invoke-interface {v0}, Lbpi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
