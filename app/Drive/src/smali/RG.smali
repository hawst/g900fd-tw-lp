.class public LRG;
.super Ljava/lang/Object;
.source "NavigationFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:LaqX;

.field final synthetic a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

.field final synthetic a:[Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;[Landroid/accounts/Account;LaqX;)V
    .locals 0

    .prologue
    .line 588
    iput-object p1, p0, LRG;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iput-object p2, p0, LRG;->a:[Landroid/accounts/Account;

    iput-object p3, p0, LRG;->a:LaqX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 592
    iget-object v0, p0, LRG;->a:[Landroid/accounts/Account;

    aget-object v0, v0, p3

    .line 593
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 594
    const-string v1, "NavigationFragment"

    const-string v2, "account switch selected %s,  %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LRG;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v5, v5, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaFO;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 596
    iget-object v1, p0, LRG;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaFO;

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 597
    iget-object v1, p0, LRG;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v2, p0, LRG;->a:LaqX;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;LaFO;LaqX;)V

    .line 599
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 602
    return-void
.end method
