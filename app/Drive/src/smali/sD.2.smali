.class public LsD;
.super Ljava/lang/Object;
.source "DocListAppConfigurationProvider.java"

# interfaces
.implements Lbxw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbxw",
        "<",
        "LsC;",
        ">;"
    }
.end annotation


# instance fields
.field a:LQr;

.field a:Lamn;

.field a:Lamt;

.field a:Landroid/content/Context;
    .annotation runtime Lajg;
    .end annotation
.end field

.field a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "Laja",
            "<",
            "LAn;",
            ">;>;"
        }
    .end annotation
.end field

.field a:Lrx;

.field a:LtK;

.field a:LzQ;

.field b:LbiP;
    .annotation runtime LQP;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LsC;",
            ">;"
        }
    .end annotation
.end field

.field c:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LSv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, LsD;->c:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LsD;->a:LtK;

    sget-object v1, Lry;->H:Lry;

    .line 243
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LsD;->a()LsC;

    move-result-object v0

    return-object v0
.end method

.method public a()LsC;
    .locals 9

    .prologue
    .line 248
    iget-object v0, p0, LsD;->a:LQr;

    const-string v1, "helpFallbackUrlDrive"

    const-string v2, "https://support.google.com/drive/topic/4525997"

    .line 249
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 250
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 251
    iget-object v0, p0, LsD;->a:Landroid/content/Context;

    invoke-static {v0}, LUs;->b(Landroid/content/Context;)Z

    move-result v8

    .line 253
    iget-object v0, p0, LsD;->b:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LsD;->b:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsC;

    .line 256
    :goto_0
    return-object v0

    .line 253
    :cond_0
    new-instance v0, LsE;

    iget-object v1, p0, LsD;->a:Lrx;

    iget-object v2, p0, LsD;->a:LtK;

    .line 254
    invoke-direct {p0}, LsD;->a()Z

    move-result v3

    iget-object v4, p0, LsD;->a:LbiP;

    .line 255
    invoke-virtual {v4}, LbiP;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, LsD;->a:LbiP;

    invoke-virtual {v4}, LbiP;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laja;

    invoke-virtual {v4}, Laja;->a()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    iget-object v6, p0, LsD;->a:Lamt;

    .line 256
    invoke-virtual {v6}, Lamt;->a()Z

    move-result v6

    iget-object v7, p0, LsD;->a:LzQ;

    invoke-direct/range {v0 .. v8}, LsE;-><init>(Lrx;LtK;ZZLandroid/net/Uri;ZLzQ;Z)V

    goto :goto_0

    .line 255
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method
