.class public LBS;
.super LzK;
.source "DocListViewListArrangementModeDelegate.java"


# instance fields
.field private a:LAD;

.field private final a:LBU;

.field private final a:LBa;

.field private final a:LII;

.field private final a:Landroid/support/v4/app/Fragment;

.field private final a:LzZ;

.field private final b:LBU;

.field private c:Z


# direct methods
.method private constructor <init>(Landroid/support/v4/app/Fragment;LAH;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;LarF;LzZ;LII;LBU;LBa;)V
    .locals 9

    .prologue
    .line 202
    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, LzK;-><init>(LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;LarF;)V

    .line 179
    const/4 v1, 0x0

    iput-boolean v1, p0, LBS;->c:Z

    .line 209
    iput-object p1, p0, LBS;->a:Landroid/support/v4/app/Fragment;

    .line 210
    new-instance v1, LBW;

    move-object/from16 v0, p7

    invoke-direct {v1, p6, v0, p2}, LBW;-><init>(Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;LAH;)V

    iput-object v1, p0, LBS;->a:LBU;

    .line 211
    move-object/from16 v0, p10

    iput-object v0, p0, LBS;->a:LzZ;

    .line 212
    invoke-static/range {p11 .. p11}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LII;

    iput-object v1, p0, LBS;->a:LII;

    .line 213
    move-object/from16 v0, p12

    iput-object v0, p0, LBS;->b:LBU;

    .line 214
    move-object/from16 v0, p13

    iput-object v0, p0, LBS;->a:LBa;

    .line 215
    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/app/Fragment;LAH;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;LarF;LzZ;LII;LBU;LBa;LBT;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct/range {p0 .. p13}, LBS;-><init>(Landroid/support/v4/app/Fragment;LAH;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;LarF;LzZ;LII;LBU;LBa;)V

    return-void
.end method


# virtual methods
.method protected a()LzZ;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, LBS;->a:LzZ;

    return-object v0
.end method

.method public a(LQX;)V
    .locals 10

    .prologue
    .line 219
    invoke-super {p0, p1}, LzK;->a(LQX;)V

    .line 221
    invoke-virtual {p1}, LQX;->a()LCl;

    move-result-object v0

    invoke-interface {v0}, LCl;->a()LCn;

    move-result-object v0

    sget-object v1, LCn;->h:LCn;

    invoke-virtual {v0, v1}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LBS;->c:Z

    .line 223
    iget-object v0, p0, LBS;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 224
    sget v1, Lxa;->m_divider_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 225
    iget-object v1, p0, LBS;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 227
    const/4 v9, 0x0

    .line 228
    iget-object v0, p0, LBS;->a:LAD;

    if-nez v0, :cond_4

    .line 229
    new-instance v7, LzY;

    new-instance v0, LCG;

    iget-object v1, p0, LBS;->a:Landroid/widget/ListView;

    invoke-direct {v0, v1}, LCG;-><init>(Landroid/widget/ListView;)V

    iget-object v1, p0, LBS;->a:LzZ;

    invoke-direct {v7, v0, v1}, LzY;-><init>(LzN;LzZ;)V

    .line 231
    new-instance v6, LzX;

    new-instance v0, LCH;

    iget-object v1, p0, LBS;->a:Landroid/widget/ListView;

    invoke-direct {v0, v1}, LCH;-><init>(Landroid/widget/ListView;)V

    iget-object v1, p0, LBS;->a:LzZ;

    invoke-direct {v6, v0, v1}, LzX;-><init>(LHp;LzZ;)V

    .line 235
    invoke-virtual {p0, p1}, LBS;->a(LQX;)Z

    move-result v8

    .line 236
    new-instance v2, LBV;

    iget-object v0, p0, LBS;->a:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, LBS;->a:LBU;

    invoke-direct {v2, v0, p1, v1, v8}, LBV;-><init>(Landroid/support/v4/app/Fragment;LQX;LBU;Z)V

    .line 239
    iget-object v0, p0, LBS;->a:LtK;

    sget-object v1, Lry;->ax:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LBS;->b:LBU;

    if-eqz v0, :cond_0

    .line 241
    new-instance v0, LBV;

    iget-object v1, p0, LBS;->a:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, LBS;->b:LBU;

    invoke-direct {v0, v1, p1, v3, v8}, LBV;-><init>(Landroid/support/v4/app/Fragment;LQX;LBU;Z)V

    .line 245
    invoke-static {v2, v0}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    .line 246
    new-instance v2, LDJ;

    invoke-direct {v2, v0}, LDJ;-><init>(Ljava/util/List;)V

    .line 250
    :cond_0
    sget-object v0, LapG;->c:LapG;

    iget-object v1, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocListView;->a()LapG;

    move-result-object v1

    invoke-virtual {v0, v1}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 251
    sget v4, Lxe;->doc_entry_group_title_onecolumn:I

    .line 256
    :goto_0
    new-instance v0, LBj;

    iget-object v1, p0, LBS;->a:Landroid/widget/ListView;

    iget-object v3, p0, LBS;->a:LII;

    .line 260
    invoke-virtual {p1}, LQX;->a()LIK;

    move-result-object v5

    invoke-direct/range {v0 .. v8}, LBj;-><init>(Landroid/widget/ListView;LBo;LII;ILIK;LHp;LzN;Z)V

    iput-object v0, p0, LBS;->a:LAD;

    .line 264
    iget-object v0, p0, LBS;->a:LzZ;

    check-cast v0, LAa;

    iget-object v1, p0, LBS;->a:LAD;

    iget-object v2, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, v1, v2}, LAa;->a(LAD;LapF;)V

    move v0, v9

    .line 270
    :goto_1
    iget-object v1, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v2, p0, LBS;->a:LzZ;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 272
    iget-object v1, p0, LBS;->a:LBa;

    iget-object v2, p0, LBS;->a:LAD;

    invoke-virtual {v1, v2}, LBa;->a(LAD;)V

    .line 273
    iget-object v1, p0, LBS;->a:LzZ;

    invoke-interface {v1, p1}, LzZ;->a(LQX;)V

    .line 275
    iget-object v1, p0, LBS;->a:LzZ;

    iget-object v2, p0, LBS;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 276
    iget-object v1, p0, LBS;->a:Landroid/widget/ListView;

    iget-object v2, p0, LBS;->a:LzZ;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 279
    :cond_1
    iget-object v1, p0, LBS;->a:LzZ;

    invoke-virtual {p0, p1, v1}, LBS;->a(LQX;LCq;)V

    .line 281
    if-eqz v0, :cond_2

    .line 282
    iget-object v0, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->h()V

    .line 283
    iget-object v0, p0, LBS;->a:LzZ;

    invoke-virtual {p1}, LQX;->a()LaFX;

    move-result-object v1

    invoke-interface {v0, v1}, LzZ;->a(LaFX;)V

    .line 285
    :cond_2
    return-void

    .line 253
    :cond_3
    sget v4, Lxe;->doc_entry_group_title:I

    goto :goto_0

    .line 267
    :cond_4
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected a(LQX;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 289
    iget-object v0, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 290
    invoke-static {v3}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v4

    .line 292
    iget-object v0, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()LapG;

    move-result-object v0

    .line 293
    sget-object v5, LapG;->c:LapG;

    invoke-virtual {v5, v0}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 295
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 296
    const/4 v6, 0x2

    if-ne v0, v6, :cond_1

    move v0, v1

    .line 298
    :goto_0
    invoke-virtual {p1}, LQX;->a()LCl;

    move-result-object v6

    invoke-interface {v6}, LCl;->a()LCn;

    move-result-object v6

    .line 299
    sget-object v7, LCn;->i:LCn;

    invoke-virtual {v6, v7}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 300
    sget-object v8, LCn;->h:LCn;

    invoke-virtual {v6, v8}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 302
    sget v8, LwY;->is_twocolumn:I

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 304
    iget-object v8, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v8}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lamt;->a(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 305
    if-eqz v3, :cond_2

    if-nez v7, :cond_2

    if-nez v6, :cond_2

    if-nez v5, :cond_2

    .line 307
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 296
    goto :goto_0

    :cond_2
    move v1, v2

    .line 305
    goto :goto_1

    .line 307
    :cond_3
    if-eqz v4, :cond_4

    if-eqz v0, :cond_4

    if-nez v7, :cond_4

    if-nez v6, :cond_4

    if-eqz v5, :cond_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, LBS;->a:LzZ;

    invoke-interface {v0, p1}, LzZ;->c(I)I

    move-result v0

    .line 331
    invoke-virtual {p0, v0}, LBS;->a(I)V

    .line 332
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, LBS;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, LBS;->a:LzZ;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->b(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 314
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 323
    iget-boolean v0, p0, LBS;->c:Z

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, LBS;->a:LzZ;

    invoke-interface {v0}, LzZ;->notifyDataSetChanged()V

    .line 326
    :cond_0
    return-void
.end method
