.class public final enum LaHf;
.super Ljava/lang/Enum;
.source "SyncRequestJournalEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaHf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaHf;

.field private static final synthetic a:[LaHf;

.field public static final enum b:LaHf;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, LaHf;

    const-string v1, "UPLOAD"

    const-string v2, "upload"

    invoke-direct {v0, v1, v3, v2}, LaHf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaHf;->a:LaHf;

    new-instance v0, LaHf;

    const-string v1, "DOWNLOAD"

    const-string v2, "download"

    invoke-direct {v0, v1, v4, v2}, LaHf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaHf;->b:LaHf;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [LaHf;

    sget-object v1, LaHf;->a:LaHf;

    aput-object v1, v0, v3

    sget-object v1, LaHf;->b:LaHf;

    aput-object v1, v0, v4

    sput-object v0, LaHf;->a:[LaHf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput-object p3, p0, LaHf;->a:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static a(Ljava/lang/String;)LaHf;
    .locals 5

    .prologue
    .line 37
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-static {}, LaHf;->values()[LaHf;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 39
    invoke-virtual {v3}, LaHf;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 40
    return-object v3

    .line 38
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal value for SyncDirection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LaHf;
    .locals 1

    .prologue
    .line 23
    const-class v0, LaHf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaHf;

    return-object v0
.end method

.method public static values()[LaHf;
    .locals 1

    .prologue
    .line 23
    sget-object v0, LaHf;->a:[LaHf;

    invoke-virtual {v0}, [LaHf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaHf;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LaHf;->a:Ljava/lang/String;

    return-object v0
.end method
