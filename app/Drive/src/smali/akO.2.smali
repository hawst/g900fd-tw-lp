.class public LakO;
.super Ljava/lang/Object;
.source "ConnectivityImpl.java"

# interfaces
.implements LaKR;


# instance fields
.field private final a:Landroid/net/ConnectivityManager;

.field private final a:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 47
    const-string v0, "connectivity"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, LakO;->a:Landroid/net/ConnectivityManager;

    .line 48
    const-string v0, "phone"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, LakO;->a:Landroid/telephony/TelephonyManager;

    .line 49
    return-void
.end method

.method private a(Landroid/net/NetworkInfo;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 89
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/net/NetworkInfo;)Z
    .locals 1

    .prologue
    .line 94
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LaKS;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, LakO;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 82
    invoke-direct {p0, v0}, LakO;->b(Landroid/net/NetworkInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    sget-object v0, LaKS;->a:LaKS;

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, LakO;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LaKS;->b:LaKS;

    goto :goto_0

    :cond_1
    sget-object v0, LaKS;->c:LaKS;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, LakO;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-direct {p0, v0}, LakO;->b(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LakO;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-direct {p0, v0}, LakO;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 99
    iget-object v1, p0, LakO;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 100
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-nez v4, :cond_1

    .line 101
    const/4 v0, 0x1

    .line 104
    :cond_0
    return v0

    .line 99
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
