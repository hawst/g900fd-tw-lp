.class final LlT;
.super LkW;
.source "RegisteredMediaRouteProvider.java"


# instance fields
.field private a:I

.field private final a:Ljava/lang/String;

.field final synthetic a:LlO;

.field private a:LlQ;

.field private a:Z

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LlO;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, LlT;->a:LlO;

    invoke-direct {p0}, LkW;-><init>()V

    .line 298
    const/4 v0, -0x1

    iput v0, p0, LlT;->a:I

    .line 305
    iput-object p2, p0, LlT;->a:Ljava/lang/String;

    .line 306
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, LlT;->a:LlO;

    invoke-static {v0, p0}, LlO;->a(LlO;LlT;)V

    .line 335
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, LlT;->a:LlQ;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, LlT;->a:LlQ;

    iget v1, p0, LlT;->c:I

    invoke-virtual {v0, v1, p1}, LlQ;->a(II)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    iput p1, p0, LlT;->a:I

    .line 359
    const/4 v0, 0x0

    iput v0, p0, LlT;->b:I

    goto :goto_0
.end method

.method public a(LlQ;)V
    .locals 2

    .prologue
    .line 309
    iput-object p1, p0, LlT;->a:LlQ;

    .line 310
    iget-object v0, p0, LlT;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, LlQ;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LlT;->c:I

    .line 311
    iget-boolean v0, p0, LlT;->a:Z

    if-eqz v0, :cond_1

    .line 312
    iget v0, p0, LlT;->c:I

    invoke-virtual {p1, v0}, LlQ;->b(I)V

    .line 313
    iget v0, p0, LlT;->a:I

    if-ltz v0, :cond_0

    .line 314
    iget v0, p0, LlT;->c:I

    iget v1, p0, LlT;->a:I

    invoke-virtual {p1, v0, v1}, LlQ;->a(II)V

    .line 315
    const/4 v0, -0x1

    iput v0, p0, LlT;->a:I

    .line 317
    :cond_0
    iget v0, p0, LlT;->b:I

    if-eqz v0, :cond_1

    .line 318
    iget v0, p0, LlT;->c:I

    iget v1, p0, LlT;->b:I

    invoke-virtual {p1, v0, v1}, LlQ;->b(II)V

    .line 319
    const/4 v0, 0x0

    iput v0, p0, LlT;->b:I

    .line 322
    :cond_1
    return-void
.end method

.method public a(Landroid/content/Intent;Lln;)Z
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, LlT;->a:LlQ;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, LlT;->a:LlQ;

    iget v1, p0, LlT;->c:I

    invoke-virtual {v0, v1, p1, p2}, LlQ;->a(ILandroid/content/Intent;Lln;)Z

    move-result v0

    .line 377
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, LlT;->a:Z

    .line 340
    iget-object v0, p0, LlT;->a:LlQ;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, LlT;->a:LlQ;

    iget v1, p0, LlT;->c:I

    invoke-virtual {v0, v1}, LlQ;->b(I)V

    .line 343
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, LlT;->a:LlQ;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, LlT;->a:LlQ;

    iget v1, p0, LlT;->c:I

    invoke-virtual {v0, v1, p1}, LlQ;->b(II)V

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    iget v0, p0, LlT;->b:I

    add-int/2addr v0, p1

    iput v0, p0, LlT;->b:I

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, LlT;->a:Z

    .line 348
    iget-object v0, p0, LlT;->a:LlQ;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, LlT;->a:LlQ;

    iget v1, p0, LlT;->c:I

    invoke-virtual {v0, v1}, LlQ;->c(I)V

    .line 351
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, LlT;->a:LlQ;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, LlT;->a:LlQ;

    iget v1, p0, LlT;->c:I

    invoke-virtual {v0, v1}, LlQ;->a(I)V

    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, LlT;->a:LlQ;

    .line 328
    const/4 v0, 0x0

    iput v0, p0, LlT;->c:I

    .line 330
    :cond_0
    return-void
.end method
