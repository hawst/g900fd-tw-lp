.class final enum Lbif;
.super Ljava/lang/Enum;
.source "AbstractIterator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbif;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbif;

.field private static final synthetic a:[Lbif;

.field public static final enum b:Lbif;

.field public static final enum c:Lbif;

.field public static final enum d:Lbif;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lbif;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lbif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbif;->a:Lbif;

    new-instance v0, Lbif;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Lbif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbif;->b:Lbif;

    new-instance v0, Lbif;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lbif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbif;->c:Lbif;

    new-instance v0, Lbif;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lbif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbif;->d:Lbif;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Lbif;

    sget-object v1, Lbif;->a:Lbif;

    aput-object v1, v0, v2

    sget-object v1, Lbif;->b:Lbif;

    aput-object v1, v0, v3

    sget-object v1, Lbif;->c:Lbif;

    aput-object v1, v0, v4

    sget-object v1, Lbif;->d:Lbif;

    aput-object v1, v0, v5

    sput-object v0, Lbif;->a:[Lbif;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbif;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lbif;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbif;

    return-object v0
.end method

.method public static values()[Lbif;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lbif;->a:[Lbif;

    invoke-virtual {v0}, [Lbif;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbif;

    return-object v0
.end method
