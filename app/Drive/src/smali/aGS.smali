.class LaGS;
.super Landroid/os/AsyncTask;
.source "ModelLoaderAsyncTaskRunner.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:LaGN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaGN",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:LaGO;

.field final synthetic a:LaGR;

.field private a:LaGT;

.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private b:Z


# direct methods
.method public constructor <init>(LaGR;Landroid/app/Activity;LaGN;LaGO;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LaGN",
            "<TT;>;",
            "LaGO;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 36
    iput-object p1, p0, LaGS;->a:LaGR;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 38
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LaGS;->a:Ljava/lang/ref/WeakReference;

    .line 39
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGN;

    iput-object v0, p0, LaGS;->a:LaGN;

    .line 40
    iput-object p4, p0, LaGS;->a:LaGO;

    .line 41
    iput-boolean p5, p0, LaGS;->a:Z

    .line 42
    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 47
    :try_start_0
    iget-object v0, p0, LaGS;->a:LaGN;

    iget-object v1, p0, LaGS;->a:LaGR;

    iget-object v1, v1, LaGR;->a:LaGM;

    invoke-virtual {v0, v1}, LaGN;->a(LaGM;)Ljava/lang/Object;

    move-result-object v0

    .line 48
    const/4 v1, 0x1

    iput-boolean v1, p0, LaGS;->b:Z
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-object v0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    iput-object v0, p0, LaGS;->a:LaGT;

    .line 52
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaGS;->a([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final onPostExecute(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, LaGS;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 59
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 60
    :cond_0
    const-string v0, "ModelLoaderAsyncTask"

    const-string v1, "Task result ignored for finished activity: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LaGS;->a:LaGN;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 77
    :cond_1
    :goto_0
    return-void

    .line 64
    :cond_2
    iget-boolean v1, p0, LaGS;->a:Z

    if-eqz v1, :cond_3

    .line 65
    iget-object v1, p0, LaGS;->a:LaGO;

    iget-object v2, p0, LaGS;->a:LaGN;

    invoke-virtual {v1, v0, v2}, LaGO;->b(Landroid/app/Activity;LaGN;)V

    .line 68
    :cond_3
    iget-object v1, p0, LaGS;->a:LaGN;

    invoke-virtual {v1}, LaGN;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 72
    iget-boolean v1, p0, LaGS;->b:Z

    if-nez v1, :cond_4

    .line 73
    iget-object v1, p0, LaGS;->a:LaGO;

    iget-object v2, p0, LaGS;->a:LaGT;

    invoke-virtual {v1, v0, v2}, LaGO;->a(Landroid/app/Activity;LaGT;)V

    goto :goto_0

    .line 76
    :cond_4
    iget-object v0, p0, LaGS;->a:LaGN;

    invoke-virtual {v0, p1}, LaGN;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 81
    const-string v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaGS;->a:LaGN;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
