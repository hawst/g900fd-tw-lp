.class public LaGt;
.super Ljava/lang/Object;
.source "Entries.java"


# direct methods
.method public static a(LaGv;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 74
    sget-object v0, LaGv;->d:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 75
    invoke-static {p1}, LaGt;->b(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LaGv;->b()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LaGv;Ljava/lang/String;Z)I
    .locals 1

    .prologue
    .line 63
    sget-object v0, LaGv;->d:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 64
    invoke-static {p1}, LaGt;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 70
    :goto_0
    return v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, LaGv;->d()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LaGv;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 49
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-static {p0}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    .line 52
    if-nez v0, :cond_0

    .line 53
    sget v0, Lxi;->document_type_unknown:I

    .line 55
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, LaGn;->d()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 23
    invoke-static {p0}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGn;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LaGv;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 88
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, LaGv;->d:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-virtual {p0}, LaGv;->f()I

    move-result v0

    .line 94
    :goto_0
    return v0

    .line 91
    :cond_0
    if-eqz p1, :cond_1

    .line 92
    invoke-static {p1}, LaGt;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 94
    :cond_1
    sget v0, Lxi;->document_type_unknown:I

    goto :goto_0
.end method

.method public static b(LaGv;Ljava/lang/String;Z)I
    .locals 1

    .prologue
    .line 103
    sget-object v0, LaGv;->d:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 104
    invoke-static {p1}, LaGt;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 110
    :goto_0
    return v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, LaGv;->d()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LaGv;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGn;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LaGv;Ljava/lang/String;Z)I
    .locals 1

    .prologue
    .line 128
    sget-object v0, LaGv;->d:LaGv;

    if-ne p0, v0, :cond_0

    if-eqz p1, :cond_0

    .line 129
    invoke-static {p1}, LaGt;->c(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 137
    :goto_0
    return v0

    .line 133
    :cond_0
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 134
    invoke-virtual {p0}, LaGv;->e()I

    move-result v0

    goto :goto_0

    .line 137
    :cond_1
    invoke-virtual {p0}, LaGv;->c()I

    move-result v0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 41
    invoke-static {p0}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGn;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
