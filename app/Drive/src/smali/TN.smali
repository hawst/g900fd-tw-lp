.class public LTN;
.super LTy;
.source "HttpIssuerResponse.java"


# instance fields
.field private a:LTL;

.field private a:Lorg/apache/http/HttpEntity;


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpResponse;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1}, LTy;-><init>(Lorg/apache/http/HttpResponse;)V

    .line 16
    iput-object v0, p0, LTN;->a:Lorg/apache/http/HttpEntity;

    .line 17
    iput-object v0, p0, LTN;->a:LTL;

    .line 26
    return-void
.end method


# virtual methods
.method public a()LTL;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, LTN;->a:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 31
    iget-object v1, p0, LTN;->a:Lorg/apache/http/HttpEntity;

    if-eq v0, v1, :cond_0

    .line 32
    iput-object v0, p0, LTN;->a:Lorg/apache/http/HttpEntity;

    .line 33
    iget-object v0, p0, LTN;->a:Lorg/apache/http/HttpEntity;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LTN;->a:LTL;

    .line 35
    :cond_0
    iget-object v0, p0, LTN;->a:LTL;

    return-object v0

    .line 33
    :cond_1
    new-instance v0, LTL;

    iget-object v1, p0, LTN;->a:Lorg/apache/http/HttpEntity;

    invoke-direct {v0, v1}, LTL;-><init>(Lorg/apache/http/HttpEntity;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, LTN;->a()LTL;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {v0}, LTL;->a()Z

    move-result v0

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic getEntity()Lorg/apache/http/HttpEntity;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, LTN;->a()LTL;

    move-result-object v0

    return-object v0
.end method
