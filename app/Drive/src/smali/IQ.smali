.class public LIQ;
.super Ljava/lang/Object;
.source "TimeRange.java"


# instance fields
.field private final a:J

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:J


# direct methods
.method public constructor <init>(Ljava/util/Calendar;)V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const-wide/32 v12, 0x5265c00

    const/4 v4, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 46
    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 47
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 48
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 49
    const/4 v5, 0x7

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 50
    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v8

    move v5, v4

    move v6, v4

    .line 52
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 53
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    iput-wide v10, p0, LIQ;->a:J

    .line 55
    iget-wide v10, p0, LIQ;->a:J

    sub-long/2addr v10, v12

    iput-wide v10, p0, LIQ;->b:J

    .line 56
    iget-wide v10, p0, LIQ;->a:J

    add-int/lit8 v2, v7, -0x1

    int-to-long v6, v2

    mul-long/2addr v6, v12

    sub-long v6, v10, v6

    iput-wide v6, p0, LIQ;->c:J

    .line 57
    iget-wide v6, p0, LIQ;->a:J

    add-int/lit8 v2, v3, -0x1

    int-to-long v2, v2

    mul-long/2addr v2, v12

    sub-long v2, v6, v2

    iput-wide v2, p0, LIQ;->d:J

    .line 58
    iget-wide v2, p0, LIQ;->a:J

    add-int/lit8 v5, v8, -0x1

    int-to-long v6, v5

    mul-long/2addr v6, v12

    sub-long/2addr v2, v6

    iput-wide v2, p0, LIQ;->e:J

    .line 60
    add-int/lit8 v1, v1, -0x1

    move v2, v4

    move v3, v9

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 61
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, LIQ;->f:J

    .line 62
    return-void
.end method


# virtual methods
.method public a(J)LIP;
    .locals 3

    .prologue
    .line 73
    iget-wide v0, p0, LIQ;->a:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 74
    sget-object v0, LIP;->a:LIP;

    .line 91
    :goto_0
    return-object v0

    .line 76
    :cond_0
    iget-wide v0, p0, LIQ;->b:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 77
    sget-object v0, LIP;->b:LIP;

    goto :goto_0

    .line 79
    :cond_1
    iget-wide v0, p0, LIQ;->c:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 80
    sget-object v0, LIP;->c:LIP;

    goto :goto_0

    .line 82
    :cond_2
    iget-wide v0, p0, LIQ;->d:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_3

    .line 83
    sget-object v0, LIP;->d:LIP;

    goto :goto_0

    .line 85
    :cond_3
    iget-wide v0, p0, LIQ;->e:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_4

    .line 86
    sget-object v0, LIP;->e:LIP;

    goto :goto_0

    .line 88
    :cond_4
    iget-wide v0, p0, LIQ;->f:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_5

    .line 89
    sget-object v0, LIP;->f:LIP;

    goto :goto_0

    .line 91
    :cond_5
    sget-object v0, LIP;->g:LIP;

    goto :goto_0
.end method
