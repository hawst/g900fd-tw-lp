.class public LaHn;
.super LaHq;
.source "NameOp.java"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    const-string v0, "rename"

    invoke-direct {p0, p1, p2, v0}, LaHq;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 34
    iput-object p3, p0, LaHn;->a:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHn;
    .locals 2

    .prologue
    .line 40
    new-instance v0, LaHn;

    const-string v1, "nameValue"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, LaHn;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(LaGe;)LaHo;
    .locals 4

    .prologue
    .line 51
    new-instance v0, LaHn;

    iget-object v1, p0, LaHn;->a:LaGg;

    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-virtual {p1}, LaGe;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaHn;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 52
    iget-object v1, p0, LaHn;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, LaGe;->a(Ljava/lang/String;)LaGe;

    .line 53
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 58
    invoke-super {p0}, LaHq;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 59
    const-string v1, "operationName"

    const-string v2, "rename"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    const-string v1, "nameValue"

    iget-object v2, p0, LaHn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    return-object v0
.end method

.method public a(LaJT;)V
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0, p1}, LaHn;->b(LaJT;)V

    .line 46
    iget-object v0, p0, LaHn;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, LaJT;->l(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 67
    instance-of v1, p1, LaHn;

    if-nez v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    check-cast p1, LaHn;

    .line 71
    invoke-virtual {p0, p1}, LaHn;->a(LaHp;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaHn;->a:Ljava/lang/String;

    iget-object v2, p1, LaHn;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, LaHn;->b()I

    move-result v0

    iget-object v1, p0, LaHn;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 81
    const-string v0, "NameOp[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaHn;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaHn;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
