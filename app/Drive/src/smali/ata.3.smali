.class final Lata;
.super Landroid/os/AsyncTask;
.source "VoucherServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LasY;

.field final synthetic a:LasZ;

.field private final a:Lasd;

.field private a:Ljava/lang/String;


# direct methods
.method constructor <init>(LasZ;LasY;Lasd;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lata;->a:LasZ;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 84
    iput-object p2, p0, Lata;->a:LasY;

    .line 85
    iput-object p3, p0, Lata;->a:Lasd;

    .line 86
    return-void
.end method

.method static synthetic a(Lata;)Lasd;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lata;->a:Lasd;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    invoke-direct {p0, p1}, Lata;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    const-string v0, "VoucherService Exception: Account"

    .line 308
    :goto_0
    return-object v0

    .line 299
    :cond_0
    sget v0, Lxi;->welcome_offer_fail_bug:I

    if-ne p1, v0, :cond_1

    .line 300
    const-string v0, "VoucherService Exception: Bug"

    goto :goto_0

    .line 301
    :cond_1
    sget v0, Lxi;->welcome_offer_fail_connect:I

    if-ne p1, v0, :cond_2

    .line 302
    const-string v0, "VoucherService Exception: Connection"

    goto :goto_0

    .line 303
    :cond_2
    sget v0, Lxi;->welcome_offer_fail_disabled:I

    if-ne p1, v0, :cond_3

    .line 304
    const-string v0, "VoucherService Exception: Disabled"

    goto :goto_0

    .line 305
    :cond_3
    sget v0, Lxi;->welcome_offer_fail_redeemed:I

    if-ne p1, v0, :cond_4

    .line 306
    const-string v0, "VoucherService Exception: Redeemed"

    goto :goto_0

    .line 308
    :cond_4
    const-string v0, "Unknown error message"

    goto :goto_0
.end method

.method private a(ILjava/lang/Exception;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 313
    iget-object v0, p0, Lata;->a:LasZ;

    iget-object v0, v0, LasZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lata;->a:Lasd;

    invoke-virtual {v2}, Lasd;->a()LaFO;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 314
    const-string v1, "VoucherServiceImpl"

    const-string v2, "Can\'t contact Voucher service because of %s, reporting %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    aput-object v0, v3, v5

    invoke-static {v1, p2, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 316
    return-object v0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 321
    invoke-direct {p0, p1}, Lata;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lata;->a:LasZ;

    iget-object v0, v0, LasZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lata;->a:Lasd;

    invoke-virtual {v3}, Lasd;->a()LaFO;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 326
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 324
    :cond_0
    iget-object v0, p0, Lata;->a:LasZ;

    iget-object v0, v0, LasZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lata;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lata;->a:Ljava/lang/String;

    return-object v0
.end method

.method private varargs a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 234
    array-length v0, p2

    if-nez v0, :cond_0

    .line 247
    :goto_0
    return-object p1

    .line 239
    :cond_0
    :try_start_0
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    invoke-virtual {v0, p1}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v1

    .line 241
    const/4 v0, 0x0

    :goto_1
    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    .line 242
    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->a()Lbtk;

    move-result-object v1

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 244
    :cond_1
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p2, v0

    invoke-virtual {v1, v0}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lbth;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object p1, v0

    goto :goto_0

    :cond_2
    const-string v0, ""
    :try_end_0
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 246
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/net/URI;
    .locals 5

    .prologue
    .line 256
    iget-object v0, p0, Lata;->a:LasZ;

    iget-object v0, v0, LasZ;->a:LQr;

    const-string v1, "voucherServiceUrl"

    const-string v2, "https://www.googleapis.com/voucherredemption/v1/products"

    .line 257
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 260
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 261
    :catch_0
    move-exception v0

    .line 262
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Voucher server URL invalid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method static synthetic a(Lata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lata;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 5

    .prologue
    .line 267
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {p0, p1}, Lata;->a(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 268
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OAuth "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :try_start_0
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    const-string v2, "{voucher:\'%s\'}"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 272
    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    return-object v0

    .line 273
    :catch_0
    move-exception v0

    .line 274
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot encode voucher entity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 252
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    long-to-int v1, v2

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lata;->publishProgress([Ljava/lang/Object;)V

    .line 253
    return-void
.end method

.method private a(LaFO;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 134
    :try_start_0
    iget-object v0, p0, Lata;->a:LasZ;

    iget-object v0, v0, LasZ;->a:LTd;

    const-string v1, "oauth2:https://www.googleapis.com/auth/vouchers"

    invoke-interface {v0, p1, v1}, LTd;->b(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lata;->a:Ljava/lang/String;

    .line 135
    const-string v0, "VoucherServiceImpl"

    const-string v1, "Got OAuth2 token: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lata;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 136
    iget-object v0, p0, Lata;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 137
    const-string v0, "VoucherServiceImpl"

    const-string v1, "null OAuth token. scope: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "oauth2:https://www.googleapis.com/auth/vouchers"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_2

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    sget v1, Lxi;->welcome_offer_fail_bug:I

    invoke-direct {p0, v6, v1, v0}, Lata;->a(ZILjava/lang/Exception;)V

    goto :goto_0

    .line 141
    :catch_1
    move-exception v0

    .line 142
    sget v1, Lxi;->welcome_offer_fail_connect:I

    invoke-direct {p0, v5, v1, v0}, Lata;->a(ZILjava/lang/Exception;)V

    goto :goto_0

    .line 143
    :catch_2
    move-exception v0

    .line 144
    sget v1, Lxi;->welcome_offer_fail_account:I

    invoke-direct {p0, v5, v1, v0}, Lata;->a(ZILjava/lang/Exception;)V

    goto :goto_0
.end method

.method private a(ZILjava/lang/Exception;)V
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lata;->a:LasZ;

    iget-object v1, v0, LasZ;->a:LqK;

    invoke-direct {p0, p2}, Lata;->a(I)Ljava/lang/String;

    move-result-object v2

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Z)V

    .line 281
    iget-object v0, p0, Lata;->a:LasY;

    invoke-direct {p0, p2, p3}, Lata;->a(ILjava/lang/Exception;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LasY;->a(ZLjava/lang/String;)V

    .line 282
    return-void

    .line 280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private varargs a(ZI[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 285
    array-length v0, p3

    if-nez v0, :cond_0

    const-string v0, ""

    .line 286
    :goto_0
    iget-object v1, p0, Lata;->a:LasZ;

    iget-object v2, v1, LasZ;->a:LqK;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p2}, Lata;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez p1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v3, v1}, LqK;->a(Ljava/lang/String;Z)V

    .line 287
    iget-object v1, p0, Lata;->a:LasY;

    invoke-direct {p0, p2, v0}, Lata;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, LasY;->a(ZLjava/lang/String;)V

    .line 288
    return-void

    .line 285
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-static {v1}, LbiI;->a(Ljava/lang/String;)LbiI;

    move-result-object v1

    invoke-virtual {v1, p3}, LbiI;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 286
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 291
    sget v0, Lxi;->welcome_offer_fail_account:I

    if-eq p1, v0, :cond_0

    sget v0, Lxi;->welcome_offer_fail_account_unlimited:I

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)Z
    .locals 8

    .prologue
    const/16 v4, 0x190

    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 176
    const/16 v2, 0xcc

    if-ne p1, v2, :cond_0

    .line 177
    const-string v1, "VoucherServiceImpl"

    const-string v2, "VoucherService redeem success (204)."

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v1, p0, Lata;->a:LasY;

    iget-object v2, p0, Lata;->a:Lasd;

    invoke-virtual {v2}, Lasd;->a()LaFO;

    move-result-object v2

    invoke-interface {v1, v2}, LasY;->a(LaFO;)V

    .line 220
    :goto_0
    return v0

    .line 180
    :cond_0
    if-ge p1, v4, :cond_6

    .line 181
    new-array v2, v0, [Ljava/lang/String;

    const-string v3, "status"

    aput-object v3, v2, v1

    invoke-direct {p0, p2, v2}, Lata;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 182
    const-string v3, "VoucherServiceImpl"

    const-string v4, "VoucherService responded (%s), Status: %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 183
    const-string v3, "REDEEMED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 184
    sget v2, Lxi;->welcome_offer_fail_account:I

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, v2, v1}, Lata;->a(ZI[Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :cond_1
    const-string v3, "DISABLED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 186
    new-array v2, v0, [Ljava/lang/String;

    const-string v3, "reason"

    aput-object v3, v2, v1

    invoke-direct {p0, p2, v2}, Lata;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 187
    sget v3, Lxi;->welcome_offer_fail_disabled:I

    new-array v4, v0, [Ljava/lang/String;

    aput-object v2, v4, v1

    invoke-direct {p0, v1, v3, v4}, Lata;->a(ZI[Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :cond_2
    const-string v3, "UNAVAILABLE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 189
    new-array v2, v0, [Ljava/lang/String;

    const-string v3, "reason"

    aput-object v3, v2, v1

    invoke-direct {p0, p2, v2}, Lata;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 190
    const-string v3, "UNLIMITED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 191
    sget v2, Lxi;->welcome_offer_fail_account_unlimited:I

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, v2, v1}, Lata;->a(ZI[Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_3
    sget v2, Lxi;->welcome_offer_fail_account:I

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, v2, v1}, Lata;->a(ZI[Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :cond_4
    const-string v3, "AVAILABLE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    .line 196
    goto :goto_0

    .line 199
    :cond_5
    const-string v3, "VoucherServiceImpl"

    const-string v4, "Unexpected voucher status (%s): %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 200
    goto/16 :goto_0

    .line 203
    :cond_6
    const/16 v2, 0x1f4

    if-ge p1, v2, :cond_9

    .line 204
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "error"

    aput-object v3, v2, v1

    const-string v3, "message"

    aput-object v3, v2, v0

    invoke-direct {p0, p2, v2}, Lata;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 206
    if-ne p1, v4, :cond_7

    const-string v3, "Expired Voucher"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 207
    sget v2, Lxi;->welcome_offer_fail_redeemed:I

    new-array v3, v1, [Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3}, Lata;->a(ZI[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 208
    :cond_7
    const/16 v3, 0x193

    if-ne p1, v3, :cond_8

    .line 209
    const-string v3, "VoucherServiceImpl"

    const-string v4, "VoucherService error (%s): %s (token %s)"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    aput-object v2, v5, v0

    iget-object v6, p0, Lata;->a:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 210
    sget v3, Lxi;->welcome_offer_fail_connect:I

    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "-- %s (%s)"

    new-array v6, v7, [Ljava/lang/Object;

    .line 211
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    aput-object v2, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 210
    invoke-direct {p0, v0, v3, v4}, Lata;->a(ZI[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 213
    :cond_8
    const-string v3, "VoucherServiceImpl"

    const-string v4, "VoucherService error (%s): %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 214
    sget v3, Lxi;->welcome_offer_fail_bug:I

    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "-- %s (%s)"

    new-array v6, v7, [Ljava/lang/Object;

    .line 215
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    aput-object v2, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 214
    invoke-direct {p0, v1, v3, v4}, Lata;->a(ZI[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 219
    :cond_9
    const-string v2, "VoucherServiceImpl"

    const-string v3, "Contacted server but it was NFW (%s): %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    aput-object p2, v4, v0

    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 220
    goto/16 :goto_0
.end method

.method private a(JLjava/util/concurrent/Callable;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 339
    invoke-interface {p3}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 342
    const/4 v2, 0x3

    .line 343
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v0, p1, v0

    .line 344
    invoke-direct {p0, v0, v1}, Lata;->a(J)V

    .line 345
    :goto_0
    if-nez v3, :cond_2

    invoke-virtual {p0}, Lata;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_2

    .line 346
    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 350
    div-int/lit8 v0, v2, 0x3

    .line 353
    :goto_1
    const-wide/16 v4, 0xbb8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, p1, v4

    invoke-direct {p0, v4, v5}, Lata;->a(J)V

    .line 356
    invoke-virtual {p0}, Lata;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361
    :cond_0
    const-string v0, "VoucherServiceImpl"

    const-string v1, "Waited %ds, trying again."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 362
    invoke-interface {p3}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 364
    mul-int/lit8 v2, v2, 0x3

    .line 365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v0, p1, v0

    .line 366
    invoke-direct {p0, v0, v1}, Lata;->a(J)V

    goto :goto_0

    .line 359
    :cond_1
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    move v0, v1

    goto :goto_1

    .line 354
    :catch_0
    move-exception v1

    goto :goto_2

    .line 368
    :cond_2
    return v3
.end method

.method static synthetic a(Lata;Lorg/apache/http/client/methods/HttpUriRequest;)Z
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lata;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    move-result v0

    return v0
.end method

.method private a(Lorg/apache/http/client/methods/HttpUriRequest;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 150
    const-string v1, "VoucherServiceImpl"

    const-string v2, "Contacting server (%s)"

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 152
    :try_start_0
    iget-object v1, p0, Lata;->a:LasZ;

    iget-object v1, v1, LasZ;->a:LTF;

    invoke-interface {v1, p1}, LTF;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 153
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 154
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 155
    if-nez v1, :cond_0

    const-string v1, ""

    .line 156
    :goto_0
    invoke-direct {p0, v2, v1}, Lata;->a(ILjava/lang/String;)Z
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 164
    iget-object v1, p0, Lata;->a:LasZ;

    iget-object v1, v1, LasZ;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    .line 166
    :goto_1
    return v0

    .line 155
    :cond_0
    :try_start_1
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lbtl; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    .line 158
    const/4 v2, 0x1

    :try_start_2
    sget v3, Lxi;->welcome_offer_fail_connect:I

    invoke-direct {p0, v2, v3, v1}, Lata;->a(ZILjava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 164
    iget-object v1, p0, Lata;->a:LasZ;

    iget-object v1, v1, LasZ;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    goto :goto_1

    .line 159
    :catch_1
    move-exception v1

    .line 160
    const/4 v2, 0x1

    :try_start_3
    sget v3, Lxi;->welcome_offer_fail_connect:I

    invoke-direct {p0, v2, v3, v1}, Lata;->a(ZILjava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 164
    iget-object v1, p0, Lata;->a:LasZ;

    iget-object v1, v1, LasZ;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    goto :goto_1

    .line 161
    :catch_2
    move-exception v1

    .line 162
    const/4 v2, 0x1

    :try_start_4
    sget v3, Lxi;->welcome_offer_fail_connect:I

    invoke-direct {p0, v2, v3, v1}, Lata;->a(ZILjava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 164
    iget-object v1, p0, Lata;->a:LasZ;

    iget-object v1, v1, LasZ;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lata;->a:LasZ;

    iget-object v1, v1, LasZ;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    throw v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 90
    const-string v0, "VoucherServiceImpl"

    const-string v1, "Start redeeming offer %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lata;->a:Lasd;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x9c40

    add-long/2addr v0, v2

    .line 93
    iget-object v2, p0, Lata;->a:Lasd;

    invoke-virtual {v2}, Lasd;->a()LaFO;

    move-result-object v2

    invoke-direct {p0, v2}, Lata;->a(LaFO;)V

    .line 94
    iget-object v2, p0, Lata;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lata;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-object v6

    .line 98
    :cond_1
    const-string v2, "/get"

    iget-object v3, p0, Lata;->a:Ljava/lang/String;

    iget-object v4, p0, Lata;->a:Lasd;

    invoke-virtual {v4}, Lasd;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lata;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    invoke-direct {p0, v2}, Lata;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    move-result v2

    .line 99
    if-nez v2, :cond_0

    .line 104
    new-instance v2, Latb;

    invoke-direct {v2, p0}, Latb;-><init>(Lata;)V

    .line 111
    :try_start_0
    invoke-direct {p0, v0, v1, v2}, Lata;->a(JLjava/util/concurrent/Callable;)Z

    move-result v0

    .line 112
    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x1

    sget v1, Lxi;->welcome_offer_fail_connect:I

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lata;->a(ZI[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    sget v1, Lxi;->welcome_offer_fail_bug:I

    invoke-direct {p0, v5, v1, v0}, Lata;->a(ZILjava/lang/Exception;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lata;->a:LasY;

    invoke-interface {v0}, LasY;->run()V

    .line 125
    return-void
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lata;->a:LasY;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v1, v2}, LasY;->a(II)V

    .line 130
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lata;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lata;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lata;->a([Ljava/lang/Integer;)V

    return-void
.end method
