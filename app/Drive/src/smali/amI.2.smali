.class public LamI;
.super Ljava/lang/Object;
.source "SQLiteTimedStringQueue.java"

# interfaces
.implements Lamm;


# instance fields
.field private a:I

.field private final a:LaKM;

.field private final a:LamJ;

.field private a:Landroid/database/sqlite/SQLiteDatabase;

.field private a:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 110
    sget-object v0, LaKN;->a:LaKN;

    invoke-direct {p0, p1, p2, p3, v0}, LamI;-><init>(ILjava/lang/String;Landroid/content/Context;LaKM;)V

    .line 111
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Landroid/content/Context;LaKM;)V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, LamI;->a:Z

    .line 101
    iput p1, p0, LamI;->a:I

    .line 102
    new-instance v0, LamJ;

    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LamJ;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, LamI;->a:LamJ;

    .line 103
    iput-object p4, p0, LamI;->a:LaKM;

    .line 104
    return-void
.end method

.method static synthetic a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-static {p0, p1}, LamI;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-boolean v0, p0, LamI;->a:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Trying to access queue after cleanUp()"

    invoke-static {v0, v2}, LbiT;->b(ZLjava/lang/Object;)V

    .line 121
    iget-object v0, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 123
    :cond_0
    :try_start_0
    iget-object v0, p0, LamI;->a:LamJ;

    invoke-virtual {v0}, LamJ;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 124
    invoke-direct {p0}, LamI;->b()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 120
    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    const-string v2, "SQLiteTimedStringQueue"

    const-string v3, "Error opening database"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    goto :goto_1
.end method

.method private static varargs b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, p0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 149
    iget-object v0, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DELETE FROM %s WHERE %s <= (SELECT %s FROM %s ORDER BY %s DESC LIMIT 1 OFFSET %d);"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "queue"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "item_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "item_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "queue"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "item_id"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v4, p0, LamI;->a:I

    .line 151
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 149
    invoke-static {v1, v2}, LamI;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 152
    return-void
.end method


# virtual methods
.method a()LamK;
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 160
    .line 162
    :try_start_0
    iget-object v0, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "queue"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "item_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "item_content"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "item_time"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "%s ASC"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "item_id"

    aput-object v11, v8, v10

    .line 163
    invoke-static {v7, v8}, LamI;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    .line 162
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 164
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    new-instance v0, LamK;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, LamK;-><init>(JLjava/lang/String;J)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 170
    if-eqz v6, :cond_0

    .line 171
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 174
    :cond_0
    :goto_0
    return-object v0

    .line 170
    :cond_1
    if-eqz v6, :cond_2

    .line 171
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v9

    .line 174
    goto :goto_0

    .line 167
    :catch_0
    move-exception v0

    move-object v1, v9

    .line 168
    :goto_2
    :try_start_2
    const-string v2, "SQLiteTimedStringQueue"

    const-string v3, "Error peeking queue"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 170
    if-eqz v1, :cond_2

    .line 171
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 170
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v9, :cond_3

    .line 171
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 170
    :catchall_1
    move-exception v0

    move-object v9, v6

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v9, v1

    goto :goto_3

    .line 167
    :catch_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public declared-synchronized a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LamI;->a()V

    .line 237
    invoke-virtual {p0}, LamI;->a()LamK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 238
    if-nez v0, :cond_0

    .line 239
    const/4 v0, 0x0

    .line 241
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, v0, LamK;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(J)V
    .locals 9

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LamI;->a()V

    .line 261
    iget-object v0, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "queue"

    const-string v2, "%s < ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "item_time"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LamI;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LamI;->a:LaKM;

    .line 262
    invoke-interface {v5}, LaKM;->a()J

    move-result-wide v6

    sub-long/2addr v6, p1

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 261
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 263
    const-string v1, "SQLiteTimedStringQueue"

    const-string v2, "%d rows deleted as they\'re older than %sms"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    monitor-exit p0

    return-void

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Ljava/lang/String;
    .locals 8

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LamI;->a()V

    .line 248
    invoke-virtual {p0}, LamI;->a()LamK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 249
    if-nez v0, :cond_0

    .line 250
    const/4 v0, 0x0

    .line 255
    :goto_0
    monitor-exit p0

    return-object v0

    .line 252
    :cond_0
    :try_start_1
    iget-object v1, p0, LamI;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "queue"

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "item_id"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, LamI;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, v0, LamK;->a:J

    .line 253
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 252
    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 254
    const-string v1, "SQLiteTimedStringQueue"

    const-string v2, "remove %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, LamK;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 255
    iget-object v0, v0, LamK;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
