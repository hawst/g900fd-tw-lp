.class public LaFP;
.super LaGm;
.source "AccountMetadata.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEc;",
        "LaEz;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Ljava/util/Date;


# instance fields
.field private final a:J

.field private a:Ljava/lang/String;

.field private b:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 22
    new-instance v0, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    sput-object v0, LaFP;->a:Ljava/util/Date;

    return-void
.end method

.method public constructor <init>(LaEz;J)V
    .locals 2

    .prologue
    .line 34
    invoke-static {}, LaEc;->a()LaEc;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 24
    sget-object v0, LaFP;->a:Ljava/util/Date;

    iput-object v0, p0, LaFP;->b:Ljava/util/Date;

    .line 35
    iput-wide p2, p0, LaFP;->a:J

    .line 36
    return-void
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaFP;
    .locals 6

    .prologue
    .line 64
    sget-object v0, LaEd;->a:LaEd;

    invoke-virtual {v0}, LaEd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 65
    new-instance v2, LaFP;

    invoke-direct {v2, p0, v0, v1}, LaFP;-><init>(LaEz;J)V

    .line 67
    invoke-static {}, LaEc;->a()LaEc;

    move-result-object v0

    invoke-virtual {v0}, LaEc;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 66
    invoke-virtual {v2, v0, v1}, LaFP;->c(J)V

    .line 68
    sget-object v0, LaEd;->b:LaEd;

    .line 69
    invoke-virtual {v0}, LaEd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 71
    new-instance v1, Ljava/util/Date;

    sget-object v3, LaEd;->c:LaEd;

    .line 72
    invoke-virtual {v3}, LaEd;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 73
    invoke-virtual {v2, v0, v1}, LaFP;->a(Ljava/lang/String;Ljava/util/Date;)V

    .line 76
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LaFP;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, LaFP;->b:Ljava/util/Date;

    return-object v0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 81
    sget-object v0, LaEd;->a:LaEd;

    invoke-virtual {v0}, LaEd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaFP;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 82
    sget-object v0, LaEd;->b:LaEd;

    invoke-virtual {v0}, LaEd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaFP;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object v0, LaEd;->c:LaEd;

    invoke-virtual {v0}, LaEd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaFP;->b:Ljava/util/Date;

    .line 85
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 84
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 86
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 42
    iput-object p1, p0, LaFP;->a:Ljava/lang/String;

    .line 43
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, LaFP;->b:Ljava/util/Date;

    .line 44
    return-void
.end method
