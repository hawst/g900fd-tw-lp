.class public final Ljf;
.super Ljava/lang/Object;
.source "MenuItemImpl.java"

# interfaces
.implements Lch;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;


# instance fields
.field private a:C

.field private final a:I

.field private a:Landroid/content/Intent;

.field private a:Landroid/graphics/drawable/Drawable;

.field private a:Landroid/view/ContextMenu$ContextMenuInfo;

.field private a:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private a:Landroid/view/View;

.field private a:LdE;

.field private a:Ldf;

.field private a:Ljava/lang/CharSequence;

.field private a:Ljava/lang/Runnable;

.field private a:Ljb;

.field private a:Ljw;

.field private a:Z

.field private b:C

.field private final b:I

.field private b:Ljava/lang/CharSequence;

.field private final c:I

.field private final d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method constructor <init>(Ljb;IIIILjava/lang/CharSequence;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput v1, p0, Ljf;->e:I

    .line 76
    const/16 v0, 0x10

    iput v0, p0, Ljf;->f:I

    .line 84
    iput v1, p0, Ljf;->g:I

    .line 89
    iput-boolean v1, p0, Ljf;->a:Z

    .line 133
    iput-object p1, p0, Ljf;->a:Ljb;

    .line 134
    iput p3, p0, Ljf;->a:I

    .line 135
    iput p2, p0, Ljf;->b:I

    .line 136
    iput p4, p0, Ljf;->c:I

    .line 137
    iput p5, p0, Ljf;->d:I

    .line 138
    iput-object p6, p0, Ljf;->a:Ljava/lang/CharSequence;

    .line 139
    iput p7, p0, Ljf;->g:I

    .line 140
    return-void
.end method

.method static synthetic a(Ljf;)Ljb;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ljf;->a:Ljb;

    return-object v0
.end method


# virtual methods
.method public a()C
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0}, Ljb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-char v0, p0, Ljf;->b:C

    :goto_0
    return v0

    :cond_0
    iget-char v0, p0, Ljf;->a:C

    goto :goto_0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Ljf;->d:I

    return v0
.end method

.method public a(I)Lch;
    .locals 3

    .prologue
    .line 608
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0}, Ljb;->a()Landroid/content/Context;

    move-result-object v0

    .line 609
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 610
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljf;->a(Landroid/view/View;)Lch;

    .line 611
    return-object p0
.end method

.method public a(Landroid/view/View;)Lch;
    .locals 2

    .prologue
    .line 597
    iput-object p1, p0, Ljf;->a:Landroid/view/View;

    .line 598
    const/4 v0, 0x0

    iput-object v0, p0, Ljf;->a:Ldf;

    .line 599
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Ljf;->a:I

    if-lez v0, :cond_0

    .line 600
    iget v0, p0, Ljf;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 602
    :cond_0
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->b(Ljf;)V

    .line 603
    return-object p0
.end method

.method public a(Ldf;)Lch;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 645
    iget-object v0, p0, Ljf;->a:Ldf;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Ljf;->a:Ldf;

    invoke-virtual {v0, v1}, Ldf;->a(Ldh;)V

    .line 648
    :cond_0
    iput-object v1, p0, Ljf;->a:Landroid/view/View;

    .line 649
    iput-object p1, p0, Ljf;->a:Ldf;

    .line 650
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 651
    iget-object v0, p0, Ljf;->a:Ldf;

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Ljf;->a:Ldf;

    new-instance v1, Ljg;

    invoke-direct {v1, p0}, Ljg;-><init>(Ljf;)V

    invoke-virtual {v0, v1}, Ldf;->a(Ldh;)V

    .line 659
    :cond_1
    return-object p0
.end method

.method public a()Ldf;
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Ljf;->a:Ldf;

    return-object v0
.end method

.method public a(Ljt;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 362
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljf;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljf;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 295
    invoke-virtual {p0}, Ljf;->a()C

    move-result v0

    .line 296
    if-nez v0, :cond_0

    .line 297
    const-string v0, ""

    .line 320
    :goto_0
    return-object v0

    .line 300
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Ljf;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 301
    sparse-switch v0, :sswitch_data_0

    .line 316
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 320
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 304
    :sswitch_0
    sget-object v0, Ljf;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 308
    :sswitch_1
    sget-object v0, Ljf;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 312
    :sswitch_2
    sget-object v0, Ljf;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 301
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0xa -> :sswitch_0
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method public a()V
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->b(Ljf;)V

    .line 544
    return-void
.end method

.method a(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Ljf;->a:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 535
    return-void
.end method

.method a(Ljw;)V
    .locals 1

    .prologue
    .line 344
    iput-object p1, p0, Ljf;->a:Ljw;

    .line 346
    invoke-virtual {p0}, Ljf;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljw;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 347
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 457
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v1, v0, -0x5

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    or-int/2addr v0, v1

    iput v0, p0, Ljf;->f:I

    .line 458
    return-void

    .line 457
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 148
    iget-object v1, p0, Ljf;->a:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljf;->a:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v1, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    iget-object v1, p0, Ljf;->a:Ljb;

    iget-object v2, p0, Ljf;->a:Ljb;

    invoke-virtual {v2}, Ljb;->a()Ljb;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Ljb;->a(Ljb;Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 156
    iget-object v1, p0, Ljf;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 157
    iget-object v1, p0, Ljf;->a:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 161
    :cond_2
    iget-object v1, p0, Ljf;->a:Landroid/content/Intent;

    if-eqz v1, :cond_3

    .line 163
    :try_start_0
    iget-object v1, p0, Ljf;->a:Ljb;

    invoke-virtual {v1}, Ljb;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ljf;->a:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v1

    .line 166
    const-string v2, "MenuItemImpl"

    const-string v3, "Can\'t find activity to handle intent; ignoring"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    :cond_3
    iget-object v1, p0, Ljf;->a:Ldf;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljf;->a:Ldf;

    invoke-virtual {v1}, Ldf;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 507
    iget v2, p0, Ljf;->f:I

    .line 508
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v3, v0, -0x9

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Ljf;->f:I

    .line 509
    iget v0, p0, Ljf;->f:I

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 508
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b(I)Lch;
    .locals 0

    .prologue
    .line 664
    invoke-virtual {p0, p1}, Ljf;->setShowAsAction(I)V

    .line 665
    return-object p0
.end method

.method b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 483
    iget v2, p0, Ljf;->f:I

    .line 484
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v3, v0, -0x3

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Ljf;->f:I

    .line 485
    iget v0, p0, Ljf;->f:I

    if-eq v2, v0, :cond_0

    .line 486
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 488
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 484
    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0}, Ljb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljf;->a()C

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 566
    if-eqz p1, :cond_0

    .line 567
    iget v0, p0, Ljf;->f:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljf;->f:I

    .line 571
    :goto_0
    return-void

    .line 569
    :cond_0
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Ljf;->f:I

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 461
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public collapseActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 684
    iget v1, p0, Ljf;->g:I

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_1

    .line 697
    :cond_0
    :goto_0
    return v0

    .line 687
    :cond_1
    iget-object v1, p0, Ljf;->a:Landroid/view/View;

    if-nez v1, :cond_2

    .line 689
    const/4 v0, 0x1

    goto :goto_0

    .line 692
    :cond_2
    iget-object v1, p0, Ljf;->a:LdE;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljf;->a:LdE;

    invoke-interface {v1, p0}, LdE;->b(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694
    :cond_3
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->b(Ljf;)Z

    move-result v0

    goto :goto_0
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 718
    iput-boolean p1, p0, Ljf;->a:Z

    .line 719
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 720
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0}, Ljb;->c()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 554
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public expandActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 670
    invoke-virtual {p0}, Ljf;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 679
    :cond_0
    :goto_0
    return v0

    .line 674
    :cond_1
    iget-object v1, p0, Ljf;->a:LdE;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljf;->a:LdE;

    invoke-interface {v1, p0}, LdE;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    :cond_2
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->a(Ljf;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 558
    iget v1, p0, Ljf;->g:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 562
    iget v0, p0, Ljf;->g:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    .prologue
    .line 634
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.getActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Ljf;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Ljf;->a:Landroid/view/View;

    .line 622
    :goto_0
    return-object v0

    .line 618
    :cond_0
    iget-object v0, p0, Ljf;->a:Ldf;

    if-eqz v0, :cond_1

    .line 619
    iget-object v0, p0, Ljf;->a:Ldf;

    invoke-virtual {v0, p0}, Ldf;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ljf;->a:Landroid/view/View;

    .line 620
    iget-object v0, p0, Ljf;->a:Landroid/view/View;

    goto :goto_0

    .line 622
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 237
    iget-char v0, p0, Ljf;->b:C

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Ljf;->b:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Ljf;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Ljf;->a:Landroid/graphics/drawable/Drawable;

    .line 417
    :goto_0
    return-object v0

    .line 410
    :cond_0
    iget v0, p0, Ljf;->e:I

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0}, Ljb;->a()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ljf;->e:I

    invoke-static {v0, v1}, LkD;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 412
    const/4 v1, 0x0

    iput v1, p0, Ljf;->e:I

    .line 413
    iput-object v0, p0, Ljf;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 417
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Ljf;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 203
    iget v0, p0, Ljf;->a:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Ljf;->a:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    .prologue
    .line 255
    iget-char v0, p0, Ljf;->a:C

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Ljf;->c:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Ljf;->a:Ljw;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 352
    iget-object v0, p0, Ljf;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Ljf;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljf;->b:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ljf;->a:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 574
    iget v0, p0, Ljf;->g:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Ljf;->a:Ljw;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 708
    iget v1, p0, Ljf;->g:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    .line 709
    iget-object v1, p0, Ljf;->a:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, Ljf;->a:Ldf;

    if-eqz v1, :cond_0

    .line 710
    iget-object v1, p0, Ljf;->a:Ldf;

    invoke-virtual {v1, p0}, Ldf;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Ljf;->a:Landroid/view/View;

    .line 712
    :cond_0
    iget-object v1, p0, Ljf;->a:Landroid/view/View;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 714
    :cond_1
    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 724
    iget-boolean v0, p0, Ljf;->a:Z

    return v0
.end method

.method public isCheckable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 442
    iget v1, p0, Ljf;->f:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChecked()Z
    .locals 2

    .prologue
    .line 466
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 492
    iget-object v2, p0, Ljf;->a:Ldf;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljf;->a:Ldf;

    invoke-virtual {v2}, Ldf;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 493
    iget v2, p0, Ljf;->f:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_1

    iget-object v2, p0, Ljf;->a:Ldf;

    invoke-virtual {v2}, Ldf;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 495
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 493
    goto :goto_0

    .line 495
    :cond_2
    iget v2, p0, Ljf;->f:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 628
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic setActionView(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Ljf;->a(I)Lch;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Ljf;->a(Landroid/view/View;)Lch;

    move-result-object v0

    return-object v0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 242
    iget-char v0, p0, Ljf;->b:C

    if-ne v0, p1, :cond_0

    .line 250
    :goto_0
    return-object p0

    .line 246
    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Ljf;->b:C

    .line 248
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    goto :goto_0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 447
    iget v2, p0, Ljf;->f:I

    .line 448
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v3, v0, -0x2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Ljf;->f:I

    .line 449
    iget v0, p0, Ljf;->f:I

    if-eq v2, v0, :cond_0

    .line 450
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 453
    :cond_0
    return-object p0

    :cond_1
    move v0, v1

    .line 448
    goto :goto_0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 471
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->a(Landroid/view/MenuItem;)V

    .line 479
    :goto_0
    return-object p0

    .line 476
    :cond_0
    invoke-virtual {p0, p1}, Ljf;->b(Z)V

    goto :goto_0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 185
    iget v0, p0, Ljf;->f:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljf;->f:I

    .line 190
    :goto_0
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 192
    return-object p0

    .line 187
    :cond_0
    iget v0, p0, Ljf;->f:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Ljf;->f:I

    goto :goto_0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 431
    const/4 v0, 0x0

    iput-object v0, p0, Ljf;->a:Landroid/graphics/drawable/Drawable;

    .line 432
    iput p1, p0, Ljf;->e:I

    .line 435
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 437
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 422
    iput v1, p0, Ljf;->e:I

    .line 423
    iput-object p1, p0, Ljf;->a:Landroid/graphics/drawable/Drawable;

    .line 424
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 426
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Ljf;->a:Landroid/content/Intent;

    .line 223
    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 260
    iget-char v0, p0, Ljf;->a:C

    if-ne v0, p1, :cond_0

    .line 268
    :goto_0
    return-object p0

    .line 264
    :cond_0
    iput-char p1, p0, Ljf;->a:C

    .line 266
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    goto :goto_0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 729
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setOnActionExpandListener()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Ljf;->a:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 525
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 273
    iput-char p1, p0, Ljf;->a:C

    .line 274
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Ljf;->b:C

    .line 276
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 278
    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 2

    .prologue
    .line 579
    and-int/lit8 v0, p1, 0x3

    packed-switch v0, :pswitch_data_0

    .line 588
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 591
    :pswitch_0
    iput p1, p0, Ljf;->g:I

    .line 592
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->b(Ljf;)V

    .line 593
    return-void

    .line 579
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Ljf;->b(I)Lch;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0}, Ljb;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljf;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 369
    iput-object p1, p0, Ljf;->a:Ljava/lang/CharSequence;

    .line 371
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 373
    iget-object v0, p0, Ljf;->a:Ljw;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Ljf;->a:Ljw;

    invoke-virtual {v0, p1}, Ljw;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 377
    :cond_0
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 392
    iput-object p1, p0, Ljf;->b:Ljava/lang/CharSequence;

    .line 395
    if-nez p1, :cond_0

    .line 396
    iget-object v0, p0, Ljf;->a:Ljava/lang/CharSequence;

    .line 399
    :cond_0
    iget-object v0, p0, Ljf;->a:Ljb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljb;->b(Z)V

    .line 401
    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 517
    invoke-virtual {p0, p1}, Ljf;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljf;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->a(Ljf;)V

    .line 519
    :cond_0
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Ljf;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
