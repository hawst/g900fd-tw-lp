.class public LQe;
.super LaGN;
.source "PickEntryDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

.field final synthetic a:Lrm;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Lrm;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, LQe;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    iput-object p2, p0, LQe;->a:Lrm;

    iput-object p3, p0, LQe;->a:Landroid/widget/TextView;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 283
    invoke-virtual {p0, p1}, LQe;->a(LaGM;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGM;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, LQe;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    iget-object v1, p0, LQe;->a:Lrm;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LaGM;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 283
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, LQe;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 291
    iget-object v0, p0, LQe;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v0

    .line 292
    if-eqz v0, :cond_0

    .line 293
    iget-object v1, p0, LQe;->a:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 295
    iget-object v1, p0, LQe;->a:Landroid/widget/TextView;

    sget v2, Lxi;->pick_entry_current_folder:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 296
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 295
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 298
    :cond_0
    return-void
.end method
