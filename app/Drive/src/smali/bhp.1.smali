.class public Lbhp;
.super Lbhm;
.source "EditorActionBarBase.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:I

.field private a:Landroid/widget/ImageButton;

.field private a:LbgY;

.field protected a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field protected b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-class v1, Lbhp;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbhp;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lbhm;-><init>(Landroid/app/Activity;)V

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbhp;->a:Ljava/util/Set;

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbhp;->b:Ljava/util/Set;

    .line 63
    return-void
.end method

.method private static a(Landroid/app/Activity;)I
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 388
    .line 390
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    .line 392
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v10, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 393
    const/4 v0, 0x0

    invoke-virtual {p0, v10, v0}, Landroid/app/Activity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 394
    const-string v1, "AndroidManifest.xml"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->openXmlResourceParser(Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v11

    .line 396
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->getEventType()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v1, v0

    move v0, v2

    .line 397
    :goto_0
    if-eq v1, v8, :cond_7

    .line 398
    const/4 v3, 0x2

    if-ne v1, v3, :cond_9

    .line 399
    :try_start_1
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 401
    const-string v3, "application"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 403
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_0

    .line 404
    const-string v3, "logo"

    invoke-interface {v11, v1}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 405
    const/4 v3, 0x0

    invoke-interface {v11, v1, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(II)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :cond_0
    move v1, v0

    .line 445
    :goto_2
    :try_start_2
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->nextToken()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    move v13, v0

    move v0, v1

    move v1, v13

    goto :goto_0

    .line 403
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 409
    :cond_2
    :try_start_3
    const-string v3, "activity"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 415
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    move v4, v2

    move-object v6, v5

    move-object v1, v5

    :goto_3
    if-ltz v3, :cond_6

    .line 417
    invoke-interface {v11, v3}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v7

    .line 418
    const-string v12, "logo"

    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 419
    const/4 v6, 0x0

    invoke-interface {v11, v3, v6}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v7, v6

    move-object v6, v1

    .line 433
    :goto_4
    if-eqz v7, :cond_3

    if-eqz v6, :cond_3

    .line 435
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 415
    :cond_3
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move-object v1, v6

    move-object v6, v7

    goto :goto_3

    .line 420
    :cond_4
    const-string v12, "name"

    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 421
    invoke-interface {v11, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v1

    .line 423
    invoke-virtual {v1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "."

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 424
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 426
    :cond_5
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v7

    if-nez v7, :cond_8

    .line 438
    :cond_6
    if-eqz v4, :cond_9

    .line 450
    :cond_7
    :goto_5
    return v0

    :cond_8
    move v4, v8

    move-object v7, v6

    move-object v6, v1

    .line 429
    goto :goto_4

    .line 447
    :catch_0
    move-exception v0

    move-object v1, v0

    move v0, v2

    .line 448
    :goto_6
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 447
    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v0

    move-object v13, v0

    move v0, v1

    move-object v1, v13

    goto :goto_6

    :cond_9
    move v1, v0

    goto/16 :goto_2

    :cond_a
    move-object v7, v6

    move-object v6, v1

    goto :goto_4
.end method

.method private a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 275
    const/4 v0, 0x1

    .line 276
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 277
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    :cond_0
    return v0
.end method

.method private a(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const v6, 0x102002c

    .line 212
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    .line 214
    invoke-direct {p0}, Lbhp;->a()Landroid/view/ViewGroup;

    move-result-object v4

    .line 215
    if-nez v4, :cond_0

    move-object v0, v1

    .line 265
    :goto_0
    return-object v0

    .line 220
    :cond_0
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v5, p0, Lbhp;->a:Landroid/app/Activity;

    if-ne v3, v6, :cond_2

    sget v0, Lcom/google/bionics/scanner/docscanner/R$attr;->ds_actionbar_compat_item_home_style:I

    :goto_1
    invoke-direct {v2, v5, v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 224
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    .line 225
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-ne v3, v6, :cond_3

    sget v0, Lcom/google/bionics/scanner/docscanner/R$dimen;->ds_actionbar_compat_button_home_width:I

    :goto_2
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    const/4 v5, -0x1

    invoke-direct {v1, v0, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 224
    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 231
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 233
    new-instance v0, Lbhq;

    invoke-direct {v0, p0, p1}, Lbhq;-><init>(Lbhp;Landroid/view/MenuItem;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_delete_page:I

    if-ne v3, v0, :cond_4

    .line 242
    invoke-direct {p0, v4}, Lbhp;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lbhp;->a:I

    .line 243
    iget v0, p0, Lbhp;->a:I

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setId(I)V

    .line 244
    iget-object v0, p0, Lbhp;->b:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 264
    :cond_1
    :goto_3
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v0, v2

    .line 265
    goto :goto_0

    .line 220
    :cond_2
    sget v0, Lcom/google/bionics/scanner/docscanner/R$attr;->ds_actionbar_compat_item_style:I

    goto :goto_1

    .line 225
    :cond_3
    sget v0, Lcom/google/bionics/scanner/docscanner/R$dimen;->ds_actionbar_compat_button_width:I

    goto :goto_2

    .line 245
    :cond_4
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_edit_page:I

    if-ne v3, v0, :cond_5

    .line 246
    invoke-direct {p0, v4}, Lbhp;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lbhp;->b:I

    .line 247
    iget v0, p0, Lbhp;->b:I

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setId(I)V

    .line 248
    iget-object v0, p0, Lbhp;->b:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 249
    :cond_5
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance:I

    if-ne v3, v0, :cond_1

    .line 250
    invoke-direct {p0, v4}, Lbhp;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lbhp;->c:I

    .line 251
    iget v0, p0, Lbhp;->c:I

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setId(I)V

    .line 252
    iget-object v0, p0, Lbhp;->b:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 253
    iput-object v2, p0, Lbhp;->a:Landroid/widget/ImageButton;

    .line 256
    new-instance v0, Lbhr;

    invoke-direct {v0, p0}, Lbhr;-><init>(Lbhp;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3
.end method

.method private a()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$id;->ds_actionbar_compat:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 92
    invoke-direct {p0}, Lbhp;->a()Landroid/view/ViewGroup;

    move-result-object v0

    .line 93
    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 97
    :cond_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v7, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 99
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 102
    new-instance v2, LbhF;

    iget-object v3, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-direct {v2, v3}, LbhF;-><init>(Landroid/content/Context;)V

    .line 103
    new-instance v3, LbhG;

    const v4, 0x102002c

    iget-object v5, p0, Lbhp;->a:Landroid/app/Activity;

    sget v6, Lcom/google/bionics/scanner/docscanner/R$string;->ds_app_name:I

    .line 104
    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v2, v4, v7, v5}, LbhG;-><init>(LbhF;IILjava/lang/CharSequence;)V

    .line 105
    iget-object v2, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-static {v2}, Lbhp;->a(Landroid/app/Activity;)I

    move-result v2

    .line 106
    if-eqz v2, :cond_1

    .line 107
    invoke-virtual {v3, v2}, LbhG;->setIcon(I)Landroid/view/MenuItem;

    .line 111
    :goto_1
    invoke-direct {p0, v3}, Lbhp;->a(Landroid/view/MenuItem;)Landroid/view/View;

    .line 114
    invoke-direct {p0, v0}, Lbhp;->a(Landroid/view/View;)I

    move-result v2

    iput v2, p0, Lbhp;->d:I

    .line 115
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lbhp;->a:Landroid/app/Activity;

    const/4 v4, 0x0

    sget v5, Lcom/google/bionics/scanner/docscanner/R$attr;->ds_actionbar_compat_title_style:I

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 116
    iget v3, p0, Lbhp;->d:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 117
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    iget-object v1, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 109
    :cond_1
    iget-object v2, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v4, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, LbhG;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 196
    new-instance v0, Lbht;

    iget-object v1, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-direct {v0, p0, v1, p1}, Lbht;-><init>(Lbhp;Landroid/content/Context;Landroid/view/MenuInflater;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    .line 68
    return-void
.end method

.method public a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 141
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, Lbhp;->c:I

    if-ne v0, v1, :cond_0

    .line 142
    invoke-super {p0, p1, p2, p3}, Lbhm;->a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 143
    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 144
    sget v1, Lcom/google/bionics/scanner/docscanner/R$menu;->ds_menu_enhance:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 147
    iget-object v0, p0, Lbhp;->a:LbgY;

    invoke-virtual {v0}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v0

    .line 148
    sget-object v1, Lbhs;->a:[I

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 162
    sget-object v1, Lbhp;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Unexpected enhancement method \'%s\' encountered!"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    :goto_0
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_enhance_context_menu_title:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 167
    :cond_0
    return-void

    .line 150
    :pswitch_0
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_none:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 153
    :pswitch_1
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_bw:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 156
    :pswitch_2
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_color:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 159
    :pswitch_3
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_color_drawing:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(LbgY;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lbhp;->a:LbgY;

    .line 180
    return-void
.end method

.method public a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    iget v1, p0, Lbhp;->d:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 185
    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 356
    iget-object v0, p0, Lbhp;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 357
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    .line 359
    :cond_1
    return-void
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lbhp;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 131
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 133
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    .line 174
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 72
    iget-object v1, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x7

    sget v3, Lcom/google/bionics/scanner/docscanner/R$layout;->ds_actionbar_compat:I

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFeatureInt(II)V

    .line 74
    invoke-direct {p0}, Lbhp;->a()V

    .line 76
    new-instance v1, LbhF;

    iget-object v2, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, LbhF;-><init>(Landroid/content/Context;)V

    .line 77
    iget-object v2, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v2, v0, v1}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 78
    :goto_0
    invoke-virtual {v1}, LbhF;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 79
    invoke-virtual {v1, v0}, LbhF;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 80
    iget-object v3, p0, Lbhp;->a:Ljava/util/Set;

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    invoke-direct {p0, v2}, Lbhp;->a(Landroid/view/MenuItem;)Landroid/view/View;

    .line 78
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 85
    iget-object v0, p0, Lbhp;->a:Landroid/app/Activity;

    iget-object v1, p0, Lbhp;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 86
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lbhp;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 369
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 371
    :cond_0
    return-void
.end method
