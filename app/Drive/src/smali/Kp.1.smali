.class public LKp;
.super Ljava/lang/Object;
.source "AnimationManager.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field public a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Point;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a()Landroid/graphics/Point;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    .line 44
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a(Landroid/view/View;)V

    .line 55
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/animation/Animator;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 98
    invoke-virtual {p0, p1}, LKp;->c(Landroid/view/View;)V

    .line 99
    new-instance v0, LKq;

    invoke-direct {v0, p0, p1}, LKq;-><init>(LKp;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 117
    invoke-virtual {p2}, Landroid/animation/Animator;->start()V

    .line 118
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 35
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    iput-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    .line 36
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->b(Landroid/view/View;)V

    .line 64
    :cond_0
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->addView(Landroid/view/View;)V

    .line 73
    :cond_0
    return-void
.end method

.method public d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, LKp;->a:Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->removeView(Landroid/view/View;)V

    .line 82
    :cond_0
    return-void
.end method
