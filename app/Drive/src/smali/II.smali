.class public LII;
.super Ljava/lang/Object;
.source "SectionIndexerFactory.java"


# instance fields
.field private final a:LIF;

.field private final a:LIR;

.field private final a:LIS;


# direct methods
.method constructor <init>(LIS;LIR;LIF;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, LII;->a:LIS;

    .line 22
    iput-object p2, p0, LII;->a:LIR;

    .line 23
    iput-object p3, p0, LII;->a:LIF;

    .line 24
    return-void
.end method


# virtual methods
.method public a(LIK;LDC;)Landroid/widget/SectionIndexer;
    .locals 3

    .prologue
    .line 30
    sget-object v0, LIJ;->a:[I

    invoke-virtual {p1}, LIK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown sortKind:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :pswitch_0
    iget-object v0, p0, LII;->a:LIS;

    invoke-virtual {v0, p2}, LIS;->a(LDC;)Landroid/widget/SectionIndexer;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    .line 34
    :pswitch_1
    iget-object v0, p0, LII;->a:LIR;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, LIR;->a(LDC;Z)Landroid/widget/SectionIndexer;

    move-result-object v0

    goto :goto_0

    .line 40
    :pswitch_2
    iget-object v0, p0, LII;->a:LIR;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, LIR;->a(LDC;Z)Landroid/widget/SectionIndexer;

    move-result-object v0

    goto :goto_0

    .line 42
    :pswitch_3
    iget-object v0, p0, LII;->a:LIF;

    invoke-virtual {v0, p2}, LIF;->a(LDC;)Landroid/widget/SectionIndexer;

    move-result-object v0

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
