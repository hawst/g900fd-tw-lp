.class LbpH;
.super LbmV;
.source "RegularImmutableMultiset.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LbmV",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final transient a:I

.field private final transient a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<TE;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbmL;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmL",
            "<TE;",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, LbmV;-><init>()V

    .line 39
    iput-object p1, p0, LbpH;->a:LbmL;

    .line 40
    iput p2, p0, LbpH;->a:I

    .line 41
    return-void
.end method

.method static synthetic a(LbpH;)LbmL;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LbpH;->a:LbmL;

    return-object v0
.end method

.method static synthetic a(Ljava/util/Map$Entry;)Lbpj;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, LbpH;->b(Ljava/util/Map$Entry;)Lbpj;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/Map$Entry;)Lbpj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map$Entry",
            "<TE;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lbpj",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lbpk;->a(Ljava/lang/Object;I)Lbpj;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, LbpH;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 50
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, LbpH;->a:LbmL;

    invoke-virtual {v0}, LbmL;->c()LbmY;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LbpH;->a:LbmL;

    invoke-virtual {v0}, LbmL;->a()Z

    move-result v0

    return v0
.end method

.method public synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, LbpH;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method c()LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Lbpj",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, LbpJ;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LbpJ;-><init>(LbpH;LbpI;)V

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, LbpH;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, LbpH;->a:LbmL;

    invoke-virtual {v0}, LbmL;->hashCode()I

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, LbpH;->a:I

    return v0
.end method
