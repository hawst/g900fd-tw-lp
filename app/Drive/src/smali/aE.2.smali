.class public LaE;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# instance fields
.field a:I

.field a:LaP;

.field a:Landroid/app/Notification;

.field a:Landroid/app/PendingIntent;

.field a:Landroid/content/Context;

.field a:Landroid/graphics/Bitmap;

.field a:Landroid/os/Bundle;

.field a:Landroid/widget/RemoteViews;

.field a:Ljava/lang/CharSequence;

.field a:Ljava/lang/String;

.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LaA;",
            ">;"
        }
    .end annotation
.end field

.field a:Z

.field b:I

.field b:Landroid/app/Notification;

.field b:Landroid/app/PendingIntent;

.field b:Ljava/lang/CharSequence;

.field b:Ljava/lang/String;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:Z

.field c:I

.field c:Ljava/lang/CharSequence;

.field c:Ljava/lang/String;

.field c:Z

.field d:I

.field d:Ljava/lang/CharSequence;

.field d:Z

.field e:I

.field e:Z

.field f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 844
    const/4 v0, 0x1

    iput-boolean v0, p0, LaE;->a:Z

    .line 854
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaE;->a:Ljava/util/ArrayList;

    .line 855
    iput-boolean v4, p0, LaE;->e:Z

    .line 858
    iput v4, p0, LaE;->e:I

    .line 859
    iput v4, p0, LaE;->f:I

    .line 862
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, LaE;->b:Landroid/app/Notification;

    .line 877
    iput-object p1, p0, LaE;->a:Landroid/content/Context;

    .line 880
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 881
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 882
    iput v4, p0, LaE;->b:I

    .line 883
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaE;->b:Ljava/util/ArrayList;

    .line 884
    return-void
.end method

.method protected static a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1489
    if-nez p0, :cond_1

    .line 1493
    :cond_0
    :goto_0
    return-object p0

    .line 1490
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1491
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 1232
    if-eqz p2, :cond_0

    .line 1233
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1237
    :goto_0
    return-void

    .line 1235
    :cond_0
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method


# virtual methods
.method public a(I)LaE;
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 929
    return-object p0
.end method

.method public a(Landroid/app/PendingIntent;)LaE;
    .locals 0

    .prologue
    .line 1024
    iput-object p1, p0, LaE;->a:Landroid/app/PendingIntent;

    .line 1025
    return-object p0
.end method

.method public a(Landroid/widget/RemoteViews;)LaE;
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1012
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)LaE;
    .locals 1

    .prologue
    .line 952
    invoke-static {p1}, LaE;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LaE;->a:Ljava/lang/CharSequence;

    .line 953
    return-object p0
.end method

.method public a(Z)LaE;
    .locals 1

    .prologue
    .line 1166
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, LaE;->a(IZ)V

    .line 1167
    return-object p0
.end method

.method public a()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 1485
    invoke-static {}, Laz;->a()LaG;

    move-result-object v0

    invoke-interface {v0, p0}, LaG;->a(LaE;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public b(I)LaE;
    .locals 2

    .prologue
    .line 1224
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->defaults:I

    .line 1225
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 1226
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1228
    :cond_0
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)LaE;
    .locals 1

    .prologue
    .line 960
    invoke-static {p1}, LaE;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LaE;->b:Ljava/lang/CharSequence;

    .line 961
    return-object p0
.end method

.method public b(Z)LaE;
    .locals 1

    .prologue
    .line 1175
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, LaE;->a(IZ)V

    .line 1176
    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;)LaE;
    .locals 2

    .prologue
    .line 1069
    iget-object v0, p0, LaE;->b:Landroid/app/Notification;

    invoke-static {p1}, LaE;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1070
    return-object p0
.end method

.method public c(Z)LaE;
    .locals 1

    .prologue
    .line 1186
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, LaE;->a(IZ)V

    .line 1187
    return-object p0
.end method
