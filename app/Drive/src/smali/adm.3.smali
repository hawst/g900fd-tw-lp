.class public Ladm;
.super Ljava/lang/Object;
.source "DocumentFileManagerImpl.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic a:LadY;

.field final synthetic a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

.field final synthetic a:Ljava/io/File;

.field final synthetic a:Ljavax/crypto/SecretKey;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ljavax/crypto/SecretKey;LadY;JLjava/io/File;)V
    .locals 0

    .prologue
    .line 848
    iput-object p1, p0, Ladm;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iput-object p2, p0, Ladm;->a:Ljavax/crypto/SecretKey;

    iput-object p3, p0, Ladm;->a:LadY;

    iput-wide p4, p0, Ladm;->a:J

    iput-object p6, p0, Ladm;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LadY;Ljava/io/File;)V
    .locals 5

    .prologue
    .line 852
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 853
    iget-object v1, p0, Ladm;->a:Ljavax/crypto/SecretKey;

    if-eqz v1, :cond_0

    .line 855
    :try_start_0
    iget-object v1, p0, Ladm;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v1

    iget-object v2, p0, Ladm;->a:Ljavax/crypto/SecretKey;

    invoke-interface {v1, v2, v0}, Lalo;->a(Ljava/security/Key;Ljava/io/OutputStream;)Ljavax/crypto/CipherOutputStream;
    :try_end_0
    .catch LQm; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 862
    :cond_0
    iget-object v1, p0, Ladm;->a:LadY;

    invoke-virtual {v1}, LadY;->b()J

    move-result-wide v2

    .line 863
    new-instance v1, Ladn;

    invoke-direct {v1, p0}, Ladn;-><init>(Ladm;)V

    .line 871
    new-instance v4, Lail;

    invoke-direct {v4, v0, v1, v2, v3}, Lail;-><init>(Ljava/io/OutputStream;LagH;J)V

    .line 873
    invoke-virtual {p1}, LadY;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 874
    iget-object v1, p0, Ladm;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v1

    invoke-interface {v1, v0, v4}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 875
    return-void

    .line 857
    :catch_0
    move-exception v1

    .line 858
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 859
    throw v1
.end method

.method private b(LadY;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 879
    invoke-virtual {p1}, LadY;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 880
    invoke-virtual {p1}, LadY;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadY;

    .line 881
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, LadY;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 882
    invoke-virtual {v0}, LadY;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v3

    if-nez v3, :cond_0

    .line 883
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to create directory "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_0
    invoke-direct {p0, v0, v2}, Ladm;->b(LadY;Ljava/io/File;)V

    goto :goto_0

    .line 888
    :cond_1
    invoke-direct {p0, p1, p2}, Ladm;->a(LadY;Ljava/io/File;)V

    .line 890
    :cond_2
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Ladm;->a:LadY;

    iget-object v1, p0, Ladm;->a:Ljava/io/File;

    invoke-direct {p0, v0, v1}, Ladm;->b(LadY;Ljava/io/File;)V

    .line 895
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 848
    invoke-virtual {p0}, Ladm;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
