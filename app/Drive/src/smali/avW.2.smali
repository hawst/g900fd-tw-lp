.class LavW;
.super Ljava/lang/Object;
.source "CycleRange.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field final synthetic a:LavV;


# direct methods
.method constructor <init>(LavV;)V
    .locals 1

    .prologue
    .line 39
    iput-object p1, p0, LavW;->a:LavV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, LavW;->a:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, LavW;->a:LavV;

    invoke-static {v0}, LavV;->b(LavV;)I

    move-result v0

    iget v1, p0, LavW;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LavW;->a:I

    mul-int/2addr v0, v1

    .line 50
    iget-object v1, p0, LavW;->a:LavV;

    invoke-static {v1}, LavV;->c(LavV;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LavW;->a:LavV;

    invoke-static {v1}, LavV;->a(LavV;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LavW;->a:LavV;

    invoke-static {v1}, LavV;->a(LavV;)I

    move-result v1

    rem-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 44
    iget v0, p0, LavW;->a:I

    iget-object v1, p0, LavW;->a:LavV;

    invoke-static {v1}, LavV;->a(LavV;)I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LavW;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
