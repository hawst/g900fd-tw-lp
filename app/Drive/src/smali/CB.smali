.class public LCB;
.super LLm;
.source "ItemOnDragListener.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final a:LDa;

.field private final a:LJR;

.field private a:Landroid/view/View;

.field private a:Lcom/google/android/apps/docs/doclist/SelectionItem;

.field private a:Z


# direct methods
.method private constructor <init>(LJR;LDa;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, LLm;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, LCB;->a:Z

    .line 53
    iput-object p1, p0, LCB;->a:LJR;

    .line 54
    iput-object p2, p0, LCB;->a:LDa;

    .line 55
    return-void
.end method

.method synthetic constructor <init>(LJR;LDa;LCC;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, LCB;-><init>(LJR;LDa;)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, LCB;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCO;

    invoke-interface {v0, p1}, LCO;->a(Z)V

    .line 134
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 151
    iget-object v0, p0, LCB;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, LCB;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 153
    iput-object v1, p0, LCB;->a:Landroid/view/View;

    .line 155
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, LCB;->a()V

    .line 146
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LCB;->a:Landroid/view/View;

    .line 147
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 148
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, LCB;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 138
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 141
    iput-boolean p1, p0, LCB;->a:Z

    .line 142
    return-void
.end method

.method public a(Landroid/view/View;LLl;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 60
    iget-boolean v0, p0, LCB;->a:Z

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return v1

    .line 64
    :cond_0
    invoke-interface {p2}, LLl;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    move v1, v2

    .line 129
    goto :goto_0

    .line 66
    :pswitch_0
    const-string v0, "ItemOnDragListener"

    const-string v1, "ACTION_DRAG_STARTED"

    invoke-static {v0, v1}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p0, LCB;->a:LJR;

    iget-object v1, p0, LCB;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0, v1}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 77
    :pswitch_1
    const-string v0, "ItemOnDragListener"

    const-string v1, "ACTION_DRAG_ENTERED"

    invoke-static {v0, v1}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    iget-object v0, p0, LCB;->a:LJR;

    iget-object v1, p0, LCB;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0, v1}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    invoke-direct {p0, v2}, LCB;->b(Z)V

    goto :goto_1

    .line 85
    :pswitch_2
    const-string v0, "ItemOnDragListener"

    const-string v1, "ACTION_DRAG_LOCATION"

    invoke-static {v0, v1}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 89
    :pswitch_3
    const-string v0, "ItemOnDragListener"

    const-string v3, "ACTION_DROP"

    invoke-static {v0, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-interface {p2}, LLl;->a()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    if-eqz v0, :cond_1

    .line 91
    invoke-interface {p2}, LLl;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    .line 92
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 93
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a()LLf;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LLf;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    const-string v0, "ItemOnDragListener"

    const-string v2, "Ignore drop without move."

    invoke-static {v0, v2}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_2
    move v2, v0

    .line 109
    goto :goto_1

    .line 98
    :cond_2
    iget-object v0, p0, LCB;->a:LJR;

    iget-object v3, p0, LCB;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0, v3}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 99
    iget-object v0, p0, LCB;->a:LDa;

    iget-object v1, p0, LCB;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, LCB;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0, v1, v3}, LDa;->a(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    move v0, v2

    goto :goto_2

    .line 101
    :cond_3
    iget-object v0, p0, LCB;->a:Landroid/view/View;

    .line 102
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "Cannot move item into itself"

    .line 101
    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 104
    goto :goto_2

    .line 107
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->h()V

    move v0, v2

    goto :goto_2

    .line 113
    :pswitch_4
    const-string v0, "ItemOnDragListener"

    const-string v3, "ACTION_DRAG_EXITED"

    invoke-static {v0, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-direct {p0, v1}, LCB;->b(Z)V

    goto/16 :goto_1

    .line 120
    :pswitch_5
    const-string v0, "ItemOnDragListener"

    const-string v2, "ACTION_DRAG_ENDED"

    invoke-static {v0, v2}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-direct {p0, v1}, LCB;->b(Z)V

    move v2, v1

    .line 123
    goto/16 :goto_1

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
