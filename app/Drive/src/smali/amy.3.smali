.class public Lamy;
.super Ljava/lang/Object;
.source "RateLimitedExecutorImpl.java"

# interfaces
.implements Lamw;


# static fields
.field public static final a:Lamx;


# instance fields
.field private final a:I

.field private a:J

.field private a:LamD;

.field private final a:Ljava/lang/Runnable;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/concurrent/Executor;

.field private final a:Ljava/util/concurrent/ExecutorService;

.field private a:Z

.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lamz;

    invoke-direct {v0}, Lamz;-><init>()V

    sput-object v0, Lamy;->a:Lamx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v1, LamA;

    invoke-direct {v1, p0}, LamA;-><init>(Lamy;)V

    iput-object v1, p0, Lamy;->a:Ljava/lang/Runnable;

    .line 58
    new-instance v1, LamB;

    invoke-direct {v1, p0}, LamB;-><init>(Lamy;)V

    iput-object v1, p0, Lamy;->b:Ljava/lang/Runnable;

    .line 78
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v1

    iput-object v1, p0, Lamy;->a:Ljava/util/concurrent/ExecutorService;

    .line 83
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lamy;->a:J

    .line 86
    iput-boolean v0, p0, Lamy;->a:Z

    .line 89
    sget-object v1, LamD;->a:LamD;

    iput-object v1, p0, Lamy;->a:LamD;

    .line 102
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 103
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lamy;->c:Ljava/lang/Runnable;

    .line 104
    iput p2, p0, Lamy;->a:I

    .line 105
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lamy;->a:Ljava/lang/String;

    .line 106
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lamy;->a:Ljava/util/concurrent/Executor;

    .line 107
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;Lamz;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Lamy;-><init>(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lamy;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lamy;->a:I

    return v0
.end method

.method static synthetic a(Lamy;J)J
    .locals 1

    .prologue
    .line 18
    iput-wide p1, p0, Lamy;->a:J

    return-wide p1
.end method

.method static synthetic a(Lamy;)LamD;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lamy;->a:LamD;

    return-object v0
.end method

.method static synthetic a(Lamy;LamD;)LamD;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lamy;->a:LamD;

    return-object p1
.end method

.method static synthetic a(Lamy;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lamy;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(Lamy;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lamy;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic a(Lamy;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lamy;->a:Z

    return p1
.end method

.method static synthetic b(Lamy;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lamy;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lamy;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lamy;->a:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 6

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lamy;->a:Z

    if-nez v0, :cond_1

    .line 118
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 119
    const-string v2, "RateLimitedExecutor"

    const-string v3, "fire called for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 120
    iget-wide v2, p0, Lamy;->a:J

    sub-long v2, v0, v2

    .line 121
    iget v4, p0, Lamy;->a:I

    int-to-long v4, v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lamy;->a:Z

    .line 123
    const-string v0, "RateLimitedExecutor"

    const-string v1, "Scheduling an updater"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lamy;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, LamC;

    invoke-direct {v1, p0, v2, v3}, LamC;-><init>(Lamy;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :goto_0
    monitor-exit p0

    return-void

    .line 136
    :cond_0
    :try_start_1
    const-string v2, "RateLimitedExecutor"

    const-string v3, "Running updater"

    invoke-static {v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v2, p0, Lamy;->b:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 138
    iput-wide v0, p0, Lamy;->a:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 141
    :cond_1
    :try_start_2
    const-string v0, "RateLimitedExecutor"

    const-string v1, "Dropping request, as an updater is already scheduled."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lamy;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 148
    return-void
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    const-string v0, "RateLimitedExecutor[owner=%s, scheduled=%s, lastUpdated=%s, lapseSinceLastUpdate=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lamy;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lamy;->a:Z

    .line 153
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lamy;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 154
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lamy;->a:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 152
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
