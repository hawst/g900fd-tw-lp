.class public Lamp;
.super Ljava/lang/Object;
.source "PreferenceUtilsImpl.java"

# interfaces
.implements Lamn;


# static fields
.field private static final a:[I


# instance fields
.field private final a:LQr;

.field private a:Lamq;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/io/File;

.field private final a:LtK;

.field private b:Ljava/io/File;

.field private c:Ljava/io/File;

.field private d:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lamp;->a:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x5
        0xa
        0x19
        0x32
        0x64
        0xfa
    .end array-data
.end method

.method public constructor <init>(Laja;LQr;LtK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LQr;",
            "LtK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lamp;->a:Lamq;

    .line 116
    iput-object p1, p0, Lamp;->a:Lbxw;

    .line 117
    iput-object p2, p0, Lamp;->a:LQr;

    .line 118
    iput-object p3, p0, Lamp;->a:LtK;

    .line 119
    return-void
.end method

.method private a()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lamp;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 190
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 200
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 201
    const/4 v1, 0x0

    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 202
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 208
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 211
    :cond_1
    return-object v0
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 234
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235
    invoke-direct {p0, v0}, Lamp;->a(Ljava/io/File;)V

    .line 236
    return-object v0
.end method

.method private a(Lamq;)V
    .locals 5

    .prologue
    .line 354
    invoke-direct {p0}, Lamp;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 355
    const-string v1, "shared_preferences.cache_size"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_0

    .line 357
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lamq;->a(I)V

    .line 360
    :cond_0
    const-string v0, "PreferenceUtils"

    const-string v1, "Cache size set to: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lamq;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 361
    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 240
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    :cond_0
    return-void

    .line 245
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 246
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to create directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - file exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to create cache directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a()[I
    .locals 8

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const/4 v3, 0x0

    .line 295
    invoke-virtual {p0}, Lamp;->b()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x4164000000000000L    # 1.048576E7

    div-double/2addr v4, v6

    .line 297
    sget-object v2, Lamp;->a:[I

    sget-object v6, Lamp;->a:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    aget v2, v2, v6

    int-to-double v6, v2

    cmpl-double v2, v4, v6

    if-lez v2, :cond_0

    .line 298
    sget-object v2, Lamp;->a:[I

    sget-object v4, Lamp;->a:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget v2, v2, v4

    int-to-double v4, v2

    .line 302
    :cond_0
    const-wide v6, 0x408f400000000000L    # 1000.0

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_2

    .line 303
    const-wide v0, 0x406f400000000000L    # 250.0

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/16 v4, 0xfa

    mul-long/2addr v0, v4

    long-to-double v0, v0

    .line 316
    :cond_1
    :goto_0
    sget-object v2, Lamp;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    .line 317
    :goto_1
    sget-object v4, Lamp;->a:[I

    aget v4, v4, v2

    int-to-double v4, v4

    cmpl-double v4, v4, v0

    if-lez v4, :cond_5

    if-ltz v2, :cond_5

    .line 319
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 304
    :cond_2
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_3

    .line 305
    const-wide/high16 v0, 0x4039000000000000L    # 25.0

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/16 v4, 0x19

    mul-long/2addr v0, v4

    long-to-double v0, v0

    goto :goto_0

    .line 306
    :cond_3
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_4

    .line 307
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/16 v4, 0x5

    mul-long/2addr v0, v4

    long-to-double v0, v0

    goto :goto_0

    .line 308
    :cond_4
    cmpl-double v2, v4, v0

    if-ltz v2, :cond_1

    .line 309
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    goto :goto_0

    .line 324
    :cond_5
    if-ltz v2, :cond_8

    sget-object v4, Lamp;->a:[I

    aget v4, v4, v2

    int-to-double v4, v4

    div-double v4, v0, v4

    const-wide/high16 v6, 0x3ff4000000000000L    # 1.25

    cmpg-double v4, v4, v6

    if-gez v4, :cond_8

    .line 327
    add-int/lit8 v2, v2, -0x1

    move v4, v2

    .line 330
    :goto_2
    if-gez v4, :cond_6

    .line 332
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 333
    sget-object v1, Lamp;->a:[I

    aget v1, v1, v3

    aput v1, v0, v3

    .line 350
    :goto_3
    return-object v0

    .line 339
    :cond_6
    add-int/lit8 v2, v4, -0x4

    add-int/lit8 v2, v2, 0x2

    .line 340
    if-gez v2, :cond_7

    move v2, v3

    .line 345
    :cond_7
    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, 0x2

    new-array v4, v4, [I

    .line 347
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    double-to-int v0, v0

    aput v0, v4, v5

    .line 349
    sget-object v0, Lamp;->a:[I

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v2, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v4

    .line 350
    goto :goto_3

    :cond_8
    move v4, v2

    goto :goto_2
.end method

.method private b()V
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lamp;->c:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "createInternalFileDir called while internalFileDir exists"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lamp;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 169
    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "fileinternal"

    invoke-direct {p0, v0, v1, v2}, Lamp;->a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lamp;->c:Ljava/io/File;

    .line 170
    return-void

    .line 166
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lamp;->d:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "createAppMetadataDir called while appMetadataDir exists"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lamp;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 176
    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "appmetadata"

    invoke-direct {p0, v0, v1, v2}, Lamp;->a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lamp;->d:Ljava/io/File;

    .line 177
    return-void

    .line 173
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lamp;->a:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "createFileCacheDir called while cacheDir exists"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 181
    iget-object v0, p0, Lamp;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 182
    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "filecache2"

    invoke-direct {p0, v0, v1, v2}, Lamp;->a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lamp;->a:Ljava/io/File;

    .line 183
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 215
    iget-object v0, p0, Lamp;->b:Ljava/io/File;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "createPinDir called while pinDir exists"

    invoke-static {v0, v3}, LbiT;->b(ZLjava/lang/Object;)V

    .line 217
    iget-object v0, p0, Lamp;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 218
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x8

    if-lt v3, v4, :cond_1

    .line 220
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 227
    :goto_1
    if-nez v0, :cond_2

    .line 228
    new-instance v0, Ljava/io/IOException;

    const-string v1, "External storage not ready"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 215
    goto :goto_0

    .line 222
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 223
    new-instance v3, Ljava/io/File;

    const-string v5, "/Android/data/%s/files/"

    new-array v1, v1, [Ljava/lang/Object;

    .line 224
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v5, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v3

    goto :goto_1

    .line 230
    :cond_2
    const-string v1, "pinned_docs_files_do_not_edit"

    invoke-direct {p0, v0, v1}, Lamp;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lamp;->b:Ljava/io/File;

    .line 231
    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 275
    invoke-virtual {p0}, Lamp;->a()Lamo;

    move-result-object v0

    .line 276
    invoke-interface {v0}, Lamo;->a()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x100000

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public a()Lamo;
    .locals 3

    .prologue
    .line 260
    iget-object v0, p0, Lamp;->a:Lamq;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lamp;->a:Lamq;

    .line 269
    :goto_0
    return-object v0

    .line 264
    :cond_0
    invoke-direct {p0}, Lamp;->a()[I

    move-result-object v0

    .line 265
    new-instance v1, Lamq;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget v2, v0, v2

    div-int/lit8 v2, v2, 0x3

    invoke-direct {v1, p0, v0, v2}, Lamq;-><init>(Lamp;[II)V

    iput-object v1, p0, Lamp;->a:Lamq;

    .line 268
    iget-object v0, p0, Lamp;->a:Lamq;

    invoke-direct {p0, v0}, Lamp;->a(Lamq;)V

    .line 269
    iget-object v0, p0, Lamp;->a:Lamq;

    goto :goto_0
.end method

.method public declared-synchronized a()Ljava/io/File;
    .locals 1

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lamp;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamp;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamp;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lamp;->a:Ljava/io/File;

    .line 131
    invoke-direct {p0}, Lamp;->d()V

    .line 134
    :cond_1
    iget-object v0, p0, Lamp;->a:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lamp;->a:Lamq;

    invoke-direct {p0, v0}, Lamp;->a(Lamq;)V

    .line 283
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lamp;->a:LtK;

    sget-object v1, Lry;->aA:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    invoke-static {}, LadV;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaFO;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 388
    if-nez p1, :cond_0

    .line 389
    const-string v0, "%s.%s"

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "shared_preferences.state"

    aput-object v4, v3, v2

    aput-object p2, v3, v1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 394
    :goto_0
    invoke-direct {p0}, Lamp;->a()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 395
    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v1

    .line 399
    :goto_1
    return v0

    .line 391
    :cond_0
    const-string v0, "%s.%s~%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "shared_preferences.state"

    aput-object v4, v3, v2

    aput-object p2, v3, v1

    aput-object p1, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 398
    :cond_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move v0, v2

    .line 399
    goto :goto_1
.end method

.method public a(LaKS;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 405
    sget-object v1, LaKS;->b:LaKS;

    invoke-virtual {v1, p1}, LaKS;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LaKS;->c:LaKS;

    .line 406
    invoke-virtual {v1, p1}, LaKS;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    invoke-direct {p0}, Lamp;->a()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "shared_preferences.sync_over_wifi_only"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()J
    .locals 4

    .prologue
    .line 286
    invoke-virtual {p0}, Lamp;->a()Ljava/io/File;

    move-result-object v0

    .line 287
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 288
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v2, v0

    .line 289
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v0

    int-to-long v0, v0

    .line 290
    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public declared-synchronized b()Ljava/io/File;
    .locals 1

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lamp;->b:Ljava/io/File;

    if-nez v0, :cond_0

    .line 141
    invoke-direct {p0}, Lamp;->e()V

    .line 144
    :cond_0
    iget-object v0, p0, Lamp;->b:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 371
    invoke-direct {p0}, Lamp;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "streaming_decryption"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized c()Ljava/io/File;
    .locals 1

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lamp;->c:Ljava/io/File;

    if-nez v0, :cond_0

    .line 150
    invoke-direct {p0}, Lamp;->b()V

    .line 153
    :cond_0
    iget-object v0, p0, Lamp;->c:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 382
    invoke-direct {p0}, Lamp;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "enable_pin_encryption"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized d()Ljava/io/File;
    .locals 1

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lamp;->d:Ljava/io/File;

    if-nez v0, :cond_0

    .line 159
    invoke-direct {p0}, Lamp;->c()V

    .line 162
    :cond_0
    iget-object v0, p0, Lamp;->d:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
