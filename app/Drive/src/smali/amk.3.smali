.class public final enum Lamk;
.super Ljava/lang/Enum;
.source "OcrImageEvaluator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lamk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lamk;

.field private static final synthetic a:[Lamk;

.field public static final enum b:Lamk;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lamk;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Lamk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamk;->a:Lamk;

    new-instance v0, Lamk;

    const-string v1, "BLURRED"

    invoke-direct {v0, v1, v3}, Lamk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamk;->b:Lamk;

    const/4 v0, 0x2

    new-array v0, v0, [Lamk;

    sget-object v1, Lamk;->a:Lamk;

    aput-object v1, v0, v2

    sget-object v1, Lamk;->b:Lamk;

    aput-object v1, v0, v3

    sput-object v0, Lamk;->a:[Lamk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lamk;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lamk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lamk;

    return-object v0
.end method

.method public static values()[Lamk;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lamk;->a:[Lamk;

    invoke-virtual {v0}, [Lamk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamk;

    return-object v0
.end method
