.class public LaaR;
.super Ljava/lang/Object;
.source "ThumbnailGenerator.java"


# instance fields
.field private final a:LaaJ;

.field private final a:LalY;

.field private final a:Landroid/content/ContentResolver;

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities;


# direct methods
.method public constructor <init>(LaaJ;Landroid/content/ContentResolver;Lcom/google/android/apps/docs/utils/BitmapUtilities;LalY;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p0, LaaR;->a:LaaJ;

    .line 44
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, LaaR;->a:Landroid/content/ContentResolver;

    .line 45
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/BitmapUtilities;

    iput-object v0, p0, LaaR;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

    .line 46
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalY;

    iput-object v0, p0, LaaR;->a:LalY;

    .line 47
    return-void
.end method

.method private a(LaGn;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 151
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 152
    :goto_0
    invoke-virtual {p1, v0}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 151
    :cond_0
    invoke-static {p2}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 143
    sget-object v0, LaGn;->b:LaGn;

    invoke-direct {p0, v0, p1}, LaaR;->a(LaGn;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 147
    sget-object v0, LaGn;->c:LaGn;

    invoke-direct {p0, v0, p1}, LaaR;->a(LaGn;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 109
    .line 110
    invoke-direct {p0, p2}, LaaR;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p2}, LaaR;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    :cond_0
    iget-object v1, p0, LaaR;->a:LalY;

    invoke-virtual {v1, p1}, LalY;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    iget-object v1, p0, LaaR;->a:LaaJ;

    const-string v2, "orientation"

    invoke-virtual {v1, p1, v2, v0}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v0

    .line 116
    :cond_1
    const-string v1, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 118
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 119
    const-string v2, "Orientation"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 121
    packed-switch v1, :pswitch_data_0

    .line 139
    :cond_2
    :goto_0
    :pswitch_0
    return v0

    .line 123
    :pswitch_1
    add-int/lit8 v0, v0, 0x5a

    .line 124
    goto :goto_0

    .line 126
    :pswitch_2
    add-int/lit16 v0, v0, 0xb4

    .line 127
    goto :goto_0

    .line 129
    :pswitch_3
    add-int/lit16 v0, v0, 0x10e

    .line 130
    goto :goto_0

    .line 134
    :catch_0
    move-exception v1

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p3}, LaaR;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, LaaR;->a:LaaJ;

    const-string v2, "_id"

    invoke-virtual {v0, p1, v2, v4, v5}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;J)J

    move-result-wide v2

    .line 53
    cmp-long v0, v2, v8

    if-lez v0, :cond_4

    .line 54
    iget-object v0, p0, LaaR;->a:Landroid/content/ContentResolver;

    invoke-static {v0, v2, v3, v6, v1}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_4

    .line 102
    :goto_0
    return-object v0

    .line 61
    :cond_0
    invoke-direct {p0, p3}, LaaR;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 62
    invoke-virtual {p0, p1, p3}, LaaR;->a(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v2

    .line 64
    iget-object v0, p0, LaaR;->a:LaaJ;

    const-string v3, "_id"

    invoke-virtual {v0, p1, v3, v4, v5}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;J)J

    move-result-wide v4

    .line 66
    cmp-long v0, v4, v8

    if-lez v0, :cond_1

    .line 67
    iget-object v0, p0, LaaR;->a:Landroid/content/ContentResolver;

    invoke-static {v0, v4, v5, v6, v1}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_1

    .line 71
    invoke-static {v0}, Lakq;->a(Landroid/graphics/Bitmap;)Lakq;

    move-result-object v0

    .line 72
    invoke-virtual {v0, v2}, Lakq;->a(I)Lakq;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lakq;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_1
    :try_start_0
    iget-object v0, p0, LaaR;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

    iget-object v3, p0, LaaR;->a:Landroid/content/ContentResolver;

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/docs/utils/BitmapUtilities;->a(Ljava/io/InputStream;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v3

    .line 82
    const/4 v0, 0x1

    .line 84
    :goto_1
    invoke-virtual {v3}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v4

    mul-int/lit8 v5, v0, 0x2

    div-int/2addr v4, v5

    if-lt v4, p2, :cond_2

    .line 85
    invoke-virtual {v3}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v4

    mul-int/lit8 v5, v0, 0x2

    div-int/2addr v4, v5

    if-ge v4, p2, :cond_3

    .line 90
    :cond_2
    iget-object v3, p0, LaaR;->a:Landroid/content/ContentResolver;

    .line 91
    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v3, v0}, Lakq;->a(Ljava/io/InputStream;I)Lakq;

    move-result-object v0

    .line 92
    invoke-virtual {v0, v2}, Lakq;->a(I)Lakq;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lakq;->a()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_3
    mul-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string v2, "ThumbnailGenerator"

    const-string v3, "Could not open image for thumbnail creation"

    invoke-static {v2, v0, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    move-object v0, v1

    .line 96
    goto :goto_0

    .line 97
    :catch_1
    move-exception v0

    .line 98
    const-string v2, "ThumbnailGenerator"

    const-string v3, "Could not open image for thumbnail creation"

    invoke-static {v2, v0, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    move-object v0, v1

    .line 99
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 102
    goto :goto_0
.end method
