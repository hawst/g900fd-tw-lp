.class LEa;
.super Ljava/lang/Object;
.source "ThumbnailFetchScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:I

.field final synthetic a:LDY;

.field private a:LEb;


# direct methods
.method private constructor <init>(LDY;)V
    .locals 1

    .prologue
    .line 64
    iput-object p1, p0, LEa;->a:LDY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    sget-object v0, LEb;->a:LEb;

    iput-object v0, p0, LEa;->a:LEb;

    .line 66
    const/4 v0, 0x0

    iput v0, p0, LEa;->a:I

    return-void
.end method

.method synthetic constructor <init>(LDY;LDZ;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, LEa;-><init>(LDY;)V

    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, LEa;->a:LDY;

    .line 125
    invoke-static {v0}, LDY;->a(LDY;)LEc;

    move-result-object v0

    invoke-interface {v0, p1}, LEc;->a(I)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    .line 127
    iget-object v1, p0, LEa;->a:LDY;

    invoke-static {v1}, LDY;->a(LDY;)LEc;

    move-result-object v1

    invoke-interface {v1}, LEc;->a()LzN;

    move-result-object v1

    invoke-interface {v1}, LzN;->b()LalS;

    move-result-object v1

    .line 128
    invoke-virtual {v1}, LalS;->a()I

    move-result v2

    .line 129
    invoke-virtual {v1}, LalS;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 131
    if-ge p1, v2, :cond_0

    if-gt p1, v1, :cond_1

    .line 132
    :cond_0
    iget-object v1, p0, LEa;->a:LDY;

    invoke-static {v1}, LDY;->a(LDY;)Lapd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lapd;->b(Ljava/lang/Object;)LbsU;

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v1, p0, LEa;->a:LDY;

    invoke-static {v1}, LDY;->a(LDY;)Lapd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lapd;->b(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    goto :goto_0
.end method

.method private a(III)V
    .locals 1

    .prologue
    .line 114
    if-ltz p3, :cond_0

    iget-object v0, p0, LEa;->a:LDY;

    invoke-static {v0}, LDY;->a(LDY;)LEc;

    move-result-object v0

    invoke-interface {v0}, LEc;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    .line 115
    if-lt p3, p1, :cond_1

    if-gt p3, p2, :cond_1

    .line 116
    invoke-direct {p0, p3}, LEa;->b(I)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    invoke-direct {p0, p3}, LEa;->a(I)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, LEa;->a:LDY;

    invoke-static {v0}, LDY;->a(LDY;)LEc;

    move-result-object v0

    invoke-interface {v0, p1}, LEc;->a(I)LEd;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 141
    sget-object v1, LEb;->a:LEb;

    iget-object v2, p0, LEa;->a:LEb;

    invoke-virtual {v1, v2}, LEb;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, LEd;->a(Z)V

    .line 143
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 69
    invoke-virtual {p0}, LEa;->b()V

    .line 70
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LEa;->a:LEb;

    invoke-virtual {v1}, LEb;->a()J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 71
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 75
    const/4 v0, 0x0

    iput v0, p0, LEa;->a:I

    .line 76
    sget-object v0, LEb;->a:LEb;

    iput-object v0, p0, LEa;->a:LEb;

    .line 77
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 81
    iget-object v0, p0, LEa;->a:LDY;

    invoke-static {v0}, LDY;->a(LDY;)LEc;

    move-result-object v0

    invoke-interface {v0}, LEc;->a()LzN;

    move-result-object v0

    invoke-interface {v0}, LzN;->a()LalS;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, LalS;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {v0}, LalS;->a()I

    move-result v1

    .line 87
    invoke-virtual {v0}, LalS;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 88
    sub-int v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    .line 89
    div-int/lit8 v3, v2, 0x2

    add-int/2addr v3, v1

    .line 90
    iget v4, p0, LEa;->a:I

    add-int/2addr v4, v3

    invoke-direct {p0, v1, v0, v4}, LEa;->a(III)V

    .line 91
    iget v4, p0, LEa;->a:I

    if-lez v4, :cond_1

    .line 92
    iget v4, p0, LEa;->a:I

    sub-int v4, v3, v4

    invoke-direct {p0, v1, v0, v4}, LEa;->a(III)V

    .line 95
    :cond_1
    iget v4, p0, LEa;->a:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LEa;->a:I

    .line 96
    iget v4, p0, LEa;->a:I

    sub-int v4, v3, v4

    sub-int/2addr v1, v4

    iget v4, p0, LEa;->a:I

    add-int/2addr v3, v4

    sub-int v0, v3, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 98
    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 100
    sget-object v1, LEb;->a:LEb;

    iget-object v3, p0, LEa;->a:LEb;

    invoke-virtual {v1, v3}, LEb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 101
    if-lez v0, :cond_2

    .line 102
    sget-object v0, LEb;->b:LEb;

    iput-object v0, p0, LEa;->a:LEb;

    .line 103
    iput v5, p0, LEa;->a:I

    .line 110
    :cond_2
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LEa;->a:LEb;

    invoke-virtual {v1}, LEb;->a()J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 106
    :cond_3
    if-le v0, v2, :cond_2

    goto :goto_0
.end method
