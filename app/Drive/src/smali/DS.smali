.class final LDS;
.super Ljava/lang/Object;
.source "SyncViewState.java"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LDS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 242
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 246
    instance-of v0, p1, LDS;

    if-nez v0, :cond_0

    .line 247
    const/4 v0, 0x0

    .line 250
    :goto_0
    return v0

    .line 249
    :cond_0
    check-cast p1, LDS;

    .line 250
    iget-object v0, p0, LDS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, p1, LDS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 255
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LDS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
